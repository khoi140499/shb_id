﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eTAXAPILib.NewChungTu
{
    public partial class DtlHq
    {
        private string _SHKB;
        private string _NGAY_KB;
        private string _MA_NV;
        private string _SO_BT;
        private string _MA_DTHU;
        private string _MA_CHUONG;
        private string _MA_TMUC;
        private string _NOI_DUNG;
        private string _SOTIEN;
        private string _SOTIEN_NT;
        private string _SO_CT;
        private string _TT_BTOAN;
        private string _MA_HQ;
        private string _MA_LHXNK;
        private string _NGAY_TK;
        private string _SO_TK;
        private string _MA_LT;
        private string _SAC_THUE;

        public string SAC_THUE
        {
            get
            {
                return _SAC_THUE;
            }
            set
            {
                _SAC_THUE = value;
            }
        }

        public string MA_LT
        {
            get
            {
                return _MA_LT;
            }
            set
            {
                _MA_LT = value;
            }
        }

        public string SO_TK
        {
            get
            {
                return _SO_TK;
            }
            set
            {
                _SO_TK = value;
            }
        }

        public string NGAY_TK
        {
            get
            {
                return _NGAY_TK;
            }
            set
            {
                _NGAY_TK = value;
            }
        }

        public string MA_LHXNK
        {
            get
            {
                return _MA_LHXNK;
            }
            set
            {
                _MA_LHXNK = value;
            }
        }

        public string MA_HQ
        {
            get
            {
                return _MA_HQ;
            }
            set
            {
                _MA_HQ = value;
            }
        }

        public string TT_BTOAN
        {
            get
            {
                return _TT_BTOAN;
            }
            set
            {
                _TT_BTOAN = value;
            }
        }

        public string SO_CT
        {
            get
            {
                return _SO_CT;
            }
            set
            {
                _SO_CT = value;
            }
        }

        public string SOTIEN_NT
        {
            get
            {
                return _SOTIEN_NT;
            }
            set
            {
                _SOTIEN_NT = value;
            }
        }

        public string SOTIEN
        {
            get
            {
                return _SOTIEN;
            }
            set
            {
                _SOTIEN = value;
            }
        }

        public string NOI_DUNG
        {
            get
            {
                return _NOI_DUNG;
            }
            set
            {
                _NOI_DUNG = value;
            }
        }

        public string MA_TMUC
        {
            get
            {
                return _MA_TMUC;
            }
            set
            {
                _MA_TMUC = value;
            }
        }

        public string MA_CHUONG
        {
            get
            {
                return _MA_CHUONG;
            }
            set
            {
                _MA_CHUONG = value;
            }
        }

        public string MA_DTHU
        {
            get
            {
                return _MA_DTHU;
            }
            set
            {
                _MA_DTHU = value;
            }
        }

        public string SO_BT
        {
            get
            {
                return _SO_BT;
            }
            set
            {
                _SO_BT = value;
            }
        }

        public string MA_NV
        {
            get
            {
                return _MA_NV;
            }
            set
            {
                _MA_NV = value;
            }
        }

        public string NGAY_KB
        {
            get
            {
                return _NGAY_KB;
            }
            set
            {
                _NGAY_KB = value;
            }
        }

        public string SHKB
        {
            get
            {
                return _SHKB;
            }
            set
            {
                _SHKB = value;
            }
        }
    }

}
