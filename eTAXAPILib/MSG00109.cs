﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG00109
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG00109
    {
        [XmlElement("Header")]
        public HeaderEtaxIN Header;
        [XmlElement("Data")]
        public Data00109 Data;
        public MSG00109()
        { }
        public MSG00109 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG00109), new XmlRootAttribute("ETAX"));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG00109 LoadedObjTmp = (MSG00109)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data00109
    {
        [XmlElement("MA_HS")]
        public string MA_HS { get; set; }
        [XmlElement("MST")]
        public string MST { get; set; }
        [XmlElement("SO")]
        public string SO { get; set; }
        public Data00109()
        {

        }

    }
}
