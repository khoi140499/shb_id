﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG06006
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG06006
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data6006 Data;
        public MSG06006()
        {
            Header = new HeaderEtax("06006");
            Data = new Data6006();
        }
        public string MSG06006toXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG06006));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;
        }
    }
    public class Data6006
    {
        public Data6006()
        {
            Thongtin_NNT = new DKY_NTHUE();
        }
        [XmlElement("DKY_NTHUE")]
        public DKY_NTHUE Thongtin_NNT;
    }
    public class DKY_NTHUE
    {
        public DKY_NTHUE()
        { }
        [XmlElement(Order = 1)]
        public string MST { get; set; }
        [XmlElement(Order = 2)]
        public string TEN_NNT { get; set; }
        [XmlElement(Order = 3)]
        public string DIACHI_NNT { get; set; }
        [XmlElement(Order = 4)]
        public string MA_CQT { get; set; }
        [XmlElement(Order = 5)]
        public string SDT_NNT { get; set; }
        [XmlElement(Order = 6)]
        public string EMAIL_NNT { get; set; }
        [XmlElement(Order = 7)]
        public string TEN_LHE_NTHUE { get; set; }
        [XmlElement(Order = 8)]
        public string SERIAL_CERT_NTHUE { get; set; }
        [XmlElement(Order = 9)]
        public string SUBJECT_CERT_NTHUE { get; set; }
        [XmlElement(Order = 10)]
        public string ISSUER_CERT_NTHUE { get; set; }
        [XmlElement(Order = 11)]
        public string MA_NHANG { get; set; }
        [XmlElement(Order = 12)]
        public string TEN_NHANG { get; set; }
        [XmlElement(Order = 13)]
        public string VAN_ID { get; set; }
        [XmlElement(Order = 14)]
        public string TEN_TVAN { get; set; }
        [XmlElement(Order = 15)]
        public string NGAY_GUI { get; set; }
        [XmlElement(Order = 16)]
        public string TRANG_THAI { get; set; }
        [XmlElement(Order = 17)]
        public string TEN_TRANG_THAI { get; set; }
         [XmlElement(Order = 18)]
        public List<TTIN_TKHOAN> TTIN_TKHOAN;
    }
    public class TTIN_TKHOAN
    {
        public TTIN_TKHOAN()
        { }
         [XmlElement("STK")]
        public string STK { get; set; }
         [XmlElement("TEN_TK")]
        public string TEN_TK { get; set; }
    }
}
