﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG102
{
    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG102
    {
        public MSG102()
        { }
        [XmlElement("Header")]
        public HEADER Header;
        [XmlElement("Data")]
        public Data102 Data;

        public string MSG102toXML(MSG102 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG102));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG102 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(eTAXAPILib.MSG102.MSG102));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            eTAXAPILib.MSG102.MSG102 LoadedObjTmp = (eTAXAPILib.MSG102.MSG102)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class HEADER
    {
        public HEADER()
        {
            Message_Type = "";
            Transaction_Date = "";
            Transaction_ID = "";
            UserID = "";
            Password = "";
            Product_Type = "";
            Request_ID = "";
        }
        [XmlElement("Message_Type")]
        public string Message_Type { get; set; }
        [XmlElement("Transaction_Date")]
        public string Transaction_Date { get; set; }
        [XmlElement("Transaction_ID")]
        public string Transaction_ID { get; set; }
        [XmlElement("UserID")]
        public string UserID { get; set; }
        [XmlElement("Password")]
        public string Password { get; set; }
        [XmlElement("Product_Type")]
        public string Product_Type { get; set; }
        [XmlElement("Request_ID")]
        public string Request_ID { get; set; }

    }
    public class Data102
    {
        public Data102()
        {

        }
        [XmlElement("Loai_DM")]
        public string Loai_DM { get; set; }

        [XmlElement("Ma_DM")]
        public string Ma_DM { get; set; }




    }
}
