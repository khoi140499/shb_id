﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG400
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG400
    {
        [XmlElement("Header")]
        public HeaderEtaxIN Header;
        [XmlElement("Data")]
        public Data400 Data;
        public MSG400()
        { }
        public MSG400 MSGToObject(string p_XML)
        {
            // XmlSerializer serializer = new XmlSerializer(typeof(MSG400), new XmlRootAttribute("ETAX"));
            XmlSerializer serializer = new XmlSerializer(typeof(eTAXAPILib.MSG400.MSG400));
            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG400 LoadedObjTmp = (MSG400)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data400
    {


        [XmlElement("SO_CHUNGTU")]
        public string SO_CHUNGTU { get; set; }
        [XmlElement("LOAI_THUE")]
        public string LOAI_THUE { get; set; }
        [XmlElement("MST")]
        public string MST { get; set; }


        public Data400()
        {

        }
    }

}
