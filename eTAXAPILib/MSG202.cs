﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG202
{
    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG202
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data202 Data;
        [XmlElement("Error")]
        public Error Error;
        public MSG202()
        {
            Header = new HeaderEtax("202");
            Data = new Data202();
            Error = new Error();
        }
        public string MSG202toXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG202));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
    public class Data202
    {
        public Data202()
        {
            MA_CQTHU = "";
            TEN_CQTHU = "";
            MA_KB = "";
            TEN_KB = "";
            MA_DBHC_KBNN = "";
            MA_TINH_KBNN = "";
            MA_NH_B = "";
            TEN_NH_B = "";
            TEN_NH_TT = "";
            MA_NH_TT = "";
        }
        [XmlElement(Order = 1)]
        public string MA_CQTHU { get; set; }
        [XmlElement(Order = 2)]
        public string TEN_CQTHU { get; set; }
        [XmlElement(Order = 3)]
        public string MA_KB { get; set; }
        [XmlElement(Order = 4)]
        public string TEN_KB { get; set; }
        [XmlElement(Order = 5)]
        public string MA_TINH_KBNN { get; set; }
        [XmlElement(Order = 6)]
        public string MA_DBHC_KBNN { get; set; }
        [XmlElement(Order = 7)]
        public string MA_NH_B { get; set; }
        [XmlElement(Order = 8)]
        public string TEN_NH_B { get; set; }
        [XmlElement(Order = 9)]
        public string MA_NH_TT { get; set; }
        [XmlElement(Order = 10)]
        public string TEN_NH_TT { get; set; }
       
    }
    
    

}
