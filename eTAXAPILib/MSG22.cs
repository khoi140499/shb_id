﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG22
{
    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG22
    {
        public MSG22()
        {
        }
        [XmlElement("Header")]
        public HEADER Header;
        [XmlElement("Data")]
        public Data Data;


        public string MSG22toXML(MSG22 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG22));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG22 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(eTAXAPILib.MSG22.MSG22));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            eTAXAPILib.MSG22.MSG22 LoadedObjTmp = (eTAXAPILib.MSG22.MSG22)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data
    {
        public Data()
        {
        }
        [XmlElement("CHUNGTU")]
        public CHUNGTU CHUNGTU;
    }
    public class CHUNGTU
    {
        public CHUNGTU()
        {
            MST = "";
            TEN_NNT = "";
            DIACHI_NNT = "";
            MST_NNTHAY = "";
            TEN_NNTHAY = "";
            DIACHI_NNTHAY = "";
            SO_TK_NHKBNN = "";
            SH_KBNN = "";
            CQ_QLY_THU = "";
            SO_CHUNGTU = "";
            NGAY_CHUNGTU = "";
            TINHCHAT_KHOAN_NOP = "";
            CQUAN_THAMQUYEN = "";
            MA_NHTM = "";
            LOAI_TIEN = "";
            MA_HTHUC_NOP = "";
            DIENGIAI_HTHUC_NOP = "";
            LHINH_NNT = "";
            MA_NH_CHUYEN = "";
            SO_QUYET_DINH = "";
            NGAY_QDINH = "";
            TK_KH_NH = "";
            MA_CN_TK = "";
            FEEAMT = "0";
            VATAMT = "0";
            TEN_CQ_THU = "";
            MA_NTK = "";
            SO_REFCORE = "";
            MA_TINH_KBNN = "";
            SO_KHUNG = "";
            SO_MAY = "";
            DAC_DIEM_PTIEN = "";
            DIA_CHI_TS = "";
            CHUNGTU_CHITIET = new List<CHUNGTU_CHITIET>();
        }
        [XmlElement("MST")]
        public string MST { get; set; }
        [XmlElement("TEN_NNT")]
        public string TEN_NNT { get; set; }
        [XmlElement("DIACHI_NNT")]
        public string DIACHI_NNT { get; set; }
        [XmlElement("MST_NNTHAY")]
        public string MST_NNTHAY { get; set; }
        [XmlElement("TEN_NNTHAY")]
        public string TEN_NNTHAY { get; set; }
        [XmlElement("DIACHI_NNTHAY")]
        public string DIACHI_NNTHAY { get; set; }
        [XmlElement("SO_TK_NHKBNN")]
        public string SO_TK_NHKBNN { get; set; }
        [XmlElement("SH_KBNN")]
        public string SH_KBNN { get; set; }
        [XmlElement("CQ_QLY_THU")]
        public string CQ_QLY_THU { get; set; }
        [XmlElement("SO_CHUNGTU")]
        public string SO_CHUNGTU { get; set; }
        [XmlElement("NGAY_CHUNGTU")]
        public string NGAY_CHUNGTU { get; set; }
        [XmlElement("TINHCHAT_KHOAN_NOP")]
        public string TINHCHAT_KHOAN_NOP { get; set; }
        [XmlElement("CQUAN_THAMQUYEN")]
        public string CQUAN_THAMQUYEN { get; set; }
        [XmlElement("MA_NHTM")]
        public string MA_NHTM { get; set; }
        [XmlElement("LOAI_TIEN")]
        public string LOAI_TIEN { get; set; }
        [XmlElement("MA_HTHUC_NOP")]
        public string MA_HTHUC_NOP { get; set; }
        [XmlElement("DIENGIAI_HTHUC_NOP")]
        public string DIENGIAI_HTHUC_NOP { get; set; }
        [XmlElement("LHINH_NNT")]
        public string LHINH_NNT { get; set; }
        [XmlElement("SO_QUYET_DINH")]
        public string SO_QUYET_DINH { get; set; }
        [XmlElement("NGAY_QDINH")]
        public string NGAY_QDINH { get; set; }
        [XmlElement("TTIEN_NOPTHUE")]
        public string TTIEN_NOPTHUE { get; set; }
        [XmlElement("TK_KH_NH")]
        public string TK_KH_NH { get; set; }
        [XmlElement("MA_CN_TK")]
        public string MA_CN_TK { get; set; }
        [XmlElement("MA_NH_CHUYEN")]
        public string MA_NH_CHUYEN { get; set; }
        [XmlElement("FEEAMT")]
        public string FEEAMT { get; set; }
        [XmlElement("VATAMT")]
        public string VATAMT { get; set; }
        [XmlElement("TEN_CQ_THU")]
        public string TEN_CQ_THU { get; set; }
        [XmlElement("MA_NTK")]
        public string MA_NTK { get; set; }

        [XmlElement("SO_REFCORE")]
        public string SO_REFCORE { get; set; }
        [XmlElement("MA_TINH_KBNN")]
        public string MA_TINH_KBNN { get; set; }
        [XmlElement("SO_KHUNG")]
        public string SO_KHUNG { get; set; }
        [XmlElement("SO_MAY")]
        public string SO_MAY { get; set; }
        [XmlElement("DAC_DIEM_PTIEN")]
        public string DAC_DIEM_PTIEN { get; set; }
        [XmlElement("DIA_CHI_TS")]
        public string DIA_CHI_TS { get; set; }
        [XmlElement("CHUNGTU_CHITIET")]
        public List<CHUNGTU_CHITIET> CHUNGTU_CHITIET;
    }
    public class CHUNGTU_CHITIET
    {
        public CHUNGTU_CHITIET()
        {
            CHUONG = "";
            TIEUMUC = "";
            THONGTIN_KHOANNOP = "";
            SO_TIEN = "";
            KY_THUE = "";
        }
        [XmlElement("CHUONG")]
        public string CHUONG { get; set; }
        [XmlElement("TIEUMUC")]
        public string TIEUMUC { get; set; }
        [XmlElement("THONGTIN_KHOANNOP")]
        public string THONGTIN_KHOANNOP { get; set; }
        [XmlElement("SO_TIEN")]
        public string SO_TIEN { get; set; }
        [XmlElement("KY_THUE")]
        public string KY_THUE { get; set; }
    }
    public class HEADER
    {
        public HEADER()
        {
            Message_Type = "";
            Transaction_Date = "";
            Transaction_ID = "";
            UserID = "";
            Password = "";
            Product_Type = "";
            Request_ID = "";
        }
        [XmlElement("Message_Type")]
        public string Message_Type { get; set; }
        [XmlElement("Transaction_Date")]
        public string Transaction_Date { get; set; }
        [XmlElement("Transaction_ID")]
        public string Transaction_ID { get; set; }
        [XmlElement("UserID")]
        public string UserID { get; set; }
        [XmlElement("Password")]
        public string Password { get; set; }
        [XmlElement("Product_Type")]
        public string Product_Type { get; set; }
        [XmlElement("Request_ID")]
        public string Request_ID { get; set; }

    }
}
