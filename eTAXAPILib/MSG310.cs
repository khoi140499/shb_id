﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG310
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG310
    {
        [XmlElement("Header")]
        public HeaderEtaxIN Header;
        [XmlElement("Data")]
        public Data6005 Data;
        public MSG310()
        { }
        public MSG310 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG310), new XmlRootAttribute("ETAX"));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG310 LoadedObjTmp = (MSG310)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data6005
    {
        [XmlElement("MST")]
        public string MST { get; set; }
        public Data6005()
        {

        }

    }
}
