﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG301
{
    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG301
    {
        public MSG301()
        { }
        [XmlElement("Header")]
        public HEADER Header;
        [XmlElement("Data")]
        public Data301 Data;

        public string MSG301toXML(MSG301 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG301));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG301 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(eTAXAPILib.MSG301.MSG301));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            eTAXAPILib.MSG301.MSG301 LoadedObjTmp = (eTAXAPILib.MSG301.MSG301)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class HEADER
    {
        public HEADER()
        {
            Message_Type = "";
            Transaction_Date = "";
            Transaction_ID = "";
            UserID = "";
            Password = "";
            Product_Type = "";
            Request_ID = "";
        }
        [XmlElement("Message_Type")]
        public string Message_Type { get; set; }
        [XmlElement("Transaction_Date")]
        public string Transaction_Date { get; set; }
        [XmlElement("Transaction_ID")]
        public string Transaction_ID { get; set; }
        [XmlElement("UserID")]
        public string UserID { get; set; }
        [XmlElement("Password")]
        public string Password { get; set; }
        [XmlElement("Product_Type")]
        public string Product_Type { get; set; }
        [XmlElement("Request_ID")]
        public string Request_ID { get; set; }

    }
    public class Data301
    {
        public Data301()
        {
            Ma_NH_TH = "";
            Ten_NH_TH = "";
            Ma_DV = "";
            Ma_Chuong = "";
            Ten_DV = "";
            Ma_KB = "";
            TKKB = "";
            Ma_NTK = "";
            Ma_HQ_PH = "";
            Ten_HQ_PH = "";
            Ma_HQ_CQT = "";
            So_CT = "";
            Ten_CQT = "";
            Ngay_BN = "";
            Ngay_BC = "";
            Ma_NT = "";
            Ty_Gia = "";
            SoTien_TO = "0";
            DienGiai = "";
            TK_KH_NH = "";
            MA_NH_CHUYEN = "";
            FEEAMT = "0";
            VATAMT = "0";

            SO_REFCORE = "";

            MA_CUC = "";
            MA_TINH_KBNN = "";
            GNT_CT = new List<GNT_CT>();
        }
        [XmlElement("Ma_NH_TH")]
        public string Ma_NH_TH { get; set; }
        [XmlElement("Ten_NH_TH")]
        public string Ten_NH_TH { get; set; }
        [XmlElement("Ma_DV")]
        public string Ma_DV { get; set; }
        [XmlElement("Ma_Chuong")]
        public string Ma_Chuong { get; set; }
        [XmlElement("Ten_DV")]
        public string Ten_DV { get; set; }

        [XmlElement("DiaChi_DV")]
        public string DiaChi_DV { get; set; }

        [XmlElement("Ma_KB")]
        public string Ma_KB { get; set; }
        [XmlElement("Ten_KB")]
        public string Ten_KB { get; set; }
        [XmlElement("TKKB")]
        public string TKKB { get; set; }
        [XmlElement("Ma_NTK")]
        public string Ma_NTK { get; set; }
        [XmlElement("Ma_HQ_PH")]
        public string Ma_HQ_PH { get; set; }
        [XmlElement("Ten_HQ_PH")]
        public string Ten_HQ_PH { get; set; }
        [XmlElement("Ma_HQ_CQT")]
        public string Ma_HQ_CQT { get; set; }
        [XmlElement("So_CT")]
        public string So_CT { get; set; }
        [XmlElement("Ten_CQT")]
        public string Ten_CQT { get; set; }
        [XmlElement("Ngay_BN")]
        public string Ngay_BN { get; set; }
        [XmlElement("Ngay_BC")]
        public string Ngay_BC { get; set; }
        [XmlElement("Ngay_CT")]
        public string Ngay_CT { get; set; }
        [XmlElement("Ma_NT")]
        public string Ma_NT { get; set; }
        [XmlElement("Ty_Gia")]
        public string Ty_Gia { get; set; }
        [XmlElement("SoTien_TO")]
        public string SoTien_TO { get; set; }
        [XmlElement("DienGiai")]
        public string DienGiai { get; set; }
        [XmlElement("TK_KH_NH")]
        public string TK_KH_NH { get; set; }
        [XmlElement("MA_NH_CHUYEN")]
        public string MA_NH_CHUYEN { get; set; }
        [XmlElement("FEEAMT")]
        public string FEEAMT { get; set; }
        [XmlElement("VATAMT")]
        public string VATAMT { get; set; }
        [XmlElement("SO_REFCORE")]
        public string SO_REFCORE { get; set; }

        [XmlElement("MA_CUC")]
        public string MA_CUC { get; set; }
        [XmlElement("MA_TINH_KBNN")]
        public string MA_TINH_KBNN { get; set; }

        [XmlElement("GNT_CT")]
        public List<GNT_CT> GNT_CT { get; set; }



        //GNT_CT					




    }
    public class GNT_CT
    {
        public GNT_CT()
        {
            TTButToan = "";
            Ma_HQ = "";
            Ma_LH = "";
            Ngay_DK = "";
            So_TK = "";
            Ma_LT = "";
            ToKhai_CT = new List<ToKhai_CT>();

        }
        [XmlElement("TTButToan")]
        public string TTButToan { get; set; }
        [XmlElement("Ma_HQ")]
        public string Ma_HQ { get; set; }
        [XmlElement("Ma_LH")]
        public string Ma_LH { get; set; }
        [XmlElement("Ngay_DK")]
        public string Ngay_DK { get; set; }
        [XmlElement("So_TK")]
        public string So_TK { get; set; }
        [XmlElement("Ma_LT")]
        public string Ma_LT { get; set; }
           [XmlElement("ToKhai_CT")]
        public List<ToKhai_CT> ToKhai_CT { get; set; }



    }
    public class ToKhai_CT
    {
        public ToKhai_CT()
        {
            Ma_ST = "";
            NDKT = "";
            SoTien_NT = "0";
            SoTien_VND = "0";

        }
        [XmlElement("Ma_ST")]
        public string Ma_ST { get; set; }
        [XmlElement("NDKT")]
        public string NDKT { get; set; }
        [XmlElement("SoTien_NT")]
        public string SoTien_NT { get; set; }
        [XmlElement("SoTien_VND")]
        public string SoTien_VND { get; set; }

    }
}
