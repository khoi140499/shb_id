﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG401b
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG401b
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data401a Data;
        public MSG401b()
        {
            Header = new HeaderEtax("401b");
            Data = new Data401a();
        }
        public string MSG401btoXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG401b));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;
        }
    }
    public class Data401a
    {
        public Data401a()
        {

        }
        [XmlElement(Order = 1)]
        public string Ma_NH_TH { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_NH_TH { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_DV { get; set; }
        [XmlElement(Order = 4)]
        public string Ma_Chuong { get; set; }
        [XmlElement(Order = 5)]
        public string Ten_DV { get; set; }
        [XmlElement(Order = 6)]
        public string DiaChi_DV { get; set; }
        [XmlElement(Order = 7)]
        public string Ma_KB { get; set; }
        [XmlElement(Order = 8)]
        public string Ten_KB { get; set; }
        [XmlElement(Order = 9)]
        public string TKKB { get; set; }
        [XmlElement(Order = 10)]
        public string Ma_NTK { get; set; }
        [XmlElement(Order = 11)]
        public string Ma_HQ_PH { get; set; }
        [XmlElement(Order = 12)]
        public string Ten_HQ_PH { get; set; }
        [XmlElement(Order = 13)]
        public string Ma_HQ_CQT { get; set; }
        [XmlElement(Order = 14)]
        public string So_CT { get; set; }
        [XmlElement(Order = 15)]
        public string Ngay_BN { get; set; }
        [XmlElement(Order = 16)]
        public string Ngay_BC { get; set; }
        [XmlElement(Order = 17)]
        public string Ngay_CT { get; set; }
        [XmlElement(Order = 18)]
        public string Ma_NT { get; set; }
        [XmlElement(Order = 19)]
        public string Ty_Gia { get; set; }
        [XmlElement(Order = 20)]
        public string SoTien_TO { get; set; }
        [XmlElement(Order = 21)]
        public string DienGiai { get; set; }
        [XmlElement(Order = 22)]
        public string TK_KH_NH { get; set; }
        [XmlElement(Order = 23)]
        public string MA_NH_CHUYEN { get; set; }
        [XmlElement(Order = 24)]
        public string FEEAMT { get; set; }
        [XmlElement(Order = 25)]
        public string VATAMT { get; set; }
        [XmlElement(Order = 26)]
        public string TRANG_THAI{ get; set; }
            [XmlElement(Order = 27)]
        public string TEN_TRANG_THAI { get; set; }
        [XmlElement(Order = 28)]
        public List<GNT_CT> GNT_CT;
    }
    public class GNT_CT
    {
        [XmlElement(Order = 1)]
        public string TTButToan { get; set; }
        [XmlElement(Order = 2)]
        public string Ma_HQ { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_LH { get; set; }
        [XmlElement(Order = 4)]
        public string Ngay_DK { get; set; }
        [XmlElement(Order = 5)]
        public string So_TK { get; set; }
        [XmlElement(Order = 6)]
        public string Ma_LT { get; set; }
        [XmlElement(Order = 7)]
        public List<ToKhai_CT> ToKhai_CT;
        public GNT_CT()
        { }



    }
    public class ToKhai_CT
    {
        public ToKhai_CT()
        { }
        [XmlElement(Order = 1)]
        public string Ma_ST { get; set; }
        [XmlElement(Order = 2)]
        public string NDKT { get; set; }
        [XmlElement(Order = 3)]
        public string THONGTIN_KHOANNOP { get; set; }
        [XmlElement(Order = 4)]
        public string SoTien_NT { get; set; }
        [XmlElement(Order = 5)]
        public string SoTien_VND { get; set; }

    }
}
