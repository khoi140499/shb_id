﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eTAXAPILib
{
    public class ErrorEtax
    {
        public ErrorEtax()
        { }

        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
       
    }
}
