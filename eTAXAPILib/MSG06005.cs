﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG06005
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG06005
    {
        [XmlElement("Header")]
        public HeaderEtaxIN Header;
        [XmlElement("Data")]
        public Data6005 Data;
        public MSG06005()
        { }
        public MSG06005 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG06005), new XmlRootAttribute("ETAX"));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG06005 LoadedObjTmp = (MSG06005)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data6005
    {
        [XmlElement("MST")]
        public string MST { get; set; }
        public Data6005()
        {

        }

    }
}
