﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using VBOracleLib;
using System.Xml;
using System.Text.RegularExpressions;
using System.Data.OracleClient;
using System.Diagnostics;
namespace eTAXAPILib
{
    public class SexyLib
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public const String strXMLdefin1 = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
        public const String strXMLdefin2 = "<DATA xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
        public const String strXMLdefin3 = "<ETAX xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
        public const String strXMLdefin4 = "<ETAX>";
        public const String strXMLdefin5 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
        public const String strXMLdefout1 = " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
        public const String strXMLdefout2 = " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"";
        public const String strXMLdefout3 = " xmlns=\"http://www.cpandl.com\"";
        public const String strXMLdefout4 = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
        public const String strXMLdefout5 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        public static string GenerateKey(string strSeq, int intLength = 0)
        {
            string strReSult = "";
            try
            {
                string strSQL = "SELECT LPAD(" + strSeq + ".NEXTVAL," + intLength + ",'0')  FROM DUAL";
                if (intLength == 0)
                    strSQL = "SELECT " + strSeq + ".NEXTVAL  FROM DUAL";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            strReSult = ds.Tables[0].Rows[0][0].ToString();
                if (intLength > 0 && strReSult.Length <= intLength)
                    strReSult = strReSult.PadLeft(intLength, '0');
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
            }
            return strReSult;
        }
        public static void insertLog(string strID, string strREQID, string strREQTYPE, string strREQMSG, string strPRODUCT_TYPE)
        {

            try
            {
                //log.Info("Insert TCS_API_LOG_TF_COREBANK: strID:" + strID + "/ strREQID:" + strREQID + "/ strREQTYPE:" + strREQTYPE + "/ strREQMSG:" + strREQMSG + "/ strPRODUCT_TYPE:" + strPRODUCT_TYPE + ".");

                //TCS_API_LOG_TF_COREBANK
                string strSQL = "INSERT INTO TCS_API_LOG_TF_COREBANK(ID ,REQID ,REQTYPE,REQMSG,REQTIME,PRODUCT_TYPE)";
                strSQL += " VALUES(:ID,:REQID,:REQTYPE,:REQMSG,sysdate,:PRODUCT_TYPE)";
                List<IDbDataParameter> lst = new List<IDbDataParameter>();
                lst.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, strID, DbType.String));
                lst.Add(DataAccess.NewDBParameter("REQID", ParameterDirection.Input, strREQID, DbType.String));
                lst.Add(DataAccess.NewDBParameter("REQTYPE", ParameterDirection.Input, strREQTYPE, DbType.String));
                lst.Add(DataAccess.NewDBParameter("REQMSG", ParameterDirection.Input, strREQMSG, DbType.String));
                lst.Add(DataAccess.NewDBParameter("PRODUCT_TYPE", ParameterDirection.Input, strPRODUCT_TYPE, DbType.String));
                DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text, lst.ToArray());

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
        public static string FomatOutString(string strXML)
        {
            string strResult = strXML;
            strResult = strResult.Replace(strXMLdefout1, "");
            strResult = strResult.Replace(strXMLdefout2, "");
            strResult = strResult.Replace(strXMLdefout3, "");
            strResult = strResult.Replace(strXMLdefout4, "");
            strResult = strResult.Replace(strXMLdefout5, "");
            return strResult.Substring(2);
        }
        public static void UPDATELog(string strID, string strRESID, string strRESTYPE, string strRESMSG, string strErrCode, string strErrMSG)
        {
            try
            {
                //TCS_API_LOG_TF_COREBANK
                string strSQL = "UPDATE TCS_API_LOG_TF_COREBANK ";
                strSQL += " SET RESID = :RESID, RESTYPE = :RESTYPE,RESMSG = :RESMSG,RESERRCODE = :RESERRCODE,RESERRMSG = :RESERRMSG,RESTIME=Sysdate ";
                strSQL += " WHERE ID=:ID";
                List<IDbDataParameter> lst = new List<IDbDataParameter>();
                lst.Add(DataAccess.NewDBParameter("RESID", ParameterDirection.Input, strRESID, DbType.String));
                lst.Add(DataAccess.NewDBParameter("RESTYPE", ParameterDirection.Input, strRESTYPE, DbType.String));
                lst.Add(DataAccess.NewDBParameter("RESMSG", ParameterDirection.Input, strRESMSG, DbType.String));
                lst.Add(DataAccess.NewDBParameter("RESERRCODE", ParameterDirection.Input, strErrCode, DbType.String));
                lst.Add(DataAccess.NewDBParameter("RESERRMSG", ParameterDirection.Input, strErrMSG, DbType.String));
                lst.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, strID, DbType.String));

                DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text, lst.ToArray());

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
        /// <summary>
        /// Ham valid header
        /// </summary>
        /// <param name="pvStrMSG"></param>
        /// <param name="pvStrErrCode"></param>
        /// <param name="pvStrErrMSG"></param>
        /// <param name="pvStrTransactionID"></param>
        /// <param name="pvStrMessage_Type"></param>
        /// <param name="pvStrProductType"></param>
        /// <param name="pvStrReQuestID"></param>
        /// <returns></returns>
        public static bool ValidHeaderMSG(string pvStrMSG, ref string pvStrErrCode, ref string pvStrErrMSG, ref string pvStrTransactionID,
           ref string pvStrMessage_Type, ref string pvStrProductType, ref string pvStrReQuestID)
        {
            bool vresult = true;
            try
            {
                string msg = RemoveSpecialCharacter(pvStrMSG);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(msg);
                XmlNodeList nodeList = xmlDoc.SelectNodes("ETAX/Header");
                if (nodeList == null || nodeList.Count == 0)
                {
                    pvStrErrCode = "90000";
                    pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Header");

                    return false;
                }
                //data
                nodeList = xmlDoc.SelectNodes("ETAX/Data");
                if (nodeList == null || nodeList.Count == 0)
                {
                    pvStrErrCode = "90000";
                    pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Data");
                    return false;
                }
                // Message_Type
                if (xmlDoc.SelectSingleNode("ETAX/Header/Message_Type") != null)
                {
                    pvStrMessage_Type = xmlDoc.SelectSingleNode("ETAX/Header/Message_Type").InnerText.Trim();
                    if (string.IsNullOrEmpty(pvStrMessage_Type))
                    {
                        pvStrErrCode = "90000";
                        pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Message_Type");
                        return false;
                    }
                }
                else
                {
                    pvStrErrCode = "90000";
                    pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Message_Type");
                    return false;
                }
                // UserID
                string strUserID = "";
                if (xmlDoc.SelectSingleNode("ETAX/Header/UserID") != null)
                {
                    strUserID = xmlDoc.SelectSingleNode("ETAX/Header/UserID").InnerText.Trim();
                    if (string.IsNullOrEmpty(strUserID))
                    {
                        pvStrErrCode = "90000";
                        pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "UserID");
                        return false;
                    }

                }
                else
                {
                    pvStrErrCode = "90000";
                    pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "UserID");
                    return false;
                }
                string strPassword = "";
                string checkPass = "1";
                try
                {
                    checkPass = System.Configuration.ConfigurationSettings.AppSettings["API.IS_CHECK_PASSWORD"].ToString();
                }
                catch (Exception ex)
                {
                    checkPass = "1";
                }
                if (checkPass != "0")
                {
                    if (xmlDoc.SelectSingleNode("ETAX/Header/Password") != null)
                    {
                        strPassword = xmlDoc.SelectSingleNode("ETAX/Header/Password").InnerText.Trim();
                        if (string.IsNullOrEmpty(strPassword))
                        {
                            pvStrErrCode = "90000";
                            pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Password");
                            return false;
                        }

                    }
                    else
                    {
                        pvStrErrCode = "90000";
                        pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Password");
                        return false;
                    }
                }
                //Product_Type
                if (xmlDoc.SelectSingleNode("ETAX/Header/Transaction_ID") != null)
                {
                    pvStrTransactionID = xmlDoc.SelectSingleNode("ETAX/Header/Transaction_ID").InnerText.Trim();
                    if (string.IsNullOrEmpty(pvStrTransactionID))
                    {
                        pvStrErrCode = "90000";
                        pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Transaction_ID");
                        return false;
                    }
                    // check lenght transaction id
                    if (pvStrTransactionID.Length > 50)
                    {
                        pvStrErrCode = "90029";
                        pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Transaction_ID");
                        return false;
                    }
                }
                else
                {
                    pvStrErrCode = "90000";
                    pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Transaction_ID");
                    return false;
                }
                // Product_Type
                if (xmlDoc.SelectSingleNode("ETAX/Header/Product_Type") != null)
                {
                    pvStrProductType = xmlDoc.SelectSingleNode("ETAX/Header/Product_Type").InnerText.Trim();
                    if (string.IsNullOrEmpty(pvStrProductType))
                    {
                        pvStrErrCode = "90000";
                        pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Product_Type");
                        return false;
                    }
                }
                else
                {
                    pvStrErrCode = "90000";
                    pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Product_Type");
                    return false;
                }
                // Request_ID
                if (xmlDoc.SelectSingleNode("ETAX/Header/Request_ID") != null)
                {
                    pvStrReQuestID = xmlDoc.SelectSingleNode("ETAX/Header/Request_ID").InnerText.Trim();
                }
                else
                {
                    pvStrErrCode = "90000";
                    pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Request_ID");
                    return false;
                }

                //Transaction_Date
                if (xmlDoc.SelectSingleNode("ETAX/Header/Transaction_Date ") != null)
                {
                    string strTranDate = xmlDoc.SelectSingleNode("ETAX/Header/Transaction_Date").InnerText.Trim();

                    if (string.IsNullOrEmpty(strTranDate))
                    {
                        pvStrErrCode = "90000";
                        pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Transaction_Date");

                        return false;
                    }
                }
                else
                {
                    pvStrErrCode = "90000";
                    pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Transaction_Date");

                    return false;
                }
                if (!string.IsNullOrEmpty(pvStrTransactionID))
                {
                    if (!checkValidIDRequest(pvStrTransactionID))
                    {
                        // pvStrErrMSG = "Transaction is already exist";
                        pvStrErrCode = "91111";
                        pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Request_ID");
                        return false;
                    }
                    // check lenght Request_ID
                    if (pvStrReQuestID.Length > 50)
                    {
                        pvStrErrCode = "90029";
                        pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", "Request_ID");
                        return false;
                    }
                }
                //kiem tra user name password
                if (!validUser(strUserID, strPassword, checkPass))
                {
                    pvStrErrCode = "99998";
                    pvStrErrMSG = "User Or Password wrong, pls checking ";
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Info(pvStrMSG);
                log.Error(ex.StackTrace + ex.Message);
                vresult = false;
                // pvStrErrCode = "90000";
                // pvStrErrMSG = "Message is invalid format ";
                pvStrErrCode = "99999";
                pvStrErrMSG = getErrorMSG(pvStrErrCode).Replace("[_REPLACE_]", ex.Message);
            }
            return vresult;
        }
        public static bool validUser(string pvStrUserID, string pv_strPassword,string checkPass)
        {
            bool resSult = false;
            try
            {
                string strSQL = "select * from tcs_dm_nhanvien where UPPER (ma_nv) = UPPER ('" + pvStrUserID.ToUpper() + "') and tinh_trang='0'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string strPass = Security.DescryptStr(ds.Tables[0].Rows[0]["MAT_KHAU"].ToString());
                            byte[] xbyte = System.Text.Encoding.UTF8.GetBytes(strPass);
                            strPass = Convert.ToBase64String(xbyte);
                            resSult = true;
                            if (checkPass != "0")
                            {
                                if (!pv_strPassword.Equals(strPass)) resSult = false;
                            }
                        }
            }
            catch (Exception ex)
            {

            }
            return resSult;


        }
        /// <summary>
        /// Ham xoa cac ky tu dac biet
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public static string RemoveSpecialCharacter(string strInput)
        {
            string patern = @"[^0-9a-zA-Z<>\-_:/\s\p{L}\.\,]+";
            string result = string.Empty;
            try
            {
                result = Regex.Replace(strInput, patern, "");
            }
            catch (Exception ex)
            {

                throw;
            }
            return strInput;
        }
        /// <summary>
        /// ham get error message
        /// </summary>
        /// <param name="pvStrErrCode"></param>
        /// <returns></returns>
        public static string getErrorMSG(string pvStrErrCode)
        {
            string strReMSG = "Error Undefined";
            try
            {
                DataSet ds = DataAccess.ExecuteReturnDataSet("select * from TCS_DEFERROR where errcode='" + pvStrErrCode + "'", CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            strReMSG = ds.Tables[0].Rows[0]["DESCIPTION"].ToString();

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return strReMSG;
        }
        /// <summary>
        /// Ham kiem tra trung request id
        /// </summary>
        /// <param name="strId"></param>
        /// <returns></returns>
        public static bool checkValidIDRequest(string strId)
        {
            bool vReSult = true;
            try
            {

                //TCS_API_LOG_TF_COREBANK
                string strSql = "SELECT * from TCS_API_LOG_TF_COREBANK WHERE REQID='" + strId + "'";

                DataSet ds = DataAccess.ExecuteReturnDataSet(strSql, System.Data.CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            vReSult = false;
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return vReSult;

        }
        /// <summary>
        /// Ham valid message obj21
        /// </summary>
        /// <param name="obj21"></param>
        /// <param name="strErrCode"></param>
        /// <param name="strErrMSG"></param>
        /// <returns></returns>
        public static bool checkValidObj21(MSG21.MSG21 obj21, ref string strErrCode, ref string strErrMSG)
        {
            bool bResult = false;
            try
            {
                if (obj21.Data.CHUNGTU == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "CHUNGTU");
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.MST, "MST", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.TEN_NNT, "TEN_NNT", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.DIACHI_NNT, "DIACHI_NNT", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj21.Data.CHUNGTU.MST_NNTHAY, "MST", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj21.Data.CHUNGTU.TEN_NNTHAY, "TEN_NNT", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj21.Data.CHUNGTU.DIACHI_NNTHAY, "DIACHI_NNT", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.SO_TK_NHKBNN, "SO_TK_NHKBNN", 15, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.SH_KBNN, "SH_KBNN", 5, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.CQ_QLY_THU, "CQ_QLY_THU", 7, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.SO_CHUNGTU, "SO_CHUNGTU", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidDateIsRequired(obj21.Data.CHUNGTU.NGAY_CHUNGTU, "NGAY_CHUNGTU", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.TINHCHAT_KHOAN_NOP, "TINHCHAT_KHOAN_NOP", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj21.Data.CHUNGTU.CQUAN_THAMQUYEN, "CQUAN_THAMQUYEN", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.MA_NHTM, "MA_NHTM", 8, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.LOAI_TIEN, "LOAI_TIEN", 3, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.MA_HTHUC_NOP, "MA_HTHUC_NOP", 2, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj21.Data.CHUNGTU.DIENGIAI_HTHUC_NOP, "DIENGIAI_HTHUC_NOP", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj21.Data.CHUNGTU.LHINH_NNT, "LHINH_NNT", 8, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.TK_KH_NH, "TK_KH_NH", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.MA_NH_CHUYEN, "MA_NH_CHUYEN", 8, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj21.Data.CHUNGTU.SO_QUYET_DINH, "SO_QUYET_DINH", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //---------------------------
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.MA_NTK, "MA_NTK", 10, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.TEN_CQ_THU, "TEN_CQTHU", 500, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //---------------------------
                if (!CheckValidStringIsNotRequired(obj21.Data.CHUNGTU.NGAY_QDINH, "NGAY_QDINH", 10, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                else
                {
                    if (obj21.Data.CHUNGTU.NGAY_QDINH.Length > 0)
                    {
                        if (!CheckValidDateIsRequired(obj21.Data.CHUNGTU.NGAY_QDINH, "NGAY_QDINH", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                        {
                            return false;
                        }
                    }
                }
                if (!CheckValidNumberIsRequired(obj21.Data.CHUNGTU.TTIEN_NOPTHUE, "TTIEN_NOPTHUE", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (obj21.Data.CHUNGTU.CHUNGTU_CHITIET == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "CHUNGTU_CHITIET");
                    return false;
                }
                if (obj21.Data.CHUNGTU.CHUNGTU_CHITIET.Count <= 0)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "CHUNGTU_CHITIET");
                    return false;
                }
                for (int i = 0; i < obj21.Data.CHUNGTU.CHUNGTU_CHITIET.Count; i++)
                {
                    if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].CHUONG, "CHUONG_" + (i + 1), 3, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].TIEUMUC, "TIEUMUC_" + (i + 1), 4, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].THONGTIN_KHOANNOP, "THONGTIN_KHOANNOP_" + (i + 1), 200, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidNumberIsRequired(obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].SO_TIEN, "SO_TIEN_" + (i + 1), 20, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].KY_THUE, "KY_THUE_" + (i + 1), 20, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return bResult;
        }
        public static bool CheckValidStringIsRequired(string input, string tagName, int lenght, ref string errorCode, ref string errorMessage)
        {
            // Neu la message 301C 

            try
            {
                if (string.IsNullOrEmpty(input.Trim()))
                {
                    errorCode = "90028";
                    errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                    return false;
                }
                if (lenght > 0)
                {
                    if (input.Length > lenght)
                    {
                        errorCode = "90029";
                        errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                errorCode = "90028";
                errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                log.Error(ex.Message);
                return false;
            }



            return true;
        }
        public static bool CheckValidDateIsRequired(string input, string tagName, int lenght, ref string errorCode, ref string errorMessage, string pvStrFormat)
        {
            try
            {
                if (string.IsNullOrEmpty(input.Trim()))
                {
                    errorCode = "90028";
                    errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                    return false;
                }
                if (lenght > 0)
                {
                    if (input.Length > lenght)
                    {
                        errorCode = "90029";
                        errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                        return false;
                    }
                }
                if (!validDate(input, pvStrFormat))
                {
                    errorCode = "90000";
                    errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                    return false;
                }
                // Business_HQ.Common.mdlCommon.ConvertStringToDate(
            }
            catch (Exception ex)
            {
                errorCode = "90028";
                errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                log.Error(ex.Message);
                return false;
            }


            return true;
        }

        public static bool validDate(string pvStrData, string pvStrFormat)
        {
            bool vbool = false;
            int vDay = 0;
            int vMon = 0;
            int vYear = 0;
            int vHours = 0;
            int vMinute = 0;
            int vSeconds = 0;

            try
            {
                if (pvStrData.Length < pvStrFormat.Length)
                {
                    return false;
                }
                if (pvStrFormat.ToUpper().IndexOf("T") > 0)
                {
                    if (pvStrFormat.ToUpper().Equals("YYYY-MM-DDTHH:MM:SS"))
                    {
                        vYear = int.Parse(pvStrData.Substring(0, 4));
                        vMon = int.Parse(pvStrData.Substring(5, 2));
                        vDay = int.Parse(pvStrData.Substring(8, 2));
                        vHours = int.Parse(pvStrData.Substring(11, 2));
                        vMinute = int.Parse(pvStrData.Substring(14, 2));
                        vSeconds = int.Parse(pvStrData.Substring(17, 2));
                    }
                    else if (pvStrFormat.ToUpper().Equals("YYYY/MM/DDTHH:MM:SS"))
                    {
                        vYear = int.Parse(pvStrData.Substring(0, 4));
                        vMon = int.Parse(pvStrData.Substring(5, 2));
                        vDay = int.Parse(pvStrData.Substring(8, 2));
                        vHours = int.Parse(pvStrData.Substring(11, 2));
                        vMinute = int.Parse(pvStrData.Substring(14, 2));
                        vSeconds = int.Parse(pvStrData.Substring(17, 2));
                    }
                    else
                    { return false; }

                    DateTime dd = new DateTime(vYear, vMon, vDay, vHours, vMinute, vSeconds);
                    return true;
                }
                else
                {
                    if (pvStrFormat.ToUpper().Equals("YYYY-MM-DD"))
                    {
                        vYear = int.Parse(pvStrData.Substring(0, 4));
                        vMon = int.Parse(pvStrData.Substring(5, 2));
                        vDay = int.Parse(pvStrData.Substring(8, 2));

                    }
                    else if (pvStrFormat.ToUpper().Equals("YYYY/MM/DD"))
                    {
                        vYear = int.Parse(pvStrData.Substring(0, 4));
                        vMon = int.Parse(pvStrData.Substring(5, 2));
                        vDay = int.Parse(pvStrData.Substring(8, 2));

                    }
                    else if (pvStrFormat.ToUpper().Equals("DD/MM/YYYY"))
                    {
                        vDay = int.Parse(pvStrData.Substring(0, 2));
                        vYear = int.Parse(pvStrData.Substring(3, 2));
                        vMon = int.Parse(pvStrData.Substring(6, 4));
                    }
                    else if (pvStrFormat.ToUpper().Equals("DD-MM-YYYY"))
                    {
                        vDay = int.Parse(pvStrData.Substring(0, 2));
                        vYear = int.Parse(pvStrData.Substring(3, 2));
                        vMon = int.Parse(pvStrData.Substring(6, 4));
                    }
                    else
                    { return false; }

                    DateTime dd = new DateTime(vYear, vMon, vDay);
                    return true;
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return false;
            }
            return vbool;
        }
        public static bool CheckValidStringIsNotRequired(string input, string tagName, int lenght, ref string errorCode, ref string errorMessage)
        {
            try
            {
                if (input.Length == 0)
                {
                    errorCode = @"0";
                    errorMessage = @"";
                    return true;
                }
                if (string.IsNullOrEmpty(input))
                {
                    errorCode = @"0";
                    errorMessage = @"";
                    return true;
                }
                if (lenght > 0)
                {
                    if (input.Length > lenght)
                    {
                        errorCode = "90029";
                        errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                errorCode = "90000";
                errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                log.Error(ex.Message);
                return false;
            }


            return true;
        }
        /// <summary>
        /// hiepvx them ham nay de valid so
        /// </summary>
        /// <param name="input"></param>
        /// <param name="tagName"></param>
        /// <param name="lenght"></param>
        /// <param name="errorCode"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool CheckValidNumberIsRequired(string input, string tagName, int lenght, ref string errorCode, ref string errorMessage)
        {
            try
            {
                if (string.IsNullOrEmpty(input.Trim()))
                {
                    errorCode = "90028";
                    errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                    return false;
                }
                if (lenght > 0)
                {
                    if (input.Length > lenght)
                    {
                        errorCode = "90029";
                        errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                        return false;
                    }
                }
                try
                {
                    var a = decimal.Parse(input);
                    if (a < 0)
                    {
                        errorCode = "90000";
                        errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                    errorCode = "90000";
                    errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                errorCode = "90028";
                errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName);
                log.Error(ex.Message);
                return false;
            }
            return true;
        }
        /// <summary>
        /// CheckValidNumber
        /// </summary>
        /// <param name="input"></param>
        /// <param name="tagName"></param>
        /// <param name="lenght"></param>
        /// <param name="errorCode"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool CheckValidNumber(string input, string tagName, int lenght, ref string errorCode, ref string errorMessage)
        {
            try
            {
                if (string.IsNullOrEmpty(input.Trim()))
                {
                    errorCode = @"90005";
                    errorMessage = @"Tag " + tagName + " is required.";
                    return false;
                }
                if (lenght > 0)
                {
                    if (input.Length > lenght)
                    {
                        errorCode = @"90029";
                        errorMessage = getErrorMSG(errorCode).Replace("[_REPLACE_]", tagName); ;
                        return false;
                    }
                }
                try
                {
                    var a = decimal.Parse(input);

                }
                catch (Exception ex)
                {
                    errorCode = @"90000";
                    errorMessage = @"Message is invalid format. Tag " + tagName + " is invalid.";
                    log.Error(ex.Message);
                    return false;
                }
            }
            catch (Exception ex)
            {
                errorCode = @"90005";
                errorMessage = @"Tag " + tagName + " is required.";
                log.Error(ex.Message);
            }
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool CheckIsValidNumber(string input)
        {
            try
            {
                var dc = Convert.ToDecimal(input.Trim());

            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
        /// <summary>
        /// Get ngay lam viec he thong
        /// </summary>
        /// <param name="strMa_NV"></param>
        /// <returns></returns>
        public static string TCS_GetNgayLV(string strMa_NV)
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV'";
                return ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
        }
        /// <summary>
        /// get ma chi nhanh cua nhan vien
        /// </summary>
        /// <param name="strMa_NV"></param>
        /// <returns></returns>
        public static string TCS_GetMACN_NV(string strMa_NV)
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE UPPER(ma_nv) = '" + strMa_NV.ToUpper() + "'";
                return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
        }
        /// <summary>
        /// get so but toan de thuc hien chung tu
        /// </summary>
        /// <param name="iNgayLV"></param>
        /// <param name="iMaNV"></param>
        /// <returns></returns>
        public static int Get_SoBT(int iNgayLV, int iMaNV)
        {
            int iResult = 0;
            try
            {
                string strSql = "Select MAX(So_BT) AS So_BT From TCS_CTU_HDR Where ngay_kb=" + iNgayLV.ToString() + " AND Ma_NV=" + iMaNV.ToString();
                DataTable dt = DatabaseHelp.ExecuteToTable(strSql);
                if (!Globals.IsNullOrEmpty(dt))
                    iResult = int.Parse(dt.Rows[0][0].ToString()) + 1;
            }
            catch (Exception ex)
            {
                iResult = 1;
            }
            return iResult;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static string ConvertDateToNumber(string strDate)
        {
            string[] arr;

            try
            {

                strDate = strDate.Trim();
                if (strDate.Length < 10)
                    return "0";
                arr = strDate.Split('/');

                arr[0] = arr[0].PadLeft(2, '0');
                arr[1] = arr[1].PadLeft(2, '0');
                if (arr[2].Length == 2)
                    arr[2] = "20" + arr[2];
            }
            catch (Exception ex)
            {
                return DateTime.Now.ToString("yyyyMMdd");
            }

            return arr[2] + arr[1] + arr[0];
        }
        public static string TCS_GetKHCT(string strSHKB, string strMaNV = "", string strLoaiGD = "01")
        {
            try
            {
                string strSQL = "";
                string strResult = "";

                if (strLoaiGD.Equals("01"))
                {
                    strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT'";
                    return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                }
                else
                {
                    try
                    {
                        string strGetCN = "select ma_cn from tcs_dm_nhanvien where ma_nv = '" + strMaNV + "'";
                        string strMaCN = DataAccess.ExecuteSQLScalar(strGetCN);

                        if (strLoaiGD == "04")
                        {
                            strSQL = "select substr(max(kyhieu_ct),0,3) || LPAD(TO_NUMBER(substr(max(kyhieu_ct),4,7))+1,7,0) KHCT from tcs_ctu_hdr ";
                            strSQL += "where ma_nv in (select ma_nv from tcs_dm_nhanvien where ma_cn like '" + strMaCN + "%') and ma_lthue = '04'";
                        }
                        else if (strLoaiGD == "05")
                        {
                            strSQL = "select substr(max(kyhieu_ct),0,3) || LPAD(TO_NUMBER(substr(max(kyhieu_ct),4,7))+1,7,0) KHCT from tcs_baolanh_hdr where ma_nv in (select ma_nv from tcs_dm_nhanvien where ma_cn like '" + strMaCN + "%') ";
                        }
                        DataTable dt = DatabaseHelp.ExecuteToTable(strSQL);
                        if (!Globals.IsNullOrEmpty(dt))
                        {
                            strResult = dt.Rows[0][0].ToString();
                            if (strResult == "")
                            {
                                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT'";
                                string strKHCT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                                strResult = strKHCT.Substring(0, 3) + "0000001";
                            }
                        }
                        else
                        {
                            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' ";
                            string strKHCT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                            strResult = strKHCT.Substring(0, 3) + "0000001";
                        }
                    }
                    catch (Exception exc)
                    {
                        strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' ";
                        string strKHCT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                        strResult = strKHCT.Substring(0, 3) + "0000001";
                    }
                    return strResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                return "";
            }
        }
        public static string ToValue(string value, string typefield, string strFormat = "")
        {
            string tmp = "";
            //'convert to from Unicode to TCVN before insert DB
            if (Convert.IsDBNull(value))
            {
                value = "' '";
                return value;
            }
            value = value.Trim();
            if (typefield.Trim().ToUpper().Equals("NUMBER"))
            {
                if (value != "")
                {
                    tmp = value.Replace(",", ".");
                    //    'Neu dau vao cho Value la Nothing thi ham tra ve chuoi "null"
                }
                else
                    tmp = "null";

            }
            else if (typefield.Trim().ToUpper().Equals("DATE"))
            {
                if (value != "")
                {
                    if (strFormat == "")
                        strFormat = "dd/MM/rrrr HH24:MI:ss";

                    tmp = "to_date('" + value + "','" + strFormat + "')";
                }
                else
                    tmp = "null";

            }

            return tmp;
        }
        /// <summary>
        /// chuyen doi tu obj21 sang cacs object HDR vaf dtl
        /// </summary>
        /// <param name="obj21"></param>
        /// <param name="hdr"></param>
        /// <param name="lstDtl"></param>
        public static void setObject21ToCTU(MSG21.MSG21 obj21, ref NewChungTu.infChungTuHDR hdr, ref List<NewChungTu.infChungTuDTL> lstDtl)
        {
            try
            {
                hdr.Ngay_KB = int.Parse(TCS_GetNgayLV(obj21.Header.UserID));
                string str_nam_kb = "";
                string str_thang_kb = "";
                str_nam_kb = hdr.Ngay_KB.ToString().Substring(2, 4);

                hdr.Ma_CN = TCS_GetMACN_NV(obj21.Header.UserID);
                //' Mã NV
                hdr.Ma_NV = int.Parse(obj21.Header.UserID);
                // ' Số bút toán
                hdr.So_BT = Get_SoBT(hdr.Ngay_KB, hdr.Ma_NV);
                //' Số CT
                hdr.So_CT = str_nam_kb + str_thang_kb + GenerateKey("TCS_CTU_ID_SEQ", 6);

                //'Trang thai bds hien tai
                hdr.TT_BDS = "";

                hdr.TT_NSTT = "N";
                //'SHKB
                hdr.SHKB = obj21.Data.CHUNGTU.SH_KBNN;
                //' Mã điểm thu
                hdr.Ma_DThu = hdr.SHKB;
                //' Mã NNTien
                hdr.KyHieu_CT = TCS_GetKHCT(hdr.SHKB);
                hdr.Ma_NNTien = obj21.Data.CHUNGTU.MST_NNTHAY;

                //' Tên NNTien
                hdr.Ten_NNTien = obj21.Data.CHUNGTU.TEN_NNTHAY;

                //' Địa chỉ NNTien
                hdr.DC_NNTien = obj21.Data.CHUNGTU.DIACHI_NNTHAY;

                //' Mã NNT
                hdr.Ma_NNThue = obj21.Data.CHUNGTU.MST;

                //' Tên NNT
                hdr.Ten_NNThue = obj21.Data.CHUNGTU.TEN_NNT;
                //' Địa chỉ NNT
                hdr.DC_NNThue = obj21.Data.CHUNGTU.DIACHI_NNT;
                //' Số bút toán FT
                hdr.So_CT_NH = obj21.Data.CHUNGTU.SO_CHUNGTU;
                // ' hdr.Ten_NH_B = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ten_NH_B).InnerText
                //  ' Lý do
                hdr.Ly_Do = "";
                //  ' Mã TQ
                hdr.Ma_TQ = "0";

                hdr.So_QD = obj21.Data.CHUNGTU.SO_QUYET_DINH;
                hdr.CQ_QD = obj21.Data.CHUNGTU.CQUAN_THAMQUYEN;
                hdr.Ngay_QD = ToValue(obj21.Data.CHUNGTU.NGAY_QDINH, "DATE", "RRRR-MM-DD");

                ////  ' Ngay CT
                hdr.Ngay_CT = int.Parse(obj21.Data.CHUNGTU.NGAY_CHUNGTU.Replace("-", ""));// clsCTU.TCS_GetNgayLV(pv_intMaNV)
                ////  ' Ngay HT
                hdr.Ngay_HT = ToValue(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE");

                hdr.Ma_CQThu = obj21.Data.CHUNGTU.CQ_QLY_THU;
                hdr.Ten_cqthu = obj21.Data.CHUNGTU.TEN_CQ_THU;
                //  'THEM MA SAN PHAM
                hdr.MA_NH_TT = ""; //v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_TT).InnerText
                hdr.SAN_PHAM = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_SANPHAM).InnerText
                hdr.TT_CITAD = "0";// 'v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TT_CITAD).InnerText
                hdr.MA_NTK = "1";
                //  ' DBHC
                hdr.Ma_Xa = "";
                hdr.XA_ID = "";// Get_XaID(hdr.Ma_Xa)
                hdr.Ma_Huyen = "";
                hdr.Ma_Tinh = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_DBHC_NNTIEN).InnerText

                //  ' TK No
                hdr.TK_No = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TKNo).InnerText.Replace(".", "").Replace(" ", "")
                hdr.TEN_TK_NO = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenTKNo).InnerText
                string v_strSHKB = obj21.Data.CHUNGTU.SH_KBNN;
                string v_strMaDBHC = obj21.Data.CHUNGTU.SH_KBNN;


                hdr.TK_GL_NH = "NULL";


                //  ' TK Co
                hdr.TK_Co = obj21.Data.CHUNGTU.SO_TK_NHKBNN;
                hdr.TEN_TK_CO = obj21.Data.CHUNGTU.TINHCHAT_KHOAN_NOP;
                //  ' So TKhai
                hdr.So_TK = "";
                hdr.Ngay_TK = "null";


                //  ' LHXNK
                hdr.LH_XNK = "";
                //  ' DVSDNS
                hdr.DVSDNS = "";
                hdr.Ten_DVSDNS = "";


                hdr.Ma_NT = obj21.Data.CHUNGTU.LOAI_TIEN;
                hdr.Ty_Gia = 1.0;// ChungTu.buChungTu.get_tygia(hdr.Ma_NT, hdr.TG_ID)
                hdr.TG_ID = "null";
                //    ' Mã Loại Thuế
                hdr.Ma_LThue = "01";
                hdr.Kenh_CT = "02";
                //  ' So Khung - So May
                hdr.So_Khung = "";
                hdr.So_May = "";
                //  'Phuong thuc thanh toan
                hdr.PT_TT = obj21.Data.CHUNGTU.MA_HTHUC_NOP;
                hdr.TT_TThu = double.Parse(obj21.Data.CHUNGTU.TTIEN_NOPTHUE);
                hdr.TTien = double.Parse(obj21.Data.CHUNGTU.TTIEN_NOPTHUE);
                hdr.Mode = "0";




                // 'KIENVT : THEM THONG TIN VE NGAN HANG
                hdr.MA_NH_A = obj21.Data.CHUNGTU.MA_NH_CHUYEN;
                hdr.MA_NH_B = obj21.Data.CHUNGTU.MA_NHTM;// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_B).InnerText 'strMaNH.Split(";")(0).ToString
                hdr.MA_NH_TT = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_TT).InnerText 'strMaNH.Split(";")(2).ToString
                hdr.Ten_NH_B = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ten_NH_B).InnerText
                hdr.TEN_NH_TT = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_NH_TT).InnerText
                string strSQL = "select * from tcs_dm_nh_giantiep_tructiep where shkb='" + hdr.SHKB + "' and ma_giantiep='" + obj21.Data.CHUNGTU.MA_NHTM + "'";
                DataSet dsNH_B = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (dsNH_B != null)
                    if (dsNH_B.Tables.Count > 0)
                        if (dsNH_B.Tables[0].Rows.Count > 0)
                        {
                            hdr.MA_NH_B = dsNH_B.Tables[0].Rows[0]["ma_giantiep"].ToString();
                            hdr.MA_NH_TT = dsNH_B.Tables[0].Rows[0]["ma_tructiep"].ToString();
                            hdr.Ten_NH_B = dsNH_B.Tables[0].Rows[0]["ten_giantiep"].ToString();
                            hdr.TEN_NH_TT = dsNH_B.Tables[0].Rows[0]["ten_tructiep"].ToString();
                        }
                //  ' hoapt sua ma ngan hang truc tiep gian tiep
                //   Dim strMaNH As String = GetTT_NH_NHAN(v_strSHKB)


                //  'Them NH_HUONG
                hdr.NH_HUONG = obj21.Data.CHUNGTU.MA_NHTM;
                //  ' TK_KH_NH 
                hdr.TK_KH_NH = obj21.Data.CHUNGTU.TK_KH_NH;
                hdr.TEN_KH_NH = "";


                //  strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.NGAY_KH_NH).InnerText
                hdr.NGAY_KH_NH = ToValue(obj21.Data.CHUNGTU.NGAY_CHUNGTU, "DATE", "RRRR-MM-DD");

                hdr.TK_KH_Nhan = "";
                hdr.Ten_KH_Nhan = "";
                hdr.Diachi_KH_Nhan = "";

                //  'THEM PHI GIAO DỊCH,VAT
                hdr.PHI_GD = obj21.Data.CHUNGTU.FEEAMT;// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_GD).InnerText.Replace(".", "")
                hdr.PHI_VAT = obj21.Data.CHUNGTU.VATAMT; ;// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_VAT).InnerText.Replace(".", "")
                hdr.PHI_GD2 = "0";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_GD2).InnerText.Replace(".", "")
                hdr.PHI_VAT2 = "0";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_VAT2).InnerText.Replace(".", "")
                hdr.PT_TINHPHI = "0";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PT_TINHPHI).InnerText.Replace(".", "")

                //  ' quận_huyên NNTien
                hdr.QUAN_HUYEN_NNTIEN = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Huyen_NNTIEN).InnerText
                //  ' Tinh _NNTien
                hdr.TINH_TPHO_NNTIEN = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Tinh_NNTIEN).InnerText
                //  Dim ref_no As String = String.Empty

                hdr.RM_REF_NO = hdr.So_CT;
                hdr.REF_NO = hdr.So_CT;

                //  ' So BK
                hdr.So_BK = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.So_BK).InnerText
                hdr.TIME_BEGIN = ToValue(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE");
                hdr.SO_CMND = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SO_CMND).InnerText
                hdr.SO_FONE = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SO_FONE).InnerText
                //  ' Ngay BK
                hdr.Ngay_BK = "null";


                hdr.Lan_In = "0";

                hdr.Ma_KS = hdr.Ma_NV.ToString();

                hdr.Trang_Thai = "07";
                hdr.LOAI_NNT = obj21.Data.CHUNGTU.LHINH_NNT;
                hdr.TEN_LOAI_NNT = "";
                hdr.TTien_NT = 0.0;
                for (int i = 0; i < obj21.Data.CHUNGTU.CHUNGTU_CHITIET.Count; i++)
                {
                    NewChungTu.infChungTuDTL dtl = new NewChungTu.infChungTuDTL();
                    dtl.SO_CT = hdr.So_CT;
                    // 'So tu tang
                    dtl.ID = GenerateKey("TCS_CTU_DTL_SEQ");
                    //                    ' SHKB
                    dtl.SHKB = hdr.SHKB;

                    //  ' Ngày KB
                    dtl.Ngay_KB = hdr.Ngay_KB;

                    //' Mã NV
                    dtl.Ma_NV = hdr.Ma_NV;

                    //' Số Bút toán
                    dtl.So_BT = hdr.So_BT;

                    // ' Mã điểm thu
                    dtl.Ma_DThu = hdr.Ma_DThu;

                    // 'Mã quỹ
                    dtl.MaQuy = "";

                    //  ' Mã Cấp
                    dtl.Ma_Cap = "";

                    //  ' Mã Chương
                    dtl.Ma_Chuong = obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].CHUONG;
                    //' CCH_ID
                    dtl.CCH_ID = "null";
                    //' Mã Loại
                    dtl.Ma_Loai = "";

                    //' Mã Khoản
                    dtl.Ma_Khoan = "";// v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.LKhoan).InnerText

                    //' CCH_ID
                    dtl.LKH_ID = "null";//Get_LKHID(dtl.Ma_Khoan)

                    //' Mã Mục
                    dtl.Ma_Muc = "";

                    //' Mã TMục
                    dtl.Ma_TMuc = obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].TIEUMUC;

                    //' MTM_ID
                    dtl.MTM_ID = "null";//Get_MTMucID(dtl.Ma_TMuc)

                    //' Mã TLDT
                    dtl.Ma_TLDT = "null";
                    //' DT_ID
                    dtl.DT_ID = "null";

                    //  '  Nội dung
                    dtl.Noi_Dung = obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].THONGTIN_KHOANNOP;


                    dtl.SoTien_NT = 0.0;
                    dtl.SoTien = double.Parse(obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].SO_TIEN);
                    hdr.TTien_NT += dtl.SoTien;

                    // ' Kỳ thuế
                    dtl.Ky_Thue = obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].KY_THUE;
                    // ' Mã dự phòng
                    dtl.Ma_DP = "null";
                    lstDtl.Add(dtl);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
        /// <summary>
        /// insert chung tu moi voi trang thai 07 gui thue loi
        /// </summary>
        /// <param name="objCTuHDR"></param>
        /// <param name="objCTuDTL"></param>
        /// <param name="Ngay_KB"></param>
        /// <param name="Ma_NV"></param>
        /// <param name="So_BT"></param>
        /// <param name="CTU_GNT_BL"></param>
        public static void Insert_New0021(NewChungTu.infChungTuHDR objCTuHDR, List<NewChungTu.infChungTuDTL> objCTuDTL, string Ngay_KB = "", string Ma_NV = "", string So_BT = "", string CTU_GNT_BL = "00")
        {

            IDbTransaction transCT = null;
            IDbConnection conn = null;
            string strSql = "";
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();

                strSql = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV,MA_CN,";
                strSql += "So_BT,Ma_DThu,So_BThu,";
                strSql += "KyHieu_CT,So_CT_NH,";
                strSql += "Ma_NNTien,Ten_NNTien,DC_NNTien,";
                strSql += "Ma_NNThue,Ten_NNThue,DC_NNThue,";
                strSql += "Ly_Do,Ma_KS,Ma_TQ,So_QD,";
                strSql += "Ngay_QD,CQ_QD,Ngay_CT,Ngay_HT,";
                strSql += "Ma_CQThu,TEN_CQTHU,XA_ID,Ma_Tinh,";
                strSql += "Ma_Huyen,Ma_Xa,TK_No,TK_Co,";
                strSql += "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS,";
                strSql += "Ma_NT,Ty_Gia,TG_ID, Ma_LThue,DIENGIAI_HQ,";
                strSql += "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH,";
                strSql += "MA_NH_A,MA_NH_B,Ten_NH_B,Ten_NH_TT,NH_HUONG,TTien,TT_TThu,";
                strSql += "Lan_In,Trang_Thai, ";
                strSql += "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT,";
                strSql += "So_BK, Ngay_BK,CTU_GNT_BL,So_CT,TK_GL_NH,TEN_KH_NH,TK_KB_NH, ";
                strSql += "ISACTIVE,PHI_GD,PHI_VAT,PHI_GD2,PHI_VAT2,TIME_BEGIN,PT_TINHPHI,SEQ_NO,REF_NO,RM_REF_NO,QUAN_HUYENNNTIEN,";
                strSql += "TINH_TPNNTIEN,MA_HQ,LOAI_TT,TEN_HQ,MA_HQ_PH,TEN_HQ_PH, MA_NH_TT,MA_SANPHAM,TT_CITAD,SO_CMND,SO_FONE,MA_NTK, KENH_CT, LOAI_NNT)";
                strSql += "Values('" + objCTuHDR.SHKB + "'," + objCTuHDR.Ngay_KB + "," + objCTuHDR.Ma_NV + ",'" + objCTuHDR.Ma_CN + "',";
                strSql += objCTuHDR.So_BT + ",'" + objCTuHDR.Ma_DThu + "'," + objCTuHDR.So_BThu + ",'";
                strSql += objCTuHDR.KyHieu_CT + "','" + objCTuHDR.So_CT_NH + "','";
                strSql += objCTuHDR.Ma_NNTien + "',:Ten_NNTien,:DC_NNTien,'" ;
                strSql += objCTuHDR.Ma_NNThue + "',:Ten_NNThue,:DC_NNThue,'" ;
                strSql += objCTuHDR.Ly_Do + "'," + objCTuHDR.Ma_KS + "," + objCTuHDR.Ma_TQ + ",'" + objCTuHDR.So_QD + "'," ;
                strSql += objCTuHDR.Ngay_QD + ",'" + objCTuHDR.CQ_QD + "'," + objCTuHDR.Ngay_CT + "," ;
                strSql += objCTuHDR.Ngay_HT + ",'" + objCTuHDR.Ma_CQThu + "',:TEN_CQTHU,'";
                strSql += objCTuHDR.XA_ID + "','" + objCTuHDR.Ma_Tinh + "','" + objCTuHDR.Ma_Huyen + "','" ;
                strSql += objCTuHDR.Ma_Xa + "','" + objCTuHDR.TK_No + "','" + objCTuHDR.TK_Co + "','" ;
                strSql += objCTuHDR.So_TK + "'," + objCTuHDR.Ngay_TK + ",'" + objCTuHDR.LH_XNK + "','" ;
                strSql += objCTuHDR.DVSDNS + "','" + objCTuHDR.Ten_DVSDNS + "','" + objCTuHDR.Ma_NT + "'," ;
                strSql += objCTuHDR.Ty_Gia + "," + objCTuHDR.TG_ID + ",'" + objCTuHDR.Ma_LThue + "','" + objCTuHDR.DIENGIAI_HQ + "','" ;
                strSql += objCTuHDR.So_Khung + "','" + objCTuHDR.So_May + "','" + objCTuHDR.TK_KH_NH + "'," ;
                strSql += objCTuHDR.NGAY_KH_NH + ",'" + objCTuHDR.MA_NH_A + "','" + objCTuHDR.MA_NH_B + "','" + objCTuHDR.Ten_NH_B + "','" + objCTuHDR.TEN_NH_TT + "','" + objCTuHDR.NH_HUONG + "'," ;
                strSql += objCTuHDR.TTien + "," + objCTuHDR.TT_TThu + "," + objCTuHDR.Lan_In + ",'" ;
                strSql += objCTuHDR.Trang_Thai + "','" + objCTuHDR.TK_KH_Nhan + "','" + objCTuHDR.Ten_KH_Nhan + "','" ;
                strSql += objCTuHDR.Diachi_KH_Nhan + "'," + objCTuHDR.TTien_NT + ",'" + objCTuHDR.PT_TT + "','" + objCTuHDR.So_BK + "'," ;
                strSql += objCTuHDR.Ngay_BK + "," + Globals.EscapeQuote(CTU_GNT_BL) + ",'" + objCTuHDR.So_CT + "','" ;
                strSql += objCTuHDR.TK_GL_NH + "',:TEN_KH_NH,'";
                strSql += objCTuHDR.TK_KB_NH + "'," + objCTuHDR.Mode + "," + objCTuHDR.PHI_GD + ",";
                strSql += objCTuHDR.PHI_VAT + "," + objCTuHDR.PHI_GD2 + "," + objCTuHDR.PHI_VAT2 + "," + objCTuHDR.TIME_BEGIN + ",'" + objCTuHDR.PT_TINHPHI + "','";
                strSql += objCTuHDR.SEQ_NO + "','" + objCTuHDR.REF_NO + "','";
                strSql += objCTuHDR.RM_REF_NO + "','" + objCTuHDR.QUAN_HUYEN_NNTIEN + "','" + objCTuHDR.TINH_TPHO_NNTIEN + "','" + objCTuHDR.MA_HQ + "','";
                strSql += objCTuHDR.LOAI_TT + "','" + objCTuHDR.TEN_HQ + "','" + objCTuHDR.Ma_hq_ph + "','" + objCTuHDR.TEN_HQ_PH + "','";
                strSql += objCTuHDR.MA_NH_TT + "','" + objCTuHDR.SAN_PHAM + "','" + objCTuHDR.TT_CITAD + "','";
                strSql += objCTuHDR.SO_CMND + "','" + objCTuHDR.SO_FONE + "','" + objCTuHDR.MA_NTK + "','" + objCTuHDR.Kenh_CT + "','" + objCTuHDR.LOAI_NNT + "')";
                List<IDbDataParameter> lsthdr = new List<IDbDataParameter>();
                lsthdr.Add(DataAccess.NewDBParameter("Ten_NNTien", ParameterDirection.Input, objCTuHDR.Ten_NNTien, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("DC_NNTien", ParameterDirection.Input, objCTuHDR.DC_NNTien, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("Ten_NNThue", ParameterDirection.Input, objCTuHDR.Ten_NNThue, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("DC_NNThue", ParameterDirection.Input, objCTuHDR.DC_NNThue, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("TEN_CQTHU", ParameterDirection.Input, objCTuHDR.Ten_cqthu, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("TEN_KH_NH", ParameterDirection.Input, objCTuHDR.TEN_KH_NH, DbType.String));
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text, lsthdr.ToArray(), transCT);
                //Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
                foreach (NewChungTu.infChungTuDTL objDTL in objCTuDTL)
                {
                    // 'Kienvt : chi insert cac truong <>0
                    if (objDTL != null)
                        if (objDTL.SoTien > 0)
                        {
                            strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," +
                              "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," +
                              "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,SO_CT,ma_dp) " +
                              "Values (" + objDTL.ID + ",'" + objDTL.SHKB + "'," + objDTL.Ngay_KB + "," + objDTL.Ma_NV + "," +
                              objDTL.So_BT + ",'" + objDTL.Ma_DThu + "'," + objDTL.CCH_ID + ",'" +
                              objDTL.Ma_Cap + "','" + objDTL.Ma_Chuong + "'," + objDTL.LKH_ID + ",'" +
                              objDTL.Ma_Loai + "','" + objDTL.Ma_Khoan + "'," +
                              objDTL.MTM_ID + ",'" + objDTL.Ma_Muc + "','" + objDTL.Ma_TMuc + "','" +
                              objDTL.Noi_Dung + "'," +
                              objDTL.DT_ID + ",'','" + objDTL.Ky_Thue + "'," +
                              objDTL.SoTien + "," + objDTL.SoTien_NT + "," + Globals.EscapeQuote(objDTL.MaQuy) + "," + Globals.EscapeQuote(objDTL.SO_CT) + ",'')";
                            //  ' Insert Vào bảng TCS_CTU_DTL    
                            DataAccess.ExecuteNonQuery(strSql, CommandType.Text, transCT);
                        }
                }
                //INSERT BANG LOG PAYMENT
                strSql = "INSERT INTO tcs_log_payment(tran_code,tran_type,tran_date,product_code,branch_no,acct_no ,acct_type,acct_ccy,buy_rate,bnfc_bank_id  ,bnfc_bank_name,amt    ,fee,response_code,rm_ref_no,response_msg)";
                strSql += "VALUES('TX002','1',SYSDATE,'TDT','" + objCTuHDR.Ma_CN + "','" + objCTuHDR.TK_KH_NH + "','D','" + objCTuHDR.Ma_NT + "',10000000,'" + objCTuHDR.TK_Co + "','" + objCTuHDR.MA_NH_B + "'," + objCTuHDR.TTien + "," + objCTuHDR.PHI_GD + ",'0','" + objCTuHDR.So_CT + "','" + objCTuHDR.So_CT_NH + "' )";
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text, transCT);

                // ' Cập nhật dữ liệu vào CSDL
                transCT.Commit();
            }
            catch (Exception ex)
            {
                transCT.Rollback();
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
            finally
            {
                //   ' Giải phóng Connection tới CSDL
                if (transCT != null)
                    transCT.Dispose();

                if (conn != null)
                    conn.Dispose();

            }
        }
        /// <summary>
        /// cap nhat trang thai chung tu
        /// </summary>
        /// <param name="pv_SoCT"></param>
        /// <param name="pv_SHKB"></param>
        /// <param name="pv_MA_NV"></param>
        /// <param name="pv_Ngay_KB"></param>
        /// <param name="pv_So_BT"></param>
        /// <param name="strTT"></param>
        /// <param name="strTKHT"></param>
        /// <param name="strRefNo"></param>
        /// <param name="strSeqNo"></param>
        /// <param name="strMaKS"></param>
        /// <param name="strMaHuyKS"></param>
        /// <param name="strRMNo"></param>
        /// <param name="strTT_CThue"></param>
        /// <param name="strLyDoHuy"></param>
        /// <param name="strNgayKSVCT"></param>
        public static void CTU_SetTrangThai2(string pv_SoCT, string pv_SHKB, string pv_MA_NV, string pv_Ngay_KB, string pv_So_BT, string strTT, string strTKHT = "", string strRefNo = "",
                                            string strSeqNo = "", string strMaKS = "", string strMaHuyKS = "", string strRMNo = "", string strTT_CThue = "0", string strMa_ntk="",string ten_cqthu="", string strLyDoHuy = "", bool strNgayKSVCT = false)
        {

            try
            {
                string strSQL = "";
                string strNgayKS = "";
                strSQL = "update TCS_CTU_HDR " +
                    "   set TRANG_THAI = '" + strTT + "'";
                if (strRefNo.Length > 0)
                    strSQL += ",ref_no = '" + strRefNo + "'";
                if (strSeqNo.Length > 0)
                    strSQL += ",seq_no = '" + strSeqNo + "'";
                if (strMaKS.Length > 0)
                    strSQL += ",Ma_KS = '" + strMaKS + "'";
                if (strMaKS.Length > 0)
                    strSQL += ",ngay_ks=SYSDATE";
                if (strMaHuyKS.Length > 0)
                    strSQL += ",Ma_HuyKS = '" + strMaHuyKS + "', NGAY_KSV_HUY=SYSDATE ";
                if (strRMNo.Length > 0)
                    strSQL += ",RM_REF_NO = '" + strRMNo + "'";
                if (strTT_CThue.Length > 0)
                    strSQL += ",TT_CTHUE = '" + strTT_CThue + "'";
                if (strTKHT.Length > 0)
                    strSQL += ",tkht = '" + strTKHT + "'";
                if (strMa_ntk.Length > 0)
                    strSQL += ",MA_NTK='" + strMa_ntk +"'";
                if (ten_cqthu.Length > 0)
                    strSQL += ",TEN_CQTHU=q'[" + ten_cqthu + "]'";
                if (strLyDoHuy.Length > 0)
                    strSQL += ", ly_do_huy = '" + strLyDoHuy + "'";
                if (strNgayKSVCT == true)
                    strSQL += ", ngay_ksv_ct = SYSDATE";
                strSQL += "   where shkb = '" + pv_SHKB.ToString() + "' and " +
                "   ngay_kb = " + pv_Ngay_KB.ToString() + " and " +
                "   ma_nv = " + pv_MA_NV.ToString() + " and " +
                "   so_bt = " + pv_So_BT.ToString() + " and " +
             " so_ct = '" + pv_SoCT.ToString() + "' ";

                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
        }
        //--------------------------------------------------
        public static void CTU_SetTrangThai(string pv_SoCT, string pv_SHKB, string pv_MA_NV, string pv_Ngay_KB, string pv_So_BT, string strTT, string strTKHT = "", string strRefNo = "",
                                           string strSeqNo = "", string strMaKS = "", string strMaHuyKS = "", string strRMNo = "", string strTT_CThue = "0", string strLyDoHuy = "", bool strNgayKSVCT = false)
        {

            try
            {
                string strSQL = "";
                string strNgayKS = "";
                strSQL = "update TCS_CTU_HDR " +
                    "   set TRANG_THAI = '" + strTT + "'";
                if (strRefNo.Length > 0)
                    strSQL += ",ref_no = '" + strRefNo + "'";
                if (strSeqNo.Length > 0)
                    strSQL += ",seq_no = '" + strSeqNo + "'";
                if (strMaKS.Length > 0)
                    strSQL += ",Ma_KS = '" + strMaKS + "'";
                if (strMaKS.Length > 0)
                    strSQL += ",ngay_ks=SYSDATE";
                if (strMaHuyKS.Length > 0)
                    strSQL += ",Ma_HuyKS = '" + strMaHuyKS + "', NGAY_KSV_HUY=SYSDATE ";
                if (strRMNo.Length > 0)
                    strSQL += ",RM_REF_NO = '" + strRMNo + "'";
                if (strTT_CThue.Length > 0)
                    strSQL += ",TT_CTHUE = '" + strTT_CThue + "'";
                if (strTKHT.Length > 0)
                    strSQL += ",tkht = '" + strTKHT + "'";
                if (strLyDoHuy.Length > 0)
                    strSQL += ", ly_do_huy = '" + strLyDoHuy + "'";
                if (strNgayKSVCT == true)
                    strSQL += ", ngay_ksv_ct = SYSDATE";
                strSQL += "   where shkb = '" + pv_SHKB.ToString() + "' and " +
                "   ngay_kb = " + pv_Ngay_KB.ToString() + " and " +
                "   ma_nv = " + pv_MA_NV.ToString() + " and " +
                "   so_bt = " + pv_So_BT.ToString() + " and " +
             " so_ct = '" + pv_SoCT.ToString() + "' ";

                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
        }
      //---------------------------------------------------
        public static string getTenHQ(string pv_strMaHQ)
        {
            try
            {
                string sSQL = "select TEN from tcs_dm_cqthu where ma_hq='" + pv_strMaHQ + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            return ds.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return "";
        }
        public static string getNOIDUNGKT(string pv_strMaHQ)
        {
            try
            {
                string sSQL = "select TEN from tcs_dm_muc_tmuc where ma_tmuc='" + pv_strMaHQ + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            return ds.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return pv_strMaHQ;
        }
        public static void setObject301ToCTU(MSG301.MSG301 obj31, ref NewChungTu.HdrHq objHdrHq, ref List<NewChungTu.DtlHq> lstDtl)
        {
            try
            {
                objHdrHq.NGAY_KB = TCS_GetNgayLV(obj31.Header.UserID);
                string str_nam_kb = "";
                string str_thang_kb = "";
                str_nam_kb = objHdrHq.NGAY_KB.Substring(2, 4);

                objHdrHq.MA_CN = TCS_GetMACN_NV(obj31.Header.UserID);
                //' Mã NV
                objHdrHq.MA_NV = obj31.Header.UserID;
                // ' Số bút toán
                objHdrHq.SO_BT = Get_SoBT(int.Parse(objHdrHq.NGAY_KB), int.Parse(objHdrHq.MA_NV)).ToString();
                //' Số CT
                objHdrHq.SO_CT = str_nam_kb + str_thang_kb + GenerateKey("TCS_CTU_ID_SEQ", 6);
                objHdrHq.MA_DTHU = obj31.Data.Ma_KB;
                objHdrHq.SHKB = obj31.Data.Ma_KB;
                objHdrHq.MA_TINH = "";
                objHdrHq.MA_HUYEN = "";
                objHdrHq.MA_XA = "";
                objHdrHq.TK_CO = obj31.Data.TKKB;
                objHdrHq.MA_LTHUE = "04";
                objHdrHq.MA_NNTIEN = "";
                objHdrHq.TEN_NNTIEN = "";
                objHdrHq.DC_NNTIEN = "";
                objHdrHq.MA_QUAN_HUYENNNTIEN = "";
                objHdrHq.MA_TINH_TPNNTIEN = "";
                objHdrHq.MA_NNTHUE = obj31.Data.Ma_DV;
                objHdrHq.TEN_NNTHUE = obj31.Data.Ten_DV;
                objHdrHq.DC_NNTHUE = obj31.Data.DiaChi_DV;
                objHdrHq.MA_HUYEN_NNTHUE = "";
                objHdrHq.MA_TINH_NNTHUE = "";
                objHdrHq.NGAY_CT = obj31.Data.Ngay_CT.Replace("-", "");
                objHdrHq.NGAY_KH_NH = obj31.Data.Ngay_BN;

                objHdrHq.MA_CQTHU = obj31.Data.Ma_HQ_CQT;
                objHdrHq.PT_TT = "01";
                objHdrHq.MA_NT = obj31.Data.Ma_NT;
                objHdrHq.TY_GIA = obj31.Data.Ty_Gia;
                objHdrHq.TTIEN = obj31.Data.SoTien_TO;
                objHdrHq.TTIEN_NT = obj31.Data.SoTien_TO;
                objHdrHq.TT_TTHU = (decimal.Parse(obj31.Data.SoTien_TO) + decimal.Parse(obj31.Data.FEEAMT) + decimal.Parse(obj31.Data.VATAMT)).ToString();
                objHdrHq.TK_KH_NH = obj31.Data.TK_KH_NH;// $('#txtTK_KH_NH').val();
                objHdrHq.TEN_KH_NH = obj31.Data.Ten_DV;//$('#txtTenTK_KH_NH').val();
                objHdrHq.TK_GL_NH = "";

                objHdrHq.MA_NH_A = obj31.Data.MA_NH_CHUYEN;
                objHdrHq.MA_NH_B = obj31.Data.Ma_NH_TH;
                objHdrHq.TEN_NH_B = obj31.Data.Ten_NH_TH;
                objHdrHq.MA_NH_TT = "";
                objHdrHq.TEN_NH_TT = "";
                objHdrHq.KYHIEU_CT = TCS_GetKHCT(objHdrHq.SHKB, obj31.Header.UserID, "04");
                objHdrHq.MA_KS = objHdrHq.MA_NV;
                string strSQL = "select * from tcs_dm_nh_giantiep_tructiep where shkb='" + obj31.Data.Ma_KB + "' and ma_giantiep='" + obj31.Data.Ma_NH_TH + "'";
                DataSet dsNH_B = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (dsNH_B != null)
                    if (dsNH_B.Tables.Count > 0)
                        if (dsNH_B.Tables[0].Rows.Count > 0)
                        {

                            objHdrHq.MA_NH_TT = dsNH_B.Tables[0].Rows[0]["ma_tructiep"].ToString();

                            objHdrHq.TEN_NH_TT = dsNH_B.Tables[0].Rows[0]["ten_tructiep"].ToString();
                        }
                objHdrHq.TRANG_THAI = "07";

                objHdrHq.PT_TINHPHI = "O";

                //objHdrHq.PT_TINHPHI = "";
                objHdrHq.PHI_GD = obj31.Data.FEEAMT;
                objHdrHq.PHI_VAT = obj31.Data.VATAMT;
                objHdrHq.PHI_GD2 = "0";
                objHdrHq.PHI_VAT2 = "0";
                objHdrHq.TT_CTHUE = "0";
                objHdrHq.TKHT = "";
                objHdrHq.LOAI_TT = "";       //Chuyen xuong HDR
                objHdrHq.MA_NTK = obj31.Data.Ma_NTK;// $('#ddlMa_NTK').val();
                objHdrHq.MA_HQ = obj31.Data.GNT_CT[0].Ma_HQ;// $('#txtMaHQ').val();
                objHdrHq.TEN_HQ = getTenHQ(objHdrHq.MA_HQ);
                objHdrHq.MA_HQ_PH = obj31.Data.Ma_HQ_PH;
                objHdrHq.TEN_HQ_PH = obj31.Data.Ten_HQ_PH;
                objHdrHq.KENH_CT = "01";

                objHdrHq.MA_SANPHAM = "";
                objHdrHq.TT_CITAD = "0";
                objHdrHq.SO_CMND = "";
                objHdrHq.SO_FONE = "";
                objHdrHq.LOAI_CTU = "T";
                objHdrHq.REMARKS = obj31.Data.DienGiai;
                //objHdrHq.ten_cq=objHdrHq.TEN_HQ_PH;

                //sonmt: bo sung them cac truong cua mizuho

                objHdrHq.SO_CT_NH = obj31.Data.So_CT;

                objHdrHq.DIENGIAI_HQ = obj31.Data.DienGiai;
                objHdrHq.TEN_KB = obj31.Data.Ten_KB;
                objHdrHq.TEN_CQTHU = obj31.Data.Ten_HQ_PH;
                objHdrHq.RM_REF_NO = objHdrHq.SO_CT;
                objHdrHq.REF_NO = objHdrHq.SO_CT;
                objHdrHq.NGAY_BN = obj31.Data.Ngay_BN;
                objHdrHq.NGAY_BC = obj31.Data.Ngay_BC;
                objHdrHq.SO_TK = "";
                for (int i = 0; i < obj31.Data.GNT_CT.Count; i++)
                {
                    if (objHdrHq.SO_TK.Length > 0)
                        objHdrHq.SO_TK += ";" + obj31.Data.GNT_CT[i].So_TK;
                    else
                    {
                        objHdrHq.SO_TK = obj31.Data.GNT_CT[i].So_TK;
                        objHdrHq.NGAY_TK = obj31.Data.GNT_CT[i].Ngay_DK;
                    }
                    for (int x = 0; x < obj31.Data.GNT_CT[i].ToKhai_CT.Count; x++)
                    {

                        NewChungTu.DtlHq objdtl = new NewChungTu.DtlHq();
                        objdtl.SO_CT = objHdrHq.SO_CT;
                        objdtl.SO_BT = objHdrHq.SO_BT;
                        objdtl.SHKB = objHdrHq.SHKB;
                        objdtl.MA_DTHU = objHdrHq.MA_DTHU;
                        objdtl.MA_NV = objHdrHq.MA_NV;
                        objdtl.NGAY_KB = objHdrHq.NGAY_KB;
                        objdtl.SO_TK = obj31.Data.GNT_CT[i].So_TK;
                        objdtl.MA_CHUONG = obj31.Data.Ma_Chuong;
                        objdtl.MA_HQ = obj31.Data.GNT_CT[i].Ma_HQ;
                        objdtl.MA_LHXNK = obj31.Data.GNT_CT[i].Ma_LH;
                        objdtl.MA_LT = obj31.Data.GNT_CT[i].Ma_LT;
                        objdtl.NGAY_TK = obj31.Data.GNT_CT[i].Ngay_DK;
                        objdtl.TT_BTOAN = obj31.Data.GNT_CT[i].TTButToan;
                        objdtl.SAC_THUE = obj31.Data.GNT_CT[i].ToKhai_CT[x].Ma_ST;
                        objdtl.MA_TMUC = obj31.Data.GNT_CT[i].ToKhai_CT[x].NDKT;
                        objdtl.SOTIEN = obj31.Data.GNT_CT[i].ToKhai_CT[x].SoTien_VND;
                        objdtl.SOTIEN_NT = obj31.Data.GNT_CT[i].ToKhai_CT[x].SoTien_NT;
                        objdtl.NOI_DUNG = getNOIDUNGKT(obj31.Data.GNT_CT[i].ToKhai_CT[x].NDKT);
                        lstDtl.Add(objdtl);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool checkValidObj301(MSG301.MSG301 obj301, ref string strErrCode, ref string strErrMSG)
        {
            bool bResult = false;
            try
            {
                if (obj301.Data == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "Data");
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.Ma_NH_TH, "Ma_NH_TH", 8, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj301.Data.Ten_NH_TH, "Ten_NH_TH", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.Ma_DV, "Ma_DV", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.Ma_Chuong, "Ma_Chuong", 3, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.Ten_DV, "Ten_DV", 255, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.DiaChi_DV, "DiaChi_DV", 255, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.Ma_KB, "Ma_KB", 5, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.TKKB, "TKKB", 15, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.Ma_NTK, "Ma_NTK", 1, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.Ma_HQ_PH, "Ma_HQ_PH", 6, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.Ten_HQ_PH, "Ten_HQ_PH", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.Ma_HQ_CQT, "Ma_HQ_CQT", 7, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.So_CT, "So_CT", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidDateIsRequired(obj301.Data.Ngay_BN, "Ngay_BN", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                {
                    return false;
                }
                if (!CheckValidDateIsRequired(obj301.Data.Ngay_BC, "Ngay_BC", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.Ma_NT, "Ma_NT", 3, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.Ty_Gia, "Ty_Gia", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.SoTien_TO, "SoTien_TO", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.DienGiai, "DienGiai", 255, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.TK_KH_NH, "TK_KH_NH", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.MA_NH_CHUYEN, "MA_NH_CHUYEN", 8, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidNumberIsRequired(obj301.Data.FEEAMT, "FEEAMT", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.VATAMT, "VATAMT", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                for (int i = 0; i < obj301.Data.GNT_CT.Count; i++)
                {
                    if (!CheckValidNumberIsRequired(obj301.Data.GNT_CT[i].TTButToan, "TTButToan", 5, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.GNT_CT[i].Ma_HQ, "Ma_HQ", 8, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.GNT_CT[i].Ma_LH, "Ma_LH", 8, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidDateIsRequired(obj301.Data.GNT_CT[i].Ngay_DK, "Ngay_DK", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.GNT_CT[i].So_TK, "So_TK", 15, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.GNT_CT[i].Ma_LT, "Ma_LT", 2, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    for (int x = 0; x < obj301.Data.GNT_CT[i].ToKhai_CT.Count; x++)
                    {
                        if (!CheckValidStringIsRequired(obj301.Data.GNT_CT[i].ToKhai_CT[x].Ma_ST, "Ma_ST", 2, ref strErrCode, ref strErrMSG))
                        {
                            return false;
                        }
                        if (!CheckValidStringIsRequired(obj301.Data.GNT_CT[i].ToKhai_CT[x].NDKT, "NDKT", 4, ref strErrCode, ref strErrMSG))
                        {
                            return false;
                        }
                        if (!CheckValidNumberIsRequired(obj301.Data.GNT_CT[i].ToKhai_CT[x].SoTien_NT, "SoTien_NT", 20, ref strErrCode, ref strErrMSG))
                        {
                            return false;
                        }
                        if (!CheckValidNumberIsRequired(obj301.Data.GNT_CT[i].ToKhai_CT[x].SoTien_VND, "SoTien_VND", 20, ref strErrCode, ref strErrMSG))
                        {
                            return false;
                        }
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return bResult;
        }

        public static void Insert_New301(NewChungTu.HdrHq hdr, List<NewChungTu.DtlHq> lstDtl)
        {
            IDbConnection conn = null;
            IDbTransaction tran = null;
            string sSQL = "";
            int iRet = 0;
            List<IDataParameter> listParam = new List<IDataParameter>();
            try
            {


                conn = DataAccess.GetConnection();
                tran = conn.BeginTransaction();

                sSQL = "INSERT INTO TCS_CTU_HDR(SHKB,NGAY_KB,MA_TINH,MA_HUYEN,MA_XA,TK_CO,MA_NV,MA_CN,SO_BT,MA_DTHU, SO_BTHU, " +
                            "KYHIEU_CT,SO_CT,MA_LTHUE,MA_NNTIEN,TEN_NNTIEN,DC_NNTIEN,MA_QUAN_HUYENNNTIEN,MA_TINH_TPNNTIEN,MA_NNTHUE,TEN_NNTHUE," +
                            "DC_NNTHUE,MA_HUYEN_NNTHUE,MA_TINH_NNTHUE,NGAY_CT,NGAY_HT,MA_CQTHU,SO_TK,NGAY_TK,LH_XNK,PT_TT," +
                            "MA_NT,TY_GIA,TTIEN,TTIEN_NT,TT_TTHU,TK_KH_NH,TEN_KH_NH,NGAY_KH_NH,MA_NH_A,MA_NH_B, " +
                            "TEN_NH_B,MA_NH_TT,TEN_NH_TT,LAN_IN,TRANG_THAI,PT_TINHPHI,PHI_GD,PHI_VAT,PHI_GD2,PHI_VAT2, " +
                            "TT_CTHUE,TKHT,LOAI_TT,MA_NTK,MA_HQ,TEN_HQ,MA_HQ_PH,TEN_HQ_PH,KENH_CT,MA_SANPHAM, " +
                            "MA_CUC,TEN_CUC, " +
                            "TT_CITAD, SO_CMND, SO_FONE, LOAI_CTU, REMARKS,DIENGIAI_HQ,REF_NO,RM_REF_NO, TK_GL_NH,ngay_baono,ngay_baoco,ma_ks,so_ct_nh)" +
                        "VALUES(:SHKB, :NGAY_KB, :MA_TINH, :MA_HUYEN, :MA_XA, :TK_CO, :MA_NV, :MA_CN, :SO_BT, :MA_DTHU, 0," +
                            ":KYHIEU_CT, :SO_CT, :MA_LTHUE, :MA_NNTIEN, :TEN_NNTIEN, :DC_NNTIEN, :MA_QUAN_HUYENNNTIEN, :MA_TINH_TPNNTIEN, :MA_NNTHUE, :TEN_NNTHUE, " +
                            ":DC_NNTHUE, :MA_HUYEN_NNTHUE, :MA_TINH_NNTHUE, to_char(sysdate,'yyyyMMdd'), sysdate, :MA_CQTHU, :SO_TK, to_date(:NGAY_TK,'RRRR-MM-DD'), :LH_XNK, :PT_TT, " +
                            ":MA_NT, :TY_GIA, :TTIEN, :TTIEN_NT, :TT_TTHU, :TK_KH_NH, :TEN_KH_NH, sysdate, :MA_NH_A, :MA_NH_B, " +
                            ":TEN_NH_B, :MA_NH_TT, :TEN_NH_TT, 0, :TRANG_THAI, :PT_TINHPHI, :PHI_GD, :PHI_VAT, :PHI_GD2, :PHI_VAT2, " +
                            ":TT_CTHUE, :TKHT, :LOAI_TT, :MA_NTK, :MA_HQ, :TEN_HQ, :MA_HQ_PH, :TEN_HQ_PH, :KENH_CT, :MA_SANPHAM," +
                            ":MA_CUC,:TEN_CUC, " +
                            ":TT_CITAD, :SO_CMND, :SO_FONE, :LOAI_CTU, :REMARKS,:DIENGIAI_HQ, :REF_NO, :RM_REF_NO, :TK_GL_NH,to_date(:ngay_bn,'RRRR-MM-DD'),to_date(:ngay_bc,'RRRR-MM-DD'),:ma_ks,:so_ct_nh)";

                //  listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, hdr.SHKB, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, hdr.NGAY_KB, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_TINH", ParameterDirection.Input, hdr.MA_TINH, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_HUYEN", ParameterDirection.Input, hdr.MA_HUYEN, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_XA", ParameterDirection.Input, hdr.MA_XA, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, hdr.TK_CO, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, hdr.MA_NV, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, hdr.MA_CN, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, hdr.SO_BT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, hdr.MA_DTHU, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":KYHIEU_CT", ParameterDirection.Input, hdr.KYHIEU_CT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, hdr.SO_CT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, hdr.MA_LTHUE, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, hdr.MA_NNTIEN, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, hdr.TEN_NNTIEN, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, hdr.DC_NNTIEN, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_QUAN_HUYENNNTIEN", ParameterDirection.Input, hdr.MA_QUAN_HUYENNNTIEN, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_TINH_TPNNTIEN", ParameterDirection.Input, hdr.MA_TINH_TPNNTIEN, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, hdr.MA_NNTHUE, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, hdr.TEN_NNTHUE, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, hdr.DC_NNTHUE, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_HUYEN_NNTHUE", ParameterDirection.Input, hdr.MA_HUYEN_NNTHUE, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_TINH_NNTHUE", ParameterDirection.Input, hdr.MA_TINH_NNTHUE, DbType.String));
                // 'listParam.Add(DataAccess.NewDBParameter(":NGAY_CT", ParameterDirection.Input, hdr.NGAY_CT), DbType.String))       'Fix to_char(sysdate,'yyyyMMdd')
                // 'listParam.Add(DataAccess.NewDBParameter(":NGAY_HT", ParameterDirection.Input, hdr.NGAY_HT), DbType.String))       'Fix sysdate
                listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, hdr.MA_CQTHU, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, hdr.SO_TK, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, hdr.NGAY_TK, DbType.Date));
                listParam.Add(DataAccess.NewDBParameter(":LH_XNK", ParameterDirection.Input, hdr.LH_XNK, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, hdr.PT_TT, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, hdr.MA_NT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TY_GIA", ParameterDirection.Input, hdr.TY_GIA, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, hdr.TTIEN, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TTIEN_NT", ParameterDirection.Input, hdr.TTIEN_NT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TT_TTHU", ParameterDirection.Input, hdr.TT_TTHU, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, hdr.TK_KH_NH, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, hdr.TEN_KH_NH, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, hdr.MA_NH_A, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_NH_B", ParameterDirection.Input, hdr.MA_NH_B, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":TEN_NH_B", ParameterDirection.Input, hdr.TEN_NH_B, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, hdr.MA_NH_TT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, hdr.TEN_NH_TT, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, hdr.TRANG_THAI, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":PT_TINHPHI", ParameterDirection.Input, hdr.PT_TINHPHI, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":PHI_GD", ParameterDirection.Input, hdr.PHI_GD, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":PHI_VAT", ParameterDirection.Input, hdr.PHI_VAT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":PHI_GD2", ParameterDirection.Input, hdr.PHI_GD2, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":PHI_VAT2", ParameterDirection.Input, hdr.PHI_VAT2, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":TT_CTHUE", ParameterDirection.Input, hdr.TT_CTHUE, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TKHT", ParameterDirection.Input, hdr.TKHT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":LOAI_TT", ParameterDirection.Input, hdr.LOAI_TT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, hdr.MA_NTK, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, hdr.MA_HQ, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, hdr.TEN_HQ, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, hdr.MA_HQ_PH, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, hdr.TEN_HQ_PH, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":KENH_CT", ParameterDirection.Input, hdr.KENH_CT, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":MA_SANPHAM", ParameterDirection.Input, hdr.MA_SANPHAM, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":TT_CITAD", ParameterDirection.Input, "1", DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":SO_CMND", ParameterDirection.Input, "", DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":SO_FONE", ParameterDirection.Input, "", DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":LOAI_CTU", ParameterDirection.Input, hdr.LOAI_CTU, DbType.String));

                hdr.REMARKS = Globals.RemoveSign4VietnameseString(hdr.REMARKS);


                listParam.Add(DataAccess.NewDBParameter(":REMARKS", ParameterDirection.Input, hdr.REMARKS, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":DIENGIAI_HQ", ParameterDirection.Input, hdr.DIENGIAI_HQ, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":REF_NO", ParameterDirection.Input, hdr.REF_NO, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":RM_REF_NO", ParameterDirection.Input, hdr.RM_REF_NO, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TK_GL_NH", ParameterDirection.Input, hdr.TK_GL_NH, DbType.String));

                listParam.Add(DataAccess.NewDBParameter(":MA_CUC", ParameterDirection.Input, hdr.MA_CUC, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":TEN_CUC", ParameterDirection.Input, hdr.TEN_CUC, DbType.String));
                //to_date(:ngay_bn,'RRRR-MM-DD'),to_date(:ngay_bc,'RRRR-MM-DD'),:ten_cqthu,:ma_ks)";
                listParam.Add(DataAccess.NewDBParameter(":ngay_bn", ParameterDirection.Input, hdr.NGAY_BN, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":ngay_bc", ParameterDirection.Input, hdr.NGAY_BC, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":ma_ks", ParameterDirection.Input, hdr.MA_KS, DbType.String));
                listParam.Add(DataAccess.NewDBParameter(":so_ct_nh", ParameterDirection.Input, hdr.SO_CT_NH, DbType.String));

                DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, listParam.ToArray(), tran);
                foreach (NewChungTu.DtlHq objDtl in lstDtl)
                {
                    sSQL = "INSERT INTO TCS_CTU_DTL(" +
                         "ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MTM_ID,MA_MUC,MA_TMUC," +
                         "NOI_DUNG,KY_THUE,SOTIEN,SOTIEN_NT,SO_CT, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ, " +
                         "SAC_THUE,TT_BTOAN) " +
                     "VALUES(" +
                         "TCS_CTU_DTL_SEQ.NEXTVAL, :SHKB, :NGAY_KB, :MA_NV, :SO_BT, :MA_DTHU, :MA_CHUONG, '', '', :MA_TMUC," +
                         ":NOI_DUNG, '', :SOTIEN, :SOTIEN_NT, :SO_CT, :SO_TK, TO_DATE(:NGAY_TK,'RRRR-MM-DD'), :MA_LHXNK, :MA_LT, :MA_HQ, " +
                         ":SAC_THUE, :TT_BTOAN)";
                    //string sSQLTMP=" INSERT INTO TCS_CTU_DTL(" +
                    //     "ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MTM_ID,MA_MUC,MA_TMUC," +
                    //     "NOI_DUNG,KY_THUE,SOTIEN,SOTIEN_NT,SO_CT, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ, " +
                    //     "SAC_THUE,TT_BTOAN) " +
                    // "VALUES(" +
                    //     "TCS_CTU_DTL_SEQ.NEXTVAL, '" + objDtl.SHKB + "','" + objDtl.NGAY_KB + "','" + objDtl.MA_NV + "', '" + objDtl.SO_BT + "','" + objDtl.MA_DTHU + "', '" + objDtl.MA_CHUONG + "', '', '', '" + objDtl.MA_TMUC + "'," +
                    //     ":NOI_DUNG, '','" + objDtl.SOTIEN + "', '" + objDtl.SOTIEN_NT + "', '" + objDtl.SO_CT + "', '" + objDtl.SO_TK + "', TO_DATE('" + objDtl.NGAY_TK + "','RRRR-MM-DD'), '" + objDtl.MA_LHXNK + "', '" + objDtl.MA_LT + "', '" + objDtl.MA_HQ + "', " +
                    //     "'" + objDtl.SAC_THUE + "', '" + objDtl.TT_BTOAN + "')";
                    List<IDataParameter> listParamdtl = new List<IDataParameter>();
                    listParamdtl.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, objDtl.SHKB, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, objDtl.NGAY_KB, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, objDtl.MA_NV, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, objDtl.SO_BT, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, objDtl.MA_DTHU, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, objDtl.MA_CHUONG, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":MA_TMUC", ParameterDirection.Input, objDtl.MA_TMUC, DbType.String));

                    listParamdtl.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, objDtl.NOI_DUNG, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":SOTIEN", ParameterDirection.Input, objDtl.SOTIEN, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":SOTIEN_NT", ParameterDirection.Input, objDtl.SOTIEN_NT, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, objDtl.SO_CT, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, objDtl.SO_TK, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, objDtl.NGAY_TK, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":MA_LHXNK", ParameterDirection.Input, objDtl.MA_LHXNK, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":MA_LT", ParameterDirection.Input, objDtl.MA_LT, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, objDtl.MA_HQ, DbType.String));

                    listParamdtl.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, objDtl.SAC_THUE, DbType.String));
                    listParamdtl.Add(DataAccess.NewDBParameter(":TT_BTOAN", ParameterDirection.Input, objDtl.TT_BTOAN, DbType.String));
                    DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, listParamdtl.ToArray(), tran);
                }
                sSQL = "INSERT INTO tcs_log_payment(tran_code,tran_type,tran_date,product_code,branch_no,acct_no ,acct_type,acct_ccy,buy_rate,bnfc_bank_id  ,bnfc_bank_name,amt    ,fee,response_code,rm_ref_no,response_msg)";
                sSQL += "VALUES('TX002','1',SYSDATE,'TDT','" + hdr.MA_CN + "','" + hdr.TK_KH_NH + "','D','" + hdr.MA_NT + "',10000000,'" + hdr.TK_CO + "','" + hdr.MA_NH_B + "'," + hdr.TTIEN + "," + hdr.PHI_GD + ",'0','" + hdr.SO_CT + "','" + hdr.SO_CT_NH + "' )";
                DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, tran);
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
            finally
            {
                if (tran != null)
                    tran.Dispose();
                if (conn != null)
                    conn.Close();
            }

        }
        public static void UpdateDCCTHQ(string strID, string pvTimeTN, string pvSOTN, string pvSO_CT)
        {
            try
            {
                string sSQL = "UPDATE TCS_CTU_HDR SET transaction_id='" + strID + "',so_tn_ct='" + pvSOTN + "',ngay_tn_ct='" + pvTimeTN + "' where SO_CT='" + pvSO_CT + "'";
                DataAccess.ExecuteNonQuery(sSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
        public static void InsertDCHQ(string pvStrSoCT)
        {
            try
            {
                string sSQL = " INSERT INTO TCS_DCHIEU_NHHQ_HDR (id, transaction_id, transaction_type, shkb, ten_kb," +
                             " ngay_kb, so_bt, kyhieu_ct, so_ct, ngay_ct, ngay_bn, ngay_bc, ma_nnthue, ten_nnthue," +
                             " ma_cqthu, ten_cqthu, so_tk, ngay_tk, ttien, ttien_nt, trang_thai, ma_ntk, accept_yn," +
                             " ma_hq_ph, ma_nh_ph, ten_nh_ph, ma_hq_cqt, error_code_hq, so_bt_hq, ma_hq, ten_hq_ph," +
                             " ma_nh_th, tkkb_ct, ma_nv, ma_nt, ty_gia, so_tn_ct, ngay_tn_ct, ten_nh_th ) " +
                             " SELECT SEQ_DCHIEU_NHHQ_HDR_ID.NEXTVAL ID,   A.transaction_id,'M47' transaction_type," +
                             " A.shkb,(SELECT MAX (TEN) FROM tcs_dm_khobac KB WHERE KB.SHKB = A.shkb) ten_kb, A.ngay_kb," +
                             " A.so_bt,A.kyhieu_ct,A.so_ct,A.ngay_CT,A.ngay_baono,A.ngay_baoco,A.ma_nnthue, A.ten_nnthue," +
                             " A.ma_cqthu,A.ten_hq_ph,A.so_tk,A.ngay_tk,ttien,ttien_nt,trang_thai,ma_ntk,'Y' accept_yn, " +
                             " ma_hq_ph,A.ma_nh_a,'" + System.Configuration.ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Name") + "' ten_nh_ph,A.ma_cqthu ma_hq_cqt,'0' error_code_hq, " +
                             " 0 so_bt_hq,ma_hq,ten_hq_ph,A.ma_nh_B ma_nh_th,A.tk_co tkkb_ct,ma_nv,ma_nt,TO_CHAR (NVL (A.ty_gia, 0)) ty_gia,so_tn_ct, " +
                             " ngay_tn_ct,A.ten_nh_b ten_nh_th FROM TCS_CTU_HDR A WHERE SO_CT = '" + pvStrSoCT + "'";
                DataAccess.ExecuteNonQuery(sSQL, CommandType.Text);
            }
            catch (Exception ex)
            {

                log.Error(ex.StackTrace + ex.Message);

            }

        }
        public static bool checkExistRegister(string mst)
        {
            bool Result = false;
            try
            {
                string strSQL = "SELECT * FROM tcs_dm_nntdt  where mst ='" + mst + "'";
                DataTable dt = null;
                dt = DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = true;
                }
                return Result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool checkValidObj06007(MSG06007.MSG06007 obj301, ref string strErrCode, ref string strErrMSG)
        {
            bool bResult = false;
            try
            {
                if (obj301.Data == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "Data");
                    return false;
                }
                if (obj301.Data.TBAO_TKHOAN_NH == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "TBAO_TKHOAN_NH");
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.TBAO_TKHOAN_NH.MST, "MST", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //kiem tra xem cos tai khoan ko
                if (obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN == null)
                {
                    strErrCode = "90028";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "TTIN_TKHOAN");
                    return false;
                }
                if (obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN.Count <= 0)
                {
                    strErrCode = "90028";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "TTIN_TKHOAN");
                    return false;
                }
                int x = getMaxSoTaiKhoangDKNTDT();
                if (obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN.Count > x && x > 0)
                {
                    strErrCode = "91002";
                    strErrMSG = getErrorMSG(strErrCode);
                    return false;
                }
                for (int i = 0; i < obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN.Count; i++)
                {
                    if (!CheckValidNumberIsRequired(obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[i].STK, "STK", 200, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[i].TEN_TK, "TEN_TK", 200, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[i].BRANCH_CODE, "BRANCH_CODE", 20, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[i].CIFNO, "CIFNO", 20, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }

                }
                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return bResult;
        }
        private static int getMaxSoTaiKhoangDKNTDT()
        {
            try
            {
                string StrSQL = "Select GIATRI_TS from TCS_THAMSO_HT where TEN_TS='MAX_SO_TK_NH'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return int.Parse(ds.Tables[0].Rows[0][0].ToString());
                        }
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public static void UPDATE_GDT_TK06007(MSG06007.MSG06007 obj06007, string strNTDT_ID)
        {
            IDbConnection conn = null;
            IDbTransaction tran = null;
            string sSQL = "";
            int iRet = 0;

            try
            {


                conn = DataAccess.GetConnection();
                tran = conn.BeginTransaction();
                //xoa tai khoan
                sSQL = "DELETE FROM NTDT_MAP_MST_TKNH WHERE NNTDT_ID=" + strNTDT_ID ;
                DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, tran);
                //insert bang
                sSQL = "Insert into ntdt_map_mst_tknh(nntdt_id,MST,SO_TK_NH,ACY_AVL_BAL,CIFNO,BRANCH_CODE,ACCOUNTNAME,ID) values(";
                sSQL += ":NNTDT_ID,:MST,:SO_TK_NH,0,:CIFNO,:BRANCH_CODE,:ACCOUNTNAME,NTDT_ID_MAP_TK_MST.nextval)";
                foreach (MSG06007.TTIN_TKHOAN objDtl in obj06007.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN)
                {
                    //  listParam = New List(Of IDataParameter)
                    List<IDataParameter> listParam = new List<IDataParameter>();
                    listParam.Add(DataAccess.NewDBParameter(":NNTDT_ID", ParameterDirection.Input, strNTDT_ID, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":MST", ParameterDirection.Input, obj06007.Data.TBAO_TKHOAN_NH.MST, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":SO_TK_NH", ParameterDirection.Input, objDtl.STK, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":CIFNO", ParameterDirection.Input, objDtl.CIFNO, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":BRANCH_CODE", ParameterDirection.Input, objDtl.BRANCH_CODE, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":ACCOUNTNAME", ParameterDirection.Input, objDtl.TEN_TK, DbType.String));
                    DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, listParam.ToArray(), tran);
                }
                sSQL = " UPDATE TCS_DM_NNTDT SET TRANG_THAI='01', NV_HOAN_THIEN='" + obj06007.Header.UserID + "', NV_TT='" + obj06007.Header.UserID + "'," +
                    " MA_CN=(select max( ma_cn) from tcs_dm_nhanvien where UPPER (ma_nv) = UPPER ('" + obj06007.Header.UserID + "'))" +
                   ", NGAY_HOAN_THIEN=SYSDATE, TENTK_NHANG=:TENTK_NHANG WHERE NNTDT_ID=" + strNTDT_ID;
                List<IDataParameter> lst = new List<IDataParameter>();
                lst.Add(DataAccess.NewDBParameter(":TENTK_NHANG", ParameterDirection.Input, obj06007.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[0].TEN_TK, DbType.String));

                DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, lst.ToArray(), tran);
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
            finally
            {
                if (tran != null)
                    tran.Dispose();
                if (conn != null)
                    conn.Close();
            }

        }
        public static void SetDataToDB06007(ref Business.NTDT.NNTDT.infNNTDT item, string strID)
        {
            try
            {
                item = null;
                string strSQL = "select * from tcs_dm_nntdt A WHERE A.trang_thai IN ('01')  AND A.NNTDT_ID=" + strID;
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            item = new Business.NTDT.NNTDT.infNNTDT();
                            item.MST = ds.Tables[0].Rows[0]["MST"].ToString();// obj06007.Data.TBAO_TKHOAN_NH.MST;
                            item.TEN_NNT = ds.Tables[0].Rows[0]["TEN_NNT"].ToString();
                            item.DIACHI_NNT = ds.Tables[0].Rows[0]["DIACHI_NNT"].ToString();
                            item.MA_CQT = ds.Tables[0].Rows[0]["MA_CQT"].ToString();
                            item.EMAIL_NNT = ds.Tables[0].Rows[0]["EMAIL_NNT"].ToString();
                            item.SDT_NNT = ds.Tables[0].Rows[0]["SDT_NNT"].ToString();
                            item.TEN_LHE_NTHUE = ds.Tables[0].Rows[0]["TEN_LHE_NTHUE"].ToString();
                            item.SERIAL_CERT_NTHUE = ds.Tables[0].Rows[0]["SERIAL_CERT_NTHUE"].ToString();
                            item.ISSUER_CERT_NTHUE = ds.Tables[0].Rows[0]["ISSUER_CERT_NTHUE"].ToString();
                            item.MA_NHANG = ds.Tables[0].Rows[0]["MA_NHANG"].ToString();
                            item.TEN_NHANG = ds.Tables[0].Rows[0]["TEN_NHANG"].ToString();
                            item.VAN_ID = ds.Tables[0].Rows[0]["VAN_ID"].ToString();
                            item.TEN_TVAN = ds.Tables[0].Rows[0]["TEN_TVAN"].ToString();
                            item.NGAY_GUI = ds.Tables[0].Rows[0]["NGAY_GUI"].ToString();
                            item.TENTK_NHANG = ds.Tables[0].Rows[0]["TENTK_NHANG"].ToString();
                            item.NNTDT_ID = strID;
                            item.NV_TT = ds.Tables[0].Rows[0]["NV_TT"].ToString();
                            item.LDO_TCHOI = ds.Tables[0].Rows[0]["LDO_TCHOI"].ToString();
                            item.SUBJECT_CERT_NTHUE = ds.Tables[0].Rows[0]["SUBJECT_CERT_NTHUE"].ToString();
                            item.STK_NHANG = GetSo_TK_NH(item.NNTDT_ID, item.MST);
                            item.MA_GDICH = ds.Tables[0].Rows[0]["MA_GDICH"].ToString();
                            item.UPDATE_TT = ds.Tables[0].Rows[0]["UPDATE_TT"].ToString();
                            item.TRANG_THAI = "08";
                        }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static string GetSo_TK_NH(string NNTDT_ID, string MST)
        {

            string So_TK_NH = "";
            string sql = "Select * from NTDT_MAP_MST_TKNH where NNTDT_ID=" + NNTDT_ID + " AND MST='" + MST + "'";
            DataTable dt = DataAccess.ExecuteToTable(sql);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    So_TK_NH = So_TK_NH + dt.Rows[i]["SO_TK_NH"].ToString() + ";";
                }
                So_TK_NH = So_TK_NH.Substring(0, So_TK_NH.Length - 1);
            }
            return So_TK_NH;


        }
        public static void UPDATE_GDT_TK06013(MSG06013.MSG06013 obj06007, string strNTDT_ID)
        {
            IDbConnection conn = null;
            IDbTransaction tran = null;
            string sSQL = "";
            int iRet = 0;

            try
            {


                conn = DataAccess.GetConnection();
                tran = conn.BeginTransaction();
                //xoa tai khoan
                sSQL = "DELETE FROM NTDT_MAP_MST_TKNH WHERE NNTDT_ID=" + strNTDT_ID ;
                DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, tran);
                //insert bang
                sSQL = "Insert into ntdt_map_mst_tknh(nntdt_id,MST,SO_TK_NH,ACY_AVL_BAL,CIFNO,BRANCH_CODE,ACCOUNTNAME,ID) values(";
                sSQL += ":NNTDT_ID,:MST,:SO_TK_NH,0,:CIFNO,:BRANCH_CODE,:ACCOUNTNAME,NTDT_ID_MAP_TK_MST.nextval)";
                foreach (MSG06013.TTIN_TKHOAN objDtl in obj06007.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN)
                {
                    //  listParam = New List(Of IDataParameter)
                    List<IDataParameter> listParam = new List<IDataParameter>();
                    listParam.Add(DataAccess.NewDBParameter(":NNTDT_ID", ParameterDirection.Input, strNTDT_ID, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":MST", ParameterDirection.Input, obj06007.Data.TBAO_TKHOAN_NH.MST, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":SO_TK_NH", ParameterDirection.Input, objDtl.STK, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":CIFNO", ParameterDirection.Input, objDtl.CIFNO, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":BRANCH_CODE", ParameterDirection.Input, objDtl.BRANCH_CODE, DbType.String));
                    listParam.Add(DataAccess.NewDBParameter(":ACCOUNTNAME", ParameterDirection.Input, objDtl.TEN_TK, DbType.String));
                    DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, listParam.ToArray(), tran);
                }
                sSQL = " UPDATE TCS_DM_NNTDT SET TRANG_THAI='05', NV_HOAN_THIEN='" + obj06007.Header.UserID + "', NV_TT='" + obj06007.Header.UserID + "'," +
                    " MA_CN=(select max( ma_cn) from tcs_dm_nhanvien where UPPER (ma_nv) = UPPER ('" + obj06007.Header.UserID + "'))" +
                   ", NGAY_HOAN_THIEN=SYSDATE, TENTK_NHANG=:TENTK_NHANG WHERE NNTDT_ID=" + strNTDT_ID;
                List<IDataParameter> lst = new List<IDataParameter>();
                lst.Add(DataAccess.NewDBParameter(":TENTK_NHANG", ParameterDirection.Input, obj06007.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[0].TEN_TK, DbType.String));

                DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, lst.ToArray(), tran);
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
            finally
            {
                if (tran != null)
                    tran.Dispose();
                if (conn != null)
                    conn.Close();
            }

        }
        public static void SetDataToDB06013(ref Business.NTDT.NNTDT.infNNTDT item, string strID)
        {
            try
            {
                item = null;
                string strSQL = "select * from tcs_dm_nntdt A WHERE A.trang_thai IN ('05')  AND A.NNTDT_ID=" + strID;
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            item = new Business.NTDT.NNTDT.infNNTDT();
                            item.MST = ds.Tables[0].Rows[0]["MST"].ToString();// obj06007.Data.TBAO_TKHOAN_NH.MST;
                            item.TEN_NNT = ds.Tables[0].Rows[0]["TEN_NNT"].ToString();
                            item.DIACHI_NNT = ds.Tables[0].Rows[0]["DIACHI_NNT"].ToString();
                            item.MA_CQT = ds.Tables[0].Rows[0]["MA_CQT"].ToString();
                            item.EMAIL_NNT = ds.Tables[0].Rows[0]["EMAIL_NNT"].ToString();
                            item.SDT_NNT = ds.Tables[0].Rows[0]["SDT_NNT"].ToString();
                            item.TEN_LHE_NTHUE = ds.Tables[0].Rows[0]["TEN_LHE_NTHUE"].ToString();
                            item.SERIAL_CERT_NTHUE = ds.Tables[0].Rows[0]["SERIAL_CERT_NTHUE"].ToString();
                            item.ISSUER_CERT_NTHUE = ds.Tables[0].Rows[0]["ISSUER_CERT_NTHUE"].ToString();
                            item.MA_NHANG = ds.Tables[0].Rows[0]["MA_NHANG"].ToString();
                            item.TEN_NHANG = ds.Tables[0].Rows[0]["TEN_NHANG"].ToString();
                            item.VAN_ID = ds.Tables[0].Rows[0]["VAN_ID"].ToString();
                            item.TEN_TVAN = ds.Tables[0].Rows[0]["TEN_TVAN"].ToString();
                            item.NGAY_GUI = ds.Tables[0].Rows[0]["NGAY_GUI"].ToString();
                            item.TENTK_NHANG = ds.Tables[0].Rows[0]["TENTK_NHANG"].ToString();
                            item.NNTDT_ID = strID;
                            item.NV_TT = ds.Tables[0].Rows[0]["NV_TT"].ToString();
                            item.LDO_TCHOI = ds.Tables[0].Rows[0]["LDO_TCHOI"].ToString();
                            item.SUBJECT_CERT_NTHUE = ds.Tables[0].Rows[0]["SUBJECT_CERT_NTHUE"].ToString();
                            item.STK_NHANG = GetSo_TK_NH(item.NNTDT_ID, item.MST);
                            item.MA_GDICH = ds.Tables[0].Rows[0]["MA_GDICH"].ToString();
                            item.UPDATE_TT = ds.Tables[0].Rows[0]["UPDATE_TT"].ToString();
                            item.TRANG_THAI = "08";
                        }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static bool checkValidObj06013(MSG06013.MSG06013 obj301, ref string strErrCode, ref string strErrMSG)
        {
            bool bResult = false;
            try
            {
                if (obj301.Data == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "Data");
                    return false;
                }
                if (obj301.Data.TBAO_TKHOAN_NH == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "TBAO_TKHOAN_NH");
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.TBAO_TKHOAN_NH.MST, "MST", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //kiem tra xem cos tai khoan ko
                if (obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN == null)
                {
                    strErrCode = "90028";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "TTIN_TKHOAN");
                    return false;
                }
                if (obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN.Count <= 0)
                {
                    strErrCode = "90028";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "TTIN_TKHOAN");
                    return false;
                }
                int x = getMaxSoTaiKhoangDKNTDT();
                if (obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN.Count > x && x > 0)
                {
                    strErrCode = "91002";
                    strErrMSG = getErrorMSG(strErrCode);
                    return false;
                }
                for (int i = 0; i < obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN.Count; i++)
                {
                    if (!CheckValidStringIsRequired(obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[i].STK, "STK", 20, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[i].TEN_TK, "TEN_TK", 200, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[i].BRANCH_CODE, "BRANCH_CODE", 20, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj301.Data.TBAO_TKHOAN_NH.TTIN_TKHOAN[i].CIFNO, "CIFNO", 20, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }

                }
                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return bResult;
        }
        public static bool checkValidObj312(MSG312.MSG312 obj301, ref string strErrCode, ref string strErrMSG)
        {
            bool bResult = false;
            try
            {
                if (obj301.Data == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "Data");
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.So_HS, "So_HS", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.Ma_DV, "Ma_DV", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidDateIsRequired(obj301.Data.Ngay_HL, "Ngay_HL", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                {
                    return false;
                }
                //kiem tra xem cos tai khoan ko
                if (obj301.Data.ThongTinTaiKhoan == null)
                {
                    strErrCode = "90028";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "ThongTinTaiKhoan");
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.ThongTinTaiKhoan.TaiKhoan_TH, "TaiKhoan_TH", 50, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.ThongTinTaiKhoan.Ten_TaiKhoan_TH, "Ten_TaiKhoan_TH", 255, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.ThongTinTaiKhoan.CIFNO, "CIFNO", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.ThongTinTaiKhoan.BRANCH_CODE, "BRANCH_CODE", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return bResult;
        }
        public static string convertDateHQ(string strDate)
        {

            string str = "";
            try
            {
                str = strDate.Substring(8, 2) + "/" + strDate.Substring(5, 2) + "/" + strDate.Substring(0, 4);
                if (strDate.Trim().Length > 10)
                {
                    str += strDate.Substring(10, strDate.Length - 10);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " - " + ex.StackTrace);

            }

            return str;
        }
        public static string getUserName(string strMa_NV)
        {
            try
            {
                string StrSQL = "Select ten_dn from tcs_dm_nhanvien where ma_nv='" + strMa_NV + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getMA_KBNN(string strMaCQTHU,string strMaHQ)
        {
            try
            {
                string StrSQL = "SELECT A.SHKB FROM TCS_DM_CQTHU A ";
                if (strMaHQ.Length > 0)
                {
                    StrSQL += " WHERE MA_HQ='" + strMaHQ + "'";
                }
                else
                {
                    StrSQL = " WHERE MA_CQTHU='" + strMaCQTHU + "'";
                }
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getTEN_CQTHU(string strMaCQTHU, string strMaHQ)
        {
            try
            {
                string StrSQL = "SELECT A.TEN FROM TCS_DM_CQTHU A ";
                if (strMaHQ.Length > 0)
                {
                    StrSQL += " WHERE MA_HQ='" + strMaHQ + "'";
                }
                else
                {
                    StrSQL = " WHERE MA_CQTHU='" + strMaCQTHU + "'";
                }
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getTEN_KBNN(string strSHKB)
        {
            try
            {
                string StrSQL = "SELECT A.TEN FROM TCS_DM_KHOBAC A WHERE SHKB='"+ strSHKB +"'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getTEN_TMUC(string strMA_TMUC)
        {
            try
            {
                string StrSQL = "SELECT A.TEN FROM TCS_DM_MUC_TMUC A WHERE A.MA_TMUC='" + strMA_TMUC + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getTEN_GIANTIEP(string strSHKB, string strMA_NH_B)
        {
            try
            {
                string StrSQL = "select a.ten_giantiep from tcs_dm_nh_giantiep_tructiep a where a.shkb='" + strSHKB + "' and a.ma_giantiep='" + strMA_NH_B + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static bool checkValidObj101(MSG101.MSG101 obj301, ref string strErrCode, ref string strErrMSG)
        {
            bool bResult = false;
            try
            {
                if (obj301.Data == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "Data");
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.Ma_DV, "Ma_DV", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (obj301.Data.Nam_DK!=null)
                if (!CheckValidStringIsNotRequired(obj301.Data.Nam_DK, "Nam_DK",4, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (obj301.Data.So_TK != null)
                if (!CheckValidStringIsNotRequired(obj301.Data.So_TK, "So_TK", 12, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                
                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return bResult;
        }
        public static bool checkValidObjE981(MSGE981.MSGE981 obj301, ref string strErrCode, ref string strErrMSG)
        {
            bool bResult = false;
            try
            {
                if (obj301.Data == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "Data");
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.MA_NNTHUE, "MA_NNTHUE", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj301.Data.TEN_NNTHUE, "TEN_NNTHUE", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj301.Data.DC_NNTHUE, "DC_NNTHUE", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.MA_NNTIEN, "MA_NNTIEN", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.TEN_NNTIEN, "TEN_NNTIEN", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.DC_NNTIEN, "DC_NNTIEN", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidDateIsRequired(obj301.Data.NGAY_CT, "NGAY_CT", 19, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.MA_KB, "MA_KB", 4, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.CQ_QD, "CQ_QD", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.TEN_CQ, "TEN_CQ", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.SO_QD, "SO_QD", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidDateIsRequired(obj301.Data.NGAY_QD, "NGAY_QD", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.MA_DV, "MA_DV", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.TEN_DV, "TEN_DV", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.MA_LTHUE, "MA_LTHUE", 5, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //MA_CQTHU

                if (!CheckValidStringIsNotRequired(obj301.Data.MA_CQTHU, "MA_CQTHU", 7, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //MA_DBHC
                if (!CheckValidStringIsNotRequired(obj301.Data.MA_DBHC, "MA_DBHC", 5, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj301.Data.MA_NH_A, "MA_NH_A", 10, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.MA_NT, "MA_NT", 3, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.TY_GIA, "TY_GIA", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.TTIEN, "TTIEN", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.TTIEN_NT, "TTIEN_NT", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.DIEN_GIAI, "DIEN_GIAI", 1000, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.SO_CT, "SO_CT", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.TK_KH_NH, "TK_KH_NH", 21, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.PHUONGTHUC_TT, "PHUONGTHUC_TT", 2, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //SO_THAM_CHIEU
                if (!CheckValidStringIsRequired(obj301.Data.SO_THAM_CHIEU, "SO_THAM_CHIEU", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //MA_NH_B
                if (!CheckValidStringIsNotRequired(obj301.Data.MA_NH_B, "MA_NH_B", 10, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.MA_LHTHU, "MA_LHTHU", 3, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //TEN_LHTHU
                if (!CheckValidStringIsRequired(obj301.Data.TEN_LHTHU, "TEN_LHTHU", 100, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //TK_NS
                if (!CheckValidStringIsNotRequired(obj301.Data.TK_NS, "TK_NS", 4, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //MA_CHUONG
                if (!CheckValidStringIsNotRequired(obj301.Data.MA_CHUONG, "MA_CHUONG", 4, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj301.Data.MA_NDKT, "MA_NDKT", 4, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //LY_DO
                if (!CheckValidStringIsRequired(obj301.Data.LY_DO, "LY_DO", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidNumberIsRequired(obj301.Data.TTIEN_VPHC, "TTIEN_VPHC", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //LY_DO_NOP_CHAM
                if (!CheckValidStringIsNotRequired(obj301.Data.LY_DO_NOP_CHAM, "LY_DO_NOP_CHAM", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.TTIEN_NOP_CHAM, "TTIEN_NOP_CHAM", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return bResult;
        }
        /// <summary>
        /// mac  dinh chuoi dau vao dang yyyy-MM-ddTHH:mm:ss
        /// </summary>
        /// <param name="pvStrDate"></param>
        public static DateTime fnc_StringToDateTime(string pvStrDate) 
        {
            DateTime strResult =new DateTime();
            try {
                if (pvStrDate.Length > 10)
                {
                    //string[] pStr = pvStrDate.Split('T');
                    //strResult = Business_HQ.Common.mdlCommon.ConvertStringToDate(pStr[0], "YYYY-MM-DD");
                    //string[] pStrH = pStr[1].Split(':');
                    //strResult.AddHours(int.Parse(pStrH[0]));
                    //strResult.AddMinutes(int.Parse(pStrH[1]));
                    //strResult.AddSeconds(int.Parse(pStrH[2]));
                   int vYear = int.Parse( pvStrDate.Substring(0, 4));
                   int vMon = int.Parse( pvStrDate.Substring(5, 2));
                   int vDay =int.Parse( pvStrDate.Substring(8, 2));
                   int vHours =int.Parse( pvStrDate.Substring(11, 2));
                   int vMinute =int.Parse( pvStrDate.Substring(14, 2));
                   int vSeconds = int.Parse( pvStrDate.Substring(17, 2));
                   strResult = new DateTime(vYear, vMon, vDay, vHours, vMinute, vSeconds);
                }
                else
                {
                    strResult = Business_HQ.Common.mdlCommon.ConvertStringToDate(pvStrDate, "YYYY-MM-DD");
                }
            }
            catch (Exception e)
            {
                
                log.Error(e.Message);
                throw e;
            }
            return strResult;
        }
        public static bool  CheckValidStatusObjE981(MSGE981.MSGE981 obj301, ref string strErrCode, ref string strErrMSG)
        {
            bool bResult = false;
            try
            {
                if (obj301.Data == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "Data");
                    return bResult;
                }
                if (!CheckValidStringIsRequired(obj301.Data.MA_NNTHUE, "MA_NNTHUE", 14, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidStringIsNotRequired(obj301.Data.TEN_NNTHUE, "TEN_NNTHUE", 200, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidStringIsNotRequired(obj301.Data.DC_NNTHUE, "DC_NNTHUE", 200, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidStringIsRequired(obj301.Data.MA_NNTIEN, "MA_NNTIEN", 14, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidStringIsRequired(obj301.Data.TEN_NNTIEN, "TEN_NNTIEN", 200, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidStringIsRequired(obj301.Data.DC_NNTIEN, "DC_NNTIEN", 200, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidDateIsRequired(obj301.Data.NGAY_CT, "NGAY_CT", 19, ref strErrCode, ref strErrMSG, "YYYY-MM-DDTHH:MM:SS"))
                {
                    return bResult;
                }

                if (!CheckValidStringIsRequired(obj301.Data.MA_KB, "MA_KB", 4, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidStringIsRequired(obj301.Data.CQ_QD, "CQ_QD", 20, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }

                if (!CheckValidStringIsRequired(obj301.Data.TEN_CQ, "TEN_CQ", 200, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidStringIsRequired(obj301.Data.SO_QD, "SO_QD", 30, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidDateIsRequired(obj301.Data.NGAY_QD, "NGAY_QD", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                {
                    return bResult;
                }

                if (!CheckValidStringIsRequired(obj301.Data.MA_DV, "MA_DV", 20, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }

                if (!CheckValidStringIsRequired(obj301.Data.TEN_DV, "TEN_DV", 200, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidStringIsRequired(obj301.Data.MA_LTHUE, "MA_LTHUE", 5, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                //MA_CQTHU

                if (!CheckValidStringIsRequired(obj301.Data.MA_CQTHU, "MA_CQTHU", 7, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                //MA_DBHC
                if (!CheckValidStringIsRequired(obj301.Data.MA_DBHC, "MA_DBHC", 5, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidStringIsRequired(obj301.Data.MA_NH_A, "MA_NH_A", 10, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }

                if (!CheckValidStringIsRequired(obj301.Data.MA_NT, "MA_NT", 3, ref strErrCode, ref strErrMSG))
                {
                    return bResult;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.TY_GIA, "TY_GIA", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.TTIEN, "TTIEN", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.TTIEN_NT, "TTIEN_NT", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.DIEN_GIAI, "DIEN_GIAI", 1000, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.SO_CT, "SO_CT", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.TK_KH_NH, "TK_KH_NH", 21, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.PHUONGTHUC_TT, "PHUONGTHUC_TT", 2, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //SO_THAM_CHIEU
                if (!CheckValidStringIsRequired(obj301.Data.SO_THAM_CHIEU, "SO_THAM_CHIEU", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //MA_NH_B
                if (!CheckValidStringIsRequired(obj301.Data.MA_NH_B, "MA_NH_B", 10, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidStringIsRequired(obj301.Data.MA_LHTHU, "MA_LHTHU", 3, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //TEN_LHTHU
                if (!CheckValidStringIsRequired(obj301.Data.TEN_LHTHU, "TEN_LHTHU", 100, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //TK_NS
                if (!CheckValidStringIsRequired(obj301.Data.TK_NS, "TK_NS", 4, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //MA_CHUONG
                if (!CheckValidStringIsRequired(obj301.Data.MA_CHUONG, "MA_CHUONG", 4, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj301.Data.MA_NDKT, "MA_NDKT", 4, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //LY_DO
                if (!CheckValidStringIsRequired(obj301.Data.LY_DO, "LY_DO", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                if (!CheckValidNumberIsRequired(obj301.Data.TTIEN_VPHC, "TTIEN_VPHC", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //LY_DO_NOP_CHAM
                if (!CheckValidStringIsNotRequired(obj301.Data.LY_DO_NOP_CHAM, "LY_DO_NOP_CHAM", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidNumberIsRequired(obj301.Data.TTIEN_NOP_CHAM, "TTIEN_NOP_CHAM", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return bResult;
        }
        public static string getSOCTU(string strMaNV)
        {
            try
            {
                string strNGAY_KB = TCS_GetNgayLV(strMaNV);
                string str_nam_kb = "";
                string str_thang_kb = "";
                str_nam_kb = strNGAY_KB.Substring(2, 4);
                return str_nam_kb + str_thang_kb + GenerateKey("TCS_CTU_ID_SEQ", 6);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "";
        }
        public static bool checkValidObj22(MSG22.MSG22 obj22, ref string strErrCode, ref string strErrMSG)
        {
            bool bResult = false;
            try
            {
                if (obj22.Data.CHUNGTU == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "CHUNGTU");
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.MST, "MST", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.TEN_NNT, "TEN_NNT", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.DIACHI_NNT, "DIACHI_NNT", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.MST_NNTHAY, "MST", 14, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.TEN_NNTHAY, "TEN_NNT", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.DIACHI_NNTHAY, "DIACHI_NNT", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.SO_TK_NHKBNN, "SO_TK_NHKBNN", 15, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.SH_KBNN, "SH_KBNN", 5, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.CQ_QLY_THU, "CQ_QLY_THU", 7, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.SO_CHUNGTU, "SO_CHUNGTU", 10, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidDateIsRequired(obj22.Data.CHUNGTU.NGAY_CHUNGTU, "NGAY_CHUNGTU", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.TINHCHAT_KHOAN_NOP, "TINHCHAT_KHOAN_NOP", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.CQUAN_THAMQUYEN, "CQUAN_THAMQUYEN", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.MA_NHTM, "MA_NHTM", 8, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.LOAI_TIEN, "LOAI_TIEN", 3, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.MA_HTHUC_NOP, "MA_HTHUC_NOP", 2, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.DIENGIAI_HTHUC_NOP, "DIENGIAI_HTHUC_NOP", 200, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.LHINH_NNT, "LHINH_NNT", 8, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.TK_KH_NH, "TK_KH_NH", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.MA_CN_TK, "MA_CN_TK", 10, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.MA_NH_CHUYEN, "MA_NH_CHUYEN", 8, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.SO_QUYET_DINH, "SO_QUYET_DINH", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //---------------------------
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.MA_NTK, "MA_NTK", 10, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.TEN_CQ_THU, "TEN_CQTHU", 500, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.SO_CHUNGTU, "SO_REFCORE", 30, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                //---------------------------
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.NGAY_QDINH, "NGAY_QDINH", 10, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                else
                {
                    if (obj22.Data.CHUNGTU.NGAY_QDINH.Length > 0)
                    {
                        if (!CheckValidDateIsRequired(obj22.Data.CHUNGTU.NGAY_QDINH, "NGAY_QDINH", 10, ref strErrCode, ref strErrMSG, "YYYY-MM-DD"))
                        {
                            return false;
                        }
                    }
                }
                if (!CheckValidNumberIsRequired(obj22.Data.CHUNGTU.TTIEN_NOPTHUE, "TTIEN_NOPTHUE", 20, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                // begin tb
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.SO_KHUNG, "SO_KHUNG", 50, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.SO_MAY, "SO_MAY", 50, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.DAC_DIEM_PTIEN, "DAC_DIEM_PTIEN", 500, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }
                if (!CheckValidStringIsNotRequired(obj22.Data.CHUNGTU.DIA_CHI_TS, "DIA_CHI_TS", 250, ref strErrCode, ref strErrMSG))
                {
                    return false;
                }

                //end of tb

                if (obj22.Data.CHUNGTU.CHUNGTU_CHITIET == null)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "CHUNGTU_CHITIET");
                    return false;
                }
                if (obj22.Data.CHUNGTU.CHUNGTU_CHITIET.Count <= 0)
                {
                    strErrCode = "90000";
                    strErrMSG = getErrorMSG(strErrCode).Replace("[_REPLACE_]", "CHUNGTU_CHITIET");
                    return false;
                }
                for (int i = 0; i < obj22.Data.CHUNGTU.CHUNGTU_CHITIET.Count; i++)
                {
                    if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].CHUONG, "CHUONG_" + (i + 1), 3, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].TIEUMUC, "TIEUMUC_" + (i + 1), 4, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].THONGTIN_KHOANNOP, "THONGTIN_KHOANNOP_" + (i + 1), 200, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidNumberIsRequired(obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].SO_TIEN, "SO_TIEN_" + (i + 1), 20, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                    if (!CheckValidStringIsRequired(obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].KY_THUE, "KY_THUE_" + (i + 1), 20, ref strErrCode, ref strErrMSG))
                    {
                        return false;
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return bResult;
        }
        public static void setObject22ToCTU(MSG22.MSG22 obj22, ref NewChungTu.infChungTuHDR hdr, ref List<NewChungTu.infChungTuDTL> lstDtl)
        {
            try
            {
                hdr.Ngay_KB = int.Parse(TCS_GetNgayLV(obj22.Header.UserID));
                string str_nam_kb = "";
                string str_thang_kb = "";
                str_nam_kb = hdr.Ngay_KB.ToString().Substring(2, 4);
                hdr.So_CT = obj22.Data.CHUNGTU.SO_CHUNGTU;
                hdr.Ma_CN = TCS_GetMACN_NV(obj22.Header.UserID);
                //' Mã NV
                hdr.Ma_NV = int.Parse(obj22.Header.UserID);
                // ' Số bút toán
                hdr.So_BT = Get_SoBT(hdr.Ngay_KB, hdr.Ma_NV);
                //' Số CT
                //    hdr.So_CT = str_nam_kb + str_thang_kb + GenerateKey("TCS_CTU_ID_SEQ", 6);

                //'Trang thai bds hien tai
                hdr.TT_BDS = "";

                hdr.TT_NSTT = "N";
                //'SHKB
                hdr.SHKB = obj22.Data.CHUNGTU.SH_KBNN;
                //' Mã điểm thu
                hdr.Ma_DThu = hdr.SHKB;
                //' Mã NNTien
                hdr.KyHieu_CT = TCS_GetKHCT(hdr.SHKB);
                hdr.Ma_NNTien = obj22.Data.CHUNGTU.MST_NNTHAY;

                //' Tên NNTien
                hdr.Ten_NNTien = obj22.Data.CHUNGTU.TEN_NNTHAY;

                //' Địa chỉ NNTien
                hdr.DC_NNTien = obj22.Data.CHUNGTU.DIACHI_NNTHAY;

                //' Mã NNT
                hdr.Ma_NNThue = obj22.Data.CHUNGTU.MST;

                //' Tên NNT
                hdr.Ten_NNThue = obj22.Data.CHUNGTU.TEN_NNT;
                //' Địa chỉ NNT
                hdr.DC_NNThue = obj22.Data.CHUNGTU.DIACHI_NNT;
                //' Số bút toán FT
                hdr.So_CT_NH = obj22.Data.CHUNGTU.SO_REFCORE;
                // ' hdr.Ten_NH_B = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ten_NH_B).InnerText
                //  ' Lý do
                hdr.Ly_Do = "";
                //  ' Mã TQ
                hdr.Ma_TQ = "0";

                hdr.So_QD = obj22.Data.CHUNGTU.SO_QUYET_DINH;
                hdr.CQ_QD = obj22.Data.CHUNGTU.CQUAN_THAMQUYEN;
                hdr.Ngay_QD = ToValue(obj22.Data.CHUNGTU.NGAY_QDINH, "DATE", "RRRR-MM-DD");

                ////  ' Ngay CT
                hdr.Ngay_CT = int.Parse(obj22.Data.CHUNGTU.NGAY_CHUNGTU.Replace("-", ""));// clsCTU.TCS_GetNgayLV(pv_intMaNV)
                ////  ' Ngay HT
                hdr.Ngay_HT = ToValue(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE");

                hdr.Ma_CQThu = obj22.Data.CHUNGTU.CQ_QLY_THU;
                hdr.Ten_cqthu = obj22.Data.CHUNGTU.TEN_CQ_THU;
                //  'THEM MA SAN PHAM
                hdr.MA_NH_TT = ""; //v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_TT).InnerText
                hdr.SAN_PHAM = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_SANPHAM).InnerText
                hdr.TT_CITAD = "0";// 'v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TT_CITAD).InnerText
                hdr.MA_NTK = obj22.Data.CHUNGTU.MA_NTK;// "1";
                //  ' DBHC
                hdr.Ma_Xa = "";
                hdr.XA_ID = "";// Get_XaID(hdr.Ma_Xa)
                hdr.Ma_Huyen = "";
                hdr.Ma_Tinh = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_DBHC_NNTIEN).InnerText

                //  ' TK No
                hdr.TK_No = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TKNo).InnerText.Replace(".", "").Replace(" ", "")
                hdr.TEN_TK_NO = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenTKNo).InnerText
                string v_strSHKB = obj22.Data.CHUNGTU.SH_KBNN;
                string v_strMaDBHC = obj22.Data.CHUNGTU.SH_KBNN;


                hdr.TK_GL_NH = "NULL";


                //  ' TK Co
                hdr.TK_Co = obj22.Data.CHUNGTU.SO_TK_NHKBNN;
                hdr.TEN_TK_CO = obj22.Data.CHUNGTU.TINHCHAT_KHOAN_NOP;
                //  ' So TKhai
                hdr.So_TK = "";
                hdr.Ngay_TK = "null";


                //  ' LHXNK
                hdr.LH_XNK = "";
                //  ' DVSDNS
                hdr.DVSDNS = "";
                hdr.Ten_DVSDNS = "";


                hdr.Ma_NT = obj22.Data.CHUNGTU.LOAI_TIEN;
                hdr.Ty_Gia = 1.0;// ChungTu.buChungTu.get_tygia(hdr.Ma_NT, hdr.TG_ID)
                hdr.TG_ID = "null";
                //    ' Mã Loại Thuế
                hdr.Ma_LThue = "01";

                //  ' So Khung - So May
                hdr.So_Khung = "";
                hdr.So_May = "";
                //  'Phuong thuc thanh toan
                hdr.PT_TT = obj22.Data.CHUNGTU.MA_HTHUC_NOP;
                hdr.TT_TThu = double.Parse(obj22.Data.CHUNGTU.TTIEN_NOPTHUE);
                hdr.TTien = double.Parse(obj22.Data.CHUNGTU.TTIEN_NOPTHUE);
                hdr.Mode = "0";




                // 'KIENVT : THEM THONG TIN VE NGAN HANG
                hdr.MA_NH_A = obj22.Data.CHUNGTU.MA_NH_CHUYEN;
                hdr.MA_NH_B = obj22.Data.CHUNGTU.MA_NHTM;// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_B).InnerText 'strMaNH.Split(";")(0).ToString
                hdr.MA_NH_TT = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_TT).InnerText 'strMaNH.Split(";")(2).ToString
                hdr.Ten_NH_B = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ten_NH_B).InnerText
                hdr.TEN_NH_TT = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_NH_TT).InnerText

                string strOnTTSP = "";
                try
                {
                    strOnTTSP = System.Configuration.ConfigurationSettings.AppSettings["ON_TTSP"].ToString();
                }
                catch (Exception exc)
                {
                    strOnTTSP = "N";
                }
                string strSQL = "";
                if (strOnTTSP.Equals("Y"))
                {
                    strSQL = "select * from tcs_dm_nh_giantiep_tructiep where shkb='" + hdr.SHKB + "' and ma_giantiep='" + obj22.Data.CHUNGTU.MA_NHTM + "'";
                }
                else
                {
                    strSQL = "select * from tcs_dm_nh_giantiep_tructiep where shkb='" + hdr.SHKB + "' and ma_giantiep='" + obj22.Data.CHUNGTU.MA_NHTM + "' AND TT_SHB <> '1'  ";
                }


                DataSet dsNH_B = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (dsNH_B != null)
                    if (dsNH_B.Tables.Count > 0)
                        if (dsNH_B.Tables[0].Rows.Count > 0)
                        {
                            hdr.MA_NH_B = dsNH_B.Tables[0].Rows[0]["ma_giantiep"].ToString();
                            hdr.MA_NH_TT = dsNH_B.Tables[0].Rows[0]["ma_tructiep"].ToString();
                            hdr.Ten_NH_B = dsNH_B.Tables[0].Rows[0]["ten_giantiep"].ToString();
                            hdr.TEN_NH_TT = dsNH_B.Tables[0].Rows[0]["ten_tructiep"].ToString();
                        }
                //  ' hoapt sua ma ngan hang truc tiep gian tiep
                //   Dim strMaNH As String = GetTT_NH_NHAN(v_strSHKB)


                //  'Them NH_HUONG
                hdr.NH_HUONG = obj22.Data.CHUNGTU.MA_NHTM;
                //  ' TK_KH_NH 
                hdr.TK_KH_NH = obj22.Data.CHUNGTU.TK_KH_NH;
                hdr.TEN_KH_NH = "";

                hdr.MA_CN_TK = obj22.Data.CHUNGTU.MA_CN_TK;
                //  strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.NGAY_KH_NH).InnerText
                hdr.NGAY_KH_NH = ToValue(obj22.Data.CHUNGTU.NGAY_CHUNGTU, "DATE", "RRRR-MM-DD");

                hdr.TK_KH_Nhan = "";

                hdr.Ten_KH_Nhan = "";
                hdr.Diachi_KH_Nhan = "";

                //  'THEM PHI GIAO DỊCH,VAT
                hdr.PHI_GD = obj22.Data.CHUNGTU.FEEAMT;// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_GD).InnerText.Replace(".", "")
                hdr.PHI_VAT = obj22.Data.CHUNGTU.VATAMT; ;// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_VAT).InnerText.Replace(".", "")
                hdr.PHI_GD2 = "0";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_GD2).InnerText.Replace(".", "")
                hdr.PHI_VAT2 = "0";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_VAT2).InnerText.Replace(".", "")
                hdr.PT_TINHPHI = "0";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PT_TINHPHI).InnerText.Replace(".", "")

                //  ' quận_huyên NNTien
                hdr.QUAN_HUYEN_NNTIEN = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Huyen_NNTIEN).InnerText
                //  ' Tinh _NNTien
                hdr.TINH_TPHO_NNTIEN = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Tinh_NNTIEN).InnerText
                //  Dim ref_no As String = String.Empty

                hdr.RM_REF_NO = hdr.So_CT;
                hdr.REF_NO = hdr.So_CT;

                //  ' So BK
                hdr.So_BK = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.So_BK).InnerText
                hdr.TIME_BEGIN = ToValue(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE");
                hdr.SO_CMND = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SO_CMND).InnerText
                hdr.SO_FONE = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SO_FONE).InnerText
                //  ' Ngay BK
                hdr.Ngay_BK = "null";


                hdr.Lan_In = "0";

                hdr.Ma_KS = hdr.Ma_NV.ToString();

                hdr.Trang_Thai = "07";
                hdr.LOAI_NNT = obj22.Data.CHUNGTU.LHINH_NNT;
                hdr.TEN_LOAI_NNT = "";
                hdr.TTien_NT = 0.0;
                hdr.Kenh_CT = obj22.Header.Product_Type;

                hdr.Ma_Xa = obj22.Data.CHUNGTU.MA_TINH_KBNN;

                hdr.So_May = obj22.Data.CHUNGTU.SO_MAY;
                hdr.So_Khung = obj22.Data.CHUNGTU.SO_KHUNG;
                hdr.DAC_DIEM_PTIEN = obj22.Data.CHUNGTU.DAC_DIEM_PTIEN;
                hdr.DIA_CHI_TS = obj22.Data.CHUNGTU.DIA_CHI_TS;

                for (int i = 0; i < obj22.Data.CHUNGTU.CHUNGTU_CHITIET.Count; i++)
                {
                    NewChungTu.infChungTuDTL dtl = new NewChungTu.infChungTuDTL();
                    dtl.SO_CT = hdr.So_CT;
                    // 'So tu tang
                    dtl.ID = GenerateKey("TCS_CTU_DTL_SEQ");
                    //                    ' SHKB
                    dtl.SHKB = hdr.SHKB;

                    //  ' Ngày KB
                    dtl.Ngay_KB = hdr.Ngay_KB;

                    //' Mã NV
                    dtl.Ma_NV = hdr.Ma_NV;

                    //' Số Bút toán
                    dtl.So_BT = hdr.So_BT;

                    // ' Mã điểm thu
                    dtl.Ma_DThu = hdr.Ma_DThu;

                    // 'Mã quỹ
                    dtl.MaQuy = "";

                    //  ' Mã Cấp
                    dtl.Ma_Cap = "";

                    //  ' Mã Chương
                    dtl.Ma_Chuong = obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].CHUONG;
                    //' CCH_ID
                    dtl.CCH_ID = "null";
                    //' Mã Loại
                    dtl.Ma_Loai = "";

                    //' Mã Khoản
                    dtl.Ma_Khoan = "";// v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.LKhoan).InnerText

                    //' CCH_ID
                    dtl.LKH_ID = "null";//Get_LKHID(dtl.Ma_Khoan)

                    //' Mã Mục
                    dtl.Ma_Muc = "";

                    //' Mã TMục
                    dtl.Ma_TMuc = obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].TIEUMUC;

                    //' MTM_ID
                    dtl.MTM_ID = "null";//Get_MTMucID(dtl.Ma_TMuc)

                    //' Mã TLDT
                    dtl.Ma_TLDT = "null";
                    //' DT_ID
                    dtl.DT_ID = "null";

                    //  '  Nội dung
                    dtl.Noi_Dung = obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].THONGTIN_KHOANNOP;


                    dtl.SoTien_NT = 0.0;
                    dtl.SoTien = double.Parse(obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].SO_TIEN);
                    hdr.TTien_NT += dtl.SoTien;

                    // ' Kỳ thuế
                    dtl.Ky_Thue = obj22.Data.CHUNGTU.CHUNGTU_CHITIET[i].KY_THUE;
                    // ' Mã dự phòng
                    dtl.Ma_DP = "null";
                    lstDtl.Add(dtl);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
    }
}
