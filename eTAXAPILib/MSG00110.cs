﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG00110
{

    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG00110
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data110 Data;
        [XmlElement("Error")]
        public ErrorEtax Error;
        public MSG00110()
        {
            Header = new HeaderEtax("00110");
            Data = new Data110();
        }
        public string MSG00110toXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG00110));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;
        }
    }
    public class Data110
    {
        public Data110()
        {
            SoThue = new SOTHUE();
        }
        [XmlElement("SOTHUE")]
        public SOTHUE SoThue;
    }
    public class SOTHUE
    {
        public SOTHUE()
        { Row_Sothue = new List<ROW_SOTHUE>(); }
        [XmlElement("ROW_SOTHUE")]
        public List<ROW_SOTHUE> Row_Sothue;
    }
    public class ROW_SOTHUE
    {
        public ROW_SOTHUE()
        { }
        public string MST { get; set; }
        public string TEN_NNT { get; set; }
        public string MOTA_DIACHI { get; set; }
        public string MA_TINH { get; set; }
        public string TEN_TINH { get; set; }
        public string MA_HUYEN { get; set; }
        public string TEN_HUYEN { get; set; }
        public string MA_XA { get; set; }
        public string TEN_XA { get; set; }
        public string MA_CHUONG { get; set; }
        public string MA_CQ_THU { get; set; }
        public string MA_TMUC { get; set; }
        public string NO_CUOI_KY { get; set; }
        public string SO_TAI_KHOAN_CO { get; set; }
        public string SO_QDINH { get; set; }
        public string NGAY_QDINH { get; set; }
        public string TI_GIA { get; set; }
        public string LOAI_TIEN { get; set; }
        public string LOAI_THUE { get; set; }
        public string TEN_CQ_THU { get; set; }
        public string MA_KB { get; set; }
        public string TEN_KB { get; set; }
        public string NOI_DUNG { get; set; }
        public string MA_TINH_KBNN { get; set; }
        public string MA_DBHC_KBNN { get; set; }
        public string MA_DBHC { get; set; }
        public string MA_NH_B { get; set; }
        public string TEN_NH_B { get; set; }
        public string MA_NH_TT { get; set; }
        public string TEN_NH_TT { get; set; }
        public string DIA_CHI_TS { get; set; }
        public string MA_HS { get; set; }
        public string MA_GCN { get; set; }
        public string SO_THUADAT { get; set; }
        public string SO_TO_BANDO { get; set; }
        public string HAN_NOP { get; set; }
    }
}
