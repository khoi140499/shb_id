﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG401a
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG401a
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data401a Data;
        public MSG401a()
        {
            Header = new HeaderEtax("401a");
            Data = new Data401a();
        }
        public string MSG401atoXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG401a));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;
        }
    }
    public class Data401a
    {
        public Data401a()
        {
            CHUNGTU = new CHUNGTU();
        }
        [XmlElement("CHUNGTU")]
        public CHUNGTU CHUNGTU;
    }
    public class CHUNGTU
    {
        public CHUNGTU()
        { }
        [XmlElement(Order = 1)]
        public string MST { get; set; }

        [XmlElement(Order = 2)]
        public string TEN_NNT { get; set; }
        [XmlElement(Order = 3)]
        public string DIACHI_NNT { get; set; }
        [XmlElement(Order = 4)]
        public string MST_NNTHAY { get; set; }
        [XmlElement(Order = 5)]
        public string TEN_NNTHAY { get; set; }
        [XmlElement(Order = 6)]
        public string DIACHI_NNTHAY { get; set; }
        [XmlElement(Order = 7)]
        public string SO_TK_NHKBNN { get; set; }
        [XmlElement(Order = 8)]
        public string SH_KBNN { get; set; }
        [XmlElement(Order = 9)]
        public string CQ_QLY_THU { get; set; }
        [XmlElement(Order = 10)]
        public string SO_CHUNGTU { get; set; }
        [XmlElement(Order = 11)]
        public string NGAY_CHUNGTU { get; set; }
        [XmlElement(Order = 12)]
        public string TINHCHAT_KHOAN_NOP { get; set; }
        [XmlElement(Order = 13)]
        public string CQUAN_THAMQUYEN { get; set; }
        [XmlElement(Order = 14)]
        public string MA_NHTM { get; set; }
        [XmlElement(Order = 15)]
        public string LOAI_TIEN { get; set; }
        [XmlElement(Order = 16)]
        public string MA_HTHUC_NOP { get; set; }
        [XmlElement(Order = 17)]
        public string DIENGIAI_HTHUC_NOP { get; set; }
        [XmlElement(Order = 18)]
        public string LHINH_NNT { get; set; }
        [XmlElement(Order = 19)]
        public string SO_QUYET_DINH { get; set; }
        [XmlElement(Order = 20)]
        public string NGAY_QDINH { get; set; }
        [XmlElement(Order = 21)]
        public string TK_KH_NH { get; set; }
        [XmlElement(Order = 22)]
        public string MA_NH_CHUYEN { get; set; }
        [XmlElement(Order = 23)]
        public string TTIEN_NOPTHUE { get; set; }
        [XmlElement(Order = 24)]
        public string FEEAMT { get; set; }
        [XmlElement(Order = 25)]
        public string VATAMT { get; set; }
        [XmlElement(Order = 26)]
        public string TRANG_THAI { get; set; }
        [XmlElement(Order = 27)]
        public string TEN_TRANG_THAI { get; set; }
        [XmlElement(Order = 28)]
        public List<CHUNGTU_CHITIET> CHUNGTU_CHITIET;
    }
    public class CHUNGTU_CHITIET
    {
        public CHUNGTU_CHITIET()
        { }
        [XmlElement(Order = 1)]
        public string CHUONG { get; set; }
        [XmlElement(Order = 2)]
        public string TIEUMUC { get; set; }
        [XmlElement(Order = 3)]
        public string THONGTIN_KHOANNOP { get; set; }
        [XmlElement(Order = 4)]
        public string SO_TIEN { get; set; }
        [XmlElement(Order = 5)]
        public string KY_THUE { get; set; }

    }
}
