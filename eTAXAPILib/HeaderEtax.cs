﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib
{
    
    public class HeaderEtax
    {
        public string Message_Type { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Product_Type { get; set; }
        public string Request_ID { get; set; }
        public HeaderEtax()
        {
            Message_Type = "";
            Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            Transaction_ID = Guid.NewGuid().ToString(); 
            Product_Type = "";
            Request_ID = "";
        }
        public HeaderEtax(string pvMSGType)
        {
            Message_Type = pvMSGType;
            Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            Transaction_ID = Guid.NewGuid().ToString();
            Product_Type = "";
            Request_ID = "";
        }
        public HeaderEtax(string pvMSGType,string pvStrReQuestID)
        {
            Message_Type = pvMSGType;
            Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            Transaction_ID = Guid.NewGuid().ToString();
            Product_Type = "";
            Request_ID = pvStrReQuestID;
        }
        public HeaderEtax(string pvMSGType, string pvStrReQuestID, string pvProduct_Type)
        {
            Message_Type = pvMSGType;
            Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            Transaction_ID = Guid.NewGuid().ToString();
            Product_Type = pvProduct_Type;
            Request_ID = pvStrReQuestID;
        }
    }
    public class HeaderEtaxIN
    {
        public string Message_Type { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public string Product_Type { get; set; }
        public string Request_ID { get; set; }
        public HeaderEtaxIN()
        { }
    }
}
