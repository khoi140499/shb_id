﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG00047
{

    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG00047
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data47 Data;
        [XmlElement("Error")]
        public ErrorEtax Error;

        public MSG00047()
        {
            Header = new HeaderEtax("00047");
            Data = new Data47();
        }
        public string MSG00047toXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG00047));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;
        }
    }
    public class Data47
    {
        public Data47()
        {
            Thongtin_NNT = new THONGTIN_NNT();
        }
        [XmlElement("THONGTIN_NNT")]
        public THONGTIN_NNT Thongtin_NNT;
    }
    public class THONGTIN_NNT
    {
        public THONGTIN_NNT()
        {
            Thongtinchung = new THONGTINCHUNG();
            Diachi = new DIACHI();
            Sothue = new SOTHUE();
        }
        [XmlElement("THONGTINCHUNG")]
        public THONGTINCHUNG Thongtinchung;
        [XmlElement("DIACHI")]
        public DIACHI Diachi;
        [XmlElement("SOTHUE")]
        public SOTHUE Sothue;
    }
    public class THONGTINCHUNG
    {
        public THONGTINCHUNG()
        { row_nnt = new List<ROW_NNT>(); }
        [XmlElement("ROW_NNT")]
        public List<ROW_NNT> row_nnt;
    }
    public class ROW_NNT
    {
        public ROW_NNT()
        { }
        public string MST { get; set; }
        public string TEN_NNT { get; set; }
        public string LOAI_NNT { get; set; }
        public string CHUONG { get; set; }
        public string SO_CMND { get; set; }
        public string MA_CQT_QL { get; set; }
    }
    public class DIACHI
    {
        public DIACHI()
        { Row_Diachi = new List<ROW_DIACHI>(); }
        [XmlElement("ROW_DIACHI")]
        public List<ROW_DIACHI> Row_Diachi;
    }
    public class ROW_DIACHI
    {
        public ROW_DIACHI()
        { }
        public string MOTA_DIACHI { get; set; }
        public string MA_TINH { get; set; }
        public string MA_HUYEN { get; set; }
        public string MA_XA { get; set; }
    }
    public class SOTHUE
    {
        public SOTHUE()
        { Row_Sothue = new List<ROW_SOTHUE>(); }
        [XmlElement("ROW_SOTHUE")]
        public List<ROW_SOTHUE> Row_Sothue;
    }
    public class ROW_SOTHUE
    {
        public ROW_SOTHUE()
        { }
        public string MA_CHUONG { get; set; }
        public string MA_CQ_THU { get; set; }
        public string MA_TMUC { get; set; }
        public string NO_CUOI_KY { get; set; }
        public string SO_TAI_KHOAN_CO { get; set; }
        public string SO_QDINH { get; set; }
        public string NGAY_QDINH { get; set; }
        public string TI_GIA { get; set; }
        public string LOAI_TIEN { get; set; }
        public string LOAI_THUE { get; set; }
        public string TEN_CQ_THU { get; set; }
        public string MA_KB { get; set; }
        public string TEN_KB { get; set; }
        public string NOI_DUNG { get; set; }
        public string MA_TINH_KBNN { get; set; }
        public string MA_DBHC_KBNN { get; set; }
        public string MA_NH_B { get; set; }
        public string TEN_NH_B { get; set; }
        public string MA_NH_TT { get; set; }
        public string TEN_NH_TT { get; set; }
        public string SO_KHUNG { get; set; }
        public string SO_MAY { get; set; }
        public string MA_DBHC { get; set; }
        public string KY_THUE { get; set; }
        public string DAC_DIEM_PTIEN { get; set; }
    }
}
