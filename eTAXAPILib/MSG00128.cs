﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG00128
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG00128
    {
        [XmlElement("Header")]
        public HeaderEtaxIN Header;
        [XmlElement("Data")]
        public Data00128 Data;
        public MSG00128()
        { }
        public MSG00128 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG00128), new XmlRootAttribute("ETAX"));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG00128 LoadedObjTmp = (MSG00128)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data00128
    {
        [XmlElement("MST")]
        public string MST { get; set; }
        public Data00128()
        {

        }

    }
}
