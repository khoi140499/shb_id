﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib
{
    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG299
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data299 Data;
        [XmlElement("Error")]
        public Error Error;
        public MSG299()
        {
            Header = new HeaderEtax("E0299");
            Data = new Data299();
            Error = new Error();
        }
        public string MSG299toXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG299));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
    public class Data299
    {
        public Data299()
        {
           

        }
    }
    public class Error
    {
        public Error() { ErrorNumber="99999"; ErrorMessage = "System Error "; }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }
    
}
