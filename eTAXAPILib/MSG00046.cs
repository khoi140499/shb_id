﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG00046
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG00046
    {
        [XmlElement("Header")]
        public HeaderEtaxIN Header;
        [XmlElement("Data")]
        public Data00046 Data;
        public MSG00046()
        { }
        public MSG00046 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG00046), new XmlRootAttribute("ETAX"));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG00046 LoadedObjTmp = (MSG00046)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data00046
    {
        [XmlElement("SO_TK")]
        public string SO_TK { get; set; }
        [XmlElement("MST")]
        public string MST { get; set; }
        public Data00046()
        {

        }

    }
}
