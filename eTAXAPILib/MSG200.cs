﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG200
{
    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG200
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data200 Data;
        [XmlElement("Error")]
        public Error Error;
        public MSG200()
        {
            Header = new HeaderEtax("E0200");
            Data = new Data200();
            Error = new Error();
        }
        public string MSG200toXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG200));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
    public class Data200
    {
        public Data200()
        {
            So_TN_CT = "";
            Ngay_TN_CT = "";
        }
         [XmlElement("So_TN_CT")]
        public string So_TN_CT { get; set; }
         [XmlElement("Ngay_TN_CT")]
        public string Ngay_TN_CT { get; set; }
    }
    public class Error
    {
        public Error() { ErrorNumber="99999"; ErrorMessage = "System Error "; }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }
    
}
