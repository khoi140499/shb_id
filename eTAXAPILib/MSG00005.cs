﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG00005
{
     [Serializable]
    [XmlRootAttribute( Namespace = "http://www.cpandl.com",ElementName = "ETAX", IsNullable=false)]
    public class MSG00005
    {
        [XmlElement("Header")]
         public HeaderEtaxIN Header;
        [XmlElement("Data")]
        public Data005 Data;
        public MSG00005()
        { }
        public MSG00005 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG00005), new XmlRootAttribute("ETAX"));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG00005 LoadedObjTmp = (MSG00005)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
     public class Data005
     {
         [XmlElement("MST")]
         public string MST { get; set; }
         public Data005()
         {
            
         }
     
     }
}
