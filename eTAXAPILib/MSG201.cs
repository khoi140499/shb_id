﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG201
{
    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG201
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data201 Data;
        [XmlElement("Error")]
        public Error Error;
        public MSG201()
        {
            Header = new HeaderEtax("201");
            Data = new Data201();
           
        }
        public string MSG201toXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG201));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
    public class Data201
    {
        public Data201()
        {
            Item = new List<Item>();
        }

        [XmlElement("Item")]
        public List<Item> Item { get; set; }
    }
    public class Item
    {
        public Item()
        {
            Ma_Cuc = "";
            Ten_Cuc = "";
            Ma_HQ_PH = "";
            Ten_HQ_PH = "";
            Ma_HQ_CQT = "";
            Ma_DV = "";
            Ten_DV = "";
            Ma_Chuong = "";
            Ma_HQ = "";
            Ten_HQ = "";
            Ma_LH = "";
            Ten_LH = "";
            Nam_DK = "";
            So_TK = "";
            Ma_NTK = "";
            Ten_NTK = "";
            Ma_LT = "";
            Ma_HTVCHH = "";
            Ten_HTVCHH = "";
            Ngay_DK = "";
            Ma_KB = "";
            Ten_KB = "";
            TKKB = "";
            TTNo = "";
            Ten_TTN = "";
            TTNo_CT = "";
            Ten_TTN_CT = "";
            DuNo_TO = "";
            MA_DBHC_KBNN = "";
            MA_TINH_KBNN = "";
            MA_NH_B = "";
            TEN_NH_B = "";
            TEN_NH_TT = "";
            MA_NH_TT = "";
            CT_No = new List<CT_No>();

        }
        [XmlElement(Order = 1)]
        public string Ma_Cuc { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_Cuc { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_HQ_PH { get; set; }
        [XmlElement(Order = 4)]
        public string Ten_HQ_PH { get; set; }
        [XmlElement(Order = 5)]
        public string Ma_HQ_CQT { get; set; }
        [XmlElement(Order = 6)]
        public string Ma_DV { get; set; }
        [XmlElement(Order = 7)]
        public string Ten_DV { get; set; }
        [XmlElement(Order = 8)]
        public string Ma_Chuong { get; set; }
        [XmlElement(Order = 9)]
        public string Ma_HQ { get; set; }
        [XmlElement(Order = 10)]
        public string Ten_HQ { get; set; }
        [XmlElement(Order = 11)]
        public string Ma_LH { get; set; }
        [XmlElement(Order = 12)]
        public string Ten_LH { get; set; }
        [XmlElement(Order = 13)]
        public string Nam_DK { get; set; }
        [XmlElement(Order = 14)]
        public string So_TK { get; set; }
        [XmlElement(Order = 15)]
        public string Ma_NTK { get; set; }
        [XmlElement(Order = 16)]
        public string Ten_NTK { get; set; }
        [XmlElement(Order = 17)]
        public string Ma_LT { get; set; }
        [XmlElement(Order = 18)]
        public string Ma_HTVCHH { get; set; }
        [XmlElement(Order = 19)]
        public string Ten_HTVCHH { get; set; }
        [XmlElement(Order = 20)]
        public string Ngay_DK { get; set; }
        [XmlElement(Order = 21)]
        public string Ma_KB { get; set; }
        [XmlElement(Order = 22)]
        public string Ten_KB { get; set; }
        [XmlElement(Order = 23)]
        public string TKKB { get; set; }
        [XmlElement(Order = 24)]
        public string TTNo { get; set; }
        [XmlElement(Order = 25)]
        public string Ten_TTN { get; set; }
        [XmlElement(Order = 26)]
        public string TTNo_CT { get; set; }
        [XmlElement(Order = 27)]
        public string Ten_TTN_CT { get; set; }
        [XmlElement(Order = 28)]
        public string DuNo_TO { get; set; }
        [XmlElement(Order = 29)]
        public string MA_TINH_KBNN { get; set; }
        [XmlElement(Order = 30)]
        public string MA_DBHC_KBNN { get; set; }
        [XmlElement(Order = 31)]
        public string MA_NH_B { get; set; }
        [XmlElement(Order = 32)]
        public string TEN_NH_B { get; set; }
        [XmlElement(Order = 33)]
        public string MA_NH_TT { get; set; }
        [XmlElement(Order = 34)]
        public string TEN_NH_TT { get; set; }
        [XmlElement(Order = 35)]
        public List<CT_No> CT_No { get; set; }

    }
    public class CT_No
    {

        [XmlElement(Order = 1)]
        public string LoaiThue { get; set; }

        [XmlElement(Order = 2)]
        public string Khoan { get; set; }

        [XmlElement(Order = 3)]
        public string TieuMuc { get; set; }

        [XmlElement(Order = 4)]
        public string DuNo { get; set; }
        [XmlElement(Order = 5)]
        public string NoiDung { get; set; }

    }
    

}
