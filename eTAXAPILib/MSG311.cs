﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace eTAXAPILib.MSG311
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG311
    {
        [XmlElement("Header")]
        public HeaderEtax Header;
        [XmlElement("Data")]
        public Data311 Data;
        public MSG311()
        {
            Header = new HeaderEtax("311");
            Data = new Data311();
        }
        public string MSG311toXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG311));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, this);
            string strTemp = sw.ToString();
            return strTemp;
        }
    }
    public class Data311
    {
        public Data311()
        {
            DKY_NTHUE = new List<eTAXAPILib.MSG311.DKY_NTHUE>();
        }
        [XmlElement("DKY_NTHUE")]
        public List<DKY_NTHUE> DKY_NTHUE;
    }
    public class DKY_NTHUE
    {
        public DKY_NTHUE()
        { }
        [XmlElement(Order = 1)]
        public string So_HS { get; set; }
        [XmlElement(Order = 2)]
        public string Loai_HS { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_DV { get; set; }
        [XmlElement(Order = 4)]
        public string Ten_DV { get; set; }
        [XmlElement(Order = 5)]
        public string DiaChi { get; set; }
        [XmlElement(Order = 6)]
        public ThongTin_NNT ThongTin_NNT;

        [XmlElement(Order = 7)]
        public string TRANG_THAI { get; set; }
        [XmlElement(Order = 8)]
        public string TEN_TRANG_THAI { get; set; }
        [XmlElement(Order = 9)]
        public ThongTinTaiKhoan ThongTinTaiKhoan;
        
    }
    public class ThongTin_NNT
    {
        public ThongTin_NNT()
        { }
         [XmlElement(Order = 1)]
        public string So_CMT { get; set; }
         [XmlElement(Order = 2)]
         public string Ho_Ten { get; set; }
         [XmlElement(Order = 3)]
         public string NgaySinh { get; set; }
         [XmlElement(Order = 4)]
         public string NguyenQuan { get; set; }
         [XmlElement(Order = 5)]
         public List<ThongTinLienHe> ThongTinLienHe;
         [XmlElement(Order = 6)]
         public ChungThuSo ChungThuSo;
    }

    public class ChungThuSo
    {
        public ChungThuSo()
        { }
        
        [XmlElement(Order = 1)]
        public string SerialNumber	{ get; set; }
[XmlElement(Order = 2)]
        public string Noi_Cap	{ get; set; }
[XmlElement(Order = 3)]
        public string Ngay_HL	{ get; set; }
[XmlElement(Order = 4)]
public string Ngay_HHL { get; set; }

    }
    public class ThongTinLienHe
    {
        public ThongTinLienHe()
        { }
        [XmlElement(Order = 1)]
        public string So_DT { get; set; }
        [XmlElement(Order = 2)]
        public string Email { get; set; }
    }
    public class ThongTinTaiKhoan
    {
        public ThongTinTaiKhoan()
        { }
        [XmlElement(Order = 1)]
        public string TaiKhoan_TH { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_TaiKhoan_TH { get; set; }
    }
}
