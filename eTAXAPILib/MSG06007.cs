﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG06007
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG06007
    {
        [XmlElement("Header")]
        public HeaderEtaxIN Header;
        [XmlElement("Data")]
        public Data06007 Data;
        public MSG06007()
        { }
        public MSG06007 MSGToObject(string p_XML)
        {
           // XmlSerializer serializer = new XmlSerializer(typeof(MSG06007), new XmlRootAttribute("ETAX"));
            XmlSerializer serializer = new XmlSerializer(typeof(eTAXAPILib.MSG06007.MSG06007));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG06007 LoadedObjTmp = (MSG06007)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data06007
    {
        [XmlElement("TBAO_TKHOAN_NH")]
        public TBAO_TKHOAN_NH TBAO_TKHOAN_NH { get; set; }
        public Data06007()
        {

        }

    }
    public class TBAO_TKHOAN_NH
    {
        public TBAO_TKHOAN_NH() { }
        [XmlElement("MA_TBAO")]
        public string MA_TBAO { get; set; }
        [XmlElement("MST")]
        public string MST { get; set; }
        [XmlElement("TEN_NNT")]
        public string TEN_NNT { get; set; }
        [XmlElement("DIACHI_NNT")]
        public string DIACHI_NNT { get; set; }
        [XmlElement("SDT_NNT")]
        public string SDT_NNT { get; set; }
        [XmlElement("EMAIL_NNT")]
        public string EMAIL_NNT { get; set; }
        [XmlElement("SERIAL_CERT_NTHUE")]
        public string SERIAL_CERT_NTHUE { get; set; }
        [XmlElement("ISSUER_CERT_NTHUE")]
        public string ISSUER_CERT_NTHUE { get; set; }
        [XmlElement("TTIN_TKHOAN")]
        public List<TTIN_TKHOAN> TTIN_TKHOAN;

    }
    public class TTIN_TKHOAN
    {
        [XmlElement("STK")]
        public string STK { get; set; }
        [XmlElement("TEN_TK")]
        public string TEN_TK { get; set; }
        [XmlElement("BRANCH_CODE")]
        public string BRANCH_CODE { get; set; }
        [XmlElement("CIFNO")]
        public string CIFNO { get; set; }
        public TTIN_TKHOAN()
        {
           

        }
    }
}
