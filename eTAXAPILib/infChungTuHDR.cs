﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eTAXAPILib.NewChungTu
{
    public partial class infChungTuHDR
    {

        private string strSHKB;
        private int iNgay_KB;
        private int iMa_NV;
        private int iSo_BT;  // số bút toán
        private string strMa_DThu;
        private int iSo_BThu;
        private string strKyHieu_CT;
        private string strSo_CT;
        private string strSo_CT_TuChinh;
        private string strSo_CT_NH;
        private string strMa_NNTien;
        private string strTen_NNTien;
        private string strDC_NNTien;
        private string strMa_NNThue;
        private string strTen_NNThue;
        private string strDC_NNThue;
        private string strLy_Do;
        private string iMa_KS;
        private string iMa_TQ;
        private string strSo_QD;
        private string strNgay_QD;
        private string strCQ_QD;
        private int iNgay_CT;
        private string strNgay_HT;
        private string strMa_CQThu;
        private string strXA_ID;
        private string strMa_Tinh;
        private string _Ten_tinh;
        private string strMa_Huyen;
        private string strMa_Xa;
        private string _Ten_Xa;
        private string strTK_No;
        private string strTen_TKNo;
        private string strTK_Co;
        private string strTen_TKCo;
        private string strSo_TK;
        private string strNgay_TK;
        private string strLH_XNK;
        private string strDVSDNS;
        private string strTen_DVSDNS;
        private string strMa_NT;
        private double dblTy_Gia;
        private string iTG_ID;
        private string strMa_LThue;
        private string strSo_Khung;
        private string strSo_May;

        private string strTK_KH_NH;
        private string strNGAY_KH_NH;

        private string strMA_NH_A;
        private string strMA_NH_B;

        private string strPHI_GD;
        private string strPHI_VAT;
        private string strPT_TINHPHI;

        private double dblTTien;
        private double dblTT_TThu;
        private string strLan_In;
        private string strTrang_Thai;

        private string strTK_KH_Nhan;
        private string strTen_KH_Nhan;
        private string strDiachi_KH_Nhan;
        private double dblTTien_NT;
        private int intSo_BT_KTKT;

        private string strSo_BK;
        private string strNgay_BK;
        private string strPT_TT;
        private string strTK_GL_NH;
        private string strTEN_KH_NH;
        private string strTK_KB_NH;
        private string intMode;
        private string strTT_NSTT;
        private string strTT_BDS;
        private string strtellerGDV;
        private string strtellerKSV;
        private string strSoRM;
        private string strRM_REF_NO;
        private string strREF_NO;

        private string strQuan_Huyen_NNTien;
        private string strTinh_TPho_NNTien;
        private string strTKHTNH;

        // Thuoc tinh dung cho Bao lanh HQ
        private string strKhct;
        private string strTen_kb;
        private string strTen_nv;
        private string strHuyen_nnthue;
        private string strTinh_nnthue;
        private string strTen_ks;
        private string strTen_cqthu;
        private string strLhxnk;
        private string strVt_lhxnk;
        private string strTen_lhxnk;
        private string strTen_nt;
        private string strTen_nh_a;
        private long strTtien_tthu;
        private string strGhi_chu;
        private string strTk_ns;
        private string strTen_tk_ns;
        private string strPhuong_thuc;
        private string strHinh_thuc;
        private string strKhoi_phuc;
        private string strReponse_code;
        private string strHq_tran_id;
        private string strHq_loai_thue;
        private string strRes_hq;
        private string strSo_bl;
        private int strSongay_bl;
        private string strKieu_bl;
        private string strMa_hq_ph;
        private string strDien_giai;
        private string strTrangthai_lapct;
        private string strNgay_batdau;
        private string strNgay_ketthuc;
        private string strError_code_hq;
        private string strSo_ct_ctu;
        private string strDchieu_hq;
        private string strIs_ma_hq;
        private string strNgay_dc_hq;
        private string strKb_thu_ho;
        private string strMa_loaitien;
        private string strMa_hq_cqt;
        private int strSo_bt_hq;
        private string strTrangThai_BL;

        private string strTen_hq;
        private string strTen_hq_ph;
        // thêm thông tin về số hợp đồng
        private string strHD_NT;
        private string strNgay_HD_NT;
        private string strHDon;
        private string strVTD;
        private string strNgay_HDon;
        private string strNgay_VTD;
        private string strKenh_CT = "01";
        private string strVTD2;
        private string strNgay_VTD2;
        private string strVTD3;
        private string strNgay_VTD3;
        private string strVTD4;
        private string strNgay_VTD4;
        private string strVTD5;
        private string strNgay_VTD5;
        private string strMa_hq_kb;
        // ****************************************************************************
        // Người sửa: Anhld
        // Ngày 14/09/2012
        // Mục đích: Them ma hai quan va loai tien thue
        // *****************************************************************************
        private string strMa_hq;
        private string strLoai_tt;
        private string strTen_MA_HQ;
        private string strLy_do_ctra;
        private string strMA_SANPHAM;
        private string strTT_CITAD;
        private string strSEQ_NO;
        private string strLoaiBL;
        private string strMaCapChuong;
        private string strPHI_GD2;
        private string strPHI_VAT2;
        private string strCif;
        private string strTIME_BEGIN;
        private string strSO_CMND;
        private string strSO_FONE;
        // 01/10/2012-manhnv
        private string strMa_nh_tt;
        private string strTen_nh_tt;
        private string strMa_DV_DD;
        private string strTen_DV_DD;
        private string strMA_NTK;
        private string str_Ten_NH_B;
        private string strMA_CN;
        private string strSO_XCARD;
        private string strNH_HUONG;
        private string strDIENGIAI_HQ;
        // sonmt
        private string strLoaiNNT;
        private string strTenLoaiNNT;
        private string _LOAI_CTU;
        private string _Ma_Huyen_NNThue;
        private string _Ma_Tinh_NNThue;
        private string _MA_QUAN_HUYENNNTIEN;
        private string _MA_TINH_TPNNTIEN;
        private DateTime _NGAY_BAONO;
        private DateTime _NGAY_BAOCO;
        private int _TT_CTHUE;
        private string _MA_CUC;
        private string _TEN_CUC;
        private string _REMARKS;
        private string _DIENGIAI_HQ;
        private string strNgayKS;
        // tt84
        private string strMa_ThamChieu;
        private string strMA_CN_TK;
        private string strDacDiemPTien;
        private string strDiaChiTS;


        public string DAC_DIEM_PTIEN
        {
            get
            {
                return strDacDiemPTien;
            }
            set
            {
                strDacDiemPTien = value;
            }
        }
        public string DIA_CHI_TS
        {
            get
            {
                return strDiaChiTS;
            }
            set
            {
                strDiaChiTS = value;
            }
        }

        public string MA_CN_TK
        {
            get
            {
                return strMA_CN_TK;
            }
            set
            {
                strMA_CN_TK = value;
            }
        }
        public string MA_THAMCHIEU
        {
            get
            {
                return strMa_ThamChieu;
            }
            set
            {
                strMa_ThamChieu = value;
            }
        }
        public string NGAY_KS
        {
            get
            {
                return strNgayKS;
            }
            set
            {
                strNgayKS = value;
            }
        }
        public string REMARKS
        {
            get
            {
                return _REMARKS;
            }
            set
            {
                _REMARKS = value;
            }
        }

        public string TEN_LOAI_NNT
        {
            get
            {
                return strTenLoaiNNT;
            }
            set
            {
                strTenLoaiNNT = value;
            }
        }
        public string LOAI_NNT
        {
            get
            {
                return strLoaiNNT;
            }
            set
            {
                strLoaiNNT = value;
            }
        }

        public string SO_XCARD
        {
            get
            {
                return strSO_XCARD;
            }
            set
            {
                strSO_XCARD = value;
            }
        }
        public string Ten_NH_B
        {
            get
            {
                return str_Ten_NH_B;
            }
            set
            {
                str_Ten_NH_B = value;
            }
        }
        public string TrangThai_BL
        {
            get
            {
                return strTrangThai_BL;
            }
            set
            {
                strTrangThai_BL = value;
            }
        }
        public string Kenh_CT
        {
            get
            {
                return strKenh_CT;
            }
            set
            {
                strKenh_CT = value;
            }
        }
        public string Ngay_HD_NT
        {
            get
            {
                return strNgay_HD_NT;
            }
            set
            {
                strNgay_HD_NT = value;
            }
        }
        public string Ngay_HDon
        {
            get
            {
                return strNgay_HDon;
            }
            set
            {
                strNgay_HDon = value;
            }
        }
        public string Ngay_VTD
        {
            get
            {
                return strNgay_VTD;
            }
            set
            {
                strNgay_VTD = value;
            }
        }
        public string HDon
        {
            get
            {
                return strHDon;
            }
            set
            {
                strHDon = value;
            }
        }
        public string VTD
        {
            get
            {
                return strVTD;
            }
            set
            {
                strVTD = value;
            }
        }
        public string HD_NT
        {
            get
            {
                return strHD_NT;
            }
            set
            {
                strHD_NT = value;
            }
        }
        public string MA_HQ
        {
            get
            {
                return strMa_hq;
            }
            set
            {
                strMa_hq = value;
            }
        }
        public string TEN_HQ
        {
            get
            {
                return strTen_hq;
            }
            set
            {
                strTen_hq = value;
            }
        }
        public string TEN_HQ_PH
        {
            get
            {
                return strTen_hq_ph;
            }
            set
            {
                strTen_hq_ph = value;
            }
        }
        public string LOAI_TT
        {
            get
            {
                return strLoai_tt;
            }
            set
            {
                strLoai_tt = value;
            }
        }
        public string TEN_MA_HQ
        {
            get
            {
                return strTen_MA_HQ;
            }
            set
            {
                strTen_MA_HQ = value;
            }
        }

        public string LY_DO_CTRA
        {
            get
            {
                return strLy_do_ctra;
            }
            set
            {
                strLy_do_ctra = value;
            }
        }

        // ****************************************************************************
        // End
        // *****************************************************************************

        public string KHCT
        {
            get
            {
                return strKhct;
            }
            set
            {
                strKhct = value;
            }
        }


        public string Ten_kb
        {
            get
            {
                return strTen_kb;
            }
            set
            {
                strTen_kb = value;
            }
        }

        public string Ten_nv
        {
            get
            {
                return strTen_nv;
            }
            set
            {
                strTen_nv = value;
            }
        }

        public string Huyen_nnthue
        {
            get
            {
                return strHuyen_nnthue;
            }
            set
            {
                strHuyen_nnthue = value;
            }
        }

        public string Tinh_nnthue
        {
            get
            {
                return strTinh_nnthue;
            }
            set
            {
                strTinh_nnthue = value;
            }
        }

        public string Ten_ks
        {
            get
            {
                return strTen_ks;
            }
            set
            {
                strTen_ks = value;
            }
        }

        public string Ten_cqthu
        {
            get
            {
                return strTen_cqthu;
            }
            set
            {
                strTen_cqthu = value;
            }
        }

        public string Lhxnk
        {
            get
            {
                return strLhxnk;
            }
            set
            {
                strLhxnk = value;
            }
        }

        public string Vt_lhxnk
        {
            get
            {
                return strVt_lhxnk;
            }
            set
            {
                strVt_lhxnk = value;
            }
        }

        public string Ten_lhxnk
        {
            get
            {
                return strTen_lhxnk;
            }
            set
            {
                strTen_lhxnk = value;
            }
        }

        public string Ten_nt
        {
            get
            {
                return strTen_nt;
            }
            set
            {
                strTen_nt = value;
            }
        }

        public string Ten_nh_a
        {
            get
            {
                return strTen_nh_a;
            }
            set
            {
                strTen_nh_a = value;
            }
        }

        public long Ttien_tthu
        {
            get
            {
                return strTtien_tthu;
            }
            set
            {
                strTtien_tthu = value;
            }
        }

        public string Ghi_chu
        {
            get
            {
                return strGhi_chu;
            }
            set
            {
                strGhi_chu = value;
            }
        }

        public string Tk_ns
        {
            get
            {
                return strTk_ns;
            }
            set
            {
                strTk_ns = value;
            }
        }

        public string Ten_tk_ns
        {
            get
            {
                return strTen_tk_ns;
            }
            set
            {
                strTen_tk_ns = value;
            }
        }

        public string Phuong_thuc
        {
            get
            {
                return strPhuong_thuc;
            }
            set
            {
                strPhuong_thuc = value;
            }
        }

        public string Hinh_thuc
        {
            get
            {
                return strHinh_thuc;
            }
            set
            {
                strHinh_thuc = value;
            }
        }

        public string Khoi_phuc
        {
            get
            {
                return strKhoi_phuc;
            }
            set
            {
                strKhoi_phuc = value;
            }
        }

        public string Reponse_code
        {
            get
            {
                return strReponse_code;
            }
            set
            {
                strReponse_code = value;
            }
        }

        public string Hq_tran_id
        {
            get
            {
                return strHq_tran_id;
            }
            set
            {
                strHq_tran_id = value;
            }
        }

        public string Hq_loai_thue
        {
            get
            {
                return strHq_loai_thue;
            }
            set
            {
                strHq_loai_thue = value;
            }
        }

        public string Res_hq
        {
            get
            {
                return strRes_hq;
            }
            set
            {
                strRes_hq = value;
            }
        }

        public string So_bl
        {
            get
            {
                return strSo_bl;
            }
            set
            {
                strSo_bl = value;
            }
        }

        public int Songay_bl
        {
            get
            {
                return strSongay_bl;
            }
            set
            {
                strSongay_bl = value;
            }
        }

        public string Kieu_bl
        {
            get
            {
                return strKieu_bl;
            }
            set
            {
                strKieu_bl = value;
            }
        }

        public string Ma_hq_ph
        {
            get
            {
                return strMa_hq_ph;
            }
            set
            {
                strMa_hq_ph = value;
            }
        }

        public string Dien_giai
        {
            get
            {
                return strDien_giai;
            }
            set
            {
                strDien_giai = value;
            }
        }

        public string Trangthai_lapct
        {
            get
            {
                return strTrangthai_lapct;
            }
            set
            {
                strTrangthai_lapct = value;
            }
        }

        public string Ngay_batdau
        {
            get
            {
                return strNgay_batdau;
            }
            set
            {
                strNgay_batdau = value;
            }
        }

        public string Ngay_ketthuc
        {
            get
            {
                return strNgay_ketthuc;
            }
            set
            {
                strNgay_ketthuc = value;
            }
        }

        public string Error_code_hq
        {
            get
            {
                return strError_code_hq;
            }
            set
            {
                strError_code_hq = value;
            }
        }

        public string So_ct_ctu
        {
            get
            {
                return strSo_ct_ctu;
            }
            set
            {
                strSo_ct_ctu = value;
            }
        }

        public string Dchieu_hq
        {
            get
            {
                return strDchieu_hq;
            }
            set
            {
                strDchieu_hq = value;
            }
        }

        public string Is_ma_hq
        {
            get
            {
                return strIs_ma_hq;
            }
            set
            {
                strIs_ma_hq = value;
            }
        }

        public string Ngay_dc_hq
        {
            get
            {
                return strNgay_dc_hq;
            }
            set
            {
                strNgay_dc_hq = value;
            }
        }

        public string Kb_thu_ho
        {
            get
            {
                return strKb_thu_ho;
            }
            set
            {
                strKb_thu_ho = value;
            }
        }

        public string Ma_loaitien
        {
            get
            {
                return strMa_loaitien;
            }
            set
            {
                strMa_loaitien = value;
            }
        }

        public string Ma_hq_cqt
        {
            get
            {
                return strMa_hq_cqt;
            }
            set
            {
                strMa_hq_cqt = value;
            }
        }

        public int So_bt_hq
        {
            get
            {
                return strSo_bt_hq;
            }
            set
            {
                strSo_bt_hq = value;
            }
        }

        // --------------------------

        public string QUAN_HUYEN_NNTIEN
        {
            get
            {
                return strQuan_Huyen_NNTien;
            }
            set
            {
                strQuan_Huyen_NNTien = value;
            }
        }
        public string TKHT_NH
        {
            get
            {
                return strTKHTNH;
            }
            set
            {
                strTKHTNH = value;
            }
        }
        public string TEN_TK_NO
        {
            get
            {
                return strTen_TKNo;
            }
            set
            {
                strTen_TKNo = value;
            }
        }
        public string TEN_TK_CO
        {
            get
            {
                return strTen_TKCo;
            }
            set
            {
                strTen_TKCo = value;
            }
        }
        public string TINH_TPHO_NNTIEN
        {
            get
            {
                return strTinh_TPho_NNTien;
            }
            set
            {
                strTinh_TPho_NNTien = value;
            }
        }

        public string So_RM
        {
            get
            {
                return strSoRM;
            }
            set
            {
                strSoRM = value;
            }
        }
        public string MATELLER_GDV
        {
            get
            {
                return strtellerGDV;
            }
            set
            {
                strtellerGDV = value;
            }
        }
        public string MATELLER_KSV
        {
            get
            {
                return strtellerKSV;
            }
            set
            {
                strtellerKSV = value;
            }
        }
        public string RM_REF_NO
        {
            get
            {
                return strRM_REF_NO;
            }
            set
            {
                strRM_REF_NO = value;
            }
        }

        public string REF_NO
        {
            get
            {
                return strREF_NO;
            }
            set
            {
                strREF_NO = value;
            }
        }

        public string TK_KB_NH
        {
            get
            {
                return strTK_KB_NH;
            }
            set
            {
                strTK_KB_NH = value;
            }
        }
        public string Mode
        {
            get
            {
                return intMode;
            }
            set
            {
                intMode = value;
            }
        }
        public string PT_TT
        {
            get
            {
                return strPT_TT;
            }
            set
            {
                strPT_TT = value;
            }
        }
        public string TEN_KH_NH
        {
            get
            {
                return strTEN_KH_NH;
            }
            set
            {
                strTEN_KH_NH = value;
            }
        }
        public string TK_GL_NH
        {
            get
            {
                return strTK_GL_NH;
            }
            set
            {
                strTK_GL_NH = value;
            }
        }

        public string SHKB
        {
            get
            {
                return strSHKB;
            }
            set
            {
                strSHKB = value;
            }
        }
        public int Ngay_KB
        {
            get
            {
                return iNgay_KB;
            }
            set
            {
                iNgay_KB = value;
            }
        }
        public int Ma_NV
        {
            get
            {
                return iMa_NV;
            }
            set
            {
                iMa_NV = value;
            }
        }
        public int So_BT
        {
            get
            {
                return iSo_BT;
            }
            set
            {
                iSo_BT = value;
            }
        }
        public string Ma_DThu
        {
            get
            {
                return strMa_DThu;
            }
            set
            {
                strMa_DThu = value;
            }
        }
        public int So_BThu
        {
            get
            {
                return iSo_BThu;
            }
            set
            {
                iSo_BThu = value;
            }
        }
        public string KyHieu_CT
        {
            get
            {
                return strKyHieu_CT;
            }
            set
            {
                strKyHieu_CT = value;
            }
        }
        public string So_CT
        {
            get
            {
                return strSo_CT;
            }
            set
            {
                strSo_CT = value;
            }
        }
        public string So_CT_TuChinh
        {
            get
            {
                return strSo_CT_TuChinh;
            }
            set
            {
                strSo_CT_TuChinh = value;
            }
        }
        public string So_CT_NH
        {
            get
            {
                return strSo_CT_NH;
            }
            set
            {
                strSo_CT_NH = value;
            }
        }
        public string Ma_NNTien
        {
            get
            {
                return strMa_NNTien;
            }
            set
            {
                strMa_NNTien = value;
            }
        }
        public string Ten_NNTien
        {
            get
            {
                return strTen_NNTien;
            }
            set
            {
                strTen_NNTien = value;
            }
        }
        public string DC_NNTien
        {
            get
            {
                return strDC_NNTien;
            }
            set
            {
                strDC_NNTien = value;
            }
        }
        public string Ma_NNThue
        {
            get
            {
                return strMa_NNThue;
            }
            set
            {
                strMa_NNThue = value;
            }
        }
        public string Ten_NNThue
        {
            get
            {
                return strTen_NNThue;
            }
            set
            {
                strTen_NNThue = value;
            }
        }
        public string DC_NNThue
        {
            get
            {
                return strDC_NNThue;
            }
            set
            {
                strDC_NNThue = value;
            }
        }
        public string Ly_Do
        {
            get
            {
                return strLy_Do;
            }
            set
            {
                strLy_Do = value;
            }
        }
        public string Ma_KS
        {
            get
            {
                return iMa_KS;
            }
            set
            {
                iMa_KS = value;
            }
        }
        public string Ma_TQ
        {
            get
            {
                return iMa_TQ;
            }
            set
            {
                iMa_TQ = value;
            }
        }
        public string So_QD
        {
            get
            {
                return strSo_QD;
            }
            set
            {
                strSo_QD = value;
            }
        }
        public string Ngay_QD
        {
            get
            {
                return strNgay_QD;
            }
            set
            {
                strNgay_QD = value;
            }
        }
        public string CQ_QD
        {
            get
            {
                return strCQ_QD;
            }
            set
            {
                strCQ_QD = value;
            }
        }
        public int Ngay_CT
        {
            get
            {
                return iNgay_CT;
            }
            set
            {
                iNgay_CT = value;
            }
        }
        public string Ngay_HT
        {
            get
            {
                return strNgay_HT;
            }
            set
            {
                strNgay_HT = value;
            }
        }
        public string Ma_CQThu
        {
            get
            {
                return strMa_CQThu;
            }
            set
            {
                strMa_CQThu = value;
            }
        }
        public string XA_ID
        {
            get
            {
                return strXA_ID;
            }
            set
            {
                strXA_ID = value;
            }
        }
        public string Ma_Tinh
        {
            get
            {
                return strMa_Tinh;
            }
            set
            {
                strMa_Tinh = value;
            }
        }
        public string Ten_tinh
        {
            get
            {
                return _Ten_tinh;
            }
            set
            {
                _Ten_tinh = value;
            }
        }
        public string Ma_Huyen
        {
            get
            {
                return strMa_Huyen;
            }
            set
            {
                strMa_Huyen = value;
            }
        }
        public string Ma_Xa
        {
            get
            {
                return strMa_Xa;
            }
            set
            {
                strMa_Xa = value;
            }
        }

        public string Ten_Xa
        {
            get
            {
                return _Ten_Xa;
            }
            set
            {
                _Ten_Xa = value;
            }
        }

        public string TK_No
        {
            get
            {
                return strTK_No;
            }
            set
            {
                strTK_No = value;
            }
        }
        public string TK_Co
        {
            get
            {
                return strTK_Co;
            }
            set
            {
                strTK_Co = value;
            }
        }
        public string So_TK
        {
            get
            {
                return strSo_TK;
            }
            set
            {
                strSo_TK = value;
            }
        }
        public string Ngay_TK
        {
            get
            {
                return strNgay_TK;
            }
            set
            {
                strNgay_TK = value;
            }
        }
        public string LH_XNK
        {
            get
            {
                return strLH_XNK;
            }
            set
            {
                strLH_XNK = value;
            }
        }
        public string DVSDNS
        {
            get
            {
                return strDVSDNS;
            }
            set
            {
                strDVSDNS = value;
            }
        }
        public string Ten_DVSDNS
        {
            get
            {
                return strTen_DVSDNS;
            }
            set
            {
                strTen_DVSDNS = value;
            }
        }
        public string Ma_LThue
        {
            get
            {
                return strMa_LThue;
            }
            set
            {
                strMa_LThue = value;
            }
        }
        public string TG_ID
        {
            get
            {
                return iTG_ID;
            }
            set
            {
                iTG_ID = value;
            }
        }
        public string Ma_NT
        {
            get
            {
                return strMa_NT;
            }
            set
            {
                strMa_NT = value;
            }
        }
        public double Ty_Gia
        {
            get
            {
                return dblTy_Gia;
            }
            set
            {
                dblTy_Gia = value;
            }
        }
        public string So_Khung
        {
            get
            {
                return strSo_Khung;
            }
            set
            {
                strSo_Khung = value;
            }
        }
        public string So_May
        {
            get
            {
                return strSo_May;
            }
            set
            {
                strSo_May = value;
            }
        }

        public string TK_KH_NH
        {
            get
            {
                return strTK_KH_NH;
            }
            set
            {
                strTK_KH_NH = value;
            }
        }
        public string NGAY_KH_NH
        {
            get
            {
                return strNGAY_KH_NH;
            }
            set
            {
                strNGAY_KH_NH = value;
            }
        }
        public string MA_NH_A
        {
            get
            {
                return strMA_NH_A;
            }
            set
            {
                strMA_NH_A = value;
            }
        }
        public string MA_NH_B
        {
            get
            {
                return strMA_NH_B;
            }
            set
            {
                strMA_NH_B = value;
            }
        }

        public double TTien
        {
            get
            {
                return dblTTien;
            }
            set
            {
                dblTTien = value;
            }
        }
        public double TT_TThu
        {
            get
            {
                return dblTT_TThu;
            }
            set
            {
                dblTT_TThu = value;
            }
        }
        public string Lan_In
        {
            get
            {
                return strLan_In;
            }
            set
            {
                strLan_In = value;
            }
        }
        public string Trang_Thai
        {
            get
            {
                return strTrang_Thai;
            }
            set
            {
                strTrang_Thai = value;
            }
        }

        public string TK_KH_Nhan
        {
            get
            {
                return strTK_KH_Nhan;
            }
            set
            {
                strTK_KH_Nhan = value;
            }
        }
        public string Ten_KH_Nhan
        {
            get
            {
                return strTen_KH_Nhan;
            }
            set
            {
                strTen_KH_Nhan = value;
            }
        }
        public string Diachi_KH_Nhan
        {
            get
            {
                return strDiachi_KH_Nhan;
            }
            set
            {
                strDiachi_KH_Nhan = value;
            }
        }
        public double TTien_NT
        {
            get
            {
                return dblTTien_NT;
            }
            set
            {
                dblTTien_NT = value;
            }
        }

        public int So_BT_KTKB
        {
            get
            {
                return intSo_BT_KTKT;
            }
            set
            {
                intSo_BT_KTKT = value;
            }
        }

        public string So_BK
        {
            get
            {
                return strSo_BK;
            }
            set
            {
                strSo_BK = value;
            }
        }

        public string Ngay_BK
        {
            get
            {
                return strNgay_BK;
            }
            set
            {
                strNgay_BK = value;
            }
        }

        public string TT_BDS
        {
            get
            {
                return strTT_BDS;
            }
            set
            {
                strTT_BDS = value;
            }
        }


        public string TT_NSTT
        {
            get
            {
                return strTT_NSTT;
            }
            set
            {
                strTT_NSTT = value;
            }
        }



        public string PHI_GD
        {
            get
            {
                return strPHI_GD;
            }
            set
            {
                strPHI_GD = value;
            }
        }


        public string PHI_VAT
        {
            get
            {
                return strPHI_VAT;
            }
            set
            {
                strPHI_VAT = value;
            }
        }
        public string PHI_GD2
        {
            get
            {
                return strPHI_GD2;
            }
            set
            {
                strPHI_GD2 = value;
            }
        }


        public string PHI_VAT2
        {
            get
            {
                return strPHI_VAT2;
            }
            set
            {
                strPHI_VAT2 = value;
            }
        }

        public string PT_TINHPHI
        {
            get
            {
                return strPT_TINHPHI;
            }
            set
            {
                strPT_TINHPHI = value;
            }
        }

        public string MA_NH_TT
        {
            get
            {
                return strMa_nh_tt;
            }
            set
            {
                strMa_nh_tt = value;
            }
        }

        public string TEN_NH_TT
        {
            get
            {
                return strTen_nh_tt;
            }
            set
            {
                strTen_nh_tt = value;
            }
        }

        public string CIF
        {
            get
            {
                return strCif;
            }
            set
            {
                strCif = value;
            }
        }
        public string SAN_PHAM
        {
            get
            {
                return strMA_SANPHAM;
            }
            set
            {
                strMA_SANPHAM = value;
            }
        }
        public string TT_CITAD
        {
            get
            {
                return strTT_CITAD;
            }
            set
            {
                strTT_CITAD = value;
            }
        }
        public string SEQ_NO
        {
            get
            {
                return strSEQ_NO;
            }
            set
            {
                strSEQ_NO = value;
            }
        }
        public string LOAI_BL
        {
            get
            {
                return strLoaiBL;
            }
            set
            {
                strLoaiBL = value;
            }
        }
        public string MA_CAP_CHUONG
        {
            get
            {
                return strMaCapChuong;
            }
            set
            {
                strMaCapChuong = value;
            }
        }
        public string TIME_BEGIN
        {
            get
            {
                return strTIME_BEGIN;
            }
            set
            {
                strTIME_BEGIN = value;
            }
        }
        public string SO_CMND
        {
            get
            {
                return strSO_CMND;
            }
            set
            {
                strSO_CMND = value;
            }
        }
        public string SO_FONE
        {
            get
            {
                return strSO_FONE;
            }
            set
            {
                strSO_FONE = value;
            }
        }
        public string MA_DV_DD
        {
            get
            {
                return strMa_DV_DD;
            }
            set
            {
                strMa_DV_DD = value;
            }
        }
        public string TEN_DV_DD
        {
            get
            {
                return strTen_DV_DD;
            }
            set
            {
                strTen_DV_DD = value;
            }
        }
        public string MA_NTK
        {
            get
            {
                return strMA_NTK;
            }
            set
            {
                strMA_NTK = value;
            }
        }
        public string Ma_CN
        {
            get
            {
                return strMA_CN;
            }
            set
            {
                strMA_CN = value;
            }
        }
        public string VTD2
        {
            get
            {
                return strVTD2;
            }
            set
            {
                strVTD2 = value;
            }
        }
        public string Ngay_VTD2
        {
            get
            {
                return strNgay_VTD2;
            }
            set
            {
                strNgay_VTD2 = value;
            }
        }
        public string VTD3
        {
            get
            {
                return strVTD3;
            }
            set
            {
                strVTD3 = value;
            }
        }
        public string Ngay_VTD3
        {
            get
            {
                return strNgay_VTD3;
            }
            set
            {
                strNgay_VTD3 = value;
            }
        }
        public string VTD4
        {
            get
            {
                return strVTD4;
            }
            set
            {
                strVTD4 = value;
            }
        }
        public string Ngay_VTD4
        {
            get
            {
                return strNgay_VTD4;
            }
            set
            {
                strNgay_VTD4 = value;
            }
        }
        public string VTD5
        {
            get
            {
                return strVTD5;
            }
            set
            {
                strVTD5 = value;
            }
        }
        public string Ngay_VTD5
        {
            get
            {
                return strNgay_VTD5;
            }
            set
            {
                strNgay_VTD5 = value;
            }
        }
        public string MA_HQ_KB
        {
            get
            {
                return strMa_hq_kb;
            }
            set
            {
                strMa_hq_kb = value;
            }
        }
        public string NH_HUONG
        {
            get
            {
                return strNH_HUONG;
            }
            set
            {
                strNH_HUONG = value;
            }
        }
        public string DIENGIAI_HQ
        {
            get
            {
                return strDIENGIAI_HQ;
            }
            set
            {
                strDIENGIAI_HQ = value;
            }
        }

        public string LOAI_CTU
        {
            get
            {
                return _LOAI_CTU;
            }
            set
            {
                _LOAI_CTU = value;
            }
        }

        public string Ma_Huyen_NNThue
        {
            get
            {
                return _Ma_Huyen_NNThue;
            }
            set
            {
                _Ma_Huyen_NNThue = value;
            }
        }

        public string Ma_Tinh_NNThue
        {
            get
            {
                return _Ma_Tinh_NNThue;
            }
            set
            {
                _Ma_Tinh_NNThue = value;
            }
        }

        public string MA_QUAN_HUYENNNTIEN
        {
            get
            {
                return _MA_QUAN_HUYENNNTIEN;
            }
            set
            {
                _MA_QUAN_HUYENNNTIEN = value;
            }
        }

        public string MA_TINH_TPNNTIEN
        {
            get
            {
                return _MA_TINH_TPNNTIEN;
            }
            set
            {
                _MA_TINH_TPNNTIEN = value;
            }
        }

        public DateTime NGAY_BAOCO
        {
            get
            {
                return _NGAY_BAOCO;
            }
            set
            {
                _NGAY_BAOCO = value;
            }
        }

        public DateTime NGAY_BAONO
        {
            get
            {
                return _NGAY_BAONO;
            }
            set
            {
                _NGAY_BAONO = value;
            }
        }

        public int TT_CTHUE
        {
            get
            {
                return _TT_CTHUE;
            }
            set
            {
                _TT_CTHUE = value;
            }
        }
        public string MA_CUC
        {
            get
            {
                return _MA_CUC;
            }
            set
            {
                _MA_CUC = value;
            }
        }
        public string TEN_CUC
        {
            get
            {
                return _TEN_CUC;
            }
            set
            {
                _TEN_CUC = value;
            }
        }


    }
}
