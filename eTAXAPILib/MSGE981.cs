﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSGE981
{
    [Serializable]
    [XmlRootAttribute("ETAX", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSGE981
    {
        public MSGE981()
        { }
        [XmlElement("Header")]
        public HEADER Header;
        [XmlElement("Data")]
        public Data981 Data;

        public string MSGE981toXML(MSGE981 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSGE981));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSGE981 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(eTAXAPILib.MSGE981.MSGE981));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            eTAXAPILib.MSGE981.MSGE981 LoadedObjTmp = (eTAXAPILib.MSGE981.MSGE981)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class HEADER
    {
        public HEADER()
        {
            Message_Type = "E981";
            Transaction_Date = "";
            Transaction_ID = "";
            UserID = "";
            Password = "";
            Product_Type = "";
            Request_ID = "";
        }
        [XmlElement("Message_Type")]
        public string Message_Type { get; set; }
        [XmlElement("Transaction_Date")]
        public string Transaction_Date { get; set; }
        [XmlElement("Transaction_ID")]
        public string Transaction_ID { get; set; }
        [XmlElement("UserID")]
        public string UserID { get; set; }
        [XmlElement("Password")]
        public string Password { get; set; }
        [XmlElement("Product_Type")]
        public string Product_Type { get; set; }
        [XmlElement("Request_ID")]
        public string Request_ID { get; set; }

    }
    public class Data981
    {
        public Data981()
        {
            MA_NNTHUE = "";
            TEN_NNTHUE = "";
            DC_NNTHUE = "";
            MA_NNTIEN = "";
            TEN_NNTIEN = "";
            DC_NNTIEN = "";
            NGAY_CT = "";
            MA_KB = "";
            CQ_QD = "";
            TEN_CQ = "";
            SO_QD = "";
            NGAY_QD = "";
            MA_DV = "";
            TEN_DV = "";
            MA_LTHUE = "";
            MA_CQTHU = "";
            MA_DBHC = "";
            MA_NH_A = "";
            MA_NT = "";
            TY_GIA = "";
            TTIEN = "";
            TTIEN_NT = "";
            DIEN_GIAI = "";
            SO_CT = "";
            TK_KH_NH = "";
            PHUONGTHUC_TT = "";
            SO_THAM_CHIEU = "";
            MA_NH_B = "";
            MA_LHTHU = "";
            TEN_LHTHU = "";
            TK_NS = "";
            MA_CHUONG = "";
            MA_NDKT = "";
            LY_DO = "";
            TTIEN_VPHC = "";
            LY_DO_NOP_CHAM = "";
            TTIEN_NOP_CHAM = "";

        }
        [XmlElement("MA_NNTHUE")]
        public string MA_NNTHUE { get; set; }
        [XmlElement("TEN_NNTHUE")]
        public string TEN_NNTHUE { get; set; }
        [XmlElement("DC_NNTHUE")]
        public string DC_NNTHUE { get; set; }
        [XmlElement("MA_NNTIEN")]
        public string MA_NNTIEN { get; set; }
        [XmlElement("TEN_NNTIEN")]
        public string TEN_NNTIEN { get; set; }
        [XmlElement("DC_NNTIEN")]
        public string DC_NNTIEN { get; set; }
        [XmlElement("NGAY_CT")]
        public string NGAY_CT { get; set; }
        [XmlElement("MA_KB")]
        public string MA_KB { get; set; }
        [XmlElement("CQ_QD")]
        public string CQ_QD { get; set; }
        [XmlElement("TEN_CQ")]
        public string TEN_CQ { get; set; }
        [XmlElement("SO_QD")]
        public string SO_QD { get; set; }
        [XmlElement("NGAY_QD")]
        public string NGAY_QD { get; set; }
        [XmlElement("MA_DV")]
        public string MA_DV { get; set; }
        [XmlElement("TEN_DV")]
        public string TEN_DV { get; set; }
        [XmlElement("MA_LTHUE")]
        public string MA_LTHUE { get; set; }
        [XmlElement("MA_CQTHU")]
        public string MA_CQTHU { get; set; }
        [XmlElement("MA_DBHC")]
        public string MA_DBHC { get; set; }
        [XmlElement("MA_NH_A")]
        public string MA_NH_A { get; set; }
        [XmlElement("MA_NT")]
        public string MA_NT { get; set; }
        [XmlElement("TY_GIA")]
        public string TY_GIA { get; set; }
        [XmlElement("TTIEN")]
        public string TTIEN { get; set; }
        [XmlElement("TTIEN_NT")]
        public string TTIEN_NT { get; set; }
        [XmlElement("DIEN_GIAI")]
        public string DIEN_GIAI { get; set; }
        [XmlElement("SO_CT")]
        public string SO_CT { get; set; }
        [XmlElement("TK_KH_NH")]
        public string TK_KH_NH { get; set; }
        [XmlElement("PHUONGTHUC_TT")]
        public string PHUONGTHUC_TT { get; set; }
        [XmlElement("SO_THAM_CHIEU")]
        public string SO_THAM_CHIEU { get; set; }
        [XmlElement("MA_NH_B")]
        public string MA_NH_B { get; set; }
        [XmlElement("MA_LHTHU")]
        public string MA_LHTHU { get; set; }
        [XmlElement("TEN_LHTHU")]
        public string TEN_LHTHU { get; set; }
        [XmlElement("TK_NS")]
        public string TK_NS { get; set; }
        [XmlElement("MA_CHUONG")]
        public string MA_CHUONG { get; set; }
        [XmlElement("MA_NDKT")]
        public string MA_NDKT { get; set; }
        [XmlElement("LY_DO")]
        public string LY_DO { get; set; }
        [XmlElement("TTIEN_VPHC")]
        public string TTIEN_VPHC { get; set; }
        [XmlElement("LY_DO_NOP_CHAM")]
        public string LY_DO_NOP_CHAM { get; set; }
        [XmlElement("TTIEN_NOP_CHAM")]
        public string TTIEN_NOP_CHAM { get; set; }
        [XmlElement("SERI")]
        public string SERI { get; set; }
        [XmlElement("SO_BIENLAI")]
        public string SO_BIENLAI { get; set; }


    }

}
