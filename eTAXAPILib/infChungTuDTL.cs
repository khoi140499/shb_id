﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eTAXAPILib.NewChungTu
{
    public partial class infChungTuDTL
    {
        private string strDTL_ID;
        private string strSHKB;
        private int iNgay_KB;
        private int iMa_NV;
        private int iSo_BT;  // số bút toán
        private string strMa_DThu;
        private string iCCH_ID;
        private string strMa_Cap;
        private string strMa_Chuong;
        private string iLKH_ID;
        private string strMa_Loai;
        private string strMa_Khoan;
        private string iMTM_ID;
        private string strMa_Muc;
        private string strMa_TMuc;
        private string strNoi_Dung;
        private string iDT_ID;
        private string strMa_TLDT;
        private string strKy_Thue;
        private double dblSoTien;
        private double dblSoTien_NT;
        private string strSo_TK;
        private string dNgay_TK;
        private string strLH_XNK;
        private string strMa_HQ;
        private string strLoai_TT;
        private string strSacThue;

        private string strMaQuy;
        private string strMaDP;
        private string _PT_TT;

        // Thuoc tinh dung cho Bao lanh HQ
        private string strMa_nkt;
        private string strMa_nkt_cha;
        private string strMa_ndkt;
        private string strMa_ndkt_cha;
        private int strMa_tlpc;
        private string strMa_khtk;
        private string strMa_nt;
        private string strLoai_thue_hq;
        private string strSO_CT;
        private string strTT_BT;
        private string _MA_LT;

        public string TT_BTOAN
        {
            get
            {
                return strTT_BT;
            }
            set
            {
                strTT_BT = value;
            }
        }
        public string Ma_nkt
        {
            get
            {
                return strMa_nkt;
            }
            set
            {
                strMa_nkt = value;
            }
        }

        public string Ma_nkt_cha
        {
            get
            {
                return strMa_nkt_cha;
            }
            set
            {
                strMa_nkt_cha = value;
            }
        }

        public string Ma_ndkt
        {
            get
            {
                return strMa_ndkt;
            }
            set
            {
                strMa_ndkt = value;
            }
        }

        public string Ma_ndkt_cha
        {
            get
            {
                return strMa_ndkt_cha;
            }
            set
            {
                strMa_ndkt_cha = value;
            }
        }

        public int Ma_tlpc
        {
            get
            {
                return strMa_tlpc;
            }
            set
            {
                strMa_tlpc = value;
            }
        }

        public string Ma_khtk
        {
            get
            {
                return strMa_khtk;
            }
            set
            {
                strMa_khtk = value;
            }
        }

        public string Ma_nt
        {
            get
            {
                return strMa_nt;
            }
            set
            {
                strMa_nt = value;
            }
        }

        public string Loai_thue_hq
        {
            get
            {
                return strLoai_thue_hq;
            }
            set
            {
                strLoai_thue_hq = value;
            }
        }


        public infChungTuDTL()
        {
        }

        public string PT_TT
        {
            get
            {
                return _PT_TT;
            }
            set
            {
                _PT_TT = value;
            }
        }

        public string ID
        {
            get
            {
                return strDTL_ID;
            }
            set
            {
                strDTL_ID = value;
            }
        }
        public string SHKB
        {
            get
            {
                return strSHKB;
            }
            set
            {
                strSHKB = value;
            }
        }
        public int Ngay_KB
        {
            get
            {
                return iNgay_KB;
            }
            set
            {
                iNgay_KB = value;
            }
        }
        public int Ma_NV
        {
            get
            {
                return iMa_NV;
            }
            set
            {
                iMa_NV = value;
            }
        }
        public int So_BT
        {
            get
            {
                return iSo_BT;
            }
            set
            {
                iSo_BT = value;
            }
        }
        public string Ma_DThu
        {
            get
            {
                return strMa_DThu;
            }
            set
            {
                strMa_DThu = value;
            }
        }
        public string CCH_ID
        {
            get
            {
                return iCCH_ID;
            }
            set
            {
                iCCH_ID = value;
            }
        }
        public string Ma_Cap
        {
            get
            {
                return strMa_Cap;
            }
            set
            {
                strMa_Cap = value;
            }
        }
        public string Ma_Chuong
        {
            get
            {
                return strMa_Chuong;
            }
            set
            {
                strMa_Chuong = value;
            }
        }
        public string LKH_ID
        {
            get
            {
                return iLKH_ID;
            }
            set
            {
                iLKH_ID = value;
            }
        }
        public string Ma_Loai
        {
            get
            {
                return strMa_Loai;
            }
            set
            {
                strMa_Loai = value;
            }
        }
        public string Ma_Khoan
        {
            get
            {
                return strMa_Khoan;
            }
            set
            {
                strMa_Khoan = value;
            }
        }
        public string MTM_ID
        {
            get
            {
                return iMTM_ID;
            }
            set
            {
                iMTM_ID = value;
            }
        }

        public string Ma_Muc
        {
            get
            {
                return strMa_Muc;
            }
            set
            {
                strMa_Muc = value;
            }
        }
        public string Ma_TMuc
        {
            get
            {
                return strMa_TMuc;
            }
            set
            {
                strMa_TMuc = value;
            }
        }
        public string Noi_Dung
        {
            get
            {
                return strNoi_Dung;
            }
            set
            {
                strNoi_Dung = value;
            }
        }
        public string DT_ID
        {
            get
            {
                return iDT_ID;
            }
            set
            {
                iDT_ID = value;
            }
        }
        public string Ma_TLDT
        {
            get
            {
                return strMa_TLDT;
            }
            set
            {
                strMa_TLDT = value;
            }
        }

        public string Ky_Thue
        {
            get
            {
                return strKy_Thue;
            }
            set
            {
                strKy_Thue = value;
            }
        }
        public double SoTien
        {
            get
            {
                return dblSoTien;
            }
            set
            {
                dblSoTien = value;
            }
        }
        public double SoTien_NT
        {
            get
            {
                return dblSoTien_NT;
            }
            set
            {
                dblSoTien_NT = value;
            }
        }
        public string MaQuy
        {
            get
            {
                return strMaQuy;
            }
            set
            {
                strMaQuy = value;
            }
        }

        public string Ma_DP
        {
            get
            {
                return strMaDP;
            }
            set
            {
                strMaDP = value;
            }
        }
        public string SO_CT
        {
            get
            {
                return strSO_CT;
            }
            set
            {
                strSO_CT = value;
            }
        }
        public string SO_TK
        {
            get
            {
                return strSo_TK;
            }
            set
            {
                strSo_TK = value;
            }
        }
        public string NGAY_TK
        {
            get
            {
                return dNgay_TK;
            }
            set
            {
                dNgay_TK = value;
            }
        }
        public string LH_XNK
        {
            get
            {
                return strLH_XNK;
            }
            set
            {
                strLH_XNK = value;
            }
        }
        public string MA_HQ
        {
            get
            {
                return strMa_HQ;
            }
            set
            {
                strMa_HQ = value;
            }
        }
        public string LOAI_TT
        {
            get
            {
                return strLoai_TT;
            }
            set
            {
                strLoai_TT = value;
            }
        }
        public string SAC_THUE
        {
            get
            {
                return strSacThue;
            }
            set
            {
                strSacThue = value;
            }
        }
        public string MA_LT
        {
            get
            {
                return _MA_LT;
            }
            set
            {
                _MA_LT = value;
            }
        }
    }

}
