﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
namespace eTAXAPILib.MSG312
{
    [Serializable]
    [XmlRootAttribute(Namespace = "http://www.cpandl.com", ElementName = "ETAX", IsNullable = false)]
    public class MSG312
    {
        [XmlElement("Header")]
        public HeaderEtaxIN Header;
        [XmlElement("Data")]
        public Data312 Data;
        public MSG312()
        { }
        public MSG312 MSGToObject(string p_XML)
        {
            // XmlSerializer serializer = new XmlSerializer(typeof(MSG312), new XmlRootAttribute("ETAX"));
            XmlSerializer serializer = new XmlSerializer(typeof(eTAXAPILib.MSG312.MSG312));
            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG312 LoadedObjTmp = (MSG312)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data312
    {

        [XmlElement("So_HS")]
        public string So_HS { get; set; }
        [XmlElement("Loai_HS")]
        public string Loai_HS { get; set; }
        [XmlElement("Ma_DV")]
        public string Ma_DV { get; set; }
        [XmlElement("Ten_DV")]
        public string Ten_DV { get; set; }
        [XmlElement("DiaChi")]
        public string DiaChi { get; set; }
        [XmlElement("Ngay_HL")]
        public string Ngay_HL { get; set; }
        [XmlElement("ThongTin_NNT")]
        public ThongTin_NNT ThongTin_NNT;
        [XmlElement("ThongTinTaiKhoan")]
        public ThongTinTaiKhoan ThongTinTaiKhoan;

        public Data312()
        {

        }
    }
    public class ThongTin_NNT
    {
        [XmlElement("So_CMT")]
        public string So_CMT { get; set; }
        [XmlElement("Ho_Ten")]
        public string Ho_Ten { get; set; }
        [XmlElement("NgaySinh")]
        public string NgaySinh { get; set; }
        [XmlElement("NguyenQuan")]
        public string NguyenQuan { get; set; }
        [XmlElement("ThongTinLienHe")]
        public List<ThongTinLienHe> ThongTinLienHe;
        public ThongTin_NNT()
        { }
    }
    public class ThongTinLienHe
    {

        [XmlElement("So_DT")]
        public string So_DT { get; set; }
        [XmlElement("Email")]
        public string Email { get; set; }
        public ThongTinLienHe()
        { }
    }
    public class ThongTinTaiKhoan
    {

        [XmlElement("TaiKhoan_TH")]
        public string TaiKhoan_TH { get; set; }
        [XmlElement("Ten_TaiKhoan_TH")]
        public string Ten_TaiKhoan_TH { get; set; }

        [XmlElement("CIFNO")]
        public string CIFNO { get; set; }
        [XmlElement("BRANCH_CODE")]
        public string BRANCH_CODE { get; set; }
        public ThongTinTaiKhoan()
        { }
    }
}
