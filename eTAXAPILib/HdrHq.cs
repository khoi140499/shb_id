﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eTAXAPILib.NewChungTu
{
    public partial class HdrHq
    {
        private string _SHKB;         // Số hiệu kho bạc
        private string _NGAY_KB;      // Ngày kho bạc
        private string _MA_TINH;      // Mã tỉnh của kho bạc
        private string _MA_HUYEN;     // Mã huyện của kho bạc
        private string _MA_XA;        // Mã xã của kho bạc
        private string _TK_CO;        // Tài khoản có của kho bạc

        private string _MA_NV;        // x  'Mã nhân viên lập
        private string _MA_KS;        // x  'Mã nhân viên kiểm soát
        private string _MA_CN;        // x  'Mã chi nhánh nhân viên lập
        private string _NGAY_KS;      // x  Ngày kiểm soát

        private string _SO_BT;        // Số bút toán
        private string _MA_DTHU;      // Mã điểm thu => 0
        private string _KYHIEU_CT;    // Ký hiệu chứng từ
        private string _SO_CT;        // Số chứng từ
        private string _MA_LTHUE;     // Mã loại thuế (Thuế nội địa, Thuế hq,....)

        private string _MA_NNTIEN;    // Mã số thuế người nộp tiền
        private string _TEN_NNTIEN;   // Tên người nộp tiền
        private string _DC_NNTIEN;    // Địa chỉ người nộp tiền
        private string _MA_QUAN_HUYENNNTIEN;  // Mã quận/huyện người nộp tiền 018HH
        private string _MA_TINH_TPNNTIEN;     // Mã tỉnh/tp người nộp tiền    01TTT

        private string _MA_NNTHUE;    // Mã số thuế người nộp thuế
        private string _TEN_NNTHUE;   // Tên người nộp thuế
        private string _DC_NNTHUE;    // Địa chỉ người nộp thuế
        private string _MA_HUYEN_NNTHUE;  // Mã huyện người nộp thuế
        private string _MA_TINH_NNTHUE;   // Mã tỉnh người nộp thuế

        private string _NGAY_CT;     // Ngày chứng từ
        private string _NGAY_HT;      // Ngày hệ thống
        private string _MA_CQTHU;     // Mã cơ quan thu

        private string _SO_TK;        // Số tờ khai(nhiều tờ khai ngăn nhau = dấu ',')
        private string _NGAY_TK;      // Ngày tờ khai
        private string _LH_XNK;       // Loại hình xuất nhập khẩu

        private string _PT_TT;        // x  'Phương thức thanh toán tiền: Tiền mặt, chuyển khoản, GL,...
        private string _MA_NT;        // Mã nguyên tệ (VND, USD)
        private string _TY_GIA;       // Tỷ giá so với VND
        private string _TTIEN;        // Tổng tiền VND
        private string _TTIEN_NT;     // Tông tiền ngoại tệ (VND,USD,...)
        private string _TT_TTHU;      // Tổng tiền thực thu

        private string _TK_KH_NH;     // x  'Số tài khoản của khách hàng
        private string _TEN_KH_NH;    // x  'Tên khách hàng trong corebank
        private string _NGAY_KH_NH;   // x  'Ngày cắt tiền khách hàng

        private string _MA_NH_A;      // x  'Mã ngân hàng chuyển(MSB, DongA, ABB, EXIM,...)
        private string _MA_NH_B;      // x  'Mã ngân hàng thụ hưởng(AGIRBANK, BIDV, VCB, VIETIN,...)
        private string _TEN_NH_B;     // x  'Tên ngân hàng thụ hưởng
        private string _MA_NH_TT;     // x  'Mã ngân hàng trực tiếp
        private string _TEN_NH_TT;    // x  'Tên ngân hàng trực tiếp

        private string _LAN_IN;       // x  'Số lần in chứng từ

        private string _TRANG_THAI;   // Trạng thái chứng từ

        private string _PT_TINHPHI;   // x  'Phương pháp tình phí
        private string _PHI_GD;       // x  'Phí giao dịch
        private string _PHI_VAT;      // x  'VAT của phí giao dịch
        private string _PHI_GD2;      // x  'Phí giao dịch(kiểm đếm,...)
        private string _PHI_VAT2;     // x  'VAT của phí kiểm đếm

        private string _TT_CTHUE;     // x  'Trạng thái gửi thuế

        private string _TKHT;         // Tài khoản hạch toán

        private string _LOAI_TT;      // Loại tiền thuế: 1,2,....
        private string _MA_NTK;       // Mã nhóm tài khoản
        private string _MA_HQ;        // Mã hải quan
        private string _TEN_HQ;       // Tên hải quan
        private string _MA_HQ_PH;     // Mã hải quan phát hành
        private string _TEN_HQ_PH;    // Tên hải quan phát hành

        private string _KENH_CT;      // x  'Kênh chứng từ - etax - ebank
        private string _MA_SANPHAM;   // x  'Mã sản phẩm

        private string _TT_CITAD;     // x  'Trạng thái citad - trực tiếp - gián tiếp
        private string _SO_CMND;      // x  'Số chứng minh thư => Nộp tiền mặt
        private string _SO_FONE;      // x  'Số điện thoại => Nộp tiền mặt

        private string _LOAI_CTU;     // Loại chứng từ (Thuế hải quan - Phí hải quan)

        private string _REMARKS;      // x  'Nội dung citad

        private string _DIENGIAI_HQ;      // x  'Diễn giải hải quan

        private string _MA_CUC;      // x  'Mã cục HQ
        private string _TEN_CUC;      // x  'Tên cục HQ

        // MSB

        private string _TK_GL_NH;

        private string _REF_NO;

        private string _RM_REF_NO;
        public string SO_CT_NH { get; set; }
        public string TEN_KB { get; set; }
        public string TEN_CQTHU { get; set; }
        public string NGAY_BN { get; set; }
        public string NGAY_BC { get; set; }
        public string TK_GL_NH
        {
            get
            {
                return _TK_GL_NH;
            }
            set
            {
                _TK_GL_NH = value;
            }
        }

        public string RM_REF_NO
        {
            get
            {
                return _RM_REF_NO;
            }
            set
            {
                _RM_REF_NO = value;
            }
        }

        public string REF_NO
        {
            get
            {
                return _REF_NO;
            }
            set
            {
                _REF_NO = value;
            }
        }


        public string REMARKS
        {
            get
            {
                return _REMARKS;
            }
            set
            {
                _REMARKS = value;
            }
        }
        public string DIENGIAI_HQ
        {
            get
            {
                return _DIENGIAI_HQ;
            }
            set
            {
                _DIENGIAI_HQ = value;
            }
        }
        public string LOAI_CTU
        {
            get
            {
                return _LOAI_CTU;
            }
            set
            {
                _LOAI_CTU = value;
            }
        }

        public string SO_FONE
        {
            get
            {
                return _SO_FONE;
            }
            set
            {
                _SO_FONE = value;
            }
        }

        public string SO_CMND
        {
            get
            {
                return _SO_CMND;
            }
            set
            {
                _SO_CMND = value;
            }
        }

        public string TT_CITAD
        {
            get
            {
                return _TT_CITAD;
            }
            set
            {
                _TT_CITAD = value;
            }
        }

        public string MA_SANPHAM
        {
            get
            {
                return _MA_SANPHAM;
            }
            set
            {
                _MA_SANPHAM = value;
            }
        }

        public string KENH_CT
        {
            get
            {
                return _KENH_CT;
            }
            set
            {
                _KENH_CT = value;
            }
        }

        public string TEN_HQ_PH
        {
            get
            {
                return _TEN_HQ_PH;
            }
            set
            {
                _TEN_HQ_PH = value;
            }
        }

        public string MA_HQ_PH
        {
            get
            {
                return _MA_HQ_PH;
            }
            set
            {
                _MA_HQ_PH = value;
            }
        }

        public string TEN_HQ
        {
            get
            {
                return _TEN_HQ;
            }
            set
            {
                _TEN_HQ = value;
            }
        }

        public string MA_HQ
        {
            get
            {
                return _MA_HQ;
            }
            set
            {
                _MA_HQ = value;
            }
        }

        public string MA_NTK
        {
            get
            {
                return _MA_NTK;
            }
            set
            {
                _MA_NTK = value;
            }
        }

        public string LOAI_TT
        {
            get
            {
                return _LOAI_TT;
            }
            set
            {
                _LOAI_TT = value;
            }
        }


        public string TKHT
        {
            get
            {
                return _TKHT;
            }
            set
            {
                _TKHT = value;
            }
        }

        public string NGAY_KS
        {
            get
            {
                return _NGAY_KS;
            }
            set
            {
                _NGAY_KS = value;
            }
        }

        public string TT_CTHUE
        {
            get
            {
                return _TT_CTHUE;
            }
            set
            {
                _TT_CTHUE = value;
            }
        }

        public string PHI_VAT2
        {
            get
            {
                return _PHI_VAT2;
            }
            set
            {
                _PHI_VAT2 = value;
            }
        }

        public string PHI_GD2
        {
            get
            {
                return _PHI_GD2;
            }
            set
            {
                _PHI_GD2 = value;
            }
        }

        public string PHI_VAT
        {
            get
            {
                return _PHI_VAT;
            }
            set
            {
                _PHI_VAT = value;
            }
        }

        public string PHI_GD
        {
            get
            {
                return _PHI_GD;
            }
            set
            {
                _PHI_GD = value;
            }
        }

        public string PT_TINHPHI
        {
            get
            {
                return _PT_TINHPHI;
            }
            set
            {
                _PT_TINHPHI = value;
            }
        }

        public string TRANG_THAI
        {
            get
            {
                return _TRANG_THAI;
            }
            set
            {
                _TRANG_THAI = value;
            }
        }

        public string TEN_NH_TT
        {
            get
            {
                return _TEN_NH_TT;
            }
            set
            {
                _TEN_NH_TT = value;
            }
        }

        public string MA_NH_TT
        {
            get
            {
                return _MA_NH_TT;
            }
            set
            {
                _MA_NH_TT = value;
            }
        }

        public string TEN_NH_B
        {
            get
            {
                return _TEN_NH_B;
            }
            set
            {
                _TEN_NH_B = value;
            }
        }

        public string MA_NH_B
        {
            get
            {
                return _MA_NH_B;
            }
            set
            {
                _MA_NH_B = value;
            }
        }

        public string MA_NH_A
        {
            get
            {
                return _MA_NH_A;
            }
            set
            {
                _MA_NH_A = value;
            }
        }

        public string NGAY_KH_NH
        {
            get
            {
                return _NGAY_KH_NH;
            }
            set
            {
                _NGAY_KH_NH = value;
            }
        }

        public string TEN_KH_NH
        {
            get
            {
                return _TEN_KH_NH;
            }
            set
            {
                _TEN_KH_NH = value;
            }
        }

        public string TK_KH_NH
        {
            get
            {
                return _TK_KH_NH;
            }
            set
            {
                _TK_KH_NH = value;
            }
        }

        public string TT_TTHU
        {
            get
            {
                return _TT_TTHU;
            }
            set
            {
                _TT_TTHU = value;
            }
        }

        public string TTIEN_NT
        {
            get
            {
                return _TTIEN_NT;
            }
            set
            {
                _TTIEN_NT = value;
            }
        }

        public string TTIEN
        {
            get
            {
                return _TTIEN;
            }
            set
            {
                _TTIEN = value;
            }
        }

        public string TY_GIA
        {
            get
            {
                return _TY_GIA;
            }
            set
            {
                _TY_GIA = value;
            }
        }

        public string MA_NT
        {
            get
            {
                return _MA_NT;
            }
            set
            {
                _MA_NT = value;
            }
        }

        public string PT_TT
        {
            get
            {
                return _PT_TT;
            }
            set
            {
                _PT_TT = value;
            }
        }


        public string LH_XNK
        {
            get
            {
                return _LH_XNK;
            }
            set
            {
                _LH_XNK = value;
            }
        }

        public string NGAY_TK
        {
            get
            {
                return _NGAY_TK;
            }
            set
            {
                _NGAY_TK = value;
            }
        }

        public string SO_TK
        {
            get
            {
                return _SO_TK;
            }
            set
            {
                _SO_TK = value;
            }
        }

        public string MA_CQTHU
        {
            get
            {
                return _MA_CQTHU;
            }
            set
            {
                _MA_CQTHU = value;
            }
        }

        public string NGAY_CT
        {
            get
            {
                return _NGAY_CT;
            }
            set
            {
                _NGAY_CT = value;
            }
        }

        public string MA_TINH_NNTHUE
        {
            get
            {
                return _MA_TINH_NNTHUE;
            }
            set
            {
                _MA_TINH_NNTHUE = value;
            }
        }

        public string MA_HUYEN_NNTHUE
        {
            get
            {
                return _MA_HUYEN_NNTHUE;
            }
            set
            {
                _MA_HUYEN_NNTHUE = value;
            }
        }

        public string DC_NNTHUE
        {
            get
            {
                return _DC_NNTHUE;
            }
            set
            {
                _DC_NNTHUE = value;
            }
        }

        public string TEN_NNTHUE
        {
            get
            {
                return _TEN_NNTHUE;
            }
            set
            {
                _TEN_NNTHUE = value;
            }
        }

        public string MA_NNTHUE
        {
            get
            {
                return _MA_NNTHUE;
            }
            set
            {
                _MA_NNTHUE = value;
            }
        }

        public string MA_TINH_TPNNTIEN
        {
            get
            {
                return _MA_TINH_TPNNTIEN;
            }
            set
            {
                _MA_TINH_TPNNTIEN = value;
            }
        }

        public string MA_QUAN_HUYENNNTIEN
        {
            get
            {
                return _MA_QUAN_HUYENNNTIEN;
            }
            set
            {
                _MA_QUAN_HUYENNNTIEN = value;
            }
        }

        public string DC_NNTIEN
        {
            get
            {
                return _DC_NNTIEN;
            }
            set
            {
                _DC_NNTIEN = value;
            }
        }

        public string TEN_NNTIEN
        {
            get
            {
                return _TEN_NNTIEN;
            }
            set
            {
                _TEN_NNTIEN = value;
            }
        }

        public string MA_NNTIEN
        {
            get
            {
                return _MA_NNTIEN;
            }
            set
            {
                _MA_NNTIEN = value;
            }
        }

        public string MA_LTHUE
        {
            get
            {
                return _MA_LTHUE;
            }
            set
            {
                _MA_LTHUE = value;
            }
        }

        public string SO_CT
        {
            get
            {
                return _SO_CT;
            }
            set
            {
                _SO_CT = value;
            }
        }

        public string KYHIEU_CT
        {
            get
            {
                return _KYHIEU_CT;
            }
            set
            {
                _KYHIEU_CT = value;
            }
        }

        public string MA_DTHU
        {
            get
            {
                return _MA_DTHU;
            }
            set
            {
                _MA_DTHU = value;
            }
        }

        public string SO_BT
        {
            get
            {
                return _SO_BT;
            }
            set
            {
                _SO_BT = value;
            }
        }

        public string MA_CN
        {
            get
            {
                return _MA_CN;
            }
            set
            {
                _MA_CN = value;
            }
        }



        public string MA_KS
        {
            get
            {
                return _MA_KS;
            }
            set
            {
                _MA_KS = value;
            }
        }

        public string MA_NV
        {
            get
            {
                return _MA_NV;
            }
            set
            {
                _MA_NV = value;
            }
        }


        public string TK_CO
        {
            get
            {
                return _TK_CO;
            }
            set
            {
                _TK_CO = value;
            }
        }

        public string MA_XA
        {
            get
            {
                return _MA_XA;
            }
            set
            {
                _MA_XA = value;
            }
        }

        public string MA_HUYEN
        {
            get
            {
                return _MA_HUYEN;
            }
            set
            {
                _MA_HUYEN = value;
            }
        }

        public string MA_TINH
        {
            get
            {
                return _MA_TINH;
            }
            set
            {
                _MA_TINH = value;
            }
        }

        public string NGAY_KB
        {
            get
            {
                return _NGAY_KB;
            }
            set
            {
                _NGAY_KB = value;
            }
        }

        public string SHKB
        {
            get
            {
                return _SHKB;
            }
            set
            {
                _SHKB = value;
            }
        }

        public string MA_CUC
        {
            get
            {
                return _MA_CUC;
            }
            set
            {
                _MA_CUC = value;
            }
        }

        public string TEN_CUC
        {
            get
            {
                return _TEN_CUC;
            }
            set
            {
                _TEN_CUC = value;
            }
        }
    }

}
