﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using VBOracleLib;
using System.Diagnostics;
using System.IO;
using System.Configuration;
namespace eTAXAPILib
{
    public class ProcessMSG
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string strLogMessage = "N";
        public string XuLyMSG(string strXml)
        {

            string strREQID = "";
            string strID = DateTime.Now.ToString("yyyyMMdd") + SexyLib.GenerateKey("tcs_api_log_tf_corebank_seq", 10);
            string strREQMSG = strXml;
            string strREQTYPE = "";
            string strRESID = "";
            string strRESTYPE = "";
            string strRESMSG = "";
            string strERRCODE = "";
            string strERRMSG = "";

            string strReSult = "";
            string strPRODUCTTYPE = "";
            string strRequestID = "";
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                try
                {
                    strLogMessage = System.Configuration.ConfigurationSettings.AppSettings["API.IS_WRITE_LOG_MSG"].ToString();
                }
                catch (Exception exc)
                {
                    strLogMessage = "N";

                }
                if (strLogMessage.Equals("Y"))
                {
                    log.Info(strXml);
                }
                if (!SexyLib.ValidHeaderMSG(strXml, ref strERRCODE, ref strERRMSG, ref strREQID, ref strREQTYPE, ref strPRODUCTTYPE, ref strRequestID))
                {
                    SexyLib.insertLog(strID, strREQID, strREQTYPE, strREQMSG, strPRODUCTTYPE);
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = strPRODUCTTYPE;
                    obj299.Error.ErrorMessage = strERRMSG;
                    obj299.Error.ErrorNumber = strERRCODE;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    strRESID = obj299.Header.Transaction_ID;
                    strRESTYPE = "299";
                    strERRMSG = obj299.Error.ErrorMessage;

                    strReSult = obj299.MSG299toXML();
                    strReSult = SexyLib.FomatOutString(strReSult);
                    SexyLib.UPDATELog(strID, strRESID, strRESTYPE, strERRMSG, strERRCODE, strERRMSG);
                    return strReSult;
                }
                SexyLib.insertLog(strID, strREQID, strREQTYPE, strREQMSG, strPRODUCTTYPE);
                //NEU LA MSG 101A THI XU LY PRC101A
                if (strREQTYPE.ToUpper().Equals("E981"))
                {
                    prcMSGE981(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);

                }

                if (strREQTYPE.ToUpper().Equals("00005"))
                {
                    prcMSG005(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);

                }
                if (strREQTYPE.ToUpper().Equals("00021"))
                {
                    prcMSG021(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);

                }
                if (strREQTYPE.ToUpper().Equals("101"))
                {
                    prcMSG101(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);

                }
                if (strREQTYPE.ToUpper().Equals("301"))
                {
                    prcMSG301(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);

                }
                if (strREQTYPE.ToUpper().Equals("06005"))
                {
                    prcMSG6005(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);
                }
                if (strREQTYPE.ToUpper().Equals("06007"))
                {
                    prcMSG06007(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);
                }
                if (strREQTYPE.ToUpper().Equals("06013"))
                {
                    prcMSG06013(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);
                }
                if (strREQTYPE.ToUpper().Equals("310"))
                {
                    prcMSG310(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);
                }
                if (strREQTYPE.ToUpper().Equals("312"))
                {
                    prcMSG312(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);
                }
                if (strREQTYPE.ToUpper().Equals("400"))
                {
                    prcMSG400(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);
                }
                if (strREQTYPE.ToUpper().Equals("100"))
                {
                    prcMSG100(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);
                }
                if (strREQTYPE.ToUpper().Equals("102"))
                {
                    prcMSG102(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);
                }

                //thêm cho phần thuế cá nhân, thuế trước bạ
                if (strREQTYPE.ToUpper().Equals("00128"))
                {
                    prcMSG128(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);

                }
                if (strREQTYPE.ToUpper().Equals("00046"))
                {
                    prcMSG00046(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);

                }
                if (strREQTYPE.ToUpper().Equals("00109"))
                {
                    prcMSG00109(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);

                }
                //truyền message 00021 cho TCT 
                if (strREQTYPE.ToUpper().Equals("00022"))
                {
                    prcMSG022(strREQMSG, strREQID, strPRODUCTTYPE, ref strRESMSG, ref strRESID, ref strRESTYPE, ref strERRCODE, ref strERRMSG);

                }

                //strRESMSG = SexyLib.FomatOutString(strRESMSG);
                //SexyLib.UPDATELog(strID, strRESID, strRESTYPE, strRESMSG, strERRCODE, strERRMSG);
                //strReSult = strRESMSG;


                if (strREQMSG.Length <= 0)
                {
                    {
                       // SexyLib.insertLog(strID, strREQID, strREQTYPE, strREQMSG, strPRODUCTTYPE);
                        strERRCODE = "99999";
                        strERRMSG="Message type invalid";

                        MSG299 obj299 = new MSG299();
                        obj299.Header.Request_ID = strREQID;
                        obj299.Header.Product_Type = strPRODUCTTYPE;
                        obj299.Error.ErrorMessage = strERRMSG;
                        obj299.Error.ErrorNumber = strERRCODE;
                        if (obj299.Error.ErrorMessage.Length > 255)
                            obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                        strRESID = obj299.Header.Transaction_ID;
                        strRESTYPE = "299";
                        strERRMSG = obj299.Error.ErrorMessage;

                        strReSult = obj299.MSG299toXML();
                        strReSult = SexyLib.FomatOutString(strReSult);
                        SexyLib.UPDATELog(strID, strRESID, strRESTYPE, strERRMSG, strERRCODE, strERRMSG);
                        return strReSult;
                    }
                }
                strRESMSG = SexyLib.FomatOutString(strRESMSG);
                SexyLib.UPDATELog(strID, strRESID, strRESTYPE, strRESMSG, strERRCODE, strERRMSG);
                strReSult = strRESMSG;

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
            }
            return strReSult;
        }

        private void prcMSGE981(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";

            try
            {
                //Lay ma so thue

                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSGE981.MSGE981 obj21 = new MSGE981.MSGE981();
                obj21 = obj21.MSGToObject(strXMLTMP);
                string strMST = "";
                string strSHKB = "";
                string strSo_CT = "";
                string strSo_BT = "";
                string strMa_NV = "";
                string strNgayKB = "";
                string strTrang_thai = "";
                string strMa_NH_KB = "";

                if (obj21 != null)
                {
                    //valid message
                    if (SexyLib.checkValidObjE981(obj21, ref pv_StrErrCode, ref  pv_StrErrMSG))
                    {
                        

                        string strSQL = "select * from TCS_BIENLAI_HDR where so_ct_nh='" + obj21.Data.SO_CT + "'";
                        DataSet dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                        if (dsCT != null)
                            if (dsCT.Tables.Count > 0)
                                if (dsCT.Tables[0].Rows.Count > 0)
                                {
                                    strMST = dsCT.Tables[0].Rows[0]["ma_nnthue"].ToString();
                                    strSHKB = dsCT.Tables[0].Rows[0]["shkb"].ToString();
                                    strSo_CT = dsCT.Tables[0].Rows[0]["so_ct"].ToString();
                                    strTrang_thai = dsCT.Tables[0].Rows[0]["trang_thai"].ToString();
                                }
                        //neu chua ton tai chung tu thi dien them du lieu
                        if (strSo_CT.Length <= 0)
                        {
                            Business.BienLaiObj.HdrBl ctuHdr = new Business.BienLaiObj.HdrBl();
                            ctuHdr.MA_NTK = "1";
                            List<Business.BienLaiObj.DtlBl> ctuDtl = new List<Business.BienLaiObj.DtlBl>();
                            strSQL = "SELECT ID, MA_QUY, TK_NS, MA_CHUONG, MA_NKT, MA_NDKT,  ";
                            strSQL += "MA_DBHC, KHTK, MA_LH, SHKB, MA_CQQD, MA_DVSDNS,   TEN_CQ  ";
                            strSQL += "FROM TCS_MAP_CQQD_LHTHU A   WHERE A.SHKB='" + obj21.Data.MA_KB + "' AND A.MA_CQQD='" + obj21.Data.MA_DV.Replace(".", "") + "' and a.MA_LH='"+ obj21.Data.MA_LHTHU +"' order by ID desc ";
                            string strIsKBinSHB = "N";

                            DataSet dsx = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                            if (dsx != null)
                                if (dsx.Tables.Count > 0)
                                    if (dsx.Tables[0].Rows.Count > 0)
                                    {
                                        //ma chuong
                                        if (string.IsNullOrEmpty(obj21.Data.MA_CHUONG))
                                            obj21.Data.MA_CHUONG = dsx.Tables[0].Rows[0]["MA_CHUONG"].ToString();
                                        if (string.IsNullOrEmpty(obj21.Data.MA_CQTHU))
                                            obj21.Data.MA_CQTHU = dsx.Tables[0].Rows[0]["MA_DVSDNS"].ToString();
                                        if (string.IsNullOrEmpty(obj21.Data.MA_NDKT))
                                            obj21.Data.MA_NDKT = dsx.Tables[0].Rows[0]["MA_NDKT"].ToString();
                                        if (string.IsNullOrEmpty(obj21.Data.MA_DBHC))
                                            obj21.Data.MA_DBHC = dsx.Tables[0].Rows[0]["MA_DBHC"].ToString();
                                        if (string.IsNullOrEmpty(obj21.Data.TK_NS))
                                            obj21.Data.TK_NS = dsx.Tables[0].Rows[0]["TK_NS"].ToString();
                                        //MA_NKT
                                      //  ctuHdr.MA_NTK = dsx.Tables[0].Rows[0]["MA_NKT"].ToString();
                                    }
                            //ma loai thue
                            if (string.IsNullOrEmpty(obj21.Data.MA_LTHUE))
                                obj21.Data.MA_LTHUE = "03";
                            if (string.IsNullOrEmpty(obj21.Data.LY_DO_NOP_CHAM))
                                obj21.Data.LY_DO_NOP_CHAM = "";
                            if (string.IsNullOrEmpty(obj21.Data.TTIEN_NOP_CHAM))
                                obj21.Data.TTIEN_NOP_CHAM = "0";
                            
                            //lay ngan hang gian tiep
                            CorebankServiceESB.clsSongPhuong cls_songphuong = new CorebankServiceESB.clsSongPhuong();
                            string strResultKB = cls_songphuong.ValiadSHBBienLai(obj21.Data.MA_KB);
                            if (strResultKB.Length > 8)
                            {

                                string[] strarr = strResultKB.Split(';');
                                string strNHB = strarr[0].ToString();
                                if (string.IsNullOrEmpty(obj21.Data.MA_NH_B))
                                    obj21.Data.MA_NH_B = strNHB;
                                if (obj21.Data.MA_NH_B.Equals(strNHB))
                                    strIsKBinSHB = "Y";
                                strMa_NH_KB = strarr[1].ToString();
                            }
                            //
                            
                            //gan data cho hdr
                            ctuHdr.MA_NNTHUE = obj21.Data.MA_NNTHUE;
                            ctuHdr.TEN_NNTHUE = obj21.Data.TEN_NNTHUE;
                            if (string.IsNullOrEmpty(obj21.Data.DC_NNTHUE))
                                ctuHdr.DC_NNTHUE = "";
                            else
                                ctuHdr.DC_NNTHUE = obj21.Data.DC_NNTHUE;
                            ctuHdr.MA_NNTIEN = obj21.Data.MA_NNTIEN;
                            ctuHdr.TEN_NNTIEN = obj21.Data.TEN_NNTIEN;
                            ctuHdr.DC_NNTIEN = obj21.Data.DC_NNTIEN;
                            string strNgayCT = SexyLib.fnc_StringToDateTime(obj21.Data.NGAY_CT).ToString("dd/MM/yyyy HH:mm:ss");
                            ctuHdr.NGAY_NTIEN = strNgayCT;
                            //ctuHdr.NGAY_CT = strNgayCT;
                            ctuHdr.NGAY_KH_NH = strNgayCT;
                            ctuHdr.SHKB = obj21.Data.MA_KB;
                            ctuHdr.TEN_KB = SexyLib.getTEN_KBNN(obj21.Data.MA_KB);
                            ctuHdr.CQ_QD = obj21.Data.CQ_QD.Replace(".", "");
                            
                            ctuHdr.TEN_CQQD = obj21.Data.TEN_CQ;
                            ctuHdr.SO_QD = obj21.Data.SO_QD;
                            ctuHdr.NGAY_QD = Business_HQ.Common.mdlCommon.ConvertStringToDate(obj21.Data.NGAY_QD, "YYYY-MM-DD").ToString("dd/MM/yyyy");
                            ctuHdr.MA_CQTHU = obj21.Data.MA_CQTHU;
                            ctuHdr.PRODUCT = obj21.Header.Product_Type;
                            ctuHdr.TEN_CQTHU = obj21.Data.TEN_DV;
                            ctuHdr.MA_LTHUE = obj21.Data.MA_LTHUE;
                            ctuHdr.MA_DBHC = obj21.Data.MA_DBHC;
                            ctuHdr.MA_NH_B = obj21.Data.MA_NH_B;
                            ctuHdr.MA_NH_A = obj21.Data.MA_NH_A;
                            ctuHdr.TEN_NH_B = SexyLib.getTEN_GIANTIEP(ctuHdr.SHKB, ctuHdr.MA_NH_B);
                            ctuHdr.MA_NT = obj21.Data.MA_NT;
                            ctuHdr.TY_GIA = obj21.Data.TY_GIA;
                            ctuHdr.TTIEN = obj21.Data.TTIEN;
                            ctuHdr.TTIEN_NT = obj21.Data.TTIEN_NT;
                            ctuHdr.MA_LHTHU = obj21.Data.MA_LHTHU;
                            ctuHdr.TEN_LHTHU = obj21.Data.TEN_LHTHU;
                            ctuHdr.DIENGIAI = obj21.Data.DIEN_GIAI;
                            ctuHdr.SO_CT_NH = obj21.Data.SO_CT;
                            ctuHdr.TK_KH_NH = obj21.Data.TK_KH_NH;
                            ctuHdr.PT_TT = obj21.Data.PHUONGTHUC_TT;
                            ctuHdr.SO_THAM_CHIEU = obj21.Data.SO_THAM_CHIEU;
                            ctuHdr.MA_DV = obj21.Data.MA_DV.Replace(".", "");
                            ctuHdr.TK_CO = obj21.Data.TK_NS;
                            ctuHdr.LY_DO_VPHC = obj21.Data.LY_DO;
                            ctuHdr.TTIEN_VPHC = obj21.Data.TTIEN_VPHC;
                            ctuHdr.TTIEN_NOP_CHAM = obj21.Data.TTIEN_NOP_CHAM;
                            //SO BIEN LAI do core bank sinh
                            ctuHdr.SO_BIENLAI = obj21.Data.SO_BIENLAI; //Business.SP.SongPhuongController.fnc_GetSoBienLai();
                            ctuHdr.SERI = obj21.Data.SERI;
                            //chung tu lech tao  chung tu mac dinh la lech => cho kiem soat hoac 08 ko xu ly
                            ctuHdr.TRANG_THAI = "07";
                            ctuHdr.MA_NV = obj21.Header.UserID;
                            //if (strNgayCT.Length >= 10)
                            //{
                            //    ctuHdr.KY_THUE = strNgayCT.Substring(3, 7);
                            //}
                            //else
                            //{
                            //    ctuHdr.KY_THUE = "";
                            //}
                            ctuHdr.KY_THUE = "";
                            ctuHdr.HT_THU = "1";
                            ctuHdr.TEN_LTHUE = "Khoan thu do co quan khac quan ly";
                            ctuHdr.TTIEN_CTBL = obj21.Data.TTIEN;
                            ctuHdr.MA_CN = SexyLib.TCS_GetMACN_NV(obj21.Header.UserID);
                            ctuHdr.PRODUCT = obj21.Header.Product_Type;
                            ctuHdr.MA_NH_KB = strMa_NH_KB;
                            ctuHdr.MA_CHUONG = obj21.Data.MA_CHUONG;
                            ctuHdr.MA_NDKT = obj21.Data.MA_NDKT;
                            ctuHdr.MA_CQQD = obj21.Data.CQ_QD;
                            //VOI CON NAY KO CAT TIEN CHI DI TTSP
                            ctuHdr.PAYMENT = "2";
                            //doan nay em build noi dung theo yeu cau kho bac
                            //"G22.99.1+<Mã ngân hàng 8 số của đơn vị KBNN nhận khoản phạt>+<Mã cơ quan ra quyết định xử phạt>+<Số Quyết định xử phạt>+<Mã loại hình thu>+<Số Seri của biên lai đã trả cho người nộp>+<Số biên lai đã trả cho người nộp>+<Ngày nộp tiền>+STPhat:<Số tiền phạt vi phạm>+STChamnop:<Số tiền phạt chậm nộp>+STC:<So_tham_chieu>”
                            ctuHdr.NOI_DUNG = "G22.99.1";
                            //+<Mã ngân hàng 8 số của đơn vị KBNN nhận khoản phạt>
                            ctuHdr.NOI_DUNG += "+" + strMa_NH_KB;
                            //+<Mã cơ quan ra quyết định xử phạt>
                            ctuHdr.NOI_DUNG += "+" + obj21.Data.CQ_QD;
                            //+<Số Quyết định xử phạt>
                            ctuHdr.NOI_DUNG += "+" + obj21.Data.SO_QD;
                            //+<Mã loại hình thu>
                            ctuHdr.NOI_DUNG += "+" + obj21.Data.MA_LHTHU;
                            //+<Số Seri của biên lai đã trả cho người nộp>
                            ctuHdr.NOI_DUNG += "+" + obj21.Data.SERI;
                            //+<Số biên lai đã trả cho người nộp>
                            ctuHdr.NOI_DUNG += "+" + obj21.Data.SO_BIENLAI;
                            //+<Ngày nộp tiền>
                            ctuHdr.NOI_DUNG += "+" + SexyLib.fnc_StringToDateTime(obj21.Data.NGAY_CT).ToString("ddMMyyyy");
                            //+STPhat:<Số tiền phạt vi phạm>
                            ctuHdr.NOI_DUNG += "+STPhat:" + obj21.Data.TTIEN_VPHC;
                            //+STChamnop:<Số tiền phạt chậm nộp>
                            ctuHdr.NOI_DUNG += "+STChamnop:" + obj21.Data.TTIEN_NOP_CHAM;
                            //+STC:<So_tham_chieu>”
                            ctuHdr.NOI_DUNG += "+STC:" + obj21.Data.SO_THAM_CHIEU;
                           // ctuHdr.pay
                            if (!strIsKBinSHB.Equals("Y"))
                            { 
                                //ctuHdr.ly
                                ctuHdr.TRANG_THAI = "10";
                            }
                            if (strIsKBinSHB.Equals("Y") && SexyLib.CheckValidStatusObjE981(obj21, ref pv_StrErrCode, ref  pv_StrErrMSG))
                            {
                                ctuHdr.TRANG_THAI = "03";
                                //gan luon ca ma nhan vien kiem soat vaf thoi gian kiem soat
                                ctuHdr.MA_KS = ctuHdr.MA_NV;
                                ctuHdr.NGAY_KS = strNgayCT;
                            }
                            else
                            { 
                                //de nguyen trang thai lech , them phaan ly do
                             
                            }

                            if (Business.SP.SongPhuongController.Insert_ChungtuBL(ctuHdr,ref strSo_CT))
                            {
                                if (ctuHdr.TRANG_THAI.Equals("03"))
                                {
                                    strTrang_thai = "03";
                                    pv_StrErrCode = "0";
                                    pv_StrErrMSG = "Successfull";
                                }
                                else if (ctuHdr.TRANG_THAI.Equals("10"))
                                {
                                    strTrang_thai = "10";
                                    pv_StrErrCode = "910401";
                                    pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode);
                                }
                                else
                                {
                                    pv_StrErrCode = "9999";
                                    pv_StrErrMSG = "Error When insert CTU Bien Lai";
                                }
                            }
                            else
                            {
                                pv_StrErrCode = "9999";
                                pv_StrErrMSG = "Error When insert CTU Bien Lai";
                            }
                           
                            

                        }
                        //neu ghi nhan chung tu thanh cong
                        if (pv_StrErrCode.Equals("0") && strTrang_thai.Equals("03"))
                        {
                            if (strSo_CT.Length > 0)
                            {

                                //gui lenh di song phuong
                                if (Business.SP.SongPhuongController.fnc_Update_TRANG_THAI_BIEN_LAI_CHUYEN_SP_LOI(strSo_CT))
                                {
                                    strTrang_thai = "08";
                                }else
                                {
                                      pv_StrErrCode = "99999";
                                      pv_StrErrMSG = "Has Error in Update status send TTSP CTU["+ strSo_CT +"] ";
                                }
                            }
                            
                        }
                        if (pv_StrErrCode.Equals("0") && strTrang_thai.Equals("08") && strSo_CT.Length>0)
                        {
                          //cap nhat trang thai chuyen SP loi

                                CorebankServiceESB.clsSongPhuong cls_songphuong = new CorebankServiceESB.clsSongPhuong();
                                string strErrorNum = cls_songphuong.SendBienLaiSP_New(strSo_CT);
                                if (strErrorNum.ToUpper().Equals("SUCCESS"))
                                {
                                    Business.SP.SongPhuongController.fnc_Update_TRANG_THAI_BIEN_LAI_CHUYEN_THANH_CONG(strSo_CT);
                                    strTrang_thai = "09";
                                    
                                }
                        }
                        if (strTrang_thai.Equals("09") && pv_StrErrCode.Equals("0"))
                        {
                            if (strTrang_thai.Equals("09"))
                            pv_StrErrCode = "0";
                            pv_StrErrMSG = "SucessFull";

                        }
                        if (pv_StrErrCode.Equals("0"))
                        { 
                        
                        }
                        else
                        {
                            pv_StrErrCode = "91003";
                            pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode);

                        }

                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                }
                //phan hoi 200
                if (pv_StrErrCode.Equals("0"))
                {
                    MSG200.MSG200 obj200 = new MSG200.MSG200();
                    obj200.Header.Request_ID = obj21.Header.Transaction_ID;
                    obj200.Data = new MSG200.Data200();
                    obj200.Data.So_TN_CT = strSo_CT;
                    obj200.Data.Ngay_TN_CT = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG200toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "E0299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "E0299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }

        private void prcMSG005(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";
            //      string strREQID = "";
            try
            {
                //Lay ma so thue
                string strMST = "";
                XmlDocument vdoc = new XmlDocument();
                vdoc.LoadXml(pv_strXML);
                strMST = vdoc.SelectSingleNode("ETAX/Data/MST").InnerText;

                string strXML006 = ProcMegService.ProMsg.getMSG05API(strMST);

                if (strXML006.Length > 0)
                {
                    ProcMegService.MSG.MSG06.MSG06 msg06 = new ProcMegService.MSG.MSG06.MSG06();


                    strXML006 = strXML006.Replace("<DATA>", SexyLib.strXMLdefin2);
                    ProcMegService.MSG.MSG06.MSG06 objTemp = msg06.MSGToObject(strXML006);
                    if (objTemp.Body.Row.Thongtin_NNT != null)
                    {
                        pv_StrErrCode = "0";
                        pv_StrErrMSG = "Successfull";
                        MSG00006.MSG00006 obj06 = new MSG00006.MSG00006();
                        obj06.Header.Request_ID = strREQID;
                        obj06.Header.Product_Type = pv_StrProductType;
                        if (objTemp.Body.Row.Thongtin_NNT.Thongtinchung != null)
                        {
                            if (objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt != null)
                            {
                                obj06.Data.Thongtin_NNT.Thongtinchung = new MSG00006.THONGTINCHUNG();
                                obj06.Data.Thongtin_NNT.Thongtinchung.row_nnt = new List<MSG00006.ROW_NNT>();
                                for (int i = 0; i < objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt.Count; i++)
                                {
                                    MSG00006.ROW_NNT objROW_NNT = new MSG00006.ROW_NNT();
                                    objROW_NNT.CHUONG = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].CHUONG;
                                    objROW_NNT.LOAI_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].LOAI_NNT;
                                    objROW_NNT.MA_CQT_QL = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].MA_CQT_QL;
                                    objROW_NNT.MST = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].MST;
                                    objROW_NNT.SO = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].SO;
                                    objROW_NNT.TEN_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].TEN_NNT;
                                    obj06.Data.Thongtin_NNT.Thongtinchung.row_nnt.Add(objROW_NNT);
                                }
                            }
                        }
                        if (objTemp.Body.Row.Thongtin_NNT.Diachi != null)
                        {
                            if (objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi != null)
                            {
                                obj06.Data.Thongtin_NNT.Diachi = new MSG00006.DIACHI();
                                obj06.Data.Thongtin_NNT.Diachi.Row_Diachi = new List<MSG00006.ROW_DIACHI>();
                                for (int i = 0; i < objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi.Count; i++)
                                {
                                    MSG00006.ROW_DIACHI objROW_DIACHI = new MSG00006.ROW_DIACHI();
                                    objROW_DIACHI.MA_HUYEN = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MA_HUYEN;
                                    objROW_DIACHI.MA_TINH = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MA_TINH;
                                    objROW_DIACHI.MA_XA = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MA_XA;
                                    objROW_DIACHI.MOTA_DIACHI = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MOTA_DIACHI;
                                    obj06.Data.Thongtin_NNT.Diachi.Row_Diachi.Add(objROW_DIACHI);
                                }

                            }
                        }
                        if (objTemp.Body.Row.Thongtin_NNT.Sothue != null)
                        {
                            if (objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue != null)
                            {
                                obj06.Data.Thongtin_NNT.Sothue = new MSG00006.SOTHUE();
                                obj06.Data.Thongtin_NNT.Sothue.Row_Sothue = new List<MSG00006.ROW_SOTHUE>();
                                for (int i = 0; i < objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue.Count; i++)
                                {
                                    MSG00006.ROW_SOTHUE objROW_SOTHUE = new MSG00006.ROW_SOTHUE();

                                    objROW_SOTHUE.LOAI_TIEN = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].LOAI_TIEN;
                                    objROW_SOTHUE.MA_CHUONG = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_CHUONG;
                                    objROW_SOTHUE.MA_CQ_THU = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_CQ_THU;
                                    objROW_SOTHUE.MA_TMUC = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_TMUC;
                                    objROW_SOTHUE.NGAY_QDINH = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].NGAY_QDINH;
                                    objROW_SOTHUE.NO_CUOI_KY = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].NO_CUOI_KY;
                                    objROW_SOTHUE.SO_QDINH = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SO_QDINH;
                                    objROW_SOTHUE.SO_TAI_KHOAN_CO = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SO_TAI_KHOAN_CO;
                                    objROW_SOTHUE.TI_GIA = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].TI_GIA;
                                    //
                                    objROW_SOTHUE.TEN_CQ_THU = SexyLib.getTEN_CQTHU(objROW_SOTHUE.MA_CQ_THU, ""); // objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].TEN_CQ_THU;
                                    objROW_SOTHUE.MA_KB = SexyLib.getMA_KBNN(objROW_SOTHUE.MA_CQ_THU, "");// objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_KB;
                                    //objROW_SOTHUE.TEN_KB = SexyLib.getTEN_KBNN(objROW_SOTHUE.MA_KB);// objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].TEN_KB;
                                    objROW_SOTHUE.NOI_DUNG = SexyLib.getTEN_TMUC(objROW_SOTHUE.MA_TMUC);// objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].NOI_DUNG;
                                    // doan nay hhiepvx add them cac phan lay them thong tin kho bac
                                    string strSQL = "select shkb,ten,ma_tinh, ma_db from tcs_dm_khobac where shkb='" + objROW_SOTHUE.MA_KB + "'";
                                    DataSet dsKB = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                    if (dsKB != null)
                                        if (dsKB.Tables.Count > 0)
                                            if (dsKB.Tables[0].Rows.Count > 0)
                                            {
                                                objROW_SOTHUE.TEN_KB = dsKB.Tables[0].Rows[0]["ten"].ToString();
                                                objROW_SOTHUE.MA_DBHC_KBNN = dsKB.Tables[0].Rows[0]["ma_db"].ToString();
                                                objROW_SOTHUE.MA_TINH_KBNN = dsKB.Tables[0].Rows[0]["ma_tinh"].ToString();
                                            }
                                    //doan nay lay thong tin ngan hang huong
                                    //strSQL = "select ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep from tcs_dm_nh_giantiep_tructiep where shkb='" + objROW_SOTHUE.MA_KB + "' order by id ";
                                    //DataSet dsNH = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                    //if (dsNH != null)
                                    //    if (dsNH.Tables.Count > 0)
                                    //        if (dsNH.Tables[0].Rows.Count > 0)
                                    //        {
                                    //            objROW_SOTHUE.MA_NH_TT = dsNH.Tables[0].Rows[0]["ma_tructiep"].ToString();
                                    //            objROW_SOTHUE.TEN_NH_TT = dsNH.Tables[0].Rows[0]["ten_tructiep"].ToString();
                                    //            objROW_SOTHUE.MA_NH_B = dsNH.Tables[0].Rows[0]["ma_giantiep"].ToString();
                                    //            objROW_SOTHUE.TEN_NH_B = dsNH.Tables[0].Rows[0]["ten_giantiep"].ToString();

                                    //        }
                                    string ma_tt = "";
                                    string ten_tt = "";
                                    string ma_gt = "";
                                    string ten_gt = "";
                                    GetNHGianTiepTrucTiep(objROW_SOTHUE.MA_KB, ref ma_tt, ref ten_tt, ref ma_gt, ref ten_gt);
                                    objROW_SOTHUE.MA_NH_TT = ma_tt;
                                    objROW_SOTHUE.TEN_NH_TT = ten_tt;
                                    objROW_SOTHUE.MA_NH_B = ma_gt;
                                    objROW_SOTHUE.TEN_NH_B = ten_gt;

                                    obj06.Data.Thongtin_NNT.Sothue.Row_Sothue.Add(objROW_SOTHUE);
                                }
                            }
                        }
                        strReSult = obj06.MSG00006toXML();
                        pv_StrRESID = obj06.Header.Transaction_ID;
                        pv_StrRESTYPE = obj06.Header.Message_Type;
                    }
                    else
                    {
                        pv_StrErrCode = "99999";
                        pv_StrErrMSG = "System error when Inquiry Tax Info";
                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        private void prcMSG101(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";
            //      string strREQID = "";
            try
            {
                //Lay ma so thue
                MSG101.MSG101 obj101 = new MSG101.MSG101();
                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                obj101 = obj101.MSGToObject(strXMLTMP);
                pv_StrErrCode = "0";
                pv_StrErrMSG = "";
                if (SexyLib.checkValidObj101(obj101, ref pv_StrErrCode, ref pv_StrErrMSG))
                {
                    if (obj101.Data.Nam_DK == null)
                        obj101.Data.Nam_DK = "";
                    if (obj101.Data.So_TK == null)
                        obj101.Data.So_TK = "";
                    string strMSGIn = CustomsServiceV3.ProcessMSG.getMSG12API(obj101.Data.Ma_DV, obj101.Data.Nam_DK, obj101.Data.So_TK, ref pv_StrErrCode, ref pv_StrErrMSG);
                    if (pv_StrErrCode.Equals("0"))
                    {
                        CustomsV3.MSG.MSG201.MSG201 obj201c = new CustomsV3.MSG.MSG201.MSG201();
                        obj201c = obj201c.MSGToObject(strMSGIn);
                        MSG201.MSG201 obj21 = new MSG201.MSG201();
                        for (int i = 0; i < obj201c.Data.Item.Count; i++)
                        {
                            MSG201.Item objitem = new MSG201.Item();
                            objitem.Ma_Cuc = obj201c.Data.Item[i].Ma_Cuc;
                            objitem.Ten_Cuc = obj201c.Data.Item[i].Ten_Cuc;
                            objitem.Ma_HQ_PH = obj201c.Data.Item[i].Ma_HQ_PH;//Ma_HQ_PH				
                            objitem.Ten_HQ_PH = obj201c.Data.Item[i].Ten_HQ_PH;//Ten_HQ_PH				
                            objitem.Ma_HQ_CQT = obj201c.Data.Item[i].Ma_HQ_CQT;//Ma_HQ_CQT				
                            objitem.Ma_DV = obj201c.Data.Item[i].Ma_DV;//Ma_DV				
                            objitem.Ten_DV = obj201c.Data.Item[i].Ten_DV;//Ten_DV				
                            objitem.Ma_Chuong = obj201c.Data.Item[i].Ma_Chuong;//Ma_Chuong				
                            objitem.Ma_HQ = obj201c.Data.Item[i].Ma_HQ;//Ma_HQ				
                            objitem.Ten_HQ = obj201c.Data.Item[i].Ten_HQ;//Ten_HQ				
                            objitem.Ma_LH = obj201c.Data.Item[i].Ma_LH;//Ma_LH				
                            objitem.Ten_LH = obj201c.Data.Item[i].Ten_LH;//Ten_LH				
                            objitem.Nam_DK = obj201c.Data.Item[i].Nam_DK;//Nam_DK				
                            objitem.So_TK = obj201c.Data.Item[i].So_TK;//So_TK				
                            objitem.Ma_NTK = obj201c.Data.Item[i].Ma_NTK;//Ma_NTK				
                            objitem.Ten_NTK = obj201c.Data.Item[i].Ten_NTK;//Ten_NTK				
                            objitem.Ma_LT = obj201c.Data.Item[i].Ma_LT;//Ma_LT				
                            objitem.Ma_HTVCHH = obj201c.Data.Item[i].Ma_HTVCHH;//Ma_HTVCHH				
                            objitem.Ten_HTVCHH = obj201c.Data.Item[i].Ten_HTVCHH;//Ten_HTVCHH				
                            objitem.Ngay_DK = obj201c.Data.Item[i].Ngay_DK;//Ngay_DK				
                            objitem.Ma_KB = obj201c.Data.Item[i].Ma_KB;//Ma_KB				
                            objitem.Ten_KB = obj201c.Data.Item[i].Ten_KB;//Ten_KB				
                            objitem.TKKB = obj201c.Data.Item[i].TKKB;//TKKB				
                            objitem.TTNo = obj201c.Data.Item[i].TTNo;//TTNo				
                            objitem.Ten_TTN = obj201c.Data.Item[i].Ten_TTN;//Ten_TTN				
                            objitem.TTNo_CT = obj201c.Data.Item[i].TTNo_CT;//TTNo_CT				
                            objitem.Ten_TTN_CT = obj201c.Data.Item[i].Ten_TTN_VT;//Ten_TTN_CT				
                            objitem.DuNo_TO = obj201c.Data.Item[i].DuNo_TO;//DuNo_TO		
                            string strSQL = "select shkb,ten,ma_tinh, ma_db from tcs_dm_khobac where shkb='" + objitem.Ma_KB + "'";
                            DataSet dsKB = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                            if (dsKB != null)
                                if (dsKB.Tables.Count > 0)
                                    if (dsKB.Tables[0].Rows.Count > 0)
                                    {

                                        objitem.MA_DBHC_KBNN = dsKB.Tables[0].Rows[0]["ma_db"].ToString();
                                        objitem.MA_TINH_KBNN = dsKB.Tables[0].Rows[0]["ma_tinh"].ToString();
                                    }
                            //doan nay lay thong tin ngan hang huong
                            //strSQL = "select ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep from tcs_dm_nh_giantiep_tructiep where shkb='" + objitem.Ma_KB + "' order by id ";
                            //DataSet dsNH = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                            //if (dsNH != null)
                            //    if (dsNH.Tables.Count > 0)
                            //        if (dsNH.Tables[0].Rows.Count > 0)
                            //        {
                            //            objitem.MA_NH_TT = dsNH.Tables[0].Rows[0]["ma_tructiep"].ToString();
                            //            objitem.TEN_NH_TT = dsNH.Tables[0].Rows[0]["ten_tructiep"].ToString();
                            //            objitem.MA_NH_B = dsNH.Tables[0].Rows[0]["ma_giantiep"].ToString();
                            //            objitem.TEN_NH_B = dsNH.Tables[0].Rows[0]["ten_giantiep"].ToString();

                            //        }
                            string ma_tt = "";
                            string ten_tt = "";
                            string ma_gt = "";
                            string ten_gt = "";
                            GetNHGianTiepTrucTiep(objitem.Ma_KB, ref ma_tt, ref ten_tt, ref ma_gt, ref ten_gt);
                            objitem.MA_NH_TT = ma_tt;
                            objitem.TEN_NH_TT = ten_tt;
                            objitem.MA_NH_B = ma_gt;
                            objitem.TEN_NH_B = ten_gt;


                            objitem.CT_No = new List<MSG201.CT_No>();
                            for (int x = 0; x < obj201c.Data.Item[i].CT_No.Count; x++)
                            {
                                MSG201.CT_No objct = new MSG201.CT_No();
                                objct.DuNo = obj201c.Data.Item[i].CT_No[x].DuNo;
                                objct.TieuMuc = obj201c.Data.Item[i].CT_No[x].TieuMuc;
                                objct.Khoan = obj201c.Data.Item[i].CT_No[x].Khoan;
                                objct.LoaiThue = obj201c.Data.Item[i].CT_No[x].LoaiThue;
                                objct.NoiDung = SexyLib.getTEN_TMUC(objct.TieuMuc);
                                objitem.CT_No.Add(objct);
                            }
                            obj21.Data.Item.Add(objitem);
                        }
                        obj21.Header.Request_ID = strREQID;
                        obj21.Header.Product_Type = pv_StrProductType;
                        pv_StrRESID = obj21.Header.Transaction_ID;
                        pv_StrRESTYPE = obj21.Header.Message_Type;
                        pv_XMLOut = obj21.MSG201toXML();
                        return;
                    }
                }

                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = pv_StrErrMSG;
                obj299.Error.ErrorNumber = pv_StrErrCode;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                pv_XMLOut = obj299.MSG299toXML();

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        private void prcMSG021(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";

            try
            {
                //Lay ma so thue

                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSG21.MSG21 obj21 = new MSG21.MSG21();
                obj21 = obj21.MSGToObject(strXMLTMP);
                string strMST = "";
                string strSHKB = "";
                string strSo_CT = "";
                string strSo_BT = "";
                string strMa_NV = "";
                string strNgayKB = "";
                string strTrang_thai = "";
                string strmaCQT = "";
                string strTen_CQT = "";
                string strMa_NTK = "";
                if (obj21 != null)
                {
                    //valid message
                    if (SexyLib.checkValidObj21(obj21, ref pv_StrErrCode, ref  pv_StrErrMSG))
                    {
                        //ma_cqthu,ma_ntk 
                        string strSQL = "select * from tcs_ctu_hdr where so_ct_nh='" + obj21.Data.CHUNGTU.SO_REFCORE + "' and so_ct='" + obj21.Data.CHUNGTU.SO_CHUNGTU + "'";
                        DataSet dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                        if (dsCT != null)
                            if (dsCT.Tables.Count > 0)
                                if (dsCT.Tables[0].Rows.Count > 0)
                                {
                                    strMST = dsCT.Tables[0].Rows[0]["ma_nnthue"].ToString();
                                    strSHKB = dsCT.Tables[0].Rows[0]["shkb"].ToString();
                                    strSo_CT = dsCT.Tables[0].Rows[0]["so_ct"].ToString();
                                    strSo_BT = dsCT.Tables[0].Rows[0]["so_bt"].ToString();
                                    strMa_NV = dsCT.Tables[0].Rows[0]["ma_nv"].ToString();
                                    strNgayKB = dsCT.Tables[0].Rows[0]["ngay_kb"].ToString();
                                    strTrang_thai = dsCT.Tables[0].Rows[0]["trang_thai"].ToString();
                                    strmaCQT = dsCT.Tables[0].Rows[0]["ma_cqthu"].ToString();
                                    strMa_NTK = dsCT.Tables[0].Rows[0]["ma_ntk"].ToString();
                                    strTen_CQT = dsCT.Tables[0].Rows[0]["TEN_CQTHU"].ToString();
                                }
                        pv_StrErrCode = "0";
                        pv_StrErrMSG = "Successfull";
                        if (strMST.Length > 0)
                            if (!strMST.Equals(obj21.Data.CHUNGTU.MST))
                            {
                                pv_StrErrCode = "99997";
                                pv_StrErrMSG = "TAXNO or Voucher Info not same with the first message, pls contact admin system ";
                            }
                        if (pv_StrErrCode.Equals("0"))
                        {
                            if (strSo_CT.Length <= 0)
                            {
                                //khai bao cac object HDR va DTL
                                NewChungTu.infChungTuHDR objCTuHDR = new NewChungTu.infChungTuHDR();
                                List<NewChungTu.infChungTuDTL> objCTuDTL = new List<NewChungTu.infChungTuDTL>();
                                SexyLib.setObject21ToCTU(obj21, ref objCTuHDR, ref objCTuDTL);
                                //insert chung tu
                                SexyLib.Insert_New0021(objCTuHDR, objCTuDTL);
                                strMST = objCTuHDR.Ma_NNThue;
                                strSHKB = objCTuHDR.SHKB;// dsCT.Tables[0].Rows[0]["shkb"].ToString();
                                strSo_CT = objCTuHDR.So_CT;// dsCT.Tables[0].Rows[0]["so_ct"].ToString();
                                strSo_BT = objCTuHDR.So_BT.ToString();// dsCT.Tables[0].Rows[0]["so_bt"].ToString();
                                strMa_NV = objCTuHDR.Ma_NV.ToString();// dsCT.Tables[0].Rows[0]["ma_nv"].ToString();
                                strNgayKB = objCTuHDR.Ngay_KB.ToString();// dsCT.Tables[0].Rows[0]["ngay_kb"].ToString();
                                strTrang_thai = "07";
                            }
                            if (strTrang_thai.Equals("07"))
                            {
                                //gui thue
                                string errcode = "01";
                                string errdesc = "";

                                ProcMegService.ProMsg.getMSG21(strMST, strSHKB, strSo_CT, strSo_BT, strMa_NV, "01", ref errcode, ref errdesc);
                                //duyet chung tu
                                //neu thanh cong  
                                if (errcode.Equals("01"))
                                {
                                    SexyLib.CTU_SetTrangThai2(strSo_CT, strSHKB, strMa_NV, strNgayKB, strSo_BT, "01", "", "", "", strMa_NV, "", "", "1", strMa_NTK, strTen_CQT);
                                    //"", "", "", "", "", "", "1","",true);
                                }
                                else
                                {
                                    pv_StrErrCode = "99999";
                                    pv_StrErrMSG = "[" + errcode + "]" + errdesc;
                                }
                            }


                        }

                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                }
                //phan hoi 200
                if (pv_StrErrCode.Equals("0"))
                {
                    MSG200.MSG200 obj200 = new MSG200.MSG200();
                    obj200.Header.Request_ID = obj21.Header.Transaction_ID;
                    obj200.Data = new MSG200.Data200();
                    obj200.Data.So_TN_CT = strSo_CT;
                    obj200.Data.Ngay_TN_CT = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG200toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        private void prcMSG301(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";

            try
            {
                //Lay ma so thue

                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSG301.MSG301 obj21 = new MSG301.MSG301();
                obj21 = obj21.MSGToObject(strXMLTMP);
                string strMST = "";
                string strSHKB = "";
                string strSo_CT = "";
                string strSo_BT = "";
                string strMa_NV = "";
                string strNgayKB = "";
                string strTrang_thai = "";
                if (obj21 != null)
                {
                    //valid message
                    if (SexyLib.checkValidObj301(obj21, ref pv_StrErrCode, ref  pv_StrErrMSG))
                    {

                        string strSQL = "select * from tcs_ctu_hdr where so_ct_nh='" + obj21.Data.SO_REFCORE + "' and so_ct='" + obj21.Data.So_CT + "'";
                        DataSet dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                        if (dsCT != null)
                            if (dsCT.Tables.Count > 0)
                                if (dsCT.Tables[0].Rows.Count > 0)
                                {
                                    strMST = dsCT.Tables[0].Rows[0]["ma_nnthue"].ToString();
                                    strSHKB = dsCT.Tables[0].Rows[0]["shkb"].ToString();
                                    strSo_CT = dsCT.Tables[0].Rows[0]["so_ct"].ToString();
                                    strSo_BT = dsCT.Tables[0].Rows[0]["so_bt"].ToString();
                                    strMa_NV = dsCT.Tables[0].Rows[0]["ma_nv"].ToString();
                                    strNgayKB = dsCT.Tables[0].Rows[0]["ngay_kb"].ToString();
                                    strTrang_thai = dsCT.Tables[0].Rows[0]["trang_thai"].ToString();
                                }
                        pv_StrErrCode = "0";
                        pv_StrErrMSG = "Successfull";
                        if (strMST.Length > 0)
                            if (!strMST.Equals(obj21.Data.Ma_DV))
                            {
                                pv_StrErrCode = "99997";
                                pv_StrErrMSG = "TAXNO or Voucher Info not same with the first message, pls contact admin system ";
                            }
                        if (pv_StrErrCode.Equals("0"))
                        {
                            if (strSo_CT.Length <= 0)
                            {
                                //khai bao cac object HDR va DTL
                                NewChungTu.HdrHq objCTuHDR = new NewChungTu.HdrHq();
                                List<NewChungTu.DtlHq> objCTuDTL = new List<NewChungTu.DtlHq>();
                                SexyLib.setObject301ToCTU(obj21, ref objCTuHDR, ref objCTuDTL);
                                //insert chung tu
                                SexyLib.Insert_New301(objCTuHDR, objCTuDTL);
                                strMST = objCTuHDR.MA_NNTHUE;
                                strSHKB = objCTuHDR.SHKB;// dsCT.Tables[0].Rows[0]["shkb"].ToString();
                                strSo_CT = objCTuHDR.SO_CT;// dsCT.Tables[0].Rows[0]["so_ct"].ToString();
                                strSo_BT = objCTuHDR.SO_BT.ToString();// dsCT.Tables[0].Rows[0]["so_bt"].ToString();
                                strMa_NV = objCTuHDR.MA_NV.ToString();// dsCT.Tables[0].Rows[0]["ma_nv"].ToString();
                                strNgayKB = objCTuHDR.NGAY_KB.ToString();// dsCT.Tables[0].Rows[0]["ngay_kb"].ToString();
                                strTrang_thai = "07";
                            }
                            if (strTrang_thai.Equals("07"))
                            {
                                //gui thue
                                string errcode = "01";
                                string errdesc = "";
                                string strTranID = "";
                                string pvSoTN = "";
                                string hTiepNhan = "";
                                CustomsServiceV3.ProcessMSG.guiCTThueHQ(strMST, strSo_CT, ref errcode, ref errdesc, ref strTranID, ref pvSoTN, ref hTiepNhan);
                                // ProcMegService.ProMsg.getMSG21(strMST, strSHKB, strSo_CT, strSo_BT, strMa_NV, "01", ref errcode, ref errdesc);
                                //duyet chung tu
                                //neu thanh cong  
                                if (errcode.Equals("0"))
                                {

                                    SexyLib.CTU_SetTrangThai(strSo_CT, strSHKB, strMa_NV, strNgayKB, strSo_BT, "01", "", "", "", strMa_NV, "", "", "1");
                                    SexyLib.UpdateDCCTHQ(strTranID, hTiepNhan, pvSoTN, strSo_CT);
                                    //insert doi chieu
                                    SexyLib.InsertDCHQ(strSo_CT);
                                    //"", "", "", "", "", "", "1","",true);
                                }
                                else
                                {
                                    pv_StrErrCode = "99999";
                                    pv_StrErrMSG = "[" + errcode + "]" + errdesc;
                                }
                            }


                        }

                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                }
                //phan hoi 200
                if (pv_StrErrCode.Equals("0"))
                {
                    MSG200.MSG200 obj200 = new MSG200.MSG200();
                    obj200.Header.Request_ID = obj21.Header.Transaction_ID;
                    obj200.Data = new MSG200.Data200();
                    obj200.Data.So_TN_CT = strSo_CT;
                    obj200.Data.Ngay_TN_CT = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG200toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        private void prcMSG6005(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";
            //      string strREQID = "";
            try
            {
                //Lay ma so thue
                string strMST = "";
                XmlDocument vdoc = new XmlDocument();
                vdoc.LoadXml(pv_strXML);
                strMST = vdoc.SelectSingleNode("ETAX/Data/MST").InnerText;
                eTAXAPILib.MSG06006.MSG06006 obj = new MSG06006.MSG06006();
                obj.Data.Thongtin_NNT = new MSG06006.DKY_NTHUE();
                //   log.Info(" Start getMSG6005API  MST = " + strMST);

                if (SexyLib.checkExistRegister(strMST))
                {
                    string strSQL = "select a.nntdt_id,a.MST,a.ten_nnt,a.diachi_nnt,a.ma_cqt,(select max(b.ten) from tcs_dm_cqthu b  where b.ma_qlt=a.ma_cqt ) ten_cqt, ";
                    strSQL += " a.SDT_NNT,EMAIL_NNT,a.TEN_LHE_NTHUE,a.SERIAL_CERT_NTHUE,a.SUBJECT_CERT_NTHUE,a.ISSUER_CERT_NTHUE ,a.MA_NHANG,a.TEN_NHANG, ";
                    strSQL += " a.VAN_ID,a.TEN_TVAN,a.ngay_gui,a.TRANG_THAI,( select max(b.mota) from tcs_dm_trangthai_ntdt b  where b.trang_thai = a.trang_thai and b.ghichu='NNT' )TEN_TRANG_THAI from tcs_dm_nntdt  a  where mst='" + strMST + "' order by nntdt_id desc ";
                    DataSet dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);

                    if (dsCT != null)
                    {
                        if (dsCT.Tables.Count > 0)
                        {
                            if (dsCT.Tables[0].Rows.Count > 0)
                            {
                                //strMST = dsCT.Tables[0].Rows[0]["ma_nnthue"].ToString();
                                //map header

                                obj.Data.Thongtin_NNT.MST = dsCT.Tables[0].Rows[0]["MST"].ToString();
                                obj.Data.Thongtin_NNT.TEN_NNT = dsCT.Tables[0].Rows[0]["TEN_NNT"].ToString();
                                obj.Data.Thongtin_NNT.DIACHI_NNT = dsCT.Tables[0].Rows[0]["DIACHI_NNT"].ToString();
                                obj.Data.Thongtin_NNT.MA_CQT = dsCT.Tables[0].Rows[0]["MA_CQT"].ToString();
                                obj.Data.Thongtin_NNT.SDT_NNT = dsCT.Tables[0].Rows[0]["SDT_NNT"].ToString();
                                obj.Data.Thongtin_NNT.EMAIL_NNT = dsCT.Tables[0].Rows[0]["EMAIL_NNT"].ToString();
                                obj.Data.Thongtin_NNT.TEN_LHE_NTHUE = dsCT.Tables[0].Rows[0]["TEN_LHE_NTHUE"].ToString();
                                obj.Data.Thongtin_NNT.SERIAL_CERT_NTHUE = dsCT.Tables[0].Rows[0]["SERIAL_CERT_NTHUE"].ToString();
                                obj.Data.Thongtin_NNT.SUBJECT_CERT_NTHUE = dsCT.Tables[0].Rows[0]["SUBJECT_CERT_NTHUE"].ToString();
                                obj.Data.Thongtin_NNT.ISSUER_CERT_NTHUE = dsCT.Tables[0].Rows[0]["ISSUER_CERT_NTHUE"].ToString();
                                obj.Data.Thongtin_NNT.MA_NHANG = dsCT.Tables[0].Rows[0]["TEN_NHANG"].ToString();
                                obj.Data.Thongtin_NNT.VAN_ID = dsCT.Tables[0].Rows[0]["VAN_ID"].ToString();
                                obj.Data.Thongtin_NNT.TEN_TVAN = dsCT.Tables[0].Rows[0]["TEN_TVAN"].ToString();
                                obj.Data.Thongtin_NNT.NGAY_GUI = dsCT.Tables[0].Rows[0]["NGAY_GUI"].ToString();
                                obj.Data.Thongtin_NNT.TRANG_THAI = dsCT.Tables[0].Rows[0]["TRANG_THAI"].ToString();
                                obj.Data.Thongtin_NNT.TEN_TRANG_THAI = dsCT.Tables[0].Rows[0]["TEN_TRANG_THAI"].ToString();
                                //lay ra danh sach tai khoan
                                obj.Data.Thongtin_NNT.TTIN_TKHOAN = new List<MSG06006.TTIN_TKHOAN>();
                                strSQL = "select * from ntdt_map_mst_tknh a where a.nntdt_id='" + dsCT.Tables[0].Rows[0]["nntdt_id"].ToString() + "'";
                                DataSet dsDTL = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                if (dsDTL != null)
                                    if (dsDTL.Tables.Count > 0)
                                        if (dsDTL.Tables[0].Rows.Count > 0)
                                        {
                                            for (int i = 0; i < dsDTL.Tables[0].Rows.Count; i++)
                                            {
                                                MSG06006.TTIN_TKHOAN objDtl = new MSG06006.TTIN_TKHOAN();
                                                objDtl.STK = dsDTL.Tables[0].Rows[i]["so_tk_nh"].ToString();
                                                objDtl.TEN_TK = dsDTL.Tables[0].Rows[i]["accountname"].ToString();
                                                obj.Data.Thongtin_NNT.TTIN_TKHOAN.Add(objDtl);
                                            }
                                        }


                            }
                        }
                    }
                    if (string.IsNullOrEmpty(obj.Data.Thongtin_NNT.MST))
                    {
                        pv_StrErrCode = "91001";
                        pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode); //"Tax registration  does not exists";
                    }
                    else
                    {
                        pv_StrErrCode = "0";
                        pv_StrErrMSG = "Successfull";
                    }

                }
                else
                {
                    pv_StrErrCode = "91001";
                    //pv_StrErrMSG = "Tax registration  does not exists";
                    pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode);
                }

                if (pv_StrErrCode.Equals("0"))
                {
                    obj.Header.Product_Type = pv_StrProductType;
                    obj.Header.Request_ID = strREQID;
                    pv_StrRESTYPE = obj.Header.Message_Type;
                    pv_StrRESID = obj.Header.Transaction_ID;
                    strReSult = obj.MSG06006toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        private void prcMSG06007(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";

            try
            {
                //Lay ma so thue

                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSG06007.MSG06007 obj6007 = new MSG06007.MSG06007();
                obj6007 = obj6007.MSGToObject(strXMLTMP);
                string strNTDT_ID = "";
                pv_StrErrCode = "91003";
                pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode);
                if (obj6007 != null)
                {
                    //valid message
                    if (SexyLib.checkValidObj06007(obj6007, ref pv_StrErrCode, ref  pv_StrErrMSG))
                    {
                        string strSQL = "select * from tcs_dm_nntdt A WHERE A.trang_thai IN ('00')  AND A.MST='" + obj6007.Data.TBAO_TKHOAN_NH.MST + "'";
                        DataSet dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                        if (dsCT != null)
                            if (dsCT.Tables.Count > 0)
                                if (dsCT.Tables[0].Rows.Count > 0)
                                {
                                    strNTDT_ID = dsCT.Tables[0].Rows[0]["NNTDT_ID"].ToString();
                                    string strTrangThai = dsCT.Tables[0].Rows[0]["trang_thai"].ToString();
                                    //cap nhat lai thong tin tai khoan
                                    if (strTrangThai.Equals("00"))
                                        SexyLib.UPDATE_GDT_TK06007(obj6007, strNTDT_ID);
                                    Business.NTDT.NNTDT.infNNTDT objUpdate = new Business.NTDT.NNTDT.infNNTDT();
                                    SexyLib.SetDataToDB06007(ref objUpdate, strNTDT_ID);
                                    if (objUpdate != null)
                                    {
                                        //gui lenh di TCT
                                        ETAX_GDT.ProcessThongBaoDangKyNNTDT_NH_TTXL sendMSG = new ETAX_GDT.ProcessThongBaoDangKyNNTDT_NH_TTXL();

                                        sendMSG.sendMSG06007(objUpdate, ref pv_StrErrCode, ref pv_StrErrMSG);
                                        //neu thanh cong
                                        if (pv_StrErrCode.Equals("00"))
                                        {
                                            Business.NTDT.NNTDT.buNNTDT bu = new Business.NTDT.NNTDT.buNNTDT();
                                            //cap nhat trang thai ve OK
                                            if (bu.Update_TrangThai(objUpdate))
                                            {
                                                pv_StrErrCode = "0";
                                                pv_StrErrMSG = "Successfull";
                                                //build  vaf phan hoi 200
                                            }
                                        }
                                        else
                                        {
                                            pv_StrErrCode = "99999";
                                            pv_StrErrMSG = "System error when Inquiry Tax Info";
                                            log.Error("loi trong qua trinh xu ly 06007 API errcode[" + pv_StrErrCode + "]msg[" + pv_StrErrMSG + "]");

                                        }
                                    }

                                }
                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                }
                //phan hoi 200
                if (pv_StrErrCode.Equals("0"))
                {
                    MSG200.MSG200 obj200 = new MSG200.MSG200();
                    obj200.Header.Request_ID = strREQID;
                    obj200.Data = new MSG200.Data200();
                    obj200.Data.So_TN_CT = strNTDT_ID;
                    obj200.Data.Ngay_TN_CT = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG200toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }

        private void prcMSG06013(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";

            try
            {
                //Lay ma so thue

                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSG06013.MSG06013 obj6007 = new MSG06013.MSG06013();
                obj6007 = obj6007.MSGToObject(strXMLTMP);
                string strNTDT_ID = "";
                pv_StrErrCode = "91003";
                pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode);
                if (obj6007 != null)
                {
                    //valid message
                    if (SexyLib.checkValidObj06013(obj6007, ref pv_StrErrCode, ref  pv_StrErrMSG))
                    {

                        string strSQL = "select * from tcs_dm_nntdt A WHERE A.trang_thai IN ('03')  AND A.MST='" + obj6007.Data.TBAO_TKHOAN_NH.MST + "'";
                        DataSet dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                        if (dsCT != null)
                            if (dsCT.Tables.Count > 0)
                                if (dsCT.Tables[0].Rows.Count > 0)
                                {
                                    strNTDT_ID = dsCT.Tables[0].Rows[0]["NNTDT_ID"].ToString();
                                    string strTrangThai = dsCT.Tables[0].Rows[0]["trang_thai"].ToString();
                                    //cap nhat lai thong tin tai khoan
                                    if (strTrangThai.Equals("03"))
                                        SexyLib.UPDATE_GDT_TK06013(obj6007, strNTDT_ID);
                                    Business.NTDT.NNTDT.infNNTDT objUpdate = new Business.NTDT.NNTDT.infNNTDT();
                                    SexyLib.SetDataToDB06013(ref objUpdate, strNTDT_ID);
                                    if (objUpdate != null)
                                    {
                                        //gui lenh di TCT
                                        ETAX_GDT.ProcessThayDoiTTTK_NH_TTXL sendMSG = new ETAX_GDT.ProcessThayDoiTTTK_NH_TTXL();
                                        sendMSG.sendmsg06013(objUpdate, ref pv_StrErrCode, ref pv_StrErrMSG);
                                        //neu thanh cong
                                        if (pv_StrErrCode.Equals("00"))
                                        {
                                            Business.NTDT.NNTDT.buNNTDT bu = new Business.NTDT.NNTDT.buNNTDT();
                                            //cap nhat trang thai ve OK
                                            if (bu.Update_TrangThai(objUpdate))
                                            {
                                                pv_StrErrCode = "0";
                                                pv_StrErrMSG = "Successfull";
                                                //build  vaf phan hoi 200
                                            }
                                        }
                                        else
                                        {
                                            pv_StrErrCode = "99999";
                                            pv_StrErrMSG = "System error when Inquiry Tax Info";
                                            log.Error("loi trong qua trinh xu ly 06007 API errcode[" + pv_StrErrCode + "]msg[" + pv_StrErrMSG + "]");

                                        }
                                    }
                                }
                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                }
                //phan hoi 200
                if (pv_StrErrCode.Equals("0"))
                {
                    MSG200.MSG200 obj200 = new MSG200.MSG200();
                    obj200.Header.Request_ID = strREQID;
                    obj200.Data = new MSG200.Data200();
                    obj200.Data.So_TN_CT = strNTDT_ID;
                    obj200.Data.Ngay_TN_CT = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG200toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorNumber = "99999";
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        private void prcMSG310(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";
            //      string strREQID = "";
            try
            {
                //Lay ma so thue
                string strMST = "";
                XmlDocument vdoc = new XmlDocument();
                vdoc.LoadXml(pv_strXML);
                strMST = vdoc.SelectSingleNode("ETAX/Data/MST").InnerText;
                eTAXAPILib.MSG311.MSG311 obj = new MSG311.MSG311();
                obj.Data.DKY_NTHUE = new List<MSG311.DKY_NTHUE>();
                //   log.Info(" Start getMSG6005API  MST = " + strMST);
                pv_StrErrCode = "91001";
                pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode); //"Tax registration  does not exists";
                //gan luon ko tim thay

                string strSQL = "SELECT * FROM (";
                strSQL += "  SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,   ";
                strSQL += " A.HO_TEN, to_char( A.NGAYSINH,'YYYY-MM-DD') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,   ";
                strSQL += " A.EMAIL, A.SERIALNUMBER, A.NOICAP,to_char( A.NGAY_HL,'YYYY-MM-DD HH24:MI:SS') NGAY_HL,   ";
                strSQL += " to_char( A.NGAY_HHL,'YYYY-MM-DD HH24:MI:SS') NGAY_HHL,(case when a.trangthai in ('08','09','10','11','12') then a.cur_taikhoan_th else A.TAIKHOAN_TH end)  TAIKHOAN_TH,   ";
                strSQL += " (case when a.trangthai in ('08','09','10','11','12') then a.cur_ten_taikhoan_th else A.TEN_TAIKHOAN_TH end) TEN_TAIKHOAN_TH, A.TRANGTHAI,   ";
                strSQL += " (select max(mota) from tcs_dm_trangthai_yeucau b where b.trang_thai=a.TRANGTHAI) TEN_TRANG_THAI   ";
                strSQL += " FROM tcs_dm_ntdt_hq_311 a where A.LOAI_HS in ('1','2') AND A.trangthai <>'13' AND A.MA_DV='" + strMST + "'    ";
                strSQL += " union all     ";
                strSQL += " SELECT A.ID,A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,   ";
                strSQL += " A.HO_TEN, to_char( A.NGAYSINH,'YYYY-MM-DD') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,   ";
                strSQL += " A.EMAIL, A.SERIALNUMBER, A.NOICAP,to_char( A.NGAY_HL,'YYYY-MM-DD HH24:MI:SS') NGAY_HL,   ";
                strSQL += " to_char( A.NGAY_HHL,'YYYY-MM-DD HH24:MI:SS') NGAY_HHL,(case when a.trangthai in ('08','09','10','11','12') then a.cur_taikhoan_th else A.TAIKHOAN_TH end)  TAIKHOAN_TH,   ";
                strSQL += " (case when a.trangthai in ('08','09','10','11','12') then a.cur_ten_taikhoan_th else A.TEN_TAIKHOAN_TH end) TEN_TAIKHOAN_TH, A.TRANGTHAI,   ";
                strSQL += " (select max(mota) from tcs_dm_trangthai_yeucau b where b.trang_thai=a.TRANGTHAI) TEN_TRANG_THAI   ";
                strSQL += " FROM tcs_dm_ntdt_hq a where A.LOAI_HS in ('1','2')  and a.trangthai='13'  AND A.MA_DV='" + strMST + "' ";
                strSQL += " ) ORDER BY ID DESC";
                DataSet dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);

                if (dsCT != null)
                {
                    if (dsCT.Tables.Count > 0)
                    {
                        if (dsCT.Tables[0].Rows.Count > 0)
                        {
                            //strMST = dsCT.Tables[0].Rows[0]["ma_nnthue"].ToString();
                            //map header
                            obj.Data.DKY_NTHUE = new List<MSG311.DKY_NTHUE>();
                            foreach (DataRow vrow in dsCT.Tables[0].Rows)
                            {
                                MSG311.DKY_NTHUE objDtl = new MSG311.DKY_NTHUE();
                                objDtl.So_HS = vrow["So_HS"].ToString();
                                objDtl.Loai_HS = vrow["Loai_HS"].ToString();
                                objDtl.Ma_DV = vrow["Ma_DV"].ToString();
                                objDtl.Ten_DV = vrow["Ten_DV"].ToString();
                                objDtl.DiaChi = vrow["DiaChi"].ToString();
                                objDtl.TRANG_THAI = vrow["TRANGTHAI"].ToString();
                                objDtl.TEN_TRANG_THAI = vrow["TEN_TRANG_THAI"].ToString();
                                objDtl.ThongTin_NNT = new MSG311.ThongTin_NNT();
                                objDtl.ThongTin_NNT.So_CMT = vrow["So_CMT"].ToString();
                                objDtl.ThongTin_NNT.Ho_Ten = vrow["Ho_Ten"].ToString();
                                objDtl.ThongTin_NNT.NgaySinh = vrow["NgaySinh"].ToString();
                                objDtl.ThongTin_NNT.NguyenQuan = vrow["NguyenQuan"].ToString();
                                //thong tin lien he
                                objDtl.ThongTin_NNT.ThongTinLienHe = new List<MSG311.ThongTinLienHe>();
                                StringReader strReader = new StringReader("<data>" + vrow["THONGTINLIENHE"].ToString() + "</data>");
                                DataSet dsTMP = new DataSet();
                                dsTMP.ReadXml(strReader);
                                if (dsTMP.Tables.Count > 0)
                                    if (dsTMP.Tables[0].Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dsTMP.Tables[0].Rows.Count; i++)
                                        {
                                            MSG311.ThongTinLienHe item = new MSG311.ThongTinLienHe();
                                            item.Email = dsTMP.Tables[0].Rows[i]["Email"].ToString();
                                            item.So_DT = dsTMP.Tables[0].Rows[i]["So_DT"].ToString();
                                            objDtl.ThongTin_NNT.ThongTinLienHe.Add(item);
                                        }
                                    }
                                //chung thu so
                                objDtl.ThongTin_NNT.ChungThuSo = new MSG311.ChungThuSo();
                                objDtl.ThongTin_NNT.ChungThuSo.SerialNumber = vrow["SerialNumber"].ToString();
                                objDtl.ThongTin_NNT.ChungThuSo.Noi_Cap = vrow["NoiCap"].ToString();
                                objDtl.ThongTin_NNT.ChungThuSo.Ngay_HL = vrow["Ngay_HL"].ToString();
                                objDtl.ThongTin_NNT.ChungThuSo.Ngay_HHL = vrow["Ngay_HHL"].ToString();

                                objDtl.ThongTinTaiKhoan = new MSG311.ThongTinTaiKhoan();
                                objDtl.ThongTinTaiKhoan.TaiKhoan_TH = vrow["TaiKhoan_TH"].ToString();
                                objDtl.ThongTinTaiKhoan.Ten_TaiKhoan_TH = vrow["Ten_TaiKhoan_TH"].ToString();
                                obj.Data.DKY_NTHUE.Add(objDtl);
                            }
                            pv_StrErrCode = "0";
                            pv_StrErrMSG = "Successfull";

                        }
                    }
                }



                if (pv_StrErrCode.Equals("0"))
                {
                    obj.Header.Product_Type = pv_StrProductType;
                    obj.Header.Request_ID = strREQID;
                    pv_StrRESTYPE = obj.Header.Message_Type;
                    pv_StrRESID = obj.Header.Transaction_ID;
                    strReSult = obj.MSG311toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        private void prcMSG312(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";
            //      string strREQID = "";
            try
            {
                //Lay ma so thue
                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSG312.MSG312 obj312 = new MSG312.MSG312();
                obj312 = obj312.MSGToObject(strXMLTMP);
                string strNTDT_ID = "";
                pv_StrErrCode = "91003";
                pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode);
                //gan luon ko tim thay
                string strSoTN = "";
                string strID = "";
                if (obj312 != null)
                {
                    //valid message
                    if (SexyLib.checkValidObj312(obj312, ref pv_StrErrCode, ref  pv_StrErrMSG))
                    {
                        string strSQL = " SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,   ";
                        strSQL += " A.HO_TEN, to_char( A.NGAYSINH,'YYYY-MM-DD') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,   ";
                        strSQL += " A.EMAIL, A.SERIALNUMBER, A.NOICAP,to_char( A.NGAY_HL,'YYYY-MM-DD HH24:MI:SS') NGAY_HL,   ";
                        strSQL += " to_char( A.NGAY_HHL,'YYYY-MM-DD HH24:MI:SS') NGAY_HHL,(case when a.trangthai in ('08','09','10','11','12') then a.cur_taikhoan_th else A.TAIKHOAN_TH end)  TAIKHOAN_TH,   ";
                        strSQL += " (case when a.trangthai in ('08','09','10','11','12') then a.cur_ten_taikhoan_th else A.TEN_TAIKHOAN_TH end) TEN_TAIKHOAN_TH, A.TRANGTHAI,   ";
                        strSQL += " (select max(mota) from tcs_dm_trangthai_yeucau b where b.trang_thai=a.TRANGTHAI) TEN_TRANG_THAI   ";
                        strSQL += " FROM tcs_dm_ntdt_hq_311 a where A.LOAI_HS in ('1') AND A.trangthai ='04' AND A.So_HS='" + obj312.Data.So_HS + "'  and a.MA_DV='" + obj312.Data.Ma_DV + "'   ";

                        DataSet dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);

                        if (dsCT != null)
                        {
                            if (dsCT.Tables.Count > 0)
                            {
                                if (dsCT.Tables[0].Rows.Count > 0)
                                {
                                    strID = dsCT.Tables[0].Rows[0]["ID"].ToString();
                                    string strUsername = SexyLib.getUserName(obj312.Header.UserID);
                                    if (strUsername.Length <= 0)
                                    {
                                        strUsername = obj312.Header.Product_Type;
                                    }
                                    //mac dinh ko dc sua thong tin nay nen tam gan = "" 
                                    string strThongTinLineHe = "";
                                    //chuyen kiem soat
                                    Decimal decOk = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG312(strID, SexyLib.convertDateHQ(obj312.Data.Ngay_HL),
                                       obj312.Data.ThongTinTaiKhoan.TaiKhoan_TH, obj312.Data.ThongTinTaiKhoan.Ten_TaiKhoan_TH, strThongTinLineHe, strUsername,
                                        obj312.Data.ThongTinTaiKhoan.CIFNO, obj312.Data.ThongTinTaiKhoan.BRANCH_CODE, obj312.Header.UserID);
                                    if (decOk != Business_HQ.Common.mdlCommon.TCS_HQ247_OK)
                                    {
                                        pv_StrErrCode = decOk.ToString();
                                        pv_StrErrMSG = Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString());
                                    }
                                    //kiem soat lenh
                                    if (decOk == Business_HQ.Common.mdlCommon.TCS_HQ247_OK)
                                    {
                                        //kiem tra lai tran thai
                                        decOk = Business_HQ.HQ247.TCS_DM_NTDT_HQ.CheckerMSG312(strID, "", "1", strUsername);
                                        if (decOk != Business_HQ.Common.mdlCommon.TCS_HQ247_OK)
                                        {
                                            pv_StrErrCode = decOk.ToString();
                                            pv_StrErrMSG = Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString());
                                        }
                                    }
                                    if (decOk == Business_HQ.Common.mdlCommon.TCS_HQ247_OK)
                                    {
                                        decOk = CustomsServiceV3.ProcessMSG.sendMSG312("311", "", strID, strUsername, ref pv_StrErrCode, ref pv_StrErrMSG);
                                        if (decOk == Business_HQ.Common.mdlCommon.TCS_HQ247_OK)
                                        {
                                            pv_StrErrCode = "0";
                                            pv_StrErrMSG = "Successfull";
                                        }

                                    }
                                }
                            }
                        }
                    }
                }


                if (pv_StrErrCode.Equals("0"))
                {
                    MSG200.MSG200 obj200 = new MSG200.MSG200();
                    obj200.Header.Request_ID = strREQID;
                    obj200.Data = new MSG200.Data200();
                    obj200.Data.So_TN_CT = strID;
                    obj200.Data.Ngay_TN_CT = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG200toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorNumber = "99999";
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }


        private void prcMSG400(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";
            //      string strREQID = "";
            try
            {
                //Lay ma so thue
                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSG400.MSG400 obj312 = new MSG400.MSG400();
                obj312 = obj312.MSGToObject(strXMLTMP);
                string strNTDT_ID = "";
                pv_StrErrCode = "91004";
                pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode);
                //gan luon ko tim thay
                string strSoTN = "";
                string strID = "";
                if (obj312 != null)
                {
                    //lenh thue noi dia
                    if (obj312.Data.LOAI_THUE.Equals("ND"))
                    {
                        string strSQL = "SELECT HDR.*,(SELECT MAX(B.MOTA) FROM tcs_dm_trangthai_ntdt B  WHERE GHICHU='ND' AND B.TRANG_THAI=HDR.TRANG_THAI) TEN_TRANG_THAI FROM (SELECT A.SO_CT,A.NGAY_KB,A.MA_NV,A.MA_DTHU, A.MA_NNTHUE MST, " +
                                            " A.TEN_NNTHUE TEN_NNT, " +
                                            " A.DC_NNTHUE DIACHI_NNT, " +
                                            " A.MA_NNTIEN MST_NNTHAY, " +
                                            " A.TEN_NNTIEN TEN_NNTHAY, " +
                                            " A.DC_NNTIEN DIACHI_NNTHAY, " +
                                            " A.TK_CO SO_TK_NHKBNN, " +
                                            " A.SHKB SH_KBNN, " +
                                            " A.MA_CQTHU CQ_QLY_THU, " +
                                            " A.SO_CT_NH SO_CHUNGTU, " +
                                            " TO_CHAR(A.NGAY_KH_NH,'YYYY-MM-DD' ) NGAY_CHUNGTU, " +
                                            " A.MA_NTK TINHCHAT_KHOAN_NOP, " +
                                            " A.CQ_QD CQUAN_THAMQUYEN, " +
                                            " A.MA_NH_B MA_NHTM, " +
                                            " A.MA_NT LOAI_TIEN, " +
                                            " A.PT_TT MA_HTHUC_NOP, " +
                                            " (SELECT MAX(B.TEN_PTTT) FROM TCS_DM_PTTT B WHERE STATUS='1') DIENGIAI_HTHUC_NOP, " +
                                            " A.LOAI_NNT LHINH_NNT, " +
                                            " A.SO_QD SO_QUYET_DINH, " +
                                            " A.NGAY_QD NGAY_QDINH, " +
                                            " TK_KH_NH, " +
                                            " A.MA_NH_A MA_NH_CHUYEN, " +
                                            " A.TTIEN TTIEN_NOPTHUE, " +
                                            " A.PHI_GD FEEAMT, " +
                                            " A.PHI_VAT VATAMT,(CASE WHEN A.TRANG_THAI='01' AND A.TT_CTHUE='0' THEN '02' " +
                                            " WHEN A.TRANG_THAI='01' AND A.TT_CTHUE='1' THEN '01' ELSE A.TRANG_THAI END) TRANG_THAI FROM TCS_CTU_HDR A WHERE A.MA_LTHUE<>'04' AND (A.MA_NNTHUE='" + obj312.Data.MST + "' OR A.MA_NNTIEN='" + obj312.Data.MST + "' ) AND A.SO_CT_NH='" + obj312.Data.SO_CHUNGTU + "' ) HDR order by ngay_kb desc "
                                            ;
                        DataSet dsCTUND = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                        if (dsCTUND != null)
                            if (dsCTUND.Tables.Count > 0)
                                if (dsCTUND.Tables[0].Rows.Count > 0)
                                {
                                    MSG401a.MSG401a obj401A = new MSG401a.MSG401a();
                                    obj401A.Data.CHUNGTU = new MSG401a.CHUNGTU();
                                    obj401A.Data.CHUNGTU.MST = dsCTUND.Tables[0].Rows[0]["MST"].ToString();
                                    obj401A.Data.CHUNGTU.TEN_NNT = dsCTUND.Tables[0].Rows[0]["TEN_NNT"].ToString();
                                    obj401A.Data.CHUNGTU.DIACHI_NNT = dsCTUND.Tables[0].Rows[0]["DIACHI_NNT"].ToString();
                                    obj401A.Data.CHUNGTU.MST_NNTHAY = dsCTUND.Tables[0].Rows[0]["MST_NNTHAY"].ToString();
                                    obj401A.Data.CHUNGTU.TEN_NNTHAY = dsCTUND.Tables[0].Rows[0]["TEN_NNTHAY"].ToString();
                                    obj401A.Data.CHUNGTU.DIACHI_NNTHAY = dsCTUND.Tables[0].Rows[0]["DIACHI_NNTHAY"].ToString();
                                    obj401A.Data.CHUNGTU.SO_TK_NHKBNN = dsCTUND.Tables[0].Rows[0]["SO_TK_NHKBNN"].ToString();
                                    obj401A.Data.CHUNGTU.SH_KBNN = dsCTUND.Tables[0].Rows[0]["SH_KBNN"].ToString();
                                    obj401A.Data.CHUNGTU.CQ_QLY_THU = dsCTUND.Tables[0].Rows[0]["CQ_QLY_THU"].ToString();
                                    obj401A.Data.CHUNGTU.SO_CHUNGTU = dsCTUND.Tables[0].Rows[0]["SO_CHUNGTU"].ToString();
                                    obj401A.Data.CHUNGTU.NGAY_CHUNGTU = dsCTUND.Tables[0].Rows[0]["NGAY_CHUNGTU"].ToString();
                                    obj401A.Data.CHUNGTU.TINHCHAT_KHOAN_NOP = dsCTUND.Tables[0].Rows[0]["TINHCHAT_KHOAN_NOP"].ToString();
                                    obj401A.Data.CHUNGTU.CQUAN_THAMQUYEN = dsCTUND.Tables[0].Rows[0]["CQUAN_THAMQUYEN"].ToString();
                                    obj401A.Data.CHUNGTU.MA_NHTM = dsCTUND.Tables[0].Rows[0]["MA_NHTM"].ToString();
                                    obj401A.Data.CHUNGTU.LOAI_TIEN = dsCTUND.Tables[0].Rows[0]["LOAI_TIEN"].ToString();
                                    obj401A.Data.CHUNGTU.MA_HTHUC_NOP = dsCTUND.Tables[0].Rows[0]["MA_HTHUC_NOP"].ToString();
                                    obj401A.Data.CHUNGTU.DIENGIAI_HTHUC_NOP = dsCTUND.Tables[0].Rows[0]["DIENGIAI_HTHUC_NOP"].ToString();
                                    obj401A.Data.CHUNGTU.LHINH_NNT = dsCTUND.Tables[0].Rows[0]["LHINH_NNT"].ToString();
                                    obj401A.Data.CHUNGTU.SO_QUYET_DINH = dsCTUND.Tables[0].Rows[0]["SO_QUYET_DINH"].ToString();
                                    obj401A.Data.CHUNGTU.NGAY_QDINH = dsCTUND.Tables[0].Rows[0]["NGAY_QDINH"].ToString();
                                    obj401A.Data.CHUNGTU.TK_KH_NH = dsCTUND.Tables[0].Rows[0]["TK_KH_NH"].ToString();
                                    obj401A.Data.CHUNGTU.MA_NH_CHUYEN = dsCTUND.Tables[0].Rows[0]["MA_NH_CHUYEN"].ToString();
                                    obj401A.Data.CHUNGTU.TTIEN_NOPTHUE = dsCTUND.Tables[0].Rows[0]["TTIEN_NOPTHUE"].ToString();
                                    obj401A.Data.CHUNGTU.FEEAMT = dsCTUND.Tables[0].Rows[0]["FEEAMT"].ToString();
                                    obj401A.Data.CHUNGTU.VATAMT = dsCTUND.Tables[0].Rows[0]["VATAMT"].ToString();
                                    obj401A.Data.CHUNGTU.TRANG_THAI = dsCTUND.Tables[0].Rows[0]["TRANG_THAI"].ToString();
                                    obj401A.Data.CHUNGTU.TEN_TRANG_THAI = dsCTUND.Tables[0].Rows[0]["TEN_TRANG_THAI"].ToString();
                                    obj401A.Data.CHUNGTU.CHUNGTU_CHITIET = new List<MSG401a.CHUNGTU_CHITIET>();
                                    //doan nay lay dtl
                                    strSQL = "select dtl.ma_chuong CHUONG,dtl.ma_tmuc TIEUMUC,dtl.noi_dung THONGTIN_KHOANNOP,dtl.sotien SO_TIEN, " +
                                            "dtl.ky_thue KY_THUE from tcs_ctu_dtl dtl where dtl.so_ct='" + dsCTUND.Tables[0].Rows[0]["SO_CT"].ToString() + "' ";
                                    DataSet dsDTL = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                    if (dsDTL != null)
                                        if (dsDTL.Tables.Count > 0)
                                            if (dsDTL.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow vrow in dsDTL.Tables[0].Rows)
                                                {
                                                    MSG401a.CHUNGTU_CHITIET objdtl = new MSG401a.CHUNGTU_CHITIET();
                                                    objdtl.CHUONG = vrow["CHUONG"].ToString();
                                                    objdtl.TIEUMUC = vrow["TIEUMUC"].ToString();
                                                    objdtl.THONGTIN_KHOANNOP = vrow["THONGTIN_KHOANNOP"].ToString();
                                                    objdtl.SO_TIEN = vrow["SO_TIEN"].ToString();
                                                    objdtl.KY_THUE = vrow["KY_THUE"].ToString();

                                                    obj401A.Data.CHUNGTU.CHUNGTU_CHITIET.Add(objdtl);
                                                }
                                            }
                                    obj401A.Header.Request_ID = strREQID;
                                    pv_StrRESTYPE = obj401A.Header.Message_Type;
                                    pv_StrRESID = obj401A.Header.Transaction_ID;
                                    obj401A.Header.Product_Type = pv_StrProductType;
                                    pv_StrErrCode = "0";
                                    pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode);
                                    pv_XMLOut = obj401A.MSG401atoXML();
                                    //exit function
                                    return;

                                }

                    }
                    //neu la thue HQ
                    if (obj312.Data.LOAI_THUE.Equals("HQ"))
                    {
                        StringBuilder sbsql = new StringBuilder();
                        sbsql.Append("SELECT HDR.*,(SELECT MAX(B.MOTA) FROM TCS_DM_TRANGTHAI_NTDT B  WHERE GHICHU='ND' AND B.TRANG_THAI=HDR.TRANG_THAI) TEN_TRANG_THAI");
                        sbsql.Append("  FROM ( SELECT A.MA_NH_B MA_NH_TH,");
                        sbsql.Append(" A.TEN_NH_B TEN_NH_TH,");
                        sbsql.Append("  A.MA_NNTHUE MA_DV,");
                        sbsql.Append(" (SELECT MAX(B.MA_CHUONG) FROM TCS_CTU_DTL B WHERE B.SO_CT=A.SO_CT) MA_CHUONG,");
                        sbsql.Append(" A.TEN_NNTHUE TEN_DV,");
                        sbsql.Append(" A.DC_NNTHUE DIACHI_DV,");
                        sbsql.Append(" A.SHKB MA_KB,");
                        sbsql.Append(" TCS_PCK_UTIL.FNC_GET_TEN_KBNN (A.SHKB) TEN_KB,");
                        sbsql.Append(" A.TK_CO TKKB,");
                        sbsql.Append(" A.MA_NTK,");
                        sbsql.Append(" A.MA_HQ_PH MA_HQ_PH,");
                        sbsql.Append(" A.TEN_HQ_PH TEN_HQ_PH,");
                        sbsql.Append(" A.MA_CQTHU MA_HQ_CQT,");
                        sbsql.Append(" A.SO_CT_NH SO_CT,");
                        sbsql.Append(" TO_CHAR (A.NGAY_BAONO, 'RRRR-MM-DD') NGAY_BN,");
                        sbsql.Append(" TO_CHAR (A.NGAY_BAOCO, 'RRRR-MM-DD') NGAY_BC,");
                        sbsql.Append(" TO_CHAR(TO_DATE(A.NGAY_CT,'YYYYMMDD'),'YYYY-MM-DD')  NGAY_CT,");
                        sbsql.Append(" A.MA_NT MA_NT,");
                        sbsql.Append(" A.TY_GIA TY_GIA,");
                        sbsql.Append(" A.TTIEN SOTIEN_TO,");
                        sbsql.Append(" A.REMARKS DIENGIAI,");
                        sbsql.Append(" A.TK_KH_NH     TK_KH_NH,");
                        sbsql.Append(" A.MA_NH_A MA_NH_CHUYEN,");
                        sbsql.Append(" A.PHI_GD FEEAMT,NGAY_KB, A.SO_CT SO_CTU, A.SO_TK,   A.LOAI_TT MA_LT,");
                        sbsql.Append(" A.PHI_VAT VATAMT,(CASE WHEN A.TRANG_THAI='01' AND A.TT_CTHUE='0' THEN '02'");
                        sbsql.Append(" WHEN A.TRANG_THAI='01' AND A.TT_CTHUE='1' THEN '01' ELSE A.TRANG_THAI END) TRANG_THAI");
                        sbsql.Append(" FROM TCS_CTU_HDR A  WHERE A.TRANG_THAI='01' AND A.MA_LTHUE='04' AND A.SO_CT_NH='" + obj312.Data.SO_CHUNGTU + "' ");
                        if (!string.IsNullOrEmpty(obj312.Data.MST))
                            sbsql.Append("  AND  (A.MA_NNTHUE='" + obj312.Data.MST + "' OR A.MA_NNTIEN='" + obj312.Data.MST + "' )");
                        sbsql.Append(" ) HDR ORDER BY NGAY_KB DESC");

                        //string strTen_NH_PH = System.Configuration.ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
                        DataSet dsCTUHQ = DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text);
                        if (dsCTUHQ != null)
                            if (dsCTUHQ.Tables.Count > 0)
                                if (dsCTUHQ.Tables[0].Rows.Count > 0)
                                {
                                    MSG401b.MSG401b obj401b = new MSG401b.MSG401b();
                                    obj401b.Data = new MSG401b.Data401a();

                                    obj401b.Data.Ma_NH_TH = dsCTUHQ.Tables[0].Rows[0]["MA_NH_TH"].ToString();
                                    obj401b.Data.Ten_NH_TH = dsCTUHQ.Tables[0].Rows[0]["Ten_NH_TH"].ToString();
                                    obj401b.Data.Ma_DV = dsCTUHQ.Tables[0].Rows[0]["Ma_DV"].ToString();
                                    obj401b.Data.Ma_Chuong = dsCTUHQ.Tables[0].Rows[0]["Ma_Chuong"].ToString();
                                    obj401b.Data.Ten_DV = dsCTUHQ.Tables[0].Rows[0]["Ten_DV"].ToString();
                                    obj401b.Data.DiaChi_DV = dsCTUHQ.Tables[0].Rows[0]["DiaChi_DV"].ToString();
                                    obj401b.Data.Ma_KB = dsCTUHQ.Tables[0].Rows[0]["Ma_KB"].ToString();
                                    obj401b.Data.Ten_KB = dsCTUHQ.Tables[0].Rows[0]["Ten_KB"].ToString();
                                    obj401b.Data.TKKB = dsCTUHQ.Tables[0].Rows[0]["TKKB"].ToString();
                                    obj401b.Data.Ma_NTK = dsCTUHQ.Tables[0].Rows[0]["Ma_NTK"].ToString();
                                    obj401b.Data.Ma_HQ_PH = dsCTUHQ.Tables[0].Rows[0]["Ma_HQ_PH"].ToString();
                                    obj401b.Data.Ten_HQ_PH = dsCTUHQ.Tables[0].Rows[0]["Ten_HQ_PH"].ToString();
                                    obj401b.Data.Ma_HQ_CQT = dsCTUHQ.Tables[0].Rows[0]["Ma_HQ_CQT"].ToString();
                                    obj401b.Data.So_CT = dsCTUHQ.Tables[0].Rows[0]["So_CT"].ToString();
                                    obj401b.Data.Ngay_BN = dsCTUHQ.Tables[0].Rows[0]["Ngay_BN"].ToString();
                                    obj401b.Data.Ngay_BC = dsCTUHQ.Tables[0].Rows[0]["Ngay_BC"].ToString();
                                    obj401b.Data.Ngay_CT = dsCTUHQ.Tables[0].Rows[0]["Ngay_CT"].ToString();
                                    obj401b.Data.Ma_NT = dsCTUHQ.Tables[0].Rows[0]["Ma_NT"].ToString();
                                    obj401b.Data.Ty_Gia = dsCTUHQ.Tables[0].Rows[0]["Ty_Gia"].ToString();
                                    obj401b.Data.SoTien_TO = dsCTUHQ.Tables[0].Rows[0]["SoTien_TO"].ToString();
                                    obj401b.Data.DienGiai = dsCTUHQ.Tables[0].Rows[0]["DienGiai"].ToString();
                                    obj401b.Data.TK_KH_NH = dsCTUHQ.Tables[0].Rows[0]["TK_KH_NH"].ToString();
                                    obj401b.Data.MA_NH_CHUYEN = dsCTUHQ.Tables[0].Rows[0]["MA_NH_CHUYEN"].ToString();
                                    obj401b.Data.FEEAMT = dsCTUHQ.Tables[0].Rows[0]["FEEAMT"].ToString();
                                    obj401b.Data.VATAMT = dsCTUHQ.Tables[0].Rows[0]["VATAMT"].ToString();
                                    obj401b.Data.TRANG_THAI = dsCTUHQ.Tables[0].Rows[0]["TRANG_THAI"].ToString();
                                    obj401b.Data.TEN_TRANG_THAI = dsCTUHQ.Tables[0].Rows[0]["TEN_TRANG_THAI"].ToString();


                                    // lấy ra mảng tờ khai va loại tiền, sau đó lấy chi tiết từng tờ khai đó
                                    string so_tk = dsCTUHQ.Tables[0].Rows[0]["so_tk"].ToString();
                                    string ma_loaitienthue = dsCTUHQ.Tables[0].Rows[0]["ma_lt"].ToString();
                                    string ty_gia = dsCTUHQ.Tables[0].Rows[0]["ty_gia"].ToString();
                                    string ma_nt = dsCTUHQ.Tables[0].Rows[0]["ma_nt"].ToString();
                                    string strSoCT = dsCTUHQ.Tables[0].Rows[0]["SO_CTU"].ToString();
                                    string[] tokhaiso_arr = so_tk.Split(';');

                                    //string[] loaitienthue_arr = ma_loaitienthue.Split(';');
                                    List<MSG401b.GNT_CT> listGNT = new List<MSG401b.GNT_CT>();

                                    //Lay thong tin detail
                                    for (int i = 0; i <= tokhaiso_arr.Length - 1; i++)
                                    {
                                        sbsql = new StringBuilder();
                                        sbsql.Append("SELECT   d.ky_thue,");
                                        sbsql.Append("         d.ma_chuong,");
                                        sbsql.Append("         d.ma_tmuc,");
                                        sbsql.Append("         d.noi_dung,");
                                        sbsql.Append("         d.tt_btoan,");
                                        sbsql.Append("         d.ma_hq,");
                                        sbsql.Append("         d.ma_lhxnk,");
                                        sbsql.Append("         to_char(d.ngay_tk,'yyyy') ngay_tk,");
                                        sbsql.Append("         d.so_tk,");
                                        sbsql.Append("         d.sac_thue,");
                                        sbsql.Append("         d.ma_lt,");
                                        sbsql.Append("         d.sotien_nt,");
                                        sbsql.Append("         d.sotien");
                                        sbsql.Append("  FROM   tcs_ctu_dtl d");
                                        sbsql.Append(" WHERE   (d.so_ct ='" + strSoCT + "')");
                                        sbsql.Append("          AND (d.so_tk ='" + tokhaiso_arr[i] + "')");

                                        DataSet ds = DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text);

                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                                        {

                                            List<MSG401b.ToKhai_CT> listToKhai = new List<MSG401b.ToKhai_CT>();
                                            MSG401b.GNT_CT GNT = new MSG401b.GNT_CT();
                                            GNT.TTButToan = ds.Tables[0].Rows[0]["tt_btoan"].ToString();
                                            GNT.So_TK = ds.Tables[0].Rows[0]["so_tk"].ToString();
                                            //                                            GNT.Nam_DK = ds.Tables[0].Rows[0]["ngay_tk"].ToString();
                                            GNT.Ma_HQ = ds.Tables[0].Rows[0]["ma_hq"].ToString();
                                            GNT.Ma_LH = ds.Tables[0].Rows[0]["ma_lhxnk"].ToString();
                                            GNT.Ma_LT = ds.Tables[0].Rows[0]["ma_lt"].ToString();

                                            // GNT.Ty_Gia = ty_gia;
                                            foreach (DataRow row in ds.Tables[0].Rows)
                                            {
                                                MSG401b.ToKhai_CT ToKhai_CT = new MSG401b.ToKhai_CT();
                                                ToKhai_CT.Ma_ST = row["sac_thue"].ToString();
                                                ToKhai_CT.NDKT = row["ma_tmuc"].ToString();
                                                ToKhai_CT.THONGTIN_KHOANNOP = row["noi_dung"].ToString();
                                                ToKhai_CT.SoTien_VND = row["sotien"].ToString();
                                                ToKhai_CT.SoTien_NT = row["sotien_nt"].ToString();
                                                listToKhai.Add(ToKhai_CT);
                                            }
                                            GNT.ToKhai_CT = listToKhai;
                                            listGNT.Add(GNT);
                                        }
                                        else
                                        {
                                            throw new Exception("Không tìm thấy chi tiết chứng từ!");
                                        }
                                    }
                                    obj401b.Data.GNT_CT = listGNT;
                                    obj401b.Header.Request_ID = strREQID;
                                    pv_StrRESTYPE = obj401b.Header.Message_Type;
                                    pv_StrRESID = obj401b.Header.Transaction_ID;
                                    obj401b.Header.Product_Type = pv_StrProductType;
                                    pv_StrErrCode = "0";
                                    pv_StrErrMSG = SexyLib.getErrorMSG(pv_StrErrCode);
                                    pv_XMLOut = obj401b.MSG401btoXML();
                                    //exit function
                                    return;
                                }


                    }

                }


                if (pv_StrErrCode.Equals("0"))
                {
                    MSG200.MSG200 obj200 = new MSG200.MSG200();
                    obj200.Header.Request_ID = strREQID;
                    obj200.Data = new MSG200.Data200();
                    obj200.Data.So_TN_CT = strID;
                    obj200.Data.Ngay_TN_CT = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG200toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorNumber = "99999";
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        /// <summary>
        /// ham lay so chung tu
        /// </summary>
        /// <param name="pv_strXML"></param>
        /// <param name="strREQID"></param>
        /// <param name="pv_StrProductType"></param>
        /// <param name="pv_XMLOut"></param>
        /// <param name="pv_StrRESID"></param>
        /// <param name="pv_StrRESTYPE"></param>
        /// <param name="pv_StrErrCode"></param>
        /// <param name="pv_StrErrMSG"></param>
        private void prcMSG100(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";

            try
            {
                //Lay ma so thue

                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSG21.MSG21 obj21 = new MSG21.MSG21();
                obj21 = obj21.MSGToObject(strXMLTMP);

                string strSo_CT = "";

                if (obj21 != null)
                {
                    strSo_CT = SexyLib.getSOCTU(obj21.Header.UserID);
                    pv_StrErrCode = "0";
                    pv_StrErrMSG = "Successful";
                    if (strSo_CT.Length < 10)
                    {
                        pv_StrErrCode = "99999";
                        pv_StrErrMSG = "System error when generate Voucher No";
                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                }
                //phan hoi 200
                if (pv_StrErrCode.Equals("0"))
                {
                    MSG200.MSG200 obj200 = new MSG200.MSG200();
                    obj200.Header.Request_ID = obj21.Header.Transaction_ID;
                    obj200.Data = new MSG200.Data200();
                    obj200.Data.So_TN_CT = strSo_CT;
                    obj200.Data.Ngay_TN_CT = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG200toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        private void prcMSG102(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";

            try
            {
                //Lay ma so thue

                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSG102.MSG102 obj21 = new MSG102.MSG102();
                obj21 = obj21.MSGToObject(strXMLTMP);
                string strMaCQTHU = "";
                string strTenCQTHU = "";
                string strMaKB = "";
                string strTenKB = "";
                string strSo_CT = "";
                pv_StrErrCode = "99022";
                pv_StrErrMSG = "Ma CQTHU khong dung hoac khong ton tai";
                if (obj21 != null)
                {
                    if (obj21.Data.Loai_DM.Equals("01"))
                    {
                        string strSQL = "SELECT * FROM TCS_DM_CQTHU A WHERE A.MA_CQTHU='" + obj21.Data.Ma_DM + "'";
                        DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                        if (ds != null)
                            if (ds.Tables.Count > 0)
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    pv_StrErrCode = "0";
                                    pv_StrErrMSG = "Successful";
                                    strMaCQTHU = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
                                    strTenCQTHU = ds.Tables[0].Rows[0]["ten"].ToString();
                                    strMaKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                                    strTenKB = SexyLib.getTEN_KBNN(strMaKB);
                                }

                    }
                    else
                    {
                        pv_StrErrCode = "99022";
                        pv_StrErrMSG = "Ma CQTHU khong dung hoac khong ton tai";
                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                }
                //phan hoi 200
                if (pv_StrErrCode.Equals("0"))
                {
                    MSG202.MSG202 obj200 = new MSG202.MSG202();
                    obj200.Header.Request_ID = obj21.Header.Transaction_ID;
                    obj200.Data = new MSG202.Data202();
                    obj200.Data.MA_CQTHU = strMaCQTHU;
                    obj200.Data.TEN_CQTHU = strTenCQTHU;
                    obj200.Data.MA_KB = strMaKB;
                    obj200.Data.TEN_KB = strTenKB;
                    string strSQL = "select shkb,ten,ma_tinh, ma_db from tcs_dm_khobac where shkb='" + strMaKB + "'";
                    DataSet dsKB = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                    if (dsKB != null)
                        if (dsKB.Tables.Count > 0)
                            if (dsKB.Tables[0].Rows.Count > 0)
                            {

                                obj200.Data.MA_DBHC_KBNN = dsKB.Tables[0].Rows[0]["ma_db"].ToString();
                                obj200.Data.MA_TINH_KBNN = dsKB.Tables[0].Rows[0]["ma_tinh"].ToString();
                            }
                    //doan nay lay thong tin ngan hang huong
                    strSQL = "select ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep from tcs_dm_nh_giantiep_tructiep where shkb='" + strMaKB + "' order by id ";
                    DataSet dsNH = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                    if (dsNH != null)
                        if (dsNH.Tables.Count > 0)
                            if (dsNH.Tables[0].Rows.Count > 0)
                            {
                                obj200.Data.MA_NH_TT = dsNH.Tables[0].Rows[0]["ma_tructiep"].ToString();
                                obj200.Data.TEN_NH_TT = dsNH.Tables[0].Rows[0]["ten_tructiep"].ToString();
                                obj200.Data.MA_NH_B = dsNH.Tables[0].Rows[0]["ma_giantiep"].ToString();
                                obj200.Data.TEN_NH_B = dsNH.Tables[0].Rows[0]["ten_giantiep"].ToString();

                            }
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG202toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }

        //thuế cá nhân, thuế trước bạ
        private void prcMSG128(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";
            //      string strREQID = "";
            try
            {
                //Lay ma so thue
                string strMST = "";
                XmlDocument vdoc = new XmlDocument();
                vdoc.LoadXml(pv_strXML);
                strMST = vdoc.SelectSingleNode("ETAX/Data/MST").InnerText;

                //   if(strLogMessage.Equals("Y"))
                //  log.Info(" Start getMSG05API  MST = " + strMST);

                string strXML129 = ProcMegService.ProMsg.getMSG00128API(strMST, "", "13");

                if (strXML129.Length > 0)
                {
                    ProcMegService.MSG.MSG129.MSG129 msg129 = new ProcMegService.MSG.MSG129.MSG129();

                    strXML129 = strXML129.Replace("<DATA>", SexyLib.strXMLdefin2);
                    ProcMegService.MSG.MSG129.MSG129 objTemp = msg129.MSGToObject(strXML129);
                    if (objTemp.Body.Row.Thongtin_NNT != null)
                    {
                        pv_StrErrCode = "0";
                        pv_StrErrMSG = "Successfull";
                        MSG00129.MSG00129 obj129 = new MSG00129.MSG00129();
                        obj129.Header.Request_ID = strREQID;
                        obj129.Header.Product_Type = pv_StrProductType;
                        if (objTemp.Body.Row.Thongtin_NNT.Thongtinchung != null)
                        {
                            if (objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt != null)
                            {
                                obj129.Data.Thongtin_NNT.Thongtinchung = new MSG00129.THONGTINCHUNG();
                                obj129.Data.Thongtin_NNT.Thongtinchung.row_nnt = new List<MSG00129.ROW_NNT>();
                                for (int i = 0; i < objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt.Count; i++)
                                {
                                    MSG00129.ROW_NNT objROW_NNT = new MSG00129.ROW_NNT();
                                    objROW_NNT.CHUONG = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].CHUONG;
                                    objROW_NNT.LOAI_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].LOAI_NNT;
                                    objROW_NNT.MA_CQT_QL = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].MA_CQT_QL;
                                    objROW_NNT.MST = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].MST;
                                    objROW_NNT.SO_CMND = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].SO_CMND;
                                    objROW_NNT.SO_CCCD = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].SO_CCCD;
                                    objROW_NNT.TEN_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].TEN_NNT;
                                    obj129.Data.Thongtin_NNT.Thongtinchung.row_nnt.Add(objROW_NNT);
                                }
                            }
                        }
                        if (objTemp.Body.Row.Thongtin_NNT.Diachi != null)
                        {
                            if (objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi != null)
                            {
                                obj129.Data.Thongtin_NNT.Diachi = new MSG00129.DIACHI();
                                obj129.Data.Thongtin_NNT.Diachi.Row_Diachi = new List<MSG00129.ROW_DIACHI>();
                                for (int i = 0; i < objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi.Count; i++)
                                {
                                    MSG00129.ROW_DIACHI objROW_DIACHI = new MSG00129.ROW_DIACHI();
                                    objROW_DIACHI.MA_HUYEN = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MA_HUYEN;
                                    objROW_DIACHI.MA_TINH = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MA_TINH;
                                    objROW_DIACHI.MA_XA = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MA_XA;
                                    objROW_DIACHI.MOTA_DIACHI = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MOTA_DIACHI;
                                    obj129.Data.Thongtin_NNT.Diachi.Row_Diachi.Add(objROW_DIACHI);
                                }

                            }
                        }
                        if (objTemp.Body.Row.Thongtin_NNT.Sothue != null)
                        {
                            if (objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue != null)
                            {
                                obj129.Data.Thongtin_NNT.Sothue = new MSG00129.SOTHUE();
                                obj129.Data.Thongtin_NNT.Sothue.Row_Sothue = new List<MSG00129.ROW_SOTHUE>();
                                for (int i = 0; i < objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue.Count; i++)
                                {
                                    MSG00129.ROW_SOTHUE objROW_SOTHUE = new MSG00129.ROW_SOTHUE();
                                    objROW_SOTHUE.MA_CHUONG = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_CHUONG;
                                    objROW_SOTHUE.MA_CQ_THU = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_CQ_THU;
                                    objROW_SOTHUE.MA_TMUC = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_TMUC;
                                    objROW_SOTHUE.NO_CUOI_KY = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].NO_CUOI_KY;
                                    objROW_SOTHUE.SO_TAI_KHOAN_CO = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SO_TAI_KHOAN_CO;
                                    objROW_SOTHUE.SO_QDINH = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SO_QDINH;
                                    objROW_SOTHUE.NGAY_QDINH = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].NGAY_QDINH;
                                    objROW_SOTHUE.TI_GIA = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].TI_GIA;
                                    objROW_SOTHUE.LOAI_TIEN = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].LOAI_TIEN;
                                    objROW_SOTHUE.LOAI_THUE = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].LOAI_THUE;
                                    objROW_SOTHUE.TEN_CQ_THU = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].TEN_CQ_THU;
                                    objROW_SOTHUE.MA_KB = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SHKB;
                                    //objROW_SOTHUE.TEN_KB = SexyLib.getTEN_KBNN(objROW_SOTHUE.MA_KB); ;
                                    objROW_SOTHUE.NOI_DUNG = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].TEN_TMUC;
                                    objROW_SOTHUE.MA_DBHC = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_DBHC;
                                    // doan nay hhiepvx add them cac phan lay them thong tin kho bac
                                    string strSQL = "select shkb,ten,ma_tinh, ma_db from tcs_dm_khobac where shkb='" + objROW_SOTHUE.MA_KB + "'";
                                    DataSet dsKB = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                    if (dsKB != null)
                                        if (dsKB.Tables.Count > 0)
                                            if (dsKB.Tables[0].Rows.Count > 0)
                                            {
                                                objROW_SOTHUE.TEN_KB = dsKB.Tables[0].Rows[0]["ten"].ToString();
                                                objROW_SOTHUE.MA_DBHC_KBNN = dsKB.Tables[0].Rows[0]["ma_db"].ToString();
                                                objROW_SOTHUE.MA_TINH_KBNN = dsKB.Tables[0].Rows[0]["ma_tinh"].ToString();
                                            }
                                    //doan nay lay thong tin ngan hang huong
                                    //strSQL = "select ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep from tcs_dm_nh_giantiep_tructiep where shkb='" + objROW_SOTHUE.MA_KB + "' order by id ";
                                    //DataSet dsNH = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                    //if (dsNH != null)
                                    //    if (dsNH.Tables.Count > 0)
                                    //        if (dsNH.Tables[0].Rows.Count > 0)
                                    //        {
                                    //            objROW_SOTHUE.MA_NH_TT = dsNH.Tables[0].Rows[0]["ma_tructiep"].ToString();
                                    //            objROW_SOTHUE.TEN_NH_TT = dsNH.Tables[0].Rows[0]["ten_tructiep"].ToString();
                                    //            objROW_SOTHUE.MA_NH_B = dsNH.Tables[0].Rows[0]["ma_giantiep"].ToString();
                                    //            objROW_SOTHUE.TEN_NH_B = dsNH.Tables[0].Rows[0]["ten_giantiep"].ToString();

                                    //        }
                                    string ma_tt = "";
                                    string ten_tt = "";
                                    string ma_gt = "";
                                    string ten_gt = "";
                                    GetNHGianTiepTrucTiep(objROW_SOTHUE.MA_KB, ref ma_tt, ref ten_tt, ref ma_gt, ref ten_gt);
                                    objROW_SOTHUE.MA_NH_TT = ma_tt;
                                    objROW_SOTHUE.TEN_NH_TT = ten_tt;
                                    objROW_SOTHUE.MA_NH_B = ma_gt;
                                    objROW_SOTHUE.TEN_NH_B = ten_gt;

                                    obj129.Data.Thongtin_NNT.Sothue.Row_Sothue.Add(objROW_SOTHUE);
                                }
                            }
                        }
                        obj129.Error = new ErrorEtax();
                        obj129.Error.ErrorNumber = pv_StrErrCode;
                        obj129.Error.ErrorMessage = pv_StrErrMSG;
                        strReSult = obj129.MSG00129toXML();
                        pv_StrRESID = obj129.Header.Transaction_ID;
                        pv_StrRESTYPE = obj129.Header.Message_Type;
                    }
                    else
                    {
                        pv_StrErrCode = "99999";
                        pv_StrErrMSG = "System error when Inquiry Tax Info";

                        strReSult = GetMSG299(strREQID, pv_StrProductType, pv_StrErrCode, pv_StrErrMSG);
                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                    strReSult = GetMSG299(strREQID, pv_StrProductType, pv_StrErrCode, pv_StrErrMSG);
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }

        //thuế trước bạ ô tô xe máy
        private void prcMSG00046(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";
            //      string strREQID = "";
            try
            {
                //Lay ma so thue
                string strMST = "";
                string so_tk = "";
                XmlDocument vdoc = new XmlDocument();
                vdoc.LoadXml(pv_strXML);
                strMST = vdoc.SelectSingleNode("ETAX/Data/MST").InnerText;
                so_tk = vdoc.SelectSingleNode("ETAX/Data/SO_TK").InnerText;
                //   if(strLogMessage.Equals("Y"))
                //  log.Info(" Start getMSG05API  MST = " + strMST);

                //int a = strMST.Trim().Length;
                //int b = so_tk.Trim().Length;
                if (strMST.Trim().Length > 0 || so_tk.Trim().Length > 0)
                {


                    string strXML047 = ProcMegService.ProMsg.getMSG046API(strMST, so_tk);

                    if (strXML047.Length > 0)
                    {
                        ProcMegService.MSG.MSG047.MSG047 msg047 = new ProcMegService.MSG.MSG047.MSG047();

                        strXML047 = strXML047.Replace("<DATA>", SexyLib.strXMLdefin2);
                        ProcMegService.MSG.MSG047.MSG047 objTemp = msg047.MSGToObject(strXML047);
                        if (objTemp.Body.Row.Thongtin_NNT != null)
                        {
                            pv_StrErrCode = "0";
                            pv_StrErrMSG = "Successfull";
                            MSG00047.MSG00047 obj047 = new MSG00047.MSG00047();
                            obj047.Header.Request_ID = strREQID;
                            obj047.Header.Product_Type = pv_StrProductType;
                            if (objTemp.Body.Row.Thongtin_NNT.Thongtinchung != null)
                            {
                                if (objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt != null)
                                {
                                    obj047.Data.Thongtin_NNT.Thongtinchung = new MSG00047.THONGTINCHUNG();
                                    obj047.Data.Thongtin_NNT.Thongtinchung.row_nnt = new List<MSG00047.ROW_NNT>();
                                    for (int i = 0; i < objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt.Count; i++)
                                    {
                                        MSG00047.ROW_NNT objROW_NNT = new MSG00047.ROW_NNT();
                                        objROW_NNT.CHUONG = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].CHUONG;
                                        objROW_NNT.LOAI_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].LOAI_NNT;
                                        objROW_NNT.MA_CQT_QL = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].MA_CQT_QL;
                                        objROW_NNT.MST = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].MST;
                                        objROW_NNT.SO_CMND = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].SO_CMND;
                                        objROW_NNT.TEN_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[i].TEN_NNT;
                                        obj047.Data.Thongtin_NNT.Thongtinchung.row_nnt.Add(objROW_NNT);
                                    }
                                }
                            }
                            if (objTemp.Body.Row.Thongtin_NNT.Diachi != null)
                            {
                                if (objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi != null)
                                {
                                    obj047.Data.Thongtin_NNT.Diachi = new MSG00047.DIACHI();
                                    obj047.Data.Thongtin_NNT.Diachi.Row_Diachi = new List<MSG00047.ROW_DIACHI>();
                                    for (int i = 0; i < objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi.Count; i++)
                                    {
                                        MSG00047.ROW_DIACHI objROW_DIACHI = new MSG00047.ROW_DIACHI();
                                        objROW_DIACHI.MA_HUYEN = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MA_HUYEN;
                                        objROW_DIACHI.MA_TINH = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MA_TINH;
                                        objROW_DIACHI.MA_XA = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MA_XA;
                                        objROW_DIACHI.MOTA_DIACHI = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[i].MOTA_DIACHI;
                                        obj047.Data.Thongtin_NNT.Diachi.Row_Diachi.Add(objROW_DIACHI);
                                    }

                                }
                            }
                            if (objTemp.Body.Row.Thongtin_NNT.Sothue != null)
                            {
                                if (objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue != null)
                                {
                                    obj047.Data.Thongtin_NNT.Sothue = new MSG00047.SOTHUE();
                                    obj047.Data.Thongtin_NNT.Sothue.Row_Sothue = new List<MSG00047.ROW_SOTHUE>();
                                    for (int i = 0; i < objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue.Count; i++)
                                    {
                                        MSG00047.ROW_SOTHUE objROW_SOTHUE = new MSG00047.ROW_SOTHUE();
                                        objROW_SOTHUE.MA_CHUONG = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_CHUONG;
                                        objROW_SOTHUE.MA_CQ_THU = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_CQ_THU;
                                        objROW_SOTHUE.MA_TMUC = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_TMUC;
                                        objROW_SOTHUE.NO_CUOI_KY = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].NO_CUOI_KY;
                                        objROW_SOTHUE.SO_TAI_KHOAN_CO = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SO_TAI_KHOAN_CO;
                                        objROW_SOTHUE.SO_QDINH = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SO_QDINH;
                                        objROW_SOTHUE.NGAY_QDINH = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].NGAY_QDINH;
                                        objROW_SOTHUE.TI_GIA = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].TI_GIA;
                                        objROW_SOTHUE.LOAI_TIEN = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].LOAI_TIEN;
                                        objROW_SOTHUE.LOAI_THUE = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].LOAI_THUE;
                                        objROW_SOTHUE.TEN_CQ_THU = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].TEN_CQ_THU;
                                        objROW_SOTHUE.MA_KB = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SHKB;
                                        //objROW_SOTHUE.TEN_KB = SexyLib.getTEN_KBNN(objROW_SOTHUE.MA_KB); ;
                                        objROW_SOTHUE.NOI_DUNG = SexyLib.getTEN_TMUC(objROW_SOTHUE.MA_TMUC);//objROW_SOTHUE.NOI_DUNG = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].TEN_TMUC;


                                        objROW_SOTHUE.MA_DBHC = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].MA_DBHC;
                                        objROW_SOTHUE.SO_KHUNG = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SO_KHUNG;
                                        objROW_SOTHUE.SO_MAY = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].SO_MAY;
                                        objROW_SOTHUE.KY_THUE = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].KY_THUE;
                                        objROW_SOTHUE.DAC_DIEM_PTIEN = objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue[i].DAC_DIEM_PTIEN;
                                        // doan nay hhiepvx add them cac phan lay them thong tin kho bac
                                        string strSQL = "select shkb,ten,ma_tinh, ma_db from tcs_dm_khobac where shkb='" + objROW_SOTHUE.MA_KB + "'";
                                        DataSet dsKB = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                        if (dsKB != null)
                                            if (dsKB.Tables.Count > 0)
                                                if (dsKB.Tables[0].Rows.Count > 0)
                                                {
                                                    objROW_SOTHUE.TEN_KB = dsKB.Tables[0].Rows[0]["ten"].ToString();
                                                    objROW_SOTHUE.MA_DBHC_KBNN = dsKB.Tables[0].Rows[0]["ma_db"].ToString();
                                                    objROW_SOTHUE.MA_TINH_KBNN = dsKB.Tables[0].Rows[0]["ma_tinh"].ToString();
                                                }
                                        //doan nay lay thong tin ngan hang huong
                                        //strSQL = "select ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep from tcs_dm_nh_giantiep_tructiep where shkb='" + objROW_SOTHUE.MA_KB + "' order by id ";
                                        //DataSet dsNH = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                        //if (dsNH != null)
                                        //    if (dsNH.Tables.Count > 0)
                                        //        if (dsNH.Tables[0].Rows.Count > 0)
                                        //        {
                                        //            objROW_SOTHUE.MA_NH_TT = dsNH.Tables[0].Rows[0]["ma_tructiep"].ToString();
                                        //            objROW_SOTHUE.TEN_NH_TT = dsNH.Tables[0].Rows[0]["ten_tructiep"].ToString();
                                        //            objROW_SOTHUE.MA_NH_B = dsNH.Tables[0].Rows[0]["ma_giantiep"].ToString();
                                        //            objROW_SOTHUE.TEN_NH_B = dsNH.Tables[0].Rows[0]["ten_giantiep"].ToString();

                                        //        }
                                        string ma_tt = "";
                                        string ten_tt = "";
                                        string ma_gt = "";
                                        string ten_gt = "";
                                        GetNHGianTiepTrucTiep(objROW_SOTHUE.MA_KB, ref ma_tt, ref ten_tt, ref ma_gt, ref ten_gt);
                                        objROW_SOTHUE.MA_NH_TT = ma_tt;
                                        objROW_SOTHUE.TEN_NH_TT = ten_tt;
                                        objROW_SOTHUE.MA_NH_B = ma_gt;
                                        objROW_SOTHUE.TEN_NH_B = ten_gt;

                                        obj047.Data.Thongtin_NNT.Sothue.Row_Sothue.Add(objROW_SOTHUE);
                                    }
                                }
                            }

                            obj047.Error = new ErrorEtax();

                            obj047.Error.ErrorNumber = pv_StrErrCode;
                            obj047.Error.ErrorMessage = pv_StrErrMSG;

                            strReSult = obj047.MSG00047toXML();
                            pv_StrRESID = obj047.Header.Transaction_ID;
                            pv_StrRESTYPE = obj047.Header.Message_Type;
                        }
                        else
                        {
                            pv_StrErrCode = "99999";
                            pv_StrErrMSG = "System error when Inquiry Tax Info";
                            strReSult = GetMSG299(strREQID, pv_StrProductType, pv_StrErrCode, pv_StrErrMSG);
                        }
                    }
                    else
                    {
                        pv_StrErrCode = "99999";
                        pv_StrErrMSG = "System error when Inquiry Tax Info";
                        strReSult = GetMSG299(strREQID, pv_StrProductType, pv_StrErrCode, pv_StrErrMSG);
                    }
                }
                else
                {
                    pv_StrErrCode = "94001";
                    pv_StrErrMSG = "Tax code or Declaration number can not be Null";
                    strReSult = GetMSG299(strREQID, pv_StrProductType, pv_StrErrCode, pv_StrErrMSG);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        //thuế trước bạ nhà đất
        private void prcMSG00109(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";
            //      string strREQID = "";
            try
            {
                //Lay ma so thue
                string strMST = "";
                string so = "";
                string ma_hs = "";
                string so_quyet_dinh = "";
                XmlDocument vdoc = new XmlDocument();
                vdoc.LoadXml(pv_strXML);
                strMST = vdoc.SelectSingleNode("ETAX/Data/MST").InnerText;
                //số giấy tờ
                so = vdoc.SelectSingleNode("ETAX/Data/SO").InnerText;
                ma_hs = vdoc.SelectSingleNode("ETAX/Data/MA_HS").InnerText;
                so_quyet_dinh = vdoc.SelectSingleNode("ETAX/Data/SO_QUYET_DINH").InnerText;

                if ((ma_hs.Trim().Length > 0 || so_quyet_dinh.Trim().Length > 0) && (strMST.Trim().Length > 0 && so.Trim().Length > 0))
                {
                    string strXML110 = ProcMegService.ProMsg.getMSG109API(ma_hs, so, so_quyet_dinh);

                    if (strXML110.Length > 0)
                    {
                        ProcMegService.MSG.MSG110.MSG110 msg110 = new ProcMegService.MSG.MSG110.MSG110();

                        strXML110 = strXML110.Replace("<DATA>", SexyLib.strXMLdefin2);
                        ProcMegService.MSG.MSG110.MSG110 objTemp = msg110.MSGToObject(strXML110);
                        if (objTemp.Body.Row.SoThue != null)
                        {
                            if (objTemp.Body.Row.SoThue.TRANGTHAI.Equals("01"))
                            {
                                if (objTemp.Body.Row.SoThue.row_tongtien != null)
                                {
                                    pv_StrErrCode = "0";
                                    pv_StrErrMSG = "Successfull";
                                    MSG00110.MSG00110 obj110 = new MSG00110.MSG00110();
                                    obj110.Header.Request_ID = strREQID;
                                    obj110.Header.Product_Type = pv_StrProductType;
                                    for (int i = 0; i < objTemp.Body.Row.SoThue.row_tongtien.Count; i++)
                                    {
                                        if (objTemp.Body.Row.SoThue.row_tongtien[i].MA_SO_THUE.Equals(strMST))
                                        {
                                            if (objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue != null)
                                            {
                                                obj110.Data.SoThue = new MSG00110.SOTHUE();
                                                obj110.Data.SoThue.Row_Sothue = new List<MSG00110.ROW_SOTHUE>();
                                                for (int j = 0; j < objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue.Count; j++)
                                                {
                                                    MSG00110.ROW_SOTHUE objRow_SoThue = new MSG00110.ROW_SOTHUE();
                                                    objRow_SoThue.MST = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_SO_THUE;
                                                    objRow_SoThue.TEN_NNT = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].TEN_NNT;
                                                    objRow_SoThue.MOTA_DIACHI = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MOTA_DIACHI;
                                                    objRow_SoThue.MA_TINH = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_TINH;
                                                    objRow_SoThue.TEN_TINH = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].TEN_TINH;
                                                    objRow_SoThue.MA_HUYEN = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_HUYEN;
                                                    objRow_SoThue.TEN_HUYEN = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].TEN_HUYEN;
                                                    objRow_SoThue.MA_XA = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_XA;
                                                    objRow_SoThue.MA_CHUONG = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_CHUONG;
                                                    objRow_SoThue.MA_CQ_THU = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_CQUAN_THU;
                                                    objRow_SoThue.MA_TMUC = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_TMUC;
                                                    objRow_SoThue.NO_CUOI_KY = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].SO_CON_PHAI_NOP;
                                                    objRow_SoThue.SO_TAI_KHOAN_CO = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].LOAI_TK_NSNN;
                                                    objRow_SoThue.SO_QDINH = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].SO_QUYET_DINH;
                                                    objRow_SoThue.NGAY_QDINH = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].NGAY_QUYET_DINH;
                                                    objRow_SoThue.TI_GIA = "";
                                                    objRow_SoThue.LOAI_TIEN = "VND";
                                                    objRow_SoThue.LOAI_THUE = "03";
                                                    objRow_SoThue.TEN_CQ_THU = SexyLib.getTEN_CQTHU(objRow_SoThue.MA_CQ_THU, ""); ;
                                                    objRow_SoThue.MA_KB = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].SHKB;
                                                    //objRow_SoThue.TEN_KB = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_SO_THUE;
                                                    objRow_SoThue.NOI_DUNG = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].TEN_TMUC;
                                                    objRow_SoThue.MA_DBHC = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_DBHC;
                                                    objRow_SoThue.DIA_CHI_TS = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].DIA_CHI_TS;
                                                    objRow_SoThue.MA_HS = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_HS;
                                                    objRow_SoThue.MA_GCN = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].MA_GCN;
                                                    objRow_SoThue.SO_THUADAT = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].SO_THUADAT;
                                                    objRow_SoThue.SO_TO_BANDO = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].SO_TO_BANDO;
                                                    objRow_SoThue.HAN_NOP = objTemp.Body.Row.SoThue.row_tongtien[i].Row_Sothue[j].HAN_NOP;
                                                    // doan nay hhiepvx add them cac phan lay them thong tin kho bac
                                                    string strSQL = "select shkb,ten,ma_tinh, ma_db from tcs_dm_khobac where shkb='" + objRow_SoThue.MA_KB + "'";
                                                    DataSet dsKB = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                                    if (dsKB != null)
                                                        if (dsKB.Tables.Count > 0)
                                                            if (dsKB.Tables[0].Rows.Count > 0)
                                                            {
                                                                objRow_SoThue.TEN_KB = dsKB.Tables[0].Rows[0]["ten"].ToString();
                                                                objRow_SoThue.MA_DBHC_KBNN = dsKB.Tables[0].Rows[0]["ma_db"].ToString();
                                                                objRow_SoThue.MA_TINH_KBNN = dsKB.Tables[0].Rows[0]["ma_tinh"].ToString();
                                                            }
                                                    //doan nay lay thong tin ngan hang huong
                                                    //strSQL = "select ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep from tcs_dm_nh_giantiep_tructiep where shkb='" + objRow_SoThue.MA_KB + "' order by id ";
                                                    //DataSet dsNH = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                                                    //if (dsNH != null)
                                                    //    if (dsNH.Tables.Count > 0)
                                                    //        if (dsNH.Tables[0].Rows.Count > 0)
                                                    //        {
                                                    //            objRow_SoThue.MA_NH_TT = dsNH.Tables[0].Rows[0]["ma_tructiep"].ToString();
                                                    //            objRow_SoThue.TEN_NH_TT = dsNH.Tables[0].Rows[0]["ten_tructiep"].ToString();
                                                    //            objRow_SoThue.MA_NH_B = dsNH.Tables[0].Rows[0]["ma_giantiep"].ToString();
                                                    //            objRow_SoThue.TEN_NH_B = dsNH.Tables[0].Rows[0]["ten_giantiep"].ToString();

                                                    //        }
                                                    string ma_tt = "";
                                                    string ten_tt = "";
                                                    string ma_gt = "";
                                                    string ten_gt = "";
                                                    GetNHGianTiepTrucTiep(objRow_SoThue.MA_KB, ref ma_tt, ref ten_tt, ref ma_gt, ref ten_gt);
                                                    objRow_SoThue.MA_NH_TT = ma_tt;
                                                    objRow_SoThue.TEN_NH_TT = ten_tt;
                                                    objRow_SoThue.MA_NH_B = ma_gt;
                                                    objRow_SoThue.TEN_NH_B = ten_gt;

                                                    obj110.Data.SoThue.Row_Sothue.Add(objRow_SoThue);
                                                }
                                            }
                                        }
                                    }
                                    obj110.Error = new ErrorEtax();
                                    obj110.Error.ErrorNumber = pv_StrErrCode;
                                    obj110.Error.ErrorMessage = pv_StrErrMSG;
                                    strReSult = obj110.MSG00110toXML();
                                    pv_StrRESID = obj110.Header.Transaction_ID;
                                    pv_StrRESTYPE = obj110.Header.Message_Type;
                                }

                            }
                            else
                            {
                                pv_StrErrCode = "94003";
                                pv_StrErrMSG = "File number and ID do not have Tax Info";
                                strReSult = GetMSG299(strREQID, pv_StrProductType, pv_StrErrCode, pv_StrErrMSG);
                            }
                        }
                        else
                        {
                            pv_StrErrCode = "99999";
                            pv_StrErrMSG = "System error when Inquiry Tax Info";
                            strReSult = GetMSG299(strREQID, pv_StrProductType, pv_StrErrCode, pv_StrErrMSG);
                        }
                    }
                    else
                    {
                        pv_StrErrCode = "99999";
                        pv_StrErrMSG = "System error when Inquiry Tax Info";
                        strReSult = GetMSG299(strREQID, pv_StrProductType, pv_StrErrCode, pv_StrErrMSG);
                    }
                }
                else
                {
                    pv_StrErrCode = "94002";
                    pv_StrErrMSG = "Tax code or File number or ID can not be Null";
                    strReSult = GetMSG299(strREQID, pv_StrProductType, pv_StrErrCode, pv_StrErrMSG);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }

        private void prcMSG022(string pv_strXML, string strREQID, string pv_StrProductType, ref string pv_XMLOut, ref string pv_StrRESID, ref string pv_StrRESTYPE, ref string pv_StrErrCode, ref string pv_StrErrMSG)
        {
            string strReSult = "";

            try
            {
                //Lay ma so thue

                string strXMLTMP = SexyLib.strXMLdefin5 + pv_strXML.Replace(SexyLib.strXMLdefin4, SexyLib.strXMLdefin3);
                eTAXAPILib.MSG22.MSG22 obj22 = new MSG22.MSG22();
                obj22 = obj22.MSGToObject(strXMLTMP);
                string strMST = "";
                string strSHKB = "";
                string strSo_CT = "";
                string strSo_BT = "";
                string strMa_NV = "";
                string strNgayKB = "";
                string strTrang_thai = "";
                string strmaCQT = "";
                string strTen_CQT = "";
                string strMa_NTK = "";
                if (obj22 != null)
                {
                    //valid message
                    if (SexyLib.checkValidObj22(obj22, ref pv_StrErrCode, ref  pv_StrErrMSG))
                    {
                        //ma_cqthu,ma_ntk 
                        string strSQL = "select * from tcs_ctu_hdr where so_ct_nh='" + obj22.Data.CHUNGTU.SO_REFCORE + "' and so_ct='" + obj22.Data.CHUNGTU.SO_CHUNGTU + "'";
                        DataSet dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                        if (dsCT != null)
                            if (dsCT.Tables.Count > 0)
                                if (dsCT.Tables[0].Rows.Count > 0)
                                {
                                    strMST = dsCT.Tables[0].Rows[0]["ma_nnthue"].ToString();
                                    strSHKB = dsCT.Tables[0].Rows[0]["shkb"].ToString();
                                    strSo_CT = dsCT.Tables[0].Rows[0]["so_ct"].ToString();
                                    strSo_BT = dsCT.Tables[0].Rows[0]["so_bt"].ToString();
                                    strMa_NV = dsCT.Tables[0].Rows[0]["ma_nv"].ToString();
                                    strNgayKB = dsCT.Tables[0].Rows[0]["ngay_kb"].ToString();
                                    strTrang_thai = dsCT.Tables[0].Rows[0]["trang_thai"].ToString();
                                    strmaCQT = dsCT.Tables[0].Rows[0]["ma_cqthu"].ToString();
                                    strMa_NTK = dsCT.Tables[0].Rows[0]["ma_ntk"].ToString();
                                    strTen_CQT = dsCT.Tables[0].Rows[0]["TEN_CQTHU"].ToString();
                                }
                        pv_StrErrCode = "0";
                        pv_StrErrMSG = "Successfull";
                        if (strMST.Length > 0)
                            if (!strMST.Equals(obj22.Data.CHUNGTU.MST))
                            {
                                pv_StrErrCode = "99997";
                                pv_StrErrMSG = "TAXNO or Voucher Info not same with the first message, pls contact admin system ";
                            }
                        if (pv_StrErrCode.Equals("0"))
                        {
                            if (strSo_CT.Length <= 0)
                            {
                                //khai bao cac object HDR va DTL
                                NewChungTu.infChungTuHDR objCTuHDR = new NewChungTu.infChungTuHDR();
                                List<NewChungTu.infChungTuDTL> objCTuDTL = new List<NewChungTu.infChungTuDTL>();
                                SexyLib.setObject22ToCTU(obj22, ref objCTuHDR, ref objCTuDTL);
                                //insert chung tu
                                SexyLib.Insert_New0021(objCTuHDR, objCTuDTL);
                                strMST = objCTuHDR.Ma_NNThue;
                                strSHKB = objCTuHDR.SHKB;// dsCT.Tables[0].Rows[0]["shkb"].ToString();
                                strSo_CT = objCTuHDR.So_CT;// dsCT.Tables[0].Rows[0]["so_ct"].ToString();
                                strSo_BT = objCTuHDR.So_BT.ToString();// dsCT.Tables[0].Rows[0]["so_bt"].ToString();
                                strMa_NV = objCTuHDR.Ma_NV.ToString();// dsCT.Tables[0].Rows[0]["ma_nv"].ToString();
                                strNgayKB = objCTuHDR.Ngay_KB.ToString();// dsCT.Tables[0].Rows[0]["ngay_kb"].ToString();
                                strTrang_thai = "07";
                            }
                            if (strTrang_thai.Equals("07"))
                            {
                                //gui thue
                                string errcode = "01";
                                string errdesc = "";

                                ProcMegService.ProMsg.SendMSG21_CN_TB(strMST, strSHKB, strSo_CT, strSo_BT, strMa_NV, "01", ref errcode, ref errdesc);
                                //duyet chung tu
                                //neu thanh cong  
                                if (errcode.Equals("01"))
                                {
                                    SexyLib.CTU_SetTrangThai2(strSo_CT, strSHKB, strMa_NV, strNgayKB, strSo_BT, "01", "", "", "", strMa_NV, "", "", "1", strMa_NTK, strTen_CQT);
                                    //"", "", "", "", "", "", "1","",true);
                                }
                                else
                                {
                                    pv_StrErrCode = "99999";
                                    pv_StrErrMSG = "[" + errcode + "]" + errdesc;
                                }
                            }


                        }

                    }
                }
                else
                {
                    pv_StrErrCode = "99999";
                    pv_StrErrMSG = "System error when Inquiry Tax Info";
                }
                //phan hoi 200
                if (pv_StrErrCode.Equals("0"))
                {
                    MSG200.MSG200 obj200 = new MSG200.MSG200();
                    obj200.Header.Request_ID = obj22.Header.Transaction_ID;
                    obj200.Data = new MSG200.Data200();
                    obj200.Data.So_TN_CT = strSo_CT;
                    obj200.Data.Ngay_TN_CT = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    pv_StrRESTYPE = obj200.Header.Message_Type;
                    pv_StrRESID = obj200.Header.Transaction_ID;
                    obj200.Header.Product_Type = pv_StrProductType;
                    obj200.Error.ErrorNumber = pv_StrErrCode;
                    obj200.Error.ErrorMessage = pv_StrErrMSG;
                    strReSult = obj200.MSG200toXML();
                }
                //phan hoi 299
                else
                {
                    MSG299 obj299 = new MSG299();
                    obj299.Header.Request_ID = strREQID;
                    obj299.Header.Product_Type = pv_StrProductType;
                    obj299.Error.ErrorMessage = pv_StrErrMSG;
                    if (obj299.Error.ErrorMessage.Length > 255)
                        obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                    pv_StrRESID = obj299.Header.Transaction_ID;
                    pv_StrRESTYPE = "299";
                    obj299.Error.ErrorNumber = pv_StrErrCode;
                    pv_StrErrMSG = obj299.Error.ErrorMessage;
                    strReSult = obj299.MSG299toXML();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorMessage = obj299.Error.ErrorMessage + ex.Message;
                if (obj299.Error.ErrorMessage.Length > 255)
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                pv_StrRESID = obj299.Header.Transaction_ID;
                pv_StrRESTYPE = "299";
                pv_StrErrCode = obj299.Error.ErrorNumber;
                pv_StrErrMSG = obj299.Error.ErrorMessage;
                strReSult = obj299.MSG299toXML();
            }
            pv_XMLOut = strReSult;
        }
        public static string GetMSG299(string strREQID, string pv_StrProductType, string ErrorNumber, string ErrorMessage)
        {
            string strReSult = "";
            try
            {
                MSG299 obj299 = new MSG299();
                obj299.Header.Request_ID = strREQID;
                obj299.Header.Product_Type = pv_StrProductType;
                obj299.Error.ErrorNumber = ErrorNumber;
                obj299.Error.ErrorMessage = ErrorMessage;
                if (obj299.Error.ErrorMessage.Length > 255)
                {
                    obj299.Error.ErrorMessage = obj299.Error.ErrorMessage.Substring(0, 255);
                }

                strReSult = obj299.MSG299toXML();
            }
            catch (Exception ex)
            {

            }

            return strReSult;
        }
        public static bool GetNHGianTiepTrucTiep(string shkb, ref string MA_TT, ref string TEN_TT, ref string MA_GT, ref string TEN_GT)
        {
            bool kq = false;
            string strOnTTSP = "";
            try
            {
                strOnTTSP = System.Configuration.ConfigurationSettings.AppSettings["ON_TTSP"].ToString();
            }
            catch (Exception exc)
            {
                strOnTTSP = "N";
            }
            string strSQL = "";
            if (strOnTTSP.Equals("Y"))
            {
                strSQL = "select ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep from tcs_dm_nh_giantiep_tructiep where shkb='" + shkb + "' order by TT_SHB desc ";
            }
            else
            {
                strSQL = "select ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep from tcs_dm_nh_giantiep_tructiep where shkb='" + shkb + "' and TT_SHB <> '1' order by id ";
            }
            try
            {
                //doan nay lay thong tin ngan hang huong
                DataSet dsNH = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (dsNH != null)
                {
                    if (dsNH.Tables.Count > 0)
                    {
                        if (dsNH.Tables[0].Rows.Count > 0)
                        {
                            MA_TT = dsNH.Tables[0].Rows[0]["ma_tructiep"].ToString();
                            TEN_TT = dsNH.Tables[0].Rows[0]["ten_tructiep"].ToString();
                            MA_GT = dsNH.Tables[0].Rows[0]["ma_giantiep"].ToString();
                            TEN_GT = dsNH.Tables[0].Rows[0]["ten_giantiep"].ToString();
                            kq = true;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("EX - GetNHGianTiepTrucTiep: " + ex.Message + ex.StackTrace);
                return false;
            }

            return kq;
        }
    }
}
