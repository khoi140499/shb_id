﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Security.Cryptography.Xml;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Configuration;
using System.IO;
using log4net;

namespace APIMOBITCT
{
   
    public class SignatureDigital
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string decodeDataTCT(TCTObjectIn objIn)
        {
            X509Certificate2 certificate = GetCertTCT();

            return "";
        }
        public static X509Certificate2 loadCertPathFromBase64String(String aCertChainBase64Encoded) 
        {
            byte[] certChainEncoded = System.Convert.FromBase64String(aCertChainBase64Encoded);
            X509Certificate2 xCert01 = new X509Certificate2();
            xCert01.Import(certChainEncoded);
            return xCert01;
        }
        static public X509Certificate2 GetCertTCT()
        {
            X509Certificate2 certificate = new X509Certificate2(ConfigurationManager.AppSettings["CertPathTCT"]);

            return certificate;
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }


        public static bool getVerifyMobile(string content)
        {
            string on_off = ConfigurationManager.AppSettings["MOBITCT.Check_CKS"];
            log.Info("1.getVerifyMobile - on_off:" + on_off);
            if (on_off.Equals("1"))
            {
                try
                {
                    string SIGNATUREURL = Business.Common.mdlCommon.TCS_GETTS("DIGITAL.SIGNATURE.URL");
                    string certPath = ConfigurationManager.AppSettings["CertPathTCT"];

                    log.Info("2.getVerifyMobile - SIGNATUREURL:" + SIGNATUREURL + "------- certPath: " + certPath);
                    bool valid = false;
                    string rs = "";

                    byte[] bufferCertPath = Encoding.UTF8.GetBytes(certPath);
                    string maskedCertPath = Convert.ToBase64String(bufferCertPath);

                    rs = MessageRequest(SIGNATUREURL + "/VerifyTCT/" + maskedCertPath, content);

                    if (rs.Equals("true"))
                    {
                        valid = true;
                    }
                    else
                    {
                        log.Info("Error source: DigitalSignature | getVerifyMobile()\\n" + "Error code: System error!" + "\\n" + "Error message: " + rs);
                        valid = false;
                    }
                    log.Info("3.getVerifyMobile - rs:" + rs);
                    //output = rs;
                    return valid;
                }
                catch (Exception ex)
                {
                    log.Info("Error source: " + ex.Source + " | getVerifyMobile()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message);
                    return false;
                }
            }
            else
            {
                return true;
            }

        }
        public static string MessageRequest(string url, string content)
        {
            string strResponse = "";
            try
            {
                string strURL = url;
                byte[] dataByte = Encoding.UTF8.GetBytes(content);

                HttpWebRequest POSTRequest = (HttpWebRequest)WebRequest.Create(strURL);
                //Method type
                POSTRequest.Method = "POST";
                // Data type - message body coming in xml
                POSTRequest.ContentType = "text/xml";
                POSTRequest.KeepAlive = true;
                //POSTRequest.Timeout = WS_TIMEPOUT '5000
                //Content length of message body
                POSTRequest.ContentLength = dataByte.Length;

                // Get the request stream
                Stream POSTstream = POSTRequest.GetRequestStream();
                // Write the data bytes in the request stream
                POSTstream.Write(dataByte, 0, dataByte.Length);

                //Get response from server
                HttpWebResponse POSTResponse = (HttpWebResponse)POSTRequest.GetResponse();

                StreamReader reader = new StreamReader(POSTResponse.GetResponseStream(), Encoding.UTF8);
                strResponse = reader.ReadToEnd().ToString();


            }
            catch (Exception ex)
            {
                log.Info("Error source: " + ex.Source + " | MessageRequest_MOBILE()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message);
            }

            return strResponse;
        }
    }
}