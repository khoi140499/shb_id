﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using Newtonsoft.Json;
using APIMOBITCT.Models;
using CorebankServiceESB;
using System.Data;
namespace APIMOBITCT.Controllers
{
    public class APIXACTHUCOTPController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        public object Get(object pIn)
        {
            XACTHUC_OTP_OUT result = new XACTHUC_OTP_OUT();
            result.TThai = "";
            string strLOGID = "";
            string strMST = "";
            try
            {
                //check chữ ký số TCT
                if (SignatureDigital.getVerifyMobile(pIn.ToString()))
                {
                    strLOGID = MobiTCTProcess.getLogID();
                    CorebankServiceESB.clsCoreBank cls = new CorebankServiceESB.clsCoreBank();
                    log.Info(pIn.ToString());
                    TCTObjectIn vobjTCT = JsonConvert.DeserializeObject<TCTObjectIn>(pIn.ToString());
                    XACTHUC_OTP_IN vIn = JsonConvert.DeserializeObject<XACTHUC_OTP_IN>(SignatureDigital.Base64Decode(vobjTCT.duLieu));
                    MobiTCTProcess.InsertLog(strLOGID, pIn.ToString(), "XACTHUCOTP");
                    OTPInfo otpObject = MobiTCTProcess.getThongtinGiaoDich(vIn.Magiaodich, vIn.loaiOTP);
                    //neu tim thay thong tin giao dich dang cho xu ly
                    if (otpObject.ID.Length > 0)
                    {
                        strMST = otpObject.MST;
                        log.Info("0.APIXACTHUCOTPController:strMST:  " + strMST + " - vIn.Magiaodich: " + vIn.Magiaodich);
                        string strSoTK = "";
                        string strCifNo = "";
                        string strSoTK_The = "";
                        DataSet ds = null;
                        if (otpObject.LoaiGD.Equals("Open") || otpObject.LoaiGD.Equals("Close"))
                        {
                             ds = MobiTCTProcess.getTTLienKetOpen(strMST, vIn.Magiaodich);
                        }
                        else
                        {
                            ds = MobiTCTProcess.getTTLienKetHachToan(otpObject.ewallettoken);
                        }
                      
                        if (ds != null)
                            if (ds.Tables.Count > 0)
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    strCifNo = ds.Tables[0].Rows[0]["CIFNO"].ToString();
                                    strSoTK = ds.Tables[0].Rows[0]["SOTAIKHOAN"].ToString();
                                    strSoTK_The = ds.Tables[0].Rows[0]["SOTAIKHOAN_THE"].ToString();
                                }

                        //
                        if (strCifNo.Trim().Length <= 0)
                        {
                            result.TThai = "06";
                            result.Mota = "Lỗi hệ thống";
                        }
                        else
                        {
                            log.Info("1.APIXACTHUCOTPController:strCifNo:  " + strCifNo + " - strSoTK: " + strSoTK);
                            CorebankServiceESB.clsCoreBank svr = new CorebankServiceESB.clsCoreBank();
                            string strAmount = "";
                            if (otpObject.LoaiGD.Equals("Open") || otpObject.LoaiGD.Equals("Close"))
                            {
                                strAmount = "0";
                            }
                            else
                            {
                                THANHTOANMOBI_IN objIn = JsonConvert.DeserializeObject<THANHTOANMOBI_IN>(otpObject.MSG);
                                MobiTCTProcess.SoTien_NNT(objIn.maThamChieu,ref strAmount);
                                strAmount = Business.Common.mdlCommon.NumberToStringVN2(strAmount).ToString();
                            }
                            result.TThai = svr.validOTP(vIn.Magiaodich, strCifNo, vIn.OTP, strAmount);
                            if (result.TThai.Equals("00"))
                            {
                                //neu la loai dang ky
                                if (otpObject.LoaiGD.Equals("Open") || otpObject.LoaiGD.Equals("Close"))
                                {
                                    MobiTCTProcess.MOLIENKETTK_OTP(otpObject.ID, otpObject.LoaiGD, otpObject.ID);
                                    result.Mota = "thanh cong";
                                    //Cập nhật ewallet ở đây
                                    result.ewalletToken = MobiTCTProcess.Base64Encode(otpObject.ID);
                                }
                                //neu la chung tu
                                else if (otpObject.LoaiGD.Equals("CTU"))
                                {
                                    //tao chung tu thue
                                    THANHTOANMOBI_IN objIn = JsonConvert.DeserializeObject<THANHTOANMOBI_IN>(otpObject.MSG);
                                    string strGhichu = "";
                                    string strSoCTU = "";
                                    //luôn hạch toán bằng số tài khoản
                                    objIn.SoTaiKhoan = strSoTK;
                                    objIn.phuongThuc = "Account";

                                    //Them loaits
                                    if (objIn.thongTinBienLai.maLoaiHinhThue.Equals("02") == true)
                                    {
                                        foreach (var item in objIn.thongTinBienLai.dskhoanNop)
                                        {
                                            item.loaiTs = item.noiDung.Split('+')[2];
                                        }
                                    }

                                    result.TThai = MobiTCTProcess.THANHTOAN_B3_TAO_CTU(objIn, ref strGhichu, ref strSoCTU);
                                    //day vao core cat tien
                                    if (result.TThai.Equals(""))
                                    {
                                        result.TThai = "00";
                                        result.Mota = "thanh cong";
                                    }
                                    else
                                    {
                                        //cap nhat hach toan loi
                                        if (result.TThai.Equals("112") || result.TThai.Equals("114") || result.TThai.Equals("01") || result.TThai.Equals("123"))
                                        {
                                            MobiTCTProcess.UPDATE_CTU_THANHTOAN_TRANGTHAI_07(objIn.maThamChieu, strSoCTU, strGhichu);
                                        }
                                        if (!String.IsNullOrEmpty(MobiTCTProcess.getDefError(result.TThai)))
                                        {
                                            result.Mota = MobiTCTProcess.getDefError(result.TThai);
                                        }
                                        else
                                        {
                                            result.Mota = strGhichu;
                                        }

                                    }
                                }

                            }
                            else
                            {
                                //cap nhat huy do sai OTP
                                result.TThai = "071";
                                result.Mota = "OTP không hợp lệ";
                                if (otpObject.LoaiGD.Equals("CTU"))
                                {
                                    MobiTCTProcess.UPDATE_CTU_THANHTOAN_TRANGTHAI_04(otpObject.ID, result.TThai + "-" + result.Mota);

                                }
                                else
                                {
                                    MobiTCTProcess.UPDATE_MOLIENKETTK_TRANGTHAI_04(otpObject.ID);
                                }
                            }
                        }
                    }
                    else
                    {
                        result.TThai = "071";
                        result.Mota = "OTP không hợp lệ";
                    }
                }
                else
                {
                    log.Info("result.TThai: 07 - result.Mota = Lỗi xác thực");
                    result.TThai = "07";
                    result.Mota = "Lỗi xác thực";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
                result.TThai = "06";
                result.Mota = "Lỗi hệ thống";
            }
            string strResult = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            MobiTCTProcess.UpdateLog(strLOGID, strResult, result.TThai, result.Mota, strMST);
            //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return result;
        }
    }
}
