﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APIMOBITCT.Models;
using log4net;
using Newtonsoft.Json;
using System.Configuration;
using CorebankServiceESB;
using System.Data;
namespace APIMOBITCT.Controllers
{
    public class THANHTOANMOBITCTController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        public object Get(object id)
        {
            THANHTOANMOBI_OUT result = new THANHTOANMOBI_OUT();
            result.Tthai = "";
            string strLOGID = "";// MobiTCTProcess.getLogID();
            string strMST = "";
            string strCifNo = "";
            string strSoTK = "";
            try
            {
                //check chữ ký số TCT
                if (SignatureDigital.getVerifyMobile(id.ToString()))
                {
                    strLOGID = MobiTCTProcess.getLogID();

                    log.Info(id.ToString());
                    TCTObjectIn vobjTCT = JsonConvert.DeserializeObject<TCTObjectIn>(id.ToString());//(XACTHUCNTTIN)Newtonsoft.Json.JsonConvert.DeserializeObject(pIn);

                    MobiTCTProcess.InsertLog(strLOGID, id.ToString(), "NOPTHUE");
                    if (vobjTCT.duLieu.Length > 0)
                    {
                        THANHTOANMOBI_IN vIn = JsonConvert.DeserializeObject<THANHTOANMOBI_IN>(SignatureDigital.Base64Decode(vobjTCT.duLieu));
                        strMST = vIn.thongTinBienLai.mst;
                        //neu khong dung ma cua ngan hang
                        if (!vIn.maDoiTac.Equals(ConfigurationManager.AppSettings["MA_NH"]))
                        {
                            result.Tthai = "09";
                            result.Mota = "Mã đối tác không tồn tại";
                            // return Newtonsoft.Json.JsonConvert.SerializeObject(result);
                        }
                        CorebankServiceESB.clsCoreBank cls = new CorebankServiceESB.clsCoreBank();


                        if (result.Tthai.Equals(""))
                        {
                            DataSet ds = MobiTCTProcess.getTTTK_LienKet_Huy(MobiTCTProcess.Base64Decode(vIn.ewalletToken));
                            string strSoTK_the = "";
                            string strDienthoai = null;
                            string strIDLK = "";
                            string strMaCN = "";
                            string strMA_NNTIEN = "";
                            string strTEN_NNTIEN = "";
                            if (ds != null)
                                if (ds.Tables.Count > 0)
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        strSoTK_the = ds.Tables[0].Rows[0]["soTaiKhoan_The"].ToString();
                                        strDienthoai = ds.Tables[0].Rows[0]["dienThoai"].ToString();
                                        strIDLK = ds.Tables[0].Rows[0]["ID"].ToString();
                                        strCifNo = ds.Tables[0].Rows[0]["CIFNO"].ToString();
                                        strSoTK = ds.Tables[0].Rows[0]["SOTAIKHOAN"].ToString();
                                        strMaCN = ds.Tables[0].Rows[0]["MA_CN"].ToString();
                                        strMA_NNTIEN = ds.Tables[0].Rows[0]["mst"].ToString();
                                        strTEN_NNTIEN = ds.Tables[0].Rows[0]["ten_nnt"].ToString();
                                    }

                            //neu ko tim thay tai khoan thi bao loi
                            if (strSoTK == null || strDienthoai == null)
                            {
                                result.Tthai = "060";
                                result.Mota = "STK/Thẻ không tồn tại trên hệ thống";
                            }
                            //doan nay em kiem tra tai khoan co khopko
                            if (result.Tthai.Equals(""))
                            {
                                if (vIn.phuongThuc.ToUpper().Equals("ACCOUNT"))
                                {
                                    if (!strSoTK.Substring(0, 3).Equals(vIn.soTaiKhoan_The.Substring(0, 3)) ||
                                   !strSoTK.Substring(strSoTK.Length - 4, 4).Equals(vIn.soTaiKhoan_The.Substring(vIn.soTaiKhoan_The.Length - 4, 4)))
                                    {
                                        result.Tthai = "060";
                                        result.Mota = "STK/Thẻ không tồn tại trên hệ thống";
                                    }
                                }
                            }
                            if (result.Tthai.Equals(""))
                            {
                                //kiểm tra mã nguyên tệ tài khoản và mã nguyên tệ lệnh thanh toán
                                CorebankServiceESB.MOBI.AccountResp obj = cls.GetTK_XACTHUCNTT(strSoTK_the, vIn.phuongThuc);
                                if (!(obj.cCrrcy).Equals(vIn.maTienTe))
                                {
                                    //tạm thời để mã 114 vì không có mã nào là  nguyên tệ lệnh khác nguyên tệ tài khoản
                                    result.Tthai = "120";
                                    result.Mota = "Loại tài khoản không thể xác thực/liên kết";
                                }
                            }                                                                                                                   
                            if (result.Tthai.Equals(""))
                            {
                                if (MobiTCTProcess.CheckExitsCTU(vIn.maThamChieu))
                                {
                                    vIn.SoTaiKhoan = strSoTK;
                                    vIn.IdTaiKhoanLienKet = strIDLK;
                                    vIn.dienThoai = strDienthoai;
                                    vIn.ma_NNTien = strMA_NNTIEN;
                                    vIn.ten_NNTien = strTEN_NNTIEN;
                                    vIn.soTaiKhoanThe_Decrypt = strSoTK_the;
                                    result.Tthai = MobiTCTProcess.InsertCTU_B1(vIn, strMaCN);
                                    //kiem tra danh muc ( kho bac, ngan hang truc tiep, gian tiep) neu ko co thi tra ve loi he thong 

                                }
                                else
                                {
                                    result.Tthai = "03";
                                    result.Mota = "Mã giao dịch đã tồn tại";
                                }

                                //valid số tiền chi tiết và số tiền tổng

                                if (MobiTCTProcess.CheckMoney(vIn) == false)
                                {
                                    result.Tthai = "01";
                                    result.Mota = "Giao dịch thất bại - Sai số tiền";
                                }

                                //phan nay danh cho tinh phi
                                if (result.Tthai.Equals(""))
                                {
                                    //tính phí ở đây
                                    log.Info("1.THANHTOANMOBITCTController");
                                    string phi = cls.GetChareEtaxMobile(vIn.soTien, vIn.SoTaiKhoan);
                                    log.Info("2.THANHTOANMOBITCTController");
                                    if (phi.Equals("-1"))
                                    {
                                        result.Tthai = "113";
                                        result.Mota = "Lỗi lấy lệ phí";
                                    }
                                    else
                                    {
                                        result.lephi = phi;
                                    }
                                    log.Info("3.THANHTOANMOBITCTController");
                                }
                                //sinh OTP
                                if (result.Tthai.Equals(""))
                                {
                                    log.Info("4.THANHTOANMOBITCTController");
                                    //goi OTP hehe
                                    CorebankServiceESB.MOBI.SendOTPInfo objOTP = cls.SendOTP(strCifNo, Business.Common.mdlCommon.NumberToStringVN2(vIn.soTien).ToString());
                                    //neu thanh cong sinh OTP
                                    if (objOTP.ERRCODE.Equals("0"))
                                    {
                                        result.Tthai = "00";
                                        result.Mota = "Thanh cong";
                                        result.Magiaodich = objOTP.TRANID;
                                        result.loaiOTP = objOTP.loaiOTP;
                                        //thiet lap phan thong bao phi

                                        if (objOTP.DESC.Length > 0)
                                            result.smartOTPUnlockCode = objOTP.DESC;
                                        else
                                            result.smartOTPUnlockCode = null;
                                        //cap nhat trang thai 02
                                        if (!MobiTCTProcess.UpdateThongtinOTPCTU(vIn.maThamChieu, objOTP.TRANID, result.loaiOTP))
                                        {
                                            result.Tthai = "074";
                                            result.Mota = "Không gửi được SMS OTP";
                                            MobiTCTProcess.XoaDSKHOANNOP_kosinhdcOTP(vIn.maThamChieu, result.Tthai + "-" + result.Mota);
                                        }

                                    }
                                    else if (objOTP.ERRCODE.Equals("04")==true)
                                    {
                                        result.Tthai = "04";
                                        result.Mota = "Timeout";
                                        //xoa du lieu lien ket
                                        MobiTCTProcess.XoaDSKHOANNOP_kosinhdcOTP(vIn.maThamChieu, result.Tthai + "-" + result.Mota);
                                    }
                                    else
                                    {
                                        result.Tthai = "074";
                                        result.Mota = "Không gửi được SMS OTP";
                                        //xoa du lieu lien ket
                                        MobiTCTProcess.XoaDSKHOANNOP_kosinhdcOTP(vIn.maThamChieu, result.Tthai + "-" + result.Mota);
                                    }
                                }

                            }
                        }
                        else
                        {
                            result.Tthai = "99";
                            result.Mota = "Không tìm thấy dữ liệu";
                            //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
                        }
                    }
                    if (result.Tthai.Equals(""))
                    {
                        result.Tthai = "00";
                        result.Mota = "Thành công";
                    }
                }
                else
                {
                    log.Info("result.TThai: 07 - result.Mota = Lỗi xác thực");
                    result.Tthai = "07";
                    result.Mota = "Lỗi xác thực";
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
                result.Tthai = "06";
                result.Mota = "Lỗi hệ thống";
            }
            string strResult = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            MobiTCTProcess.UpdateLog(strLOGID, strResult, result.Tthai, result.Mota, strMST);
           // return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return result;
        }
    }
}
