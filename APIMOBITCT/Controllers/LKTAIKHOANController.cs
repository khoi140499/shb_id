﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CorebankServiceESB;
using APIMOBITCT.Models;
using Newtonsoft.Json;
using System.Configuration;
using System.Data;
namespace APIMOBITCT.Controllers
{
    public class LKTAIKHOANController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        public object Get(object pIn)
        {
            LKTAIKHOANOUT result = new LKTAIKHOANOUT();
            result.TThai = "";
            string strLOGID = "";
            string strIDLK = "";
            string strMST = "";
            string strCifNo = "";
            string strSoTK = "";
            try
            {
                //check chữ ký số TCT
                if (SignatureDigital.getVerifyMobile(pIn.ToString()))
                {
                    strLOGID = MobiTCTProcess.getLogID();
                    CorebankServiceESB.clsCoreBank cls = new CorebankServiceESB.clsCoreBank();
                    log.Info(pIn.ToString());
                    TCTObjectIn vobjTCT = JsonConvert.DeserializeObject<TCTObjectIn>(pIn.ToString());
                    LKTAIKHOANIN vIn = JsonConvert.DeserializeObject<LKTAIKHOANIN>(SignatureDigital.Base64Decode(vobjTCT.duLieu));
                   
                    strMST = vIn.mst;
                    if (vIn.loai.Equals("Open"))
                        MobiTCTProcess.InsertLog(strLOGID, pIn.ToString(), "MOLKTAIKHOAN");
                    else
                        MobiTCTProcess.InsertLog(strLOGID, pIn.ToString(), "DONGLKTAIKHOAN");
                    //kiem tra thong tin tai khoan
                    if (!vIn.maDoiTac.Equals(ConfigurationManager.AppSettings["MA_NH"]))
                    {
                        result.TThai = "09";
                        result.Mota = "Mã đối tác không tồn tại";
                        //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
                    }
                    if (result.TThai.Equals(""))
                    {
                        //neu la mo tai khoan
                        if (vIn.loai.Equals("Open"))
                        {
                            CorebankServiceESB.MOBI.AccountResp obj = cls.GetTK_XACTHUCNTT_NEW(vIn.soTaiKhoan_The, vIn.phuongThuc, vIn.loaiGiayTo);
                            
                            if (obj.errorCode.Equals("0"))
                            {
                                strCifNo = obj.cifNo;
                                strSoTK = obj.accNo;
                                //so sanh ma so thue
                                if (vIn.phuongThuc.ToUpper().Equals("ACCOUNT"))
                                {
                                    if (!obj.accNo.Equals(vIn.soTaiKhoan_The))
                                    {
                                        result.TThai = "056";
                                        result.Mota = "Sai số tài khoản";
                                    }
                                }
                                
                                if (result.TThai.Equals("") && !vIn.dienThoai.Equals(obj.dienThoai))
                                {
                                    result.TThai = "058";
                                    result.Mota = "Sai số điện thoại";
                                }
                                if (result.TThai.Equals(""))
                                {
                                    //neu loai giay to la cmnd
                                    
                                    if (vIn.loaiGiayTo.ToUpper().Equals("CMND"))
                                    {
                                        if (!obj.loaiGiayTo.ToUpper().Equals("CMND"))
                                        {
                                            result.TThai = "052";
                                            result.Mota = "Sai số chứng minh nhân dân";
                                        }
                                        if (result.TThai.Equals("") && !vIn.soGiayTo.ToUpper().Equals(obj.soGiayTo.ToUpper()))
                                        {
                                            result.TThai = "052";
                                            result.Mota = "Sai số chứng minh nhân dân";
                                        }

                                    }
                                    //else if (vIn.mst.ToUpper().Equals(obj.mst.ToUpper()) == false)
                                    //{
                                    //    result.TThai = "124";
                                    //    result.Mota = "Tài khoản đã được liên kết với MST khác";
                                    //}
                                    else if(obj.cCrrcy.Equals("VND")==false)
                                    {
                                        result.TThai = "120";
                                        result.Mota = "Loại tài khoản không thể xác thực/liên kết";
                                    }
                                    else if (vIn.loaiGiayTo.ToUpper().Equals("CCCD"))
                                    {
                                        if (!obj.loaiGiayTo.ToUpper().Equals("CCCD"))
                                        {
                                            result.TThai = "053";
                                            result.Mota = "Sai số căn cước công dân";
                                        }
                                        if (result.TThai.Equals("") && !vIn.soGiayTo.ToUpper().Equals(obj.soGiayTo.ToUpper()))
                                        {
                                            result.TThai = "053";
                                            result.Mota = "Sai số căn cước công dân";
                                        }
                                    }
                                    else if (vIn.loaiGiayTo.ToUpper().Equals("HC"))
                                    {
                                        if (!obj.loaiGiayTo.ToUpper().Equals("HC"))
                                        {
                                            result.TThai = "054";
                                            result.Mota = "Sai số hộ chiếu";
                                        }
                                        if (result.TThai.Equals("") && !vIn.soGiayTo.ToUpper().Equals(obj.soGiayTo.ToUpper()))
                                        {
                                            result.TThai = "054";
                                            result.Mota = "Sai số hộ chiếu";
                                        }
                                    }
                                    else
                                    {
                                        result.TThai = "052";
                                        result.Mota = "Sai số chứng minh nhân dân";
                                    }
                                }

                            }
                            else if (obj.errorCode.Equals("052"))
                            {
                                result.TThai = "052";
                                result.Mota = "Sai số chứng minh nhân dân";
                            }
                            else if (obj.errorCode.Equals("053"))
                            {
                                result.TThai = "053";
                                result.Mota = "Sai số căn cước công dân";
                            }
                            else if (obj.errorCode.Equals("054"))
                            {
                                result.TThai = "054";
                                result.Mota = "Sai số hộ chiếu ";
                            }
                            else if (obj.errorCode.Equals("99"))
                            {
                                result.TThai = "99";
                                result.Mota = "Lỗi không xác định ";
                            }
                            else if (obj.errorCode.Equals("060"))
                            {
                                result.TThai = "060";
                                result.Mota = "STK/Thẻ không tồn tại trên hệ thống";
                                //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
                            }
                            //neu tai khoan OK
                            if (result.TThai.Equals(""))
                            {
                                // ghi nhan bang quan ly du lieu
                                strIDLK = strLOGID;
                                result.TThai = MobiTCTProcess.MoLienKetTaiKhoan(strLOGID, vIn, strCifNo, strSoTK, obj.branchCode);
                            }
                        }
                        else
                        {

                            //lay thong tin tai khoan
                            DataSet ds = MobiTCTProcess.getTTTK_LienKet_Huy(MobiTCTProcess.Base64Decode(vIn.ewalletToken));
                            if (ds != null)
                                if (ds.Tables.Count > 0)
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        vIn.soTaiKhoan_The = ds.Tables[0].Rows[0]["soTaiKhoan_The"].ToString();
                                        vIn.dienThoai = ds.Tables[0].Rows[0]["dienThoai"].ToString();
                                        strIDLK = ds.Tables[0].Rows[0]["ID"].ToString();
                                        strCifNo = ds.Tables[0].Rows[0]["CIFNO"].ToString();
                                    }
                            //neu ko tim thay tai khoan thi bao loi
                            if (vIn.soTaiKhoan_The == null || vIn.dienThoai == null)
                            {
                                result.TThai = "060";
                                result.Mota = "STK/Thẻ không tồn tại trên hệ thống";
                            }
                            //ghi nhan vao he thong phan dang cho huy
                            if (result.TThai.Equals(""))
                            {
                                //ghi nhan du lieu huy
                                result.TThai = MobiTCTProcess.HuyLienKet_B1(strLOGID, strIDLK, vIn.mst);
                            }


                        }

                    }


                    //neu tai khoan OK thi goi OTP
                    if (result.TThai.Equals(""))
                    {

                        //goi OTP // send OTP bang so CIF: strCifNo
                        CorebankServiceESB.MOBI.SendOTPInfo objOTP = cls.SendOTP(strCifNo, "0");
                        //neu thanh cong sinh OTP
                        if (objOTP.ERRCODE.Equals("0"))
                        {
                            result.TThai = "00";
                            result.Mota = "Thanh cong";
                            result.Magiaodich = objOTP.TRANID;
                            result.loaiOTP = objOTP.loaiOTP;
                            if (objOTP.DESC.Length > 0)
                                result.smartOTPUnlockCode = objOTP.DESC;
                            else
                                result.smartOTPUnlockCode = null;
                            MobiTCTProcess.UpdateThongtinOTP(strLOGID, objOTP.TRANID, result.loaiOTP);
                        }
                        else
                        {
                            result.TThai = "074";
                            result.Mota = "Không gửi được SMS OTP";
                            //xoa du lieu lien ket
                            MobiTCTProcess.XoaLienKetTaiKhoan_kosinhdcOTP(strLOGID);
                        }
                    }

                }
                else
                {
                    log.Info("result.TThai: 07 - result.Mota = Lỗi xác thực");
                    result.TThai = "07";
                    result.Mota = "Lỗi xác thực";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
                result.TThai = "06";
                result.Mota = "Lỗi hệ thống";
            }
            string strResult = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            MobiTCTProcess.UpdateLog(strLOGID, strResult, result.TThai, result.Mota, strMST);
            //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return result;
        }
    }
}
