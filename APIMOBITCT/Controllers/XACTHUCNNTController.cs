﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CorebankServiceESB;
using APIMOBITCT.Models;
using Newtonsoft.Json;
using System.Configuration;
namespace APIMOBITCT.Controllers
{
    public class XACTHUCNNTController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        // public object Get()

        [HttpPost]
        public object Get(object id)
        {
            XACTHUCNTTOUT result = new XACTHUCNTTOUT();
            result.tthai = "";
            string strLOGID = "";// MobiTCTProcess.getLogID();
            string strMST = "";
            try
            {
                log.Info("1.XACTHUCNTTOUT");
                //check chữ ký số TCT
                if (SignatureDigital.getVerifyMobile(id.ToString()))
                {
                    strLOGID = MobiTCTProcess.getLogID();

                    log.Info(id.ToString());
                    TCTObjectIn vobjTCT = JsonConvert.DeserializeObject<TCTObjectIn>(id.ToString());//(XACTHUCNTTIN)Newtonsoft.Json.JsonConvert.DeserializeObject(pIn);

                    MobiTCTProcess.InsertLog(strLOGID, id.ToString(), "XACTHUCNNT");
                    if (vobjTCT.duLieu.Length > 0)
                    {
                        XACTHUCNTTIN vIn = JsonConvert.DeserializeObject<XACTHUCNTTIN>(SignatureDigital.Base64Decode(vobjTCT.duLieu));
                        strMST = vIn.mst;
                        //neu khong dung ma cua ngan hang
                        if (!vIn.maDoiTac.Equals(ConfigurationManager.AppSettings["MA_NH"]))
                        {
                            result.tthai = "09";
                            result.mota = "Mã đối tác không tồn tại";
                            //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
                        }
                        if (result.tthai.Equals(""))
                        {
                            CorebankServiceESB.clsCoreBank cls = new CorebankServiceESB.clsCoreBank();
                            CorebankServiceESB.MOBI.AccountResp obj = cls.GetTK_XACTHUCNTT_NEW(vIn.soTaiKhoan_The, vIn.phuongThuc, vIn.loaiGiayTo);
                            result.tthai = "";
                            if (obj.errorCode.Equals("0"))
                            {
                                if (vIn.phuongThuc.ToUpper().Equals("ACCOUNT"))
                                {
                                    //so sanh ma so thue
                                    if (!obj.accNo.Equals(vIn.soTaiKhoan_The))
                                    {
                                        result.tthai = "056";
                                        result.mota = "Sai số tài khoản";
                                    }
                                }
                                if (result.tthai.Equals("") && !vIn.dienThoai.Equals(obj.dienThoai))
                                {
                                    result.tthai = "058";
                                    result.mota = "Sai số điện thoại";
                                }
                                if (result.tthai.Equals(""))
                                {
                                    //neu loai giay to la cmnd

                                    if (vIn.loaiGiayTo.ToUpper().Equals("CMND"))
                                    {
                                        if (!obj.loaiGiayTo.ToUpper().Equals("CMND"))
                                        {
                                            result.tthai = "052";
                                            result.mota = "Sai số chứng minh nhân dân";
                                        }
                                        if (result.tthai.Equals("") && !vIn.soGiayTo.ToUpper().Equals(obj.soGiayTo.ToUpper()))
                                        {
                                            result.tthai = "052";
                                            result.mota = "Sai số chứng minh nhân dân";
                                        }

                                    }
                                    else if (vIn.loaiGiayTo.ToUpper().Equals("CCCD"))
                                    {
                                        if (!obj.loaiGiayTo.ToUpper().Equals("CCCD"))
                                        {
                                            result.tthai = "053";
                                            result.mota = "Sai số căn cước công dân";
                                        }
                                        if (result.tthai.Equals("") && !vIn.soGiayTo.ToUpper().Equals(obj.soGiayTo.ToUpper()))
                                        {
                                            result.tthai = "053";
                                            result.mota = "Sai số căn cước công dân";
                                        }
                                    }
                                    else if (vIn.loaiGiayTo.ToUpper().Equals("HC"))
                                    {
                                        if (!obj.loaiGiayTo.ToUpper().Equals("HC"))
                                        {
                                            result.tthai = "054";
                                            result.mota = "Sai số hộ chiếu";
                                        }
                                        if (result.tthai.Equals("") && !vIn.soGiayTo.ToUpper().Equals(obj.soGiayTo.ToUpper()))
                                        {
                                            result.tthai = "054";
                                            result.mota = "Sai số hộ chiếu";
                                        }
                                    }
                                    else
                                    {
                                        result.tthai = "052";
                                        result.mota = "Sai số chứng minh nhân dân";
                                    }
                                }

                            }
                            else if (obj.errorCode.Equals("052"))
                            {
                                result.tthai = "052";
                                result.mota = "Sai số chứng minh nhân dân";
                            }
                            else if (obj.errorCode.Equals("053"))
                            {
                                result.tthai = "053";
                                result.mota = "Sai số căn cước công dân";
                            }
                            else if (obj.errorCode.Equals("054"))
                            {
                                result.tthai = "054";
                                result.mota = "Sai số hộ chiếu ";
                            }
                            else if (obj.errorCode.Equals("99"))
                            {
                                result.tthai = "99";
                                result.mota = "Lỗi không xác định ";
                            }
                            else if (obj.errorCode.Equals("060"))
                            {
                                result.tthai = "060";
                                result.mota = "STK/Thẻ không tồn tại trên hệ thống";
                                //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
                            }
                            else
                            {
                                result.tthai = "99";
                                result.mota = "Không tìm thấy dữ liệu";
                                //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
                            }
                        }
                    }
                    if (result.tthai.Equals(""))
                    {
                        result.tthai = "00";
                        result.mota = "Thành công";
                    }

                }
                else
                {
                    log.Info("result.TThai: 07 - result.Mota = Lỗi xác thực");
                    result.tthai = "07";
                    result.mota = "Lỗi xác thực";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
                result.tthai = "06";
                result.mota = "Lỗi hệ thống";
            }
            string strResult = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            MobiTCTProcess.UpdateLog(strLOGID, strResult, result.tthai, result.mota, strMST);
            //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return result;
        }
        //[HttpPost]
        //public object Get(string id)
        //{
        //    XACTHUCNTTOUT result = new XACTHUCNTTOUT();
        //    result.tthai = "";
        //    string strLOGID = "";// MobiTCTProcess.getLogID();
        //    try
        //    {
        //        strLOGID = MobiTCTProcess.getLogID();

        //        log.Info(id);
        //        TCTObjectIn vobjTCT = JsonConvert.DeserializeObject<TCTObjectIn>(id);//(XACTHUCNTTIN)Newtonsoft.Json.JsonConvert.DeserializeObject(pIn);

        //        MobiTCTProcess.InsertLog(strLOGID, id, "XACTHUCNNT");
        //        if (vobjTCT.duLieu.Length > 0)
        //        {
        //            XACTHUCNTTIN vIn = JsonConvert.DeserializeObject<XACTHUCNTTIN>(SignatureDigital.Base64Decode(vobjTCT.duLieu));
        //            //neu khong dung ma cua ngan hang
        //            if (!vIn.maDoiTac.Equals(ConfigurationManager.AppSettings["MA_NH"]))
        //            {
        //                result.tthai = "09";
        //                result.mota = "Mã đối tác không tồn tại";
        //                return Newtonsoft.Json.JsonConvert.SerializeObject(result);
        //            }
        //            ClsCoreBank cls = new ClsCoreBank();
        //            AccountResp obj = cls.GetTK_XACTHUCNTT(vIn.soTaiKhoan_The);
        //            result.tthai = "";
        //            if (obj.errorCode.Equals("0"))
        //            {
        //                //so sanh ma so thue
        //                if (!obj.accNo.Equals(vIn.soTaiKhoan_The))
        //                {
        //                    result.tthai = "056";
        //                    result.mota = "Sai số tài khoản";
        //                }
        //                if (result.tthai.Equals("") && !vIn.dienThoai.Equals(obj.dienThoai))
        //                {
        //                    result.tthai = "058";
        //                    result.mota = "Sai số điện thoại";
        //                }
        //                if (result.tthai.Equals(""))
        //                {
        //                    //neu loai giay to la cmnd

        //                    if (vIn.loaiGiayTo.Equals("cmnd"))
        //                    {
        //                        if (!obj.loaiGiayTo.Equals("cmnd"))
        //                        {
        //                            result.tthai = "052";
        //                            result.mota = "Sai số chứng minh nhân dân";
        //                        }
        //                        if (result.tthai.Equals("") && !vIn.soGiayTo.Equals(obj.soGiayTo))
        //                        {
        //                            result.tthai = "052";
        //                            result.mota = "Sai số chứng minh nhân dân";
        //                        }

        //                    }
        //                    else if (vIn.loaiGiayTo.Equals("cccd"))
        //                    {
        //                        if (!obj.loaiGiayTo.Equals("cccd"))
        //                        {
        //                            result.tthai = "053";
        //                            result.mota = "Sai số căn cước công dân";
        //                        }
        //                        if (result.tthai.Equals("") && !vIn.soGiayTo.Equals(obj.soGiayTo))
        //                        {
        //                            result.tthai = "053";
        //                            result.mota = "Sai số căn cước công dân";
        //                        }
        //                    }
        //                    else if (vIn.loaiGiayTo.Equals("hc"))
        //                    {
        //                        if (!obj.loaiGiayTo.Equals("hc"))
        //                        {
        //                            result.tthai = "054";
        //                            result.mota = "Sai số hộ chiếu";
        //                        }
        //                        if (result.tthai.Equals("") && !vIn.soGiayTo.Equals(obj.soGiayTo))
        //                        {
        //                            result.tthai = "054";
        //                            result.mota = "Sai số hộ chiếu";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        result.tthai = "052";
        //                        result.mota = "Sai số chứng minh nhân dân";
        //                    }
        //                }

        //            }
        //            else
        //            {
        //                result.tthai = "99";
        //                result.mota = "Không tìm thấy dữ liệu";
        //                //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
        //            }
        //        }
        //        if (result.tthai.Equals(""))
        //        {
        //            result.tthai = "00";
        //            result.mota = "Thành công";
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.Message + "-" + ex.StackTrace);
        //        result.tthai = "06";
        //        result.mota = "Lỗi hệ thống";
        //    }
        //    string strResult = Newtonsoft.Json.JsonConvert.SerializeObject(result);
        //    MobiTCTProcess.UpdateLog(strLOGID, strResult, result.tthai, result.mota);
        //    return Newtonsoft.Json.JsonConvert.SerializeObject(result);
        //} 

    }
}
