﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APIMOBITCT.Models;
using Newtonsoft.Json;
using System.Configuration;
using System.Data;
using log4net;
using Newtonsoft.Json;
using APIMOBITCT.Models;
using CorebankServiceESB;
using System.Text;
namespace APIMOBITCT.Controllers
{
    public class APICAPNHATTRANGTHAIController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        public object Get(object pIn)
        {
            TCTXACNHANTRANGTHAI_OUT result = new TCTXACNHANTRANGTHAI_OUT();
            result.TThai = "";
            string strLOGID = "";
            string strMST = "";
            try
            {
                //check chữ ký số TCT
                if (SignatureDigital.getVerifyMobile(pIn.ToString()))
                {
                    strLOGID = MobiTCTProcess.getLogID();
                    log.Info(pIn.ToString());
                    TCTObjectIn vobjTCT = JsonConvert.DeserializeObject<TCTObjectIn>(pIn.ToString());
                    TCTXACNHANTRANGTHAI_IN vIn = JsonConvert.DeserializeObject<TCTXACNHANTRANGTHAI_IN>(SignatureDigital.Base64Decode(vobjTCT.duLieu));
                    MobiTCTProcess.InsertLog(strLOGID, pIn.ToString(), "XACTHUCTRANGTHAI");
                    OTPInfo otpObject = MobiTCTProcess.getThongtin_XACNHAN_GiaoDich(vIn.Magiaodich);
                    //neu tim thay thong tin giao dich dang cho xu ly
                    if (otpObject.ID.Length > 0)
                    {
                        strMST = otpObject.MST;
                        CorebankServiceESB.clsCoreBank svr = new CorebankServiceESB.clsCoreBank();
                        // result.TThai = svr.validOTP(vIn.Magiaodich, vIn.loaiOTP, vIn.OTP);
                        if (vIn.TThai.Equals("999"))
                        {
                            //neu la loai dang ky
                            if (otpObject.LoaiGD.Equals("Open"))
                            {
                                result.TThai = MobiTCTProcess.MOLIENKETTK_THANHCONG(otpObject.ID);
                                result.Mota = "thanh cong";
                            }
                            else if (otpObject.LoaiGD.Equals("Close"))
                            {
                                result.TThai = MobiTCTProcess.MOLIENKETTK_HUY(otpObject.ID, otpObject.IDREF);
                                result.Mota = "thanh cong";
                            }
                            else if (otpObject.LoaiGD.Equals("CTU"))
                            {
                                MobiTCTProcess.UPDATE_CTU_THANHTOAN_TRANGTHAI_06(otpObject.ID);
                                result.TThai = "00";
                                result.Mota = "thanh cong";
                            }

                        }
                        else
                        {
                            if (otpObject.LoaiGD.Equals("Open") || otpObject.LoaiGD.Equals("Close"))
                            {
                                MobiTCTProcess.UPDATE_MOLIENKETTK_TRANGTHAI_04(otpObject.ID);
                                result.TThai = "00";
                                result.Mota = "thanh cong";
                            }
                        }
                    }
                    else
                    {
                        result.TThai = "071";
                        result.Mota = "OTP không hợp lệ";
                    }
                }
                else
                {
                    log.Info("result.TThai: 07 - result.Mota = Lỗi xác thực");
                    result.TThai = "07";
                    result.Mota = "Lỗi xác thực";
                }            
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
                result.TThai = "06";
                result.Mota = "Lỗi hệ thống";
            }
          // Formatting.Indented
            //JsonSerializerSettings es = new Newtonsoft.Json.JsonSerializerSettings();
          
            string strResult = Newtonsoft.Json.JsonConvert.SerializeObject(result);
          //  StringBuilder strBulil = new StringBuilder();
          //  strBulil.Append(strResult);
            MobiTCTProcess.UpdateLog(strLOGID, strResult, result.TThai, result.Mota, strMST);
           // return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            //return strBulil.ToString();
            return result;
        }
    }
}
