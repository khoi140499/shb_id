﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VBOracleLib;
using APIMOBITCT.Models;
using System.Data.OracleClient;
using Newtonsoft.Json;
using System.Configuration;
using System.Threading;
using CorebankServiceESB;
using Business;
namespace APIMOBITCT
{
    public class MobiTCTProcess
    {
        private static string Core_version = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string getLogID()
        {
            string strResult = DateTime.Now.ToString("yyyyMMdd");
            string strSQL = "SELECT LPAD( SEQ_LOG_API_TCT_MOBI.NEXTVAL,8,'0') FROM DUAL";
            DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        strResult = strResult + ds.Tables[0].Rows[0][0].ToString();
            return strResult;
        }
        public static void InsertLog(string strId, string strMSG, string strMSGType)
        {
            try
            {
                string strSQL = "INSERT INTO SET_LOG_API_TCT_MOBI(ID, REQ_TYPE, REQ_MSG, REQ_TIME)";
                strSQL += "VALUES(:ID, :REQ_TYPE, :REQ_MSG, SYSDATE)";

                List<IDbDataParameter> lst = new List<IDbDataParameter>();
                lst.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, strId, DbType.String));
                lst.Add(DataAccess.NewDBParameter("REQ_TYPE", ParameterDirection.Input, strMSGType, DbType.String));
                lst.Add(DataAccess.NewDBParameter("REQ_MSG", ParameterDirection.Input, strMSG, DbType.String));
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray());
                //  return true;

            }
            catch (Exception ex)
            {
                log.Info("InsertLog :" + strMSG);
                log.Error(ex.Message + "-" + ex.StackTrace);
                //  return false;
            }
            //  return false;

        }
        public static void UpdateLog(string strId, string strRESMSG, string strErrCode, string strErrMSG, string strMST = "")
        {
            try
            {
                string strSQL = "UPDATE SET_LOG_API_TCT_MOBI SET RES_TIME=sysdate, RES_MSG=:RES_MSG, ERRCODE=:ERRCODE, ERRMSG=:ERRMSG,MST='" + strMST + "' where id='" + strId + "'";


                List<IDbDataParameter> lst = new List<IDbDataParameter>();

                lst.Add(DataAccess.NewDBParameter("RES_MSG", ParameterDirection.Input, strRESMSG, DbType.String));
                lst.Add(DataAccess.NewDBParameter("ERRCODE", ParameterDirection.Input, strErrCode, DbType.String));
                lst.Add(DataAccess.NewDBParameter("ERRMSG", ParameterDirection.Input, strErrMSG, DbType.String));
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray());
                // return true;

            }
            catch (Exception ex)
            {
                log.Info("Update Log:" + strRESMSG);
                log.Error(ex.Message + "-" + ex.StackTrace);
                // return false;
            }
            // return false;

        }

        public static string MoLienKetTaiKhoan(string strId, LKTAIKHOANIN objIn, string cifno, string so_tk, string ma_cn)
        {
            try
            {
                string strSQL = "INSERT INTO SET_DSTK_LIENKET_LOG(ID, MADOITAC, LOAI, MST, TEN_NNT, LOAIGIAYTO, ";
                strSQL += " SOGIAYTO, DIENTHOAI, PHUONGTHUC, SOTAIKHOAN_THE, ";
                strSQL += "  NGAYPHATHANH, NGAYHT, TRANGTHAI,NGAYMO,CIFNO, SOTAIKHOAN, MA_CN )";
                strSQL += " VALUES(:ID, :MADOITAC, :LOAI, :MST, :TEN_NNT, :LOAIGIAYTO, ";
                strSQL += " :SOGIAYTO, :DIENTHOAI, :PHUONGTHUC, :SOTAIKHOAN_THE, ";
                strSQL += " :NGAYPHATHANH, SYSDATE, '01',sysdate,:CIFNO, :SOTAIKHOAN, :MA_CN)";

                List<IDbDataParameter> lst = new List<IDbDataParameter>();
                lst.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, strId, DbType.String));
                lst.Add(DataAccess.NewDBParameter("MADOITAC", ParameterDirection.Input, objIn.maDoiTac, DbType.String));
                lst.Add(DataAccess.NewDBParameter("LOAI", ParameterDirection.Input, objIn.loai, DbType.String));
                lst.Add(DataAccess.NewDBParameter("MST", ParameterDirection.Input, objIn.mst, DbType.String));
                lst.Add(DataAccess.NewDBParameter("TEN_NNT", ParameterDirection.Input, objIn.ten_nnt, DbType.String));
                lst.Add(DataAccess.NewDBParameter("LOAIGIAYTO", ParameterDirection.Input, objIn.loaiGiayTo, DbType.String));
                lst.Add(DataAccess.NewDBParameter("SOGIAYTO", ParameterDirection.Input, objIn.soGiayTo, DbType.String));
                lst.Add(DataAccess.NewDBParameter("DIENTHOAI", ParameterDirection.Input, objIn.dienThoai, DbType.String));
                lst.Add(DataAccess.NewDBParameter("PHUONGTHUC", ParameterDirection.Input, objIn.phuongThuc, DbType.String));
                lst.Add(DataAccess.NewDBParameter("SOTAIKHOAN_THE", ParameterDirection.Input, objIn.soTaiKhoan_The, DbType.String));
                lst.Add(DataAccess.NewDBParameter("NGAYPHATHANH", ParameterDirection.Input, objIn.ngayPhatHanh, DbType.String));
                lst.Add(DataAccess.NewDBParameter("CIFNO", ParameterDirection.Input, cifno, DbType.String));
                lst.Add(DataAccess.NewDBParameter("SOTAIKHOAN", ParameterDirection.Input, so_tk, DbType.String));
                lst.Add(DataAccess.NewDBParameter("MA_CN", ParameterDirection.Input, ma_cn, DbType.String));

                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray());
                //  return true;
                return "";
            }
            catch (Exception ex)
            {

                log.Error(ex.Message + "-" + ex.StackTrace);
                //  return false;
            }
            return "06";

        }
        //public static LKTAIKHOANOUT getMobiSMS(string strID, LKTAIKHOANIN objIn)
        //{ 

        //}
        /// <summary>
        /// ham nay lay thong tin len de huy tam thoi thieu trang thai
        /// </summary>
        /// <param name="strMST"></param>
        /// <returns></returns>
        public static DataSet getTTTK_LienKet_Huy(string ewalletToken)
        {
            try
            {
                string strSQL = "select ID,SOTAIKHOAN_THE,DIENTHOAI,CIFNO,SOTAIKHOAN,MA_CN,mst,ten_nnt from SET_DSTK_LIENKET where ewalletToken='" + ewalletToken + "' and trangthai='06'";
                return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
            }
            catch (Exception ex)
            { }
            return null;
        }
        public static DataSet getTTLienKet(string strMST)
        {
            try
            {
                string strSQL = "select ID,SOTAIKHOAN_THE,DIENTHOAI,CIFNO,SOTAIKHOAN from SET_DSTK_LIENKET where MST='" + strMST + "' and trangthai='06'";
                log.Info("1.getTTLienKet: " + strSQL);
                return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);

            }
            catch (Exception ex)
            {
                log.Info("99.getTTLienKet:Exception: " + ex.Message + ex.StackTrace);
            }
            return null;
        }
        public static DataSet getTTLienKetHachToan(string ewallettoken)
        {
            try
            {
                string strSQL = "select ID,SOTAIKHOAN_THE,DIENTHOAI,CIFNO,SOTAIKHOAN from SET_DSTK_LIENKET where ewallettoken='" + ewallettoken + "' and trangthai='06'";
                log.Info("1.getTTLienKet: " + strSQL);
                return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);

            }
            catch (Exception ex)
            {
                log.Info("99.getTTLienKet:Exception: " + ex.Message + ex.StackTrace);
            }
            return null;
        }
        public static DataSet getTTLienKetOpen(string strMST, string strMaGD)
        {
            try
            {
                string strSQL = "select ID,SOTAIKHOAN_THE,DIENTHOAI,CIFNO,SOTAIKHOAN from SET_DSTK_LIENKET_LOG where MST='" + strMST + "' and magiaodich='" + strMaGD + "' and ngayht>(sysdate-1)";
                return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);

            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public static string getSoTK(string strMST)
        {
            //string strSoCif = "";
            try
            {
                string strSoTK = "";
                string strSQL = "select ID,SOTAIKHOAN_THE,DIENTHOAI,CIFNO,SOTAIKHOAN from SET_DSTK_LIENKET where MST='" + strMST + "' and trangthai='06'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return strSoTK = ds.Tables[0].Rows[0]["soTaiKhoan_The"].ToString();
                        }

            }
            catch (Exception ex)
            {

            }
            return "";
        }

        public static void UpdateThongtinOTP(string strID, string strMaGD, string strLoaiOTP)
        {
            try
            {
                //cap nhat lai ma giao dich va trang thai cho tct xac nhan
                string strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Magiaodich='" + strMaGD + "',trangthai='02',NGAYHT=SYSDATE,loaiotp='" + strLoaiOTP + "' WHERE ID='" + strID + "'";
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                //strSQL = "UPDATE SET_DSTK_LIENKET_HUY SET Magiaodich='" + strMaGD + "',NGAYHT=SYSDATE,loaiotp='" + strLoaiOTP + "' WHERE ID='" + strID + "'";
                //DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
        }
        public static void XoaLienKetTaiKhoan_kosinhdcOTP(string strID)
        {
            try
            {
                string strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Trangthai='03',NGAYHT=SYSDATE WHERE ID='" + strID + "'";
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                //strSQL = "UPDATE SET_DSTK_LIENKET_HUY SET TRANGTHAI='03',NGAYHT=SYSDATE WHERE ID='" + strID + "'";
                //DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
        }
        public static string getDefError(string strErrCode)
        {
            try
            {
                string strSQL = "SELECT  a.desciption  FROM tcs_deferror a where a.errcode='" + strErrCode + "' ";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            return ds.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
            return "";
        }
        public static OTPInfo getThongtinGiaoDich(string strMaGD, string strLoaiOTP)
        {
            OTPInfo result = new OTPInfo();
            try
            {
                //kiem tra xem co phai la dang ky moi khong
                string strSQL = "SELECT a.id,a.LOAI,MST  FROM set_dstk_lienket_log a  where  a.loaiotp='" + strLoaiOTP + "' and  a.magiaodich='" + strMaGD + "' and ngayht>(sysdate-1) ";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            //if (ds.Tables[0].Rows[0]["LOAI"].ToString().Equals("Open"))
                            result.ID = ds.Tables[0].Rows[0]["id"].ToString();
                            result.LoaiGD = ds.Tables[0].Rows[0]["LOAI"].ToString();
                            result.MST = ds.Tables[0].Rows[0]["MST"].ToString();
                            return result;
                        }
                //kiem tra xem co phai la lenh thanh toan ko
                strSQL = "SELECT a.maThamChieu,MSGCONTENT,MST,ewallettoken  FROM SET_CTU_HDR_MOBITCT a  where  a.loaiotp='" + strLoaiOTP + "' and  a.TRANIDOTP='" + strMaGD + "' and ngaymsg>(sysdate-1) and trangthai='02' ";
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            //if (ds.Tables[0].Rows[0]["LOAI"].ToString().Equals("Open"))
                            result.ID = ds.Tables[0].Rows[0]["maThamChieu"].ToString();
                            result.LoaiGD = "CTU";
                            result.MSG = ds.Tables[0].Rows[0]["MSGCONTENT"].ToString();
                            result.MST = ds.Tables[0].Rows[0]["MST"].ToString();
                            result.ewallettoken = Base64Decode(ds.Tables[0].Rows[0]["ewallettoken"].ToString());
                            return result;
                        }
            }
            catch (Exception ex)
            { log.Error(ex.Message + "-" + ex.StackTrace); }
            return result;
        }
        public static string MOLIENKETTK_THANHCONG(string strID)
        {
            IDbConnection _conn = null;// DataAccess.GetConnectionNo();
            IDbTransaction _tran = null;
            try
            {
                _conn = DataAccess.GetConnectionNo();
                _tran = _conn.BeginTransaction();
                //cap nhat trang thai cho tct cap nhat trang thai
                string strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Trangthai='06',TTHAI_TCT='999',NGAYHT=SYSDATE WHERE ID='" + strID + "'";
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, _tran);
                strSQL = "insert into SET_DSTK_LIENKET(id, madoitac, loai, mst, ten_nnt, loaigiayto, ";
                strSQL += " sogiayto, dienthoai, phuongthuc, sotaikhoan_the, ";
                strSQL += " ngayphathanh, ngayht, trangthai, loaiotp, magiaodich,       ngaymo, CIFNO, SOTAIKHOAN,ewalletToken,ma_cn) ";
                strSQL += " SELECT id, madoitac, loai, mst, ten_nnt, loaigiayto, ";
                strSQL += " sogiayto, dienthoai, phuongthuc, sotaikhoan_the, ";
                strSQL += " ngayphathanh, ngayht, trangthai, loaiotp, magiaodich,       ngaymo,CIFNO, SOTAIKHOAN,ewalletToken,ma_cn  ";
                strSQL += " FROM set_dstk_lienket_log a where id='" + strID + "' ";
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, _tran);
                _tran.Commit();
                return "00";
            }
            catch (Exception ex)
            {
                log.Info(" MOLIENKETTK_THANHCONG Loi ID[" + strID + "]");
                log.Error(ex.Message + "-" + ex.StackTrace);
                if (_tran != null)
                    _tran.Rollback();
            }
            finally
            {
                if (_tran != null)
                { _tran.Dispose(); }
                if (_conn != null)
                {
                    _conn.Close();
                    _conn.Dispose();
                }
            }
            return "06";
        }
        public static string MOLIENKETTK_HUY(string strID, string strIDREF)
        {
            IDbConnection _conn = null;// DataAccess.GetConnectionNo();
            IDbTransaction _tran = null;
            try
            {
                _conn = DataAccess.GetConnectionNo();
                _tran = _conn.BeginTransaction();
                //cap nhat trang thai cho tct cap nhat trang thai
                string strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Trangthai='07',TTHAI_TCT='999',NGAYHT=SYSDATE WHERE ID='" + strID + "'";
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, _tran);
                strSQL = " delete from SET_DSTK_LIENKET where id='" + strIDREF + "' ";
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, _tran);
                strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Trangthai='07' WHERE ID='" + strIDREF + "'";
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, _tran);
                _tran.Commit();
                return "00";
            }
            catch (Exception ex)
            {
                log.Info(" MOLIENKETTK_HUY Loi ID[" + strID + "] IDREF[" + strIDREF + "]");
                log.Error(ex.Message + "-" + ex.StackTrace);
                if (_tran != null)
                    _tran.Rollback();
            }
            finally
            {
                if (_tran != null)
                { _tran.Dispose(); }
                if (_conn != null)
                {
                    _conn.Close();
                    _conn.Dispose();
                }
            }
            return "06";
        }
        public static OTPInfo getThongtin_XACNHAN_GiaoDich(string strMaGD)
        {
            OTPInfo result = new OTPInfo();
            try
            {
                //kiem tra xem co phai la dang ky moi khong
                string strSQL = "SELECT a.id,Loai,IDREF,MST   FROM set_dstk_lienket_log a  where  a.trangthai='05' and  a.magiaodich='" + strMaGD + "' and ngayht>(sysdate-1) ";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            result.ID = ds.Tables[0].Rows[0]["ID"].ToString();
                            result.LoaiGD = ds.Tables[0].Rows[0]["LOAI"].ToString();
                            result.IDREF = ds.Tables[0].Rows[0]["IDREF"].ToString();
                            result.MST = ds.Tables[0].Rows[0]["MST"].ToString();
                            return result;
                        }
                //kiem tra xem co phai la dang ky huy khong
                strSQL = "SELECT a.maThamChieu,MSGCONTENT,MST  FROM SET_CTU_HDR_MOBITCT a  where  a.TRANIDOTP='" + strMaGD + "' and ngaymsg>(sysdate-1) and trangthai='05' ";
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            //if (ds.Tables[0].Rows[0]["LOAI"].ToString().Equals("Open"))
                            result.ID = ds.Tables[0].Rows[0]["maThamChieu"].ToString();
                            result.LoaiGD = "CTU";
                            result.MSG = ds.Tables[0].Rows[0]["MSGCONTENT"].ToString();
                            result.MST = ds.Tables[0].Rows[0]["MST"].ToString();
                            return result;
                        }
                //kiem tra xem co phai la lenh thanh toan ko
            }
            catch (Exception ex)
            { log.Error(ex.Message + "-" + ex.StackTrace); }
            return result;
        }
        public static void MOLIENKETTK_OTP(string strID, string loaiGD, string ewalletToken)
        {
            try
            {
                string strSQL = "";
                if (loaiGD.Equals("Open"))
                {
                    //cap nhat lai ma giao dich va trang thai cho tct xac nhan
                    strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET trangthai='05',NGAYHT=SYSDATE,ewalletToken='" + ewalletToken + "'  WHERE ID='" + strID + "'";
                }
                else
                {
                    //cap nhat lai ma giao dich va trang thai cho tct xac nhan
                    strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET trangthai='05',NGAYHT=SYSDATE WHERE ID='" + strID + "'";
                }

                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                //strSQL = "UPDATE SET_DSTK_LIENKET_HUY SET Magiaodich='" + strMaGD + "',NGAYHT=SYSDATE,loaiotp='" + strLoaiOTP + "' WHERE ID='" + strID + "'";
                //DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
        }
        /// <summary>
        /// tao ho so huy lien ket
        /// </summary>
        /// <param name="strID"> ID log cho ho so huy</param>
        /// <param name="strIDRef">ID tham chieu cho ho so dang active</param>
        /// <returns></returns>
        public static string HuyLienKet_B1(string strID, string strIDRef, string strMST)
        {
            IDbConnection _conn = null;// DataAccess.GetConnectionNo();
            IDbTransaction _tran = null;
            try
            {
                _conn = DataAccess.GetConnectionNo();
                _tran = _conn.BeginTransaction();
                //cap nhat trang thai cho tct cap nhat trang thai
                string strSQL = "INSERT INTO set_dstk_lienket_log (id, ";
                strSQL += "  madoitac, ";
                strSQL += "    loai, ";
                strSQL += "  mst, ";
                strSQL += "    ten_nnt, ";
                strSQL += "    loaigiayto, ";
                strSQL += "    sogiayto, ";
                strSQL += "    dienthoai, ";
                strSQL += "    phuongthuc, ";
                strSQL += "    sotaikhoan_the, ";
                strSQL += "    ngayphathanh, ";
                strSQL += "    ngayht, ";
                strSQL += "    trangthai, ";
                strSQL += "    loaiotp, ";
                strSQL += "    magiaodich, ";
                strSQL += "    ngaymo, ";
                strSQL += "    CIFNO, ";
                strSQL += "    SOTAIKHOAN, ";
                strSQL += "    ewalletToken, ";
                strSQL += "    ma_cn, ";
                strSQL += "    idref ";
                strSQL += "   ) ";
                strSQL += "  SELECT '" + strID + "', ";
                strSQL += "  madoitac, ";
                strSQL += "  'Close', ";
                strSQL += "  mst, ";
                strSQL += "  ten_nnt, ";
                strSQL += "  loaigiayto, ";
                strSQL += "  sogiayto, ";
                strSQL += "  dienthoai, ";
                strSQL += "  phuongthuc, ";
                strSQL += "  sotaikhoan_the, ";
                strSQL += "  ngayphathanh, ";
                strSQL += "  sysdate, ";
                strSQL += "  '01', ";
                strSQL += "  NULL, ";
                strSQL += "  NULL, ";
                strSQL += "  SYSDATE, ";
                strSQL += "  CIFNO, ";
                strSQL += "  SOTAIKHOAN, ";
                strSQL += "  ewalletToken, ";
                strSQL += "  ma_cn, ";
                strSQL += "  ID ";
                strSQL += "  FROM SET_DSTK_LIENKET a ";
                strSQL += "  WHERE a.id = '" + strIDRef + "' AND mst = '" + strMST + "'";

                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, _tran);
                _tran.Commit();
                return "";
            }
            catch (Exception ex)
            {
                log.Info(" HuyLienKet_B1 Loi ID[" + strID + "]");
                log.Error(ex.Message + "-" + ex.StackTrace);
                if (_tran != null)
                    _tran.Rollback();
            }
            finally
            {
                if (_tran != null)
                { _tran.Dispose(); }
                if (_conn != null)
                {
                    _conn.Close();
                    _conn.Dispose();
                }
            }
            return "06";
        }

        public static bool CheckExitsCTU(string maThamChieu)
        {
            try
            {
                string strSQL = "select * from set_ctu_hdr_mobitct a  where a.mathamchieu='" + maThamChieu + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            return false;
                return true;
            }
            catch (Exception ex)
            {
                log.Info(" CheckExitsCTU Loi ID[" + maThamChieu + "]");
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
            return false;
        }

        public static bool CheckMoney(THANHTOANMOBI_IN objIn)
        {
            String Sotien = objIn.soTien;
        
            Decimal TongSOTIENCT = 0;
            try
            {
                foreach (PhiLePhi phiItem in objIn.thongTinBienLai.phiLePhi)
                {

                    TongSOTIENCT = TongSOTIENCT + Decimal.Parse(phiItem.soTien);
                }

                if (Decimal.Parse(Sotien) != TongSOTIENCT)
                {
                    return false;
                }
            }catch (Exception ex){

                return false;
            }

            return true;
        }
        public static string InsertCTU_B1(THANHTOANMOBI_IN objIn, string strMaCN)
        {
            IDbConnection _conn = null;// DataAccess.GetConnectionNo();
            IDbTransaction _tran = null;
            string strID = "";
            try
            {
                strID = objIn.maThamChieu;
                _conn = DataAccess.GetConnectionNo();
                _tran = _conn.BeginTransaction();
                List<IDbDataParameter> lst = new List<IDbDataParameter>();
                //List<IDataParameter> lst = new List<IDataParameter>();
                string strMaCQQD = "";
                string strTENCOQUANQD = "";
                string strSOQD = "";
                string strNGAYQD = "";
                string strSOKHUNG = "";
                string strSOMAY = "";
                string strMaDBHC = "";
                foreach (PhiLePhi phiItem in objIn.thongTinBienLai.phiLePhi)
                {
                    if (strSOKHUNG.Length == 0)
                    {
                        strSOKHUNG = phiItem.GetSO_KHUNG();
                        strSOMAY = phiItem.GetSO_MAY();
                    }
                    if (strMaDBHC.Length <= 0)
                    {
                        strMaDBHC = phiItem.GetMA_DBHC();
                        if (strMaDBHC.Trim().Length <= 0)
                        {
                            strMaDBHC = "00000";
                        }
                    }
                }
                string strLog = "";
                //insert hdr
                string strSQL = "INSERT INTO SET_CTU_HDR_MOBITCT( ";
                strSQL += " MADOITAC, ";
                strSQL += " MATHAMCHIEU, ";
                strSQL += " SOTIEN, ";
                strSQL += " PHICHUYENPHAT, ";
                strSQL += " NGONNGU, ";
                strSQL += " MATIENTE, ";
                strSQL += " THONGTINGIAODICH, ";
                strSQL += " MADICHVU, ";
                strSQL += " MAHOSO, ";
                strSQL += " TKTHUHUONG, ";
                strSQL += " MALOAIHINHTHUPHAT, ";
                strSQL += " MALOAIHINHTHUE, ";
                strSQL += " TENLOAIHINHTHUE, ";
                strSQL += " MACQTHU, ";
                strSQL += " TENCQTHU, ";
                strSQL += " SHKB, ";
                strSQL += " MACOQUANQD, ";
                strSQL += " TENCOQUANQD, ";
                strSQL += " SOQD, ";
                strSQL += " NGAYQD, ";
                strSQL += " MST, ";
                strSQL += " HOTENNGUOINOP, ";
                strSQL += " SOCMNDNGUOINOP, ";
                strSQL += " DIACHINGUOINOP, ";
                strSQL += " HUYENNGUOINOP, ";
                strSQL += " TINHNGUOINOP, ";
                strSQL += " mstNopThay, ";
                strSQL += " hoTenNguoiNopThay, ";
                strSQL += " diaChiNguoiNopThay, ";
                strSQL += " huyenNguoiNopThay, ";
                strSQL += " tinhNguoiNopThay, ";
                strSQL += " DSKHOANNOP, ";
                strSQL += " MSGCONTENT, ";
                strSQL += " MA_NNTIEN, ";
                strSQL += " TEN_NNTIEN, ";
                strSQL += " TRANGTHAI, ";
                strSQL += " NGAY_HT, ";
                strSQL += " NGAYMSG, ";
                strSQL += " SOKHUNG, ";
                strSQL += " SOMAY, ";
                strSQL += " MADBHC, ";
                strSQL += " THOIGIANGD, ";
                strSQL += " MANGANHANG, ";
                strSQL += " PHUONGTHUC, ";
                strSQL += " SOTAIKHOAN_THE, ";
                strSQL += " SOTAIKHOAN, ";
                strSQL += " IDTAIKHOANLIENKET, ";
                strSQL += " MA_CN, ";
                strSQL += " SOTAIKHOAN_THE_DECRYPT, ";
                strSQL += " EWALLETTOKEN ";
                strSQL += ")VALUES(";
                strLog = strSQL;
                strSQL += " :MADOITAC, ";
                strLog += "'" + objIn.maDoiTac + "'";
                lst.Add(DataAccess.NewDBParameter("MADOITAC", ParameterDirection.Input, objIn.maDoiTac, DbType.String));
                strSQL += " :MATHAMCHIEU, ";
                lst.Add(DataAccess.NewDBParameter("MATHAMCHIEU", ParameterDirection.Input, objIn.maThamChieu, DbType.String));
                strSQL += " :SOTIEN, ";
                lst.Add(DataAccess.NewDBParameter("SOTIEN", ParameterDirection.Input, objIn.soTien, DbType.String));
                strSQL += " :PHICHUYENPHAT, ";
                lst.Add(DataAccess.NewDBParameter("PHICHUYENPHAT", ParameterDirection.Input, objIn.phiChuyenPhat, DbType.String));
                strSQL += " :NGONNGU, ";
                lst.Add(DataAccess.NewDBParameter("NGONNGU", ParameterDirection.Input, objIn.ngonNgu, DbType.String));
                strSQL += " :MATIENTE, ";
                lst.Add(DataAccess.NewDBParameter("MATIENTE", ParameterDirection.Input, objIn.maTienTe, DbType.String));
                strSQL += " :THONGTINGIAODICH, ";
                lst.Add(DataAccess.NewDBParameter("THONGTINGIAODICH", ParameterDirection.Input, objIn.thongTinGiaoDich, DbType.String));
                strSQL += " :MADICHVU, ";
                lst.Add(DataAccess.NewDBParameter("MADICHVU", ParameterDirection.Input, objIn.thongTinBienLai.maDichVu, DbType.String));
                strSQL += " :MAHOSO, ";
                if (objIn.thongTinBienLai.maHoSo != null)
                    lst.Add(DataAccess.NewDBParameter("MAHOSO", ParameterDirection.Input, objIn.thongTinBienLai.maHoSo, DbType.String));
                else
                    lst.Add(DataAccess.NewDBParameter("MAHOSO", ParameterDirection.Input, "", DbType.String));
                strSQL += " :TKTHUHUONG, ";
                lst.Add(DataAccess.NewDBParameter("TKTHUHUONG", ParameterDirection.Input, objIn.thongTinBienLai.tkthuHuong, DbType.String));
                strSQL += " :MALOAIHINHTHUPHAT, ";
                lst.Add(DataAccess.NewDBParameter("MALOAIHINHTHUPHAT", ParameterDirection.Input, objIn.thongTinBienLai.maLoaiHinhThuPhat, DbType.String));
                strSQL += " :MALOAIHINHTHUE, ";
                lst.Add(DataAccess.NewDBParameter("MALOAIHINHTHUE", ParameterDirection.Input, objIn.thongTinBienLai.maLoaiHinhThue, DbType.String));
                strSQL += " :TENLOAIHINHTHUE, ";
                lst.Add(DataAccess.NewDBParameter("TENLOAIHINHTHUE", ParameterDirection.Input, objIn.thongTinBienLai.tenLoaiHinhThue, DbType.String));
                strSQL += " :MACQTHU, ";
                lst.Add(DataAccess.NewDBParameter("MACQTHU", ParameterDirection.Input, objIn.thongTinBienLai.maCoQuanQD, DbType.String));
                strSQL += " :TENCQTHU, ";
                lst.Add(DataAccess.NewDBParameter("TENCQTHU", ParameterDirection.Input, objIn.thongTinBienLai.tenCoQuanQD, DbType.String));
                strSQL += " :SHKB, ";
                lst.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, objIn.thongTinBienLai.khoBac, DbType.String));
                strSQL += " :MACOQUANQD, ";
                lst.Add(DataAccess.NewDBParameter("MACOQUANQD", ParameterDirection.Input, strMaCQQD, DbType.String));
                strSQL += " :TENCOQUANQD, ";
                lst.Add(DataAccess.NewDBParameter("TENCOQUANQD", ParameterDirection.Input, strTENCOQUANQD, DbType.String));
                strSQL += " :SOQD, ";
                lst.Add(DataAccess.NewDBParameter("SOQD", ParameterDirection.Input, objIn.thongTinBienLai.soQD, DbType.String));


                strNGAYQD = objIn.thongTinBienLai.ngayQD;
                if (strNGAYQD.Trim().Length <= 0)
                {
                    strSQL += " :NGAYQD, ";
                    lst.Add(DataAccess.NewDBParameter("NGAYQD", ParameterDirection.Input, DBNull.Value, DbType.String));
                }
                else
                {
                    strSQL += " to_date(:NGAYQD,'DD/MM/YYYY'), ";
                    if (strNGAYQD.IndexOf("-") > 0)
                    {
                        string[] arr = strNGAYQD.Split('-');
                        strNGAYQD = arr[2] + "/" + arr[1] + "/" + arr[0];

                    }
                    lst.Add(DataAccess.NewDBParameter("NGAYQD", ParameterDirection.Input, strNGAYQD, DbType.String));
                }


                strSQL += " :MST, ";
                lst.Add(DataAccess.NewDBParameter("MST", ParameterDirection.Input, objIn.thongTinBienLai.mst, DbType.String));
                strSQL += " :HOTENNGUOINOP, ";
                lst.Add(DataAccess.NewDBParameter("HOTENNGUOINOP", ParameterDirection.Input, objIn.thongTinBienLai.hoTenNguoiNop, DbType.String));
                //strSQL += " :SOCMNDNGUOINOP, ";
                //lst.Add(DataAccess.NewDBParameter("SOCMNDNGUOINOP", ParameterDirection.Input, objIn.thongTinBienLai.soCMNDNguoiNop, DbType.String));
                strSQL += " :SOCMNDNGUOINOP, ";
                if (objIn.thongTinBienLai.soCMNDNguoiNop != null)
                    lst.Add(DataAccess.NewDBParameter("SOCMNDNGUOINOP", ParameterDirection.Input, objIn.thongTinBienLai.soCMNDNguoiNop, DbType.String));
                else
                    lst.Add(DataAccess.NewDBParameter("SOCMNDNGUOINOP", ParameterDirection.Input, "", DbType.String));
                strSQL += " :DIACHINGUOINOP, ";
                lst.Add(DataAccess.NewDBParameter("DIACHINGUOINOP", ParameterDirection.Input, objIn.thongTinBienLai.diaChiNguoiNop, DbType.String));
                strSQL += " :HUYENNGUOINOP, ";
                lst.Add(DataAccess.NewDBParameter("HUYENNGUOINOP", ParameterDirection.Input, objIn.thongTinBienLai.huyenNguoiNop, DbType.String));
                strSQL += " :TINHNGUOINOP, ";
                lst.Add(DataAccess.NewDBParameter("TINHNGUOINOP", ParameterDirection.Input, objIn.thongTinBienLai.tinhNguoiNop, DbType.String));
                strSQL += " :mstNopThay, ";
                lst.Add(DataAccess.NewDBParameter("mstNopThay", ParameterDirection.Input, objIn.thongTinBienLai.mstNopThay, DbType.String));
                strSQL += " :hoTenNguoiNopThay, ";
                lst.Add(DataAccess.NewDBParameter("hoTenNguoiNopThay", ParameterDirection.Input, objIn.thongTinBienLai.hoTenNguoiNopThay, DbType.String));
                strSQL += " :diaChiNguoiNopThay, ";
                lst.Add(DataAccess.NewDBParameter("diaChiNguoiNopThay", ParameterDirection.Input, objIn.thongTinBienLai.diaChiNguoiNopThay, DbType.String));
                strSQL += " :huyenNguoiNopThay, ";
                lst.Add(DataAccess.NewDBParameter("huyenNguoiNopThay", ParameterDirection.Input, objIn.thongTinBienLai.huyenNguoiNopThay, DbType.String));
                strSQL += " :tinhNguoiNopThay, ";
                lst.Add(DataAccess.NewDBParameter("tinhNguoiNopThay", ParameterDirection.Input, objIn.thongTinBienLai.tinhNguoiNopThay, DbType.String));
                strSQL += " :DSKHOANNOP, ";
                lst.Add(DataAccess.NewDBParameter("DSKHOANNOP", ParameterDirection.Input, Newtonsoft.Json.JsonConvert.SerializeObject(objIn.thongTinBienLai.dskhoanNop), DbType.String));
                strSQL += " :MSGCONTENT, ";
                lst.Add(DataAccess.NewDBParameter("MSGCONTENT", ParameterDirection.Input, Newtonsoft.Json.JsonConvert.SerializeObject(objIn), DbType.String));
                strSQL += " :MA_NNTIEN, ";
                lst.Add(DataAccess.NewDBParameter("MA_NNTIEN", ParameterDirection.Input, objIn.ma_NNTien, DbType.String));
                strSQL += " :TEN_NNTIEN, ";
                lst.Add(DataAccess.NewDBParameter("TEN_NNTIEN", ParameterDirection.Input, objIn.ten_NNTien, DbType.String));
                strSQL += " '01', ";
                strSQL += " SYSDATE, ";
                strSQL += " SYSDATE, ";
                strSQL += " :SOKHUNG, ";
                lst.Add(DataAccess.NewDBParameter("SOKHUNG", ParameterDirection.Input, strSOKHUNG, DbType.String));
                strSQL += " :SOMAY, ";
                lst.Add(DataAccess.NewDBParameter("SOMAY", ParameterDirection.Input, strSOMAY, DbType.String));
                strSQL += " :MADBHC, ";
                lst.Add(DataAccess.NewDBParameter("MADBHC", ParameterDirection.Input, strMaDBHC, DbType.String));
                strSQL += " TO_DATE(:THOIGIANGD,'YYYYMMDDHH24MISS'), ";
                lst.Add(DataAccess.NewDBParameter("THOIGIANGD", ParameterDirection.Input, objIn.thoiGianGD, DbType.String));
                strSQL += " :MANGANHANG, ";
                lst.Add(DataAccess.NewDBParameter("MANGANHANG", ParameterDirection.Input, objIn.maNganHang, DbType.String));
                strSQL += " :PHUONGTHUC, ";
                lst.Add(DataAccess.NewDBParameter("PHUONGTHUC", ParameterDirection.Input, objIn.phuongThuc, DbType.String));
                strSQL += " :SOTAIKHOAN_THE, ";
                lst.Add(DataAccess.NewDBParameter("SOTAIKHOAN_THE", ParameterDirection.Input, objIn.soTaiKhoan_The, DbType.String));
                strSQL += " :SOTAIKHOAN, ";
                lst.Add(DataAccess.NewDBParameter("SOTAIKHOAN", ParameterDirection.Input, objIn.SoTaiKhoan, DbType.String));
                strSQL += " :IDTAIKHOANLIENKET, ";
                lst.Add(DataAccess.NewDBParameter("IDTAIKHOANLIENKET", ParameterDirection.Input, objIn.IdTaiKhoanLienKet, DbType.String));
                strSQL += " :MA_CN, ";
                lst.Add(DataAccess.NewDBParameter("MA_CN", ParameterDirection.Input, strMaCN, DbType.String));
                strSQL += " :SOTAIKHOAN_THE_DECRYPT, ";
                lst.Add(DataAccess.NewDBParameter("SOTAIKHOAN_THE_DECRYPT", ParameterDirection.Input, objIn.soTaiKhoanThe_Decrypt, DbType.String));
                strSQL += " :EWALLETTOKEN ";
                lst.Add(DataAccess.NewDBParameter("EWALLETTOKEN", ParameterDirection.Input, objIn.ewalletToken, DbType.String));
                strSQL += ")";


                //string logSQL = VBOracleLib.DataAccess.ConvertToSql(strSQL, lst);
                //log.Info("InsertCTU_B1.logSQL: " + logSQL);
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray(), _tran);
                foreach (PhiLePhi phiItem in objIn.thongTinBienLai.phiLePhi)
                {
                    lst = new List<IDbDataParameter>();
                    strSQL = "INSERT INTO SET_CTU_DTL_MOBITCT(";
                    strSQL += " MATHAMCHIEU, ";
                    strSQL += " LOAIPHILEPHI, ";
                    strSQL += " MAPHILEPHI, ";
                    strSQL += " TENPHILEPHI, ";
                    strSQL += " SOTIEN, ";
                    strSQL += " MACHUONG, ";
                    strSQL += " MATMUC, ";
                    strSQL += " SOKHUNG, ";
                    strSQL += " SOMAY, ";
                    strSQL += " SOQD, ";
                    strSQL += " NGAYQD, ";
                    strSQL += " MADBHC, ";
                    strSQL += " THOIGIANGD ";
                    strSQL += ")Values(";
                    strSQL += " :MATHAMCHIEU, ";
                    lst.Add(DataAccess.NewDBParameter("MATHAMCHIEU", ParameterDirection.Input, objIn.maThamChieu, DbType.String));
                    strSQL += " :LOAIPHILEPHI, ";
                    lst.Add(DataAccess.NewDBParameter("LOAIPHILEPHI", ParameterDirection.Input, phiItem.loaiPhiLePhi, DbType.String));
                    strSQL += " :MAPHILEPHI, ";
                    lst.Add(DataAccess.NewDBParameter("MAPHILEPHI", ParameterDirection.Input, phiItem.maPhiLePhi, DbType.String));
                    strSQL += " :TENPHILEPHI, ";
                    lst.Add(DataAccess.NewDBParameter("TENPHILEPHI", ParameterDirection.Input, phiItem.tenPhiLePhi, DbType.String));
                    strSQL += " :SOTIEN, ";
                    lst.Add(DataAccess.NewDBParameter("SOTIEN", ParameterDirection.Input, phiItem.soTien, DbType.String));
                    strSQL += " :MACHUONG, ";
                    lst.Add(DataAccess.NewDBParameter("MACHUONG", ParameterDirection.Input, phiItem.GetMaChuong(), DbType.String));
                    strSQL += " :MATMUC, ";
                    lst.Add(DataAccess.NewDBParameter("MATMUC", ParameterDirection.Input, phiItem.GetMaTM(), DbType.String));
                    strSQL += " :SOKHUNG, ";
                    lst.Add(DataAccess.NewDBParameter("SOKHUNG", ParameterDirection.Input, phiItem.GetSO_KHUNG(), DbType.String));
                    strSQL += " :SOMAY, ";
                    lst.Add(DataAccess.NewDBParameter("SOMAY", ParameterDirection.Input, phiItem.GetSO_MAY(), DbType.String));
                    strSQL += " :SOQD, ";
                    lst.Add(DataAccess.NewDBParameter("SOQD", ParameterDirection.Input, phiItem.GetSO_QUYET_DINH(), DbType.String));
                    strSQL += " to_date(:NGAYQD,'DD/MM/YYYY'), ";
                    strNGAYQD = phiItem.GetNGAY_QUYET_DINH();

                    if (strNGAYQD.IndexOf("-") > 0)
                    {
                        string[] arr = strNGAYQD.Split('-');
                        strNGAYQD = arr[2] + "/" + arr[1] + "/" + arr[0];

                    }
                    lst.Add(DataAccess.NewDBParameter("NGAYQD", ParameterDirection.Input, strNGAYQD, DbType.String));
                    strSQL += " :MADBHC, ";
                    lst.Add(DataAccess.NewDBParameter("MADBHC", ParameterDirection.Input, phiItem.GetMA_DBHC(), DbType.String));
                    strSQL += " TO_DATE(:THOIGIANGD,'YYYYMMDDHH24MISS') ";
                    lst.Add(DataAccess.NewDBParameter("THOIGIANGD", ParameterDirection.Input, objIn.thoiGianGD, DbType.String));
                    strSQL += " )";
                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray(), _tran);

                }
                _tran.Commit();
                return "";
            }
            catch (Exception ex)
            {
                log.Info(" HuyLienKet_B1 Loi ID[" + strID + "]");
                log.Error(ex.Message + "-" + ex.StackTrace);
                if (_tran != null)
                    _tran.Rollback();
            }
            finally
            {
                if (_tran != null)
                { _tran.Dispose(); }
                if (_conn != null)
                {
                    _conn.Close();
                    _conn.Dispose();
                }
            }
            return "06";
        }

        public static bool UpdateThongtinOTPCTU(string strID, string strMaGD, string strLoaiOTP)
        {
            try
            {
                //cap nhat lai ma giao dich va trang thai cho tct xac nhan
                string strSQL = "UPDATE SET_CTU_HDR_MOBITCT SET TRANIDOTP='" + strMaGD + "',trangthai='02',NGAY_HT=SYSDATE,loaiotp='" + strLoaiOTP + "' WHERE MATHAMCHIEU='" + strID + "'";
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
            return false;
        }
        public static void XoaDSKHOANNOP_kosinhdcOTP(string strID, string StrGhiChu = "")
        {
            try
            {
                string strSQL = "UPDATE SET_CTU_HDR_MOBITCT SET Trangthai='03',NGAY_HT=SYSDATE,ghichu=:ghichu WHERE MATHAMCHIEU='" + strID + "'";
                List<IDataParameter> lst = new List<IDataParameter>();
                if (StrGhiChu.Length <= 500)
                    lst.Add(DataAccess.NewDBParameter("ghichu", ParameterDirection.Input, StrGhiChu, DbType.String));
                else
                    lst.Add(DataAccess.NewDBParameter("ghichu", ParameterDirection.Input, StrGhiChu.Substring(0, 500), DbType.String));
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray());
                //  DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                //strSQL = "UPDATE SET_DSTK_LIENKET_HUY SET TRANGTHAI='03',NGAYHT=SYSDATE WHERE ID='" + strID + "'";
                //DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
        }

        public static void SoTien_NNT(string strMaThamChieu, ref string strReturn)
        {
            try
            {
                string strUserID = ConfigurationManager.AppSettings.Get("CUSTOMS.Ma_AccountHQ247");
                string strSQL = "SELECT A.SOTIEN";
                strSQL += " FROM SET_CTU_HDR_MOBITCT A WHERE A.MATHAMCHIEU='" + strMaThamChieu + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    strReturn = ds.Tables[0].Rows[0]["SOTIEN"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string THANHTOAN_B3_TAO_CTU(THANHTOANMOBI_IN objIn, ref string pvStrErrMSG, ref string pvStrSOCTU)
        {
            try
            {
                infChungTuHDR objCTuHDR = new infChungTuHDR();
                List<infChungTuDTL> objCTuDTL = new List<infChungTuDTL>();

                CorebankServiceESB.clsCoreBank clscorebank = new CorebankServiceESB.clsCoreBank();
                CorebankServiceESB.MOBI.AccountResp v_objAcc = clscorebank.GetTK_XACTHUCNTT(objIn.SoTaiKhoan, objIn.phuongThuc);
                // check loaits
                foreach (var item in objIn.thongTinBienLai.dskhoanNop)
                {
                    if (!string.IsNullOrEmpty(item.loaiTs))
                    {
                        objCTuHDR.strLoaiTS = item.loaiTs;
                    }
                    objCTuHDR.noiDung = item.noiDung;

                }
                bool checkThuSHB = false;
                setObject21ToCTU(objIn.maThamChieu, ref objCTuHDR, ref objCTuDTL, v_objAcc, ref checkThuSHB);
                pvStrSOCTU = objCTuHDR.So_CT;
                //kiem tra thong tin
                //danh muc ngan hang truc tiep gian tiep
                if (objCTuHDR.MA_NH_B.Length <= 0)
                {
                    pvStrErrMSG = "Thiếu danh mục ngân hàng hưởng";
                    return "112";
                }
                //neu insert chung tu loi thi cap nhat trang thai huy chung tu
                try
                {
                    Insert_New0021(objIn.maThamChieu, objCTuHDR, objCTuDTL);
                }
                catch (Exception ex)
                {
                    pvStrErrMSG = "Lỗi khi insert chung tu";
                    return "114";
                }
              
                //string strRemark = objCTuHDR.REMARKS;

                string CCY = "704";
                string REMARK = objCTuHDR.REMARKS;
                string ngay_ht = "";

                if (objIn.maTienTe.ToUpper().Equals("VND"))
                {
                    CCY = "704";
                }
                else if (objIn.maTienTe.ToUpper().Equals("USD"))
                {
                    CCY = "840";
                }

                ngay_ht = clscorebank.Get_VALUE_DATE();

                ngay_ht = ConvertNumberToDate(ngay_ht).ToString("dd/MM/yyyy");

               
                string ten_kb = Globals.RemoveSign4VietnameseString(objCTuHDR.Ten_kb);
                if (ten_kb.Length > 100)
                {
                    ten_kb = ten_kb.Substring(0, 100);
                }
                else
                {
                    ten_kb = ten_kb.PadRight(100);
                }
                //Kiểm tra hạch toán đi citad hay đi TTSP
                //song phuong o day
                //string strRefCoreTtsp = "";
                string tt_kb = "";
                string ten_tt_kb = "";

                string errorCode = "";
                //đoạn này lấy từ NTDT để chỉnh lại cho etax mobile
                string ngay_GD_SP = ConvertNumberToDate(ngay_ht).ToString("yyyyMMdd");
                string TransactionDate = ConvertDateToNumber(DateTime.Now.ToString("dd/MM/yyyy")).ToString();
                string KENH_HT = "MOBILE";
                string v_strErrorCode = "";
                string v_strRef_No = "";

                //check thêm số dư trước khi hạch toán
                // kiểm tra số dư
                string maloi = "";
                string sodu_KD = "";
                string nguyente_tk = "";
                string branch_tk = objCTuHDR.MA_CN_TK;
                string v_strMa_NV = "";
                string v_strMa_KSV = "";
                string BEN_CITY = "";  //quận huyện người nộp tiền - tên tỉnh
                string BANK_BEN_DESC = "";
                string BR_BEN_DESC = "";
                string CITY_BEN_DESC = "";
                string BANK_BEN_CODE = "";
                string BR_BEN_CODE = "";
                string CITY_BEN_CODE = "";


                CITY_BEN_DESC = GetTinhThanhNHTH(objCTuHDR.MA_TINH_KBNN);

                BANK_BEN_DESC = objCTuHDR.Ten_NH_B;
                BR_BEN_DESC = objCTuHDR.Ten_NH_B;
                BANK_BEN_CODE = objCTuHDR.MA_NH_B.ToString().Substring(2, 3);
                CITY_BEN_CODE = objCTuHDR.MA_NH_B.ToString().Substring(0, 2);
                BR_BEN_CODE = objCTuHDR.MA_NH_B.ToString().Substring(5, 3);

                string thong_tin_nnt_duoc_nop_thay = "";
                if (objCTuHDR.Ma_NNTien.Trim().Length > 0)
                {
                    thong_tin_nnt_duoc_nop_thay = "[nop thay cho mst:]" + objCTuHDR.Ma_NNTien + "-" + objCTuHDR.Ten_NNTien;
                }

                if (checkThuSHB == true)
                {
                    //cap nhat TTSP la 1
                    // Utility.Log_Action("checkThuSHB", "TRUE");

                    string strSQL = "UPDATE TCS_CTU_HDR_MOBITCT SET TT_SP='1' WHERE so_ct='" + objCTuHDR.So_CT + "' ";
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);

                    //Gọi hàm thanh toán mới vào kho bạc SHB
                    clscorebank.PaymentSP(KENH_HT, ngay_GD_SP, objCTuHDR.So_CT, "110000", branch_tk, objCTuHDR.TK_KH_NH, objCTuHDR.TTien_NT.ToString(), objCTuHDR.Ma_NT, "", REMARK,
                             v_strMa_NV, v_strMa_KSV, "MOBILE", objCTuHDR.TK_Co, objCTuHDR.MA_NH_B.ToString().Substring(2, 3), objCTuHDR.MA_NH_B, objCTuHDR.ACC_NHB, objCTuHDR.MA_CN_SP, ref v_strErrorCode, ref v_strRef_No);
                    //Utility.Log_Action("PaymentSP", objhdr.MA_HIEU_KBAC);
                }
                else
                {
                    
                    if (Core_version.Equals("2"))
                    {
                        //Core V2.0 20180330---------------------------
                        string EcomStr = String.Empty;

                        //int v_count = 0;
                        foreach (infChungTuDTL objDTL in objCTuDTL)
                        {
                            // 'Kienvt : chi insert cac truong <>0
                            if (objDTL != null)
                                if (objDTL.SoTien > 0)
                                {
                                    EcomStr += builecomchitiet_V20(objDTL);
                                    //if (v_count != 0)
                                    //{
                                    EcomStr += "####";
                                    //}
                                    //v_count++;
                                }
                        }
                    

                        REMARK = V_SubString(REMARK, 210);
                        string str_TenMST = V_SubString(objCTuHDR.Ten_NNThue, 200);
                        string str_MST = V_SubString(objCTuHDR.Ma_NNThue, 14);
                        string str_MaLThue = V_SubString(objCTuHDR.Ma_LThue, 2);
                        //Ngay nop thue dinh dang dd-MM-yyyy
                        string str_NNT = DateTime.Now.ToString("yyyyMMdd");
                        string str_AddNNT = V_SubString(objCTuHDR.DC_NNThue, 200);
                        string strMaCQT = V_SubString(objCTuHDR.Ma_CQThu, 7);
                        string strTenCQT = V_SubString(objCTuHDR.Ten_cqthu, 200);
                        string strMaDBHC = V_SubString(objCTuHDR.MA_DBHC_KBNN, 5);
                        string strTenDBHC = V_SubString(VBOracleLib.Globals.RemoveSign4VietnameseString(objCTuHDR.Ten_Xa), 200);
                        clscorebank.Payment_V20(objCTuHDR.TK_KH_NH, objCTuHDR.TK_Co, objCTuHDR.TTien_NT.ToString(), CCY, REMARK, TransactionDate, VBOracleLib.Globals.RemoveSign4VietnameseString(objCTuHDR.Ten_cqthu).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_CITY),
                                                 VBOracleLib.Globals.RemoveSign4VietnameseString(objCTuHDR.Ten_cqthu).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(objCTuHDR.Ten_cqthu).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BANK_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BR_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC), BANK_BEN_CODE, BR_BEN_CODE, CITY_BEN_CODE,
                                                 KENH_HT, objCTuHDR.So_CT, objCTuHDR.MA_NH_B.ToString(), v_strMa_NV, v_strMa_KSV, "MOBILE", thong_tin_nnt_duoc_nop_thay,
                                                 str_NNT, str_MaLThue, str_MST, strMaCQT, strTenCQT, strMaDBHC, strTenDBHC, str_TenMST, str_AddNNT, EcomStr,
                                                 ref v_strErrorCode, ref v_strRef_No);

                        //Core V2.0 20180330--------------------------------
                    }
                    else
                    {
                        clscorebank.Payment(objCTuHDR.TK_KH_NH, objCTuHDR.TK_Co, objCTuHDR.TTien_NT.ToString(), CCY, REMARK, TransactionDate, VBOracleLib.Globals.RemoveSign4VietnameseString(objCTuHDR.Ten_cqthu).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_CITY),
                                                 VBOracleLib.Globals.RemoveSign4VietnameseString(objCTuHDR.Ten_cqthu).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(objCTuHDR.Ten_cqthu).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BANK_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BR_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC), BANK_BEN_CODE, BR_BEN_CODE, CITY_BEN_CODE,
                                                 KENH_HT, objCTuHDR.So_CT, objCTuHDR.MA_NH_B.ToString(), v_strMa_NV, v_strMa_KSV, "MOBILE", thong_tin_nnt_duoc_nop_thay, ref v_strErrorCode, ref v_strRef_No);
                    }
                }

                if (v_strErrorCode != null && v_strErrorCode.Equals("00000"))
                {
                    // hạch toán thành công
                    //nếu thành công thì gửi TCT
                    log.Info("Bắt đầu cập nhật trạng thái gd\n ttgd" + v_strErrorCode + "\n tentt:" + v_strRef_No);

                    UPDATE_CTU_THANHTOAN_THANHCONG(v_strRef_No, objCTuHDR.So_CT);
                    log.Info("Kết thúc cập nhật trạng thái:");
                    //Nếu hạch thoán thành công thì gửi đi song phương
                    if (checkThuSHB == true)
                    {
                        try
                        {
                            CorebankServiceESB.clsSongPhuong clsSP = new CorebankServiceESB.clsSongPhuong();
                            clsSP.SendChungTuSP(objCTuHDR.So_CT, "MOBILE");
                        }
                        catch (Exception ex)
                        { 
                        
                        }
                        
                    }
                    else if (checkThuSHB == false)
                    {
                        Business.CitadV25 citad25 = new Business.CitadV25();

                        string core_date = clscorebank.Get_VALUE_DATE();
                        string content_ex = citad25.CreateContentEx(objCTuHDR.So_CT, "MOBILE", core_date);
                        content_ex = Business.Common.mdlCommon.EncodeBase64_ContentEx(content_ex);
                        string v_strErrorCode_25 = "";
                        clscorebank.SendContentEx(objCTuHDR.So_CT, v_strRef_No, "MOBILE", content_ex, ref v_strErrorCode_25);
                        if (v_strErrorCode_25.Equals("00000"))
                        {
                            //cap nhap send core thanh cong: 1 la thanh cong, 0 la chua thanh cong
                            VBOracleLib.DataAccess.ExecuteNonQuery("update TCS_CTU_HDR_MOBITCT set SEND_CONTENT_EX = '1' where so_ct='" + objCTuHDR.So_CT + "'", CommandType.Text);
                        }
                    }
                }
                else
                {
                    if (v_strErrorCode.Equals("9999"))
                    {
                        //bị timeout
                        pvStrErrMSG = "Timeout";
                        return "04";
                    }
                    else
                    {
                        //tạm thời để 1 mã lỗi, sau có nhiều mã lỗi sẽ tính sau
                        pvStrErrMSG = "Giao dịch thất bại";
                        return "01";
                    }
                }

                //end đoạn hạch toán

                if (v_strErrorCode.Equals("00000"))
                {
                    //cap nhat trang thai 05
                    UPDATE_CTU_THANHTOAN_TRANGTHAI_05(objIn.maThamChieu, objCTuHDR.So_CT, v_objAcc.branchCode);
                    //tao thread gui chung tu den thue
                    Thread t = new Thread(delegate() { Prc_SendCTUToTCT(objIn.maThamChieu); });
                    t.IsBackground = true;
                    t.Start();
                    //v_strTransID = v_objPaymentResp.tranCoreRef;
                }

                return "";
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
            return "06";
        }
        public static void Prc_SendCTUToTCT(string strMaThamChieu)
        {
            try
            {
                //05: cho tct phan hôi
                //09: thanh cong- da gui chung tu di thue
                //06: cho gui chung tu di thue
                string strSQL = "select * from set_ctu_hdr_mobitct where MaThamChieu='" + strMaThamChieu + "' AND TRANGTHAI in ('06','05','09')";
                string strSo_CT = "";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            strSo_CT = ds.Tables[0].Rows[0]["so_ct"].ToString();
                        }
                if (strSo_CT.Length <= 0)
                {
                    log.Error("Prc_SendCTUToTCT mathamchieu[" + strMaThamChieu + "]: loi khi gui chung tu di thue, khong tim thay chung tu hoac tran thai chung tu khong dung");
                    return;
                }
                string strErr = "";
                string strErrMSG = "";
                ProcMegService.ProMsg.SendMSG21_MobiTCT(strMaThamChieu, strSo_CT, ref strErr, ref strErrMSG);
                if (strErr.Equals("01"))
                    Business.MobiTCT.MobiTCTCTU.UPDATE_CTU_THANHTOAN_TRANGTHAI_10(strMaThamChieu, strSo_CT);
                else
                {
                    Business.MobiTCT.MobiTCTCTU.UPDATE_CTU_THANHTOAN_TRANGTHAI_09(strMaThamChieu, strSo_CT);
                    log.Error("Prc_SendCTUToTCT mathamchieu[" + strMaThamChieu + "]: " + strErr + "-" + strErrMSG);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void Prc_SendCTUToTTSP(string strSo_ct, string type)
        {
            try
            {
                CorebankServiceESB.clsSongPhuong objClsSongPhuong = new CorebankServiceESB.clsSongPhuong();
                objClsSongPhuong.SendChungTuSP(strSo_ct, type);
            }
            catch (Exception ex)
            {
                log.Error("Prc_SendCTUToTTSP strSo_ct[" + strSo_ct + "]: " + ex.Message + "-" + ex.StackTrace);
            }
        }
        public static void UPDATE_CTU_TRANGTHAI_GUI_KHOBAC(string strSO_CT, string strTT_KB, string strTen_TT_KB)
        {
            try
            {
                string strSql = "UPDATE TCS_CTU_HDR_MOBITCT A SET A.TT_KB ='" + strTT_KB + "', A.TEN_TT_KB ='" + strTen_TT_KB + "' WHERE A.so_CT='" + strSO_CT + "'";
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
            }
            catch (Exception ex)
            { log.Error(ex.Message + "-" + ex.StackTrace); }
        }
        public static void UPDATE_CTU_TRANGTHAI_GUI_TTSP(string strSO_CT, string strTT_SP)
        {
            try
            {
                string strSql = "UPDATE TCS_CTU_HDR_MOBITCT A SET A.TT_SP ='" + strTT_SP + "' WHERE A.so_CT='" + strSO_CT + "'";
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
            }
            catch (Exception ex)
            { log.Error(ex.Message + "-" + ex.StackTrace); }
        }
        //public static void UPDATE_CTU_THANHTOAN_TRANGTHAI_09(string strMaThamCHieu,string strSO_CT)
        //{
        //    try
        //    {
        //       string strSql = "UPDATE SET_CTU_HDR_MOBITCT A SET TRANGTHAI='09', ngay_ht=sysdate WHERE A.MATHAMCHIEU='" + strMaThamCHieu + "' AND A.TRANGTHAIin('05','06','09')";
        //       DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
        //        //cap nhat chung tu tai quay ve trang thai 07 gui thue loi de sau call gui thue
        //       strSql = "UPDATE TCS_CTU_HDR A SET TRANG_THAI='07', ngay_ht=sysdate WHERE A.so_CT='" + strSO_CT + "' AND A.TRANG_THAI in ('07','01')";
        //       DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
        //    }catch(Exception ex)
        //    { log.Error(ex.Message + "-" + ex.StackTrace);}
        //}
        //public static void UPDATE_CTU_THANHTOAN_TRANGTHAI_10(string strMaThamCHieu, string strSO_CT)
        //{
        //    try
        //    {
        //        string strSql = "UPDATE SET_CTU_HDR_MOBITCT A SET TRANGTHAI='10', ngay_ht=sysdate WHERE A.MATHAMCHIEU='" + strMaThamCHieu + "' AND A.TRANGTHAIin('05','06','09','10')";
        //        DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
        //        //cap nhat chung tu tai quay ve trang thai 07 gui thue loi de sau call gui thue
        //        strSql = "UPDATE TCS_CTU_HDR A SET TRANG_THAI='01',tt_cthue=1, ngay_ht=sysdate WHERE A.so_CT='" + strSO_CT + "' AND A.TRANG_THAI in ('07','01')";
        //        DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
        //    }
        //    catch (Exception ex)
        //    { log.Error(ex.Message + "-" + ex.StackTrace); }
        //}
        public static void UPDATE_CTU_THANHTOAN_THANHCONG(string strSo_CT_NH, string pv_strSo_ctu)
        {
            try
            {
                string strSql = "UPDATE set_ctu_hdr_mobitct SET SO_CT_NH = '" + strSo_CT_NH + "', TG_KY=sysdate  WHERE SO_CT='" + pv_strSo_ctu + "'";
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
                //cập nhật số chứng từ ngân hàng sau khi cắt tiền thành công
                strSql = "UPDATE TCS_CTU_HDR_MOBITCT SET TRANG_THAI='07',SO_CT_NH = '" + strSo_CT_NH + "',TG_KY=sysdate WHERE SO_CT='" + pv_strSo_ctu + "'";
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
            }
            catch (Exception ex)
            { log.Error(ex.Message + "-" + ex.StackTrace); }
        }
        public static void UPDATE_CTU_THANHTOAN_TRANGTHAI_05(string strMaThamCHieu, string strSO_CT, string strMaCN)
        {
            try
            {
                string strSql = "UPDATE SET_CTU_HDR_MOBITCT A SET A.TRANGTHAI='05', A.ngay_ht=sysdate, A.MA_CN='" + strMaCN + "' WHERE A.MATHAMCHIEU='" + strMaThamCHieu + "' AND A.TRANGTHAI='08'";
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
                //cap nhat chung tu tai quay ve trang thai 07 gui thue loi de sau call gui thue
                strSql = "UPDATE TCS_CTU_HDR_MOBITCT A SET A.TRANG_THAI='07', A.ngay_ht=sysdate, A.MA_CN='" + strMaCN + "' WHERE A.so_CT='" + strSO_CT + "' AND A.TRANG_THAI='05'";
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
            }
            catch (Exception ex)
            { log.Error(ex.Message + "-" + ex.StackTrace); }
        }
        public static void UPDATE_CTU_THANHTOAN_TRANGTHAI_06(string strMaThamCHieu)
        {
            try
            {
                string strSql = "UPDATE SET_CTU_HDR_MOBITCT A SET TRANGTHAI=(CASE WHEN A.TRANGTHAI='05' THEN '06' ELSE  A.TRANGTHAI END), ngay_ht=sysdate WHERE A.MATHAMCHIEU='" + strMaThamCHieu + "' ";
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
                //cap nhat chung tu tai quay ve trang thai 07 gui thue loi de sau call gui thue
                //GUI DI THUE
            }
            catch (Exception ex)
            { log.Error(ex.Message + "-" + ex.StackTrace); }
        }
        /// <summary>
        /// cap nhat trang thai thanh toan la 04 huy do sai OTP
        /// </summary>
        /// <param name="strMaThamCHieu"></param>
        public static void UPDATE_CTU_THANHTOAN_TRANGTHAI_04(string strMaThamCHieu, string StrGhiChu = "")
        {
            try
            {
                Business.MobiTCT.MobiTCTCTU.UPDATE_CTU_THANHTOAN_TRANGTHAI_04(strMaThamCHieu, StrGhiChu);
            }
            catch (Exception ex)
            { log.Error(ex.Message + "-" + ex.StackTrace); }
        }
        public static void UPDATE_CTU_THANHTOAN_TRANGTHAI_07(string strMaThamCHieu, string strSO_CT, string StrGhiChu = "")
        {
            try
            {
                Business.MobiTCT.MobiTCTCTU.UPDATE_CTU_THANHTOAN_TRANGTHAI_07(strMaThamCHieu, strSO_CT, StrGhiChu);
            }
            catch (Exception ex)
            { log.Error(ex.Message + "-" + ex.StackTrace); }
        }
        /// <summary>
        /// Get ngay lam viec he thong
        /// </summary>
        /// <param name="strMa_NV"></param>
        /// <returns></returns>
        public static string TCS_GetNgayLV(string strMa_NV)
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV'";
                return ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static string ConvertDateToNumber(string strDate)
        {
            string[] arr;

            try
            {

                strDate = strDate.Trim();
                if (strDate.Length < 10)
                    return "0";
                arr = strDate.Split('/');

                arr[0] = arr[0].PadLeft(2, '0');
                arr[1] = arr[1].PadLeft(2, '0');
                if (arr[2].Length == 2)
                    arr[2] = "20" + arr[2];
            }
            catch (Exception ex)
            {
                return DateTime.Now.ToString("yyyyMMdd");
            }

            return arr[2] + arr[1] + arr[0];
        }
        /// <summary>
        /// ham lay so chung tu
        /// </summary>
        /// <param name="strMaNV"></param>
        public static string getSOCTU(string strMaNV)
        {
            try
            {
                string strNGAY_KB = TCS_GetNgayLV(strMaNV);
                string str_nam_kb = "";
                string str_thang_kb = "";
                str_nam_kb = strNGAY_KB.Substring(2, 4);
                return str_nam_kb + str_thang_kb + GenerateKey("TCS_CTU_ID_SEQ", 6);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "";
        }
        public static string GenerateKey(string strSeq, int intLength = 0)
        {
            string strReSult = "";
            try
            {
                string strSQL = "SELECT LPAD(" + strSeq + ".NEXTVAL," + intLength + ",'0')  FROM DUAL";
                if (intLength == 0)
                    strSQL = "SELECT " + strSeq + ".NEXTVAL  FROM DUAL";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            strReSult = ds.Tables[0].Rows[0][0].ToString();
                if (intLength > 0 && strReSult.Length <= intLength)
                    strReSult = strReSult.PadLeft(intLength, '0');
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
            }
            return strReSult;
        }
        /// <summary>
        /// get ma chi nhanh cua nhan vien
        /// </summary>
        /// <param name="strMa_NV"></param>
        /// <returns></returns>
        public static string TCS_GetMACN_NV(string strMa_NV)
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE UPPER(ma_nv) = '" + strMa_NV.ToUpper() + "'";
                return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
        }
        /// <summary>
        /// get so but toan de thuc hien chung tu
        /// </summary>
        /// <param name="iNgayLV"></param>
        /// <param name="iMaNV"></param>
        /// <returns></returns>
        public static int Get_SoBT(int iNgayLV, int iMaNV)
        {
            int iResult = 0;
            try
            {
                string strSql = "Select MAX(So_BT) AS So_BT From TCS_CTU_HDR Where ngay_kb=" + iNgayLV.ToString() + " AND Ma_NV=" + iMaNV.ToString();
                DataTable dt = DatabaseHelp.ExecuteToTable(strSql);
                if (!Globals.IsNullOrEmpty(dt))
                    iResult = int.Parse(dt.Rows[0][0].ToString()) + 1;
            }
            catch (Exception ex)
            {
                iResult = 1;
            }
            return iResult;
        }
        public static string TCS_GetKHCT(string strSHKB, string strMaNV = "", string strLoaiGD = "01")
        {
            try
            {
                string strSQL = "";
                string strResult = "";

                if (strLoaiGD.Equals("01"))
                {
                    strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT'";
                    return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                }
                else
                {
                    try
                    {
                        string strGetCN = "select ma_cn from tcs_dm_nhanvien where ma_nv = '" + strMaNV + "'";
                        string strMaCN = DataAccess.ExecuteSQLScalar(strGetCN);

                        if (strLoaiGD == "04")
                        {
                            strSQL = "select substr(max(kyhieu_ct),0,3) || LPAD(TO_NUMBER(substr(max(kyhieu_ct),4,7))+1,7,0) KHCT from tcs_ctu_hdr ";
                            strSQL += "where ma_nv in (select ma_nv from tcs_dm_nhanvien where ma_cn like '" + strMaCN + "%') and ma_lthue = '04'";
                        }
                        else if (strLoaiGD == "05")
                        {
                            strSQL = "select substr(max(kyhieu_ct),0,3) || LPAD(TO_NUMBER(substr(max(kyhieu_ct),4,7))+1,7,0) KHCT from tcs_baolanh_hdr where ma_nv in (select ma_nv from tcs_dm_nhanvien where ma_cn like '" + strMaCN + "%') ";
                        }
                        DataTable dt = DatabaseHelp.ExecuteToTable(strSQL);
                        if (!Globals.IsNullOrEmpty(dt))
                        {
                            strResult = dt.Rows[0][0].ToString();
                            if (strResult == "")
                            {
                                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT'";
                                string strKHCT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                                strResult = strKHCT.Substring(0, 3) + "0000001";
                            }
                        }
                        else
                        {
                            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' ";
                            string strKHCT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                            strResult = strKHCT.Substring(0, 3) + "0000001";
                        }
                    }
                    catch (Exception exc)
                    {
                        strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' ";
                        string strKHCT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                        strResult = strKHCT.Substring(0, 3) + "0000001";
                    }
                    return strResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                return "";
            }
        }
        public static string ToValue(string value, string typefield, string strFormat = "")
        {
            string tmp = "";
            //'convert to from Unicode to TCVN before insert DB
            if (Convert.IsDBNull(value))
            {
                value = "' '";
                return value;
            }
            value = value.Trim();
            if (typefield.Trim().ToUpper().Equals("NUMBER"))
            {
                if (value != "")
                {
                    tmp = value.Replace(",", ".");
                    //    'Neu dau vao cho Value la Nothing thi ham tra ve chuoi "null"
                }
                else
                    tmp = "null";

            }
            else if (typefield.Trim().ToUpper().Equals("DATE"))
            {
                if (value != "")
                {
                    if (strFormat == "")
                        strFormat = "dd/MM/rrrr HH24:MI:ss";

                    tmp = "to_date('" + value + "','" + strFormat + "')";
                }
                else
                    tmp = "null";

            }

            return tmp;
        }
        public static DataTable GetNHChuyenByAccountNo()
        {
            DataTable dt = new DataTable();
            try
            {
                string strSql = "SELECT MA_NH_CHUYEN,TEN_NH_CHUYEN FROM TCS_NGANHANG_CHUYEN";

                dt = DataAccess.ExecuteToTable(strSql);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }

        public static void setObject21ToCTU(string strMaThamChieu, ref infChungTuHDR hdr, ref List<infChungTuDTL> lstDtl, CorebankServiceESB.MOBI.AccountResp v_objAcc, ref bool checkThuSHB)
        {
            try
            {
                //dung chung Account HQ247
                string strUserID = ConfigurationManager.AppSettings.Get("CUSTOMS.Ma_AccountHQ247");
                string strSQL = "SELECT A.MADOITAC, A.MATHAMCHIEU, A.SOTIEN, A.PHICHUYENPHAT, A.NGONNGU, ";
                strSQL += " A.MATIENTE, A.THONGTINGIAODICH, A.MADICHVU, A.MAHOSO, ";
                strSQL += " A.TKTHUHUONG, A.MALOAIHINHTHUPHAT, A.MALOAIHINHTHUE, ";
                strSQL += " A.TENLOAIHINHTHUE, A.MACQTHU, A.TENCQTHU, A.SHKB, A.MACOQUANQD, ";
                strSQL += " A.TENCOQUANQD, A.SOQD,TO_CHAR( A.NGAYQD,'YYYY-MM-DD') NGAYQD, A.MST, A.HOTENNGUOINOP, ";
                strSQL += " A.SOCMNDNGUOINOP, A.DIACHINGUOINOP, A.HUYENNGUOINOP, ";
                strSQL += " A.TINHNGUOINOP, A.DSKHOANNOP, A.MSGCONTENT, A.SO_CT, A.TRANGTHAI, ";
                strSQL += " A.SOKHUNG, A.SOMAY, A.MADBHC, A.THOIGIANGD, ";
                strSQL += " A.MANGANHANG, A.PHUONGTHUC, A.SOTAIKHOAN_THE, A.EWALLETTOKEN, ";
                strSQL += " A.SOTAIKHOAN, A.IDTAIKHOANLIENKET, A.LOAIOTP, A.TRANIDOTP, ";
                strSQL += " A.DIENTHOAI, A.TTHAI_TCT, A.MA_NNTIEN, A.TEN_NNTIEN,A.mstNopThay,   ";
                strSQL += " A.hoTenNguoiNopThay, A.diaChiNguoiNopThay, A.huyenNguoiNopThay, A.tinhNguoiNopThay   ";
                strSQL += " FROM SET_CTU_HDR_MOBITCT A WHERE A.MATHAMCHIEU='" + strMaThamChieu + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);

                string strMADOITAC = "";
                string strSOTIEN = "";
                string strPHICHUYENPHAT = "";
                string strNGONNGU = "";
                string strMATIENTE = "";
                string strTHONGTINGIAODICH = "";
                string strMADICHVU = "";
                string strMAHOSO = "";
                string strTKTHUHUONG = "";
                string strMALOAIHINHTHUPHAT = "";
                string strMALOAIHINHTHUE = "";
                string strTENLOAIHINHTHUE = "";
                string strMACQTHU = "";
                string strTENCQTHU = "";
                string strSHKB = "";
                string strMACOQUANQD = "";
                string strTENCOQUANQD = "";
                string strSOQD = "";
                string strNGAYQD = "";
                string strMST = "";
                string strHOTENNGUOINOP = "";
                string strSOCMNDNGUOINOP = "";
                string strDIACHINGUOINOP = "";
                string strHUYENNGUOINOP = "";
                string strTINHNGUOINOP = "";
                string strDSKHOANNOP = "";
                string strMSGCONTENT = "";
                string strSO_CT = "";
                string strTRANGTHAI = "";
                string strSOKHUNG = "";
                string strSOMAY = "";
                string strMADBHC = "";
                string strTHOIGIANGD = "";
                string strMANGANHANG = "";
                string strPHUONGTHUC = "";
                string strSOTAIKHOAN_THE = "";
                string strEWALLETTOKEN = "";
                string strSOTAIKHOAN = "";
                string strIDTAIKHOANLIENKET = "";
                string strLOAIOTP = "";
                string strTRANIDOTP = "";
                string strMA_NNTIEN = "";
                string strTEN_NNTIEN = "";

                string mstNopThay = "";
                string hoTenNguoiNopThay = "";
                string diaChiNguoiNopThay = "";
                string huyenNguoiNopThay = "";
                string tinhNguoiNopThay = "";


                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            strMADOITAC = ds.Tables[0].Rows[0]["MADOITAC"].ToString();
                            strSOTIEN = ds.Tables[0].Rows[0]["SOTIEN"].ToString();
                            strPHICHUYENPHAT = ds.Tables[0].Rows[0]["PHICHUYENPHAT"].ToString();
                            strNGONNGU = ds.Tables[0].Rows[0]["NGONNGU"].ToString();
                            strMATIENTE = ds.Tables[0].Rows[0]["MATIENTE"].ToString();
                            strTHONGTINGIAODICH = ds.Tables[0].Rows[0]["THONGTINGIAODICH"].ToString();
                            strMADICHVU = ds.Tables[0].Rows[0]["MADICHVU"].ToString();
                            strMAHOSO = ds.Tables[0].Rows[0]["MAHOSO"].ToString();
                            strTKTHUHUONG = ds.Tables[0].Rows[0]["TKTHUHUONG"].ToString();
                            strMALOAIHINHTHUPHAT = ds.Tables[0].Rows[0]["MALOAIHINHTHUPHAT"].ToString();
                            strMALOAIHINHTHUE = ds.Tables[0].Rows[0]["MALOAIHINHTHUE"].ToString();
                            strTENLOAIHINHTHUE = ds.Tables[0].Rows[0]["TENLOAIHINHTHUE"].ToString();
                            strMACQTHU = ds.Tables[0].Rows[0]["MACQTHU"].ToString();
                            strTENCQTHU = ds.Tables[0].Rows[0]["TENCQTHU"].ToString();
                            strSHKB = ds.Tables[0].Rows[0]["SHKB"].ToString();
                            strMACOQUANQD = ds.Tables[0].Rows[0]["MACOQUANQD"].ToString();
                            strTENCOQUANQD = ds.Tables[0].Rows[0]["TENCOQUANQD"].ToString();
                            strSOQD = ds.Tables[0].Rows[0]["SOQD"].ToString();
                            strNGAYQD = ds.Tables[0].Rows[0]["NGAYQD"].ToString();
                            strMST = ds.Tables[0].Rows[0]["MST"].ToString();
                            strHOTENNGUOINOP = ds.Tables[0].Rows[0]["HOTENNGUOINOP"].ToString();
                            strSOCMNDNGUOINOP = ds.Tables[0].Rows[0]["SOCMNDNGUOINOP"].ToString();
                            strDIACHINGUOINOP = ds.Tables[0].Rows[0]["DIACHINGUOINOP"].ToString();
                            strHUYENNGUOINOP = ds.Tables[0].Rows[0]["HUYENNGUOINOP"].ToString();
                            strTINHNGUOINOP = ds.Tables[0].Rows[0]["TINHNGUOINOP"].ToString();
                            strDSKHOANNOP = ds.Tables[0].Rows[0]["DSKHOANNOP"].ToString();
                            strMSGCONTENT = ds.Tables[0].Rows[0]["MSGCONTENT"].ToString();
                            strSO_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
                            strTRANGTHAI = ds.Tables[0].Rows[0]["TRANGTHAI"].ToString();
                            strSOKHUNG = ds.Tables[0].Rows[0]["SOKHUNG"].ToString();
                            strSOMAY = ds.Tables[0].Rows[0]["SOMAY"].ToString();
                            strMADBHC = ds.Tables[0].Rows[0]["MADBHC"].ToString();
                            // strTHOIGIANGD = ds.Tables[0].Rows[0]["THOIGIANGD"].ToString();
                            strMANGANHANG = ds.Tables[0].Rows[0]["MANGANHANG"].ToString();
                            strPHUONGTHUC = ds.Tables[0].Rows[0]["PHUONGTHUC"].ToString();
                            strSOTAIKHOAN_THE = ds.Tables[0].Rows[0]["SOTAIKHOAN_THE"].ToString();
                            strEWALLETTOKEN = ds.Tables[0].Rows[0]["EWALLETTOKEN"].ToString();
                            strSOTAIKHOAN = ds.Tables[0].Rows[0]["SOTAIKHOAN"].ToString();
                            strIDTAIKHOANLIENKET = ds.Tables[0].Rows[0]["IDTAIKHOANLIENKET"].ToString();

                            strMA_NNTIEN = ds.Tables[0].Rows[0]["MA_NNTIEN"].ToString();
                            strTEN_NNTIEN = ds.Tables[0].Rows[0]["TEN_NNTIEN"].ToString();

                            mstNopThay = ds.Tables[0].Rows[0]["mstNopThay"].ToString();
                            hoTenNguoiNopThay = ds.Tables[0].Rows[0]["hoTenNguoiNopThay"].ToString();
                            diaChiNguoiNopThay = ds.Tables[0].Rows[0]["diaChiNguoiNopThay"].ToString();
                            huyenNguoiNopThay = ds.Tables[0].Rows[0]["huyenNguoiNopThay"].ToString();
                            tinhNguoiNopThay = ds.Tables[0].Rows[0]["tinhNguoiNopThay"].ToString();

                        }
                hdr.Ngay_KB = int.Parse(TCS_GetNgayLV(strUserID));
                string str_nam_kb = "";
                string str_thang_kb = "";
                str_nam_kb = hdr.Ngay_KB.ToString().Substring(2, 4);
                str_thang_kb = hdr.Ngay_KB.ToString().Substring(4, 2);
                hdr.Ten_cqthu = strTENCQTHU;
                hdr.So_CT = getSOCTU(strUserID);
                hdr.Ma_CN = v_objAcc.branchCode;
                //' Mã NV
                hdr.Ma_NV = int.Parse(strUserID);
                // ' Số bút toán
                hdr.So_BT = Get_SoBT(hdr.Ngay_KB, hdr.Ma_NV);
                hdr.cifNo = v_objAcc.cifNo;
                hdr.CIF = v_objAcc.cifNo;
                //' Số CT
                //    hdr.So_CT = str_nam_kb + str_thang_kb + GenerateKey("TCS_CTU_ID_SEQ", 6);

                //'Trang thai bds hien tai
                hdr.TT_BDS = "";

                hdr.TT_NSTT = "N";
                //'SHKB
                hdr.SHKB = strSHKB;// obj21.thongTinBienLai.khoBac;
                //' Mã điểm thu
                hdr.Ma_DThu = hdr.SHKB;
                //' Mã NNTien
                hdr.KyHieu_CT = TCS_GetKHCT(hdr.SHKB);
                hdr.Ma_NNTien = strMA_NNTIEN; //obj21.Data.CHUNGTU.MST_NNTHAY;

                //' Tên NNTien
                hdr.Ten_NNTien = strTEN_NNTIEN;// obj21.Data.CHUNGTU.TEN_NNTHAY;

                //' Địa chỉ NNTien
                hdr.DC_NNTien = "";// obj21.Data.CHUNGTU.DIACHI_NNTHAY;

                //' Mã NNT
                hdr.Ma_NNThue = strMST;// obj21.Data.CHUNGTU.MST;

                //' Tên NNT
                hdr.Ten_NNThue = strHOTENNGUOINOP;// obj21.Data.CHUNGTU.TEN_NNT;
                //' Địa chỉ NNT
                hdr.DC_NNThue = strDIACHINGUOINOP;// obj21.Data.CHUNGTU.DIACHI_NNT;

                if (mstNopThay.Trim().Length > 0)
                {
                    hdr.Ma_NNTien = mstNopThay;
                }
                if (hoTenNguoiNopThay.Trim().Length > 0)
                {
                    hdr.Ten_NNTien = hoTenNguoiNopThay;
                }
                hdr.DC_NNTien = diaChiNguoiNopThay;
                hdr.huyenNguoiNopThay = huyenNguoiNopThay;
                hdr.tinhNguoiNopThay = tinhNguoiNopThay;

                //' Số bút toán FT
                hdr.So_CT_NH = "";// obj21.Data.CHUNGTU.SO_REFCORE;
                // ' hdr.Ten_NH_B = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ten_NH_B).InnerText

                hdr.REF_CORE_TTSP = str_nam_kb + str_thang_kb + CTuCommon.Get_SoRefCore_TTSP();
                //  ' Lý do
                hdr.Ly_Do = "";
                //  ' Mã TQ
                hdr.Ma_TQ = "0";

                hdr.So_QD = strSOQD; // obj21.Data.CHUNGTU.SO_QUYET_DINH;
                hdr.CQ_QD = strMACOQUANQD;// obj21.Data.CHUNGTU.CQUAN_THAMQUYEN;
                hdr.Ngay_QD = ToValue(strNGAYQD, "DATE", "RRRR-MM-DD"); //ToValue(obj21.Data.CHUNGTU.NGAY_QDINH, "DATE", "RRRR-MM-DD");

                ////  ' Ngay CT
                hdr.Ngay_CT = hdr.Ngay_KB;// clsCTU.TCS_GetNgayLV(pv_intMaNV)
                ////  ' Ngay HT
                hdr.Ngay_HT = ToValue(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE");

                hdr.Ma_CQThu = strMACQTHU;// obj21.Data.CHUNGTU.CQ_QLY_THU;
                hdr.Ten_cqthu = strTENCQTHU;// obj21.Data.CHUNGTU.TEN_CQ_THU;
                //  'THEM MA SAN PHAM
                hdr.MA_NH_TT = ""; //v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_TT).InnerText
                hdr.SAN_PHAM = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_SANPHAM).InnerText
                hdr.TT_CITAD = "0";// 'v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TT_CITAD).InnerText
                hdr.MA_NTK = "1";
                //  ' DBHC
                hdr.Ma_Xa = "";
                hdr.XA_ID = "";// Get_XaID(hdr.Ma_Xa)
                hdr.Ma_Huyen = "";// strHUYENNGUOINOP;
                hdr.Ma_Tinh = "";// strTINHNGUOINOP;// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_DBHC_NNTIEN).InnerText

                //  ' TK No
                hdr.TK_No = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TKNo).InnerText.Replace(".", "").Replace(" ", "")
                hdr.TEN_TK_NO = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenTKNo).InnerText
                string v_strSHKB = strSHKB;
                //string v_strMaDBHC = strSHKB;


                hdr.TK_GL_NH = "NULL";


                //  ' TK Co
                hdr.TK_Co = strTKTHUHUONG;// obj21.Data.CHUNGTU.SO_TK_NHKBNN;
                hdr.TEN_TK_CO = "";// obj21.Data.CHUNGTU.TINHCHAT_KHOAN_NOP;
                //  ' So TKhai
                hdr.So_TK = "";
                hdr.Ngay_TK = "null";


                //  ' LHXNK
                hdr.LH_XNK = "";
                //  ' DVSDNS
                hdr.DVSDNS = "";
                hdr.Ten_DVSDNS = "";


                hdr.Ma_NT = strMATIENTE;// obj21.Data.CHUNGTU.LOAI_TIEN;
                hdr.Ty_Gia = 1.0;// ChungTu.buChungTu.get_tygia(hdr.Ma_NT, hdr.TG_ID)
                hdr.TG_ID = "null";
                //    ' Mã Loại Thuế
                hdr.Ma_LThue = "01";

                //  ' So Khung - So May
                hdr.So_Khung = strSOKHUNG;
                hdr.So_May = strSOMAY;
                //  'Phuong thuc thanh toan mac dinh chuyen khoan
                hdr.PT_TT = "01";// obj21.Data.CHUNGTU.MA_HTHUC_NOP;
                hdr.TT_TThu = double.Parse(strSOTIEN);// double.Parse(obj21.Data.CHUNGTU.TTIEN_NOPTHUE);
                hdr.TTien = double.Parse(strSOTIEN);// double.Parse(obj21.Data.CHUNGTU.TTIEN_NOPTHUE);
                hdr.Mode = "0";
                CorebankServiceESB.clsCoreBank clsCore = new CorebankServiceESB.clsCoreBank();

                DataTable dtNhChuyen = GetNHChuyenByAccountNo();

                string strMa_NH_A = dtNhChuyen.Rows[0]["MA_NH_CHUYEN"].ToString();
                string strTen_NH_A = dtNhChuyen.Rows[0]["TEN_NH_CHUYEN"].ToString();
                //string maCitad = dtNhChuyen.Rows[0]["MA_CITAD"].ToString();


                // 'KIENVT : THEM THONG TIN VE NGAN HANG
                hdr.MA_NH_A = strMa_NH_A;
                hdr.Ten_nh_a = strTen_NH_A;
                hdr.MA_NH_B = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_B).InnerText 'strMaNH.Split(";")(0).ToString
                hdr.MA_NH_TT = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_TT).InnerText 'strMaNH.Split(";")(2).ToString
                hdr.Ten_NH_B = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ten_NH_B).InnerText
                hdr.TEN_NH_TT = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_NH_TT).InnerText

                //check đi ttsp ở đây:
                string strNHB = "";
                CorebankServiceESB.clsSongPhuong objClsSongPhuong = new CorebankServiceESB.clsSongPhuong();

                string strResult = objClsSongPhuong.ValiadSHB(hdr.SHKB);
                string[] arrResult;
                if (strResult.Length > 8)
                {
                    arrResult = strResult.Split(';');
                    strNHB = arrResult[0].ToString();
                    hdr.ACC_NHB = arrResult[1].ToString();
                    hdr.MA_CN_SP = arrResult[2].ToString();
      
                    strSQL = "select * from tcs_dm_nh_giantiep_tructiep where shkb='" + hdr.SHKB + "' and ma_giantiep ='" + strNHB + "'  order by ID ";
                    checkThuSHB = true;
                }
                else
                {
                    strSQL = "select * from tcs_dm_nh_giantiep_tructiep where shkb='" + hdr.SHKB + "'  and (TT_SHB <> '1' OR TT_SHB is NULL)  order by ID ";
                }
                //chỗ này kiểm tra giờ chạy COD luôn
                Business.CTuCommon bus = new Business.CTuCommon();
                string ON_TTSP = bus.GetThamSoHT("ON_TTSP");
                if (ON_TTSP.Equals("Y"))
                {
                    // tham số này là N thì mới kiểm tra tiếp, còn Y thì đã điều hướng toàn bộ
                    string CHECK_TTSP_COD = bus.GetThamSoHT("CHECK_TTSP_COD");
                    if (CHECK_TTSP_COD.Equals("Y"))
                    {
                        string dd = DateTime.Now.ToString("dd");
                        string gioXuLy = DateTime.Now.ToString("HHmmss");
                        //string NGAY_COD = bus.GetThamSoHT("NGAY_COD");

                        //if (NGAY_COD.Equals(dd))
                        //{
                        string timeBegin = bus.GetThamSoHT("TIME_BEGIN_CHECK_COD").Replace(":", "");
                        string timeEnd = bus.GetThamSoHT("TIME_END_CHECK_COD").Replace(":", "");

                        if (Int32.Parse(gioXuLy) < Int32.Parse(timeBegin) || Int32.Parse(gioXuLy) > Int32.Parse(timeEnd))
                        {
                            // trong khoảng này thì điều hướng đi citad hết
                            checkThuSHB = false;
                            strSQL = "select * from tcs_dm_nh_giantiep_tructiep where shkb='" + hdr.SHKB + "' and (TT_SHB <> '1' OR TT_SHB is NULL)   order by ID";
                        }

                        //}
                    }
                }
                //end kiểm tra gio chạy COD



                DataSet dsNH_B = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (dsNH_B != null)
                    if (dsNH_B.Tables.Count > 0)
                        if (dsNH_B.Tables[0].Rows.Count > 0)
                        {
                            hdr.MA_NH_B = dsNH_B.Tables[0].Rows[0]["ma_giantiep"].ToString();
                            hdr.MA_NH_TT = dsNH_B.Tables[0].Rows[0]["ma_tructiep"].ToString();
                            hdr.Ten_NH_B = dsNH_B.Tables[0].Rows[0]["ten_giantiep"].ToString();
                            hdr.TEN_NH_TT = dsNH_B.Tables[0].Rows[0]["ten_tructiep"].ToString();
                        }
                //  ' hoapt sua ma ngan hang truc tiep gian tiep
                //   Dim strMaNH As String = GetTT_NH_NHAN(v_strSHKB)

                //end check


                //  'Them NH_HUONG
                hdr.NH_HUONG = hdr.MA_NH_B;
                //  ' TK_KH_NH 
                hdr.TK_KH_NH = strSOTAIKHOAN;
                hdr.TEN_KH_NH = v_objAcc.accName;
                hdr.MA_CN_TK = v_objAcc.branchCode;

                //  strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.NGAY_KH_NH).InnerText
                hdr.NGAY_KH_NH = ToValue(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE");// ToValue(DateTime.Now.ToString("yyyy-MM-dd"), "DATE", "RRRR-MM-DD");//ToValue(obj21.Data.CHUNGTU.NGAY_CHUNGTU, "DATE", "RRRR-MM-DD");

                hdr.TK_KH_Nhan = "";
                hdr.Ten_KH_Nhan = "";
                hdr.Diachi_KH_Nhan = "";

                //  'THEM PHI GIAO DỊCH,VAT
                hdr.PHI_GD = "0";// obj21.Data.CHUNGTU.FEEAMT;// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_GD).InnerText.Replace(".", "")
                hdr.PHI_VAT = "0";// obj21.Data.CHUNGTU.VATAMT; ;// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_VAT).InnerText.Replace(".", "")
                hdr.PHI_GD2 = "0";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_GD2).InnerText.Replace(".", "")
                hdr.PHI_VAT2 = "0";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_VAT2).InnerText.Replace(".", "")
                hdr.PT_TINHPHI = "0";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PT_TINHPHI).InnerText.Replace(".", "")

                //  ' quận_huyên NNTien
                hdr.QUAN_HUYEN_NNTIEN = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Huyen_NNTIEN).InnerText
                //  ' Tinh _NNTien
                hdr.TINH_TPHO_NNTIEN = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Tinh_NNTIEN).InnerText
                //  Dim ref_no As String = String.Empty

                hdr.RM_REF_NO = hdr.So_CT;
                hdr.REF_NO = hdr.So_CT;

                //  ' So BK
                hdr.So_BK = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.So_BK).InnerText
                hdr.TIME_BEGIN = ToValue(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE");
                hdr.SO_CMND = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SO_CMND).InnerText
                hdr.SO_FONE = "";// v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SO_FONE).InnerText
                //  ' Ngay BK
                hdr.Ngay_BK = "null";

                hdr.branchCode = v_objAcc.branchCode;
                hdr.Lan_In = "0";

                hdr.Ma_KS = hdr.Ma_NV.ToString();

                hdr.Trang_Thai = "05";
                //fix tam mac dinh 0300
                hdr.LOAI_NNT = "0300";// obj21.Data.CHUNGTU.LHINH_NNT;
                hdr.TEN_LOAI_NNT = "";
                hdr.TTien_NT = 0.0;
                hdr.Kenh_CT = strMALOAIHINHTHUPHAT;// obj21.Header.Product_Type;
                //phan nay xu ly lay ten kho bac
                //if (hdr.Ten_kb.Length <= 0)
                //{
                //    hdr.Ten_kb = getTEN_KBNN(hdr.SHKB);
                //}
                hdr.Ma_Xa = strMADBHC;
                if (hdr.Ten_cqthu.Length <= 0)
                {
                    hdr.Ten_cqthu = getTEN_CQTHU(hdr.Ma_CQThu);
                }
                //doan nay danh rieng cho ong EIB
                strSQL = "select shkb,ten,ma_tinh, ma_db from tcs_dm_khobac where shkb='" + hdr.SHKB + "'";
                DataSet dsKB = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (dsKB != null)
                    if (dsKB.Tables.Count > 0)
                        if (dsKB.Tables[0].Rows.Count > 0)
                        {
                            hdr.Ten_kb = dsKB.Tables[0].Rows[0]["ten"].ToString();
                            hdr.MA_DBHC_KBNN = dsKB.Tables[0].Rows[0]["ma_db"].ToString();
                            hdr.MA_TINH_KBNN = dsKB.Tables[0].Rows[0]["ma_tinh"].ToString();
                        }


                //build chuỗi diễn giải 210 ký tự:
                string strRemarks = "";

                strRemarks = buildRemark(hdr.SHKB, hdr.Ten_kb, hdr.Ma_NNThue, hdr.MA_DBHC_KBNN, hdr.TK_Co, hdr.Ma_CQThu, hdr.Ma_LThue);
                string result = "";
                // for (int i = 0; i < obj21.Data.CHUNGTU.CHUNGTU_CHITIET.Count; i++)
                strSQL = "SELECT A.MATHAMCHIEU, A.LOAIPHILEPHI, A.MAPHILEPHI, A.TENPHILEPHI, "
                        + " A.SOTIEN, A.MACHUONG, A.MATMUC, A.SOKHUNG, A.SOMAY, A.SOQD,"
                        + " to_char(A.NGAYQD,'dd/MM/yyyy') NGAYQD, A.MADBHC "
                        + " FROM SET_CTU_DTL_MOBITCT A where A.MATHAMCHIEU='" + strMaThamChieu + "'";
                DataSet dsDtl = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                for (int i = 0; i < dsDtl.Tables[0].Rows.Count; i++)
                {
                    infChungTuDTL dtl = new infChungTuDTL();
                    dtl.SO_TK = dsDtl.Tables[0].Rows[i]["SOQD"].ToString();

                    dtl.SO_CT = hdr.So_CT;
                    // 'So tu tang
                    dtl.ID = GenerateKey("TCS_CTU_DTL_SEQ");
                    //                    ' SHKB
                    dtl.SHKB = hdr.SHKB;

                    //  ' Ngày KB
                    dtl.Ngay_KB = hdr.Ngay_KB;

                    //' Mã NV
                    dtl.Ma_NV = hdr.Ma_NV;

                    //' Số Bút toán
                    dtl.So_BT = hdr.So_BT;

                    // ' Mã điểm thu
                    dtl.Ma_DThu = hdr.Ma_DThu;

                    // 'Mã quỹ
                    dtl.MaQuy = "";

                    //  ' Mã Cấp
                    dtl.Ma_Cap = "";

                    //  ' Mã Chương
                    dtl.Ma_Chuong = dsDtl.Tables[0].Rows[i]["MACHUONG"].ToString();
                    //' CCH_ID
                    dtl.CCH_ID = "null";
                    //' Mã Loại
                    dtl.Ma_Loai = "";

                    //' Mã Khoản
                    dtl.Ma_Khoan = "";// v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.LKhoan).InnerText

                    //' CCH_ID
                    dtl.LKH_ID = "null";//Get_LKHID(dtl.Ma_Khoan)

                    //' Mã Mục
                    dtl.Ma_Muc = "";

                    //' Mã TMục
                    dtl.Ma_TMuc = dsDtl.Tables[0].Rows[i]["MATMUC"].ToString(); //obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].TIEUMUC;

                    //' MTM_ID
                    dtl.MTM_ID = "null";//Get_MTMucID(dtl.Ma_TMuc)

                    //' Mã TLDT
                    dtl.Ma_TLDT = "null";
                    //' DT_ID
                    dtl.DT_ID = "null";

                    //  '  Nội dung
                    dtl.Noi_Dung = dsDtl.Tables[0].Rows[i]["TENPHILEPHI"].ToString();// obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].THONGTIN_KHOANNOP;


                    dtl.SoTien_NT = 0.0;
                    dtl.SoTien = double.Parse(dsDtl.Tables[0].Rows[i]["SOTIEN"].ToString());
                    hdr.TTien_NT += dtl.SoTien;

                    string ngayqd = dsDtl.Tables[0].Rows[i]["NGAYQD"].ToString();
                    if (ngayqd.Trim().Length > 0)
                    {
                        dtl.Ky_Thue = ngayqd;
                    }
                    else
                    {
                        // ' Kỳ thuế mac dinh la ngay hien tai
                        dtl.Ky_Thue = DateTime.Now.ToString("dd/MM/yyyy"); // obj21.Data.CHUNGTU.CHUNGTU_CHITIET[i].KY_THUE;
                    }
                    // ' Mã dự phòng
                    dtl.Ma_DP = "null";
                    result += "(C:" + dsDtl.Tables[0].Rows[i]["MACHUONG"].ToString();
                    result += "-TM:" + dsDtl.Tables[0].Rows[i]["MATMUC"].ToString();
                    result += "-KT:" + dtl.Ky_Thue;
                    result += "-ST:" + dsDtl.Tables[0].Rows[i]["SOTIEN"].ToString();
                    result += ")";

                    lstDtl.Add(dtl);
                }

                strRemarks += result;

                //Phải hỏi lại WRB nếu chuổi diễn giải lớn hơn 210 ký tự thì sao XXXX
                if (strRemarks.Length > 210)
                {
                    strRemarks = strRemarks.Substring(0, 210);
                }
                hdr.REMARKS = VBOracleLib.Globals.RemoveSign4VietnameseString(strRemarks);
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
        public static string getTEN_CQTHU(string strMaCQTHU, string strMaHQ = "")
        {
            try
            {
                string StrSQL = "SELECT A.TEN FROM TCS_DM_CQTHU A ";
                if (strMaHQ.Length > 0)
                {
                    StrSQL += " WHERE MA_HQ='" + strMaHQ + "'";
                }
                else
                {
                    StrSQL += " WHERE MA_CQTHU='" + strMaCQTHU + "'";
                }
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getTEN_KBNN(string strSHKB)
        {
            try
            {
                string StrSQL = "SELECT A.TEN FROM TCS_DM_KHOBAC A WHERE SHKB='" + strSHKB + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        /// <summary>
        /// insert chung tu moi voi trang thai 07 gui thue loi
        /// </summary>
        /// <param name="objCTuHDR"></param>
        /// <param name="objCTuDTL"></param>
        /// <param name="Ngay_KB"></param>
        /// <param name="Ma_NV"></param>
        /// <param name="So_BT"></param>
        /// <param name="CTU_GNT_BL"></param>
        private static void Insert_New0021(string strMaThamchieu, infChungTuHDR objCTuHDR, List<infChungTuDTL> objCTuDTL, string Ngay_KB = "", string Ma_NV = "", string So_BT = "", string CTU_GNT_BL = "00")
        {

            IDbTransaction transCT = null;
            IDbConnection conn = null;
            string strSql = "";
            try
            {
                conn = DataAccess.GetConnectionNo();
                transCT = conn.BeginTransaction();

                strSql = "Insert into TCS_CTU_HDR_MOBITCT(SHKB,Ngay_KB,Ma_NV,MA_CN,";
                strSql += "So_BT,Ma_DThu,So_BThu,";
                strSql += "KyHieu_CT,So_CT_NH,";
                strSql += "Ma_NNTien,Ten_NNTien,DC_NNTien,huyenNguoiNopThay,tinhNguoiNopThay,";
                strSql += "Ma_NNThue,Ten_NNThue,DC_NNThue,";
                strSql += "Ly_Do,Ma_KS,Ma_TQ,So_QD,";
                strSql += "Ngay_QD,CQ_QD,Ngay_CT,Ngay_HT,";
                strSql += "Ma_CQThu,TEN_CQTHU,XA_ID,Ma_Tinh,";
                strSql += "Ma_Huyen,Ma_Xa,TK_No,TK_Co,";
                strSql += "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS,";
                strSql += "Ma_NT,Ty_Gia,TG_ID, Ma_LThue,DIENGIAI_HQ,";
                strSql += "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH,";
                strSql += "MA_NH_A,MA_NH_B,Ten_NH_B,Ten_NH_TT,NH_HUONG,TTien,TT_TThu,";
                strSql += "Lan_In,Trang_Thai, ";
                strSql += "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT,";
                strSql += "So_BK, Ngay_BK,CTU_GNT_BL,So_CT,TK_GL_NH,TEN_KH_NH,TK_KB_NH, ";
                strSql += "ISACTIVE,PHI_GD,PHI_VAT,PHI_GD2,PHI_VAT2,TIME_BEGIN,PT_TINHPHI,SEQ_NO,REF_NO,RM_REF_NO,QUAN_HUYENNNTIEN,";
                strSql += "TINH_TPNNTIEN,MA_HQ,LOAI_TT,TEN_HQ,MA_HQ_PH,TEN_HQ_PH, MA_NH_TT,MA_SANPHAM,TT_CITAD,SO_CMND,SO_FONE,MA_NTK, KENH_CT, LOAI_NNT, LOAITS, noiDung)";
                strSql += "Values('" + objCTuHDR.SHKB + "'," + objCTuHDR.Ngay_KB + "," + objCTuHDR.Ma_NV + ",'" + objCTuHDR.Ma_CN + "',";
                strSql += objCTuHDR.So_BT + ",'" + objCTuHDR.Ma_DThu + "'," + objCTuHDR.So_BThu + ",'";
                strSql += objCTuHDR.KyHieu_CT + "','" + objCTuHDR.So_CT_NH + "','";
                strSql += objCTuHDR.Ma_NNTien + "',:Ten_NNTien,:DC_NNTien,:huyenNguoiNopThay,:tinhNguoiNopThay,'";
                strSql += objCTuHDR.Ma_NNThue + "',:Ten_NNThue,:DC_NNThue,'";
                strSql += objCTuHDR.Ly_Do + "'," + objCTuHDR.Ma_KS + "," + objCTuHDR.Ma_TQ + ",'" + objCTuHDR.So_QD + "',";
                strSql += objCTuHDR.Ngay_QD + ",'" + objCTuHDR.CQ_QD + "'," + objCTuHDR.Ngay_CT + ",";
                strSql += objCTuHDR.Ngay_HT + ",'" + objCTuHDR.Ma_CQThu + "',:TEN_CQTHU,'";
                strSql += objCTuHDR.XA_ID + "','" + objCTuHDR.Ma_Tinh + "','" + objCTuHDR.Ma_Huyen + "','";
                strSql += objCTuHDR.Ma_Xa + "','" + objCTuHDR.TK_No + "','" + objCTuHDR.TK_Co + "','";
                strSql += objCTuHDR.So_TK + "'," + objCTuHDR.Ngay_TK + ",'" + objCTuHDR.LH_XNK + "','";
                strSql += objCTuHDR.DVSDNS + "','" + objCTuHDR.Ten_DVSDNS + "','" + objCTuHDR.Ma_NT + "',";
                strSql += objCTuHDR.Ty_Gia + "," + objCTuHDR.TG_ID + ",'" + objCTuHDR.Ma_LThue + "',:DIENGIAI_HQ,'";
                strSql += objCTuHDR.So_Khung + "','" + objCTuHDR.So_May + "','" + objCTuHDR.TK_KH_NH + "',";
                strSql += objCTuHDR.NGAY_KH_NH + ",'" + objCTuHDR.MA_NH_A + "','" + objCTuHDR.MA_NH_B + "',:Ten_NH_B,:TEN_NH_TT,'" + objCTuHDR.NH_HUONG + "',";
                strSql += objCTuHDR.TTien + "," + objCTuHDR.TT_TThu + "," + objCTuHDR.Lan_In + ",'";
                strSql += objCTuHDR.Trang_Thai + "','" + objCTuHDR.TK_KH_Nhan + "','" + objCTuHDR.Ten_KH_Nhan + "','";
                strSql += objCTuHDR.Diachi_KH_Nhan + "'," + objCTuHDR.TTien_NT + ",'" + objCTuHDR.PT_TT + "'," + Globals.EscapeQuote(objCTuHDR.So_BK) + ",";
                strSql += objCTuHDR.Ngay_BK + "," + Globals.EscapeQuote(CTU_GNT_BL) + "," + Globals.EscapeQuote(objCTuHDR.So_CT) + ",'";
                strSql += objCTuHDR.TK_GL_NH + "',:TEN_KH_NH,'";
                strSql += objCTuHDR.TK_KB_NH + "'," + objCTuHDR.Mode + "," + objCTuHDR.PHI_GD + ",";
                strSql += objCTuHDR.PHI_VAT + "," + objCTuHDR.PHI_GD2 + "," + objCTuHDR.PHI_VAT2 + "," + objCTuHDR.TIME_BEGIN + ",'" + objCTuHDR.PT_TINHPHI + "','";
                strSql += objCTuHDR.SEQ_NO + "','" + objCTuHDR.REF_NO + "','";
                strSql += objCTuHDR.RM_REF_NO + "','" + objCTuHDR.QUAN_HUYEN_NNTIEN + "','" + objCTuHDR.TINH_TPHO_NNTIEN + "','" + objCTuHDR.MA_HQ + "','";
                strSql += objCTuHDR.LOAI_TT + "','" + objCTuHDR.TEN_HQ + "','" + objCTuHDR.Ma_hq_ph + "','" + objCTuHDR.TEN_HQ_PH + "','";
                strSql += objCTuHDR.MA_NH_TT + "','" + objCTuHDR.SAN_PHAM + "','" + objCTuHDR.TT_CITAD + "','";
                strSql += objCTuHDR.SO_CMND + "','" + objCTuHDR.SO_FONE + "','" + objCTuHDR.MA_NTK + "','" + objCTuHDR.Kenh_CT + "','" + objCTuHDR.LOAI_NNT + "', '" + objCTuHDR.strLoaiTS + "',:noiDung)";
                List<IDbDataParameter> lsthdr = new List<IDbDataParameter>();
                lsthdr.Add(DataAccess.NewDBParameter("Ten_NNTien", ParameterDirection.Input, objCTuHDR.Ten_NNTien, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("DC_NNTien", ParameterDirection.Input, objCTuHDR.DC_NNTien, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("huyenNguoiNopThay", ParameterDirection.Input, objCTuHDR.huyenNguoiNopThay, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("tinhNguoiNopThay", ParameterDirection.Input, objCTuHDR.tinhNguoiNopThay, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("Ten_NNThue", ParameterDirection.Input, objCTuHDR.Ten_NNThue, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("DC_NNThue", ParameterDirection.Input, objCTuHDR.DC_NNThue, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("TEN_CQTHU", ParameterDirection.Input, objCTuHDR.Ten_cqthu, DbType.String));

                //'" + objCTuHDR.DIENGIAI_HQ + "'
                lsthdr.Add(DataAccess.NewDBParameter("DIENGIAI_HQ", ParameterDirection.Input, objCTuHDR.DIENGIAI_HQ, DbType.String));
                //'" + objCTuHDR.Ten_NH_B + "'
                lsthdr.Add(DataAccess.NewDBParameter("Ten_NH_B", ParameterDirection.Input, objCTuHDR.Ten_NH_B, DbType.String));
                //'" + objCTuHDR.TEN_NH_TT + "'
                lsthdr.Add(DataAccess.NewDBParameter("TEN_NH_TT", ParameterDirection.Input, objCTuHDR.TEN_NH_TT, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("TEN_KH_NH", ParameterDirection.Input, objCTuHDR.TEN_KH_NH, DbType.String));
                lsthdr.Add(DataAccess.NewDBParameter("noiDung", ParameterDirection.Input, objCTuHDR.noiDung, DbType.String));
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text, lsthdr.ToArray(), transCT);
                //Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
                foreach (infChungTuDTL objDTL in objCTuDTL)
                {
                    // 'Kienvt : chi insert cac truong <>0
                    if (objDTL != null)
                        if (objDTL.SoTien > 0)
                        {
                            strSql = "Insert into TCS_CTU_DTL_MOBITCT(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," +
                              "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," +
                              "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,SO_CT,ma_dp, SO_TK) " +
                              "Values (" + objDTL.ID + ",'" + objDTL.SHKB + "'," + objDTL.Ngay_KB + "," + objDTL.Ma_NV + "," +
                              objDTL.So_BT + ",'" + objDTL.Ma_DThu + "'," + objDTL.CCH_ID + ",'" +
                              objDTL.Ma_Cap + "','" + objDTL.Ma_Chuong + "'," + objDTL.LKH_ID + ",'" +
                              objDTL.Ma_Loai + "','" + objDTL.Ma_Khoan + "'," +
                              objDTL.MTM_ID + ",'" + objDTL.Ma_Muc + "','" + objDTL.Ma_TMuc + "','" +
                              objDTL.Noi_Dung + "'," +
                              objDTL.DT_ID + ",'','" + objDTL.Ky_Thue + "'," +
                              objDTL.SoTien + "," + objDTL.SoTien_NT + "," + Globals.EscapeQuote(objDTL.MaQuy) + "," + Globals.EscapeQuote(objDTL.SO_CT) + ",'','"+ objDTL.SO_TK +"')";
                            //  ' Insert Vào bảng TCS_CTU_DTL    
                            DataAccess.ExecuteNonQuery(strSql, CommandType.Text, transCT);
                        }
                }
                //INSERT BANG LOG PAYMENT
                //strSql = "INSERT INTO tcs_log_payment(tran_code,tran_type,tran_date,product_code,branch_no,acct_no ,acct_type,acct_ccy,buy_rate,bnfc_bank_id  ,bnfc_bank_name,amt    ,fee,response_code,rm_ref_no,response_msg)";
                //strSql += "VALUES('TX002','1',SYSDATE,'TDT','" + objCTuHDR.Ma_CN + "','" + objCTuHDR.TK_KH_NH + "','D','" + objCTuHDR.Ma_NT + "',10000000,'" + objCTuHDR.TK_Co + "','" + objCTuHDR.MA_NH_B + "'," + objCTuHDR.TTien + "," + objCTuHDR.PHI_GD + ",'0','" + objCTuHDR.So_CT + "','" + objCTuHDR.So_CT_NH + "' )";
                //DataAccess.ExecuteNonQuery(strSql, CommandType.Text, transCT);
                //cap nhat lai so chung tu lenh SET_CTU_HDR_MOBITCT
                //TRANG THAI 08 CHO CORE CAT TIEN
                strSql = "UPDATE SET_CTU_HDR_MOBITCT A SET SO_CT='" + objCTuHDR.So_CT + "',TRANGTHAI='08',MA_NH_A=:MA_NH_A,TEN_NH_A=:TEN_NH_A,MA_NH_B=:MA_NH_B,TEN_NH_B=:TEN_NH_B "
                + " WHERE A.MATHAMCHIEU='" + strMaThamchieu + "' AND A.TRANGTHAI='02' AND NVL( A.SO_CT,'__')='__'";
                List<IDbDataParameter> lsthdrUPDATE = new List<IDbDataParameter>();
                lsthdrUPDATE.Add(DataAccess.NewDBParameter("MA_NH_A", ParameterDirection.Input, objCTuHDR.MA_NH_A, DbType.String));
                lsthdrUPDATE.Add(DataAccess.NewDBParameter("TEN_NH_A", ParameterDirection.Input, objCTuHDR.Ten_nh_a, DbType.String));
                lsthdrUPDATE.Add(DataAccess.NewDBParameter("MA_NH_B", ParameterDirection.Input, objCTuHDR.MA_NH_B, DbType.String));
                lsthdrUPDATE.Add(DataAccess.NewDBParameter("TEN_NH_B", ParameterDirection.Input, objCTuHDR.Ten_NH_B, DbType.String));
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text, lsthdrUPDATE.ToArray(), transCT);
                // ' Cập nhật dữ liệu vào CSDL
                transCT.Commit();


            }
            catch (Exception ex)
            {
                transCT.Rollback();
                log.Error(ex.StackTrace + ex.Message);
                throw ex;
            }
            finally
            {
                //   ' Giải phóng Connection tới CSDL
                if (transCT != null)
                    transCT.Dispose();

                if (conn != null)
                    conn.Dispose();

            }
        }
        public static void UPDATE_MOLIENKETTK_TRANGTHAI_04(string strID)
        {
            try
            {
                //cap nhat lai ma giao dich va trang thai cho tct xac nhan
                string strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET trangthai='04',NGAYHT=SYSDATE WHERE ID='" + strID + "'";
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                //strSQL = "UPDATE SET_DSTK_LIENKET_HUY SET Magiaodich='" + strMaGD + "',NGAYHT=SYSDATE,loaiotp='" + strLoaiOTP + "' WHERE ID='" + strID + "'";
                //DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static DateTime ConvertNumberToDate(string strText)
        {

            DateTime dt = new DateTime();
            string strDate = null;
            string strMonth = null;
            string strYear = null;
            try
            {
                strText = strText.Trim();
                if (strText.Length < 8)
                {
                    return DateTime.Now;
                }
                strDate = strText.Substring(6, 2);
                strMonth = strText.Substring(4, 2);
                strYear = strText.Substring(0, 4);

                int intDate = 0;
                int intMonth = 0;
                int intYear = 0;
                intDate = Convert.ToInt32(strDate);
                intMonth = Convert.ToInt32(strMonth);
                intYear = Convert.ToInt32(strYear);

                dt = new DateTime(intYear, intMonth, intDate);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                dt = DateTime.Now;
            }
            return dt;
        }
        private static string V_SubString(String s, int lenght)
        {
            if (s != null && s.Length > lenght)
            {
                s = s.Substring(0, lenght);
            }
            return s;

        }
        private static string builecomchitiet_V20(infChungTuDTL objChitiet)
        {
            string result = "";
            result += V_SubString(objChitiet.Ma_TMuc, 4);
            result += "~" + V_SubString(objChitiet.Ma_Chuong, 3);
            result += "~" + V_SubString(objChitiet.SoTien.ToString(), 20);
            result += "~" + V_SubString(objChitiet.Noi_Dung, 100);
            result += "~";//Khong co so quyet dinh /so to khai
            result += "~" + objChitiet.Ky_Thue;
            return result;
        }
        private static string buildRemark(string strSHKB, string strTenKB, string strMST_NNOP, string strMaDBHC, string strTK_CO, string strMa_CQThu, string strMA_LTHUE)
        {
            string result = "";
            string ngayNT = "";
            ngayNT = DateTime.Now.ToString("ddMMyyyy");
            //}
            result += "NTDT+KB:" + strSHKB;
            result += "-" + strTenKB;
            result += "+NgayNT:" + ngayNT;
            result += "+MST:" + strMST_NNOP;
            result += "+DBHC:" + strMaDBHC;
            result += "+TKNS:" + strTK_CO;
            result += "+CQT:" + strMa_CQThu;
            result += "+LThue:" + strMA_LTHUE;
            return result;
        }
        private static string GetTinhThanhNHTH(string ma_tinhthanh)
        {
            DataSet ds = null;
            try
            {
                string strSql = "Select ten  From TCS_DM_XA Where MA_XA= :MA_XA";
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "MA_XA";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = ma_tinhthanh + "TTT";
                ds = DataAccess.ExecuteReturnDataSet(strSql.ToString(), CommandType.Text, p);
                return ds.Tables[0].Rows[0][0].ToString();
            }
            catch
            {
                return "";
            }

        }
    }

}