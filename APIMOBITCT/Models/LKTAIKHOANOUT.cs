﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIMOBITCT.Models
{
    public class LKTAIKHOANOUT
    {
        public string TThai { get; set; }
        public string Mota { get; set; }
        public string Magiaodich { get; set; }
        public string loaiOTP{ get; set; }
        public string smartOTPUnlockCode { get; set; }

    }
}