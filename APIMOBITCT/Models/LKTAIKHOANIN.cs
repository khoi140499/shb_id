﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIMOBITCT.Models
{
    public class LKTAIKHOANIN
    {
        public string maDoiTac { get; set; }
        public string loai { get; set; }
        public string mst { get; set; }
        public string ten_nnt { get; set; }
        public string loaiGiayTo { get; set; }
        public string soGiayTo { get; set; }
        public string dienThoai { get; set; }
        public string phuongThuc { get; set; }
        public string soTaiKhoan_The { get; set; }
        public string ngayPhatHanh { get; set; }
        public string ewalletToken { get; set; }
    }
}