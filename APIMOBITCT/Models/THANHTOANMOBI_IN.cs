﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIMOBITCT.Models
{
    public class THANHTOANMOBI_IN
    {
        public string maDoiTac { get; set; }
        public string maThamChieu { get; set; }
        public string soTien { get; set; }
        public string phiChuyenPhat { get; set; }
        public string ngonNgu { get; set; }
        public string maTienTe { get; set; }
        public string thongTinGiaoDich { get; set; }
        public ThongTinBienLai thongTinBienLai { get; set; }
        public string thoiGianGD { get; set; }
        public string maNganHang { get; set; }
        public string phuongThuc { get; set; }
        public string soTaiKhoan_The { get; set; }
        public string ewalletToken { get; set; }
        public string SoTaiKhoan { get; set; }
        public string IdTaiKhoanLienKet { get; set; }
        public string dienThoai { get; set; }
        public string ma_NNTien { get; set; }
        public string ten_NNTien { get; set; }
        public string loaiGiayTo { get; set; }
        public string soTaiKhoanThe_Decrypt { get; set; }
    }
    public class ThongTinBienLai
    {
        public string maDichVu { get; set; }
        public string maHoSo { get; set; }
        public List<PhiLePhi> phiLePhi{ get; set; }
        public string tkthuHuong { get; set; }
        public string maLoaiHinhThuPhat { get; set; }
        public string maLoaiHinhThue { get; set; }
        public string tenLoaiHinhThue { get; set; }
        public string maCoQuanQD { get; set; }
        public string tenCoQuanQD { get; set; }
        public string khoBac { get; set; }
        public string ngayQD { get; set; }
        public string soQD { get; set; }
        public string mst { get; set; }
        public string hoTenNguoiNop { get; set; }
        public string soCMNDNguoiNop { get; set; }
        public string diaChiNguoiNop { get; set; }
        public string huyenNguoiNop { get; set; }
        public string tinhNguoiNop { get; set; }

        public string mstNopThay { get; set; }
        public string hoTenNguoiNopThay { get; set; }
        public string diaChiNguoiNopThay { get; set; }
        public string huyenNguoiNopThay { get; set; }
        public string tinhNguoiNopThay { get; set; }

        public string GetMaHuyen()
        {
            string strResult = "";
            try
            {
                string[] arr = huyenNguoiNop.Split('-');
                strResult = arr[0];
            }
            catch (Exception ex)
            { }
            return strResult;
        }
        public string GetMaTinh()
        {
            string strResult = "";
            try
            {
                string[] arr = tinhNguoiNop.Split('-');
                strResult = arr[0];
            }
            catch (Exception ex)
            { }
            return strResult;
        }
        public List< DSkhoanNop> dskhoanNop{ get; set; }

    }
    public class PhiLePhi
    {
        public string loaiPhiLePhi { get; set; }
        public string maPhiLePhi { get; set; }
        public string tenPhiLePhi { get; set; }

        public string soTien { get; set; }
        public string GetMaTM() {
            string strResult = "";
            try
            {
                string[] arr = maPhiLePhi.Split('+');
                strResult = arr[2];
            }
            catch (Exception ex)
            { }
            return strResult;
        }
        public string GetMaChuong()
        {
            string strResult = "";
            try
            {
                string[] arr = maPhiLePhi.Split('+');
                strResult = arr[1];
            }
            catch (Exception ex)
            { }
            return strResult;
        }
        public string GetMA_DBHC()
        {
            string strResult = "";
            try
            {
                string[] arr = maPhiLePhi.Split('+');
                strResult = arr[3];
            }
            catch (Exception ex)
            { }
            return strResult;
        }
        public string GetSO_QUYET_DINH()
        {
            string strResult = "";
            try
            {
                string[] arr = loaiPhiLePhi.Split('+');
                strResult = arr[1];
            }
            catch (Exception ex)
            { }
            return strResult;
        }
        public string GetNGAY_QUYET_DINH()
        {
            string strResult = "";
            try
            {
                string[] arr = loaiPhiLePhi.Split('+');
                strResult = arr[2];
            }
            catch (Exception ex)
            { }
            return strResult;
        }
        public string GetSO_KHUNG()
        {
            string strResult = "";
            try
            {
                string[] arr = loaiPhiLePhi.Split('+');
                strResult = arr[3];
            }
            catch (Exception ex)
            { }
            return strResult;
        }
        public string GetSO_MAY()
        {
            string strResult = "";
            try
            {
                string[] arr = loaiPhiLePhi.Split('+');
                strResult = arr[4];
            }
            catch (Exception ex)
            { }
            return strResult;
        }
    }
    public class DSkhoanNop
    {
        public string noiDung { get; set; }
        public string soTien { get; set; }
        public string loaiTs { get; set; }
    }

}