﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIMOBITCT
{
    public class TCTObjectIn
    {
       public string duLieu{get;set;}
       public Signature signature;
    }
    public class Signature
    {
        public string value{get;set;}
        public string certificates { get; set; }
    }
}