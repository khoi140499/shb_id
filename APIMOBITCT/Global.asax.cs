﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using VBOracleLib;

namespace APIMOBITCT
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            Thread t1 = new Thread(ProcessAutoExprireOTP);
            t1.Start();
        }

        private void ProcessAutoExprireOTP()
        {
            string strMaxAutoProcess = ConfigurationSettings.AppSettings.Get("Mobile.MaxAutoProcess").ToString();
            string timeExpire = ConfigurationSettings.AppSettings.Get("Mobile.TimeExpire").ToString();
            bool check = true;
            //expire OTP cho mở hủy liên kết và thanh toán
            int x = 0;
            do
            {
                //cập nhật expire OTP cho mở/hủy liên kết
                try
                {
                    string strSQL = "SELECT ID FROM SET_DSTK_LIENKET_LOG WHERE  (NGAYHT + (1/1440*+ " + timeExpire + " )) < sysdate  ";
                    strSQL += " AND (TRANGTHAI IN ('02') AND AUTOPROCESS >= 0 AND AUTOPROCESS < " + strMaxAutoProcess + ")  AND NGAYHT IS NOT NULL AND rownum < 20  ORDER BY ID";
                    DataSet ds = new DataSet();

                    ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {
                                    try
                                    {
                                        //cap nhat trang thai xu ly tu dong    
                                        strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET AUTOPROCESS=AUTOPROCESS+1 ,trangthai='04',NGAYHT=SYSDATE WHERE ID='" + ds.Tables[0].Rows[i][0].ToString() + "' ";
                                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                    }
                                    catch (Exception exc)
                                    { }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Thread.Sleep(5000);
                }
                //cập nhật expire OTP cho thanh toán
                try
                {
                    string strSQL = "SELECT MATHAMCHIEU FROM SET_CTU_HDR_MOBITCT WHERE (NGAY_HT + (1/1440*+ " + timeExpire + " )) < sysdate ";
                    strSQL += " AND (TRANGTHAI IN ('02') AND AUTOPROCESS >= 0 AND AUTOPROCESS<" + strMaxAutoProcess + ") and rownum < 20 ORDER BY MATHAMCHIEU";
                    DataSet ds = new DataSet();

                    ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {
                                    try
                                    {
                                        //cap nhat trang thai xu ly tu dong    
                                        strSQL = "UPDATE SET_CTU_HDR_MOBITCT SET AUTOPROCESS=AUTOPROCESS+1 ,trangthai='04',NGAY_HT=SYSDATE WHERE MATHAMCHIEU='" + ds.Tables[0].Rows[i][0].ToString() + "' ";
                                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                    }
                                    catch (Exception exc)
                                    { }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Thread.Sleep(5000);
                }
                x++;
                if (x >= 20)
                {
                    x = 0;
                    Thread.Sleep(5000);
                }
                else
                { Thread.Sleep(1000); }

            } while (check == false);
        }
    }
}