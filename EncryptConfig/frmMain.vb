﻿Public Class frmMain

    Private Sub btnEncrypt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEncrypt.Click
        Try
            If (txtInput.Text.Trim.Length = 0) Then
                MessageBox.Show("Bạn hãy nhập dữ liệu đầu vào")
                txtInput.Focus()
                Exit Sub
            End If

            If (txtKey.Text.Trim.Length = 0) Then
                MessageBox.Show("Bạn hãy nhập chuỗi khóa")
                txtKey.Focus()
                Exit Sub
            End If

            If Not txtKey.Text.Trim.Length = 4 Then
                MessageBox.Show("Chuỗi khóa phải có độ dài = 4")
                txtKey.Focus()
                Exit Sub
            End If

            Dim v_cbo As String = cboLoai.Text
            Dim v_intType As Integer = 1
            If (v_cbo.Equals("DATABASE")) Then
                v_intType = 1
            ElseIf (v_cbo.Equals("OSB")) Then
                v_intType = 2
            ElseIf (v_cbo.Equals("PFX")) Then
                v_intType = 3
            ElseIf (v_cbo.Equals("CITAD")) Then
                v_intType = 4
            End If


            Dim v_Encrypt As String = VBOracleLib.Security.EncryptStr(txtInput.Text.Trim, txtKey.Text.Trim, v_intType)

            txtOutput.Text = v_Encrypt

        Catch ex As Exception
            MessageBox.Show("Có lỗi khi mã hóa chuỗi config - Mô tả:" & ex.Message)
        End Try
    End Sub

    Private Sub btnDecrypt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDecrypt.Click
        Try
            If (txtInput.Text.Trim.Length = 0) Then
                MessageBox.Show("Bạn hãy nhập dữ liệu đầu vào")
                txtInput.Focus()
                Exit Sub
            End If

            If (txtKey.Text.Trim.Length = 0) Then
                MessageBox.Show("Bạn hãy nhập chuỗi khóa")
                txtKey.Focus()
                Exit Sub
            End If

            If Not txtKey.Text.Trim.Length = 4 Then
                MessageBox.Show("Chuỗi khóa phải có độ dài = 4")
                txtKey.Focus()
                Exit Sub
            End If

            Dim v_cbo As String = cboLoai.Text
            Dim v_intType As Integer = 1
            If (v_cbo.Equals("DATABASE")) Then
                v_intType = 1
            ElseIf (v_cbo.Equals("OSB")) Then
                v_intType = 2
            ElseIf (v_cbo.Equals("PFX")) Then
                v_intType = 3
            ElseIf (v_cbo.Equals("CITAD")) Then
                v_intType = 4
            End If

            Dim v_Encrypt As String = VBOracleLib.Security.DescryptStr(txtInput.Text.Trim, txtKey.Text.Trim, v_intType)

            txtOutput.Text = v_Encrypt

        Catch ex As Exception
            MessageBox.Show("Có lỗi khi giải mã hóa chuỗi config - Mô tả:" & ex.Message)
        End Try
    End Sub
End Class
