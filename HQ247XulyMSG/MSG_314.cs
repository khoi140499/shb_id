﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using VBOracleLib;
using Business_HQ;
using Business_HQ.Common;
using Business_HQ.nhothu;
using System.IO;
namespace HQ247XulyMSG
{
    public class MSG_314
    {
        public string AccountHQ247 = "";
        public string Ma_AccountHQ247 = "";
        public string strSender_Code = "";
        public string strMaxAutoProcess = "";
        public string strWEB = "N";
         public MSG_314()
        {
            try
            {
                AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.AccountHQ247").ToString();
                Ma_AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.Ma_AccountHQ247").ToString();
                strSender_Code = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString();

            }
            catch (Exception ex)
            { 
            }
        }
         public MSG_314(string pvStrWeb)
        {
            strWEB = pvStrWeb;
            try
            {
                AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.AccountHQ247").ToString();
                Ma_AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.Ma_AccountHQ247").ToString();
                strSender_Code = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString();

            }
            catch (Exception ex)
            { 
            }
        }
         public decimal IsHQ314TMP(string strID)
         {
             try
             {
                 string strSQL = "SELECT * FROM TCS_HQ_311_TMP WHERE ID='" + strID + "' ";
                 DataSet ds = new DataSet();
                 ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                 if (ds != null)
                     if (ds.Tables.Count > 0)
                         if (ds.Tables[0].Rows.Count > 0)
                             return 0;
             }
             catch (Exception ex)
             {
                 return 1;
             }
             return 1;
         }
         public string ProcessData314(string StrID314)
         {
             string strTrangThai = "01";
             try
             {
                 string strHuongDan = "";
                 string strSQL = "select * from TCS_HQ247_314_TMP where id='" + StrID314 + "'";
                 DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                 string strLoaiHS = "0";

                 string strLoai213 = "1";

                 if (ds != null)
                     if (ds.Tables.Count > 0)
                         if (ds.Tables[0].Rows.Count > 0)
                         {
                             strLoaiHS = ds.Tables[0].Rows[0]["LOAI_HS"].ToString();
                             strTrangThai = ds.Tables[0].Rows[0]["TRANGTHAI"].ToString();
                             strHuongDan = ds.Tables[0].Rows[0]["LY_DO_HUY"].ToString();
                             strLoai213 = ds.Tables[0].Rows[0]["LOAIMSG213"].ToString();
                         }
                 if (strTrangThai.Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_MOINHAN))
                 {
                     strLoai213 = "1";
                     //neu la them moi
                     if (strLoaiHS.Equals("1"))
                     {
                         strHuongDan = getNoiDungHDmoi311();
                     }
                     //neu la sua thong tin
                     if (strLoaiHS.Equals("2"))
                     {
                         strHuongDan = getNoiDungHDSua311();
                     }
                     //neu la huy
                     if (strLoaiHS.Equals("3"))
                     {
                         strHuongDan = getNoiDungHDHuy311();
                     }
                     decimal decOk =  daTCS_HQ247_314.MakerMSG213(StrID314, strHuongDan, strLoai213, AccountHQ247);
                     if (decOk == 0)
                     {
                         strTrangThai = mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213;
                     }
                     else
                     {
                         return "Loi xu ly 311[" + StrID314 + "] o trang thai [" + mdlCommon.TCS_HQ247_314_TRANGTHAI_MOINHAN + "]: " + mdlCommon.Get_ErrorDesciption(decOk.ToString());
                     }
                 }
                 if (strTrangThai.Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213))
                 {
                     decimal decOk = daTCS_HQ247_314.CheckerMSG213(StrID314, strHuongDan, strLoai213, AccountHQ247);
                     if (decOk == 0)
                     {
                         strTrangThai = mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213;
                     }
                     else
                     {
                         return "Loi xu ly 311[" + StrID314 + "] o trang thai [" + mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213 + "]: " + mdlCommon.Get_ErrorDesciption(decOk.ToString());
                     }
                 }
                 if (strTrangThai.Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213))
                 {
                     if (!strLoaiHS.Equals(""))
                     {

                         decimal decOk = CustomsServiceV3.ProcessMSG.sendMSG213_314("314", strLoai213, strHuongDan, StrID314, AccountHQ247);
                         //decimal decOk = pro.sendMSG213("311", strLoai213, strHuongDan, StrID311, AccountHQ247);
                         if (decOk == 0)
                         {
                             strTrangThai = mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213;
                         }
                         else
                         {
                             return "Loi xu ly 311[" + StrID314 + "] o trang thai [" + mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213 + "]: " + mdlCommon.Get_ErrorDesciption(decOk.ToString());
                         }
                     }

                 }
                 return "Xu ly 311[" + StrID314 + "] thanh cong [" + strTrangThai + "]: ";

             }
             catch (Exception ex)
             {
                 return "Loi xu ly 311[" + StrID314 + "] o trang thai [" + strTrangThai + "]: " + ex.Message;
             }
             return "";

         }
        //DOAN NAY EM THUC HIEN XU LY HET HAN TU DONG
         public string XuLyHetHanTD_314(string strID)
         {
             string strTrangThai = "-1";
             try
             {
                 string strSO_HS="";
                 string strSQL = "";
                 //kiem tra xem co du lieu trong hq_314_tmp xem co du lieu chua
                 strSQL = "SELECT A.* FROM TCS_HQ247_314_TMP A where A.ID='" + strID + "'";
                 DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                 //neu chua co thi xu ly
                 if (Business_HQ.Common.mdlCommon.IsEmptyDataSet(ds))
                 { 
                       strSQL = "SELECT A.* FROM TCS_HQ247_314 A where A.ID='" + strID + "'";
                        ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                        decimal decOk  = Business_HQ.nhothu.daTCS_HQ247_314.fnc_Maker312_HuyTaiNH(ds.Tables[0].Rows[0]["SO_HS"].ToString(), AccountHQ247);
                    if (decOk == Business_HQ.Common.mdlCommon.TCS_HQ247_OK)
                    {
                        strTrangThai=Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312;
                        strSO_HS = ds.Tables[0].Rows[0]["SO_HS"].ToString();
                    }else
                    {
                        return "Loi xu ly 314[" + strID + "] o trang thai [" + strTrangThai + "]: khi thuc hien maker 314 huy " ;
                    }
                 }else
                 {
                     //neu da co thi lay trang thai
                    strTrangThai=ds.Tables[0].Rows[0]["TRANGTHAI"].ToString();
                    strSO_HS = ds.Tables[0].Rows[0]["SO_HS"].ToString();
                 }
                 //neu trang thai 
                 //cho duyet
                 if (strTrangThai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312))
                 { 
                     string strHQErrorCode  = "0";
                     string strHQErrorMSG  = "";
                     decimal errID  = daTCS_HQ247_314.CheckerMSG312(strID, "He thong tu dong huy", "1", AccountHQ247);
                     if (errID == Business_HQ.Common.mdlCommon.TCS_HQ247_OK)
                     {
                        
                         strTrangThai = Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312;
                     }else
                     {
                        return "Loi xu ly 314[" + strID + "] o trang thai [" + strTrangThai + "]: khi thuc hien checker 314 huy " ;
                     }
                 }
                 if (strTrangThai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312))
                 {
                     string strHQErrorCode = "0";
                     string strHQErrorMSG = "";
                     decimal errID = CustomsServiceV3.ProcessMSG.sendMSG314("314", "He thong tu dong huy", strID, AccountHQ247,ref strHQErrorCode,ref strHQErrorMSG);
                     if (errID == Business_HQ.Common.mdlCommon.TCS_HQ247_OK)
                     {
                        // strTrangThai = Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312;
                         return "Xu ly 314[" + strID + "] huy tu dong  trang thai [" + strTrangThai + "] ";
                     }
                     else
                     {
                         return "Loi xu ly huy tu dong 314[" + strID + "] o trang thai [" + strTrangThai + "]: khi thuc hien checker 314 huy ";
                     }
                 }
              }
             catch (Exception ex)
             {
                 return "Loi xu ly 314[" + strID + "] o trang thai [" + strTrangThai + "]: " + ex.Message;
             }
             return "";
         }
         private string getNoiDungHDmoi311()
         {
             try
             {
                 string strData = "";
                 if (strWEB.Equals("N"))
                 {
                     strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_01.txt");
                 }
                 else
                 {
                     strData = File.ReadAllText(ConfigurationSettings.AppSettings.Get("HQ247_NDPHDANGKY") + "\\NoidungHD311_01.txt");
                 }
                 return strData;
             }
             catch (Exception ex)
             { }
             return "Quý khách hàng vui lòng chờ trong ít phút nhân viên PGBank sẽ liên hệ trực tiếp đến khách hàng để thực hiện.";
         }
         private string getNoiDungHDSua311()
         {
             try
             {
                 string strData = "";
                 //strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_02.txt");
                 if (strWEB.Equals("N"))
                 {
                     strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_02.txt");
                 }
                 else
                 {
                     strData = File.ReadAllText(ConfigurationSettings.AppSettings.Get("HQ247_NDPHDANGKY") + "\\NoidungHD311_02.txt");
                 }
                 return strData;
             }
             catch (Exception ex)
             { }
             return "Quý khách hàng vui lòng chờ trong ít phút nhân viên PGBank sẽ liên hệ trực tiếp đến khách hàng để thực hiện.";
         }
         private string getNoiDungHDHuy311()
         {
             try
             {
                 string strData = "";
                 // strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_03.txt");
                 if (strWEB.Equals("N"))
                 {
                     strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_03.txt");
                 }
                 else
                 {
                     strData = File.ReadAllText(ConfigurationSettings.AppSettings.Get("HQ247_NDPHDANGKY") + "\\NoidungHD311_03.txt");
                 }
                 return strData;
             }
             catch (Exception ex)
             { }
             return "Quý khách hàng vui lòng chờ trong ít phút nhân viên PGBank sẽ liên hệ trực tiếp đến khách hàng để thực hiện.";
         }
         public decimal xulyHQ314TMP(string strID)
         {
             try
             {
                 
             }
             catch (Exception ex)
             { }
             return 1;
         }
    }
}
