﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data;
using VBOracleLib;
using Business_HQ;
using Business_HQ.Common;
using Business_HQ.HQ247;
using CustomsV3.MSG.MSG311;
using Business_HQ.nhothu;
namespace HQ247XulyMSG
{
    public class MSG_XULY311
    {
        public string AccountHQ247 = "";
        public string Ma_AccountHQ247 = "";
        public string strSender_Code = "";
        public string strMaxAutoProcess = "";
        public string strWEB="N";
        public MSG_XULY311()
        {
            try
            {
                AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.AccountHQ247").ToString();
                Ma_AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.Ma_AccountHQ247").ToString();
                strSender_Code = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString();

            }
            catch (Exception ex)
            { }
        }
        public MSG_XULY311(string pvStrWeb)
        {
            strWEB = pvStrWeb;
            try
            {
                AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.AccountHQ247").ToString();
                Ma_AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.Ma_AccountHQ247").ToString();
                strSender_Code = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString();

            }
            catch (Exception ex)
            { }
        }
        public decimal IsHQ311TMP(string strID)
        {
            try
            {
                string strSQL = "SELECT * FROM TCS_HQ_311_TMP WHERE ID='" + strID + "' ";
                DataSet ds  = new DataSet();
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                    if(ds !=null)
                        if(ds.Tables.Count>0)
                            if(ds.Tables[0].Rows.Count>0)
                                return 0;
            }
            catch (Exception ex)
            {
                return 1;
            }
            return 1;
        }
        public decimal xulyHQ311TMP(string strID)
        {
            try
            {
                string strMSGType ="299";
                string strMSG ="";
                string strSQL = "SELECT * FROM TCS_HQ_311_TMP WHERE ID='" + strID + "' ";
                DataSet ds = new DataSet();
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            strMSG = ds.Tables[0].Rows[0]["MSG_IN"].ToString();
                        }
                MSG311 obj311 = new MSG311();
                strMSG = strMSG.Replace("<Customs>", CustomsServiceV3.ProcessMSG.strXMLdefin2);
                //strMSG = CustomsServiceV3.ProcessMSG.ProcessMSG311(strMSG,ref strMSGType);
                //---------------------------------
                obj311 = obj311.MSGToObject(strMSG);
                //neu la case sua
                if (obj311.Document.Data.Loai_HS.Equals("2"))
                {
                    decimal decResult = 0;
                    string strSo_HS = obj311.Document.Data.So_HS;
                    //co duoc phep thuc hien sua khong
                    decResult = Business_HQ.HQ247.daTCS_DM_NTDT_HQ.fnc_checkTrangThaiHS(obj311.Document.Data.So_HS, "2");
                    //neu tra va =0 thi theo quy trinh sua HQ tai khoan BT
                    if (decResult == 0)
                    {
                        DataSet dsHQ = VBOracleLib.DataAccess.ExecuteReturnDataSet("select ID,SO_HS,CUR_TEN_TAIKHOAN_TH,CUR_TAIKHOAN_TH from TCS_DM_NTDT_HQ where SO_HS='" + strSo_HS + "' and TRANGTHAI = '" + Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE + "'", CommandType.Text);
                        string strCUR_TEN_TAIKHOAN_TH = "";
                        string strCUR_TAIKHOAN_TH = "";
                        string strID311 = dsHQ.Tables[0].Rows[0]["ID"].ToString();
                        decimal decErrorID = 1;
                        decErrorID = Business_HQ.HQ247.daTCS_DM_NTDT_HQ.UPDATE_HQ_GET311_LOAI_HS_2(strSo_HS);
                        //- (2) Insert HS sửa nhận được này vào bảng HQ_311
                        CustomsServiceV3.ProcessMSG.INSERTMSG311(obj311, "'" + strID311 + "'");
                        //- (3) Update CUR_TEN_TAIKHOAN_TH và CUR_TAIKHOAN_TH của HS này trong bảng HQ_311 
                        // thành CUR_TEN_TAIKHOAN_TH và CUR_TAIKHOAN_TH của HS này trong bảng HQ; 
                        // SET TRANGTHAI cua HS nay trong bang HQ thanh 01
                        strCUR_TEN_TAIKHOAN_TH = dsHQ.Tables[0].Rows[0]["CUR_TEN_TAIKHOAN_TH"].ToString();
                        strCUR_TAIKHOAN_TH = dsHQ.Tables[0].Rows[0]["CUR_TAIKHOAN_TH"].ToString();
                        if (decErrorID == 0)
                        {
                            decErrorID = Business_HQ.HQ247.daTCS_DM_NTDT_HQ.UPDATE_HQ311_GET311_LOAI_HS_2(strSo_HS, strCUR_TEN_TAIKHOAN_TH, strCUR_TAIKHOAN_TH, "01");
                            //- (4) Send msg 200 cho TCHQ
                            if (decErrorID == 0)
                            {
                                strMSGType = "200";

                            }
                            else strMSGType = "299";
                        }
                        else strMSGType = "299";
                    }
                }
                //neu la huy
                else if (obj311.Document.Data.Loai_HS.Equals("3"))
                {
                    decimal decResult = Business_HQ.HQ247.daTCS_DM_NTDT_HQ.fnc_checkTrangThaiHS(obj311.Document.Data.So_HS, "3");
                    if (decResult == 0)
                    {
                        decResult = Business_HQ.HQ247.daTCS_DM_NTDT_HQ.fnc_UpdateHuy311_fromHQ(obj311.Document.Data.So_HS, "3", obj311.Document.Header.Transaction_ID, "", AccountHQ247);
                        if (decResult == 0)
                        {
                            strMSGType = "200";
                            //vinhvv
                            //Huy 311 xong, neu co nho thu thi phai huy ca nho thu
                             decResult = daTCS_HQ247_314.fnc_UpdateHuy314_fromHQ(obj311.Document.Data.So_HS, "3", obj311.Document.Header.Transaction_ID, "", AccountHQ247);
                        }
                        else strMSGType = "299";
                    }
                    else strMSGType = "299";



                }
                //---------------------------------
                //neu la 200 thi xoa temp
                if (strMSGType.Equals("200"))
                {
                    strSQL = "DELETE FROM TCS_HQ_311_TMP WHERE ID='" + strID + "' ";
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);

                    return 0;
                }
                else
                {
                    strSQL = "UPDATE  TCS_HQ_311_TMP SET DESCRIPTION='LOI LAN XU LY ["+ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") +"]' WHERE ID='" + strID + "' ";
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                }
            }
            catch (Exception ex)
            { }
            return 1;
        }
        public string ProcessData311(string StrID311)
        {
            string strTrangThai = "01";
            try
            {
                string strHuongDan = "";
                string strSQL = "select * from tcs_dm_ntdt_hq_311 where id='" + StrID311 + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                string strLoaiHS = "0";

                string strLoai213 = "1";

                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            strLoaiHS = ds.Tables[0].Rows[0]["LOAI_HS"].ToString();
                            strTrangThai = ds.Tables[0].Rows[0]["TRANGTHAI"].ToString();
                            strHuongDan = ds.Tables[0].Rows[0]["LY_DO_HUY"].ToString();
                            strLoai213 = ds.Tables[0].Rows[0]["LOAIMSG213"].ToString();
                        }
                if (strTrangThai.Equals(mdlCommon.TCS_HQ247_HS_TRANGTHAI_MOINHAN))
                {
                    strLoai213 = "1";
                    //neu la them moi
                    if (strLoaiHS.Equals("1"))
                    {
                        strHuongDan = getNoiDungHDmoi311();
                    }
                    //neu la sua thong tin
                    if (strLoaiHS.Equals("2"))
                    {
                        strHuongDan = getNoiDungHDSua311();
                    }
                    //neu la huy
                    if (strLoaiHS.Equals("3"))
                    {
                        strHuongDan = getNoiDungHDHuy311();
                    }
                    decimal decOk = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG213(StrID311, strHuongDan, strLoai213, AccountHQ247);
                    if (decOk == 0)
                    {
                        strTrangThai = mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER213;
                    }
                    else
                    {
                        return "Loi xu ly 311[" + StrID311 + "] o trang thai [" + mdlCommon.TCS_HQ247_HS_TRANGTHAI_MOINHAN + "]: " + mdlCommon.Get_ErrorDesciption(decOk.ToString());
                    }
                }
                if (strTrangThai.Equals(mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER213))
                {
                    decimal decOk = TCS_DM_NTDT_HQ.CheckerMSG213(StrID311, strHuongDan, strLoai213, AccountHQ247);
                    if (decOk == 0)
                    {
                        strTrangThai = mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213;
                    }
                    else
                    {
                        return "Loi xu ly 311[" + StrID311 + "] o trang thai [" + mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER213 + "]: " + mdlCommon.Get_ErrorDesciption(decOk.ToString());
                    }
                }
                if (strTrangThai.Equals(mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213))
                {
                    if (!strLoaiHS.Equals(""))
                    {
                        decimal decOk = CustomsServiceV3.ProcessMSG.sendMSG213("311", strLoai213, strHuongDan, StrID311, AccountHQ247);
                        if (decOk == 0)
                        {
                            strTrangThai = mdlCommon.TCS_HQ247_HS_TRANGTHAI_HQ_RESPONSE213;
                        }
                        else
                        {
                            return "Loi xu ly 311[" + StrID311 + "] o trang thai [" + mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213 + "]: " + mdlCommon.Get_ErrorDesciption(decOk.ToString());
                        }
                    }
                     
                }
                return "Xu ly 311[" + StrID311 + "] thanh cong [" + strTrangThai + "]: ";

            }
            catch (Exception ex)
            {
                return "Loi xu ly 311[" + StrID311 + "] o trang thai [" + strTrangThai + "]: " + ex.Message;
            }
            return "";

        }
        private string getNoiDungHDmoi311()
        {
            try
            {
                string strData = "";
                if (strWEB.Equals("N"))
                {
                    strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_01.txt");
                }
                else
                {
                    strData = File.ReadAllText(ConfigurationSettings.AppSettings.Get("HQ247_NDPHDANGKY") + "\\NoidungHD311_01.txt");
                }
                return strData;
            }
            catch (Exception ex)
            { }
            return "Quý khách hàng vui lòng chờ trong ít phút nhân viên SHB sẽ liên hệ trực tiếp đến khách hàng để thực hiện.";
        }
        private string getNoiDungHDSua311()
        {
            try
            {
                string strData = "";
                //strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_02.txt");
                if (strWEB.Equals("N"))
                {
                    strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_02.txt");
                }
                else
                {
                    strData = File.ReadAllText(ConfigurationSettings.AppSettings.Get("HQ246_NDPHDANGKY") + "\\NoidungHD311_02.txt");
                }
                return strData;
            }
            catch (Exception ex)
            { }
            return "Quý khách hàng vui lòng chờ trong ít phút nhân viên SHB sẽ liên hệ trực tiếp đến khách hàng để thực hiện.";
        }
        private string getNoiDungHDHuy311()
        {
            try
            {
                string strData = "";
               // strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_03.txt");
                if (strWEB.Equals("N"))
                {
                    strData = File.ReadAllText(Directory.GetCurrentDirectory() + "\\NoidungHD311_03.txt");
                }
                else
                {
                    strData = File.ReadAllText(ConfigurationSettings.AppSettings.Get("HQ246_NDPHDANGKY") + "\\NoidungHD311_03.txt");
                }
                return strData;
            }
            catch (Exception ex)
            { }
            return "Quý khách hàng vui lòng chờ trong ít phút nhân viên SHB sẽ liên hệ trực tiếp đến khách hàng để thực hiện.";
        }
    }
}
