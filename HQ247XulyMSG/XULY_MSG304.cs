﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using VBOracleLib;
using Business_HQ.HQ247;
using Business_HQ.Common;
using CustomsV3.MSG.MSG304;
using System.Configuration;
using CorebankServiceESB;
using System.Data.OracleClient;
namespace HQ247XulyMSG
{
    public class XULY_MSG304
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string AccountHQ247 = "";
        public string Ma_AccountHQ247 = "";
        public string strSender_Code = "";
        public string strMaxAutoProcess = "";
        private static String Core_version = ConfigurationSettings.AppSettings.Get("CORE.VERSION").ToString();
        public XULY_MSG304()
        {
            try
            {
                AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.AccountHQ247").ToString();
                Ma_AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.Ma_AccountHQ247").ToString();
                strSender_Code = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString();

            }
            catch (Exception ex)
            { }
        }

        public XULY_MSG304(string pvStrAccountHQ247, string pvStrMa_AccountHQ247, string pvstrSender_Code)
        {

            AccountHQ247 = pvStrAccountHQ247;
            Ma_AccountHQ247 = pvStrMa_AccountHQ247;
            strSender_Code = pvstrSender_Code;
        }
        public string ProcessInsertDCNHHQ_HQ247(string strID)
        {
            try
            {
                string strSQL = "SELECT * FROM TCS_HQ247_304 WHERE ID='" + strID + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                MSG304 obj = new MSG304();
                obj.SETDATA(ds.Tables[0].Rows[0]["MSG_CONTENT"].ToString());
                INSERT_KQDCThueToDB_NTDT304(obj);
                if (ds.Tables[0].Rows[0]["TRANGTHAI"].ToString().Equals("04"))
                {
                    string strso_ct = ds.Tables[0].Rows[0]["SO_CTU"].ToString();
                    string pv_so_tn_ct = ds.Tables[0].Rows[0]["SO_TN_CT"].ToString();
                    string v_strTransaction_id = ds.Tables[0].Rows[0]["MSG_ID301"].ToString();
                    string pv_ngay_tn_ct = ds.Tables[0].Rows[0]["NGAY_TN_CT"].ToString();
                    string strErrorNum = "0";
                    string strErrorMes = "";

                    if (strso_ct.Length <= 0)
                        return "Xu ly loi so_ctu trong TCS_HQ247_304 emty khong insert duoc doi chieu 801 ";
                    if (pv_so_tn_ct.Length <= 0)
                        return "Xu ly loi SO_TN_CT trong TCS_HQ247_304 emty khong insert duoc doi chieu 801 ";
                    if (v_strTransaction_id.Length <= 0)
                        return "Xu ly loi MSG_ID301 trong TCS_HQ247_304 emty khong insert duoc doi chieu 801 ";
                    if (pv_ngay_tn_ct.Length <= 0)
                        return "Xu ly loi NGAY_TN_CT trong TCS_HQ247_304 emty khong insert duoc doi chieu 801 ";


                    if (pv_so_tn_ct.Length > 0)
                    {
                        string strSQLDel = "DELETE FROM tcs_dchieu_nhhq_hdr WHERE transaction_id='" + v_strTransaction_id + "'";
                        DataAccess.ExecuteNonQuery(strSQLDel, CommandType.Text);
                        Business_HQ.infDoiChieuNHHQ_HDR hdr = new Business_HQ.infDoiChieuNHHQ_HDR();
                        Business_HQ.infDoiChieuNHHQ_DTL[] dtls = null;
                        //insert doi chieu
                        SetObjectToDoiChieuNHHQ(obj.Data.ThongTinChungTu.Ma_KB, strso_ct, "", Ma_AccountHQ247, "CTU", ref hdr, ref dtls, obj.Data.ThongTinChungTu.Ma_KB, strErrorNum, mdlCommon.TransactionHQ_Type.M47.ToString(), "", pv_so_tn_ct, pv_ngay_tn_ct);
                        hdr.transaction_id = v_strTransaction_id;
                        hdr.KENHGD = "HQ247";

                        //'sonmt override ngay_bn, ngay_bc
                        //hdr.ngay_bn = "to_date('" & ngay_bao_no.ToString("dd/MM/yyyy HH:mm:ss") & "', 'dd/MM/yyyy HH24:MI:SS')"
                        //hdr.ngay_bc = "to_date('" & ngay_bao_co.ToString("dd/MM/yyyy HH:mm:ss") & "', 'dd/MM/yyyy HH24:MI:SS')"
                        Business_HQ.ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, "1", strErrorMes);
                    }
                }
            }
            catch (Exception ex)
            {
               log.Error("XULY_MSG304->ProcessInsertDCNHHQ_HQ247: Xu ly loi : " + ex.Message+"-"+ ex.StackTrace);
                return "Xu ly loi : " + ex.Message;
            }
            return "Xu ly thanh cong";
        }
        public static void SetObjectToDoiChieuNHHQ(string pv_strSHKB, string pv_strSoCT, string pv_strSoBT, string pv_strMaNV,
                                              string pv_strType, ref Business_HQ.infDoiChieuNHHQ_HDR hdr, ref Business_HQ.infDoiChieuNHHQ_DTL[] dtls,
                                              string pv_strMaDT = "", string pv_strErroCodeHQ = "", string pv_strTransactionType = "",
                                              string pv_strNgayKB = "", string pv_strSo_TN_CT = "", string pv_strNgay_TN_CT = "", bool p_flagBL = false)
        {
            string strSender_Code = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
            string strSender_Name = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Name").ToString();

            DataTable hdrBL = new DataTable();
            DataTable dtlsBL = new DataTable();
            Business_HQ.infDoiChieuNHHQ_DTL objDTL = null;
            List<Business_HQ.infDoiChieuNHHQ_DTL> lstdtl = new List<Business_HQ.infDoiChieuNHHQ_DTL>();
            int k = 0;
            try
            {


                //'Lay thong tin ChungTu
                if (pv_strType == "CTU")
                {

                    hdrBL = daCTUHQ304.SelectCT_HDRHQ247(pv_strSoCT).Tables[0];

                    dtlsBL = daCTUHQ304.SelectCT_DTLHQ247(pv_strSoCT).Tables[0];
                }
                else
                {
                    return;

                }


                if (hdrBL.Rows.Count > 0)
                {
                    hdr.shkb = hdrBL.Rows[0]["shkb"].ToString();
                    hdr.ngay_kb = long.Parse(hdrBL.Rows[0]["ngay_kb"].ToString());
                    hdr.so_bt = long.Parse(hdrBL.Rows[0]["so_bt"].ToString());
                    hdr.kyhieu_ct = hdrBL.Rows[0]["kyhieu_ct"].ToString();
                    hdr.so_ct = hdrBL.Rows[0]["so_ct"].ToString();
                    hdr.ngay_ct = long.Parse(hdrBL.Rows[0]["ngay_ct"].ToString());
                    hdr.ma_nnthue = hdrBL.Rows[0]["ma_nnthue"].ToString();
                    hdr.ten_nnthue = hdrBL.Rows[0]["ten_nnthue"].ToString();
                    hdr.ma_cqthu = hdrBL.Rows[0]["ma_cqthu"].ToString();
                    hdr.ma_hq_cqt = hdrBL.Rows[0]["ma_cqthu"].ToString();
                    hdr.so_tk = hdrBL.Rows[0]["so_tk"].ToString();
                    hdr.ttien = long.Parse(hdrBL.Rows[0]["ttien"].ToString());
                    hdr.ttien_nt = long.Parse(hdrBL.Rows[0]["ttien_nt"].ToString());
                    hdr.trang_thai = hdrBL.Rows[0]["trang_thai"].ToString();
                    hdr.ly_do_huy = hdrBL.Rows[0]["ly_do_huy"].ToString();
                    hdr.ma_nt = hdrBL.Rows[0]["ma_nt"].ToString();
                    hdr.ten_nh_ph = strSender_Name;
                    if (hdrBL.Columns.Contains("ma_dv_dd"))
                        hdr.ma_dv_dd = hdrBL.Rows[0]["ma_dv_dd"].ToString();
                    if (hdrBL.Columns.Contains("ten_nh_b"))
                        hdr.ten_nh_th = hdrBL.Rows[0]["ten_nh_b"].ToString();

                    if (hdrBL.Columns.Contains("ty_gia"))
                        hdr.ty_gia = hdrBL.Rows[0]["ty_gia"].ToString();

                    if (hdrBL.Columns.Contains("ttien_nt"))
                        hdr.ttien_nt = long.Parse(hdrBL.Rows[0]["ttien_nt"].ToString());
                    if (hdrBL.Columns.Contains("ten_dv_dd"))
                        hdr.ten_dv_dd = hdrBL.Rows[0]["ten_dv_dd"].ToString();
                    if (hdrBL.Columns.Contains("ngay_hdon"))
                    { hdr.ngay_hd = "to_date('" + hdrBL.Rows[0]["ngay_hdon"].ToString() + "','dd/mm/yyyy')"; }
                    else { hdr.ngay_hd = "''"; }

                    if (hdrBL.Columns.Contains("hoa_don"))
                        hdr.so_hd = hdrBL.Rows[0]["hoa_don"].ToString();
                    if (!hdrBL.Rows[0]["ngay_tk"].ToString().Equals(""))
                        hdr.ngay_tk = "to_date('" + hdrBL.Rows[0]["ngay_tk"].ToString() + "','dd/mm/yyyy')";
                    else
                        hdr.ngay_tk = "''";
                    if (hdrBL.Columns.Contains("lh_xnk"))
                        hdr.lhxnk = hdrBL.Rows[0]["lh_xnk"].ToString();
                    if (hdrBL.Columns.Contains("ma_nv"))
                        hdr.ma_nv = hdrBL.Rows[0]["ma_nv"].ToString();
                    if (hdrBL.Columns.Contains("lhxnk"))
                        hdr.lhxnk = hdrBL.Rows[0]["lhxnk"].ToString();
                    if (hdrBL.Columns.Contains("vt_lhxnk"))
                        hdr.vt_lhxnk = hdrBL.Rows[0]["vt_lhxnk"].ToString();
                    if (hdrBL.Columns.Contains("ten_lhxnk"))
                        hdr.ten_lhxnk = hdrBL.Rows[0]["ten_lhxnk"].ToString();
                    if (hdrBL.Columns.Contains("tk_ns"))
                        hdr.tk_ns = hdrBL.Rows[0]["tk_ns"].ToString();

                    if (hdrBL.Columns.Contains("ten_tk_ns"))
                        hdr.ten_tk_ns = hdrBL.Rows[0]["ten_tk_ns"].ToString();

                    if (hdrBL.Columns.Contains("so_bl"))
                        hdr.so_bl = hdrBL.Rows[0]["so_bl"].ToString();
                    hdr.ngay_bl_batdau = "''";
                    if (hdrBL.Columns.Contains("ngay_batdau"))
                        if (!hdrBL.Rows[0]["ngay_batdau"].ToString().Equals(""))
                            hdr.ngay_bl_batdau = "to_date('" + hdrBL.Rows[0]["ngay_batdau"].ToString() + "','dd/mm/yyyy')";
                    hdr.ngay_bl_ketthuc = "''";
                    if (hdrBL.Columns.Contains("ngay_ketthuc"))
                        if (!hdrBL.Rows[0]["ngay_ketthuc"].ToString().Equals(""))

                            hdr.ngay_bl_ketthuc = "to_date('" + hdrBL.Rows[0]["ngay_ketthuc"].ToString() + "','dd/mm/yyyy')";

                    if (hdrBL.Columns.Contains("songay_bl"))
                        hdr.songay_bl = int.Parse(hdrBL.Rows[0]["songay_bl"].ToString());

                    if (hdrBL.Columns.Contains("kieu_bl"))
                        hdr.kieu_bl = hdrBL.Rows[0]["kieu_bl"].ToString();

                    if (hdrBL.Columns.Contains("dien_giai"))
                        hdr.dien_giai = hdrBL.Rows[0]["dien_giai"].ToString();
                    if (hdrBL.Columns.Contains("ma_hq_ph"))
                        hdr.ma_hq_ph = hdrBL.Rows[0]["ma_hq_ph"].ToString();
                    if (hdrBL.Columns.Contains("ten_hq_ph"))
                        hdr.ten_hq_ph = hdrBL.Rows[0]["ten_hq_ph"].ToString();
                    if (hdrBL.Columns.Contains("ten_hq"))
                        hdr.ten_hq = hdrBL.Rows[0]["ten_hq"].ToString();
                    if (hdrBL.Columns.Contains("ma_hq"))
                        hdr.ma_hq = hdrBL.Rows[0]["ma_hq"].ToString();

                    if (hdrBL.Columns.Contains("ma_loaitien"))
                        hdr.ma_loaitien = hdrBL.Rows[0]["ma_loaitien"].ToString();
                    if (hdrBL.Columns.Contains("ma_hq_cqt"))
                        hdr.ma_hq_cqt = hdrBL.Rows[0]["ma_hq_cqt"].ToString();
                    if (hdrBL.Columns.Contains("so_bt_hq"))
                        if (!hdrBL.Rows[0]["so_bt_hq"].ToString().Trim().Equals(""))
                            hdr.so_bt_hq = long.Parse(hdrBL.Rows[0]["so_bt_hq"].ToString());
                    if (hdrBL.Columns.Contains("ma_ntk"))
                        if (!hdrBL.Rows[0]["ma_ntk"].ToString().Trim().Equals(""))
                            hdr.ma_ntk = hdrBL.Rows[0]["ma_ntk"].ToString();

                    hdr.error_code_hq = pv_strErroCodeHQ;
                    hdr.transaction_type = pv_strTransactionType;
                    if (pv_strErroCodeHQ == "0")
                        hdr.accept_yn = "Y";
                    else
                        hdr.accept_yn = "N";

                    hdr.ten_kb = Globals.RemoveSign4VietnameseString(mdlCommon.Get_TenKhoBac(hdr.shkb));
                    hdr.ten_cqthu = hdr.ten_cqthu;
                    hdr.ngay_bc = "sysdate";
                    hdr.ngay_bn = "sysdate";
                    hdr.parent_transaction_id = "";
                    hdr.tk_ns_hq = "";
                    hdr.so_tn_ct = pv_strSo_TN_CT;
                    hdr.ngay_tn_ct = pv_strNgay_TN_CT;
                    if (hdrBL.Columns.Contains("ngay_baoco"))
                        hdr.ngay_bc = "to_date('" + hdrBL.Rows[0]["ngay_baoco"].ToString() + "','dd/mm/yyyy')";
                    if (hdrBL.Columns.Contains("ngay_baono"))
                        hdr.ngay_bn = "to_date('" + hdrBL.Rows[0]["ngay_baono"].ToString() + "','dd/mm/yyyy')";


                    if (hdrBL.Columns.Contains("ma_nh_a"))
                        hdr.ma_nh_ph = hdrBL.Rows[0]["ma_nh_a"].ToString();
                    if (hdrBL.Columns.Contains("ma_nh_b"))
                        hdr.ma_nh_th = hdrBL.Rows[0]["ma_nh_b"].ToString();

                    if (hdrBL.Columns.Contains("tk_co"))
                        hdr.tkkb_ct = hdrBL.Rows[0]["tk_co"].ToString();
                    hdr.so_tn_ct = pv_strSo_TN_CT;
                    hdr.ngay_tn_ct = pv_strNgay_TN_CT;
                    if (hdrBL.Columns.Contains("ngay_hdon"))
                        hdr.ngay_hd = "to_da;te('" + hdrBL.Rows[0]["ngay_hdon"].ToString() + "','dd/mm/yyyy')";
                    else
                        hdr.ngay_hd = "''";
                    if (hdrBL.Columns.Contains("hoa_don"))
                        hdr.so_hd = hdrBL.Rows[0]["hoa_don"].ToString();
                    if (hdrBL.Columns.Contains("ngay_vtd"))
                        hdr.ngay_vtd = "to_date('" + hdrBL.Rows[0]["ngay_vtd"].ToString() + "','dd/mm/yyyy')";
                    else
                        hdr.ngay_vtd = "''";

                    if (hdrBL.Columns.Contains("vt_don"))
                        hdr.vtd = hdrBL.Rows[0]["vt_don"].ToString();

                    if (hdrBL.Columns.Contains("ngay_vtd2"))
                        hdr.ngay_vtd2 = "to_date('" + hdrBL.Rows[0]["ngay_vtd2"].ToString() + "','dd/mm/yyyy')";
                    else
                        hdr.ngay_vtd2 = "''";

                    if (hdrBL.Columns.Contains("vt_don2"))
                        hdr.vtd2 = hdrBL.Rows[0]["vt_don2"].ToString();
                    hdr.ngay_vtd3 = "''";
                    if (hdrBL.Columns.Contains("ngay_vtd3"))
                        hdr.ngay_vtd3 = "to_date('" + hdrBL.Rows[0]["ngay_vtd3"].ToString() + "','dd/mm/yyyy')";

                    if (hdrBL.Columns.Contains("vt_don3"))
                        hdr.vtd3 = hdrBL.Rows[0]["vt_don3"].ToString();
                    hdr.ngay_vtd4 = "''";
                    if (hdrBL.Columns.Contains("ngay_vtd4"))
                        hdr.ngay_vtd4 = "to_date('" + hdrBL.Rows[0]["ngay_vtd4"].ToString() + "','dd/mm/yyyy')";
                    if (hdrBL.Columns.Contains("vt_don4"))
                        hdr.vtd4 = hdrBL.Rows[0]["vt_don4"].ToString();
                    hdr.ngay_vtd5 = "''";
                    if (hdrBL.Columns.Contains("ngay_vtd5"))
                        hdr.ngay_vtd5 = "to_date('" + hdrBL.Rows[0]["ngay_vtd5"].ToString() + "','dd/mm/yyyy')";
                    if (hdrBL.Columns.Contains("vt_don5"))
                        hdr.vtd5 = hdrBL.Rows[0]["vt_don5"].ToString();
                    if (hdrBL.Columns.Contains("DIENGIAI_HQ"))
                        hdr.dien_giai = hdrBL.Rows[0]["DIENGIAI_HQ"].ToString();
                    hdr.KENHGD = "HQ247";

                }

                if (dtlsBL.Rows.Count > 0)
                {
                    foreach (DataRow row in dtlsBL.Rows)
                    {
                        try
                        {
                            objDTL = new Business_HQ.infDoiChieuNHHQ_DTL();
                            objDTL.ma_cap = row["ma_cap"].ToString();
                            objDTL.ma_chuong = row["ma_chuong"].ToString();
                            objDTL.noi_dung = row["noi_dung"].ToString();
                            objDTL.ma_dp = row["ma_dp"].ToString();
                            objDTL.ky_thue = row["ky_thue"].ToString();
                            objDTL.sotien = long.Parse(row["sotien"].ToString());

                            if (dtlsBL.Columns.Contains("ma_quy"))
                                objDTL.ma_quy = row["ma_quy"].ToString();
                            if (dtlsBL.Columns.Contains("ma_nkt"))
                                objDTL.ma_nkt = row["ma_nkt"].ToString();

                            if (dtlsBL.Columns.Contains("ma_tmuc"))
                                objDTL.ma_ndkt = row["ma_tmuc"].ToString();
                            if (dtlsBL.Columns.Contains("ma_nkt_cha"))
                                objDTL.ma_nkt_cha = row["ma_nkt_cha"].ToString();
                            if (dtlsBL.Columns.Contains("ma_ndkt"))
                                objDTL.ma_ndkt = row["ma_ndkt"].ToString();
                            if (dtlsBL.Columns.Contains("ma_khoan"))
                                objDTL.ma_nkt = row["ma_khoan"].ToString();

                            if (dtlsBL.Columns.Contains("ma_ndkt_cha"))
                                objDTL.ma_ndkt_cha = row["ma_ndkt_cha"].ToString();

                            if (dtlsBL.Columns.Contains("ma_tlpc"))
                                if (!row["ma_tlpc"].ToString().Trim().Equals(""))
                                    objDTL.ma_tlpc = int.Parse(row["ma_tlpc"].ToString());

                            if (dtlsBL.Columns.Contains("ma_khtk"))
                                objDTL.ma_khtk = row["ma_khtk"].ToString();

                            if (dtlsBL.Columns.Contains("SoTien_NT"))
                                objDTL.sotien_nt = long.Parse(row["SoTien_NT"].ToString());
                            lstdtl.Add(objDTL);


                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.Message + "-" + ex.StackTrace);
                            throw ex;

                        }

                    }
                    dtls = lstdtl.ToArray();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
                throw ex;
            }

        }
        public void INSERT_KQDCThueToDB_NTDT304(CustomsV3.MSG.MSG304.MSG304 objTran)
        {
            OracleConnection conn = DataAccess.GetConnection();// new OracleConnection(VBOracleLib.DataAccess.strConnection);
            IDbTransaction transCT = null;
            string strTransactionType = objTran.Header.Message_Type;
            string strRequest_ID = objTran.Header.Request_ID;
            string strtran_ID = objTran.Header.Transaction_ID;
            try
            {
              //  conn.Open();
                transCT = conn.BeginTransaction();
                //Xoa du lieu dc cu
                DataAccess.ExecuteNonQuery("delete from TCS_HQ247_304_DCNHHQ_HDR where TRANSACTION_ID='" + strtran_ID + "' ", CommandType.Text, transCT);
                DataAccess.ExecuteNonQuery("delete from TCS_HQ247_304_DCNHHQ_DTL where TRANSACTION_ID='" + strtran_ID + "' ", CommandType.Text, transCT);


                string strID = DataAccess.ExecuteSQLScalar("SELECT TO_CHAR(SYSDATE,'RRRRMMDD')||LPAD(SEQ_TCS_HQ247_304.NEXTVAL,10,'0') FROM DUAL ");
                StringBuilder sbsql = null;
                sbsql = new StringBuilder();
                sbsql.Append("INSERT INTO TCS_HQ247_304_DCNHHQ_HDR (ID,  TRANSACTION_TYPE,");
                sbsql.Append("TRANSACTION_ID, NGAYLAP_CT, NGAYTRUYEN_CT, MA_DV,");
                sbsql.Append("MA_CHUONG, TEN_DV, MA_KB, TEN_KB, TKKB, MA_NTK,");
                sbsql.Append("MA_HQ_PH, MA_HQ_CQT, KYHIEU_CT, SO_CT, LOAI_CT,");
                sbsql.Append("NGAY_BN, NGAY_CT, MA_NT, TY_GIA, SOTIEN_TO, DIENGIAI,");
                sbsql.Append("MA_ST, SO_CMT, TEN_NNT, DIACHI, MA_NH_TH,");
                sbsql.Append("TAIKHOAN_TH)");
                sbsql.Append("  VALUES   ('" + strID + "', '857',");
                sbsql.Append("'" + strtran_ID + "',TO_DATE('" + objTran.Data.ThongTinChungTu.NgayLap_CT.Replace("T", " ") + "','RRRR-MM-DD HH24:MI:SS'),TO_DATE('" + objTran.Data.ThongTinChungTu.NgayTruyen_CT.Replace("T", " ") + "','RRRR-MM-DD HH24:MI:SS'),");
                sbsql.Append("'" + objTran.Data.ThongTinChungTu.Ma_DV + "','" + objTran.Data.ThongTinChungTu.Ma_Chuong + "',");
                sbsql.Append(":TEN_DV,");
                sbsql.Append("'" + objTran.Data.ThongTinChungTu.Ma_KB + "',");
                sbsql.Append(":TEN_KB,");
                sbsql.Append("'" + objTran.Data.ThongTinChungTu.TKKB + "','" + objTran.Data.ThongTinChungTu.Ma_NTK + "' ,'" + objTran.Data.ThongTinChungTu.Ma_HQ_PH + "',");
                sbsql.Append("'" + objTran.Data.ThongTinChungTu.Ma_HQ_CQT + "' ,'" + objTran.Data.ThongTinChungTu.KyHieu_CT + "' ,'" + objTran.Data.ThongTinChungTu.So_CT + "',");
                sbsql.Append("'" + objTran.Data.ThongTinChungTu.Loai_CT + "' ,TO_DATE('" + objTran.Data.ThongTinChungTu.Ngay_BN.Replace("T", " ") + "','RRRR-MM-DD HH24:MI:SS')");
                sbsql.Append(",TO_DATE('" + objTran.Data.ThongTinChungTu.Ngay_CT.Replace("T", " ") + "','RRRR-MM-DD HH24:MI:SS') ,'" + objTran.Data.ThongTinChungTu.Ma_NT + "',");
                sbsql.Append("'" + objTran.Data.ThongTinChungTu.Ty_Gia + "'," + objTran.Data.ThongTinChungTu.SoTien_TO + ", :DIENGIAI,");
                sbsql.Append("'" + objTran.Data.ThongTinGiaoDich.NguoiNopTien.Ma_ST + "','" + objTran.Data.ThongTinGiaoDich.NguoiNopTien.So_CMT + "',");
                sbsql.Append(":TEN_NNT,:DIACHI,'" + objTran.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ma_NH_TH + "',");
                sbsql.Append("'" + objTran.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH + "')");
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[5];
                p[0] = new OracleParameter();
                p[0].ParameterName = "TEN_DV";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = objTran.Data.ThongTinChungTu.Ten_DV;

                p[1] = new OracleParameter();
                p[1].ParameterName = "TEN_KB";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = objTran.Data.ThongTinChungTu.Ten_KB;

                p[2] = new OracleParameter();
                p[2].ParameterName = "DIENGIAI";
                p[2].DbType = DbType.String;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = objTran.Data.ThongTinChungTu.DienGiai;

                p[3] = new OracleParameter();
                p[3].ParameterName = "TEN_NNT";
                p[3].DbType = DbType.String;
                p[3].Direction = ParameterDirection.Input;
                p[3].Value = objTran.Data.ThongTinGiaoDich.NguoiNopTien.Ten_NNT;

                p[4] = new OracleParameter();
                p[4].ParameterName = "DIACHI";
                p[4].DbType = DbType.String;
                p[4].Direction = ParameterDirection.Input;
                p[4].Value = objTran.Data.ThongTinGiaoDich.NguoiNopTien.DiaChi;
                DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);
                //chay cho cac dong chi tiet
                foreach (CustomsV3.MSG.MSG304.GNT_CT objGNT_CT in objTran.Data.ThongTinChungTu.GNT_CT)
                    foreach (CustomsV3.MSG.MSG304.ToKhai_CT objToKhai_CT in objGNT_CT.ToKhai_CT)
                    {
                        string strSQL = "INSERT INTO TCS_HQ247_304_DCNHHQ_DTL(IDHDR, NGAYTRUYEN_CT, TRANSACTION_ID, ID_HS, TTBUTTOAN, MA_HQ, MA_LH,";
                        strSQL += "NAM_DK, SO_TK, MA_LT, MA_ST, NDKT, SOTIEN_NT,       SOTIEN_VND,NGAY_DK)";
                        strSQL += "VALUES('" + strID + "',TO_DATE('" + objTran.Data.ThongTinChungTu.NgayTruyen_CT.Replace("T", " ") + "','RRRR-MM-DD HH24:MI:SS'),";
                        strSQL += "'" + strtran_ID + "','" + objGNT_CT.IS_HS + "','" + objGNT_CT.TTButToan + "',";
                        strSQL += "'" + objGNT_CT.Ma_HQ + "','" + objGNT_CT.Ma_LH + "','" + objGNT_CT.Nam_DK + "','" + objGNT_CT.So_TK + "','" + objGNT_CT.Ma_LT + "'";
                        strSQL += ",'" + objToKhai_CT.Ma_ST + "','" + objToKhai_CT.NDKT + "'," + objToKhai_CT.SoTien_NT + "," + objToKhai_CT.SoTien_VND;
                        strSQL += ",TO_DATE('" + objGNT_CT.Ngay_DK.Replace("T", " ") + "','RRRR-MM-DD HH24:MI:SS'))";
                        DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, transCT);
                    }



                transCT.Commit();


            }
            catch (Exception ex)
            {
                transCT.Rollback();
                StringBuilder sbErrMsg = default(StringBuilder);
                sbErrMsg = new StringBuilder();
                sbErrMsg.Append("Lỗi trong quá trình INSERT đối chiếu dữ liệu giao dịch thành công : TRAN ID - " + objTran.Header.Transaction_ID);

                sbErrMsg.Append("insert vao CSDL\n");
                sbErrMsg.Append(ex.Message);
                sbErrMsg.Append("\n");
                sbErrMsg.Append(ex.StackTrace);
                log.Error(sbErrMsg.ToString());

                log.Error(ex.Message + "-" + ex.StackTrace);

                throw ex;

            }
            finally
            {
                if ((transCT != null))
                {
                    transCT.Dispose();
                }
                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                   
                    conn.Close();
                    conn.Dispose();
                }
            }

        }
        public string ProcessData(string str304ID)
        {
            try
            {
                string strDienGiai = "";
                decimal checkProcess = 0;
                string strTrangThai = "";
                string strLOAIMSG = "";
                string strCoreOn = "1";
                string lydo = "";
                //lay message len
                string strSQL = "SELECT * FROM TCS_HQ247_304 WHERE ID='" + str304ID + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                string strCodeFee = "____";
                decimal decVAT = 0;
                decimal decFeeAmt = 0;
                string strso_ct = "";
                string strso_ctNH = "";
                decimal decSodu = 0;
                string StrResult = "Xử lý chứng từ thành công";
                clsCoreBank objClsCoreBankESB = new clsCoreBank();
                clsSongPhuong objClsSongPhuong = new clsSongPhuong();
                Business_HQ.NewChungTu.infChungTuHDR objHdrHq = new Business_HQ.NewChungTu.infChungTuHDR();
                List<Business_HQ.NewChungTu.infChungTuDTL> listDTL = new List<Business_HQ.NewChungTu.infChungTuDTL>();

                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            strTrangThai = ds.Tables[0].Rows[0]["TRANGTHAI"].ToString();
                            strLOAIMSG = ds.Tables[0].Rows[0]["LOAIMSG"].ToString();
                            lydo = ds.Tables[0].Rows[0]["LYDO"].ToString();
                            strso_ct = ds.Tables[0].Rows[0]["SO_CTU"].ToString();
                            //strDienGiai = ds.Tables[0].Rows[0]["DIEN_GIAI"].ToString();
                        }
                if (strLOAIMSG.Equals(""))
                {
                    return "Khong ton tai MSG id[" + str304ID + "]";
                }
                if (strLOAIMSG.Equals("304"))
                {
                    //map vao object 304
                    MSG304 obj = new MSG304();
                    obj.SETDATA(ds.Tables[0].Rows[0]["MSG_CONTENT"].ToString());
                    //loi sai cau truc data
                    if (Check_CauTtruc_MSG(obj) > 0)
                    {
                        //tra loi 213 tu choi giao dich"Chưa đến ngày hiệu lực trên hợp đồng ủy quyền trích nợ nên chưa thanh toán được";
                        strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                        lydo = "Sai cấu trúc MSG 304";
                        daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(str304ID, lydo, AccountHQ247);
                    }
                    INSERT_KQDCThueToDB_NTDT304(obj);
                    //neu trang thai la moi nhan
                    //check xem co dung ngan hang khong
                    if (!strSender_Code.Equals(obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ma_NH_TH))
                    {
                        //chuyen trang thai thanh tu choi
                        log.Info("1.strSender_Code: " + strSender_Code + "-" + obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ma_NH_TH);
                        strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                        daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(str304ID, mdlCommon.Get_ErrorDesciption(mdlCommon.TCS_HQ247_HQ_SAI_NH_TH.ToString()), AccountHQ247);
                    }
                    if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_304_MOINHAN))
                    {
                        //neu da co so chung tu etax va so ct ngan hang thi khong check nua bo qua luon

                        //check ma ngan hang xem co dung khong
                        checkProcess = daCTUHQ304.CheckValidTAIKHOAN_TH(obj.Data.ThongTinGiaoDich.NguoiNopTien.Ma_ST, obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH, "");
                        if (checkProcess > 0)
                        {
                            //tra loi 213 tu choi giao dich
                           log.Info("1.CheckValidTAIKHOAN_TH: " + checkProcess);
                            lydo = mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                            decimal xs = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(str304ID, lydo, AccountHQ247);
                            if (xs == 0)
                                strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                            else return "Loi " + mdlCommon.Get_ErrorDesciption(xs.ToString());
                        }
                        else
                        {
                            //check xem co du tien thuc hien thanh toan khong va cac phan phi
                            //Nhamlt 20180123 Nếu Ngày truyền CT trong thông điệp GNT nhỏ hơn Ngày hiệu lực thì 
                            //hệ thống từ chối chứng từ và điền vào thẻ <Noidung_XL> trong thông điệp từ chối chứng từ gửi sang TCHQ với nội dung
                            //”Chưa đến ngày hiệu lực trên hợp đồng ủy quyền trích nợ nên chưa thanh toán được”.
                            string strBranch_tk = "";
                            string strNgayTruyenCT = obj.Data.ThongTinChungTu.NgayTruyen_CT.ToString().Replace("T", " ");
                            strNgayTruyenCT = DateTime.ParseExact(strNgayTruyenCT, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
                            string ma_dv = obj.Data.ThongTinChungTu.Ma_DV;
                            string so_tk = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH;
                            checkProcess = daCTUHQ304.CheckValidNgayTruyenCT(strNgayTruyenCT, ma_dv, so_tk);
                            if (checkProcess == mdlCommon.TCS_HQ247_OK)
                            {
                                log.Error("1.CheckValidNgayTruyenCT: " + checkProcess + " strNgayTruyenCT: " + strNgayTruyenCT);
                                //tra loi 213 tu choi giao dich"Chưa đến ngày hiệu lực trên hợp đồng ủy quyền trích nợ nên chưa thanh toán được";
                                lydo = "Chua den ngay hieu luc tren hop dong uy quyen trich no nen chua thanh toan duoc";
                                decimal xs = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(str304ID, lydo, AccountHQ247);
                                if (xs == 0)
                                    strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                                else return "Loi " + mdlCommon.Get_ErrorDesciption(xs.ToString());
                            }
                            else
                            {
                                if (strCoreOn.Equals("1"))
                                {

                                    string strSodu = "0";
                                    string strErrorCode = "";
                                    string strErrorMSG = "";

                                    strErrorMSG = objClsCoreBankESB.GetTK_KH_NH(obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH, ref strErrorCode, ref strSodu, ref strBranch_tk);
                                    if (strSodu.Length > 0)
                                    {
                                        try
                                        {
                                            decSodu = decimal.Parse(strSodu);
                                        }
                                        catch (Exception ex)
                                        { decSodu = 0; }
                                    }

                                }
                                else
                                {
                                    decSodu = 9999999999;
                                }
                                //neu chua tạo chung tu thi tao chung tư
                                if (strso_ct.Equals(""))
                                {
                                    //neu khong du so du thi khoi tinh cho thanh tu choi thanh toan vi khong du tien voi cacs ngan hang realtime

                                    if (decSodu < decimal.Parse(obj.Data.ThongTinChungTu.SoTien_TO) + decFeeAmt + decVAT)
                                    {
                                        daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(str304ID, "tu choi thanh toan vi khong du so du", AccountHQ247);
                                        strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                                        StrResult = "tu choi thanh toan ct[" + str304ID + "] vi khong du so du";
                                        //                                    return "tu choi thanh toan ct[" + str304ID + "] vi khong du so du";
                                    }
                                    //if (!strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_304_MOINHAN))
                                    if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_304_MOINHAN))
                                    {
                                        //thuc hien thanh toan insert giao dichj chung tu va cat tien

                                        checkProcess = SetChungtuHQ(obj, ref objHdrHq, ref listDTL);
                                        if (checkProcess > 0)
                                        {
                                           log.Info("1.SetChungtuHQ: " + checkProcess);
                                            lydo = mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                                            decimal xs = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(str304ID, lydo, AccountHQ247);
                                            if (xs == 0)
                                                strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                                            else return "Loi " + mdlCommon.Get_ErrorDesciption(xs.ToString());
                                        }
                                        else
                                        {
                                            objHdrHq.Ma_CN = ds.Tables[0].Rows[0]["MA_CN"].ToString();
                                            checkProcess = daCTUHQ304.Insert301(objHdrHq, listDTL, str304ID, AccountHQ247);

                                            if (checkProcess > 0)
                                            {
                                                log.Info("1.Insert301: " + checkProcess);
                                                lydo = mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                                                decimal xs = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(str304ID, lydo, AccountHQ247);
                                                if (xs == 0)
                                                    strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                                                else return "Loi " + mdlCommon.Get_ErrorDesciption(xs.ToString());
                                            }
                                            strso_ct = objHdrHq.So_CT;

                                        }
                                        //doan nay lien quan dens viec thanh toan ngan hang
                                        //if (!strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_304_MOINHAN))
                                        if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_304_MOINHAN))
                                        {
                                            strso_ctNH = objHdrHq.So_CT_NH;//Them dong nay de xem da hach toan chua
                                            //thanh toan tien neu chua co so chung tu ngan hang thi thanh toan
                                            if (strso_ctNH.Equals(""))
                                            {

                                                if (strCoreOn.Equals("1"))
                                                {
                                                    //--------------------------------------------------
                                                    string str_PayMent = "";
                                                    string rm_ref_no = "";
                                                    string req_Internal_rmk = "";
                                                    string v_strErrorCode = "";
                                                    string str_AccountNo = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH;
                                                    string strTKCo = obj.Data.ThongTinChungTu.TKKB;
                                                    string strTongTienTT = obj.Data.ThongTinChungTu.SoTien_TO;
                                                    string strMa_NguyenTe = obj.Data.ThongTinChungTu.Ma_NT;
                                                    string v_strNgay_KB = objHdrHq.Ngay_KB.ToString();
                                                    string str_MaTinh = objHdrHq.Ma_Tinh;
                                                    string str_TenTinh = "";
                                                    string ngay_NT = "";
                                                    ngay_NT = objClsCoreBankESB.Get_VALUE_DATE();
                                                    ngay_NT = Business_HQ.Common.mdlCommon.ConvertNumbertoString_HQ(ngay_NT);
                                                    if (objHdrHq.QUAN_HUYEN_NNTIEN != null)
                                                    {
                                                        str_TenTinh = Globals.RemoveSign4VietnameseString(objHdrHq.QUAN_HUYEN_NNTIEN);
                                                    }
                                                    string strTenCQT = "";
                                                    if (objHdrHq.Ten_cqthu != null)
                                                    {
                                                        strTenCQT = Globals.RemoveSign4VietnameseString(objHdrHq.Ten_cqthu);
                                                    }
                                                    string strTen_NH_B = "";
                                                    if (objHdrHq.Ten_NH_B != null)
                                                    {
                                                        strTen_NH_B = Globals.RemoveSign4VietnameseString(objHdrHq.Ten_NH_B);
                                                    }
                                                    string strMa_NH_B = objHdrHq.MA_NH_B;
                                                    string v_strSo_CT = objHdrHq.So_CT;
                                                    string v_strMa_NV = Ma_AccountHQ247;//Ma nhan vien nay la ma nhan vien cua nguoi thuc hien, truong hop nay la ma_nv trong config cua app goi den
                                                    string v_strMa_NV_CT = objHdrHq.Ma_NV.ToString();//Ma nhan vien nay la ma nhan vien trong hdr cua chung tu,
                                                    //str_PayMent = objClsCoreBank.PaymentTQ(str_AccountNo, strTKCo, strTongTienTT, strMa_NguyenTe, str_Desc, v_strNgay_KB, strTenCQT,
                                                    //str_MaTinh, strTenCQT, strTenCQT, strTen_NH_B, strTen_NH_B, str_TenTinh,
                                                    // strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B.ToString().Substring(5, 3), strMa_NH_B.ToString().Substring(0, 2), "TAX", v_strSo_CT, strMa_NH_B, v_strMa_NV_CT, v_strMa_NV, "ETAX_HQ", req_Internal_rmk, ref  v_strErrorCode, ref rm_ref_no);
                                                    ////--------------------------------------------------

                                                    //bỏ dấu trường remark trước khi hạch toan
                                                    //str_Desc = Globals.RemoveSign4VietnameseString(str_Desc);

                                                    //Kiểm tra có đi kho bạc SHB hay không
                                                    string strResult = objClsSongPhuong.ValiadSHB(objHdrHq.SHKB);
                                                    string[] arrResult;
                                                    string strNHB = "";
                                                    string strACC_NHB = "";
                                                    string strMA_CN_SP = "";
                                                    if (strResult.Length > 8)
                                                    {
                                                        arrResult = strResult.Split(';');
                                                        strNHB = arrResult[0].ToString();
                                                        strACC_NHB = arrResult[1].ToString();
                                                        strMA_CN_SP = arrResult[2].ToString();
                                                    }
                                                    bool checkThuSHB = false;
                                                    if (strNHB.Length == 8)
                                                    {
                                                        checkThuSHB = true;
                                                        string ngay_GD_SP = DateTime.Now.ToString("yyyyMMdd");

                                                        checkProcess = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_CHO_CORE_RESPONSE(str304ID);
                                                        if (checkProcess > 0)
                                                        {
                                                            return "Loi khi cap nhat trang thai chung tu cho phan hoi core bank";
                                                        }

                                                        //cap nhat TTSP la 1
                                                        strSQL = "UPDATE tcs_hq247_304 SET TT_SP='1' WHERE so_ctu='" + v_strSo_CT + "' ";
                                                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                                        //Gọi hàm thanh toán mới vào kho bạc SHB
                                                        //20180514_Lay dien giai
                                                        strDienGiai = BuildRemarkForPayment(objHdrHq, listDTL, ngay_NT);
                                                        objClsCoreBankESB.PaymentSP("TAX", ngay_GD_SP, v_strSo_CT, "110000", strBranch_tk, str_AccountNo, strTongTienTT, strMa_NguyenTe, "", strDienGiai,
                                                                 v_strMa_NV_CT, v_strMa_NV, "ETAX_HQ247", strTKCo, strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B, strACC_NHB,strMA_CN_SP, ref  v_strErrorCode, ref rm_ref_no);

                                                    }
                                                    else
                                                    {
                                                        //Đi citad theo hàm thanh toán cũ
                                                        if (Core_version.Equals("2"))
                                                        {
                                                            //Core V2.0 20180330--------------

                                                            string EcomStr = BuildEcomForPayment_V20(objHdrHq, listDTL);
                                                            strDienGiai = BuildRemarkForPayment_V20(objHdrHq, listDTL);
                                                            string str_TenMST = V_Substring(objHdrHq.Ten_NNThue, 200);
                                                            string str_MST = V_Substring(objHdrHq.Ma_NNThue, 14);
                                                            string str_MaLThue = V_Substring(objHdrHq.Ma_LThue, 2);
                                                            //Ngay nop thue dinh dang ddMMyyyy
                                                            string str_NNT = DateTime.Now.ToString("yyyyMMdd");

                                                            string str_AddNNT = V_Substring(objHdrHq.DC_NNThue, 200);
                                                            string strMaCQT = V_Substring(objHdrHq.Ma_CQThu, 7);
                                                            strTenCQT = V_Substring(strTenCQT, 200);
                                                            string strMaDBHC = "";
                                                            string strTenDBHC = "";
                                                            req_Internal_rmk = objHdrHq.Ma_NNThue + "-" + Globals.RemoveSign4VietnameseString(objHdrHq.Ten_NNThue);

                                                            checkProcess = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_CHO_CORE_RESPONSE(str304ID);
                                                            if (checkProcess > 0)
                                                            {
                                                                return "Loi khi cap nhat trang thai chung tu cho phan hoi core bank";
                                                            }

                                                            str_PayMent = objClsCoreBankESB.PaymentTQ_V20(str_AccountNo, strTKCo, strTongTienTT, strMa_NguyenTe, strDienGiai, v_strNgay_KB, strTenCQT,
                                                                str_MaTinh, strTenCQT, strTenCQT, strTen_NH_B, strTen_NH_B, str_TenTinh,
                                                                strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B.ToString().Substring(5, 3), strMa_NH_B.ToString().Substring(0, 2), "TAX", v_strSo_CT, strMa_NH_B, v_strMa_NV_CT, v_strMa_NV, "ETAX_HQ247", req_Internal_rmk,
                                                                str_NNT, str_MaLThue, str_MST, strMaCQT, strTenCQT, strMaDBHC, strTenDBHC, str_TenMST, str_AddNNT, EcomStr,
                                                                ref  v_strErrorCode, ref rm_ref_no);
                                                            //Core V2.0 20180330----------------

                                                        }
                                                        else
                                                        {
                                                            checkProcess = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_CHO_CORE_RESPONSE(str304ID);
                                                            if (checkProcess > 0)
                                                            {
                                                                return "Loi khi cap nhat trang thai chung tu cho phan hoi core bank";
                                                            }
                                                            //20180514_Lay dien giai
                                                            strDienGiai = BuildRemarkForPayment(objHdrHq, listDTL, ngay_NT);
                                                            req_Internal_rmk = objHdrHq.Ma_NNThue + "-" + Globals.RemoveSign4VietnameseString(objHdrHq.Ten_NNThue);
                                                            str_PayMent = objClsCoreBankESB.PaymentTQ(str_AccountNo, strTKCo, strTongTienTT, strMa_NguyenTe, strDienGiai, v_strNgay_KB, strTenCQT,
                                                            str_MaTinh, strTenCQT, strTenCQT, strTen_NH_B, strTen_NH_B, str_TenTinh,
                                                             strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B.ToString().Substring(5, 3), strMa_NH_B.ToString().Substring(0, 2), "TAX", v_strSo_CT, strMa_NH_B, v_strMa_NV_CT, v_strMa_NV, "ETAX_HQ247", req_Internal_rmk, ref  v_strErrorCode, ref rm_ref_no);
                                                        }
                                                    }


                                                    if (v_strErrorCode != null && v_strErrorCode.Equals("00000"))
                                                    {

                                                        //-----Cap nhat lai so chung tu core
                                                        checkProcess = daCTUHQ304.Update304_HachToan(str304ID, rm_ref_no, strDienGiai, AccountHQ247);
                                                        if (checkProcess > 0)
                                                        {
                                                            return "Loi khi cap nhat so chung tu core" + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                                                        }
                                                        //maker213 chap nhan
                                                        checkProcess = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213(str304ID, "Chap nhan thanh toan", AccountHQ247);
                                                        if (checkProcess > 0)
                                                        {
                                                            return "Loi " + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                                                        }
                                                        //gan tran thai = da maker 213
                                                        strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213;

                                                        //Core trả ra thành công thì gửi đi song phương
                                                        if (checkThuSHB == true)
                                                        {
                                                            //-----Cap nhat lai thong tin vao hdr hq247,ngay_kh_nh
                                                            //daCTUHQ304.UpdateTTSP(strso_ct, objHdrHq.MA_NH_TT, objHdrHq.TEN_NH_TT, strMa_NH_B, strTen_NH_B, rm_ref_no, strDienGiai);
                                                            //objClsSongPhuong.SendChungTuSP(str304ID, "HQ247");
                                                            objClsSongPhuong.SendChungTuSP(strso_ct, "HQ247");
                                                        }
                                                        else
                                                        {
                                                            Business.CitadV25 citad25 = new Business.CitadV25();
                                                            string core_date = objClsCoreBankESB.Get_VALUE_DATE();
                                                            string content_ex = citad25.CreateContentEx(v_strSo_CT, "HQDT", core_date);
                                                            content_ex = Business.Common.mdlCommon.EncodeBase64_ContentEx(content_ex);

                                                            objClsCoreBankESB.SendContentEx(v_strSo_CT, rm_ref_no, "HQDT", content_ex, ref v_strErrorCode);
                                                            if (v_strErrorCode.Equals("00000"))
                                                            {
                                                                //cap nhap send core thanh cong: 1 la thanh cong, 0 la chua thanh cong
                                                                VBOracleLib.DataAccess.ExecuteNonQuery("update tcs_hq247_304 set SEND_CONTENT_EX = '1' where so_ctu='" + v_strSo_CT + "'", CommandType.Text);
                                                            }
                                                        
                                                        }
                                                    }
                                                    else if (v_strErrorCode != null && v_strErrorCode.Equals("005"))
                                                    {
                                                        //Khong hach toan duoc
                                                        lydo = "Loi xay ra khi hach toan chung tu: Tài khoản không đủ để hạch toán";
                                                        decimal xs = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(str304ID, lydo, AccountHQ247);
                                                        if (xs == 0)
                                                            strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                                                        else return "Loi " + mdlCommon.Get_ErrorDesciption(xs.ToString());
                                                        // Loi hach toan
                                                    }      
                                                    else if ((v_strErrorCode != null && v_strErrorCode.Equals("TO999")))
                                                    {
                                                        lydo = "Loi: Corebank timeout";
                                                        return lydo;
                                                    }
                                                    else
                                                    {
                                                        //Khong hach toan duoc
                                                        lydo = "Loi xay ra khi hach toan chung tu:" + mdlCommon.Get_ErrorDesciptionCoreBank(v_strErrorCode);
                                                        decimal xs = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(str304ID, lydo, AccountHQ247);
                                                        if (xs == 0)
                                                            strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                                                        else return "Loi " + mdlCommon.Get_ErrorDesciption(xs.ToString());
                                                        // Loi hach toan

                                                    }
                                                    //--------------------------------------------------
                                                }
                                            }
                                            //maker213 chap nhan
                                            //checkProcess = daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213(str304ID, "Chap nhan thanh toan", AccountHQ247);
                                            //if (checkProcess > 0)
                                            //{
                                            //    return "Loi " + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                                            //}
                                            ////gan tran thai = da maker 213
                                            //strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213;
                                        }
                                    }
                                }


                            }

                        }

                    }

                    //neu trang thai = maker 213 thi em send 213
                    if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213))
                    {
                        checkProcess = Business_HQ.HQ247.daCTUHQ304.checkTrangthaiMSG304(str304ID, Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213);
                        if (checkProcess > 0)
                        {
                            return "Loi " + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                        }
                        //send 213 di
                        checkProcess = CustomsServiceV3.ProcessMSG.sendMSG213("304", "1", "Chap nhan thanh toan", str304ID, AccountHQ247);
                        if (checkProcess > 0)
                        {
                            return "Loi " + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                        }
                        //gan trang thai = da gui 213
                        strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_CHEKER213;
                    }
                    //neu trang thai la da gui 213 thi maker 301
                    if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_304_CHEKER213) || strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR))
                    {
                        String strErrorNum = "0";
                        String strErrorMes = "";
                        String v_strTransaction_id = "";
                        String pv_so_tn_ct = "";
                        String pv_ngay_tn_ct = "";
                        CustomsServiceV3.ProcessMSG.guiCTThueHQ247(obj.Data.ThongTinChungTu.Ma_DV, strso_ct, ref strErrorNum, ref strErrorMes, ref v_strTransaction_id, ref pv_so_tn_ct, ref pv_ngay_tn_ct);
                        if (!strErrorNum.Equals("0"))
                        {
                            Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR(str304ID, AccountHQ247);
                            return "Da hac toan ct[" + str304ID + "] Gui HQ loi";
                        }
                        decimal decResult = Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_OK(str304ID, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct, AccountHQ247);
                        if (pv_so_tn_ct.Length > 0)
                        {
                            Business_HQ.infDoiChieuNHHQ_HDR hdr = new Business_HQ.infDoiChieuNHHQ_HDR();
                            Business_HQ.infDoiChieuNHHQ_DTL[] dtls = null;
                            //insert doi chieu
                            SetObjectToDoiChieuNHHQ(obj.Data.ThongTinChungTu.Ma_KB, strso_ct, "", Ma_AccountHQ247, "CTU", ref hdr, ref dtls, obj.Data.ThongTinChungTu.Ma_KB, strErrorNum, mdlCommon.TransactionHQ_Type.M47.ToString(), "", pv_so_tn_ct, pv_ngay_tn_ct);
                            hdr.transaction_id = v_strTransaction_id;


                            //'sonmt override ngay_bn, ngay_bc
                            //hdr.ngay_bn = "to_date('" & ngay_bao_no.ToString("dd/MM/yyyy HH:mm:ss") & "', 'dd/MM/yyyy HH24:MI:SS')"
                            //hdr.ngay_bc = "to_date('" & ngay_bao_co.ToString("dd/MM/yyyy HH:mm:ss") & "', 'dd/MM/yyyy HH24:MI:SS')"
                            Business_HQ.ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, "1", strErrorMes);
                        }
                    }
                    //neu la trang thai maker213 tu choi
                    if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI))
                    {
                        //gui HQ tu choi
                        Decimal decResult = Business_HQ.HQ247.daCTUHQ304.checkTrangthaiMSG304(str304ID, Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER213_TUCHOI);
                        if (decResult == 0)
                            decResult = CustomsServiceV3.ProcessMSG.sendMSG213("304", "2", lydo, str304ID, AccountHQ247);
                    }


                }
                //tinh phi giao dich
                //check co du so du khong
                //gui tu choi giao dich

                //thuc hien thanh toan
                return StrResult;
            }
            catch (Exception ex)
            {
                return "Xử lý chứng từ lỗi: " + ex.Message;
            }
            return "";
        }
        public decimal SetChungtuHQ(MSG304 obj304, ref Business_HQ.NewChungTu.infChungTuHDR objHdrHq, ref List<Business_HQ.NewChungTu.infChungTuDTL> listDTL)
        {
            try
            {
                clsSongPhuong objClsSongPhuong = new clsSongPhuong();
                objHdrHq = new Business_HQ.NewChungTu.infChungTuHDR();
                listDTL = new List<Business_HQ.NewChungTu.infChungTuDTL>();
                objHdrHq.SHKB = obj304.Data.ThongTinChungTu.Ma_KB;
                objHdrHq.Ma_Tinh = "";
                objHdrHq.Ma_Huyen = "";
                objHdrHq.Ma_Xa = "";
                objHdrHq.TK_Co = obj304.Data.ThongTinChungTu.TKKB;
                objHdrHq.Ma_LThue = "04";
                objHdrHq.Ma_NNTien = obj304.Data.ThongTinGiaoDich.NguoiNopTien.Ma_ST;
                objHdrHq.Ten_NNTien = obj304.Data.ThongTinGiaoDich.NguoiNopTien.Ten_NNT;
                objHdrHq.DC_NNTien = obj304.Data.ThongTinGiaoDich.NguoiNopTien.DiaChi;
                objHdrHq.Ten_NNThue = obj304.Data.ThongTinChungTu.Ten_DV;
                objHdrHq.Ma_NNThue = obj304.Data.ThongTinChungTu.Ma_DV;

                objHdrHq.Ma_Huyen_NNThue = "";
                objHdrHq.Ma_Tinh_NNThue = "";
                objHdrHq.Ngay_CT = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                objHdrHq.Ngay_HT = DateTime.Now.ToString("dd/MM/yyyy");
                objHdrHq.Ma_CQThu = obj304.Data.ThongTinChungTu.Ma_HQ_CQT;
                objHdrHq.PT_TT = "01";
                objHdrHq.Ma_NT = obj304.Data.ThongTinChungTu.Ma_NT;
                objHdrHq.Ty_Gia = double.Parse(obj304.Data.ThongTinChungTu.Ty_Gia);
                objHdrHq.TTien = double.Parse(obj304.Data.ThongTinChungTu.SoTien_TO);
                objHdrHq.TTien_NT = double.Parse(obj304.Data.ThongTinChungTu.SoTien_TO);
                objHdrHq.TT_TThu = double.Parse(obj304.Data.ThongTinChungTu.SoTien_TO);
                objHdrHq.TK_KH_NH = obj304.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH;
                objHdrHq.TEN_KH_NH = obj304.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH;
                objHdrHq.TK_GL_NH = "";
                objHdrHq.MA_NH_A = obj304.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ma_NH_TH;

                string strResult = objClsSongPhuong.ValiadSHB(objHdrHq.SHKB);
                string[] arrResult;
                string strNHB = "";
                string strACC_NHB = "";
                if (strResult.Length > 8)
                {
                    arrResult = strResult.Split(';');
                    strNHB = arrResult[0].ToString();
                    strACC_NHB = arrResult[1].ToString();
                }
                //phan nay danh cho ngan hang truc tiep gian tiep
                DataSet dsx = null;

                if (strNHB.Length == 8)
                {
                    dsx = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiepKB_SHB(objHdrHq.SHKB, strNHB);
                }
                else
                {
                    dsx = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiep(obj304.Data.ThongTinChungTu.Ma_KB);
                }

                if (dsx != null)
                    if (dsx.Tables.Count > 0)
                        if (dsx.Tables[0].Rows.Count > 0)
                        {
                            objHdrHq.MA_NH_TT = dsx.Tables[0].Rows[0]["MA_TRUCTIEP"].ToString();
                            objHdrHq.TEN_NH_TT = dsx.Tables[0].Rows[0]["TEN_TRUCTIEP"].ToString();
                            objHdrHq.MA_NH_B = dsx.Tables[0].Rows[0]["MA_GIANTIEP"].ToString();
                            objHdrHq.Ten_NH_B = dsx.Tables[0].Rows[0]["TEN_GIANTIEP"].ToString();
                        }
                if (objHdrHq.MA_NH_TT == null || objHdrHq.MA_NH_TT.Length == 0)
                    return Business_HQ.Common.mdlCommon.TCS_HQ247_HQ_NGANHANGTT_GIANTIEP_LOI;
                objHdrHq.Trang_Thai = "05";
                //'doan nay lien quan den tinh phi
                //'chua thuc hien bao h lam thuc thi tinh
                objHdrHq.PT_TINHPHI = "O";
                objHdrHq.PHI_GD = "0";
                objHdrHq.PHI_VAT = "0";
                objHdrHq.PHI_GD2 = "0";
                objHdrHq.PHI_VAT2 = "0";
                objHdrHq.TT_CTHUE = 0;

                objHdrHq.LOAI_TT = "";// '    //Chuyen xuong HDR
                objHdrHq.MA_NTK = obj304.Data.ThongTinChungTu.Ma_NTK;
                objHdrHq.MA_HQ = obj304.Data.ThongTinChungTu.Ma_HQ_PH;
                objHdrHq.TEN_HQ = Business_HQ.HQ247.daCTUHQ304.getTenHQ_PH(obj304.Data.ThongTinChungTu.Ma_HQ_PH);
                objHdrHq.Ma_hq_ph = obj304.Data.ThongTinChungTu.Ma_HQ_PH;
                objHdrHq.TEN_HQ_PH = Business_HQ.HQ247.daCTUHQ304.getTenHQ_PH(obj304.Data.ThongTinChungTu.Ma_HQ_PH);
                objHdrHq.Kenh_CT = "ETAX";
                objHdrHq.SAN_PHAM = "XXX";// '$('#txtSanPham').val();
                objHdrHq.TT_CITAD = "0";
                objHdrHq.SO_CMND = obj304.Data.ThongTinGiaoDich.NguoiNopTien.So_CMT;
                objHdrHq.SO_FONE = "";
                objHdrHq.LOAI_CTU = "T";
                objHdrHq.REMARKS = "";
                objHdrHq.Ten_cqthu = Business_HQ.HQ247.daCTUHQ304.getTenHQ_CQT(obj304.Data.ThongTinChungTu.Ma_HQ_CQT, obj304.Data.ThongTinChungTu.Ma_HQ_PH);
                objHdrHq.So_TK = "";
                objHdrHq.So_CT_NH = "";
                objHdrHq.Ghi_chu = obj304.Data.ThongTinChungTu.Ten_KB;// obj304.Data.ThongTinChungTu.DienGiai;
                objHdrHq.MA_CUC = "";
                objHdrHq.TEN_CUC = "";
                objHdrHq.REMARKS = obj304.Data.ThongTinChungTu.DienGiai;
                objHdrHq.DIENGIAI_HQ = obj304.Data.ThongTinChungTu.DienGiai;
                objHdrHq.Ten_kb = obj304.Data.ThongTinChungTu.Ten_KB;
                objHdrHq.Ma_NV = int.Parse(Ma_AccountHQ247);
                objHdrHq.KyHieu_CT = Business_HQ.HaiQuan.HaiQuanController.GetKyHieuChungTu(objHdrHq.SHKB, objHdrHq.Ma_NV.ToString(), "01");
                objHdrHq.Ngay_KB = int.Parse(Business_HQ.HaiQuan.HaiQuanController.GetNgayKhoBac(objHdrHq.SHKB).ToString());
                objHdrHq.NGAY_BAONO = DateTime.Now;
                
                string strTT_CUTOFFTIME = Business_HQ.CTuCommon.GetValue_THAMSOHT("TT_CUTOFFTIME");
                DateTime ngay_tiep_theo = DateTime.Now.AddDays(1);
                while (true)
                {
                    DataTable dtNgayNghi = DataAccess.ExecuteReturnDataSet("SELECT ngay_nghi FROM tcs_dm_ngaynghi WHERE ngay_nghi = to_date('" + ngay_tiep_theo.ToString("dd/MM/yyyy") + "', 'dd/MM/yyyy')", CommandType.Text).Tables[0];
                    if (dtNgayNghi.Rows.Count > 0)
                        ngay_tiep_theo = ngay_tiep_theo.AddDays(1);
                    else
                        break;
                }
                objHdrHq.NGAY_BAOCO = ngay_tiep_theo;
                objHdrHq.Ma_DThu = obj304.Data.ThongTinChungTu.Ma_KB;
                String sSo_BT = String.Empty;
                String sNam_KB = "";
                String sThang_KB = "";
                sNam_KB = objHdrHq.Ngay_KB.ToString().Substring(2, 2);
                sThang_KB = objHdrHq.Ngay_KB.ToString().Substring(4, 2);
                objHdrHq.So_CT = sNam_KB + sThang_KB + Business_HQ.CTuCommon.Get_SoCT();
                foreach (CustomsV3.MSG.MSG304.GNT_CT objGNT_CT in obj304.Data.ThongTinChungTu.GNT_CT)
                    foreach (CustomsV3.MSG.MSG304.ToKhai_CT objToKhai_CT in objGNT_CT.ToKhai_CT)
                    {
                        Business_HQ.NewChungTu.infChungTuDTL objDTL = new Business_HQ.NewChungTu.infChungTuDTL();
                        objDTL.SO_TK = objGNT_CT.So_TK;
                        objDTL.LH_XNK = objGNT_CT.Ma_LH;
                        objDTL.SAC_THUE = objToKhai_CT.Ma_ST;
                        objDTL.Ma_TMuc = objToKhai_CT.NDKT;
                        objDTL.SoTien = double.Parse(objToKhai_CT.SoTien_VND);
                        objDTL.SoTien_NT = double.Parse(objToKhai_CT.SoTien_NT);
                        objDTL.Noi_Dung = Business_HQ.Common.mdlCommon.Get_TenMTM(objToKhai_CT.NDKT);
                        objDTL.MA_LT = objGNT_CT.Ma_LT;
                        objDTL.Ma_Chuong = obj304.Data.ThongTinChungTu.Ma_Chuong;
                        objDTL.TT_BTOAN = objGNT_CT.TTButToan;
                        objDTL.MA_HQ = objGNT_CT.Ma_HQ;
                        if (objHdrHq.So_TK.Length <= 0)
                            objHdrHq.So_TK = objGNT_CT.So_TK;
                        else
                        {
                            if (objHdrHq.So_TK.IndexOf(objGNT_CT.So_TK) < 0)
                                objHdrHq.So_TK += ";" + objGNT_CT.So_TK;
                        }

                        if (objGNT_CT.Ngay_DK != null)
                        {
                            objDTL.NGAY_TK = DateTime.ParseExact(objGNT_CT.Ngay_DK, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            objDTL.NGAY_TK = "01/01/" + objGNT_CT.Nam_DK;
                        }

                        listDTL.Add(objDTL);
                    }


                return mdlCommon.TCS_HQ247_OK;

            }
            catch (Exception ex)
            { }
            return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
        }

        //Nhamlt_20180330
        private string BuildEcomForPayment_V20(Business_HQ.NewChungTu.infChungTuHDR objHdrHq, List<Business_HQ.NewChungTu.infChungTuDTL> listDTL)
        {
            string v_strRetRemark = "";
            //if (listDTL.Count > 1)
            //{
            for (int i = 0; i < listDTL.Count; i++)
            {

                //Moi truong co do dai 200
                //Noi dung kinh te- mã tiểu mục
                v_strRetRemark += V_Substring(listDTL.ElementAt(i).Ma_TMuc, 4);
                //'mã chương
                v_strRetRemark += "~" + V_Substring(listDTL.ElementAt(i).Ma_Chuong, 3);
                //Số tiền
                string sotien = listDTL.ElementAt(i).SoTien.ToString();
                v_strRetRemark += "~" + V_Substring(VBOracleLib.Globals.Format_Number(sotien, ".").Replace(".", ""), 20);
                //'Nội dung
                v_strRetRemark += "~" + V_Substring(listDTL.ElementAt(i).Noi_Dung, 100);
                //'Số tờ khai
                v_strRetRemark += "~" + V_Substring(objHdrHq.So_TK, 30);
                string ngay_tk = "";
                if (listDTL.ElementAt(i).NGAY_TK != null)
                {
                    ngay_tk = DateTime.ParseExact(listDTL.ElementAt(i).NGAY_TK, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MM-yyyy");
                }
                v_strRetRemark += "~" + V_Substring(ngay_tk, 10);
                //if (i != 0)
                //{
                v_strRetRemark += "####";
                //}

            }

            v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark);


            //}

            return v_strRetRemark;
        }
        //Nhamlt_20180330
        //Nhamlt_20180330
        private string V_Substring(String s, int length)
        {
            if (s != null && s.Length > length)
            {
                s = s.Substring(0, length);
            }
            return s;

        }

        private static string BuildRemarkForPayment(Business_HQ.NewChungTu.infChungTuHDR objHdrHq, List<Business_HQ.NewChungTu.infChungTuDTL> listDTL, string ngayNT)
        {
            //Sua theo thong tu 2936 cua TCHQ_20180531
            string v_strRetRemark = "";
            try
            {
                string Ma_HQ = "";
                string So_TK = "";
                string LH_XNK = "";
                string v_strRetRemarkDetail = "";
                string Ma_chuong = "";
                string MA_LT = "";
                string NGAY_TK = "";
                string strMST = objHdrHq.Ma_NNThue;
                string strChiNhanhMST = "";
                if (strMST.Length == 13)
                {
                    strChiNhanhMST = strMST.Substring(10, 3);
                    strMST = strMST.Substring(0, 10) + "-" + strChiNhanhMST;
                     
                }

                foreach (Business_HQ.NewChungTu.infChungTuDTL objDtl in listDTL)
                {
                    v_strRetRemarkDetail += "(";
                    v_strRetRemarkDetail += "TM" + objDtl.Ma_TMuc;
                    v_strRetRemarkDetail += "+" + getMapSTDienGiai(objDtl.SAC_THUE);
                    string sotien = objDtl.SoTien.ToString();
                    v_strRetRemarkDetail += "+T" + VBOracleLib.Globals.Format_Number(sotien, ".").Replace(".", "");
                    v_strRetRemarkDetail += ")";
                    So_TK = objDtl.SO_TK.ToString();

                    LH_XNK = objDtl.LH_XNK;
                    Ma_HQ = objDtl.MA_HQ;
                    Ma_chuong = objDtl.Ma_Chuong;
                    MA_LT = objDtl.MA_LT;
                    NGAY_TK = objDtl.NGAY_TK;
                }
                v_strRetRemark += "HQDT+ID";
                v_strRetRemark += Business_HQ.HQ247.TCS_DM_NTDT_HQ.getSoHOSO(objHdrHq.Ma_NNThue, objHdrHq.TK_KH_NH);
                v_strRetRemark += "+MST" + strMST;
                v_strRetRemark += "+C" + Ma_chuong;
                //v_strRetRemark += "+NNT" + GetNgayKhoBac(objHdrHq.SHKB).ToString();
                v_strRetRemark += "+NNT" + ngayNT;
                v_strRetRemark += "+HQ" + objHdrHq.Ma_hq_ph;
                v_strRetRemark += "-" + Ma_HQ;
                v_strRetRemark += "-" + objHdrHq.Ma_CQThu;
                v_strRetRemark += "+TK" + So_TK;
                v_strRetRemark += "+NDK" + NGAY_TK.Replace("/", "");
                v_strRetRemark += "+LH" + LH_XNK;
                v_strRetRemark += "+NTK" + objHdrHq.MA_NTK;
                v_strRetRemark += "+LT" + MA_LT;
                v_strRetRemark += "+KB" + objHdrHq.SHKB;
                v_strRetRemark += "+TKNS" + objHdrHq.TK_Co;
                v_strRetRemark += "+VND";
                v_strRetRemark += v_strRetRemarkDetail;
                v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark);


            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog(ex, "", "");
                throw ex;
            }
            return v_strRetRemark;
        }
        private static string getMapSTDienGiai(string sacthue)
        {
            //XK	Thuế xuất khẩu	ST1
            //NK	Thuế nhập khẩu	ST2
            //VA	Thuế giá trị gia tăng	ST3
            //TD	Thuế tiêu thụ đặc biệt	ST4
            //TV	Thuế tự vệ chống bán phá giá	ST5
            //MT	Thuế bảo vệ môi trường	ST6
            //TC	Thuế chống trợ cấp	ST7
            //PB	Thuế chống phân biệt đối xử	ST8
            //PG	Thuế chống bán phá giá	ST10
            //KH	Thuế khác	ST9
            switch (sacthue)
            {
                case "XK": return "ST1";
                case "NK": return "ST2";
                case "VA": return "ST3";
                case "TD": return "ST4";
                case "TV": return "ST5";
                case "MT": return "ST6";
                case "TC": return "ST7";
                case "PB": return "ST8";
                case "PG": return "ST10";
                case "KH": return "ST9";
                case "LP": return "ST11";
            }
            return "";
        }
        //Nhamlt_20180330  
        private string BuildRemarkForPayment_V20(Business_HQ.NewChungTu.infChungTuHDR objHdrHq, List<Business_HQ.NewChungTu.infChungTuDTL> listDTL)
        {
            string v_strRetRemark = "";
            string strLHXNK = "";
            foreach (Business_HQ.NewChungTu.infChungTuDTL objDTL in listDTL)
            {
                if (listDTL.ElementAt(0).LH_XNK != null && listDTL.ElementAt(0).LH_XNK.Length > 0)
                {
                    strLHXNK = listDTL.ElementAt(0).LH_XNK;
                    break;
                }
            }
            //v_strRetRemark += "KT" + listDTL.ElementAt(i).Ky_Thue;         
            v_strRetRemark += "LH:" + strLHXNK;// objHdrHq.LH_XNK;            
            v_strRetRemark += ".DNCT " + objHdrHq.Ghi_chu;
            v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark);
            return v_strRetRemark;
        }

        private static string GetNgayKhoBac(string strSHKB)
        {
            string lRet = "";
            string sSQL = "";
            try
            {
                sSQL = " SELECT TO_CHAR (TO_DATE (giatri_ts, 'DD/MM/YYYY'), 'DDMMYYYY') FROM tcs_thamso ";
                sSQL += " WHERE ten_ts = 'NGAY_LV' AND ma_dthu = (SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS = '" + strSHKB + "')";

                DataSet ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            lRet = ds.Tables[0].Rows[0][0].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lRet = DateTime.Now.ToString("ddMMyyyy");
            }

            return lRet;
        }

        public decimal Check_CauTtruc_MSG(MSG304 obj304)
        {
            decimal result = mdlCommon.TCS_HQ247_OK;
            try
            {
                if (obj304 != null)
                {
                    decimal ttsum = 0;
                    //kiem tra thong tin chung tu
                    if (obj304.Data.ThongTinChungTu.GNT_CT == null)
                        return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                    if (obj304.Data.ThongTinChungTu.GNT_CT.Count <= 0)
                        return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                    for (int x = 0; x < obj304.Data.ThongTinChungTu.GNT_CT.Count; x++)
                    {
                        if (obj304.Data.ThongTinChungTu.GNT_CT[x].Ma_HQ == null || obj304.Data.ThongTinChungTu.GNT_CT[x].Ma_HQ.Length <= 0)
                            return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                        if (obj304.Data.ThongTinChungTu.GNT_CT[x].Ma_LH == null || obj304.Data.ThongTinChungTu.GNT_CT[x].Ma_LH.Length <= 0)
                            return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                        if (obj304.Data.ThongTinChungTu.GNT_CT[x].Ma_LT == null || obj304.Data.ThongTinChungTu.GNT_CT[x].Ma_LT.Length <= 0)
                            return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                        if (obj304.Data.ThongTinChungTu.GNT_CT[x].So_TK == null || obj304.Data.ThongTinChungTu.GNT_CT[x].So_TK.Length <= 0)
                            return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                        if (obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT == null)
                            return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                        if (obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT.Count <= 0)
                            return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                        for (int y = 0; y < obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT.Count; y++)
                        {
                            if (obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT[y].Ma_ST == null || obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT[y].Ma_ST.Length <= 0)
                                return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                            if (obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT[y].NDKT == null || obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT[y].NDKT.Length <= 0)
                                return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                            if (obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT[y].SoTien_NT == null || obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT[y].SoTien_NT.Length <= 0)
                                return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                            if (obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT[y].SoTien_VND == null || obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT[y].SoTien_VND.Length <= 0)
                                return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                            ttsum += decimal.Parse(obj304.Data.ThongTinChungTu.GNT_CT[x].ToKhai_CT[y].SoTien_VND);

                        }

                    }
                    if (decimal.Parse(obj304.Data.ThongTinChungTu.SoTien_TO) != ttsum)
                    {
                       log.Error("tu choi chung tu hq247[" + obj304.Data.ThongTinChungTu.So_CT + "] do tong so tien <> tong so tien chi tiet");
                        return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
                    }

                }
            }
            catch (Exception ex)
            { 
                log.Error("co loi trong XULY_MSG304 => Check_CauTtruc_MSG" + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
            return result;// mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
        }
    }
}
