﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business_HQ;
using System.Data;
using CustomService_V3;
using VBOracleLib;
using System.Configuration;
using System.Data.OracleClient;
namespace HQ247XulyMSG
{
    
    public class ReConnectDB
    {
        private static String strStart = ConfigurationSettings.AppSettings.Get("TIME_BEGIN_SEND_DK").ToString();
        private static String strEnd = ConfigurationSettings.AppSettings.Get("TIME_END_SEND_DK").ToString();
        private static bool isSend = false;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public  void CheckReConnectDB()
        {
            while (true)
            {
                Int32 hhmm = Int32.Parse(DateTime.Now.ToString("HHmmss"));
                Int32 iStart = Int32.Parse(strStart);
                Int32 iEnd = Int32.Parse(strEnd);

                if (!isSend && (hhmm >= iStart && hhmm <= iEnd))
                {
                    try
                    {
                        checkConnectDB();
                        isSend = true;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message + "-" + ex.StackTrace);
                       // LogDebug.WriteLog("CheckReConnectDB: " + ex.Message + " \n " + ex.StackTrace, System.Diagnostics.EventLogEntryType.Error);
                    }
                }
                if (isSend && (hhmm < iStart || hhmm > iEnd))
                {
                    isSend = false;
                }
            }
        }

        public string checkConnectDB()
        {
            string kqCheck = "";

            DataSet ds = null;

            try
            {
                StringBuilder sbSqlSelect = new StringBuilder("");
                sbSqlSelect.Append("select * from tcs_dm_nhanvien");
                ds =DataAccess.ExecuteReturnDataSet(sbSqlSelect.ToString(), CommandType.Text);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        kqCheck = "Connect database success";
                    }
                    else
                    {
                        kqCheck = "Connect database false";
                    }


                }
                else
                {
                    kqCheck = "Connect database false";
                }

            }
            catch (Exception ex)
            {
                kqCheck = "Connect database false: " + ex.Message;
                log.Error("checkConnectDB: " + kqCheck + " \n " + ex.StackTrace);
            }

            return kqCheck;
        }

       
    }
}
