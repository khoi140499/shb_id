﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Business_HQ;
using System.Data;
using CustomService_V3;
using VBOracleLib;
namespace HQ247XulyMSG
{
    public  class HQ247AutoDC
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool isSendAutoAntagonize()
        {
            try
            {
                string strSQL = "SELECT A.* FROM TCS_QLY_DC A,( SELECT MAX(ID) ID FROM TCS_QLY_DC ";
                strSQL += " WHERE   TO_CHAR (NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "'";
                strSQL += "GROUP BY   LOAI_GD) B WHERE  TO_CHAR (A.NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "' AND  A.ID=B.ID and a.LOAI_GD='807'";// and ERRNUM='0'";
                string strStartTimeDC = Business_HQ.Common.mdlCommon.TCS_GETTS("AUTO_DC_SEND_HQ247");
                strStartTimeDC = strStartTimeDC.Replace(":", "");
                if (int.Parse(strStartTimeDC) <= int.Parse(DateTime.Now.ToString("HHmmss")))
                {
                    DataSet ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL,CommandType.Text);
                        if (ds == null)
                        {
                            return true;
                        }
                    if(ds.Tables.Count<=0)
                    {
                        return true;
                    }
                    if(ds.Tables[0].Rows.Count<=0)
                    {
                        return true;
                    }
 
                    
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public static bool isReciveAutoAntagonize()
        {
            try
            {
                bool is807OK = false;
                string strSQL = "   SELECT A.* FROM TCS_QLY_DC A   ,( SELECT MAX(ID) ID     FROM   TCS_QLY_DC ";
                strSQL += " WHERE   TO_CHAR (NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "'";
                strSQL += "GROUP BY   LOAI_GD) B WHERE  TO_CHAR (A.NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "' AND  A.ID=B.ID and a.LOAI_GD='857' and ERRNUM='0'";
                //them điều kiện đã gửi 807 thanh cong thi moi gui nhan
                string strSQL807 = "   SELECT A.* FROM TCS_QLY_DC A   ,( SELECT MAX(ID) ID     FROM   TCS_QLY_DC ";
                strSQL807 += " WHERE   TO_CHAR (NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "'";
                strSQL807 += "GROUP BY   LOAI_GD) B WHERE  TO_CHAR (A.NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "' AND  A.ID=B.ID and a.LOAI_GD='807' and ERRNUM='0' ";
                DataSet dsx = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL807, CommandType.Text);
                if (dsx != null)
                    if (dsx.Tables.Count > 0)
                        if (dsx.Tables[0].Rows.Count > 0)
                            is807OK = true;
                string strStartTimeDC = Business_HQ.Common.mdlCommon.TCS_GETTS("AUTO_DC_RECIVE_HQ247");
                strStartTimeDC = strStartTimeDC.Replace(":", "");
                if (int.Parse(strStartTimeDC) <= int.Parse(DateTime.Now.ToString("HHmmss")))
                {
                    DataSet ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                    if (ds == null && is807OK)
                    {
                        return true;
                    }
                    if (ds.Tables.Count <= 0 && is807OK)
                    {
                        return true;
                    }
                    if (ds.Tables[0].Rows.Count <= 0 && is807OK)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

       public static string SendAutoAntagonize807()
        {
            string strErrorNum="1";
             string strErrorMes="loi he thong";
             DateTime dtNgayDC = DateTime.Now;
            try
            {
                CustomsServiceV3.ProcessMSG.GuiTDDoiChieuThuePhi_HQ_NTDT(dtNgayDC,ref strErrorNum,ref strErrorMes);
            }
            catch (Exception ex)
            {
                StringBuilder sbErrMsg = default(StringBuilder);
                sbErrMsg = new StringBuilder();
                sbErrMsg.Append("Lỗi trong quá trình gui đối chiếu dữ liệu giao dịch thành công : ngay -" + dtNgayDC.ToString("yyyy-MM-dd"));

                sbErrMsg.Append("insert vao CSDL\n");
                sbErrMsg.Append(ex.Message);
                sbErrMsg.Append("\n");
                sbErrMsg.Append(ex.StackTrace);
               log.Error(sbErrMsg.ToString());
               log.Error(ex.Message + "-" + ex.StackTrace);
                return sbErrMsg.ToString();
            }
            return "gui dc error[" + strErrorNum + "] errMSG[" + strErrorMes + "]";
            
        }
        public static string ReciveAutoAntagonize857()
        {
            string strErrorNum = "1";
            string strErrorMes = "loi he thong";
            DateTime dtNgayDC = DateTime.Now;
            try
            {
                CustomsServiceV3.ProcessMSG.getKetQuaDC_HQ247(dtNgayDC,ref strErrorNum,ref strErrorMes);
            }
            catch (Exception ex)
            {
                StringBuilder sbErrMsg = default(StringBuilder);
                sbErrMsg = new StringBuilder();
                sbErrMsg.Append("Lỗi trong quá trình lấy đối chiếu dữ liệu giao dịch thành công : ngay -" + dtNgayDC.ToString("yyyy-MM-dd"));

                sbErrMsg.Append("insert vao CSDL\n");
                sbErrMsg.Append(ex.Message);
                sbErrMsg.Append("\n");
                sbErrMsg.Append(ex.StackTrace);
               log.Error(sbErrMsg.ToString());
                log.Error(ex.Message + "-" + ex.StackTrace);
                return sbErrMsg.ToString();
            }
            return "nhan ket qua dc error[" + strErrorNum + "] errMSG[" + strErrorMes + "]";

        }
        public static bool isSendAutoAntagonizeTQ()
        {
            try
            {
                string strSQL = "   SELECT A.* FROM TCS_QLY_DC A   ,( SELECT MAX(ID) ID     FROM   TCS_QLY_DC ";
                strSQL += " WHERE   TO_CHAR (NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "'";
                strSQL += "GROUP BY   LOAI_GD) B WHERE  TO_CHAR (A.NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "' AND  A.ID=B.ID and a.LOAI_GD='801' ";
                string strStartTimeDC = Business_HQ.Common.mdlCommon.TCS_GETTS("AUTO_DC_SEND_HQ247");
                strStartTimeDC = strStartTimeDC.Replace(":", "");
                if (int.Parse(strStartTimeDC) <= int.Parse(DateTime.Now.ToString("HHmmss")))
                {
                    DataSet ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                    if (ds == null)
                    {
                        return true;
                    }
                    if (ds.Tables.Count <= 0)
                    {
                        return true;
                    }
                    if (ds.Tables[0].Rows.Count <= 0)
                    {
                        return true;
                    }


                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public static bool isReciveAutoAntagonizeTQ()
        {
            try
            {
                bool is801OK = false;
                string strSQL = "   SELECT A.* FROM TCS_QLY_DC A   ,( SELECT MAX(ID) ID     FROM   TCS_QLY_DC ";
                strSQL += " WHERE   TO_CHAR (NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "'";
                strSQL += "GROUP BY   LOAI_GD) B WHERE  TO_CHAR (A.NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "' AND  A.ID=B.ID and a.LOAI_GD='800' and ERRNUM='0'";
                //them điều kiện đã gửi 801 thanh cong thi moi gui nhan
                string strSQL801 = "   SELECT A.* FROM TCS_QLY_DC A   ,( SELECT MAX(ID) ID     FROM   TCS_QLY_DC ";
                strSQL801 += " WHERE   TO_CHAR (NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "'";
                strSQL801 += "GROUP BY   LOAI_GD) B WHERE  TO_CHAR (A.NGAY_DC, 'YYYYMMDD') = '" + DateTime.Now.ToString("yyyyMMdd") + "' AND  A.ID=B.ID and a.LOAI_GD='801' and ERRNUM='0' ";
                DataSet dsx = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL801, CommandType.Text);
                if (dsx != null)
                    if (dsx.Tables.Count > 0)
                        if (dsx.Tables[0].Rows.Count > 0)
                            is801OK = true;
                string strStartTimeDC = Business_HQ.Common.mdlCommon.TCS_GETTS("AUTO_DC_RECIVE_HQ247");
                strStartTimeDC = strStartTimeDC.Replace(":", "");
                if (int.Parse(strStartTimeDC) <= int.Parse(DateTime.Now.ToString("HHmmss")))
                {
                    DataSet ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                    if (ds == null && is801OK)
                    {
                        return true;
                    }
                    if (ds.Tables.Count <= 0 && is801OK)
                    {
                        return true;
                    }
                    if (ds.Tables[0].Rows.Count <= 0 && is801OK)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

    }
}
