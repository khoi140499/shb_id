﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data;
using VBOracleLib;
using Business_HQ;
using Business_HQ.Common;
using Business_HQ.HQ247;
using CustomsV3.MSG.MSG311;

namespace HQ247XulyMSG
{
    public class MSG_XULY_311IB
    {
        
        public string ProcessData311_IB(string StrID311)
        {
            try
            {
                DataSet ds = Business_HQ.HQ247.TCS_DM_NTDT_HQ.getMakerMSG213(StrID311,"","");
                string strID = StrID311;
                string strSo_HS = ds.Tables[0].Rows[0]["So_HS"].ToString();
                string strLoai_HS = ds.Tables[0].Rows[0]["Loai_HS"].ToString();  //1: Khai mới; 2: Khai sửa, 3: Khai hủy
                string strNoiDung_XL = ds.Tables[0].Rows[0]["LY_DO_CHUYEN_TRA"].ToString(); // Lý do Hủy, kèm hướng dẫn xử lý
                string strTenDN = ds.Tables[0].Rows[0]["ID_NV_CK312"].ToString(); // Tên đăng nhập
                string strLoai312 = "1";//1: Duyệt, 2: Chuyển trả
                string strHQErrorCode = "0";
                string strHQErrorMSG = "";
                //Update TRANGTHAI của Đăng ký trên bảng HQ_311 thành //11// (Đã kiểm soát - chờ xác nhận từ TCHQ)
                Decimal errID = TCS_DM_NTDT_HQ.CheckerMSG312(strID, strNoiDung_XL, strLoai312, strTenDN);//Nhận mã lỗi trả về
                if (errID == 0)
                {
                    //Build msg 312 và gửi đi TCHQ
                    //Trường hợp trả lời msg 311 (Đăng ký mới tại đầu TCHQ)
                    if (!strSo_HS.Equals(""))
                    {
                        errID = CustomsServiceV3.ProcessMSG.sendMSG312("311", strNoiDung_XL, strID, strTenDN, ref strHQErrorCode, ref strHQErrorMSG);
                    }
                    else
                    {
                        //Trường hợp đăng ký mới tại đầu NHTM
                        errID = CustomsServiceV3.ProcessMSG.sendMSG312("", strNoiDung_XL, strID, strTenDN, ref strHQErrorCode, ref strHQErrorMSG);
                    }
                    if (errID == 0)
                    {
                        return "Cập nhật trạng thái thành công !";
                    }
                    else
                    {
                        return "Lỗi: " + Business_HQ.Common.mdlCommon.Get_ErrorDesciption(errID.ToString()) + strHQErrorMSG + " !";
                    }
                }
                else
                {
                    return "Lỗi: " + Business_HQ.Common.mdlCommon.Get_ErrorDesciption(errID.ToString());
                }
            }
            catch (Exception ex)
            {
                return "Chuyển kiểm soát lỗi: " + ex.Message;
            }
        }
    }
}
