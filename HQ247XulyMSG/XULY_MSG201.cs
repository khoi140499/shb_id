﻿using System;
using System.Collections.Generic;
using Business_HQ.Common;
using CustomsV3.MSG.MSG201;
using CustomsV3.MSG.MSG314;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using VBOracleLib;
using Business_HQ.nhothu;
using CorebankServiceESB;
using System.Linq;

namespace HQ247XulyMSG
{
    public class XULY_MSG201
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string AccountHQ247 = "";
        public string Ma_AccountHQ247 = "";
        public string strSender_Code = "";
        public string strSender_Name = "";
        public string strMaxAutoProcess = "";
        public string strCORELPSF = "";
        private static String Core_version = ConfigurationSettings.AppSettings.Get("CORE.VERSION").ToString();
        public XULY_MSG201()
        {
            try
            {
                AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.AccountHQ247").ToString();
                Ma_AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.Ma_AccountHQ247").ToString();
                strSender_Code = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
                strSender_Name = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
                strCORELPSF = ConfigurationSettings.AppSettings.Get("CORE.LPSF").ToString();

            }
            catch (Exception ex)
            { }
        }

        public XULY_MSG201(string pvStrAccountHQ247, string pvStrMa_AccountHQ247, string pvstrSender_Code)
        {

            AccountHQ247 = pvStrAccountHQ247;
            Ma_AccountHQ247 = pvStrMa_AccountHQ247;
            strSender_Code = pvstrSender_Code;
            strSender_Name = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
            strCORELPSF = ConfigurationSettings.AppSettings.Get("CORE.LPSF").ToString();
        }
        public string ProcessData(string str201ID)
        {
            string lydo = "Xử lý thành công";
            string strErrMSG = "";
            string strErrcode = "";
            try
            {
                bool ko_du_so_du = false;
                string strDienGiai = "";
                decimal checkProcess = 0;
                string strTrangThai = "";
                string strCoreOn = "1";
                //lay message len
                string strSQL = "SELECT * FROM TCS_HQ247_201 WHERE ID='" + str201ID + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                //string strCodeFee = "____";
                //decimal decVAT = 0;
                //decimal decFeeAmt = 0;
                string strso_ct = "";
                string strso_ctNH = "";
                decimal decSodu = 0;
                //string strCIFNO = "";

                string strMa_NH_GTiep = "";
                string strTen_NH_GTiep = "";
                string strRemark = "";
                string strMa_NH_TT = "";
                string strTen_NH_TT = "";
                string strNDKTMP = "";
                string strSo_HS = "";
                string strMa_CN = "";
                string v_strTransaction_id = "";
                string pv_so_tn_ct = "";
                string pv_ngay_tn_ct = "";
                clsCoreBank objClsCoreBankESB = new clsCoreBank();
                clsSongPhuong objClsSongPhuong = new clsSongPhuong();
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            strTrangThai = ds.Tables[0].Rows[0]["TRANGTHAI"].ToString();
                            lydo = ds.Tables[0].Rows[0]["LYDO"].ToString();
                            strso_ct = ds.Tables[0].Rows[0]["SO_CTU"].ToString();
                            strSo_HS = ds.Tables[0].Rows[0]["SO_HS"].ToString();
                            strMa_CN = ds.Tables[0].Rows[0]["MA_CN"].ToString();
                            pv_so_tn_ct = ds.Tables[0].Rows[0]["so_tn_ct"].ToString();
                            pv_ngay_tn_ct = ds.Tables[0].Rows[0]["ngay_tn_ct"].ToString();
                            v_strTransaction_id = ds.Tables[0].Rows[0]["msg_id301"].ToString();
                            strso_ctNH = ds.Tables[0].Rows[0]["so_ct_nh"].ToString();
                        }
                MSG201 obj = new MSG201();
                obj = obj.MSGToObject(ds.Tables[0].Rows[0]["MSG_CONTENT"].ToString());
                MSG314 obj314 = new MSG314(obj.Header.Request_ID);
                Business_HQ.NewChungTu.infChungTuHDR objHdrHq = new Business_HQ.NewChungTu.infChungTuHDR();
                List<Business_HQ.NewChungTu.infChungTuDTL> listDTL = new List<Business_HQ.NewChungTu.infChungTuDTL>();
                //neu trang thai la moi nhan
                if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN))
                {
                    //kiem tra ho so
                    if (strSo_HS.Length <= 0)
                    {
                        strSo_HS = obj.Header.Request_ID;
                    }
                    if (mdlCommon.TCS_HQ247_OK != daTCS_HQ247_314.gf_CheckValidHS201(strSo_HS))
                    {
                        lydo = mdlCommon.Get_ErrorDesciption(mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE.ToString());
                        //chuyen trang thai thanh tu choi
                        strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI;
                        checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_TUCHOI(str201ID, mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI);
                    }
                    //doan nay em khai bao de lay thong tin so du tai khoan de kiem tra du tien hay khong truoc khi tao chnung tu va thanh toan
                    //neu ko du so du la tu choi luon
                    //neu qua phan kiem tra ho so thi moi lam tiep phan so chung tu
                    string strBranch_tk = "";
                    if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN) && strso_ct.Length <= 0)
                    {
                        //------hoang lv edit 04/02/2020-------------------------------------------------------------
                        string strSodu = "0";
                        string strErrorCode = "";
                        string strErrorMSG = "";

                        if (strCoreOn.Equals("1"))
                        {
                            strErrorMSG = objClsCoreBankESB.GetTK_KH_NH(obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.TaiKhoan_TH, ref strErrorCode, ref strSodu, ref strBranch_tk);
                            if (strErrorCode == "TO999")
                            {
                                //checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_CHO_CORE_RESPONSE(str201ID);
                                //if (checkProcess > 0)
                                //{
                                //    return "Loi khi cap nhat trang thai chung tu, khi truy van so du tai khoan Core Bank xay ra loi timeout";
                                //}
                                return "Loi timeout khi truy van so du tai khoan CoreBank";
                            }
                            if (strSodu.Length > 0)
                            {
                                try
                                {
                                    decSodu = decimal.Parse(strSodu);

                                }
                                catch (Exception ex)
                                {
                                    decSodu = 0;
                                }
                            }
                        }
                        else
                        {
                            decSodu = 100000000000;
                        }
                        if (!strErrorCode.Equals("00000"))
                        {
                            lydo = "Lỗi khi truy vấn tài khoản Core bank.";
                            //chuyen trang thai thanh tu choi
                            strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI;
                            checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_TUCHOI(str201ID, mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI);
                        }
                        else
                        {
                            //cap nhat lai ma chi nhanh cua tia khoan va cifno
                            //strSQL = "UPDATE TCS_HQ247_201 SET MA_CN='" + objbankinfo._BRANCH_CODE + "',CIFNO='" + objbankinfo._CIFNO + "' WHERE ID='" + str201ID + "'";
                            //VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                            //strMa_CN = objbankinfo._BRANCH_CODE;
                            //neu khong du so du thi khoi lam gi nua
                            if (decSodu < decimal.Parse(obj.Data.Item[0].DuNo_TO))
                            {
                                lydo = mdlCommon.Get_ErrorDesciption(mdlCommon.TCS_HQ247_HQ_KHONGDUSODU.ToString());
                                //chuyen trang thai thanh tu choi
                                strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI;
                                checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_TUCHOI(str201ID, mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI);
                                ko_du_so_du = true;
                            }
                        }

                        //sau khi kiem tra so du ma van ngon  thi kiem tra nga hang truc tiep gian tiep
                        if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN))
                        {
                            DataSet dsx = null;
                            string strResult = objClsSongPhuong.ValiadSHB(obj.Data.Item[0].Ma_KB);


                            string[] arrResult;
                            string strNHB = "";
                            string strACC_NHB = "";
                            if (strResult.Length > 8)
                            {
                                arrResult = strResult.Split(';');
                                strNHB = arrResult[0].ToString();
                                strACC_NHB = arrResult[1].ToString();
                            }

                            if (strNHB.Length == 8)
                            {
                                dsx = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiepKB_SHB(obj.Data.Item[0].Ma_KB, strNHB);
                            }
                            else
                            {
                                dsx = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiep(obj.Data.Item[0].Ma_KB);
                            }

                            if (dsx != null)
                                if (dsx.Tables.Count > 0)
                                    if (dsx.Tables[0].Rows.Count > 0)
                                    {
                                        strMa_NH_TT = dsx.Tables[0].Rows[0]["MA_TRUCTIEP"].ToString();
                                        strTen_NH_TT = dsx.Tables[0].Rows[0]["TEN_TRUCTIEP"].ToString();
                                        strMa_NH_GTiep = dsx.Tables[0].Rows[0]["MA_GIANTIEP"].ToString();
                                        strTen_NH_GTiep = dsx.Tables[0].Rows[0]["TEN_GIANTIEP"].ToString();
                                    }
                            //NEU MA  GIAN TIEP MA RONG THI ONG TRA LAI
                            if (strMa_NH_GTiep.Length <= 0)
                            {
                                lydo = "Lỗi trong qua trình thanh toán không tồn tại mã ngân hàng trực tiếp ";
                                strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI;
                                checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_TUCHOI(str201ID, mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI);
                            }
                        }
                        //tiep theo den doan remark
                        strRemark += "MST" + obj.Data.Item[0].Ma_DV;
                        strRemark += " LT04";
                        for (int i = 0; i < obj.Data.Item.Count; i++)
                        {
                            strRemark += "TK" + obj.Data.Item[i].So_TK + " ";
                            strRemark += "  NGAYDK" + Business_HQ.Common.mdlCommon.ConvertStringToDate(obj.Data.Item[i].Ngay_DK, "yyyy-MM-dd").ToString("dd/MM/yyyy");
                            strNDKTMP = Business_HQ.Common.mdlCommon.ConvertStringToDate(obj.Data.Item[i].Ngay_DK, "yyyy-MM-dd").ToString("dd/MM/yyyy");
                            strRemark += " LHXNK" + obj.Data.Item[i].Ma_LH + " (";
                            for (int x = 0; x < obj.Data.Item[i].CT_No.Count; x++)
                            {
                                strRemark += "C" + obj.Data.Item[i].Ma_Chuong;
                                strRemark += " TM" + obj.Data.Item[i].CT_No[x].TieuMuc;
                                strRemark += " ST" + obj.Data.Item[i].CT_No[x].DuNo;
                            }
                            strRemark += ")";
                        }
                        strRemark += obj.Data.Item[0].Ten_KB;
                        string strRemark_new = PGB_buildRemark1(obj);
                        string sCheckCITAD = Business_HQ.CTuCommon.GetValue_THAMSOHT("CITAD_REMARK").ToString();
                        if (sCheckCITAD.Equals("2"))
                        {
                            strRemark = Globals.RemoveSign4VietnameseString(strRemark);
                        }
                        else
                        {
                            strRemark = strRemark_new;
                        }
                        //kiem tra chuoi 210
                        if (!Business_HQ.CTuCommon.GetValue_THAMSOHT("CITAD_VERSION").ToString().Equals("2.3") && !!Business_HQ.CTuCommon.GetValue_THAMSOHT("CITAD_VERSION").ToString().Equals("2.5"))
                        {
                            if (strRemark.Length > 210)
                            {
                                lydo = "Loi vuot qua 210 ky tu di citad";
                                strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI;
                                checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_TUCHOI(str201ID, mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI);
                            }
                        }
                        //doan nay bat dau ghi nhan chung tu
                        if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN))
                        {
                            if (daCTUHQ201.checkTrangthaiMSG201CTU(str201ID, strTrangThai, "1") != mdlCommon.TCS_HQ247_OK)
                            {
                                return "khong tiep tuc thuc hien do co luong khac dang xu ly chung tu";
                            }

                            checkProcess = SetChungtuHQ(obj, ref objHdrHq, ref listDTL);
                            if (checkProcess != 0)
                                return "Xử lý từ chứng từ lỗi id[" + str201ID + "] phan set chung tu can xu ly lai";
                            //day them thong tin
                            //ngan hang truc tiep gian tiep
                            objHdrHq.MA_NH_TT = strMa_NH_TT;
                            objHdrHq.TEN_NH_TT = strTen_NH_TT;
                            objHdrHq.MA_NH_B = strMa_NH_GTiep;
                            objHdrHq.Ten_NH_B = strTen_NH_GTiep;
                            objHdrHq.MA_NH_A = strSender_Code;
                            objHdrHq.Ghi_chu = strRemark;
                            objHdrHq.TK_KH_NH = obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.TaiKhoan_TH;
                            objHdrHq.TEN_KH_NH = obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.Ten_TaiKhoan_TH;
                            objHdrHq.DC_NNTien = obj314.Document.Data.DiaChi;
                            objHdrHq.DC_NNThue = obj314.Document.Data.DiaChi;
                            //objHdrHq.Ma_CN = strMa_CN;
                            objHdrHq.REF_NO = str201ID;
                            objHdrHq.DIENGIAI_HQ = strRemark;
                            objHdrHq.SO_CMND = obj314.Document.Data.ThongTin_NNT.So_CMT;
                            objHdrHq.Ten_nh_a = strSender_Name;
                            //VALID LAI XEM CO SO CHUNG TU CHUA NEU CO ROI THI EM KHONG CHAY NUA OUT LUONG
                            if (daCTUHQ201.checkTrangthaiMSG201CTU(str201ID, strTrangThai, "1") != mdlCommon.TCS_HQ247_OK)
                            {
                                return "khong tiep tuc thuc hien do co luong khac dang xu ly chung tu";
                            }
                            checkProcess = daCTUHQ201.Insert301(objHdrHq, listDTL, str201ID, AccountHQ247);
                            if (checkProcess > 0)
                            {
                                lydo = mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                                strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI;
                            }
                            else
                            {
                                strso_ct = objHdrHq.So_CT;
                            }
                        }
                    }
                    //neu da co chung tu thi hach toan
                    if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN) && strso_ct.Length > 0 && strso_ctNH.Length <= 0)
                    {
                        //kiem tra lai so chung tu da nho may co oong nao cung xu ly
                        if (daCTUHQ201.checkTrangthaiMSG201CTUNH(str201ID, strTrangThai, strso_ct) != mdlCommon.TCS_HQ247_OK)
                        {
                            return "khong tiep tuc thuc hien do co luong khac dang xu ly chung tu";
                        }
                        //chuan bi cac thong tin khac de gui cat tien
                        string recvAddr = obj.Data.Item[0].Ma_KB + "-" + obj.Data.Item[0].Ten_KB;
                        //string strRefCode = "";
                        //string strRefMSG = "";
                        if (recvAddr.Length > 100)
                        {
                            recvAddr = recvAddr.Substring(0, 99);
                        }
                        //--------------hoang lv add new change---------------------------------------------------------------------
                        if (strCoreOn.Equals("1"))
                        {
                            string str_PayMent = "";
                            string rm_ref_no = "";
                            string req_Internal_rmk = "";
                            string v_strErrorCode = "";
                            string str_AccountNo = obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.TaiKhoan_TH;
                            string strTKCo = obj.Data.Item[0].TKKB;
                            string strTongTienTT = obj.Data.Item[0].DuNo_TO;
                            string strMa_NguyenTe = "VND";
                            string v_strNgay_KB = objHdrHq.Ngay_KB.ToString();
                            string str_MaTinh = objHdrHq.Ma_Tinh;
                            string str_TenTinh = "";

                            string ngay_NT = "";
                            ngay_NT = objClsCoreBankESB.Get_VALUE_DATE();
                            ngay_NT = Business_HQ.Common.mdlCommon.ConvertNumbertoString_HQ(ngay_NT);

                            if (objHdrHq.So_CT == null)
                            {
                                objHdrHq = loadHDR(strso_ct);
                            }
                            if (objHdrHq.So_CT.Length <= 0)
                            {
                                objHdrHq = loadHDR(strso_ct);
                            }
                            if (objHdrHq.QUAN_HUYEN_NNTIEN != null)
                            {
                                str_TenTinh = Globals.RemoveSign4VietnameseString(objHdrHq.QUAN_HUYEN_NNTIEN);
                            }
                            string strTenCQT = "";
                            strTenCQT = Globals.RemoveSign4VietnameseString(obj.Data.Item[0].Ten_HQ_PH);
                            string strTen_NH_B = "";
                            if (objHdrHq.Ten_NH_B != null)
                            {
                                strTen_NH_B = Globals.RemoveSign4VietnameseString(objHdrHq.Ten_NH_B);
                            }
                            string strMa_NH_B = objHdrHq.MA_NH_B;
                            string v_strSo_CT = strso_ct;
                            string v_strMa_NV = Ma_AccountHQ247;//Ma nhan vien nay la ma nhan vien cua nguoi thuc hien, truong hop nay la ma_nv trong config cua app goi den
                            string v_strMa_NV_CT = objHdrHq.Ma_NV.ToString();//Ma nhan vien nay la ma nhan vien trong hdr cua chung tu,


                            //Kiểm tra có đi kho bạc SHB hay không
                            string strResult = objClsSongPhuong.ValiadSHB(objHdrHq.SHKB);

                            string[] arrResult;
                            string strNHB = "";
                            string strACC_NHB = "";
                            string strMA_CN_SP = "";
                            if (strResult.Length > 8)
                            {
                                arrResult = strResult.Split(';');
                                strNHB = arrResult[0].ToString();
                                strACC_NHB = arrResult[1].ToString();
                                strMA_CN_SP = arrResult[2].ToString();
                            }

                            bool checkThuSHB = false;
                            if (strNHB.Length == 8)
                            {

                                checkThuSHB = true;
                                //20180514_Lay dien giai
                                strDienGiai = BuildRemarkForPayment(obj314.Document.Data.So_HS, obj, ngay_NT);

                                string ngay_GD_SP = DateTime.Now.ToString("yyyyMMdd");
                                checkProcess = daCTUHQ201.checkTrangthaiMSG201(str201ID, Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN);
                                if (checkProcess > 0)
                                {
                                    return "Khong tiep tuc thuc hien do co luong khac dang xu ly chung tu.";
                                }
                                checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_CHO_CORE_RESPONSE(str201ID);
                                if (checkProcess > 0)
                                {
                                    return "Loi khi cap nhat trang thai chung tu cho phan hoi core bank";
                                }
                                //cap nhat TTSP la 1
                                strSQL = "UPDATE TCS_HQ247_201 SET TT_SP='1' WHERE so_ctu='" + v_strSo_CT + "' ";
                                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                //Gọi hàm thanh toán mới vào kho bạc SHB
                                objClsCoreBankESB.PaymentSP("TAX", ngay_GD_SP, v_strSo_CT, "110000", strBranch_tk, str_AccountNo, strTongTienTT, strMa_NguyenTe, "", strDienGiai,
                                         v_strMa_NV_CT, v_strMa_NV, "ETAX_HQ247", strTKCo, strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B, strACC_NHB,strMA_CN_SP, ref  v_strErrorCode, ref rm_ref_no);

                            }
                            else
                            {
                                if (Core_version.Equals("2"))
                                {
                                    //Core V2.0 20180330--------------
                                    string EcomStr = BuildEcomForPayment_V20(obj);
                                    strDienGiai = BuildRemarkForPayment_V20(obj);
                                    string str_TenMST = V_Substring(obj314.Document.Data.Ten_DV, 200);// objHdrHq.Ten_NNThue, 200);
                                    string str_MST = V_Substring(obj.Data.Item[0].Ma_DV, 14);// objHdrHq.Ma_NNThue, 14);
                                    string str_MaLThue = V_Substring(objHdrHq.Ma_LThue, 2);
                                    //Ngay nop thue dinh dang ddMMyyyy
                                    string str_NNT = DateTime.Now.ToString("yyyyMMdd");
                                    string str_AddNNT = V_Substring(obj314.Document.Data.DiaChi, 200);// objHdrHq.DC_NNThue, 200);
                                    string strMaCQT = V_Substring(obj.Data.Item[0].Ma_HQ_CQT, 7);// objHdrHq.Ma_CQThu, 7);
                                    strTenCQT = V_Substring(strTenCQT, 200);
                                    string strMaDBHC = "";
                                    string strTenDBHC = "";
                                    checkProcess = daCTUHQ201.checkTrangthaiMSG201(str201ID, Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN);
                                    if (checkProcess > 0)
                                    {
                                        return "Khong tiep tuc thuc hien do co luong khac dang xu ly chung tu.";
                                    }
                                    checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_CHO_CORE_RESPONSE(str201ID);
                                    if (checkProcess > 0)
                                    {
                                        return "Loi khi cap nhat trang thai chung tu cho phan hoi core bank";
                                    }
                                    //Core V2.0 20180330----------------
                                    str_PayMent = objClsCoreBankESB.PaymentTQ_V20(str_AccountNo, strTKCo, strTongTienTT, strMa_NguyenTe, strDienGiai, v_strNgay_KB, strTenCQT,
                                        str_MaTinh, strTenCQT, strTenCQT, strTen_NH_B, strTen_NH_B, str_TenTinh,
                                        strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B.ToString().Substring(5, 3), strMa_NH_B.ToString().Substring(0, 2), "TAX", v_strSo_CT, strMa_NH_B, v_strMa_NV_CT, v_strMa_NV, "ETAX_HQ247", req_Internal_rmk,
                                        str_NNT, str_MaLThue, str_MST, strMaCQT, strTenCQT, strMaDBHC, strTenDBHC, str_TenMST, str_AddNNT, EcomStr,
                                        ref  v_strErrorCode, ref rm_ref_no);
                                }
                                else
                                {
                                    //20180514_Lay dien giai
                                    strDienGiai = BuildRemarkForPayment(obj314.Document.Data.So_HS, obj, ngay_NT);
                                    checkProcess = daCTUHQ201.checkTrangthaiMSG201(str201ID, Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN);
                                    if (checkProcess > 0)
                                    {
                                        return "Khong tiep tuc thuc hien do co luong khac dang xu ly chung tu.";
                                    }
                                    checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_CHO_CORE_RESPONSE(str201ID);
                                    if (checkProcess > 0)
                                    {
                                        return "Loi khi cap nhat trang thai chung tu cho phan hoi core bank";
                                    }
                                    str_PayMent = objClsCoreBankESB.PaymentTQ(str_AccountNo, strTKCo, strTongTienTT, strMa_NguyenTe, strDienGiai, v_strNgay_KB, strTenCQT,
                                    str_MaTinh, strTenCQT, strTenCQT, strTen_NH_B, strTen_NH_B, str_TenTinh,
                                    strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B.ToString().Substring(5, 3), strMa_NH_B.ToString().Substring(0, 2), "TAX", v_strSo_CT, strMa_NH_B, v_strMa_NV_CT, v_strMa_NV, "ETAX_HQ247", req_Internal_rmk, ref  v_strErrorCode, ref rm_ref_no);
                                }
                            }

                            if (v_strErrorCode != null && v_strErrorCode.Equals("00000"))
                            {
                                strso_ctNH = rm_ref_no;
                                //-----Cap nhat lai so chung tu core
                                checkProcess = daCTUHQ201.Update201_HachToan(str201ID, rm_ref_no, strDienGiai, AccountHQ247);
                                if (checkProcess > 0)
                                {
                                    return "Loi khi cap nhat so chung tu core" + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                                }

                                //Core trả ra thành công thì gửi đi song phương
                                if (checkThuSHB == true)
                                {
                                    //-----Cap nhat lai thong tin vao hdr hq201,ngay_kh_nh
                                    // daCTUHQ201.UpdateTTSP(strso_ct, objHdrHq.MA_NH_TT, objHdrHq.TEN_NH_TT, strMa_NH_B, strTen_NH_B, rm_ref_no, strDienGiai);
                                    //objClsSongPhuong.SendChungTuSP(str304ID, "HQ247");
                                    objClsSongPhuong.SendChungTuSP(strso_ct, "NHOTHU");
                                }
                                else
                                {
                                    Business.CitadV25 citad25 = new Business.CitadV25();
                                    string core_date = objClsCoreBankESB.Get_VALUE_DATE();
                                    string content_ex = citad25.CreateContentEx(v_strSo_CT, "HQNT", core_date);
                                    content_ex = Business.Common.mdlCommon.EncodeBase64_ContentEx(content_ex);

                                    objClsCoreBankESB.SendContentEx(v_strSo_CT, rm_ref_no, "HQNT", content_ex, ref v_strErrorCode);
                                    if (v_strErrorCode.Equals("00000"))
                                    {
                                        //cap nhap send core thanh cong: 1 la thanh cong, 0 la chua thanh cong
                                        VBOracleLib.DataAccess.ExecuteNonQuery("update tcs_hq247_201 set SEND_CONTENT_EX = '1' where so_ctu='" + v_strSo_CT + "'", CommandType.Text);
                                    }
                                
                                }

                            }
                            else if (v_strErrorCode != null && v_strErrorCode.Equals("005"))
                            {
                                lydo = "Loi xay ra khi hach toan chung tu: Tài khoản không đủ để hạch toán";
                                strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI;
                                checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_TUCHOI(str201ID, mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI);
                                ko_du_so_du = true;
                            }
                            else if ((v_strErrorCode != null && v_strErrorCode.Equals("TO999")) || (rm_ref_no != null && rm_ref_no == strCORELPSF))
                            {
                                lydo = "Loi: Corebank timeout";
                                return lydo;
                            }
                            else
                            {
                                lydo = "Loi xay ra khi hach toan chung tu:" + mdlCommon.Get_ErrorDesciptionCoreBank(v_strErrorCode);
                                strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI;
                                checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_TUCHOI(str201ID, mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI);
                            }

                        }
                        if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN))
                        {

                            //maker213 chap nhan
                            checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_MAKER213(str201ID, "Chap nhan thanh toan", AccountHQ247);
                            if (checkProcess > 0)
                            {
                                return "Loi " + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                            }
                            //gan tran thai = da maker 213
                            strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213;
                        }
                        //--------------------------------------------------------------------------------------
                    }
                    if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN))
                    {
                        //maker213 chap nhan
                        checkProcess = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_MAKER213(str201ID, "Chap nhan thanh toan", AccountHQ247);
                        if (checkProcess > 0)
                        {
                            return "Loi " + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                        }
                        //gan tran thai = da maker 213
                        strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213;
                    }
                }
                //neu trang thai = maker 213 thi em send 213
                if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213))
                {
                    checkProcess = daCTUHQ201.checkTrangthaiMSG201CTU(str201ID, Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213, strso_ct);
                    if (checkProcess > 0)
                    {
                        return "Loi " + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                    }
                    //send 213 di
                    checkProcess = CustomsServiceV3.ProcessMSG.sendMSG213_201("201", "1", "Chap nhan thanh toan", str201ID, AccountHQ247, ref strErrcode, ref strErrMSG);
                    if (checkProcess > 0)
                    {
                        return "Loi " + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                    }
                    //gan trang thai = da gui 213
                    strTrangThai = mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213;
                }
                //neu trang thai la da gui 213 thi maker 301
                if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213) || strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_SEND301_ERROR))
                {
                    String strErrorNum = "0";
                    String strErrorMes = "";
                    checkProcess = daCTUHQ201.checkTrangthaiMSG201CTU(str201ID, strTrangThai, strso_ct);
                    if (checkProcess > 0)
                    {
                        return "Loi " + mdlCommon.Get_ErrorDesciption(checkProcess.ToString());
                    }
                    //kiem tra lai xem da co so tiep nhan chua
                    if (pv_so_tn_ct.Length <= 0)
                    {
                        daCTUHQ201.getSotiepNhan(str201ID, strso_ct, ref pv_so_tn_ct, ref pv_ngay_tn_ct, ref v_strTransaction_id);
                    }
                    if (pv_so_tn_ct.Length <= 0)
                    {
                        CustomsServiceV3.ProcessMSG.guiCTThueHQNHOTHU(obj.Data.Item[0].Ma_DV, strso_ct, ref strErrorNum, ref strErrorMes, ref v_strTransaction_id, ref pv_so_tn_ct, ref pv_ngay_tn_ct);
                        if (!strErrorNum.Equals("0"))
                        {
                            daCTUHQ201.TCS_HQ247_TRANGTHAI_201_SEND301_ERROR(str201ID, AccountHQ247);
                            return "Da hach toan ct[" + str201ID + "] Gui HQ loi";
                        }
                    }
                    if (pv_so_tn_ct.Length > 0)
                    {
                        Business_HQ.infDoiChieuNHHQ_HDR hdr = new Business_HQ.infDoiChieuNHHQ_HDR();
                        Business_HQ.infDoiChieuNHHQ_DTL[] dtls = null;
                        SetObjectToDoiChieuNHHQ(obj.Data.Item[0].Ma_KB, strso_ct, "", Ma_AccountHQ247, "CTU", ref hdr, ref dtls, obj.Data.Item[0].Ma_KB, strErrorNum, mdlCommon.TransactionHQ_Type.M47.ToString(), "", pv_so_tn_ct, pv_ngay_tn_ct);
                        hdr.transaction_id = v_strTransaction_id;
                        Business_HQ.ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, "1", strErrorMes);
                    }
                    decimal decResult = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_SEND301_OK(str201ID, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct, AccountHQ247);
                    lydo = "Xử lý thành công";
                }
                if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI))
                {
                    Decimal decResult = daCTUHQ201.checkTrangthaiMSG201(str201ID, Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI);
                    if (decResult == 0)
                    {
                        string ma_kq_xl = "2";
                        if (ko_du_so_du) ma_kq_xl = "9";
                        decResult = daCTUHQ201.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI(str201ID, lydo, AccountHQ247, ma_kq_xl);
                        if (decResult != 0)
                            return "Xử lý từ chối lỗi id[" + str201ID + "]";
                        strTrangThai = Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI;
                    }
                    else
                    {
                        return "tam dung tu choi khong chay nua do khong dung trang thai";
                    }
                }
                //neu la trang thai maker213 tu choi
                if (strTrangThai.Equals(mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI))//05
                {
                    Decimal decResult = daCTUHQ201.checkTrangthaiMSG201(str201ID, Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI);
                    string ma_kq_xl = "2";
                    if (ko_du_so_du) ma_kq_xl = "9";

                    //Business_HQ.nhothu.daCTUHQ201.TCS_HQ247_TRANGTHAI_201_KQXL(str201ID, ma_kq_xl, lydo);

                    if (decResult == 0)
                    {
                        decResult = CustomsServiceV3.ProcessMSG.sendMSG213_201("201", ma_kq_xl, lydo, str201ID, AccountHQ247, ref strErrcode, ref strErrMSG);
                    }
                    else
                    {
                        return "tam dung tu choi khong chay nua do khong dung trang thai";
                    }
                    if (decResult == 0)
                    {
                        lydo = "Đã từ chối chứng từ thành công lý do: " + lydo;
                    }
                    if (decResult > 0)
                    {
                        Business_HQ.nhothu.daCTUHQ201.TCS_HQ247_TRANGTHAI_201_CHEKER213_TUCHOI(str201ID, AccountHQ247);
                        lydo = "Từ chối chứng từ không thành công lý do: " + lydo + ", do gửi thông điệp 213 lỗi.";
                        return lydo;
                    }

                }
                //--------------hoanglv edit 20200311----------------------------------------------------------
                if (strTrangThai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213_TUCHOI))//06
                {
                    //gui HQ tu choi
                    string ma_kq_xl = "2";
                    if (ko_du_so_du) ma_kq_xl = "9";

                    //Business_HQ.nhothu.daCTUHQ201.TCS_HQ247_TRANGTHAI_201_KQXL(str201ID, ma_kq_xl, lydo);

                    Decimal decResult = daCTUHQ201.checkTrangthaiMSG201(str201ID, Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213_TUCHOI);
                    if (decResult == 0)
                    {
                        decResult = CustomsServiceV3.ProcessMSG.sendMSG213_201("201", ma_kq_xl, lydo, str201ID, AccountHQ247, ref strErrcode, ref strErrMSG);
                        if (strErrcode == "0" || strErrcode.Trim() == "")
                        {
                            lydo = "Đã từ chối chứng từ thành công lý do: " + lydo;
                        }
                        else
                        {
                            lydo = "Từ chối chứng từ không thành công lý do: gửi thông điệp 213 lỗi.";
                        }
                    }
                    else
                    {
                        return "tam dung tu choi khong chay nua do khong dung trang thai";
                    }

                }

            }
            catch (Exception ex)
            {
                return "Xử lý chứng từ lỗi: " + ex.Message;
            }
            return lydo;
        }
        public static string PGB_buildRemark1(MSG201 obj304)
        {
            string sCheckCITAD = Business_HQ.CTuCommon.GetValue_THAMSOHT("CITAD_REMARK").ToString();
            if (sCheckCITAD.Equals("1"))
            {
                string strRemark_new = "";

                for (int i = 0; i < obj304.Data.Item.Count; i++)
                {
                    string strMST = obj304.Data.Item[i].Ma_DV;
                    string strChiNhanhMST = "";

                    if (strMST.Length == 13)
                    {
                        strChiNhanhMST = strMST.Substring(10, 3);
                        strMST = strMST.Substring(0, 10) + "-" + strChiNhanhMST;

                    }

                    strRemark_new += "HQDT";
                    strRemark_new += "+ID0";
                    strRemark_new += "+MST" + strMST;
                    strRemark_new += "+C" + obj304.Data.Item[i].Ma_Chuong;
                    strRemark_new += "+NNT" + DateTime.Now.ToString("ddMMyyyy");
                    strRemark_new += "+HQ" + obj304.Data.Item[i].Ma_HQ_PH;
                    strRemark_new += "-" + obj304.Data.Item[i].Ma_HQ;
                    strRemark_new += "-" + obj304.Data.Item[i].Ma_HQ_CQT;
                    strRemark_new += "+TK" + obj304.Data.Item[i].So_TK;
                    strRemark_new += "+NDK" + Business_HQ.Common.mdlCommon.ConvertStringToDate(obj304.Data.Item[i].Ngay_DK, "yyyy-MM-dd").ToString("ddMMyyyy");
                    strRemark_new += "+LH" + obj304.Data.Item[i].Ma_LH;
                    strRemark_new += "+NTK" + obj304.Data.Item[i].Ma_NTK;
                    strRemark_new += "+LT" + obj304.Data.Item[i].Ma_LT;
                    strRemark_new += "+KB" + obj304.Data.Item[i].Ma_KB;
                    strRemark_new += "+TKNS" + obj304.Data.Item[i].TKKB;
                    strRemark_new += "+VND";
                    for (int x = 0; x < obj304.Data.Item[i].CT_No.Count; x++)
                    {
                        strRemark_new += "(";
                        strRemark_new += "TM" + obj304.Data.Item[i].CT_No[x].TieuMuc;
                        strRemark_new += "+ST" + getMa_ST(obj304.Data.Item[i].CT_No[x].LoaiThue);
                        strRemark_new += "+T" + obj304.Data.Item[i].CT_No[x].DuNo;
                        strRemark_new += ")";
                    }
                }
                return strRemark_new;
            }
            else
            {
                string result = "MST" + obj304.Data.Item[0].Ma_DV;
                result += " LT04";
                for (int i = 0; i < obj304.Data.Item.Count; i++)
                {
                    result += "TK" + obj304.Data.Item[i].So_TK + " ";
                    result += "  NGAYDK" + Business_HQ.Common.mdlCommon.ConvertStringToDate(obj304.Data.Item[i].Ngay_DK, "yyyy-MM-dd").ToString("dd/MM/yyyy");
                    result += " LHXNK" + obj304.Data.Item[i].Ma_LH + " (";
                    for (int x = 0; x < obj304.Data.Item[i].CT_No.Count; x++)
                    {
                        result += "C" + obj304.Data.Item[i].Ma_Chuong;
                        result += " TM" + obj304.Data.Item[i].CT_No[x].TieuMuc;
                        result += " ST" + obj304.Data.Item[i].CT_No[x].DuNo;
                    }
                    result += ")";
                }

                result += obj304.Data.Item[0].Ten_KB;
                return result;
            }
        }
        public static string getMa_ST(string pvSAC_THUE)
        {
            string strGIA_TRI = "";
            string strSql = "Select GIA_TRI From tcs_map_st Where SAC_THUE= :SAC_THUE";
            DataSet ds;
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SAC_THUE";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = pvSAC_THUE;
            //ds = DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text, p);
            ds = DataAccess.ExecuteReturnDataSet(strSql, p);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    strGIA_TRI = ds.Tables[0].Rows[0]["GIA_TRI"].ToString();
                }
                else
                {// trạng thái lỗi
                    return "";
                }
            }

            return strGIA_TRI;
        }
        private DataSet GetNHTHANHTOAN(string strSHKB)
        {
            string strSql = "Select MA_TRUCTIEP MA, TEN_TRUCTIEP TEN From TCS_DM_NH_GIANTIEP_TRUCTIEP Where SHKB= :SHKB";
            DataSet ds;
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SHKB";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = strSHKB;
            ds = DataAccess.ExecuteReturnDataSet(strSql.ToString(), CommandType.Text, p);
            return ds;
        }
        private DataSet GetNHNhan(string strSHKB)
        {
            string strSql = "Select MA_GIANTIEP MA, TEN_GIANTIEP TEN From TCS_DM_NH_GIANTIEP_TRUCTIEP Where SHKB= :SHKB";
            DataSet ds;
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SHKB";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = strSHKB;
            ds = DataAccess.ExecuteReturnDataSet(strSql.ToString(), CommandType.Text, p);
            return ds;
        }
        public decimal SetChungtuHQ(MSG201 obj304, ref Business_HQ.NewChungTu.infChungTuHDR objHdrHq, ref List<Business_HQ.NewChungTu.infChungTuDTL> listDTL)
        {
            try
            {
                objHdrHq = new Business_HQ.NewChungTu.infChungTuHDR();
                listDTL = new List<Business_HQ.NewChungTu.infChungTuDTL>();
                objHdrHq.SHKB = obj304.Data.Item[0].Ma_KB;
                objHdrHq.Ma_Tinh = "";
                objHdrHq.Ma_Huyen = "";
                objHdrHq.Ma_Xa = "";
                objHdrHq.TK_Co = obj304.Data.Item[0].TKKB;
                objHdrHq.Ma_LThue = "04";
                objHdrHq.Ma_NNTien = obj304.Data.Item[0].Ma_DV;
                objHdrHq.Ten_NNTien = obj304.Data.Item[0].Ten_DV;
                // objHdrHq.DC_NNTien = obj304.Data.ThongTinGiaoDich.NguoiNopTien.DiaChi;
                objHdrHq.Ten_NNThue = obj304.Data.Item[0].Ten_DV;
                objHdrHq.Ma_NNThue = obj304.Data.Item[0].Ma_DV;

                objHdrHq.Ma_Huyen_NNThue = "";
                objHdrHq.Ma_Tinh_NNThue = "";
                objHdrHq.Ngay_CT = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                objHdrHq.Ngay_HT = DateTime.Now.ToString("dd/MM/yyyy");
                objHdrHq.Ma_CQThu = obj304.Data.Item[0].Ma_HQ_CQT;
                objHdrHq.PT_TT = "01";
                objHdrHq.Ma_NT = "VND";
                objHdrHq.Ty_Gia = double.Parse("1");
                objHdrHq.TTien = double.Parse(obj304.Data.Item[0].DuNo_TO);
                objHdrHq.TTien_NT = double.Parse(obj304.Data.Item[0].DuNo_TO);
                objHdrHq.TT_TThu = double.Parse(obj304.Data.Item[0].DuNo_TO);
                //   objHdrHq.TK_KH_NH = obj304.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH;
                //   objHdrHq.TEN_KH_NH = obj304.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH;
                objHdrHq.TK_GL_NH = "";

                objHdrHq.Trang_Thai = "05";
                //'doan nay lien quan den tinh phi
                //'chua thuc hien bao h lam thuc thi tinh
                objHdrHq.PT_TINHPHI = "O";
                objHdrHq.PHI_GD = "0";
                objHdrHq.PHI_VAT = "0";
                objHdrHq.PHI_GD2 = "0";
                objHdrHq.PHI_VAT2 = "0";
                objHdrHq.TT_CTHUE = 0;

                objHdrHq.LOAI_TT = "";// '    //Chuyen xuong HDR
                objHdrHq.MA_NTK = obj304.Data.Item[0].Ma_NTK;
                objHdrHq.MA_HQ = obj304.Data.Item[0].Ma_HQ;

                //objHdrHq.MA_HQ = obj304.Data.ThongTinChungTu.Ma_HQ_PH;
                objHdrHq.TEN_HQ = obj304.Data.Item[0].Ten_HQ;
                objHdrHq.Ma_hq_ph = obj304.Data.Item[0].Ma_HQ_PH;
                objHdrHq.TEN_HQ_PH = obj304.Data.Item[0].Ten_HQ_PH;
                objHdrHq.Ten_cqthu = obj304.Data.Item[0].Ten_HQ_PH;
                objHdrHq.Kenh_CT = "HQNHOTHU";
                objHdrHq.SAN_PHAM = "XXX";// '$('#txtSanPham').val();
                objHdrHq.TT_CITAD = "1";
                //objHdrHq.SO_CMND = obj304.Data.ThongTinGiaoDich.NguoiNopTien.So_CMT;
                objHdrHq.SO_FONE = "";
                objHdrHq.LOAI_CTU = "T";
                //objHdrHq.REMARKS = "";
                //  objHdrHq.Ten_cqthu = Business_HQ.HQ247.daCTUHQ304.getTenHQ_CQT(obj304.Data.ThongTinChungTu.Ma_HQ_CQT, obj304.Data.ThongTinChungTu.Ma_HQ_PH);
                objHdrHq.So_TK = "";
                objHdrHq.So_CT_NH = "";
                // objHdrHq.Ghi_chu = obj304.Data.ThongTinChungTu.DienGiai;
                objHdrHq.MA_CUC = obj304.Data.Item[0].Ma_Cuc;
                //objHdrHq.TEN_CUC = "";
                //objHdrHq.REMARKS = obj304.Data.ThongTinChungTu.DienGiai;
                //objHdrHq.DIENGIAI_HQ = obj304.Data.ThongTinChungTu.DienGiai;
                objHdrHq.Ten_kb = obj304.Data.Item[0].Ten_KB;
                objHdrHq.Ma_NV = int.Parse(Ma_AccountHQ247);
                objHdrHq.KyHieu_CT = Business_HQ.HaiQuan.HaiQuanController.GetKyHieuChungTu(objHdrHq.SHKB, objHdrHq.Ma_NV.ToString(), "01");
                objHdrHq.Ngay_KB = int.Parse(Business_HQ.HaiQuan.HaiQuanController.GetNgayKhoBac(objHdrHq.SHKB).ToString());
                objHdrHq.NGAY_BAONO = DateTime.Now;
                string strTT_CUTOFFTIME = Business_HQ.CTuCommon.GetValue_THAMSOHT("TT_CUTOFFTIME");
                DateTime ngay_tiep_theo = DateTime.Now.AddDays(1);
                while (true)
                {
                    DataTable dtNgayNghi = DataAccess.ExecuteReturnDataSet("SELECT ngay_nghi FROM tcs_dm_ngaynghi WHERE ngay_nghi = to_date('" + ngay_tiep_theo.ToString("dd/MM/yyyy") + "', 'dd/MM/yyyy')", CommandType.Text).Tables[0];
                    if (dtNgayNghi.Rows.Count > 0)
                        ngay_tiep_theo = ngay_tiep_theo.AddDays(1);
                    else
                        break;
                }
                objHdrHq.NGAY_BAOCO = ngay_tiep_theo;
                objHdrHq.Ma_DThu = obj304.Data.Item[0].Ma_KB;
                String sSo_BT = String.Empty;
                String sNam_KB = "";
                String sThang_KB = "";
                sNam_KB = objHdrHq.Ngay_KB.ToString().Substring(2, 2);
                sThang_KB = objHdrHq.Ngay_KB.ToString().Substring(4, 2);
                objHdrHq.So_CT = sNam_KB + sThang_KB + Business_HQ.CTuCommon.Get_SoCT();
                string[] sday = obj304.Data.Item[0].Ngay_DK.Split('-');
                try
                {
                    objHdrHq.Ngay_TK = sday[2] + "/" + sday[1] + "/" + sday[0];
                }
                catch
                {
                    objHdrHq.Ngay_TK = "";
                }
                int vintTTButToan = 0;
                foreach (CustomsV3.MSG.MSG201.ItemData objGNT_CT in obj304.Data.Item)
                    foreach (CustomsV3.MSG.MSG201.CT_No objToKhai_CT in objGNT_CT.CT_No)
                    {
                        Business_HQ.NewChungTu.infChungTuDTL objDTL = new Business_HQ.NewChungTu.infChungTuDTL();
                        objDTL.SO_TK = objGNT_CT.So_TK;
                        objDTL.LH_XNK = objGNT_CT.Ma_LH;
                        objDTL.SAC_THUE = objToKhai_CT.LoaiThue;
                        objDTL.Ma_TMuc = objToKhai_CT.TieuMuc;
                        objDTL.SoTien = double.Parse(objToKhai_CT.DuNo);
                        objDTL.SoTien_NT = double.Parse(objToKhai_CT.DuNo);
                        objDTL.Noi_Dung = Business_HQ.Common.mdlCommon.Get_TenMTM(objToKhai_CT.TieuMuc);
                        objDTL.MA_LT = objGNT_CT.Ma_LT;
                        objDTL.Ma_Chuong = objGNT_CT.Ma_Chuong;
                        vintTTButToan++;
                        objDTL.TT_BTOAN = vintTTButToan.ToString();
                        objDTL.MA_HQ = objGNT_CT.Ma_HQ;
                        if (objHdrHq.So_TK.Length <= 0)
                            objHdrHq.So_TK = objGNT_CT.So_TK;
                        else
                        {
                            if (objHdrHq.So_TK.IndexOf(objGNT_CT.So_TK) < 0)
                                objHdrHq.So_TK += ";" + objGNT_CT.So_TK;
                        }
                        //vi ko co ngay to khai nen tam dien them 01/01 cong nam to khai
                        objDTL.NGAY_TK = objGNT_CT.Ngay_DK;
                        listDTL.Add(objDTL);
                    }
                return mdlCommon.TCS_HQ247_OK;
            }
            catch (Exception ex)
            { }
            return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK;
        }
        public static void SetObjectToDoiChieuNHHQ(string pv_strSHKB, string pv_strSoCT, string pv_strSoBT, string pv_strMaNV,
                                        string pv_strType, ref Business_HQ.infDoiChieuNHHQ_HDR hdr, ref Business_HQ.infDoiChieuNHHQ_DTL[] dtls,
                                        string pv_strMaDT = "", string pv_strErroCodeHQ = "", string pv_strTransactionType = "",
                                        string pv_strNgayKB = "", string pv_strSo_TN_CT = "", string pv_strNgay_TN_CT = "", bool p_flagBL = false)
        {
            string strSender_Code = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
            string strSender_Name = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Name").ToString();

            DataTable hdrBL = new DataTable();
            DataTable dtlsBL = new DataTable();
            Business_HQ.infDoiChieuNHHQ_DTL objDTL = null;
            List<Business_HQ.infDoiChieuNHHQ_DTL> lstdtl = new List<Business_HQ.infDoiChieuNHHQ_DTL>();
            int k = 0;
            try
            {
                //'Lay thong tin ChungTu
                if (pv_strType == "CTU")
                {
                    hdrBL = daCTUHQ201.SelectCT_HDR(pv_strSoCT).Tables[0];
                    dtlsBL = daCTUHQ201.SelectCT_DTL(pv_strSoCT).Tables[0];
                }
                else
                {
                    return;
                }
                if (hdrBL.Rows.Count > 0)
                {
                    hdr.shkb = hdrBL.Rows[0]["shkb"].ToString();
                    hdr.ngay_kb = long.Parse(hdrBL.Rows[0]["ngay_kb"].ToString());
                    hdr.so_bt = long.Parse(hdrBL.Rows[0]["so_bt"].ToString());
                    hdr.kyhieu_ct = hdrBL.Rows[0]["kyhieu_ct"].ToString();
                    hdr.so_ct = hdrBL.Rows[0]["so_ct"].ToString();
                    hdr.ngay_ct = long.Parse(hdrBL.Rows[0]["ngay_ct"].ToString());
                    hdr.ma_nnthue = hdrBL.Rows[0]["ma_nnthue"].ToString();
                    hdr.ten_nnthue = hdrBL.Rows[0]["ten_nnthue"].ToString();
                    hdr.ma_cqthu = hdrBL.Rows[0]["ma_cqthu"].ToString();
                    hdr.ma_hq_cqt = hdrBL.Rows[0]["ma_cqthu"].ToString();
                    hdr.so_tk = hdrBL.Rows[0]["so_tk"].ToString();
                    hdr.ttien = long.Parse(hdrBL.Rows[0]["ttien"].ToString());
                    hdr.ttien_nt = long.Parse(hdrBL.Rows[0]["ttien_nt"].ToString());
                    hdr.trang_thai = hdrBL.Rows[0]["trang_thai"].ToString();
                    hdr.ly_do_huy = hdrBL.Rows[0]["ly_do_huy"].ToString();
                    hdr.ma_nt = hdrBL.Rows[0]["ma_nt"].ToString();
                    hdr.ten_nh_ph = strSender_Name;
                    if (hdrBL.Columns.Contains("ma_dv_dd"))
                        hdr.ma_dv_dd = hdrBL.Rows[0]["ma_dv_dd"].ToString();
                    if (hdrBL.Columns.Contains("ten_nh_b"))
                        hdr.ten_nh_th = hdrBL.Rows[0]["ten_nh_b"].ToString();
                    if (hdrBL.Columns.Contains("ty_gia"))
                        hdr.ty_gia = hdrBL.Rows[0]["ty_gia"].ToString();
                    if (hdrBL.Columns.Contains("ttien_nt"))
                        hdr.ttien_nt = long.Parse(hdrBL.Rows[0]["ttien_nt"].ToString());
                    if (hdrBL.Columns.Contains("ten_dv_dd"))
                        hdr.ten_dv_dd = hdrBL.Rows[0]["ten_dv_dd"].ToString();
                    if (hdrBL.Columns.Contains("ngay_hdon"))
                    { hdr.ngay_hd = "to_date('" + hdrBL.Rows[0]["ngay_hdon"].ToString() + "','dd/mm/yyyy')"; }
                    else { hdr.ngay_hd = "''"; }
                    if (hdrBL.Columns.Contains("hoa_don"))
                        hdr.so_hd = hdrBL.Rows[0]["hoa_don"].ToString();
                    if (!hdrBL.Rows[0]["ngay_tk"].ToString().Equals(""))
                        hdr.ngay_tk = "to_date('" + hdrBL.Rows[0]["ngay_tk"].ToString() + "','dd/mm/yyyy')";
                    else
                        hdr.ngay_tk = "''";
                    if (hdrBL.Columns.Contains("lh_xnk"))
                        hdr.lhxnk = hdrBL.Rows[0]["lh_xnk"].ToString();
                    if (hdrBL.Columns.Contains("ma_nv"))
                        hdr.ma_nv = hdrBL.Rows[0]["ma_nv"].ToString();
                    if (hdrBL.Columns.Contains("lhxnk"))
                        hdr.lhxnk = hdrBL.Rows[0]["lhxnk"].ToString();
                    if (hdrBL.Columns.Contains("vt_lhxnk"))
                        hdr.vt_lhxnk = hdrBL.Rows[0]["vt_lhxnk"].ToString();
                    if (hdrBL.Columns.Contains("ten_lhxnk"))
                        hdr.ten_lhxnk = hdrBL.Rows[0]["ten_lhxnk"].ToString();
                    if (hdrBL.Columns.Contains("tk_ns"))
                        hdr.tk_ns = hdrBL.Rows[0]["tk_ns"].ToString();
                    if (hdrBL.Columns.Contains("ten_tk_ns"))
                        hdr.ten_tk_ns = hdrBL.Rows[0]["ten_tk_ns"].ToString();
                    if (hdrBL.Columns.Contains("so_bl"))
                        hdr.so_bl = hdrBL.Rows[0]["so_bl"].ToString();
                    hdr.ngay_bl_batdau = "''";
                    if (hdrBL.Columns.Contains("ngay_batdau"))
                        if (!hdrBL.Rows[0]["ngay_batdau"].ToString().Equals(""))
                            hdr.ngay_bl_batdau = "to_date('" + hdrBL.Rows[0]["ngay_batdau"].ToString() + "','dd/mm/yyyy')";
                    hdr.ngay_bl_ketthuc = "''";
                    if (hdrBL.Columns.Contains("ngay_ketthuc"))
                        if (!hdrBL.Rows[0]["ngay_ketthuc"].ToString().Equals(""))
                            hdr.ngay_bl_ketthuc = "to_date('" + hdrBL.Rows[0]["ngay_ketthuc"].ToString() + "','dd/mm/yyyy')";
                    if (hdrBL.Columns.Contains("songay_bl"))
                        hdr.songay_bl = int.Parse(hdrBL.Rows[0]["songay_bl"].ToString());
                    if (hdrBL.Columns.Contains("kieu_bl"))
                        hdr.kieu_bl = hdrBL.Rows[0]["kieu_bl"].ToString();
                    if (hdrBL.Columns.Contains("dien_giai"))
                        hdr.dien_giai = hdrBL.Rows[0]["dien_giai"].ToString();
                    if (hdrBL.Columns.Contains("ma_hq_ph"))
                        hdr.ma_hq_ph = hdrBL.Rows[0]["ma_hq_ph"].ToString();
                    if (hdrBL.Columns.Contains("ten_hq_ph"))
                        hdr.ten_hq_ph = hdrBL.Rows[0]["ten_hq_ph"].ToString();
                    if (hdrBL.Columns.Contains("ten_hq"))
                        hdr.ten_hq = hdrBL.Rows[0]["ten_hq"].ToString();
                    if (hdrBL.Columns.Contains("ma_hq"))
                        hdr.ma_hq = hdrBL.Rows[0]["ma_hq"].ToString();
                    if (hdrBL.Columns.Contains("ma_loaitien"))
                        hdr.ma_loaitien = hdrBL.Rows[0]["ma_loaitien"].ToString();
                    if (hdrBL.Columns.Contains("ma_hq_cqt"))
                        hdr.ma_hq_cqt = hdrBL.Rows[0]["ma_hq_cqt"].ToString();
                    if (hdrBL.Columns.Contains("so_bt_hq"))
                        if (!hdrBL.Rows[0]["so_bt_hq"].ToString().Trim().Equals(""))
                            hdr.so_bt_hq = long.Parse(hdrBL.Rows[0]["so_bt_hq"].ToString());
                    if (hdrBL.Columns.Contains("ma_ntk"))
                        if (!hdrBL.Rows[0]["ma_ntk"].ToString().Trim().Equals(""))
                            hdr.ma_ntk = hdrBL.Rows[0]["ma_ntk"].ToString();
                    hdr.error_code_hq = pv_strErroCodeHQ;
                    hdr.transaction_type = pv_strTransactionType;
                    if (pv_strErroCodeHQ == "0")
                        hdr.accept_yn = "Y";
                    else
                        hdr.accept_yn = "N";
                    //hdr.ten_kb = Globals.RemoveSign4VietnameseString(mdlCommon.Get_TenKhoBac(hdr.shkb));
                    hdr.ten_kb = hdrBL.Rows[0]["ten_kb"].ToString();
                    hdr.ten_cqthu = hdrBL.Rows[0]["ten_cqthu"].ToString();
                    hdr.ngay_bc = "sysdate";
                    hdr.ngay_bn = "sysdate";
                    hdr.parent_transaction_id = "";
                    hdr.tk_ns_hq = "";
                    hdr.so_tn_ct = pv_strSo_TN_CT;
                    hdr.ngay_tn_ct = pv_strNgay_TN_CT;
                    if (hdrBL.Columns.Contains("ngay_baoco"))
                        hdr.ngay_bc = "to_date('" + hdrBL.Rows[0]["ngay_baoco"].ToString() + "','dd/mm/yyyy')";
                    if (hdrBL.Columns.Contains("ngay_baono"))
                        hdr.ngay_bn = "to_date('" + hdrBL.Rows[0]["ngay_baono"].ToString() + "','dd/mm/yyyy')";
                    if (hdrBL.Columns.Contains("ma_nh_a"))
                        hdr.ma_nh_ph = hdrBL.Rows[0]["ma_nh_a"].ToString();
                    if (hdrBL.Columns.Contains("ma_nh_b"))
                        hdr.ma_nh_th = hdrBL.Rows[0]["ma_nh_b"].ToString();
                    if (hdrBL.Columns.Contains("tk_co"))
                        hdr.tkkb_ct = hdrBL.Rows[0]["tk_co"].ToString();
                    hdr.so_tn_ct = pv_strSo_TN_CT;
                    hdr.ngay_tn_ct = pv_strNgay_TN_CT;
                    if (hdrBL.Columns.Contains("ngay_hdon"))
                        hdr.ngay_hd = "to_date('" + hdrBL.Rows[0]["ngay_hdon"].ToString() + "','dd/mm/yyyy')";
                    else
                        hdr.ngay_hd = "''";
                    if (hdrBL.Columns.Contains("hoa_don"))
                        hdr.so_hd = hdrBL.Rows[0]["hoa_don"].ToString();
                    if (hdrBL.Columns.Contains("ngay_vtd"))
                        hdr.ngay_vtd = "to_date('" + hdrBL.Rows[0]["ngay_vtd"].ToString() + "','dd/mm/yyyy')";
                    else
                        hdr.ngay_vtd = "''";

                    if (hdrBL.Columns.Contains("vt_don"))
                        hdr.vtd = hdrBL.Rows[0]["vt_don"].ToString();

                    if (hdrBL.Columns.Contains("ngay_vtd2"))
                        hdr.ngay_vtd2 = "to_date('" + hdrBL.Rows[0]["ngay_vtd2"].ToString() + "','dd/mm/yyyy')";
                    else
                        hdr.ngay_vtd2 = "''";
                    if (hdrBL.Columns.Contains("vt_don2"))
                        hdr.vtd2 = hdrBL.Rows[0]["vt_don2"].ToString();
                    hdr.ngay_vtd3 = "''";
                    if (hdrBL.Columns.Contains("ngay_vtd3"))
                        hdr.ngay_vtd3 = "to_date('" + hdrBL.Rows[0]["ngay_vtd3"].ToString() + "','dd/mm/yyyy')";
                    if (hdrBL.Columns.Contains("vt_don3"))
                        hdr.vtd3 = hdrBL.Rows[0]["vt_don3"].ToString();
                    hdr.ngay_vtd4 = "''";
                    if (hdrBL.Columns.Contains("ngay_vtd4"))
                        hdr.ngay_vtd4 = "to_date('" + hdrBL.Rows[0]["ngay_vtd4"].ToString() + "','dd/mm/yyyy')";
                    if (hdrBL.Columns.Contains("vt_don4"))
                        hdr.vtd4 = hdrBL.Rows[0]["vt_don4"].ToString();
                    hdr.ngay_vtd5 = "''";
                    if (hdrBL.Columns.Contains("ngay_vtd5"))
                        hdr.ngay_vtd5 = "to_date('" + hdrBL.Rows[0]["ngay_vtd5"].ToString() + "','dd/mm/yyyy')";
                    if (hdrBL.Columns.Contains("vt_don5"))
                        hdr.vtd5 = hdrBL.Rows[0]["vt_don5"].ToString();
                    if (hdrBL.Columns.Contains("DIENGIAI_HQ"))
                        hdr.dien_giai = hdrBL.Rows[0]["DIENGIAI_HQ"].ToString();
                    hdr.KENHGD = "NHOTHU";
                }
                if (dtlsBL.Rows.Count > 0)
                {
                    foreach (DataRow row in dtlsBL.Rows)
                    {
                        try
                        {
                            objDTL = new Business_HQ.infDoiChieuNHHQ_DTL();
                            objDTL.ma_cap = row["ma_cap"].ToString();
                            objDTL.ma_chuong = row["ma_chuong"].ToString();
                            objDTL.noi_dung = row["noi_dung"].ToString();
                            objDTL.ma_dp = row["ma_dp"].ToString();
                            objDTL.ky_thue = row["ky_thue"].ToString();
                            objDTL.sotien = long.Parse(row["sotien"].ToString());

                            if (dtlsBL.Columns.Contains("ma_quy"))
                                objDTL.ma_quy = row["ma_quy"].ToString();
                            if (dtlsBL.Columns.Contains("ma_nkt"))
                                objDTL.ma_nkt = row["ma_nkt"].ToString();

                            if (dtlsBL.Columns.Contains("ma_tmuc"))
                                objDTL.ma_ndkt = row["ma_tmuc"].ToString();
                            if (dtlsBL.Columns.Contains("ma_nkt_cha"))
                                objDTL.ma_nkt_cha = row["ma_nkt_cha"].ToString();
                            if (dtlsBL.Columns.Contains("ma_ndkt"))
                                objDTL.ma_ndkt = row["ma_ndkt"].ToString();
                            if (dtlsBL.Columns.Contains("ma_khoan"))
                                objDTL.ma_nkt = row["ma_khoan"].ToString();

                            if (dtlsBL.Columns.Contains("ma_ndkt_cha"))
                                objDTL.ma_ndkt_cha = row["ma_ndkt_cha"].ToString();

                            if (dtlsBL.Columns.Contains("ma_tlpc"))
                                if (!row["ma_tlpc"].ToString().Trim().Equals(""))
                                    objDTL.ma_tlpc = int.Parse(row["ma_tlpc"].ToString());

                            if (dtlsBL.Columns.Contains("ma_khtk"))
                                objDTL.ma_khtk = row["ma_khtk"].ToString();

                            if (dtlsBL.Columns.Contains("SoTien_NT"))
                                objDTL.sotien_nt = long.Parse(row["SoTien_NT"].ToString());
                            lstdtl.Add(objDTL);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    dtls = lstdtl.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //--------------------------------------------------------------------------------------------------------------------
        //Nhamlt_20180330
        private string BuildEcomForPayment_V20(MSG201 obj201)
        {
            string v_strRetRemark = "";
            //if (listDTL.Count > 1)
            //{
            for (int i = 0; i < obj201.Data.Item[0].CT_No.Count; i++)
            {

                //Moi truong co do dai 200
                //Noi dung kinh te- mã tiểu mục
                v_strRetRemark += V_Substring(obj201.Data.Item[0].CT_No[i].TieuMuc, 4);// listDTL.ElementAt(i).Ma_TMuc, 4);
                //'mã chương
                v_strRetRemark += "~" + V_Substring(obj201.Data.Item[0].Ma_Chuong, 3);
                //Số tiền
                string sotien = obj201.Data.Item[0].CT_No[i].DuNo;
                v_strRetRemark += "~" + V_Substring(VBOracleLib.Globals.Format_Number(sotien, ".").Replace(".", ""), 20);
                //'Nội dung
                v_strRetRemark += "~" + V_Substring(Business_HQ.Common.mdlCommon.Get_TenMTM(obj201.Data.Item[0].CT_No[i].TieuMuc), 100);// listDTL.ElementAt(i).Noi_Dung, 100);
                //'Số tờ khai
                v_strRetRemark += "~" + V_Substring(obj201.Data.Item[0].So_TK, 3);// objHdrHq.So_TK, 30);
                string ngay_tk = "";
                if (obj201.Data.Item[0].Ngay_DK != null)
                {
                    ngay_tk = Business_HQ.Common.mdlCommon.ConvertStringToDate(obj201.Data.Item[0].Ngay_DK, "YYYY-MM-DD").ToString("dd-MM-yyyy");// DateTime.ParseExact(listDTL.ElementAt(i).NGAY_TK, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MM-yyyy");
                }
                v_strRetRemark += "~" + V_Substring(ngay_tk, 10);
                //if (i != 0)
                //{
                v_strRetRemark += "####";
                //}

            }

            v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark);


            //}

            return v_strRetRemark;
        }
        //Nhamlt_20180330
        //Nhamlt_20180330
        private string V_Substring(String s, int length)
        {
            if (s != null && s.Length > length)
            {
                s = s.Substring(0, length);
            }
            return s;

        }

        private static string BuildRemarkForPayment(string strSOHS, MSG201 obj201, string ngayNT)
        {
            //Sua theo thong tu 2936 cua TCHQ_20180531
            string v_strRetRemark = "";
            try
            {
                string Ma_HQ = "";
                string So_TK = "";
                string LH_XNK = "";
                string v_strRetRemarkDetail = "";
                string Ma_chuong = "";
                string MA_LT = "";
                string strMST = obj201.Data.Item[0].Ma_DV;
                string strChiNhanhMST = "";
                if (strMST.Length == 13)
                {
                    strChiNhanhMST = strMST.Substring(10, 3);
                    strMST = strMST.Substring(0, 10) + "-" + strChiNhanhMST;
                }
                foreach (CustomsV3.MSG.MSG201.CT_No objDtl in obj201.Data.Item[0].CT_No)
                {
                    v_strRetRemarkDetail += "(";
                    v_strRetRemarkDetail += "TM" + objDtl.TieuMuc;
                    v_strRetRemarkDetail += "+" + getMapSTDienGiai(objDtl.LoaiThue);
                    string sotien = objDtl.DuNo.ToString();
                    v_strRetRemarkDetail += "+T" + VBOracleLib.Globals.Format_Number(sotien, ".").Replace(".", "");
                    v_strRetRemarkDetail += ")";
                    So_TK = obj201.Data.Item[0].So_TK;

                    LH_XNK = obj201.Data.Item[0].Ma_LH;// objDtl.LH_XNK;
                    Ma_HQ = obj201.Data.Item[0].Ma_HQ;// objDtl.MA_HQ;
                    Ma_chuong = obj201.Data.Item[0].Ma_Chuong;// objDtl.Ma_Chuong;
                    MA_LT = obj201.Data.Item[0].Ma_LT;// objDtl.MA_LT;
                }
                v_strRetRemark += "HQDT+ID";
                v_strRetRemark += strSOHS;
                v_strRetRemark += "+MST" + strMST;// objHdrHq.Ma_NNThue;
                v_strRetRemark += "+C" + Ma_chuong;
                //v_strRetRemark += "+NNT" + GetNgayKhoBac(obj201.Data.Item[0].Ma_KB).ToString();
                v_strRetRemark += "+NNT" + ngayNT;
                v_strRetRemark += "+HQ" + obj201.Data.Item[0].Ma_HQ_PH;// objHdrHq.Ma_hq_ph;
                v_strRetRemark += "-" + Ma_HQ;
                v_strRetRemark += "-" + obj201.Data.Item[0].Ma_HQ_CQT;
                v_strRetRemark += "+TK" + So_TK;
                v_strRetRemark += "+NDK" + Business_HQ.Common.mdlCommon.ConvertStringToDate(obj201.Data.Item[0].Ngay_DK, "YYYY-MM-DD").ToString("ddMMyyyy").Replace("/", "");
                v_strRetRemark += "+LH" + LH_XNK;
                v_strRetRemark += "+NTK" + obj201.Data.Item[0].Ma_NTK;// objHdrHq.MA_NTK;
                v_strRetRemark += "+LT" + MA_LT;
                v_strRetRemark += "+KB" + obj201.Data.Item[0].Ma_KB;// objHdrHq.SHKB;
                v_strRetRemark += "+TKNS" + obj201.Data.Item[0].TKKB;// objHdrHq.TK_Co;
                v_strRetRemark += "+VND";
                v_strRetRemark += v_strRetRemarkDetail;
                v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark);


            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog(ex, "", "");
                throw ex;
            }
            return v_strRetRemark;
        }
        private static string getMapSTDienGiai(string sacthue)
        {
            //XK	Thuế xuất khẩu	ST1
            //NK	Thuế nhập khẩu	ST2
            //VA	Thuế giá trị gia tăng	ST3
            //TD	Thuế tiêu thụ đặc biệt	ST4
            //TV	Thuế tự vệ chống bán phá giá	ST5
            //MT	Thuế bảo vệ môi trường	ST6
            //TC	Thuế chống trợ cấp	ST7
            //PB	Thuế chống phân biệt đối xử	ST8
            //PG	Thuế chống bán phá giá	ST10
            //KH	Thuế khác	ST9
            switch (sacthue)
            {
                case "XK": return "ST1";
                case "NK": return "ST2";
                case "VA": return "ST3";
                case "TD": return "ST4";
                case "TV": return "ST5";
                case "MT": return "ST6";
                case "TC": return "ST7";
                case "PB": return "ST8";
                case "PG": return "ST10";
                case "KH": return "ST9";
                case "LP": return "ST11";
            }
            return "";
        }
        //Nhamlt_20180330  
        private string BuildRemarkForPayment_V20(MSG201 obj201)
        {
            string v_strRetRemark = "";
            string strLHXNK = obj201.Data.Item[0].Ma_LH;

            v_strRetRemark += "LH:" + strLHXNK;
            v_strRetRemark += ".DNCT " + obj201.Data.Item[0].Ten_KB;// objHdrHq.Ghi_chu;
            v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark);
            return v_strRetRemark;
        }
        private static string GetNgayKhoBac(string strSHKB)
        {
            string lRet = "";
            string sSQL = "";
            try
            {
                sSQL = " SELECT TO_CHAR (TO_DATE (giatri_ts, 'DD/MM/YYYY'), 'DDMMYYYY') FROM tcs_thamso ";
                sSQL += " WHERE ten_ts = 'NGAY_LV' AND ma_dthu = (SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS = '" + strSHKB + "')";

                DataSet ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            lRet = ds.Tables[0].Rows[0][0].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lRet = DateTime.Now.ToString("ddMMyyyy");
            }

            return lRet;
        }
        //--------------------------------------------------------------------------------------------------------------------
        private static Business_HQ.NewChungTu.infChungTuHDR loadHDR(string strSoCT)
        {
            Business_HQ.NewChungTu.infChungTuHDR objHdrHq = new Business_HQ.NewChungTu.infChungTuHDR();
            try
            {
                string SQL = "select a.Ngay_KB,a.Ma_Tinh,a.Ten_NH_B,a.ma_nh_b,a.ma_nv,a.Ma_LThue from tcs_ctu_hq_nhothu_hdr a where a.so_ct='" + strSoCT + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(SQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            objHdrHq.Ngay_KB = int.Parse(ds.Tables[0].Rows[0]["Ngay_KB"].ToString());
                            objHdrHq.Ten_NH_B = ds.Tables[0].Rows[0]["Ten_NH_B"].ToString();
                            objHdrHq.MA_NH_B = ds.Tables[0].Rows[0]["ma_nh_b"].ToString();
                            objHdrHq.Ma_NV = int.Parse(ds.Tables[0].Rows[0]["ma_nv"].ToString());
                            objHdrHq.Ma_LThue = ds.Tables[0].Rows[0]["Ma_LThue"].ToString();
                            objHdrHq.So_CT = strSoCT;
                        }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objHdrHq;
        }
        public string InsertDoiChieu201(string str201ID)
        {
            string v_strTransaction_id = "";
            string pv_so_tn_ct = "";
            string pv_ngay_tn_ct = "";
            string strso_ct = "";
            String strErrorNum = "0";
            String strErrorMes = "";
            string valid = "";
            string trangthai = "";
            string strSQL = "";
            //strSQL ="Select * from TCS_DCHIEU_NHHQ_HDR where so_ct='' and transaction_type='M47' and transaction_id='' and so_tn_ct='' and ngay_tn_ct=''"
            strSQL = "SELECT * FROM TCS_HQ247_201 WHERE ID='" + str201ID + "'";
            DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        trangthai = ds.Tables[0].Rows[0]["trangthai"].ToString();
                        strso_ct = ds.Tables[0].Rows[0]["SO_CTU"].ToString();
                        pv_so_tn_ct = ds.Tables[0].Rows[0]["so_tn_ct"].ToString();
                        pv_ngay_tn_ct = ds.Tables[0].Rows[0]["ngay_tn_ct"].ToString();
                        v_strTransaction_id = ds.Tables[0].Rows[0]["msg_id301"].ToString();
                        valid = "0";
                    }
            if (valid == "")
            {
                return "Xử lý đối chiếu lỗi, không tìm thấy chứng từ [" + str201ID + "]";
            }
            if (trangthai != "04")
            {
                return "Xử lý đối chiếu lỗi, chứng từ có trạng thái không hợp lệ.";
            }
            if (strso_ct.Trim().Length <= 0)
                return "Xử lý lỗi: so_ctu trong TCS_HQ247_201 trống, không insert được đối chiếu";
            if (pv_so_tn_ct.Trim().Length <= 0)
                return "Xử lý lỗi: SO_TN_CT trong TCS_HQ247_201 trống, không insert được đối chiếu";
            if (v_strTransaction_id.Trim().Length <= 0)
                return "Xử lý lỗi: MSG_ID301 trong TCS_HQ247_201 trống, không insert được đối chiếu";
            if (pv_ngay_tn_ct.Trim().Length <= 0)
                return "Xử lý lỗi: NGAY_TN_CT trong TCS_HQ247_201 trống, không insert được đối chiếu";
            MSG201 obj = new MSG201();
            obj = obj.MSGToObject(ds.Tables[0].Rows[0]["MSG_CONTENT"].ToString());
            string kq = "";
            valid = "";
            strSQL = "Select * from TCS_DCHIEU_NHHQ_HDR where so_ct='" + strso_ct + "'";// and transaction_type='M47' and transaction_id='' and so_tn_ct='' and ngay_tn_ct=''"
            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        valid = "0";
                    }
            if (valid != "")
            {
                return "Không insert được đối chiếu, chứng từ[" + str201ID + "] đã tồn tại trong bảng đối chiếu.";
            }
            if (pv_so_tn_ct.Trim().Length > 0)
            {
                try
                {
                    Business_HQ.infDoiChieuNHHQ_HDR hdr = new Business_HQ.infDoiChieuNHHQ_HDR();
                    Business_HQ.infDoiChieuNHHQ_DTL[] dtls = null;
                    SetObjectToDoiChieuNHHQ(obj.Data.Item[0].Ma_KB, strso_ct, "", Ma_AccountHQ247, "CTU", ref hdr, ref dtls, obj.Data.Item[0].Ma_KB, strErrorNum, mdlCommon.TransactionHQ_Type.M47.ToString(), "", pv_so_tn_ct, pv_ngay_tn_ct);
                    hdr.transaction_id = v_strTransaction_id;
                    Business_HQ.ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, "1", strErrorMes);
                    kq = "Xử lý dối chiếu thành công.";
                }
                catch (Exception ex)
                {
                    kq = ex.Message;
                }
            }
            return kq;
        }
    }
}
