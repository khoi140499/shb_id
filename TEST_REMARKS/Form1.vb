﻿Imports Business


Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim content As String = RichTextBox1.Text
        Dim result As String = ""

        '3000 thi la citad 2.5
        If content.Length >= 1000 Then
            'doan nay em lam cho phan chuoi content ex
            content = content.Trim
            Dim oRet As ChungTuSP.infChungTuSP = Nothing

            Try
                Dim objEX As CitadContentEx_25.CitadContentEx_25 = New CitadContentEx_25.CitadContentEx_25()
                objEX = objEX.MSGToObject(content)
                Dim HDR As New ChungTuSP.infChungTuSP_HDR

                Dim STC As String = objEX.STC    'Số tham chiếu
                Dim SCT As String = objEX.SCT     'Số chứng từ
                Dim KCT As String = objEX.KCT   'Ký hiệu chứng từ
                Dim TNT As String = objEX.TNT     'Tên Người nộp thuế
                Dim DNT As String = objEX.DNT   'Địa chỉ người nộp thuế
                Dim MST As String = objEX.MST   'Mã số thuế
                Dim MDB As String = objEX.MDB   'Mã địa bàn hành chính
                Dim CQT As String = objEX.CQT   'Mã cơ quan thu
                Dim TCQ As String = objEX.TCQ   'Tên cơ quan thu
                Dim NNT As String = objEX.NNT   'Ngày nộp thuế
                Dim LTH As String = objEX.LTH     'Loại thuế 04 hai quam, <> 04 noi dia
                Dim TKN As String = objEX.TKN   ' Tài khoản nộp vào: 01: Nộp vào NSNN 02: TK tạm thu 03: TK thu hồi quỹ hoàn thuếGTGT()
                Dim KLN As String = objEX.KLN   ' Kết luận nộp của cơquan có thẩm quyền option 01

                HDR.SO_CT = SCT.Trim
                HDR.KYHIEU_CT = KCT.Trim
                HDR.TEN_NNTHUE = TNT.Trim
                HDR.DC_NNTHUE = DNT.Trim
                HDR.MA_NNTHUE = MST.Trim
                HDR.MA_DBHC = MDB.Trim
                HDR.MA_CQTHU = CQT.Trim
                HDR.TEN_CQTHU = TCQ.Trim
                HDR.NGAY_NTIEN = NNT.Trim
                HDR.MA_LTHUE = LTH.Trim
                HDR.CQ_QD = KLN.Trim

                Dim listDTL As New List(Of ChungTuSP.infChungTuSP_DTL)
                Dim DTL As ChungTuSP.infChungTuSP_DTL = Nothing
                For Each objDTL In objEX.VSTD
                    DTL = New ChungTuSP.infChungTuSP_DTL

                    DTL.SO_TK = objDTL.STK.Trim
                    DTL.Ma_Chuong = objDTL.MCH.Trim
                    DTL.Ma_TMuc = objDTL.MND.Trim
                    DTL.Noi_Dung = objDTL.NDN.Trim
                    If LTH.Equals("04") Then
                        DTL.NGAY_TK = objDTL.NTK.Trim
                    Else
                        DTL.Ky_Thue = objDTL.NTK.Trim
                    End If

                    DTL.SoTien = objDTL.STN.Replace(",", ".")
                    DTL.SoTien_NT = 0

                    listDTL.Add(DTL)
                Next

                oRet = New ChungTuSP.infChungTuSP(HDR, listDTL)

            Catch ex As Exception
                result = ex.Message
            End Try

            result = Newtonsoft.Json.JsonConvert.SerializeObject(oRet).ToString()

        End If

        If content.Length > 4 Then
            Dim lthue As String = content.Substring(0, 4)

            If lthue.ToUpper().Equals("HQDT") Then
                Dim arr() As String
                arr = content.Split("+")
                Dim ct_LTHUE As String = arr(0).ToString() 'loại thuế : HQDT
                Dim ct_ID As String = arr(1).ToString()    ' ID  : ID0
                Dim ct_MST As String = arr(2).ToString()    ' mã số thuế : MST0123456789
                Dim ct_C As String = arr(3).ToString()     ' chương : C152
                Dim ct_NNT As String = arr(4).ToString()   ' ngày nộp thuế  : NNT07052019
                Dim ct_HQ As String = arr(5).ToString()    ' hải quan : HQ18A1-18A1-2995100
                Dim ct_TK As String = arr(6).ToString()    ' tờ khai  : TK10223490175
                Dim ct_NDK As String = arr(7).ToString()   ' năm đăng ký, năm tờ khai : NDK21092018
                Dim ct_LH As String = arr(8).ToString()   ' mã loại hình xuất nhập khẩu : LHA12
                Dim ct_NTK As String = arr(9).ToString()   '  mã nhóm tài khoản
                Dim ct_LT As String = arr(10).ToString()   ' Mã loại tiền (Thuế, phí, phạt...) : LT1
                Dim ct_KB As String = arr(11).ToString()   ' kho bạc : KB1117
                Dim ct_TKNS As String = arr(12).ToString()  ' tài khoản thu NS: TKNS7111
                Dim ct_VND As String = arr(13).ToString()  ' nguyên tệ + chi tiết thuế : VND(TM1901+ST2+T9494179)(TM1901+ST2+T9494179)


                Dim ID As String = ct_ID.Substring(2, ct_ID.Length - 2)
                Dim MST As String = ct_MST.Substring(3, ct_MST.Length - 3)
                Dim C As String = ct_C.Substring(1, ct_C.Length - 1)
                Dim NNT As String = ct_NNT.Substring(3, ct_NNT.Length - 3)

                Dim arrHQ() As String
                arrHQ = ct_HQ.Split("-")
                Dim ctHQ_HQ As String = arrHQ(0).ToString()
                Dim HQ_PH As String = ctHQ_HQ.Substring(2, ctHQ_HQ.Length - 2)
                Dim HQ As String = arrHQ(1).ToString()
                Dim CQT As String = arrHQ(2).ToString()

                Dim TK As String = ct_TK.Substring(2, ct_TK.Length - 2)
                Dim NDK As String = ct_NDK.Substring(3, ct_NDK.Length - 3)
                Dim LH As String = ct_LH.Substring(2, ct_LH.Length - 2)
                Dim NTK As String = ct_NTK.Substring(3, ct_NTK.Length - 3)
                Dim LT As String = ct_LT.Substring(2, ct_LT.Length - 2)
                Dim KB As String = ct_KB.Substring(2, ct_KB.Length - 2)
                Dim TKNS As String = ct_TKNS.Substring(4, ct_TKNS.Length - 4)
                Dim VND As String = ct_VND.Substring(0, 3)


                result = ID & "\n" & MST & "\n" & C & "\n" & NNT & "\n" & HQ_PH & "\n" & HQ & "\n" & CQT & "\n" & TK & "\n" & NDK & "\n" & LH & "\n" & NTK & "\n" & LT & "\n" & KB & "\n" & TKNS & "\n" & VND

                'Phân tách phần chi tiết thuế
                Dim ctThue As String = ""
                Dim dIndex = content.IndexOf("(")
                If (dIndex > -1) Then
                    ctThue = content.Substring(dIndex, content.Length - dIndex) '(TM1901+ST2+T9494179)(TM1901+ST2+T9494179)
                End If

                Dim count As Integer = CountCharacter(ctThue, "(")
                If count > 0 Then
                    Dim arrCThue() As String
                    arrCThue = ctThue.Split(")")
                    Dim sl As Integer = arrCThue.Length
                    If arrCThue(sl - 1).Length <= 0 Then
                        sl = sl - 1
                    End If
                    For i = 0 To sl - 1
                        Dim ct_i As String = arrCThue(i).ToString().Replace("(", "").Replace(")", "")

                        Dim arrChiTiet() As String
                        arrChiTiet = ct_i.Split("+")
                        Dim ct_TM As String = arrChiTiet(0).ToString() 'tiểu mục
                        Dim ct_ST As String = arrChiTiet(1).ToString()  'sắc thuế
                        Dim ct_T As String = arrChiTiet(2).ToString()   ' số tiền

                        Dim TM As String = ct_TM.Substring(2, ct_TM.Length - 2)
                        Dim ST As String = ct_ST.Substring(2, ct_ST.Length - 2)
                        Dim T As String = ct_T.Substring(1, ct_T.Length - 1)

                        result &= "\n " & TM & " " & ST & " " & T
                    Next
                End If

            ElseIf lthue.ToUpper().Equals("NTDT") Then
                Dim arr() As String
                arr = content.Split("+")
                'Dim LTHUE As String = arr(0).ToString() 'loại thuế : NTDT
                Dim ct_KB As String = arr(1).ToString()    ' kho bạc - tên kho bạc : KB:0111-VP KBNN Ho Chi Minh
                Dim ct_NgayNT As String = arr(2).ToString()    ' ngày nộp thuế  : NgayNT:22072020
                Dim ct_MST As String = arr(3).ToString()     ' mã số thuế : MST:0100512273
                Dim ct_DBHC As String = arr(4).ToString()   ' địa bàn hành chính  : DBHC:777HH
                Dim ct_TKNS As String = arr(5).ToString()    ' tài khoản thu NS: TKNS:7111
                Dim ct_CQT As String = arr(6).ToString()    ' cơ quan thu  : CQT:1054218
                Dim ct_LThue As String = arr(7).ToString()   ' loai thue : LThue:01

                Dim KB As String = ct_KB.Substring(3, 4)
                Dim Ten_KB As String = ct_KB.Substring(8, ct_KB.Length - 8)
                Dim NgayNT As String = ct_NgayNT.Split(":")(1).ToString()
                Dim MST As String = ct_MST.Split(":")(1).ToString()
                Dim DBHC As String = ct_DBHC.Split(":")(1).ToString()
                Dim TKNS As String = ct_TKNS.Split(":")(1).ToString()
                Dim CQT As String = ct_CQT.Split(":")(1).ToString()
                Dim MaLThue As String = ct_LThue.Split(":")(1).ToString().Substring(0, 2)

                result = KB & "\n" & Ten_KB & "\n" & NgayNT & "\n" & MST & "\n" & DBHC & "\n" & TKNS & "\n" & CQT & "\n" & MaLThue & "\n"
                'Phân tách phần chi tiết thuế
                Dim ctThue As String = ""
                Dim dIndex = content.IndexOf("(")
                If (dIndex > -1) Then
                    ctThue = content.Substring(dIndex, content.Length - dIndex) '(C:174-TM:1704-KT:22/07/2020-ST:24000000-GChu:)
                End If

                Dim arrCThue() As String
                arrCThue = ctThue.Split(")")
                Dim sl As Integer = arrCThue.Length
                If arrCThue(sl - 1).Length <= 0 Then
                    sl = sl - 1
                End If
                For i = 0 To sl - 1
                    Dim ct_i As String = arrCThue(i).ToString().Replace("(", "").Replace(")", "")

                    Dim arrChiTiet() As String
                    arrChiTiet = ct_i.Split("-")
                    If arrChiTiet.Length < 4 Then
                    Else
                        Dim ct_C As String = arrChiTiet(0).ToString() 'chương
                        Dim ct_TM As String = arrChiTiet(1).ToString() 'tiểu mục
                        Dim ct_KT As String = arrChiTiet(2).ToString()  'kỳ thuế
                        Dim ct_T As String = arrChiTiet(3).ToString()   ' số tiền
                        Dim ct_GChu As String = arrChiTiet(4).ToString() 'ghi chú GChu:day la dau (xxx- xxx) aa

                        Dim C As String = ct_C.Split(":")(1).ToString()
                        Dim TM As String = ct_TM.Split(":")(1).ToString()
                        Dim KT As String = ct_KT.Split(":")(1).ToString()
                        Dim T As String = ct_T.Split(":")(1).ToString()
                        Dim GChu As String = ct_GChu.Substring(5, ct_GChu.Length - 5)
                        result &= "\n " & C & " " & TM & " " & KT & " " & T & " " & GChu

                    End If
                Next


            End If

        End If





        RichTextBox2.Text = result
    End Sub
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim vStrTranDate As String = "20210910"
        vStrTranDate = vStrTranDate.Substring(2, 6)
        Dim content As String = RichTextBox1.Text
        Dim result As String = ""
        If content.Contains("&amp;") Then
            content = content.Replace("&amp;", " ")
        End If
        content = content.Replace("&", " ")

        result = content

        RichTextBox2.Text = result

    End Sub
End Class
