﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Configuration;
using System.IO;
using System.Diagnostics;
namespace EtaxHQ247AutoProcess
{
    class Program
    {
        public static string logFile = ConfigurationSettings.AppSettings.Get("logfile.HQDT").ToString();

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("==START==");
            Process mobj_pro = Process.GetCurrentProcess();
            Process[] mobj_proList = Process.GetProcessesByName(mobj_pro.ProcessName);
            if (mobj_proList.Length > 1)
            {
                Console.WriteLine("Application already running in this computer!");
                Console.WriteLine("Press any Key to turn off this application!");
                Console.ReadKey();
            }
            else
            {
              
                Console.WriteLine("==START==");
                String message = null;
                int x = 0;
                int y = 0;
                int z = 0;
                string ngayDC = "";
                DateTime dtngayDC;
                string strGD = "";
                string strGDNhanDL = "";
                string strErrorNum = "";
                string strErrorMes = "";
                int intTimeDelay = 30000;
                try
                {
                    intTimeDelay = int.Parse(ConfigurationSettings.AppSettings.Get("HQ247.TimeDelay").ToString());
                    intTimeDelay = intTimeDelay * 1000;
                }
                catch (Exception ex)
                {
                    intTimeDelay = 30000;
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Load HQ247.TimeDelay " + ex.Message + "-" + ex.StackTrace);
                }
                do
                {
                    Console.WriteLine("Waiting...");
                    //writeLog("Main 1.");
                    try
                    {
                        HQ247Process obj = new HQ247Process();
                        //   string id = obj.getSoChungTuChuaXuLy();
                        List<string> strID = obj.getListSoChungTuChuaXuLy();
                        if (strID != null)
                        {
                            if (strID.Count <= 0)
                            {
                                Thread.Sleep(intTimeDelay);
                            }
                            while (strID.Count > 0)
                            {
                                string id = strID[0];
                                if (id.Length > 0)
                                {
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_304=" + id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_304=" + id);
                                    string MSG = obj.ProcessData(id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_304[" + id + "] MSG: " + MSG);
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_304[" + id + "] MSG: " + MSG);
                                }
                                strID.RemoveAt(0);
                            }
                        }
                        else
                        {
                            Thread.Sleep(intTimeDelay);
                        }
                        //doan nay danh cho thu ho
                       // writeLog("Main 2.");
                        strID = obj.getListSoChungTuChuaXuLy201();
                        if (strID != null)
                        {
                            while (strID.Count > 0)
                            {
                                string id = strID[0];
                                if (id.Length > 0)
                                {
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_201=" + id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_201=" + id);
                                    string MSG = obj.ProcessData201(id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_201[" + id + "] MSG: " + MSG);
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_201[" + id + "] MSG: " + MSG);
                                }
                                strID.RemoveAt(0);
                            }
                        }
                        //doan nay danh cho xu ly phan hoi 213 tu dong
                        //writeLog("Main 3.");
                        //delay 5s rooif moi xl 311
                        Thread.Sleep(5000);
                        strID = obj.getLIST311();
                        //writeLog("Main 4. ");
                        if (strID != null)
                        {
                            while (strID.Count > 0)
                            {
                                string id = strID[0];
                                //writeLog("Main 4.1: " + id);
                                if (id.Length > 0)
                                {
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_311=" + id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_311=" + id);
                                    HQ247XulyMSG.MSG_XULY311 objxl = new HQ247XulyMSG.MSG_XULY311();
                                    string MSG = objxl.ProcessData311(id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_311[" + id + "] MSG: " + MSG);
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_311[" + id + "] MSG: " + MSG);
                                }
                                strID.RemoveAt(0);
                            }
                        }
                       // writeLog("Main 5.");
                        //doan nay danh cho xu ly MSG 311 tu Internet Banking
                        string isAutoISBK = "N";
                        try
                        {
                            isAutoISBK = ConfigurationSettings.AppSettings.Get("CUSTOMS.AUTO311IB").ToString();
                        }
                        catch (Exception ex)
                        {
                            isAutoISBK = "N";
                        }
                        if (isAutoISBK != null && isAutoISBK.Equals("Y"))
                        {
                            strID = obj.getLIST311IB();
                            if (strID != null)
                            {
                                while (strID.Count > 0)
                                {
                                    string id = strID[0];
                                    if (id.Length > 0)
                                    {
                                        Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message from Internet Banking: ID_311=" + id);
                                        writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message from Internet Banking: ID_311=" + id);
                                        HQ247XulyMSG.MSG_XULY_311IB objxl = new HQ247XulyMSG.MSG_XULY_311IB();
                                        string MSG = objxl.ProcessData311_IB(id);
                                        writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_311[" + id + "] MSG: " + MSG);
                                        Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_311[" + id + "] MSG: " + MSG);
                                    }
                                    strID.RemoveAt(0);
                                }
                            }
                        }
                        //doan nay cho ddnag ky nho thu
                        strID = obj.getLIST314();
                        if (strID != null)
                        {
                            while (strID.Count > 0)
                            {
                                string id = strID[0];
                                if (id.Length > 0)
                                {
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_314=" + id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_314=" + id);
                                    HQ247XulyMSG.MSG_314 objxl = new HQ247XulyMSG.MSG_314();
                                    string MSG = objxl.ProcessData314(id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_314[" + id + "] MSG: " + MSG);
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_314[" + id + "] MSG: " + MSG);
                                }
                                strID.RemoveAt(0);
                            }
                        }

                        ////doan nay cho phan tu dong huy ho so het han
                        //strID = obj.getLIST314_HETHAN_TUDONG();
                        //if (strID != null)
                        //{
                        //    while (strID.Count > 0)
                        //    {
                        //        string id = strID[0];
                        //        if (id.Length > 0)
                        //        {
                        //            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Expired date Message: ID_314=" + id);
                        //            writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Expired date Message: ID_314=" + id);
                        //            HQ247XulyMSG.MSG_314 objxl = new HQ247XulyMSG.MSG_314();
                        //            string MSG = objxl.XuLyHetHanTD_314(id);
                        //            writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Expired date ID_314[" + id + "] MSG: " + MSG);
                        //            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_314[" + id + "] MSG: " + MSG);
                        //        }
                        //        strID.RemoveAt(0);
                        //    }
                        //}

                        strID = obj.getLIST311TMP();
                        if (strID != null)
                        {
                            while (strID.Count > 0)
                            {
                                string id = strID[0];
                                if (id.Length > 0)
                                {
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_311=" + id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # Received Message: ID_311=" + id);
                                    HQ247XulyMSG.MSG_XULY311 objxl = new HQ247XulyMSG.MSG_XULY311();
                                    decimal decMSG = objxl.xulyHQ311TMP(id);
                                    writeLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_311[" + id + "] MSG: " + decMSG);
                                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # ID_311[" + id + "] MSG: " + decMSG);
                                }
                                strID.RemoveAt(0);
                            }
                        }
                        x++;
                        if (x >= 20)
                        {
                            x = 0;
                            if (HQ247XulyMSG.HQ247AutoDC.isSendAutoAntagonize())
                            {
                                string strTMP = HQ247XulyMSG.HQ247AutoDC.SendAutoAntagonize807();
                                Console.WriteLine("send nDC: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # " + strTMP);
                                writeLog(strTMP);
                            }
                            if (HQ247XulyMSG.HQ247AutoDC.isReciveAutoAntagonize())
                            {
                                string strTMP = HQ247XulyMSG.HQ247AutoDC.ReciveAutoAntagonize857();
                                Console.WriteLine("Recive DC: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # " + strTMP);
                                writeLog(strTMP);
                            }
                            Thread.Sleep(5000);

                        }
                        else
                        { Thread.Sleep(1000); }
                        //đối chiếu thuế tại quầy
                        y++;
                        if (y >= 20)
                        {
                            y = 0;
                            ngayDC = DateTime.Now.ToString("dd/MM/yyyy");
                            dtngayDC = DateTime.ParseExact(ngayDC, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat);
                            if (HQ247XulyMSG.HQ247AutoDC.isSendAutoAntagonizeTQ())
                            {
                                strGD = "801";
                                CustomsServiceV3.ProcessMSG.GuiTDDoiChieuThuePhi_HQ(dtngayDC, strGD, ref strErrorNum, ref strErrorMes);
                                Console.WriteLine("send nDC tq: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # " + strErrorMes);
                                writeLog(strErrorMes);

                            }
                            if (HQ247XulyMSG.HQ247AutoDC.isReciveAutoAntagonizeTQ())
                            {
                                strGDNhanDL = "01";
                                CustomsServiceV3.ProcessMSG.getKetQuaDC(dtngayDC, strGDNhanDL, ref strErrorNum, ref strErrorMes);
                                Console.WriteLine("Recive DC tq: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # " + strErrorMes);
                                writeLog(strErrorMes);
                            }

                            Thread.Sleep(5000);
                        }
                        else
                        { Thread.Sleep(1000); }

                         z++;
                         if (z >= 30)
                         {
                             z = 0;
                             //re connect db
                             HQ247XulyMSG.ReConnectDB objRe = new HQ247XulyMSG.ReConnectDB();
                             objRe.CheckReConnectDB();
                         }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # " + e.Message);
                        writeLog(e.Message);
                        Thread.Sleep(10000);
                    }

                } while (message != "BYE");
            }
            Console.WriteLine("==END==");
        }
        //Ghi log
        public static void writeLog(string msg)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(logFile + "\\LogHQ247Auto_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(DateTime.Now.ToString() + " " + msg);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }
    }
}
