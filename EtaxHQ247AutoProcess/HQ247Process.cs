﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using VBOracleLib;
using Business_HQ.Common;
using Business_HQ.HQ247;
using CustomsV3.MSG.MSG304;
using System.Configuration;
using System.IO;
namespace EtaxHQ247AutoProcess
{
    public class HQ247Process
    {
        public string AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.AccountHQ247").ToString();
        public string Ma_AccountHQ247 = ConfigurationSettings.AppSettings.Get("CUSTOMS.Ma_AccountHQ247").ToString();
        private static String strSender_Code = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
        private static String strMaxAutoProcess = ConfigurationSettings.AppSettings.Get("CUSTOMS.MaxAutoProcess").ToString();
        private string strTimeDelay = ConfigurationSettings.AppSettings.Get("CUSTOMS.AUTOPAYEMTTIMEDELAY").ToString();

        public List<string> getListSoChungTuChuaXuLy()
        {
            try
            {
                List<string> strID = new List<string>();
                string strSQL = "SELECT ID FROM TCS_HQ247_304 WHERE ((TRANGTHAI ='01') ";
                strSQL += " OR(TRANGTHAI IN('02','03','05','06','10') AND AUTOPROCESS>0 AND AUTOPROCESS<" + strMaxAutoProcess + ")) and rownum<20  ORDER BY ID";
                DataSet ds = new DataSet();
          
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strID.Add(ds.Tables[0].Rows[i][0].ToString());
                                try
                                {
                                    //cap nhat trang thai xu ly tu dong    
                                    strSQL = "UPDATE TCS_HQ247_304 SET AUTOPROCESS=AUTOPROCESS+1 WHERE ID='" + ds.Tables[0].Rows[i][0].ToString() + "' ";
                                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                }
                                catch (Exception exc)
                                { }
                            }
                 
              
                return strID;

            }
            catch (Exception ex)
            {

            }
            return null;

        }

        public string getSoChungTuChuaXuLy()
        {
            try
            {
                string strSQL = "SELECT ID FROM TCS_HQ247_304 WHERE ((TRANGTHAI ='01') ";
                strSQL += " OR(TRANGTHAI IN('02','03','05','06','10') AND AUTOPROCESS>0 AND AUTOPROCESS<" + strMaxAutoProcess + ")) and rownum<20  ORDER BY ID";
                       DataSet ds = new DataSet();
                       string strID = "";
                       ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                       if (ds != null)
                           if (ds.Tables.Count > 0)
                               if (ds.Tables[0].Rows.Count > 0)
                                   strID = ds.Tables[0].Rows[0][0].ToString();
                //cap nhat trang thai xu ly tu dong       
                if (!strID.Equals(""))
                {
                    strSQL = "UPDATE TCS_HQ247_304 SET AUTOPROCESS=AUTOPROCESS+1 WHERE ID='" + strID + "' ";
                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                }
                return strID;

            }
            catch (Exception ex)
            { 
            
            }
            return "";
        
        }
        public string ProcessData(string str304ID)
        {
            try
            {
                HQ247XulyMSG.XULY_MSG304 objXly = new HQ247XulyMSG.XULY_MSG304(AccountHQ247,Ma_AccountHQ247,strSender_Code);
                return objXly.ProcessData(str304ID);
                
            }
            catch (Exception ex)
            {

            }
            return "";
        }
        public void sendTuChoi213(string strID, string LyDo)
        { 
        
        }
        public string ProcessData201(string str201ID)
        {
            try
            {
                HQ247XulyMSG.XULY_MSG201 objXly = new HQ247XulyMSG.XULY_MSG201(AccountHQ247, Ma_AccountHQ247, strSender_Code);
                return objXly.ProcessData(str201ID);

            }
            catch (Exception ex)
            {

            }
            return "";
        }
        public List<string> getLIST311()
        {
            try
            {
                List<string> strID = new List<string>();
                string strSQL = "SELECT * FROM TCS_DM_NTDT_HQ_311 WHERE ( TRANGTHAI IN ('01') OR (AUTOPROCESS <" + strMaxAutoProcess + " AND TRANGTHAI IN ('02','03','05','06') ))";
               // DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                DataSet ds = new DataSet();

                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strID.Add(ds.Tables[0].Rows[i][0].ToString());
                                try
                                {
                                    //cap nhat trang thai xu ly tu dong    
                                    strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET AUTOPROCESS=AUTOPROCESS+1 WHERE ID='" + ds.Tables[0].Rows[i][0].ToString() + "' ";
                                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                }
                                catch (Exception exc)
                                { }
                            }


                return strID;
            }
            catch (Exception ex)
            { 
            }
            return null;
        
        }
        public List<string> getLIST311TMP()
        {
            try
            {
                List<string> strID = new List<string>();
                string strSQL = "SELECT * FROM TCS_HQ_311_TMP ORDER BY NGAY_NHAN311 ";
                // DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                DataSet ds = new DataSet();

                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strID.Add(ds.Tables[0].Rows[i]["ID"].ToString());
                               
                            }


                return strID;
            }
            catch (Exception ex)
            {
            }
            return null;

        }
        public List<string> getLIST311IB()
        {
            try
            {
                List<string> strID = new List<string>();
                string strSQL = "SELECT * FROM TCS_DM_NTDT_HQ_311 WHERE TRANGTHAI IN ('09','11') AND IS_BK='1' AND AUTOPROCESS <" + strMaxAutoProcess;
                // DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                DataSet ds = new DataSet();

                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strID.Add(ds.Tables[0].Rows[i][0].ToString());
                                try
                                {
                                    //cap nhat trang thai xu ly tu dong    
                                    strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET AUTOPROCESS=AUTOPROCESS+1 WHERE ID='" + ds.Tables[0].Rows[i][0].ToString() + "' ";
                                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                }
                                catch (Exception exc)
                                { }
                            }


                return strID;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public List<string> getLIST314()
        {
            try
            {
                List<string> strID = new List<string>();
                string strSQL = "SELECT * FROM TCS_HQ247_314_TMP WHERE TRANGTHAI IN ('01','02','03','05','06') AND AUTOPROCESS <" + strMaxAutoProcess + " AND AUTOPAYMENTTIME<='" + DateTime.Now.ToString("yyyyMMddHHmmss") + "'";
                // DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                DataSet ds = new DataSet();

                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strID.Add(ds.Tables[0].Rows[i][0].ToString());
                                try
                                {
                                    DateTime vTimeDelay = DateTime.Now;
                                    vTimeDelay = vTimeDelay.AddSeconds(double.Parse(strTimeDelay));
                                    //cap nhat trang thai xu ly tu dong    
                                    strSQL = "UPDATE TCS_HQ247_314_TMP SET AUTOPROCESS=AUTOPROCESS+1,AUTOPAYMENTTIME='" + vTimeDelay.ToString("yyyyMMddHHmmss") + "' WHERE ID='" + ds.Tables[0].Rows[i][0].ToString() + "' ";
                                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                }
                                catch (Exception exc)
                                { }
                            }


                return strID;
            }
            catch (Exception ex)
            {
            }
            return null;

        }
        public List<string> getListSoChungTuChuaXuLy201()
        {
            try
            {
                List<string> strID = new List<string>();
                string strSQL = "SELECT ID FROM TCS_HQ247_201 WHERE TRANGTHAI IN('01','02','03','05','06','10') ";
                strSQL += " AND AUTOPROCESS<" + strMaxAutoProcess + " and AUTOPAYMENTTIME<'" + DateTime.Now.ToString("yyyyMMddHHmmss") + "' and rownum<20  ORDER BY ID";
                DataSet ds = new DataSet();

                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strID.Add(ds.Tables[0].Rows[i][0].ToString());
                                try
                                {
                                    DateTime vTimeDelay = DateTime.Now;
                                    vTimeDelay = vTimeDelay.AddSeconds(double.Parse(strTimeDelay));
                                    //cap nhat trang thai xu ly tu dong    
                                    strSQL = "UPDATE TCS_HQ247_201 SET AUTOPROCESS=AUTOPROCESS+1,AUTOPAYMENTTIME='" + vTimeDelay.ToString("yyyyMMddHHmmss") + "' WHERE ID='" + ds.Tables[0].Rows[i][0].ToString() + "' ";
                                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                }
                                catch (Exception exc)
                                { }
                            }


                return strID;

            }
            catch (Exception ex)
            {

            }
            return null;

        }
        public List<string> getLIST314_HETHAN_TUDONG()
        {
            try
            {
                List<string> strID = new List<string>();
                string strSQL = "SELECT * FROM TCS_HQ247_314 WHERE  TRANGTHAI IN ('13','16') AND TO_CHAR( NGAY_HHL_UQ,'RRRRMMDD')<'" + DateTime.Now.ToString("yyyyMMdd") + "' ";
                // DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                DataSet ds = new DataSet();

                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                strID.Add(ds.Tables[0].Rows[i][0].ToString());
                                try
                                {
                                    DateTime vTimeDelay = DateTime.Now;
                                    vTimeDelay = vTimeDelay.AddSeconds(double.Parse(strTimeDelay));
                                    //cap nhat trang thai xu ly tu dong    
                                    strSQL = "UPDATE TCS_HQ247_314_TMP SET AUTOPROCESS=AUTOPROCESS+1,AUTOPAYMENTTIME='" + vTimeDelay.ToString("yyyyMMddHHmmss") + "' WHERE ID='" + ds.Tables[0].Rows[i][0].ToString() + "' ";
                                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                                }
                                catch (Exception exc)
                                { }
                            }


                return strID;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
