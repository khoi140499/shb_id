﻿Namespace HaiQuanObj

    Public Class HdrHq

        Private _SHKB As String         'Số hiệu kho bạc
        Private _NGAY_KB As String      'Ngày kho bạc
        Private _MA_TINH As String      'Mã tỉnh của kho bạc
        Private _MA_HUYEN As String     'Mã huyện của kho bạc
        Private _MA_XA As String        'Mã xã của kho bạc
        Private _TK_CO As String        'Tài khoản có của kho bạc

        Private _MA_NV As String        'x  'Mã nhân viên lập
        Private _MA_KS As String        'x  'Mã nhân viên kiểm soát
        Private _MA_CN As String        'x  'Mã chi nhánh nhân viên lập
        Private _NGAY_KS As String      'x  Ngày kiểm soát

        Private _SO_BT As String        'Số bút toán
        Private _MA_DTHU As String      'Mã điểm thu => 0
        Private _KYHIEU_CT As String    'Ký hiệu chứng từ
        Private _SO_CT As String        'Số chứng từ
        Private _MA_LTHUE As String     'Mã loại thuế (Thuế nội địa, Thuế hq,....)

        Private _MA_NNTIEN As String    'Mã số thuế người nộp tiền
        Private _TEN_NNTIEN As String   'Tên người nộp tiền
        Private _DC_NNTIEN As String    'Địa chỉ người nộp tiền
        Private _MA_QUAN_HUYENNNTIEN As String  'Mã quận/huyện người nộp tiền 018HH
        Private _MA_TINH_TPNNTIEN As String     'Mã tỉnh/tp người nộp tiền    01TTT

        Private _MA_NNTHUE As String    'Mã số thuế người nộp thuế
        Private _TEN_NNTHUE As String   'Tên người nộp thuế
        Private _DC_NNTHUE As String    'Địa chỉ người nộp thuế
        Private _MA_HUYEN_NNTHUE As String  'Mã huyện người nộp thuế
        Private _MA_TINH_NNTHUE As String   'Mã tỉnh người nộp thuế

        Private _NGAY_CT As String     'Ngày chứng từ
        Private _NGAY_HT As String      'Ngày hệ thống
        Private _MA_CQTHU As String     'Mã cơ quan thu

        Private _SO_TK As String        'Số tờ khai(nhiều tờ khai ngăn nhau = dấu ',')
        Private _NGAY_TK As String      'Ngày tờ khai
        Private _LH_XNK As String       'Loại hình xuất nhập khẩu

        Private _PT_TT As String        'x  'Phương thức thanh toán tiền: Tiền mặt, chuyển khoản, GL,...
        Private _MA_NT As String        'Mã nguyên tệ (VND, USD)
        Private _TY_GIA As String       'Tỷ giá so với VND
        Private _TTIEN As String        'Tổng tiền VND
        Private _TTIEN_NT As String     'Tông tiền ngoại tệ (VND,USD,...)
        Private _TT_TTHU As String      'Tổng tiền thực thu

        Private _TK_KH_NH As String     'x  'Số tài khoản của khách hàng
        Private _TEN_KH_NH As String    'x  'Tên khách hàng trong corebank
        Private _NGAY_KH_NH As String   'x  'Ngày cắt tiền khách hàng

        Private _MA_NH_A As String      'x  'Mã ngân hàng chuyển(MSB, DongA, ABB, EXIM,...)
        Private _MA_NH_B As String      'x  'Mã ngân hàng thụ hưởng(AGIRBANK, BIDV, VCB, VIETIN,...)
        Private _TEN_NH_B As String     'x  'Tên ngân hàng thụ hưởng
        Private _MA_NH_TT As String     'x  'Mã ngân hàng trực tiếp
        Private _TEN_NH_TT As String    'x  'Tên ngân hàng trực tiếp

        Private _LAN_IN As String       'x  'Số lần in chứng từ

        Private _TRANG_THAI As String   'Trạng thái chứng từ

        Private _PT_TINHPHI As String   'x  'Phương pháp tình phí
        Private _PHI_GD As String       'x  'Phí giao dịch
        Private _PHI_VAT As String      'x  'VAT của phí giao dịch
        Private _PHI_GD2 As String      'x  'Phí giao dịch(kiểm đếm,...)
        Private _PHI_VAT2 As String     'x  'VAT của phí kiểm đếm

        Private _TT_CTHUE As String     'x  'Trạng thái gửi thuế

        Private _TKHT As String         'Tài khoản hạch toán

        Private _LOAI_TT As String      'Loại tiền thuế: 1,2,....
        Private _MA_NTK As String       'Mã nhóm tài khoản
        Private _MA_HQ As String        'Mã hải quan
        Private _TEN_HQ As String       'Tên hải quan
        Private _MA_HQ_PH As String     'Mã hải quan phát hành
        Private _TEN_HQ_PH As String    'Tên hải quan phát hành

        Private _KENH_CT As String      'x  'Kênh chứng từ - etax - ebank
        Private _MA_SANPHAM As String   'x  'Mã sản phẩm

        Private _TT_CITAD As String     'x  'Trạng thái citad - trực tiếp - gián tiếp
        Private _SO_CMND As String      'x  'Số chứng minh thư => Nộp tiền mặt
        Private _SO_FONE As String      'x  'Số điện thoại => Nộp tiền mặt

        Private _LOAI_CTU As String     'Loại chứng từ (Thuế hải quan - Phí hải quan)

        Private _REMARKS As String      'x  'Nội dung citad

        Private _DIENGIAI_HQ As String      'x  'Diễn giải hải quan

        Private _MA_CUC As String      'x  'Mã cục HQ
        Private _TEN_CUC As String      'x  'Tên cục HQ

        'MSB

        Private _TK_GL_NH As String
        Private _REF_NO As String
        Private _RM_REF_NO As String

        'SHB
        Private _GHI_CHU As String

        Public Property GHI_CHU() As String
            Get
                Return _GHI_CHU
            End Get
            Set(ByVal value As String)
                _GHI_CHU = value
            End Set
        End Property

        Public Property TK_GL_NH() As String
            Get
                Return _TK_GL_NH
            End Get
            Set(ByVal value As String)
                _TK_GL_NH = value
            End Set
        End Property

        Public Property RM_REF_NO() As String
            Get
                Return _RM_REF_NO
            End Get
            Set(ByVal value As String)
                _RM_REF_NO = value
            End Set
        End Property

        Public Property REF_NO() As String
            Get
                Return _REF_NO
            End Get
            Set(ByVal value As String)
                _REF_NO = value
            End Set
        End Property


        Public Property REMARKS() As String
            Get
                Return _REMARKS
            End Get
            Set(ByVal value As String)
                _REMARKS = value
            End Set
        End Property
        Public Property DIENGIAI_HQ() As String
            Get
                Return _DIENGIAI_HQ
            End Get
            Set(ByVal value As String)
                _DIENGIAI_HQ = value
            End Set
        End Property
        Public Property LOAI_CTU() As String
            Get
                Return _LOAI_CTU
            End Get
            Set(ByVal value As String)
                _LOAI_CTU = value
            End Set
        End Property

        Public Property SO_FONE() As String
            Get
                Return _SO_FONE
            End Get
            Set(ByVal value As String)
                _SO_FONE = value
            End Set
        End Property

        Public Property SO_CMND() As String
            Get
                Return _SO_CMND
            End Get
            Set(ByVal value As String)
                _SO_CMND = value
            End Set
        End Property

        Public Property TT_CITAD() As String
            Get
                Return _TT_CITAD
            End Get
            Set(ByVal value As String)
                _TT_CITAD = value
            End Set
        End Property

        Public Property MA_SANPHAM() As String
            Get
                Return _MA_SANPHAM
            End Get
            Set(ByVal value As String)
                _MA_SANPHAM = value
            End Set
        End Property

        Public Property KENH_CT() As String
            Get
                Return _KENH_CT
            End Get
            Set(ByVal value As String)
                _KENH_CT = value
            End Set
        End Property

        Public Property TEN_HQ_PH() As String
            Get
                Return _TEN_HQ_PH
            End Get
            Set(ByVal value As String)
                _TEN_HQ_PH = value
            End Set
        End Property

        Public Property MA_HQ_PH() As String
            Get
                Return _MA_HQ_PH
            End Get
            Set(ByVal value As String)
                _MA_HQ_PH = value
            End Set
        End Property

        Public Property TEN_HQ() As String
            Get
                Return _TEN_HQ
            End Get
            Set(ByVal value As String)
                _TEN_HQ = value
            End Set
        End Property

        Public Property MA_HQ() As String
            Get
                Return _MA_HQ
            End Get
            Set(ByVal value As String)
                _MA_HQ = value
            End Set
        End Property

        Public Property MA_NTK() As String
            Get
                Return _MA_NTK
            End Get
            Set(ByVal value As String)
                _MA_NTK = value
            End Set
        End Property

        Public Property LOAI_TT() As String
            Get
                Return _LOAI_TT
            End Get
            Set(ByVal value As String)
                _LOAI_TT = value
            End Set
        End Property


        Public Property TKHT() As String
            Get
                Return _TKHT
            End Get
            Set(ByVal value As String)
                _TKHT = value
            End Set
        End Property

        Public Property NGAY_KS() As String
            Get
                Return _NGAY_KS
            End Get
            Set(ByVal value As String)
                _NGAY_KS = value
            End Set
        End Property

        Public Property TT_CTHUE() As String
            Get
                Return _TT_CTHUE
            End Get
            Set(ByVal value As String)
                _TT_CTHUE = value
            End Set
        End Property

        Public Property PHI_VAT2() As String
            Get
                Return _PHI_VAT2
            End Get
            Set(ByVal value As String)
                _PHI_VAT2 = value
            End Set
        End Property

        Public Property PHI_GD2() As String
            Get
                Return _PHI_GD2
            End Get
            Set(ByVal value As String)
                _PHI_GD2 = value
            End Set
        End Property

        Public Property PHI_VAT() As String
            Get
                Return _PHI_VAT
            End Get
            Set(ByVal value As String)
                _PHI_VAT = value
            End Set
        End Property

        Public Property PHI_GD() As String
            Get
                Return _PHI_GD
            End Get
            Set(ByVal value As String)
                _PHI_GD = value
            End Set
        End Property

        Public Property PT_TINHPHI() As String
            Get
                Return _PT_TINHPHI
            End Get
            Set(ByVal value As String)
                _PT_TINHPHI = value
            End Set
        End Property

        Public Property TRANG_THAI() As String
            Get
                Return _TRANG_THAI
            End Get
            Set(ByVal value As String)
                _TRANG_THAI = value
            End Set
        End Property

        Public Property TEN_NH_TT() As String
            Get
                Return _TEN_NH_TT
            End Get
            Set(ByVal value As String)
                _TEN_NH_TT = value
            End Set
        End Property

        Public Property MA_NH_TT() As String
            Get
                Return _MA_NH_TT
            End Get
            Set(ByVal value As String)
                _MA_NH_TT = value
            End Set
        End Property

        Public Property TEN_NH_B() As String
            Get
                Return _TEN_NH_B
            End Get
            Set(ByVal value As String)
                _TEN_NH_B = value
            End Set
        End Property

        Public Property MA_NH_B() As String
            Get
                Return _MA_NH_B
            End Get
            Set(ByVal value As String)
                _MA_NH_B = value
            End Set
        End Property

        Public Property MA_NH_A() As String
            Get
                Return _MA_NH_A
            End Get
            Set(ByVal value As String)
                _MA_NH_A = value
            End Set
        End Property

        Public Property NGAY_KH_NH() As String
            Get
                Return _NGAY_KH_NH
            End Get
            Set(ByVal value As String)
                _NGAY_KH_NH = value
            End Set
        End Property

        Public Property TEN_KH_NH() As String
            Get
                Return _TEN_KH_NH
            End Get
            Set(ByVal value As String)
                _TEN_KH_NH = value
            End Set
        End Property

        Public Property TK_KH_NH() As String
            Get
                Return _TK_KH_NH
            End Get
            Set(ByVal value As String)
                _TK_KH_NH = value
            End Set
        End Property

        Public Property TT_TTHU() As String
            Get
                Return _TT_TTHU
            End Get
            Set(ByVal value As String)
                _TT_TTHU = value
            End Set
        End Property

        Public Property TTIEN_NT() As String
            Get
                Return _TTIEN_NT
            End Get
            Set(ByVal value As String)
                _TTIEN_NT = value
            End Set
        End Property

        Public Property TTIEN() As String
            Get
                Return _TTIEN
            End Get
            Set(ByVal value As String)
                _TTIEN = value
            End Set
        End Property

        Public Property TY_GIA() As String
            Get
                Return _TY_GIA
            End Get
            Set(ByVal value As String)
                _TY_GIA = value
            End Set
        End Property

        Public Property MA_NT() As String
            Get
                Return _MA_NT
            End Get
            Set(ByVal value As String)
                _MA_NT = value
            End Set
        End Property

        Public Property PT_TT() As String
            Get
                Return _PT_TT
            End Get
            Set(ByVal value As String)
                _PT_TT = value
            End Set
        End Property


        Public Property LH_XNK() As String
            Get
                Return _LH_XNK
            End Get
            Set(ByVal value As String)
                _LH_XNK = value
            End Set
        End Property

        Public Property NGAY_TK() As String
            Get
                Return _NGAY_TK
            End Get
            Set(ByVal value As String)
                _NGAY_TK = value
            End Set
        End Property

        Public Property SO_TK() As String
            Get
                Return _SO_TK
            End Get
            Set(ByVal value As String)
                _SO_TK = value
            End Set
        End Property

        Public Property MA_CQTHU() As String
            Get
                Return _MA_CQTHU
            End Get
            Set(ByVal value As String)
                _MA_CQTHU = value
            End Set
        End Property

        Public Property NGAY_CT() As String
            Get
                Return _NGAY_CT
            End Get
            Set(ByVal value As String)
                _NGAY_CT = value
            End Set
        End Property

        Public Property MA_TINH_NNTHUE() As String
            Get
                Return _MA_TINH_NNTHUE
            End Get
            Set(ByVal value As String)
                _MA_TINH_NNTHUE = value
            End Set
        End Property

        Public Property MA_HUYEN_NNTHUE() As String
            Get
                Return _MA_HUYEN_NNTHUE
            End Get
            Set(ByVal value As String)
                _MA_HUYEN_NNTHUE = value
            End Set
        End Property

        Public Property DC_NNTHUE() As String
            Get
                Return _DC_NNTHUE
            End Get
            Set(ByVal value As String)
                _DC_NNTHUE = value
            End Set
        End Property

        Public Property TEN_NNTHUE() As String
            Get
                Return _TEN_NNTHUE
            End Get
            Set(ByVal value As String)
                _TEN_NNTHUE = value
            End Set
        End Property

        Public Property MA_NNTHUE() As String
            Get
                Return _MA_NNTHUE
            End Get
            Set(ByVal value As String)
                _MA_NNTHUE = value
            End Set
        End Property

        Public Property MA_TINH_TPNNTIEN() As String
            Get
                Return _MA_TINH_TPNNTIEN
            End Get
            Set(ByVal value As String)
                _MA_TINH_TPNNTIEN = value
            End Set
        End Property

        Public Property MA_QUAN_HUYENNNTIEN() As String
            Get
                Return _MA_QUAN_HUYENNNTIEN
            End Get
            Set(ByVal value As String)
                _MA_QUAN_HUYENNNTIEN = value
            End Set
        End Property

        Public Property DC_NNTIEN() As String
            Get
                Return _DC_NNTIEN
            End Get
            Set(ByVal value As String)
                _DC_NNTIEN = value
            End Set
        End Property

        Public Property TEN_NNTIEN() As String
            Get
                Return _TEN_NNTIEN
            End Get
            Set(ByVal value As String)
                _TEN_NNTIEN = value
            End Set
        End Property

        Public Property MA_NNTIEN() As String
            Get
                Return _MA_NNTIEN
            End Get
            Set(ByVal value As String)
                _MA_NNTIEN = value
            End Set
        End Property

        Public Property MA_LTHUE() As String
            Get
                Return _MA_LTHUE
            End Get
            Set(ByVal value As String)
                _MA_LTHUE = value
            End Set
        End Property

        Public Property SO_CT() As String
            Get
                Return _SO_CT
            End Get
            Set(ByVal value As String)
                _SO_CT = value
            End Set
        End Property

        Public Property KYHIEU_CT() As String
            Get
                Return _KYHIEU_CT
            End Get
            Set(ByVal value As String)
                _KYHIEU_CT = value
            End Set
        End Property

        Public Property MA_DTHU() As String
            Get
                Return _MA_DTHU
            End Get
            Set(ByVal value As String)
                _MA_DTHU = value
            End Set
        End Property

        Public Property SO_BT() As String
            Get
                Return _SO_BT
            End Get
            Set(ByVal value As String)
                _SO_BT = value
            End Set
        End Property

        Public Property MA_CN() As String
            Get
                Return _MA_CN
            End Get
            Set(ByVal value As String)
                _MA_CN = value
            End Set
        End Property



        Public Property MA_KS() As String
            Get
                Return _MA_KS
            End Get
            Set(ByVal value As String)
                _MA_KS = value
            End Set
        End Property

        Public Property MA_NV() As String
            Get
                Return _MA_NV
            End Get
            Set(ByVal value As String)
                _MA_NV = value
            End Set
        End Property


        Public Property TK_CO() As String
            Get
                Return _TK_CO
            End Get
            Set(ByVal value As String)
                _TK_CO = value
            End Set
        End Property

        Public Property MA_XA() As String
            Get
                Return _MA_XA
            End Get
            Set(ByVal value As String)
                _MA_XA = value
            End Set
        End Property

        Public Property MA_HUYEN() As String
            Get
                Return _MA_HUYEN
            End Get
            Set(ByVal value As String)
                _MA_HUYEN = value
            End Set
        End Property

        Public Property MA_TINH() As String
            Get
                Return _MA_TINH
            End Get
            Set(ByVal value As String)
                _MA_TINH = value
            End Set
        End Property

        Public Property NGAY_KB() As String
            Get
                Return _NGAY_KB
            End Get
            Set(ByVal value As String)
                _NGAY_KB = value
            End Set
        End Property

        Public Property SHKB() As String
            Get
                Return _SHKB
            End Get
            Set(ByVal value As String)
                _SHKB = value
            End Set
        End Property

        Public Property MA_CUC() As String
            Get
                Return _MA_CUC
            End Get
            Set(ByVal value As String)
                _MA_CUC = value
            End Set
        End Property

        Public Property TEN_CUC() As String
            Get
                Return _TEN_CUC
            End Get
            Set(ByVal value As String)
                _TEN_CUC = value
            End Set
        End Property


    End Class
End Namespace