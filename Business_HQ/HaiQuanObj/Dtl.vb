﻿Namespace HaiQuanObj
    Public Class Dtl

        Private _SHKB As String
        Private _NGAY_KB As String
        Private _MA_NV As String
        Private _SO_BT As String
        Private _MA_DTHU As String
        Private _MA_CHUONG As String
        Private _MA_TMUC As String
        Private _NOI_DUNG As String
        Private _SOTIEN As String
        Private _SOTIEN_NT As String
        Private _SO_CT As String
        Private _TT_BTOAN As String
        Private _MA_HQ As String
        Private _MA_LHXNK As String
        Private _NGAY_TK As String
        Private _SO_TK As String
        Private _MA_LT As String
        Private _SAC_THUE As String

        Public Property SAC_THUE() As String
            Get
                Return _SAC_THUE
            End Get
            Set(ByVal value As String)
                _SAC_THUE = value
            End Set
        End Property

        Public Property MA_LT() As String
            Get
                Return _MA_LT
            End Get
            Set(ByVal value As String)
                _MA_LT = value
            End Set
        End Property

        Public Property SO_TK() As String
            Get
                Return _SO_TK
            End Get
            Set(ByVal value As String)
                _SO_TK = value
            End Set
        End Property

        Public Property NGAY_TK() As String
            Get
                Return _NGAY_TK
            End Get
            Set(ByVal value As String)
                _NGAY_TK = value
            End Set
        End Property

        Public Property MA_LHXNK() As String
            Get
                Return _MA_LHXNK
            End Get
            Set(ByVal value As String)
                _MA_LHXNK = value
            End Set
        End Property

        Public Property MA_HQ() As String
            Get
                Return _MA_HQ
            End Get
            Set(ByVal value As String)
                _MA_HQ = value
            End Set
        End Property

        Public Property TT_BTOAN() As String
            Get
                Return _TT_BTOAN
            End Get
            Set(ByVal value As String)
                _TT_BTOAN = value
            End Set
        End Property

        Public Property SO_CT() As String
            Get
                Return _SO_CT
            End Get
            Set(ByVal value As String)
                _SO_CT = value
            End Set
        End Property

        Public Property SOTIEN_NT() As String
            Get
                Return _SOTIEN_NT
            End Get
            Set(ByVal value As String)
                _SOTIEN_NT = value
            End Set
        End Property

        Public Property SOTIEN() As String
            Get
                Return _SOTIEN
            End Get
            Set(ByVal value As String)
                _SOTIEN = value
            End Set
        End Property

        Public Property NOI_DUNG() As String
            Get
                Return _NOI_DUNG
            End Get
            Set(ByVal value As String)
                _NOI_DUNG = value
            End Set
        End Property

        Public Property MA_TMUC() As String
            Get
                Return _MA_TMUC
            End Get
            Set(ByVal value As String)
                _MA_TMUC = value
            End Set
        End Property

        Public Property MA_CHUONG() As String
            Get
                Return _MA_CHUONG
            End Get
            Set(ByVal value As String)
                _MA_CHUONG = value
            End Set
        End Property

        Public Property MA_DTHU() As String
            Get
                Return _MA_DTHU
            End Get
            Set(ByVal value As String)
                _MA_DTHU = value
            End Set
        End Property

        Public Property SO_BT() As String
            Get
                Return _SO_BT
            End Get
            Set(ByVal value As String)
                _SO_BT = value
            End Set
        End Property

        Public Property MA_NV() As String
            Get
                Return _MA_NV
            End Get
            Set(ByVal value As String)
                _MA_NV = value
            End Set
        End Property

        Public Property NGAY_KB() As String
            Get
                Return _NGAY_KB
            End Get
            Set(ByVal value As String)
                _NGAY_KB = value
            End Set
        End Property

        Public Property SHKB() As String
            Get
                Return _SHKB
            End Get
            Set(ByVal value As String)
                _SHKB = value
            End Set
        End Property

    End Class
End Namespace
