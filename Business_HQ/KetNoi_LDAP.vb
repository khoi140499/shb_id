﻿Imports System.Configuration
Imports System.DirectoryServices
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports Business_HQ
Imports System.Data
Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports System.Net

Public Class KetNoi_LDAP
    Private username As String = "xxUserNamexx"
    Private pwd As String = "xxPwdxx"
    Private Shared domain As String = ConfigurationManager.AppSettings.Get("DOMAIN").ToString()
    Private Shared _path As String = ConfigurationManager.AppSettings.Get("LDAP_PATH").ToString()
    Private Shared _filterAttribute As String
    Public Shared Function IsAuthenticated(ByVal username As String, ByVal pwd As String) As Boolean
        Dim domainAndUsername As String = domain + "\" + username
        Dim entry As New DirectoryEntry(_path, domainAndUsername, pwd, AuthenticationTypes.None)
        Dim b_result As Boolean = True
        Try
            Dim obj As Object = entry.NativeObject

            Dim search As New DirectorySearcher(entry)

            search.Filter = "(SAMAccountName=" + username + ")"
            search.PropertiesToLoad.Add("cn")
            Dim result As SearchResult = search.FindOne()

            If result Is Nothing Then
                'Throw New Exception("Error authenticating user.")
                Return False
            End If

        Catch ex As Exception
            'Throw New Exception("Error authenticating user. " + ex.Message)
            Return False
        End Try

        Return True
    End Function
    Public Shared Function CheckPassWord(ByVal username As String, ByVal pwd As String) As Boolean
        Dim cnUser As DataAccess
        Dim transCT As IDbTransaction
        Dim strLog_DN As String = ""
        cnUser = New DataAccess
        Dim b_result As Boolean = True
        Try
            Dim strSQL_LDAP As String
            Dim strUserDomain As String = ""

            'Kiem tra LDAP hay ko
            strSQL_LDAP = "select * from tcs_dm_nhanvien where UPPER (ten_dn) = UPPER ('" & username & "')"
            Dim dt_LDAP As New DataTable
            dt_LDAP = DataAccess.ExecuteToTable(strSQL_LDAP)
            'Ket thuc
            If Not dt_LDAP Is Nothing And dt_LDAP.Rows.Count > 0 Then
                'If dt_LDAP.Rows(0)("LDAP").ToString() = "0" Then
                '    strUserDomain = dt_LDAP.Rows(0)("USERDOMAIN").ToString()
                '    'CHECK LDAP
                '    If username.Equals("admin") Then
                '        If Not pwd.Equals(DescryptStr(dt_LDAP.Rows(0)("MAT_KHAU"))) Then
                '            b_result = False
                '        End If
                '    Else
                '        Dim checkConnectLDAP As Boolean = IsAuthenticated(strUserDomain.ToUpper(), pwd)
                '        If (checkConnectLDAP = False) Then
                '            b_result = False
                '        End If
                '    End If
                '    'END CHECK LDAP
                'Else
                If dt_LDAP.Rows(0)("MAT_KHAU").ToString() <> EncryptStr(pwd) Then
                    b_result = False
                End If
                'End If
            End If
        Catch ex As Exception
        End Try
        Return b_result
    End Function
End Class
