﻿Imports VBOracleLib

Namespace HaiQuan


    Public Class HaiQuan
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        ''' <summary>
        ''' Lấy transaction_idtại bảng tcs_dchieu_nhhq
        ''' </summary>
        ''' <param name="strSoCT"></param>
        ''' <returns></returns>
        ''' <remarks>sonmt</remarks>
        Public Shared Function DCHIEU_NHHQ_GET_TRANSID(ByVal strSoCT As String) As String
            Dim strSQL As String
            Dim strResult As String = ""
            Try
                strSQL = "select transaction_id from tcs_dchieu_nhhq_hdr where so_ct=:so_ct order by id desc "
                Dim arrParam(0) As IDataParameter
                arrParam(0) = DataAccess.NewDBParameter("so_ct", ParameterDirection.Input, CStr(strSoCT), OracleClient.OracleType.NVarChar)

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, arrParam))

                If Not dt Is Nothing And dt.Rows.Count > 0 Then
                    strResult = dt.Rows(0)("transaction_id").ToString
                End If
            Catch ex As Exception
            Finally
            End Try
            Return strResult
        End Function

        ''' <summary>
        ''' Lấy danh sách nhân viên trong bảng tcs_dm_nhanvien theo điều kiện
        ''' </summary>
        ''' <param name="strcheckHS"></param>
        ''' <param name="str_Ma_Nhom"></param>
        ''' <param name="ma_cn"></param>
        ''' <param name="ma_nv"></param>
        ''' <param name="strMaNhom_GDV"></param>
        ''' <returns></returns>
        ''' <remarks>sonmt</remarks>
        Public Shared Function GET_DM_NV(ByVal strcheckHS As String, ByVal str_Ma_Nhom As String, ByVal ma_cn As String, ByVal ma_nv As String, ByVal strMaNhom_GDV As String) As DataTable
            Dim strSQL As String
            Dim dtResult As DataTable = New DataTable()
            Dim dk As String = ""
            Try
                strSQL = ""
                Dim arrParam(2) As IDataParameter

                If strcheckHS <> "1" Then
                    If strcheckHS = "2" Then
                        dk = "and b.branch_id= :ma_cn"
                    Else
                        dk = "and b.id= :ma_cn"
                    End If
                End If
                If str_Ma_Nhom = strMaNhom_GDV Then
                    strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nv=:ma_nv and a.ma_nhom=:ma_nhom " & dk
                Else
                    strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nhom=:ma_nhom and :ma_nv=:ma_nv " & dk
                End If

                arrParam(0) = DataAccess.NewDBParameter("ma_cn", ParameterDirection.Input, ma_cn, OracleClient.OracleType.NVarChar)
                arrParam(1) = DataAccess.NewDBParameter("ma_nv", ParameterDirection.Input, ma_nv, OracleClient.OracleType.NVarChar)
                arrParam(2) = DataAccess.NewDBParameter("ma_nhom", ParameterDirection.Input, strMaNhom_GDV, OracleClient.OracleType.NVarChar)


                dtResult = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, arrParam))


            Catch ex As Exception
            Finally
            End Try
            Return dtResult
        End Function


        Public Shared Function GET_MA_NHOM(ByVal ma_nv As String) As String
            Dim strSQL As String
            Dim strResult As String = ""
            Try
                strSQL = "select ma_nhom from tcs_dm_nhanvien where ma_nv=:ma_nv"
                Dim arrParam(0) As IDataParameter
                arrParam(0) = DataAccess.NewDBParameter("ma_nv", ParameterDirection.Input, ma_nv, OracleClient.OracleType.Number)

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, arrParam))

                If Not dt Is Nothing And dt.Rows.Count > 0 Then
                    strResult = dt.Rows(0)("ma_nhom").ToString
                End If
            Catch ex As Exception
            Finally
            End Try
            Return strResult
        End Function


        Public Shared Function GET_DS_CT_PHI_BO_NGANH(ByVal strNgay_LV As String, ByVal strTrangThai_CT As String, ByVal ma_ks As String, ByVal TT_CT As String, ByVal ma_cn As String) As DataSet
            Dim strSQL As String
            Dim dtResult As DataSet = New DataSet()
            Dim dk As String = ""
            Try

                Dim ngay_ct As DateTime = New DateTime()
                ngay_ct = Common.mdlCommon.ConvertNumberToDate(strNgay_LV)

                strSQL = ""
                Dim arrParam As List(Of IDataParameter) = New List(Of IDataParameter)

                strSQL = "SELECT ct.SO_CT, ct.kyhieu_ct, ct.trangthai_ct,ct.trangthai_xuly,ct.ma_nv,ct.ma_ks,nv.ten_dn,dc.so_tn_ct,dc.ngay_tn_ct,ks.ten_dn ten_ks, fee, vat, tongtien_truoc_vat FROM tcs_thue_phi_bo_nganh ct, tcs_dm_nhanvien nv, tcs_dm_nhanvien ks, tcs_dchieu_thue_phi_bo_nganh dc where ct.ma_nv=nv.ma_nv(+) and ct.ma_ks=ks.ma_nv(+) and ct.so_ct=dc.so_ct(+) and (ct.ngay_ct = to_date(to_char(sysdate,'DD/MM/YYYY'),'dd/MM/yyyy') or (ct.trangthai_ct in ('02','03','05') or (ct.trangthai_ct ='01' and ct.trangthai_xuly=0) )) "
                'arrParam.Add(DataAccess.NewDBParameter("ngay_ct", ParameterDirection.Input, ngay_ct, OracleClient.OracleType.DateTime))

                If strTrangThai_CT.Trim().Length > 0 Then
                    strSQL &= " and ct.trangthai_ct = :tthai_ct"
                    arrParam.Add(DataAccess.NewDBParameter("tthai_ct", ParameterDirection.Input, strTrangThai_CT, OracleClient.OracleType.NVarChar))
                End If
                If TT_CT.Trim().Length > 0 AndAlso TT_CT <> "-1" Then
                    strSQL &= " and ct.trangthai_xuly = :tthai_xl"
                    arrParam.Add(DataAccess.NewDBParameter("tthai_xl", ParameterDirection.Input, TT_CT, OracleClient.OracleType.NVarChar))
                End If
                If ma_ks.Trim().Length > 0 Then
                    strSQL = strSQL & " and ct.MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV=" + ma_ks + ") "
                End If

                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, arrParam.ToArray())


            Catch ex As Exception
            Finally
            End Try
            Return dtResult
        End Function

        Public Shared Function GET_DS_CT_PHI_BO_NGANH(ByVal strNgay_LV As String, ByVal strTrangThai_CT As String, ByVal ma_ks As String, ByVal type As String, ByVal TT_CT As String, ByVal ma_cn As String) As DataSet
            Dim strSQL As String
            Dim dtResult As DataSet = New DataSet()
            Dim dk As String = ""
            Try

                Dim ngay_ct As DateTime = New DateTime()
                ngay_ct = Common.mdlCommon.ConvertNumberToDate(strNgay_LV)

                strSQL = ""
                Dim arrParam As List(Of IDataParameter) = New List(Of IDataParameter)

                ' BIENNT - 29/12/2016 sửa lại hàm lấy danh sách chứng từ.
                strSQL = "SELECT nv.ten_dn,a.ly_do, a.ma_nh_ph,  a.ma_nh_ph, a.ten_nh_ph, a.kyhieu_ct, a.so_ct, a.ngay_ct, a.ngay_bn, a.ngay_bc, a.so_hs, a.ma_dvql, a.ten_dvql,  a.kyhieu_ct_pt, a.so_ct_pt, a.nam_ct_pt, a.nnt_mst, a.nnt_ten_dv,       a.nnt_diachi, a.nnt_tt_khac, a.tt_nt_ma_nt, a.tt_nt_tygia,       a.tt_nt_tongtien_nt, a.tt_nt_tongtien_vnd, a.chungtu_ct_stt,       a.chungtu_ct_ndkt, a.chungtu_ct_ten_ndkt, a.chungtu_ct_sotien_nt,       a.chungtu_ct_sotien_vnd, a.chungtu_ct_ghichu,       a.taikhoan_nt_ma_nh_th, a.taikhoan_nt_ten_nh_th,       a.taikhoan_nt_tk_th, a.taikhoan_nt_ten_tk_th, a.trangthai_xuly,       a.trangthai_ct, a.ma_nv, a.ma_ks, a.ngay_xuly, a.ly_do,       a.ngay_gdv_huy, a.ma_ks_huy, a.ngay_ks_huy, a.message_error,       a.ma_nv_huy, a.pttt, a.tk_thanhtoan, a.ten_tk_chuyen, a.so_du,       a.request_id, a.tt_nt_so_cmnd, a.tt_nt_so_dt, a.nnt_tinh_tp,       a.nnt_quan_huyen, a.tk_thanh_toan_tinh_tp,    a.tk_thanh_toan_quan_huyen,a.TK_THANH_TOAN_DIACHI, a.fee, a.vat, a.tongtien_truoc_vat, a.TAIKHOAN_NT_MA_NH_TT, a.TAIKHOAN_NT_TEN_NH_TT, (Select ten_dn From tcs_dm_nhanvien where ma_nv = a.ma_ks) ten_ks  FROM tcs_thue_phi_bo_nganh a, tcs_dm_nhanvien nv where a.ma_nv=nv.ma_nv(+) "
                'strSQL = "SELECT nv.ten_dn,a.ly_do, a.ma_nh_ph,  a.ma_nh_ph, a.ten_nh_ph, a.kyhieu_ct, a.so_ct, a.ngay_ct, a.ngay_bn, a.ngay_bc, a.so_hs, a.ma_dvql, a.ten_dvql,  a.kyhieu_ct_pt, a.so_ct_pt, a.nam_ct_pt, a.nnt_mst, a.nnt_ten_dv,       a.nnt_diachi, a.nnt_tt_khac, a.tt_nt_ma_nt, a.tt_nt_tygia,       a.tt_nt_tongtien_nt, a.tt_nt_tongtien_vnd, a.chungtu_ct_stt,       a.chungtu_ct_ndkt, a.chungtu_ct_ten_ndkt, a.chungtu_ct_sotien_nt,       a.chungtu_ct_sotien_vnd, a.chungtu_ct_ghichu,       a.taikhoan_nt_ma_nh_th, a.taikhoan_nt_ten_nh_th,       a.taikhoan_nt_tk_th, a.taikhoan_nt_ten_tk_th, a.trangthai_xuly,       a.trangthai_ct, a.ma_nv, a.ma_ks, a.ngay_xuly, a.ly_do,       a.ngay_gdv_huy, a.ma_ks_huy, a.ngay_ks_huy, a.message_error,       a.ma_nv_huy, a.pttt, a.tk_thanhtoan, a.ten_tk_chuyen, a.so_du,       a.request_id, a.tt_nt_so_cmnd, a.tt_nt_so_dt, a.nnt_tinh_tp,       a.nnt_quan_huyen, a.tk_thanh_toan_tinh_tp,    a.tk_thanh_toan_quan_huyen,a.TK_THANH_TOAN_DIACHI, a.fee, a.vat, a.tongtien_truoc_vat, a.TAIKHOAN_NT_MA_NH_TT, a.TAIKHOAN_NT_TEN_NH_TT   FROM tcs_thue_phi_bo_nganh a, tcs_dm_nhanvien nv , tcs_thue_phi_bo_nganh dc,  tcs_dm_nhanvien ks  where a.ma_nv=nv.ma_nv(+) and a.ma_ks=ks.ma_nv(+) and a.so_ct=dc.so_ct(+) and (a.ngay_ht = to_date(to_char(sysdate,'DD/MM/YYYY'),'dd/MM/yyyy') or (a.trangthai_ct in ('02','03','05') or (a.trangthai_ct ='01' and a.trangthai_xuly=0) )) "
                'arrParam.Add(DataAccess.NewDBParameter("ngay_ct", ParameterDirection.Input, ngay_ct, OracleClient.OracleType.DateTime))

                If strTrangThai_CT.Trim().Length > 0 Then
                    strSQL &= " and a.trangthai_ct = :tthai_ct"
                    arrParam.Add(DataAccess.NewDBParameter("tthai_ct", ParameterDirection.Input, strTrangThai_CT, OracleClient.OracleType.NVarChar))
                End If

                If TT_CT <> "-1" Then
                    If ma_ks.Trim().Length > 0 Then
                        strSQL = strSQL & " and a.trangthai_xuly ='" + TT_CT + "' "
                    End If
                End If

                If ma_ks.Trim().Length > 0 Then
                    strSQL = strSQL & " and a.ma_nv =" + ma_ks
                End If

                If ma_cn.Trim().Length > 0 Then
                    strSQL = strSQL & " and a.ma_cn =" + ma_cn
                End If
                strSQL = strSQL & " AND NOT (a.trangthai_ct = '04' AND TO_DATE(to_char(a.NGAY_HT, 'DD/MM/YYYY'), 'dd/mm/yyyy') < to_date(to_char(sysdate,'DD/MM/YYYY'),'dd/MM/yyyy') )"
                strSQL = strSQL & " AND NOT (a.trangthai_ct = '01' AND TO_DATE(to_char(a.NGAY_HT, 'DD/MM/YYYY'), 'dd/mm/yyyy') < to_date(to_char(sysdate,'DD/MM/YYYY'),'dd/MM/yyyy') )"
                strSQL = strSQL + " order by a.so_ct desc "
                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, arrParam.ToArray())


            Catch ex As Exception
            Finally
            End Try
            Return dtResult
        End Function


        Public Shared Function GET_CT_PHI_BO_NGANH(ByVal strSO_CT As String) As DataSet
            Dim strSQL As String
            Dim dtResult As DataSet = New DataSet()
            Try

                strSQL = ""
                Dim arrParam As List(Of IDataParameter) = New List(Of IDataParameter)

                strSQL = "SELECT p.*,dc.so_tn_ct,dc.ngay_tn_ct FROM tcs_thue_phi_bo_nganh p, tcs_dchieu_thue_phi_bo_nganh dc where p.so_ct = dc.so_ct(+) and p.so_ct=:so_ct order by p.so_ct desc"
                arrParam.Add(DataAccess.NewDBParameter("so_ct", ParameterDirection.Input, strSO_CT, OracleClient.OracleType.VarChar))

                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, arrParam.ToArray())

            Catch ex As Exception
            Finally
            End Try
            Return dtResult
        End Function

        Public Shared Function GET_GNT_PHI_BO_NGANH(ByVal strSO_CT As String) As DataSet
            Dim strSQL As String
            Dim dtResult As DataSet = New DataSet()
            Try

                strSQL = ""
                Dim arrParam As List(Of IDataParameter) = New List(Of IDataParameter)

                strSQL = "SELECT p.*,p.TRANGTHAI_CT ngay_hs, p.TRANGTHAI_CT ten_phi, p.TRANGTHAI_CT loai_nnt from tcs_thue_phi_bo_nganh p where p.so_ct=:so_ct"
                arrParam.Add(DataAccess.NewDBParameter("so_ct", ParameterDirection.Input, strSO_CT, OracleClient.OracleType.VarChar))

                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, arrParam.ToArray())

            Catch ex As Exception
            Finally
            End Try
            Return dtResult
        End Function


        Public Shared Function GET_GNT_PHI_BO_NGANH_DETAIL(ByVal strSO_CT As String) As DataSet
            Dim strSQL As String
            Dim dtResult As DataSet = New DataSet()
            Try

                strSQL = ""
                Dim arrParam As List(Of IDataParameter) = New List(Of IDataParameter)

                strSQL = "SELECT  p.so_ct_pt so_hs, to_char(p.ngay_ct,'DD/MM/YYYY') ngay_hs ,p.TT_NT_MA_NT loai_nt, dtl.ten_ndkt ten_phi, p.TRANGTHAI_CT loai_nnt , dtl.sotien_vnd so_tien from tcs_thue_phi_bo_nganh p inner join tcs_thue_phi_bo_nganh_dtl dtl on p.so_ct = dtl.so_ct where p.so_ct=:so_ct"
                arrParam.Add(DataAccess.NewDBParameter("so_ct", ParameterDirection.Input, strSO_CT, OracleClient.OracleType.VarChar))

                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, arrParam.ToArray())

            Catch ex As Exception
            Finally
            End Try
            Return dtResult
        End Function



        Public Shared Function UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(ByVal strSO_CT As String, ByVal strTrangThai_CT As String, ByVal ma_ks As String, ByVal TT_CT As Integer, ByVal lydo As String) As String
            Dim strSQL As String
            Dim aResult As ArrayList = New ArrayList()
            Try

                strSQL = ""
                Dim arrParam As List(Of IDataParameter) = New List(Of IDataParameter)

                strSQL = "update tcs_thue_phi_bo_nganh set trangthai_ct=:trangthai_ct, LYDO_HUY_SKV=:lydo, trangthai_xuly=:tt_ct, ngay_xuly = to_date(to_char(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY'), ngay_ht = to_date(to_char(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY') "
                arrParam.Add(DataAccess.NewDBParameter("trangthai_ct", ParameterDirection.Input, strTrangThai_CT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("lydo", ParameterDirection.Input, lydo, OracleClient.OracleType.NVarChar))
                arrParam.Add(DataAccess.NewDBParameter("tt_ct", ParameterDirection.Input, TT_CT, OracleClient.OracleType.NVarChar))
                If ma_ks.Trim.Length > 0 Then
                    strSQL &= ",ma_ks=:ma_ks"
                    arrParam.Add(DataAccess.NewDBParameter("ma_ks", ParameterDirection.Input, ma_ks, OracleClient.OracleType.Number))
                End If
                strSQL &= " where so_ct=:so_ct"
                arrParam.Add(DataAccess.NewDBParameter("so_ct", ParameterDirection.Input, strSO_CT, OracleClient.OracleType.VarChar))

                aResult = DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, arrParam.ToArray())

            Catch ex As Exception
                Throw ex
            Finally
            End Try
            Return aResult.ToString()
        End Function

        Public Shared Function UPDATE_TRANGTHAI_CT_PHI_BO_NGANH_NGAY_XL(ByVal strSO_CT As String, ByVal strTrangThai_CT As String, ByVal ma_ks As String, ByVal lydo_huy As String, ByVal PTTT As String, ByVal TK_THANHTOAN As String, ByVal TEN_TK_CHUYEN As String, ByVal SO_DU As String, ByVal TT_NT_SO_CMND As String, ByVal TT_NT_SO_DT As String, ByVal NNT_TINH_TP As String, ByVal NNT_QUAN_HUYEN As String, ByVal TK_THANH_TOAN_TINH_TP As String, ByVal TK_THANH_TOAN_QUAN_HUYEN As String, ByVal Ma_NH_PH As String, ByVal TEN_NH_PH As String, ByVal TK_THANH_TOAN_DIACHI As String, ByVal FEE As String, ByVal VAT As String, ByVal TONGTIEN_TRUOC_VAT As String, ByVal TAIKHOAN_NT_MA_NH_TT As String, ByVal TAIKHOAN_NT_TEN_NH_TT As String) As String

            Dim strSQL As String
            Dim aResult As ArrayList = New ArrayList()
            Try

                strSQL = ""
                Dim arrParam As List(Of IDataParameter) = New List(Of IDataParameter)

                strSQL = "update tcs_thue_phi_bo_nganh set trangthai_ct=:trangthai_ct, ngay_xuly = to_date(to_char(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY'), NGAY_GDV_HUY=to_date(to_char(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY'), pttt =:PTTT, TK_THANHTOAN=:TK_THANHTOAN, TEN_TK_CHUYEN=:TEN_TK_CHUYEN, SO_DU=:SO_DU, TT_NT_SO_CMND=:TT_NT_SO_CMND, TT_NT_SO_DT=:TT_NT_SO_DT, NNT_TINH_TP=:NNT_TINH_TP,NNT_QUAN_HUYEN=:NNT_QUAN_HUYEN,TK_THANH_TOAN_TINH_TP=:TK_THANH_TOAN_TINH_TP,TK_THANH_TOAN_QUAN_HUYEN=:TK_THANH_TOAN_QUAN_HUYEN , Ma_NH_PH=:MA_NH_PH, Ten_NH_PH=:TEN_NH_PH,TK_THANH_TOAN_DIACHI=:TK_THANH_TOAN_DIACHI, FEE=:FEE, VAT=:VAT, TONGTIEN_TRUOC_VAT=:TONGTIEN_TRUOC_VAT, TAIKHOAN_NT_MA_NH_TT=:TAIKHOAN_NT_MA_NH_TT, TAIKHOAN_NT_TEN_NH_TT=:TAIKHOAN_NT_TEN_NH_TT, ngay_ht = to_date(to_char(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY') "

                arrParam.Add(DataAccess.NewDBParameter("trangthai_ct", ParameterDirection.Input, strTrangThai_CT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("PTTT", ParameterDirection.Input, PTTT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TK_THANHTOAN", ParameterDirection.Input, TK_THANHTOAN, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TEN_TK_CHUYEN", ParameterDirection.Input, TEN_TK_CHUYEN, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("SO_DU", ParameterDirection.Input, SO_DU, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TT_NT_SO_CMND", ParameterDirection.Input, TT_NT_SO_CMND, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TT_NT_SO_DT", ParameterDirection.Input, TT_NT_SO_DT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("NNT_TINH_TP", ParameterDirection.Input, NNT_TINH_TP, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("NNT_QUAN_HUYEN", ParameterDirection.Input, NNT_QUAN_HUYEN, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TK_THANH_TOAN_TINH_TP", ParameterDirection.Input, TK_THANH_TOAN_TINH_TP, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TK_THANH_TOAN_QUAN_HUYEN", ParameterDirection.Input, TK_THANH_TOAN_QUAN_HUYEN, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("MA_NH_PH", ParameterDirection.Input, Ma_NH_PH, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TEN_NH_PH", ParameterDirection.Input, TEN_NH_PH, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TK_THANH_TOAN_DIACHI", ParameterDirection.Input, TK_THANH_TOAN_DIACHI, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("FEE", ParameterDirection.Input, FEE, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("VAT", ParameterDirection.Input, VAT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TONGTIEN_TRUOC_VAT", ParameterDirection.Input, TONGTIEN_TRUOC_VAT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TAIKHOAN_NT_MA_NH_TT", ParameterDirection.Input, TAIKHOAN_NT_MA_NH_TT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TAIKHOAN_NT_TEN_NH_TT", ParameterDirection.Input, TAIKHOAN_NT_TEN_NH_TT, OracleClient.OracleType.VarChar))

                'Select 

                If ma_ks.Trim.Length > 0 Then
                    strSQL &= ",MA_NV_HUY=:MA_NV_HUY"
                    arrParam.Add(DataAccess.NewDBParameter("MA_NV_HUY", ParameterDirection.Input, ma_ks, OracleClient.OracleType.Number))
                End If
                If lydo_huy.Length > 0 Then
                    strSQL &= ", ly_do=:lydo"
                    arrParam.Add(DataAccess.NewDBParameter("lydo", ParameterDirection.Input, lydo_huy, OracleClient.OracleType.NVarChar))

                End If

                strSQL &= " where so_ct=:so_ct"
                arrParam.Add(DataAccess.NewDBParameter("so_ct", ParameterDirection.Input, strSO_CT, OracleClient.OracleType.VarChar))
                aResult = DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, arrParam.ToArray())


            Catch ex As Exception

                Return ex.Message
            Finally
            End Try
            Return aResult.ToString()
        End Function



        Public Shared Function UPDATE_TRANGTHAI_CT_PHI_BO_NGANH_NGAY_XL(ByVal strSO_CT As String, ByVal strTrangThai_CT As String, ByVal ma_ks As String, ByVal PTTT As String, ByVal TK_THANHTOAN As String, ByVal TEN_TK_CHUYEN As String, ByVal SO_DU As String, ByVal TT_NT_SO_CMND As String, ByVal TT_NT_SO_DT As String, ByVal NNT_TINH_TP As String, ByVal NNT_QUAN_HUYEN As String, ByVal TK_THANH_TOAN_TINH_TP As String, ByVal TK_THANH_TOAN_QUAN_HUYEN As String, ByVal Ma_NH_PH As String, ByVal TEN_NH_PH As String, ByVal TK_THANH_TOAN_DIACHI As String, ByVal Ma_NT As String, ByVal TyGia As String, ByVal TongTien_NT As String, ByVal TongTien_VND As String, ByVal Ma_NH_TH As String, ByVal Ten_NH_TH As String, ByVal TaiKhoan_TH As String, ByVal Ten_TaiKhoan_TH As String, ByVal FEE As String, ByVal VAT As String, ByVal TONGTIEN_TRUOC_VAT As String, ByVal ListChiTiet As List(Of Business_HQ.VM_ChiTiet_CT)) As String
            Dim strSQL As String
            Dim aResult As ArrayList = New ArrayList()
            Try

                strSQL = ""
                Dim arrParam As List(Of IDataParameter) = New List(Of IDataParameter)
                arrParam.Add(DataAccess.NewDBParameter("PTTT", ParameterDirection.Input, PTTT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TK_THANHTOAN", ParameterDirection.Input, TK_THANHTOAN, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TEN_TK_CHUYEN", ParameterDirection.Input, TEN_TK_CHUYEN, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("SO_DU", ParameterDirection.Input, SO_DU, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TT_NT_SO_CMND", ParameterDirection.Input, TT_NT_SO_CMND, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TT_NT_SO_DT", ParameterDirection.Input, TT_NT_SO_DT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("NNT_TINH_TP", ParameterDirection.Input, NNT_TINH_TP, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("NNT_QUAN_HUYEN", ParameterDirection.Input, NNT_QUAN_HUYEN, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TK_THANH_TOAN_TINH_TP", ParameterDirection.Input, TK_THANH_TOAN_TINH_TP, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TK_THANH_TOAN_QUAN_HUYEN", ParameterDirection.Input, TK_THANH_TOAN_QUAN_HUYEN, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("MA_NH_PH", ParameterDirection.Input, Ma_NH_PH, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TEN_NH_PH", ParameterDirection.Input, TEN_NH_PH, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TK_THANH_TOAN_DIACHI", ParameterDirection.Input, TK_THANH_TOAN_DIACHI, OracleClient.OracleType.VarChar))

                'Ma_NT,TyGia,TongTien_NT,TongTien_VND,Ma_NH_TH,Ten_NH_TH,TaiKhoan_TH,Ten_TaiKhoan_TH

                arrParam.Add(DataAccess.NewDBParameter("TT_NT_MA_NT", ParameterDirection.Input, Ma_NT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TT_NT_TYGIA", ParameterDirection.Input, TyGia, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TT_NT_TONGTIEN_NT", ParameterDirection.Input, TongTien_NT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TT_NT_TONGTIEN_VND", ParameterDirection.Input, TongTien_VND, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TAIKHOAN_NT_MA_NH_TH", ParameterDirection.Input, Ma_NH_TH, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TAIKHOAN_NT_TEN_NH_TH", ParameterDirection.Input, Ten_NH_TH, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TAIKHOAN_NT_TK_TH", ParameterDirection.Input, TaiKhoan_TH, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TAIKHOAN_NT_TEN_TK_TH", ParameterDirection.Input, Ten_TaiKhoan_TH, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("FEE", ParameterDirection.Input, FEE, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("VAT", ParameterDirection.Input, VAT, OracleClient.OracleType.VarChar))
                arrParam.Add(DataAccess.NewDBParameter("TONGTIEN_TRUOC_VAT", ParameterDirection.Input, TONGTIEN_TRUOC_VAT, OracleClient.OracleType.VarChar))



                strSQL = "update tcs_thue_phi_bo_nganh set trangthai_ct=:trangthai_ct,  pttt =:PTTT, TK_THANHTOAN=:TK_THANHTOAN, TEN_TK_CHUYEN=:TEN_TK_CHUYEN, SO_DU=:SO_DU, TT_NT_SO_CMND=:TT_NT_SO_CMND, TT_NT_SO_DT=:TT_NT_SO_DT, NNT_TINH_TP=:NNT_TINH_TP,NNT_QUAN_HUYEN=:NNT_QUAN_HUYEN,TK_THANH_TOAN_TINH_TP=:TK_THANH_TOAN_TINH_TP,TK_THANH_TOAN_QUAN_HUYEN=:TK_THANH_TOAN_QUAN_HUYEN,  Ma_NH_PH=:MA_NH_PH, Ten_NH_PH=:TEN_NH_PH,TK_THANH_TOAN_DIACHI=:TK_THANH_TOAN_DIACHI,TT_NT_MA_NT =:TT_NT_MA_NT,TT_NT_TYGIA=:TT_NT_TYGIA, TT_NT_TONGTIEN_NT=:TT_NT_TONGTIEN_NT, TT_NT_TONGTIEN_VND=:TT_NT_TONGTIEN_VND,   TAIKHOAN_NT_MA_NH_TH=: TAIKHOAN_NT_MA_NH_TH, TAIKHOAN_NT_TEN_NH_TH =: TAIKHOAN_NT_TEN_NH_TH, TAIKHOAN_NT_TK_TH =: TAIKHOAN_NT_TK_TH, TAIKHOAN_NT_TEN_TK_TH =: TAIKHOAN_NT_TEN_TK_TH, FEE=:FEE, VAT=:VAT, TONGTIEN_TRUOC_VAT=:TONGTIEN_TRUOC_VAT, ngay_ht = to_date(to_char(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY')"

                arrParam.Add(DataAccess.NewDBParameter("trangthai_ct", ParameterDirection.Input, strTrangThai_CT, OracleClient.OracleType.VarChar))
                If ma_ks.Trim.Length > 0 Then
                    strSQL &= ",ma_nv=:ma_ks"
                    arrParam.Add(DataAccess.NewDBParameter("ma_ks", ParameterDirection.Input, ma_ks, OracleClient.OracleType.Number))
                End If
                strSQL &= " where so_ct=:so_ct"
                arrParam.Add(DataAccess.NewDBParameter("so_ct", ParameterDirection.Input, strSO_CT, OracleClient.OracleType.VarChar))
                aResult = DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, arrParam.ToArray())

                'Delete Chung Tu
                If Delete_SoCT(strSO_CT) Then
                    For Each chitiet_CT As VM_ChiTiet_CT In ListChiTiet
                        Dim Id As Int64 = HaiQuanController.GenSequenceKeyTable("TCS_CTU_DTL_SEQ.NEXTVAL")
                        InsertChiTiet_CT(Id.ToString(), strSO_CT, chitiet_CT.STT.ToString(), chitiet_CT.NDKT, chitiet_CT.Ten_NDKT, chitiet_CT.SoTien_NT.ToString(), chitiet_CT.SoTien_VND.ToString(), chitiet_CT.GhiChu)
                    Next
                End If

            Catch ex As Exception
                Throw ex
            Finally
            End Try
            Return aResult.ToString()
        End Function

        Public Shared Function InsertChiTiet_CT(ByVal id As String, ByVal so_ct As String, ByVal stt As String, ByVal ndkt As String, ByVal ten_ndkt As String, ByVal SoTien_NT As String, ByVal SoTien_VND As String, ByVal GhiChu As String) As Boolean
            Dim strSql As String = "INSERT INTO tcs_thue_phi_bo_nganh_dtl(id,so_ct,stt,ndkt,ten_ndkt,sotien_nt, sotien_vnd, ghichu)VALUES (" + id + ", '" + so_ct + "',   " + stt + ", '" + ndkt + "', '" + ten_ndkt + "', " + SoTien_NT + ", " + SoTien_VND + ", '" + GhiChu + "') "
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            'Dim strSql As String = "INSERT INTO tcs_thue_phi_bo_nganh " & _
            '                        "            (id,so_ct,stt,ndkt,ten_ndkt,sotien_nt, sotien_vnd, ghichu) " & _
            '                        "     VALUES ('" + id + "', '" + so_ct + "', " & _
            '                        "             '" + stt + "', '" + ndkt + "', '" + ten_ndkt + "', '" + SoTien_NT + "', '" + SoTien_VND + "', '" + GhiChu + "') "

            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Thêm mới chi tiết chứng từ thành công")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Shared Function Delete_SoCT(ByVal So_CT As String) As Boolean
            Dim dataAccess As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_THUE_PHI_BO_NGANH_DTL " & _
                           "WHERE so_ct='" & So_CT & "'"
            Try
                dataAccess = New DataAccess
                dataAccess.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi xóa chứng từ chi tiết")
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá tài khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not dataAccess Is Nothing Then dataAccess.Dispose()
            End Try
            Return blnResult
        End Function

        Public Shared Function ListChiTietBySoCT(ByVal so_ct As String) As DataSet
            Dim strSQL As String = String.Empty
            Dim ListTyGiaNT As List(Of Business_HQ.VM_TyGia) = New List(Of VM_TyGia)
            Dim dtResult As DataSet = New DataSet()
            Try
                strSQL = "select p.*, ROWNUM sSTT from tcs_thue_phi_bo_nganh_dtl p  where p.so_ct = '" + so_ct + "'"
                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
            Finally
            End Try
            Return dtResult
        End Function

        Public Shared Function getTaiKhoanTrucTiep(ByVal ma_nh_th As String) As DataSet
            Dim strSQL As String = String.Empty
            Dim dtResult As DataSet = New DataSet()
            Try
                strSQL = "SELECT P.MA_NGANHANG_TT, P.TEN_NGANHANG_TT FROM TCS_MAP_NGANHANG P  WHERE P.MA_NGANHANG_TH = '" + ma_nh_th + "'"
                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
            Finally
            End Try
            Return dtResult
        End Function

        Public Shared Function ListTyGia() As List(Of Business_HQ.VM_TyGia)
            Dim strSQL As String = String.Empty
            Dim ListTyGiaNT As List(Of Business_HQ.VM_TyGia) = New List(Of VM_TyGia)
            Dim dtResult As DataSet = New DataSet()
            Try

                strSQL = "select b.Ma_NT Ma_NT, a.Ty_Gia TyGia from tcs_dm_tygia a join tcs_dm_nguyente b on a.Ma_NT = b.Ma_NT"
                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not dtResult Is Nothing Then
                    For Each row As DataRow In dtResult.Tables(0).Rows
                        Dim tyGia As VM_TyGia = New VM_TyGia()
                        tyGia.TEN_NT = row("Ma_NT")
                        tyGia.TYGIA = row("TyGia")
                        ListTyGiaNT.Add(tyGia)
                    Next
                Else
                    Return ListTyGiaNT
                End If
            Catch ex As Exception
            Finally
            End Try
            Return ListTyGiaNT
        End Function

        Public Shared Function ListNDKT() As List(Of Business_HQ.VM_NDKT)
            Dim strSQL As String = String.Empty
            Dim List_VM_NDKT As List(Of Business_HQ.VM_NDKT) = New List(Of VM_NDKT)
            Dim dtResult As DataSet = New DataSet()
            Try

                strSQL = "select ma_tmuc, ten from tcs_dm_muc_tmuc"
                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not dtResult Is Nothing Then
                    For Each row As DataRow In dtResult.Tables(0).Rows
                        Dim ndkt As VM_NDKT = New VM_NDKT()
                        ndkt.MA_TMUC = row("ma_tmuc")
                        ndkt.TEN_TMUC = row("ten")
                        List_VM_NDKT.Add(ndkt)
                    Next
                Else
                    Return List_VM_NDKT
                End If
            Catch ex As Exception
            Finally
            End Try
            Return List_VM_NDKT
        End Function

        Public Shared Function GET_DM_CHINHANH(ByVal ma_cn As String) As P_VM_ChiNhanh
            Dim strSQL As String

            Dim m_vm_chinhanh As P_VM_ChiNhanh = New P_VM_ChiNhanh()
            Dim listDMChiNhanh As New List(Of Business_HQ.VM_ChiNhanh)

            Dim dtResult As DataSet = New DataSet()
            Try

                strSQL = ""
                strSQL = "SELECT ID, NAME from tcs_dm_chinhanh start with ID='" + ma_cn + "' connect by nocycle PRIOR SUBSTR(id, 2, LENGTH(id) - 2) = branch_ID"

                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)

                For Each row As DataRow In dtResult.Tables(0).Rows
                    Dim chiTiet As New VM_ChiNhanh()
                    chiTiet.ID = row("ID")
                    chiTiet.NAME = row("NAME")
                    listDMChiNhanh.Add(chiTiet)
                Next
                m_vm_chinhanh.ListChiNhanh = listDMChiNhanh
                m_vm_chinhanh.RootID = ma_cn

            Catch ex As Exception
            Finally
            End Try
            Return m_vm_chinhanh
        End Function

        Public Shared Function GET_DM_CHINHANHFORPHIBONGANH(ByVal ma_cn As String, ByVal typeHSC As String) As P_VM_ChiNhanh
            Dim strSQL As String

            Dim m_vm_chinhanh As P_VM_ChiNhanh = New P_VM_ChiNhanh()
            Dim listDMChiNhanh As New List(Of Business_HQ.VM_ChiNhanh)

            Dim dtResult As DataSet = New DataSet()
            Try
                strSQL = ""
                strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a "
                'Tạm thời rào lại chưa cần check theo User
                'If typeHSC = "1" Then
                '    strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a "
                'Else
                '    strSQL = "SELECT ID, NAME from tcs_dm_chinhanh start with ID='" + ma_cn + "' connect by nocycle PRIOR SUBSTR(id, 2, LENGTH(id) - 2) = branch_ID"
                'End If

                dtResult = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)

                For Each row As DataRow In dtResult.Tables(0).Rows
                    Dim chiTiet As New VM_ChiNhanh()
                    chiTiet.ID = row("ID")
                    chiTiet.NAME = row("NAME")
                    listDMChiNhanh.Add(chiTiet)
                Next
                m_vm_chinhanh.ListChiNhanh = listDMChiNhanh
                m_vm_chinhanh.RootID = ma_cn

            Catch ex As Exception
            Finally
            End Try
            Return m_vm_chinhanh
        End Function

        Public Shared Function GetCategoryLoaiTienThue() As DataTable
            Dim sRet As DataTable = Nothing
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT MA_LT, MA_LT ||'-'||ten_lt ten_lt from tcs_dm_loaitien_thue"
                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                sRet = dt
            Catch ex As Exception
                sRet = Nothing
            End Try
            Return sRet
        End Function

        Public Shared Function GetCODE_TIENTE(ByVal ma_nt As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT CODE_TIENTE from Tcs_dm_nguyente where MA_NT='" & ma_nt & "'"
                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                sRet = dt.Rows(0)("CODE_TIENTE").ToString
            Catch ex As Exception
                sRet = Nothing
            End Try
            Return sRet
        End Function
    End Class

End Namespace