﻿Namespace NewChungTu
    Public Class infChungTu
        Private _HDR As infChungTuHDR
        Private _ListDTL As List(Of infChungTuDTL)

        Public Property HDR() As infChungTuHDR
            Get
                Return _HDR
            End Get
            Set(ByVal value As infChungTuHDR)
                _HDR = value
            End Set
        End Property

        Public Property ListDTL() As List(Of infChungTuDTL)
            Get
                Return _ListDTL
            End Get
            Set(ByVal value As List(Of infChungTuDTL))
                _ListDTL = value
            End Set
        End Property

        Public Sub New()
        End Sub

        Public Sub New(ByVal HDR As infChungTuHDR, ByVal ListDTL As List(Of infChungTuDTL))
            Me.HDR = HDR
            Me.ListDTL = ListDTL
        End Sub
    End Class
End Namespace

