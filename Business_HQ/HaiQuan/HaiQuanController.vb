﻿Imports System
Imports System.Data
Imports VBOracleLib
Imports System.Text
Imports Common
Imports Business_HQ.NewChungTu
Imports System.Globalization
Imports System.Data.OracleClient
Imports Business

Namespace HaiQuan
    Public Class HaiQuanController
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Shared Function fnc_getTCS_LOG_MSG_HQ(ByVal pStrID As String, ByVal pStrCol As String) As String
            Dim strResult As String = ""
            Try
                Dim strSQL As String = "SELECT " & pStrCol & " FROM TCS_LOG_MSG_HQ WHERE ID='" & pStrID & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            strResult = ds.Tables(0).Rows(0)(pStrCol).ToString()
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
            Return strResult
        End Function
        ''' <summary>
        ''' Lấy các tham số để tính phí và VAT giao dịch
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>sonmt</remarks>
        Public Shared Function PopulateHangSoTinhPhi() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "select ten_ts,giatri_ts from tcs_thamso_ht where ten_ts in('FEE_RATE','FEE_MAX','FEE_MIN','VAT_RATE','VAT_MAX','VAT_MIN')"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()

                    For index As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" var " & dt.Rows(index)("ten_ts").ToString & " = '" & dt.Rows(index)("giatri_ts").ToString & "';")
                    Next

                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Public Shared Function PopulateArrSacThue() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select SAC_THUE,GIA_TRI From tcs_map_st Where 1=1"
                'Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrSacThue = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrSacThue[" & i & "] ='" & dt.Rows(i)("SAC_THUE") & ";" & dt.Rows(i)("GIA_TRI") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Public Shared Function GetChungTu(ByVal sSoChungTu As String) As NewChungTu.infChungTu
            Dim oRet As NewChungTu.infChungTu = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Dim dDateTmp As Date
            Try
                'get HDR
                Dim HDR As New NewChungTu.infChungTuHDR
                sSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV, a.Ly_do_huy, " & _
                       "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                       "a.KyHieu_CT,a.So_CT,a.So_CT_NH,a.SEQ_NO,a.so_xcard," & _
                       "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                       "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
                       "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD,a.TK_GL_NH," & _
                       "a.Ngay_QD ,a.CQ_QD,a.Ngay_CT," & _
                       "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh,tinh.ten ten_tinh," & _
                       "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co,a.ngay_ht," & _
                       "a.So_TK, a.Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                       "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                       "a.So_Khung,a.So_May,a.TK_KH_NH,a.NGAY_KH_NH,a.TEN_KH_NH as TEN_KH_NH," & _
                       "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu, a.MA_HUYEN_NNTHUE, a.MA_TINH_NNTHUE, " & _
                       "(select xa1.TEN from TCS_DM_XA xa1 where xa1.MA_XA = a.MA_HUYEN_NNTHUE) HUYEN_NNTHUE, " & _
                       "(select xa2.TEN from TCS_DM_XA xa2 where xa2.MA_XA = a.MA_TINH_NNTHUE) TINH_NNTHUE, " & _
                       "a.Lan_In,a.Trang_Thai,a.so_bt_ktkb, a.TT_CTHUE,a.ma_sanpham, a.MA_QUAN_HUYENNNTIEN, a.MA_TINH_TPNNTIEN, " & _
                       "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT,a.TT_CITAD,A.SO_CMND,A.SO_FONE,A.MA_CUC,A.TEN_CUC, " & _
                       "nha.TEN_NH_CHUYEN as TEN_NH_A, a.ten_nh_b, (Select TEN_LH  From TCS_DM_LHINH Where TCS_DM_LHINH.Ma_LH= a.LH_XNK) as Ten_LHXNK, " & _
                       "xa.ten as TenXa, a.MA_NTK,a.time_begin,a.ngay_ks, a.PO_DATE, a.NGAY_BAONO, a.NGAY_BAOCO, " & _
                       "cqthu.ten as Ten_CQThu, nt.ten as Ten_NT, lt.ten as Ten_LThue,a.PT_TT as PT_TT,a.isactive,a.tt_tthu,a.ma_tq, " & _
                       "a.So_BK, a.Ngay_BK,tcs_dm_khobac.ten as ten_kb,a.PHI_GD,A.PHI_VAT,a.PHI_GD2,A.PHI_VAT2,a.PT_TINHPHI,a.RM_REF_NO,a.REF_NO, " & _
                       "a.QUAN_HUYENNNTIEN, a.TINH_TPNNTIEN, a.MA_HQ, a.LOAI_TT, a.TEN_HQ,a.REMARKS,a.ghi_chu, a.MA_HQ_PH, a.TEN_HQ_PH, a.MA_NH_TT, a.ten_nh_tt,a.MA_SANPHAM, a.MSG_LOI, a.LOAI_CTU, a.ghi_chu, a.dien_giai_hq, a.ma_cn, a.LOAI_NNT, a.SO_CMND, a.SO_FONE " & _
                       "from TCS_CTU_HDR a, TCS_DM_XA xa,TCS_DM_XA tinh, " & _
                       "TCS_DM_CQTHU cqthu, TCS_DM_NGUYENTE nt, TCS_DM_LTHUE lt, TCS_NGANHANG_CHUYEN nha, tcs_dm_nh_giantiep_tructiep nhb, tcs_dm_nh_giantiep_tructiep nhtt, tcs_dm_khobac " & _
                       "where (a.xa_id = xa.xa_id(+)) and a.Ma_Tinh=tinh.ma_tinh(+) " & _
                       " and (a.ma_nh_a = nha.MA_NH_CHUYEN(+)) and (a.ma_nh_b = nhb.ma_giantiep(+)) and (a.ma_nh_tt = nhtt.ma_tructiep(+)) and (a.SHKB=nhb.shkb(+)) and (a.SHKB=nhtt.shkb(+))  " & _
                       " and (a.ma_cqthu = cqthu.ma_cqthu(+)) and (a.ma_nt = nt.ma_nt(+)) and (a.ma_lthue = lt.ma_lthue(+)) " & _
                       " and (a.shkb = tcs_dm_khobac.shkb(+)) " & _
                       " and (a.SO_CT = :SO_CT) "
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    HDR.So_CT = dt.Rows(0)("So_CT").ToString()
                    HDR.LOAI_NNT = dt.Rows(0)("LOAI_NNT").ToString()
                    HDR.LOAI_CTU = dt.Rows(0)("LOAI_CTU").ToString()
                    HDR.Trang_Thai = dt.Rows(0)("Trang_Thai").ToString()
                    HDR.TT_BDS = dt.Rows(0)("ISACTIVE").ToString()
                    HDR.TT_NSTT = dt.Rows(0)("TT_TTHU").ToString()
                    If dt.Rows(0)("TT_TTHU") Is Nothing OrElse IsDBNull(dt.Rows(0)("TT_TTHU")) Then
                        HDR.TT_TThu = 0
                    Else
                        HDR.TT_TThu = dt.Rows(0)("TT_TTHU")
                    End If
                    If dt.Rows(0)("MA_NV") Is Nothing OrElse IsDBNull(dt.Rows(0)("MA_NV")) Then
                        HDR.Ma_NV = 0
                    Else
                        HDR.Ma_NV = dt.Rows(0)("MA_NV")
                    End If
                    If dt.Rows(0)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(0)("So_BT")) Then
                        HDR.So_BT = 0
                    Else
                        HDR.So_BT = dt.Rows(0)("So_BT")
                    End If
                    HDR.SHKB = dt.Rows(0)("SHKB").ToString()
                    HDR.Ten_kb = dt.Rows(0)("TEN_KB").ToString()
                    If dt.Rows(0)("TTIEN_NT") Is Nothing OrElse IsDBNull(dt.Rows(0)("TTIEN_NT")) Then
                        HDR.TTien_NT = 0
                    Else
                        HDR.TTien_NT = dt.Rows(0)("TTIEN_NT")
                    End If
                    HDR.MA_HQ = dt.Rows(0)("MA_HQ").ToString()
                    HDR.LOAI_TT = dt.Rows(0)("LOAI_TT").ToString()
                    HDR.TEN_HQ = dt.Rows(0)("TEN_HQ").ToString()
                    HDR.Ma_hq_ph = dt.Rows(0)("MA_HQ_PH").ToString()
                    HDR.TEN_HQ_PH = dt.Rows(0)("TEN_HQ_PH").ToString()
                    If dt.Rows(0)("NGAY_KB") Is Nothing OrElse IsDBNull(dt.Rows(0)("NGAY_KB")) Then
                        HDR.Ngay_KB = 0
                    Else
                        HDR.Ngay_KB = dt.Rows(0)("NGAY_KB")
                    End If
                    HDR.TK_Co = dt.Rows(0)("TK_Co").ToString()
                    HDR.TK_No = dt.Rows(0)("TK_No").ToString()
                    'ma va ten DBHC
                    HDR.XA_ID = dt.Rows(0)("XA_ID").ToString()
                    HDR.Ma_Xa = dt.Rows(0)("MA_XA").ToString()
                    HDR.Ma_Huyen = dt.Rows(0)("MA_HUYEN").ToString()
                    HDR.Ma_Tinh = dt.Rows(0)("MA_TINH").ToString()
                    HDR.Ten_Xa = dt.Rows(0)("TenXa").ToString()
                    HDR.Ma_NNThue = dt.Rows(0)("MA_NNTHUE").ToString()
                    HDR.Ten_NNThue = dt.Rows(0)("TEN_NNTHUE").ToString()
                    HDR.DC_NNThue = dt.Rows(0)("DC_NNTHUE").ToString()
                    HDR.Ma_Huyen_NNThue = dt.Rows(0)("MA_HUYEN_NNTHUE").ToString()
                    HDR.Huyen_nnthue = dt.Rows(0)("quan_huyennntien").ToString()
                    HDR.Ma_Tinh_NNThue = dt.Rows(0)("MA_TINH_NNTHUE").ToString()
                    HDR.Tinh_nnthue = dt.Rows(0)("tinh_tpnntien").ToString()
                    HDR.Ma_NNTien = dt.Rows(0)("Ma_NNTien").ToString()
                    HDR.Ten_NNTien = dt.Rows(0)("TEN_NNTIEN").ToString()
                    HDR.DC_NNTien = dt.Rows(0)("DC_NNTIEN").ToString()
                    HDR.MA_QUAN_HUYENNNTIEN = dt.Rows(0)("MA_QUAN_HUYENNNTIEN").ToString()
                    HDR.QUAN_HUYEN_NNTIEN = dt.Rows(0)("QUAN_HUYENNNTIEN").ToString()
                    HDR.MA_TINH_TPNNTIEN = dt.Rows(0)("MA_TINH_TPNNTIEN").ToString()
                    HDR.TINH_TPHO_NNTIEN = dt.Rows(0)("TINH_TPNNTIEN").ToString()
                    HDR.Ten_tinh = dt.Rows(0)("Ten_Tinh").ToString()
                    HDR.KHCT = dt.Rows(0)("KyHieu_CT").ToString()
                    HDR.KyHieu_CT = dt.Rows(0)("KyHieu_CT").ToString()
                    HDR.Ma_NT = dt.Rows(0)("Ma_NT").ToString()
                    HDR.Ten_nt = dt.Rows(0)("Ten_NT").ToString()
                    HDR.Ma_CQThu = dt.Rows(0)("MA_CQTHU").ToString()
                    HDR.Ten_cqthu = dt.Rows(0)("TEN_CQTHU").ToString()
                    HDR.LH_XNK = dt.Rows(0)("LH_XNK").ToString()
                    HDR.Lhxnk = dt.Rows(0)("LH_XNK").ToString()
                    HDR.Ten_lhxnk = dt.Rows(0)("Ten_LHXNK").ToString()
                    HDR.So_Khung = dt.Rows(0)("SO_KHUNG").ToString()
                    HDR.So_May = dt.Rows(0)("SO_MAY").ToString()
                    HDR.So_TK = dt.Rows(0)("SO_TK").ToString()
                    If dt.Rows(0)("NGAY_TK") IsNot Nothing AndAlso dt.Rows(0)("NGAY_TK") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("NGAY_TK")
                        HDR.Ngay_TK = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.Ngay_TK = String.Empty
                    End If
                    If dt.Rows(0)("Ngay_QD") IsNot Nothing AndAlso dt.Rows(0)("Ngay_QD") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("Ngay_QD")
                        HDR.Ngay_QD = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.Ngay_QD = String.Empty
                    End If
                    HDR.So_BK = dt.Rows(0)("So_BK").ToString()
                    If dt.Rows(0)("Ngay_BK") IsNot Nothing AndAlso dt.Rows(0)("Ngay_BK") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("Ngay_BK")
                        HDR.Ngay_BK = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.Ngay_BK = String.Empty
                    End If
                    HDR.So_CT_NH = dt.Rows(0)("SO_CT_NH").ToString()
                    HDR.TK_KH_NH = dt.Rows(0)("TK_KH_NH").ToString()
                    HDR.TEN_KH_NH = dt.Rows(0)("TEN_KH_NH").ToString()
                    If dt.Rows(0)("NGAY_KH_NH") IsNot Nothing AndAlso dt.Rows(0)("NGAY_KH_NH") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("NGAY_KH_NH")
                        HDR.NGAY_KH_NH = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.NGAY_KH_NH = String.Empty
                    End If
                    If dt.Rows(0)("TIME_BEGIN") IsNot Nothing AndAlso dt.Rows(0)("TIME_BEGIN") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("TIME_BEGIN")
                        HDR.TIME_BEGIN = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.TIME_BEGIN = String.Empty
                    End If
                    If dt.Rows(0)("NGAY_BAONO") IsNot Nothing AndAlso dt.Rows(0)("NGAY_BAONO") IsNot DBNull.Value Then
                        HDR.NGAY_BAONO = dt.Rows(0)("NGAY_BAONO")
                    End If
                    If dt.Rows(0)("NGAY_BAOCO") IsNot Nothing AndAlso dt.Rows(0)("NGAY_BAOCO") IsNot DBNull.Value Then
                        HDR.NGAY_BAOCO = dt.Rows(0)("NGAY_BAOCO")
                    End If

                    HDR.PT_TT = dt.Rows(0)("PT_TT").ToString()
                    HDR.MA_NH_A = dt.Rows(0)("MA_NH_A").ToString()
                    HDR.Ten_nh_a = dt.Rows(0)("Ten_NH_A").ToString()
                    HDR.MA_NH_B = dt.Rows(0)("MA_NH_B").ToString()
                    HDR.Ten_NH_B = dt.Rows(0)("Ten_NH_B").ToString()
                    HDR.MA_NH_TT = dt.Rows(0)("MA_NH_TT").ToString()
                    HDR.TEN_NH_TT = dt.Rows(0)("TEN_NH_TT").ToString()
                    If dt.Rows(0)("TT_CTHUE") Is Nothing OrElse IsDBNull(dt.Rows(0)("TT_CTHUE")) Then
                        HDR.TT_CTHUE = 0
                    Else
                        HDR.TT_CTHUE = dt.Rows(0)("TT_CTHUE")
                    End If
                    HDR.Ma_LThue = dt.Rows(0)("MA_LTHUE").ToString()
                    HDR.PHI_GD = dt.Rows(0)("PHI_GD").ToString()
                    HDR.PHI_VAT = dt.Rows(0)("PHI_VAT").ToString()
                    HDR.PHI_GD2 = dt.Rows(0)("PHI_GD2").ToString()
                    HDR.PHI_VAT2 = dt.Rows(0)("PHI_VAT2").ToString()
                    HDR.PT_TINHPHI = dt.Rows(0)("PT_TINHPHI").ToString()
                    HDR.Ma_KS = dt.Rows(0)("MA_KS").ToString()
                    HDR.SAN_PHAM = dt.Rows(0)("MA_SANPHAM").ToString()
                    HDR.TT_CITAD = dt.Rows(0)("TT_CITAD").ToString()
                    'HDR.Ly_Do = dt.Rows(0)("ly_do_huy").ToString()
                    HDR.Ly_Do = dt.Rows(0)("ly_do").ToString()
                    HDR.SO_CMND = dt.Rows(0)("SO_CMND").ToString()
                    HDR.SO_FONE = dt.Rows(0)("SO_FONE").ToString()
                    HDR.MA_NTK = dt.Rows(0)("MA_NTK").ToString()
                    If dt.Rows(0)("NGAY_HT") IsNot Nothing AndAlso dt.Rows(0)("NGAY_HT") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("NGAY_HT")
                        HDR.Ngay_HT = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.Ngay_HT = String.Empty
                    End If
                    If dt.Rows(0)("Ty_Gia") Is Nothing OrElse IsDBNull(dt.Rows(0)("Ty_Gia")) Then
                        HDR.Ty_Gia = 1
                    Else
                        HDR.Ty_Gia = dt.Rows(0)("Ty_Gia")
                    End If
                    If dt.Rows(0)("TTien") Is Nothing OrElse IsDBNull(dt.Rows(0)("TTien")) Then
                        HDR.TTien = 0
                    Else
                        HDR.TTien = dt.Rows(0)("TTien")
                    End If
                    'Bo sung
                    If dt.Rows(0)("Ngay_CT") Is Nothing OrElse IsDBNull(dt.Rows(0)("Ngay_CT")) Then
                        HDR.Ngay_CT = 0
                    Else
                        HDR.Ngay_CT = dt.Rows(0)("Ngay_CT")
                    End If
                End If
                HDR.SEQ_NO = dt.Rows(0).Item("seq_no").ToString()

                HDR.MA_CUC = dt.Rows(0)("MA_CUC").ToString()
                HDR.TEN_CUC = dt.Rows(0)("TEN_CUC").ToString()

                HDR.REMARKS = dt.Rows(0)("REMARKS").ToString()
                HDR.DIENGIAI_HQ = dt.Rows(0)("dien_giai_hq").ToString()

                HDR.Ghi_chu = dt.Rows(0)("ghi_chu").ToString()
                HDR.Ma_CN = dt.Rows(0)("ma_cn").ToString()

                HDR.SO_CMND = dt.Rows(0)("SO_CMND").ToString()
                HDR.SO_FONE = dt.Rows(0)("SO_FONE").ToString()

                'get DTL list
                Dim listDTL As New List(Of NewChungTu.infChungTuDTL)
                Dim DTL As NewChungTu.infChungTuDTL = Nothing
                dt = Nothing
                listParam = New List(Of IDataParameter)
                sSQL = "select ID, SHKB, Ngay_KB, Ma_NV, So_BT, Ma_DThu, CCH_ID, Ma_Cap, " & _
                        " Ma_Chuong, LKH_ID, Ma_Loai, Ma_Khoan, MTM_ID, Ma_Muc, Ma_TMuc, Noi_Dung," & _
                        " DT_ID, MaDT, Ky_Thue, SoTien ttien, SoTien_NT, MaQuy, Ma_DP, 0 SODATHUTK, SO_CT, TT_BTOAN, " & _
                        " Ma_HQ, MA_LHXNK, So_TK, Ngay_TK, Ma_LT, Sac_Thue " & _
                        " FROM TCS_CTU_DTL " & _
                        " where SO_CT = :SO_CT"
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        DTL = New NewChungTu.infChungTuDTL
                        DTL.ID = dt.Rows(i)("ID").ToString()
                        DTL.SHKB = dt.Rows(i)("SHKB").ToString()
                        If dt.Rows(i)("Ngay_KB") Is Nothing OrElse IsDBNull(dt.Rows(i)("Ngay_KB")) Then
                            DTL.Ngay_KB = 0
                        Else
                            DTL.Ngay_KB = dt.Rows(i)("Ngay_KB")
                        End If
                        If dt.Rows(i)("Ma_NV") Is Nothing OrElse IsDBNull(dt.Rows(i)("Ma_NV")) Then
                            DTL.Ma_NV = 0
                        Else
                            DTL.Ma_NV = dt.Rows(i)("Ma_NV")
                        End If
                        If dt.Rows(i)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(i)("So_BT")) Then
                            DTL.So_BT = 0
                        Else
                            DTL.So_BT = dt.Rows(i)("So_BT")
                        End If
                        DTL.Ma_DThu = dt.Rows(i)("Ma_DThu").ToString()
                        DTL.CCH_ID = dt.Rows(i)("CCH_ID").ToString()
                        DTL.Ma_Cap = dt.Rows(i)("Ma_Cap").ToString()
                        DTL.Ma_Chuong = dt.Rows(i)("Ma_Chuong").ToString()
                        DTL.LKH_ID = dt.Rows(i)("LKH_ID").ToString()
                        DTL.Ma_Loai = dt.Rows(i)("Ma_Loai").ToString()
                        DTL.Ma_Khoan = dt.Rows(i)("Ma_Khoan").ToString()
                        DTL.Ma_Muc = dt.Rows(i)("Ma_Muc").ToString()
                        DTL.Ma_TMuc = dt.Rows(i)("Ma_TMuc").ToString()
                        DTL.Ky_Thue = dt.Rows(i)("Ky_Thue").ToString()
                        DTL.Ma_DP = dt.Rows(i)("Ma_DP").ToString()
                        DTL.SO_CT = dt.Rows(i)("SO_CT").ToString()
                        DTL.TT_BTOAN = dt.Rows(i)("TT_BTOAN").ToString()
                        DTL.MA_HQ = dt.Rows(i)("Ma_HQ").ToString()
                        DTL.LH_XNK = dt.Rows(i)("MA_LHXNK").ToString()
                        DTL.SO_TK = dt.Rows(i)("So_TK").ToString()
                        If dt.Rows(i)("Ngay_TK") IsNot Nothing AndAlso dt.Rows(i)("Ngay_TK") IsNot DBNull.Value Then
                            dDateTmp = dt.Rows(i)("Ngay_TK")
                            DTL.NGAY_TK = dDateTmp.ToString("dd/MM/yyyy")
                        Else
                            DTL.NGAY_TK = String.Empty
                        End If
                        DTL.Noi_Dung = dt.Rows(i)("Noi_Dung").ToString()
                        DTL.LOAI_TT = dt.Rows(i)("Ma_LT").ToString()
                        DTL.Ma_nt = dt.Rows(i)("Ma_LT").ToString()
                        DTL.MA_LT = dt.Rows(i)("Ma_LT").ToString()
                        If dt.Rows(i)("ttien") Is Nothing OrElse IsDBNull(dt.Rows(i)("ttien")) Then
                            DTL.SoTien = 0
                        Else
                            DTL.SoTien = dt.Rows(i)("ttien")
                        End If
                        If dt.Rows(i)("SoTien_NT") Is Nothing OrElse IsDBNull(dt.Rows(i)("SoTien_NT")) Then
                            DTL.SoTien_NT = 0
                        Else
                            DTL.SoTien_NT = dt.Rows(i)("SoTien_NT")
                        End If
                        DTL.SAC_THUE = dt.Rows(i)("Sac_Thue").ToString()

                        listDTL.Add(DTL)
                    Next
                End If

                oRet = New NewChungTu.infChungTu(HDR, listDTL)
            Catch ex As Exception
                oRet = Nothing
                Throw ex
            End Try

            Return oRet
        End Function

        Public Shared Function GetListChungTu(ByVal sNgayKB As String, ByVal sMaNV As String, ByVal sMA_CN As String, ByVal sMaLThue As String, ByVal sSoChungTu As String, ByVal sLoaiCTu As String) As DataSet
            Dim ds As DataSet = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                If String.IsNullOrEmpty(sSoChungTu) Then
                    ds = HaiQuanController.GetListChungTu(sNgayKB, sMaNV, sMA_CN, sMaLThue, sLoaiCTu)
                Else
                    If sMaLThue = "04" Then
                        sSQL = "select KYHIEU_CT, SO_CT, SO_BT, SO_CT_NH, TRANG_THAI, So_BThu, TTien, A.Ma_NV, Lan_In, SHKB, NGAY_KB, MA_DTHU, tt_cthue,B.TEN_DN,a.Ma_KS " & _
                                " from TCS_CTU_HDR A, TCS_DM_NHANVIEN B where A.MA_NV = B.MA_NV AND B.TINH_TRANG = '0' and A.Ma_NV = :Ma_NV and a.MA_CN = :MA_CN " & _
                                " and a.NGAY_KB = :NGAY_KB and (a.SO_CT like '%' || :SO_CT || '%')and (a.LOAI_CTU = :LOAI_CTU or :LOAI_CTU is null) " & _
                                " and a.Ma_LThue = '04' order by so_bt desc "
                    Else
                        sSQL = "select KYHIEU_CT, SO_CT, SO_BT, SO_CT_NH, TRANG_THAI, So_BThu, TTien, A.Ma_NV, Lan_In, SHKB, NGAY_KB, MA_DTHU, tt_cthue,B.TEN_DN,a.Ma_KS " & _
                                " from TCS_CTU_HDR A, TCS_DM_NHANVIEN B where A.MA_NV = B.MA_NV AND B.TINH_TRANG = '0' and A.Ma_NV = :Ma_NV and a.MA_CN = :MA_CN " & _
                                " and a.NGAY_KB = :NGAY_KB and (a.SO_CT like '%' || :SO_CT || '%') and (a.LOAI_CTU = :LOAI_CTU or :LOAI_CTU is null) " & _
                                " and a.Ma_LThue <> '04' order by so_bt desc "
                    End If

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter("MA_CN", ParameterDirection.Input, CStr(sMA_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter("NGAY_KB", ParameterDirection.Input, CStr(sNgayKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, sSoChungTu, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter("LOAI_CTU", ParameterDirection.Input, sLoaiCTu, DbType.String))

                    ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray())
                End If
            Catch ex As Exception
                ds = Nothing
            End Try

            Return ds
        End Function

        Public Shared Function GetListChungTu(ByVal sNgayKB As String, ByVal sMaNV As String, ByVal sMA_CN As String, ByVal sMaLThue As String, ByVal sLOAI_CTU As String) As DataSet
            Dim ds As DataSet = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                If sMaLThue = "04" Then
                    sSQL = "select KYHIEU_CT, SO_CT, SO_BT, SO_CT_NH, TRANG_THAI, So_BThu, TTien, A.Ma_NV, Lan_In, SHKB, NGAY_KB, MA_DTHU, tt_cthue,B.TEN_DN,a.Ma_KS " & _
                            " from TCS_CTU_HDR A, TCS_DM_NHANVIEN B where A.MA_NV = B.MA_NV AND B.TINH_TRANG = '0'  and  a.MA_CN = :MA_CN " & _
                            " and a.NGAY_KB = :NGAY_KB and a.MA_NV = :MA_NV " & _
                            " and a.Ma_LThue = '04' order by so_ct desc "
                Else
                    sSQL = "select KYHIEU_CT, SO_CT, SO_BT, SO_CT_NH, TRANG_THAI, So_BThu, TTien, A.Ma_NV, Lan_In, SHKB, NGAY_KB, MA_DTHU, tt_cthue,B.TEN_DN,a.Ma_KS " & _
                            " from TCS_CTU_HDR A, TCS_DM_NHANVIEN B where A.MA_NV = B.MA_NV AND B.TINH_TRANG = '0'  and a.MA_CN = :MA_CN " & _
                            " and a.NGAY_KB = :NGAY_KB AND and a.MA_NV = :MA_NV  " & _
                            " and a.Ma_LThue <> '04' order by so_ct desc "
                End If
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_CN", ParameterDirection.Input, CStr(sMA_CN), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("NGAY_KB", ParameterDirection.Input, CStr(sNgayKB), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))
                'listParam.Add(DataAccess.NewDBParameter("LOAI_CTU", ParameterDirection.Input, sLOAI_CTU, DbType.String))

                ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray())
            Catch ex As Exception
                ds = Nothing
            End Try

            Return ds
        End Function

        Public Shared Function GetSoCtu(ByVal sMaNV As String, ByVal sSoCtu As String, ByVal sLoaiCTu As String) As DataTable
            Dim v_dt As DataTable
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "select KYHIEU_CT, SO_CT, SO_BT, SO_CT_NH, TRANG_THAI, So_BThu, TTien, A.Ma_NV, Lan_In, SHKB, NGAY_KB, MA_DTHU, tt_cthue,B.TEN_DN,a.Ma_KS " & _
                        " from TCS_CTU_HDR A, TCS_DM_NHANVIEN B " & _
                        " where A.MA_NV = B.MA_NV AND A.TRANG_THAI = '01' AND A.Ma_LThue = '04'" & _
                        "  AND A.SO_CT = :SO_CT " & _
                        " order by SO_CT desc "

                listParam = New List(Of IDataParameter)
                'listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, sSoCtu.Trim, DbType.String))
                'listParam.Add(DataAccess.NewDBParameter("LOAI_CTU", ParameterDirection.Input, sLoaiCTu, DbType.String))

                v_dt = DataAccess.ExecuteReturnDataTable(sSQL, CommandType.Text, listParam.ToArray())
            Catch ex As Exception
                v_dt = Nothing
            End Try

            Return v_dt
        End Function

        Public Shared Function GetTenNguoiNopThue(ByVal sMaNNT As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select TEN_NNT From TCS_DM_NNT Where MA_NNT = :MA_NNT"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_NNT", ParameterDirection.Input, CStr(sMaNNT), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetSHKB(ByVal sMaDiemThu As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT b.shkb FROM TCS_THAMSO a,TCS_DM_KHOBAC b WHERE a.giatri_ts=b.shkb And a.ten_ts='SHKB' and a.ma_dthu = :ma_dthu"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("ma_dthu", ParameterDirection.Input, CStr(sMaDiemThu), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Public Shared Function GetDataByCQT(ByVal sMaCQT As String, ByVal sMaHQ As String, ByVal sMaHQPH As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                If (sMaHQPH <> "") Then
                    sSQL = "SELECT (MA_HQ || '|' || SHKB || '|' || TEN || '|' || MA_CQTHU) FROM TCS_DM_CQTHU WHERE  MA_HQ = :MA_HQ"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("MA_HQ", ParameterDirection.Input, sMaHQPH, DbType.String))
                ElseIf (String.IsNullOrEmpty(sMaHQ) And sMaCQT <> "") Then
                    sSQL = "SELECT (MA_HQ || '|' || SHKB || '|' || TEN || '|' || MA_CQTHU) FROM TCS_DM_CQTHU WHERE MA_HQ IS NOT NULL AND MA_CQTHU  = :MA_CQTHU"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("MA_CQTHU", ParameterDirection.Input, sMaCQT, DbType.String))

                ElseIf (String.IsNullOrEmpty(sMaCQT) And sMaHQ <> "") Then
                    sSQL = "SELECT (MA_HQ || '|' || SHKB || '|' || TEN || '|' || MA_CQTHU) FROM TCS_DM_CQTHU WHERE MA_HQ IS NOT NULL AND MA_HQ = :MA_HQ"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("MA_HQ", ParameterDirection.Input, sMaHQ, DbType.String))
                ElseIf (String.IsNullOrEmpty(sMaHQ) And sMaHQPH <> "") Then
                    sSQL = "SELECT (MA_HQ || '|' || SHKB || '|' || TEN || '|' || MA_CQTHU) FROM TCS_DM_CQTHU WHERE  MA_HQ = :MA_HQ"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("MA_HQ", ParameterDirection.Input, sMaHQPH, DbType.String))
                Else
                    sSQL = "SELECT (MA_HQ || '|' || SHKB || '|' || TEN || '|' || MA_CQTHU) FROM TCS_DM_CQTHU WHERE MA_CQTHU = :MA_CQTHU AND MA_HQ = :MA_HQ "
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("MA_CQTHU", ParameterDirection.Input, sMaCQT, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter("MA_HQ", ParameterDirection.Input, sMaHQ, DbType.String))
                End If

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Public Shared Function LoadTenCQT(ByVal sMaCQT As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                If (sMaCQT <> "") Then
                    sSQL = "SELECT TEN FROM TCS_DM_CQTHU WHERE  MA_CQTHU = :MA_CQTHU"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("MA_CQTHU", ParameterDirection.Input, sMaCQT, DbType.String))
                Else
                    Return sRet = ""
                End If

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetSoBT2(ByVal iNgayLV As Integer, ByVal iMaNV As Integer) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select MAX(So_BT) AS So_BT From TCS_CTU_HDR Where ngay_kb = '" & iNgayLV & "' AND Ma_NV = '" & iMaNV & "'"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("ngay_kb", ParameterDirection.Input, iNgayLV, DbType.Int32))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, iMaNV, DbType.Int32))

                Dim dt As DataSet = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text)
                If dt IsNot Nothing AndAlso dt.Tables(0).Rows.Count > 0 Then
                    sRet = dt.Tables(0).Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function


        Public Shared Function GetSoBT(ByVal iNgayLV As Integer, ByVal iMaNV As Integer) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select MAX(So_BT) AS So_BT From TCS_CTU_HDR Where ngay_kb = :ngay_kb AND Ma_NV = :Ma_NV"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("ngay_kb", ParameterDirection.Input, iNgayLV, DbType.Int32))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, iMaNV, DbType.Int32))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GenSequenceKeyTable(ByVal sSeqName As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select " & sSeqName & " as ID from dual"
                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt(0)(0)
                End If

            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetKyHieuChungTu(ByVal strSHKB As String, Optional ByVal strMaNV As String = "", Optional ByVal strLoaiGD As String = "01") As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                sSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT'"
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    If strLoaiGD = "01" Then
                        sRet = dt.Rows(0)(0)
                    Else
                        sRet = dt.Rows(0)(0).ToString().Substring(0, 3)
                    End If
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetDefaultDBHC(ByVal sMaDiemThu As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT A.MA_XA, A.TEN FROM TCS_DM_XA A, TCS_THAMSO B WHERE A.MA_XA = B.GIATRI_TS AND B.TEN_TS = 'MA_DBHC' AND B.MA_DTHU = :ma_dthu"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("ma_dthu", ParameterDirection.Input, CStr(sMaDiemThu), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function CtuLoad(ByVal sMaNV As String, ByVal lNgayKB As Long, ByVal sTrangThai As String, ByVal sSoCT As String) As DataTable
            Dim dt As DataTable = Nothing
            Dim sSQL As String = ""
            Dim sSQLWhere As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                Select Case sTrangThai
                    Case "02"
                        sSQLWhere = " AND TT_TTHU<>0 AND MA_TQ<>0"
                    Case "01"
                        sSQLWhere = " AND TT_TTHU=0 "
                End Select

                If String.IsNullOrEmpty(sSoCT) Then
                    sSQL = "SELECT SHKB, KYHIEU_CT, SO_CT,Ma_NV, SO_BT,TRANG_THAI, So_BThu,TTien, TT_TTHU, NGAY_KB, MA_XA " & _
                        " FROM TCS_CTU_HDR  " & _
                        " WHERE NGAY_KB = :NGAY_KB " & _
                            " AND TRANG_THAI='00' " & sSQLWhere & _
                            " AND ISACTIVE='1'" & _
                            " AND MA_LTHUE = '04'" & _
                            " AND Ma_NV = :Ma_NV " & _
                            " ORDER BY so_bt DESC "
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("NGAY_KB", ParameterDirection.Input, CDec(lNgayKB), DbType.Decimal))
                    listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, sMaNV, DbType.String))
                Else
                    sSQL = "SELECT SHKB, KYHIEU_CT, SO_CT,Ma_NV, SO_BT,TRANG_THAI, So_BThu,TTien, TT_TTHU, NGAY_KB, MA_XA " & _
                        " FROM TCS_CTU_HDR  " & _
                        " WHERE NGAY_KB = :NGAY_KB " & _
                            " AND TRANG_THAI='00' " & sSQLWhere & _
                            " AND ISACTIVE='1'" & _
                            " AND MA_LTHUE = '04'" & _
                            " AND Ma_NV = :Ma_NV " & _
                            " AND So_CT = :So_CT" & _
                            " ORDER BY so_bt DESC "

                    sSQLWhere = sSQLWhere & " AND So_CT = :So_CT"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("NGAY_KB", ParameterDirection.Input, CDec(lNgayKB), DbType.Decimal))
                    listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, sMaNV, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter("So_CT", ParameterDirection.Input, sSoCT, DbType.String))
                End If

                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
            Catch ex As Exception
                dt = Nothing
            End Try

            Return dt
        End Function

        Public Shared Function GetDefaultMaVangLai() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "Select distinct MA_NNT as ID, TEN_NNT as Title From TCS_DM_NNT_KB Order By MA_NNT"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetDefaultMaNHTH(ByVal sSHKB As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT A.MA_GIANTIEP MA, A.TEN_GIANTIEP TEN FROM TCS_DM_NH_GIANTIEP_TRUCTIEP A WHERE A.SHKB = :SHKB"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, CStr(sSHKB), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetDefaultMaNHTT(ByVal sSHKB As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT A.MA_TRUCTIEP MA, A.TEN_TRUCTIEP TEN FROM TCS_DM_NH_GIANTIEP_TRUCTIEP A WHERE A.SHKB = :SHKB"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, CStr(sSHKB), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetDefaultMaHQ(ByVal sMaDiemThu As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT MA_HQ FROM TCS_DM_CQTHU WHERE MA_HQ IS NOT NULL AND MA_CQTHU = :MA_CQTHU"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_CQTHU", ParameterDirection.Input, CStr(sMaDiemThu), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrSHKB(ByVal sMaDiemThu As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT b.shkb,b.ten from  TCS_DM_KHOBAC b WHERE b.tinh_trang=1"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrSHKB = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrSHKB[" & i & "] ='" & dt.Rows(i)("SHKB") & ";" & dt.Rows(i)("TEN") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrDBHC(ByVal strSHKB As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT b.shkb ma_kb, b.ma_tinh, A.TEN FROM TCS_DM_XA A, TCS_DM_KHOBAC B  WHERE  B.ma_tinh= A.MA_XA AND B.SHKB = :SHKB "
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, CStr(strSHKB), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrDBHC = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrDBHC[" & i & "] ='" & dt.Rows(i)("ma_kb") & ";" & dt.Rows(i)("ma_tinh") & ";" & dt.Rows(i)("ten") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrCQThu(ByVal sMaDiemThu As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU WHERE MA_DTHU = :MA_DTHU"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_DTHU", ParameterDirection.Input, CStr(sMaDiemThu), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrCQThu = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrCQThu[" & i & "] ='" & dt.Rows(i)("MA_DTHU") & ";" & dt.Rows(i)("MA_CQTHU") & ";" & dt.Rows(i)("TEN") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrTKGL() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT SO_TK, TEN_TK FROM TCS_DM_TK_TG"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrTKGL = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrTKGL[" & i & "] ='" & dt.Rows(i)("SO_TK") & ";" & dt.Rows(i)("TEN_TK") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrNT() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT Ma_NT, TEN from Tcs_dm_nguyente order by ten"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrMaNT = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrMaNT[" & i & "] ='" & dt.Rows(i)("Ma_NT") & ";" & dt.Rows(i)("TEN") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrTyGia() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT ma_nt,ty_gia,to_fix FROM tcs_dm_tygia where trang_thai = 1"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrTyGia = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrTyGia[" & i & "] ='" & dt.Rows(i)("ma_nt") & ";" & dt.Rows(i)("ty_gia") & ";" & dt.Rows(i)("TO_FIX") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrLoaiThue() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT Ma_LTHUE, TEN from TCS_DM_LTHUE WHERE MA_LTHUE = '04'"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrMaLTHUE = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrMaLTHUE[" & i & "] ='" & dt.Rows(i)("Ma_LTHUE") & ";" & dt.Rows(i)("TEN") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrLoaiTien() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT MA_LT, MA_LT ||'-'||ten_lt ten_lt from tcs_dm_loaitien_thue"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrMaLTIEN = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrMaLTIEN[" & i & "] ='" & dt.Rows(i)("MA_LT") & ";" & dt.Rows(i)("ten_lt") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrTKNoNSNN(ByVal sSHKB As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT distinct a.tk,a.ten_tk,a.shkb FROM tcs_dm_taikhoan A WHERE a.shkb = :shkb AND a.TK_KB_NH='1'"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("shkb", ParameterDirection.Input, CStr(sSHKB), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrTKNoNSNN = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrTKNoNSNN[" & i & "] ='" & dt.Rows(i)("shkb") & ";" & dt.Rows(i)("tk") & ";" & dt.Rows(i)("ten_tk") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrTKCoNSNN(ByVal sSHKB As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT tk,ten_tk,dbhc,MA_CQTHU FROM TCS_DM_TAIKHOAN WHERE TRIM(MA_CQTHU) IS NOT NULL AND DBHC IN (SELECT DBHC FROM TCS_MAP_TK_NH_KB " & _
                        " WHERE MA_KB = :MA_KB AND TINH_TRANG='1'  AND TK_KB_NH='0'"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_KB", ParameterDirection.Input, CStr(sSHKB), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrTKCoNSNN = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrTKCoNSNN[" & i & "] ='" & dt.Rows(i)("dbhc") & ";" & dt.Rows(i)("MA_CQTHU") & ";" & dt.Rows(i)("tk") & ";" & dt.Rows(i)("ten_tk") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrLHSX() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT MA_LH,  TEN_LH,TEN_VT  FROM TCS_DM_LHINH  ORDER BY TEN_VT"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrLHSX = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrLHSX[" & i & "] ='" & dt.Rows(i)("MA_LH") & ";" & dt.Rows(i)("TEN_LH") & ";" & dt.Rows(i)("TEN_VT") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrTT_NH_B() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT DISTINCT( MA_GIANTIEP) as MA_NH_B,  TEN_GIANTIEP as TEN_NH_B, MA_TRUCTIEP as MA_NH_TT,  TEN_TRUCTIEP AS TEN_NH_TT, SHKB " & _
                        " FROM tcs_dm_nh_giantiep_tructiep  ORDER BY MA_GIANTIEP"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrTT_NH_B = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrTT_NH_B[" & i & "] ='" & dt.Rows(i)("MA_NH_B") & ";" & dt.Rows(i)("TEN_NH_B") & ";" & dt.Rows(i)("MA_NH_TT") & ";" & dt.Rows(i)("TEN_NH_TT") & ";" & dt.Rows(i)("SHKB") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrTT_NH_TT() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT DISTINCT( MA_TRUCTIEP) as MA_NH_TT,  TEN_TRUCTIEP as TEN_NH_TT  FROM tcs_dm_nh_giantiep_tructiep  ORDER BY MA_TRUCTIEP"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrTT_NH_TT = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrTT_NH_TT[" & i & "] ='" & dt.Rows(i)("MA_NH_TT") & ";" & dt.Rows(i)("TEN_NH_TT") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrDM_MA_NH_A() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT ma_nh_chuyen,ten_nh_chuyen FROM tcs_nganhang_chuyen"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrMA_NHA = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrMA_NHA[" & i & "] ='" & dt.Rows(i)("ma_nh_chuyen") & ";" & dt.Rows(i)("ten_nh_chuyen") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function PopulateArrDM_PTTT() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "SELECT ma_pttt,ten_pttt FROM tcs_dm_pttt where status=1 order by ten_pttt"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim myJavaScript As StringBuilder = New StringBuilder()
                    myJavaScript.Append(" var arrPTTT = new Array(" & dt.Rows.Count & ");")
                    For i As Integer = 0 To dt.Rows.Count - 1
                        myJavaScript.Append(" arrPTTT[" & i & "] ='" & dt.Rows(i)("ma_pttt") & ";" & dt.Rows(i)("ten_pttt") & "';")
                    Next
                    sRet = myJavaScript.ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Private Shared Function GetBdsStatus() As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Try
                sSQL = "Select TEN_TS,GIATRI_TS From TCS_THAMSO WHERE MA_DTHU='00' AND TEN_TS='MODE'"

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)("GIATRI_TS").ToString().ToUpper()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetNgayKhoBac(ByVal strSHKB As String) As Long
            Dim lRet As Long = 0
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = " SELECT TO_NUMBER (TO_CHAR (TO_DATE (giatri_ts, 'DD/MM/YYYY'), 'YYYYMMDD')) FROM tcs_thamso " & _
                        " WHERE ten_ts = 'NGAY_LV' AND ma_dthu = (SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS = :GIATRI_TS)"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("GIATRI_TS", ParameterDirection.Input, CStr(strSHKB), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    If Not Long.TryParse(dt.Rows(0)(0).ToString(), lRet) Then lRet = Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
                Else
                    lRet = Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
                End If
            Catch ex As Exception
                lRet = Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
            End Try

            Return lRet
        End Function

        Public Shared Function GetMaDiemThu(ByVal sMaNV As String) As String
            Dim sRet As String = String.Empty
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='MA_DT' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV = :MA_NV)"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = String.Empty
            End Try

            Return sRet
        End Function

        Public Shared Function GetNgaylamViec(ByVal sMaNV As String) As String
            Dim sRet As String = String.Empty
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = " SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV = :MA_NV)"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = String.Empty
            End Try

            Return sRet
        End Function

        Public Shared Function GetDataByMaHQ(ByVal sMaHQ As String) As String 'Mã HQ PH
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select ma_cqthu || ';' || TEN || ';' || SHKB  From tcs_dm_cqthu Where Ma_HQ = :Ma_HQ"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("Ma_HQ", ParameterDirection.Input, CStr(sMaHQ).Trim(), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetTaiKhoanKH_NH(ByVal sSoTK_KHNH As String, Optional ByVal sMaCN As String = "") As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                If Not String.IsNullOrEmpty(sMaCN) Then
                    sSQL = "Select TENTAIKHOAN || '#' || MALOAITIEN from TCS_DM_TAIKHOAN_KH where SOTAIKHOAN = :SOTAIKHOAN and MA_CN = :MA_CN "
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("SOTAIKHOAN", ParameterDirection.Input, CStr(sSoTK_KHNH).Trim(), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter("MA_CN", ParameterDirection.Input, CStr(sMaCN).Trim(), DbType.String))
                Else
                    sSQL = "Select TENTAIKHOAN || '#' || MALOAITIEN from TCS_DM_TAIKHOAN_KH where SOTAIKHOAN = :SOTAIKHOAN "
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("SOTAIKHOAN", ParameterDirection.Input, CStr(sSoTK_KHNH).Trim(), DbType.String))
                End If
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
                sRet = ""
                Throw ex
            End Try

            Return sRet
        End Function

        Public Shared Function GetDataBySHKB(ByVal sLoaiDM As String, ByVal sSHKB As String) As String
            Dim sRet As String = sLoaiDM & "|"
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                listParam = New List(Of IDataParameter)
                Select Case sLoaiDM
                    Case "DBHC_DEF".ToUpper
                        sSQL = " SELECT A.MA_XA, A.TEN FROM TCS_DM_XA A, TCS_DM_KHOBAC B  WHERE  B.ma_db= A.MA_XA AND B.SHKB = :SHKB"
                        listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, CStr(sSHKB).Trim(), DbType.String))
                        dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                    Case "DBHC".ToUpper
                        sSQL = "select b.ma_xa, b.ten from tcs_dm_xa b inner join tcs_dm_khobac a on (b.ma_xa = a.ma_db or b.MA_CHA = a.ma_db) where a.shkb = :SHKB"
                        listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, CStr(sSHKB).Trim(), DbType.String))
                        dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                    Case "TaiKhoanNo".ToUpper
                        sSQL = "SELECT distinct a.shkb,a.tk,a.ten_tk FROM tcs_dm_taikhoan A WHERE a.shkb = :SHKB AND a.TK_KB_NH='1'"
                        listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, CStr(sSHKB).Trim(), DbType.String))
                        dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                    Case "TaiKhoanCo".ToUpper
                        sSQL = " SELECT dbhc,MA_CQTHU,tk,ten_tk FROM TCS_DM_TAIKHOAN WHERE TRIM(MA_CQTHU) IS NOT NULL AND SHKB = :SHKB AND TK_KB_NH='0'"
                        listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, sSHKB.Trim(), DbType.String))
                        dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                    Case "CQTHU_SHKB".ToUpper
                        sSQL = " SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU WHERE SHKB = :SHKB"
                        listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, CStr(sSHKB).Trim(), DbType.String))
                        dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                    Case "CQTHU".ToUpper
                        sSQL = " SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU WHERE MA_HQ is not null "
                        dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                    Case "LHSX".ToUpper
                        sSQL = "SELECT MA_LH as MA_DM, TEN_LH as TEN_DM, TEN_VT FROM TCS_DM_LHINH  ORDER BY TEN_VT"
                        dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                    Case "SHKB".ToUpper
                    Case "KHCT".ToUpper
                        sSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' AND ROWNUM=1 "
                        dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                    Case "TINH_SHKB".ToUpper
                        sSQL = "SELECT B.MA_XA,B.TEN FROM TCS_DM_KHOBAC A, TCS_DM_XA B WHERE  A.MA_TINH= B.MA_XA AND A.SHKB = :SHKB"
                        listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, CStr(sSHKB).Trim(), DbType.String))
                        dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                End Select

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        Dim sTmp As String = String.Empty
                        For j As Integer = 0 To dt.Columns.Count - 1
                            sTmp &= dt.Rows(i)(j).ToString & ";"
                        Next
                        sRet = sRet & sTmp.Substring(0, sTmp.LastIndexOf(";")) & "|"
                    Next
                End If
            Catch ex As Exception
                sRet = sLoaiDM & "|"
            End Try

            Return sRet
        End Function

        Public Shared Function GetTT_NganHang(ByVal sSHKB As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select MA_GIANTIEP, TEN_GIANTIEP, MA_TRUCTIEP, TEN_TRUCTIEP  From TCS_DM_NH_GIANTIEP_TRUCTIEP Where SHKB = :SHKB "
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SHKB", ParameterDirection.Input, CStr(sSHKB), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)("MA_GIANTIEP").ToString() & ";" & dt.Rows(0)("TEN_GIANTIEP").ToString & ";" & dt.Rows(0)("MA_TRUCTIEP").ToString & ";" & dt.Rows(0)("TEN_TRUCTIEP").ToString
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Public Shared Function GetTTTinhThanhKB(ByVal sMaTinh As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select ten  From tcs_dm_xa Where ma_tinh = :MATINH "
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MATINH", ParameterDirection.Input, CStr(sMaTinh), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = sMaTinh & ";" & dt.Rows(0)("ten").ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Public Shared Function GhiChungTu(ByVal HDR As NewChungTu.infChungTuHDR, ByVal ListDTL As List(Of NewChungTu.infChungTuDTL), ByVal sActionType As String, ByVal sSaveType As String) As String
            Dim sTenDN As String = ""
            Dim sDesCTU As String = ""
            Dim sRet As String = ""
            Try
                'If GetBdsStatus() <> Common.mdlSystemVariables.gblnTTBDS Then
                '    sRet = ""
                '    Throw New Exception("Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại")
                '    Return sRet
                'End If
                If HDR Is Nothing Then
                    sRet = ""
                    Throw New Exception("Không có thông tin chứng từ để lưu. Vui lòng kiểm tra lại")
                    Return sRet
                End If

                Dim strNgay_DK As String = ""
                Dim objUser = Nothing
                If HDR IsNot Nothing Then objUser = New CTuUser(HDR.Ma_NV)
                If objUser IsNot Nothing Then sTenDN = objUser.Ten_DN

                Dim blnVND As Boolean = True
                If (HDR.Ma_NT <> "VND") Then blnVND = False
                Dim intVitriLoi() As Integer = Check_ListDTL(ListDTL, True, blnVND, True, sTenDN)
                If (Not IsNothing(intVitriLoi)) Then
                    sRet = ""
                    Throw New Exception("Có lỗi trong quá trình kiểm tra chi tiết chứng từ: " & intVitriLoi(1).ToString)
                    Return sRet
                End If

                'Kiem tra xem co so_ctu chua => bo xung 1 so truong cho so chung tu do
                Dim v_dtHdr As DataTable = getAndSetHDR(HDR.So_CT)
                If v_dtHdr IsNot Nothing Then
                    'Trang thai 00 - Cho kiem soat, 03 - Chuyen tra => duoc phep sua, update => luu lai 1 so thong tin
                    If HDR.Trang_Thai.Equals("00") Or HDR.Trang_Thai.Equals("03") Then
                        'Cap nhat loai tien thue => vao obj DTL
                        For Each objDtl As NewChungTu.infChungTuDTL In ListDTL
                            objDtl.LOAI_TT = HDR.LOAI_TT
                        Next
                    End If
                End If

                Select Case sActionType
                    Case "GHI"
                        HDR.Trang_Thai = "00"
                        If (sSaveType = "INSERT") Then
                            If String.IsNullOrEmpty(HDR.Ngay_QD) Then
                                HDR.Ngay_QD = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            If String.IsNullOrEmpty(HDR.Ngay_HT) Then
                                HDR.Ngay_HT = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            If String.IsNullOrEmpty(HDR.Ngay_TK) Then
                                HDR.Ngay_TK = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            If String.IsNullOrEmpty(HDR.NGAY_KH_NH) Then
                                HDR.NGAY_KH_NH = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            If String.IsNullOrEmpty(HDR.Ngay_BK) Then
                                HDR.Ngay_BK = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            For Each dtl In ListDTL
                                If String.IsNullOrEmpty(dtl.NGAY_TK) Then
                                    dtl.NGAY_TK = DateTime.Now.ToString("dd/MM/yyyy")
                                End If
                            Next

                            If HaiQuanController.Insert_ChungTu(HDR, ListDTL) Then
                                sRet = "Success;Hoàn thiện chứng từ thành công"
                            Else
                                sRet = String.Empty
                            End If
                        ElseIf sSaveType = "UPDATE" Then
                            If HDR.Trang_Thai = "00" OrElse HDR.Trang_Thai = "03" Then
                                HDR.Ma_KS = "0"
                                HDR.Ngay_QD = DateTime.Now.ToString("dd/MM/yyyy")
                                'HDR.Ngay_HT = DateTime.Now.ToString("dd/MM/yyyy")
                                HDR.Ngay_TK = DateTime.Now.ToString("dd/MM/yyyy")
                                HDR.NGAY_KH_NH = DateTime.Now.ToString("dd/MM/yyyy")
                                HDR.Ngay_BK = DateTime.Now.ToString("dd/MM/yyyy")
                                If String.IsNullOrEmpty(HDR.Ngay_QD) Then HDR.Ngay_QD = DateTime.Now

                                If HaiQuanController.Update_ChungTu(HDR, ListDTL) Then
                                    sRet = "Success;Cập nhật chứng từ thành công"
                                Else
                                    sRet = String.Empty
                                End If
                                'Throw New Exception("Cập nhật chứng từ: đang cập nhật chức năng")
                            Else
                                sRet = ""
                                Throw New Exception("Trạng thái chứng từ không cho phép cập nhật")
                                Return sRet
                            End If
                        End If
                    Case "GHIKS"
                        If sSaveType = "INSERT_KS" Then
                            HDR.Trang_Thai = "05"
                            If String.IsNullOrEmpty(HDR.Ngay_QD) Then
                                HDR.Ngay_QD = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            If String.IsNullOrEmpty(HDR.Ngay_HT) Then
                                HDR.Ngay_HT = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            If String.IsNullOrEmpty(HDR.Ngay_TK) Then
                                HDR.Ngay_TK = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            If String.IsNullOrEmpty(HDR.NGAY_KH_NH) Then
                                HDR.NGAY_KH_NH = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            If String.IsNullOrEmpty(HDR.Ngay_BK) Then
                                HDR.Ngay_BK = DateTime.Now.ToString("dd/MM/yyyy")
                            End If
                            For Each dtl In ListDTL
                                If String.IsNullOrEmpty(dtl.NGAY_TK) Then
                                    dtl.NGAY_TK = DateTime.Now.ToString("dd/MM/yyyy")
                                End If
                            Next

                            If HaiQuanController.Insert_ChungTu(HDR, ListDTL) Then
                                sRet = "Success;Hoàn thiện và gửi kiểm soát chứng từ thành công"
                            Else
                                sRet = String.Empty
                            End If
                        ElseIf sSaveType = "UPDATE_KS" Then
                            If HDR.Trang_Thai = "05" Then
                                HDR.Ma_KS = "0"
                                HDR.Ngay_QD = DateTime.Now.ToString("dd/MM/yyyy")
                                'HDR.Ngay_HT = DateTime.Now.ToString("dd/MM/yyyy")
                                HDR.Ngay_TK = DateTime.Now.ToString("dd/MM/yyyy")
                                HDR.NGAY_KH_NH = DateTime.Now.ToString("dd/MM/yyyy")
                                HDR.Ngay_BK = DateTime.Now.ToString("dd/MM/yyyy")

                                If String.IsNullOrEmpty(HDR.Ngay_QD) Then HDR.Ngay_QD = DateTime.Now

                                If HaiQuanController.Update_ChungTu(HDR, ListDTL, "'05'") Then
                                    sRet = "Success;Cập nhật và gửi kiểm soát thành công"
                                Else
                                    sRet = String.Empty
                                End If
                                'Throw New Exception("Cập nhật chứng từ: đang cập nhật chức năng")
                            ElseIf HDR.Trang_Thai = "00" OrElse HDR.Trang_Thai = "03" Then
                                HDR.Trang_Thai = "05"
                                If HaiQuanController.Update_ChungTu2(HDR, ListDTL) Then
                                    sRet = "Success;Hoàn thiện và gửi kiểm soát chứng từ thành công"
                                Else
                                    sRet = String.Empty
                                End If
                            Else
                                sRet = ""
                                Throw New Exception("Trạng thái chứng từ không cho phép cập nhật")
                                Return sRet
                            End If
                        End If
                    Case "GHIDC"
                        HDR.Trang_Thai = "19" 'Trạng thái 19 - điều chỉnh gửi kiểm soát
                        If sSaveType = "UPDATE_DC" Then
                            HDR.Trang_Thai = "19"
                            If HaiQuanController.Update_ChungTu2(HDR, ListDTL) Then
                                sRet = "Success;Điều chỉnh và gửi kiểm soát thành công"
                            Else
                                sRet = String.Empty
                            End If

                        End If
                End Select

                'Nối thêm thông tin load lên form
                sRet = sRet & ";" & HDR.SHKB & "/" & HDR.So_CT & "/" & HDR.So_BT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi ghi chứng từ", sTenDN)
                If ex.Message.Contains("TCS_CTU_HDR_PK") Then
                    sRet = ""
                    If sSaveType.StartsWith("INSERT") Then
                        Throw New Exception("Không thể thêm mới! Đã tồn tại giấy nộp tiền này trong hệ thống")
                    ElseIf sSaveType.StartsWith("UPDATE") Then
                        Throw New Exception("Không thể cập nhật! Đã tồn tại giấy nộp tiền này trong hệ thống")
                    End If
                Else
                    sRet = ""
                    Throw ex
                End If
            End Try

            Return sRet
        End Function

        Public Shared Function ChuyenKSChungTu(ByVal sSoCT As String, ByVal sMaNV As String) As Boolean
            Dim bRet As Boolean = False
            Dim sSQL As String = String.Empty
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim iRet As Integer = 0
            Try
                sSQL = "update tcs_ctu_hdr set trang_thai = '05', Ma_NV = :Ma_NV, Ngay_HT = SYSDATE where SO_CT = :SO_CT and Trang_Thai = '00'"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, sMaNV, DbType.String))
                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSoCT, DbType.String))
                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
                If iRet > 0 Then
                    bRet = True
                End If
            Catch ex As Exception
                bRet = False
                Throw New Exception("Có lỗi trong quá trình chuyển kiểm soát chứng từ")
            End Try

            Return bRet
        End Function

        Public Shared Function HuyChungTu(ByVal sSo_CT As String, ByVal sLyDo As String, ByVal sMaNV As String) As String
            Dim sRet As String = String.Empty
            Dim sTenDN As String = ""
            Dim sSQL As String = String.Empty
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                Dim sTrangThaiCTu As String = String.Empty
                Dim sTT_CThue As String = String.Empty
                Dim sSHKB As String = String.Empty
                Dim sSo_BT As String = String.Empty

                If String.IsNullOrEmpty(sSo_CT) Then
                    sRet = ""
                    Throw New Exception("Vui lòng chọn chứng từ để hủy")
                    Return sRet
                Else
                    sSQL = "select TRANG_THAI, TT_CTHUE, SHKB, SO_BT from TCS_CTU_HDR where SO_CT = :SO_CT and 1 = 1"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                    dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                        If dt.Rows(0)("TRANG_THAI") IsNot Nothing AndAlso dt.Rows(0)("TRANG_THAI") IsNot DBNull.Value Then
                            sTrangThaiCTu = dt.Rows(0)("TRANG_THAI").ToString()
                        End If
                        If dt.Rows(0)("TT_CTHUE") IsNot Nothing AndAlso Not IsDBNull(dt.Rows(0)("TT_CTHUE")) Then
                            sTT_CThue = dt.Rows(0)("TT_CTHUE")
                        End If
                        If dt.Rows(0)("SHKB") IsNot Nothing AndAlso dt.Rows(0)("SHKB") IsNot DBNull.Value Then
                            sSHKB = dt.Rows(0)("SHKB")
                        End If
                        If dt.Rows(0)("SO_BT") IsNot Nothing AndAlso dt.Rows(0)("SO_BT") IsNot DBNull.Value Then
                            sSo_BT = dt.Rows(0)("SO_BT")
                        End If

                        If Not String.IsNullOrEmpty(sTrangThaiCTu) Then
                            If sTrangThaiCTu = "01" Then
                                ' GDV => 02 => KSV => 04
                                sSQL = "update TCS_CTU_HDR set TRANG_THAI = '02',so_bt_ktkb=0,NGAY_GDV_HUY= sysdate, note = :LY_DO where SO_CT = :SO_CT and TRANG_THAI = '01'"
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":LY_DO", ParameterDirection.Input, sLyDo, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
                            ElseIf (sTrangThaiCTu = "02") Then
                                'KSV huy: 02 => 04
                                If sTT_CThue = "1" Then
                                    sSQL = "update TCS_CTU_HDR set TRANG_THAI = '07',so_bt_ktkb=0,MA_HUYKS = :MA_HUYKS, NGAY_KSV_HUY = sysdate,LY_DO = :LY_DO where SO_CT = :SO_CT and TRANG_THAI in ('02') and TT_CTHUE = 1 "
                                ElseIf sTT_CThue = "0" Then
                                    sSQL = "update TCS_CTU_HDR set TRANG_THAI = '08',so_bt_ktkb=0,MA_HUYKS = :MA_HUYKS, NGAY_KSV_HUY = sysdate,LY_DO = :LY_DO where SO_CT = :SO_CT and TRANG_THAI in ('02') and TT_CTHUE = 0 "
                                End If
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":MA_HUYKS", ParameterDirection.Input, sMaNV, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":LY_DO", ParameterDirection.Input, sLyDo, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
                            ElseIf (sTrangThaiCTu = "00" Or sTrangThaiCTu = "03") Then
                                'Hủy chứng từ chưa kiểm soát 00 hoặc chuyển trả 03
                                sSQL = "update TCS_CTU_HDR set TRANG_THAI = '04',so_bt_ktkb=0,NGAY_GDV_HUY= sysdate, LY_DO = :LY_DO where SO_CT = :SO_CT and TRANG_THAI in ('00', '03')"
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":LY_DO", ParameterDirection.Input, sLyDo, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
                            Else
                                Throw New Exception("Trạng thái chứng từ không cho phép hủy")
                            End If

                            If iRet > 0 Then
                                sRet = "Hủy chứng từ thành công"
                                'Nối thêm thông tin load lên form
                                sRet = sRet & ";" & sSHKB & "/" & sSo_CT & "/" & sSo_BT
                            End If
                        End If
                    Else
                        sRet = ""
                        Throw New Exception("Không tìm thấy chứng từ trong hệ thống")
                        Return sRet
                    End If
                End If
            Catch ex As Exception
                'VBOracleLib.LogApp.WriteLog("HuyChungTu", ex, "Lỗi hủy chứng từ", sTenDN)
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi hủy chứng từ", sTenDN)
                sRet = ""
                Throw ex
            End Try

            Return sRet
        End Function

        Public Shared Function Insert_ChungTu(ByVal objChungTu As NewChungTu.infChungTu) As Boolean
            Dim bRet As Boolean = False
            Try
                'Insert by only one transaction
                If objChungTu IsNot Nothing Then
                    bRet = Insert_ChungTu(objChungTu.HDR, objChungTu.ListDTL)
                End If
            Catch ex As Exception
                bRet = False
                Throw ex
            End Try

            Return bRet
        End Function

        Public Shared Function Insert_Chungtu(ByVal HDR As NewChungTu.infChungTuHDR, ByVal listDTL As List(Of NewChungTu.infChungTuDTL)) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If HDR IsNot Nothing Then
                    Try
                        HDR.KyHieu_CT = HaiQuanController.GetKyHieuChungTu(HDR.SHKB, HDR.Ma_NV, "01")
                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh ký hiệu chứng từ")
                        Return bRet
                    End Try

                    Try
                        'nên chuyển vào trong câu query
                        HDR.Ngay_KB = HaiQuanController.GetNgayKhoBac(HDR.SHKB)
                        Dim sSo_BT As String = String.Empty
                        Dim sNam_KB As String
                        Dim sThang_KB As String
                        sNam_KB = Mid(HDR.Ngay_KB, 3, 2)
                        sThang_KB = Mid(HDR.Ngay_KB, 5, 2)
                        HDR.So_CT = sNam_KB + sThang_KB + CTuCommon.Get_SoCT()
                        sSo_BT = HaiQuanController.GetSoBT(HDR.Ngay_KB, HDR.Ma_NV)
                        If Not String.IsNullOrEmpty(sSo_BT) Then HDR.So_BT = CInt(sSo_BT + 1)

                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh số chứng từ")
                        Return bRet
                    End Try

                    'Using conn1 As IDbConnection = VBOracleLib.DataAccess.GetConnection()
                    '    Using tran1 As IDbTransaction = conn.BeginTransaction()

                    '    End Using
                    'End Using
                    conn = VBOracleLib.DataAccess.GetConnection()
                    '  conn.Open()
                    tran = conn.BeginTransaction()

                    sSQL = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV," & _
                              "So_BT,Ma_DThu,So_BThu,KyHieu_CT,So_CT_NH,Ma_NNTien,Ten_NNTien,DC_NNTien," & _
                              "Ma_NNThue,Ten_NNThue,DC_NNThue,Ly_Do,Ma_KS,Ma_TQ,So_QD,Ngay_QD,CQ_QD,Ngay_CT," & _
                              "Ma_CQThu,XA_ID,Ma_Tinh,Ma_Huyen,Ma_Xa,TK_No,TK_Co,So_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
                              "Ma_NT,Ty_Gia,TG_ID, Ma_LThue,So_Khung,So_May,TK_KH_NH,MA_NH_A,MA_NH_B,Ten_NH_B,Ten_NH_TT,TTien,TT_TThu," & _
                              "Lan_In,Trang_Thai, TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT,So_BK, CTU_GNT_BL,So_CT," & _
                              "TK_GL_NH,TEN_KH_NH,TK_KB_NH, ISACTIVE,PHI_GD,PHI_VAT,PT_TINHPHI,SEQ_NO,REF_NO,RM_REF_NO,QUAN_HUYENNNTIEN," & _
                              "TINH_TPNNTIEN,MA_HQ,LOAI_TT,TEN_HQ,MA_HQ_PH,TEN_HQ_PH, MA_NH_TT,MA_SANPHAM,TT_CITAD,MA_NTK, KENH_CT, " & _
                              "MA_CN,partition_id,transaction_id,save_type, tran_type, " & _
                              "value_date,so_lo,note,address,remarks,TEN_CQTHU,NAME_TEN_CQTHU,SO_TIEN_CHU,TEN_LTHUE, " & _
                              "TEN_LHXNK,TEN_KB,MA_CUC,TEN_CUC, LOAI_NNT, PO_Date, TT_THU, " & _
                              "Ma_Huyen_NNThue, Ma_Tinh_NNThue, MA_QUAN_HUYENNNTIEN, MA_TINH_TPNNTIEN, Ngay_HT, Ngay_TK," & _
                              "NGAY_KH_NH, Ngay_BK, LOAI_CTU,SO_CMND,SO_FONE)" & _
                              "Values(:SHKB, :Ngay_KB, :Ma_NV, " & _
                              ":So_BT, :Ma_DThu, :So_BThu, :KyHieu_CT, :So_CT_NH, :Ma_NNTien, :Ten_NNTien, :DC_NNTien, " & _
                              ":Ma_NNThue, :Ten_NNThue, :DC_NNThue, :Ly_Do, :Ma_KS, :Ma_TQ, :So_QD, TO_DATE (:Ngay_QD, 'dd/MM/yyyy HH24:MI:SS'), :CQ_QD, :Ngay_CT, " & _
                              ":Ma_CQThu, :XA_ID, :Ma_Tinh, :Ma_Huyen, :Ma_Xa, :TK_No, :TK_Co, :So_TK,  :LH_XNK, :DVSDNS, :Ten_DVSDNS, " & _
                              ":Ma_NT, :Ty_Gia, :TG_ID, :Ma_LThue, :So_Khung, :So_May, :TK_KH_NH,  :MA_NH_A, :MA_NH_B, :Ten_NH_B, :Ten_NH_TT, :TTien, :TT_TThu, " & _
                              ":Lan_In, :Trang_Thai, :TK_KH_Nhan, :Ten_KH_Nhan, :Diachi_KH_Nhan, :TTien_NT, :PT_TT, :So_BK,  :CTU_GNT_BL, :So_CT, " & _
                              ":TK_GL_NH, :TEN_KH_NH, :TK_KB_NH, :ISACTIVE, :PHI_GD, :PHI_VAT, :PT_TINHPHI, :SEQ_NO, :REF_NO, :RM_REF_NO, :QUAN_HUYENNNTIEN, " & _
                              ":TINH_TPNNTIEN, :MA_HQ, :LOAI_TT, :TEN_HQ, :MA_HQ_PH, :TEN_HQ_PH, :MA_NH_TT, :MA_SANPHAM, :TT_CITAD, :MA_NTK, :KENH_CT, " & _
                              ":MA_CN, :partition_id, :transaction_id, :save_type, :tran_type, " & _
                              ":value_date, :so_lo, :note, :address, :remarks, :TEN_CQTHU, :NAME_TEN_CQTHU, :SO_TIEN_CHU, :TEN_LTHUE, " & _
                              ":TEN_LHXNK, :TEN_KB, :MA_CUC, :TEN_CUC, :LOAI_NNT, TO_DATE (:PO_Date, 'dd/MM/yyyy HH24:MI:SS'), :TT_THU, " & _
                              ":Ma_Huyen_NNThue, :Ma_Tinh_NNThue, :MA_QUAN_HUYENNNTIEN, :MA_TINH_TPNNTIEN, TO_DATE(:Ngay_HT, 'dd/MM/yyyy HH24:MI:SS'), TO_DATE(:Ngay_TK, 'dd/MM/yyyy HH24:MI:SS')," & _
                              "TO_DATE (:NGAY_KH_NH, 'dd/MM/yyyy HH24:MI:SS'), TO_DATE (:Ngay_BK, 'dd/MM/yyyy HH24:MI:SS'), :LOAI_CTU, :SO_CMND, :SO_FONE)"

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(HDR.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_KB", ParameterDirection.Input, CStr(HDR.Ngay_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, CStr(HDR.Ma_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BT", ParameterDirection.Input, CStr(HDR.So_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_DThu", ParameterDirection.Input, CStr(HDR.Ma_DThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BThu", ParameterDirection.Input, CStr(HDR.So_BThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KyHieu_CT", ParameterDirection.Input, CStr(HDR.KyHieu_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_CT_NH", ParameterDirection.Input, CStr(HDR.So_CT_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NNTien", ParameterDirection.Input, CStr(HDR.Ma_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NNTien", ParameterDirection.Input, CStr(HDR.Ten_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTien", ParameterDirection.Input, CStr(HDR.DC_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NNThue", ParameterDirection.Input, CStr(HDR.Ma_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NNThue", ParameterDirection.Input, CStr(HDR.Ten_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNThue", ParameterDirection.Input, CStr(HDR.DC_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ly_Do", ParameterDirection.Input, CStr(HDR.Ly_Do), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_KS", ParameterDirection.Input, CStr(HDR.Ma_KS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_TQ", ParameterDirection.Input, CStr(HDR.Ma_TQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_QD", ParameterDirection.Input, CStr(HDR.So_QD), DbType.String))
                    'If String.IsNullOrEmpty(HDR.Ngay_QD) Then HDR.Ngay_QD = DateTime.Now
                    'Dim today As Date = DateTime.ParseExact(HDR.Ngay_QD, "dd/MM/yyyy HH:mm tt", CultureInfo.CurrentCulture)
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_QD", ParameterDirection.Input, HDR.Ngay_QD, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":CQ_QD", ParameterDirection.Input, CStr(HDR.CQ_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_CT", ParameterDirection.Input, CStr(HDR.Ngay_CT), DbType.String))
                    'Dim dNgay_HT As DateTime = DateTime.ParseExact(HDR.Ngay_HT, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_HT", ParameterDirection.Input, HDR.Ngay_HT, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_CQThu", ParameterDirection.Input, CStr(HDR.Ma_CQThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":XA_ID", ParameterDirection.Input, CStr(HDR.XA_ID), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Tinh", ParameterDirection.Input, CStr(HDR.Ma_Tinh), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Huyen", ParameterDirection.Input, CStr(HDR.Ma_Huyen), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Xa", ParameterDirection.Input, CStr(HDR.Ma_Xa), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_No", ParameterDirection.Input, CStr(HDR.TK_No), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_Co", ParameterDirection.Input, CStr(HDR.TK_Co), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_TK", ParameterDirection.Input, CStr(HDR.So_TK), DbType.String))
                    'If String.IsNullOrEmpty(HDR.Ngay_TK) Then HDR.Ngay_TK = DateTime.Now
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_TK", ParameterDirection.Input, HDR.Ngay_TK, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":LH_XNK", ParameterDirection.Input, CStr(HDR.LH_XNK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DVSDNS", ParameterDirection.Input, CStr(HDR.DVSDNS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_DVSDNS", ParameterDirection.Input, CStr(HDR.Ten_DVSDNS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NT", ParameterDirection.Input, CStr(HDR.Ma_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ty_Gia", ParameterDirection.Input, CStr(HDR.Ty_Gia), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TG_ID", ParameterDirection.Input, CStr(HDR.TG_ID), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_LThue", ParameterDirection.Input, CStr(HDR.Ma_LThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_Khung", ParameterDirection.Input, CStr(HDR.So_Khung), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_May", ParameterDirection.Input, CStr(HDR.So_May), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(HDR.TK_KH_NH), DbType.String))
                    'If String.IsNullOrEmpty(HDR.NGAY_KH_NH) Then HDR.NGAY_KH_NH = DateTime.Now
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, HDR.NGAY_KH_NH, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, CStr(HDR.MA_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_B", ParameterDirection.Input, CStr(HDR.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NH_B", ParameterDirection.Input, CStr(HDR.Ten_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NH_TT", ParameterDirection.Input, CStr(HDR.TEN_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTien", ParameterDirection.Input, CStr(HDR.TTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_TThu", ParameterDirection.Input, CStr(HDR.TT_TThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Lan_In", ParameterDirection.Input, CStr(HDR.Lan_In), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Trang_Thai", ParameterDirection.Input, CStr(HDR.Trang_Thai), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_Nhan", ParameterDirection.Input, CStr(HDR.TK_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_KH_Nhan", ParameterDirection.Input, CStr(HDR.Ten_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Diachi_KH_Nhan", ParameterDirection.Input, CStr(HDR.Diachi_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTien_NT", ParameterDirection.Input, CStr(HDR.TTien_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(HDR.PT_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BK", ParameterDirection.Input, CStr(HDR.So_BK), DbType.String))
                    'If String.IsNullOrEmpty(HDR.Ngay_BK) Then HDR.Ngay_BK = DateTime.Now
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_BK", ParameterDirection.Input, HDR.Ngay_BK, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":CTU_GNT_BL", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_CT", ParameterDirection.Input, CStr(HDR.So_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_GL_NH", ParameterDirection.Input, CStr(HDR.TK_GL_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(HDR.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KB_NH", ParameterDirection.Input, CStr(HDR.TK_KB_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ISACTIVE", ParameterDirection.Input, 1, DbType.Int32))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD", ParameterDirection.Input, CStr(HDR.PHI_GD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT", ParameterDirection.Input, CStr(HDR.PHI_VAT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TINHPHI", ParameterDirection.Input, CStr(HDR.PT_TINHPHI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SEQ_NO", ParameterDirection.Input, CStr(HDR.SEQ_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":REF_NO", ParameterDirection.Input, CStr(HDR.REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":RM_REF_NO", ParameterDirection.Input, CStr(HDR.RM_REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(HDR.QUAN_HUYEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TINH_TPNNTIEN", ParameterDirection.Input, CStr(HDR.TINH_TPHO_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(HDR.MA_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_TT", ParameterDirection.Input, CStr(HDR.LOAI_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, CStr(HDR.TEN_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, CStr(HDR.Ma_hq_ph), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, CStr(HDR.TEN_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(HDR.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_SANPHAM", ParameterDirection.Input, CStr(HDR.SAN_PHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_CITAD", ParameterDirection.Input, CStr(HDR.TT_CITAD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(HDR.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KENH_CT", ParameterDirection.Input, CStr(HDR.Kenh_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(HDR.Ma_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":partition_id", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":transaction_id", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":save_type", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":record_type", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":tran_type", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":gl_glsub", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":method", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":dup_allowflg", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":mod_allowflg", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":ac", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":file_import", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":debit_method", ParameterDirection.Input, Nothing, DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":value_date", ParameterDirection.Input, CInt(DateTime.Now.ToString("yyyyMMdd")), DbType.Int32))
                    listParam.Add(DataAccess.NewDBParameter(":so_lo", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":note", ParameterDirection.Input, CStr(HDR.Ghi_chu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":address", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":remarks", ParameterDirection.Input, CStr(HDR.Ghi_chu), DbType.String))

                    HDR.Ghi_chu = Globals.RemoveSign4VietnameseString(HDR.Ghi_chu)
                    Dim strRemarks As String = HDR.Ghi_chu
                    If strRemarks.Length > 210 Then
                        strRemarks = strRemarks.Substring(0, 210)
                    End If

                    listParam.Add(DataAccess.NewDBParameter(":remarks", ParameterDirection.Input, CStr(strRemarks), DbType.String))


                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(HDR.Ten_cqthu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NAME_TEN_CQTHU", ParameterDirection.Input, CStr(HDR.Ten_cqthu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_TIEN_CHU", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LTHUE", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHXNK", ParameterDirection.Input, CStr(HDR.Ten_lhxnk), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(HDR.Ten_kb), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CUC", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CUC", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":BENE_SHORT_NAME", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_NNT", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":IMPORT_FILE", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PO_Date", ParameterDirection.Input, DateTime.Now.ToString("dd/MM/yyyy"), DbType.Date))
                    'listParam.Add(DataAccess.NewDBParameter(":HILOWVALUE", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_THU", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Huyen_NNThue", ParameterDirection.Input, CStr(HDR.Ma_Huyen_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Tinh_NNThue", ParameterDirection.Input, CStr(HDR.Ma_Tinh_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(HDR.MA_QUAN_HUYENNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_TPNNTIEN", ParameterDirection.Input, CStr(HDR.MA_TINH_TPNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_CTU", ParameterDirection.Input, CStr(HDR.LOAI_CTU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":SO_CMND", ParameterDirection.Input, CStr(HDR.SO_CMND), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_FONE", ParameterDirection.Input, CStr(HDR.SO_FONE), DbType.String))

                    Dim v_sqlT As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....

                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If listDTL IsNot Nothing AndAlso listDTL.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each DTL In listDTL
                                DTL.ID = HaiQuanController.GenSequenceKeyTable("TCS_CTU_DTL_SEQ.NEXTVAL")
                                DTL.SO_CT = HDR.So_CT
                                DTL.SHKB = HDR.SHKB
                                DTL.Ngay_KB = HDR.Ngay_KB
                                DTL.Ma_NV = HDR.Ma_NV
                                DTL.So_BT = HDR.So_BT
                                DTL.Ma_DThu = HDR.Ma_DThu

                                iRet = 0
                                sSQL = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                                        "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                                        "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,SO_CT,ma_dp, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ,SAC_THUE,TT_BTOAN) " & _
                                        "Values (:ID, :SHKB, :Ngay_KB, :Ma_NV, :So_BT, :Ma_DThu, :CCH_ID, :Ma_Cap, " & _
                                        ":Ma_Chuong, :LKH_ID, :Ma_Loai, :Ma_Khoan, :MTM_ID, :Ma_Muc, :Ma_TMuc, :Noi_Dung," & _
                                        ":DT_ID, :Ma_TLDT, :Ky_Thue, :SoTien, :SoTien_NT, :maquy, :SO_CT, :ma_dp, :SO_TK, TO_DATE (:NGAY_TK, 'dd/MM/yyyy HH24:MI:SS'), :MA_LHXNK, :MA_LT, :MA_HQ, :SAC_THUE, :TT_BTOAN) "

                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":ID", ParameterDirection.Input, DTL.ID, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(DTL.SHKB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ngay_KB", ParameterDirection.Input, CStr(DTL.Ngay_KB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, CStr(DTL.Ma_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":So_BT", ParameterDirection.Input, CStr(DTL.So_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_DThu", ParameterDirection.Input, CStr(DTL.Ma_DThu), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":CCH_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Cap", ParameterDirection.Input, CStr(DTL.Ma_Cap), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Chuong", ParameterDirection.Input, CStr(DTL.Ma_Chuong), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":LKH_ID", ParameterDirection.Input, CStr(DTL.LKH_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Loai", ParameterDirection.Input, CStr(DTL.Ma_Loai), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Khoan", ParameterDirection.Input, CStr(DTL.Ma_Khoan), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MTM_ID", ParameterDirection.Input, CStr(DTL.MTM_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Muc", ParameterDirection.Input, CStr(DTL.Ma_Muc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TMuc", ParameterDirection.Input, CStr(DTL.Ma_TMuc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Noi_Dung", ParameterDirection.Input, CStr(DTL.Noi_Dung), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":DT_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TLDT", ParameterDirection.Input, CStr(DTL.Ma_TLDT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ky_Thue", ParameterDirection.Input, CStr(DTL.Ky_Thue), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien", ParameterDirection.Input, CStr(DTL.SoTien), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien_NT", ParameterDirection.Input, CStr(DTL.SoTien_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":maquy", ParameterDirection.Input, CStr(DTL.MaQuy), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(DTL.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":ma_dp", ParameterDirection.Input, CStr(DTL.Ma_DP), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(DTL.SO_TK), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, DTL.NGAY_TK, DbType.Date))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LHXNK", ParameterDirection.Input, CStr(DTL.LH_XNK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LT", ParameterDirection.Input, CStr(DTL.MA_LT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(DTL.MA_HQ), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(DTL.SAC_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":TT_BTOAN", ParameterDirection.Input, CStr(DTL.TT_BTOAN), DbType.String))

                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function

        Public Shared Function Update_ChungTu(ByVal HDR As NewChungTu.infChungTuHDR, ByVal listDTL As List(Of NewChungTu.infChungTuDTL), Optional ByVal sTrangThaiChungTu As String = "", Optional ByVal sTrangThaiChuyenThue As String = "") As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If HDR IsNot Nothing Then
                    conn = VBOracleLib.DataAccess.GetConnection()
                    ' conn.Open()
                    tran = conn.BeginTransaction()
                    'delete DTL
                    sSQL = "Delete from TCS_CTU_DTL where So_CT = :So_CT"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":So_CT", ParameterDirection.Input, CStr(HDR.So_CT), DbType.String))
                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)

                    'update HDR
                    'Bo qua ko update cac cot: KYHIEU_CT, SO_BT, NGAY_KB
                    sSQL = "Update TCS_CTU_HDR set " & _
                           "SHKB = :SHKB," & _
                           "So_BThu = :So_BThu," & _
                           "So_CT_NH = :So_CT_NH," & _
                           "Ma_NNTien = :Ma_NNTien," & _
                           "Ten_NNTien = :Ten_NNTien," & _
                           "DC_NNTien = :DC_NNTien," & _
                           "Ma_NNThue = :Ma_NNThue," & _
                           "Ten_NNThue = :Ten_NNThue," & _
                           "DC_NNThue = :DC_NNThue," & _
                           "Ma_KS = :Ma_KS," & _
                           "So_QD = :So_QD," & _
                           "MA_NTK = :MA_NTK," & _
                           "PT_TT = :PT_TT," & _
                           "TK_KB_NH = :TK_KB_NH," & _
                           "Ngay_QD = TO_DATE(:Ngay_QD, 'dd/MM/yyyy HH24:MI:SS')," & _
                           "CQ_QD = :CQ_QD," & _
                           "Ngay_CT = :Ngay_CT," & _
                           "Ngay_HT = TO_DATE(:Ngay_HT, 'dd/MM/yyyy HH24:MI:SS')," & _
                           "Ma_CQThu = :Ma_CQThu," & _
                           "TEN_CQTHU = :TEN_CQTHU," & _
                           "NAME_TEN_CQTHU = :NAME_TEN_CQTHU," & _
                           "XA_ID = :XA_ID," & _
                           "Ma_Tinh = :Ma_Tinh," & _
                           "Ma_Huyen = :Ma_Huyen," & _
                           "Ma_Xa = :Ma_Xa," & _
                           "TK_No = :TK_No," & _
                           "TK_Co = :TK_Co," & _
                           "SO_TIEN_CHU = :SO_TIEN_CHU," & _
                           "So_TK = :So_TK," & _
                           "Ngay_TK = TO_DATE(:Ngay_TK, 'dd/MM/yyyy HH24:MI:SS')," & _
                           "LH_XNK = :LH_XNK," & _
                           "DVSDNS = :DVSDNS," & _
                           "Ten_DVSDNS = :Ten_DVSDNS," & _
                           "remarks = :remarks," & _
                           "Ma_NT = :Ma_NT," & _
                           "Ty_Gia = :Ty_Gia," & _
                           "TG_ID = :TG_ID," & _
                           "Ma_LThue = :Ma_LThue," & _
                           "So_Khung = :So_Khung," & _
                           "So_May = :So_May," & _
                           "Ten_KH_Nhan = :Ten_KH_Nhan," & _
                           "TK_KH_NH = :TK_KH_NH," & _
                           "NGAY_KH_NH = TO_DATE(:NGAY_KH_NH, 'dd/MM/yyyy HH24:MI:SS')," & _
                           "MA_NH_A = :MA_NH_A," & _
                           "MA_NH_B = :MA_NH_B," & _
                           "MA_NH_TT = :MA_NH_TT," & _
                           "Ten_NH_B = :Ten_NH_B," & _
                           "Ten_NH_TT = :Ten_NH_TT," & _
                           "TTien = :TTien," & _
                           "Lan_In = :Lan_In," & _
                           "LOAI_TT = :LOAI_TT," & _
                           "Trang_Thai = :Trang_Thai," & _
                           "TEN_KB = :TEN_KB," & _
                           "PHI_GD = :PHI_GD," & _
                           "PT_TINHPHI = :PT_TINHPHI," & _
                           "PHI_VAT = :PHI_VAT," & _
                           "TK_KH_Nhan = :TK_KH_Nhan," & _
                           "Diachi_KH_Nhan = :Diachi_KH_Nhan," & _
                           "TTien_NT = :TTien_NT," & _
                           "Ly_Do = :Ly_Do," & _
                           "So_BK = :So_BK," & _
                           "MA_SANPHAM = :MA_SANPHAM," & _
                           "TT_CITAD = :TT_CITAD," & _
                           "Ngay_BK = TO_DATE(:Ngay_BK, 'dd/MM/yyyy HH24:MI:SS')," & _
                           "QUAN_HUYENNNTIEN = :QUAN_HUYENNNTIEN," & _
                           "TINH_TPNNTIEN = :TINH_TPNNTIEN," & _
                           "MA_HQ = :MA_HQ," & _
                           "TEN_HQ = :TEN_HQ," & _
                           "TT_TThu = :TT_TThu," & _
                           "MA_HQ_PH = :MA_HQ_PH," & _
                           "TEN_HQ_PH = :TEN_HQ_PH," & _
                           "MA_CN = :MA_CN," & _
                           "value_date = :value_date," & _
                           "so_lo = :so_lo," & _
                           "MA_CUC = :MA_CUC," & _
                           "TEN_CUC = :TEN_CUC," & _
                           "LOAI_NNT = :LOAI_NNT," & _
                           "note = :note," & _
                           "TT_THU = :TT_THU," & _
                           "Ma_NV = :Ma_NV," & _
                           "Ma_DThu = :Ma_DThu," & _
                           "Ma_TQ = :Ma_TQ," & _
                           "CTU_GNT_BL = :CTU_GNT_BL," & _
                           "TK_GL_NH = :TK_GL_NH," & _
                           "TEN_KH_NH = :TEN_KH_NH," & _
                           "ISACTIVE = :ISACTIVE," & _
                           "SEQ_NO = :SEQ_NO," & _
                           "REF_NO = :REF_NO," & _
                           "RM_REF_NO = :RM_REF_NO," & _
                           "KENH_CT = :KENH_CT," & _
                           "partition_id = :partition_id," & _
                           "transaction_id = :transaction_id," & _
                           "save_type = :save_type," & _
                           "tran_type = :tran_type," & _
                           "address = :address," & _
                           "TEN_LTHUE = :TEN_LTHUE," & _
                           "TEN_LHXNK = :TEN_LHXNK," & _
                           "PO_Date = TO_DATE(:PO_Date, 'dd/MM/yyyy HH24:MI:SS')," & _
                           "Ma_Huyen_NNThue = :Ma_Huyen_NNThue," & _
                           "Ma_Tinh_NNThue = :Ma_Tinh_NNThue," & _
                           "MA_QUAN_HUYENNNTIEN = :MA_QUAN_HUYENNNTIEN," & _
                           "MA_TINH_TPNNTIEN = :MA_TINH_TPNNTIEN," & _
                           "LOAI_CTU = :LOAI_CTU, " & _
                            "NGAY_BAOCO = :NGAY_BAOCO," & _
                            "NGAY_BAONO = :NGAY_BAONO" & _
                           " where So_CT  = :So_CT"

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(HDR.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BThu", ParameterDirection.Input, CStr(HDR.So_BThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_CT_NH", ParameterDirection.Input, CStr(HDR.So_CT_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NNTien", ParameterDirection.Input, CStr(HDR.Ma_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NNTien", ParameterDirection.Input, CStr(HDR.Ten_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTien", ParameterDirection.Input, CStr(HDR.DC_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NNThue", ParameterDirection.Input, CStr(HDR.Ma_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NNThue", ParameterDirection.Input, CStr(HDR.Ten_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNThue", ParameterDirection.Input, CStr(HDR.DC_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_KS", ParameterDirection.Input, CStr(HDR.Ma_KS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_QD", ParameterDirection.Input, CStr(HDR.So_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(HDR.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(HDR.PT_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KB_NH", ParameterDirection.Input, CStr(HDR.TK_KB_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_QD", ParameterDirection.Input, HDR.Ngay_QD, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":CQ_QD", ParameterDirection.Input, CStr(HDR.CQ_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_CT", ParameterDirection.Input, HDR.Ngay_CT, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_HT", ParameterDirection.Input, HDR.Ngay_HT, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_CQThu", ParameterDirection.Input, CStr(HDR.Ma_CQThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(HDR.Ten_cqthu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NAME_TEN_CQTHU", ParameterDirection.Input, CStr(HDR.Ten_cqthu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":XA_ID", ParameterDirection.Input, CStr(HDR.XA_ID), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Tinh", ParameterDirection.Input, CStr(HDR.Ma_Tinh), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Huyen", ParameterDirection.Input, CStr(HDR.Ma_Huyen), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Xa", ParameterDirection.Input, CStr(HDR.Ma_Xa), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_No", ParameterDirection.Input, CStr(HDR.TK_No), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_Co", ParameterDirection.Input, CStr(HDR.TK_Co), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_TIEN_CHU", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_TK", ParameterDirection.Input, CStr(HDR.So_TK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_TK", ParameterDirection.Input, DateTime.Now, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":LH_XNK", ParameterDirection.Input, CStr(HDR.LH_XNK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DVSDNS", ParameterDirection.Input, CStr(HDR.DVSDNS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_DVSDNS", ParameterDirection.Input, CStr(HDR.Ten_DVSDNS), DbType.String))

                    HDR.Ghi_chu = Globals.RemoveSign4VietnameseString(HDR.Ghi_chu)
                    Dim strRemarks As String = HDR.Ghi_chu
                    If strRemarks.Length > 210 Then
                        strRemarks = strRemarks.Substring(0, 210)
                    End If

                    listParam.Add(DataAccess.NewDBParameter(":Ma_NT", ParameterDirection.Input, CStr(HDR.Ma_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ty_Gia", ParameterDirection.Input, CStr(HDR.Ty_Gia), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TG_ID", ParameterDirection.Input, CStr(HDR.TG_ID), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_LThue", ParameterDirection.Input, CStr(HDR.Ma_LThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_Khung", ParameterDirection.Input, CStr(HDR.So_Khung), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_May", ParameterDirection.Input, CStr(HDR.So_May), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_KH_Nhan", ParameterDirection.Input, CStr(HDR.Ten_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(HDR.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, HDR.NGAY_KH_NH, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, CStr(HDR.MA_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_B", ParameterDirection.Input, CStr(HDR.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(HDR.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NH_B", ParameterDirection.Input, CStr(HDR.Ten_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NH_TT", ParameterDirection.Input, CStr(HDR.TEN_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTien", ParameterDirection.Input, CStr(HDR.TTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Lan_In", ParameterDirection.Input, CStr(HDR.Lan_In), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_TT", ParameterDirection.Input, CStr(HDR.LOAI_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Trang_Thai", ParameterDirection.Input, CStr(HDR.Trang_Thai), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(HDR.Ten_kb), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD", ParameterDirection.Input, CStr(HDR.PHI_GD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TINHPHI", ParameterDirection.Input, CStr(HDR.PT_TINHPHI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT", ParameterDirection.Input, CStr(HDR.PHI_VAT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_Nhan", ParameterDirection.Input, CStr(HDR.TK_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Diachi_KH_Nhan", ParameterDirection.Input, CStr(HDR.Diachi_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTien_NT", ParameterDirection.Input, CStr(HDR.TTien_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ly_Do", ParameterDirection.Input, CStr(HDR.Ly_Do), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BK", ParameterDirection.Input, CStr(HDR.So_BK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_SANPHAM", ParameterDirection.Input, CStr(HDR.SAN_PHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_CITAD", ParameterDirection.Input, CStr(HDR.TT_CITAD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_BK", ParameterDirection.Input, DateTime.Now, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(HDR.QUAN_HUYEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TINH_TPNNTIEN", ParameterDirection.Input, CStr(HDR.TINH_TPHO_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(HDR.MA_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, CStr(HDR.TEN_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_TThu", ParameterDirection.Input, CStr(HDR.TT_TThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, CStr(HDR.Ma_hq_ph), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, CStr(HDR.TEN_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(HDR.Ma_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":value_date", ParameterDirection.Input, CInt(DateTime.Now.ToString("yyyyMMdd")), DbType.Int32))
                    listParam.Add(DataAccess.NewDBParameter(":so_lo", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CUC", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CUC", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_NNT", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":note", ParameterDirection.Input, CStr(HDR.Ghi_chu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_THU", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":Ngay_KB", ParameterDirection.Input, CStr(HDR.Ngay_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, CStr(HDR.Ma_NV), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":So_BT", ParameterDirection.Input, CStr(HDR.So_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_DThu", ParameterDirection.Input, CStr(HDR.Ma_DThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_TQ", ParameterDirection.Input, CStr(HDR.Ma_TQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":CTU_GNT_BL", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_GL_NH", ParameterDirection.Input, CStr(HDR.TK_GL_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(HDR.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ISACTIVE", ParameterDirection.Input, 1, DbType.Int32))
                    listParam.Add(DataAccess.NewDBParameter(":SEQ_NO", ParameterDirection.Input, CStr(HDR.SEQ_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":REF_NO", ParameterDirection.Input, CStr(HDR.REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":RM_REF_NO", ParameterDirection.Input, CStr(HDR.RM_REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KENH_CT", ParameterDirection.Input, CStr(HDR.Kenh_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":partition_id", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":transaction_id", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":save_type", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":tran_type", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":address", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LTHUE", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHXNK", ParameterDirection.Input, CStr(HDR.Ten_lhxnk), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PO_Date", ParameterDirection.Input, DateTime.Now, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Huyen_NNThue", ParameterDirection.Input, CStr(HDR.Ma_Huyen_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Tinh_NNThue", ParameterDirection.Input, CStr(HDR.Ma_Tinh_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(HDR.MA_QUAN_HUYENNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_TPNNTIEN", ParameterDirection.Input, CStr(HDR.MA_TINH_TPNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_CTU", ParameterDirection.Input, CStr(HDR.LOAI_CTU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_BAOCO", ParameterDirection.Input, HDR.NGAY_BAOCO, DbType.DateTime))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_BAONO", ParameterDirection.Input, HDR.NGAY_BAONO, DbType.DateTime))
                    listParam.Add(DataAccess.NewDBParameter(":So_CT", ParameterDirection.Input, CStr(HDR.So_CT), DbType.String))

                    If Not String.IsNullOrEmpty(sTrangThaiChungTu) Then
                        sSQL = sSQL & " and Trang_Thai  = :Trang_Thai"
                        listParam.Add(DataAccess.NewDBParameter(":Trang_Thai", ParameterDirection.Input, sTrangThaiChungTu, DbType.String))
                    End If
                    If Not String.IsNullOrEmpty(sTrangThaiChuyenThue) Then
                        sSQL = sSQL & " and tt_cthue = :tt_cthue"
                        listParam.Add(DataAccess.NewDBParameter(":tt_cthue", ParameterDirection.Input, sTrangThaiChuyenThue, DbType.Int32))
                    End If

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    If iRet > 0 Then 'if exist ctu by so_ct then execute update dtl list otherwise rollback
                        If listDTL IsNot Nothing AndAlso listDTL.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each DTL In listDTL
                                'insert DTL
                                DTL.ID = HaiQuanController.GenSequenceKeyTable("TCS_CTU_DTL_SEQ.NEXTVAL")
                                DTL.SO_CT = HDR.So_CT
                                DTL.SHKB = HDR.SHKB
                                DTL.Ngay_KB = HDR.Ngay_KB
                                DTL.Ma_NV = HDR.Ma_NV
                                DTL.So_BT = HDR.So_BT
                                DTL.Ma_DThu = HDR.Ma_DThu

                                iRet = 0
                                sSQL = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                                        "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                                        "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,SO_CT,ma_dp, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ,SAC_THUE,TT_BTOAN) " & _
                                        "Values (:ID, :SHKB, :Ngay_KB, :Ma_NV, :So_BT, :Ma_DThu, :CCH_ID, :Ma_Cap, " & _
                                        ":Ma_Chuong, :LKH_ID, :Ma_Loai, :Ma_Khoan, :MTM_ID, :Ma_Muc, :Ma_TMuc, :Noi_Dung," & _
                                        ":DT_ID, :Ma_TLDT, :Ky_Thue, :SoTien, :SoTien_NT, :maquy, :SO_CT, :ma_dp, :SO_TK, TO_DATE(:NGAY_TK, 'dd/MM/yyyy HH24:MI:SS'), :MA_LHXNK, :MA_LT, :MA_HQ, :SAC_THUE, :TT_BTOAN) "

                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":ID", ParameterDirection.Input, DTL.ID, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(DTL.SHKB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ngay_KB", ParameterDirection.Input, CStr(DTL.Ngay_KB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, CStr(DTL.Ma_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":So_BT", ParameterDirection.Input, CStr(DTL.So_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_DThu", ParameterDirection.Input, CStr(DTL.Ma_DThu), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":CCH_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Cap", ParameterDirection.Input, CStr(DTL.Ma_Cap), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Chuong", ParameterDirection.Input, CStr(DTL.Ma_Chuong), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":LKH_ID", ParameterDirection.Input, CStr(DTL.LKH_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Loai", ParameterDirection.Input, CStr(DTL.Ma_Loai), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Khoan", ParameterDirection.Input, CStr(DTL.Ma_Khoan), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MTM_ID", ParameterDirection.Input, CStr(DTL.MTM_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Muc", ParameterDirection.Input, CStr(DTL.Ma_Muc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TMuc", ParameterDirection.Input, CStr(DTL.Ma_TMuc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Noi_Dung", ParameterDirection.Input, CStr(DTL.Noi_Dung), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":DT_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TLDT", ParameterDirection.Input, CStr(DTL.Ma_TLDT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ky_Thue", ParameterDirection.Input, CStr(DTL.Ky_Thue), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien", ParameterDirection.Input, CStr(DTL.SoTien), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien_NT", ParameterDirection.Input, CStr(DTL.SoTien_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":maquy", ParameterDirection.Input, CStr(DTL.MaQuy), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(DTL.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":ma_dp", ParameterDirection.Input, CStr(DTL.Ma_DP), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(DTL.SO_TK), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, DTL.NGAY_TK, DbType.Date))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LHXNK", ParameterDirection.Input, CStr(DTL.LH_XNK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LT", ParameterDirection.Input, CStr(DTL.MA_LT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(DTL.MA_HQ), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(DTL.SAC_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":TT_BTOAN", ParameterDirection.Input, CStr(DTL.TT_BTOAN), DbType.String))

                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()

                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function

        Public Shared Function Update_ChungTu2(ByVal HDR As NewChungTu.infChungTuHDR, ByVal listDTL As List(Of NewChungTu.infChungTuDTL)) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If HDR IsNot Nothing Then
                    conn = VBOracleLib.DataAccess.GetConnection()
                    ' conn.Open()
                    tran = conn.BeginTransaction()
                    'delete DTL
                    sSQL = "Delete from TCS_CTU_DTL where So_CT = :So_CT"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":So_CT", ParameterDirection.Input, CStr(HDR.So_CT), DbType.String))
                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)

                    sSQL = "UPDATE tcs_ctu_hdr a set a.ma_dthu= '" & HDR.Ma_DThu & "', a.so_bthu= '" & HDR.So_BThu & "', " & _
                                "a.so_ct_nh= '" & HDR.So_CT_NH & "', a.ma_nntien= '" & HDR.Ma_NNTien & "', " & _
                                "a.ten_nntien= '" & HDR.Ten_NNTien & "', a.dc_nntien= '" & HDR.DC_NNTien & "', a.ma_nnthue= '" & HDR.Ma_NNThue & "', a.ten_nnthue= '" & HDR.Ten_NNThue & "', " & _
                                "a.dc_nnthue= '" & HDR.DC_NNThue & "', a.ly_do= '" & HDR.Ly_Do & "', a.ma_ks= '" & HDR.Ma_KS & "', a.ngay_ct= '" & HDR.Ngay_CT & "', " & _
                                "a.ngay_ht= sysdate, a.ma_cqthu= '" & HDR.Ma_CQThu & "', a.xa_id= '" & HDR.XA_ID & "', a.ma_tinh= '" & HDR.Ma_Tinh & "', " & _
                                "a.ma_huyen= '" & HDR.Ma_Huyen & "', a.ma_xa= '" & HDR.Ma_Xa & "', a.tk_no= '" & HDR.TK_No & "', a.tk_co= '" & HDR.TK_Co & "', " & _
                                "a.ngay_tk= sysdate, a.lh_xnk= '" & HDR.LH_XNK & "', " & _
                                "a.ma_nt= '" & HDR.Ma_NT & "', a.ty_gia= '" & HDR.Ty_Gia & "', a.tg_id= '" & HDR.TG_ID & "', " & _
                                "a.ma_lthue= '" & HDR.Ma_LThue & "', a.so_khung= '" & HDR.So_Khung & "', a.so_may= '" & HDR.So_May & "', a.tk_kh_nh= '" & HDR.TK_KH_NH & "', " & _
                                "a.ma_nh_a= '" & HDR.MA_NH_A & "', a.ma_nh_b= '" & HDR.MA_NH_B & "',a.ttien= '" & HDR.TTien & "', " & _
                                "a.tt_tthu= '" & HDR.TT_TThu & "', a.lan_in= '" & HDR.Lan_In & "', a.trang_thai= '" & HDR.Trang_Thai & "', a.tk_kh_nhan= '" & HDR.TK_KH_Nhan & "'," & _
                                "a.ten_kh_nhan= '" & HDR.Ten_KH_Nhan & "', a.diachi_kh_nhan= '" & HDR.Diachi_KH_Nhan & "', a.ttien_nt= '" & HDR.TTien_NT & "', a.so_bt_ktkb= '" & HDR.So_BT_KTKB & "'," & _
                                "a.pt_tt= '" & HDR.PT_TT & "', a.ref_no= '" & HDR.REF_NO & "', " & _
                                "a.ten_kh_nh= '" & HDR.TEN_KH_NH & "', a.tk_gl_nh= '" & HDR.TK_GL_NH & "', a.tk_kb_nh= '" & HDR.TK_KB_NH & "',a.seq_no= '" & HDR.SEQ_NO & "', " & _
                                "a.phi_gd= '" & HDR.PHI_GD & "', a.phi_vat= '" & HDR.PHI_VAT & "', a.pt_tinhphi= '" & HDR.PT_TINHPHI & "',a.tt_cthue= '" & HDR.TT_CTHUE & "', " & _
                                "a.rm_ref_no= '" & HDR.RM_REF_NO & "', a.quan_huyennntien= '" & HDR.QUAN_HUYEN_NNTIEN & "', a.tinh_tpnntien= '" & HDR.TINH_TPHO_NNTIEN & "', " & _
                                "a.tkht= '" & HDR.TKHT_NH & "', a.ly_do_huy= '" & HDR.Ly_Do & "', a.ma_hq= '" & HDR.MA_HQ & "'," & _
                                "a.ma_ntk= '" & HDR.MA_NTK & "', a.ten_hq= '" & HDR.TEN_HQ & "', a.ma_hq_ph= '" & HDR.Ma_hq_ph & "', a.ten_hq_ph= '" & HDR.TEN_HQ_PH & "', " & _
                                "a.ma_nh_tt= '" & HDR.MA_NH_TT & "', a.ten_nh_tt= '" & HDR.TEN_NH_TT & "', a.kenh_ct= '" & HDR.Kenh_CT & "', a.ma_sanpham= '" & HDR.SAN_PHAM & "', " & _
                                "a.tt_citad= '" & HDR.TT_CITAD & "', a.phi_gd2= '" & HDR.PHI_GD2 & "', a.phi_vat2= '" & HDR.PHI_VAT2 & "',a.so_cmnd= '" & HDR.SO_CMND & "', " & _
                                "a.so_fone= '" & HDR.SO_FONE & "', a.ten_nh_b= '" & HDR.Ten_NH_B & "', a.ma_cn= '" & HDR.Ma_CN & "', " & _
                                "a.so_xcard= '" & HDR.SO_XCARD & "', a.loai_ctu= '" & HDR.LOAI_CTU & "', a.ma_huyen_nnthue= '" & HDR.Ma_Huyen_NNThue & "',a.ma_tinh_nnthue= '" & HDR.Ma_Tinh_NNThue & "', " & _
                                "a.ma_quan_huyennntien= '" & HDR.MA_QUAN_HUYENNNTIEN & "', a.ma_tinh_tpnntien= '" & HDR.MA_TINH_TPNNTIEN & "', " & _
                                "a.ten_kb= '" & HDR.Ten_kb & "', a.ten_lhxnk= '" & HDR.Ten_lhxnk & "', " & _
                                "a.name_ten_cqthu= '" & HDR.Ten_cqthu & "', a.ten_cqthu= '" & HDR.Ten_cqthu & "', a.remarks= '" & HDR.Ghi_chu & "' " & _
                                "where so_ct = '" & HDR.So_CT & "'"


                    iRet = DataAccess.ExecuteNonQuery(sSQL, CommandType.Text, tran)
                    If iRet > 0 Then 'if exist ctu by so_ct then execute update dtl list otherwise rollback
                        If listDTL IsNot Nothing AndAlso listDTL.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each DTL In listDTL
                                'insert DTL
                                DTL.ID = HaiQuanController.GenSequenceKeyTable("TCS_CTU_DTL_SEQ.NEXTVAL")
                                DTL.SO_CT = HDR.So_CT
                                DTL.SHKB = HDR.SHKB
                                DTL.Ngay_KB = HDR.Ngay_KB
                                DTL.Ma_NV = HDR.Ma_NV
                                DTL.So_BT = HDR.So_BT
                                DTL.Ma_DThu = HDR.Ma_DThu

                                iRet = 0
                                sSQL = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                                        "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                                        "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,SO_CT,ma_dp, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ,SAC_THUE,TT_BTOAN) " & _
                                        "Values (:ID, :SHKB, :Ngay_KB, :Ma_NV, :So_BT, :Ma_DThu, :CCH_ID, :Ma_Cap, " & _
                                        ":Ma_Chuong, :LKH_ID, :Ma_Loai, :Ma_Khoan, :MTM_ID, :Ma_Muc, :Ma_TMuc, :Noi_Dung," & _
                                        ":DT_ID, :Ma_TLDT, :Ky_Thue, :SoTien, :SoTien_NT, :maquy, :SO_CT, :ma_dp, :SO_TK, TO_DATE(:NGAY_TK, 'dd/MM/yyyy HH24:MI:SS'), :MA_LHXNK, :MA_LT, :MA_HQ, :SAC_THUE, :TT_BTOAN) "

                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":ID", ParameterDirection.Input, DTL.ID, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(DTL.SHKB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ngay_KB", ParameterDirection.Input, CStr(DTL.Ngay_KB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, CStr(DTL.Ma_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":So_BT", ParameterDirection.Input, CStr(DTL.So_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_DThu", ParameterDirection.Input, CStr(DTL.Ma_DThu), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":CCH_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Cap", ParameterDirection.Input, CStr(DTL.Ma_Cap), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Chuong", ParameterDirection.Input, CStr(DTL.Ma_Chuong), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":LKH_ID", ParameterDirection.Input, CStr(DTL.LKH_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Loai", ParameterDirection.Input, CStr(DTL.Ma_Loai), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Khoan", ParameterDirection.Input, CStr(DTL.Ma_Khoan), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MTM_ID", ParameterDirection.Input, CStr(DTL.MTM_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Muc", ParameterDirection.Input, CStr(DTL.Ma_Muc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TMuc", ParameterDirection.Input, CStr(DTL.Ma_TMuc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Noi_Dung", ParameterDirection.Input, CStr(DTL.Noi_Dung), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":DT_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TLDT", ParameterDirection.Input, CStr(DTL.Ma_TLDT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ky_Thue", ParameterDirection.Input, CStr(DTL.Ky_Thue), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien", ParameterDirection.Input, CStr(DTL.SoTien), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien_NT", ParameterDirection.Input, CStr(DTL.SoTien_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":maquy", ParameterDirection.Input, CStr(DTL.MaQuy), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(DTL.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":ma_dp", ParameterDirection.Input, CStr(DTL.Ma_DP), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(DTL.SO_TK), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, DTL.NGAY_TK, DbType.Date))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LHXNK", ParameterDirection.Input, CStr(DTL.LH_XNK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LT", ParameterDirection.Input, CStr(DTL.MA_LT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(DTL.MA_HQ), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(DTL.SAC_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":TT_BTOAN", ParameterDirection.Input, CStr(DTL.TT_BTOAN), DbType.String))

                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()

                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function

        Public Shared Function DieuChinh_ChungTu(ByVal sSoCTu As String) As Boolean
            Dim bRet As Boolean = False

            Dim iRet As Integer = 0
            Dim sSQL As String = String.Empty
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "UPDATE TCS_CTU_HDR SET TRANG_THAI = '19' WHERE TRANG_THAI = '01' AND TT_CTHUE = 1 AND SO_CT = :SO_CT"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, sSoCTu, DbType.String))

                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
                If iRet > 0 Then
                    bRet = True
                End If
            Catch ex As Exception
                bRet = False
                Throw ex
            End Try

            Return bRet
        End Function

        Private Shared Function Check_ListDTL(ByVal listDTL As List(Of NewChungTu.infChungTuDTL), _
                                            ByVal fblnBatBuocMLNS As Boolean, _
                                            ByVal fblnVND As Boolean, _
                                            ByVal fblnBatBuocTLDT As Boolean, ByVal sTenDN As String) As Integer()
            Dim intReturn(1) As Integer
            Try
                Dim strMaQuy As String = ""
                Dim i, iCol As Integer
                For i = 0 To listDTL.Count - 1
                    iCol = Valid_DTL(listDTL(i), fblnBatBuocMLNS, fblnVND, fblnBatBuocTLDT, sTenDN)
                    If (iCol <> -1) Then
                        intReturn(0) = i
                        intReturn(1) = iCol
                        Return intReturn
                    End If
                Next
                Return Nothing
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi valid dữ liệu dtl", sTenDN)
                Return Nothing
            End Try
        End Function

        Private Shared Function Valid_DTL(ByVal dtl As NewChungTu.infChungTuDTL, _
                ByVal fblnBatBuocMLNS As Boolean, _
                ByVal fblnVND As Boolean, _
                ByVal fblnBatBuocTLDT As Boolean, ByVal sTenDN As String) As Integer
            Try
                If (fblnBatBuocMLNS = True) Then

                    If (Not HaiQuanController.Valid_TMuc(dtl.Ma_TMuc, True)) Then
                        Return Common.mdlCommon.DTLColumn.MTieuMuc
                    End If
                End If

                If (dtl.SoTien = 0) Then
                    If (fblnVND) Then
                        Return Common.mdlCommon.DTLColumn.Tien
                    End If
                End If
                Return -1
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi valid chứng từ DTL", sTenDN)
                Return -1
            End Try
        End Function

        Public Shared Function Valid_TMuc(ByVal sMaTMuc As String, Optional ByVal bDangHDong As Boolean = False) As Boolean
            Dim bRet As Boolean = False
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                If bDangHDong Then
                    sSQL = "Select 1 From TCS_DM_MUC_TMUC Where MA_TMUC = :MA_TMUC and tinh_trang = '1' "
                Else
                    sSQL = "Select 1 From TCS_DM_MUC_TMUC Where MA_TMUC = :MA_TMUC"
                End If

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_TMUC", ParameterDirection.Input, CStr(sMaTMuc), DbType.String))
                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString()
                End If

                If Not String.IsNullOrEmpty(sRet) Then
                    bRet = True
                End If
            Catch ex As Exception
                sRet = ""
                bRet = False
            End Try

            Return sRet
        End Function

        Public Shared Function onBlurMaTieuMuc(ByVal sMaNDKT As String) As DataTable
            Dim v_sql As String = String.Empty
            Dim v_return As String = String.Empty
            Dim listParam As List(Of IDataParameter) = Nothing

            Dim dt As DataTable = Nothing
            Try

                v_sql = "SELECT 1 AS STT, A.SAC_THUE AS VALUE FROM TCS_MAP_TM_ST A WHERE A.MA_NDKT = :MA_NDKT AND A.TINHTRANG = 1 " & _
                        "UNION ALL " & _
                        "SELECT 2 AS STT, B.TEN AS VALUE  FROM TCS_DM_MUC_TMUC B WHERE  MA_TMUC = :MA_NDKT AND TINH_TRANG = 1"

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_NDKT", ParameterDirection.Input, CStr(sMaNDKT), DbType.String))
                dt = DataAccess.ExecuteReturnDataTable(v_sql, CommandType.Text, listParam.ToArray())

            Catch ex As Exception

            End Try
            Return dt
        End Function

        Public Shared Function getNoiDungByMaTieuMuc(ByVal v_jSon As String) As DataTable
            Dim v_sql As String = String.Empty
            Dim v_return As String = String.Empty
            Dim listParam As List(Of IDataParameter) = Nothing

            Dim dt As DataTable = Nothing
            Try

                v_sql = "SELECT MA_TMUC, TEN FROM TCS_DM_MUC_TMUC T WHERE T.MA_TMUC IN (" + v_jSon + ")"

                dt = DataAccess.ExecuteToTable(v_sql)

            Catch ex As Exception

            End Try
            Return dt
        End Function

        Public Shared Function getAndSetHDR(ByVal v_sCtu As String) As DataTable
            Dim v_sql As String = String.Empty
            Dim v_return As String = String.Empty

            Dim dt As DataTable = Nothing
            Try
                v_sql = "SELECT COUNT(1) FROM TCS_CTU_HDR A WHERE A.SO_CT = '" & v_sCtu & "'"

                Dim v_count As String = DataAccess.ExecuteSQLScalar(v_sql)

                If v_count.Length = 0 Then
                    Return dt
                Else
                    If CInt(v_count) = 0 Then
                        Return dt
                    End If
                End If

                v_sql = "SELECT A.SHKB, A.NGAY_KB, A.MA_NV, A.SO_BT, A.MA_DTHU, A.SO_BTHU," & _
                        "A.KYHIEU_CT, A.SO_CT, A.SO_CT_NH, A.MA_NNTIEN, A.TEN_NNTIEN, " & _
                        "A.DC_NNTIEN, A.MA_NNTHUE, A.TEN_NNTHUE, A.DC_NNTHUE, A.LY_DO, " & _
                        "A.MA_KS, A.MA_TQ, A.SO_QD, A.NGAY_QD, A.CQ_QD, A.NGAY_CT, " & _
                        "A.NGAY_HT, A.MA_CQTHU, A.XA_ID, A.MA_TINH, A.MA_HUYEN, A.MA_XA, " & _
                        "A.TK_NO, A.TK_CO, A.SO_TK, A.NGAY_TK, A.LH_XNK, A.DVSDNS, " & _
                        "A.TEN_DVSDNS, A.MA_NT, A.TY_GIA, A.TG_ID, A.MA_LTHUE, A.SO_KHUNG, " & _
                        "A.SO_MAY, A.TK_KH_NH, A.NGAY_KH_NH, A.MA_NH_A, A.MA_NH_B," & _
                        "A.TTIEN, A.TT_TTHU, A.LAN_IN, A.TRANG_THAI, A.TK_KH_NHAN," & _
                        "A.TEN_KH_NHAN, A.DIACHI_KH_NHAN, A.TTIEN_NT, A.SO_BT_KTKB," & _
                        "A.PT_TT, A.TT_IN, A.TT_IN_BK, A.SO_BK, A.NGAY_BK, A.CTU_GNT_BL," & _
                        "A.REF_NO, A.TT_THU, A.TEN_KH_NH, A.TK_GL_NH, A.TK_KB_NH," & _
                        "A.ISACTIVE, A.SEQ_NO, A.PHI_GD, A.PHI_VAT, A.PT_TINHPHI," & _
                        "A.TT_CTHUE, A.RM_REF_NO, A.NGAY_KS, A.QUAN_HUYENNNTIEN," & _
                        "A.TINH_TPNNTIEN, A.TKHT, A.LY_DO_HUY, A.MA_HQ, A.LOAI_TT," & _
                        "A.MA_NTK, A.TEN_HQ, A.MA_HQ_PH, A.TEN_HQ_PH, A.MA_NH_TT," & _
                        "A.TEN_NH_TT, A.KENH_CT, A.MA_SANPHAM, A.NGAY_GDV_HUY, A.MA_HUYKS," & _
                        "A.NGAY_KSV_HUY, A.MSG_LOI, A.TT_CITAD, A.PHI_GD2, A.PHI_VAT2," & _
                        "A.TIME_BEGIN, A.SO_CMND, A.SO_FONE, A.TEN_NH_B, A.MA_CN," & _
                        "A.NGAY_KSV_CT, A.SO_XCARD, A.LOAI_CTU, A.MA_HUYEN_NNTHUE," & _
                        "A.MA_TINH_NNTHUE, A.MA_QUAN_HUYENNNTIEN, A.MA_TINH_TPNNTIEN," & _
                        "A.PO_DATE, A.LOAI_NNT, A.MA_CUC, A.TEN_CUC, A.TEN_KB," & _
                        "A.TEN_LHXNK, A.TEN_LTHUE, A.SO_TIEN_CHU, A.NAME_TEN_CQTHU," & _
                        "A.TEN_CQTHU, A.REMARKS, A.ADDRESS, A.NOTE, A.SO_LO, A.VALUE_DATE," & _
                        "A.TRAN_TYPE, A.SAVE_TYPE, A.TRANSACTION_ID, A.PARTITION_ID," & _
                        "A.NGAY_BAONO, A.NGAY_BAOCO FROM TCS_CTU_HDR A WHERE A.SO_CT = '" & v_sCtu & "'"

                dt = DataAccess.ExecuteToTable(v_sql)

            Catch ex As Exception

            End Try
            Return dt
        End Function

        Public Shared Function GhiChungTu2(ByVal hdr As HaiQuanObj.HdrHq, ByVal dtl() As HaiQuanObj.Dtl, ByVal sActionType As String, ByVal sSaveType As String) As String
            Dim sTenDN As String = ""
            Dim sDesCTU As String = ""
            Dim sRet As String = ""
            Try
                'bỏ dấu trường remark trước khi insert, update
                hdr.REMARKS = Globals.RemoveSign4VietnameseString(hdr.REMARKS)

                If hdr.REMARKS.Length > 210 Then
                    hdr.REMARKS = hdr.REMARKS.Substring(0, 210)
                End If

                If hdr Is Nothing Then
                    sRet = ""
                    Throw New Exception("Không có thông tin chứng từ để lưu. Vui lòng kiểm tra lại")
                    Return sRet
                End If

                Dim objUser = Nothing
                If hdr IsNot Nothing Then objUser = New CTuUser(hdr.MA_NV)
                If objUser IsNot Nothing Then sTenDN = objUser.Ten_DN

                Dim blnVND As Boolean = True
                If (hdr.MA_NT <> "VND") Then blnVND = False
                Dim intVitriLoi() As Integer = Check_ListDTL2(dtl, True, blnVND, True, sTenDN)
                If (Not IsNothing(intVitriLoi)) Then
                    sRet = ""
                    Throw New Exception("Có lỗi trong quá trình kiểm tra chi tiết chứng từ: " & intVitriLoi(1).ToString)
                    Return sRet
                End If

                'Kiem tra xem co so_ctu chua => bo xung 1 so truong cho so chung tu do
                Dim v_dtHdr As DataTable = getAndSetHDR(hdr.SO_CT)
                If v_dtHdr IsNot Nothing Then
                    'Trang thai 00 - Cho kiem soat, 03 - Chuyen tra => duoc phep sua, update => luu lai 1 so thong tin
                    If hdr.TRANG_THAI.Equals("00") Or hdr.TRANG_THAI.Equals("03") Then
                        'Cap nhat loai tien thue => vao obj DTL
                        'For Each objDtl As HaiQuanObj.Dtl In dtl
                        '    objDtl.MA_LT = hdr.LOAI_TT
                        'Next
                    End If
                End If

                Select Case sActionType
                    Case "GHI"
                        hdr.TRANG_THAI = "00"
                        If (sSaveType = "INSERT") Then

                            If HaiQuanController.Insert_Chungtu2(hdr, dtl) Then
                                sRet = "Success;Hoàn thiện chứng từ thành công"
                            Else
                                sRet = String.Empty
                            End If
                        ElseIf sSaveType = "UPDATE" Then
                            If hdr.TRANG_THAI = "00" OrElse hdr.TRANG_THAI = "03" Then
                                hdr.TRANG_THAI = "00"
                                If HaiQuanController.Update_ChungTu22(hdr, dtl) Then
                                    sRet = "Success;Cập nhật chứng từ thành công"
                                Else
                                    sRet = String.Empty
                                End If
                            Else
                                sRet = ""
                                Throw New Exception("Trạng thái chứng từ không cho phép cập nhật")
                                Return sRet
                            End If
                        End If
                    Case "GHIKS"
                        If sSaveType = "INSERT_KS" Then
                            hdr.TRANG_THAI = "05"

                            If HaiQuanController.Insert_Chungtu2(hdr, dtl) Then
                                sRet = "Success;Hoàn thiện và gửi kiểm soát chứng từ thành công"
                            Else
                                sRet = String.Empty
                            End If
                        ElseIf sSaveType = "UPDATE_KS" Then
                            If hdr.TRANG_THAI = "05" Then

                                If HaiQuanController.Update_ChungTu22(hdr, dtl) Then
                                    sRet = "Success;Cập nhật và gửi kiểm soát thành công"
                                Else
                                    sRet = String.Empty
                                End If
                            ElseIf hdr.TRANG_THAI = "00" OrElse hdr.TRANG_THAI = "03" Then
                                hdr.TRANG_THAI = "05"
                                If HaiQuanController.Update_ChungTu22(hdr, dtl) Then
                                    sRet = "Success;Hoàn thiện và gửi kiểm soát chứng từ thành công"
                                Else
                                    sRet = String.Empty
                                End If
                            Else
                                sRet = ""
                                Throw New Exception("Trạng thái chứng từ không cho phép cập nhật")
                                Return sRet
                            End If
                        End If
                    Case "GHIDC"
                        hdr.TRANG_THAI = "19" 'Trạng thái 19 - điều chỉnh gửi kiểm soát
                        If sSaveType = "UPDATE_DC" Then
                            hdr.TRANG_THAI = "19"
                            If HaiQuanController.Update_ChungTu22(hdr, dtl) Then
                                sRet = "Success;Điều chỉnh và gửi kiểm soát thành công"
                            Else
                                sRet = String.Empty
                            End If

                        End If
                End Select

                'Nối thêm thông tin load lên form
                sRet = sRet & ";" & hdr.SHKB & "/" & hdr.SO_CT & "/" & hdr.SO_BT
            Catch ex As Exception
                'VBOracleLib.LogApp.WriteLog("GHI", ex, "Lỗi ghi chứng từ", sTenDN)
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi ghi chứng từ", sTenDN)
                If ex.Message.Contains("TCS_CTU_HDR_PK") Then
                    sRet = ""
                    If sSaveType.StartsWith("INSERT") Then
                        Throw New Exception("Không thể thêm mới! Đã tồn tại giấy nộp tiền này trong hệ thống")
                    ElseIf sSaveType.StartsWith("UPDATE") Then
                        Throw New Exception("Không thể cập nhật! Đã tồn tại giấy nộp tiền này trong hệ thống")
                    End If
                Else
                    sRet = ""
                    Throw ex
                End If
            End Try

            Return sRet
        End Function

        Private Shared Function Check_ListDTL2(ByVal dtl() As HaiQuanObj.Dtl, ByVal fblnBatBuocMLNS As Boolean, _
                                    ByVal fblnVND As Boolean, ByVal fblnBatBuocTLDT As Boolean, ByVal sTenDN As String) As Integer()
            Dim intReturn(1) As Integer
            Try
                Dim strMaQuy As String = ""
                Dim i, iCol As Integer
                For i = 0 To dtl.Count - 1
                    iCol = Valid_DTL2(dtl(i), fblnBatBuocMLNS, fblnVND, fblnBatBuocTLDT, sTenDN)
                    If (iCol <> -1) Then
                        intReturn(0) = i
                        intReturn(1) = iCol
                        Return intReturn
                    End If
                Next
                Return Nothing
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi valid dữ liệu dtl", sTenDN)
                Return Nothing
            End Try
        End Function

        Private Shared Function Valid_DTL2(ByVal dtl As HaiQuanObj.Dtl, ByVal fblnBatBuocMLNS As Boolean, _
                                            ByVal fblnVND As Boolean, ByVal fblnBatBuocTLDT As Boolean, ByVal sTenDN As String) As Integer
            Try
                If (fblnBatBuocMLNS = True) Then
                    If (Not HaiQuanController.Valid_TMuc(dtl.MA_TMUC, True)) Then
                        Return Common.mdlCommon.DTLColumn.MTieuMuc
                    End If
                End If

                If (dtl.SOTIEN = 0) Then
                    If (fblnVND) Then
                        Return Common.mdlCommon.DTLColumn.Tien
                    End If
                End If
                Return -1
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi valid chứng từ DTL", sTenDN)
                Return -1
            End Try
        End Function

        Public Shared Function Insert_Chungtu2(ByVal hdr As HaiQuanObj.HdrHq, ByVal dtl() As HaiQuanObj.Dtl) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then
                    Try
                        hdr.KYHIEU_CT = HaiQuanController.GetKyHieuChungTu(hdr.SHKB, hdr.MA_NV, "04")
                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh ký hiệu chứng từ")
                        Return bRet
                    End Try

                    Try
                        'nên chuyển vào trong câu query
                        hdr.NGAY_KB = HaiQuanController.GetNgayKhoBac(hdr.SHKB)
                        Dim sSo_BT As String = "0" 'String.Empty
                        Dim sNam_KB As String
                        Dim sThang_KB As String
                        sNam_KB = Mid(hdr.NGAY_KB, 3, 2)
                        sThang_KB = Mid(hdr.NGAY_KB, 5, 2)
                        Dim sSoCT = CTuCommon.Get_SoCT()
                        hdr.SO_CT = sNam_KB + sThang_KB + sSoCT

                        hdr.REF_NO = hdr.SO_CT
                        hdr.RM_REF_NO = hdr.SO_CT
                        hdr.KYHIEU_CT = hdr.KYHIEU_CT & sSoCT.PadLeft(7, "0")
                        sSo_BT = HaiQuanController.GetSoBT(hdr.NGAY_KB, hdr.MA_NV)
                        If Not String.IsNullOrEmpty(sSo_BT) Then
                            hdr.SO_BT = CInt(sSo_BT + 1)
                        Else
                            hdr.SO_BT = 1
                        End If




                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh số chứng từ")
                        Return bRet
                    End Try

                    conn = DataAccess.GetConnection() 'New OracleConnection(VBOracleLib.DataAccess.strConnection)
                    ' conn.Open()
                    tran = conn.BeginTransaction()

                    sSQL = "INSERT INTO TCS_CTU_HDR(SHKB,NGAY_KB,MA_TINH,MA_HUYEN,MA_XA,TK_CO,MA_NV,MA_CN,SO_BT,MA_DTHU, SO_BTHU, " & _
                                "KYHIEU_CT,SO_CT,MA_LTHUE,MA_NNTIEN,TEN_NNTIEN,DC_NNTIEN,MA_QUAN_HUYENNNTIEN,MA_TINH_TPNNTIEN,MA_NNTHUE,TEN_NNTHUE," & _
                                "DC_NNTHUE,MA_HUYEN_NNTHUE,MA_TINH_NNTHUE,NGAY_CT,NGAY_HT,MA_CQTHU,DS_TOKHAI,NGAY_TK,LH_XNK,PT_TT," & _
                                "MA_NT,TY_GIA,TTIEN,TTIEN_NT,TT_TTHU,TK_KH_NH,TEN_KH_NH,NGAY_KH_NH,MA_NH_A,MA_NH_B, " & _
                                "TEN_NH_B,MA_NH_TT,TEN_NH_TT,LAN_IN,TRANG_THAI,PT_TINHPHI,PHI_GD,PHI_VAT,PHI_GD2,PHI_VAT2, " & _
                                "TT_CTHUE,TKHT,LOAI_TT,MA_NTK,MA_HQ,TEN_HQ,MA_HQ_PH,TEN_HQ_PH,KENH_CT,MA_SANPHAM, " & _
                                "MA_CUC,TEN_CUC, " & _
                                "TT_CITAD, SO_CMND, SO_FONE, LOAI_CTU, REMARKS,dien_giai_hq,REF_NO,RM_REF_NO, TK_GL_NH,GHI_CHU)" & _
                            "VALUES(:SHKB, :NGAY_KB, :MA_TINH, :MA_HUYEN, :MA_XA, :TK_CO, :MA_NV, :MA_CN, :SO_BT, :MA_DTHU, 0," & _
                                ":KYHIEU_CT, :SO_CT, :MA_LTHUE, :MA_NNTIEN, :TEN_NNTIEN, :DC_NNTIEN, :MA_QUAN_HUYENNNTIEN, :MA_TINH_TPNNTIEN, :MA_NNTHUE, :TEN_NNTHUE, " & _
                                ":DC_NNTHUE, :MA_HUYEN_NNTHUE, :MA_TINH_NNTHUE, to_char(sysdate,'yyyyMMdd'), sysdate, :MA_CQTHU, :DS_TOKHAI, to_date(:NGAY_TK,'dd/MM/yyyy'), :LH_XNK, :PT_TT, " & _
                                ":MA_NT, :TY_GIA, :TTIEN, :TTIEN_NT, :TT_TTHU, :TK_KH_NH, :TEN_KH_NH, sysdate, :MA_NH_A, :MA_NH_B, " & _
                                ":TEN_NH_B, :MA_NH_TT, :TEN_NH_TT, 0, :TRANG_THAI, :PT_TINHPHI, :PHI_GD, :PHI_VAT, :PHI_GD2, :PHI_VAT2, " & _
                                ":TT_CTHUE, :TKHT, :LOAI_TT, :MA_NTK, :MA_HQ, :TEN_HQ, :MA_HQ_PH, :TEN_HQ_PH, :KENH_CT, :MA_SANPHAM," & _
                                ":MA_CUC,:TEN_CUC, " & _
                                ":TT_CITAD, :SO_CMND, :SO_FONE, :LOAI_CTU, :REMARKS,:dien_giai_hq, :REF_NO, :RM_REF_NO, :TK_GL_NH, :GHI_CHU)"

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(hdr.NGAY_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH", ParameterDirection.Input, CStr(hdr.MA_TINH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HUYEN", ParameterDirection.Input, CStr(hdr.MA_HUYEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_XA", ParameterDirection.Input, CStr(hdr.MA_XA), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_CO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(hdr.MA_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(hdr.MA_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(hdr.SO_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(hdr.MA_DTHU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":KYHIEU_CT", ParameterDirection.Input, CStr(hdr.KYHIEU_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, CStr(hdr.MA_LTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.MA_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(hdr.MA_QUAN_HUYENNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_TPNNTIEN", ParameterDirection.Input, CStr(hdr.MA_TINH_TPNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, CStr(hdr.DC_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HUYEN_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_HUYEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_TINH_NNTHUE), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_CT", ParameterDirection.Input, CStr(hdr.NGAY_CT), DbType.String))       'Fix to_char(sysdate,'yyyyMMdd')
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_HT", ParameterDirection.Input, CStr(hdr.NGAY_HT), DbType.String))       'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DS_TOKHAI", ParameterDirection.Input, CStr(hdr.SO_TK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, CStr(hdr.NGAY_TK), DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":LH_XNK", ParameterDirection.Input, CStr(hdr.LH_XNK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(hdr.PT_TT), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TY_GIA", ParameterDirection.Input, CStr(hdr.TY_GIA), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NT", ParameterDirection.Input, CStr(hdr.TTIEN_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_TTHU", ParameterDirection.Input, CStr(hdr.TT_TTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(hdr.TEN_KH_NH), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, CStr(hdr.NGAY_KH_NH), DbType.String)) 'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, CStr(hdr.MA_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_B", ParameterDirection.Input, CStr(hdr.MA_NH_B), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_B", ParameterDirection.Input, CStr(hdr.TEN_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(hdr.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, CStr(hdr.TEN_NH_TT), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":LAN_IN", ParameterDirection.Input, CStr(hdr.LAN_IN), DbType.String))         'Fix 0
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TINHPHI", ParameterDirection.Input, CStr(hdr.PT_TINHPHI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD", ParameterDirection.Input, CStr(hdr.PHI_GD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT", ParameterDirection.Input, CStr(hdr.PHI_VAT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD2", ParameterDirection.Input, CStr(hdr.PHI_GD2), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT2", ParameterDirection.Input, CStr(hdr.PHI_VAT2), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TT_CTHUE", ParameterDirection.Input, CStr(hdr.TT_CTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TKHT", ParameterDirection.Input, CStr(hdr.TKHT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_TT", ParameterDirection.Input, CStr(hdr.LOAI_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(hdr.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(hdr.MA_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, CStr(hdr.TEN_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, CStr(hdr.MA_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, CStr(hdr.TEN_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KENH_CT", ParameterDirection.Input, CStr(hdr.KENH_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_SANPHAM", ParameterDirection.Input, CStr(hdr.MA_SANPHAM), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TT_CITAD", ParameterDirection.Input, "1", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CMND", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_FONE", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_CTU", ParameterDirection.Input, CStr(hdr.LOAI_CTU), DbType.String))

                    hdr.REMARKS = Globals.RemoveSign4VietnameseString(hdr.REMARKS)
                    Dim strRemarks As String = hdr.REMARKS
                    If hdr.REMARKS.Length > 210 Then
                        strRemarks = hdr.REMARKS.Substring(0, 210)
                    End If

                    listParam.Add(DataAccess.NewDBParameter(":REMARKS", ParameterDirection.Input, CStr(strRemarks), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":dien_giai_hq", ParameterDirection.Input, CStr(hdr.DIENGIAI_HQ), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":REF_NO", ParameterDirection.Input, CStr(hdr.REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":RM_REF_NO", ParameterDirection.Input, CStr(hdr.RM_REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_GL_NH", ParameterDirection.Input, CStr(hdr.TK_GL_NH), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_CUC", ParameterDirection.Input, CStr(hdr.MA_CUC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CUC", ParameterDirection.Input, CStr(hdr.TEN_CUC), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":GHI_CHU", ParameterDirection.Input, CStr(hdr.GHI_CHU), DbType.String))

                    Dim v_sqlT As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....

                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If dtl IsNot Nothing AndAlso dtl.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each objDtl In dtl
                                objDtl.SO_CT = hdr.SO_CT
                                objDtl.SHKB = hdr.SHKB
                                objDtl.NGAY_KB = hdr.NGAY_KB
                                objDtl.MA_NV = hdr.MA_NV
                                objDtl.SO_BT = hdr.SO_BT
                                objDtl.MA_DTHU = hdr.MA_DTHU

                                If objDtl.SAC_THUE.Length > 2 Then
                                    bFlag = False
                                    Exit For
                                End If

                                iRet = 0
                                sSQL = "INSERT INTO TCS_CTU_DTL(" & _
                                            "ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MTM_ID,MA_MUC,MA_TMUC," & _
                                            "NOI_DUNG,KY_THUE,SOTIEN,SOTIEN_NT,SO_CT, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ, " & _
                                            "SAC_THUE,TT_BTOAN) " & _
                                        "VALUES(" & _
                                            "TCS_CTU_DTL_SEQ.NEXTVAL, :SHKB, :NGAY_KB, :MA_NV, :SO_BT, :MA_DTHU, :MA_CHUONG, '', '', :MA_TMUC," & _
                                            ":NOI_DUNG, '', :SOTIEN, :SOTIEN_NT, :SO_CT, :SO_TK, TO_DATE(:NGAY_TK,'dd/MM/yyyy'), :MA_LHXNK, :MA_LT, :MA_HQ, " & _
                                            ":SAC_THUE, :TT_BTOAN)"

                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(objDtl.SHKB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(objDtl.NGAY_KB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(objDtl.MA_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(objDtl.SO_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(objDtl.MA_DTHU), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(objDtl.MA_CHUONG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_TMUC", ParameterDirection.Input, CStr(objDtl.MA_TMUC), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(objDtl.NOI_DUNG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN", ParameterDirection.Input, CStr(objDtl.SOTIEN), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN_NT", ParameterDirection.Input, CStr(objDtl.SOTIEN_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(objDtl.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(objDtl.SO_TK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, objDtl.NGAY_TK, DbType.Date))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LHXNK", ParameterDirection.Input, CStr(objDtl.MA_LHXNK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LT", ParameterDirection.Input, CStr(objDtl.MA_LT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(objDtl.MA_HQ), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(objDtl.SAC_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":TT_BTOAN", ParameterDirection.Input, CStr(objDtl.TT_BTOAN), DbType.String))

                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function
        Public Shared Function DieuChinh_Chungtu2(ByVal hdr As HaiQuanObj.HdrHq, ByVal dtl() As HaiQuanObj.Dtl) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim so_CT_DC As String = ""
            Try
                'Dieu chinh là update ban ghi cu thanh trạng thai 99, và thêm 1 bản ghi mới
                If hdr IsNot Nothing And hdr.SO_CT.Length > 0 Then
                    Try
                        hdr.KYHIEU_CT = HaiQuanController.GetKyHieuChungTu(hdr.SHKB, hdr.MA_NV, "01")
                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh ký hiệu chứng từ")
                        Return bRet
                    End Try
                    so_CT_DC = hdr.SO_CT
                    Try
                        'nên chuyển vào trong câu query
                        hdr.NGAY_KB = HaiQuanController.GetNgayKhoBac(hdr.SHKB)
                        Dim sSo_BT As String = "0" 'String.Empty
                        Dim sNam_KB As String
                        Dim sThang_KB As String
                        sNam_KB = Mid(hdr.NGAY_KB, 3, 2)
                        sThang_KB = Mid(hdr.NGAY_KB, 5, 2)
                        hdr.SO_CT = sNam_KB + sThang_KB + CTuCommon.Get_SoCT()
                        sSo_BT = HaiQuanController.GetSoBT(hdr.NGAY_KB, hdr.MA_NV)
                        If Not String.IsNullOrEmpty(sSo_BT) Then
                            hdr.SO_BT = CInt(sSo_BT + 1)
                        Else
                            hdr.SO_BT = 1
                        End If




                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh số chứng từ")
                        Return bRet
                    End Try

                    conn = DataAccess.GetConnection() 'New OracleConnection(VBOracleLib.DataAccess.strConnection)
                    '  conn.Open()
                    tran = conn.BeginTransaction()
                    'update HDR
                    sSQL = "UPDATE   TCS_CTU_HDR set TRANG_THAI='99' where SO_CT= :SO_CT "
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(so_CT_DC), DbType.String))
                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Insert 1 bản ghi mới
                    sSQL = "INSERT INTO TCS_CTU_HDR(SHKB,NGAY_KB,MA_TINH,MA_HUYEN,MA_XA,TK_CO,MA_NV,MA_CN,SO_BT,MA_DTHU, SO_BTHU, " & _
                                "KYHIEU_CT,SO_CT,MA_LTHUE,MA_NNTIEN,TEN_NNTIEN,DC_NNTIEN,MA_QUAN_HUYENNNTIEN,MA_TINH_TPNNTIEN,MA_NNTHUE,TEN_NNTHUE," & _
                                "DC_NNTHUE,MA_HUYEN_NNTHUE,MA_TINH_NNTHUE,NGAY_CT,NGAY_HT,MA_CQTHU,SO_TK,NGAY_TK,LH_XNK,PT_TT," & _
                                "MA_NT,TY_GIA,TTIEN,TTIEN_NT,TT_TTHU,TK_KH_NH,TEN_KH_NH,NGAY_KH_NH,MA_NH_A,MA_NH_B, " & _
                                "TEN_NH_B,MA_NH_TT,TEN_NH_TT,LAN_IN,TRANG_THAI,PT_TINHPHI,PHI_GD,PHI_VAT,PHI_GD2,PHI_VAT2, " & _
                                "TT_CTHUE,TKHT,LOAI_TT,MA_NTK,MA_HQ,TEN_HQ,MA_HQ_PH,TEN_HQ_PH,KENH_CT,MA_SANPHAM, " & _
                                "MA_CUC,TEN_CUC, " & _
                                "TT_CITAD, SO_CMND, SO_FONE, LOAI_CTU, REMARKS,REF_NO,RM_REF_NO, TK_GL_NH,SO_CT_DC)" & _
                            "VALUES(:SHKB, :NGAY_KB, :MA_TINH, :MA_HUYEN, :MA_XA, :TK_CO, :MA_NV, :MA_CN, :SO_BT, :MA_DTHU, 0," & _
                                ":KYHIEU_CT, :SO_CT, :MA_LTHUE, :MA_NNTIEN, :TEN_NNTIEN, :DC_NNTIEN, :MA_QUAN_HUYENNNTIEN, :MA_TINH_TPNNTIEN, :MA_NNTHUE, :TEN_NNTHUE, " & _
                                ":DC_NNTHUE, :MA_HUYEN_NNTHUE, :MA_TINH_NNTHUE, to_char(sysdate,'yyyyMMdd'), sysdate, :MA_CQTHU, :SO_TK, to_date(:NGAY_TK,'dd/MM/yyyy'), :LH_XNK, :PT_TT, " & _
                                ":MA_NT, :TY_GIA, :TTIEN, :TTIEN_NT, :TT_TTHU, :TK_KH_NH, :TEN_KH_NH, sysdate, :MA_NH_A, :MA_NH_B, " & _
                                ":TEN_NH_B, :MA_NH_TT, :TEN_NH_TT, 0, :TRANG_THAI, :PT_TINHPHI, :PHI_GD, :PHI_VAT, :PHI_GD2, :PHI_VAT2, " & _
                                ":TT_CTHUE, :TKHT, :LOAI_TT, :MA_NTK, :MA_HQ, :TEN_HQ, :MA_HQ_PH, :TEN_HQ_PH, :KENH_CT, :MA_SANPHAM," & _
                                ":MA_CUC,:TEN_CUC, " & _
                                ":TT_CITAD, :SO_CMND, :SO_FONE, :LOAI_CTU, :REMARKS, :REF_NO, :RM_REF_NO, :TK_GL_NH, :SO_CT_DC)"

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(hdr.NGAY_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH", ParameterDirection.Input, CStr(hdr.MA_TINH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HUYEN", ParameterDirection.Input, CStr(hdr.MA_HUYEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_XA", ParameterDirection.Input, CStr(hdr.MA_XA), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_CO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(hdr.MA_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(hdr.MA_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(hdr.SO_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(hdr.MA_DTHU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":KYHIEU_CT", ParameterDirection.Input, CStr(hdr.KYHIEU_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, CStr(hdr.MA_LTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.MA_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(hdr.MA_QUAN_HUYENNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_TPNNTIEN", ParameterDirection.Input, CStr(hdr.MA_TINH_TPNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, CStr(hdr.DC_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HUYEN_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_HUYEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_TINH_NNTHUE), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_CT", ParameterDirection.Input, CStr(hdr.NGAY_CT), DbType.String))       'Fix to_char(sysdate,'yyyyMMdd')
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_HT", ParameterDirection.Input, CStr(hdr.NGAY_HT), DbType.String))       'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(hdr.SO_TK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, CStr(hdr.NGAY_TK), DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":LH_XNK", ParameterDirection.Input, CStr(hdr.LH_XNK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(hdr.PT_TT), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TY_GIA", ParameterDirection.Input, CStr(hdr.TY_GIA), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NT", ParameterDirection.Input, CStr(hdr.TTIEN_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_TTHU", ParameterDirection.Input, CStr(hdr.TT_TTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(hdr.TEN_KH_NH), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, CStr(hdr.NGAY_KH_NH), DbType.String)) 'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, CStr(hdr.MA_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_B", ParameterDirection.Input, CStr(hdr.MA_NH_B), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_B", ParameterDirection.Input, CStr(hdr.TEN_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(hdr.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, CStr(hdr.TEN_NH_TT), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":LAN_IN", ParameterDirection.Input, CStr(hdr.LAN_IN), DbType.String))         'Fix 0
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TINHPHI", ParameterDirection.Input, CStr(hdr.PT_TINHPHI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD", ParameterDirection.Input, CStr(hdr.PHI_GD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT", ParameterDirection.Input, CStr(hdr.PHI_VAT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD2", ParameterDirection.Input, CStr(hdr.PHI_GD2), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT2", ParameterDirection.Input, CStr(hdr.PHI_VAT2), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TT_CTHUE", ParameterDirection.Input, CStr(hdr.TT_CTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TKHT", ParameterDirection.Input, CStr(hdr.TKHT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_TT", ParameterDirection.Input, CStr(hdr.LOAI_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(hdr.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(hdr.MA_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, CStr(hdr.TEN_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, CStr(hdr.MA_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, CStr(hdr.TEN_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KENH_CT", ParameterDirection.Input, CStr(hdr.KENH_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_SANPHAM", ParameterDirection.Input, CStr(hdr.MA_SANPHAM), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TT_CITAD", ParameterDirection.Input, "1", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CMND", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_FONE", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_CTU", ParameterDirection.Input, CStr(hdr.LOAI_CTU), DbType.String))

                    hdr.REMARKS = Globals.RemoveSign4VietnameseString(hdr.REMARKS)
                    Dim strRemarks As String = hdr.REMARKS
                    If hdr.REMARKS.Length > 210 Then
                        strRemarks = hdr.REMARKS.Substring(0, 210)
                    End If

                    listParam.Add(DataAccess.NewDBParameter(":REMARKS", ParameterDirection.Input, CStr(strRemarks), DbType.String))


                    listParam.Add(DataAccess.NewDBParameter(":REF_NO", ParameterDirection.Input, CStr(hdr.REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":RM_REF_NO", ParameterDirection.Input, CStr(hdr.RM_REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_GL_NH", ParameterDirection.Input, CStr(hdr.TK_GL_NH), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_CUC", ParameterDirection.Input, CStr(hdr.MA_CUC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CUC", ParameterDirection.Input, CStr(hdr.TEN_CUC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT_DC", ParameterDirection.Input, CStr(so_CT_DC), DbType.String))
                    'Dim v_sqlT As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....

                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If dtl IsNot Nothing AndAlso dtl.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each objDtl In dtl
                                objDtl.SO_CT = hdr.SO_CT
                                objDtl.SHKB = hdr.SHKB
                                objDtl.NGAY_KB = hdr.NGAY_KB
                                objDtl.MA_NV = hdr.MA_NV
                                objDtl.SO_BT = hdr.SO_BT
                                objDtl.MA_DTHU = hdr.MA_DTHU

                                iRet = 0
                                sSQL = "INSERT INTO TCS_CTU_DTL(" & _
                                            "ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MTM_ID,MA_MUC,MA_TMUC," & _
                                            "NOI_DUNG,KY_THUE,SOTIEN,SOTIEN_NT,SO_CT, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ, " & _
                                            "SAC_THUE,TT_BTOAN) " & _
                                        "VALUES(" & _
                                            "TCS_CTU_DTL_SEQ.NEXTVAL, :SHKB, :NGAY_KB, :MA_NV, :SO_BT, :MA_DTHU, :MA_CHUONG, '', '', :MA_TMUC," & _
                                            ":NOI_DUNG, '', :SOTIEN, :SOTIEN_NT, :SO_CT, :SO_TK, TO_DATE(:NGAY_TK,'dd/MM/yyyy'), :MA_LHXNK, :MA_LT, :MA_HQ, " & _
                                            ":SAC_THUE, :TT_BTOAN)"

                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(objDtl.SHKB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(objDtl.NGAY_KB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(objDtl.MA_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(objDtl.SO_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(objDtl.MA_DTHU), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(objDtl.MA_CHUONG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_TMUC", ParameterDirection.Input, CStr(objDtl.MA_TMUC), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(objDtl.NOI_DUNG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN", ParameterDirection.Input, CStr(objDtl.SOTIEN), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN_NT", ParameterDirection.Input, CStr(objDtl.SOTIEN_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(objDtl.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(objDtl.SO_TK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, objDtl.NGAY_TK, DbType.Date))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LHXNK", ParameterDirection.Input, CStr(objDtl.MA_LHXNK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LT", ParameterDirection.Input, CStr(objDtl.MA_LT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(objDtl.MA_HQ), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(objDtl.SAC_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":TT_BTOAN", ParameterDirection.Input, CStr(objDtl.TT_BTOAN), DbType.String))

                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function
        Public Shared Function Update_ChungTu22(ByVal hdr As HaiQuanObj.HdrHq, ByVal dtl() As HaiQuanObj.Dtl) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then
                    conn = VBOracleLib.DataAccess.GetConnection()
                    '  conn.Open()
                    tran = conn.BeginTransaction()
                    'delete DTL
                    sSQL = "DELETE FROM TCS_CTU_DTL WHERE SO_CT = :SO_CT"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)

                    'update HDR
                    sSQL = "UPDATE   TCS_CTU_HDR " & _
                           "SET   MA_TINH = :MA_TINH, MA_HUYEN = :MA_HUYEN, MA_XA = :MA_XA, " & _
                                 "TK_CO = :TK_CO, MA_CN = :MA_CN, MA_LTHUE = :MA_LTHUE, " & _
                                 "MA_NNTIEN = :MA_NNTIEN,TEN_NNTIEN = :TEN_NNTIEN,DC_NNTIEN = :DC_NNTIEN, " & _
                                 "MA_QUAN_HUYENNNTIEN = :MA_QUAN_HUYENNNTIEN,MA_TINH_TPNNTIEN = :MA_TINH_TPNNTIEN,MA_NNTHUE = :MA_NNTHUE, " & _
                                 "TEN_NNTHUE = :TEN_NNTHUE,DC_NNTHUE = :DC_NNTHUE,MA_HUYEN_NNTHUE = :MA_HUYEN_NNTHUE, " & _
                                 "MA_TINH_NNTHUE = :MA_TINH_NNTHUE,NGAY_HT = SYSDATE,MA_CQTHU = :MA_CQTHU, " & _
                                 "DS_TOKHAI = :SO_TK,NGAY_TK = TO_DATE(:NGAY_TK,'dd/MM/yyyy'),LH_XNK = :LH_XNK, " & _
                                 "PT_TT = :PT_TT,MA_NT = :MA_NT,TY_GIA = :TY_GIA, " & _
                                 "TTIEN = :TTIEN,TTIEN_NT = :TTIEN_NT,TT_TTHU = :TT_TTHU, " & _
                                 "TK_KH_NH = :TK_KH_NH,TEN_KH_NH = :TEN_KH_NH,MA_NH_A = :MA_NH_A, " & _
                                 "MA_NH_B = :MA_NH_B,TEN_NH_B = :TEN_NH_B,MA_NH_TT = :MA_NH_TT, " & _
                                 "TEN_NH_TT = :TEN_NH_TT,TRANG_THAI = :TRANG_THAI,PT_TINHPHI = :PT_TINHPHI, " & _
                                 "PHI_GD = :PHI_GD,PHI_VAT = :PHI_VAT,PHI_GD2 = :PHI_GD2, " & _
                                 "PHI_VAT2 = :PHI_VAT2,TKHT = :TKHT,LOAI_TT = :LOAI_TT, " & _
                                 "MA_NTK = :MA_NTK,MA_HQ = :MA_HQ,TEN_HQ = :TEN_HQ, " & _
                                 "MA_HQ_PH = :MA_HQ_PH,TEN_HQ_PH = :TEN_HQ_PH,MA_SANPHAM = :MA_SANPHAM, " & _
                                 "TT_CITAD = :TT_CITAD,SO_CMND = :SO_CMND,SO_FONE = :SO_FONE, " & _
                                 "REMARKS = :REMARKS ,dien_giai_hq = :dien_giai_hq , TK_GL_NH =: TK_GL_NH, " & _
                                 "MA_CUC = :MA_CUC , TEN_CUC =: TEN_CUC, GHI_CHU=:GHI_CHU " & _
                         "WHERE   SO_CT = :SO_CT "

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH", ParameterDirection.Input, CStr(hdr.MA_TINH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HUYEN", ParameterDirection.Input, CStr(hdr.MA_HUYEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_XA", ParameterDirection.Input, CStr(hdr.MA_XA), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_CO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(hdr.MA_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, CStr(hdr.MA_LTHUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.MA_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(hdr.MA_QUAN_HUYENNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_TPNNTIEN", ParameterDirection.Input, CStr(hdr.MA_TINH_TPNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, CStr(hdr.DC_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HUYEN_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_HUYEN_NNTHUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_TINH_NNTHUE), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_HT", ParameterDirection.Input, CStr(hdr.NGAY_HT), DbType.String))       'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(hdr.SO_TK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, CStr(hdr.NGAY_TK), DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":LH_XNK", ParameterDirection.Input, CStr(hdr.LH_XNK), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(hdr.PT_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TY_GIA", ParameterDirection.Input, CStr(hdr.TY_GIA), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NT", ParameterDirection.Input, CStr(hdr.TTIEN_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_TTHU", ParameterDirection.Input, CStr(hdr.TT_TTHU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(hdr.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, CStr(hdr.MA_NH_A), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_B", ParameterDirection.Input, CStr(hdr.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_B", ParameterDirection.Input, CStr(hdr.TEN_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(hdr.MA_NH_TT), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, CStr(hdr.TEN_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TINHPHI", ParameterDirection.Input, CStr(hdr.PT_TINHPHI), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD", ParameterDirection.Input, CStr(hdr.PHI_GD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT", ParameterDirection.Input, CStr(hdr.PHI_VAT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD2", ParameterDirection.Input, CStr(hdr.PHI_GD2), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT2", ParameterDirection.Input, CStr(hdr.PHI_VAT2), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TKHT", ParameterDirection.Input, CStr(hdr.TKHT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_TT", ParameterDirection.Input, CStr(hdr.LOAI_TT), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(hdr.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(hdr.MA_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, CStr(hdr.TEN_HQ), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, CStr(hdr.MA_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, CStr(hdr.TEN_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_SANPHAM", ParameterDirection.Input, CStr(hdr.MA_SANPHAM), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TT_CITAD", ParameterDirection.Input, CStr(hdr.TT_CITAD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CMND", ParameterDirection.Input, CStr(hdr.SO_CMND), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_FONE", ParameterDirection.Input, CStr(hdr.SO_FONE), DbType.String))

                    hdr.REMARKS = Globals.RemoveSign4VietnameseString(hdr.REMARKS)
                    Dim strRemarks As String = hdr.REMARKS
                    If hdr.REMARKS.Length > 210 Then
                        strRemarks = hdr.REMARKS.Substring(0, 210)
                    End If

                    listParam.Add(DataAccess.NewDBParameter(":REMARKS", ParameterDirection.Input, CStr(strRemarks), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TK_GL_NH", ParameterDirection.Input, CStr(hdr.TK_GL_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_CUC", ParameterDirection.Input, CStr(hdr.MA_CUC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CUC", ParameterDirection.Input, CStr(hdr.TEN_CUC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":dien_giai_hq", ParameterDirection.Input, CStr(hdr.DIENGIAI_HQ), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":GHI_CHU", ParameterDirection.Input, CStr(hdr.GHI_CHU), DbType.String))


                    Dim vSql As String = DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)

                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If dtl IsNot Nothing AndAlso dtl.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each objDtl In dtl
                                objDtl.SO_CT = hdr.SO_CT
                                objDtl.SHKB = hdr.SHKB
                                objDtl.NGAY_KB = hdr.NGAY_KB
                                objDtl.MA_NV = hdr.MA_NV
                                objDtl.SO_BT = hdr.SO_BT
                                objDtl.MA_DTHU = hdr.MA_DTHU

                                If objDtl.SAC_THUE.Length > 2 Then
                                    bFlag = False
                                    Exit For
                                End If

                                iRet = 0
                                sSQL = "INSERT INTO TCS_CTU_DTL(" & _
                                            "ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MTM_ID,MA_MUC,MA_TMUC," & _
                                            "NOI_DUNG,KY_THUE,SOTIEN,SOTIEN_NT,SO_CT, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ, " & _
                                            "SAC_THUE,TT_BTOAN) " & _
                                        "VALUES(" & _
                                            "TCS_CTU_DTL_SEQ.NEXTVAL, :SHKB, :NGAY_KB, :MA_NV, :SO_BT, :MA_DTHU, :MA_CHUONG, '', '', :MA_TMUC," & _
                                            ":NOI_DUNG, '', :SOTIEN, :SOTIEN_NT, :SO_CT, :SO_TK, TO_DATE(:NGAY_TK,'dd/MM/yyyy'), :MA_LHXNK, :MA_LT, :MA_HQ, " & _
                                            ":SAC_THUE, :TT_BTOAN)"

                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(objDtl.SHKB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(objDtl.NGAY_KB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(objDtl.MA_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(objDtl.SO_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(objDtl.MA_DTHU), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(objDtl.MA_CHUONG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_TMUC", ParameterDirection.Input, CStr(objDtl.MA_TMUC), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(objDtl.NOI_DUNG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN", ParameterDirection.Input, CStr(objDtl.SOTIEN), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN_NT", ParameterDirection.Input, CStr(objDtl.SOTIEN_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(objDtl.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(objDtl.SO_TK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, objDtl.NGAY_TK, DbType.Date))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LHXNK", ParameterDirection.Input, CStr(objDtl.MA_LHXNK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LT", ParameterDirection.Input, CStr(objDtl.MA_LT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(objDtl.MA_HQ), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(objDtl.SAC_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":TT_BTOAN", ParameterDirection.Input, CStr(objDtl.TT_BTOAN), DbType.String))

                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()

                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function

        Function getCTu_Goc(ByVal so_Ctu_DChinh As String) As String
            Try


                Dim v_sql As String = ""
                Dim dt As DataTable
                v_sql = "SELECT SO_CT_DC WHERE A.SO_CT = '" & so_Ctu_DChinh & "'"

                dt = DataAccess.ExecuteToTable(v_sql)
                If dt IsNot Nothing And dt.Rows.Count > 0 Then
                    Return dt.Rows(0)("SO_CT_DC")
                Else
                    Return ""
                End If
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Function getBCTHopCTTThueXNKhau(ByVal type1 As String, ByVal type2 As String, _
                                          ByVal strFromDate As String, ByVal strToDate As String, ByVal strMaCN As String, ByVal ma_nt As String, Optional ByVal pStrKENH_GD As String = "") As DataSet
            Dim strSQL As String
            Dim strQueryHQ As String
            Dim strQueryNH As String
            Dim strQueryHeader As String
            Dim strQueryFooter As String
            Dim strDateWhere As String
            Dim strDateWhere2 As String
            Dim strWhereMaCN As String
            Dim strWhereMaCN2 As String
            Dim strWhereMaNT As String
            Dim strCheckHS As String
            Dim result As New DataSet
            Dim dsNganHang As DataSet
            Dim strTableCtu As String
            Dim strTTCthue As String
            Dim strMaKS As String
            Dim strGIO_DC As String = DataAccess.ExecuteReturnDataSet("SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='GIO_DC_CT'", CommandType.Text).Tables(0).Rows(0)(0).ToString()
            If type1 = "M47" Then
                strDateWhere = ""
            ElseIf type1 = "M91" Or type1 = "M51" Then
                '   strDateWhere = " and b.trang_thai='04' "
                ' strMaKS = " and b.ma_ks <> 0 "
            End If
            If type1 = "M81" Then
                strDateWhere = ""
            End If
            'If type1 = "M41" Then
            '    strTTCthue = " and tt_cthue = '1' "
            'ElseIf type1 = "M51" Then
            '    strTTCthue = " and tt_cthue = '0' "
            'End If


            'strWhereMaNT = "AND a.Ma_NT = '" & ma_nt & "'"
            If strFromDate.Trim <> "" Then
                strDateWhere = strDateWhere & " AND to_char(to_timestamp(a.ngay_tn_ct,'YYYY-MM-DD""T""HH24:MI:SS""Z""'),'yyyy-MM-dd HH24:MI:SS') >= " & "'" & strFromDate & " " & strGIO_DC & "'"
                strDateWhere2 = " AND to_char(to_timestamp(a.ngay_tn_ct,'YYYY-MM-DD""T""HH24:MI:SS""Z""'),'yyyy-MM-dd HH24:MI:SS') <= " & "'" & strToDate & " " & strGIO_DC & "'"
            End If

            Try
                Dim strKenhGD As String = ""
                If pStrKENH_GD.Length > 0 Then
                    strKenhGD = " AND a.kenhgd ='" & pStrKENH_GD & "'"
                End If
                strSQL = "SELECT ROWNUM stt, MCQThu,SMonTruyen,STienTruyen,SMonNhan,STienNhan, nvl(stientruyen,0)- nvl(stiennhan,0) CLech" & _
                         " FROM ( " & _
                         " select ma_cqthu MCQThu, sum(smontruyen) smontruyen, sum(stientruyen) stientruyen,sum(smonnhan) smonnhan, sum(stiennhan) stiennhan " & _
                         " from ( " & _
                         " SELECT a.ma_cqthu, COUNT(*) smontruyen, sum(a.ttien) stientruyen, 0 smonnhan,0 stiennhan  FROM tcs_dchieu_nhhq_hdr a " & _
                          " where  a.accept_yn='Y'   " & strDateWhere & strDateWhere2 & strTTCthue & strMaKS & "  And a.transaction_type='" & type1 & "'" & _
                            strWhereMaCN & strWhereMaNT & strKenhGD & _
                         " GROUP by a.ma_cqthu " & _
                         " UNION ALL" & _
                         " SELECT a.ma_cqthu,0 smontruyen,0 stientruyen, COUNT(*) smonnhan, sum(a.ttien) stiennhan  FROM tcs_dchieu_hqnh_hdr a " & _
                          " where 1=1  " & strDateWhere & strDateWhere2 & _
                          " and a.transaction_type='" & type2 & "'" & _
                            strWhereMaCN2 & strWhereMaNT & strKenhGD & _
                         " group by a.ma_cqthu) " & _
                         " group by ma_cqthu) "

                dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not dsNganHang Is Nothing Then
                    result = dsNganHang
                End If
            Catch ex As Exception
            Finally

            End Try

            Return result
        End Function
        Public Function getBCCTietTThueXNKhau(ByVal type As String, ByVal strfromDate As String, ByVal strtoDate As String, ByVal strMaCN As String, Optional ByVal pStrKENH_GD As String = "") As DataSet
            Dim strSQL As String
            Dim strWhereMaCN As String = ""
            Dim dsNganHang As DataSet
            Dim result As New DataSet
            Dim strCheckHS As String
            Dim strDate As String
            Dim strGIO_DC As String = DataAccess.ExecuteReturnDataSet("SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='GIO_DC_CT'", CommandType.Text).Tables(0).Rows(0)(0).ToString()
            If strfromDate.Trim <> "" Then
                strDate += " AND to_char(to_timestamp(a.ngay_tn_ct,'YYYY-MM-DD""T""HH24:MI:SS""Z""'),'yyyy-MM-dd HH24:MI:SS') >= " & "'" & strfromDate & " " & strGIO_DC & "'"
                strDate += " AND to_char(to_timestamp(a.ngay_tn_ct,'YYYY-MM-DD""T""HH24:MI:SS""Z""'),'yyyy-MM-dd HH24:MI:SS') <= " & "'" & strtoDate & " " & strGIO_DC & "'"
            End If

            Try

                Dim strKenhGD As String = ""
                If pStrKENH_GD.Length > 0 Then
                    strKenhGD = " AND a.kenhgd ='" & pStrKENH_GD & "'"
                End If
                strSQL = " SELECT ROWNUM STT, (A.KYHIEU_CT || '-' || A.SO_CT) KHSCTU, A.TEN_NNTHUE TDVNTHUE, A.MA_NNTHUE MSTHUE, A.MA_HQ_PH MHQUANPH,B.SO_TK STKHAI "
                strSQL &= " ,B.NAM_DK NDKTKHAI,B.MA_LH LHINH,A.SHKB MKBAC,A.TK_NS TKTHU,A.TK_NS TKTNS,A.MA_NTK,"
                strSQL &= " (CASE  WHEN  A.KQ_DC IS NULL THEN '00' WHEN  A.KQ_DC IS NULL THEN '22'  ELSE A.KQ_DC END) TTRANG, A.MA_CQTHU MCQT,B.NDKT  MA_TMUC,  B.SOTIEN_NT TTIEN"
                strSQL &= " FROM TCS_DCHIEU_HQNH_HDR A, TCS_DCHIEU_HQNH_DTL B  WHERE A.ID=B.ID_HDR  " & strDate & strKenhGD & " order by a.ma_ntk,a.so_ct"

                'strSQL = strSQL & " and ma_ks<>0  " & strDate & strWhereMaCN & " order by c.ma_ntk,c.so_ct"
                dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not dsNganHang Is Nothing Then
                    result = dsNganHang
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally

            End Try

            Return result
        End Function

        Public Shared Function GetKyThue(ByVal strNgayLV As String) As String
            Dim sRet As String = ""
            Try
                If Not String.IsNullOrEmpty(strNgayLV) AndAlso strNgayLV.Length = 8 Then
                    sRet = strNgayLV.Substring(4, 2) & "/" & strNgayLV.Substring(0, 4)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function GetDataByCQT_new(ByVal sMaCQT As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try

                sSQL = "SELECT (MA_HQ || '|' || SHKB || '|' || TEN || '|' || MA_CQTHU) FROM TCS_DM_CQTHU WHERE  trang_thai='1' and MA_CQTHU = :MA_CQTHU "
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_CQTHU", ParameterDirection.Input, sMaCQT, DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                    log.Info("GetDataByCQT_new: " & sRet)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

    End Class

    
End Namespace


