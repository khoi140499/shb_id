﻿Imports System.Xml.Serialization

Public Class VMReportPhi
    Public Sub New()
    End Sub

    <XmlElement(Order:=1)> _
    Public Property SO_HS() As String
        Get
            Return m_SO_HS
        End Get
        Set(ByVal value As String)
            m_SO_HS = value
        End Set
    End Property
    Private m_SO_HS As String

    <XmlElement(Order:=2)> _
    Public Property NNT_MST() As String
        Get
            Return m_NNT_MST
        End Get
        Set(ByVal value As String)
            m_NNT_MST = value
        End Set
    End Property
    Private m_NNT_MST As String

    <XmlElement(Order:=3)> _
    Public Property SO_CT() As String
        Get
            Return m_SO_CT
        End Get
        Set(ByVal value As String)
            m_SO_CT = value
        End Set
    End Property
    Private m_SO_CT As String

    <XmlElement(Order:=4)> _
    Public Property NAM_CT_PT() As String
        Get
            Return m_NAM_CT_PT
        End Get
        Set(ByVal value As String)
            m_NAM_CT_PT = value
        End Set
    End Property
    Private m_NAM_CT_PT As String

    <XmlElement(Order:=5)> _
    Public Property MA_DVQL() As String
        Get
            Return m_MA_DVQL
        End Get
        Set(ByVal value As String)
            m_MA_DVQL = value
        End Set
    End Property
    Private m_MA_DVQL As String

    <XmlElement(Order:=6)> _
    Public Property KYHIEU_CT() As String
        Get
            Return m_KYHIEU_CT
        End Get
        Set(ByVal value As String)
            m_KYHIEU_CT = value
        End Set
    End Property
    Private m_KYHIEU_CT As String

    <XmlElement(Order:=7)> _
    Public Property TT_NT_MA_NT() As String
        Get
            Return m_TT_NT_MA_NT
        End Get
        Set(ByVal value As String)
            m_TT_NT_MA_NT = value
        End Set
    End Property
    Private m_TT_NT_MA_NT As String

    <XmlElement(Order:=8)> _
    Public Property TT_NT_TYGIA() As String
        Get
            Return m_TT_NT_TYGIA
        End Get
        Set(ByVal value As String)
            m_TT_NT_TYGIA = value
        End Set
    End Property
    Private m_TT_NT_TYGIA As String

    <XmlElement(Order:=9)> _
    Public Property CHUNGTU_CT_NDKT() As String
        Get
            Return m_CHUNGTU_CT_NDKT
        End Get
        Set(ByVal value As String)
            m_CHUNGTU_CT_NDKT = value
        End Set
    End Property
    Private m_CHUNGTU_CT_NDKT As String

    <XmlElement(Order:=10)> _
    Public Property CHUNGTU_CT_TEN_NDKT() As String
        Get
            Return m_CHUNGTU_CT_TEN_NDKT
        End Get
        Set(ByVal value As String)
            m_CHUNGTU_CT_TEN_NDKT = value
        End Set
    End Property
    Private m_CHUNGTU_CT_TEN_NDKT As String

    <XmlElement(Order:=11)> _
    Public Property TT_NT_TONGTIEN_VND() As String
        Get
            Return m_TT_NT_TONGTIEN_VND
        End Get
        Set(ByVal value As String)
            m_TT_NT_TONGTIEN_VND = value
        End Set
    End Property
    Private m_TT_NT_TONGTIEN_VND As String

    <XmlElement(Order:=12)> _
    Public Property TT_NT_TONGTIEN_NT() As String
        Get
            Return m_TT_NT_TONGTIEN_NT
        End Get
        Set(ByVal value As String)
            m_TT_NT_TONGTIEN_NT = value
        End Set
    End Property
    Private m_TT_NT_TONGTIEN_NT As String

    <XmlElement(Order:=13)> _
    Public Property NGUOILAP() As String
        Get
            Return m_NGUOILAP
        End Get
        Set(ByVal value As String)
            m_NGUOILAP = value
        End Set
    End Property
    Private m_NGUOILAP As String

    <XmlElement(Order:=14)> _
    Public Property NGUOIDUYET() As String
        Get
            Return m_NGUOIDUYET
        End Get
        Set(ByVal value As String)
            m_NGUOIDUYET = value
        End Set
    End Property
    Private m_NGUOIDUYET As String

    <XmlElement(Order:=15)> _
    Public Property TAIKHOAN_NT_MA_NH_TH() As String
        Get
            Return m_TAIKHOAN_NT_MA_NH_TH
        End Get
        Set(ByVal value As String)
            m_TAIKHOAN_NT_MA_NH_TH = value
        End Set
    End Property
    Private m_TAIKHOAN_NT_MA_NH_TH As String

    <XmlElement(Order:=16)> _
    Public Property TAIKHOAN_NT_TEN_TK_TH() As String
        Get
            Return m_TAIKHOAN_NT_TEN_TK_TH
        End Get
        Set(ByVal value As String)
            m_TAIKHOAN_NT_TEN_TK_TH = value
        End Set
    End Property
    Private m_TAIKHOAN_NT_TEN_TK_TH As String

    <XmlElement(Order:=17)> _
    Public Property TRANGTHAI_CT() As String
        Get
            Return m_TRANGTHAI_CT
        End Get
        Set(ByVal value As String)
            m_TRANGTHAI_CT = value
        End Set
    End Property
    Private m_TRANGTHAI_CT As String

    <XmlElement(Order:=18)> _
    Public Property TRANGTHAI_XULY() As String
        Get
            Return m_TRANGTHAI_XULY
        End Get
        Set(ByVal value As String)
            m_TRANGTHAI_XULY = value
        End Set
    End Property
    Private m_TRANGTHAI_XULY As String

    <XmlElement(Order:=19)> _
    Public Property LISTCHITIET() As List(Of VM_ChiTiet_CT)
        Get
            Return m_LISTCHITIET
        End Get
        Set(ByVal value As List(Of VM_ChiTiet_CT))
            m_LISTCHITIET = value
        End Set
    End Property
    Private m_LISTCHITIET As List(Of VM_ChiTiet_CT)


End Class

