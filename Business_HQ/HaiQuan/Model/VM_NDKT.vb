﻿Imports System.Xml.Serialization

Public Class VM_NDKT
    Public Sub New()
    End Sub
    <XmlElement(Order:=1)> _
   Public Property MA_TMUC() As String
        Get
            Return m_ma_tmuc
        End Get
        Set(ByVal value As String)
            m_ma_tmuc = value
        End Set
    End Property
    Private m_ma_tmuc As String
    <XmlElement(Order:=2)> _
    Public Property TEN_TMUC() As String
        Get
            Return m_ten_tmuc
        End Get
        Set(ByVal value As String)
            m_ten_tmuc = value
        End Set
    End Property
    Private m_ten_tmuc As String
End Class
