﻿Imports System.Xml.Serialization

Public Class VM_TyGia
    Public Sub New()
    End Sub
    <XmlElement(Order:=1)> _
   Public Property TEN_NT() As String
        Get
            Return m_TenNT
        End Get
        Set(ByVal value As String)
            m_TenNT = value
        End Set
    End Property
    Private m_TenNT As String
    <XmlElement(Order:=2)> _
    Public Property TYGIA() As String
        Get
            Return m_TyGia
        End Get
        Set(ByVal value As String)
            m_TyGia = value
        End Set
    End Property
    Private m_TyGia As String
End Class

Public Class VM_ChiNhanh
    Public Sub New()
    End Sub
    <XmlElement(Order:=1)> _
   Public Property ID() As String
        Get
            Return m_ID
        End Get
        Set(ByVal value As String)
            m_ID = value
        End Set
    End Property
    Private m_ID As String
    <XmlElement(Order:=2)> _
    Public Property NAME() As String
        Get
            Return m_Name
        End Get
        Set(ByVal value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String

End Class

Public Class P_VM_ChiNhanh
    Public Sub New()
    End Sub
    <XmlElement(Order:=1)> _
   Public Property RootID() As String
        Get
            Return m_RootID
        End Get
        Set(ByVal value As String)
            m_RootID = value
        End Set
    End Property
    Private m_RootID As String
    <XmlElement(Order:=2)> _
    Public Property ListChiNhanh() As List(Of VM_ChiNhanh)
        Get
            Return m_ListChiNhanh
        End Get
        Set(ByVal value As List(Of VM_ChiNhanh))
            m_ListChiNhanh = value
        End Set
    End Property
    Private m_ListChiNhanh As List(Of VM_ChiNhanh)

End Class

