﻿Imports System.Xml.Serialization


Public Class VM_ChiTiet_CT
    Public Sub New()
    End Sub

    <XmlElement(Order:=1)> _
    Public Property STT() As String
        Get
            Return m_STT
        End Get
        Set(ByVal value As String)
            m_STT = value
        End Set
    End Property
    Private m_STT As String
    <XmlElement(Order:=2)> _
    Public Property NDKT() As String
        Get
            Return m_NDKT
        End Get
        Set(ByVal value As String)
            m_NDKT = value
        End Set
    End Property
    Private m_NDKT As String

    <XmlElement(Order:=3)> _
    Public Property Ten_NDKT() As String
        Get
            Return m_Ten_NDKT
        End Get
        Set(ByVal value As String)
            m_Ten_NDKT = value
        End Set
    End Property
    Private m_Ten_NDKT As String
    <XmlElement(Order:=4)> _
    Public Property SoTien_NT() As String
        Get
            Return m_SoTien_NT
        End Get
        Set(ByVal value As String)
            m_SoTien_NT = value
        End Set
    End Property
    Private m_SoTien_NT As String

    <XmlElement(Order:=5)> _
    Public Property SoTien_VND() As String
        Get
            Return m_SoTien_VND
        End Get
        Set(ByVal value As String)
            m_SoTien_VND = value
        End Set
    End Property
    Private m_SoTien_VND As String
    <XmlElement(Order:=6)> _
    Public Property GhiChu() As String
        Get
            Return m_GhiChu
        End Get
        Set(ByVal value As String)
            m_GhiChu = value
        End Set
    End Property
    Private m_GhiChu As String

End Class
