﻿Imports Microsoft.VisualBasic

Public Class LogApp

    Private Shared log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Public Shared Sub AddDebug(ByVal pv_strAction As String, ByVal pv_strMessage As String)
        Dim v_strUserName As String = System.Web.HttpContext.Current.Session("UserName")
        Dim v_strBranh As String = System.Web.HttpContext.Current.Session("MA_CN_USER_LOGON")
        Dim v_strIp As String = System.Web.HttpContext.Current.Session("IP_LOCAL")

        log.Debug("userName=""" & v_strUserName & """ branchId=" & v_strBranh & " userIp=" & v_strIp & " action=" & pv_strAction & " Message=" & pv_strMessage & Environment.NewLine)


    End Sub
    Public Shared Sub AddDebug2(ByVal So_CT As String, ByVal So_TN As String)

        log.Debug(" SoCT=""" & So_CT & " So_TN_YC " & So_TN & Environment.NewLine)


    End Sub


    Private Shared Function GetIPv4Address() As String
        GetIPv4Address = String.Empty
        Dim strHostName As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(strHostName)

        For Each ipheal As System.Net.IPAddress In iphe.AddressList
            If ipheal.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                GetIPv4Address = ipheal.ToString()
            End If
        Next

    End Function


End Class
