﻿Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables

Namespace XLCuoiNgay
    Public Class buXLCN

#Region "Danh Mục"
        Public Shared Function DS_CTHuy(ByVal strWhere As String) As DataSet
            Try
                Dim db As New daXLCN
                Return db.DS_CTHuy(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "TCS_BKE_REPORT"

        Public Shared Function TCS_CTU_LIETKE(ByVal mstrWhere As String) As DataSet
            Try
                Dim db As New daXLCN
                Return db.TCS_CTU_LIETKE(mstrWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_BK_CTU_CHITIET(ByVal mstrWhere As String) As DataSet
            Try
                Dim db As New daXLCN
                Return db.TCS_BK_CTU_CHITIET(mstrWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_BK_CTU_CHITIET_TB(ByVal mstrWhere As String) As DataSet
            Try
                Dim db As New daXLCN
                Return db.TCS_BK_CTU_CHITIET_TB(mstrWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_BK_CTU_NO_DTL(ByVal mstrWhere As String) As DataSet
            Try
                Dim db As New daXLCN
                Return db.TCS_BK_CTU_NO_DTL(mstrWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_BK_CTU_WITH_DTL(ByVal mstrWhere As String) As DataSet
            Try
                Dim db As New daXLCN
                Return db.TCS_BK_CTU_WITH_DTL(mstrWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_BK_CTU_WITH_DTL_CHITIET(ByVal mstrWhere As String) As DataSet
            Try
                Dim db As New daXLCN
                Return db.TCS_BK_CTU_WITH_DTL_CHITIET(mstrWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_PHIEUKT(ByVal mstrWhere As String, Optional ByVal blnTHOP As Boolean = False) As DataSet
            Try
                Dim db As New daXLCN
                Return db.TCS_PHIEUKT(mstrWhere, blnTHOP)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_SOTHUTM(ByVal mstrWhere As String) As DataSet
            Try
                Dim db As New daXLCN
                Return db.TCS_SOTHUTM(mstrWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "XỬ LÝ CUỐI NGÀY - BẢNG KÊ"
        Public Shared Function TCS_BKBLT_CT(ByVal strWhere As String) As DataSet
            Try
                Dim daXL As New daXLCN
                Return daXL.TCS_BKBLT_CT(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DsBK_ThuTP(ByVal strwhere As String)
            Try
                Dim objda As New daXLCN
                Dim ds As DataSet
                ds = objda.dsBKThuTP(strwhere)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function DsBK_THop_ThuP(ByVal sql As String) As DataSet
            Try
                Dim ds As DataSet
                Dim obj As New daXLCN
                ds = obj.dsBKTHopThuP(sql)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

    End Class
End Namespace