﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.Common.mdlCommon

Namespace XLCuoiNgay
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Class daXLCN
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "Danh mục"
        Public Sub New()

        End Sub

        Public Function DS_CTHuy(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh sách chứng từ Hủy.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 18/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------        
            Try
                Dim conn As New DataAccess
                Dim strSQL As String
                strSQL = "Select hdr.kyhieu_ct, hdr.so_ct, hdr.ma_nnthue, hdr.ten_nnthue, hdr.TTIEN " & _
                    " From TCS_CTU_HDR hdr " & _
                    " where (hdr.trang_thai = '04') " & _
                    " and (not ((hdr.ma_tq <> 0) and (hdr.tt_tthu = 0))) " & _
                    strWhere & _
                    " order by kyhieu_ct, so_ct"

                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách chứng từ hủy")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            End Try
        End Function
#End Region

#Region "TCS_BKE_REPORT"

        Public Function TCS_CTU_LIETKE(ByVal mstrWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report        
            ' Ngày viết: 09/06/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess
                strSQL = "select hdr.ngay_kb as Ngay_CT, hdr.so_ct as So_CT, (lpad(to_char(hdr.ma_nv),3,'0')||'/'||lpad(to_char(hdr.So_BT),4,'0')) as MaNV_SoBT, " & _
                                "hdr.tk_no as TK_No, hdr.tk_co as tk_co, " & _
                                "(dtl.ma_chuong||'-'||dtl.ma_khoan||'-'||dtl.ma_tmuc) as MLNS, " & _
                                "dtl.ma_TLDT as Ma_TLDT , dtl.sotien as So_Tien " & _
                                "from tcs_ctu_hdr hdr, tcs_ctu_dtl dtl, TCS_DM_XA xa, TCS_DM_CQTHU cqthu, TCS_DM_LTHUE lt  " & _
                                "where (hdr.shkb =dtl.shkb) and (hdr.ngay_kb =dtl.ngay_kb) and (hdr.ma_nv =dtl.ma_nv ) " & _
                                "and (hdr.so_bt =dtl.so_bt ) and (hdr.ma_dthu =dtl.ma_dthu) and (hdr.xa_id =xa.xa_id )  " & _
                                "and (hdr.ma_cqthu =cqthu.ma_cqthu ) and (hdr.ma_lthue =lt.ma_lthue(+)) " & _
                                "and hdr.trang_thai = '01' and hdr.ma_dthu=cqthu.ma_dthu " & mstrWhere & _
                                " Order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt asc"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình Truy vấn BK CTU đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_BK_CTU_CHITIET(ByVal mstrWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report        
            ' Ngày viết: 08/05/2008
            ' Người viết: Lê Hồng Hà

            ' Người sửa: Hoàng Văn Anh
            ' Mục đích: sửa theo COA của Ngân Hàng
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess
                'ICB
                'Hoàng Văn Anh: Thêm vào Tên Nhân Viên trong câu select nv.TEN
                'strSQL = "select hdr.ma_nnthue as MaSoThue, hdr.ten_nnthue as Ten_NNT, " & _
                '        "hdr.so_ct as So_CT, hdr.kyhieu_ct as kh_ct, hdr.ngay_kb as Ngay_CT, dtl.ky_thue as Ky_Thue, " & _
                '        "(dtl.ma_chuong||'.'||dtl.ma_khoan||'.'||dtl.ma_tmuc) as MLNS, " & _
                '        "dtl.sotien as So_Tien , hdr.so_tk as So_TK, to_char(hdr.ngay_tk, 'dd/MM/rrrr') as Ngay_TK, " & _
                '        "(hdr.ma_xa) as DBHC,  " & _
                '        "hdr.tk_co as TK_Co, (hdr.kyhieu_ct||hdr.so_ct) as MaNV_SoBT ,nv.TEN   " & _
                '        "from TCS_CTU_HDR hdr, TCS_CTU_DTL dtl, TCS_DM_XA xa, TCS_DM_CQTHU cqthu, TCS_DM_LTHUE lt ,tcs_dm_nhanvien nv " & _
                '        "where (hdr.shkb =dtl.shkb) and (hdr.ngay_kb =dtl.ngay_kb ) and (hdr.ma_nv =dtl.ma_nv ) " & _
                '        "and (hdr.so_bt =dtl.so_bt ) and (hdr.ma_dthu =dtl.ma_dthu) and (hdr.xa_id =xa.xa_id )  " & _
                '        "and (hdr.ma_cqthu =cqthu.ma_cqthu ) and (hdr.ma_lthue =lt.ma_lthue(+))  AND (hdr.MA_NV=nv.MA_NV)" & _
                '        "and (hdr.trang_thai <> '04')" & mstrWhere & _
                '        " Order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt asc"



                strSQL = "select hdr.kyhieu_ct as kyhieu_ct, hdr.so_ct as so_ct, " & _
                            "hdr.ngay_ct as ngay_ct, hdr.so_tk as so_tk,  " & _
                            "to_char(hdr.ngay_tk, 'dd/MM/yyyy') as ngay_tk, hdr.lh_xnk as lhxnk, " & _
                            "hdr.ma_nnthue as ma_nnthue, hdr.ten_nnthue as ten_nnthue, " & _
                            "(dtl.ma_chuong) as CC, (dtl.ma_khoan) as LK, " & _
                            "(dtl.ma_tmuc) as MTM, dtl.ky_thue as ky_thue, " & _
                            "(hdr.ma_xa) as DBHC, " & _
                            "(hdr.tk_no) as tk_no, (hdr.tk_co) as tk_co, " & _
                            "dtl.sotien as so_tien,  " & _
                            "hdr.tk_co as Dau_TK_Co, (hdr.kyhieu_ct||hdr.so_ct) as KeyCT, " & _
                            "to_char(ngay_kh_nh, 'dd/MM/yyyy') as ngay_nh, hdr.dc_nnthue as Ten_DBHC,  " & _
                            "hdr.ma_nv as ma_nv, hdr.so_bt as so_bt " & _
                            "from tcs_ctu_hdr hdr, tcs_ctu_dtl dtl, tcs_dm_lhinh lh, tcs_dm_xa xa " & _
                            "where (hdr.shkb = dtl.shkb(+)) and (hdr.ngay_kb = dtl.ngay_kb(+)) and " & _
                            "(hdr.ma_nv = dtl.ma_nv(+)) and (hdr.so_bt = dtl.so_bt(+)) and " & _
                            "(hdr.ma_dthu = dtl.ma_dthu(+)) and (hdr.lh_xnk = lh.ma_lh(+))" & _
                            " and (hdr.ma_xa = xa.ma_xa(+)) and (hdr.trang_thai = '01') " & _
                            mstrWhere & _
                            " union all " & _
                            "select hdr.kyhieu_ct as kyhieu_ct, hdr.so_ct as so_ct, " & _
                            "hdr.ngay_kb as ngay_ct, hdr.so_tk as so_tk,  " & _
                            "to_char(hdr.ngay_tk, 'dd/MM/yyyy') as ngay_tk, hdr.lh_xnk as lhxnk, " & _
                            "hdr.ma_nnthue as ma_nnthue, hdr.ten_nnthue as ten_nnthue, " & _
                            "(dtl.ma_chuong) as CC, (dtl.ma_khoan) as LK, " & _
                            "(dtl.ma_tmuc) as MTM, dtl.ky_thue as ky_thue, " & _
                            "(hdr.ma_xa) as DBHC, " & _
                            "(hdr.tk_no) as tk_no, (hdr.tk_co) as tk_co, " & _
                            "dtl.sotien as so_tien,  " & _
                            "hdr.tk_co as Dau_TK_Co, (hdr.kyhieu_ct||hdr.so_ct) as KeyCT," & _
                            "to_char(ngay_kh_nh, 'dd/MM/yyyy') as ngay_nh, hdr.dc_nnthue as Ten_DBHC,  " & _
                            "hdr.ma_nv as ma_nv, hdr.so_bt as so_bt " & _
                            "from tcs_ctu_thop_hdr hdr, tcs_ctu_thop_dtl dtl, tcs_dm_lhinh lh, tcs_dm_xa xa " & _
                            "where (hdr.shkb = dtl.shkb(+)) and (hdr.ngay_kb = dtl.ngay_kb(+)) and " & _
                            "(hdr.ma_nv = dtl.ma_nv(+)) and (hdr.so_bt = dtl.so_bt(+)) and " & _
                            "(hdr.ma_dthu = dtl.ma_dthu(+)) and (hdr.lh_xnk = lh.ma_lh(+)) " & _
                            " and (hdr.ma_xa = xa.ma_xa(+)) and (hdr.trang_thai = '01') and (hdr.ma_dthu <> '01') " & _
                            mstrWhere & _
                            "order by dau_tk_co, ngay_ct, ma_nv, so_bt "
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình Truy vấn BK CTU đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_BK_CTU_CHITIET_TB(ByVal mstrWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report        
            ' Ngày viết: 08/05/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess
                strSQL = "select hdr.ten_nnthue as Ten_NNT, " & _
                                "hdr.so_ct as So_CT, hdr.kyhieu_ct as kh_ct, hdr.ngay_kb as Ngay_CT, " & _
                                "(dtl.ma_chuong||'.'||dtl.ma_khoan||'.'||dtl.ma_tmuc) as MLNS, " & _
                                "dtl.sotien as So_Tien , hdr.dc_nnthue as Dia_Chi, " & _
                                "(hdr.ma_xa) as DBHC,  " & _
                                "'' as TKCo, (hdr.kyhieu_ct||hdr.so_ct) as MaNV_SoBT,hdr.MA_KS    " & _
                                "from TCS_CTU_HDR hdr, TCS_CTU_DTL dtl, TCS_DM_XA xa, TCS_DM_CQTHU cqthu, TCS_DM_LTHUE lt  " & _
                                "where (hdr.shkb =dtl.shkb) and (hdr.ngay_kb =dtl.ngay_kb ) and (hdr.ma_nv =dtl.ma_nv ) " & _
                                "and (hdr.so_bt =dtl.so_bt ) and (hdr.ma_dthu =dtl.ma_dthu) and (hdr.xa_id =xa.xa_id )  " & _
                                "and (hdr.ma_cqthu =cqthu.ma_cqthu ) and (hdr.ma_lthue =lt.ma_lthue(+)) " & _
                                "and (hdr.trang_thai <> '04')" & mstrWhere & _
                                " Order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt asc"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình Truy vấn BK CTU đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_BK_CTU_WITH_DTL_CHITIET(ByVal mstrWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 16/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess
                strSQL = "select hdr.tk_co as TK_Co , hdr.ma_nnthue as Ma_NNT, hdr.ten_nnthue as Ten_NNT, " & _
                                "hdr.kyhieu_ct as Kyhieu_CT, hdr.so_ct as So_CT, " & _
                                "(dtl.ma_chuong||'.'||dtl.ma_khoan||'.'||dtl.ma_tmuc) as MLNS, " & _
                                "cqthu.ma_cqthu as Ma_CQThu, cqthu.ten as Ten_CQThu, " & _
                                "(hdr.ma_xa) as Ma_DB, xa.ten as Ten_DB, " & _
                                "dtl.noi_dung as Noidung, hdr.ngay_kb as Ngay_KB, dtl.ky_thue as KyThue, " & _
                                "hdr.so_bthu as Ma_BT, dtl.sotien as Tien , hdr.tt_tthu as So_TThu, " & _
                                "lt.ma_lthue as Ma_LoaiThue, lt.ten as Ten_LoaiThue, hdr.tk_no as TK_No " & _
                                "from TCS_CTU_HDR hdr, TCS_CTU_DTL dtl, TCS_DM_XA xa, TCS_DM_CQTHU cqthu, TCS_DM_LTHUE lt  " & _
                                "where (hdr.shkb =dtl.shkb) and (hdr.ngay_kb =dtl.ngay_kb ) and (hdr.ma_nv =dtl.ma_nv ) " & _
                                "and (hdr.so_bt =dtl.so_bt ) and (hdr.ma_dthu =dtl.ma_dthu) and (hdr.xa_id =xa.xa_id )  " & _
                                "and (hdr.ma_cqthu =cqthu.ma_cqthu ) and (hdr.ma_lthue =lt.ma_lthue(+)) " & _
                                "and (hdr.trang_thai = '01')" & mstrWhere & _
                                " Order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt asc"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình Truy vấn BK CTU đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_BK_CTU_WITH_DTL(ByVal mstrWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 16/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess
                strSQL = "select hdr.tk_co as TK_Co , hdr.ma_nnthue as Ma_NNT, hdr.ten_nnthue as Ten_NNT, " & _
                                 "hdr.kyhieu_ct as Kyhieu_CT, hdr.so_ct as So_CT, " & _
                                 "(dtl.ma_chuong||'.'||dtl.ma_khoan||'.'||dtl.ma_tmuc) as MLNS, " & _
                                 "cqthu.ma_cqthu as Ma_CQThu, cqthu.ten as Ten_CQThu, " & _
                                 "(hdr.ma_xa) as Ma_DB, xa.ten as Ten_DB, " & _
                                 "dtl.noi_dung as Noidung, hdr.ngay_kb as Ngay_KB, dtl.ky_thue as KyThue, " & _
                                 "hdr.so_bthu as Ma_BT, sum(dtl.sotien) as Tien , hdr.tt_tthu as So_TThu, " & _
                                 "lt.ma_lthue as Ma_LoaiThue, lt.ten as Ten_LoaiThue, hdr.tk_no as TK_No " & _
                                 "from TCS_CTU_HDR hdr, TCS_CTU_DTL dtl, TCS_DM_XA xa, TCS_DM_CQTHU cqthu, TCS_DM_LTHUE lt  " & _
                                 "where (hdr.shkb =dtl.shkb) and (hdr.ngay_kb =dtl.ngay_kb ) and (hdr.ma_nv =dtl.ma_nv ) " & _
                                 "and (hdr.so_bt =dtl.so_bt ) and (hdr.ma_dthu =dtl.ma_dthu) and (hdr.xa_id =xa.xa_id )  " & _
                                 "and (hdr.ma_cqthu =cqthu.ma_cqthu ) and (hdr.ma_lthue =lt.ma_lthue(+)) " & _
                                 "and (hdr.trang_thai = '01') and hdr.ma_dthu=cqthu.ma_dthu " & mstrWhere & _
                                 " GROUP BY hdr.tk_co , hdr.ma_nnthue , hdr.ten_nnthue , hdr.kyhieu_ct , " & _
                                 "hdr.so_ct,cqthu.ma_cqthu, cqthu.ten,xa.ten, dtl.noi_dung , hdr.ngay_kb, " & _
                                 "dtl.ky_thue, hdr.so_bthu,hdr.tt_tthu, lt.ma_lthue,lt.ten, hdr.tk_no, " & _
                                 "(hdr.ma_xa), " & _
                                 "(dtl.ma_chuong||'.'||dtl.ma_khoan||'.'||dtl.ma_tmuc) "
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình Truy vấn BK CTU đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_BK_CTU_NO_DTL(ByVal mstrWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 16/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess
                strSQL = "select nv.ten_dn ma_bt,hdr.tk_co as TK_Co , hdr.ma_nnthue as Ma_NNT, hdr.ten_nnthue as Ten_NNT, " & _
                               "hdr.kyhieu_ct as Kyhieu_CT, hdr.so_ct as So_CT, " & _
                               "cqthu.ma_cqthu as Ma_CQThu, cqthu.ten as Ten_CQThu, " & _
                               "(hdr.ma_xa) as Ma_DB, xa.ten as Ten_DB, " & _
                               "hdr.ngay_kb as Ngay_KB, " & _
                               "hdr.ttien as Tien , hdr.tt_tthu as So_TThu, " & _
                               "lt.ma_lthue as Ma_LoaiThue, lt.ten as Ten_LoaiThue, hdr.tk_no as TK_No " & _
                               "from TCS_CTU_HDR hdr, TCS_DM_XA xa, TCS_DM_CQTHU cqthu, TCS_DM_LTHUE lt,tcs_dm_nhanvien nv " & _
                               "where (hdr.xa_id =xa.xa_id )  " & _
                               "and (hdr.ma_nv = nv.ma_nv) and (hdr.ma_cqthu =cqthu.ma_cqthu ) and (hdr.ma_lthue =lt.ma_lthue(+)) " & _
                               "and (hdr.trang_thai = '01') and hdr.ma_dthu=cqthu.ma_dthu " & mstrWhere & _
                               " Order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt asc"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình Truy vấn BK CTU đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        'Kienvt :Check here
        Public Function TCS_PHIEUKT(ByVal mstrWhere As String, Optional ByVal blnTHOP As Boolean = False) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn Phiếu kế toán Địa Bàn Thu đưa ra DataSet để gắn vào report
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 18/10/2007
            ' Người viết: Lê Hồng Hà
            ' Người sửa: Nguyễn Mạnh Tuấn
            ' Ngày : 08/12/2008
            ' Mục đích: Truy vấn Phiếu kế toán Địa Bàn Thu theo Mã COA
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Dim strFromTable As String
            Try
                If blnTHOP = True Then
                    strFromTable = "from TCS_CTU_THOP_HDR hdr, TCS_CTU_THOP_DTL dtl, TCS_DM_XA xa, TCS_DM_CQTHU cqthu "
                Else
                    strFromTable = "from TCS_CTU_HDR hdr, TCS_CTU_DTL dtl, TCS_DM_XA xa, TCS_DM_CQTHU cqthu "
                End If
                conn = New DataAccess
                strSQL = "select hdr.tk_no as TK_No, hdr.tk_co as TK_Co, " & _
                        "(dtl.ma_chuong||'.'||dtl.ma_khoan||'.'||dtl.ma_tmuc) as MLNS, " & _
                        "(hdr.ma_xa) as Ma_DB, " & _
                        "xa.ten as Ten_DB, cqthu.ma_cqthu as Ma_CQThu, cqthu.ten as Ten_CQThu, " & _
                        "sum(dtl.sotien) as So_Tien " & _
                        strFromTable & _
                        "where (hdr.shkb =dtl.shkb) and (hdr.ngay_kb =dtl.ngay_kb ) and (hdr.ma_nv =dtl.ma_nv ) " & _
                        "and (hdr.so_bt =dtl.so_bt ) and (hdr.ma_dthu =dtl.ma_dthu) and (hdr.xa_id =xa.xa_id ) " & _
                        "and (hdr.ma_cqthu =cqthu.ma_cqthu ) " & _
                        "and (hdr.trang_thai <> '04')" & mstrWhere & _
                        " GROUP BY hdr.tk_no,hdr.tk_co,(dtl.ma_chuong||'.'||dtl.ma_khoan||'.'||dtl.ma_tmuc),hdr.ma_xa,xa.ten,cqthu.ma_cqthu,cqthu.ten "
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình Truy vấn Phiếu kế toán Địa Bàn Thu đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        'Public Function TCS_PHIEUKT(ByVal mstrWhere As String) As DataSet
        '    '-----------------------------------------------------
        '    ' Mục đích: Truy vấn Phiếu kế toán Địa Bàn Thu đưa ra DataSet để gắn vào report
        '    ' Tham số: 
        '    ' Giá trị trả về:
        '    ' Ngày viết: 18/10/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim conn As DataAccess
        '    Dim strSQL As String
        '    Try
        '        conn = New DataAccess
        '        strSQL = "select hdr.tk_no as TK_No, hdr.tk_co as TK_Co, " & _
        '                       "(dtl.ma_cap||'.'||dtl.ma_chuong||'.'||dtl.ma_loai||'.'||dtl.ma_khoan||'.'||dtl.ma_muc||'.'||dtl.ma_tmuc) as MLNS, " & _
        '                       "(hdr.ma_tinh||'.'||hdr.ma_huyen||'.'||hdr.ma_xa) as Ma_DB, " & _
        '                       "xa.ten as Ten_DB, cqthu.ma_cqthu as Ma_CQThu, cqthu.ten as Ten_CQThu, " & _
        '                       "dtl.sotien as So_Tien " & _
        '                       "from TCS_CTU_HDR hdr, TCS_CTU_DTL dtl, TCS_DM_XA xa, TCS_DM_CQTHU cqthu " & _
        '                       "where (hdr.shkb =dtl.shkb) and (hdr.ngay_kb =dtl.ngay_kb ) and (hdr.ma_nv =dtl.ma_nv ) " & _
        '                       "and (hdr.so_bt =dtl.so_bt ) and (hdr.ma_dthu =dtl.ma_dthu) and (hdr.xa_id =xa.xa_id ) " & _
        '                       "and (hdr.ma_cqthu =cqthu.ma_cqthu ) " & _
        '                       "and (hdr.trang_thai <> '04')" & mstrWhere
        '        Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If (Not IsNothing(conn)) Then conn.Dispose()
        '    End Try
        'End Function
        Public Function TCS_SOTHUTM(ByVal mstrWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn sổ thu tiền mặt đưa ra DataSet để gắn vào report
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 18/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess

                strSQL = "select distinct hdr.ten_nnthue as Ten_NNT, hdr.kyhieu_ct as Kyhieu_CT, " & _
                                "hdr.so_ct as So_CT, hdr.ngay_kb as Ngay_CT, tq.ma_nv as Ma_TQ, " & _
                                "tq.ten as Ten_TQ, hdr.ttien as So_ThucThu, " & _
                                "(hdr.tt_tthu - hdr.ttien ) as Tien_Thua, " & _
                                "hdr.ngay_kb, hdr.ma_nv , hdr.so_bt " & _
                                "from TCS_CTU_HDR hdr, TCS_DM_NHANVIEN tq, TCS_KHOQUY_DTL kq " & _
                                "where (hdr.ma_tq = tq.ma_nv) and (hdr.trang_thai <>'04') and " & _
                                "(hdr.kyhieu_ct=kq.kyhieu_ct) and (hdr.So_CT=kq.So_CT) " & _
                                mstrWhere & _
                                " Order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt asc"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi Truy vấn sổ thu tiền mặt đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
#End Region

#Region "XỬ LÝ CUỐI NGÀY - BẢNG KÊ"
        Public Function TCS_BKBLT_CT(ByVal strWhere As String) As DataSet
            Dim conn As DataAccess
            Dim strSql As String
            Try
                Dim ds As New DataSet
                strSql = "Select BL.Ten_NNTien as Ten_NNTien,BL.DC_NNTien as DC_NNTien," & _
                         "BL.MA_CQQD as Ma_CQQD,CQ.Ten_CQQD as Ten_CQQD," & _
                         "BL.So_QD as So_QD,BL.Ngay_QD as Ngay_QD, BL.Kyhieu_BL as Kyhieu_BL," & _
                         "BL.So_BL as So_BL, BL.Ttien as So_Tien from TCS_CTBL BL, TCS_DM_CQQD CQ " & _
                         "Where (BL.Ma_CQQD = CQ.Ma_CQQD) " & strWhere & " order by BL.Kyhieu_BL, BL.So_BL "

                conn = New DataAccess
                ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy dữ liệu cho bản kê theo hình thức thu")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu cho bản kê theo hình thức thu: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Function Get_LHThu() As DataSet
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy danh sách các loại hình thu
            ' Tham số       : 
            ' Giá trị trả về: 
            ' Ngày viết     : 9/11/2007
            ' Người viết    : Lê anh Ngọc
            ' ----------------------------------------------------------------
            Dim conn As DataAccess
            Dim SQL As String
            Dim ds As DataSet
            Try
                SQL = " Select Ma_LH, Ten_LH from TCS_DM_LHTHU Order by Ten_LH"
                conn = New DataAccess
                ds = New DataSet
                ds = conn.ExecuteReturnDataSet(SQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy loại hình thu")
                'LogDebug.WriteLog("Loi khi lay loai hinh thu " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not ds Is Nothing Then
                    ds.Dispose()
                End If
                If Not conn Is Nothing Then conn.Dispose()
            End Try
        End Function

        Public Function dsBKThuTP(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy BK thu tien phat
            ' Tham số       : 
            ' Giá trị trả về: 
            ' Ngày viết     : 10/11/2007
            ' Người viết    : Lê anh Ngọc
            ' ----------------------------------------------------------------
            Dim conn As DataAccess
            Dim sql As String
            Try
                sql = " Select CB.Ten_NNTien as Ten_NNTien, CB.DC_NNTien as DC_NNTien,CB.Ma_CQQD as Ma_CQQD, CQ.Ten_CQQD as Ten_CQQD," & _
                               " CB.So_QD as So_QD, CB.Ngay_QD as Ngay_QD, CB.Kyhieu_BL as Kyhieu_BL, CB.So_BL as So_BL, to_char(CB.Ngay_CT) as Ngay_CT, CB.TTien as So_Tien" & _
                               " From TCS_CTBL CB " & strWhere & _
                               "" & " order by CB.Kyhieu_BL, CB.So_BL"
                conn = New DataAccess
                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy bảng kê thu tiền phạt")
                'LogDebug.WriteLog("Lấy BK thu tien phat " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try

        End Function
        Public Function dsBKTHopThuP(ByVal sql As String) As DataSet
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình truy vấn bảng kê tài khoản thu")
                'LogDebug.WriteLog("Lỗi trong khi quy vấn bảng kê tài khoản thu " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                conn.Dispose()
            End Try
        End Function
#End Region

    End Class
End Namespace