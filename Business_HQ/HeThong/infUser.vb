Imports VBOracleLib

Namespace HeThong

    Public Class infUser
        Private strMa_NV As Integer
        Private strMa_Nhom As String
        Private strTen As String
        Private strMat_Khau As String
        Private strBan_Lv As String
        Private strChuc_Danh As String
        Private strTen_DN As String
        Private strTellerId As String
        Private strMaCN As String
        Private strTrang_thai As String
        'Kiểu ĐN
        Private strLDAP As String
        Private strHanMucTu As String
        Private strHanMucDen As String
        Private strUserDomain As String
        Public Property Trang_thai() As String
            Get
                Return strTrang_thai
            End Get
            Set(ByVal Value As String)
                strTrang_thai = Value
            End Set
        End Property
        Public Property Ma_NV() As Integer
            Get
                Return strMa_NV
            End Get
            Set(ByVal Value As Integer)
                strMa_NV = Value
            End Set
        End Property
        Public Property Teller_Id() As String
            Get
                Return strTellerId
            End Get
            Set(ByVal Value As String)
                strTellerId = Value
            End Set
        End Property
        Public Property MaCN() As String
            Get
                Return strMaCN
            End Get
            Set(ByVal Value As String)
                strMaCN = Value
            End Set
        End Property
        Public Property TEN_ND() As String
            Get
                Return strTen_DN
            End Get
            Set(ByVal Value As String)
                strTen_DN = Value
            End Set
        End Property
        Public Property Ma_Nhom() As String
            Get
                Return strMa_Nhom
            End Get
            Set(ByVal Value As String)
                strMa_Nhom = Value
            End Set
        End Property
        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property
        Public Property Mat_Khau() As String
            Get
                Return strMat_Khau
            End Get
            Set(ByVal Value As String)
                strMat_Khau = Value
            End Set
        End Property
        Public Property Chuc_Danh() As String
            Get
                Return strChuc_Danh
            End Get
            Set(ByVal Value As String)
                strChuc_Danh = Value
            End Set
        End Property
        Public Property Ban_Lv() As String
            Get
                Return strBan_Lv
            End Get
            Set(ByVal Value As String)
                strBan_Lv = Value
            End Set
        End Property
        Public Property LDAP() As String
            Get
                Return strLDAP
            End Get
            Set(ByVal Value As String)
                strLDAP = Value
            End Set
        End Property
        Public Property HanMuc_Tu() As String
            Get
                Return strHanMucTu
            End Get
            Set(ByVal Value As String)
                strHanMucTu = Value
            End Set
        End Property
        Public Property HanMuc_Den() As String
            Get
                Return strHanMucDen
            End Get
            Set(ByVal Value As String)
                strHanMucDen = Value
            End Set
        End Property
        Public Property User_Domain() As String
            Get
                Return strUserDomain
            End Get
            Set(ByVal Value As String)
                strUserDomain = Value
            End Set
        End Property
        Sub New()
            'strMa_NV = ""
            strMa_Nhom = ""
            strTen = ""
            strBan_Lv = ""
            strChuc_Danh = ""
            strMat_Khau = ""
            strTen_DN = ""
            strMaCN = ""
            strTellerId = ""
            strTrang_thai = ""
            strLDAP = 0
            strHanMucDen = ""
            strHanMucTu = ""
            strUserDomain = ""
        End Sub
        Sub New(ByVal userID As String)
            Try

                Dim slqStr As String = "select * from tcs_dm_nhanvien where ma_nv='" & CInt(userID) & "'"
                Dim dt As DataTable = DatabaseHelp.ExecuteToTable(slqStr)
                strMa_NV = CInt(userID)
                strMa_Nhom = dt.Rows(0)("ma_nhom").ToString()
                strTen = dt.Rows(0)("ten").ToString()
                strBan_Lv = dt.Rows(0)("ban_lv").ToString()
                strChuc_Danh = dt.Rows(0)("chuc_danh").ToString()
                strMat_Khau = dt.Rows(0)("mat_khau").ToString()
                strTen_DN = dt.Rows(0)("ten_dn").ToString()
                strMaCN = dt.Rows(0)("ma_cn").ToString()
                strTellerId = dt.Rows(0)("teller_id").ToString()
                strTrang_thai = dt.Rows(0)("tinh_trang").ToString()
                strLDAP = dt.Rows(0)("ldap").ToString()
                strHanMucTu = dt.Rows(0)("hanmuc_tu").ToString()
                strHanMucDen = dt.Rows(0)("hanmuc_den").ToString()
                strUserDomain = dt.Rows(0)("UserDomain").ToString()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class

End Namespace