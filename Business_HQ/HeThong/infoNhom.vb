Namespace HeThong

    Public Class infNhom
        Private strMa_Nhom As String
        Private strTen As String
        Private strID As String

        Public Sub New()
            strMa_Nhom = ""
            strTen = ""
            strID = ""
        End Sub
        Public Property Ma_Nhom() As String
            Get
                Return strMa_Nhom
            End Get
            Set(ByVal Value As String)
                strMa_Nhom = Value
            End Set
        End Property
        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property
        Public Property ID() As String
            Get
                Return strID
            End Get
            Set(ByVal Value As String)
                strID = Value
            End Set
        End Property
    End Class

End Namespace