﻿'Imports VBOracleLib
'Namespace HeThong

'    Public Class daThamso
'        Public Function Thamso(ByVal sqlstr As String) As DataSet
'            Dim conn As DataAccess
'            Try
'                conn = New DataAccess
'                Return conn.ExecuteReturnDataSet(sqlstr, CommandType.Text)
'            Catch ex As Exception
'                LogDebug.WriteLog("Lỗi khi truy vấn tham số dùng chung " & ex.ToString, EventLogEntryType.Error)
'                Throw ex
'            Finally
'                If Not conn Is Nothing Then
'                    conn.Dispose()
'                End If
'            End Try
'        End Function

'        Public Function UpdateThamso(ByVal sql1 As String, ByVal sql2 As String) As Boolean
'            Dim conn As DataAccess
'            Dim trans As IDbTransaction
'            Try
'                conn = New DataAccess
'                trans = conn.BeginTransaction
'                conn.ExecuteNonQuery(sql1, CommandType.Text, trans)
'                If Not "".Equals(sql2) And (Not sql2 Is Nothing) Then
'                    conn.ExecuteNonQuery(sql2, CommandType.Text, trans)
'                End If
'                trans.Commit()
'            Catch ex As Exception
'                trans.Rollback()
'                LogDebug.WriteLog("Lỗi khi cập nhật tham số dùng chung " & ex.ToString, EventLogEntryType.Error)
'                Throw ex
'            Finally
'                If Not trans Is Nothing Then
'                    trans.Dispose()
'                End If
'                If Not conn Is Nothing Then
'                    conn.Dispose()
'                End If
'            End Try
'        End Function
'        Public Function UpdateThamsoHT(ByVal sql As String) As Boolean
'            Dim conn As DataAccess
'            Dim trans As IDbTransaction
'            Try
'                conn = New DataAccess
'                conn.ExecuteNonQuery(sql, CommandType.Text)
'            Catch ex As Exception
'                trans.Rollback()
'                LogDebug.WriteLog("Lỗi khi cập nhật tham số hệ thống " & ex.ToString, EventLogEntryType.Error)
'                Throw ex
'            Finally
'                If Not conn Is Nothing Then
'                    conn.Dispose()
'                End If
'            End Try
'        End Function

'        ''' ***************BEGIN****************************************'
'        ''' ---------------CREATE--------------------------
'        '''  Tên:			Updates the thamso_ DC.
'        '''  Người viết:	ANHLD
'        '''  Ngày viết:		10/02/2012 00:00:00
'        '''  Mục đích:	    Cap nhat tham so khi doi chieu tu dong
'        ''' ---------------MODIFY--------------------------
'        '''  Người sửa:	
'        '''  Ngày sửa: 	
'        '''  Mục đích:	
'        ''' ---------------PARAM--------------------------
'        ''' <param name="sql">The SQL.</param>
'        ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
'        Public Function UpdateThamso_DC(ByVal sql As String) As Boolean
'            Dim conn As DataAccess
'            Dim trans As IDbTransaction
'            Try
'                conn = New DataAccess
'                trans = conn.BeginTransaction
'                conn.ExecuteNonQuery(sql, CommandType.Text, trans)

'                trans.Commit()
'            Catch ex As Exception
'                trans.Rollback()
'                LogDebug.WriteLog("Lỗi khi cập nhật tham số dùng chung " & ex.ToString, EventLogEntryType.Error)
'                Throw ex
'            Finally
'                If Not trans Is Nothing Then
'                    trans.Dispose()
'                End If
'                If Not conn Is Nothing Then
'                    conn.Dispose()
'                End If
'            End Try
'        End Function

'        ''' ***************BEGIN****************************************'
'        ''' ---------------CREATE--------------------------
'        '''  Tên:			Insert_s the log_ DC.
'        '''  Người viết:	ANHLD
'        '''  Ngày viết:		10/02/2012 00:00:00
'        '''  Mục đích:	    Insert log khi thuc hien doi chieu tu dong
'        ''' ---------------MODIFY--------------------------
'        '''  Người sửa:	
'        '''  Ngày sửa: 	
'        '''  Mục đích:	
'        ''' ---------------PARAM--------------------------
'        ''' <param name="sql">The SQL.</param>
'        ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
'        Public Function Insert_Log_DC(ByVal sql As String) As Boolean
'            Dim conn As DataAccess
'            Dim trans As IDbTransaction
'            Try
'                conn = New DataAccess
'                trans = conn.BeginTransaction
'                conn.ExecuteNonQuery(sql, CommandType.Text, trans)

'                trans.Commit()
'            Catch ex As Exception
'                trans.Rollback()
'                LogDebug.WriteLog("Lỗi khi cập nhật tham số dùng chung " & ex.ToString, EventLogEntryType.Error)
'                Throw ex
'            Finally
'                If Not trans Is Nothing Then
'                    trans.Dispose()
'                End If
'                If Not conn Is Nothing Then
'                    conn.Dispose()
'                End If
'            End Try
'        End Function

'    End Class

'End Namespace