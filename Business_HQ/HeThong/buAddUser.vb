Imports VBOracleLib
Namespace HeThong

    Public Class buAddUser
        Public Shared Function Insert(ByVal objUser As infUser) As Boolean
            Dim objAddUser As New daUser
            Try
                Return objAddUser.Insert(objUser)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function Delete(ByVal objUser As infUser, ByVal strTrangThai As String) As Boolean
            Dim objAdduser As New daUser
            Try
                Return objAdduser.Delete(objUser, strTrangThai)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function DoiTen(ByVal objUser As infUser, Optional ByVal tt_doiCN As Boolean = False) As Boolean
            Dim objAdduser As New daUser
            Try
                Return objAdduser.DoiTen(objUser, tt_doiCN)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DoiCN_EXITS(ByVal objUser As infUser) As Boolean
            Dim objAdduser As New daUser
            Try
                Return objAdduser.DoiCN_EXITS(objUser)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function DoiMK(ByVal objUser As infUser) As Boolean
            Dim objAddUser As New daUser
            Try
                Return objAddUser.DoiMK(objUser)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function DoiNhom(ByVal objUser As infUser) As Boolean
            Dim objAddUser As New daUser
            Try
                Return objAddUser.DoiNhom(objUser)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function UpdateBanlv(ByVal objUser As infUser, ByVal strbanlv As String, ByVal strIP As String) As Boolean
            Dim objAddUser As New daUser
            Try
                Return objAddUser.UpdateBanLV(objUser, strbanlv, strIP)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function SelectNhom() As DataSet
            Dim objAddUser As New daUser
            Try
                Return objAddUser.SelectNhom()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CheckUSer_GD(ByVal strMaNV As String) As String
            Dim objAddUser As New daUser
            Try
                Return objAddUser.CheckUSer_GD(strMaNV)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class

End Namespace