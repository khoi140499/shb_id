﻿Namespace HeThong

    Public Class buNhom
        Public Shared Function Insert(ByVal objNhom As infNhom) As Boolean
            Dim Nhomda As New daNhom
            Dim blnResult As Boolean = True
            Try
                ' Kiểm tra xem Nhóm đã tồn tại hay chưa

                If Nhomda.SelectItem(objNhom).Ma_Nhom = objNhom.Ma_Nhom Then
                    MsgBox("Tên nhóm này đã có rồi. Bạn kiểm tra lại!", MsgBoxStyle.Critical, "Kiểm tra dữ liệu")
                    ' throw 
                    Exit Function
                Else
                    Nhomda.Insert(objNhom)
                End If
            Catch ex As Exception
                blnResult = False
                Throw ex
            End Try
            Return blnResult
        End Function
        Public Shared Function SelectItem(ByVal objNhom As infNhom) As infNhom
            Dim DAO As New daNhom
            Return DAO.SelectItem(objNhom)
        End Function
        Public Shared Function SelectAll() As DataSet
            Dim objNhom As New daNhom
            Try
                Return objNhom.SelectAll()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class

End Namespace