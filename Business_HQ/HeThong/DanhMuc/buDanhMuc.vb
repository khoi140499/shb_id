Imports System.Data.OleDb

Namespace DanhMuc
    Public Class buDanhMuc
        Private strID As String
        Private strDanhMuc As String
        Private objDanhMuc As Object
        Private strStatus As String

        Private strMa_Tinh As String
        Private strMa_Huyen As String

        Sub New()
            strDanhMuc = ""
            objDanhMuc = Nothing
            ' objDanhMuc = New CapChuong.buCapChuong
        End Sub

        Sub New(ByVal strTmpDanhMuc As String)
            strDanhMuc = strTmpDanhMuc
            objDanhMuc = Nothing
            Select Case strDanhMuc
                '        Case "LOAITIEN"
                '            objDanhMuc = New LoaiTien.buLoaiTien
                '        Case "LOAITHUE"
                '            objDanhMuc = New LoaiThue.buLoaiThue
                '        Case "CQTHU"
                '            objDanhMuc = New CQThu.buCQThu
                'Case "CAPCHUONG"
                '    objDanhMuc = New CapChuong.buCapChuong
                '        Case "LOAIKHOAN"
                '            objDanhMuc = New LoaiKhoan.buLoaiKhoan
                '        Case "MUCTMUC"
                '            objDanhMuc = New MucTMuc.buMucTMuc
                '        Case "KHOBAC"
                '            objDanhMuc = New KhoBac.buKhoBac
                '        Case "TAIKHOAN", "DB_TAIKHOAN"
                '            objDanhMuc = New TaiKhoan.buTaiKhoan
                '        Case "NGANHANG", "DB_NGANHANG"
                '            objDanhMuc = New NganHang.buNganHang
                '        Case "NGUYENTE"
                '            objDanhMuc = New NguyenTe.buNguyenTe
                '        Case "TYGIA"
                '            objDanhMuc = New TyGia.buTyGia
                '        Case "DBHC"
                '            objDanhMuc = New DBHC.buDBHC
                '            'objDanhMuc = New DBHC.buDBHC
                '            'Case "TINH"
                '            '    objDanhMuc = New Tinh.buTinh
                '            'Case "HUYEN"
                '            '    objDanhMuc = New Huyen.buHuyen
                '            'Case "XA"
                '            '    objDanhMuc = New Xa.buXa
                '        Case "NNT"
                '            objDanhMuc = New NNThue.buNNThue
                '        Case "CQQD"
                '            objDanhMuc = New CQQD.buCQQD
                '        Case "MAQUY", "DB_MAQUY"
                '            objDanhMuc = New MaQuy.buMaQuy
                Case "DIEMTHU"
                    objDanhMuc = New DiemThu.buDiemthu
                    '        Case Else
                    '            objDanhMuc = Nothing

                    'sonmt
                Case "LOAIHINH"
                    objDanhMuc = New LoaiHinh.buLoaiHinh
                Case "HAIQUAN"
                    objDanhMuc = New HaiQuan.buHaiQuan

            End Select
        End Sub

        'Sub New(ByVal strTmpDanhMuc As String, ByVal strTmpID As String)
        '    Me.New(strTmpDanhMuc)
        '    strID = strTmpID
        'End Sub

        'Sub New(ByVal strTmpDanhMuc As String, ByVal strMaTinh As String, ByVal strMaHuyen As String)
        '    Me.New(strTmpDanhMuc)
        '    strMa_Tinh = strMaTinh
        '    strMa_Huyen = strMaHuyen
        'End Sub

        Public Property DanhMuc() As String
            Get
                Return strDanhMuc
            End Get
            Set(ByVal Value As String)
                strDanhMuc = Value
            End Set
        End Property

        Public Property ID() As String
            Get
                Return strID
            End Get
            Set(ByVal Value As String)
                strID = Value
            End Set
        End Property

        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property

        'Public Function Title() As String
        '    Dim tmpTitle As String = String.Empty
        '    Select Case strDanhMuc
        '        Case "LOAITIEN"
        '            tmpTitle = "  Danh mục loại tiền"
        '        Case "LOAITHUE"
        '            tmpTitle = "  Danh mục loại thuế"
        '        Case "CQTHU"
        '            tmpTitle = "  Danh mục cơ quan thu"
        '        Case "CQTHUE"
        '            tmpTitle = "  Danh mục cơ quan thuế"
        '        Case "CAPCHUONG"
        '            tmpTitle = "  Danh mục mã chương"
        '        Case "LOAIKHOAN"
        '            tmpTitle = "  Danh mục mã ngành kinh tế"
        '        Case "MUCTMUC"
        '            tmpTitle = "  Danh mục nội dung kinh tế"
        '        Case "KHOBAC"
        '            tmpTitle = "  Danh mục kho bạc"
        '        Case "TAIKHOAN"
        '            tmpTitle = "  Danh mục tài khoản kế toán"
        '        Case "DB_TAIKHOAN"
        '            tmpTitle = "  Nhận danh mục tài khoản kế toán từ KTKB"
        '        Case "NGANHANG"
        '            tmpTitle = "  Danh mục ngân hàng"
        '        Case "DB_NGANHANG"
        '            tmpTitle = "  Nhận danh mục ngân hàng từ KTKB"
        '        Case "NGUYENTE"
        '            tmpTitle = "  Danh mục nguyên tệ"
        '        Case "TYGIA"
        '            tmpTitle = "  Danh mục tỷ giá"
        '        Case "DBHC"
        '            tmpTitle = "  Danh mục địa bàn"
        '        Case "TINH"
        '            tmpTitle = "  Danh mục tỉnh"
        '        Case "HUYEN"
        '            tmpTitle = "  Danh mục huyện"
        '        Case "XA"
        '            tmpTitle = "  Danh mục xã"
        '        Case "NNT"
        '            tmpTitle = "  Danh mục người nộp thuế"
        '        Case "CQQD"
        '            tmpTitle = "  Danh mục cơ quan quyết định"
        '        Case "DIEMTHU"
        '            tmpTitle = "  Danh mục điểm thu"
        '        Case "MAQUY"
        '            tmpTitle = "  Danh mục mã quỹ"
        '        Case "DB_MAQUY"
        '            tmpTitle = "  Nhận danh mục mã quỹ từ KTKB"
        '        Case Else
        '            tmpTitle = "  Danh mục không xác định"
        '    End Select

        '    Select Case strStatus
        '        Case "ADD"
        '            tmpTitle = tmpTitle.Replace("Danh mục", "Thêm mới")
        '        Case "EDIT"
        '            tmpTitle = tmpTitle.Replace("Danh mục", "Sửa thông tin")
        '    End Select
        '    Return tmpTitle
        'End Function

        'Public Sub FormatFlexHeader(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    If objDanhMuc Is Nothing Then Exit Sub
        '    objDanhMuc.FormatFlexHeader(flxDanhMuc)
        'End Sub

        'Public Sub FormatFlexHeaderKTKB(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    If objDanhMuc Is Nothing Then Exit Sub
        '    objDanhMuc.FormatFlexHeaderKTKB(flxDanhMuc)
        '    flxDanhMuc.Cols(flxDanhMuc.Cols.Count - 1).DataType = GetType(System.Boolean)

        '    Dim intTmp As Integer
        '    For intTmp = 0 To flxDanhMuc.Cols.Count - 2
        '        flxDanhMuc.Cols(intTmp).AllowEditing = False
        '    Next
        '    flxDanhMuc.Cols(flxDanhMuc.Cols.Count - 1).AllowEditing = True
        'End Sub

        'Public Sub FormatFrm(ByRef frm As Windows.Forms.Form)
        '    If objDanhMuc Is Nothing Then Exit Sub
        '    objDanhMuc.FormatFrm(frm)
        'End Sub

        Public Function GetDataSet(ByVal WhereClause As String) As DataSet
            Dim ds As DataSet
            'objDanhMuc = New CapChuong.buCapChuong()
            Try
                ds = objDanhMuc.GetDataSet(WhereClause)
            Catch ex As Exception

            End Try
            Return ds
        End Function

        'Public Sub LoadKTKB(ByRef flxDanhMuc As Object, ByVal strDieuKien As String)
        '    objDanhMuc.loadKTKB(flxDanhMuc, strDieuKien)
        'End Sub

        'Public Sub UpdateFlex(ByRef flxDanhMuc As Object)
        '    objDanhMuc.UpdateFlex(flxDanhMuc)
        'End Sub

        'Public Sub UpdateFlex(ByRef flxDanhMuc As Object, ByVal obj As Object)
        '    objDanhMuc.UpdateFlex(flxDanhMuc, obj)
        'End Sub

        'Public Function Save(ByVal frm As Windows.Forms.GroupBox) As Boolean
        '    If strStatus = "ADD" Then
        '        Return objDanhMuc.Insert(frm)
        '    ElseIf strStatus = "EDIT" Then
        '        Return objDanhMuc.Update(frm)
        '    Else
        '        Return False
        '    End If
        'End Function

        'Public Function Delete(ByVal strTmpMa As String) As Boolean
        '    Return objDanhMuc.Delete(strTmpMa)
        'End Function

        'Public Function DongBoKTKB(ByVal strID As String, ByVal blnGhiDe As Boolean) As Integer
        '    Return objDanhMuc.DongBoKTKB(strID, blnGhiDe)
        'End Function

        'Public Function Add(ByVal obj As Object) As Object
        '    Return objDanhMuc.Add(obj)
        'End Function

        'Public Function Edit(ByVal obj As Object, ByVal lstDMDBHC As Object) As Boolean
        '    Return objDanhMuc.Edit(obj, lstDMDBHC)
        'End Function

        'Public Shared Function Num2Date(ByVal strDate As String) As Date
        '    Try
        '        Return New Date(Int(strDate.Substring(0, 4)), Int(strDate.Substring(4, 2)), Int(strDate.Substring(6, 2)))
        '    Catch ex As Exception
        '        Return Now()
        '    End Try
        'End Function

        'Public Function Add() As Object
        '    Return objDanhMuc.Add()
        'End Function

        'Public Function Edit() As Boolean
        '    Return objDanhMuc.Edit(strID)
        'End Function

        'Public Function GetReport() As String
        '    Return objDanhMuc.GetReport()
        'End Function

    End Class
End Namespace
