Imports VBOracleLib
Imports System.Data.OracleClient
Imports Common.mdlCommon
Imports System.Text
Imports System.Data




Namespace DanhMuc.HaiQuan
    Public Class daHaiQuan
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objHaiQuan As infHaiQuan) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As OracleConnection = Nothing
            Dim transCT As IDbTransaction = Nothing
            Try
                conn = DataAccess.GetConnection()
                transCT = conn.BeginTransaction()

                'insert TCS_DM_HQ
                Dim sbsql As StringBuilder = New StringBuilder()

                sbsql.Append("INSERT INTO TCS_DM_HQ (MA_HQ, ")
                sbsql.Append("MA_CU, ")
                sbsql.Append("MA_QHNS, ")
                sbsql.Append("TEN )")

                sbsql.Append(" VALUES(:MA_HQ, ")
                sbsql.Append(":MA_CU, ")
                sbsql.Append(":MA_QHNS, ")
                sbsql.Append(":TEN) ")

                Dim p(3) As IDbDataParameter
                p(0) = New OracleParameter()
                p(0).ParameterName = "MA_HQ"
                p(0).DbType = DbType.String
                p(0).Direction = ParameterDirection.Input
                p(0).Value = DBNull.Value
                p(0).Value = objHaiQuan.MA_HQ

                p(1) = New OracleParameter()
                p(1).ParameterName = "MA_CU"
                p(1).DbType = DbType.String
                p(1).Direction = ParameterDirection.Input
                p(1).Value = DBNull.Value
                p(1).Value = objHaiQuan.MA_CU

                p(2) = New OracleParameter()
                p(2).ParameterName = "MA_QHNS"
                p(2).DbType = DbType.String
                p(2).Direction = ParameterDirection.Input
                p(2).Value = DBNull.Value
                p(2).Value = objHaiQuan.MA_QHNS

                p(3) = New OracleParameter()
                p(3).ParameterName = "TEN"
                p(3).DbType = DbType.String
                p(3).Direction = ParameterDirection.Input
                p(3).Value = DBNull.Value
                p(3).Value = objHaiQuan.TEN
                DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT)
                transCT.Commit()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới DM - Hải quan")
                blnResult = False
                Throw ex
            Finally
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Or conn.State <> ConnectionState.Closed Then
                    conn.Dispose()
                    conn.Close()
                End If
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objHaiQuan As infHaiQuan) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As OracleConnection = Nothing
            Dim transCT As IDbTransaction = Nothing
            Try
                conn = DataAccess.GetConnection()
                transCT = conn.BeginTransaction()
                'update tcs_dm_hq
                Dim sbsql As StringBuilder = New StringBuilder()
                sbsql.Append("UPDATE TCS_DM_HQ SET MA_CU = :MA_CU, MA_QHNS = :MA_QHNS, TEN = :TEN")
                sbsql.Append(" WHERE MA_HQ = :MA_HQ ")
                Dim p(3) As IDbDataParameter
                p(0) = New OracleParameter()
                p(0).ParameterName = "MA_CU"
                p(0).DbType = DbType.String
                p(0).Direction = ParameterDirection.Input
                p(0).Value = objHaiQuan.MA_CU

                p(1) = New OracleParameter()
                p(1).ParameterName = "MA_QHNS"
                p(1).DbType = DbType.String
                p(1).Direction = ParameterDirection.Input
                p(1).Value = objHaiQuan.MA_QHNS

                p(2) = New OracleParameter()
                p(2).ParameterName = "TEN"
                p(2).DbType = DbType.String
                p(2).Direction = ParameterDirection.Input
                p(2).Value = objHaiQuan.TEN

                p(3) = New OracleParameter()
                p(3).ParameterName = "MA_HQ"
                p(3).DbType = DbType.String
                p(3).Direction = ParameterDirection.Input
                p(3).Value = objHaiQuan.MA_HQ

                DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT)
                transCT.Commit()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật DM - Hải quan")
                blnResult = False
                Throw ex
            Finally
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Or conn.State <> ConnectionState.Closed Then
                    conn.Dispose()
                    conn.Close()
                End If
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objHaiQuan As infHaiQuan) As Boolean

            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_HQ " & _
                    "WHERE MA_HQ='" & objHaiQuan.MA_HQ & "'"
            Try

                DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa DM - Hải quan")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá cơ mục lục cấp - chương: " & ex.ToString)
                blnResult = False
            Finally

            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strMA_HQ As String) As infHaiQuan
            Dim objHaiQuan As New infHaiQuan
           
            Dim strSQL As String = "Select * From TCS_DM_HQ " & _
                        "Where MA_HQ='" & strMA_HQ & "'"
            Try

                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            objHaiQuan = New infHaiQuan
                            objHaiQuan.MA_HQ = strMA_HQ
                            objHaiQuan.MA_CU = ds.Tables(0).Rows(0)("MA_CU").ToString()
                            objHaiQuan.MA_QHNS = ds.Tables(0).Rows(0)("MA_QHNS").ToString()
                            objHaiQuan.TEN = ds.Tables(0).Rows(0)("TEN").ToString()
                        End If
                    End If
                  
                End If
                Return objHaiQuan
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin DM - Hải quan")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
               
            End Try
            Return objHaiQuan
        End Function
        Public Function Load(ByVal tmpHaiQuan As infHaiQuan) As infHaiQuan
            Dim objHaiQuan As New infHaiQuan
            Dim cnHaiQuan As DataAccess
            Dim drHaiQuan As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc
            Dim strSQL As String = "Select * From TCS_DM_HQ " & _
                "Where " & IIf(tmpHaiQuan.MA_HQ <> "", "MA_HQ = '" & tmpHaiQuan.MA_HQ & "'", "")

            Try
                cnHaiQuan = New DataAccess
                drHaiQuan = cnHaiQuan.ExecuteDataReader(strSQL, CommandType.Text)
                If drHaiQuan.Read Then
                    objHaiQuan = New infHaiQuan
                    objHaiQuan.MA_HQ = drHaiQuan("MA_HQ").ToString()
                    objHaiQuan.MA_CU = drHaiQuan("MA_CU").ToString()
                    objHaiQuan.MA_QHNS = drHaiQuan("MA_QHNS").ToString()
                    objHaiQuan.TEN = drHaiQuan("TEN").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin DM - Loai Hinh")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drHaiQuan Is Nothing Then drHaiQuan.Close()
                If Not cnHaiQuan Is Nothing Then cnHaiQuan.Dispose()
            End Try
            Return objHaiQuan
        End Function
        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String

            Dim dsHaiQuan As DataSet

            Try
                strSQL = " SELECT * " & _
                         " FROM TCS_DM_HQ " & _
                         " WHERE 1 = 1 " & WhereClause & _
                         " ORDER BY MA_HQ "

                dsHaiQuan = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin DM - Loại hình")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
            End Try

            Return dsHaiQuan
        End Function
        'Public Function CheckDelete(ByVal strCCH_ID As String) As Object
        '    Dim cnKhoBac As DataAccess
        '    Dim dsKhoBac As DataSet

        '    Try
        '        Dim strSQL As String = "select ma_chuong from (select ma_chuong from tcs_ctu_dtl union all select ma_chuong from tcs_baolanh_dtl) " & _
        '                "Where ma_chuong=" & strCCH_ID

        '        cnKhoBac = New DataAccess
        '        dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception
        '        CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
        '        'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
        '        Throw ex
        '    Finally
        '        If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
        '    End Try

        '    Return dsKhoBac
        'End Function

        Public Function CheckExist(ByVal objHaiQuan As infHaiQuan) As Boolean
            Return Load(objHaiQuan).MA_HQ <> ""
        End Function

        Public Function CheckExist(ByVal strMa_HQ As String) As Boolean
            Return Load(strMa_HQ).MA_HQ <> ""
        End Function
    End Class

End Namespace
