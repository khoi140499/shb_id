﻿Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.DanhMuc.NgayNghi
Imports System.Data
Namespace DanhMuc.NgayNghi
    Public Class daNgayNghi
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        '-------------------
        ' Lop thuc hien thao tac ngay nghi
        '-------------------
        Public Function Save(ByVal sql)
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                conn.ExecuteNonQuery(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi khi thêm mới ngày nghỉ")
                'LogDebug.Writelog("Lỗi khi thực hiện ghi mới điểm thu :" & ex.ToString)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function ExitsNgayNghi(ByVal sqlngaynghi As String) As Byte
            Dim conn As DataAccess
            Dim rd As IDataReader
            Dim byr As Byte = 0
            Try

                conn = New DataAccess
                rd = conn.ExecuteDataReader(sqlngaynghi, CommandType.Text)
                If rd.Read Then
                    byr = 1
                    Return byr
                End If
                rd.Close()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi kiểm tra sự tồn tại của ngày nghỉ")
                'LogDebug.Writelog("Lỗi khi thực hiện kiem tra tồn tại mã điểm thu :" & ex.ToString)
                Throw ex
            Finally

                If Not rd Is Nothing Then
                    rd.Close()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function loadDetail(ByVal sql) As DataSet
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin ngày nghỉ")
                'LogDebug.Writelog("Lỗi khi thực hiện ghi mới điểm thu :" & ex.ToString)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try

        End Function
        Public Function DsNgayNghi(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim sql As String = "SELECT ngay_nghi, ghi_chu " & _
                                    "    FROM tcs_dm_NgayNghi " & _
                                    "   WHERE 1 = 1 " & WhereClause & _
                                    "ORDER BY ngay_nghi desc"
                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin ngày nghỉ")
                'LogDebug.Writelog("Lỗi khi thực hiện Get danh sách mã điểm thu :" & ex.ToString)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try
        End Function
        Public Function DelNgaynghi(ByVal ngay_nghi As String) As Boolean
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim sql As String = "Delete from TCS_DM_NgayNghi where ngay_nghi=to_date('" & ngay_nghi & "','MM/dd/yyyy')"
                conn.ExecuteNonQuery(sql, CommandType.Text)
                Return True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi xóa ngày nghỉ")
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function UpdateNgayNghi(ByVal objNgayNghi As infNgayNghi)
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim sql As String = "Update TCS_DM_NgayNghi Set ngay_nghi=to_date('" & objNgayNghi.NgayNghi & "','DD/MM/RRRR'),ghi_chu='" & objNgayNghi.GhiChu & "' where ngay_nghi=to_date('" & objNgayNghi.NgayNghi & "','dd/MM/yyyy')"
                conn.ExecuteNonQuery(sql, CommandType.Text)
                Return True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi cập nhật ngày nghỉ")
                'LogDebug.Writelog("Lỗi khi thực hiện Get danh sách mã điểm thu :" & ex.ToString)
                Return False
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        Public Function Load(ByVal strngay_nghi As String) As infNgayNghi
            Dim objNgayNghi As New infNgayNghi
            Dim cnNgayNghi As DataAccess
            Dim drNgayNghi As IDataReader
            Dim strSQL As String = "Select ngay_nghi, ghi_chu From TCS_DM_NgayNghi " & _
                        "Where ngay_nghi='" & strngay_nghi & "'"
            Try
                cnNgayNghi = New DataAccess
                drNgayNghi = cnNgayNghi.ExecuteDataReader(strSQL, CommandType.Text)
                If drNgayNghi.Read Then
                    objNgayNghi = New infNgayNghi
                    objNgayNghi.NgayNghi = strngay_nghi
                    objNgayNghi.GhiChu = drNgayNghi("ghi_chu").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) 'CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin ngày nghỉ")
                'LogDebug.Writelog("Có lỗi sảy ra khi lấy thông tin loại thuế từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drNgayNghi Is Nothing Then drNgayNghi.Close()
                If Not cnNgayNghi Is Nothing Then cnNgayNghi.Dispose()
            End Try

            Return objNgayNghi
        End Function
    End Class
End Namespace
