﻿Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace DanhMuc.CNNganHang
    Public Class daCNNganHang
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnNganHang As DataAccess
            Dim dsNganHang As DataSet

            Try
                strSQL = "SELECT a.id, a.name, a.branch_code, a.tinh_trang, a.phan_cap,a.ma_xa, " & _
                "case when  a.tinh_trang= 1 then 'hoat dong' else ' ngung hoat dong' end tinhtrang, " & _
                "decode (phan_cap,'1','HSC','2','CN','0','PGD') phancap" & _
                "    FROM tcs_dm_chinhanh a " & _
                "   WHERE 1 = 1 " & WhereClause & _
                "ORDER BY id "

                cnNganHang = New DataAccess
                dsNganHang = cnNganHang.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Chi nhánh ngân hàng")
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try

            Return dsNganHang
        End Function

        'Public Function Load(ByVal objCNNganHang As infCNNganHang) As Object
        '    Dim strSQL As String
        '    Dim cnNganHang As DataAccess
        '    Dim dsNganHang As DataSet

        '    Try
        '        strSQL = "SELECT  id, ma_xa, ma,  ten,  shkb " & _
        '                 "    FROM tcs_dm_chinhanh " & _
        '                 "   WHERE 1 = 1 " & _
        '                 "ORDER BY ma_xa "

        '        cnNganHang = New DataAccess
        '        dsNganHang = cnNganHang.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception
        '    Finally
        '        If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
        '    End Try

        '    Return dsNganHang
        'End Function

        'Public Function Load(ByVal strMA_XA As String, ByVal strMA_NH As String) As infCNNganHang
        '    Dim objCNNganHang As New infCNNganHang
        '    Dim cnNganHang As DataAccess
        '    Dim drNganHang As IDataReader
        '    Dim strSQL As String = ""
        '    Try
        '        strSQL = "SELECT id,ma_xa, ma, ten, shkb " & _
        '                 "  FROM tcs_dm_chinhanh " & _
        '                 " WHERE ma_xa = '" + strMA_XA + "' " & _
        '                 "   AND ma = '" + strMA_NH + "' "

        '        cnNganHang = New DataAccess
        '        drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drNganHang.Read Then
        '            objCNNganHang = New infCNNganHang
        '            objCNNganHang.MA_XA = strMA_XA
        '            objCNNganHang.MA_NH = drNganHang("ma").ToString()
        '            objCNNganHang.TEN_NH = drNganHang("ten").ToString()
        '            objCNNganHang.SHKB = drNganHang("shkb").ToString()
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If Not drNganHang Is Nothing Then drNganHang.Close()
        '        If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
        '    End Try
        '    Return objCNNganHang
        'End Function
        Public Function LoadID(ByVal strID As String) As infCNNganHang
            Dim objCNNganHang As New infCNNganHang
            Dim cnNganHang As DataAccess
            Dim drNganHang As IDataReader
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT a.id, a.name, a.branch_id, a.tinh_trang, a.phan_cap " & _
                         "  FROM tcs_dm_chinhanh a" & _
                         " WHERE a.id = '" + strID + "' "

                cnNganHang = New DataAccess
                drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
                If drNganHang.Read Then
                    objCNNganHang = New infCNNganHang
                    objCNNganHang.MA_PGD = drNganHang("id").ToString()
                    objCNNganHang.TEN_PGD = drNganHang("name").ToString()
                    objCNNganHang.MA_CN = drNganHang("branch_id").ToString()
                    objCNNganHang.TINH_TRANG = drNganHang("tinh_trang").ToString()
                    objCNNganHang.PHAN_CAP = drNganHang("phan_cap").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Chi nhánh ngân hàng theo ID")
                Throw ex
            Finally
                If Not drNganHang Is Nothing Then drNganHang.Close()
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return objCNNganHang
        End Function

        Public Function Insert(ByVal objCNNganHang As infCNNganHang) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO tcs_dm_chinhanh " & _
                                    "            (id,name,branch_id,tinh_trang,phan_cap,ma_xa) " & _
                                    "     VALUES ('" + objCNNganHang.MA_PGD + "', '" + objCNNganHang.TEN_PGD + "', " & _
                                    "             '" + objCNNganHang.MA_CN + "', '" + objCNNganHang.TINH_TRANG + "', '" + objCNNganHang.PHAN_CAP + "', '" + objCNNganHang.MA_XA + "') "

            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi thêm mới Chi nhánh ngân hàng")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objCNNganHang As infCNNganHang) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE tcs_dm_chinhanh " & _
                                   "   SET name = '" + objCNNganHang.TEN_PGD + "', " & _
                                    "       branch_id = '" + objCNNganHang.MA_CN + "', " & _
                                   "       tinh_trang = '" + objCNNganHang.TINH_TRANG + "', " & _
                                   "       phan_cap = '" + objCNNganHang.PHAN_CAP + "', " & _
                                   "       ma_xa = '" + objCNNganHang.MA_XA + "' " & _
                                   " WHERE id = '" + objCNNganHang.MA_PGD + "' "
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi cập nhật Chi nhánh ngân hàng")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal strMa_PGD As String) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM tcs_dm_chinhanh " & _
                                    "      WHERE id = '" + strMa_PGD + "' "
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá danh mục loại - khoản: " & ex.ToString)
                blnResult = False
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi xóa Chi nhánh ngân hàng")
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function CheckExist(ByVal objCNNganHang As infCNNganHang) As Boolean
            Return LoadID(objCNNganHang.MA_PGD).MA_PGD <> ""
        End Function

    End Class
End Namespace
