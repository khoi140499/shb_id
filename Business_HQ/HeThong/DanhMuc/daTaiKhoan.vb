Imports VBOracleLib

Namespace DanhMuc.TaiKhoan
    Public Class daTaiKhoan
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Dim cnTaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO TCS_DM_TAIKHOAN (TK, TEN_TK, " & IIf(objTaiKhoan.MA_DBHC = "", "", "DBHC, ") & _
                                    IIf(objTaiKhoan.MA_CQTHU = "", "", "MA_CQThu, ") & "TINH_TRANG,SHKB,TK_KB_NH) " & _
                                    "VALUES ('" & objTaiKhoan.TK & "','" & _
                                    objTaiKhoan.TEN_TK & "'," & _
                                    IIf(objTaiKhoan.MA_DBHC = "", "", "'" & objTaiKhoan.MA_DBHC & "',") & _
                                    IIf(objTaiKhoan.MA_CQTHU = "", "", "'" & objTaiKhoan.MA_CQTHU & "',") & _
                                    "'" & objTaiKhoan.TINH_TRANG & "','" & objTaiKhoan.SHKB & "','" & objTaiKhoan.TK_KB_NH & "')"

            Try
                cnTaiKhoan = New DataAccess
                cnTaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi thêm mới tài khoản")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Dim cnTaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_TAIKHOAN SET TEN_TK='" & objTaiKhoan.TEN_TK & _
                                   "',DBHC=" & IIf(objTaiKhoan.MA_DBHC = "", "NULL", "'" & objTaiKhoan.MA_DBHC & "'") & _
                                   ",MA_CQTHU=" & IIf(objTaiKhoan.MA_CQTHU = "", "NULL", "'" & objTaiKhoan.MA_CQTHU & "'") & _
                                   ",TINH_TRANG='" & objTaiKhoan.TINH_TRANG & "',SHKB='" & objTaiKhoan.SHKB & "',TK_KB_NH='" & objTaiKhoan.TK_KB_NH & "'" & _
                                   " WHERE TK='" & objTaiKhoan.TK & "'"

            Try
                cnTaiKhoan = New DataAccess
                cnTaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi cập nhật tài khoản")
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Dim cnTaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_TAIKHOAN " & _
                           "WHERE " & "TK='" & _
                           objTaiKhoan.TK & "'"
            Try
                cnTaiKhoan = New DataAccess
                cnTaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi khi xóa tài khoản")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá tài khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strTK As String) As infTaiKhoan
            Dim objTaiKhoan As New infTaiKhoan
            Dim cnTaiKhoan As DataAccess
            Dim drTaiKhoan As IDataReader
            Dim strSQL As String = "Select TK, TEN_TK,DBHC, MA_CQTHU, TINH_TRANG From TCS_DM_TAIKHOAN " & _
                        "Where TK='" & strTK & "'"
            Try
                cnTaiKhoan = New DataAccess
                drTaiKhoan = cnTaiKhoan.ExecuteDataReader(strSQL, CommandType.Text)
                If drTaiKhoan.Read Then
                    objTaiKhoan = New infTaiKhoan
                    objTaiKhoan.TK = strTK
                    objTaiKhoan.TEN_TK = drTaiKhoan("TEN_TK").ToString()
                    objTaiKhoan.MA_DBHC = drTaiKhoan("DBHC").ToString()
                    objTaiKhoan.MA_CQTHU = drTaiKhoan("MA_CQTHU").ToString()
                    objTaiKhoan.TINH_TRANG = drTaiKhoan("TINH_TRANG").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin tài khoản")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drTaiKhoan Is Nothing Then drTaiKhoan.Close()
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return objTaiKhoan
        End Function
        Public Function CheckExistTK(ByVal inf As infTaiKhoan) As Boolean
            Dim objTaiKhoan As New infTaiKhoan
            Dim cnTaiKhoan As DataAccess
            Dim drTaiKhoan As IDataReader
            Dim returnValue As Boolean = True
            Dim strSQL As String = "Select TK, TEN_TK,DBHC, MA_CQTHU, TINH_TRANG From TCS_DM_TAIKHOAN " & _
                        "Where TK='" & inf.TK & "' and MA_CQTHU='" & inf.MA_CQTHU & "' and DBHC='" & inf.MA_DBHC & "' AND SHKB='" & inf.SHKB & "'"
            Try
                cnTaiKhoan = New DataAccess
                drTaiKhoan = cnTaiKhoan.ExecuteDataReader(strSQL, CommandType.Text)
                If drTaiKhoan.Read Then
                    returnValue = True
                Else
                    returnValue = False
                End If
            Catch ex As Exception
                returnValue = False
                Throw ex
            Finally
                If Not drTaiKhoan Is Nothing Then drTaiKhoan.Close()
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return returnValue
        End Function
        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As Object
            Dim strSQL As String
            Dim cnTaiKhoan As DataAccess
            Dim dsTaiKhoan As DataSet
            '*****TYNK - Thêm tk_kb_nh và SHKB vào câu lệnh sql *********25/02/2013
            Try
                strSQL = "SELECT   tk, (TO_CHAR (tk)) tk_view, ten_tk, dbhc, ma_cqthu, " & _
                         "         DECODE (tcs_dm_taikhoan.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngung hoat dong', " & _
                         "                 1, 'Dang hoat dong' " & _
                         "                ) tinh_trang, " & _
                         "         DECODE (tcs_dm_taikhoan.tk_kb_nh, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Co', " & _
                         "                 1, 'No' " & _
                         "                ) tk_kb_nh ," & _
                         "                tcs_dm_khobac.Ten ten" & _
                         "    FROM tcs_dm_taikhoan inner join tcs_dm_khobac on tcs_dm_taikhoan.SHKB=tcs_dm_khobac.SHKB" & _
                         "  WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY tk ASC "
                '****************************************************

                cnTaiKhoan = New DataAccess
                dsTaiKhoan = cnTaiKhoan.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin tài khoản")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try

            Return dsTaiKhoan
        End Function

        Public Function CheckExist(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Return Load(objTaiKhoan.TK).TK <> ""
        End Function

        Public Shared Function Check2Del(ByVal objTaiKhoan As infTaiKhoan) As Byte
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tài khoản có thể xoá không
            ' Tham số: objTaiKhoan: tài khoản cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 26/10/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------
            Dim cnTaiKhoan As DataAccess
            Dim drTaiKhoan As IDataReader
            Dim strSQL As String
            Dim bytResult As Byte = 0

            Try
                strSQL = "SELECT SHKB FROM TCS_CTU_HDR WHERE TK_CO = '" & objTaiKhoan.TK & "' OR TK_NO = '" & objTaiKhoan.TK & "'"
                cnTaiKhoan = New DataAccess
                drTaiKhoan = cnTaiKhoan.ExecuteDataReader(strSQL, CommandType.Text)
                If drTaiKhoan.Read Then bytResult = 1
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi check thông tin tài khoản")
                'LogDebug.Writelog("Có lỗi xảy ra khi kiểm tra quan hệ giữa tài khoản với chứng từ: " & ex.ToString)
                Throw ex
            Finally
                If Not drTaiKhoan Is Nothing Then drTaiKhoan.Close()
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try

            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function

    End Class

End Namespace
