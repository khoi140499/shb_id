Imports VBOracleLib
Imports System.Data.OracleClient
Imports Common.mdlCommon
Imports System.Text
Imports System.Data




Namespace DanhMuc.LoaiHinh
    Public Class daLoaiHinh
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Function Insert(ByVal objLoaiHinh As infLoaiHinh) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As OracleConnection = Nothing
            Dim transCT As IDbTransaction = Nothing
            Try
                conn = DataAccess.GetConnection()
                transCT = conn.BeginTransaction()

                'insert TCS_DM_LHINH
                Dim sbsql As StringBuilder = New StringBuilder()

                sbsql.Append("INSERT INTO TCS_DM_LHINH (MA_LH, ")
                sbsql.Append("TEN_LH, ")
                sbsql.Append("TEN_VT, ")
                sbsql.Append("NGAY_ANHAN )")

                sbsql.Append("VALUES( :MA_LH, ")
                sbsql.Append(":TEN_LH, ")
                sbsql.Append(":TEN_VT, ")
                sbsql.Append(":NGAY_ANHAN) ")

                Dim p(3) As IDbDataParameter
                p(0) = New OracleParameter()
                p(0).ParameterName = "MA_LH"
                p(0).DbType = DbType.String
                p(0).Direction = ParameterDirection.Input
                p(0).Value = DBNull.Value
                p(0).Value = objLoaiHinh.MA_LH

                p(1) = New OracleParameter()
                p(1).ParameterName = "TEN_LH"
                p(1).DbType = DbType.String
                p(1).Direction = ParameterDirection.Input
                p(1).Value = DBNull.Value
                p(1).Value = objLoaiHinh.TEN_LH

                p(2) = New OracleParameter()
                p(2).ParameterName = "TEN_VT"
                p(2).DbType = DbType.String
                p(2).Direction = ParameterDirection.Input
                p(2).Value = DBNull.Value
                p(2).Value = objLoaiHinh.TEN_VT

                p(3) = New OracleParameter()
                p(3).ParameterName = "NGAY_ANHAN"
                p(3).DbType = DbType.String
                p(3).Direction = ParameterDirection.Input
                p(3).Value = DBNull.Value
                p(3).Value = objLoaiHinh.NGAY_ANHAN
                DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT)
                transCT.Commit()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới DM - Loại hình")
                blnResult = False
                Throw ex
            Finally
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Or conn.State <> ConnectionState.Closed Then
                    conn.Dispose()
                    conn.Close()
                End If
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objLoaiHinh As infLoaiHinh) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As OracleConnection = Nothing
            Dim transCT As IDbTransaction = Nothing
            Try
                conn = DataAccess.GetConnection()
                transCT = conn.BeginTransaction()
                'update tcs_dm_lhinh
                Dim sbsql As StringBuilder = New StringBuilder()
                sbsql.Append("UPDATE TCS_DM_LHINH SET TEN_LH = :TEN_LH, TEN_VT = :TEN_VT, NGAY_ANHAN = :NGAY_ANHAN")
                sbsql.Append(" WHERE MA_LH = :MA_LH ")
                Dim p(3) As IDbDataParameter
                p(0) = New OracleParameter()
                p(0).ParameterName = "TEN_LH"
                p(0).DbType = DbType.String
                p(0).Direction = ParameterDirection.Input
                p(0).Value = objLoaiHinh.TEN_LH

                p(1) = New OracleParameter()
                p(1).ParameterName = "TEN_VT"
                p(1).DbType = DbType.String
                p(1).Direction = ParameterDirection.Input
                p(1).Value = objLoaiHinh.TEN_VT

                p(2) = New OracleParameter()
                p(2).ParameterName = "NGAY_ANHAN"
                p(2).DbType = DbType.String
                p(2).Direction = ParameterDirection.Input
                p(2).Value = objLoaiHinh.NGAY_ANHAN

                p(3) = New OracleParameter()
                p(3).ParameterName = "MA_LH"
                p(3).DbType = DbType.String
                p(3).Direction = ParameterDirection.Input
                p(3).Value = objLoaiHinh.MA_LH
                
                DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT)
                transCT.Commit()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật DM - Loại hình")
                blnResult = False
                Throw ex
            Finally
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Or conn.State <> ConnectionState.Closed Then
                    conn.Dispose()
                    conn.Close()
                End If
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objLoaiHinh As infLoaiHinh) As Boolean
            Dim cnLoaiHinh As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_LHINH " & _
                    "WHERE MA_LH='" & objLoaiHinh.MA_LH & "'"
            Try
                cnLoaiHinh = New DataAccess
                cnLoaiHinh.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa DM - Loại hình")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá cơ mục lục cấp - chương: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnLoaiHinh Is Nothing Then cnLoaiHinh.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strMA_LH As String) As infLoaiHinh
            Dim objLoaiHinh As New infLoaiHinh
            Dim cnLoaiHinh As DataAccess
            Dim drLoaiHinh As IDataReader
            Dim strSQL As String = "Select * From TCS_DM_LHINH " & _
                        "Where MA_LH='" & strMA_LH & "'"
            Try
                cnLoaiHinh = New DataAccess
                drLoaiHinh = cnLoaiHinh.ExecuteDataReader(strSQL, CommandType.Text)
                If drLoaiHinh.Read Then
                    objLoaiHinh = New infLoaiHinh
                    objLoaiHinh.MA_LH = strMA_LH
                    objLoaiHinh.TEN_LH = drLoaiHinh("TEN_LH").ToString()
                    objLoaiHinh.TEN_VT = drLoaiHinh("TEN_VT").ToString()
                    objLoaiHinh.NGAY_ANHAN = drLoaiHinh("NGAY_ANHAN").ToString()
                End If
                Return objLoaiHinh
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '     CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin DM - Loại hình")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drLoaiHinh Is Nothing Then drLoaiHinh.Close()
                If Not cnLoaiHinh Is Nothing Then cnLoaiHinh.Dispose()
            End Try
            Return objLoaiHinh
        End Function
        Public Function Load(ByVal tmpLoaiHinh As infLoaiHinh) As infLoaiHinh
            Dim objLoaiHinh As New infLoaiHinh
            Dim cnLoaiHinh As DataAccess
            Dim drLoaiHinh As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc
            Dim strSQL As String = "Select * From TCS_DM_LHINH " & _
                "Where " & IIf(tmpLoaiHinh.MA_LH <> "", "MA_LH = '" & tmpLoaiHinh.MA_LH & "'", "")

            Try
                cnLoaiHinh = New DataAccess
                drLoaiHinh = cnLoaiHinh.ExecuteDataReader(strSQL, CommandType.Text)
                If drLoaiHinh.Read Then
                    objLoaiHinh = New infLoaiHinh
                    objLoaiHinh.MA_LH = drLoaiHinh("MA_LH").ToString()
                    objLoaiHinh.TEN_LH = drLoaiHinh("TEN_LH").ToString()
                    objLoaiHinh.TEN_VT = drLoaiHinh("TEN_VT").ToString()
                    objLoaiHinh.NGAY_ANHAN = drLoaiHinh("NGAY_ANHAN").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin DM - Loai Hinh")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drLoaiHinh Is Nothing Then drLoaiHinh.Close()
                If Not cnLoaiHinh Is Nothing Then cnLoaiHinh.Dispose()
            End Try
            Return objLoaiHinh
        End Function
        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String
            Dim cnLoaiHinh As DataAccess
            Dim dsLoaiHinh As DataSet

            Try
                strSQL = " SELECT * " & _
                         " FROM TCS_DM_LHINH " & _
                         " WHERE 1 = 1 " & WhereClause & _
                         " ORDER BY MA_LH "
                cnLoaiHinh = New DataAccess
                dsLoaiHinh = cnLoaiHinh.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin DM - Loại hình")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnLoaiHinh.Dispose()
            End Try

            Return dsLoaiHinh
        End Function
        'Public Function CheckDelete(ByVal strCCH_ID As String) As Object
        '    Dim cnKhoBac As DataAccess
        '    Dim dsKhoBac As DataSet

        '    Try
        '        Dim strSQL As String = "select ma_chuong from (select ma_chuong from tcs_ctu_dtl union all select ma_chuong from tcs_baolanh_dtl) " & _
        '                "Where ma_chuong=" & strCCH_ID

        '        cnKhoBac = New DataAccess
        '        dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception
        '        CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
        '        'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
        '        Throw ex
        '    Finally
        '        If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
        '    End Try

        '    Return dsKhoBac
        'End Function

        Public Function CheckExist(ByVal objLoaiHinh As infLoaiHinh) As Boolean
            Return Load(objLoaiHinh).MA_LH <> ""
        End Function

        Public Function CheckExist(ByVal strMa_LH As String) As Boolean
            Return Load(strMa_LH).MA_LH <> ""
        End Function
    End Class

End Namespace
