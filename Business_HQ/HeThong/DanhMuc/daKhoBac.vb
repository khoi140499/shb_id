Imports VBOracleLib

Namespace DanhMuc.KhoBac
    Public Class daKhoBac
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objKhoBac As infKhoBac) As Boolean
            Dim cnKhoBac As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO TCS_DM_KhoBac (SHKB, TEN,MA_TINH,MA_DB, Tinh_Trang)" & _
                                    "VALUES ('" & objKhoBac.SHKB & "','" & objKhoBac.TEN_KB & "','" & objKhoBac.MaTinh & "','" & objKhoBac.MaDBHC & "','" & objKhoBac.TINH_TRANG & "')"

            Try
                cnKhoBac = New DataAccess
                cnKhoBac.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi thêm mới kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objKhobac As infKhoBac) As Boolean
            Dim cnKhoBac As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_KhoBac SET TEN='" & objKhobac.TEN_KB & "' " & _
                                   ",TINH_TRANG='" & objKhobac.TINH_TRANG & "',MA_TINH='" & objKhobac.MaTinh & "',MA_DB='" & objKhobac.MaDBHC & "' WHERE SHKB='" & objKhobac.SHKB & "'"

            Try
                cnKhoBac = New DataAccess
                cnKhoBac.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi cập nhật kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal strSHKB As String) As Boolean
            Dim cnKhoBac As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_KhoBac " & _
                           "WHERE shkb='" & strSHKB & "'"
            Try
                cnKhoBac = New DataAccess
                cnKhoBac.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi xóa kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá tài khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function LoadDetail(ByVal strSHKB As String) As infKhoBac
            Dim objKhoBac As New infKhoBac
            Dim cnKhoBac As DataAccess
            Dim drKhoBac As IDataReader
            Dim strSQL As String = "Select SHKB,Ten,MA_TINH,MA_DB, tinh_trang From TCS_DM_KhoBac " & _
                        "Where SHKB='" & strSHKB & "'"
            Try
                cnKhoBac = New DataAccess
                drKhoBac = cnKhoBac.ExecuteDataReader(strSQL, CommandType.Text)
                If drKhoBac.Read Then
                    objKhoBac = New infKhoBac
                    objKhoBac.SHKB = strSHKB
                    objKhoBac.TEN_KB = drKhoBac("TEN").ToString()
                    objKhoBac.TINH_TRANG = drKhoBac("TINH_TRANG").ToString()
                    objKhoBac.MaTinh = drKhoBac("MA_TINH").ToString()
                    objKhoBac.MaDBHC = drKhoBac("MA_DB").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drKhoBac Is Nothing Then drKhoBac.Close()
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return objKhoBac
        End Function

        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnKhoBac As DataAccess
            Dim dsKhoBac As DataSet

            Try
                strSQL = "SELECT SHKB,Ten,MA_TINH,MA_DB, tinh_trang,DECODE (tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 1, 'Dang hoat dong', " & _
                         "                 0, 'Ngung hoat dong' " & _
                         "                ) tinhtrang  from tcs_dm_khobac" & _
                         "  WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY SHKB ASC "

                cnKhoBac = New DataAccess
                dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try

            Return dsKhoBac
        End Function

        Public Function CheckExist(ByVal objKhoBac As infKhoBac) As Boolean
            Return LoadDetail(objKhoBac.SHKB).SHKB <> ""
        End Function

        Public Function CheckDelete(ByVal strSHKB As String) As Object
            Dim cnKhoBac As DataAccess
            Dim dsKhoBac As DataSet

            Try
                Dim strSql As String = "select shkb from (select shkb from tcs_ctu_hdr union " & _
                           "select shkb from tcs_baolanh_hdr) where shkb='" & strSHKB & "'"

                cnKhoBac = New DataAccess
                dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try

            Return dsKhoBac
        End Function
    End Class

End Namespace
