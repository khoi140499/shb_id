Namespace DanhMuc.CapChuong
    Public Class infCapChuong
        Private strCCH_ID As String
        Private strMA_CHUONG As String
        Private strTen As String
        Private strGHICHU As String
        Private strTINH_TRANG As String

        Sub New()
            strCCH_ID = ""
            strMA_CHUONG = ""
            strTen = ""
            strGHICHU = ""
            strTINH_TRANG = "1"
        End Sub

        Sub New(ByVal drwCapChuong As DataRow)
            strCCH_ID = drwCapChuong("CCH_ID").ToString().Trim()
            strMA_CHUONG = drwCapChuong("MA_CHUONG").ToString().Trim()
            strTen = drwCapChuong("Ten").ToString().Trim()
            strGHICHU = drwCapChuong("GHI_CHU").ToString().Trim()
            strTINH_TRANG = drwCapChuong("TINH_TRANG").ToString().Trim()
        End Sub

        Public Property CCH_ID() As String
            Get
                Return strCCH_ID
            End Get
            Set(ByVal Value As String)
                strCCH_ID = Value
            End Set
        End Property

        Public Property MA_CHUONG() As String
            Get
                Return strMA_CHUONG
            End Get
            Set(ByVal Value As String)
                strMA_CHUONG = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property

        Public Property GHICHU() As String
            Get
                Return strGHICHU
            End Get
            Set(ByVal Value As String)
                strGHICHU = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTINH_TRANG
            End Get
            Set(ByVal Value As String)
                strTINH_TRANG = Value
            End Set
        End Property
    End Class
End Namespace
