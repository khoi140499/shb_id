﻿Namespace DanhMuc.TieuMuc
    Public Class infTieuMuc
        Private strMTM_ID As String
        Private strMA_TMUC As String
        Private strTen As String
        Private strGHICHU As String
        Private strTINH_TRANG As String
        Sub New()
            strMTM_ID = ""
            strTen = ""
            strGHICHU = ""
            strTINH_TRANG = "1"
        End Sub

        Sub New(ByVal drwLoaiKhoan As DataRow)
            strMTM_ID = drwLoaiKhoan("MTM_ID").ToString().Trim()
            strMA_TMUC = drwLoaiKhoan("MA_TMUC").ToString().Trim()
            strTen = drwLoaiKhoan("Ten").ToString().Trim()
            strTINH_TRANG = drwLoaiKhoan("TINH_TRANG").ToString().Trim()
            strGHICHU = drwLoaiKhoan("GHI_CHU").ToString().Trim()
        End Sub

        Public Property MTM_ID() As String
            Get
                Return strMTM_ID
            End Get
            Set(ByVal Value As String)
                strMTM_ID = Value
            End Set
        End Property

        Public Property MA_TMUC() As String
            Get
                Return strMA_TMUC
            End Get
            Set(ByVal Value As String)
                strMA_TMUC = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property

        Public Property GHICHU() As String
            Get
                Return strGHICHU
            End Get
            Set(ByVal Value As String)
                strGHICHU = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTINH_TRANG
            End Get
            Set(ByVal Value As String)
                strTINH_TRANG = Value
            End Set
        End Property
    End Class
End Namespace

