Imports VBOracleLib
Namespace DanhMuc.LoaiKhoan
    Public Class buLoaiKhoan

        Private strStatus As String

        Sub New()
        End Sub

        Sub New(ByVal strTmpStatus As String)
            strStatus = strTmpStatus
        End Sub

        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property

        Public Function Insert(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
            Dim tmpLoaiKhoan As New daLoaiKhoan
            If tmpLoaiKhoan.CheckExist(objLoaiKhoan) = False Then
                Return tmpLoaiKhoan.Insert(objLoaiKhoan)
            Else

                Return False
            End If
        End Function

        Public Function Update(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
            Dim tmpLoaiKhoan As New daLoaiKhoan
            Try
                Dim objExist As infLoaiKhoan = tmpLoaiKhoan.Load(objLoaiKhoan)
                If objExist.LKH_ID = "" Then
                    Return tmpLoaiKhoan.Update(objLoaiKhoan)
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function


        Public Function Delete(ByVal strTmpMaLoaiKhoan As String) As Boolean
            Dim objLoaiKhoan As New infLoaiKhoan
            objLoaiKhoan.LKH_ID = strTmpMaLoaiKhoan
            Dim tmpLoaiKhoan As New daLoaiKhoan
            Return tmpLoaiKhoan.Delete(objLoaiKhoan)
        End Function

        Public Function Load(ByVal WhereClause As String) As DataSet
            Dim objLoaiKhoan As New daLoaiKhoan
            Try
                Dim dsLoaiKhoan As DataSet = objLoaiKhoan.Load(WhereClause, "")
                Return dsLoaiKhoan
            Catch ex As Exception
            Finally
            End Try

        End Function

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    Dim objLoaiKhoan As New daLoaiKhoan
        '    Try
        '        Dim tmpLoaiKhoan As infLoaiKhoan = objLoaiKhoan.Load(flxDanhMuc.Item(flxDanhMuc.Row, 0))
        '        flxDanhMuc(flxDanhMuc.Row, 0) = tmpLoaiKhoan.LKH_ID
        '        flxDanhMuc(flxDanhMuc.Row, 1) = tmpLoaiKhoan.MA_KHOAN
        '        flxDanhMuc(flxDanhMuc.Row, 2) = tmpLoaiKhoan.Ten
        '        flxDanhMuc(flxDanhMuc.Row, 3) = tmpLoaiKhoan.GHICHU
        '        flxDanhMuc(flxDanhMuc.Row, 4) = IIf(tmpLoaiKhoan.TINH_TRANG = "0", "Ngung hoat dong", "Dang hoat dong")
        '        If flxDanhMuc.Cols(flxDanhMuc.Col).Sort <> C1.Win.C1FlexGrid.SortFlags.None Then flxDanhMuc.Sort(flxDanhMuc.Cols(flxDanhMuc.Col).Sort, flxDanhMuc.Col)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid, ByVal objLoaiKhoan As infLoaiKhoan)
        '    Try
        '        flxDanhMuc.Rows.Add()
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 0) = objLoaiKhoan.LKH_ID
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 1) = objLoaiKhoan.MA_KHOAN
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 2) = objLoaiKhoan.Ten
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 3) = objLoaiKhoan.GHICHU
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 4) = IIf(objLoaiKhoan.TINH_TRANG = "0", "Ngung hoat dong", "Dang hoat dong")
        '        flxDanhMuc.Select(flxDanhMuc.Rows.Count - 1, flxDanhMuc.Col)
        '        If flxDanhMuc.Cols(flxDanhMuc.Col).Sort <> C1.Win.C1FlexGrid.SortFlags.None Then flxDanhMuc.Sort(flxDanhMuc.Cols(flxDanhMuc.Col).Sort, flxDanhMuc.Col)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Function Add() As Object
        '    Dim frmAdd As New MLNS.frmMLNS("LOAIKHOAN")
        '    frmAdd.ShowDialog()
        '    If frmAdd.DialogResult = DialogResult.OK Then
        '        Dim objLoaiKhoan As New infLoaiKhoan
        '        Dim tmpLoaiKhoan As New daLoaiKhoan
        '        objLoaiKhoan.MA_KHOAN = frmAdd.txtChuong.Text.Trim
        '        objLoaiKhoan.TINH_TRANG = (1 - frmAdd.cboTinh_Trang.SelectedIndex).ToString
        '        objLoaiKhoan = tmpLoaiKhoan.Load(objLoaiKhoan)
        '        If objLoaiKhoan.LKH_ID <> "" Then
        '            Return objLoaiKhoan
        '        Else
        '            Return Nothing
        '        End If
        '    Else
        '        Return Nothing
        '    End If
        'End Function

        'Public Function Edit(ByVal strID As String) As Boolean
        '    Dim frmAdd As New MLNS.frmMLNS("LOAIKHOAN", strID)
        '    frmAdd.ShowDialog()
        '    Return frmAdd.DialogResult = DialogResult.OK
        'End Function

        Public Function GetReport() As String
            Return Common.mdlCommon.Report_Type.DM_LOAIKHOAN
        End Function

    End Class
End Namespace
