﻿Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace DanhMuc.TieuMuc
    Public Class daTieuMuc
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objTieuMuc As infTieuMuc) As Boolean
            Dim cnTieuMuc As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO TCS_DM_MUC_TMUC (MTM_ID, MA_TMUC, TEN, GHI_CHU, TINH_TRANG) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("TCS_MTM_ID_SEQ.NEXTVAL") & ", '" & objTieuMuc.MA_TMUC & "', '" & objTieuMuc.Ten & "', '" & objTieuMuc.GHICHU & "', '" & objTieuMuc.TINH_TRANG & "')"
            Try
                cnTieuMuc = New DataAccess
                cnTieuMuc.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnTieuMuc Is Nothing Then cnTieuMuc.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objTieuMuc As infTieuMuc) As Boolean
            Dim cnTieuMuc As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_MUC_TMUC SET MA_TMUC='" & objTieuMuc.MA_TMUC & "', TEN='" & objTieuMuc.Ten & "', " & _
                   "GHI_CHU='" & objTieuMuc.GHICHU & "', TINH_TRANG='" & objTieuMuc.TINH_TRANG & "' WHERE MTM_ID=" & objTieuMuc.MTM_ID
            Try
                cnTieuMuc = New DataAccess
                cnTieuMuc.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnTieuMuc Is Nothing Then cnTieuMuc.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objTieuMuc As infTieuMuc) As Boolean
            Dim cnTieuMuc As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_MUC_TMUC " & _
                    "WHERE MTM_ID=" & objTieuMuc.MTM_ID
            Try
                cnTieuMuc = New DataAccess
                cnTieuMuc.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  'LogDebug.Writelog("Có lỗi xảy ra khi xoá danh mục loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnTieuMuc Is Nothing Then cnTieuMuc.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strMTM_ID As String) As infTieuMuc
            Dim objTieuMuc As New infTieuMuc
            Dim cnTieuMuc As DataAccess
            Dim drTieuMuc As IDataReader
            Dim strSQL As String = "Select MTM_ID, MA_TMUC, TEN, GHI_CHU, TINH_TRANG From TCS_DM_MUC_TMUC " & _
                        "Where MTM_ID=" & strMTM_ID
            Try
                cnTieuMuc = New DataAccess
                drTieuMuc = cnTieuMuc.ExecuteDataReader(strSQL, CommandType.Text)
                If drTieuMuc.Read Then
                    objTieuMuc.MTM_ID = strMTM_ID
                    objTieuMuc.MA_TMUC = drTieuMuc("MA_KHOAN").ToString()
                    objTieuMuc.Ten = drTieuMuc("TEN").ToString()
                    objTieuMuc.GHICHU = drTieuMuc("GHI_CHU").ToString()
                    objTieuMuc.TINH_TRANG = drTieuMuc("TINH_TRANG").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin loại - khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drTieuMuc Is Nothing Then drTieuMuc.Close()
                If Not cnTieuMuc Is Nothing Then cnTieuMuc.Dispose()
            End Try
            Return objTieuMuc
        End Function

        Public Function Load(ByVal tmpTieuMuc As infTieuMuc) As infTieuMuc
            Dim objTieuMuc As New infTieuMuc
            Dim cnTieuMuc As DataAccess
            Dim drTieuMuc As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc

            Dim strSQL As String = "Select MTM_ID, MA_TMUC, TEN, GHI_CHU, TINH_TRANG From TCS_DM_MUC_TMUC " & _
                "Where " & IIf(tmpTieuMuc.MTM_ID <> "", "MTM_ID = " & tmpTieuMuc.MTM_ID & " and ", "") & "MA_TMUC='" & tmpTieuMuc.MA_TMUC & "'"

            Try
                cnTieuMuc = New DataAccess
                drTieuMuc = cnTieuMuc.ExecuteDataReader(strSQL, CommandType.Text)
                If drTieuMuc.Read Then
                    objTieuMuc = New infTieuMuc
                    objTieuMuc.MTM_ID = drTieuMuc("MTM_ID").ToString()
                    objTieuMuc.MA_TMUC = drTieuMuc("MA_TMUC").ToString()
                    objTieuMuc.Ten = drTieuMuc("TEN").ToString()
                    objTieuMuc.TINH_TRANG = drTieuMuc("TINH_TRANG").ToString()
                    objTieuMuc.GHICHU = drTieuMuc("GHI_CHU").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin loại - khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drTieuMuc Is Nothing Then drTieuMuc.Close()
                If Not cnTieuMuc Is Nothing Then cnTieuMuc.Dispose()
            End Try
            Return objTieuMuc
        End Function

        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As Object
            Dim strSQL As String
            Dim cnTieuMuc As DataAccess
            Dim dsLoaiKhoan As DataSet

            Try
                strSQL = "SELECT   mtm_id, ma_tmuc, ten, ghi_chu, " & _
                         "         DECODE (tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngung hoat dong', " & _
                         "                 1, 'Dang hoat dong' " & _
                         "                ) tinh_trang " & _
                         "    FROM tcs_dm_muc_tmuc " & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY ma_tmuc "
                cnTieuMuc = New DataAccess
                dsLoaiKhoan = cnTieuMuc.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            Finally
                If Not cnTieuMuc Is Nothing Then cnTieuMuc.Dispose()
            End Try

            Return dsLoaiKhoan
        End Function
        Public Function CheckDelete(ByVal strTieuMuc As String) As Object
            Dim cnTieuMuc As DataAccess
            Dim dsTieuMuc As DataSet

            Try
                Dim strSQL As String = "select ma_tmuc from (select ma_tmuc from tcs_ctu_dtl) " & _
                        "Where ma_tmuc=" & strTieuMuc

                cnTieuMuc = New DataAccess
                dsTieuMuc = cnTieuMuc.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnTieuMuc Is Nothing Then cnTieuMuc.Dispose()
            End Try

            Return dsTieuMuc
        End Function

        Public Function CheckExist(ByVal objTieuMuc As infTieuMuc) As Boolean
            Return Load(objTieuMuc).MTM_ID <> ""
        End Function

        Public Function CheckExist(ByVal strMTM_ID As String) As Boolean
            Return Load(strMTM_ID).MTM_ID <> ""
        End Function

    End Class

End Namespace
