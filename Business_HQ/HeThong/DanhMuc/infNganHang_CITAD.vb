﻿Namespace DanhMuc.NganHang
    Public Class infNganHang_CITAD
        Private strSHKB As String
        Private strTenKB As String
        Private strMA_NH_TT As String
        Private strTEN_NH_TT As String
        Private strMA_NH_TH As String
        Private strTEN_NH_TH As String
        Private strTT_KB As String

        Public Property MA_NH_TT() As String
            Get
                Return strMA_NH_TT
            End Get
            Set(ByVal Value As String)
                strMA_NH_TT = Value
            End Set
        End Property
        Public Property TT_KB() As String
            Get
                Return strTT_KB
            End Get
            Set(ByVal Value As String)
                strTT_KB = Value
            End Set
        End Property
        Public Property TEN_NH_TT() As String
            Get
                Return strTEN_NH_TT
            End Get
            Set(ByVal Value As String)
                strTEN_NH_TT = Value
            End Set
        End Property

        Public Property MA_NH_TH() As String
            Get
                Return strMA_NH_TH
            End Get
            Set(ByVal Value As String)
                strMA_NH_TH = Value
            End Set
        End Property

        Public Property TEN_NH_TH() As String
            Get
                Return strTEN_NH_TH
            End Get
            Set(ByVal Value As String)
                strTEN_NH_TH = Value
            End Set
        End Property

        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property
        Public Property Ten_KB() As String
            Get
                Return strTenKB
            End Get
            Set(ByVal Value As String)
                strTenKB = Value
            End Set
        End Property
    End Class
End Namespace
