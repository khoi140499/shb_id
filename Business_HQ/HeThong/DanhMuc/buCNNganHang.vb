﻿Imports VBOracleLib

Namespace DanhMuc.CNNganHang
    Public Class buCNNganHang
        Sub New()
        End Sub

        Public Function Load(ByVal WhereClause As String) As DataSet
            Dim objCNNganHang As New daCNNganHang
            Try
                Dim dsNganHang As DataSet = objCNNganHang.Load(WhereClause)
                Return dsNganHang
            Catch ex As Exception
            Finally
            End Try
        End Function

        Public Function Delete(ByVal strID As String) As Boolean
            ' Dim objCNNganHang As New infCNNganHang
            ' objCNNganHang.ID = strID
            ' objCNNganHang.MA_DTHU = strMA_DTHU
            Dim tmpNganHang As New daCNNganHang
            'Dim kpKT As Byte = daCNNganHang.Check2Del(objCNNganHang)
            Dim blnResult As Boolean = True
            'If kpKT = 0 Then
            blnResult = tmpNganHang.Delete(strID)
            'Else
            '    blnResult = False
            'End If
            Return blnResult
        End Function

        Public Function Insert(ByVal objCNNganHang As infCNNganHang) As Boolean
            Dim tmpNganHang As New daCNNganHang
            If tmpNganHang.CheckExist(objCNNganHang) = False Then
                Return tmpNganHang.Insert(objCNNganHang)
            Else
                MsgBox("Danh mục ngân hàng đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End If
        End Function

        Public Function Update(ByVal objCNNganHang As infCNNganHang) As Boolean
            Dim tmpNganHang As New daCNNganHang
            If tmpNganHang.CheckExist(objCNNganHang) = True Then
                Return tmpNganHang.Update(objCNNganHang)
            Else
                Return False
            End If
        End Function
    End Class
End Namespace

