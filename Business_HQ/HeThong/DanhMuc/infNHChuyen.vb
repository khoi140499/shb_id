﻿Namespace DanhMuc.NHChuyen
    Public Class infNHChuyen
        Private strMa_NHC As String
        Private strTen_NHC As String
        

        Sub New()
            strMa_NHC = ""
            strTen_NHC = ""
        End Sub

        Sub New(ByVal drwNHC As DataRow)
            strMa_NHC = drwNHC("ma_nh_chuyen").ToString().Trim()
            strTen_NHC = drwNHC("ten_nh_chuyen").ToString().Trim()
        End Sub

        Public Property Ma_NHC() As String
            Get
                Return strMa_NHC
            End Get
            Set(ByVal Value As String)
                strMa_NHC = Value
            End Set
        End Property
        Public Property Ten_NHC() As String
            Get
                Return strTen_NHC
            End Get
            Set(ByVal Value As String)
                strTen_NHC = Value
            End Set
        End Property
    End Class
End Namespace

