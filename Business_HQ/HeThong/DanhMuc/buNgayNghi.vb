﻿Imports VBOracleLib
Namespace DanhMuc.NgayNghi

    Public Class buNgayNghi
        Private strStatus As String
        Sub New()
        End Sub
        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property
        Sub New(ByVal strTmpStatus As String)
            strStatus = strTmpStatus
        End Sub

        Public Function Save(ByVal objngaynghi As infNgayNghi) As Boolean
            If strStatus = "EDIT" Then
                Return Update(objngaynghi)
            Else
                Return Insert(objngaynghi)
            End If
        End Function
        Public Function Insert(ByVal objngaynghi As infNgayNghi) As Boolean
            Dim tmpNgayNghi As New daNgayNghi
            Dim objda As New daNgayNghi
            Dim strRtu As Boolean = False
            Dim sql As String
            sql = "Insert into TCS_DM_NgayNghi(ngay_nghi,ghi_chu) values (to_date('" & objngaynghi.NgayNghi & "','dd/MM/yyyy'),'" & objngaynghi.GhiChu & "')"
            objda.Save(sql)
            strRtu = True
            Return strRtu
        End Function

        Public Function Update(ByVal objNgayNghi As infNgayNghi) As Boolean
            Try
                Dim tmpNgayNghi As New daNgayNghi
                tmpNgayNghi.UpdateNgayNghi(objNgayNghi)
                Return True
            Catch ex As Exception
                MsgBox("Cập nhật không thành công")
                Return False
            End Try

        End Function

        Public Function GetDataSet(ByVal WhereClause As String) As DataSet
            Dim ds As DataSet
            Try
                Dim objngaynghi As New daNgayNghi
                ds = objngaynghi.DsNgayNghi(WhereClause, "")
            Catch ex As Exception
                Throw ex
            End Try
            Return ds
        End Function

        'Public Sub FormatFlexHeader(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    flxDanhMuc.Cols.Remove(2)
        '    flxDanhMuc.Cols(0).Width = 100
        '    flxDanhMuc.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        '    flxDanhMuc.Cols(1).Width = 488
        '    flxDanhMuc.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
        '    flxDanhMuc.Rows(0).Item(0) = "Mã Ngày nghỉ"
        '    flxDanhMuc.Rows(0).Item(1) = "Tên Ngày nghỉ"
        'End Sub
        'Public Function Add() As Object
        '    Dim frmAdd As New frmDiemthu
        '    frmAdd.ShowDialog()
        '    If frmAdd.DialogResult = DialogResult.OK Then
        '        Dim objngaynghi As New infNgayNghi
        '        objngaynghi.MaDThu = frmAdd.txtMadiemthu.Text
        '        objngaynghi.TenDThu = frmAdd.txtTendiemthu.Text
        '        Return objngaynghi
        '    Else
        '        Return Nothing
        '    End If
        'End Function
        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid, ByVal objLoaiThue As infNgayNghi)
        '    Try
        '        flxDanhMuc.Rows.Add()
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 0) = objLoaiThue.MaDThu
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 1) = objLoaiThue.TenDThu
        '        flxDanhMuc.Select(flxDanhMuc.Rows.Count - 1, flxDanhMuc.Col)
        '        If flxDanhMuc.Cols(flxDanhMuc.Col).Sort <> C1.Win.C1FlexGrid.SortFlags.None Then flxDanhMuc.Sort(flxDanhMuc.Cols(flxDanhMuc.Col).Sort, flxDanhMuc.Col)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    Dim objngaynghi As New daNgayNghi
        '    Try
        '        Dim tmpdiemthu As infNgayNghi = objngaynghi.Load(flxDanhMuc.Item(flxDanhMuc.Row, 0))
        '        flxDanhMuc(flxDanhMuc.Row, 0) = tmpdiemthu.MaDThu
        '        flxDanhMuc(flxDanhMuc.Row, 1) = tmpdiemthu.TenDThu
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Function Edit(ByVal strID As String) As Boolean
        '    Try
        '        Dim frmAdd As New frmDiemthu(strID)
        '        frmAdd.ShowDialog()
        '        Return frmAdd.DialogResult = DialogResult.OK
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Shared Function checkExist(ByVal objngaynghi As infNgayNghi) As Byte
            Try
                Dim objda As New daNgayNghi
                Dim sqlma, sqlten As String
                sqlma = "Select 1 from TCS_DM_NgayNghi where TO_DATE(ngay_nghi,'DD/MM/RRRR')=TO_DATE('" & objngaynghi.NgayNghi & "','DD/MM/RRRR')"
                Return objda.ExitsNgayNghi(sqlma)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'hoapt: loadDetail Diem thu
        Public Shared Function loadDetail(ByVal objngaynghi As infNgayNghi) As DataSet
            Try
                Dim objda As New daNgayNghi
                Dim sqlma
                sqlma = "Select to_char(ngay_nghi,'DD/MM/RRRR') ngay_nghi,ghi_chu from TCS_DM_NgayNghi where ngay_nghi=to_date('" & objngaynghi.NgayNghi & "','DD/MM/RRRR')"
                Return objda.loadDetail(sqlma)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DsNgayNghi() As DataSet
            Try
                Dim objda As New daNgayNghi
                Return objda.DsNgayNghi("", "")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DelNgayNghi(ByVal ngay_nghi As String) As Boolean
            Try
                Dim objda As New daNgayNghi
                Return objda.DelNgaynghi(ngay_nghi)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function Delete(ByVal strTmpngaynghi As String) As Boolean
            Dim tmpngaynghi As New daNgayNghi
            Try
                tmpngaynghi.DelNgaynghi(strTmpngaynghi)
                Return True
            Catch ex As Exception
                MsgBox("Ngày nghỉ đã có quan hệ.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End Try
        End Function

        Public Function GetReport() As String
            Return Common.mdlCommon.Report_Type.DM_DIEMTHU
        End Function
    End Class
End Namespace