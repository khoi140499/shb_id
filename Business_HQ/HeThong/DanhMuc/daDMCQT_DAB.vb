﻿'Imports VBOracleLib
'Imports System.Data.OleDb
'Imports Business_HQ.Common.mdlCommon
'Imports Business_HQ.Common.mdlSystemVariables
'Imports Business_HQ.Common
'Imports System.Data
'Imports System.Xml
'Imports System.Xml.XmlDocument
'Public Class daDMCQT_DAB
'    Public Sub Insert_New(ByVal pv_strXML As String)
'        Dim conn As DataAccess
'        Dim transCT As IDbTransaction
'        Dim strSql As String
'        Dim v_xmlDocument As New XmlDocument
'        Dim v_nodeRETCODE As XmlNodeList
'        Dim v_nodePARAMS As XmlNodeList
'        Dim pr_strMA_CQTHU As String = ""
'        Dim pr_strNAMEV As String = ""
'        Dim pr_strNAMEE As String = ""
'        Dim pr_strUSED As String = ""
'        v_xmlDocument.LoadXml(pv_strXML.Replace("&", "&amp;"))
'        v_nodeRETCODE = v_xmlDocument.GetElementsByTagName("RETCODE")
'        conn = New DataAccess
'        transCT = conn.BeginTransaction
'        Try
'            If v_nodeRETCODE(0).ChildNodes.Item(0).InnerText.Trim() = "0" Then
'                'Xóa dữ liệu trước khi Đồng bộ
'                strSql = "DELETE FROM TCS_DM_CQTHU_DAB"
'                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
'                v_nodePARAMS = v_xmlDocument.SelectNodes("/PARAMETER/params")
'                For i As Integer = 0 To v_nodePARAMS.Count - 1
'                    pr_strMA_CQTHU = v_nodePARAMS(i).ChildNodes.Item(0).InnerText.Trim()
'                    pr_strNAMEV = v_nodePARAMS(i).ChildNodes.Item(1).InnerText.Trim()
'                    pr_strNAMEE = v_nodePARAMS(i).ChildNodes.Item(2).InnerText.Trim()
'                    pr_strUSED = v_nodePARAMS(i).ChildNodes.Item(3).InnerText.Trim()
'                    strSql = "Insert into TCS_DM_CQTHU_DAB(MA_CQTHU,NAMEV,NAMEE,USED) " & _
'                              "Values ('" & pr_strMA_CQTHU & "','" & pr_strNAMEV & "','" & pr_strNAMEE & "','" & pr_strUSED & "')"
'                    ' Insert Vào bảng TCS_DM_CQT_DAB    
'                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
'                Next
'            End If
'            transCT.Commit()
'        Catch ex As Exception
'            transCT.Rollback()
'            LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu đồng bộ cơ quan thuế - DAB: " & ex.ToString & vbCrLf & pr_strMA_CQTHU, EventLogEntryType.Error)
'            Throw ex
'        End Try
'    End Sub
'    Public Function Load(ByVal WhereClause As String, ByVal temp As String) As Object
'        Dim cnCQThu As DataAccess
'        Dim dsCQThu As DataSet
'        Dim strSQL As String = "SELECT   ma_cqthu,namev,namee,used " & _
'                                "    FROM tcs_dm_cqthu_dab " & _
'                                "   WHERE 1 = 1 " & WhereClause & _
'                                " ORDER BY ma_cqthu "

'        Try
'            cnCQThu = New DataAccess
'            dsCQThu = cnCQThu.ExecuteReturnDataSet(strSQL, CommandType.Text)
'        Catch ex As Exception
'            CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Cơ quan thu")
'            Throw ex
'            'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cơ quan thu từ CSDL: " & ex.ToString)
'        Finally
'            If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
'        End Try

'        Return dsCQThu
'    End Function
'End Class
