﻿Imports VBOracleLib

Namespace DanhMuc.NganHang
    Public Class buNganHang
        Sub New()
        End Sub

        Public Function Load(ByVal WhereClause As String) As DataSet
            Dim objNganHang As New daNganHang
            Try
                Dim dsNganHang As DataSet = objNganHang.Load(WhereClause)
                Return dsNganHang
            Catch ex As Exception
            Finally
            End Try
        End Function

        Public Function Delete(ByVal strID As String) As Boolean
            Dim objNganHang As New infNganHang
            objNganHang.ID = strID
            ' objNganHang.MA_DTHU = strMA_DTHU
            Dim tmpNganHang As New daNganHang
            'Dim kpKT As Byte = daNganHang.Check2Del(objNganHang)
            Dim blnResult As Boolean = True
            'If kpKT = 0 Then
            blnResult = tmpNganHang.Delete(objNganHang)
            'Else
            '    blnResult = False
            'End If
            Return blnResult
        End Function

        Public Function Insert(ByVal objNganHang As infNganHang) As Boolean
            Dim tmpNganHang As New daNganHang
            If tmpNganHang.CheckExist(objNganHang) = False Then
                Return tmpNganHang.Insert(objNganHang)
            Else
                MsgBox("Danh mục ngân hàng đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End If
        End Function

        Public Function Update(ByVal objNganHang As infNganHang) As Boolean
            Dim tmpNganHang As New daNganHang
            If tmpNganHang.CheckExist(objNganHang) = True Then
                Return tmpNganHang.Update(objNganHang)
            Else
                Return False
            End If
        End Function
    End Class
End Namespace

