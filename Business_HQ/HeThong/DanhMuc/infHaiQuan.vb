Namespace DanhMuc.HaiQuan
    Public Class infHaiQuan
        Private strMA_HQ As String
        Private strMA_CU As String
        Private strMA_QHNS As String
        Private strTEN As String

        Sub New()
            strMA_HQ = ""
            strMA_CU = ""
            strMA_QHNS = ""
            strTEN = ""

        End Sub

        Sub New(ByVal drwLoaiHinh As DataRow)
            strMA_HQ = drwLoaiHinh("MA_HQ").ToString().Trim()
            strMA_CU = drwLoaiHinh("MA_CU").ToString().Trim()
            strMA_QHNS = drwLoaiHinh("MA_QHNS").ToString().Trim()
            strTEN = drwLoaiHinh("TEN").ToString().Trim()
        End Sub

        Public Property MA_HQ() As String
            Get
                Return strMA_HQ
            End Get
            Set(ByVal Value As String)
                strMA_HQ = Value
            End Set
        End Property

        Public Property MA_CU() As String
            Get
                Return strMA_CU
            End Get
            Set(ByVal Value As String)
                strMA_CU = Value
            End Set
        End Property

        Public Property MA_QHNS() As String
            Get
                Return strMA_QHNS
            End Get
            Set(ByVal Value As String)
                strMA_QHNS = Value
            End Set
        End Property

        Public Property TEN() As String
            Get
                Return strTEN
            End Get
            Set(ByVal Value As String)
                strTEN = Value
            End Set
        End Property

    End Class
End Namespace
