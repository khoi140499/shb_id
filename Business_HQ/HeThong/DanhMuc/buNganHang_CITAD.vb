﻿Imports VBOracleLib

Namespace DanhMuc.NganHang
    Public Class buNganHang_CITAD
        Sub New()
        End Sub

        Public Function Load(ByVal WhereClause As String) As DataSet
            Dim objNganHang As New daNganHang_CITAD
            Try
                Dim dsNganHang As DataSet = objNganHang.Load(WhereClause)
                Return dsNganHang
            Catch ex As Exception
            Finally
            End Try
        End Function

        Public Function Delete(ByVal strID As String, ByVal strMa As String) As Boolean
            Dim objNganHang As New infNganHang_CITAD
            objNganHang.SHKB = strID
            objNganHang.MA_NH_TH = strMa
            ' objNganHang.MA_DTHU = strMA_DTHU
            Dim tmpNganHang As New daNganHang_CITAD
            'Dim kpKT As Byte = daNganHang_CITAD.Check2Del(objNganHang)
            Dim blnResult As Boolean = True
            'If kpKT = 0 Then
            blnResult = tmpNganHang.Delete(objNganHang)
            'Else
            '    blnResult = False
            'End If
            Return blnResult
        End Function
        Public Function CheckExist(ByVal objNganHang As infNganHang_CITAD) As Boolean
            Dim tmpNganHang As New daNganHang_CITAD
            If tmpNganHang.CheckExist(objNganHang) = False Then
                Return True
            Else
                'MsgBox("Danh mục ngân hàng đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End If
        End Function

        Public Function Insert(ByVal objNganHang As infNganHang_CITAD) As Boolean
            Dim tmpNganHang As New daNganHang_CITAD
            Return tmpNganHang.Insert(objNganHang)
        End Function

        Public Function Update(ByVal objNganHang As infNganHang_CITAD) As Boolean
            Dim tmpNganHang As New daNganHang_CITAD
            If tmpNganHang.CheckExist(objNganHang) = True Then
                Return tmpNganHang.Update(objNganHang)
            Else
                Return False
            End If
        End Function
    End Class
End Namespace

