Imports VBOracleLib

Namespace DanhMuc.DBHC
    Public Class daDBHC
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objDBHC As infDBHC) As Boolean
            Dim cnDBHC As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO TCS_DM_Xa (Xa_id,MA_XA, TEN,MA_CHA, Tinh_Trang)" & _
                                    "VALUES (tcs_xa_id_seq.nextval,'" & objDBHC.Ma_DBHC & "','" & objDBHC.TEN_DBHC & "','" & objDBHC.Ma_Cha & "','" & objDBHC.TINH_TRANG & "')"

            Try
                cnDBHC = New DataAccess
                cnDBHC.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi thêm mới địa bàn hành chính")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objDBHC As infDBHC) As Boolean
            Dim cnDBHC As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_Xa SET TEN='" & objDBHC.TEN_DBHC & "',ma_xa='" & objDBHC.Ma_DBHC & "',ma_cha='" & objDBHC.Ma_Cha & "' " & _
                                   ",TINH_TRANG='" & objDBHC.TINH_TRANG & "' WHERE xa_id='" & objDBHC.Xa_ID & "'"

            Try
                cnDBHC = New DataAccess
                cnDBHC.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi cập nhật địa bàn hành chính")
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal strXaID As String) As Boolean
            Dim cnDBHC As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_Xa " & _
                           "WHERE xa_id='" & strXaID & "'"
            Try
                cnDBHC = New DataAccess
                cnDBHC.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi xóa địa bàn hành chính")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá tài khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function LoadDetail(ByVal strXaID As String) As infDBHC
            Dim objDBHC As New infDBHC
            Dim cnDBHC As DataAccess
            Dim drDBHC As IDataReader
            Dim strSQL As String = "Select xa_id,ma_xa,Ten,ma_cha, tinh_trang From TCS_DM_Xa " & _
                        "Where xa_id='" & strXaID & "'"
            Try
                cnDBHC = New DataAccess
                drDBHC = cnDBHC.ExecuteDataReader(strSQL, CommandType.Text)
                If drDBHC.Read Then
                    objDBHC = New infDBHC
                    objDBHC.Xa_ID = strXaID
                    objDBHC.Ma_DBHC = drDBHC("ma_xa").ToString()
                    objDBHC.TEN_DBHC = drDBHC("TEN").ToString()
                    objDBHC.Ma_Cha = drDBHC("ma_cha").ToString()
                    objDBHC.TINH_TRANG = drDBHC("TINH_TRANG").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin địa bàn hành chính")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drDBHC Is Nothing Then drDBHC.Close()
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try
            Return objDBHC
        End Function

        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnDBHC As DataAccess
            Dim dsDBHC As DataSet

            Try
                strSQL = "SELECT xa_id,ma_xa,Ten,ma_cha, tinh_trang,DECODE (tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 1, 'Dang hoat dong', " & _
                         "                 0, 'Ngung hoat dong' " & _
                         "                ) tinhtrang  from TCS_DM_Xa" & _
                         "  WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY xa_id ASC "

                cnDBHC = New DataAccess
                dsDBHC = cnDBHC.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin địa bàn hành chính")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try

            Return dsDBHC
        End Function
        Public Function check_maDBHC(ByVal ma_dbhc As String) As Boolean
            Dim connect As DataAccess
            Dim Dts As DataSet

            Try
                connect = New DataAccess
                Dim strSql As String = "select * from tcs_dm_xa where ma_xa = '" & ma_dbhc & "'"
                Dts = connect.ExecuteReturnDataSet(strSql, CommandType.Text)
                If Dts.Tables(0).Rows.Count = 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                Return False

            Finally
                If Not connect Is Nothing Then connect.Dispose()
            End Try

        End Function
        Public Function CheckExist(ByVal objDBHC As infDBHC) As Boolean
            Return LoadDetail(objDBHC.Xa_ID).Xa_ID <> ""
        End Function
        Public Function CheckDelete(ByVal strMA_DBHC As String) As Object
            Dim cnDBHC As DataAccess
            Dim dsDBHC As DataSet

            Try
                Dim strSql As String = "select xa_id from (select xa_id from tcs_ctu_hdr union all " & _
                           "select xa_id from tcs_baolanh_hdr) where xa_id='" & strMA_DBHC & "'"

                cnDBHC = New DataAccess
                dsDBHC = cnDBHC.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try

            Return dsDBHC
        End Function
    End Class

End Namespace
