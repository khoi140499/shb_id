﻿Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.BaoCao

Public Class DALTcs_dchieu_nhhq_hdr
    Dim strGIO_DC As String = DataAccess.ExecuteReturnDataSet("SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='GIO_DC_CT'", CommandType.Text).Tables(0).Rows(0)(0).ToString()
    'Public Function getBCTHopCTTThueXNKhau(ByVal type1 As String, ByVal type2 As String, _
    '                                       ByVal strDate As String, ByVal strMaCN As String) As DataSet
    '    Dim strSQL As String
    '    Dim strQueryHQ As String
    '    Dim strQueryNH As String
    '    Dim strQueryHeader As String
    '    Dim strQueryFooter As String
    '    Dim strDateWhere As String
    '    Dim strDateWhere2 As String
    '    Dim strWhereMaCN As String
    '    Dim strWhereMaCN2 As String

    '    Dim result As New DataSet
    '    Dim dsNganHang As DataSet
    '    If Not strMaCN Is Nothing And Not strMaCN = "" Then
    '        'Neu la BL chi check theo chi  nhanh
    '        If type1 = "M81" Or type1 = "M91" Then
    '            If strMaCN.Length > 3 Then
    '                strMaCN = strMaCN.Substring(0, 3)
    '            End If
    '            strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn in (select cn.id from tcs_dm_chinhanh cn where TO_NUMBER(cn.branch_id)= " & CInt(strMaCN) & ")) "
    '            strWhereMaCN2 = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn in (select cn.id from tcs_dm_chinhanh cn where TO_NUMBER(cn.branch_id)= " & CInt(strMaCN) & ")) "
    '        Else
    '            If strMaCN.Length = 3 Then
    '                strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn in (select cn.id from tcs_dm_chinhanh cn where TO_NUMBER(cn.branch_id)= " & CInt(strMaCN) & ")) "
    '                strWhereMaCN2 = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn in (select cn.id from tcs_dm_chinhanh cn where TO_NUMBER(cn.branch_id)= " & CInt(strMaCN) & ")) "
    '            Else
    '                strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
    '                strWhereMaCN2 = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
    '            End If
    '        End If
    '    End If

    '    If strDate.Trim <> "" Then
    '        strDateWhere = " AND to_char(a.ngay_bn,'yyyyMMdd') = " & ConvertDateToNumber(strDate).ToString
    '        strDateWhere2 = " AND to_char(b.ngay_bn,'yyyyMMdd') = " & ConvertDateToNumber(strDate).ToString
    '    End If

    '    Try
    '        strSQL = "SELECT ROWNUM stt, MCQThu,SMonTruyen,STienTruyen,SMonNhan,STienNhan, nvl(stientruyen,0)- nvl(stiennhan,0) CLech" & _
    '                 " FROM ( " & _
    '                 " select ma_cqthu MCQThu, sum(smontruyen) smontruyen, sum(stientruyen) stientruyen,sum(smonnhan) smonnhan, sum(stiennhan) stiennhan " & _
    '                 " from ( " & _
    '                 " SELECT a.ma_cqthu, COUNT(*) smontruyen, sum(a.ttien) stientruyen, 0 smonnhan,0 stiennhan  FROM tcs_dchieu_nhhq_hdr a " & _
    '                  " where accept_yn='Y' " & strDateWhere & "  And a.transaction_type='" & type1 & "'" & _
    '                    strWhereMaCN & _
    '                 " GROUP by a.ma_cqthu " & _
    '                 " UNION ALL" & _
    '                 " SELECT a.ma_cqthu,0 smontruyen,0 stientruyen, COUNT(*) smonnhan, sum(a.ttien) stiennhan  FROM tcs_dchieu_hqnh_hdr a " & _
    '                  " where  a.kq_dc <> '22' " & strDateWhere & " and a.transaction_type='" & type2 & "'" & _
    '                    strWhereMaCN & _
    '                 " group by a.ma_cqthu) " & _
    '                 " group by ma_cqthu) "

    '        dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '        If Not dsNganHang Is Nothing Then
    '            result = dsNganHang
    '        End If
    '    Catch ex As Exception
    '    Finally

    '    End Try

    '    Return result
    'End Function
    Public Function getBCTHopCTTThueXNKhau(ByVal type1 As String, ByVal type2 As String, _
                                           ByVal strDate As String, ByVal strMaCN As String, ByVal ma_nt As String) As DataSet
        Dim strSQL As String
        Dim strQueryHQ As String
        Dim strQueryNH As String
        Dim strQueryHeader As String
        Dim strQueryFooter As String
        Dim strDateWhere As String
        Dim strDateWhere2 As String
        Dim strWhereMaCN As String
        Dim strWhereMaCN2 As String
        Dim strWhereMaNT As String
        Dim strCheckHS As String
        Dim result As New DataSet
        Dim dsNganHang As DataSet
        Dim strTableCtu As String
        Dim strTTCthue As String
        Dim strMaKS As String
        If Not strMaCN Is Nothing And Not strMaCN = "" Then
            If strMaCN.IndexOf(";") > 0 Then
                strCheckHS = strMaCN.Split(";")(1).ToString
                strMaCN = strMaCN.Split(";")(0).ToString

                If strCheckHS = "2" Then
                    strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & strMaCN & "')"
                    strWhereMaCN2 = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv, tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & strMaCN & "')"
                Else
                    strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
                    strWhereMaCN2 = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
                End If

            End If
            strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
            strWhereMaCN2 = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
            'End If
            '    End If

        End If
        If type1 = "M41" Then
            strDateWhere = ""
        ElseIf type1 = "M91" Or type1 = "M51" Then
            strDateWhere = " and b.trang_thai='04' "
            strMaKS = " and b.ma_ks <> 0 "
        End If
        If type1 = "M81" Then
            strDateWhere = ""
        End If
        'If type1 = "M41" Then
        '    strTTCthue = " and tt_cthue = '1' "
        'ElseIf type1 = "M51" Then
        '    strTTCthue = " and tt_cthue = '0' "
        'End If

        If type1 = "M51" Or type1 = "M41" Then
            strTableCtu = "tcs_ctu_hdr"
        ElseIf type1 = "M81" Or type1 = "M91" Then
            strTableCtu = "tcs_baolanh_hdr"
        End If
        'strWhereMaNT = "AND a.Ma_NT = '" & ma_nt & "'"
        If strDate.Trim <> "" Then
            strDateWhere = strDateWhere & " AND to_char(a.ngay_bn,'yyyyMMdd') = " & ConvertDateToNumber(strDate).ToString
            strDateWhere2 = " AND to_char(b.ngay_bn,'yyyyMMdd') = " & ConvertDateToNumber(strDate).ToString
        End If

        Try
            strSQL = "SELECT ROWNUM stt, MCQThu,SMonTruyen,STienTruyen,SMonNhan,STienNhan, nvl(stientruyen,0)- nvl(stiennhan,0) CLech" & _
                     " FROM ( " & _
                     " select ma_cqthu MCQThu, sum(smontruyen) smontruyen, sum(stientruyen) stientruyen,sum(smonnhan) smonnhan, sum(stiennhan) stiennhan " & _
                     " from ( " & _
                     " SELECT a.ma_cqthu, COUNT(*) smontruyen, sum(a.ttien) stientruyen, 0 smonnhan,0 stiennhan  FROM tcs_dchieu_nhhq_hdr a, " & strTableCtu & " b" & _
                      " where  a.accept_yn='Y' and a.so_ct=b.so_ct  " & strDateWhere & strTTCthue & strMaKS & "  And a.transaction_type='" & type1 & "'" & _
                        strWhereMaCN & strWhereMaNT & _
                     " GROUP by a.ma_cqthu " & _
                     " UNION ALL" & _
                     " SELECT a.ma_cqthu,0 smontruyen,0 stientruyen, COUNT(*) smonnhan, sum(a.ttien) stiennhan  FROM tcs_dchieu_hqnh_hdr a, tcs_dchieu_nhhq_hdr c," & strTableCtu & " b" & _
                      " where a.transaction_id =c.transaction_id  AND a.so_ct = b.so_ct and a.kq_dc <> '22' and c.accept_yn='Y'  and ( a.accept_yn='Y'  or a.accept_yn is null) and  a.so_ct=c.so_ct " & strDateWhere & _
                      " and a.transaction_type='" & type2 & "' and  c.transaction_type ='" & type1 & "'" & _
                        strWhereMaCN2 & strWhereMaNT & _
                     " group by a.ma_cqthu) " & _
                     " group by ma_cqthu) "

            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
        Finally

        End Try

        Return result
    End Function
    Public Function getBCCTietTThueXNKhau(ByVal type As String, ByVal strDate As String, ByVal strMaCN As String) As DataSet
        Dim strSQL As String
        Dim strWhereMaCN As String = ""
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim strCheckHS As String
        If Not strMaCN Is Nothing And Not strMaCN = "" Then
            If strMaCN.IndexOf(";") > 0 Then
                strCheckHS = strMaCN.Split(";")(1).ToString
                strMaCN = strMaCN.Split(";")(0).ToString
            End If
            If strCheckHS = "2" Then
                strWhereMaCN = " and c.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & strMaCN & "')"
            Else
                strWhereMaCN = " and c.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
            End If

        End If


        If strDate.Trim <> "" Then
            strDate = " and a.ngay_bn=b.ngay_bn(+) AND TO_CHAR (a.ngay_bn, 'yyyyMMdd') = " & ConvertDateToNumber(strDate).ToString
        End If

        Try
            If type = "M41" Then
                strSQL = " select ROWNUM STT, (c.kyhieu_ct || '-' || c.so_ct) KHSCTu, c.ten_nnthue TDVNThue, c.ma_nnthue MSThue, c.ma_hq_ph MHQuanPH,c.so_tk STKhai," & _
                                 " c.ngay_tk NDKTKhai, c.lh_xnk LHinh,c.SHKB MKBac,c.tk_no TKThu, c.tk_co TKTNS,  WHEN  b.kq_dc is null THEN '00' WHEN  b.kq_dc='22' THEN '00' WHEN  b.kq_dc='21' THEN '00' ELSE c.trang_thai end TTrang, c.ma_cqthu MCQT,d.ma_tmuc,  d.sotien TTIEN" & _
                                    " FROM tcs_ctu_hdr c,tcs_ctu_dtl d,tcs_dchieu_nhhq_hdr a,  tcs_dchieu_hqnh_hdr b  where c.shkb=d.shkb and c.ma_nv = d.ma_nv and c.so_bt=d.so_bt and c.ma_dthu=d.ma_dthu and c.ma_lthue = '04'" & _
                                    " and a.so_ct=c.so_ct and b.transaction_id(+) = a.transaction_id and (b.accept_yn='Y' or b.accept_yn is null) and a.trang_thai=c.trang_thai and ((c.trang_thai in ('01','02') and c.tt_cthue='1' and a.transaction_type='M41') or(c.trang_thai ='04' and c.tt_cthue='0' and a.transaction_type='M51') ) "
            Else
                strSQL = " select ROWNUM STT, (c.kyhieu_ct || '-' || c.so_ct) KHSCTu, c.ten_nnthue TDVNThue, c.ma_nnthue MSThue, c.ma_hq_ph MHQuanPH,c.so_tk STKhai," & _
                                 " c.ngay_tk NDKTKhai, c.lhxnk LHinh,c.SHKB MKBac,c.tk_no TKThu, c.tk_co TKTNS, case b.kq_dc when '22' then '00' else c.trang_thai end TTrang, c.ma_cqthu MCQT,d.ma_ndkt ma_tmuc,  NVL( d.sotien,0) TTIEN" & _
                                    " FROM tcs_baolanh_hdr c,tcs_baolanh_dtl d ,tcs_dchieu_nhhq_hdr a,  tcs_dchieu_hqnh_hdr b " & _
                                    " where c.shkb=d.shkb and NVL( d.sotien,0)>0 and c.ma_dthu = d.ma_dthu and c.so_bt=d.so_bt and c.ma_nv=d.ma_nv AND c.ngay_kb = d.ngay_kb " & _
                                    " and a.so_ct=c.so_ct and b.transaction_id(+)=a.transaction_id and a.accept_yn='Y'  and (b.accept_yn='Y' or b.accept_yn is null) and a.trang_thai=c.trang_thai  "
            End If
            strSQL = strSQL & " and ma_ks<>0  " & strDate & strWhereMaCN & " order by c.so_ct"
            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return result
    End Function
    Public Function getBCCTietTThueXNKhau_Goc(ByVal type As String, ByVal strDate As String, ByVal strMaCN As String) As DataSet
        Dim strSQL As String
        Dim strWhereMaCN As String = ""
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim strCheckHS As String

        If Not strMaCN Is Nothing And Not strMaCN = "" Then
            If strMaCN.IndexOf(";") > 0 Then
                strCheckHS = strMaCN.Split(";")(1).ToString
                strMaCN = strMaCN.Split(";")(0).ToString
            End If
            If strCheckHS = "2" Then
                strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & strMaCN & "')"
            Else
                strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
            End If

        End If

        If strDate.Trim <> "" Then
            strDate = " AND c.ngay_kb = " & ConvertDateToNumber(strDate).ToString
        End If

        Try
            If type = "M41" Then
                strSQL = " select ROWNUM STT, (c.kyhieu_ct || '-' || c.so_ct) KHSCTu, c.ten_nnthue TDVNThue, c.ma_nnthue MSThue, c.ma_hq_ph MHQuanPH,c.so_tk STKhai," & _
                                 " c.ngay_tk NDKTKhai, c.lh_xnk LHinh,c.SHKB MKBac,c.tk_no TKThu, c.tk_co TKTNS, c.trang_thai TTrang, c.ma_cqthu MCQT, c.ttien TTIEN" & _
                                    " FROM tcs_ctu_hdr c where c.ma_lthue = '04' and tt_cthue='1' "
            Else
                strSQL = " select ROWNUM STT, (c.kyhieu_ct || '-' || c.so_ct) KHSCTu, c.ten_nnthue TDVNThue, c.ma_nnthue MSThue, c.ma_hq_ph MHQuanPH,c.so_tk STKhai," & _
                                 " c.ngay_tk NDKTKhai, c.lhxnk LHinh,c.SHKB MKBac,c.tk_no TKThu, c.tk_co TKTNS, c.trang_thai TTrang, c.ma_cqthu MCQT, c.ttien TTIEN" & _
                                    " FROM tcs_baolanh_hdr c where 1=1 "
            End If
            strSQL = strSQL & " and c.trang_thai in ('01') and ma_ks<>0  " & strDate & strWhereMaCN & " order by c.so_ct"
            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return result
    End Function
    Public Function BC_DoiChieu_Bao_Lanh_ChiNhanh(ByVal type1 As String, ByVal type2 As String, _
                                           ByVal strDate As String, ByVal strMaCN As String) As DataSet
        Dim strSQL As String
        Dim strQueryHQ As String
        Dim strQueryNH As String
        Dim strQueryHeader As String
        Dim strQueryFooter As String
        Dim strDateWhere As String
        Dim strDateWhere2 As String
        Dim strWhereMaCN As String
        Dim strWhereMaCN2 As String
        Dim strCheckHS As String
        Dim result As New DataSet
        Dim dsNganHang As DataSet
        If Not strMaCN Is Nothing And Not strMaCN = "" Then
            If strMaCN.IndexOf(";") > 0 Then
                strCheckHS = strMaCN.Split(";")(1).ToString
                strMaCN = strMaCN.Split(";")(0).ToString

                If strCheckHS = "2" Then
                    strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & strMaCN & "')"
                    strWhereMaCN2 = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv, tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & strMaCN & "')"
                Else
                    strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
                    strWhereMaCN2 = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
                End If

            End If
            strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
            strWhereMaCN2 = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
            'End If
            '    End If

        End If

        If strDate.Trim <> "" Then
            strDateWhere = " AND to_char(a.ngay_bn,'yyyyMMdd') = " & ConvertDateToNumber(strDate).ToString
            strDateWhere2 = " AND to_char(b.ngay_bn,'yyyyMMdd') = " & ConvertDateToNumber(strDate).ToString
        End If
        If type1 = "M81" Or type1 = "M41" Then
            strDateWhere = strDateWhere & " and c.trang_thai='01'"
        ElseIf type1 = "M91" Or type1 = "M51" Then
            strDateWhere = strDateWhere & " and c.trang_thai='04'"
        End If

        Try

            strSQL = "SELECT ROWNUM stt,   " & _
                     " (select ma_cn from tcs_dm_chinhanh cn,tcs_dm_nhanvien nv where cn.id = nv.ma_cn and nv.ma_nv = a.ma_nv) ma_cn," & _
                      " (select cn.name from tcs_dm_chinhanh cn,tcs_dm_nhanvien nv where cn.id = nv.ma_cn and nv.ma_nv = a.ma_nv) ten_cn," & _
                      "a.ma_cqthu mcqt,a.so_ct,a.ttien ttien_truyen,a.so_bl,a.ten_nnthue, case  b.kq_dc when '22' then 0 else b.ttien  end ttien_nhan,nvl(a.ttien,0)- nvl(b.ttien,0) CLech" & _
                     " FROM   tcs_dchieu_nhhq_hdr a, tcs_dchieu_hqnh_hdr b, tcs_baolanh_hdr c " & _
                     " where a.transaction_id=b.transaction_id and a.so_ct= c.so_ct and a.transaction_type = '" & type1 & "' " & _
                     " and a.so_ct=b.so_ct and  a.accept_yn='Y' and (b.accept_yn='Y' or b.accept_yn is null) and b.transaction_type='" & type2 & "'  " & strDateWhere & strWhereMaCN

            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
        Finally

        End Try

        Return result
    End Function

    Public Function getBCCTietHuyTThueXNKhau(ByVal type As String, ByVal strDate As String, ByVal strMaCN As String, ByVal ma_nt As String) As DataSet
        Dim strSQL As String
        Dim strWhereMaCN As String = ""
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim strMaNT As String = ""
        Dim strCheckHS As String

        If strDate.Trim <> "" Then
            strDate = " AND to_char(a.ngay_bn,'yyyyMMdd') = " & ConvertDateToNumber(strDate).ToString
        End If
        '  strMaNT = "AND a.ma_nt = '" & ma_nt & "'"
        If Not strMaCN Is Nothing And Not strMaCN = "" Then
            If strMaCN.IndexOf(";") > 0 Then
                strCheckHS = strMaCN.Split(";")(1).ToString
                strMaCN = strMaCN.Split(";")(0).ToString
            End If
          

            If strCheckHS = "2" Then
                strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & strMaCN & "')"
            Else
                strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & strMaCN & "')"
            End If

        End If
        Try
            strSQL = " Select a.id, (a.kyhieu_ct || '-' || a.so_ct) KHSCTu, a.ten_nnthue TDVNThue, a.ma_nnthue MSThue, a.ma_hq_ph MHQuanPH," & _
                     " a.so_tk STKhai,a.ngay_tk NDKTKhai ,a.SHKB MKBac,a.ttien STien,a.ly_do_huy ldohuy" & _
                     " FROM tcs_dchieu_nhhq_hdr a " & _
                     " Where a.accept_yn = 'Y' " & strDate & " And a.transaction_type='" & type & "'" & _
                     strWhereMaCN & _
                     " order by a.so_ct"

            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return result
    End Function

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Gets the BC TH so du bao lanh XN khau.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/28/2012 00:00:00
    '''  Mục đích:	    Lấy dữ liệu cho báo cáo tổng hợp số dư bảo lãnh thuế xuất nhập khẩu
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="p_dtDenNgay">The P_DT den ngay.</param>
    ''' <returns>DataSet.</returns>
    ''' <exception cref="System.Exception"></exception>
    Public Function getBCTHSoDuBaoLanhXNKhau(ByVal p_dtTuNgay As String, ByVal p_dtDenNgay As String, ByVal p_strMaCN As String, ByVal ma_nt As String, ByVal p_strTinhTrang As String) As DataSet
        Dim strSQL As String
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim v_strWhere As String
        Dim strCheckHS As String
        If p_dtTuNgay.Trim <> "" Then
            'v_strWhere = " AND a.ngay_kb >= " & ConvertDateToNumber(p_dtTuNgay).ToString
        End If
        If p_dtTuNgay.Trim <> "" Then
            v_strWhere = " AND a.ngay_ct >= " & ConvertDateToNumber(p_dtTuNgay).ToString
        End If
        If p_dtDenNgay.Trim <> "" Then
            v_strWhere += " AND a.ngay_ct <= " & ConvertDateToNumber(p_dtDenNgay).ToString
        End If
        If Not p_strMaCN Is Nothing And Not p_strMaCN = "" Then
            If p_strMaCN.IndexOf(";") > 0 Then
                strCheckHS = p_strMaCN.Split(";")(1).ToString
                p_strMaCN = p_strMaCN.Split(";")(0).ToString
            End If
            If strCheckHS = "2" Then
                v_strWhere += " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & p_strMaCN & "')"
            Else
                v_strWhere += " AND b.ma_cn ='" & p_strMaCN & "'"
            End If
        End If
        If p_strTinhTrang = "04" Then
            v_strWhere += " AND a.trang_thai='04' AND a.MA_KS<>0"
        ElseIf p_strTinhTrang = "01" Then ' còn hiệu lực
            v_strWhere += " AND (a.trang_thai='01' or a.trang_thai='02')  AND TRUNC(a.ngay_ketthuc) >= TRUNC(SYSDATE)"
        ElseIf p_strTinhTrang = "02" Then ' hết hiệu lực
            v_strWhere += " AND (a.trang_thai='01' or a.trang_thai='02')   AND TRUNC(a.ngay_ketthuc) < TRUNC(SYSDATE)"
        ElseIf p_strTinhTrang = "00" Then ' tất cả
            v_strWhere += " and (a.trang_thai='01' or a.trang_thai='02' or (a.trang_thai='04' and a.ma_ks<>0))"
        End If
      


        'If p_strMaCN.Trim <> "" Then
        '    v_strWhere += " AND b.ma_cn ='" & p_strMaCN & "'"
        'End If
        'v_strWhere += "AND a.ma_nt = '" & ma_nt & "'"
        Try
            strSQL = " SELECT" & _
                            " ROWNUM STT," & _
                            " a1.MA_CN AS MA_CN," & _
                            " a1.SO_LUONG AS SO_LUONG," & _
                            " a1.SO_TIEN AS SO_TIEN," & _
                            " b1.name AS TEN_CN" & _
                      " FROM" & _
                        "(" & _
                            " SELECT b.ma_cn as MA_CN," & _
                                " COUNT(*) AS SO_LUONG," & _
                                " SUM(a.ttien) AS SO_TIEN" & _
                            " FROM" & _
                                " tcs_baolanh_hdr a," & _
                                " tcs_dm_nhanvien b" & _
                            " WHERE 1=1" & _
                            " AND a.ma_nv = b.ma_nv" & _
                            v_strWhere & _
                            " GROUP BY b.ma_cn" & _
                        ") a1, tcs_dm_chinhanh b1" & _
                        " WHERE " & _
                        " a1.ma_cn = b1.id"

            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return result
    End Function
    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Gets the BC TH doanh so bao lanh XN khau.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/28/2012 00:00:00
    '''  Mục đích:	    Lấy dữ liệu cho báo cáo tổng hợp doanh so bảo lãnh thuế xuất nhập khẩu
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="p_dtDenNgay">The P_DT den ngay.</param>
    ''' <returns>DataSet.</returns>
    ''' <exception cref="System.Exception"></exception>
    Public Function getBCTHDoanhSoBaoLanhXNKhau(ByVal p_dtDenNgay As String, ByVal p_strMaCN As String, ByVal ma_NT As String) As DataSet
        Dim strSQL As String
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim v_strWhere As String = ""
        Dim strCheckHS As String
        If p_dtDenNgay.Trim <> "" Then
           
            v_strWhere += " AND TRUNC(a.ngay_ketthuc) >= TRUNC(TO_DATE('" & ConvertDateToNumber(p_dtDenNgay).ToString & "','yyyyMMdd'))"
        End If
        Dim v_strWhere2 As String = ""
        If Not p_strMaCN Is Nothing And Not p_strMaCN = "" Then
            If p_strMaCN.IndexOf(";") > 0 Then
                strCheckHS = p_strMaCN.Split(";")(1).ToString
                p_strMaCN = p_strMaCN.Split(";")(0).ToString
            End If


            If strCheckHS = "2" Then
                ' v_strWhere += " AND b.branch_id ='" & p_strMaCN & "'"
                v_strWhere2 += " and b1.branch_id= '" & p_strMaCN & "'"
            Else
                v_strWhere2 += " AND b1.ID ='" & p_strMaCN & "'"
                ' v_strWhere = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & p_strMaCN & "')"
            End If

        End If

        Try
            strSQL = " SELECT" & _
                            " ROWNUM STT," & _
                            " a1.MA_CN AS MA_CN," & _
                            " a1.SO_LUONG AS SO_LUONG," & _
                            " a1.SO_TIEN AS SO_TIEN," & _
                            " b1.name AS TEN_CN" & _
                      " FROM" & _
                        "(" & _
                            " SELECT b.ma_cn as MA_CN," & _
                                " COUNT(*) AS SO_LUONG," & _
                                " SUM(a.ttien) AS SO_TIEN" & _
                            " FROM" & _
                                " tcs_baolanh_hdr a," & _
                                " tcs_dm_nhanvien b" & _
                            " WHERE 1=1" & _
                            " AND a.ma_nv = b.ma_nv" & _
                            " AND (a.trang_thai = '01' or a.trang_thai = '02' ) " & _
                            v_strWhere & _
                            " GROUP BY b.ma_cn" & _
                        ") a1, tcs_dm_chinhanh b1" & _
                        " WHERE " & _
                        " a1.ma_cn = b1.id" & v_strWhere2

            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return result
       
    End Function

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Gets the BCC tiet so du bao lanh XN khau.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/28/2012 00:00:00
    '''  Mục đích:	    Lấy dữ liệu cho báo cáo chi tiết số dư bảo lãnh thuế xuất nhập khẩu
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="p_dtDenNgay">The P_DT den ngay.</param>
    ''' <returns>DataSet.</returns>
    ''' <exception cref="System.Exception"></exception>
    Public Function getBCCTietSoDuBaoLanhXNKhau(ByVal p_dtTuNgay As String, ByVal p_dtDenNgay As String, ByVal p_strMaCN As String, ByVal ma_nt As String, ByVal p_strTinhTrang As String) As DataSet
        Dim strSQL As String
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim v_strWhere As String = ""
        Dim strCheckHS As String
        If p_dtTuNgay.Trim <> "" Then
            v_strWhere = " AND a.ngay_ct >= " & ConvertDateToNumber(p_dtTuNgay).ToString
        End If
        If p_dtDenNgay.Trim <> "" Then
            v_strWhere += " AND a.ngay_ct <= " & ConvertDateToNumber(p_dtDenNgay).ToString
        End If
        If Not p_strMaCN Is Nothing And Not p_strMaCN = "" Then
            If p_strMaCN.IndexOf(";") > 0 Then
                strCheckHS = p_strMaCN.Split(";")(1).ToString
                p_strMaCN = p_strMaCN.Split(";")(0).ToString
            End If


            If strCheckHS = "2" Then
                ' v_strWhere += " AND b.branch_id ='" & p_strMaCN & "'"
                v_strWhere += " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & p_strMaCN & "')"
            Else
                v_strWhere += " AND b.ma_cn ='" & p_strMaCN & "'"
            End If
        End If

        'If p_strMaCN.Trim <> "" Then
        '    v_strWhere += " AND b.ma_cn ='" & p_strMaCN & "'"
        'End If

        v_strWhere += " AND a.ma_nv = b.ma_nv and a.ma_nv_huy= c.ma_nv(+) "
        If p_strTinhTrang = "04" Then
            v_strWhere += " AND a.trang_thai='04' AND a.MA_KS<>0"
        ElseIf p_strTinhTrang = "01" Then ' còn hiệu lực
            v_strWhere += " AND a.trang_thai='01' and a.trang_thai_tt=0 AND TRUNC(a.ngay_ketthuc) >= TRUNC(SYSDATE)"
        ElseIf p_strTinhTrang = "02" Then ' hết hiệu lực
            v_strWhere += " AND a.trang_thai='01' and a.trang_thai_tt=0  AND TRUNC(a.ngay_ketthuc) < TRUNC(SYSDATE)"
        ElseIf p_strTinhTrang = "03" Then ' hết hiệu lực
            v_strWhere += " AND a.trang_thai='01' and a.trang_thai_tt=1"
        ElseIf p_strTinhTrang = "00" Then ' tất cả
            v_strWhere += " and (a.trang_thai='01' or (a.trang_thai='04' and a.ma_ks<>0))"
        End If
        Try
            strSQL = " SELECT" & _
                            " ROWNUM STT," & _
                            " b.ma_cn AS MA_CN," & _
                            " a.so_bl AS SO_BL," & _
                            " a.cif AS CIF_NUMBER," & _
                            " a.ten_nnthue AS TEN_NNT," & _
                            " a.ma_nnthue AS MST," & _
                            " a.So_ct AS SO_CT," & _
                            " a.ttien AS SO_DU," & _
                            " b.ten_dn AS NGUOI_LAP," & _
                            " C.ten_dn AS NGUOI_HUY," & _
                            " a.ten_cqthu AS TEN_CQT," & _
                            " to_char(a.ngay_huy,'dd/MM/yyyy') AS NGAY_HUY," & _
                            " to_char(a.ngay_batdau,'dd/MM/yyyy') AS TU_NGAY," & _
                            " to_char(a.ngay_ketthuc,'dd/MM/yyyy') AS DEN_NGAY," & _
                            "(CASE a.trang_thai when '04' then '4' else " & _
                            "(CASE a.TRANG_THAI_TT when 1 then '5' else " & _
                            " case WHEN TRUNC (a.ngay_ketthuc) >= TRUNC (SYSDATE) THEN '1'" & _
                             "ELSE '2' END end) end) AS tinh_trang FROM" & _
                            " tcs_baolanh_hdr a," & _
                            " tcs_dm_nhanvien b," & _
                            " tcs_dm_nhanvien C" & _
                      " WHERE 1=1 " & v_strWhere

            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return result
    End Function
    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Gets the BCC tiet so du bao lanh XN khau.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/28/2012 00:00:00
    '''  Mục đích:	    Lấy dữ liệu cho báo cáo chi tiết doanh số bảo lãnh thuế xuất nhập khẩu
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="p_dtDenNgay">The P_DT den ngay.</param>
    ''' <returns>DataSet.</returns>
    ''' <exception cref="System.Exception"></exception>
    Public Function getBCCTietDoanhSoBaoLanhXNKhau(ByVal p_dtDenNgay As String, ByVal p_strMaCN As String, ByVal maNT As String) As DataSet
        Dim strSQL As String
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim v_strWhere As String = ""
        Dim strCheckHS As String


        If p_dtDenNgay.Trim <> "" Then
            v_strWhere += " AND TRUNC(a.ngay_ketthuc)>= TRUNC(TO_DATE('" & ConvertDateToNumber(p_dtDenNgay).ToString & "','yyyyMMdd'))"
        End If
        If Not p_strMaCN Is Nothing And Not p_strMaCN = "" Then
            If p_strMaCN.IndexOf(";") > 0 Then
                strCheckHS = p_strMaCN.Split(";")(1).ToString
                p_strMaCN = p_strMaCN.Split(";")(0).ToString
            End If


            If strCheckHS = "2" Then
                ' v_strWhere += " AND b.branch_id ='" & p_strMaCN & "'"
                v_strWhere += " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & p_strMaCN & "')"
            Else
                v_strWhere += " AND b.ma_cn ='" & p_strMaCN & "'"
                ' v_strWhere = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & p_strMaCN & "')"
            End If

        End If

        v_strWhere += " AND (a.trang_thai = '01' or a.trang_thai = '02')"
        v_strWhere += " AND a.ma_nv = b.ma_nv"
        'v_strWhere += " AND a.ma_nt ='" & maNT & "'"
        Try
            strSQL = " SELECT" & _
                            " ROWNUM STT," & _
                            " b.ma_cn AS MA_CN," & _
                            " a.so_bl AS SO_BL," & _
                            " a.cif AS CIF_NO," & _
                            " a.ten_nnthue AS TEN_NNT," & _
                            " a.ma_nnthue AS MST," & _
                            " a.so_CT AS SO_CT," & _
                            " a.ttien AS SO_DU," & _
                            " b.ten_dn AS NGUOI_LAP," & _
                            " a.ten_cqthu AS TEN_CQT," & _
                            " to_char(a.ngay_batdau,'dd/MM/yyyy') AS TU_NGAY," & _
                            " to_char(a.ngay_ketthuc,'dd/MM/yyyy') AS DEN_NGAY," & _
                            " case WHEN TRUNC (a.ngay_ketthuc) >= TRUNC (SYSDATE) THEN '1'" & _
                             "ELSE '2' END  AS  TINH_TRANG " & _
                      " FROM" & _
                            " tcs_baolanh_hdr a," & _
                            " tcs_dm_nhanvien b" & _
                      " WHERE 1=1 " & v_strWhere

            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return result
    End Function

    Public Function getBCCTietHuyTThueXNKhau(ByVal type As String, ByVal strDate As String, ByVal ma_nt As String) As DataSet
        Dim strSQL As String
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim strMaNT As String

        If strDate.Trim <> "" Then
            strDate = " AND to_char(a.ngay_bn,'yyyyMMdd') = " & ConvertDateToNumber(strDate).ToString
        End If

        strMaNT = "AND a.ma_nt = '" & ma_nt & "'"

        Try
            strSQL = " Select a.id, (a.kyhieu_ct || '-' || a.so_ct) KHSCTu, a.ten_nnthue TDVNThue, a.ma_nnthue MSThue, a.ma_hq_ph MHQuanPH," & _
                     " a.so_tk STKhai,a.ngay_tk NDKTKhai ,a.SHKB MKBac,a.ttien STien,a.ly_do_huy ldohuy" & _
                     " FROM tcs_dchieu_nhhq_hdr a " & _
                     " Where 1=1 " & strDate & strMaNT & " And a.transaction_type='" & type & "'"

            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return result
    End Function

    Public Function getCountMonHuyTThueXNKhau(ByVal type As String, ByVal strDate As String, ByVal strMaCN As String, ByVal ma_nt As String) As String
        Dim strSQL As String
        Dim strWhereMaCN As String
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim resultStr As String = "0"
        Dim strMaNT As String

        If Not strMaCN Is Nothing And Not strMaCN = "" Then
            If strMaCN.Length > 3 Then
                strMaCN = strMaCN.Substring(0, 3)
            End If
            strWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn in (select cn.id from tcs_dm_chinhanh cn where TO_NUMBER(cn.branch_id)= " & CInt(strMaCN) & ")) "
        End If

        If strDate.Trim <> "" Then
            strDate = " AND a.ngay_ct >= " & ConvertDateToNumber(strDate).ToString
        End If

        strMaNT = "AND a.ma_nt = '" & ma_nt & "'"
        Try
            'strSQL = " Select count(*) smon" & _
            '         " FROM tcs_dchieu_nhhq_dtl b" & _
            '         " Where b.hdr_id IN( select a.id" & _
            '                             " FROM tcs_dchieu_nhhq_hdr a " & _
            '                             " where 1=1 " & strDate & " And a.transaction_type='" & type & "')"
            strSQL = " select count(0) smon " & _
                                         " FROM tcs_dchieu_nhhq_hdr a " & _
                                         " where a.accept_yn = 'Y' " & strDate & strMaNT & " And a.transaction_type='" & type & "' " & strWhereMaCN


            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If dsNganHang.Tables(0).Rows.Count > 0 Then
                result = dsNganHang
                resultStr = result.Tables(0).Rows.Item(0).Item(0).ToString()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return resultStr
    End Function
    Public Function BC_LietKe_GD_DoiChieu(ByVal strMaCN As String, ByVal StrDate As String) As DataSet
        Dim strSQL As String
        Dim dsNganHang As DataSet
        Dim result As New DataSet
        Dim strWhereMaCN As String
        Dim strWhereMaCN2 As String
        Dim strCheckHS As String
        If Not strMaCN Is Nothing And Not strMaCN = "" Then
            If strMaCN.IndexOf(";") > 0 Then
                strCheckHS = strMaCN.Split(";")(1).ToString
                strMaCN = strMaCN.Split(";")(0).ToString

                If strCheckHS = "2" Then
                    strWhereMaCN = " and d.branch_id= '" & strMaCN & "'"
                Else
                    strWhereMaCN = " and d.id= '" & strMaCN & "'"
                End If

            End If
            strWhereMaCN = " and d.id= '" & strMaCN & "'"
            'End If
            '    End If

        End If

        If StrDate.Trim <> "" Then
            strWhereMaCN = strWhereMaCN & " AND hdr.ngay_kb = " & ConvertDateToNumber(StrDate).ToString
        End If
        Try
            strSQL = "select hdr.kyhieu_ct|| '-'|| hdr.so_ct_nh KyHieu_SoCt, p.ref_seq_no SoThamChieu, hdr.ttien So_Tien, d.name  Chi_Nhanh " & _
                    "from tcs_dchieu_hqnh_hdr a,tcs_dchieu_nhhq_hdr b," & _
                    "tcs_dm_chinhanh d, tcs_dm_nhanvien n," & _
                    "tcs_ctu_hdr hdr, tcs_log_payment p   where " & _
                    "a.transaction_id = b.transaction_id" & _
                    " and d.id=n.ma_cn" & _
                    " and n.ma_nv=b.ma_nv" & _
                    " and b.so_ct=hdr.so_ct" & _
                    " and b.accept_yn='Y' " & _
                    " and p.rm_ref_no= hdr.rm_ref_no" & _
                    " and p.product_code= hdr.kenh_tt" & _
                    " and a.kq_dc= '22'" & strWhereMaCN


            dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not dsNganHang Is Nothing Then
                result = dsNganHang
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

        Return result
    End Function

    Public Function getBC_DC_TONGHOP_PBBN(ByVal sDate As String, ByVal sMaCN As String) As DataSet
        Dim dsReturn As DataSet
        Dim sSql As String = String.Empty
        Dim sWhereMaCN As String = String.Empty
        Dim sCheckHoiSo As String = String.Empty

        Dim dDoiChieu As Date = DateTime.Parse(sDate)
        Dim sToDate As String = dDoiChieu.AddDays(1).ToString("yyyy-MM-dd")

        If Not sMaCN Is Nothing And Not sMaCN = "" Then
            If sMaCN.IndexOf(";") > 0 Then
                sCheckHoiSo = sMaCN.Split(";")(1).ToString
                sMaCN = sMaCN.Split(";")(0).ToString
            End If
            If sCheckHoiSo = "2" Then
                sWhereMaCN = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & sMaCN & "')"
            Else
                sWhereMaCN = " and b.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & sMaCN & "')"
            End If
        End If

        Try
            sSql = "SELECT ROWNUM STT, MCQTHU, SMONTRUYEN, STIENTRUYEN, SMONNHAN, STIENNHAN, NVL (STIENTRUYEN, 0) - NVL (STIENNHAN, 0) CLECH " & _
                    "FROM " & _
                    "( " & _
                    "    SELECT MA_CQTHU MCQTHU, SUM (SMONTRUYEN) SMONTRUYEN, SUM (STIENTRUYEN) STIENTRUYEN, SUM (SMONNHAN) SMONNHAN,SUM (STIENNHAN) STIENNHAN " & _
                    "    FROM ( " & _
                    "        SELECT B.MA_DVQL MA_CQTHU, COUNT (1) SMONTRUYEN, SUM (B.TT_NT_TONGTIEN_VND) STIENTRUYEN, 0 SMONNHAN, 0 STIENNHAN " & _
                    "         FROM TCS_DCHIEU_THUE_PHI_BO_NGANH A, TCS_THUE_PHI_BO_NGANH B " & _
                    "         WHERE  A.SO_CT = B.SO_CT " & _
                    "            AND TO_CHAR (TO_TIMESTAMP (A.NGAY_TN_CT, 'YYYY-MM-DD""T""HH24:MI:SS""Z""'), 'yyyy-MM-dd HH24:MI:SS') >= '" & sDate & " " & strGIO_DC & "' " & _
                    "            AND TO_CHAR(TO_TIMESTAMP (A.NGAY_TN_CT, 'YYYY-MM-DD""T""HH24:MI:SS""Z""'), 'yyyy-MM-dd HH24:MI:SS') <= '" & sToDate & " " & strGIO_DC & "' " & sWhereMaCN & _
                    "         GROUP BY B.MA_DVQL " & _
                    "        UNION ALL " & _
                    "        SELECT B.MA_DVQL MA_CQTHU, 0 SMONTRUYEN, 0 STIENTRUYEN, COUNT (1) SMONNHAN, SUM (A.TT_NT_TONGTIEN_VND) STIENNHAN " & _
                    "         FROM TCS_KQDC_PHI_BO_NGANH A, TCS_DCHIEU_THUE_PHI_BO_NGANH C, TCS_THUE_PHI_BO_NGANH B " & _
                    "         WHERE A.SO_CT = C.SO_CT AND A.SO_CT = B.SO_CT " & _
                    "               AND TO_CHAR (TO_TIMESTAMP (A.NGAY_TN_CT, 'YYYY-MM-DD""T""HH24:MI:SS""Z""' ), 'yyyy-MM-dd HH24:MI:SS') >= '" & sDate & " " & strGIO_DC & "' " & _
                    "               AND TO_CHAR (TO_TIMESTAMP (A.NGAY_TN_CT, 'YYYY-MM-DD""T""HH24:MI:SS""Z""'), 'yyyy-MM-dd HH24:MI:SS') <= '" & sToDate & " " & strGIO_DC & "' " & sWhereMaCN & _
                    "         GROUP BY B.MA_DVQL " & _
                    "        ) " & _
                    "    GROUP BY MA_CQTHU " & _
                    ")"
            dsReturn = DataAccess.ExecuteReturnDataSet(sSql, CommandType.Text)

        Catch ex As Exception
        Finally
        End Try

        Return dsReturn
    End Function

    Public Function getBC_DC_CHITIET_PBBN(ByVal sDate As String, ByVal sMaCN As String) As DataSet
        Dim dsReturn As DataSet
        Dim sSql As String = String.Empty
        Dim sWhereMaCN As String = String.Empty
        Dim sCheckHoiSo As String = String.Empty

        Dim dDoiChieu As Date = DateTime.Parse(sDate)
        Dim sToDate As String = dDoiChieu.AddDays(1).ToString("yyyy-MM-dd")

        If Not sMaCN Is Nothing And Not sMaCN = "" Then
            If sMaCN.IndexOf(";") > 0 Then
                sCheckHoiSo = sMaCN.Split(";")(1).ToString
                sMaCN = sMaCN.Split(";")(0).ToString
            End If
            If sCheckHoiSo = "2" Then
                sWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv,tcs_dm_chinhanh d where d.id = nv.ma_cn and d.branch_id= '" & sMaCN & "')"
            Else
                sWhereMaCN = " and a.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn = '" & sMaCN & "')"
            End If
        End If

        sSql = "SELECT TO_CHAR(A.SO_HS) SO_HS,A.MA_DVQL MA_DV_QLY,A.KYHIEU_CT_PT KYHIEU_CTU,A.NNT_MST MST,A.NNT_TEN_DV TEN_NNTHUE,A.TT_NT_MA_NT NGUYENTE," & _
                    "A.TT_NT_TYGIA TYGIA, A.TT_NT_TONGTIEN_NT TONGTIEN_NGOAITE,A.TT_NT_TONGTIEN_VND TONGTIEN_VND, " & _
                    "A.TAIKHOAN_NT_MA_NH_TH MA_NH_THUHUONG,A.TAIKHOAN_NT_TK_TH TAIKHOAN_THUHUONG,A.TRANGTHAI_CT TRANG_THAI " & _
                "FROM TCS_THUE_PHI_BO_NGANH A, TCS_DCHIEU_THUE_PHI_BO_NGANH DC, TCS_KQDC_PHI_BO_NGANH KQ " & _
                "WHERE A.SO_CT = DC.SO_CT(+) AND A.SO_CT = KQ.SO_CT(+) " & _
                    "AND A.TRANGTHAI_CT IN ('01','02') AND TRANGTHAI_XULY = 1 " & _
                    "AND TO_CHAR (TO_TIMESTAMP (DC.NGAY_TN_CT, 'YYYY-MM-DD""T""HH24:MI:SS""Z""' ), 'yyyy-MM-dd HH24:MI:SS') >= '" & sDate & " " & strGIO_DC & "' " & _
                    "AND TO_CHAR (TO_TIMESTAMP (DC.NGAY_TN_CT, 'YYYY-MM-DD""T""HH24:MI:SS""Z""'), 'yyyy-MM-dd HH24:MI:SS') <= '" & sToDate & " " & strGIO_DC & "' " & sWhereMaCN

        dsReturn = DataAccess.ExecuteReturnDataSet(sSql, CommandType.Text)

        Return dsReturn
    End Function
End Class
