'Imports VBOracleLib

'Namespace DanhMuc.CQThu
'    Public Class daCQThu

'        Public Function Insert(ByVal objCQThu As infCQThu) As Boolean
'            Dim cnCQThu As DataAccess
'            Dim blnResult As Boolean = True
'            Dim strSql As String = "INSERT INTO TCS_DM_CQTHU (Ma_CQThu,Ten,Ma_Dthu,SHKB,MA_HQ,MA_QLT) " & _
'                                "VALUES ('" & objCQThu.Ma_CQThu & "','" & _
'                                objCQThu.Ten & "','" & objCQThu.Ma_Dthu & "','" & objCQThu.Ma_Dthu & "','" & objCQThu.Ma_HQ & "','" & objCQThu.Ma_QLT & "')"

'            Try
'                cnCQThu = New DataAccess
'                cnCQThu.ExecuteNonQuery(strSql, CommandType.Text)
'            Catch ex As Exception
'                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi thêm mới Cơ quan thu")
'                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới cơ quan thu: " & ex.ToString)
'                blnResult = False
'                Throw ex
'            Finally
'                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
'            End Try
'            Return blnResult
'        End Function

'        Public Function Update(ByVal objCQThu As infCQThu) As Boolean
'            Dim cnCQThu As DataAccess
'            Dim blnResult As Boolean = True
'            Dim strSql As String = "UPDATE TCS_DM_CQTHU SET TEN='" & objCQThu.Ten & "', Ma_Dthu='" & objCQThu.Ma_Dthu & "',shkb ='" & objCQThu.Ma_Dthu & "',ma_hq='" & objCQThu.Ma_HQ & "',ma_qlt='" & objCQThu.Ma_QLT & "'  WHERE Ma_CQThu='" & objCQThu.Ma_CQThu & "'"

'            Try
'                cnCQThu = New DataAccess
'                cnCQThu.ExecuteNonQuery(strSql, CommandType.Text)
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Lỗi khi cập nhật Cơ quan thu")
'                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật cơ quan thu: " & ex.ToString)
'                blnResult = False
'                Throw ex
'            Finally
'                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
'            End Try
'            Return blnResult
'        End Function

'        Public Function Delete(ByVal objCQThu As infCQThu) As Boolean
'            Dim cnCQThu As DataAccess
'            Dim blnResult As Boolean = True
'            Dim strSql As String = "DELETE FROM TCS_DM_CQTHU " & _
'                           "WHERE " & "Ma_CQTHU='" & _
'                           objCQThu.Ma_CQThu & "'"
'            Try
'                cnCQThu = New DataAccess
'                cnCQThu.ExecuteNonQuery(strSql, CommandType.Text)
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Lỗi khi xóa Cơ quan thu")
'                Throw ex
'                'LogDebug.Writelog("Có lỗi xảy ra khi xoá cơ quan thu: " & ex.ToString)
'                blnResult = False
'            Finally
'                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
'            End Try
'            Return blnResult
'        End Function

'        Public Function Load(ByVal strMa_CQThu As String) As infCQThu
'            Dim objCQThu As New infCQThu
'            Dim cnCQThu As DataAccess
'            Dim drCQThu As IDataReader
'            Dim strSQL As String = "Select MA_CQTHU, TEN,Ma_Dthu From TCS_DM_CQTHU " & _
'                        "Where MA_CQTHU='" & strMa_CQThu.ToUpper() & "'"
'            Try
'                cnCQThu = New DataAccess
'                drCQThu = cnCQThu.ExecuteDataReader(strSQL, CommandType.Text)
'                If drCQThu.Read Then
'                    objCQThu = New infCQThu
'                    objCQThu.Ma_CQThu = strMa_CQThu
'                    objCQThu.Ten = drCQThu("Ten").ToString()
'                End If
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Cơ quan thu")
'                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cơ quan thu từ CSDL: " & ex.ToString)
'                Throw ex
'            Finally
'                If Not drCQThu Is Nothing Then drCQThu.Close()
'                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
'            End Try
'            Return objCQThu
'        End Function

'        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As Object
'            Dim cnCQThu As DataAccess
'            Dim dsCQThu As DataSet
'            Dim strSQL As String = "SELECT   c.ma_cqthu, c.ten, c.ma_dthu, d.ten AS ten_diemthu,c.ma_hq,c.ma_qlt " & _
'                                    "    FROM tcs_dm_cqthu c, tcs_dm_diemthu d " & _
'                                    "   WHERE c.ma_dthu = d.ma_dthu " & WhereClause & _
'                                    "ORDER BY ma_cqthu "

'            Try
'                cnCQThu = New DataAccess
'                dsCQThu = cnCQThu.ExecuteReturnDataSet(strSQL, CommandType.Text)
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Cơ quan thu")
'                Throw ex
'                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cơ quan thu từ CSDL: " & ex.ToString)
'            Finally
'                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
'            End Try

'            Return dsCQThu
'        End Function

'        Public Function CheckExist(ByVal objCQThu As infCQThu) As Boolean
'            Return Load(objCQThu.Ma_CQThu).Ma_CQThu <> ""
'        End Function

'        Public Shared Function Check2Del(ByVal objCQThu As infCQThu) As Byte
'            '---------------------------------------------------------------
'            ' Mục đích: Kiểm tra CQT có thể xoá không
'            ' Tham số: objCQThu: cơ quan cần kiểm tra
'            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
'            ' Ngày viết: 26/10/2007
'            ' Người viết: Ngô Minh Trọng
'            ' ----------------------------------------------------
'            Dim cnCQThu As DataAccess
'            Dim drQThu As IDataReader
'            Dim strSQL As String
'            Dim bytResult As Byte = 0

'            If Not drQThu Is Nothing Then drQThu.Close()
'            If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
'            ' Trả lại kết quả cho hàm
'            Return bytResult
'        End Function
'        Public Function CheckDelete(ByVal strMA_CQthu As String) As Object
'            Dim cnCQT As DataAccess
'            Dim dsCQT As DataSet

'            Try
'                Dim strSQL As String = "select ma_cqthu from (select ma_cqthu from tcs_ctu_hdr union all select ma_cqthu from tcs_baolanh_hdr) " & _
'                        "Where ma_cqthu='" & strMA_CQthu & "'"

'                cnCQT = New DataAccess
'                dsCQT = cnCQT.ExecuteReturnDataSet(strSQL, CommandType.Text)
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
'                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
'                Throw ex
'            Finally
'                If Not cnCQT Is Nothing Then cnCQT.Dispose()
'            End Try

'            Return dsCQT
'        End Function
'    End Class

'End Namespace
