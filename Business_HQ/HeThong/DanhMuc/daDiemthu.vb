﻿Imports VBOracleLib
Namespace DanhMuc.DiemThu
    Public Class daDiemthu
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        '-------------------
        ' Lop thuc hien thao tac diem thu
        ' Người tạo: Ngocla
        ' Ngày tạo: 6/12/07
        ' Tham số :
        '-------------------
        Public Function Save(ByVal sql)
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                conn.ExecuteNonQuery(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi thêm mới điểm thu")
                'LogDebug.Writelog("Lỗi khi thực hiện ghi mới điểm thu :" & ex.ToString)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function ExitsDiemthu(ByVal sqlma As String, ByVal sqlten As String) As Byte
            Dim conn As DataAccess
            Dim rd As IDataReader
            Dim byr As Byte = 0
            Try

                conn = New DataAccess
                rd = conn.ExecuteDataReader(sqlma, CommandType.Text)
                If rd.Read Then
                    byr = 1
                    Return byr
                End If
                rd.Close()
                rd = conn.ExecuteDataReader(sqlten, CommandType.Text)
                If rd.Read Then
                    byr = 2
                    Return byr
                End If
                Return byr
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi kiểm tra tồn tại điểm thu")
                'LogDebug.Writelog("Lỗi khi thực hiện kiem tra tồn tại mã điểm thu :" & ex.ToString)
                Throw ex
            Finally

                If Not rd Is Nothing Then
                    rd.Close()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function loadDetail(ByVal sql) As DataSet
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin điểm thu")
                'LogDebug.Writelog("Lỗi khi thực hiện ghi mới điểm thu :" & ex.ToString)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try

        End Function
        Public Function DsDiemthu(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim sql As String = "SELECT   ma_dthu, ten " & _
                                    "    FROM tcs_dm_diemthu " & _
                                    "   WHERE 1 = 1 " & WhereClause & _
                                    "ORDER BY ma_dthu "
                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy danh sách điểm thu")
                'LogDebug.Writelog("Lỗi khi thực hiện Get danh sách mã điểm thu :" & ex.ToString)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try
        End Function
        Public Function DelDienthu(ByVal MaDT As String) As Boolean
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim sql As String = "Delete from TCS_DM_Diemthu where Ma_Dthu='" & MaDT & "'"
                conn.ExecuteNonQuery(sql, CommandType.Text)
                sql = " Delete from TCS_ThamSo where Ma_Dthu='" & MaDT & "'"
                conn.ExecuteNonQuery(sql, CommandType.Text)
                Return True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xóa điểm thu")
                'LogDebug.Writelog("Lỗi khi thực hiện Get danh sách mã điểm thu :" & ex.ToString)
                Return False
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function UpdateDiemthu(ByVal objdiemthu As infDiemthu)
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim sql As String = "Update TCS_DM_Diemthu Set Ten='" & objdiemthu.TenDThu & "' where Ma_Dthu='" & objdiemthu.MaDThu & "'"
                conn.ExecuteNonQuery(sql, CommandType.Text)
                Return True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi cập nhật điểm thu")
                'LogDebug.Writelog("Lỗi khi thực hiện Get danh sách mã điểm thu :" & ex.ToString)
                Return False
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        Public Function Load(ByVal strMaDThu As String) As infDiemthu
            Dim objDiemthu As New infDiemthu
            Dim cndiemthu As DataAccess
            Dim drDiemthu As IDataReader
            Dim strSQL As String = "Select Ma_DThu, Ten From TCS_DM_DIEMTHU " & _
                        "Where Ma_DTHu='" & strMaDThu & "'"
            Try
                cndiemthu = New DataAccess
                drDiemthu = cndiemthu.ExecuteDataReader(strSQL, CommandType.Text)
                If drDiemthu.Read Then
                    objDiemthu = New infDiemthu
                    objDiemthu.MaDThu = strMaDThu
                    objDiemthu.TenDThu = drDiemthu("Ten").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin điểm thu")
                'LogDebug.Writelog("Có lỗi sảy ra khi lấy thông tin loại thuế từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drDiemthu Is Nothing Then drDiemthu.Close()
                If Not cndiemthu Is Nothing Then cndiemthu.Dispose()
            End Try

            Return objDiemthu
        End Function
    End Class
End Namespace
