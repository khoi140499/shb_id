Namespace DanhMuc.DBHC
    Public Class infDBHC
        Private strXaID As String
        Private strDBHC As String
        Private strTenDBHC As String
        Private strTinhTrang As String
        Private strMaCha As String

        Sub New()
            strDBHC = ""
            strTenDBHC = ""
            strTinhTrang = 0
            strMaCha = ""
        End Sub

        Sub New(ByVal drwTaiKhoan As DataRow)
            strDBHC = drwTaiKhoan("Ma_xa").ToString().Trim()
            strTenDBHC = drwTaiKhoan("TEN").ToString().Trim()
            strTinhTrang = drwTaiKhoan("TINH_TRANG").ToString().Trim()
            strMaCha = drwTaiKhoan("ma_cha").ToString().Trim()
        End Sub
        Public Property Xa_ID() As String
            Get
                Return strXaID
            End Get
            Set(ByVal Value As String)
                strXaID = Value
            End Set
        End Property
        Public Property Ma_DBHC() As String
            Get
                Return strDBHC
            End Get
            Set(ByVal Value As String)
                strDBHC = Value
            End Set
        End Property

        Public Property TEN_DBHC() As String
            Get
                Return strTenDBHC
            End Get
            Set(ByVal Value As String)
                strTenDBHC = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTinhTrang
            End Get
            Set(ByVal Value As String)
                strTinhTrang = Value
            End Set
        End Property
        Public Property Ma_Cha() As String
            Get
                Return strMaCha
            End Get
            Set(ByVal Value As String)
                strMaCha = Value
            End Set
        End Property

        Public ReadOnly Property TINHTRANG() As String
            Get
                If strTinhTrang = "0" Then
                    Return "Dang hoat dong"
                ElseIf strTinhTrang = "1" Then
                    Return "Ngung hoat dong"
                Else
                    Return ""
                End If
            End Get
        End Property

    End Class
End Namespace
