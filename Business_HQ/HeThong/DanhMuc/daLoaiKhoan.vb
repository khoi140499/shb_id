Imports VBOracleLib
Imports Common.mdlCommon

Namespace DanhMuc.LoaiKhoan
    Public Class daLoaiKhoan
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
            Dim cnLoaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO TCS_DM_LOAI_KHOAN (LKH_ID, MA_KHOAN, TEN, GHI_CHU, TINH_TRANG) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("TCS_LKH_ID_SEQ.NEXTVAL") & ", '" & objLoaiKhoan.MA_KHOAN & "', '" & objLoaiKhoan.Ten & "', '" & objLoaiKhoan.GHICHU & "', '" & objLoaiKhoan.TINH_TRANG & "')"
            Try
                cnLoaiKhoan = New DataAccess
                cnLoaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi thêm mới loại - khoản")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
            Dim cnLoaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_LOAI_KHOAN SET MA_KHOAN='" & objLoaiKhoan.MA_KHOAN & "', TEN='" & objLoaiKhoan.Ten & "', " & _
                   "GHI_CHU='" & objLoaiKhoan.GHICHU & "', TINH_TRANG='" & objLoaiKhoan.TINH_TRANG & "' WHERE LKH_ID=" & objLoaiKhoan.LKH_ID
            Try
                cnLoaiKhoan = New DataAccess
                cnLoaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '     CTuCommon.WriteLog(ex, "Lỗi khi cập nhật loại - khoản")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
            Dim cnLoaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_LOAI_KHOAN " & _
                    "WHERE LKH_ID=" & objLoaiKhoan.LKH_ID
            Try
                cnLoaiKhoan = New DataAccess
                cnLoaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi xóa loại - khoản")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá danh mục loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strLKH_ID As String) As infLoaiKhoan
            Dim objLoaiKhoan As New infLoaiKhoan
            Dim cnLoaiKhoan As DataAccess
            Dim drLoaiKhoan As IDataReader
            Dim strSQL As String = "Select LKH_ID, MA_KHOAN, TEN, GHI_CHU, TINH_TRANG From TCS_DM_LOAI_KHOAN " & _
                        "Where LKH_ID=" & strLKH_ID
            Try
                cnLoaiKhoan = New DataAccess
                drLoaiKhoan = cnLoaiKhoan.ExecuteDataReader(strSQL, CommandType.Text)
                If drLoaiKhoan.Read Then
                    objLoaiKhoan.LKH_ID = strLKH_ID
                    objLoaiKhoan.MA_KHOAN = drLoaiKhoan("MA_KHOAN").ToString()
                    objLoaiKhoan.Ten = drLoaiKhoan("TEN").ToString()
                    objLoaiKhoan.GHICHU = drLoaiKhoan("GHI_CHU").ToString()
                    objLoaiKhoan.TINH_TRANG = drLoaiKhoan("TINH_TRANG").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin loại - khoản")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin loại - khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drLoaiKhoan Is Nothing Then drLoaiKhoan.Close()
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try
            Return objLoaiKhoan
        End Function

        Public Function Load(ByVal tmpLoaiKhoan As infLoaiKhoan) As infLoaiKhoan
            Dim objLoaiKhoan As New infLoaiKhoan
            Dim cnLoaiKhoan As DataAccess
            Dim drLoaiKhoan As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc

            Dim strSQL As String = "Select LKH_ID, MA_KHOAN, TEN, GHI_CHU, TINH_TRANG From TCS_DM_LOAI_KHOAN " & _
                "Where " & IIf(tmpLoaiKhoan.LKH_ID <> "", "LKH_ID <> " & tmpLoaiKhoan.LKH_ID & " and ", "") & "MA_KHOAN='" & tmpLoaiKhoan.MA_KHOAN & "'"

            Try
                cnLoaiKhoan = New DataAccess
                drLoaiKhoan = cnLoaiKhoan.ExecuteDataReader(strSQL, CommandType.Text)
                If drLoaiKhoan.Read Then
                    objLoaiKhoan = New infLoaiKhoan
                    objLoaiKhoan.LKH_ID = drLoaiKhoan("LKH_ID").ToString()
                    objLoaiKhoan.MA_KHOAN = drLoaiKhoan("MA_KHOAN").ToString()
                    objLoaiKhoan.Ten = drLoaiKhoan("TEN").ToString()
                    objLoaiKhoan.TINH_TRANG = drLoaiKhoan("TINH_TRANG").ToString()
                    objLoaiKhoan.GHICHU = drLoaiKhoan("GHI_CHU").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi khi cập nhật loại - khoản")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin loại - khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drLoaiKhoan Is Nothing Then drLoaiKhoan.Close()
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try
            Return objLoaiKhoan
        End Function

        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As Object
            Dim strSQL As String
            Dim cnLoaiKhoan As DataAccess
            Dim dsLoaiKhoan As DataSet

            Try
                strSQL = "SELECT   lkh_id, ma_khoan, ten, ghi_chu, " & _
                         "         DECODE (tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngung hoat dong', " & _
                         "                 1, 'Dang hoat dong' " & _
                         "                ) tinh_trang " & _
                         "    FROM tcs_dm_loai_khoan " & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY ma_khoan "
                cnLoaiKhoan = New DataAccess
                dsLoaiKhoan = cnLoaiKhoan.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin loại - khoản")
                Throw ex
            Finally
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try

            Return dsLoaiKhoan
        End Function

        Public Function CheckExist(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
            Return Load(objLoaiKhoan).LKH_ID <> ""
        End Function

        Public Function CheckExist(ByVal strLKH_ID As String) As Boolean
            Return Load(strLKH_ID).LKH_ID <> ""
        End Function

    End Class

End Namespace
