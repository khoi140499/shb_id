﻿Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace DanhMuc.NganHang
    Public Class daNganHang_CITAD
        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim dsNganHang As New DataSet

            Try
                strSQL = "SELECT a.SHKB, b.ten TEN_KB, a.MA_TRUCTIEP, a.TEN_TRUCTIEP, a.MA_GIANTIEP, a.TEN_GIANTIEP  FROM TCS_DM_NH_GIANTIEP_TRUCTIEP a, tcs_dm_khobac b  "
                strSQL += " where a.shkb= b.shkb and b.tinh_trang='1' " & WhereClause
                strSQL += " order by shkb "
                dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
            Finally

            End Try

            Return dsNganHang
        End Function

        'Public Function Load(ByVal strMA_XA As String, ByVal strMA_NH As String) As infNganHang_CITAD
        '    Dim objNganHang As infNganHang_CITAD
        '    Dim cnNganHang As DataAccess
        '    Dim drNganHang As IDataReader
        '    Dim strSQL As String = ""
        '    Try
        '        strSQL = "SELECT id,ma_xa, ma, ten, shkb " & _
        '                 "  FROM tcs_dm_nh_giantiep_tructiep, ,TT_KB " & _
        '                 " WHERE ma_xa = '" + strMA_XA + "' " & _
        '                 "   AND ma = '" + strMA_NH + "' "

        '        cnNganHang = New DataAccess
        '        drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drNganHang.Read Then
        '            objNganHang = New infNganHang_CITAD
        '            objNganHang.MA_XA = strMA_XA
        '            objNganHang.MA_NH = drNganHang("ma").ToString()
        '            objNganHang.TEN_NH = drNganHang("ten").ToString()
        '            objNganHang.SHKB = drNganHang("shkb").ToString()
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If Not drNganHang Is Nothing Then drNganHang.Close()
        '        If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
        '    End Try
        '    Return objNganHang
        'End Function
        Public Function LoadID(ByVal strID As String) As infNganHang_CITAD
            Dim objNganHang As infNganHang_CITAD
            Dim cnNganHang As DataAccess
            Dim drNganHang As IDataReader
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT shkb,ten_kb,ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep,TT_KB    " & _
                         "  FROM tcs_dm_nh_giantiep_tructiep " & _
                         " WHERE shkb= '" + strID + "' "

                cnNganHang = New DataAccess
                drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
                If drNganHang.Read Then
                    objNganHang = New infNganHang_CITAD
                    objNganHang.SHKB = drNganHang("shkb").ToString()
                    objNganHang.Ten_KB = drNganHang("ten_kb").ToString()
                    objNganHang.MA_NH_TT = drNganHang("ma_tructiep").ToString()
                    objNganHang.TEN_NH_TT = drNganHang("ten_tructiep").ToString()
                    objNganHang.MA_NH_TH = drNganHang("ma_giantiep").ToString()
                    objNganHang.TEN_NH_TH = drNganHang("ten_giantiep").ToString()
                    objNganHang.TT_KB = drNganHang("TT_KB").ToString()
                End If
            Catch ex As Exception
                Throw ex
            Finally
                If Not drNganHang Is Nothing Then drNganHang.Close()
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return objNganHang
        End Function

        Public Function Insert(ByVal objNganHang As infNganHang_CITAD) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO tcs_dm_nh_giantiep_tructiep " & _
                                    " (shkb,ten_kb,ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep,TT_KB ) " & _
                                    " VALUES ('" + objNganHang.SHKB + "','" + objNganHang.Ten_KB + "', '" + objNganHang.MA_NH_TT + "', " & _
                                    " '" + objNganHang.TEN_NH_TT + "', '" + objNganHang.MA_NH_TH + "', '" + objNganHang.TEN_NH_TH + "', " & _
                                    " '" + objNganHang.TT_KB + "') "
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objNganHang As infNganHang_CITAD) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE tcs_dm_nh_giantiep_tructiep " & _
                                   "   SET ma_tructiep = '" + objNganHang.MA_NH_TT + "', " & _
                                    "       ten_tructiep = '" + objNganHang.TEN_NH_TT + "', " & _
                                    "       TEN_KB = '" + objNganHang.Ten_KB + "', " & _
                                   "       ten_giantiep = '" + objNganHang.TEN_NH_TH + "', " & _
                                    "      TT_KB = '" + objNganHang.TT_KB + "' " & _
                                   " WHERE shkb = '" + objNganHang.SHKB + "' "
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objNganHang As infNganHang_CITAD) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM tcs_dm_nh_giantiep_tructiep " & _
                                    "      WHERE shkb = '" + objNganHang.SHKB + "' and ma_giantiep = '" & objNganHang.MA_NH_TH & "' "
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá danh mục loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function CheckExist(ByVal objNganHang As infNganHang_CITAD) As Boolean
            If Not LoadID(objNganHang.MA_NH_TH) Is Nothing Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function Check2Del(ByVal objNganHang As infNganHang) As Byte
            Dim cnNganHang As DataAccess
            Dim drNganHang As IDataReader
            Dim strSQL As String
            Dim bytResult As Byte = 0

            Try
                strSQL = "SELECT shkb " & _
                        "  FROM tcs_dm_nh_giantiep_tructiep " & _
                        " WHERE ma_xa = '" & objNganHang.MA_XA & "' "
                cnNganHang = New DataAccess
                drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
                If drNganHang.Read Then bytResult = 1
            Catch ex As Exception
                Throw ex
            Finally
                If Not drNganHang Is Nothing Then drNganHang.Close()
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try

            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function
    End Class
End Namespace
