Namespace DanhMuc.LoaiHinh
    Public Class infLoaiHinh
        Private strMA_LH As String
        Private strTEN_LH As String
        Private strTen_VT As String
        Private strNGAY_ANHAN As String
        
        Sub New()
            strMA_LH = ""
            strTEN_LH = ""
            strTen_VT = ""
            strNGAY_ANHAN = ""

        End Sub

        Sub New(ByVal drwLoaiHinh As DataRow)
            strMA_LH = drwLoaiHinh("MA_LH").ToString().Trim()
            strTEN_LH = drwLoaiHinh("TEN_LH").ToString().Trim()
            strTen_VT = drwLoaiHinh("TEN_VT").ToString().Trim()
            strNGAY_ANHAN = drwLoaiHinh("NGAY_ANHAN").ToString().Trim()
        End Sub

        Public Property MA_LH() As String
            Get
                Return strMA_LH
            End Get
            Set(ByVal Value As String)
                strMA_LH = Value
            End Set
        End Property

        Public Property TEN_LH() As String
            Get
                Return strTEN_LH
            End Get
            Set(ByVal Value As String)
                strTEN_LH = Value
            End Set
        End Property

        Public Property TEN_VT() As String
            Get
                Return strTen_VT
            End Get
            Set(ByVal Value As String)
                strTen_VT = Value
            End Set
        End Property

        Public Property NGAY_ANHAN() As String
            Get
                Return strNGAY_ANHAN
            End Get
            Set(ByVal Value As String)
                strNGAY_ANHAN = Value
            End Set
        End Property

    End Class
End Namespace
