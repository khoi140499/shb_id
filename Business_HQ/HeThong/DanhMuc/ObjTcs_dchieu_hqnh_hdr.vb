﻿Public Class ObjTcs_dchieu_hqnh_hdr
    Private _id As String
    Private _transaction_id As String
    Private _transaction_type As String
    Private _shkb As String
    Private _ten_kb As String
    Private _ngay_kb As String
    Private _so_bt As String
    Private _kyhieu_ct As String
    Private _so_ct As String
    Private _ngay_ct As String
    Private _ngay_bn As String
    Private _ngay_bc As String
    Private _ma_nnthue As String
    Private _ten_nnthue As String
    Private _ma_cqthu As String
    Private _ten_cqthu As String
    Private _so_tk As String
    Private _ngay_tk As String
    Private _lhxnk As String
    Private _vt_lhxnk As String
    Private _ten_lhxnk As String
    Private _ttien As String
    Private _ttien_nt As String
    Private _trang_thai As String
    Private _tk_ns As String
    Private _ten_tk_ns As String
    Private _ma_nkt As String
    Private _so_bl As String
    Private _ngay_bl_batdau As String
    Private _ngay_bl_ketthuc As String
    Private _songay_bl As String
    Private _kieu_bl As String
    Private _dien_giai As String
    Private _request_id As String
    Private _accept_yn As String
    Private _parent_transaction_id As String
    Private _ma_hq_ph As String
    Private _kq_dc As String

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(ByVal Value As String)
            _id = Value
        End Set
    End Property
    Public Property transaction_id() As String
        Get
            Return _transaction_id
        End Get
        Set(ByVal Value As String)
            _transaction_id = Value
        End Set
    End Property
    Public Property transaction_type() As String
        Get
            Return _transaction_type
        End Get
        Set(ByVal Value As String)
            _transaction_type = Value
        End Set
    End Property
    Public Property shkb() As String
        Get
            Return _shkb
        End Get
        Set(ByVal Value As String)
            _shkb = Value
        End Set
    End Property
    Public Property ten_kb() As String
        Get
            Return _ten_kb
        End Get
        Set(ByVal Value As String)
            _ten_kb = Value
        End Set
    End Property
    Public Property ngay_kb() As String
        Get
            Return _ngay_kb
        End Get
        Set(ByVal Value As String)
            _ngay_kb = Value
        End Set
    End Property
    Public Property so_bt() As String
        Get
            Return _so_bt
        End Get
        Set(ByVal Value As String)
            _so_bt = Value
        End Set
    End Property
    Public Property kyhieu_ct() As String
        Get
            Return _kyhieu_ct
        End Get
        Set(ByVal Value As String)
            _kyhieu_ct = Value
        End Set
    End Property
    Public Property so_ct() As String
        Get
            Return _so_ct
        End Get
        Set(ByVal Value As String)
            _so_ct = Value
        End Set
    End Property
    Public Property ngay_ct() As String
        Get
            Return _ngay_ct
        End Get
        Set(ByVal Value As String)
            _ngay_ct = Value
        End Set
    End Property
    Public Property ngay_bn() As String
        Get
            Return _ngay_bn
        End Get
        Set(ByVal Value As String)
            _ngay_bn = Value
        End Set
    End Property
    Public Property ngay_bc() As String
        Get
            Return _ngay_bc
        End Get
        Set(ByVal Value As String)
            _ngay_bc = Value
        End Set
    End Property
    Public Property ma_nnthue() As String
        Get
            Return _ma_nnthue
        End Get
        Set(ByVal Value As String)
            _ma_nnthue = Value
        End Set
    End Property
    Public Property ten_nnthue() As String
        Get
            Return _ten_nnthue
        End Get
        Set(ByVal Value As String)
            _ten_nnthue = Value
        End Set
    End Property
    Public Property ma_cqthu() As String
        Get
            Return _ma_cqthu
        End Get
        Set(ByVal Value As String)
            _ma_cqthu = Value
        End Set
    End Property
    Public Property ten_cqthu() As String
        Get
            Return _ten_cqthu
        End Get
        Set(ByVal Value As String)
            _ten_cqthu = Value
        End Set
    End Property
    Public Property so_tk() As String
        Get
            Return _so_tk
        End Get
        Set(ByVal Value As String)
            _so_tk = Value
        End Set
    End Property
    Public Property ngay_tk() As String
        Get
            Return _ngay_tk
        End Get
        Set(ByVal Value As String)
            _ngay_tk = Value
        End Set
    End Property
    Public Property lhxnk() As String
        Get
            Return _lhxnk
        End Get
        Set(ByVal Value As String)
            _lhxnk = Value
        End Set
    End Property
    Public Property vt_lhxnk() As String
        Get
            Return _vt_lhxnk
        End Get
        Set(ByVal Value As String)
            _vt_lhxnk = Value
        End Set
    End Property
    Public Property ten_lhxnk() As String
        Get
            Return _ten_lhxnk
        End Get
        Set(ByVal Value As String)
            _ten_lhxnk = Value
        End Set
    End Property
    Public Property ttien() As String
        Get
            Return _ttien
        End Get
        Set(ByVal Value As String)
            _ttien = Value
        End Set
    End Property
    Public Property ttien_nt() As String
        Get
            Return _ttien_nt
        End Get
        Set(ByVal Value As String)
            _ttien_nt = Value
        End Set
    End Property
    Public Property trang_thai() As String
        Get
            Return _trang_thai
        End Get
        Set(ByVal Value As String)
            _trang_thai = Value
        End Set
    End Property
    Public Property tk_ns() As String
        Get
            Return _tk_ns
        End Get
        Set(ByVal Value As String)
            _tk_ns = Value
        End Set
    End Property
    Public Property ten_tk_ns() As String
        Get
            Return _ten_tk_ns
        End Get
        Set(ByVal Value As String)
            _ten_tk_ns = Value
        End Set
    End Property
    Public Property ma_nkt() As String
        Get
            Return _ma_nkt
        End Get
        Set(ByVal Value As String)
            _ma_nkt = Value
        End Set
    End Property
    Public Property so_bl() As String
        Get
            Return _so_bl
        End Get
        Set(ByVal Value As String)
            _so_bl = Value
        End Set
    End Property
    Public Property ngay_bl_batdau() As String
        Get
            Return _ngay_bl_batdau
        End Get
        Set(ByVal Value As String)
            _ngay_bl_batdau = Value
        End Set
    End Property
    Public Property ngay_bl_ketthuc() As String
        Get
            Return _ngay_bl_ketthuc
        End Get
        Set(ByVal Value As String)
            _ngay_bl_ketthuc = Value
        End Set
    End Property
    Public Property songay_bl() As String
        Get
            Return _songay_bl
        End Get
        Set(ByVal Value As String)
            _songay_bl = Value
        End Set
    End Property
    Public Property kieu_bl() As String
        Get
            Return _kieu_bl
        End Get
        Set(ByVal Value As String)
            _kieu_bl = Value
        End Set
    End Property
    Public Property dien_giai() As String
        Get
            Return _dien_giai
        End Get
        Set(ByVal Value As String)
            _dien_giai = Value
        End Set
    End Property
    Public Property request_id() As String
        Get
            Return _request_id
        End Get
        Set(ByVal Value As String)
            _request_id = Value
        End Set
    End Property
    Public Property accept_yn() As String
        Get
            Return _accept_yn
        End Get
        Set(ByVal Value As String)
            _accept_yn = Value
        End Set
    End Property
    Public Property parent_transaction_id() As String
        Get
            Return _parent_transaction_id
        End Get
        Set(ByVal Value As String)
            _parent_transaction_id = Value
        End Set
    End Property
    Public Property ma_hq_ph() As String
        Get
            Return _ma_hq_ph
        End Get
        Set(ByVal Value As String)
            _ma_hq_ph = Value
        End Set
    End Property
    Public Property kq_dc() As String
        Get
            Return _kq_dc
        End Get
        Set(ByVal Value As String)
            _kq_dc = Value
        End Set
    End Property
End Class
