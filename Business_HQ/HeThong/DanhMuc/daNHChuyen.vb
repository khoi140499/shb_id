﻿Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace DanhMuc.NHChuyen
    Public Class daNHChuyen
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnNHChuyen As DataAccess
            Dim dsNganHang As DataSet

            Try
                strSQL = "SELECT  * " & _
                         "    FROM tcs_nganhang_chuyen " & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY ma_nh_chuyen "

                cnNHChuyen = New DataAccess
                dsNganHang = cnNHChuyen.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try

            Return dsNganHang
        End Function

        Public Function Load(ByVal objNHCHuyen As infNHChuyen) As Object
            Dim strSQL As String
            Dim cnNHChuyen As DataAccess
            Dim dsNganHang As DataSet

            Try
                strSQL = "SELECT  * " & _
                         "    FROM tcs_nganhang_chuyen " & _
                         "   WHERE 1 = 1 " & _
                         "ORDER BY ma_nh_chuyen "

                cnNHChuyen = New DataAccess
                dsNganHang = cnNHChuyen.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try

            Return dsNganHang
        End Function
        Public Function LoadID(ByVal strID As String) As Boolean
            Dim objNHCHuyen As New infNHChuyen
            Dim cnNHChuyen As DataAccess
            Dim drNHChuyen As IDataReader
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT * " & _
                         "  FROM tcs_nganhang_chuyen " & _
                         " WHERE ma_nh_chuyen = '" + strID + "' "

                cnNHChuyen = New DataAccess
                drNHChuyen = cnNHChuyen.ExecuteDataReader(strSQL, CommandType.Text)
                If drNHChuyen.Read Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not drNHChuyen Is Nothing Then drNHChuyen.Close()
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try
            'Return objNHCHuyen
        End Function

        Public Function Insert(ByVal objNHCHuyen As infNHChuyen) As Boolean
            Dim cnNHChuyen As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO tcs_nganhang_chuyen " & _
                                    "            (id,ma_nh_chuyen " & _
                                    "             ,ten_nh_chuyen " & _
                                    "            ) " & _
                                    "     VALUES (tcs_nhchuyen_id_seq.nextval,'" + objNHCHuyen.Ma_NHC + "', '" + objNHCHuyen.Ten_NHC + "' " & _
                                    "            ) "

            Try
                cnNHChuyen = New DataAccess
                cnNHChuyen.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi thêm mới Ngân hàng")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objNHCHuyen As infNHChuyen) As Boolean
            Dim cnNHChuyen As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE tcs_nganhang_chuyen " & _
                                   "   SET ten_nh_chuyen = '" + objNHCHuyen.Ten_NHC + "' " & _
                                   " WHERE ma_nh_chuyen = '" + objNHCHuyen.Ma_NHC + "' "
            Try
                cnNHChuyen = New DataAccess
                cnNHChuyen.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi cập nhật Ngân hàng")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objNHCHuyen As infNHChuyen) As Boolean
            Dim cnNHChuyen As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM tcs_nganhang_chuyen " & _
                                    "      WHERE ma_nh_chuyen = '" + objNHCHuyen.Ma_NHC + "' "
            Try
                cnNHChuyen = New DataAccess
                cnNHChuyen.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi xóa Ngân hàng")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá danh mục loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function CheckExist(ByVal objNHCHuyen As infNHChuyen) As Boolean
            If LoadID(objNHCHuyen.Ma_NHC) = True Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function Check2Del(ByVal objNHCHuyen As infNHChuyen) As Byte
            Dim cnNHChuyen As DataAccess
            Dim drNHChuyen As IDataReader
            Dim strSQL As String
            Dim bytResult As Byte = 0

            Try
                strSQL = "SELECT ma_nh_chuyen " & _
                        "  FROM tcs_nganhang_chuyen " & _
                        " WHERE ma_nh_chuyen = '" & objNHCHuyen.Ma_NHC & "' "
                cnNHChuyen = New DataAccess
                drNHChuyen = cnNHChuyen.ExecuteDataReader(strSQL, CommandType.Text)
                If drNHChuyen.Read Then bytResult = 1
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi kiểm tra thông tin Ngân hàng")
                Throw ex
            Finally
                If Not drNHChuyen Is Nothing Then drNHChuyen.Close()
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try

            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function
        Public Function CheckDelete(ByVal strMA_NHC As String) As Object
            Dim cnNHC As DataAccess
            Dim dsNHC As DataSet

            Try
                Dim strSQL As String = "select ma_nh_a from (select ma_nh_a from tcs_ctu_hdr union all select ma_nh_a from tcs_baolanh_hdr) " & _
                        "Where ma_nh_a='" & strMA_NHC & "'"

                cnNHC = New DataAccess
                dsNHC = cnNHC.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnNHC Is Nothing Then cnNHC.Dispose()
            End Try

            Return dsNHC
        End Function
    End Class
End Namespace

