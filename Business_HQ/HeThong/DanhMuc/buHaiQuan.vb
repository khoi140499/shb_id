Imports VBOracleLib

Namespace DanhMuc.HaiQuan
    Public Class buHaiQuan

        Private strStatus As String

        Sub New()
        End Sub

        Sub New(ByVal strTmpStatus As String)
            strStatus = strTmpStatus
        End Sub

        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property

        Public Function Insert(ByVal objHaiQuan As infHaiQuan) As Boolean
            Dim tmpHaiQuan As New daHaiQuan
            If tmpHaiQuan.CheckExist(objHaiQuan) = False Then
                Return tmpHaiQuan.Insert(objHaiQuan)
            Else
                Return False
            End If
        End Function

        Public Function Update(ByVal objHaiQuan As infHaiQuan) As Boolean
            Dim tmpHaiQuan As New daHaiQuan
            'Try
            'Dim objExist As infHaiQuan = tmpHaiQuan.Load(objHaiQuan)
            'If objExist.MA_HQ = "" Then
            Return tmpHaiQuan.Update(objHaiQuan)

            'Else
            'Dim strMsg As String = "Bạn đã sửa mã hải quan trùng với mã hải quan khác đã tồn tại trong hệ thống:" & Chr(13) & _
            '"Mã CU: " & objExist.MA_CU & Chr(13) & _
            '"Mã QHNS: " & objExist.MA_QHNS & Chr(13) & _
            '"Tên: " & objExist.TEN & Chr(13) & _
            'MsgBox(strMsg, MsgBoxStyle.Critical, "Chú ý")
            'Return False
            'End If
            'Catch ex As Exception
            '    Return False
            'End Try
        End Function

        Public Function Delete(ByVal strTmpMaHaiQuan As String) As Boolean
            Dim objHaiQuan As New infHaiQuan
            objHaiQuan.MA_HQ = strTmpMaHaiQuan
            Dim tmpHaiQuan As New daHaiQuan
            Return tmpHaiQuan.Delete(objHaiQuan)
        End Function

        Public Function GetDataSet(ByVal WhereClause As String) As DataSet
            Dim objHaiQuan As New daHaiQuan
            Dim dsHaiQuan As DataSet
            Try
                dsHaiQuan = objHaiQuan.Load(WhereClause, "")
            Catch ex As Exception
            Finally
            End Try

            Return dsHaiQuan
        End Function

        'Public Function GetReport() As String
        '    Return Common.mdlCommon.Report_Type.DM_CAPCHUONG
        'End Function

        'Public Function CheckDelete(ByVal strTmpMaCapChuong As String) As Boolean
        '    Dim ds As DataSet
        '    Dim tmpCapChuong As New daCapChuong
        '    ds = tmpCapChuong.CheckDelete(strTmpMaCapChuong)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        Return True
        '    Else
        '        Return False
        '    End If
        'End Function
    End Class
End Namespace
