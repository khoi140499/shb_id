﻿Namespace DanhMuc.NganHang
    Public Class infNganHang
        Private strid As String
        Private strMA_XA As String
        Private strMA_NH As String
        Private strTEN_NH As String
        Private strSHKB As String

        Sub New()
            strMA_XA = ""
            strMA_NH = ""
            strTEN_NH = ""
            strSHKB = ""
        End Sub

        Sub New(ByVal drwTaiKhoan As DataRow)
            strMA_XA = drwTaiKhoan("MA_XA").ToString().Trim()
            strMA_NH = drwTaiKhoan("MA").ToString().Trim()
            strTEN_NH = drwTaiKhoan("TEN").ToString().Trim()
            strSHKB = drwTaiKhoan("SHKB").ToString().Trim()
        End Sub

        Public Property MA_XA() As String
            Get
                Return strMA_XA
            End Get
            Set(ByVal Value As String)
                strMA_XA = Value
            End Set
        End Property
        Public Property ID() As String
            Get
                Return strid
            End Get
            Set(ByVal Value As String)
                strid = Value
            End Set
        End Property

        Public Property MA_NH() As String
            Get
                Return strMA_NH
            End Get
            Set(ByVal Value As String)
                strMA_NH = Value
            End Set
        End Property

        Public Property TEN_NH() As String
            Get
                Return strTEN_NH
            End Get
            Set(ByVal Value As String)
                strTEN_NH = Value
            End Set
        End Property

        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property
    End Class
End Namespace
