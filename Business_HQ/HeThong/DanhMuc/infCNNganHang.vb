﻿Namespace DanhMuc.CNNganHang
    Public Class infCNNganHang
        Private strid As String
        Private strMA_PGD As String
        Private strTen_PGD As String
        Private strMA_CN As String
        Private strTinh_Trang As String
        Private strPhan_Cap As String
        Private strMa_Xa As String

        Sub New()
            strid = ""
            strMA_PGD = ""
            strTen_PGD = ""
            strMA_CN = ""
            strTinh_Trang = ""
            strPhan_Cap = ""
        End Sub

      

        Public Property MA_PGD() As String
            Get
                Return strMA_PGD
            End Get
            Set(ByVal Value As String)
                strMA_PGD = Value
            End Set
        End Property
        Public Property MA_XA() As String
            Get
                Return strMa_Xa
            End Get
            Set(ByVal Value As String)
                strMa_Xa = Value
            End Set
        End Property
        Public Property ID() As String
            Get
                Return strid
            End Get
            Set(ByVal Value As String)
                strid = Value
            End Set
        End Property

        Public Property MA_CN() As String
            Get
                Return strMA_CN
            End Get
            Set(ByVal Value As String)
                strMA_CN = Value
            End Set
        End Property

        Public Property TEN_PGD() As String
            Get
                Return strTen_PGD
            End Get
            Set(ByVal Value As String)
                strTen_PGD = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTinh_Trang
            End Get
            Set(ByVal Value As String)
                strTinh_Trang = Value
            End Set
        End Property
        Public Property PHAN_CAP() As String
            Get
                Return strPhan_Cap
            End Get
            Set(ByVal Value As String)
                strPhan_Cap = Value
            End Set
        End Property
    End Class
End Namespace
