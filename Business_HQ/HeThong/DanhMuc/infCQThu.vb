Namespace DanhMuc.CQThu
    Public Class infCQThu
        Private strMa_CQThu As String
        Private strTen As String
        Private strMa_Dthu As String
        Private strMa_HQ As String
        Private strMa_QLT As String

        Sub New()
        End Sub

        Sub New(ByVal drwCQThu As DataRow)
            strMa_CQThu = drwCQThu("Ma_CQThu").ToString().Trim()
            strTen = drwCQThu("Ten").ToString().Trim()
        End Sub

        Public Property Ma_CQThu() As String
            Get
                Return strMa_CQThu
            End Get
            Set(ByVal Value As String)
                strMa_CQThu = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property
        Public Property Ma_Dthu() As String
            Get
                Return strMa_Dthu
            End Get
            Set(ByVal Value As String)
                strMa_Dthu = Value
            End Set
        End Property
        Public Property Ma_HQ() As String
            Get
                Return strMa_HQ
            End Get
            Set(ByVal Value As String)
                strMa_HQ = Value
            End Set
        End Property
        Public Property Ma_QLT() As String
            Get
                Return strMa_QLT
            End Get
            Set(ByVal Value As String)
                strMa_QLT = Value
            End Set
        End Property
    End Class
End Namespace
