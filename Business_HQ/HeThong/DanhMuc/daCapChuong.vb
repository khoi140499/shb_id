'Imports VBOracleLib
'Imports Common.mdlCommon

'Namespace DanhMuc.CapChuong
'    Public Class daCapChuong

'        Public Function Insert(ByVal objCapChuong As infCapChuong) As Boolean
'            Dim cnCapChuong As DataAccess
'            Dim blnResult As Boolean = True
'            Dim strSql As String = "INSERT INTO TCS_DM_CAP_CHUONG (CCH_ID, MA_CHUONG, TEN, GHI_CHU, TINH_TRANG) " & _
'                                "VALUES (" & Common.mdlCommon.getDataKey("TCS_CCH_ID_SEQ.NEXTVAL") & ", " & _
'                                "'" & objCapChuong.MA_CHUONG & "','" & objCapChuong.Ten & "'," & _
'                                "'" & objCapChuong.GHICHU & "','" & objCapChuong.TINH_TRANG & "')"
'            Try
'                cnCapChuong = New DataAccess
'                cnCapChuong.ExecuteNonQuery(strSql, CommandType.Text)
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới cấp - chương")
'                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới cấp - chương: " & ex.ToString)
'                blnResult = False
'                Throw ex
'            Finally
'                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
'            End Try
'            Return blnResult
'        End Function

'        Public Function Update(ByVal objCapChuong As infCapChuong) As Boolean
'            Dim cnCapChuong As DataAccess
'            Dim blnResult As Boolean = True
'            Dim strSql As String = "UPDATE TCS_DM_CAP_CHUONG SET MA_CHUONG='" & objCapChuong.MA_CHUONG & "',TEN='" & objCapChuong.Ten & "', " & _
'                   "GHI_CHU='" & objCapChuong.GHICHU & "', TINH_TRANG='" & objCapChuong.TINH_TRANG & "'  WHERE CCH_ID=" & objCapChuong.CCH_ID.Trim
'            Try
'                cnCapChuong = New DataAccess
'                cnCapChuong.ExecuteNonQuery(strSql, CommandType.Text)
'            Catch ex As Exception
'                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật cấp - chương: " & ex.ToString)
'                CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật cấp - chương")
'                blnResult = False
'                Throw ex
'            Finally
'                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
'            End Try
'            Return blnResult
'        End Function

'        Public Function Delete(ByVal objCapChuong As infCapChuong) As Boolean
'            Dim cnCapChuong As DataAccess
'            Dim blnResult As Boolean = True
'            Dim strSql As String = "DELETE FROM TCS_DM_CAP_CHUONG " & _
'                    "WHERE CCH_ID=" & objCapChuong.CCH_ID
'            Try
'                cnCapChuong = New DataAccess
'                cnCapChuong.ExecuteNonQuery(strSql, CommandType.Text)
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa cấp - chương")
'                Throw ex
'                'LogDebug.Writelog("Có lỗi xảy ra khi xoá cơ mục lục cấp - chương: " & ex.ToString)
'                blnResult = False
'            Finally
'                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
'            End Try
'            Return blnResult
'        End Function

'        Public Function Load(ByVal strCCH_ID As String) As infCapChuong
'            Dim objCapChuong As New infCapChuong
'            Dim cnCapChuong As DataAccess
'            Dim drCapChuong As IDataReader
'            Dim strSQL As String = "Select CCH_ID, MA_CHUONG, TEN, GHI_CHU, TINH_TRANG From TCS_DM_CAP_CHUONG " & _
'                        "Where CCH_ID=" & strCCH_ID
'            Try
'                cnCapChuong = New DataAccess
'                drCapChuong = cnCapChuong.ExecuteDataReader(strSQL, CommandType.Text)
'                If drCapChuong.Read Then
'                    objCapChuong = New infCapChuong
'                    objCapChuong.CCH_ID = strCCH_ID
'                    objCapChuong.MA_CHUONG = drCapChuong("MA_CHUONG").ToString()
'                    objCapChuong.Ten = drCapChuong("TEN").ToString()
'                    objCapChuong.TINH_TRANG = drCapChuong("TINH_TRANG").ToString()
'                    objCapChuong.GHICHU = drCapChuong("GHI_CHU").ToString()
'                End If
'                Return objCapChuong
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
'                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
'                Throw ex
'            Finally
'                If Not drCapChuong Is Nothing Then drCapChuong.Close()
'                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
'            End Try
'            Return objCapChuong
'        End Function

'        Public Function Load(ByVal tmpCapChuong As infCapChuong) As infCapChuong
'            Dim objCapChuong As New infCapChuong
'            Dim cnCapChuong As DataAccess
'            Dim drCapChuong As IDataReader

'            ' Kiểm tra tồn tại nếu
'            ' Mã cấp chương đã tồn tại và
'            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
'            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc
'            Dim strSQL As String = "Select CCH_ID, MA_CHUONG, TEN, GHI_CHU, TINH_TRANG From TCS_DM_CAP_CHUONG " & _
'                "Where " & IIf(tmpCapChuong.CCH_ID <> "", "CCH_ID <> " & tmpCapChuong.CCH_ID & " and ", "") & "MA_CHUONG='" & tmpCapChuong.MA_CHUONG & "'"

'            Try
'                cnCapChuong = New DataAccess
'                drCapChuong = cnCapChuong.ExecuteDataReader(strSQL, CommandType.Text)
'                If drCapChuong.Read Then
'                    objCapChuong = New infCapChuong
'                    objCapChuong.CCH_ID = drCapChuong("CCH_ID").ToString()
'                    objCapChuong.MA_CHUONG = drCapChuong("MA_CHUONG").ToString()
'                    objCapChuong.Ten = drCapChuong("TEN").ToString()
'                    objCapChuong.TINH_TRANG = drCapChuong("TINH_TRANG").ToString()
'                    objCapChuong.GHICHU = drCapChuong("GHI_CHU").ToString()
'                End If
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
'                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
'                Throw ex
'            Finally
'                If Not drCapChuong Is Nothing Then drCapChuong.Close()
'                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
'            End Try
'            Return objCapChuong
'        End Function

'        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As DataSet
'            Dim strSQL As String
'            Dim cnCapChuong As DataAccess
'            Dim dsCapChuong As DataSet

'            Try
'                strSQL = "SELECT   cch_id, ma_chuong, ten, ghi_chu, " & _
'                         "         DECODE (tinh_trang, " & _
'                         "                 NULL, '', " & _
'                         "                 0, 'Ngung hoat dong', " & _
'                         "                 1, 'Dang hoat dong' " & _
'                         "                ) tinh_trang " & _
'                         "    FROM tcs_dm_cap_chuong " & _
'                         "   WHERE 1 = 1 " & WhereClause & _
'                         "ORDER BY ma_chuong "
'                cnCapChuong = New DataAccess
'                dsCapChuong = cnCapChuong.ExecuteReturnDataSet(strSQL, CommandType.Text)
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
'                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
'                Throw ex
'            Finally
'                cnCapChuong.Dispose()
'            End Try

'            Return dsCapChuong
'        End Function
'        Public Function CheckDelete(ByVal strCCH_ID As String) As Object
'            Dim cnKhoBac As DataAccess
'            Dim dsKhoBac As DataSet

'            Try
'                Dim strSQL As String = "select ma_chuong from (select ma_chuong from tcs_ctu_dtl union all select ma_chuong from tcs_baolanh_dtl) " & _
'                        "Where ma_chuong=" & strCCH_ID

'                cnKhoBac = New DataAccess
'                dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSQL, CommandType.Text)
'            Catch ex As Exception
'                CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
'                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
'                Throw ex
'            Finally
'                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
'            End Try

'            Return dsKhoBac
'        End Function
'        Public Function CheckExist(ByVal objCapChuong As infCapChuong) As Boolean
'            Return Load(objCapChuong).CCH_ID <> ""
'        End Function

'        Public Function CheckExist(ByVal strCCH_ID As String) As Boolean
'            Return Load(strCCH_ID).CCH_ID <> ""
'        End Function
'    End Class

'End Namespace
