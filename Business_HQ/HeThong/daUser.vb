﻿Imports VBOracleLib
Imports GlobalProjects

Namespace HeThong


    Public Class daUser
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function check_MaCN(ByVal ma_cn As String) As Boolean
            Dim conn As DataAccess
            Dim strSql As String
            Dim dts As DataSet
            Dim flag As Boolean = False
            Try
                strSql = "select * from TCS_DM_CHINHANH where id='" & ma_cn & "'"
                dts = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                If dts.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False

            End Try

        End Function
        Public Function Insert(ByVal objUser As infUser) As Boolean
            Dim blnResult As Boolean = True
            Dim cnUser As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                cnUser = New DataAccess
                Dim ma_NV As String = Common.mdlCommon.getDataKey("TCS_NHANVIEN_ID_SEQ.NEXTVAL")
                transCT = cnUser.BeginTransaction
                strSql = "INSERT INTO TCS_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,UserDomain,BAN_LV) " & _
                                "VALUES (" & ma_NV & " ,'" & _
                                objUser.Ten & "','" & _
                                objUser.Mat_Khau & "','" & _
                                objUser.Ma_Nhom & "','" & _
                                objUser.Chuc_Danh & "','" & _
                                objUser.Trang_thai & "','" & _
                                objUser.TEN_ND & "','" & _
                                objUser.Teller_Id & "','" & _
                                objUser.MaCN & "'," & _
                                objUser.LDAP & ",'" & _
                                objUser.HanMuc_Tu & "','" & _
                                objUser.HanMuc_Den & "','" & _
                                objUser.User_Domain & "'," & _
                                Globals.EscapeQuote(objUser.Ban_Lv) & " )"

                cnUser.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                ' INSERT BẢNG LOG
                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,UserDomain,BAN_LV, NGAY_HT) " & _
                               "VALUES (" & ma_NV & "  ,'" & _
                               objUser.Ten & "','" & _
                               objUser.Mat_Khau & "','" & _
                               objUser.Ma_Nhom & "','" & _
                               objUser.Chuc_Danh & "','" & _
                               objUser.Trang_thai & "','" & _
                               objUser.TEN_ND & "','" & _
                               objUser.Teller_Id & "','" & _
                               objUser.MaCN & "'," & _
                               objUser.LDAP & ",'" & _
                                objUser.HanMuc_Tu & "','" & _
                                objUser.HanMuc_Den & "','" & _
                                objUser.User_Domain & "'," & _
                               Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                               " SYSDATE)"

                cnUser.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                transCT.Commit()
            Catch ex As Exception
                blnResult = False
                transCT.Rollback()
                Throw ex
            Finally
                If Not cnUser Is Nothing Then
                    transCT.Dispose()
                    cnUser.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function Delete(ByVal objUser As infUser, ByVal strTrangThai As String) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                'transCT.Connection = conn.GetConnection()
                If CheckUSer_GD(objUser.Ma_NV) = "1" Then
                    strSql = "UPDATE TCS_DM_NHANVIEN SET TINH_TRANG='" & strTrangThai & "' " & _
                                             "WHERE Ma_NV=" & objUser.Ma_NV
                Else
                    strSql = "DELETE FROM  TCS_DM_NHANVIEN WHERE Ma_NV=" & objUser.Ma_NV
                End If
                '/// Không xóa user mà chỉ cập nhật trạng thái lock user

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                ' INSERT BẢNG LOG
                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,BAN_LV, NGAY_HT) " & _
                               " (SELECT MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,'" & strTrangThai & "',TEN_DN,TELLER_ID,MA_CN,BAN_LV, SYSDATE FROM TCS_DM_NHANVIEN " & _
                               "  WHERE MA_NV=" & objUser.Ma_NV & ")"

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                transCT.Commit()
            Catch ex As Exception
                blnResult = False
                transCT.Rollback()
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    transCT.Dispose()
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function DoiTen(ByVal objUser As infUser, Optional ByVal tt_doiCN As Boolean = False) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                conn = New DataAccess
                Dim ma_NV As String = Common.mdlCommon.getDataKey("TCS_NHANVIEN_ID_SEQ.NEXTVAL")
                transCT = conn.BeginTransaction
                If tt_doiCN Then
                    If CheckUSer_GD(objUser.Ma_NV) = "1" Then
                        strSql = "UPDATE TCS_DM_NHANVIEN SET TINH_TRANG='1' " & _
                                                 "WHERE Ma_NV=" & objUser.Ma_NV
                    Else
                        strSql = "DELETE FROM  TCS_DM_NHANVIEN WHERE Ma_NV=" & objUser.Ma_NV
                    End If
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                    strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV, NGAY_HT) " & _
                                                  "VALUES (" & objUser.Ma_NV & "  ,'" & _
                                                  objUser.Ten & "','" & _
                                                  objUser.Mat_Khau & "','" & _
                                                  objUser.Ma_Nhom & "','" & _
                                                  objUser.Chuc_Danh & "','1','" & _
                                                  objUser.TEN_ND & "','" & _
                                                  objUser.Teller_Id & "','" & _
                                                  objUser.MaCN & "'," & _
                                                  objUser.LDAP & ",'" & _
                                                objUser.HanMuc_Tu & "','" & _
                                                objUser.HanMuc_Den & "','" & _
                                                objUser.User_Domain & "'," & _
                                                  Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                                  " SYSDATE)"
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                    objUser.Ma_NV = ma_NV
                    strSql = "INSERT INTO TCS_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV) " & _
                               "VALUES (" & objUser.Ma_NV & " ,'" & _
                               objUser.Ten & "','" & _
                               objUser.Mat_Khau & "','" & _
                               objUser.Ma_Nhom & "','" & _
                               objUser.Chuc_Danh & "','" & _
                               objUser.Trang_thai & "','" & _
                               objUser.TEN_ND & "','" & _
                               objUser.Teller_Id & "','" & _
                               objUser.MaCN & "'," & _
                               objUser.LDAP & ",'" & _
                                objUser.HanMuc_Tu & "','" & _
                                objUser.HanMuc_Den & "','" & _
                                objUser.User_Domain & "'," & _
                               Globals.EscapeQuote(objUser.Ban_Lv) & " )"

                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                    ' INSERT BẢNG LOG
                    strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV, NGAY_HT) " & _
                                   "VALUES (" & objUser.Ma_NV & "  ,'" & _
                                   objUser.Ten & "','" & _
                                   objUser.Mat_Khau & "','" & _
                                   objUser.Ma_Nhom & "','" & _
                                   objUser.Chuc_Danh & "','" & _
                                   objUser.Trang_thai & "','" & _
                                   objUser.TEN_ND & "','" & _
                                   objUser.Teller_Id & "','" & _
                                   objUser.MaCN & "'," & _
                                   objUser.LDAP & ",'" & _
                                    objUser.HanMuc_Tu & "','" & _
                                    objUser.HanMuc_Den & "','" & _
                                    objUser.User_Domain & "'," & _
                                   Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                   " SYSDATE)"
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                Else
                    strSql = "UPDATE TCS_DM_NHANVIEN " & _
                                           "SET TEN = '" & objUser.Ten & "' " & _
                                           ",CHUC_DANH='" & objUser.Chuc_Danh & "' " & _
                                           ",TELLER_ID='" & objUser.Teller_Id & "' " & _
                                           ",Ma_CN='" & objUser.MaCN & "' " & _
                                           ",Ma_Nhom='" & objUser.Ma_Nhom & "' " & _
                                           ",Mat_Khau = '" & objUser.Mat_Khau & "'" & _
                                           ",BAN_LV='" & objUser.Ban_Lv & "' " & _
                                           ",LDAP=" & objUser.LDAP & _
                                           ",hanmuc_tu='" & objUser.HanMuc_Tu & _
                                           "',hanmuc_den='" & objUser.HanMuc_Den & _
                                           "',USERDOMAIN='" & objUser.User_Domain & _
                                           "',TINH_TRANG='" & objUser.Trang_thai & "' " & _
                                           "WHERE Ma_NV =" & objUser.Ma_NV
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                    strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV, NGAY_HT) " & _
                                                                     "VALUES (" & objUser.Ma_NV & "  ,'" & _
                                                                     objUser.Ten & "','" & _
                                                                     objUser.Mat_Khau & "','" & _
                                                                     objUser.Ma_Nhom & "','" & _
                                                                     objUser.Chuc_Danh & "','" & _
                                                                     objUser.Trang_thai & "','" & _
                                                                     objUser.TEN_ND & "','" & _
                                                                     objUser.Teller_Id & "','" & _
                                                                     objUser.MaCN & "'," & _
                                                                     objUser.LDAP & ",'" & _
                                                                    objUser.HanMuc_Tu & "','" & _
                                                                    objUser.HanMuc_Den & "','" & _
                                                                    objUser.User_Domain & "'," & _
                                                                     Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                                                     " SYSDATE)"
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                End If

                transCT.Commit()
            Catch ex As Exception
                blnResult = False
                transCT.Rollback()
              log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    transCT.Dispose()
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function DoiCN_EXITS(ByVal objUser As infUser) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                conn = New DataAccess
                Dim ma_NV As String = Common.mdlCommon.getDataKey("TCS_NHANVIEN_ID_SEQ.NEXTVAL")
                transCT = conn.BeginTransaction
                ' If tt_doiCN Then
                If CheckUSer_GD(objUser.Ma_NV) = "1" Then
                    strSql = "UPDATE TCS_DM_NHANVIEN SET TINH_TRANG='1' " & _
                                             "WHERE Ma_NV=" & objUser.Ma_NV
                Else
                    strSql = "DELETE FROM  TCS_DM_NHANVIEN WHERE Ma_NV=" & objUser.Ma_NV
                End If
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV, NGAY_HT) " & _
                                              "VALUES (" & objUser.Ma_NV & "  ,'" & _
                                              objUser.Ten & "','" & _
                                              objUser.Mat_Khau & "','" & _
                                              objUser.Ma_Nhom & "','" & _
                                              objUser.Chuc_Danh & "','1','" & _
                                              objUser.TEN_ND & "','" & _
                                              objUser.Teller_Id & "','" & _
                                              objUser.MaCN & "'," & _
                                              objUser.LDAP & ",'" & _
                                              objUser.HanMuc_Tu & "','" & _
                                              objUser.HanMuc_Den & "','" & _
                                              objUser.User_Domain & "'," & _
                                              Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                              " SYSDATE)"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)


                strSql = "UPDATE TCS_DM_NHANVIEN " & _
                                       "SET TEN = '" & objUser.Ten & "' " & _
                                       ",CHUC_DANH='" & objUser.Chuc_Danh & "' " & _
                                       ",TELLER_ID='" & objUser.Teller_Id & "' " & _
                                       ",Ma_CN='" & objUser.MaCN & "' " & _
                                       ",Ma_Nhom='" & objUser.Ma_Nhom & "' " & _
                                       ",Mat_Khau = '" & objUser.Mat_Khau & "'" & _
                                       ",BAN_LV='" & objUser.Ban_Lv & "' " & _
                                       ",LDAP=" & objUser.LDAP & _
                                       ",hanmuc_tu='" & objUser.HanMuc_Tu & _
                                       "',hanmuc_den='" & objUser.HanMuc_Den & _
                                       "',USERDOMAIN='" & objUser.User_Domain & _
                                       "',TINH_TRANG='0' " & _
                                       " WHERE MA_NV IN (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE TEN_DN='" & objUser.TEN_ND & "' " & _
                                        " AND MA_CN='" & objUser.MaCN & "')"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,BAN_LV,hanmuc_tu,hanmuc_den,USERDOMAIN, NGAY_HT) " & _
                                                                 "VALUES (" & objUser.Ma_NV & "  ,'" & _
                                                                 objUser.Ten & "','" & _
                                                                 objUser.Mat_Khau & "','" & _
                                                                 objUser.Ma_Nhom & "','" & _
                                                                 objUser.Chuc_Danh & "','" & _
                                                                 objUser.Trang_thai & "','" & _
                                                                 objUser.TEN_ND & "','" & _
                                                                 objUser.Teller_Id & "','" & _
                                                                 objUser.MaCN & "'," & _
                                                                 objUser.LDAP & ",'" & _
                                                                objUser.HanMuc_Tu & "','" & _
                                                                objUser.HanMuc_Den & "','" & _
                                                                objUser.User_Domain & "'," & _
                                                                 Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                                                 " SYSDATE)"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                transCT.Commit()
            Catch ex As Exception
                blnResult = False
                transCT.Rollback()
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    transCT.Dispose()
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function DoiMK(ByVal objUser As infUser) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction()
                strSql = "UPDATE TCS_DM_NHANVIEN " & _
                    " SET Mat_Khau = '" & objUser.Mat_Khau & "'" & _
                    " WHERE Ma_NV =" & objUser.Ma_NV

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                ' INSERT BẢNG LOG
                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,BAN_LV, NGAY_HT) " & _
                               "VALUES (" & objUser.Ma_NV & "  ,'" & _
                               objUser.Ten & "','" & _
                               objUser.Mat_Khau & "','" & _
                               objUser.Ma_Nhom & "','" & _
                               objUser.Chuc_Danh & "','" & _
                               objUser.Trang_thai & "','" & _
                               objUser.TEN_ND & "','" & _
                               objUser.Teller_Id & "','" & _
                               objUser.MaCN & "'," & _
                               Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                               " SYSDATE)"

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                blnResult = False
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    transCT.Dispose()
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function

        Public Function UpdateBanLV(ByVal objUser As infUser, ByVal strBanLv As String, ByVal strIP As String) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "UPDATE TCS_DM_NHANVIEN " & _
                    "SET LOG_ON= '1',local_ip = '" & strIP & "' " & _
                    "WHERE Ma_NV =" & objUser.Ma_NV
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                blnResult = False
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function

        Public Function DoiNhom(ByRef objUser As infUser) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Update TCS_DM_NHANVIEN " & _
                    "Set NH_ID = '" & objUser.Ma_Nhom & "' " & _
                    "Where MA_NV =" & objUser.Ma_NV
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                blnResult = False
              log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function

        Public Function SelectNhom() As DataSet
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Select MA_NHOM,TEN_NHOM From TCS_DM_NHOM"
                Return conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        Public Function TCS_LST_DM_NHANVIEN() As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Try
                cnQNhanvien = New DataAccess
                strSQL = "SELECT N.Ma_NV,N.Ten,N.Chuc_Danh,N.Ban_LV, N.local_ip,Ten_DN,CASE tinh_trang  WHEN '0' THEN 'Hoạt động' ELSE 'Đang bị khóa'" & _
                "END TINH_TRANG FROM TCS_DM_NHANVIEN N WHERE TINH_TRANG= 0  Order by Ma_NV "
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString)
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function
        Public Function TCS_LST_DM_NHANVIEN_DIEMTHU(Optional ByRef objUser As infUser = Nothing) As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Dim where As String = ""
            Try
                cnQNhanvien = New DataAccess
                If Not objUser Is Nothing Then
                    If objUser.Chuc_Danh <> "" Then
                        where = where + " and UPPER(N.Chuc_Danh) like UPPER ('%" & objUser.Chuc_Danh & "%')"
                    End If
                    If objUser.Ten <> "" Then
                        where = where + " and UPPER(N.ten) like UPPER('%" & objUser.Ten & "%') "
                    End If
                    If objUser.TEN_ND <> "" Then
                        where = where + " and UPPER(N.ten_DN) like UPPER('%" & objUser.TEN_ND & "%')"
                    End If
                    If objUser.MaCN <> "" Then
                        where = where + " and UPPER(N.ma_CN) = UPPER('" & objUser.MaCN & "')"
                    End If
                    If objUser.Trang_thai <> "" Then
                        where = where + " and N.tinh_trang ='" & objUser.Trang_thai & "'"
                    End If
                    If objUser.Ma_Nhom <> "" Then
                        where = where + " and N.ma_nhom ='" & objUser.Ma_Nhom & "'"
                    End If
                    If objUser.LDAP <> "" Then
                        where = where + " and N.LDAP ='" & objUser.LDAP & "'"
                    End If
                    If objUser.User_Domain <> "" Then
                        where = where + " and N.userdomain ='" & objUser.User_Domain & "'"
                    End If
                End If
                strSQL = "SELECT N.Ma_NV,N.Ten,N.Chuc_Danh,N.Ban_LV,N.TELLER_ID,N.ten_DN, N.local_ip,CASE n.tinh_trang  WHEN '0' THEN 'Đang HĐ' ELSE 'Ngung HĐ' " & _
                "END TINH_TRANG, D.TEN TEN_DTHU, N.Ma_CN||'-'||T.NAME Ma_CN FROM TCS_DM_NHANVIEN N,TCS_DM_DIEMTHU D, tcs_dm_chinhanh t  WHERE  N.BAN_LV=D.MA_DTHU " & _
                " AND T.branch_code= N.MA_CN " & where & "  Order by Ma_NV "
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function
        'hoapt: lay thong tin mot nhan vien
        Public Function NHANVIEN_DETAIL(ByVal Ma_NV As String) As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Try
                cnQNhanvien = New DataAccess
                strSQL = "SELECT MA_NV,TEN,CHUC_DANH,BAN_LV,MAT_KHAU,TELLER_ID, LOCAL_IP,MA_NHOM,TEN_DN,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN, TINH_TRANG FROM TCS_DM_NHANVIEN WHERE Ma_NV='" & Ma_NV & "'"
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString)
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function
        'hoapt: lay thong tin mot nhan vien
        Public Function TIMKIEM_NHANVIEN(Optional ByVal Ma_NV As String = "", Optional ByVal Ma_CN As String = "") As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Try
                cnQNhanvien = New DataAccess
                strSQL = "SELECT a.ma_nv, a.ma_nhom,a.ten_dn,d.name ten_nh_cn, a.ten, a.ban_lv,a.chuc_danh,a.mat_khau,a.teller_id,a.ma_cn" _
                & " FROM tcs_dm_nhanvien a, tcs_dm_chinhanh d " _
                & " WHERE d.id = a.ma_cn and a.ma_cn = '" & Ma_CN & "' and log_on='1' and a.tinh_trang='0'"
                If Ma_NV.Trim <> "" Then
                    strSQL = strSQL & "AND upper(a.ten_dn) like '%" & Ma_NV.ToUpper & "%'"
                End If
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString)
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function
        'hoapt: lay thong tin mot nhan vien
        Public Function TIMKIEM_NHOMNV(ByVal Ma_NV As String) As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Try
                cnQNhanvien = New DataAccess
                strSQL = "SELECT MA_NHOM FROM TCS_DM_NHANVIEN WHERE Ma_NV='" & Ma_NV & "'"
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString)
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function
        Public Function UpdateTTLogin(ByVal strMaNV As String, ByVal strTrangThai As String, Optional ByVal strIP_Local As String = "") As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess

                strSql = "UPDATE TCS_DM_NHANVIEN " & _
                    " SET LOG_ON = '" & strTrangThai & "'," & _
                    " LOCAL_IP='" & strIP_Local & "' " & _
                    " WHERE Ma_NV =" & strMaNV
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                blnResult = False
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function CheckUSer_GD(ByVal strMaNV As String) As String
            Dim blnResult As String = String.Empty
            Dim conn As DataAccess
            Dim strSql As String
            Dim ds As DataSet
            Try
                conn = New DataAccess

                strSql = "Select 1 from tcs_ctu_hdr where ma_nv=" & strMaNV

                ds = DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
                If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                    blnResult = "1"
                Else
                    blnResult = "0"
                End If
            Catch ex As Exception
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function Insert_Log_DN(Optional ByVal strMA_NV As String = "", Optional ByVal strTEN_DN As String = "", Optional ByVal strLOCAL_IP As String = "", Optional ByVal strSTATUS As String = "", Optional ByVal strMA_CN As String = "", Optional ByVal strUSER_KICK As String = "") As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess

                strSql = "INSERT INTO TCS_LOG_DANGNHAP (MA_NV,TEN_DN,LOCAL_IP,ACTION_TIME,STATUS,MA_CN,USER_KICK) " & _
                    " VALUES (" & strMA_NV & "," & _
                    " '" & strTEN_DN & "', " & _
                    " '" & strLOCAL_IP & "', " & _
                    " SYSDATE, " & _
                    " " & strSTATUS & " ," & _
                    " '" & strMA_CN & "', " & _
                    " '" & strUSER_KICK & "' )"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                blnResult = False
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
    End Class

End Namespace