Imports VBOracleLib

Namespace HeThong
    Public Class daGroupUsers
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Private _dac As DataAccess = New DataAccess

        'Hien dung Boolean len chua mo ta ro ma loi (True - False)
        'Co the thay bang Integer voi cac dinh nghia ma loi khac nhau
        'Vi du :
        '  - 0:Thanh cong, -1: Loi he thong, 1:Trung MaNhom .....
        Private _flag As Boolean = False

        Public Function Insert(ByVal objGroup As infGroupUsers) As Boolean
            Try
                Dim strSql As String
                If (CheckBeforeInsert(objGroup)) Then
                    strSql = "INSERT INTO TCS_DM_NHOM (MA_NHOM,TEN_NHOM) " & _
                                        "VALUES ('" & objGroup.MaNhom & "','" & objGroup.TenNhom & "')"
                Else
                    Return False
                End If

                _flag = IIf(_dac.ExecuteNonQuery(strSql, CommandType.Text) > 0, True, False)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi thêm mới nhóm người sử dụng")
                _flag = False
                Throw ex
            Finally
            End Try
            Return _flag
        End Function

        Public Function Update(ByVal objGroup As infGroupUsers) As Boolean
            Dim strSql As String = "UPDATE TCS_DM_NHOM SET TEN_NHOM='" & objGroup.TenNhom & "' WHERE MA_NHOM='" & objGroup.MaNhom & "'"
            Try
                _flag = IIf(_dac.ExecuteNonQuery(strSql, CommandType.Text) > 0, True, False)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi cập nhật nhóm người sử dụng")
                _flag = False
                Throw ex
            Finally
            End Try
            Return _flag
        End Function

        Public Function Delete(ByVal objGroup As infGroupUsers) As Boolean

            Dim strSql As String = "DELETE FROM TCS_DM_NHOM WHERE MA_NHOM='" & objGroup.MaNhom & "'"
            Try
                If (CheckBeforeDeleted(objGroup)) Then
                    _flag = IIf(_dac.ExecuteNonQuery(strSql, CommandType.Text) > 0, True, False)
                    'Return False
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi xóa nhóm người sử dụng")
                Throw ex
                _flag = False
            Finally
            End Try
            Return _flag
        End Function

        Public Function Search(ByVal objGroup As infGroupUsers) As DataTable
            Dim strSQL As String = "SELECT MA_NHOM, TEN_NHOM FROM TCS_DM_NHOM"
            Try
                If objGroup.MaNhom IsNot Nothing Then
                    If objGroup.MaNhom.Length > 0 Then
                        strSQL += " WHERE MA_NHOM = '" & objGroup.MaNhom & "'"
                    End If
                End If
                If objGroup.TenNhom IsNot Nothing Then
                    If objGroup.TenNhom.Length > 0 Then
                        strSQL += " WHERE TEN_NHOM LIKE '%" & objGroup.TenNhom & "%'"
                    End If
                End If
                Return _dac.ExecuteToTable(strSQL)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin nhóm người sử dụng")
                Return New DataTable
                Throw ex
            End Try
        End Function


        Public Function CheckBeforeInsert(ByVal objGroup As infGroupUsers) As Boolean
            Dim dt As New DataTable
            dt = Search(objGroup)
            _flag = IIf(dt.Rows.Count > 0, False, True)
            Return _flag
        End Function

        Public Function CheckBeforeDeleted(ByVal objGroup As infGroupUsers) As Boolean
            Try
                Dim strSql As String

                strSql = "SELECT COUNT(1) FROM TCS_PHANQUYEN WHERE MA_NHOM = '" & objGroup.MaNhom & "'"
                _flag = IIf(Integer.Parse(_dac.ExecuteSQLScalar(strSql)) > 0, False, True)
                If _flag = True Then
                    strSql = "SELECT COUNT(1) FROM TCS_DM_NHANVIEN WHERE MA_NHOM = '" & objGroup.MaNhom & "'"
                    _flag = IIf(Integer.Parse(_dac.ExecuteSQLScalar(strSql)) > 0, False, True)
                End If
            Catch ex As Exception

            End Try
            Return _flag
        End Function
    End Class

End Namespace
