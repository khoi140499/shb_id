Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon

Namespace HeThong
    Public Class buGroupUsers

        Sub New()
        End Sub

        Private daGroup As New daGroupUsers

        Private _flag As Boolean = True
        Private _status As String

        Public Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal value As String)
                _status = value
            End Set
        End Property

        Public Function Insert(ByVal objGUsers As infGroupUsers) As Boolean
            Try
                Return daGroup.Insert(objGUsers)
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Function Update(ByVal objGUsers As infGroupUsers) As Boolean
            Try
                Return daGroup.Update(objGUsers)
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Function Save(ByVal objGUsers As infGroupUsers) As Boolean
            If _status.Equals("Update") Then
                Return Update(objGUsers)
            Else
                Return Insert(objGUsers)
            End If
        End Function

        Public Function Delete(ByVal objGUsers As infGroupUsers) As Boolean
            Try
                Return daGroup.Delete(objGUsers)
            Catch ex As Exception

            End Try
        End Function

        Public Function Search(ByVal objGUsers As infGroupUsers) As DataTable
            Return daGroup.Search(objGUsers)
        End Function

    End Class
End Namespace
