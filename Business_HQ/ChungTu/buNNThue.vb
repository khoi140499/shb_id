﻿Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common
Imports Business_HQ.NewChungTu


Namespace ChungTu
    Public Class buNNThue
        Public Shared Function SoThue_CTN_20180423(ByVal strMaNNT As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.SoThue_CTN_20180423(strMaNNT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function SoThue_CTN(ByVal strMaNNT As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.SoThue_CTN(strMaNNT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function SoThue_CTN(ByVal strMaNNT As String, ByVal strSoQD As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.SoThue_CTN(strMaNNT, strSoQD)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ToKhai(ByVal strMaNNT As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.ToKhai(strMaNNT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function ToKhai(ByVal strMaNNT As String, ByVal strSoTK As String, ByVal strLoaiTT As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.ToKhai(strMaNNT, strSoTK, strLoaiTT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function ToKhai(ByVal strMaNNT As String, ByVal strMaCQThu As String, ByVal strSoTK As String, ByVal strLHXNK As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.ToKhai(strMaNNT, strMaCQThu, strSoTK, strLHXNK)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' ******************************************************'
        ''' Name: To the khai.
        ''' Author:ANHLD
        ''' Goals: Lay to khai theo loai tien thue
        ''' Date: 21.09.2012
        ''' Event: Add
        ''' ************************BEGIN******************************'
        ''' <param name="strMaNNT">The STR ma NNT.</param>
        ''' <param name="strMaCQThu">The STR ma CQ thu.</param>
        ''' <param name="strSoTK">The STR so TK.</param>
        ''' <param name="strLHXNK">The STR LHXNK.</param>
        ''' <param name="strLtt">The STR LTT.</param>
        ''' <returns>DataSet.</returns>
        Public Shared Function ToKhai(ByVal strMaNNT As String, ByVal strMaCQThu As String, ByVal strSoTK As String, ByVal strLHXNK As String, ByVal strLtt As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.ToKhai(strMaNNT, strMaCQThu, strSoTK, strLHXNK, strLtt)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' ************************END********************************'
        ''' 
        Public Shared Function ToKhaiHQ(ByVal strMaNNT As String, ByVal strMaCQThu As String, ByVal strSoTK As String, ByVal strLHXNK As String, ByVal strLtt As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.ToKhaiHQ(strMaNNT, strMaCQThu, strSoTK, strLHXNK, strLtt)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function ToKhai(ByVal strMaNNT As String, ByVal strSoTK As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.ToKhai(strMaNNT, strSoTK)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function NNThue(ByVal strMaNNT As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.NNThue(strMaNNT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function NNT_VL(ByVal strMaNNT As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.NNT_VL(strMaNNT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TruocBa(ByVal strMaNNT As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.TruocBa(strMaNNT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function SoThue_TruocBa(ByVal strMaNNT As String) As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.SoThue_TruocBa(strMaNNT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function VangLai() As DataSet
            Try
                Dim nnt As New daNNThue
                Return nnt.VangLai()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Get_VangLai() As String
            Try
                Dim nnt As New daNNThue
                Return nnt.Get_VangLai()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Exists_NNThue(ByVal strMaNNT As String) As NguonNNT
            Try
                Dim nnt As New daNNThue
                Select Case gbytUutien
                    Case 1
                        If (nnt.Exists_TruocBa(strMaNNT)) Then Return NguonNNT.TruocBa
                        If (nnt.Exists_NNT(strMaNNT)) Then Return NguonNNT.NNThue_CTN
                        If (nnt.Exists_NNT_VL(strMaNNT)) Then Return NguonNNT.NNThue_VL
                        If (nnt.Exists_SoThue_CTN(strMaNNT)) Then Return NguonNNT.SoThue_CTN
                        If (nnt.Exists_VangLai(strMaNNT)) Then Return NguonNNT.VangLai
                    Case 2
                        If (nnt.Exists_ToKhai(strMaNNT)) Then Return NguonNNT.HaiQuan
                        If (nnt.Exists_NNT(strMaNNT)) Then Return NguonNNT.NNThue_CTN
                        If (nnt.Exists_NNT_VL(strMaNNT)) Then Return NguonNNT.NNThue_VL
                        If (nnt.Exists_SoThue_CTN(strMaNNT)) Then Return NguonNNT.SoThue_CTN
                        If (nnt.Exists_VangLai(strMaNNT)) Then Return NguonNNT.VangLai
                    Case 3
                        If (nnt.Exists_NNT(strMaNNT)) Then Return NguonNNT.NNThue_CTN
                        If (nnt.Exists_NNT_VL(strMaNNT)) Then Return NguonNNT.NNThue_VL
                        If (nnt.Exists_SoThue_CTN(strMaNNT)) Then Return NguonNNT.SoThue_CTN
                        If (nnt.Exists_VangLai(strMaNNT)) Then Return NguonNNT.VangLai
                    Case Else
                        'Kienvt : lay thong tin tu so thue truoc khong co thi moi tim trong
                        'danh muc nguoi nop thue
                        If (nnt.Exists_SoThue_CTN(strMaNNT)) Then Return NguonNNT.SoThue_CTN
                        If (nnt.Exists_VangLai(strMaNNT)) Then Return NguonNNT.VangLai
                        If (nnt.Exists_ToKhai(strMaNNT)) Then Return NguonNNT.HaiQuan
                        If (nnt.Exists_TruocBa(strMaNNT)) Then Return NguonNNT.TruocBa
                        If (nnt.Exists_NNT(strMaNNT)) Then Return NguonNNT.NNThue_CTN
                        If (nnt.Exists_NNT_VL(strMaNNT)) Then Return NguonNNT.NNThue_VL
                End Select

                Return NguonNNT.Khac
            Catch ex As Exception
                Return NguonNNT.Loi
            End Try
        End Function

        Public Shared Function Exists_VangLai(ByVal strMaNNT As String) As Boolean
            Try
                Dim nnt As New daNNThue
                Return nnt.Exists_VangLai(strMaNNT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub NNT_VL_Insert(ByVal objHDR As infChungTuHDR, ByVal objDTL As infChungTuDTL)
            Try
                Dim daNNT As New daNNThue
                daNNT.NNT_VL_Insert(objHDR, objDTL)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Public Shared Sub NNT_VL_Update(ByVal objHDR As Business.Common.infChungTuHDR, ByVal objDTL As infChungTuDTL)
        '    Try
        '        Dim daNNT As New daNNThue
        '        daNNT.NNT_VL_Update(objHDR, objDTL)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Shared Sub NNT_VL_Update(ByVal objHDR As ChungTu.infChungTuHDR, ByVal objDTL As infChungTuDTL)
        '    Try
        '        Dim daNNT As New daNNThue
        '        daNNT.NNT_VL_Update(objHDR, objDTL)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Public Shared Sub NNT_VL_Update(ByVal objHDR As infChungTuHDR, ByVal objDTL As infChungTuDTL)
            Try
                Dim daNNT As New daNNThue
                daNNT.NNT_VL_Update(objHDR, objDTL)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function CheckSum(ByVal P_TIN As String) As Byte
            Dim Prod As Long
            Dim Chkdg As String
            Dim pos As Integer
            Dim v_Prefix As String
            Dim v_Suffix As String

            Try
                pos = InStr(P_TIN, "-")
                If pos = 0 Then 'Khong co Suffix
                    v_Prefix = P_TIN
                    If v_Prefix = vbNullString OrElse v_Prefix.Length <> 10 Then Return 2
                    Prod = CLng(v_Prefix) 'Kiem tra ma Tin co ky tu khong hop le?  
                    Chkdg = Tinh_Checksum(v_Prefix)
                    If Chkdg <> Mid(v_Prefix, v_Prefix.Length, 1) Then Return 1
                Else 'co Suffix
                    v_Prefix = Mid(P_TIN, 1, pos - 1)
                    v_Suffix = Mid(P_TIN, pos + 1)
                    If (v_Prefix = vbNullString OrElse v_Prefix.Length <> 10) _
                    Or (v_Suffix = vbNullString OrElse v_Suffix.Length <> 3) Then Return 2
                    Prod = CLng(v_Prefix)  'Kiem tra ma Tin co ky tu khong hop le?  
                    Prod = CLng(v_Suffix)  'Kiem tra Suffix co ky tu khong hop le?  
                    Chkdg = Tinh_Checksum(v_Prefix)
                    If Chkdg <> Mid(v_Prefix, v_Prefix.Length, 1) Then Return 1
                End If
                Return 0
            Catch ex As Exception
                'Neu Prefix hay Suffix co chua ky tu khong hop le
                Return 3
            End Try
        End Function

        Private Shared Function Tinh_Checksum(ByVal P_Tin As String) As String
            Dim Prod As Long
            Dim Chkdg As Integer
            Prod = 31 * CInt(Mid(P_Tin, 1, 1)) + _
                   29 * CInt(Mid(P_Tin, 2, 1)) + _
                   23 * CInt(Mid(P_Tin, 3, 1)) + _
                   19 * CInt(Mid(P_Tin, 4, 1)) + _
                   17 * CInt(Mid(P_Tin, 5, 1)) + _
                   13 * CInt(Mid(P_Tin, 6, 1)) + _
                    7 * CInt(Mid(P_Tin, 7, 1)) + _
                    5 * CInt(Mid(P_Tin, 8, 1)) + _
                    3 * CInt(Mid(P_Tin, 9, 1))
            Chkdg = 10 - (Prod Mod 11)
            Return CStr(Chkdg)
        End Function
    End Class
End Namespace