﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.Common.mdlCommon

Namespace ChungTu
    Public Class daLapGNT
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "Lập GNT từ biên lai thu"
        Public Function TCS_GetLyDo(ByVal strWhere As String) As String
            Dim conn As DataAccess
            Dim ds As DataSet
            Dim strResult As String = ""
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "SELECT bl.Ly_do FROM tcs_ctbl bl WHERE (bl.trang_thai='00') " & strWhere
                ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                If Not IsEmptyDataSet(ds) Then
                    strResult = GetString(ds.Tables(0).Rows(0).Item(0))
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy lý do nộp")
                Throw ex
                'LogDebug.WriteLog("Lỗi khi lấy lý do nộp:" & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not ds Is Nothing Then
                    ds.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return strResult
        End Function

        Public Function TCS_GetNoidung(ByVal strWhere As String) As String
            Dim conn As DataAccess
            Dim ds As DataSet
            Dim strResult As String = ""
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "SELECT ten FROM TCS_DM_MUC_TMUC  WHERE tinh_trang='1' and MA_TMUC= '" + strWhere + "'"
                ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                If Not IsEmptyDataSet(ds) Then
                    strResult = GetString(ds.Tables(0).Rows(0).Item(0))
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy lý do nộp")
                Throw ex
                'LogDebug.WriteLog("Lỗi khi lấy lý do nộp:" & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not ds Is Nothing Then
                    ds.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return strResult
        End Function

        Public Function TCS_dsGNT(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy DS giấy nộp tiền
            ' Tham số       : 
            ' Giá trị trả về: 
            ' ----------------------------------------------------------------
            Dim conn As DataAccess
            Dim ds As DataSet
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "SELECT bl.tk_co, lh.maquy, lh.ma_chuong, lh.ma_khoan, lh.ma_tmuc, lh.ma_xa, " & _
                                "lh.ma_dp, SUM (bl.ttien) AS ttien, bl.trang_thai " & _
                                "FROM tcs_ctbl bl, tcs_dm_lhthu_mlns lh " & _
                                "WHERE (lh.ID(+) = bl.LH_ID) And (bl.trang_thai <>'03')" & _
                                strWhere & _
                                "GROUP BY bl.tk_co, lh.maquy, lh.ma_chuong, lh.ma_khoan, " & _
                                "lh.ma_tmuc, lh.ma_xa, lh.ma_dp, bl.trang_thai "
                ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy danh sách giấy nộp tiền")
                'LogDebug.WriteLog("Lỗi khi lấy danh sách biên lai thu phục vụ lập chứng từ:" & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not ds Is Nothing Then
                    ds.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function SelectBL(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy DS các BL có chung số tài khoản có
            ' Tham số       : 
            ' Giá trị trả về:            
            ' ----------------------------------------------------------------
            Dim connBL As DataAccess
            Dim strSql As String
            Try
                connBL = New DataAccess
                strSql = "Select bl.Ngay_KB, bl.Ma_NV, bl.So_BT, bl.KyHieu_BL,bl.So_BL From TCS_CTBL bl Where bl.Trang_Thai = '00'" & _
                         strWhere & " Order by So_BL asc"
                Return connBL.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy danh sách các BL có chung số tài khoản có")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu biên lai: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try
        End Function
        'Public Function SelectCTLap() As DataSet
        '    '-----------------------------------------------------------------
        '    ' Mục đích      : Lấy DS toàn bộ các CT lập trong ngày của nhân viên đó
        '    ' Tham số       : 
        '    ' Giá trị trả về: 
        '    ' ----------------------------------------------------------------
        '    Dim connBL As DataAccess
        '    Dim dsResult As DataSet
        '    Dim strSql As String
        '    Try
        '        connBL = New DataAccess

        '        strSql = "SELECT distinct CTU.kyhieu_ct, CTU.so_ct,CTU.so_BT,CTU.trang_thai,CTU.ma_nv,CTU.lan_in,CTU.TTien, CTU.shkb, CTU.Ngay_kb, CTU.Ma_Dthu " & _
        '                    " FROM tcs_ctu_hdr CTU " & _
        '                    " WHERE CTU.ma_nv =" & gstrTenND & _
        '                    " AND CTU.ngay_kb =" & gdtmNgayLV & _
        '                    " AND CTU.CTU_GNT_BL='01' " & _
        '                    " Order by CTU.so_bt desc "

        '        dsResult = connBL.ExecuteReturnDataSet(strSql, CommandType.Text)
        '    Catch ex As Exception
        '        LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu CT: " & ex.ToString, EventLogEntryType.Error)
        '        Throw ex
        '    Finally
        '        If Not connBL Is Nothing Then
        '            connBL.Dispose()
        '        End If
        '    End Try
        '    Return dsResult
        'End Function

        'Public Function SelectCTLap(ByVal gstrTenND As Integer) As DataSet
        '    '-----------------------------------------------------------------
        '    ' Mục đích      : Lấy DS toàn bộ các CT lập trong ngày của nhân viên đó
        '    ' Tham số       : 
        '    ' Giá trị trả về: 
        '    ' ----------------------------------------------------------------
        '    Dim connBL As DataAccess
        '    Dim dsResult As DataSet
        '    Dim strSql As String
        '    Try
        '        connBL = New DataAccess

        '        'strSql = "SELECT distinct CTU.kyhieu_ct, CTU.so_ct,CTU.so_BT,CTU.trang_thai,CTU.ma_nv,CTU.lan_in,CTU.TTien, CTU.shkb, CTU.Ngay_kb, CTU.Ma_Dthu " & _
        '        '            " FROM tcs_ctu_hdr CTU " & _
        '        '            " WHERE CTU.ma_nv =" & gstrTenND & _
        '        '            " AND CTU.ngay_kb =" & gdtmNgayLV & _
        '        '             " Order by CTU.so_bt desc "

        '        'strSql = "SELECT distinct CTU.kyhieu_ct, CTU.so_ct,CTU.so_BT,CTU.trang_thai,CTU.ma_nv,CTU.lan_in,CTU.TTien, ctu.shkb, ctu.Ngay_kb, ctu.Ma_Dthu " & _
        '        '       " FROM tcs_ctbl cbl, tcs_ctu_hdr ctu " & _
        '        '       " WHERE cbl.kyhieu_ct = ctu.kyhieu_ct  AND cbl.so_ct = ctu.so_ct " & _
        '        '       " AND CTU.ma_nv =" & gstrTenND & _
        '        '       " AND CTU.ngay_kb =" & gdtmNgayLV & _
        '        '       " Order by CTU.so_bt desc "

        '        'Bo ten ND
        '        strSql = "SELECT distinct CTU.kyhieu_ct, CTU.so_ct,CTU.so_BT,CTU.trang_thai,CTU.ma_nv,CTU.lan_in,CTU.TTien, ctu.shkb, ctu.Ngay_kb, ctu.Ma_Dthu " & _
        '              " FROM tcs_ctbl cbl, tcs_ctu_hdr ctu " & _
        '              " WHERE cbl.kyhieu_ct = ctu.kyhieu_ct  AND cbl.so_ct = ctu.so_ct " & _
        '              " AND CTU.ngay_kb =" & gdtmNgayLV & _
        '              " Order by CTU.so_bt desc "

        '        dsResult = connBL.ExecuteReturnDataSet(strSql, CommandType.Text)
        '    Catch ex As Exception
        '        LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu CT: " & ex.ToString, EventLogEntryType.Error)
        '        Throw ex
        '    Finally
        '        If Not connBL Is Nothing Then
        '            connBL.Dispose()
        '        End If
        '    End Try
        '    Return dsResult
        'End Function

        Public Function dsBKThuTP(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy BK thu tien phat
            ' Tham số       : 
            ' Giá trị trả về: 
            ' ----------------------------------------------------------------
            Dim conn As DataAccess
            Dim sql As String
            Try
                sql = " Select CB.Ten_NNTien, CB.DC_NNTien,CB.Ma_CQQD, CQ.Ten_CQQD, CB.So_QD, CB.Ngay_QD, CB.Kyhieu_BL, CB.So_BL, CB.TTien as So_Tien, CB.Ngay_CT " & _
                "From TCS_CTBL CB " & strWhere
                conn = New DataAccess
                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy bảng kê thu tiền phạt")
                'LogDebug.WriteLog("Lấy BK thu tiền phạt: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try

        End Function

        Public Function LayTLDT(ByVal sql) As DataSet
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi truy vấn mã tỷ lệ điều tiết")
                Throw ex
                'LogDebug.WriteLog("Lỗi truy vấn mã tỷ lệ điều tiết: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
#End Region

    End Class
End Namespace