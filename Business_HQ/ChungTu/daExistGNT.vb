﻿Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Namespace ChungTu
    Public Class daExistGNT
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        'Public Function GetListCT(ByVal strMaNNT As String) As DataSet
        '    Dim dsCT As New DataSet
        '    Dim cnCT As DataAccess
        '    Dim strSql As String
        '    Try
        '        cnCT = New DataAccess
        '        strSql = "Select KyHieu_CT,SO_CT,Ma_NNThue,Ten_NNThue," & _
        '                 "TTien,Lan_In " & _
        '                 "From TCS_CTU_HDR " & _
        '                 "Where Ma_NNThue='" & strMaNNT & "' And Trang_Thai <> '04' And " & _
        '                 "NGAY_CT = " &  & _
        '                 " Order by KYHIEU_CT,SO_CT Desc"
        '        dsCT = cnCT.ExecuteReturnDataSet(strSql, CommandType.Text)
        '    Catch ex As Exception
        '        LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách chứng từ: " & ex.ToString, EventLogEntryType.Error)
        '    Finally
        '        If Not cnCT Is Nothing Then
        '            cnCT.Dispose()
        '        End If
        '    End Try
        '    Return dsCT
        'End Function

        Public Function GetListCTDetail(ByVal strSoCT As String, ByVal strKHCT As String) As DataSet
            Dim cnCT As DataAccess
            Dim strSql As String
            Dim dsCT As New DataSet
            Try
                cnCT = New DataAccess
                strSql = "Select DTL.MA_CAP,DTL.MA_CHUONG,DTL.MA_LOAI,DTL.MA_KHOAN,DTL.MA_MUC," & _
                         "DTL.MA_TMUC,DTL.MA_TLDT,DTL.SOTIEN From TCS_CTU_DTL DTL,TCS_CTU_HDR HDR " & _
                         "Where (DTL.SHKB = HDR.SHKB) And (DTL.Ngay_KB = HDR.Ngay_KB) " & _
                         "And (DTL.Ma_NV = HDR.Ma_NV) And (DTL.So_BT = HDR.So_BT) " & _
                         "And (DTL.Ma_DThu = HDR.Ma_DThu) " & _
                         "And (HDR.So_CT = '" & strSoCT & "') And (HDR.KyHieu_CT ='" & strKHCT & "')"
                dsCT = cnCT.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy thông tin chi tiết chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnCT Is Nothing Then
                    cnCT.Dispose()
                End If
            End Try
            Return dsCT
        End Function

        Public Function UpdateHuyCT(ByVal strSoCT As String, ByVal strKHCT As String) As Boolean
            Dim cnCT As DataAccess
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                cnCT = New DataAccess
                strSql = "Update TCS_CTU_HDR Set TRANG_THAI='04' " & _
                         "Where (So_CT = '" & strSoCT & "') And (KyHieu_CT = '" & strKHCT & "')"
                cnCT.ExecuteNonQuery(strSql, CommandType.Text)
                blnResult = True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi cập nhật hủy chứng từ")
                blnResult = False
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình cập nhật chứng từ hủy: " & ex.ToString, EventLogEntryType.Error)

            Finally
                If Not cnCT Is Nothing Then
                    cnCT.Dispose()
                End If
            End Try
            Return blnResult
        End Function
    End Class
End Namespace