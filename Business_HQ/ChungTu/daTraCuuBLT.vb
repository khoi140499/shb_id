﻿Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon
Namespace ChungTu
    Public Class daTraCuuBLT
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function SelectBL(ByVal strWhere As String) As DataSet
            Dim connBL As DataAccess
            Dim strSql As String
            Dim dsResult As DataSet
            Try
                connBL = New DataAccess
                strSql = "Select bl.KyHieu_BL,bl.So_BL,bl.So_BT,bl.Trang_thai,bl.Ma_NV,bl.Lan_in,bl.So_CT,bl.ttien, bl.ngay_kb " & _
                         "FROM tcs_ctbl BL " & _
                         strWhere & _
                         "Order by bl.so_BT desc"
                dsResult = connBL.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thôn tin chứng từ bảo lãnh")
                'LogDebug.WriteLog("Lỗi trong quá trình tra cứu dữ liệu biên lai: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try
            Return dsResult
        End Function

        Public Function KhoiPhucBL(ByVal strKHBL As String, ByVal strSoBL As String) As Boolean
            Dim connBL As DataAccess
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                connBL = New DataAccess
                strSql = "Update tcs_ctbl Set Trang_Thai='00' " & _
                         "Where (kyhieu_bl = '" & strKHBL & "') And (so_bl = '" & strSoBL & "')"
                connBL.ExecuteNonQuery(strSql, CommandType.Text)
                blnResult = True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khôi phục chứng từ bảo lãnh")
                'LogDebug.WriteLog("Lỗi trong quá trình khôi phục biên lai: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try
            Return blnResult
        End Function
    End Class
End Namespace