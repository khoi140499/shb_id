﻿Public Class infDoiChieuNHHQ_DTL
    Public Sub New()
    End Sub
    Public Property hdr_id() As Long
        Get
            Return m_hdr_id
        End Get
        Set(ByVal value As Long)
            m_hdr_id = value
        End Set
    End Property
    Private m_hdr_id As Long
    Public Property ma_quy() As String
        Get
            Return m_ma_quy
        End Get
        Set(ByVal value As String)
            m_ma_quy = value
        End Set
    End Property
    Private m_ma_quy As String
    Public Property ma_cap() As String
        Get
            Return m_ma_cap
        End Get
        Set(ByVal value As String)
            m_ma_cap = value
        End Set
    End Property
    Private m_ma_cap As String
    Public Property ma_chuong() As String
        Get
            Return m_ma_chuong
        End Get
        Set(ByVal value As String)
            m_ma_chuong = value
        End Set
    End Property
    Private m_ma_chuong As String
    Public Property ma_nkt() As String
        Get
            Return m_ma_nkt
        End Get
        Set(ByVal value As String)
            m_ma_nkt = value
        End Set
    End Property
    Private m_ma_nkt As String
    Public Property ma_nkt_cha() As String
        Get
            Return m_ma_nkt_cha
        End Get
        Set(ByVal value As String)
            m_ma_nkt_cha = value
        End Set
    End Property
    Private m_ma_nkt_cha As String
    Public Property ma_ndkt() As String
        Get
            Return m_ma_ndkt
        End Get
        Set(ByVal value As String)
            m_ma_ndkt = value
        End Set
    End Property
    Private m_ma_ndkt As String
    Public Property ma_ndkt_cha() As String
        Get
            Return m_ma_ndkt_cha
        End Get
        Set(ByVal value As String)
            m_ma_ndkt_cha = value
        End Set
    End Property
    Private m_ma_ndkt_cha As String
    Public Property ma_tlpc() As Integer
        Get
            Return m_ma_tlpc
        End Get
        Set(ByVal value As Integer)
            m_ma_tlpc = value
        End Set
    End Property
    Private m_ma_tlpc As Integer
    Public Property ma_khtk() As String
        Get
            Return m_ma_khtk
        End Get
        Set(ByVal value As String)
            m_ma_khtk = value
        End Set
    End Property
    Private m_ma_khtk As String
    Public Property noi_dung() As String
        Get
            Return m_noi_dung
        End Get
        Set(ByVal value As String)
            m_noi_dung = value
        End Set
    End Property
    Private m_noi_dung As String
    Public Property ma_dp() As String
        Get
            Return m_ma_dp
        End Get
        Set(ByVal value As String)
            m_ma_dp = value
        End Set
    End Property
    Private m_ma_dp As String
    Public Property ky_thue() As String
        Get
            Return m_ky_thue
        End Get
        Set(ByVal value As String)
            m_ky_thue = value
        End Set
    End Property
    Private m_ky_thue As String
    Public Property sotien() As Long
        Get
            Return m_sotien
        End Get
        Set(ByVal value As Long)
            m_sotien = value
        End Set
    End Property
    Private m_sotien As Long
    Public Property sotien_nt() As Long
        Get
            Return m_sotien_nt
        End Get
        Set(ByVal value As Long)
            m_sotien_nt = value
        End Set
    End Property
    Private m_sotien_nt As Long
End Class
