﻿Imports VBOracleLib
Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common
Imports Business_HQ.NewChungTu

Namespace ChungTu
    Public Class buChungTu

        Public Shared Function get_tygia(ByVal ma_NT As String, ByRef ma_tygia As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.get_TyGia(ma_NT, ma_tygia)
            Catch ex As Exception
                Throw ex
            End Try
            
        End Function
        Public Shared Function CTU_Check_Exist_NNT(ByVal strMaNNT As String, ByVal strNgay_KS As String) As Integer
            Dim daCT As New daChungTu
            Return daCT.CTU_Check_Exist_NNT(strMaNNT, strNgay_KS)
        End Function
        Public Shared Function CTU_Check_Exist_NNT_HQ(ByVal strMaNNT As String, ByVal strSoTKhai As String) As Integer
            Dim daCT As New daChungTu
            Return daCT.CTU_Check_Exist_NNT_HQ(strMaNNT, strSoTKhai)
        End Function

        Public Shared Function Get_DSTaiKhoan(ByVal strMaDBHC As String) As DataSet
            Dim objCT As New daChungTu
            Return objCT.Get_DSTaiKhoan(strMaDBHC)
        End Function

        Public Shared Function InsertBaoLanh(ByVal hdr As NewChungTu.infChungTuHDR, ByVal dtls As NewChungTu.infChungTuDTL())
            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                daCT.InsertBaoLanh(hdr, dtls)
            Catch ex As Exception
                ' Lỗi trong quá trình insert chứng từ
                MsgBox("Lỗi trong quá trình thêm mới chứng từ CT.", MsgBoxStyle.Critical, "Chú ý")
                blnResult = False
            End Try
            Return blnResult
        End Function
        Public Shared Function CTU_checkExist_bySoCT(ByVal strCtu As String) As Boolean
            Dim daCT As New daChungTu
            Return daCT.CTU_checkExist_bySoCT(strCtu)
        End Function
        Public Shared Function CTU_checkExist_SoBL(ByVal strSoBL As String) As Boolean
            Dim daCT As New daChungTu
            Return daCT.CTU_checkExist_SoBL(strSoBL)
        End Function
        Public Shared Function CTU_checkExist_SoTK(ByVal strSoTK As String) As Boolean
            Dim daCT As New daChungTu
            Return daCT.CTU_checkExist_SoTK(strSoTK)
        End Function
        Public Shared Function UpdateBaoLanh(ByVal hdr As NewChungTu.infChungTuHDR, ByVal dtls As NewChungTu.infChungTuDTL()) As Boolean
            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                daCT.UpdateBaoLanh(hdr, dtls)
            Catch ex As Exception
                blnResult = False
                ' Lỗi trong quá trình insert chứng từ
                MsgBox("Lỗi trong quá trình cập nhật chứng từ CT.", MsgBoxStyle.Critical, "Chú ý")
            End Try
            Return blnResult
        End Function

        Public Shared Function UpdateTrangThaiBaoLanh(ByVal key As KeyCTu, ByVal trangThai As String, ByVal lyDoHuy As String) As Boolean
            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                daCT.UpdateTrangThaiBaoLanh(key, trangThai, lyDoHuy)
            Catch ex As Exception
                blnResult = False
                ' Lỗi trong quá trình cập nhật trạng thái bảo lãnh
                MsgBox("Lỗi trong quá trình cập nhật trạng thái bảo lãnh.", MsgBoxStyle.Critical, "Chú ý")
            End Try
            Return blnResult
        End Function

        Public Shared Function UpdateTrangThaiBaoLanh(ByVal key As KeyCTu, ByVal trangThai As String, ByVal lyDoHuy As String, ByVal lyDoCTra As String) As Boolean
            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                daCT.UpdateTrangThaiBaoLanh(key, trangThai, lyDoHuy, lyDoCTra)
            Catch ex As Exception
                blnResult = False
                ' Lỗi trong quá trình cập nhật trạng thái bảo lãnh
                MsgBox("Lỗi trong quá trình cập nhật trạng thái bảo lãnh.", MsgBoxStyle.Critical, "Chú ý")
            End Try
            Return blnResult
        End Function
        Public Shared Function UpdateTrangThaiThanhToan(ByVal strSoCT As String, ByVal strTT As String, ByVal strSoBT As String, ByVal strSHKB As String) As Boolean
            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                daCT.UpdateTrangThaiThanhToan(strSoCT, strTT, strSoBT, strSHKB)
            Catch ex As Exception
                blnResult = False
            End Try
            Return blnResult
        End Function

        Public Shared Function InsertDoiChieuNHHQ(ByVal hdr As infDoiChieuNHHQ_HDR, ByVal dtls() As infDoiChieuNHHQ_DTL, Optional ByVal strTT_CT As String = "", Optional ByVal DescErr As String = "") As Boolean
            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                daCT.InsertDoiChieuNHHQ(hdr, dtls, strTT_CT, DescErr)
            Catch ex As Exception
                blnResult = False
                ' Lỗi trong quá trình đối chiếu NHHQ
                'MsgBox("Lỗi trong quá trình cập nhật đối chiếu NHHQ.", MsgBoxStyle.Critical, "Chú ý")
            End Try
            Return blnResult
        End Function
        Public Shared Function InsertMSGLoi(ByVal hdr As infDoiChieuNHHQ_HDR, ByVal dtls() As infDoiChieuNHHQ_DTL, Optional ByVal strTT_CT As String = "", Optional ByVal DescErr As String = "") As Boolean
            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                daCT.InsertMSGLoi(hdr, dtls, strTT_CT, DescErr)
            Catch ex As Exception
                blnResult = False
                ' Lỗi trong quá trình đối chiếu NHHQ
                'MsgBox("Lỗi trong quá trình cập nhật đối chiếu NHHQ.", MsgBoxStyle.Critical, "Chú ý")
            End Try
            Return blnResult
        End Function
        Public Shared Function Insert(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), ByVal strLanIn As String, _
            Optional ByVal Ngay_KB As String = "", Optional ByVal Ma_NV As String = "", Optional ByVal So_BT As String = "", Optional ByVal CTU_GNT_BL As String = "00") As Boolean
            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                objCTuHDR.Lan_In = strLanIn
                If Ngay_KB = "" Then
                    daCT.Insert(objCTuHDR, objCTuDTL)
                Else
                    'Trường hợp có ngày KB tức là Lập GNT Từ BL Thu
                    daCT.Insert(objCTuHDR, objCTuDTL, Ngay_KB, Ma_NV, So_BT, "01")
                End If
                ' Cập nhật số bút toán
                gintSoBTCT += 1
                'Tăng Số CT lên
                'gstrSoCT = GetSoCT_Next()
                blnResult = True
            Catch ex As Exception
                blnResult = False
                ' Lỗi trong quá trình insert chứng từ
                MsgBox("Lỗi trong quá trình thêm mới chứng từ CT.", MsgBoxStyle.Critical, "Chú ý")
            End Try
            Return blnResult
        End Function

        Public Shared Function NextSoCT(ByVal strSoCT As String) As String
            Try
                If (strSoCT.Length > 7) Then Return ""

                If (Not IsNumeric(strSoCT)) Then Return ""

                Dim intSoCT As Long = Convert.ToInt32(strSoCT)
                intSoCT += 1
                Return intSoCT.ToString().PadLeft(7, "0")
            Catch ex As Exception
                Throw ex
            End Try
        End Function


#Region "Sinh số chứng từ"
        Public Shared Function MaxSoCT(ByVal strKHCT As String, ByVal strNgayCT As String) As String
            Dim db As DataAccess
            Dim strSQL As String
            Try
                db = New DataAccess

                strSQL = "select Max(so_ct) as MaxSoCT from tcs_ctu_hdr " & _
                        "where (kyhieu_ct = '" & strKHCT & "') " & _
                        "and (ngay_ct = " & strNgayCT & ") " & _
                        "and (length(so_ct) <= 7)"

                Dim ds As DataSet = db.ExecuteReturnDataSet(strSQL, CommandType.Text)

                If (IsEmptyDataSet(ds)) Then
                    Return MaxSoCT(strKHCT)
                End If


                Dim strMaxSoCT As String = GetString(ds.Tables(0).Rows(0)(0)).Trim()

                If (strMaxSoCT = "") Then Return MaxSoCT(strKHCT)

                Dim intSoCT As Integer = Convert.ToInt32(strMaxSoCT)

                intSoCT += 1

                Dim strSoCT As String = intSoCT.ToString.PadLeft(7, "0")

                If (CTU_Exist(strKHCT, strSoCT)) Then
                    strSoCT = MaxSoCT(strKHCT)
                End If

                Return strSoCT
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(db)) Then db.Dispose()
            End Try

        End Function

        Private Shared Function MaxSoCT(ByVal strKHCT As String) As String
            Dim db As DataAccess
            Dim strSQL As String
            Try
                db = New DataAccess

                strSQL = "select Max(so_ct) as MaxSoCT from tcs_ctu_hdr where (kyhieu_ct = '" & strKHCT & "') and (length(so_ct) <= 7)"

                Dim ds As DataSet = db.ExecuteReturnDataSet(strSQL, CommandType.Text)

                If (IsEmptyDataSet(ds)) Then Return "1".PadLeft(7, "0")

                Dim strMaxSoCT As String = GetString(ds.Tables(0).Rows(0)(0)).Trim()

                If (strMaxSoCT = "") Then strMaxSoCT = "0"

                Dim intSoCT As Integer = Convert.ToInt32(strMaxSoCT)

                intSoCT += 1

                Return intSoCT.ToString.PadLeft(7, "0")
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(db)) Then db.Dispose()
            End Try

        End Function
#End Region

        Public Shared Function Update(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), Optional ByVal blnXoaThucThu As Boolean = True) As Boolean
            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                'Cập nhật thông tin chứng từ
                daCT.Update(objCTuHDR, objCTuDTL, blnXoaThucThu)
                blnResult = True
            Catch ex As Exception
                blnResult = False
            End Try
            Return blnResult
        End Function

        Public Shared Function TCS_LoaiThue() As Boolean
            Dim daCT As New daChungTu
            Return daCT.TCS_LoaiThue
        End Function

        Public Shared Function bytTCS_GNT_Exist(ByVal strSoCT As String, ByVal strKHCT As String) As Byte
            Dim daCT As New daChungTu
            strSoCT = RemoveApostrophe(strSoCT)
            strKHCT = RemoveApostrophe(strKHCT)
            Return daCT.bytTCS_GNT_Exist(strSoCT, strKHCT)
        End Function

        Public Shared Function blnTCS_Huy_CT(ByVal strSHKB As String, ByVal iNgay_KB As Integer, _
                                      ByVal iMa_NV As Integer, ByVal iSo_BT As Integer, _
                                      ByVal strMa_DThu As String) As Boolean
            Dim daCT As New daChungTu
            Return daCT.blnTCS_Huy_CT(strSHKB, iNgay_KB, iMa_NV, iSo_BT, strMa_DThu)
        End Function

        Public Shared Function SelectCT_HDR(ByVal strSHKB As String, ByVal iNgay_KB As Integer, _
                                     ByVal iMa_NV As Integer, ByVal iSo_BT As Integer, _
                                     ByVal strMa_DThu As String) As DataSet
            Dim daCT As New daChungTu
            Return daCT.SelectCT_HDR(strSHKB, iNgay_KB, iMa_NV, iSo_BT, strMa_DThu)
        End Function
        Public Shared Function Select_Ctu_Hdr(ByVal soCtu As String) As DataSet
            Dim daCT As New daChungTu
            Return daCT.Select_Ctu_Hdr(soCtu)
        End Function
        Public Shared Function Select_key_hdr(ByVal soCtu As String, ByVal kenhCtu As String) As KeyCTu
            Dim daCt As New daChungTu
            Return daCt.Select_key_Hdr(soCtu, kenhCtu)
        End Function
        Public Shared Function SelectCT_HDR_All() As DataSet
            Dim daCT As New daChungTu
            Return daCT.SelectCT_HDR_All()
        End Function

        Public Shared Function SelectCT_DTL(ByVal strSHKB As String, ByVal iNgay_KB As Integer, _
                                     ByVal iMa_NV As Integer, ByVal iSo_BT As Integer, _
                                     ByVal strMa_DThu As String) As DataSet
            Dim daCT As New daChungTu
            Return daCT.SelectCT_DTL(strSHKB, iNgay_KB, iMa_NV, iSo_BT, strMa_DThu)
        End Function

        Public Shared Function blnTCS_Update_CT(ByVal strSHKB As String, ByVal iNgay_KB As Integer, _
                                          ByVal iMa_NV As Integer, ByVal iSo_BT As Integer, _
                                          ByVal strMa_DThu As String, ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL()) As Boolean
            Dim daCT As New daChungTu
            Return daCT.blnTCS_Update_CT(strSHKB, iNgay_KB, iMa_NV, iSo_BT, strMa_DThu, objCTuHDR, objCTuDTL)
        End Function

        Public Shared Function CTU_Exist(ByVal strKHCT As String, ByVal strSoCT As String) As Boolean
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Exist(strKHCT, strSoCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_TraCuu(ByVal strWhere As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_TraCuu(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_GetListKiemSoat_Set(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, _
            Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "") As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetListKiemSoat_Set(strNgayCT, strTrangThai, strMaDiemThu, strMaNV, strMaKS, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_GetListKiemSoat(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, ByVal strMaLThue As String, _
           Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "") As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetListKiemSoat(strNgayCT, strTrangThai, strMaDiemThu, strMaLThue, strMaNV, strMaKS, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_GetListKiemSoat_frm_HOME(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, ByVal strMaLThue As String, _
           Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "") As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetListKiemSoat_frm_HOME(strNgayCT, strTrangThai, strMaDiemThu, strMaLThue, strMaNV, strMaKS, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_GetCashNote(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetCashNote(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_GetListCT_Set(ByVal strNgayCT As String, ByVal strMaNV As String, ByVal strMaDiemThu As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetListCT_Set(strNgayCT, strMaNV, strMaDiemThu)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_GetListCT(ByVal strNgayCT As String, ByVal strMaNV As String, ByVal strMaDiemThu As String, ByVal strMaLThue As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetListCT(strNgayCT, strMaNV, strMaDiemThu, strMaLThue)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_Header_Set(ByVal key As KeyCTu) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Header_Set(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_ChiTiet_Set(ByVal key As KeyCTu) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_ChiTiet_Set(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function BaoLanh_Header_Set(ByVal key As KeyCTu) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.BaoLanh_Header_Set(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function BaoLanh_ChiTiet_Set(ByVal key As KeyCTu) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.BaoLanh_ChiTiet_Set(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_CalSoDaThu(ByVal strMa_NNT As String, ByVal strMaQuy As String, ByVal strMaChuong As String, _
                                       ByVal strMaKhoan As String, ByVal strMaMuc As String, ByVal strKyThue As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_CalSoDaThu(strMa_NNT, strMaQuy, strMaChuong, _
                                        strMaKhoan, strMaMuc, strKyThue)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_CalSoDaThu_Ngay_KS(ByVal strMa_NNT As String, ByVal strMaChuong As String, _
                                        ByVal strMaMuc As String, ByVal strKyThue As String, ByVal strNgay_KS As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_CalSoDaThu_NgayKS(strMa_NNT, strMaChuong, strMaMuc, strKyThue, strNgay_KS)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_CalSoDaThu_ToKhai(ByVal strMa_NNT As String, ByVal strMaChuong As String, _
                                      ByVal strMaMuc As String, ByVal strSoTK As String, Optional ByVal strNgay_KS As String = "") As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_CalSoDaThu_ToKhai(strMa_NNT, strMaChuong, strMaMuc, strSoTK, strNgay_KS)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_Header(ByVal key As KeyCTu) As infChungTuHDR
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Header(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_ChiTiet(ByVal key As KeyCTu) As infChungTuDTL()
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_ChiTiet(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_Huy(ByVal key As KeyCTu, ByVal blnKTKB As Boolean, ByVal strLyDoHuy As String) As String
            Try
                Dim strReturn As String = "OK"
                Dim daCT As New daChungTu
                strReturn = daCT.CTU_Huy(key, blnKTKB, strLyDoHuy)
                'daCT.STT_Delete(key.Kyhieu_CT, key.So_CT)
                Return strReturn
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Huy_Da_KS(ByVal key As KeyCTu, ByVal blnKTKB As Boolean, ByVal strLyDoHuy As String) As String
            Try
                Dim strReturn As String = "OK"
                Dim daCT As New daChungTu
                strReturn = daCT.CTU_Huy_Da_KS(key, blnKTKB, strLyDoHuy)
                'daCT.STT_Delete(key.Kyhieu_CT, key.So_CT)
                Return strReturn
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_ChuyenKS(ByVal key As KeyCTu, ByVal blnKTKB As Boolean) As String
            Try
                Dim strReturn As String = "OK"
                Dim daCT As New daChungTu
                strReturn = daCT.CTU_ChuyenKS(key, blnKTKB)
                Return strReturn
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_Huy_BL(ByVal key As KeyCTu, ByVal blnKTKB As Boolean, Optional ByVal blnTHOP As Boolean = False) As String
            Try
                Dim strReturn As String = "OK"
                Dim daCT As New daChungTu
                strReturn = daCT.CTU_Huy_BL(key, blnKTKB)
                If (Not blnTHOP) Then daCT.STT_Delete(key.Kyhieu_CT, key.So_CT)
                Return strReturn
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_Huy(ByVal strKHCT As String, ByVal strSoCT As String) As String
            Try
                Dim strReturn As String = "OK"
                Dim daCT As New daChungTu
                strReturn = daCT.CTU_Huy(strKHCT, strSoCT)
                daCT.STT_Delete(strKHCT, strSoCT)
                Return strReturn
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function STT_Delete(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String) As Boolean
            Try
                Dim daCT As New daChungTu
                daCT.STT_Delete(strKYHIEU_CT, strSO_CT)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
        'Public Shared Function Ctu_SetTrangThai_HDR(ByVal soCtu As String, ByVal trangThai As String) As Boolean
        '    Dim dsCT As New daChungTu
        '    dsCT.CTu_SetTrangThai_HDR(soCtu, trangThai)
        'End Function

        Public Shared Sub CTU_SetTrangThai(ByVal key As KeyCTu, ByVal strTT As String, Optional ByVal strTKHT As String = "", _
                        Optional ByVal strRefNo As String = "", Optional ByVal strSeqNo As String = "", _
                        Optional ByVal strMaKS As String = "", Optional ByVal strMaHuyKS As String = "", _
                        Optional ByVal strRMNo As String = "", Optional ByVal strTT_CThue As String = "0", _
                        Optional ByVal strLyDoHuy As String = "", Optional ByVal strNgayKSVCT As Boolean = False)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_SetTrangThai(key, strTT, strTKHT, strRefNo, strSeqNo, strMaKS, strMaHuyKS, strRMNo, strTT_CThue, strLyDoHuy, strNgayKSVCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub CTU_Update_TTSP(ByVal strSO_CT As String, ByVal strTTSP As String)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_Update_TTSP(strSO_CT, strTTSP)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Shared Sub CTU_KhoiPhuc(ByVal key As KeyCTu, Optional ByVal blnKiemSoat As Boolean = False)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_KhoiPhuc(key, blnKiemSoat)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CTU_KiemSoat(ByVal strSHKB As String, ByVal lngNgay_KB As Long, ByVal lngMa_NV As Long, _
                                       ByVal lngSo_BT_TCS As Long, ByVal strMa_DThu As String, ByVal lngMa_KS As Long, _
                                       ByRef strSo_BT_KTKB As String, ByRef strError As String)
            Try
                Dim daCT As New daChungTu
                daCT.ChuyenSangKTKB(strSHKB, lngNgay_KB, lngMa_NV, lngSo_BT_TCS, strMa_DThu, lngMa_KS, strSo_BT_KTKB, strError)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CTU_DTL_Update(ByVal dtl As infChungTuDTL)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_DTL_Update(dtl)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CTU_Update_LanIn(ByVal key As KeyCTu)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_Update_LanIn(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Public Shared Function CTU_get_TThai(ByVal soCTu As String) As String
        '    Try
        '        Dim daCT As New daChungTu
        '        Return daCT.CTU_get_TThai(soCTu)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        Public Shared Function CTU_Get_LanIn(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_LanIn(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_Get_TrangThai(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_TrangThai(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Get_TrangThai_BL(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_TrangThai_BL(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function CTU_Get_PTTT(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_PTTT(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Shared Function CTU_Get_TrangThai(ByVal key As KeyCTu) As String
        '    Try
        '        Dim daCT As New daChungTu
        '        Return daCT.CTU_Get_TrangThai(key)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Shared Function CTU_PrintAllow(ByVal key As KeyCTu) As Boolean
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_PrintAllow(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub CTU_Update(ByVal hdr As infChungTuHDR, ByVal dtl As infChungTuDTL())
            Try
                Dim daCT As New daChungTu
                daCT.CTU_Update(hdr, dtl)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CTU_Update_Search(ByVal hdr As infChungTuHDR, ByVal dtl As infChungTuDTL())
            Try
                Dim daCT As New daChungTu
                daCT.CTU_Update_Search(hdr, dtl)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function CTU_PhucHoi(ByVal strWhere As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_PhucHoi(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_PhucHoi_DTL(ByVal strKHCT As String, ByVal strSoCT As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_PhucHoi_DTL(strKHCT, strSoCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_GetGhichu(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetGhichu(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Get_Valid_LanKS(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_Valid_LanKS(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Get_LanKS(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_LanKS(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub CTU_SetLanKS(ByVal key As KeyCTu, ByVal strClickKS As String)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_SetLanKS(key, strClickKS)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub CTU_SetGhichu(ByVal key As KeyCTu, ByVal strGhichu As String)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_SetGhichu(key, strGhichu)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function CTU_GetSoBT_KTKB(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetSoBT_KTKB(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_IN_LASER(ByVal key As KeyCTu) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_IN_LASER(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Is_DaNhap_STThu(ByVal key As KeyCTu) As Boolean
            Try
                Dim daCT As New daChungTu
                Return daCT.Is_DaNhap_STThu(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Kienvt
        Public Shared Function Insert_New(ByVal objCTuHDR As NewChungTu.infChungTuHDR, _
                                          ByVal objCTuDTL As NewChungTu.infChungTuDTL(), _
                                          ByVal strLanIn As String, Optional ByVal Ngay_KB As String = "", _
                                          Optional ByVal Ma_NV As String = "", Optional ByVal So_BT As String = "", _
                                          Optional ByVal CTU_GNT_BL As String = "00") As Boolean

            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                objCTuHDR.Lan_In = strLanIn
                If Ngay_KB = "" Then
                    daCT.Insert_New(objCTuHDR, objCTuDTL)
                Else
                    'Trường hợp có ngày KB tức là Lập GNT Từ BL Thu
                    daCT.Insert_New(objCTuHDR, objCTuDTL, Ngay_KB, Ma_NV, So_BT, "01")
                End If
                ' Cập nhật số bút toán
                gintSoBTCT += 1
                'Tăng Số CT lên
                ' gstrSoCT = GetSoCT_Next()
                blnResult = True
            Catch ex As Exception
                blnResult = False
                Throw ex
                ' Lỗi trong quá trình insert chứng từ
                'MsgBox("Lỗi trong quá trình thêm mới chứng từ CT.", MsgBoxStyle.Critical, "Chú ý")
            End Try
            Return blnResult
        End Function
        Public Shared Function Insert_NewFromWS(ByVal objCTuHDR As NewChungTu.infChungTuHDR, _
                                         ByVal objCTuDTL As NewChungTu.infChungTuDTL(), _
                                         ByVal strLanIn As String, Optional ByVal Ngay_KB As String = "", _
                                         Optional ByVal Ma_NV As String = "", Optional ByVal So_BT As String = "", _
                                         Optional ByVal CTU_GNT_BL As String = "00") As Boolean

            Dim blnResult As Boolean = True
            Dim daCT As New daChungTu
            Try
                objCTuHDR.Lan_In = strLanIn
                If Ngay_KB = "" Then
                    daCT.Insert_NewFromWS(objCTuHDR, objCTuDTL)
                Else
                    'Trường hợp có ngày KB tức là Lập GNT Từ BL Thu
                    daCT.Insert_NewFromWS(objCTuHDR, objCTuDTL, Ngay_KB, Ma_NV, So_BT, "01")
                End If
                ' Cập nhật số bút toán
                gintSoBTCT += 1
                'Tăng Số CT lên
                ' gstrSoCT = GetSoCT_Next()
                blnResult = True
            Catch ex As Exception
                blnResult = False
                Throw ex
                ' Lỗi trong quá trình insert chứng từ
                'MsgBox("Lỗi trong quá trình thêm mới chứng từ CT.", MsgBoxStyle.Critical, "Chú ý")
            End Try
            Return blnResult
        End Function
        Public Shared Function Get_DetailsCTU(ByVal so_Ctu As String, Optional ByVal pv_strKenhCT As String = "") As DataSet
            Dim daCT As New daChungTu
            Return daCT.Get_DetailsCTU(so_Ctu, pv_strKenhCT)
        End Function

        Public Shared Function CTU_Details_Set(ByVal pv_strSHKB As String, _
                                              ByVal pv_strSoBT As String, ByVal pv_strMaNV As Integer _
                                              ) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Details_Set(pv_strSHKB, pv_strSoBT, pv_strMaNV)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Insert_Log_Message(ByVal xml_msg_signed As String, ByVal xml_msg As String, ByVal kenh_ct As String)
            Dim daCT As New daChungTu
            daCT.Insert_Log_Message(xml_msg_signed, xml_msg, kenh_ct)
        End Function
        Public Shared Function CTU_TN_CT_NHHQ(ByVal strSo_CT As String, ByVal strTypeMSG As String) As String
            Dim strResault As String = ""
            Dim daCT As New daChungTu
            Try
                strResault = daCT.CTU_TN_CT_NHHQ(strSo_CT, strTypeMSG)
                Return strResault
            Catch ex As Exception
                strResault = ""
                ' Lỗi trong quá trình đối chiếu NHHQ
                'MsgBox("Lỗi trong quá trình cập nhật đối chiếu NHHQ.", MsgBoxStyle.Critical, "Chú ý")
                Throw ex
            End Try
            Return strResault
        End Function
        Public Shared Function CTU_ChiTiet_Set_CT_TK(ByVal so_ct As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_ChiTiet_Set_CT_TK(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
   
End Namespace