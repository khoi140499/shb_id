﻿Imports VBOracleLib
Imports System.Data.OleDb
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.Common
Imports System.Data
Imports Business_HQ.NewChungTu
Imports GIPBankService

Namespace ChungTu
    Public Class daChungTu
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "Nguyễn Hữu Hoan"

        Public Sub New()

        End Sub
        Public Sub Insert_CTBL(ByVal hdr As infChungTuHDR, ByVal dtls As infChungTuDTL(), Optional ByVal lstKeyBLT As ArrayList = Nothing)
            '----------------------------------------------------------------------
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
            ' Đầu vào: đối tượng HDR và mảng DTL
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 01/11/2007
            '----------------------------------------------------------------------
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim dtl As infChungTuDTL
            Dim strSql As String
            Try
                'Lấy tham số ẩn hiện Pictures
                Dim blnLogCTU As Boolean = False
                ' Dim strLogCTU As String = Trim(Setting.GetSettingEx(Global.SubKey, Global.CompanyName, Global.AppName, Global.Section, "Log_CTU"))
                ' If (strLogCTU = "1") Then blnLogCTU = True

                conn = New DataAccess
                transCT = conn.BeginTransaction
                strSql = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV," & _
                         "So_BT,Ma_DThu,So_BThu," & _
                         "KyHieu_CT,So_CT,So_CT_NH," & _
                         "Ma_NNTien,Ten_NNTien,DC_NNTien," & _
                         "Ma_NNThue,Ten_NNThue,DC_NNThue," & _
                         "Ly_Do,Ma_KS,Ma_TQ,So_QD," & _
                         "Ngay_QD,CQ_QD,Ngay_CT,Ngay_HT," & _
                         "Ma_CQThu,XA_ID,Ma_Tinh," & _
                         "Ma_Huyen,Ma_Xa,TK_No,TK_Co," & _
                         "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
                         "Ma_NT,Ty_Gia,TG_ID, Ma_LThue," & _
                         "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH," & _
                         "MA_NH_A,MA_NH_B,TTien,TT_TThu," & _
                         "Lan_In,Trang_Thai, " & _
                         "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT, So_BK, Ngay_BK) " & _
                         "Values('" & hdr.SHKB & "'," & hdr.Ngay_KB & "," & hdr.Ma_NV & "," & _
                         hdr.So_BT & ",'" & hdr.Ma_DThu & "'," & hdr.So_BThu & ",'" & _
                         hdr.KyHieu_CT & "','" & hdr.So_CT & "','" & hdr.So_CT_NH & "','" & _
                         hdr.Ma_NNTien & "','" & hdr.Ten_NNTien & "','" & hdr.DC_NNTien & "','" & _
                         hdr.Ma_NNThue & "','" & hdr.Ten_NNThue & "','" & hdr.DC_NNThue & "','" & _
                         hdr.Ly_Do & "'," & hdr.Ma_KS & "," & hdr.Ma_TQ & ",'" & hdr.So_QD & "'," & _
                         hdr.Ngay_QD & ",'" & hdr.CQ_QD & "'," & hdr.Ngay_CT & "," & _
                         hdr.Ngay_HT & ",'" & hdr.Ma_CQThu & "'," & _
                         hdr.XA_ID & ",'" & hdr.Ma_Tinh & "','" & hdr.Ma_Huyen & "','" & _
                         hdr.Ma_Xa & "','" & hdr.TK_No & "','" & hdr.TK_Co & "','" & _
                         hdr.So_TK & "'," & hdr.Ngay_TK & ",'" & hdr.LH_XNK & "','" & _
                         hdr.DVSDNS & "','" & hdr.Ten_DVSDNS & "','" & hdr.Ma_NT & "'," & _
                         hdr.Ty_Gia & "," & hdr.TG_ID & ",'" & hdr.Ma_LThue & "','" & _
                         hdr.So_Khung & "','" & hdr.So_May & "','" & hdr.TK_KH_NH & "'," & _
                         hdr.NGAY_KH_NH & ",'" & hdr.MA_NH_A & "','" & hdr.MA_NH_B & "'," & _
                         hdr.TTien & "," & hdr.TT_TThu & "," & hdr.Lan_In & ",'" & _
                         hdr.Trang_Thai & "','" & hdr.TK_KH_Nhan & "','" & hdr.Ten_KH_Nhan & "','" & _
                         hdr.Diachi_KH_Nhan & "'," & hdr.TTien_NT & ", '" & hdr.So_BK & "', " & hdr.Ngay_BK & ")"
                ' Insert vào bảng TCS_CT_HDR

                'If (blnLogCTU) Then LogDebug.Writelog(strSql)

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
                For Each dtl In dtls
                    strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                             "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                             "DT_ID,MaDT,Ky_Thue,SoTien,SoTien_NT, MaQuy, Ma_DP) " & _
                             "Values (" & dtl.ID & ",'" & dtl.SHKB & "'," & dtl.Ngay_KB & "," & dtl.Ma_NV & "," & _
                             dtl.So_BT & ",'" & dtl.Ma_DThu & "'," & dtl.CCH_ID & ",'" & _
                             dtl.Ma_Cap & "','" & dtl.Ma_Chuong & "'," & dtl.LKH_ID & ",'" & _
                             dtl.Ma_Loai & "','" & dtl.Ma_Khoan & "'," & _
                             dtl.MTM_ID & ",'" & dtl.Ma_Muc & "','" & dtl.Ma_TMuc & "','" & _
                             dtl.Noi_Dung & "'," & _
                             dtl.DT_ID & ",'" & dtl.Ma_TLDT & "','" & dtl.Ky_Thue & "'," & _
                             dtl.SoTien & "," & dtl.SoTien_NT & ",'" & dtl.MaQuy & "', '" & dtl.Ma_DP & "')"

                    '   If (blnLogCTU) Then LogDebug.Writelog(strSql)

                    ' Insert Vào bảng TCS_CTU_DTL    
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                Next

                If Not IsNothing(lstKeyBLT) Then
                    Dim strKey() As String
                    For i As Integer = 0 To lstKeyBLT.Count - 1
                        strKey = GetString(lstKeyBLT(i)).Split(";")

                        strSql = "Update TCS_CTBL set Trang_Thai='02', Kyhieu_CT='" & hdr.KyHieu_CT & _
                                "', so_CT='" & hdr.So_CT & "'  where " & _
                                "( Ngay_KB=" & strKey(0) & " and Ma_NV=" & strKey(1) & " and So_BT=" & strKey(2) & ") "
                        '  If (blnLogCTU) Then LogDebug.Writelog(strSql)

                        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                    Next
                End If
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub

        Public Sub Insert(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), Optional ByVal Ngay_KB As String = "", Optional ByVal Ma_NV As String = "", Optional ByVal So_BT As String = "", Optional ByVal CTU_GNT_BL As String = "00")
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
            ' Đầu vào: đối tượng HDR và mảng DTL
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 01/11/2007

            ' Người sửa: Hoàng Văn Anh
            ' Ngày 23/12/2008
            ' Mục đích: Nâng cấp COA cho Ngân Hàng
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                'Bỏ phần Insert So_CT
                strSql = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV," & _
                         "So_BT,Ma_DThu,So_BThu," & _
                         "KyHieu_CT,So_CT_NH," & _
                         "Ma_NNTien,Ten_NNTien,DC_NNTien," & _
                         "Ma_NNThue,Ten_NNThue,DC_NNThue," & _
                         "Ly_Do,Ma_KS,Ma_TQ,So_QD," & _
                         "Ngay_QD,CQ_QD,Ngay_CT,Ngay_HT," & _
                         "Ma_CQThu,XA_ID,Ma_Tinh," & _
                         "Ma_Huyen,Ma_Xa,TK_No,TK_Co," & _
                         "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
                         "Ma_NT,Ty_Gia,TG_ID, Ma_LThue," & _
                         "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH," & _
                         "MA_NH_A,MA_NH_B,TTien,TT_TThu," & _
                         "Lan_In,Trang_Thai, " & _
                         "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT,So_BK, Ngay_BK,CTU_GNT_BL,So_CT) " & _
                         "Values('" & objCTuHDR.SHKB & "'," & objCTuHDR.Ngay_KB & "," & objCTuHDR.Ma_NV & "," & _
                         objCTuHDR.So_BT & ",'" & objCTuHDR.Ma_DThu & "'," & objCTuHDR.So_BThu & ",'" & _
                         objCTuHDR.KyHieu_CT & "','" & objCTuHDR.So_CT_NH & "','" & _
                         objCTuHDR.Ma_NNTien & "','" & objCTuHDR.Ten_NNTien & "','" & objCTuHDR.DC_NNTien & "','" & _
                         objCTuHDR.Ma_NNThue & "','" & objCTuHDR.Ten_NNThue & "','" & objCTuHDR.DC_NNThue & "','" & _
                         objCTuHDR.Ly_Do & "'," & objCTuHDR.Ma_KS & "," & objCTuHDR.Ma_TQ & ",'" & objCTuHDR.So_QD & "'," & _
                         objCTuHDR.Ngay_QD & ",'" & objCTuHDR.CQ_QD & "'," & objCTuHDR.Ngay_CT & "," & _
                         objCTuHDR.Ngay_HT & ",'" & objCTuHDR.Ma_CQThu & "'," & _
                         objCTuHDR.XA_ID & ",'" & objCTuHDR.Ma_Tinh & "','" & objCTuHDR.Ma_Huyen & "','" & _
                         objCTuHDR.Ma_Xa & "','" & objCTuHDR.TK_No & "','" & objCTuHDR.TK_Co & "','" & _
                         objCTuHDR.So_TK & "'," & objCTuHDR.Ngay_TK & ",'" & objCTuHDR.LH_XNK & "','" & _
                         objCTuHDR.DVSDNS & "','" & objCTuHDR.Ten_DVSDNS & "','" & objCTuHDR.Ma_NT & "'," & _
                         objCTuHDR.Ty_Gia & "," & objCTuHDR.TG_ID & ",'" & objCTuHDR.Ma_LThue & "','" & _
                         objCTuHDR.So_Khung & "','" & objCTuHDR.So_May & "','" & objCTuHDR.TK_KH_NH & "'," & _
                         objCTuHDR.NGAY_KH_NH & ",'" & objCTuHDR.MA_NH_A & "','" & objCTuHDR.MA_NH_B & "'," & _
                         objCTuHDR.TTien & "," & objCTuHDR.TT_TThu & "," & objCTuHDR.Lan_In & ",'" & _
                         objCTuHDR.Trang_Thai & "','" & objCTuHDR.TK_KH_Nhan & "','" & objCTuHDR.Ten_KH_Nhan & "','" & _
                         objCTuHDR.Diachi_KH_Nhan & "'," & objCTuHDR.TTien_NT & ",'" & objCTuHDR.PT_TT & "'," & Globals.EscapeQuote(objCTuHDR.So_BK) & "," & _
                         objCTuHDR.Ngay_BK & "," & Globals.EscapeQuote(CTU_GNT_BL) & "," & Globals.EscapeQuote(objCTuHDR.So_CT) & ")"

                'strSql = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV," & _
                '         "So_BT,Ma_DThu,So_BThu," & _
                '         "KyHieu_CT,So_CT,So_CT_NH," & _
                '         "Ma_NNTien,Ten_NNTien,DC_NNTien," & _
                '         "Ma_NNThue,Ten_NNThue,DC_NNThue," & _
                '         "Ly_Do,Ma_KS,Ma_TQ,So_QD," & _
                '         "Ngay_QD,CQ_QD,Ngay_CT,Ngay_HT," & _
                '         "Ma_CQThu,XA_ID,Ma_Tinh," & _
                '         "Ma_Huyen,Ma_Xa,TK_No,TK_Co," & _
                '         "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
                '         "Ma_NT,Ty_Gia,TG_ID, Ma_LThue," & _
                '         "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH," & _
                '         "MA_NH_A,MA_NH_B,TTien,TT_TThu," & _
                '         "Lan_In,Trang_Thai, " & _
                '         "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT,So_BK, Ngay_BK) " & _
                '         "Values('" & objCTuHDR.SHKB & "'," & objCTuHDR.Ngay_KB & "," & objCTuHDR.Ma_NV & "," & _
                '         objCTuHDR.So_BT & ",'" & objCTuHDR.Ma_DThu & "'," & objCTuHDR.So_BThu & ",'" & _
                '         objCTuHDR.KyHieu_CT & "','" & objCTuHDR.So_CT & "','" & objCTuHDR.So_CT_NH & "','" & _
                '         objCTuHDR.Ma_NNTien & "','" & objCTuHDR.Ten_NNTien & "','" & objCTuHDR.DC_NNTien & "','" & _
                '         objCTuHDR.Ma_NNThue & "','" & objCTuHDR.Ten_NNThue & "','" & objCTuHDR.DC_NNThue & "','" & _
                '         objCTuHDR.Ly_Do & "'," & objCTuHDR.Ma_KS & "," & objCTuHDR.Ma_TQ & ",'" & objCTuHDR.So_QD & "'," & _
                '         objCTuHDR.Ngay_QD & ",'" & objCTuHDR.CQ_QD & "'," & objCTuHDR.Ngay_CT & "," & _
                '         objCTuHDR.Ngay_HT & ",'" & objCTuHDR.Ma_CQThu & "'," & _
                '         objCTuHDR.XA_ID & ",'" & objCTuHDR.Ma_Tinh & "','" & objCTuHDR.Ma_Huyen & "','" & _
                '         objCTuHDR.Ma_Xa & "','" & objCTuHDR.TK_No & "','" & objCTuHDR.TK_Co & "','" & _
                '         objCTuHDR.So_TK & "'," & objCTuHDR.Ngay_TK & ",'" & objCTuHDR.LH_XNK & "','" & _
                '         objCTuHDR.DVSDNS & "','" & objCTuHDR.Ten_DVSDNS & "','" & objCTuHDR.Ma_NT & "'," & _
                '         objCTuHDR.Ty_Gia & "," & objCTuHDR.TG_ID & ",'" & objCTuHDR.Ma_LThue & "','" & _
                '         objCTuHDR.So_Khung & "','" & objCTuHDR.So_May & "','" & objCTuHDR.TK_KH_NH & "'," & _
                '         objCTuHDR.NGAY_KH_NH & ",'" & objCTuHDR.MA_NH_A & "','" & objCTuHDR.MA_NH_B & "'," & _
                '         objCTuHDR.TTien & "," & objCTuHDR.TT_TThu & "," & objCTuHDR.Lan_In & ",'" & _
                '         objCTuHDR.Trang_Thai & "','" & objCTuHDR.TK_KH_Nhan & "','" & objCTuHDR.Ten_KH_Nhan & "','" & _
                '         objCTuHDR.Diachi_KH_Nhan & "'," & objCTuHDR.TTien_NT & ",'" & objCTuHDR.PT_TT & "'," & Globals.EscapeQuote(objCTuHDR.So_BK) & "," & objCTuHDR.Ngay_BK & ")"
                ' Insert vào bảng TCS_CT_HDR
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
                For Each objDTL In objCTuDTL
                    strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                             "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                             "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,ma_dp) " & _
                             "Values (" & objDTL.ID & ",'" & objDTL.SHKB & "'," & objDTL.Ngay_KB & "," & objDTL.Ma_NV & "," & _
                             objDTL.So_BT & ",'" & objDTL.Ma_DThu & "'," & objDTL.CCH_ID & ",'" & _
                             objDTL.Ma_Cap & "','" & objDTL.Ma_Chuong & "'," & objDTL.LKH_ID & ",'" & _
                             objDTL.Ma_Loai & "','" & objDTL.Ma_Khoan & "'," & _
                             objDTL.MTM_ID & ",'" & objDTL.Ma_Muc & "','" & objDTL.Ma_TMuc & "','" & _
                             objDTL.Noi_Dung & "'," & _
                             objDTL.DT_ID & ",'" & objDTL.Ma_TLDT & "','" & objDTL.Ky_Thue & "'," & _
                             objDTL.SoTien & "," & objDTL.SoTien_NT & "," & Globals.EscapeQuote(objDTL.MaQuy) & "," & Globals.EscapeQuote(objDTL.Ma_DP) & ")"
                    ' Insert Vào bảng TCS_CTU_DTL    
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                Next
                If Not Ngay_KB = "" Then
                    Dim NgayKB() As String = Ngay_KB.Split(";")
                    Dim MaNV() As String = Ma_NV.Split(";")
                    Dim SoBT() As String = So_BT.Split(";")

                    strSql = "Update TCS_CTBL set Trang_Thai='02', Kyhieu_CT='" & objCTuHDR.KyHieu_CT & "'  WHERE "
                    For i As Byte = 0 To NgayKB.Length - 1
                        strSql &= "( Ngay_KB=" & NgayKB(i) & " and Ma_NV=" & MaNV(i) & " and So_BT=" & SoBT(i) & ") or "
                    Next
                    strSql &= " (1=2) "

                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                End If
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString & vbCrLf & strSql, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
        Public Sub Update(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), Optional ByVal blnXoaThucThu As Boolean = True)
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
            ' Đầu vào: đối tượng HDR và mảng DTL
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 09/11/2007
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction

                'Cập nhật thông tin HDR của chứng từ
                'ICB: khi cập nhật chứng từ thì giữ lại trạng thái của Số thực thu
                'VD: nếu nhập số thực thu rồi thì giữ nguyên
                'bỏ cập nhật so_CT
                strSql = "Update TCS_CTU_HDR SET " & _
                         "kyhieu_ct = '" & objCTuHDR.KyHieu_CT & "', " & _
                         " So_BThu=" & objCTuHDR.So_BThu & ",So_CT_NH='" & objCTuHDR.So_CT_NH & "'," & _
                         "Ma_NNTien='" & objCTuHDR.Ma_NNTien & "',Ten_NNTien='" & objCTuHDR.Ten_NNTien & "'," & _
                         "DC_NNTien='" & objCTuHDR.DC_NNTien & "',Ma_NNThue='" & objCTuHDR.Ma_NNThue & "'," & _
                         "Ten_NNThue='" & objCTuHDR.Ten_NNThue & "',DC_NNThue='" & objCTuHDR.DC_NNThue & "'," & _
                         "Ly_do='" & objCTuHDR.Ly_Do & "',Ma_KS=0,So_QD='" & objCTuHDR.So_QD & "'," & _
                         "Ngay_QD=" & objCTuHDR.Ngay_QD & ",CQ_QD='" & objCTuHDR.CQ_QD & "',Ngay_CT=" & objCTuHDR.Ngay_CT & "," & _
                         "Ngay_HT=" & objCTuHDR.Ngay_HT & ",Ma_CQThu='" & objCTuHDR.Ma_CQThu & "'," & _
                         "XA_ID=" & objCTuHDR.XA_ID & ",Ma_Tinh='" & objCTuHDR.Ma_Tinh & "',Ma_Huyen='" & objCTuHDR.Ma_Huyen & "'," & _
                         "Ma_Xa='" & objCTuHDR.Ma_Xa & "',TK_Co='" & objCTuHDR.TK_Co & "'," & _
                         "So_TK='" & objCTuHDR.So_TK & "',Ngay_TK=" & objCTuHDR.Ngay_TK & ",LH_XNK='" & objCTuHDR.LH_XNK & "'," & _
                         "DVSDNS='" & objCTuHDR.DVSDNS & "',TEN_DVSDNS = '" & objCTuHDR.Ten_DVSDNS & "'," & _
                         "Ma_NT='" & objCTuHDR.Ma_NT & "',Ty_Gia=" & objCTuHDR.Ty_Gia & "," & _
                         "TG_ID = " & objCTuHDR.TG_ID & ",MA_LTHUE = '" & objCTuHDR.Ma_LThue & "'," & _
                         "So_Khung='" & objCTuHDR.So_Khung & "',So_May='" & objCTuHDR.So_May & "'," & _
                         "TK_KH_NH='" & objCTuHDR.TK_KH_NH & "',NGAY_KH_NH=" & objCTuHDR.NGAY_KH_NH & "," & _
                         "MA_NH_A='" & objCTuHDR.MA_NH_A & "',MA_NH_B='" & objCTuHDR.MA_NH_B & "'," & _
                         "TTien=" & objCTuHDR.TTien & ", Lan_In=0, Trang_Thai='" & objCTuHDR.Trang_Thai & "', " & _
                         "TK_KH_Nhan='" & objCTuHDR.TK_KH_Nhan & "', Ten_KH_Nhan='" & objCTuHDR.Ten_KH_Nhan & "'," & _
                         "Diachi_KH_Nhan='" & objCTuHDR.Diachi_KH_Nhan & "', TTien_NT=" & objCTuHDR.TTien_NT & _
                         " ,TK_NO=" & Globals.EscapeQuote(objCTuHDR.TK_No) & " ,PT_TT=" & Globals.EscapeQuote(objCTuHDR.PT_TT) & _
                         ", So_BK=" & Globals.EscapeQuote(objCTuHDR.So_BK) & ", Ngay_BK=" & objCTuHDR.Ngay_BK & _
                         " Where (SHKB = '" & objCTuHDR.SHKB & "') And (Ngay_KB = " & objCTuHDR.Ngay_KB & _
                         ") And (Ma_NV = " & objCTuHDR.Ma_NV & ") And (So_BT = " & objCTuHDR.So_BT & _
                         ") And (Ma_DThu = '" & objCTuHDR.Ma_DThu & "')"
                'strSql = "Update TCS_CTU_HDR SET " & _
                '         "kyhieu_ct = '" & objCTuHDR.KyHieu_CT & "', " & _
                '         "So_CT= '" & objCTuHDR.So_CT & "', So_BThu=" & objCTuHDR.So_BThu & ",So_CT_NH='" & objCTuHDR.So_CT_NH & "'," & _
                '         "Ma_NNTien='" & objCTuHDR.Ma_NNTien & "',Ten_NNTien='" & objCTuHDR.Ten_NNTien & "'," & _
                '         "DC_NNTien='" & objCTuHDR.DC_NNTien & "',Ma_NNThue='" & objCTuHDR.Ma_NNThue & "'," & _
                '         "Ten_NNThue='" & objCTuHDR.Ten_NNThue & "',DC_NNThue='" & objCTuHDR.DC_NNThue & "'," & _
                '         "Ly_do='" & objCTuHDR.Ly_Do & "',Ma_KS=0,So_QD='" & objCTuHDR.So_QD & "'," & _
                '         "Ngay_QD=" & objCTuHDR.Ngay_QD & ",CQ_QD='" & objCTuHDR.CQ_QD & "',Ngay_CT=" & objCTuHDR.Ngay_CT & "," & _
                '         "Ngay_HT=" & objCTuHDR.Ngay_HT & ",Ma_CQThu='" & objCTuHDR.Ma_CQThu & "'," & _
                '         "XA_ID=" & objCTuHDR.XA_ID & ",Ma_Tinh='" & objCTuHDR.Ma_Tinh & "',Ma_Huyen='" & objCTuHDR.Ma_Huyen & "'," & _
                '         "Ma_Xa='" & objCTuHDR.Ma_Xa & "',TK_Co='" & objCTuHDR.TK_Co & "'," & _
                '         "So_TK='" & objCTuHDR.So_TK & "',Ngay_TK=" & objCTuHDR.Ngay_TK & ",LH_XNK='" & objCTuHDR.LH_XNK & "'," & _
                '         "DVSDNS='" & objCTuHDR.DVSDNS & "',TEN_DVSDNS = '" & objCTuHDR.Ten_DVSDNS & "'," & _
                '         "Ma_NT='" & objCTuHDR.Ma_NT & "',Ty_Gia=" & objCTuHDR.Ty_Gia & "," & _
                '         "TG_ID = " & objCTuHDR.TG_ID & ",MA_LTHUE = '" & objCTuHDR.Ma_LThue & "'," & _
                '         "So_Khung='" & objCTuHDR.So_Khung & "',So_May='" & objCTuHDR.So_May & "'," & _
                '         "TK_KH_NH='" & objCTuHDR.TK_KH_NH & "',NGAY_KH_NH=" & objCTuHDR.NGAY_KH_NH & "," & _
                '         "MA_NH_A='" & objCTuHDR.MA_NH_A & "',MA_NH_B='" & objCTuHDR.MA_NH_B & "'," & _
                '         "TTien=" & objCTuHDR.TTien & ", Lan_In=0, Trang_Thai='" & objCTuHDR.Trang_Thai & "', " & _
                '         "TK_KH_Nhan='" & objCTuHDR.TK_KH_Nhan & "', Ten_KH_Nhan='" & objCTuHDR.Ten_KH_Nhan & "'," & _
                '         "Diachi_KH_Nhan='" & objCTuHDR.Diachi_KH_Nhan & "', TTien_NT=" & objCTuHDR.TTien_NT & _
                '         " ,TK_NO=" & Globals.EscapeQuote(objCTuHDR.TK_No) & " ,PT_TT=" & Globals.EscapeQuote(objCTuHDR.PT_TT) & _
                '         ", So_BK=" & Globals.EscapeQuote(objCTuHDR.So_BK) & ", Ngay_BK=" & objCTuHDR.Ngay_BK & _
                '         " Where (SHKB = '" & objCTuHDR.SHKB & "') And (Ngay_KB = " & objCTuHDR.Ngay_KB & _
                '         ") And (Ma_NV = " & objCTuHDR.Ma_NV & ") And (So_BT = " & objCTuHDR.So_BT & _
                '         ") And (Ma_DThu = '" & objCTuHDR.Ma_DThu & "')"
                'strSql = "Update TCS_CTU_HDR SET " & _
                '         "kyhieu_ct = '" & objCTuHDR.KyHieu_CT & "', " & _
                '         "So_CT= '" & objCTuHDR.So_CT & "', So_BThu=" & objCTuHDR.So_BThu & ",So_CT_NH='" & objCTuHDR.So_CT_NH & "'," & _
                '         "Ma_NNTien='" & objCTuHDR.Ma_NNTien & "',Ten_NNTien='" & objCTuHDR.Ten_NNTien & "'," & _
                '         "DC_NNTien='" & objCTuHDR.DC_NNTien & "',Ma_NNThue='" & objCTuHDR.Ma_NNThue & "'," & _
                '         "Ten_NNThue='" & objCTuHDR.Ten_NNThue & "',DC_NNThue='" & objCTuHDR.DC_NNThue & "'," & _
                '         "Ly_do='" & objCTuHDR.Ly_Do & "',Ma_KS=0,Ma_TQ=0,So_QD='" & objCTuHDR.So_QD & "'," & _
                '         "Ngay_QD=" & objCTuHDR.Ngay_QD & ",CQ_QD='" & objCTuHDR.CQ_QD & "',Ngay_CT=" & objCTuHDR.Ngay_CT & "," & _
                '         "Ngay_HT=" & objCTuHDR.Ngay_HT & ",Ma_CQThu='" & objCTuHDR.Ma_CQThu & "'," & _
                '         "XA_ID=" & objCTuHDR.XA_ID & ",Ma_Tinh='" & objCTuHDR.Ma_Tinh & "',Ma_Huyen='" & objCTuHDR.Ma_Huyen & "'," & _
                '         "Ma_Xa='" & objCTuHDR.Ma_Xa & "',TK_Co='" & objCTuHDR.TK_Co & "'," & _
                '         "So_TK='" & objCTuHDR.So_TK & "',Ngay_TK=" & objCTuHDR.Ngay_TK & ",LH_XNK='" & objCTuHDR.LH_XNK & "'," & _
                '         "DVSDNS='" & objCTuHDR.DVSDNS & "',TEN_DVSDNS = '" & objCTuHDR.Ten_DVSDNS & "'," & _
                '         "Ma_NT='" & objCTuHDR.Ma_NT & "',Ty_Gia=" & objCTuHDR.Ty_Gia & "," & _
                '         "TG_ID = " & objCTuHDR.TG_ID & ",MA_LTHUE = '" & objCTuHDR.Ma_LThue & "'," & _
                '         "So_Khung='" & objCTuHDR.So_Khung & "',So_May='" & objCTuHDR.So_May & "'," & _
                '         "TK_KH_NH='" & objCTuHDR.TK_KH_NH & "',NGAY_KH_NH=" & objCTuHDR.NGAY_KH_NH & "," & _
                '         "MA_NH_A='" & objCTuHDR.MA_NH_A & "',MA_NH_B='" & objCTuHDR.MA_NH_B & "'," & _
                '         "TTien=" & objCTuHDR.TTien & ", TT_TThu= 0 , Lan_In=0, Trang_Thai='" & objCTuHDR.Trang_Thai & "', " & _
                '         "TK_KH_Nhan='" & objCTuHDR.TK_KH_Nhan & "', Ten_KH_Nhan='" & objCTuHDR.Ten_KH_Nhan & "'," & _
                '         "Diachi_KH_Nhan='" & objCTuHDR.Diachi_KH_Nhan & "', TTien_NT=" & objCTuHDR.TTien_NT & _
                '         " ,TK_NO=" & Globals.EscapeQuote(objCTuHDR.TK_No) & " ,PT_TT=" & Globals.EscapeQuote(objCTuHDR.PT_TT) & _
                '         " Where (SHKB = '" & objCTuHDR.SHKB & "') And (Ngay_KB = " & objCTuHDR.Ngay_KB & _
                '         ") And (Ma_NV = " & objCTuHDR.Ma_NV & ") And (So_BT = " & objCTuHDR.So_BT & _
                '         ") And (Ma_DThu = '" & objCTuHDR.Ma_DThu & "')"


                ' Update vào bảng TCS_CT_HDR
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                'Nếu tham số cho phép xóa thực thu thì mới xóa
                'Xử dụng khi cập nhật chứng từ mà không thay đổi tổng tiền
                If (blnXoaThucThu) Then
                    'Xóa phần chi tiết về số thực thu
                    strSql = "delete from tcs_khoquy_dtl " & _
                            "Where (SHKB = '" & objCTuHDR.SHKB & "') And (Ngay_KB = " & objCTuHDR.Ngay_KB & _
                            ") And (Ma_NV = " & objCTuHDR.Ma_NV & ") And (So_BT = " & objCTuHDR.So_BT & _
                            ") And (Ma_DThu = '" & objCTuHDR.Ma_DThu & "')"

                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                    'Xóa phần Header thực thu
                    strSql = "delete from tcs_khoquy_hdr hdr " & _
                            "Where (SHKB = '" & objCTuHDR.SHKB & "') And (Ngay_KB = " & objCTuHDR.Ngay_KB & _
                            ") And (Ma_NV = " & objCTuHDR.Ma_NV & ") And (So_BT = " & objCTuHDR.So_BT & _
                            ") And (Ma_DThu = '" & objCTuHDR.Ma_DThu & "')"

                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                End If

                'Xóa chứng từ chi tiết - để cập nhật lại
                strSql = "delete from TCS_CTU_DTL " & _
                        "where SHKB='" & objCTuHDR.SHKB & "' and Ngay_KB=" & objCTuHDR.Ngay_KB & " and " & _
                        "Ma_NV=" & objCTuHDR.Ma_NV & "  and So_BT=" & objCTuHDR.So_BT & "  and " & _
                        "Ma_DThu='" & objCTuHDR.Ma_DThu & "'"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
                For Each objDTL In objCTuDTL
                    strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                             "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                             "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,ma_dp) " & _
                             "Values (" & objDTL.ID & ",'" & objCTuHDR.SHKB & "'," & objCTuHDR.Ngay_KB & "," & objCTuHDR.Ma_NV & "," & _
                             objCTuHDR.So_BT & ",'" & objCTuHDR.Ma_DThu & "'," & objDTL.CCH_ID & ",'" & _
                             objDTL.Ma_Cap & "','" & objDTL.Ma_Chuong & "'," & objDTL.LKH_ID & ",'" & _
                             objDTL.Ma_Loai & "','" & objDTL.Ma_Khoan & "'," & _
                             objDTL.MTM_ID & ",'" & objDTL.Ma_Muc & "','" & objDTL.Ma_TMuc & "','" & _
                             objDTL.Noi_Dung & "'," & _
                             objDTL.DT_ID & ",'" & objDTL.Ma_TLDT & "','" & objDTL.Ky_Thue & "'," & _
                             objDTL.SoTien & "," & objDTL.SoTien_NT & "," & Globals.EscapeQuote(objDTL.MaQuy) & "," & Globals.EscapeQuote(objDTL.Ma_DP) & ")"
                    ' Insert Vào bảng TCS_CTU_DTL    
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                Next
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật dữ liệu chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub

        Public Sub InsertBaoLanh(ByVal hdr As NewChungTu.infChungTuHDR, ByVal dtls As NewChungTu.infChungTuDTL())
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_BAOLANH_HDR và DTL
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction

                strSql = " Insert into TCS_BAOLANH_HDR( " & _
                             " kyhieu_ct, so_ct, ngay_ct, ma_nv, ten_nv, " & _
                             " so_bl, kieu_bl, ngay_batdau, ngay_ketthuc, songay_bl, " & _
                             " ma_nnthue, ten_nnthue, dc_nnthue, tk_co, tk_no, ten_tk_ns, " & _
                             " ma_cqthu, ten_cqthu, so_tk, ngay_tk, lhxnk, ten_lhxnk, dien_giai, " & _
                             " shkb, ten_kb, ngay_kb, so_bt, ma_dthu, ma_xa, xa_id, trang_thai, " & _
                             " ma_loaitien, ttien, ttien_nt, so_ct_tuchinh, ma_hq_ph, ten_hq_ph," & _
                             " vt_don2, ngay_vtd2, vt_don3, ngay_vtd3,vt_don4, ngay_vtd4,vt_don5, ngay_vtd5," & _
                             " ma_hq, ten_hq, cif, hd_nt,ngay_hd,hoa_don,vt_don, ngay_hdon, ngay_vtd,type_bl,MA_DV_DD,TEN_DV_DD,trang_thai_bl)" & _
                         " Values('" & _
                             hdr.KHCT & "','" & _
                             hdr.So_CT & "'," & _
                             hdr.Ngay_CT & ", '" & _
                             hdr.Ma_NV & "','" & _
                             hdr.Ten_nv & "', '" & _
                             hdr.So_bl & "','" & _
                             hdr.Kieu_bl & "'," & _
                             hdr.Ngay_batdau & "," & _
                             hdr.Ngay_ketthuc & ",'" & _
                             hdr.Songay_bl & "','" & _
                             hdr.Ma_NNThue & "','" & _
                             hdr.Ten_NNThue & "','" & _
                             hdr.DC_NNThue & "','" & _
                             hdr.TK_KH_NH & "','" & _
                             hdr.TK_KH_NH & "','" & _
                             hdr.Ten_tk_ns & "','" & _
                             hdr.Ma_CQThu & "','" & _
                             hdr.Ten_cqthu & "','" & _
                             hdr.So_TK & "', " & _
                             hdr.Ngay_TK & " ,'" & _
                             hdr.LH_XNK & "','" & _
                             hdr.Ten_lhxnk & "','" & _
                             hdr.Dien_giai & "','" & _
                             hdr.SHKB & "','" & _
                             hdr.Ten_kb & "'," & _
                             hdr.Ngay_KB & ",'" & _
                             hdr.So_BT & "','" & _
                             hdr.Ma_DThu & "','" & _
                             hdr.Ma_Xa & "'," & _
                             hdr.XA_ID & ",'" & _
                             hdr.Trang_Thai & "','" & _
                             hdr.Ma_loaitien & "'," & _
                             hdr.TTien & "," & _
                             hdr.TTien_NT & ",'" & _
                             hdr.So_CT_TuChinh & "','" & _
                             hdr.Ma_hq_ph & "','" & _
                             hdr.TEN_HQ_PH & "','" & _
                             hdr.VTD2 & "'," & _
                             hdr.Ngay_VTD2 & ",'" & _
                             hdr.VTD3 & "'," & _
                             hdr.Ngay_VTD3 & ",'" & _
                             hdr.VTD4 & "'," & _
                             hdr.Ngay_VTD4 & ",'" & _
                             hdr.VTD5 & "'," & _
                             hdr.Ngay_VTD5 & ",'" & _
                             hdr.MA_HQ & "','" & _
                             hdr.TEN_HQ & "','" & _
                             hdr.CIF & "','" & _
                             hdr.HD_NT & "'," & _
                             hdr.Ngay_HD_NT & ",'" & _
                             hdr.HDon & "','" & _
                             hdr.VTD & "'," & _
                             hdr.Ngay_HDon & "," & _
                             hdr.Ngay_VTD & ",'" & _
                             hdr.LOAI_BL & "','" & _
                             hdr.MA_DV_DD & "','" & _
                             hdr.TEN_DV_DD & "','" & _
                             hdr.TrangThai_BL & "')"

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                ' Hình thành câu lệnh insert vào bảng TCS_BAOLANH_DTL
                'For Each objDTL In dtls
                '    strSql = " Insert into TCS_BAOLANH_DTL(" & _
                '                 " id, shkb, ngay_kb, ma_nv, so_bt, ma_dthu, ma_quy, " & _
                '                 " ma_cap, ma_chuong, ma_ndkt, noi_dung, sotien, ky_thue, ma_khtk, ma_nkt" & _
                '             " ) " & _
                '             "Values (" & _
                '                 objDTL.ID & ",'" & _
                '                 objDTL.SHKB & "'," & _
                '                 objDTL.Ngay_KB & "," & _
                '                 objDTL.Ma_NV & "," & _
                '                 objDTL.So_BT & ",'" & _
                '                 objDTL.Ma_DThu & "','" & _
                '                 objDTL.MaQuy & "','" & _
                '                 objDTL.Ma_Cap & "','" & _
                '                 objDTL.Ma_Chuong & "','" & _
                '                 objDTL.Ma_ndkt & "','" & _
                '                 objDTL.Noi_Dung & "'," & _
                '                 objDTL.SoTien & ",'" & _
                '                 objDTL.Ky_Thue & "','" & _
                '                 objDTL.Ma_khtk & "','" & _
                '                 objDTL.Ma_nkt & "'" & _
                '                 ")"
                '    ' Insert Vào bảng TCS_BAOLANH_DTL    
                '    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                'Next
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu bảo lãnh")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu bảo lãnh: " & ex.ToString & vbCrLf & strSql, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub

        Public Sub UpdateBaoLanh(ByVal hdr As infChungTuHDR, ByVal dtls As infChungTuDTL())
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu bảo lãnh vào bảng TCS_BAOLANH_HDR và DTL          
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                'Cập nhật thông tin HDR của bảo lãnh              
                strSql = " Update TCS_BAOLANH_HDR SET " & _
                         " so_bl ='" & hdr.So_bl & "', kieu_bl='" & hdr.Kieu_bl & "'," & _
                         " ngay_batdau =" & hdr.Ngay_batdau & ", ngay_ketthuc =" & hdr.Ngay_ketthuc & "," & _
                         " songay_bl ='" & hdr.Songay_bl & "', ma_nnthue ='" & hdr.Ma_NNThue & "'," & _
                         " ten_nnthue ='" & hdr.Ten_NNThue & "', dc_nnthue ='" & hdr.DC_NNThue & "'," & _
                         " so_tk ='" & hdr.So_TK & "', ten_tk_ns ='" & hdr.Ten_tk_ns & "'," & _
                         " ma_cqthu ='" & hdr.Ma_CQThu & "', ten_cqthu ='" & hdr.Ten_cqthu & "'," & _
                         " ngay_tk =" & hdr.Ngay_TK & ", lhxnk ='" & hdr.LH_XNK & "'," & _
                         " ten_lhxnk ='" & hdr.Ten_lhxnk & "', Dien_giai ='" & hdr.Dien_giai & "'," & _
                         " ma_loaitien ='" & hdr.Ma_loaitien & "', " & _
                         " trang_thai ='" & hdr.Trang_Thai & "', " & _
                         " ttien = " & hdr.TTien & ", " & _
                         " ma_hq_ph = '" & hdr.Ma_hq_ph & "', " & _
                         " ten_hq_ph = '" & hdr.TEN_HQ_PH & "', " & _
                         " ma_hq = '" & hdr.MA_HQ & "', " & _
                         " ten_hq = '" & hdr.TEN_HQ & "', " & _
                         " ttien_nt = '" & hdr.TTien & "', " & _
                         " cif = '" & hdr.CIF & "' ," & _
                         " hd_nt = '" & hdr.HD_NT & "', " & _
                        " ngay_hd = " & hdr.Ngay_HD_NT & ", " & _
                        " hoa_don = '" & hdr.HDon & "', " & _
                        " vt_don2 = '" & hdr.VTD2 & "', " & _
                        " ngay_vtd2 = " & hdr.Ngay_VTD2 & ", " & _
                        " vt_don3 = '" & hdr.VTD3 & "', " & _
                        " ngay_vtd3 = " & hdr.Ngay_VTD3 & ", " & _
                        " vt_don4 = '" & hdr.VTD4 & "', " & _
                        " ngay_vtd4 = " & hdr.Ngay_VTD4 & ", " & _
                        " vt_don5 = '" & hdr.VTD5 & "', " & _
                        " ngay_vtd5 = " & hdr.Ngay_VTD5 & ", " & _
                        " vt_don = '" & hdr.VTD & "', " & _
                        " ngay_hdon = " & hdr.Ngay_HDon & ", " & _
                        " ngay_vtd = " & hdr.Ngay_VTD & ", " & _
                        " MA_DV_DD = '" & hdr.MA_DV_DD & "', " & _
                        " TEN_DV_DD = '" & hdr.TEN_DV_DD & "'" & _
                         " Where " & _
                                " SHKB = '" & hdr.SHKB & "'" & _
                                " And Ngay_KB = " & hdr.Ngay_KB & _
                                " And Ma_NV = " & hdr.Ma_NV & _
                                 " And So_CT = " & hdr.So_CT & _
                        " And So_BT  = " & hdr.So_BT

                ' Update vào bảng TCS_CT_HDR
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                'Xóa bảo lãnh chi tiết - để cập nhật lại
                strSql = " Delete From TCS_BAOLANH_DTL " & _
                         " where " & _
                                 " SHKB ='" & hdr.SHKB & "'" & _
                                 " And Ngay_KB = " & hdr.Ngay_KB & _
                                 " And Ma_NV =" & hdr.Ma_NV & _
                                 " And So_BT = " & hdr.So_BT
                ' " And Ma_DThu ='" & hdr.Ma_DThu & "'"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                ' Hình thành câu lệnh insert vào bảng TCS_BAOLANH_DTL
                'For Each objDTL In dtls
                '    strSql = " Insert into TCS_BAOLANH_DTL(" & _
                '             " id, shkb, ngay_kb, ma_nv, so_bt, ma_dthu, ma_quy, " & _
                '             " ma_cap, ma_chuong, ma_ndkt, noi_dung, sotien, ky_thue, ma_khtk, ma_nkt" & _
                '             " ) " & _
                '             "Values (" & objDTL.ID & ",'" & objDTL.SHKB & "'," & objDTL.Ngay_KB & "," & objDTL.Ma_NV & "," & _
                '                     objDTL.So_BT & ",'" & objDTL.Ma_DThu & "','" & objDTL.MaQuy & "','" & _
                '                     objDTL.Ma_Cap & "','" & objDTL.Ma_Chuong & "','" & objDTL.Ma_ndkt & "','" & _
                '                     objDTL.Noi_Dung & "'," & objDTL.SoTien & ",'" & _
                '                     objDTL.Ky_Thue & "','" & objDTL.Ma_khtk & "','" & objDTL.Ma_nkt & "'" & _
                '                     ")"
                '    ' Insert Vào bảng TCS_BAOLANH_DTL    
                '    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                'Next
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
        Public Sub Update_TT_BaoLanh(ByVal hdr As infChungTuHDR)
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu bảo lãnh vào bảng TCS_BAOLANH_HDR và DTL          
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                'Cập nhật thông tin HDR của bảo lãnh              
                strSql = " Update TCS_BAOLANH_HDR SET " & _
                         " trang_thai ='" & hdr.Trang_Thai & "' " & _
                          " where 1=1 " & _
                        " And So_CT = '" & hdr.So_CT & "'"

                ' Update vào bảng TCS_CT_HDR
                '" hd_nt = " & hdr.HD_NT & ", " & _
                '        " ngay_hd = " & hdr.Ngay_HD_NT & ", " & _
                '        " hoa_don = " & hdr.HDon & ", " & _
                '        " vt_don = " & hdr.VTD & ", " & _
                '        " ngay_hdon = " & hdr.Ngay_HDon & ", " & _
                '        " ngay_vtd = " & hdr.Ngay_VTD & _
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                'Xóa bảo lãnh chi tiết - để cập nhật lại
                'strSql = " Delete From TCS_BAOLANH_DTL " & _
                '         " where " & _
                '                 " SHKB ='" & hdr.SHKB & "'" & _
                '                 " And Ngay_KB = " & hdr.Ngay_KB & _
                '                 " And Ma_NV =" & hdr.Ma_NV & _
                '                 " And So_BT = " & hdr.So_BT & _
                '                 " And Ma_DThu ='" & hdr.Ma_DThu & "'"
                'conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                ' Hình thành câu lệnh insert vào bảng TCS_BAOLANH_DTL
                'For Each objDTL In dtls
                '    strSql = " Insert into TCS_BAOLANH_DTL(" & _
                '             " id, shkb, ngay_kb, ma_nv, so_bt, ma_dthu, ma_quy, " & _
                '             " ma_cap, ma_chuong, ma_ndkt, noi_dung, sotien, ky_thue, ma_khtk, ma_nkt" & _
                '             " ) " & _
                '             "Values (" & objDTL.ID & ",'" & objDTL.SHKB & "'," & objDTL.Ngay_KB & "," & objDTL.Ma_NV & "," & _
                '                     objDTL.So_BT & ",'" & objDTL.Ma_DThu & "','" & objDTL.MaQuy & "','" & _
                '                     objDTL.Ma_Cap & "','" & objDTL.Ma_Chuong & "','" & objDTL.Ma_ndkt & "','" & _
                '                     objDTL.Noi_Dung & "'," & objDTL.SoTien & ",'" & _
                '                     objDTL.Ky_Thue & "','" & objDTL.Ma_khtk & "','" & objDTL.Ma_nkt & "'" & _
                '                     ")"
                '    ' Insert Vào bảng TCS_BAOLANH_DTL    
                '    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                'Next
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
        Public Shared Function UpdateTrangThaiBaoLanh(ByVal key As KeyCTu, ByVal trangThai As String, ByVal strLyDoHuy As String) As Boolean
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                'Cập nhật thông tin HDR của bảo lãnh              
                strSql = " Update TCS_BAOLANH_HDR SET " & _
                             " trang_thai = '" & trangThai & "', " & _
                             " ma_ks = '" & key.Ma_NV & "', " & _
                             " ngay_ks = sysdate, " & _
                             " ten_ks = '" & mdlCommon.Get_TenNV(key.Ma_NV) & "', " & _
                             " ngay_ht = sysdate " & _
                             IIf(strLyDoHuy.Length > 0, ", ly_do_huy = '" & strLyDoHuy & "'", "") & _
                         " Where " & _
                                    " SHKB = '" & key.SHKB & "'" & _
                                    " And Ngay_KB = " & key.Ngay_KB & _
                " and ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & key.Ma_NV & "')) " & _
                                   " And So_BT = " & key.So_BT & _
                                   ""
                '" And Ma_DThu = '" & key.Ma_Dthu & "'"

                ' Cập nhật dữ liệu vào CSDL
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Shared Function UpdateTrangThaiThanhToan(ByVal strSoCT As String, ByVal strTT As String, ByVal strSoBT As String, ByVal strSHKB As String) As Boolean
            Dim conn As DataAccess
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                'Cập nhật thông tin HDR của bảo lãnh              
                strSql = " Update TCS_BAOLANH_HDR SET " & _
                             " TRANG_THAI_TT = 1, " & _
                             " NGAY_THANH_TOAN = sysdate " & _
                         " Where SO_CT = '" & strSoCT & "'" & _
                            " And TRANG_THAI = '" & strTT & "'" & _
                            " And SO_BT = '" & strSoBT & "'" & _
                            " And SHKB = '" & strSHKB & "'"

                '" And Ma_DThu = '" & key.Ma_Dthu & "'"

                ' Cập nhật dữ liệu vào CSDL
                conn.ExecuteNonQuery(strSql, CommandType.Text)

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Shared Function UpdateTrangThaiBaoLanh(ByVal key As KeyCTu, ByVal trangThai As String, ByVal strLyDoHuy As String, ByVal strLyDoCTra As String) As Boolean
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                'Cập nhật thông tin HDR của bảo lãnh              
                strSql = " Update TCS_BAOLANH_HDR SET " & _
                             " trang_thai = '" & trangThai & "', " & _
                             " ma_ks = '" & key.Ma_NV & "', " & _
                             " ngay_ks = sysdate, " & _
                             " ly_do_ctra = '" & strLyDoCTra & "', " & _
                             " ten_ks = '" & mdlCommon.Get_TenNV(key.Ma_NV) & "', " & _
                             " ngay_ht = sysdate " & _
                             IIf(strLyDoHuy.Length > 0, ", ly_do_huy = '" & strLyDoHuy & "'", "") & _
                         " Where " & _
                                  " Ngay_KB = " & key.Ngay_KB & _
                                    " And So_BT = " & key.So_BT & _
                                     " And so_ct = '" & key.So_CT & "'"
                ' " And Ma_DThu = '" & key.Ma_Dthu & "'" & _
                '" SHKB = '" & key.SHKB & "'" & _                                    " And 

                ' Cập nhật dữ liệu vào CSDL
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật trạng thái bảo lãnh")
                'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '              & "Error code: System error!" & vbNewLine _
                '              & "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        Public Sub InsertDoiChieuNHHQ(ByVal hdr As infDoiChieuNHHQ_HDR, ByVal dtls() As infDoiChieuNHHQ_DTL, Optional ByVal strTT_CT As String = "", Optional ByVal DescErr As String = "")
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infDoiChieuNHHQ_DTL
            Dim strSql As String
            Dim strID As String
            Try
                strID = Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_HDR_ID.NextVal")
                conn = New DataAccess
                transCT = conn.BeginTransaction


                strSql = " Insert into TCS_DCHIEU_NHHQ_HDR( " & _
                             " id, transaction_id, transaction_type, shkb, ten_kb, " & _
                             " ngay_kb, so_bt, kyhieu_ct, so_ct, ngay_ct, ngay_bn, " & _
                             " ngay_bc, ma_nnthue, ten_nnthue, ma_cqthu, ten_cqthu, " & _
                             " so_tk, ngay_tk, lhxnk, vt_lhxnk, ten_lhxnk, " & _
                             " ttien, ttien_nt, trang_thai, tk_ns, ten_tk_ns, ma_ntk, " & _
                             " so_bl, ngay_bl_batdau, ngay_bl_ketthuc, songay_bl, " & _
                             " kieu_bl, diengiai, accept_yn, parent_transaction_id, " & _
                             " ma_hq_ph, ma_loaitien, tk_ns_hq, ma_nh_ph, ten_nh_ph, " & _
                             " ma_hq_cqt, error_code_hq, so_bt_hq, ma_hq, ten_hq, ten_hq_ph,ly_do_huy," & _
                             " vt_don, ngay_vtd,vt_don2, ngay_vtd2, vt_don3, ngay_vtd3,vt_don4, ngay_vtd4,vt_don5, ngay_vtd5," & _
                             " ma_nh_th, tkkb, tkkb_ct, ma_nv, ma_nt,ty_gia,so_tn_ct,ngay_tn_ct,ten_nh_th,ma_dv_dd,ten_dv_dd, tg_id, so_hd, ngay_hd,KENHGD)" & _
                         " Values( " & _
                             strID & " , '" & _
                             hdr.transaction_id & "','" & _
                             hdr.transaction_type & "','" & _
                             hdr.shkb & "','" & _
                             hdr.ten_kb & "'," & _
                             hdr.ngay_kb & "," & _
                             hdr.so_bt & ",'" & _
                             hdr.kyhieu_ct & "','" & _
                             hdr.so_ct & "'," & _
                             hdr.ngay_ct & "," & _
                             hdr.ngay_bn & "," & _
                             hdr.ngay_bc & ",'" & _
                             hdr.ma_nnthue & "','" & _
                             hdr.ten_nnthue & "','" & _
                             hdr.ma_cqthu & "','" & _
                             hdr.ten_cqthu & "','" & _
                             hdr.so_tk & "'," & _
                             hdr.ngay_tk & ",'" & _
                             hdr.lhxnk & "','" & _
                             hdr.vt_lhxnk & "','" & _
                             hdr.ten_lhxnk & "'," & _
                             hdr.ttien & "," & _
                             hdr.ttien_nt & ",'" & _
                             hdr.trang_thai & "','" & _
                             hdr.tk_ns & "','" & _
                             hdr.ten_tk_ns & "','" & _
                             hdr.ma_ntk & "','" & _
                             hdr.so_bl & "', " & _
                             hdr.ngay_bl_batdau & ", " & _
                             hdr.ngay_bl_ketthuc & "," & _
                             hdr.songay_bl & ",'" & _
                             hdr.kieu_bl & "','" & _
                             hdr.dien_giai & "','" & _
                             hdr.accept_yn & "','" & _
                             hdr.parent_transaction_id & "','" & _
                             hdr.ma_hq_ph & "','" & _
                             hdr.ma_loaitien & "','" & _
                             hdr.tk_ns_hq & "','" & _
                             hdr.ma_nh_ph & "','" & _
                             hdr.ten_nh_ph & "','" & _
                             hdr.ma_hq_cqt & "','" & _
                             hdr.error_code_hq & "'," & _
                             hdr.so_bt_hq & ",'" & _
                             hdr.ma_hq & "','" & _
                             hdr.ten_hq & "','" & _
                             hdr.ten_hq_ph & "','" & _
                             hdr.ly_do_huy & "','" & _
                             hdr.vtd & "'," & _
                             hdr.ngay_vtd & ",'" & _
                             hdr.vtd2 & "'," & _
                             hdr.ngay_vtd2 & ",'" & _
                             hdr.vtd3 & "'," & _
                             hdr.ngay_vtd3 & ",'" & _
                             hdr.vtd4 & "'," & _
                             hdr.ngay_vtd4 & ",'" & _
                             hdr.vtd5 & "'," & _
                             hdr.ngay_vtd5 & ",'" & _
                             hdr.ma_nh_th & "','" & _
                             hdr.tkkb & "','" & _
                             hdr.tkkb_ct & "','" & _
                             hdr.ma_nv & "','" & _
                             hdr.ma_nt & "','" & _
                             hdr.ty_gia & "','" & _
                             hdr.so_tn_ct & "','" & _
                             hdr.ngay_tn_ct & "','" & _
                             hdr.ten_nh_th & "','" & _
                             hdr.ma_dv_dd & "','" & _
                             hdr.ten_dv_dd & "','" & _
                             hdr.tg_id & "','" & _
                             hdr.so_hd & "'," & _
                             hdr.ngay_hd & ",'" & hdr.KENHGD & "')"

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)


                ' Insert vào bảng DOICHIEU_NHHQ_DTL
                If Not dtls Is Nothing Then
                    For Each objDTL In dtls
                        strSql = " Insert into TCS_DCHIEU_NHHQ_DTL(" & _
                                    " hdr_id, ma_quy, ma_cap, ma_chuong, ma_nkt, " & _
                                    " ma_nkt_cha, ma_ndkt, ma_ndkt_cha, ma_tlpc, " & _
                                    " ma_khtk, noi_dung, ma_dp, ky_thue, sotien,sotien_nt ) " & _
                                 " Values (" & strID & ", '" & _
                                    objDTL.ma_quy & "','" & _
                                    objDTL.ma_cap & "','" & _
                                    objDTL.ma_chuong & "','" & _
                                    objDTL.ma_nkt & "','" & _
                                    objDTL.ma_nkt_cha & "','" & _
                                    objDTL.ma_ndkt & "','" & _
                                    objDTL.ma_ndkt_cha & "'," & _
                                    objDTL.ma_tlpc & ",'" & _
                                    objDTL.ma_khtk & "','" & _
                                    objDTL.noi_dung & "','" & _
                                    objDTL.ma_dp & "','" & _
                                    objDTL.ky_thue & "'," & _
                                    objDTL.sotien & "," & _
                                    objDTL.sotien_nt & ")"
                        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                    Next
                End If
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
        Public Sub InsertMSGLoi(ByVal hdr As infDoiChieuNHHQ_HDR, ByVal dtls() As infDoiChieuNHHQ_DTL, Optional ByVal strTT_CT As String = "", Optional ByVal DescErr As String = "")
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infDoiChieuNHHQ_DTL
            Dim strSql As String
            Dim strID As String
            Try
                strID = Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_HDR_ID.NextVal")
                conn = New DataAccess
                transCT = conn.BeginTransaction
                If strTT_CT <> "" Then
                    strSql = "UPDATE TCS_CTU_HDR SET  TT_CTHUE = '" & strTT_CT & "'," & _
                            " MSG_LOI = '" & DescErr & "'" & _
                             " where so_ct='" & hdr.so_ct.ToString() & "'"
                                      
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                End If
                ' Insert vào bảng DOICHIEU_NHHQ_DTL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub

        Public Function SelectCT_HDR(ByVal strSHKB As String, ByVal iNgay_KB As Integer, _
                                     ByVal iMa_NV As Integer, ByVal iSo_BT As Integer, _
                                     ByVal strMa_DThu As String) As DataSet
            ' ****************************************************************************
            ' Mục đích: lấy ra một chứng từ trong bảng TCS_CTU_HDR với các điều kiện đầu vào
            ' Người viết: HoanNH
            ' Ngày viết: 16/10/2007
            '*****************************************************************************
            Dim dsCT As New DataSet
            Dim strSql As String
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                strSql = " Select " & _
                           " shkb, ngay_kb, ma_nv, so_bt, ma_dthu, so_bthu, " & _
                           " kyhieu_ct, so_ct, so_ct_nh, ma_nntien, ten_nntien, " & _
                           " dc_nntien, ma_nnthue, ten_nnthue, dc_nnthue, ly_do_huy, " & _
                           " ma_ks, ma_tq, so_qd, ngay_qd, cq_qd, ngay_ct, " & _
                           " ngay_ht, ma_cqthu, xa_id, ma_tinh, ma_huyen, ma_xa, " & _
                           " tk_no, tk_co, ds_tokhai so_tk, to_char(ngay_tk,'dd/MM/yyyy') ngay_tk, lh_xnk, dvsdns, " & _
                           " ten_dvsdns, ma_nt, ty_gia, tg_id, ma_lthue, so_khung, " & _
                           " so_may, tk_kh_nh, ngay_kh_nh, ma_nh_a, ma_nh_b, " & _
                           " ttien, tt_tthu, lan_in, trang_thai, tk_kh_nhan, " & _
                           " ten_kh_nhan, diachi_kh_nhan, ttien_nt, so_bt_ktkb, " & _
                           " pt_tt, tt_in, tt_in_bk, so_bk, ngay_bk, ctu_gnt_bl, " & _
                           " ref_no, tt_thu, ten_kh_nh, tk_gl_nh, tk_kb_nh, " & _
                           " isactive, seq_no, phi_gd, phi_vat, pt_tinhphi, " & _
                           " tt_cthue, rm_ref_no, ngay_ks, quan_huyennntien, " & _
                           " tinh_tpnntien, tkht, loai_tt,loai_tt ma_loaitien, ma_ntk, ma_hq, ten_hq, ma_hq_ph, ten_hq_ph " & _
                         " From TCS_CTU_HDR " & _
                         " Where (SHKB = '" & strSHKB & "') And (Ngay_KB = " & iNgay_KB & _
                         ") And (Ma_NV = " & iMa_NV & ") And (So_BT = " & iSo_BT & _
                         ") "
                dsCT = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return dsCT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu bảng TCS_CTU_HDR!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng kết nối
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function Select_Ctu_Hdr(ByVal soCTu As String) As DataSet
            Dim dtSet As New DataSet
            Dim strSql As String
            Dim connectDB As DataAccess
            Try
                connectDB = New DataAccess
                strSql = "select shkb ,MA_NNTHUE, ma_nv, so_bt from tcs_ctu_hdr where so_ct='" & soCTu & "'"
                dtSet = connectDB.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return dtSet
            Catch ex As Exception
                Throw ex
            Finally
                If Not connectDB Is Nothing Then
                    connectDB.Dispose()
                End If
            End Try
        End Function
        Public Function Select_key_Hdr(ByVal soCtu As String, ByVal kenh_Ctu As String) As KeyCTu
            Dim dtSet As New DataSet
            Dim strSql As String
            Dim connectDB As DataAccess
            Dim key As New KeyCTu
            Try
                connectDB = New DataAccess
                strSql = "select shkb,ngay_kb,ma_nv,so_bt,ma_dthu,kyhieu_ct from tcs_ctu_hdr where so_ct='" & soCtu & "' and kenh_ct='" & kenh_Ctu & "'"
                dtSet = connectDB.ExecuteReturnDataSet(strSql, CommandType.Text)
                key.So_CT = soCtu
                key.SHKB = dtSet.Tables(0).Rows(0)("shkb")
                key.Ngay_KB = dtSet.Tables(0).Rows(0)("ngay_kb")
                key.Ma_NV = dtSet.Tables(0).Rows(0)("ma_nv")
                key.Ma_Dthu = dtSet.Tables(0).Rows(0)("ma_dthu")
                key.Kyhieu_CT = dtSet.Tables(0).Rows(0)("kyhieu_ct")
                key.So_BT = dtSet.Tables(0).Rows(0)("so_bt")
                key.TT_BDS = ""
                key.TT_NSTT = ""
                key.TT_TRUYVAN = ""
                Return key
            Catch ex As Exception
                Throw ex
            Finally
                If Not connectDB Is Nothing Then
                    connectDB.Dispose()
                End If
            End Try
        End Function
        Public Function blnTCS_Huy_CT(ByVal strSHKB As String, ByVal iNgay_KB As Integer, _
                                      ByVal iMa_NV As Integer, ByVal iSo_BT As Integer, _
                                      ByVal strMa_DThu As String) As Boolean
            ' ****************************************************************************
            ' Mục đích: Hủy một chứng từ trong bảng TCS_CTU_HDR với các điều kiện đầu vào
            ' Người viết: HoanNH
            ' Ngày viết: 16/10/2007
            '*****************************************************************************
            Dim conn As DataAccess
            Dim strSql As String
            Dim blnResult As Boolean = True
            Try
                conn = New DataAccess
                strSql = "Update TCS_CTU_HDR Set Trang_Thai='04' " & _
                         "Where (SHKB = '" & strSHKB & "') And (Ngay_KB = " & iNgay_KB & _
                         ") And (Ma_NV = " & iMa_NV & ") And (So_BT = " & iSo_BT & _
                         ") And (Ma_DThu = '" & strMa_DThu & "')"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
                blnResult = True
            Catch ex As Exception
                blnResult = False
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi trong quá trình hủy chứng từ")
                Throw ex
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình Hủy chứng từ!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function blnTCS_Update_CT(ByVal strSHKB As String, ByVal iNgay_KB As Integer, _
                                          ByVal iMa_NV As Integer, ByVal iSo_BT As Integer, _
                                          ByVal strMa_DThu As String, ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL()) As Boolean
            ' Mục đích: Cập nhật các thông tin trên form vào CT đã có trong CSDL.
            '           (dùng cho trưòng hợp NSD nhập CT bằng tay, không in và muốn
            '           sửa lại các thông tin bị nhầm)
            ' Tham số: 
            ' Giá trị trả về: 
            ' Ngày viết: 23/10/2007
            ' Người viết: Nguyễn Hữu Hoan
            ' -----------------------------------------------------------------
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim blnResult As Boolean = True
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                strSql = "Update TCS_CTU_HDR SET " & _
                         "So_BThu = '" & objCTuHDR.So_BThu & ",So_CT_NH='" & objCTuHDR.So_CT_NH & "'," & _
                         "Ma_NNTien ='" & objCTuHDR.Ma_NNTien & " ',Ten_NNTien= '" & objCTuHDR.Ten_NNTien & "'," & _
                         "DC_NNTien = '" & objCTuHDR.DC_NNTien & "',Ma_NNThue = '" & objCTuHDR.Ma_NNThue & "'," & _
                         "Ten_NNThue = '" & objCTuHDR.Ten_NNThue & "',DC_NNThue='" & objCTuHDR.DC_NNThue & "'," & _
                         "Ma_KS = " & objCTuHDR.Ma_KS & ",Ma_TQ=" & objCTuHDR.Ma_TQ & ",So_QD='" & objCTuHDR.So_QD & "'," & _
                         "Ngay_QD=" & objCTuHDR.Ngay_QD & ",CQ_QD='" & objCTuHDR.CQ_QD & "',Ngay_CT=" & objCTuHDR.Ngay_CT & "," & _
                         "Ngay_HT = " & objCTuHDR.Ngay_HT & ",Ma_CQThu='" & objCTuHDR.Ma_CQThu & "'," & _
                         "XA_ID=" & objCTuHDR.XA_ID & ",Ma_Tinh='" & objCTuHDR.Ma_Tinh & "',Ma_Huyen='" & objCTuHDR.Ma_Huyen & "'," & _
                         "Ma_Xa='" & objCTuHDR.Ma_Xa & " ',TK_No='" & objCTuHDR.TK_No & "',TK_Co='" & objCTuHDR.TK_Co & "'," & _
                         "So_TK='" & objCTuHDR.So_TK & " ',Ngay_TK=" & objCTuHDR.Ngay_TK & ",LH_XNK='" & objCTuHDR.LH_XNK & "',DVSDNS='" & objCTuHDR.DVSDNS & "'," & _
                         "TEN_DVSDNS = '" & objCTuHDR.Ten_DVSDNS & ",Ma_NT='" & objCTuHDR.Ma_NT & "'," & _
                         "Ty_Gia='" & objCTuHDR.Ty_Gia & "',TG_ID = " & objCTuHDR.TG_ID & "," & _
                         "So_Khung='" & objCTuHDR.So_Khung & "',So_May='" & objCTuHDR.So_May & "',TK_KH_NH='" & objCTuHDR.TK_KH_NH & "'," & _
                         "NGAY_KH_NH=" & objCTuHDR.NGAY_KH_NH & ",MA_NH_A='" & objCTuHDR.MA_NH_A & "',MA_NH_B='" & objCTuHDR.MA_NH_B & "'," & _
                         "TTien=" & objCTuHDR.TTien & ",TT_TThu=" & objCTuHDR.TT_TThu & ",Lan_In=" & objCTuHDR.Lan_In & ",Trang_Thai='" & objCTuHDR.Trang_Thai & "' " & _
                         "Where (SHKB = '" & strSHKB & "') And (Ngay_KB = " & iNgay_KB & _
                         ") And (Ma_NV = " & iMa_NV & ") And (So_BT = " & iSo_BT & _
                         ") "

                ' Update vào bảng TCS_CT_HDR
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
                For Each objDTL In objCTuDTL
                    strSql = "Update TCS_CTU_DTL SET " & _
                             "CCH_ID=" & objDTL.CCH_ID & ",Ma_Cap='" & _
                             objDTL.Ma_Cap & "',Ma_Chuong='" & objDTL.Ma_Chuong & "',LKH_ID=" & objDTL.LKH_ID & ",'" & _
                             "Ma_Loai= " & objDTL.Ma_Loai & " ',Ma_Khoan='" & objDTL.Ma_Khoan & "',MTM_ID=" & _
                             objDTL.MTM_ID & ",Ma_Muc='" & objDTL.Ma_Muc & "',Ma_TMuc='" & objDTL.Ma_TMuc & "'," & _
                             "Noi_Dung= '" & objDTL.Noi_Dung & " ',DT_ID=" & _
                             objDTL.DT_ID & ",Ma_TLDT='" & objDTL.Ma_TLDT & "',Ky_Thue='" & objDTL.Ky_Thue & "'," & _
                             "SoTien=" & objDTL.SoTien & ",SoTien_NT=" & objDTL.SoTien_NT & _
                             " Where (SHKB = '" & strSHKB & "') And (Ngay_KB = " & iNgay_KB & _
                             ") And (Ma_NV = " & iMa_NV & ") And (So_BT = " & iSo_BT & _
                             ") and (ID = " & objDTL.ID & ")"
                    ' Update Vào bảng TCS_CTU_DTL    
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                Next
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
                blnResult = True
            Catch ex As Exception
                blnResult = False
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật dữ liệu chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình Update dữ liệu chứng từ!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function TCS_LoaiThue() As Boolean
            '----------------------------------------------------------------
            ' Mục đích: Lấy loại thuế trong bảng DTU_DM_LOAITHUE
            '           đồng thời tạo ra các mảng tiêu chí phân biệt loại thuế.
            ' Tham số: 
            ' Giá trị trả về: 
            ' Ngày viết: 25/10/2007
            ' Người viết: Nguyễn Hữu Hoan
            ' ---------------------------------------------------------------
            Dim cnLT As DataAccess
            Dim drLT As IDataReader
            Dim cnQXD As DataAccess
            Dim drXD As IDataReader
            Dim strSQL As String
            Dim strMa_Loaithue As String
            Dim strTen As String
            Dim strMa_Muc As String
            Dim strMa_TMuc As String
            Dim strMuc_Tmuc As String
            Dim i As Integer = 1
            Dim blnResult As Boolean

            Try
                cnLT = New DataAccess
                cnQXD = New DataAccess
                ' Xây dựng List Box loại thuế
                strSQL = "Select Ma_LThue,TEN From TCS_DM_LTHUE Order By Ma_LThue"
                drLT = cnLT.ExecuteDataReader(strSQL, CommandType.Text)
                Do While drLT.Read
                    strMa_Loaithue = drLT.GetValue(0)
                    strTen = drLT.GetValue(1)
                    ' Xây dựng mảng Mục, Tiểu mục của từng loại thuế
                    strSQL = "Select MA_MUC,MA_TMUC From TCS_XDINH_LTHUE " & _
                            "Where Ma_LThue = '" & strMa_Loaithue & "'"
                    ReDim Preserve garrLoaithue(i)      ' Mảng chứa loại thuế
                    ReDim Preserve garrLT_Chitieu(i)    ' Mảng chứa tiêu chí xác định loại thuế
                    garrLoaithue(i) = strMa_Loaithue
                    strMuc_Tmuc = ""
                    drXD = cnQXD.ExecuteDataReader(strSQL, CommandType.Text)
                    Do While drXD.Read
                        If drXD.IsDBNull(0) Then
                            strMa_Muc = ""
                        Else
                            strMa_Muc = drXD.GetValue(0)
                        End If
                        If drXD.IsDBNull(1) Then
                            strMa_TMuc = ""
                        Else
                            strMa_TMuc = drXD.GetValue(1)
                        End If
                        strMuc_Tmuc &= strMa_Muc & "-" & strMa_TMuc & ";"
                    Loop
                    ' Giải phóng đối tượng DataReader
                    drXD.Close()
                    ' Cat dau ;
                    If strMuc_Tmuc <> "" Then
                        strMuc_Tmuc = Mid(strMuc_Tmuc, 1, Len(strMuc_Tmuc) - 1)
                    End If
                    garrLT_Chitieu(i) = strMuc_Tmuc
                    i += 1
                Loop
                blnResult = True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin loại thuế")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình tìm kiếm loại thuế!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                blnResult = False
            Finally
                If Not drXD Is Nothing Then
                    drXD.Close()
                End If
                If Not cnQXD Is Nothing Then
                    cnQXD.Dispose()
                End If
                If Not drLT Is Nothing Then
                    drLT.Dispose()
                End If
                If Not cnLT Is Nothing Then
                    cnLT.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function bytTCS_GNT_Exist(ByVal strSoCT As String, ByVal strKHCT As String) As Byte
            '----------------------------------------------------------------
            ' Mục đích: Kiểm tra chứng từ đã tồn tại trong ngày làm việc hiện
            '           thời hay chưa? 
            ' Tham số: 
            ' Giá trị trả về:   0: Nếu chưa tồn tại
            '                   1: Đã tồn tại 
            '                   2: Lỗi khi query dữ liệu
            ' Ngày viết: 23/10/2007
            ' Người viết: Nguyễn Hữu Hoan
            ' ---------------------------------------------------------------
            Dim cnCT As New DataAccess
            Dim drCT As IDataReader
            Dim strSQL As String
            Dim bytResult As Byte = 0

            Try
                strSQL = "Select * From TCS_CTU_HDR Where KyHieu_CT='" & strKHCT & "' And So_CT='" & strSoCT & "'"
                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                If drCT.Read Then
                    bytResult = 1
                Else
                    ' Số chứng từ này chưa được lập
                    bytResult = 0
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra chứng từ CT đã tồn tại chưa ? - " & ex.ToString)
                ' Lỗi do query dữ liệu
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình kiểm tra sự tồn tại của chứng từ")
                Throw ex
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình kiểm tra chứng từ CT đã tồn tại chưa!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                bytResult = 2
            Finally
                If Not drCT Is Nothing Then
                    drCT.Dispose()
                End If
                If Not cnCT Is Nothing Then
                    cnCT.Dispose()
                End If
            End Try
            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function
        Public Function SelectCT_HDR_All() As DataSet
            ' ****************************************************************************
            ' Mục đích: lấy ra tất cả các chứng từ trong bảng TCS_CTU_HDR
            ' Người viết: HoanNH
            ' Ngày viết: 16/10/2007
            '*****************************************************************************
            Dim dsCT As New DataSet
            Dim strSql As String
            Dim conn As New DataAccess

            Try
                strSql = "Select SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,So_BThu,KyHieu_CT," & _
                         "So_CT,So_CT_NH,Ma_NNTien,Ten_NNTien,DC_NNTien,Ma_NNThue,Ten_NNThue,DC_NNThue," & _
                         "Ly_Do,Ma_KS,Ma_TQ,So_QD,Ngay_QD,CQ_QD,Ngay_CT,Ngay_HT,Ma_CQThu,XA_ID,Ma_Tinh," & _
                         "Ma_Huyen,Ma_Xa,TK_No,TK_Co,So_TK,Ngay_TK,LH_XNK,DVSDNS,TG_ID,Ma_NT,Ty_Gia," & _
                         "So_Khung,So_May,TK_KH_KB,Ten_KB,TK_KH_NH,Ten_NH,TTien,TT_TThu,Lan_In,Trang_Thai " & _
                         "From TCS_CTU_HDR " & _
                         "Where Trang_Thai <> '04'"
                dsCT = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return dsCT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy dữ liệu bảng TCS_CTU_HDR")
                Throw ex
                'LogDebug.Writelog("Lỗi trong quá trình lấy dữ liệu bảng TCS_CTU_HDR: " & ex.ToString)
            Finally
                ' Giải phóng kết nối
                conn.Dispose()
            End Try
        End Function
        Public Function SelectCT_DTL(ByVal strSHKB As String, ByVal iNgay_KB As Integer, _
                                     ByVal iMa_NV As Integer, ByVal iSo_BT As Integer, _
                                     ByVal strMa_DThu As String) As DataSet
            ' ****************************************************************************
            ' Mục đích: lấy ra một chứng từ chi tiết trong bảng TCS_CTU_DTL với các điều kiện đầu vào
            ' Người viết: HoanNH
            ' Ngày viết: 16/10/2007
            '*****************************************************************************
            Dim conn As New DataAccess
            Dim dsCT As New DataSet
            Dim strSql As String
            Try
                strSql = " Select SHKB, Ngay_KB, Ma_NV, So_BT, Ma_DThu, CCH_ID, Ma_Cap," & _
                         " Ma_Chuong, LKH_ID, Ma_Loai, Ma_Khoan, MTM_ID, Ma_Muc, Ma_TMuc, Noi_Dung," & _
                         " DT_ID, Ma_TLDT, Ma_dp, Ky_Thue, SoTien, SoTien_NT " & _
                         " From TCS_CTU_DTL " & _
                         " Where (SHKB = '" & strSHKB & "') And (Ngay_KB = " & iNgay_KB & _
                         ") And (Ma_NV = " & iMa_NV & ") And (So_BT = " & iSo_BT & _
                         ") "
                dsCT = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return dsCT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin dữ liệu chứng từ")
                Throw ex
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu từ bảng TCS_CTU_DTL!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
            Finally
                conn.Dispose()
            End Try
        End Function

#End Region

#Region "Xử lý chứng từ"

        Public Function CTU_Exist(ByVal strKHCT As String, ByVal strSoCT As String) As Boolean
            Try
                Dim cnCT As DataAccess
                Dim drCT As IDataReader
                Try
                    cnCT = New DataAccess
                    Dim strSQL As String
                    strSQL = "select 1 from tcs_ctu_hdr " & _
                        "where (kyhieu_ct = '" & strKHCT & "') and (so_ct = '" & strSoCT & "')"

                    drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                    Dim blnReturn As Boolean = False
                    If (drCT.Read()) Then
                        blnReturn = True
                    End If
                    Return blnReturn
                Catch ex As Exception
                    log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi kiểm tra sự tồn tại của chứng từ")
                    Throw ex
                Finally
                    If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                    If (Not IsNothing(drCT)) Then
                        drCT.Close()
                        drCT.Dispose()
                    End If
                End Try
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi kiểm tra sự tồn tại của chứng từ")
                Throw ex
            End Try
        End Function
        Public Function CTU_Get_LanIn(ByVal key As KeyCTu) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select LAN_IN from tcs_ctu_hdr " & _
                        "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() & " and " & _
                       " so_ct = '" & key.So_CT.ToString() & "'"

                ' " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = ""
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If
                Return strLanIn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy số lần in của chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Function CTU_GetCashNote(ByVal key As KeyCTu) As String
            Dim cnChungTu As DataAccess
            Dim cnKS As New OracleConnect
            Dim strConn As String
            Dim strSQL As String
            Dim arrOut As ArrayList
            Dim p As IDbDataParameter
            Dim arrIn As IDataParameter()
            Dim arrInHT As IDataParameter()

            Try
                cnChungTu = New DataAccess

                ReDim arrIn(6)
                p = cnKS.GetParameter("p_shkb", ParameterDirection.Input, key.SHKB, DbType.String, 100)
                arrIn(0) = p
                p = cnKS.GetParameter("p_ngay_kb", ParameterDirection.Input, key.Ngay_KB, DbType.Int64)
                arrIn(1) = p
                p = cnKS.GetParameter("p_ma_nv", ParameterDirection.Input, key.Ma_NV, DbType.Int64)
                arrIn(2) = p
                p = cnKS.GetParameter("p_so_bt", ParameterDirection.Input, key.So_BT, DbType.Int64)
                arrIn(3) = p
                p = cnKS.GetParameter("p_ma_dthu", ParameterDirection.Input, key.Ma_Dthu, DbType.String, 100)
                arrIn(4) = p
                p = cnKS.GetParameter("p_so_ct", ParameterDirection.Input, key.So_CT, DbType.String, 100)
                arrIn(5) = p
                p = cnKS.GetParameter("p_cashNote", ParameterDirection.Output, Nothing, DbType.String, 500)
                arrIn(6) = p
                strSQL = "tcs_pck_traodoi.prc_get_cash_note"

                arrOut = cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn)
                Dim strMsg As String
                strMsg = arrOut.Item(0).ToString

                Dim cashDeno As String = strMsg.Replace("&&", "&")
                strSQL = "INSERT INTO TCS_LOG_CASHNOTE(SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,SO_CT,MESSAGE) VALUES ('" & key.SHKB & "'," & key.Ngay_KB & ",'" & key.Ma_NV & "'," & key.So_BT & ",'" & key.Ma_Dthu & "','" & key.So_CT & "','" & cashDeno & "')"
                cnChungTu.ExecuteNonQuery(strSQL, CommandType.Text)

                Return strMsg
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong qua trình lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
        End Function

        Public Function CTU_Get_PTTT(ByVal key As KeyCTu) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select pt_tt from tcs_ctu_hdr " & _
                        "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() & " and " & _
                        " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strPT_TT As String = ""
                If (drCT.Read()) Then
                    strPT_TT = drCT(0).ToString()
                End If
                Return strPT_TT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function

        Public Function CTU_Get_TrangThai(ByVal key As KeyCTu) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select trang_thai from tcs_ctu_hdr " & _
                        "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() '& " and " '& _
                '" ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                'drCT1 = cnCT.ExecuteSQLScalar(strSQL)
                Dim strLanIn As String = cnCT.ExecuteSQLScalar(strSQL)
                'If (drCT.Read()) Then
                '    strLanIn = drCT(0).ToString()
                'End If
                Return strLanIn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Function CTU_Get_TrangThai_BL(ByVal key As KeyCTu) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select trang_thai from tcs_baolanh_hdr " & _
                        "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_ct = '" & key.So_CT.ToString() & "'"
                ''& " and " '& _

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = ""
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If
                Return strLanIn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ bảo lãnh")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        'Public Function CTU_get_TThai(ByVal strSoCT As String) As String
        '    Dim connectDB As DataAccess
        '    Dim dataReader As IDataReader
        '    Try
        '        connectDB = New DataAccess
        '        Dim sql As String
        '        sql = "select trang_thai from tcs_ctu_hdr where so_ct='" & strSoCT & "'"
        '        dataReader = connectDB.ExecuteDataReader(sql, CommandType.Text)
        ''        Dim strReturn As String
        '        If (dataReader.Read()) Then
        '            strReturn = dataReader(0).ToString()
        '        End If
        '        Return strReturn
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If (Not IsNothing(dataReader)) Then dataReader.Dispose()
        '        If (Not IsNothing(dataReader)) Then
        '            dataReader.Close()
        '            dataReader.Dispose()
        '        End If
        '    End Try

        'End Function

        Public Function CTU_checkExist_bySoCT(ByVal strSoCT As String) As Boolean
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select so_ct from tcs_baolanh_hdr " & _
                        "where so_ct = '" & strSoCT & "' "

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)

                If (drCT.Read()) Then
                    Return True
                End If
                Return False
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi kiểm tra sự tồn tại của số chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                '  Return False
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Function CTU_checkExist_SoBL(ByVal strSoBL As String) As Boolean
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select so_ct from tcs_baolanh_hdr " & _
                        "where so_bl = '" & strSoBL & "' and (trang_thai='01' or trang_thai='02') "

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)

                If (drCT.Read()) Then
                    Return True
                End If
                Return False
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi kiểm tra sự tồn tại của số chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                '  Return False
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Function CTU_checkExist_SoTK(ByVal strSoTK As String) As Boolean
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select so_ct from tcs_baolanh_hdr " & _
                        "where so_tk = '" & strSoTK & "' and trang_thai='01'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)

                If (drCT.Read()) Then
                    Return True
                End If
                Return False
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi kiểm tra sự tồn tại của số chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                '  Return False
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        'Public Function CTU_Get_TrangThai(ByVal key As KeyCTu) As String
        '    Dim cnCT As DataAccess
        '    Dim drCT As IDataReader
        '    Try
        '        cnCT = New DataAccess
        '        Dim strSQL As String
        '        strSQL = "select trang_thai from tcs_ctu_hdr " & _
        '                "where shkb = '" & key.SHKB.ToString() & "' and " & _
        '                " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
        '                " ma_nv = " & key.Ma_NV.ToString() & " and " & _
        '                " so_bt = " & key.So_BT.ToString() & " and " & _
        '                " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

        '        drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
        '        Dim strLanIn As String = ""
        '        If (drCT.Read()) Then
        '            strLanIn = drCT(0).ToString()
        '        End If
        '        Return strLanIn
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        '        If (Not IsNothing(drCT)) Then
        '            drCT.Close()
        '            drCT.Dispose()
        '        End If
        '    End Try
        'End Function


        Public Sub CTU_Update_LanIn(ByVal key As KeyCTu)
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select LAN_IN from tcs_ctu_hdr " & _
                                "where shkb = '" & key.SHKB.ToString() & "' and " & _
                                " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                                " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                                " so_bt = " & key.So_BT.ToString() & " and " & _
                       " so_ct = '" & key.So_CT.ToString() & "'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = "0"
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If

                If (Not IsNumeric(strLanIn)) Then strLanIn = "0"

                Dim intSoLanIn As Integer = Convert.ToInt16(strLanIn)
                intSoLanIn += 1

                cnCT = New DataAccess
                strSQL = "update tcs_ctu_hdr set LAN_IN = " & intSoLanIn.ToString() & " " & _
                        "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() & " and " & _
                       " so_ct = '" & key.So_CT.ToString() & "'"
                '" ma_dthu = '" & key.Ma_Dthu.ToString() & "' "
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi cập nhật lần in")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Sub

        Public Function CTU_Huy(ByVal key As KeyCTu, ByVal blnKTKB As Boolean, ByVal strLyDoHUy As String) As String
            '-----------------------------------------------------
            ' Mục đích: Hủy chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess

            Try
                cnCT = New DataAccess
                Dim strSQL As String

                If Not blnKTKB Then
                    strSQL = "update TCS_CTU_HDR " & _
                        " set TRANG_THAI = '04',so_bt_ktkb=0,NGAY_GDV_HUY= sysdate,LY_DO_HUY = '" & strLyDoHUy & "' " & _
                        " where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() ' & " and " & _
                    '" ma_dthu = '" & key.Ma_Dthu.ToString() & "' "


                    ' Trường hợp chưa chuyển sang KTKB                
                    cnCT.ExecuteNonQuery(strSQL, CommandType.Text)

                    'ICB: không cho khôi phục trạng thái của biên lai
                    'Hoàng Văn Anh

                    ' Cap nhat trang thai cho bien lai neu CT duoc lap tu bien lai
                    strSQL = "Update TCS_CTBL set Trang_Thai='00', Kyhieu_CT='',so_CT='' " & _
                            "where kyhieu_ct = '" & key.Kyhieu_CT.ToString() & "' and " & _
                            " so_ct = '" & key.So_CT.ToString() & "' "
                    cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
                Else
                    Dim hdr As infChungTuHDR = buChungTu.CTU_Header(key)
                    Dim strError As String
                    ' HuyKTKBCTU(hdr.SHKB, hdr.Ngay_KB, hdr.Ma_NV, hdr.So_BT, hdr.Ma_DThu, hdr.So_BT_KTKB, hdr.Ma_KS, strError)

                    Return strError
                End If

                Return "OK"
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi hủy chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_Huy_Da_KS(ByVal key As KeyCTu, ByVal blnKTKB As Boolean, ByVal strLyDoHUy As String) As String
            '-----------------------------------------------------
            ' Mục đích: Hủy chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess

            Try
                cnCT = New DataAccess
                Dim strSQL As String

                If Not blnKTKB Then
                    strSQL = "update TCS_CTU_HDR " & _
                        " set TRANG_THAI = '02',so_bt_ktkb=0,NGAY_GDV_HUY= sysdate,LY_DO_HUY = '" & strLyDoHUy & "' " & _
                        " where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() ' & " and " & _
                    '" ma_dthu = '" & key.Ma_Dthu.ToString() & "' "


                    ' Trường hợp chưa chuyển sang KTKB                
                    cnCT.ExecuteNonQuery(strSQL, CommandType.Text)

                    'ICB: không cho khôi phục trạng thái của biên lai
                    'Hoàng Văn Anh

                    ' Cap nhat trang thai cho bien lai neu CT duoc lap tu bien lai
                    strSQL = "Update TCS_CTBL set Trang_Thai='00', Kyhieu_CT='',so_CT='' " & _
                            "where kyhieu_ct = '" & key.Kyhieu_CT.ToString() & "' and " & _
                            " so_ct = '" & key.So_CT.ToString() & "' "
                    cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
                Else
                    Dim hdr As infChungTuHDR = buChungTu.CTU_Header(key)
                    Dim strError As String
                    ' HuyKTKBCTU(hdr.SHKB, hdr.Ngay_KB, hdr.Ma_NV, hdr.So_BT, hdr.Ma_DThu, hdr.So_BT_KTKB, hdr.Ma_KS, strError)

                    Return strError
                End If

                Return "OK"
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi hủy chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_ChuyenKS(ByVal key As KeyCTu, ByVal blnKTKB As Boolean) As String
            '-----------------------------------------------------
            ' Mục đích: Chuyển chứng từ sang trạng thái chờ kiểm soát.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess

            Try
                cnCT = New DataAccess
                Dim strSQL As String

                If Not blnKTKB Then
                    strSQL = "update TCS_CTU_HDR " & _
                        " set TRANG_THAI = '05',so_bt_ktkb=0 " & _
                        " where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() '& " and " & _
                    '  " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "


                    ' Trường hợp chưa chuyển sang KTKB                
                    cnCT.ExecuteNonQuery(strSQL, CommandType.Text)


                Else
                    Dim hdr As infChungTuHDR = buChungTu.CTU_Header(key)
                    Dim strError As String
                    ' HuyKTKBCTU(hdr.SHKB, hdr.Ngay_KB, hdr.Ma_NV, hdr.So_BT, hdr.Ma_DThu, hdr.So_BT_KTKB, hdr.Ma_KS, strError)
                    Return strError
                End If

                Return "OK"
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi chuyển kiểm soát chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function


        Public Function CTU_Huy_BL(ByVal key As KeyCTu, ByVal blnKTKB As Boolean) As String
            '-----------------------------------------------------
            ' Mục đích: Hủy chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess

            Try

                cnCT = New DataAccess
                Dim strSQL As String
                Dim strError As String = "OK"

                If Not blnKTKB Then
                    strSQL = "update  TCS_CTU_THOP_HDR  " & _
                       "set TRANG_THAI = '04', so_bt_ktkb = 0 " & _
                       "where shkb = '" & key.SHKB.ToString() & "' and " & _
                       " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                       " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                       " so_bt = " & key.So_BT.ToString() & " and " & _
                       " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                    ' Trường hợp chưa chuyển sang KTKB                
                    cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
                Else
                    '  Dim hdr As infChungTuHDR = buChungTu.CTU_Header(key, blnTHOP)
                    '   HuyKTKBCTU(hdr.SHKB, hdr.Ngay_KB, hdr.Ma_NV, hdr.So_BT, hdr.Ma_DThu, hdr.So_BT_KTKB, hdr.Ma_KS, strError, blnTHOP)
                End If

                ' Cap nhat trang thai cho bien lai neu CT duoc lap tu bien lai
                strSQL = "Update TCS_CTBL set Trang_Thai='00', Kyhieu_CT='',so_CT='' " & _
                        "where kyhieu_ct = '" & key.Kyhieu_CT & "' and " & _
                        " so_ct = '" & key.So_CT & "' "
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)

                Return strError
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi hủy chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_Huy(ByVal strKHCT As String, ByVal strSoCT As String) As String
            '-----------------------------------------------------
            ' Mục đích: Hủy chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess

            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "update TCS_CTU_HDR " & _
                       "set TRANG_THAI = '04',so_bt_ktkb=0 " & _
                      "where kyhieu_ct = '" & strKHCT & "' and " & _
                            " so_ct = '" & strSoCT & "' "
                ' Trường hợp chưa chuyển sang KTKB                
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)

                ' Cap nhat trang thai cho bien lai neu CT duoc lap tu bien lai
                strSQL = "Update TCS_CTBL set Trang_Thai='00', Kyhieu_CT='',so_CT='' " & _
                        "where kyhieu_ct = '" & strKHCT & "' and " & _
                        " so_ct = '" & strSoCT & "' "

                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
                Return "OK"
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi hủy chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Sub CTU_Update_TTSP(ByVal strSO_CT As String, ByVal strTTSP As String)
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                Dim strSQL As String = ""
                cnCT = New DataAccess
                strSQL = "update tcs_ctu_hdr set TT_SP = '" & strTTSP & "' " & _
                        " where  so_ct = '" & strSO_CT & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi cập nhật lần in")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Sub
        Public Sub CTU_KhoiPhuc(ByVal key As KeyCTu, Optional ByVal blnKiemSoat As Boolean = False)
            '-----------------------------------------------------
            ' Mục đích: Khôi phục chứng từ đã hủy về trạng thái chưa kiểm soát.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                Select Case blnKiemSoat
                    Case False      'Đưa về trạng thái chưa kiểm soát
                        strSQL = "update TCS_CTU_HDR " & _
                                " set TRANG_THAI = '00', " & _
                                " MA_KS = 0, Lan_In = 0 " & _
                                " where shkb = '" & key.SHKB.ToString() & "' and " & _
                                " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                                " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                                " so_bt = " & key.So_BT.ToString() '& " and " & _
                        ' " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                    Case True       'Đưa về trạng thái đã kiểm soát (giữ nguyên thông tin kiểm soát)
                        strSQL = "update TCS_CTU_HDR " & _
                                " set TRANG_THAI = '01', " & _
                                " Lan_In = 0 " & _
                                " where shkb = '" & key.SHKB.ToString() & "' and " & _
                                " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                                " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                                " so_bt = " & key.So_BT.ToString() & " and " & _
                                " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "
                End Select

                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khôi phục chứng từ đã hủy về trạng thái chưa kiểm soát")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub

        'ICB: kiểm tra xem chứng từ này đã nhập số thực thu chưa?
        'Hoàng Văn Anh

        Public Function Is_DaNhap_STThu(ByVal key As KeyCTu) As Boolean
            Dim KetNoi As DataAccess
            Dim bResult As Boolean = False
            Try
                KetNoi = New DataAccess
                Dim strSQL As String

                strSQL = "SELECT * FROM TCS_CTU_HDR" _
                    & " WHERE (ma_tq=0) AND (TT_Tthu=0)" _
                    & " AND(Trang_Thai <>'04') " _
                    & " AND shkb =" & Globals.EscapeQuote(key.SHKB.ToString()) _
                    & " AND ngay_kb = " & key.Ngay_KB.ToString() _
                    & " AND ma_nv = " & key.Ma_NV.ToString() _
                    & " AND so_bt = " & key.So_BT.ToString() _
                    & " AND ma_dthu =" & Globals.EscapeQuote(key.Ma_Dthu.ToString()) _
                    & " AND PT_TT='00'"



                'strSQL = "SELECT * FROM tcs_khoquy_hdr " & _
                '    " WHERE shkb =" & Globals.EscapeQuote(key.SHKB.ToString()) & _
                '    " AND ngay_kb = " & key.Ngay_KB.ToString() & _
                '    " AND ma_nv = " & key.Ma_NV.ToString() & _
                '    " AND so_bt = " & key.So_BT.ToString() & _
                '    " AND ma_dthu =" & Globals.EscapeQuote(key.Ma_Dthu.ToString())
                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If IsEmptyDataSet(ds) Then
                    bResult = True
                Else
                    bResult = False
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình kiểm tra mã thực thu")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(KetNoi)) Then
                    KetNoi.Dispose()
                End If
            End Try
            Return bResult
        End Function

        Public Function CTu_SetTrangThai_HDR(ByVal soCtu As String, ByVal trangThai As String) As Boolean
            Dim connect As DataAccess
            Try
                connect = New DataAccess
                Dim sqlStr As String
                sqlStr = "update TCS_CTU_HDR set TRANG_THAI = '" & trangThai & "' where soCtu='" & soCtu & "'"
                connect.ExecuteNonQuery(sqlStr, CommandType.Text)
                Return True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                Throw ex
                Return False
            End Try
        End Function

        Public Sub CTU_SetTrangThai(ByVal key As KeyCTu, ByVal strTT As String, Optional ByVal strTKHT As String = "", _
                                        Optional ByVal strRefNo As String = "", Optional ByVal strSeqNo As String = "", _
                                        Optional ByVal strMaKS As String = "", Optional ByVal strMaHuyKS As String = "", _
                                        Optional ByVal strRMNo As String = "", Optional ByVal strTT_CThue As String = "0", _
                                        Optional ByVal strLyDoHuy As String = "", Optional ByVal strNgayKSVCT As Boolean = False)
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strNgayKS As String
                strSQL = "update TCS_CTU_HDR " & _
                    "   set TRANG_THAI = '" & strTT & "'" & _
                    IIf(strRefNo.Length > 0, ",ref_no = '" & strRefNo & "'", "") & _
                    IIf(strSeqNo.Length > 0, ",seq_no = '" & strSeqNo & "'", "") & _
                    IIf(strMaKS.Length > 0, ",Ma_KS = '" & strMaKS & "'", "") & _
                    IIf(strMaKS.Length > 0, ",ngay_ks=SYSDATE" & "", "") & _
                    IIf(strMaHuyKS.Length > 0, ",Ma_HuyKS = '" & strMaHuyKS & "', NGAY_KSV_HUY=SYSDATE ", "") & _
                    IIf(strRMNo.Length > 0, ",RM_REF_NO = '" & strRMNo & "'", "") & _
                    IIf(strTT_CThue.Length > 0, ",TT_CTHUE = '" & strTT_CThue & "'", "") & _
                    IIf(strTKHT.Length > 0, ",tkht = '" & strTKHT & "'", "") & _
                    IIf(strLyDoHuy.Length > 0, ", ly_do = '" & strLyDoHuy & "'", "") & _
                    IIf(strTKHT.Length > 0, ", so_ct_nh = '" & strTKHT & "'", "") & _
                    IIf(strNgayKSVCT = True, ", ngay_ksv_ct = SYSDATE", " ") & _
                    "   where shkb = '" & key.SHKB.ToString() & "' and " & _
                    "   ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                    "   ma_nv = " & key.Ma_NV.ToString() & " and " & _
                    "   so_bt = " & key.So_BT.ToString() & " and " & _
                 " so_ct = '" & key.So_CT.ToString() & "' "

                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi set trạng thái chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub

        Public Sub CTU_DTL_Update(ByVal dtl As infChungTuDTL)
            '-----------------------------------------------------
            ' Mục đích: Cập nhật thông tin chi tiết chứng từ.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 31/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------   
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "update TCS_CTU_DTL set " & _
                    "CCH_ID = " & dtl.CCH_ID & ", " & _
                    "MA_CAP = '" & dtl.Ma_Cap & "', " & _
                    "MA_CHUONG = '" & dtl.Ma_Chuong & "', " & _
                    "LKH_ID = " & dtl.LKH_ID & ", " & _
                    "MA_LOAI = '" & dtl.Ma_Loai & "', " & _
                    "MA_KHOAN = '" & dtl.Ma_Khoan & "', " & _
                    "MTM_ID = " & dtl.MTM_ID & ", " & _
                    "MA_MUC = '" & dtl.Ma_Muc & "', " & _
                    "MA_TMUC = '" & dtl.Ma_TMuc & "', " & _
                    "DT_ID = " & dtl.DT_ID & ", " & _
                    "MA_TLDT = '" & dtl.Ma_TLDT & "', " & _
                    "NOI_DUNG = '" & dtl.Noi_Dung & "', " & _
                    "KY_THUE = '" & dtl.Ky_Thue & "' " & _
                    "where ID = " & dtl.ID
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi cập nhật thông tin chi tiết chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub

        Public Sub CTU_HDR_Update(ByVal hdr As infChungTuHDR)
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR
            ' Đầu vào: đối tượng HDR
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 09/11/2007
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction

                'Cập nhật thông tin HDR của chứng từ
                strSql = "Update TCS_CTU_HDR SET " & _
                         "So_BThu=" & hdr.So_BThu & ",So_CT_NH='" & hdr.So_CT_NH & "'," & _
                         "Ma_NNTien='" & hdr.Ma_NNTien & " ',Ten_NNTien='" & hdr.Ten_NNTien & "'," & _
                         "DC_NNTien='" & hdr.DC_NNTien & "',Ma_NNThue='" & hdr.Ma_NNThue & "'," & _
                         "Ten_NNThue='" & hdr.Ten_NNThue & "',DC_NNThue='" & hdr.DC_NNThue & "'," & _
                         "Ly_do='" & hdr.Ly_Do & "',Ma_KS=0,Ma_TQ=0,So_QD='" & hdr.So_QD & "'," & _
                         "Ngay_QD=" & hdr.Ngay_QD & ",CQ_QD='" & hdr.CQ_QD & "',Ngay_CT=" & hdr.Ngay_CT & "," & _
                         "Ngay_HT=" & hdr.Ngay_HT & ",Ma_CQThu='" & hdr.Ma_CQThu & "'," & _
                         "XA_ID=" & hdr.XA_ID & ",Ma_Tinh='" & hdr.Ma_Tinh & "',Ma_Huyen='" & hdr.Ma_Huyen & "'," & _
                         "Ma_Xa='" & hdr.Ma_Xa & "',TK_No='" & hdr.TK_No & "',TK_Co='" & hdr.TK_Co & "'," & _
                         "So_TK='" & hdr.So_TK & "',Ngay_TK=" & hdr.Ngay_TK & ",LH_XNK='" & hdr.LH_XNK & "'," & _
                         "DVSDNS='" & hdr.DVSDNS & "',TEN_DVSDNS = '" & hdr.Ten_DVSDNS & "'," & _
                         "Ma_NT='" & hdr.Ma_NT & "',Ty_Gia=" & hdr.Ty_Gia & "," & _
                         "TG_ID = " & hdr.TG_ID & ",MA_LTHUE = '" & hdr.Ma_LThue & "'," & _
                         "So_Khung='" & hdr.So_Khung & "',So_May='" & hdr.So_May & "'," & _
                         "TK_KH_NH='" & hdr.TK_KH_NH & "',NGAY_KH_NH=" & hdr.NGAY_KH_NH & "," & _
                         "MA_NH_A='" & hdr.MA_NH_A & "',MA_NH_B='" & hdr.MA_NH_B & "'," & _
                         "TTien=" & hdr.TTien & ", TT_TThu=0, Lan_In=0, Trang_Thai='00', " & _
                         "TK_KH_Nhan='" & hdr.TK_KH_Nhan & "', Ten_KH_Nhan='" & hdr.Ten_KH_Nhan & "'," & _
                         "Diachi_KH_Nhan='" & hdr.Diachi_KH_Nhan & "', TTien_NT=" & hdr.TTien_NT & _
                         " Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
                         ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
                         ")" ' And (Ma_DThu = '" & hdr.Ma_DThu & "')"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi insert dữ liệu chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình insert dữ liệu chứng từ!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub

        Public Sub CTU_Update(ByVal hdr As infChungTuHDR, ByVal dtl As infChungTuDTL())
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR
            ' Đầu vào: đối tượng HDR
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 09/11/2007
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction

                'Cập nhật thông tin HDR của chứng từ
                strSql = "Update TCS_CTU_HDR SET " & _
                         "So_BThu=" & hdr.So_BThu & ",So_CT_NH='" & hdr.So_CT_NH & "'," & _
                         "Ma_NNTien='" & hdr.Ma_NNTien & " ',Ten_NNTien='" & hdr.Ten_NNTien & "'," & _
                         "DC_NNTien='" & hdr.DC_NNTien & "',Ma_NNThue='" & hdr.Ma_NNThue & "'," & _
                         "Ten_NNThue='" & hdr.Ten_NNThue & "',DC_NNThue='" & hdr.DC_NNThue & "'," & _
                         "Ly_do='" & hdr.Ly_Do & "',Ma_KS=0,Ma_TQ=0,So_QD='" & hdr.So_QD & "'," & _
                         "Ngay_QD=" & hdr.Ngay_QD & ",CQ_QD='" & hdr.CQ_QD & "',Ngay_CT=" & hdr.Ngay_CT & "," & _
                         "Ngay_HT=" & hdr.Ngay_HT & ",Ma_CQThu='" & hdr.Ma_CQThu & "'," & _
                         "XA_ID=" & hdr.XA_ID & ",Ma_Tinh='" & hdr.Ma_Tinh & "',Ma_Huyen='" & hdr.Ma_Huyen & "'," & _
                         "Ma_Xa='" & hdr.Ma_Xa & "',TK_No='" & hdr.TK_No & "',TK_Co='" & hdr.TK_Co & "'," & _
                         "So_TK='" & hdr.So_TK & "',Ngay_TK=" & hdr.Ngay_TK & ",LH_XNK='" & hdr.LH_XNK & "'," & _
                         "DVSDNS='" & hdr.DVSDNS & "',TEN_DVSDNS = '" & hdr.Ten_DVSDNS & "'," & _
                         "Ma_NT='" & hdr.Ma_NT & "',Ty_Gia=" & hdr.Ty_Gia & "," & _
                         "TG_ID = " & hdr.TG_ID & ",MA_LTHUE = '" & hdr.Ma_LThue & "'," & _
                         "So_Khung='" & hdr.So_Khung & "',So_May='" & hdr.So_May & "'," & _
                         "TK_KH_NH='" & hdr.TK_KH_NH & "',NGAY_KH_NH=" & hdr.NGAY_KH_NH & "," & _
                         "MA_NH_A='" & hdr.MA_NH_A & "',MA_NH_B='" & hdr.MA_NH_B & "'," & _
                         "TTien=" & hdr.TTien & ", TT_TThu=0, Lan_In=0, Trang_Thai='00', " & _
                         "IS=" & hdr.TTien & ", TT_TThu=0, Lan_In=0, Trang_Thai='00', " & _
                         "TK_KH_Nhan='" & hdr.TK_KH_Nhan & "', Ten_KH_Nhan='" & hdr.Ten_KH_Nhan & "'," & _
                         "Diachi_KH_Nhan='" & hdr.Diachi_KH_Nhan & "', TTien_NT=" & hdr.TTien_NT & _
                         " Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
                         ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
                         ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)


                For Each objDTL In dtl
                    strSql = "update TCS_CTU_DTL set " & _
                            "CCH_ID = " & objDTL.CCH_ID & ", " & _
                            "MA_CAP = '" & objDTL.Ma_Cap & "', " & _
                            "MA_CHUONG = '" & objDTL.Ma_Chuong & "', " & _
                            "LKH_ID = " & objDTL.LKH_ID & ", " & _
                            "MA_LOAI = '" & objDTL.Ma_Loai & "', " & _
                            "MA_KHOAN = '" & objDTL.Ma_Khoan & "', " & _
                            "MTM_ID = " & objDTL.MTM_ID & ", " & _
                            "MA_MUC = '" & objDTL.Ma_Muc & "', " & _
                            "MA_TMUC = '" & objDTL.Ma_TMuc & "', " & _
                            "DT_ID = " & objDTL.DT_ID & ", " & _
                            "MA_TLDT = '" & objDTL.Ma_TLDT & "', " & _
                            "NOI_DUNG = '" & objDTL.Noi_Dung & "', " & _
                            "KY_THUE = '" & objDTL.Ky_Thue & "' " & _
                            "where ID = " & objDTL.ID
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                Next

                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi insert dữ liệu chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình insert dữ liệu chứng từ!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub

        Public Sub CTU_Update_Search(ByVal hdr As infChungTuHDR, ByVal dtl As infChungTuDTL())
            ' ****************************************************************************
            ' Mục đích: Cập nhật 1 số thông tin cho phép chỉnh sửa trong chức năng tra cứu
            ' Đầu vào: đối tượng HDR, dãy dtl
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 09/11/2007
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction

                'Cập nhật thông tin HDR của chứng từ
                Dim strTT As String = "00"

                'If (gbytKSCT = 0) Then
                '    strTT = "01"
                'End If

                strSql = "Update TCS_CTU_HDR SET " & _
                         "Ma_CQThu='" & hdr.Ma_CQThu & "'," & _
                         "XA_ID=" & hdr.XA_ID & ",Ma_Tinh='" & hdr.Ma_Tinh & "',Ma_Huyen='" & hdr.Ma_Huyen & "'," & _
                         "Ma_Xa='" & hdr.Ma_Xa & "',TK_No='" & hdr.TK_No & "',TK_Co='" & hdr.TK_Co & "', " & _
                         "Trang_Thai = '" & strTT & "' " & _
                         " Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
                         ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
                         ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                For Each objDTL In dtl
                    strSql = "Update TCS_CTU_DTL set " & _
                            "CCH_ID = " & objDTL.CCH_ID & ", " & _
                            "MA_CAP = '" & objDTL.Ma_Cap & "', " & _
                            "MA_CHUONG = '" & objDTL.Ma_Chuong & "', " & _
                            "LKH_ID = " & objDTL.LKH_ID & ", " & _
                            "MA_LOAI = '" & objDTL.Ma_Loai & "', " & _
                            "MA_KHOAN = '" & objDTL.Ma_Khoan & "', " & _
                            "MTM_ID = " & objDTL.MTM_ID & ", " & _
                            "MA_MUC = '" & objDTL.Ma_Muc & "', " & _
                            "MA_TMUC = '" & objDTL.Ma_TMuc & "', " & _
                            "DT_ID = " & objDTL.DT_ID & ", " & _
                            "MA_TLDT = '" & objDTL.Ma_TLDT & "', " & _
                            "NOI_DUNG = '" & objDTL.Noi_Dung & "', " & _
                            "KY_THUE = '" & objDTL.Ky_Thue & "' " & _
                            "where ID = " & objDTL.ID
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                Next

                If (strTT = "01") Then
                    strSql = "Update TCS_CTU_THOP_HDR SET " & _
                         "Ma_CQThu='" & hdr.Ma_CQThu & "'," & _
                         "XA_ID=" & hdr.XA_ID & ",Ma_Tinh='" & hdr.Ma_Tinh & "',Ma_Huyen='" & hdr.Ma_Huyen & "'," & _
                         "Ma_Xa='" & hdr.Ma_Xa & "',TK_No='" & hdr.TK_No & "',TK_Co='" & hdr.TK_Co & "' " & _
                         " Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
                         ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
                         ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                    For Each objDTL In dtl
                        strSql = "Update TCS_CTU_THOP_DTL set " & _
                                "CCH_ID = " & objDTL.CCH_ID & ", " & _
                                "MA_CAP = '" & objDTL.Ma_Cap & "', " & _
                                "MA_CHUONG = '" & objDTL.Ma_Chuong & "', " & _
                                "LKH_ID = " & objDTL.LKH_ID & ", " & _
                                "MA_LOAI = '" & objDTL.Ma_Loai & "', " & _
                                "MA_KHOAN = '" & objDTL.Ma_Khoan & "', " & _
                                "MTM_ID = " & objDTL.MTM_ID & ", " & _
                                "MA_MUC = '" & objDTL.Ma_Muc & "', " & _
                                "MA_TMUC = '" & objDTL.Ma_TMuc & "', " & _
                                "DT_ID = " & objDTL.DT_ID & ", " & _
                                "MA_TLDT = '" & objDTL.Ma_TLDT & "', " & _
                                "NOI_DUNG = '" & objDTL.Noi_Dung & "', " & _
                                "KY_THUE = '" & objDTL.Ky_Thue & "' " & _
                                "where ID = " & objDTL.ID
                        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                    Next
                End If
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi cập nhật thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình insert dữ liệu chứng từ!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
#End Region

#Region "Danh sách chứng từ"
        Public Function CTU_TraCuu(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Tra cứu chứng từ.
            ' Tham số: điều kiện tra cứu
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select distinct hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BTHU, hdr.TTIEN, hdr.TRANG_THAI, hdr.SO_BT, " & _
                            "hdr.Ma_NNThue, hdr.Ten_NNThue, hdr.MA_NV, hdr.Lan_In, hdr.ngay_kb, hdr.SHKB, hdr.MA_DTHU " & _
                            " from TCS_CTU_HDR hdr, TCS_CTU_DTL dtl " & _
                            "where (hdr.shkb = dtl.shkb) and (hdr.ngay_kb = dtl.ngay_kb) and (hdr.ma_nv = dtl.ma_nv) and " & _
                            "(hdr.so_bt = dtl.so_bt) and (hdr.ma_dthu = dtl.ma_dthu) " & _
                            strWhere & " Order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt asc"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi tra cứu chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_GetListKiemSoat_Set(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, _
            Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "") As DataSet
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select SHKB, KYHIEU_CT, SO_CT, SO_BT, TRANG_THAI, SO_BTHU, TTIEN, TT_TTHU, Lan_In, MA_NV, NGAY_KB, MA_DTHU, TT_CTHUE, MA_NNTHUE " & _
                    " from TCS_CTU_HDR " & _
                    " where NGAY_KB = " & strNgayCT & " "

                If (strTrangThai.Trim <> "") Then
                    strSQL = strSQL & " and Trang_thai='" & strTrangThai & "'"
                End If
                If Not "".Equals(strMaNV) Then
                    strSQL = strSQL & " and (Ma_NV=" & strMaNV & ") "
                End If
                If Not "".Equals(strSoBT) Then
                    strSQL = strSQL & " and (So_BT=" & strSoBT & ") "
                End If
                If Not "".Equals(strMaKS) Then
                    strSQL = strSQL & " and ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMaKS & "')) "
                End If

                'Kiênvt: Trong trường hợp thu hộ
                If strMaDiemThu.Length > 0 Then
                    strSQL &= " AND (MA_DTHU=" & strMaDiemThu & ") "
                End If

                strSQL = strSQL & " order by ma_nv, so_bt DESC"
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_GetListKiemSoat(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, ByVal strMaLThue As String, _
            Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "", Optional ByVal strKenhCT As String = "") As DataSet
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strWhereLoaiThue As String = ""
                If strMaLThue = "04" Then
                    strWhereLoaiThue = " AND MA_LTHUE = '04' "
                ElseIf Not strMaLThue = "" Then
                    strWhereLoaiThue = " AND MA_LTHUE <> '04' "
                End If

                strSQL = " Select hdr.LOAI_CTU, hdr.SHKB,hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BT,hdr.TRANG_THAI, hdr.SO_BTHU, hdr.TTIEN, hdr.TT_TTHU, hdr.Lan_In, hdr.MA_NV, " & _
                " hdr.NGAY_KB, hdr.MA_DTHU, hdr.TT_CTHUE, hdr.MA_NNTHUE,nv.TEN_DN, hdr.so_ct_nh,hdr.ma_ks, hdr.save_type " & _
                         " From TCS_CTU_HDR hdr, tcs_dm_nhanvien nv " & _
                         " Where hdr.ma_nv=nv.ma_nv and nv.tinh_trang='0' and hdr.NGAY_KB = " & strNgayCT & strWhereLoaiThue
                ' and hdr.NGAY_KB = " & strNgayCT & strWhereLoaiThue

                If (strTrangThai.Trim <> "") Then
                    If strTrangThai = "04" Then
                        'strSQL = strSQL & " and hdr.Trang_thai='04' AND hdr.MA_KS=0 "
                        strSQL = strSQL & " and hdr.Trang_thai='04' "
                    ElseIf strTrangThai = "07" Then
                        'strSQL = strSQL & " and hdr.Trang_thai='04' AND hdr.MA_KS <> 0 AND hdr.tt_cthue = '0' "
                        strSQL = strSQL & " and hdr.Trang_thai='07' AND hdr.tt_cthue = '0' "
                    ElseIf strTrangThai = "06" Then
                        'strSQL = strSQL & " and hdr.Trang_thai='01' AND hdr.tt_cthue = '0' "
                        strSQL = strSQL & " and hdr.Trang_thai='06'  "
                    ElseIf strTrangThai = "08" Then
                        'strSQL = strSQL & " and hdr.Trang_thai='04' AND hdr.MA_KS <> 0  AND hdr.tt_cthue = '1' "
                        strSQL = strSQL & " and hdr.Trang_thai='08' AND hdr.tt_cthue = '1' "
                    ElseIf strTrangThai = "02" Then
                        'strSQL = strSQL & " and hdr.Trang_thai='02' AND hdr.MA_KS <> 0"
                        strSQL = strSQL & " and hdr.Trang_thai='02' "
                    ElseIf strTrangThai = "15" Then
                        'strSQL = strSQL & " and hdr.Trang_thai='02' AND hdr.MA_KS <> 0"
                        strSQL = strSQL & " and hdr.Trang_thai='15' "
                    Else
                        strSQL = strSQL & " and hdr.Trang_thai='" & strTrangThai & "'"
                    End If

                End If
                If Not "".Equals(strMaNV) Then
                    strSQL = strSQL & " and (hdr.Ma_NV=" & strMaNV & ") "
                End If
                If Not "".Equals(strSoBT) Then
                    strSQL = strSQL & " and (hdr.So_BT=" & strSoBT & ") "
                End If
                If Not "".Equals(strMaKS) Then
                    strSQL = strSQL & " and hdr.ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMaKS & "')) "
                End If

                'Kiênvt: Trong trường hợp thu hộ
                If strMaDiemThu.Length > 0 Then
                    strSQL &= " AND (hdr.MA_DTHU=" & strMaDiemThu & ") "
                End If

                'strKenhCT lay kenh chung tu cua ebank
                If Not "".Equals(strKenhCT) Then
                    strSQL = strSQL & " and (hdr.KENH_CT='" & strKenhCT & "' or hdr.Trang_thai='15') "
                End If

                strSQL = strSQL & " order by hdr.ma_nv, hdr.so_bt DESC"
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine & "Error StackTrace: " & ex.StackTrace & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_GetListKiemSoat_frm_HOME(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, ByVal strMaLThue As String, _
            Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "") As DataSet
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strWhereLoaiThue As String = ""
                If strMaLThue = "04" Then
                    strWhereLoaiThue = " AND MA_LTHUE = '04' "
                ElseIf Not strMaLThue = "" Then
                    strWhereLoaiThue = " AND MA_LTHUE <> '04' "
                End If

                strSQL = " Select hdr.SHKB,hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BT,hdr.TRANG_THAI, hdr.SO_BTHU, hdr.TTIEN, hdr.TT_TTHU, hdr.Lan_In, hdr.MA_NV, " & _
                " hdr.NGAY_KB, hdr.MA_DTHU, hdr.TT_CTHUE, hdr.MA_NNTHUE,nv.TEN_DN, hdr.so_ct_nh,hdr.ma_ks " & _
                         " From TCS_CTU_HDR hdr, tcs_dm_nhanvien nv " & _
                         " Where hdr.ma_nv=nv.ma_nv and nv.tinh_trang='0' and hdr.NGAY_KB = " & strNgayCT & strWhereLoaiThue


                strSQL = strSQL & " and hdr.Trang_thai IN ('02','05')"

                If Not "".Equals(strMaNV) Then
                    strSQL = strSQL & " and (hdr.Ma_NV=" & strMaNV & ") "
                End If
                If Not "".Equals(strSoBT) Then
                    strSQL = strSQL & " and (hdr.So_BT=" & strSoBT & ") "
                End If
                If Not "".Equals(strMaKS) Then
                    strSQL = strSQL & " and hdr.ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMaKS & "')) "
                End If

                'Kiênvt: Trong trường hợp thu hộ
                If strMaDiemThu.Length > 0 Then
                    strSQL &= " AND (hdr.MA_DTHU=" & strMaDiemThu & ") "
                End If

                strSQL = strSQL & " order by hdr.ma_nv, hdr.so_bt DESC"
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_GetListCT_Set(ByVal strNgayCT As String, ByVal strMaNV As String, ByVal strMaDiemThu As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh sách chứng từ trong ngày để kiểm soát
            ' Tham số: Ngày chứng từ, trạng thái chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                'strSQL = "select KYHIEU_CT, SO_CT, SO_BT,TRANG_THAI, So_BThu, TTien, Ma_NV, Lan_In, SHKB, NGAY_KB, MA_DTHU " & _
                '    "from TCS_CTU_HDR  " & _
                '    "where NGAY_KB = " & strNgayCT & _
                '    " and Ma_NV =" & strMaNV & _
                '    " order by so_bt desc "
                ' " and Ma_Dthu =" & strMaDiemThu & _
                strSQL = " select KYHIEU_CT, SO_CT, SO_BT,TRANG_THAI, So_BThu, TTien, Ma_NV, Lan_In, SHKB, NGAY_KB, MA_DTHU,tt_cthue " & _
                   " from TCS_CTU_HDR  " & _
                   " where NGAY_KB = " & strNgayCT & _
                   " and Ma_NV =" & strMaNV & _
                   " order by so_bt desc "

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_GetListCT(ByVal strNgayCT As String, ByVal strMaNV As String, ByVal strMaDiemThu As String, ByVal strMaLThue As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh sách chứng từ trong ngày để kiểm soát
            ' Tham số: Ngày chứng từ, trạng thái chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strWhereLoaiThue As String = ""
                If strMaLThue = "04" Then
                    strWhereLoaiThue = " and Ma_LThue = '04' "
                ElseIf Not strMaLThue = "" Then
                    strWhereLoaiThue = " and Ma_LThue <> '04' "
                End If
                '" where A.MA_NV=B.MA_NV AND B.TINH_TRANG='0' AND A.NGAY_KB = " & strNgayCT & _
                strSQL = " select KYHIEU_CT, SO_CT, SO_BT, SO_CT_NH, TRANG_THAI, So_BThu, TTien, A.Ma_NV, Lan_In, SHKB, NGAY_KB, MA_DTHU, tt_cthue,B.TEN_DN,a.Ma_KS  " & _
                       " from TCS_CTU_HDR A, TCS_DM_NHANVIEN B  " & _
                        " where A.MA_NV=B.MA_NV AND B.TINH_TRANG='0' " & _
                       " and A.Ma_NV =" & strMaNV & _
                       strWhereLoaiThue & _
                       " order by so_bt desc "

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

#End Region

#Region "Chi tiết chứng từ"
        Public Function CTU_Header(ByVal key As KeyCTu) As infChungTuHDR
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chung của chứng từ
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Dim hdr As New infChungTuHDR
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV, a.Ly_do_huy, " & _
                               "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                               "a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
                               "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                               "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
                               "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
                               "a.Ngay_QD,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
                               "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
                               "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
                               "a.So_TK,to_char(a.Ngay_TK,'dd/MM/yyyy') as Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                               "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                               "a.So_Khung,a.So_May,a.TK_KH_NH,to_char(a.NGAY_KH_NH,'dd/MM/rrrr') as NGAY_KH_NH," & _
                               "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
                               "a.Lan_In,a.Trang_Thai, " & _
                               "a.So_BK, to_char(a.Ngay_BK,'dd/MM/yyyy') as Ngay_BK, " & _
                               "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, a.So_BT_KTKB,a.PT_TT" & _
                               " from TCS_CTU_HDR a " & _
                               "where (a.ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                               "and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _
                               "and (a.so_bt = " & key.So_BT.ToString() & ") " & _
                               "and (a.shkb = '" & key.SHKB.ToString() & "') " & _
                               "and (a.ma_dthu = '" & key.Ma_Dthu & "') "

                Dim dsCT As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

                ' Kiểm tra không có dữ liệu -> Thoát
                If (IsEmptyDataSet(dsCT)) Then Return hdr

                'Có dữ liệu -> đưa vào đối tượng "hdr"
                hdr.CQ_QD = dsCT.Tables(0).Rows(0).Item("CQ_QD").ToString()
                hdr.DC_NNThue = dsCT.Tables(0).Rows(0).Item("DC_NNTHUE").ToString()
                hdr.DC_NNTien = dsCT.Tables(0).Rows(0).Item("DC_NNTIEN").ToString()
                hdr.Diachi_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Diachi_KH_Nhan").ToString()
                hdr.DVSDNS = dsCT.Tables(0).Rows(0).Item("DVSDNS").ToString()
                hdr.KyHieu_CT = dsCT.Tables(0).Rows(0).Item("KYHIEU_CT").ToString()
                hdr.Lan_In = dsCT.Tables(0).Rows(0).Item("LAN_IN").ToString().Trim()
                hdr.LH_XNK = dsCT.Tables(0).Rows(0).Item("LH_XNK").ToString()
                hdr.Ly_Do = dsCT.Tables(0).Rows(0).Item("LY_DO").ToString()
                hdr.Ma_CQThu = dsCT.Tables(0).Rows(0).Item("MA_CQTHU").ToString()
                hdr.Ma_DThu = dsCT.Tables(0).Rows(0).Item("MA_DTHU").ToString()
                hdr.Ma_Huyen = dsCT.Tables(0).Rows(0).Item("MA_HUYEN").ToString()
                hdr.Ma_KS = dsCT.Tables(0).Rows(0).Item("MA_KS").ToString()
                hdr.Ma_LThue = dsCT.Tables(0).Rows(0).Item("MA_LTHUE").ToString()
                hdr.MA_NH_A = dsCT.Tables(0).Rows(0).Item("MA_NH_A").ToString()
                hdr.MA_NH_B = dsCT.Tables(0).Rows(0).Item("MA_NH_B").ToString()
                hdr.Ma_NNThue = dsCT.Tables(0).Rows(0).Item("MA_NNTHUE").ToString()
                hdr.Ma_NNTien = dsCT.Tables(0).Rows(0).Item("MA_NNTIEN").ToString()
                hdr.Ma_NT = dsCT.Tables(0).Rows(0).Item("MA_NT").ToString()
                hdr.Ma_NV = dsCT.Tables(0).Rows(0).Item("MA_NV").ToString()
                hdr.Ma_Tinh = dsCT.Tables(0).Rows(0).Item("MA_TINH").ToString()
                hdr.Ma_TQ = dsCT.Tables(0).Rows(0).Item("MA_TQ").ToString()
                hdr.Ma_Xa = dsCT.Tables(0).Rows(0).Item("MA_XA").ToString()
                hdr.Ngay_CT = dsCT.Tables(0).Rows(0).Item("NGAY_CT").ToString()
                hdr.Ngay_HT = dsCT.Tables(0).Rows(0).Item("NGAY_HT").ToString()
                hdr.Ngay_KB = dsCT.Tables(0).Rows(0).Item("NGAY_KB").ToString()
                hdr.NGAY_KH_NH = dsCT.Tables(0).Rows(0).Item("NGAY_KH_NH").ToString()
                hdr.Ngay_QD = dsCT.Tables(0).Rows(0).Item("NGAY_QD").ToString()
                hdr.Ngay_TK = dsCT.Tables(0).Rows(0).Item("NGAY_TK").ToString()
                hdr.SHKB = dsCT.Tables(0).Rows(0).Item("SHKB").ToString()
                hdr.So_BT = dsCT.Tables(0).Rows(0).Item("SO_BT").ToString()
                hdr.So_BThu = dsCT.Tables(0).Rows(0).Item("SO_BTHU").ToString()
                hdr.So_CT = dsCT.Tables(0).Rows(0).Item("SO_CT").ToString()
                hdr.So_CT_NH = dsCT.Tables(0).Rows(0).Item("SO_CT_NH").ToString()
                hdr.So_Khung = dsCT.Tables(0).Rows(0).Item("SO_KHUNG").ToString()
                hdr.So_May = dsCT.Tables(0).Rows(0).Item("SO_MAY").ToString()
                hdr.So_QD = dsCT.Tables(0).Rows(0).Item("SO_QD").ToString()
                hdr.So_TK = dsCT.Tables(0).Rows(0).Item("SO_TK").ToString()
                hdr.Ten_DVSDNS = dsCT.Tables(0).Rows(0).Item("Ten_DVSDNS").ToString()
                hdr.Ten_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Ten_KH_Nhan").ToString()
                hdr.Ten_NNThue = dsCT.Tables(0).Rows(0).Item("TEN_NNTHUE").ToString()
                hdr.Ten_NNTien = dsCT.Tables(0).Rows(0).Item("TEN_NNTIEN").ToString()
                hdr.TG_ID = dsCT.Tables(0).Rows(0).Item("TG_ID").ToString()
                hdr.TK_Co = dsCT.Tables(0).Rows(0).Item("TK_Co").ToString()
                hdr.TK_KH_NH = dsCT.Tables(0).Rows(0).Item("TK_KH_NH").ToString()
                hdr.TK_KH_Nhan = dsCT.Tables(0).Rows(0).Item("TK_KH_Nhan").ToString()
                hdr.TK_No = dsCT.Tables(0).Rows(0).Item("TK_No").ToString()
                hdr.Trang_Thai = dsCT.Tables(0).Rows(0).Item("TRANG_THAI").ToString()
                hdr.TT_TThu = dsCT.Tables(0).Rows(0).Item("TT_TTHU").ToString()
                hdr.TTien = dsCT.Tables(0).Rows(0).Item("TTIEN").ToString()
                hdr.TTien_NT = dsCT.Tables(0).Rows(0).Item("TTIEN_NT").ToString()
                hdr.Ty_Gia = dsCT.Tables(0).Rows(0).Item("TY_GIA").ToString()
                hdr.XA_ID = dsCT.Tables(0).Rows(0).Item("XA_ID").ToString()
                hdr.So_BT_KTKB = dsCT.Tables(0).Rows(0).Item("So_BT_KTKB").ToString()
                'hdr.PT_TT = dsCT.Tables(0).Rows(0).Item("PT_TT").ToString()
                hdr.NGAY_KH_NH = GetString(dsCT.Tables(0).Rows(0).Item("NGAY_KH_NH"))
                hdr.So_BK = GetString(dsCT.Tables(0).Rows(0).Item("So_BK"))
                hdr.Ngay_BK = GetString(dsCT.Tables(0).Rows(0).Item("Ngay_BK"))
                Return hdr
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chung chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_ChiTiet(ByVal key As KeyCTu) As infChungTuDTL()
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ---------------------------------------------------- 
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                        "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                        "DT_ID,MaDT,Ky_Thue,SoTien,SoTien_NT, MaQuy, Ma_DP " & _
                        "from TCS_CTU_DTL " & _
                        "where (ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                        "and (ma_nv = " & key.Ma_NV.ToString() & ") " & _
                        "and (so_bt = " & key.So_BT.ToString() & ") " & _
                        "and (shkb = '" & key.SHKB.ToString() & "') " & _
                        "and (ma_dthu = '" & key.Ma_Dthu & "') "
                Dim ds As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

                If (IsEmptyDataSet(ds)) Then Return Nothing
                Dim i, intCount As Integer
                intCount = ds.Tables(0).Rows.Count - 1

                Dim dtl(intCount) As infChungTuDTL

                For i = 0 To intCount
                    dtl(i) = New infChungTuDTL

                    dtl(i).CCH_ID = ds.Tables(0).Rows(i).Item("CCH_ID").ToString()
                    dtl(i).DT_ID = ds.Tables(0).Rows(i).Item("DT_ID").ToString()
                    dtl(i).ID = ds.Tables(0).Rows(i).Item("ID").ToString()
                    dtl(i).Ky_Thue = ds.Tables(0).Rows(i).Item("Ky_Thue").ToString()
                    dtl(i).LKH_ID = ds.Tables(0).Rows(i).Item("LKH_ID").ToString()
                    dtl(i).Ma_Cap = ds.Tables(0).Rows(i).Item("Ma_Cap").ToString()
                    dtl(i).Ma_Chuong = ds.Tables(0).Rows(i).Item("Ma_Chuong").ToString()
                    dtl(i).Ma_DThu = ds.Tables(0).Rows(i).Item("Ma_DThu").ToString()
                    dtl(i).Ma_Khoan = ds.Tables(0).Rows(i).Item("Ma_Khoan").ToString()
                    dtl(i).Ma_Loai = ds.Tables(0).Rows(i).Item("Ma_Loai").ToString()
                    dtl(i).Ma_Muc = ds.Tables(0).Rows(i).Item("Ma_Muc").ToString()
                    dtl(i).Ma_NV = ds.Tables(0).Rows(i).Item("Ma_NV").ToString()
                    dtl(i).Ma_TLDT = ds.Tables(0).Rows(i).Item("MaDT").ToString()
                    dtl(i).Ma_TMuc = ds.Tables(0).Rows(i).Item("Ma_TMuc").ToString()
                    dtl(i).MTM_ID = ds.Tables(0).Rows(i).Item("MTM_ID").ToString()
                    dtl(i).Ngay_KB = ds.Tables(0).Rows(i).Item("Ngay_KB").ToString()
                    dtl(i).Noi_Dung = ds.Tables(0).Rows(i).Item("Noi_Dung").ToString()
                    dtl(i).SHKB = ds.Tables(0).Rows(i).Item("SHKB").ToString()
                    dtl(i).So_BT = ds.Tables(0).Rows(i).Item("So_BT").ToString()
                    dtl(i).SoTien = ds.Tables(0).Rows(i).Item("SoTien").ToString()
                    dtl(i).SoTien_NT = ds.Tables(0).Rows(i).Item("SoTien_NT").ToString()
                    dtl(i).MaQuy = ds.Tables(0).Rows(i).Item("MaQuy").ToString()
                    dtl(i).Ma_DP = ds.Tables(0).Rows(i).Item("Ma_DP").ToString()
                Next

                Return dtl
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_Header_Set(ByVal key As KeyCTu) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chung của chứng từ     
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess

                Dim strSQL As String
                strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV, a.Ly_do_huy, " & _
                               "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                               "a.KyHieu_CT,a.So_CT,a.So_CT_NH,a.SEQ_NO,a.so_xcard," & _
                               "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                               "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue, a.REMARKS,a.dien_giai_hq," & _
                               "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
                               "to_char(a.Ngay_QD,'dd/MM/rrrr') as Ngay_QD ,a.CQ_QD,a.Ngay_CT," & _
                               "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh,tinh.ten ten_tinh," & _
                               "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co,a.TK_Co as TEN_TKCO,'' DIEN_GIAI,a.ma_cn,to_char(a.ngay_ht,'dd/MM/yyyy HH24:MI:SS') ngay_ht," & _
                               "a.ds_tokhai So_TK,to_char(a.Ngay_TK,'dd/MM/rrrr') as Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                               "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                               "a.So_Khung,a.So_May,a.TK_KH_NH,to_char(a.NGAY_KH_NH,'dd/MM/rrrr') as NGAY_KH_NH,a.TEN_KH_NH as TEN_KH_NH," & _
                               "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
                               "a.Lan_In,a.Trang_Thai,a.so_bt_ktkb, a.TT_CTHUE,a.ma_sanpham, " & _
                               "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT,a.TT_CITAD,A.SO_CMND,A.SO_FONE, " & _
                               "nha.TEN_NH_CHUYEN as TEN_NH_A, a.ten_nh_b,a.MAHUYEN_NNT,a.TENHUYEN_NNT,a.MATINH_NNT,a.TENTINH_NNT,a.TENHUYEN_NNTHAY,a.TENTINH_NNTHAY,LOAI_NNT,'' ten_loai_nnt, " & _
                               "xa.ten as TenXa, a.MA_NTK,to_char(a.time_begin,'dd/MM/yyyy HH24:MI:SS') time_begin,to_char(a.ngay_ks,'dd/MM/yyyy HH24:MI:SS') ngay_ks, " & _
                               "cqthu.ten as Ten_CQThu, nt.ten as Ten_NT, lt.ten as Ten_LThue,a.PT_TT as PT_TT,a.isactive,a.tt_tthu,a.ma_tq, " & _
                               "a.So_BK, to_char(a.Ngay_BK,'dd/MM/yyyy') as Ngay_BK,tcs_dm_khobac.ten as ten_kb,a.PHI_GD,A.PHI_VAT,a.PHI_GD2,A.PHI_VAT2,a.PT_TINHPHI,a.RM_REF_NO,a.REF_NO, " & _
                               "a.QUAN_HUYENNNTIEN, a.TINH_TPNNTIEN, a.MA_HQ, a.LOAI_TT, a.TEN_HQ, a.MA_HQ_PH, a.TEN_HQ_PH, a.MA_NH_TT, a.ten_nh_tt,a.MA_SANPHAM, a.MSG_LOI, a.MA_QUAN_HUYENNNTIEN, a.MA_TINH_TPNNTIEN, " & _
                               " a.ma_cuc,a.ten_cuc,a.ghi_chu " & _
                               "from TCS_CTU_HDR a, TCS_DM_XA xa,TCS_DM_XA tinh, " & _
                               "TCS_DM_CQTHU cqthu, TCS_DM_NGUYENTE nt, TCS_DM_LTHUE lt, TCS_NGANHANG_CHUYEN nha, tcs_dm_nh_giantiep_tructiep nhb, tcs_dm_nh_giantiep_tructiep nhtt, tcs_dm_khobac " & _
                               "where (a.xa_id = xa.xa_id(+)) and a.Ma_Tinh=tinh.ma_tinh(+) " & _
                               " and (a.ma_nh_a = nha.MA_NH_CHUYEN(+)) and (a.ma_nh_b = nhb.ma_giantiep(+)) and (a.ma_nh_tt = nhtt.ma_tructiep(+)) and (a.SHKB=nhb.shkb(+)) and (a.SHKB=nhtt.shkb(+))  " & _
                               "and (a.ma_cqthu = cqthu.ma_cqthu(+))  and (a.ma_hq = cqthu.ma_hq(+)) and (a.ma_nt = nt.ma_nt(+)) and (a.ma_lthue = lt.ma_lthue(+)) " & _
                               "and (a.ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                               "and (a.shkb = '" & key.SHKB.ToString() & "') " & _
                               "and (a.so_bt = " & key.So_BT.ToString() & ") " & _
                               "and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _
                               "and (a.shkb = tcs_dm_khobac.shkb(+)) "
                ' "and (a.ma_dthu = '" & key.Ma_Dthu & "') " & _

                '"and (a.so_ct = '" & key.So_CT & "') " & _
                '"and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _

                'Return
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chung chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_ChiTiet_Set(ByVal key As KeyCTu) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ---------------------------------------------------- 
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                strSQL = "select ID, SHKB, Ngay_KB, Ma_NV, So_BT, Ma_DThu, CCH_ID, Ma_Cap, " & _
                        " Ma_Chuong, LKH_ID, Ma_Loai, Ma_Khoan, MTM_ID, Ma_Muc, Ma_TMuc, Noi_Dung," & _
                        " DT_ID, MaDT, Ky_Thue, SoTien ttien, SoTien_NT, MaQuy, Ma_DP, 0 SODATHUTK, SO_CT, TT_BTOAN, " & _
                        " Ma_HQ, MA_LHXNK, So_TK, Ngay_TK, Ma_LT, Sac_Thue " & _
                        " FROM TCS_CTU_DTL " & _
                        " where (shkb = '" & key.SHKB & "') and (ngay_kb = " & key.Ngay_KB & ") " & _
                        " and (so_bt = " & key.So_BT & ") and (ma_nv = " & key.Ma_NV & ")"
                'and (ma_dthu = '" & key.Ma_Dthu & "')
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_ChiTiet_Set_CT_TK(ByVal SO_CT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ---------------------------------------------------- 
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                strSQL = "select ID, SHKB, Ngay_KB, Ma_NV, So_BT, Ma_DThu, CCH_ID, Ma_Cap," & _
                        " Ma_Chuong, LKH_ID, Ma_Loai, Ma_Khoan, MTM_ID, Ma_Muc, Ma_TMuc, Noi_Dung," & _
                        " DT_ID, MaDT, Ky_Thue, SoTien ttien, SoTien_NT, MaQuy, Ma_DP," & _
                        " SO_TK,MA_LHXNK,MA_LT, TO_CHAR(NGAY_TK,'dd/MM/yyyy') NGAY_TK,MA_HQ , SAC_THUE" & _
                        " FROM TCS_CTU_DTL " & _
                        " where (SO_CT='" & SO_CT & "')"
                'and (ma_dthu = '" & key.Ma_Dthu & "')
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function BaoLanh_Header_Set(ByVal key As KeyCTu) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chung của bao lanh           
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess

                Dim strSQL As String
                strSQL = "   SELECT  shkb, ten_kb, ngay_kb, a.ma_nv, a.ten_nv,b.ma_nv ma_ks,b.ten ten_ks, so_bt," & _
                                   " ma_dthu, so_bthu, kyhieu_ct, so_ct, ma_nnthue, " & _
                                   " ten_nnthue, dc_nnthue, huyen_nnthue, tinh_nnthue, " & _
                                   " ngay_ct, to_char(a.ngay_ht,'dd/MM/yyyy') ngay_ht, ma_ks, ten_ks, ma_cqthu, ten_cqthu, " & _
                                   " so_tk, xa_id, ma_tinh, ma_huyen, ma_xa, tk_no, " & _
                                   " tk_co, to_char(ngay_tk,'dd/MM/yyyy') ngay_tk, lhxnk, vt_lhxnk, ten_lhxnk, ma_nt, " & _
                                   " ten_nt, ty_gia, ma_nh_a, ten_nh_a, ttien, ttien_tthu, " & _
                                   " ttien_nt, trang_thai, ghi_chu, tk_ns, ten_tk_ns, " & _
                                   " phuong_thuc, hinh_thuc, khoi_phuc, reponse_code, " & _
                                   " hq_tran_id, hq_loai_thue, res_hq, so_bl, songay_bl,MA_DV_DD,TEN_DV_DD, " & _
                                   " kieu_bl, ma_hq_ph, dien_giai, trangthai_lapct, " & _
                                   " to_char(ngay_batdau,'dd/MM/yyyy') ngay_batdau, to_char(ngay_ketthuc,'dd/MM/yyyy') ngay_ketthuc, error_code_hq, so_ct_ctu, " & _
                                   " dchieu_hq, is_ma_hq, ngay_dc_hq, kb_thu_ho, " & _
                                   " ma_loaitien,ly_do_ctra, ma_hq_cqt, ma_hq,ten_hq, ma_hq_ph, ten_hq_ph, so_bt_hq, ly_do_huy, so_ct_tuchinh, ngay_ct ngay_bn, ngay_ct ngay_ct, ma_hq_cqt ma_cqthu, cif, " & _
                                   " a.hd_nt,to_char(a.ngay_hd,'dd/MM/yyyy') ngay_hd, a.hoa_don,to_char(a.ngay_hdon,'dd/MM/yyyy') ngay_hdon, a.vt_don, to_char(ngay_vtd,'dd/MM/yyyy') ngay_vtd, " & _
                                   " a.vt_don2, to_char(ngay_vtd2,'dd/MM/yyyy') ngay_vtd2,a.vt_don3, to_char(ngay_vtd3,'dd/MM/yyyy') ngay_vtd3,a.vt_don4, to_char(ngay_vtd4,'dd/MM/yyyy') ngay_vtd4,a.vt_don5, to_char(ngay_vtd5,'dd/MM/yyyy') ngay_vtd5, " & _
                                   " a.TRANG_THAI_BL,to_char(a.NGAY_THANH_TOAN,'dd/MM/yyyy') NGAY_THANH_TOAN,a.type_bl,A.TRANG_THAI_TT " & _
                           " FROM tcs_baolanh_hdr a,tcs_dm_nhanvien b" & _
                           " WHERE a.ma_ks=b.ma_nv(+) " & _
                                   " And shkb like '%" & key.SHKB.ToString() & "%' "
                If (Not IsNothing(key.So_CT)) Then
                    strSQL = strSQL & _
                                   " And so_ct = '" & key.So_CT & "' "
                End If
                If (Not IsNothing(key.Ngay_KB) And Not key.Ngay_KB = 0) Then
                    strSQL = strSQL & _
                                   " And ngay_kb = " & key.Ngay_KB.ToString()
                End If
                strSQL = strSQL & _
                                   " And so_bt = " & key.So_BT.ToString() & _
                                    " And ma_dthu like'%" & key.Ma_Dthu & "%'"
                ' " AND a.MA_NV in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV=" & key.Ma_NV.ToString() & ")) " & _

                'Return
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chung của bảo lãnh")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function BaoLanh_ChiTiet_Set(ByVal key As KeyCTu) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của bảo lãnh       
            ' ---------------------------------------------------- 
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                strSQL = " Select id, shkb, ngay_kb, ma_nv, so_bt, ma_dthu, ma_quy, " & _
                         " ma_cap, ma_chuong, ma_nkt, ma_nkt_cha, ma_ndkt, " & _
                         " ma_ndkt_cha, ma_tlpc, ma_khtk, noi_dung, ky_thue, " & _
                         " ma_nt, sotien, sotien_nt, ma_dp, loai_thue_hq " & _
                         " From tcs_baolanh_dtl " & _
                         " Where " & _
                                " shkb like '%" & key.SHKB & "%'"
                If (Not IsNothing(key.Ngay_KB)) Then
                    strSQL = strSQL & _
                                   " And ngay_kb = " & key.Ngay_KB.ToString()
                End If
                strSQL = strSQL & _
                                " And so_bt = " & key.So_BT & _
                                " And ma_dthu like '%" & key.Ma_Dthu & "%'" & _
                 " AND MA_NV =" & key.Ma_NV.ToString()

                '//' ****************************************************************************
                '//' Người sửa: Anhld
                '//' Ngày 25.09.2012
                '//' Mục đích: Order by ket qua theo tieu muc
                '//'***************************************************************************** */
                strSQL = strSQL & " Order by ma_ndkt asc"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết của bảo lãnh")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_CalSoDaThu(ByVal strMa_NNT As String, ByVal strMaQuy As String, ByVal strMaChuong As String, _
                                       ByVal strMaKhoan As String, ByVal strMaMuc As String, ByVal strKyThue As String) As String
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                strSQL = "SELECT   a.ma_nnthue, SUM (b.sotien) AS sodathutk, b.ma_chuong, b.ma_khoan,b.ma_tmuc, b.ky_thue" _
                    & " FROM tcs_ctu_hdr a, tcs_ctu_dtl b WHERE a.shkb = b.shkb AND a.ngay_kb = b.ngay_kb" _
                    & " AND a.ma_nv = b.ma_nv AND a.ma_dthu = b.ma_dthu AND a.so_bt = b.so_bt" _
                     & " and  a.ma_nnthue='" & strMa_NNT & "'" _
                    & " and b.ma_chuong ='" & strMaChuong & "' " _
                    & " and b.ma_tmuc ='" & strMaMuc & "' " _
                    & " and a.trang_thai ='01' " _
                    & " GROUP BY a.ma_nnthue, b.ma_chuong, b.ma_khoan, b.ma_tmuc, b.ky_thue"

                If cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows.Count > 0 Then
                    Return Globals.Format_Number(cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)("sodathutk").ToString, ".")
                Else
                    Return "0"
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin của bảo lãnh")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_CalSoDaThu_NgayKS(ByVal strMa_NNT As String, ByVal strMaChuong As String, _
                                       ByVal strMaMuc As String, ByVal strKyThue As String, ByVal strNgayKS As String) As String
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                strSQL = "SELECT   a.ma_nnthue, SUM (b.sotien) AS sodathutk, b.ma_chuong, b.ma_khoan,b.ma_tmuc, b.ky_thue" _
                    & " FROM tcs_ctu_hdr a, tcs_ctu_dtl b WHERE a.shkb = b.shkb AND a.ngay_kb = b.ngay_kb" _
                    & " AND a.ma_nv = b.ma_nv AND a.ma_dthu = b.ma_dthu AND a.so_bt = b.so_bt" _
                     & " and  a.ma_nnthue='" & strMa_NNT & "'" _
                    & " and b.ma_chuong ='" & strMaChuong & "' " _
                    & " and b.ma_tmuc ='" & strMaMuc & "' " _
                    & " and upper(b.ky_thue) =upper('" & strKyThue & "' )" _
                    & " and to_char(a.ngay_ks,'dd/MM/yyyy') ='" & strNgayKS & "' " _
                    & " and a.trang_thai ='01' " _
                    & " GROUP BY a.ma_nnthue, b.ma_chuong, b.ma_khoan, b.ma_tmuc, b.ky_thue"

                If cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows.Count > 0 Then
                    Return Globals.Format_Number(cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)("sodathutk").ToString, ".")
                Else
                    Return "0"
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin của bảo lãnh")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_CalSoDaThu_ToKhai(ByVal strMa_NNT As String, ByVal strMaChuong As String, _
                                      ByVal strMaMuc As String, ByVal strSoToKhai As String, Optional ByVal strNgayKS As String = "") As String
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                strSQL = "SELECT   a.ma_nnthue, SUM (b.sotien) AS sodathutk, b.ma_chuong, b.ma_khoan,b.ma_tmuc, b.ky_thue" _
                    & " FROM tcs_ctu_hdr a, tcs_ctu_dtl b WHERE a.shkb = b.shkb AND a.ngay_kb = b.ngay_kb" _
                    & " AND a.ma_nv = b.ma_nv AND a.ma_dthu = b.ma_dthu AND a.so_bt = b.so_bt" _
                    & " and  a.ma_nnthue='" & strMa_NNT & "'" _
                    & " and b.ma_chuong ='" & strMaChuong & "' " _
                    & " and b.ma_tmuc ='" & strMaMuc & "' " _
                    & " and b.SO_TK ='" & strSoToKhai & "' " _
                    & " and a.trang_thai ='01' " _
                    & " GROUP BY a.ma_nnthue, b.ma_chuong, b.ma_khoan, b.ma_tmuc, b.ky_thue"

                If cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows.Count > 0 Then
                    Return Globals.Format_Number(cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)("sodathutk").ToString, ".")
                Else
                    Return "0"
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin của bảo lãnh")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
#End Region

#Region "Xóa thực thu"
        Public Function CTU_GetPKey(ByVal strKHCT As String, ByVal strSoCT As String) As String
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                'Lấy key
                strSQL = "select (shkb||ngay_kb||ma_nv||so_bt||ma_dthu) as PKey " & _
                            "from tcs_ctu_hdr " & _
                            "where KYHIEU_CT='" & strKHCT & "' " & _
                            "AND SO_CT='" & strSoCT & "'"

                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strPKey As String = ""
                If (dr.Read()) Then
                    strPKey = dr(0).ToString()
                End If
                dr.Close()

                Return strPKey
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin thực thu")
                Throw ex
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Return ""
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function KQ_GetPKey(ByVal strKHCT As String, ByVal strSoCT As String) As String
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select (shkb||ngay_kb||ma_nv||so_bt||ma_dthu) as PKey " & _
                            "from tcs_khoquy_dtl " & _
                            "where KYHIEU_CT='" & strKHCT & "' " & _
                            "AND SO_CT='" & strSoCT & "'"

                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strPKey As String = ""
                If (dr.Read()) Then
                    strPKey = dr(0).ToString()
                End If
                dr.Close()

                Return strPKey
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin PKey")
                Throw ex
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Return ""
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function KQ_GetKH_SO_CT(ByVal strKQKey As String, ByRef KHCT As String(), ByRef SoCT As String()) As Integer
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select kyhieu_ct, so_ct " & _
                            "from tcs_khoquy_dtl " & _
                            "where (shkb||ngay_kb||ma_nv||so_bt||ma_dthu) = '" & strKQKey & "' "

                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim intCount As Integer = 0
                While (dr.Read())
                    KHCT(intCount) = dr(0).ToString()
                    SoCT(intCount) = dr(1).ToString()
                    intCount += 1
                End While
                Return intCount
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin khách hàng số chứng từ")
                Throw ex
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Sub CTU_Delete(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String)
            '-----------------------------------------------------
            ' Mục đích: Xóa chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 06/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Dim tran As IDbTransaction
            Try
                Dim strCTU_PKey As String = CTU_GetPKey(strKYHIEU_CT, strSO_CT)
                Dim strKQ_PKey As String = KQ_GetPKey(strKYHIEU_CT, strSO_CT)

                Dim KHCT(20), SoCT(20) As String
                Dim intCount As Integer = 0
                intCount = KQ_GetKH_SO_CT(strKQ_PKey, KHCT, SoCT)

                Dim strSQL As String
                cnCT = New DataAccess
                tran = cnCT.BeginTransaction()

                'Xóa phần chi tiết về số thực thu
                strSQL = "delete from tcs_khoquy_dtl " & _
                        "where (shkb||ngay_kb||ma_nv||so_bt||ma_dthu) = '" & strKQ_PKey & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text, tran)

                'Xóa phần Header thực thu
                strSQL = "delete from tcs_khoquy_hdr " & _
                        "where (shkb||ngay_kb||ma_nv||so_bt||ma_dthu) = '" & strKQ_PKey & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text, tran)

                'Xóa chi tiết chứng từ
                strSQL = "delete from tcs_ctu_dtl " & _
                        "where (shkb||ngay_kb||ma_nv||so_bt||ma_dthu) = '" & strCTU_PKey & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text, tran)

                'Xóa phần header chứng từ   
                strSQL = "delete from TCS_CTU_HDR " & _
                        "where (shkb||ngay_kb||ma_nv||so_bt||ma_dthu) = '" & strCTU_PKey & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text, tran)

                'Cập nhật lại trạng thái số thực thu của chứng từ
                Dim i As Integer
                For i = 0 To intCount - 1
                    If (KHCT(i) <> strKYHIEU_CT And SoCT(i) <> strSO_CT) Then
                        strSQL = "update tcs_ctu_hdr " & _
                                "set ma_tq = 0, tt_tthu = 0 " & _
                                "where kyhieu_ct='" & KHCT(i) & "' AND so_ct='" & SoCT(i) & "'"
                        cnCT.ExecuteNonQuery(strSQL, CommandType.Text, tran)
                    End If
                Next

                ' Cap nhat trang thai cho bien lai neu CT duoc lap tu bien lai
                strSQL = "Update TCS_CTBL set Trang_Thai='00', Kyhieu_CT='',so_CT=''  where "
                strSQL &= " Kyhieu_CT='" & strKYHIEU_CT & "' and So_CT='" & strSO_CT & "'"

                cnCT.ExecuteNonQuery(strSQL, CommandType.Text, tran)

                tran.Commit()
            Catch ex As Exception
                If (Not IsNothing(tran)) Then tran.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xóa thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(tran)) Then tran.Dispose()
            End Try
        End Sub
        Public Sub STT_Delete(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String)
            '-----------------------------------------------------
            ' Mục đích: Xóa Số thực thu của chứng từ
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 15/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Dim tran As IDbTransaction
            Try
                Dim strKQ_PKey As String = KQ_GetPKey(strKYHIEU_CT, strSO_CT)
                Dim KHCT(20), SoCT(20) As String
                Dim intCount As Integer = 0
                intCount = KQ_GetKH_SO_CT(strKQ_PKey, KHCT, SoCT)

                cnCT = New DataAccess
                Dim strSQL As String
                tran = cnCT.BeginTransaction()

                'Xóa phần chi tiết về số thực thu
                strSQL = "delete from tcs_khoquy_dtl " & _
                        "where (shkb||ngay_kb||ma_nv||so_bt||ma_dthu) = '" & strKQ_PKey & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text, tran)

                'Xóa phần Header thực thu
                strSQL = "delete from tcs_khoquy_hdr " & _
                        "where (shkb||ngay_kb||ma_nv||so_bt||ma_dthu) = '" & strKQ_PKey & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text, tran)

                'Cập nhật lại trạng thái số thực thu của chứng từ
                Dim i As Integer
                For i = 0 To intCount - 1
                    strSQL = "update tcs_ctu_hdr " & _
                            "set ma_tq = 0, tt_tthu = 0 " & _
                            "where kyhieu_ct='" & KHCT(i) & "' AND so_ct='" & SoCT(i) & "'"
                    cnCT.ExecuteNonQuery(strSQL, CommandType.Text, tran)
                Next

                tran.Commit()
            Catch ex As Exception
                If (Not IsNothing(tran)) Then tran.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xóa số thực thu chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(tran)) Then tran.Dispose()
            End Try
        End Sub
#End Region

#Region "Others"
        Public Function CTU_PrintAllow(ByVal key As KeyCTu) As Boolean
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select tk_no, tk_co, so_ct " & _
                    "from tcs_ctu_hdr " & _
                    "where shkb = '" & key.SHKB.ToString() & "' and " & _
                    " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                    " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                    " so_bt = " & key.So_BT.ToString() & " and " & _
                    " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim blnReturn As String = False

                If (dr.Read()) Then
                    Dim strTKNo As String = dr(0).ToString()
                    Dim strTKCo As String = dr(1).ToString()
                    Dim strSoCT As String = dr(2).ToString()

                    Dim intKieuMH, intKieuCT As Integer

                    Get_Map_TK_Form(strTKNo, strTKCo, intKieuMH, intKieuCT)

                    Select Case intKieuMH
                        Case 1
                            If (strSoCT.Length > 7) Then
                                blnReturn = False
                            Else
                                blnReturn = True
                            End If
                        Case Else
                            blnReturn = False
                    End Select
                End If
                dr.Close()

                Return blnReturn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi hệ thống")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Return False
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_GetSoBT_KTKB(ByVal key As KeyCTu) As String
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select ma_nv, so_bt_ktkb " & _
                        "from tcs_ctu_hdr " & _
                       "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() & " and "
                '  " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strMaNV As String = ""
                Dim strSoBT As String = ""
                If (dr.Read()) Then
                    strMaNV = GetString(dr(0))
                    strSoBT = GetString(dr(1))
                End If
                dr.Close()

                If (strSoBT = "" Or strSoBT = "0") Then
                    strSoBT = ""
                Else
                    strSoBT = strSoBT.PadLeft(5, "0")
                    strMaNV = strMaNV.PadLeft(3, "0")

                    strSoBT = strMaNV & "/" & strSoBT
                End If

                Return strSoBT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin số bút toán")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
#End Region

#Region "Chứng từ phục hồi"

        Public Function CTU_PhucHoi(ByVal strWhere As String) As DataSet
            Dim cnCT As New DataAccess
            Dim strSQL As String
            Try
                strSQL = "select distinct hdr.kyhieu_ct, hdr.so_ct, hdr.ma_nnthue, hdr.ten_nnthue, " & _
                        "(hdr.ma_tinh||'.'||hdr.ma_huyen||'.'||hdr.ma_xa) as DBHC, hdr.ma_cqthu, " & _
                        "hdr.tk_no, hdr.tk_co, hdr.ttien " & _
                        " FROM tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " & _
                        "where (hdr.shkb = dtl.shkb) and (hdr.ngay_kb =dtl.ngay_kb) and " & _
                        "(hdr.ma_nv =dtl.ma_nv) and (hdr.so_bt =dtl.so_bt) and (hdr.ma_dthu = hdr.ma_dthu)" & _
                        strWhere

                'strSQL = "select distinct hdr.kyhieu_ct, hdr.so_ct, hdr.ma_nnthue, hdr.ten_nnthue, " & _
                '        "(hdr.ma_tinh||'.'||hdr.ma_huyen||'.'||hdr.ma_xa) as DBHC, hdr.ma_cqthu, " & _
                '        "hdr.tk_no, hdr.tk_co, hdr.ttien " & _
                '        "from tcs_ctu_thop_hdr hdr, tcs_ctu_thop_dtl dtl " & _
                '        "where (hdr.shkb = dtl.shkb) and (hdr.ngay_kb =dtl.ngay_kb) and " & _
                '        "(hdr.ma_nv =dtl.ma_nv) and (hdr.so_bt =dtl.so_bt) and (hdr.ma_dthu = hdr.ma_dthu)" & _
                '        strWhere
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi phục hồi chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_PhucHoi_DTL(ByVal strKHCT As String, ByVal strSoCT As String) As DataSet
            Dim cnCT As New DataAccess
            Dim strSQL As String
            Try
                strSQL = "SELECT dtl.noi_dung AS noi_dung, dtl.ma_chuong AS chuong, dtl.ma_loai AS loai, " & _
                        "       dtl.ma_khoan AS khoan, dtl.ma_muc AS muc, dtl.ma_tmuc AS tmuc, " & _
                        "       dtl.ky_thue AS kythue, dtl.sotien AS sotien, dtl.ma_tldt AS madieutiet, " & _
                        "       (hdr.kyhieu_ct || hdr.so_ct) AS khsoct, hdr.ma_nnthue AS ma_nnthue, " & _
                        "       hdr.ten_nnthue AS ten_nnthue, hdr.dc_nnthue AS dc_nnthue, " & _
                        "       hdr.ma_nntien AS ma_nntien, hdr.ten_nntien AS ten_nntien, " & _
                        "       hdr.dc_nntien AS dc_nntien, hdr.tk_no AS tk_no, hdr.tk_co AS tk_co, " & _
                        "       nha.ten AS nh_a, nhb.ten AS nh_b, hdr.ma_cqthu AS ma_cqthu, " & _
                        "       cqthu.ten AS ten_cqthu, hdr.so_tk AS so_tk, " & _
                        "       TO_CHAR (hdr.ngay_tk, 'dd/MM/yyyy') AS ngay_tk, t.ten AS tinh, " & _
                        "       t.ten AS tinh2, h.ten AS huyen, h.ten AS huyen2, x.ten AS xa, " & _
                        "       x.ten AS xa2, hdr.ttien AS tien " & _
                        "  FROM tcs_ctu_thop_hdr hdr, " & _
                        "       tcs_ctu_thop_dtl dtl, " & _
                        "       tcs_dm_nganhang nha, " & _
                        "       tcs_dm_nganhang nhb, " & _
                        "       tcs_dm_cqthu cqthu, " & _
                        "       tcs_dm_tinh t, " & _
                        "       tcs_dm_huyen h, " & _
                        "       tcs_dm_xa x " & _
                        " WHERE (hdr.shkb = dtl.shkb) " & _
                        "   AND (hdr.ngay_kb = dtl.ngay_kb) " & _
                        "   AND (hdr.ma_nv = dtl.ma_nv) " & _
                        "   AND (hdr.so_bt = dtl.so_bt) " & _
                        "   AND (hdr.ma_dthu = hdr.ma_dthu) " & _
                        "   AND (hdr.ma_nh_a = nha.ma(+)) " & _
                        "   AND (hdr.ma_nh_b = nhb.ma(+)) " & _
                        "   AND (hdr.ma_cqthu = cqthu.ma_cqthu) " & _
                        "   AND (hdr.ma_tinh = t.ma_tinh) " & _
                        "   AND (hdr.ma_tinh = h.ma_tinh) " & _
                        "   AND (hdr.ma_huyen = h.ma_huyen) " & _
                        "   AND (hdr.xa_id = x.xa_id) " & _
                        "and (hdr.kyhieu_ct = '" & strKHCT & "') and (hdr.so_ct = '" & strSoCT & "')"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi phục hồi chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

#End Region

#Region "Ký, chuyển sang KTKB"
        Public Sub ChuyenSangKTKB(ByVal strSHKB As String, ByVal lngNgay_KB As Long, ByVal lngMa_NV As Long, _
                                       ByVal lngSo_BT_TCS As Long, ByVal strMa_DThu As String, ByVal lngMa_KS As Long, _
                                       ByRef strSo_BT_KTKB As String, ByRef strError As String)
            'Dim cnChungTu As DataAccess
            'Dim cnKS As New OracleConnect
            'Dim strConn As String
            'Dim strSQL As String
            'Dim arrOut As ArrayList
            'Dim p As IDbDataParameter
            'Dim arrIn As IDataParameter()
            'Dim arrInHT As IDataParameter()

            'Try
            '    cnChungTu = New DataAccess

            '    ReDim arrIn(6)
            '    p = cnKS.GetParameter("p1_shkb", ParameterDirection.Input, strSHKB, DbType.String, 100)
            '    arrIn(0) = p
            '    p = cnKS.GetParameter("p1_ngay_kb", ParameterDirection.Input, lngNgay_KB, DbType.Int64)
            '    arrIn(1) = p
            '    p = cnKS.GetParameter("p1_ma_nv", ParameterDirection.Input, lngMa_NV, DbType.Int64)
            '    arrIn(2) = p
            '    p = cnKS.GetParameter("p1_so_bt_tcs", ParameterDirection.Input, lngSo_BT_TCS, DbType.Int64)
            '    arrIn(3) = p
            '    p = cnKS.GetParameter("p1_ma_dthu", ParameterDirection.Input, strMa_DThu, DbType.String, 100)
            '    arrIn(4) = p
            '    p = cnKS.GetParameter("p1_ma_ks", ParameterDirection.Input, lngMa_KS, DbType.Int64)
            '    arrIn(5) = p
            '    p = cnKS.GetParameter("p1_error", ParameterDirection.Output, Nothing, DbType.String, 500)
            '    arrIn(6) = p
            '    strSQL = "TCS_PCK_KTKB.prc1_insert_ctu_tdtt"

            '    arrOut = cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn)

            '    Dim strMsg As String
            '    strMsg = arrOut.Item(0).ToString

            '    If (strMsg = "OK") Then
            '        ' Thuc hien chuyen sang KTKB
            '        ReDim arrInHT(7)
            '        p = cnKS.GetParameter("p_shkb", ParameterDirection.Input, strSHKB, DbType.String, 100)
            '        arrInHT(0) = p
            '        p = cnKS.GetParameter("p_ngay_kb", ParameterDirection.Input, lngNgay_KB, DbType.Int64)
            '        arrInHT(1) = p
            '        p = cnKS.GetParameter("p_ma_nv", ParameterDirection.Input, lngMa_NV, DbType.Int64)
            '        arrInHT(2) = p
            '        p = cnKS.GetParameter("p_so_bt_tcs", ParameterDirection.Input, lngSo_BT_TCS, DbType.Int64)
            '        arrInHT(3) = p
            '        p = cnKS.GetParameter("p_ma_dthu", ParameterDirection.Input, strMa_DThu, DbType.String, 100)
            '        arrInHT(4) = p
            '        p = cnKS.GetParameter("p_ma_ks", ParameterDirection.Input, lngMa_KS, DbType.Int64)
            '        arrInHT(5) = p
            '        p = cnKS.GetParameter("p_so_bt_ktkb", ParameterDirection.Output, Nothing, DbType.String, 10)
            '        arrInHT(6) = p
            '        p = cnKS.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String, 500)
            '        arrInHT(7) = p
            '        strSQL = "TDTT_PCK_KTKB.prc_hach_toan_ktkb"
            '        arrOut = cnKS.ExecuteNonquery(strSQL, arrInHT)

            '        strSo_BT_KTKB = arrOut.Item(0).ToString()
            '        strError = arrOut.Item(1).ToString()
            '    End If
            'Catch ex As Exception
            '    'LogDebug.Writelog("Lỗi trong quá trình chuyển chứng từ sang KTKB: " & ex.ToString)
            '    Throw ex
            'Finally
            '    Try
            '        cnChungTu = New DataAccess

            '        ' Dọn dữ liệu Temp
            '        Dim arrInXoa As IDataParameter()
            '        ReDim arrInXoa(6)

            '        p = cnKS.GetParameter("p_shkb", ParameterDirection.Input, strSHKB, DbType.String, 100)
            '        arrInXoa(0) = p
            '        p = cnKS.GetParameter("p_ngay_kb", ParameterDirection.Input, lngNgay_KB, DbType.Int64)
            '        arrInXoa(1) = p
            '        p = cnKS.GetParameter("p_ma_nv", ParameterDirection.Input, lngMa_NV, DbType.Int64)
            '        arrInXoa(2) = p
            '        p = cnKS.GetParameter("p_so_bt_tcs", ParameterDirection.Input, lngSo_BT_TCS, DbType.Int64)
            '        arrInXoa(3) = p
            '        p = cnKS.GetParameter("p_ma_dthu", ParameterDirection.Input, strMa_DThu, DbType.String, 100)
            '        arrInXoa(4) = p
            '        p = cnKS.GetParameter("p_ma_ks", ParameterDirection.Input, lngMa_KS, DbType.Int64)
            '        arrInXoa(5) = p
            '        p = cnKS.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String, 500)
            '        arrInXoa(6) = p
            '        strSQL = "TCS_PCK_KTKB.prc_xoa_tdtt"
            '        arrOut = cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrInXoa)
            '    Catch ex As Exception

            '    End Try
            '    If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            'End Try
        End Sub
#End Region

#Region "Lấy tài khoản theo địa bàn"
        Public Function Get_DSTaiKhoan(ByVal strMaDBHC As String) As DataSet
            Dim cnTK As DataAccess
            Dim strSql As String
            Dim dsTK As New DataSet
            Try
                cnTK = New DataAccess
                strSql = "Select tk, ten_tk From TCS_dm_taikhoan where dbhc='" & strMaDBHC.Replace(".", "") & "'"
                dsTK = cnTK.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách tài khoản theo địa bàn")
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách tài khoản theo địa bàn: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not cnTK Is Nothing Then cnTK.Dispose()
            End Try
            Return dsTK
        End Function
#End Region

#Region "Ghi chú"
        Public Function CTU_GetGhichu(ByVal key As KeyCTu) As String
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select ly_do " & _
                        "from tcs_ctu_hdr " & _
                        "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() & " and "
                '  " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strGhichu As String = ""
                If (dr.Read()) Then
                    strGhichu = dr(0).ToString()
                End If
                dr.Close()

                Return strGhichu
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin ghi chú chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Sub CTU_SetGhichu(ByVal key As KeyCTu, ByVal strGhichu As String)
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "update tcs_ctu_hdr " & _
                        "set ly_do = '" & RemoveApostrophe(strGhichu) & "'" & _
                        "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() & " and "
                ' " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)

                dr.Close()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khởi tạo ghi chú chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub
        Public Function CTU_Get_Valid_LanKS(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select save_type from tcs_ctu_hdr " & _
                        "where so_ct = '" & so_ct & "'"
                Dim strLanKS As String = cnCT.ExecuteSQLScalar(strSQL)

                Return strLanKS
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        '
        Public Function CTU_Get_LanKS(ByVal key As KeyCTu) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select save_type from tcs_ctu_hdr " & _
                        "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() '& " and " '& _
                '" ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                'drCT1 = cnCT.ExecuteSQLScalar(strSQL)
                Dim strLanKS As String = cnCT.ExecuteSQLScalar(strSQL)
                'If (drCT.Read()) Then
                '    strLanIn = drCT(0).ToString()
                'End If
                Return strLanKS
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        'CTU_SetLanKS
        Public Sub CTU_SetLanKS(ByVal key As KeyCTu, ByVal strClickKS As String)
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "update tcs_ctu_hdr " & _
                        "set save_type = '" & strClickKS & "'" & _
                        "where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString()
                ' " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)

                dr.Close()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khởi tạo ghi chú chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub
#End Region

#Region "In chứng từ"
        Public Function CTU_IN_LASER(ByVal key As KeyCTu) As DataSet
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select noi_dung as NoiDung, (ma_cap||ma_chuong) as CC, " & _
                                 "(ma_loai||ma_khoan) as LK, (ma_muc||ma_tmuc) as MTM, " & _
                                 "ky_thue as KyThue, sotien as SoTien " & _
                                 "from tcs_ctu_dtl " & _
                                 "where shkb = '" & key.SHKB.ToString() & "' and " & _
                                 " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                                 " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                                 " so_bt = " & key.So_BT.ToString() & " and "
                ' ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình in chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
#End Region

        Public Function CTU_Check_Exist_NNT(ByVal strMaNNT As String, ByVal strNgay_KS As String) As Integer
            '-----------------------------------------------------------------------
            ' Mục đích: Kiểm tra xem Mã NNT hiện tại đã được in và ghi chưa.
            ' Tham số: 
            ' Giá trị trả về: - 1: Nếu tìm thấy
            '                 - 0: Nếu không tìm thấy
            '                 - 2: Lỗi
            ' Ngày viết: 24/10/2007
            ' ----------------------------------------------------------------------
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Dim strSQL As String
            Dim bytResult As String
            Dim gdtmNgayLV As Long
            Try
                cnCT = New DataAccess
                strSQL = "select count(*)  " & _
                         "From TCS_CTU_HDR " & _
                         "Where Ma_NNThue='" & strMaNNT & _
                         "' And TRANG_THAI ='01' And " & _
                         "Ngay_CT = " & strNgay_KS

                bytResult = Int16.Parse(cnCT.ExecuteSQLScalar(strSQL))
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình kiểm tra tồn tại GNT")
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình kiểm tra tồn tại GNT ! - " & ex.ToString, EventLogEntryType.Error)
                ' Lỗi query dữ liệu
                bytResult = 0
            Finally
                If Not drCT Is Nothing Then
                    drCT.Dispose()
                End If
                If Not cnCT Is Nothing Then
                    cnCT.Dispose()
                End If
            End Try
            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function
        Public Function CTU_Check_Exist_NNT_HQ(ByVal strMaNNT As String, ByVal strSoToKhai As String) As Integer
            '-----------------------------------------------------------------------
            ' Mục đích: Kiểm tra xem Mã NNT hiện tại đã được in và ghi chưa.
            ' Tham số: 
            ' Giá trị trả về: - 1: Nếu tìm thấy
            '                 - 0: Nếu không tìm thấy
            '                 - 2: Lỗi
            ' Ngày viết: 24/10/2007
            ' ----------------------------------------------------------------------
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Dim strSQL As String
            Dim bytResult As String
            Dim gdtmNgayLV As Long
            Try
                cnCT = New DataAccess
                strSQL = "select count(*)  " & _
                         "From TCS_CTU_HDR " & _
                         "Where Ma_NNThue='" & strMaNNT & _
                         "' And TRANG_THAI <> '04' And " & _
                         " so_tk = '" & strSoToKhai & "'"

                bytResult = Int16.Parse(cnCT.ExecuteSQLScalar(strSQL))
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình kiểm tra tồn tại GNT")
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình kiểm tra tồn tại GNT ! - " & ex.ToString, EventLogEntryType.Error)
                ' Lỗi query dữ liệu
                bytResult = 0
            Finally
                If Not drCT Is Nothing Then
                    drCT.Dispose()
                End If
                If Not cnCT Is Nothing Then
                    cnCT.Dispose()
                End If
            End Try
            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function

        Public Sub Insert_New(ByVal objCTuHDR As NewChungTu.infChungTuHDR, ByVal objCTuDTL As NewChungTu.infChungTuDTL(), Optional ByVal Ngay_KB As String = "", Optional ByVal Ma_NV As String = "", Optional ByVal So_BT As String = "", Optional ByVal CTU_GNT_BL As String = "00")
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
            ' Đầu vào: đối tượng HDR và mảng DTL
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 01/11/2007

            ' Người sửa: Hoàng Văn Anh
            ' Ngày 23/12/2008
            ' Mục đích: Nâng cấp COA cho Ngân Hàng
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As NewChungTu.infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                'Bỏ phần Insert So_CT
                strSql = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV,MA_CN," & _
                          "So_BT,Ma_DThu,So_BThu," & _
                          "KyHieu_CT,So_CT_NH," & _
                          "Ma_NNTien,Ten_NNTien,DC_NNTien," & _
                          "Ma_NNThue,Ten_NNThue,DC_NNThue," & _
                          "Ly_Do,Ma_KS,Ma_TQ,So_QD," & _
                          "Ngay_QD,CQ_QD,Ngay_CT,Ngay_HT," & _
                          "Ma_CQThu,XA_ID,Ma_Tinh," & _
                          "Ma_Huyen,Ma_Xa,TK_No,TK_Co," & _
                          "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
                          "Ma_NT,Ty_Gia,TG_ID, Ma_LThue," & _
                          "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH," & _
                          "MA_NH_A,MA_NH_B,Ten_NH_B,Ten_NH_TT,TTien,TT_TThu," & _
                          "Lan_In,Trang_Thai, " & _
                          "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT," & _
                          "So_BK, Ngay_BK,CTU_GNT_BL,So_CT,TK_GL_NH,TEN_KH_NH,TK_KB_NH, " & _
                          "ISACTIVE,PHI_GD,PHI_VAT,PHI_GD2,PHI_VAT2,TIME_BEGIN,PT_TINHPHI,SEQ_NO,REF_NO,RM_REF_NO,QUAN_HUYENNNTIEN," & _
                          "TINH_TPNNTIEN,MA_HQ,LOAI_TT,TEN_HQ,MA_HQ_PH,TEN_HQ_PH, MA_NH_TT,MA_SANPHAM,TT_CITAD,SO_CMND,SO_FONE,MA_NTK, KENH_CT)" & _
                          "Values('" & objCTuHDR.SHKB & "'," & objCTuHDR.Ngay_KB & "," & objCTuHDR.Ma_NV & ",'" & objCTuHDR.Ma_CN & "'," & _
                          objCTuHDR.So_BT & ",'" & objCTuHDR.Ma_DThu & "'," & objCTuHDR.So_BThu & ",'" & _
                          objCTuHDR.KyHieu_CT & "','" & objCTuHDR.So_CT_NH & "','" & _
                          objCTuHDR.Ma_NNTien & "','" & objCTuHDR.Ten_NNTien & "','" & objCTuHDR.DC_NNTien & "','" & _
                          objCTuHDR.Ma_NNThue & "','" & objCTuHDR.Ten_NNThue & "','" & objCTuHDR.DC_NNThue & "','" & _
                          objCTuHDR.Ly_Do & "'," & objCTuHDR.Ma_KS & "," & objCTuHDR.Ma_TQ & ",'" & objCTuHDR.So_QD & "'," & _
                          objCTuHDR.Ngay_QD & ",'" & objCTuHDR.CQ_QD & "'," & objCTuHDR.Ngay_CT & "," & _
                          objCTuHDR.Ngay_HT & ",'" & objCTuHDR.Ma_CQThu & "','" & _
                          objCTuHDR.XA_ID & "','" & objCTuHDR.Ma_Tinh & "','" & objCTuHDR.Ma_Huyen & "','" & _
                          objCTuHDR.Ma_Xa & "','" & objCTuHDR.TK_No & "','" & objCTuHDR.TK_Co & "','" & _
                          objCTuHDR.So_TK & "'," & objCTuHDR.Ngay_TK & ",'" & objCTuHDR.LH_XNK & "','" & _
                          objCTuHDR.DVSDNS & "','" & objCTuHDR.Ten_DVSDNS & "','" & objCTuHDR.Ma_NT & "'," & _
                          objCTuHDR.Ty_Gia & "," & objCTuHDR.TG_ID & ",'" & objCTuHDR.Ma_LThue & "','" & _
                          objCTuHDR.So_Khung & "','" & objCTuHDR.So_May & "','" & objCTuHDR.TK_KH_NH & "'," & _
                          objCTuHDR.NGAY_KH_NH & ",'" & objCTuHDR.MA_NH_A & "','" & objCTuHDR.MA_NH_B & "','" & objCTuHDR.Ten_NH_B & "','" & objCTuHDR.TEN_NH_TT & "'," & _
                          objCTuHDR.TTien & "," & objCTuHDR.TT_TThu & "," & objCTuHDR.Lan_In & ",'" & _
                          objCTuHDR.Trang_Thai & "','" & objCTuHDR.TK_KH_Nhan & "','" & objCTuHDR.Ten_KH_Nhan & "','" & _
                          objCTuHDR.Diachi_KH_Nhan & "'," & objCTuHDR.TTien_NT & ",'" & objCTuHDR.PT_TT & "'," & Globals.EscapeQuote(objCTuHDR.So_BK) & "," & _
                          objCTuHDR.Ngay_BK & "," & Globals.EscapeQuote(CTU_GNT_BL) & "," & Globals.EscapeQuote(objCTuHDR.So_CT) _
                          & ",'" & objCTuHDR.TK_GL_NH & "'" & ",'" & objCTuHDR.TEN_KH_NH & "'" _
                          & ",'" & objCTuHDR.TK_KB_NH & "'," & objCTuHDR.Mode & "," & objCTuHDR.PHI_GD & "," & _
                          objCTuHDR.PHI_VAT & "," & objCTuHDR.PHI_GD2 & "," & objCTuHDR.PHI_VAT2 & "," & objCTuHDR.TIME_BEGIN & ",'" & objCTuHDR.PT_TINHPHI & "','" & objCTuHDR.SEQ_NO & "','" & objCTuHDR.REF_NO & "','" & _
                          objCTuHDR.RM_REF_NO & "','" & objCTuHDR.QUAN_HUYEN_NNTIEN & "','" & objCTuHDR.TINH_TPHO_NNTIEN & "','" & objCTuHDR.MA_HQ & "','" & _
                          objCTuHDR.LOAI_TT & "','" & objCTuHDR.TEN_HQ & "','" & objCTuHDR.Ma_hq_ph & "','" & objCTuHDR.TEN_HQ_PH & "','" & objCTuHDR.MA_NH_TT & "','" & objCTuHDR.SAN_PHAM & "','" & objCTuHDR.TT_CITAD & "','" & _
                          objCTuHDR.SO_CMND & "','" & objCTuHDR.SO_FONE & "','" & objCTuHDR.MA_NTK & "','" & objCTuHDR.Kenh_CT & "')"




                'Kienvt : Khong cho phep thuc thi nhung chung tu khong co dong detail
                If objCTuDTL Is Nothing Then Exit Sub
                If objCTuDTL.Length = 0 Then Exit Sub

                ' Insert vào bảng TCS_CT_HDR
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
                For Each objDTL In objCTuDTL
                    'strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                    '         "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                    '         "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,ma_dp) " & _
                    '         "Values (" & objDTL.ID & ",'" & objDTL.SHKB & "'," & objDTL.Ngay_KB & "," & objDTL.Ma_NV & "," & _
                    '         objDTL.So_BT & ",'" & objDTL.Ma_DThu & "'," & objDTL.CCH_ID & ",'" & _
                    '         objDTL.Ma_Cap & "','" & objDTL.Ma_Chuong & "'," & objDTL.LKH_ID & ",'" & _
                    '         objDTL.Ma_Loai & "','" & objDTL.Ma_Khoan & "'," & _
                    '         objDTL.MTM_ID & ",'" & objDTL.Ma_Muc & "','" & objDTL.Ma_TMuc & "','" & _
                    '         objDTL.Noi_Dung & "'," & _
                    '         objDTL.DT_ID & ",'" & objDTL.Ma_TLDT & "','" & objDTL.Ky_Thue & "'," & _
                    '         objDTL.SoTien & "," & objDTL.SoTien_NT & "," & Globals.EscapeQuote(objDTL.MaQuy) & "," & Globals.EscapeQuote(objDTL.Ma_DP) & ")"

                    'Kienvt : chi insert cac truong <>0
                    If Not objDTL Is Nothing Then
                        If objDTL.SoTien <> 0 Then
                            If objDTL.NGAY_TK Is Nothing Or objDTL.NGAY_TK = "" Then
                                objDTL.NGAY_TK = "null"
                            End If
                            strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                              "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                              "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,SO_CT,ma_dp, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ,SAC_THUE,TT_BTOAN) " & _
                              "Values (" & objDTL.ID & ",'" & objDTL.SHKB & "'," & objDTL.Ngay_KB & "," & objDTL.Ma_NV & "," & _
                              objDTL.So_BT & ",'" & objDTL.Ma_DThu & "'," & objDTL.CCH_ID & ",'" & _
                              objDTL.Ma_Cap & "','" & objDTL.Ma_Chuong & "'," & objDTL.LKH_ID & ",'" & _
                              objDTL.Ma_Loai & "','" & objDTL.Ma_Khoan & "'," & _
                              objDTL.MTM_ID & ",'" & objDTL.Ma_Muc & "','" & objDTL.Ma_TMuc & "','" & _
                              objDTL.Noi_Dung & "'," & _
                              objDTL.DT_ID & ",'','" & objDTL.Ky_Thue & "'," & _
                              objDTL.SoTien & "," & objDTL.SoTien_NT & "," & Globals.EscapeQuote(objDTL.MaQuy) & "," & Globals.EscapeQuote(objDTL.SO_CT) & ",'','" & _
                              objDTL.SO_TK & "'," & objDTL.NGAY_TK & ",'" & objDTL.LH_XNK & "','" & objDTL.LOAI_TT & "','" & objDTL.MA_HQ & "','" & objDTL.SAC_THUE & "','" & objDTL.TT_BTOAN & "')"
                            ' Insert Vào bảng TCS_CTU_DTL    
                            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                        End If
                    End If
                Next

                If Not Ngay_KB = "" Then
                    Dim NgayKB() As String = Ngay_KB.Split(";")
                    Dim MaNV() As String = Ma_NV.Split(";")
                    Dim SoBT() As String = So_BT.Split(";")

                    strSql = "Update TCS_CTBL set Trang_Thai='02', Kyhieu_CT='" & objCTuHDR.KyHieu_CT & "'  WHERE "
                    For i As Byte = 0 To NgayKB.Length - 1
                        strSql &= "( Ngay_KB=" & NgayKB(i) & " and Ma_NV=" & MaNV(i) & " and So_BT=" & SoBT(i) & ") or "
                    Next
                    strSql &= " (1=2) "

                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                End If
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert dữ liệu chứng từ")
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString & vbCrLf & strSql, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub

        Public Sub Insert_NewFromWS(ByVal objCTuHDR As NewChungTu.infChungTuHDR, ByVal objCTuDTL As NewChungTu.infChungTuDTL(), Optional ByVal Ngay_KB As String = "", Optional ByVal Ma_NV As String = "", Optional ByVal So_BT As String = "", Optional ByVal CTU_GNT_BL As String = "00")
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
            ' Đầu vào: đối tượng HDR và mảng DTL
            ' hoapt sửa
            ' Mục đích: Chuyen ct sang tct
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As NewChungTu.infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                'Bỏ phần Insert So_CT
                strSql = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV," & _
                          "So_BT,Ma_DThu,So_BThu," & _
                          "KyHieu_CT,So_CT_NH," & _
                          "Ma_NNTien,Ten_NNTien,DC_NNTien," & _
                          "Ma_NNThue,Ten_NNThue,DC_NNThue," & _
                          "Ly_Do,Ma_KS,Ma_TQ,So_QD," & _
                          "Ngay_QD,CQ_QD,Ngay_CT,Ngay_HT," & _
                          "Ma_CQThu,XA_ID,Ma_Tinh," & _
                          "Ma_Huyen,Ma_Xa,TK_No,TK_Co," & _
                          "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
                          "Ma_NT,Ty_Gia,TG_ID, Ma_LThue," & _
                          "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH," & _
                          "MA_NH_A,MA_NH_B,TTien,TT_TThu," & _
                          "Lan_In,Trang_Thai, " & _
                          "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT," & _
                          "So_BK, Ngay_BK,CTU_GNT_BL,So_CT,TK_GL_NH,TEN_KH_NH,TK_KB_NH, " & _
                          "ISACTIVE,PHI_GD,PHI_VAT,PT_TINHPHI,REF_NO,RM_REF_NO,QUAN_HUYENNNTIEN," & _
                          "TINH_TPNNTIEN,MA_HQ,LOAI_TT,TEN_HQ,MA_HQ_PH,TEN_HQ_PH, MA_NH_TT, KENH_CT)" & _
                          "Values('" & objCTuHDR.SHKB & "'," & objCTuHDR.Ngay_KB & "," & objCTuHDR.Ma_NV & "," & _
                          objCTuHDR.So_BT & ",'" & objCTuHDR.Ma_DThu & "'," & objCTuHDR.So_BThu & ",'" & _
                          objCTuHDR.KyHieu_CT & "','" & objCTuHDR.So_CT_NH & "','" & _
                          objCTuHDR.Ma_NNTien & "','" & objCTuHDR.Ten_NNTien & "','" & objCTuHDR.DC_NNTien & "','" & _
                          objCTuHDR.Ma_NNThue & "','" & objCTuHDR.Ten_NNThue & "','" & objCTuHDR.DC_NNThue & "','" & _
                          objCTuHDR.Ly_Do & "'," & objCTuHDR.Ma_KS & "," & objCTuHDR.Ma_TQ & ",'" & objCTuHDR.So_QD & "'," & _
                          objCTuHDR.Ngay_QD & ",'" & objCTuHDR.CQ_QD & "'," & objCTuHDR.Ngay_CT & "," & _
                          objCTuHDR.Ngay_HT & ",'" & objCTuHDR.Ma_CQThu & "'," & _
                          objCTuHDR.XA_ID & ",'" & objCTuHDR.Ma_Tinh & "','" & objCTuHDR.Ma_Huyen & "','" & _
                          objCTuHDR.Ma_Xa & "','" & objCTuHDR.TK_No & "','" & objCTuHDR.TK_Co & "','" & _
                          objCTuHDR.So_TK & "'," & objCTuHDR.Ngay_TK & ",'" & objCTuHDR.LH_XNK & "','" & _
                          objCTuHDR.DVSDNS & "','" & objCTuHDR.Ten_DVSDNS & "','" & objCTuHDR.Ma_NT & "'," & _
                          objCTuHDR.Ty_Gia & "," & objCTuHDR.TG_ID & ",'" & objCTuHDR.Ma_LThue & "','" & _
                          objCTuHDR.So_Khung & "','" & objCTuHDR.So_May & "','" & objCTuHDR.TK_KH_NH & "'," & _
                          objCTuHDR.NGAY_KH_NH & ",'" & objCTuHDR.MA_NH_A & "','" & objCTuHDR.MA_NH_B & "'," & _
                          objCTuHDR.TTien & "," & objCTuHDR.TT_TThu & "," & objCTuHDR.Lan_In & ",'" & _
                          objCTuHDR.Trang_Thai & "','" & objCTuHDR.TK_KH_Nhan & "','" & objCTuHDR.Ten_KH_Nhan & "','" & _
                          objCTuHDR.Diachi_KH_Nhan & "'," & objCTuHDR.TTien_NT & ",'" & objCTuHDR.PT_TT & "'," & Globals.EscapeQuote(objCTuHDR.So_BK) & "," & _
                          objCTuHDR.Ngay_BK & "," & Globals.EscapeQuote(CTU_GNT_BL) & "," & Globals.EscapeQuote(objCTuHDR.So_CT) _
                          & ",'" & objCTuHDR.TK_GL_NH & "'" & ",'" & objCTuHDR.TEN_KH_NH & "'" _
                          & ",'" & objCTuHDR.TK_KB_NH & "'," & objCTuHDR.Mode & "," & objCTuHDR.PHI_GD & "," & _
                          objCTuHDR.PHI_VAT & ",'" & objCTuHDR.PT_TINHPHI & "','" & objCTuHDR.REF_NO & "','" & _
                          objCTuHDR.RM_REF_NO & "','" & objCTuHDR.QUAN_HUYEN_NNTIEN & "','" & objCTuHDR.TINH_TPHO_NNTIEN & "','" & objCTuHDR.MA_HQ & "','" & _
                          objCTuHDR.LOAI_TT & "','" & objCTuHDR.TEN_HQ & "','" & objCTuHDR.Ma_hq_ph & "','" & objCTuHDR.TEN_HQ_PH & "','" & objCTuHDR.MA_NH_TT & "','" & objCTuHDR.Kenh_CT & "')"




                'Kienvt : Khong cho phep thuc thi nhung chung tu khong co dong detail
                If objCTuDTL Is Nothing Then Exit Sub
                If objCTuDTL.Length = 0 Then Exit Sub

                ' Insert vào bảng TCS_CT_HDR
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
                For Each objDTL In objCTuDTL
                    If Not objDTL Is Nothing Then
                        If objDTL.SoTien <> 0 Then
                            strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                              "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                              "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,ma_dp) " & _
                              "Values (" & objDTL.ID & ",'" & objDTL.SHKB & "'," & objDTL.Ngay_KB & "," & objDTL.Ma_NV & "," & _
                              objDTL.So_BT & ",'" & objDTL.Ma_DThu & "'," & objDTL.CCH_ID & ",'" & _
                              objDTL.Ma_Cap & "','" & objDTL.Ma_Chuong & "'," & objDTL.LKH_ID & ",'" & _
                              objDTL.Ma_Loai & "','" & objDTL.Ma_Khoan & "'," & _
                              objDTL.MTM_ID & ",'" & objDTL.Ma_Muc & "','" & objDTL.Ma_TMuc & "','" & _
                              objDTL.Noi_Dung & "'," & _
                              objDTL.DT_ID & ",'','" & objDTL.Ky_Thue & "'," & _
                              objDTL.SoTien & "," & objDTL.SoTien_NT & "," & Globals.EscapeQuote(objDTL.MaQuy) & ",'')"
                            ' Insert Vào bảng TCS_CTU_DTL    
                            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                        End If
                    End If
                Next

                If Not Ngay_KB = "" Then
                    Dim NgayKB() As String = Ngay_KB.Split(";")
                    Dim MaNV() As String = Ma_NV.Split(";")
                    Dim SoBT() As String = So_BT.Split(";")

                    strSql = "Update TCS_CTBL set Trang_Thai='02', Kyhieu_CT='" & objCTuHDR.KyHieu_CT & "'  WHERE "
                    For i As Byte = 0 To NgayKB.Length - 1
                        strSql &= "( Ngay_KB=" & NgayKB(i) & " and Ma_NV=" & MaNV(i) & " and So_BT=" & SoBT(i) & ") or "
                    Next
                    strSql &= " (1=2) "

                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                End If
                ' Cập nhật dữ liệu vào CSDL
                Dim v_strTrangThai_CT As String = "01"
                Dim strTT_CThue As String = "1"
                If objCTuHDR.Trang_Thai = "01" Then
                    ' insert vào bảng hdr trạng thái đã kiểm soát và chuyển thuế

                    Try
                        'GIPBankInterface.capnhatCTU(objCTuHDR.Ma_NNThue, objCTuHDR.SHKB, objCTuHDR.So_CT, objCTuHDR.So_BT, objCTuHDR.Ma_NV)
                    Catch ex As Exception
                        ' blnSuccess = False
                        log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog("Lỗi trong quá trình cập nhật sang thuế: " & ex.ToString & vbCrLf & strSql, EventLogEntryType.Error)
                        transCT.Rollback()
                        Throw ex
                        Exit Sub
                    End Try
                    strSql = "update TCS_CTU_HDR " & _
                  "   set TRANG_THAI = '" & v_strTrangThai_CT & "'" & _
                  ",Ma_KS = '" & objCTuHDR.Ma_KS & "'" & _
                  ",ngay_ks=(SELECT SYSDATE FROM DUAL)" & _
                  ",TT_CTHUE = '" & strTT_CThue & "'" & _
                  "   where shkb = '" & objCTuHDR.SHKB.ToString() & "' and " & _
                  "   ngay_kb = " & objCTuHDR.Ngay_KB.ToString() & " and " & _
                  "   ma_nv = " & objCTuHDR.Ma_NV.ToString() & " and " & _
                  "   so_bt = " & objCTuHDR.So_BT.ToString() & " and " & _
                  " so_ct = '" & objCTuHDR.So_CT.ToString() & "' "

                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                End If
                Dim str_Ten_TK_KH As String = objCTuHDR.TEN_KH_NH
                Dim cif_no As String = str_Ten_TK_KH.Split("-")(1).ToString

                strSql = "INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," _
                  & " CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SELL_RATE," _
                  & " AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO) " _
                  & " VALUES('TX002','" + objCTuHDR.Kenh_CT + "','1',SYSDATE,'OL2','" + cif_no + "'," _
                  & " '" + objCTuHDR.Ma_NNThue + "','','" + objCTuHDR.Ma_Xa + "','" + objCTuHDR.TK_KH_NH + "','D','VND','10000000','','" + objCTuHDR.MA_NH_B + "','10000000'," _
                  & " '" + objCTuHDR.TTien_NT.ToString + "', 0 ,0 ,'Insert từ WS','" + objCTuHDR.So_CT + "','OK','','','" + objCTuHDR.RM_REF_NO + "')"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                'OL2','" + objCTuHDR.TK_KH_NH + "',"  Chua bit lay cif_no o dau
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '   LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString & vbCrLf & strSql, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
        Public Sub Insert_Log_Message(ByVal xml_msg_signed As String, ByVal xml_msg As String, ByVal kenh_ct As String)
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
            ' Đầu vào: đối tượng HDR và mảng DTL
            ' hoapt sửa
            ' Mục đích: Chuyen ct sang tct
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As NewChungTu.infChungTuDTL
            Dim strSql As String
            Try
                Dim ma_msg As String = GetData_Seq("SEQ_ID_TCS_MSG_API.NEXTVAL")
                conn = New DataAccess
                strSql = "INSERT INTO tcs_message_api (ma_msg,xml_msg_signed, xml_msg,kenh_ct,NGAY_TH)" & _
                            " VALUES ('" + ma_msg + "','" + xml_msg_signed + "','" + xml_msg + "','" + kenh_ct + "', SYSDATE)"
                DatabaseHelp.Execute(strSql)
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '    LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu bảng tcs_message_api: " & ex.ToString & vbCrLf & strSql, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
        Public Shared Function get_TyGia(ByVal ma_nt As String, ByRef ma_tg As String) As String
            Dim connnect As DataAccess

            Try
                connnect = New DataAccess
                Dim strSql As String = "select ty_gia,tg_id from tcs_dm_tygia where ma_nt='" & ma_nt & "'"
                Dim dts As DataSet = connnect.ExecuteReturnDataSet(strSql, CommandType.Text)
                Dim strReturn_tg As String = dts.Tables(0).Rows(0)(0).ToString()
                ma_tg = dts.Tables(0).Rows(0)(1).ToString()
                Return strReturn_tg
            Catch ex As Exception
                Throw ex
                Return Nothing
            Finally
                connnect.Dispose()
            End Try


        End Function
        Public Function Get_DetailsCTU(ByVal so_CTu As String, Optional ByVal pv_strKenhCT As String = "") As DataSet
            Dim DS As New DataSet
            Dim Sql As String
            Dim cnCT As DataAccess
            Dim strWhere As String = ""
            If pv_strKenhCT <> "" Then
                strWhere = " and (a.Kenh_CT = " & pv_strKenhCT & ")"
            End If
            Try
                cnCT = New DataAccess
                Sql = "SELECT * FROM TCS_CTU_HDR a WHERE " & _
                        " (a.SO_CT = '" & so_CTu & "')" & strWhere

                DS = cnCT.ExecuteReturnDataSet(Sql, CommandType.Text)
                Return DS
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                Throw ex
            Finally
                If Not cnCT Is Nothing Then
                    cnCT.Dispose()
                End If
            End Try
        End Function

        Public Function CTU_Details_Set(ByVal pv_strSHKB As String, _
                                              ByVal pv_strSoBT As String, ByVal pv_strMaNV As Integer _
                                              ) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ---------------------------------------------------- 
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                strSQL = "select ID, SHKB, Ngay_KB, Ma_NV, So_BT, Ma_DThu, CCH_ID, Ma_Cap," & _
                        " Ma_Chuong, LKH_ID, Ma_Loai, Ma_Khoan, MTM_ID, Ma_Muc, Ma_TMuc, Noi_Dung," & _
                        " DT_ID, MaDT, Ky_Thue, SoTien ttien, SoTien_NT, MaQuy, Ma_DP, 0 SODATHUTK " & _
                        " FROM TCS_CTU_DTL " & _
                        " where (shkb = '" & pv_strSHKB & "')" & _
                        " and (so_bt = " & pv_strSoBT & ") and (ma_nv = " & pv_strMaNV & ")"
                'and (ma_dthu = '" & key.Ma_Dthu & "')
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_TN_CT_NHHQ(ByVal pv_strSO_CT As String, ByVal strTypeMSG As String) As String
            Dim connnect As DataAccess

            Try
                Dim strwhere As String = ""
                If strTypeMSG <> "" Then
                    strwhere = " and transaction_type ='" & strTypeMSG & "'"
                End If
                connnect = New DataAccess
                Dim strSql As String = "select so_tn_ct,ngay_tn_ct from tcs_dchieu_nhhq_hdr where so_ct='" & pv_strSO_CT & "'" & strwhere & " order by so_tn_ct desc"
                Dim dts As DataSet = connnect.ExecuteReturnDataSet(strSql, CommandType.Text)
                Dim strReturn_tg As String = dts.Tables(0).Rows(0)(0).ToString() & ";" & dts.Tables(0).Rows(0)(1).ToString()
                Return strReturn_tg
            Catch ex As Exception
                'Throw ex
                log.Error(ex.Message & "-" & ex.StackTrace) '
                Return ""
            Finally
                connnect.Dispose()
            End Try
        End Function
    End Class
End Namespace
