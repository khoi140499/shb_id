﻿Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports System.Data.OracleClient
Imports Business_HQ.NewChungTu

Namespace Common
    Public Class daChungTuTD
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "Sửa chi tiết chứng từ"
        Public Sub Update(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), Optional ByVal strSoPKT As String = "")
            ' ****************************************************************************
            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
            ' Đầu vào: đối tượng HDR và mảng DTL
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 09/11/2007
            '*****************************************************************************
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infChungTuDTL
            Dim strSql As String
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction

                'Cập nhật thông tin HDR của chứng từ
                strSql = "Update TCS_CTU_THOP_HDR SET " & _
                         "Ma_CQThu='" & objCTuHDR.Ma_CQThu & "'," & _
                         "XA_ID=" & objCTuHDR.XA_ID & ",Ma_Tinh='" & objCTuHDR.Ma_Tinh & "',Ma_Huyen='" & objCTuHDR.Ma_Huyen & "'," & _
                         "Ma_Xa='" & objCTuHDR.Ma_Xa & "',TK_No='" & objCTuHDR.TK_No & "',TK_Co='" & objCTuHDR.TK_Co & "' " & _
                         " Where (SHKB = '" & objCTuHDR.SHKB & "') And (Ngay_KB = " & objCTuHDR.Ngay_KB & _
                         ") And (Ma_NV = " & objCTuHDR.Ma_NV & ") And (So_BT = " & objCTuHDR.So_BT & _
                         ") And (Ma_DThu = '" & objCTuHDR.Ma_DThu & "')"

                ' Insert vào bảng TCS_CT_HDR
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
                For Each objDTL In objCTuDTL
                    strSql = "Update TCS_CTU_THOP_DTL set " & _
                             "cch_id = " & objDTL.CCH_ID & "," & _
                             "MA_CAP = '" & objDTL.Ma_Cap & "', " & _
                             "MA_CHUONG = '" & objDTL.Ma_Chuong & "', " & _
                             "lkh_id = " & objDTL.LKH_ID & "," & _
                             "MA_LOAI = '" & objDTL.Ma_Loai & "', " & _
                             "MA_KHOAN = '" & objDTL.Ma_Khoan & "', " & _
                             "mtm_id = " & objDTL.MTM_ID & "," & _
                             "MA_MUC = '" & objDTL.Ma_Muc & "', " & _
                             "MA_TMUC = '" & objDTL.Ma_TMuc & "', " & _
                             "dt_id = " & objDTL.DT_ID & "," & _
                             "MADT = '" & objDTL.Ma_TLDT & "', " & _
                             "NOI_DUNG = '" & objDTL.Noi_Dung & "', " & _
                             "KY_THUE = '" & objDTL.Ky_Thue & "', " & _
                             "MAQUY = '" & objDTL.MaQuy & "' " & _
                             "where ID = " & objDTL.ID
                    ' Insert Vào bảng TCS_CTU_DTL    
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                Next
                ' Cập nhật lại các chứng từ có cùng số phiếu kế toán
                If Not "".Equals(strSoPKT) And Not "0".Equals(strSoPKT) Then
                    strSql = "Update tcs_ctu_thop_hdr set trang_thai='01' where so_phieu_kt =" & strSoPKT & _
                             " And ngay_kb =" & objCTuHDR.Ngay_KB
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                End If
                ' Cập nhật dữ liệu vào CSDL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập dữ liệu chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình cập dữ liệu chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
#End Region

#Region "HoanNH"
        Public Function CapNhatPKT(ByVal strSoPKT As String, ByVal Update As Boolean) As Boolean
            Dim cnPKT As DataAccess
            Dim strSql As String
            Try
                cnPKT = New DataAccess
                If Update Then
                    strSql = "Delete From tdt_tmp_checkchon Where session_id = " & gstrTenND & _
                             " And block_name='TCS_CTU_THOP_HDR' And id_hdr='" & strSoPKT & "'"
                Else
                    strSql = "Insert Into tdt_tmp_checkchon(session_id, block_name,id_hdr) " & _
                             "Values(" & gstrTenND & ",'TCS_CTU_THOP_HDR','" & strSoPKT & "')"
                End If
                cnPKT.ExecuteNonQuery(strSql, CommandType.Text)
                Return True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật PKT vào bảng temp")
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình cập nhật PKT vào bảng temp: " & ex.ToString, EventLogEntryType.Error)
                Return False
            Finally
                If Not cnPKT Is Nothing Then cnPKT.Dispose()
            End Try
        End Function
        Public Function InsertPKTTemp(ByVal strWhere As String, ByVal Update As Boolean) As Boolean
            Dim cnPKT As DataAccess
            Dim strSql As String
            Try
                cnPKT = New DataAccess
                If Update Then
                    strSql = "Delete From tdt_tmp_checkchon Where session_id = " & gstrTenND & _
                             " And block_name='TCS_CTU_THOP_HDR'"
                Else
                    strSql = "Insert Into tdt_tmp_checkchon(id_hdr,session_id, block_name) " & _
                             "(Select distinct HDR.SO_PHIEU_KT || HDR.ngay_kb ," & gstrTenND & ",'TCS_CTU_THOP_HDR' " & _
                             "From TCS_CTU_THOP_HDR HDR WHERE (1=1) " & strWhere & ")"
                End If
                cnPKT.ExecuteNonQuery(strSql, CommandType.Text)
                Return True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật PKT vào bảng temp")
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình cập nhật PKT vào bảng temp: " & ex.ToString, EventLogEntryType.Error)
                Return False
            Finally
                If Not cnPKT Is Nothing Then cnPKT.Dispose()
            End Try
        End Function
        Public Function LoadDSDThu() As DataSet
            Dim cnDT As DataAccess
            Dim strSql As String
            Dim dsResult As DataSet
            Try
                cnDT = New DataAccess
                strSql = "SELECT ma_dthu, ten FROM tcs_dm_diemthu"
                dsResult = cnDT.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return dsResult
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách điểm thu: " & ex.ToString)
                Throw ex
            Finally
                If Not cnDT Is Nothing Then cnDT.Dispose()
            End Try
        End Function
        Public Function LoadDSCT(ByVal strSoPKT As String, ByVal lngNgayKB As Long, ByVal strMaDT As String) As DataSet
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Dim strSQL As String
            Try
                cnChungTu = New DataAccess
                strSQL = "Select KYHIEU_CT, SO_CT, SO_BTHU, TRANG_THAI, SO_BT, MA_NV, Lan_In, TTien, SHKB, NGAY_KB, MA_DTHU " & _
                         " from TCS_CTU_THOP_HDR " & _
                         "where trang_thai ='00' And ma_dthu ='" & strMaDT & "' And ngay_kb =" & lngNgayKB & " And so_phieu_kt =" & strSoPKT
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return dsChungTu
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
        End Function
        Public Function LoadDSCT(ByVal key As KeyCTu, _
                                Optional ByVal blnTHOP As Boolean = False, _
                                Optional ByVal blnTrangThaiAll As Boolean = False, _
                                Optional ByVal strTrangThaiChiTiet As String = "00") As DataSet

            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Dim strSQL As String
            Try
                Dim strTableName As String = "TCS_CTU_HDR"
                If (blnTHOP) Then strTableName = "TCS_CTU_THOP_HDR"
                Dim strWhereTrangThai As String = ""
                If (Not blnTrangThaiAll) Then
                    strWhereTrangThai = " and (trang_thai = '" & strTrangThaiChiTiet & "')"
                End If

                cnChungTu = New DataAccess
                strSQL = "Select KYHIEU_CT, SO_CT, SO_BTHU, TRANG_THAI, SO_BT, MA_NV, Lan_In, TTien, SHKB, NGAY_KB, MA_DTHU " & _
                         " from " & strTableName & " " & _
                         "where (ma_dthu ='" & key.Ma_Dthu & "') And (ngay_kb =" & key.Ngay_KB & ") And (so_bt =" & key.So_BT & _
                         ") and (shkb = '" & key.SHKB & "') and (ma_nv = " & key.Ma_NV & ") " & _
                         strWhereTrangThai & _
                        " order by ngay_kb, ma_nv, so_bt "

                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return dsChungTu
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách chứng từ")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
        End Function
#Region "Kết xuất sang KTKB"
        Public Function LoadCTKBHDR(ByVal strWhere As String, ByVal intTT As Integer) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Nguyễn Hữu Hoan
            ' ----------------------------------------------------------------
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Dim tranCT As IDbTransaction
            Dim strSQL As String
            Dim p As IDbDataParameter
            Dim arrIn As IDataParameter()
            Try
                cnChungTu = New DataAccess
                If intTT = 0 Then
                    tranCT = cnChungTu.BeginTransaction
                    ' Thực hiện gom chứng từ
                    ReDim arrIn(1)
                    p = cnChungTu.GetParameter("p_manv", ParameterDirection.Input, CInt(gstrTenND), DbType.Int64)
                    arrIn(0) = p
                    p = cnChungTu.GetParameter("p_where", ParameterDirection.Input, strWhere, DbType.String)
                    p.Size = 10000
                    arrIn(1) = p
                    strSQL = "TCS_PCK_KTKB.prc_gom_ctu"
                    cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn, tranCT)
                    ' Xóa trước khi đưa vào
                    strSQL = "Delete From tdt_tmp_checkchon Where session_id=" & gstrTenND & _
                             " And block_name='TCS_CTU_THOP_HDR'"
                    cnChungTu.ExecuteNonQuery(strSQL, CommandType.Text, tranCT)
                    ' Insert vào bảng tdt_tmp_checkchon
                    strSQL = "Insert Into tdt_tmp_checkchon(id_hdr,session_id, block_name) " & _
                             "(Select distinct HDR.SO_PHIEU_KT||HDR.Ngay_KB," & gstrTenND & ",'TCS_CTU_THOP_HDR' " & _
                             "From TCS_CTU_THOP_HDR HDR WHERE (1=1) " & strWhere & ")"
                    cnChungTu.ExecuteNonQuery(strSQL, CommandType.Text, tranCT)
                    ' Lấy dữ liệu sau khi gom chứng từ
                    strSQL = "Select HDR.SO_PHIEU_KT," & _
                             "TCS_PCK_TRAODOI.Fnc_ToDate (HDR.NGAY_KB) as NGAY_KB," & _
                             "TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_NO) as TK_NO, TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_CO) as TK_CO," & _
                             "(HDR.MA_TINH || HDR.MA_HUYEN || HDR.MA_XA) as DBHC," & _
                             "HDR.MA_CQTHU,HDR.MA_DTHU, " & _
                             "SUM(HDR.TTIEN) AS TONGTIEN,1,HDR.SHKB,HDR.so_bt_ktkb,HDR.ma_nv " & _
                             "FROM TCS_CTU_THOP_HDR HDR " & _
                             "WHERE (hdr.trang_thai='00') " & strWhere & _
                             " Group By SO_PHIEU_KT,SHKB,NGAY_KB,MA_DTHU,TK_NO,TK_CO,MA_TINH,MA_HUYEN,MA_XA,MA_CQTHU,so_bt_ktkb,ma_nv " & _
                             " Order by so_phieu_kt asc"
                    dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
                    tranCT.Commit()
                Else
                    ' Tra cứu các chứng từ đã được chuyển sang ktkb
                    ' Lấy dữ liệu sau khi gom chứng từ
                    strSQL = "Select HDR.SO_PHIEU_KT," & _
                             "TCS_PCK_TRAODOI.Fnc_ToDate (HDR.NGAY_KB) as NGAY_KB," & _
                             "TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_NO) as TK_NO, TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_CO) as TK_CO," & _
                             "(HDR.MA_TINH || HDR.MA_HUYEN || HDR.MA_XA) as DBHC," & _
                             "HDR.MA_CQTHU,HDR.MA_DTHU, " & _
                             "SUM(HDR.TTIEN) AS TONGTIEN,0,HDR.SHKB,HDR.so_bt_ktkb,HDR.ma_nv " & _
                             "FROM TCS_CTU_THOP_HDR HDR " & _
                             "WHERE (hdr.trang_thai='01') " & strWhere & _
                             " AND hdr.ma_dthu<>'01' " _
                             & " Group By SO_PHIEU_KT,SHKB,NGAY_KB,MA_DTHU,TK_NO,TK_CO,MA_TINH,MA_HUYEN,MA_XA,MA_CQTHU,so_bt_ktkb,ma_nv " & _
                             " Order by so_phieu_kt asc"
                    dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
                End If
            Catch ex As Exception
                tranCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not tranCT Is Nothing Then tranCT.Dispose()
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function
        Public Function LoadCTKBCTHDR(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ
            '         : không thực hiện gom chứng từ
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Nguyễn Hữu Hoan
            ' ----------------------------------------------------------------
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Dim strSQL As String
            Try
                cnChungTu = New DataAccess
                strSQL = "Select HDR.SO_CT," & _
                         "TCS_PCK_TRAODOI.Fnc_ToDate (HDR.NGAY_KB) as NGAY_KB," & _
                         "TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_NO) as TK_NO, TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_CO) as TK_CO," & _
                         "(HDR.MA_TINH || HDR.MA_HUYEN || HDR.MA_XA) as DBHC," & _
                         "HDR.MA_CQTHU,HDR.MA_DTHU, " & _
                         "HDR.TTIEN AS TONGTIEN,HDR.SHKB,hdr.ma_nv,hdr.so_bt " & _
                         "FROM TCS_CTU_THOP_HDR HDR " & _
                         "WHERE (hdr.trang_thai='01') " & strWhere
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function
        'Public Function HachToanKTKB(ByVal strWhere As String, _
        '                              ByVal strSHKB As String, ByVal maNV As Int64, _
        '                              ByVal NgayKTKB As Int64, ByRef ds As DataSet) As String
        '    Dim cnChungTu As DataAccess
        '    Dim cnKS As New OracleConnect
        '    Dim strConn As String
        '    Dim strSQL As String
        '    Dim arrOut As ArrayList
        '    Dim p As IDbDataParameter
        '    Dim arrIn As IDataParameter()
        '    Dim strResult As String
        '    Try
        '        cnChungTu = New DataAccess
        '        ' Thực hiện gom chứng từ
        '        ReDim arrIn(2)
        '        p = cnChungTu.GetParameter("p_where", ParameterDirection.Input, strWhere, DbType.String)
        '        p.Size = 10000
        '        arrIn(0) = p
        '        p = cnChungTu.GetParameter("p_ma_ks", ParameterDirection.Input, maNV, DbType.Int64)
        '        p.Size = 10
        '        arrIn(1) = p
        '        p = cnChungTu.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String)
        '        p.Size = 500
        '        arrIn(2) = p
        '        strSQL = "TCS_PCK_KTKB.Prc_Insert_ctuthop_tdtt"
        '        arrOut = cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn)
        '        cnChungTu.Dispose()

        '        strResult = arrOut.Item(0).ToString
        '        If strResult.Trim().ToUpper.Equals("OK") Then
        '            ' Thuc hien chuyen sang KTKB
        '            ReDim arrIn(2)
        '            p = cnKS.GetParameter("p_ngaylv", ParameterDirection.Input, gdtmNgayLV, DbType.Int64)
        '            arrIn(0) = p
        '            p = cnKS.GetParameter("p_ma_ks", ParameterDirection.Input, maNV, DbType.Int64)
        '            arrIn(1) = p
        '            p = cnKS.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String, 500)
        '            arrIn(2) = p
        '            strSQL = "TDTT_PCK_KTKB.prc_hach_toan_ktkb_thop"
        '            arrOut = cnKS.ExecuteNonquery(strSQL, arrIn)
        '            strResult = arrOut.Item(0).ToString()
        '            strSQL = "Select so_phieu_kt,noidung,to_char(thoigian,'dd/MM/rrrr HH24:MI:ss') From TDT_Log order by so_phieu_kt asc"
        '            ds = cnKS.ExecuteReturnDataset(strSQL, CommandType.Text)
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình hạch toánh KTKT: " & ex.ToString)
        '        Throw ex
        '    Finally
        '        cnChungTu = New DataAccess
        '        ' Thực hiện gom chứng từ
        '        ReDim arrIn(1)
        '        p = cnChungTu.GetParameter("p_ma_ks", ParameterDirection.Input, maNV, DbType.Int64)
        '        p.Size = 10
        '        arrIn(0) = p
        '        p = cnChungTu.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String)
        '        p.Size = 500
        '        arrIn(1) = p
        '        strSQL = "TCS_PCK_KTKB.prc_xoa_tdtt"
        '        arrOut = cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn)
        '        cnChungTu.Dispose()
        '        If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        '    End Try
        '    Return strResult
        'End Function
        Public Function LoadCTKBDTL(ByVal strSHKB As String, ByVal intNgay_KB As Integer, _
                                    ByVal strMa_DThu As String, ByVal strTk_No As String, _
                                    ByVal strTk_Co As String, ByVal strDBHC As String, _
                                    ByVal strMa_CQthu As String, ByVal intTT As Integer) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Nguyễn Hữu Hoan
            ' ---------------------------------------------------- 
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Try
                cnChungTu = New DataAccess
                Dim strSQL As String
                Dim strTT As String = "00"
                If (intTT <> 0) Then strTT = "01"

                strSQL = "select dtl.ma_chuong,dtl.ma_khoan,dtl.ma_tmuc, " & _
                        "sum(dtl.sotien) as sotien, dtl.madt, max(dtl.noi_dung) as noi_dung, max(dtl.ky_thue) as ky_thue, dtl.ma_dp, dtl.maquy " & _
                        "from TCS_CTU_THOP_HDR hdr, TCS_CTU_THOP_DTL dtl " & _
                        "Where (hdr.shkb = dtl.shkb) And (hdr.ngay_kb = dtl.ngay_kb) and (hdr.ma_nv = dtl.ma_nv) " & _
                        "And (hdr.so_bt = dtl.so_bt) And (hdr.ma_dthu = dtl.ma_dthu) " & _
                        "And (hdr.trang_thai='" & strTT & "') " & _
                        "And (hdr.SHKB = '" & strSHKB & "') " & _
                        "And (hdr.NGAY_KB = " & intNgay_KB & ") " & _
                        "And (hdr.MA_DTHU ='" & strMa_DThu & "') " & _
                        "And (hdr.TK_NO = '" & ChuanHoa_SoTK(strTk_No) & "') " & _
                        "And (hdr.TK_CO ='" & ChuanHoa_SoTK(strTk_Co) & "') " & _
                        "And ((HDR.MA_TINH || HDR.MA_HUYEN || HDR.MA_XA)='" & strDBHC & "') " & _
                        "And (hdr.Ma_cqthu = '" & strMa_CQthu & "') " & _
                        "group by dtl.ma_chuong, dtl.ma_khoan, dtl.ma_tmuc, " & _
                        "dtl.madt, dtl.ma_dp, dtl.maquy " & _
                        "order by dtl.ma_chuong,dtl.ma_khoan, dtl.ma_tmuc "

                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function
        Public Function LoadCTKBCTDTL(ByVal strSHKB As String, ByVal intNgay_KB As Integer, _
                                    ByVal strMa_DThu As String, ByVal strMa_NV As String, _
                                    ByVal strSo_BT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Nguyễn Hữu Hoan
            ' ---------------------------------------------------- 
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Try
                cnChungTu = New DataAccess
                Dim strSQL As String
                strSQL = "select dtl.ma_cap,dtl.ma_chuong,dtl.ma_loai,dtl.ma_khoan,dtl.ma_muc,dtl.ma_tmuc, " & _
                         "dtl.sotien " & _
                         "from TCS_CTU_THOP_DTL dtl " & _
                         "Where (dtl.SHKB = '" & strSHKB & "') " & _
                         "And (dtl.NGAY_KB = " & intNgay_KB & ") " & _
                         "And (dtl.MA_DTHU ='" & strMa_DThu & "') " & _
                         "And (dtl.ma_nv = " & strMa_NV & ") " & _
                         "And (dtl.so_bt =" & strSo_BT & ")"
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function
#End Region
#End Region

#Region "TrongNM"
        Public Function WhereLcn_Recv(ByVal strTSNCode As String) As String
            Dim strtmp As String
            If "01,02,03,04,05,32,33".IndexOf(strTSNCode) >= 0 Then Return " (1=1) "

            strtmp = "((Substr(LCN_OWNER,4,5) ='" & _
                         Mid(gstrMaNoiLamViec, 4, 5) & "') Or (Substr(lcn_recv,4,5) ='" & _
                         Mid(gstrMaNoiLamViec, 4, 5) & "')) "
            Return strtmp
        End Function
        Public Function LoadDataHeaderNhanCTNVP(ByVal strTSNCode As String, ByVal strPkgID As String, Optional ByVal strWhereNgay As String = "") As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh trọng
            ' Người sửa: Lê Hồng Hà
            ' Nội dung sửa: Cho phép tra cứu không theo gói dữ liệu
            ' ----------------------------------------------------------------
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Dim strSQL As String
            Try
                Dim strWhere As String = ""
                If (strPkgID <> "") Then
                    strWhere = " (TRAN_UP.PKG_ID = '" & strPkgID & "') And "
                Else
                    strWhere = " (tran_up.pkg_id IN ( SELECT p.ID FROM data_pkg p " & _
                                " WHERE (" & strWhereNgay & _
                                " (p.pkg_type = '1') AND (p.tsn_code = '62')))) and "
                End If
                cnChungTu = New DataAccess
                strSQL = "Select distinct Char03, Char04, TCS_PCK_TRAODOI.Fnc_ToDate(num01) AS num01, Char08, " & _
                         "TCS_PCK_TRAODOI.Fnc_FormatTK(Char18) Char18, " & _
                         "TCS_PCK_TRAODOI.Fnc_FormatTK(Char19) Char19, " & _
                         "char15, char16, char17, num09 , num02, num03, char02, char01 From TRAN_UP, MESS_UP " & _
                         "Where " & strWhere & _
                         "(TRAN_UP.Tran_No = MESS_UP.Tran_No) And" & _
                         "((TRAN_UP.Sts_Code='" & Common.mdlSystemVariables.gstrSSN & "') Or " & _
                         "((TRAN_UP.Sts_Code = '" & Common.mdlSystemVariables.gstrDN & "') And " & _
                         "(TRAN_UP.Resu_Code = '01'))) And " & WhereLcn_Recv(strTSNCode) & _
                         " Group by char01,char02,char03,char04,char08, char18, char19, char15, char16, char17, num01, num02, num03, num09"
                ', tran_up.tran_no
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function

        Public Function LoadDataDetailNhanCTNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String, ByVal strTranNo As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách detail của chứng từ
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh trọng
            ' Người sửa: Hoàng Văn Anh
            ' Mục Đích: sửa theo COA mới
            ' ----------------------------------------------------------------
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Dim strSQL As String
            Try
                'Tran_No='" & strTranNo & "' And
                cnChungTu = New DataAccess
                strSQL = "Select distinct Char42, Char34, Char36, Char38, Char39, CHAR40, Num15, CHAR41, CHAR43 From MESS_UP " & _
                "Where ( Char03 = '" & strKYHIEU_CT & "') And " & _
                "(Char04 = '" & strSO_CT & "')"

                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function

        Public Function LoadDataHeaderKXGNT(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Dim strSQL As String
            Try
                cnChungTu = New DataAccess

                strSQL = "SELECT hdr.kyhieu_ct, hdr.so_ct, hdr.ma_nnthue, hdr.ten_nnthue, " & _
                    "TCS_PCK_TRAODOI.Fnc_ToDate (hdr.ngay_ct) ngay_ct, " & _
                    "hdr.ttien, hdr.shkb " & _
                "FROM tcs_ctu_hdr hdr " & _
                "WHERE " & strWhere


                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL")
                Throw ex
                'LogDebug.WriteLog("Có lỗi sảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try

            Return dsChungTu
        End Function

        Public Function LoadBKKXCTNVP(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 13/12/2007
            ' Người viết: Ngô Minh Trọng
            ' Người Sửa: Hoàng Văn Anh
            ' Mục đích: sửa theo COA
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess

                strSQL = " SELECT DISTINCT dtl.maquy AS ma_quy, hdr.ngay_kb AS ngay_kb, hdr.shkb AS shkb," _
                    & " hdr.tk_no AS tk_no, hdr.tk_co AS tk_co, hdr.ma_xa AS dbhc, " _
                    & " (hdr.kyhieu_ct||hdr.so_ct) AS so_ct, hdr.ten_nnthue AS ten_nnt, " _
                    & " dtl.ma_chuong AS ma_chuong, dtl.ma_khoan AS ma_nganhkt, " _
                    & " dtl.ma_tmuc AS nd_kt, dtl.sotien AS so_tien " _
                    & " FROM tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " _
                    & " WHERE hdr.shkb = dtl.shkb " _
                    & "  AND hdr.ngay_kb = dtl.ngay_kb " _
                    & " AND hdr.ma_nv = dtl.ma_nv " _
                    & " AND hdr.so_bt = dtl.so_bt " _
                    & " AND hdr.ma_dthu = dtl.ma_dthu " _
                    & " AND " & strWhere _
                    & " ORDER BY (hdr.kyhieu_ct||hdr.so_ct), hdr.ngay_kb "

                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi Truy vấn BK CTU đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function LoadDataHeaderCQT(ByVal strWhere As String, ByVal strWhereNgay As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Dim strSQL As String
            Try
                'If (Not Thop_Kxuat_CQThu(strWhereNgay)) Then Return Nothing

                cnChungTu = New DataAccess
                strSQL = "SELECT hdr.kyhieu_ct || '/' || hdr.so_ct, " & _
                         "TCS_PCK_TRAODOI.Fnc_ToDate(hdr.ngay_ct) ngay_ct, hdr.ma_nnthue, " & _
                         "TCS_PCK_TRAODOI.Fnc_FormatTK(hdr.tk_no) tk_no, " & _
                         "TCS_PCK_TRAODOI.Fnc_FormatTK(hdr.tk_co)tk_co, " & _
                         "lpad(to_char(hdr.ma_nv),3,'0'), hdr.ma_cqthu, " & _
                         "hdr.ttien,hdr.shkb,hdr.ngay_kb,hdr.ma_nv,hdr.so_bt,hdr.ma_dthu, hdr.ma_xa " & _
                         "FROM kx_cqt_hdr hdr Where (1=1) " & strWhere & " order by hdr.so_ct asc"
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function

        'Public Function Thop_Kxuat_CQThu(ByVal strWhereNgay As String) As Boolean
        '    Dim cnChungTu As DataAccess
        '    Dim cnKS As New OracleConnect
        '    Dim strSQL As String
        '    Dim p As IDbDataParameter
        '    Dim arrIn As IDataParameter()
        '    Dim arrOut As ArrayList

        '    Try
        '        cnChungTu = New DataAccess

        '        ReDim arrIn(1)
        '        p = cnKS.GetParameter("p_where", ParameterDirection.Input, strWhereNgay, DbType.String, 1000)
        '        arrIn(0) = p
        '        p = cnKS.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String, 500)
        '        arrIn(1) = p

        '        strSQL = "TCS_PCK_TRAODOI.prc_thop_kxuat_cqthu"
        '        arrOut = cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn)
        '        Dim strError As String = GetString(arrOut.Item(0))
        '        If strError = "OK" Then Return True

        '        Return False
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Function LoadDataDetail(ByVal key As infChungTuHDR) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng 
            ' Người Sửa: Hoàng Văn Anh
            ' Mục Đích: Sửa theo COA
            ' ---------------------------------------------------- 
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Try
                cnChungTu = New DataAccess
                Dim strSQL As String
                strSQL = "select MAQUY,ma_chuong,ma_khoan,ma_tmuc, " & _
                         "madt,noi_dung,sotien,ky_thue,MA_DP " & _
                         "from TCS_CTU_DTL " & _
                         "Where (shkb = '" & key.SHKB & "') And (ngay_kb = " & key.Ngay_KB & ") and (ma_nv = " & key.Ma_NV & ") " & _
                         "And (so_bt = " & key.So_BT & ") And (ma_dthu = '" & key.Ma_DThu & "')"
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function

        Public Function LoadDataDetailCQT(ByVal strSHKB As String, ByVal strNgayKB As String, _
                                     ByVal strMaNV As String, ByVal strSoBT As String, ByVal strMaDThu As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng 
            ' ---------------------------------------------------- 
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Try
                cnChungTu = New DataAccess
                Dim strSQL As String
                strSQL = "select ma_chuong,ma_khoan,ma_tmuc, " & _
                         "sotien,ky_thue, madt, noi_dung, ky_thue, maquy " & _
                         "from kx_cqt_dtl  " & _
                         "Where (shkb ='" & strSHKB & "') And (ngay_kb = " & strNgayKB & ") and (ma_nv = " & strMaNV & ") " & _
                         "And (so_bt = " & strSoBT & ") And (ma_dthu ='" & strMaDThu & "')"
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function
        Public Function LoadDataDetailCQT(ByVal strSHKB As String, ByVal strNgayKB As String, _
                                     ByVal strMaNV As String, ByVal strSoBT As String, ByVal strMaDThu As String, _
                                     ByVal strMaNNT As String, ByVal strTKNo As String, ByVal strTKCo As String, _
                                     ByVal strMaXa As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng 
            ' ---------------------------------------------------- 
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Try
                cnChungTu = New DataAccess
                Dim strSQL As String
                strSQL = "select ma_chuong,ma_khoan,ma_tmuc, " & _
                         "sotien,ky_thue, madt, noi_dung, ky_thue, maquy " & _
                         "from kx_cqt_dtl  " & _
                         "Where (shkb ='" & strSHKB & "') And (ngay_kb = " & strNgayKB & ") and (ma_nv = " & strMaNV & ") " & _
                         "And (so_bt = " & strSoBT & ") And (ma_dthu ='" & strMaDThu & "') and (ma_nnthue = '" & strMaNNT & "') " & _
                         "and (tk_no = '" & strTKNo.Replace(".", "") & "') and (tk_co = '" & strTKCo.Replace(".", "") & "') " & _
                         "and (ma_xa = '" & strMaXa & "')"
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try
            Return dsChungTu
        End Function

        Public Function LoadTK(Optional ByVal strType As String = "") As Object
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách tài khoản có, tài khoản nợ
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------

            Dim strSQL As String
            Dim cnTaiKhoan As DataAccess
            Dim dsTaiKhoan As DataSet
            strType = strType.ToUpper.Trim()

            Try
                If (strType = "") Then
                    strSQL = "Select TK_NO, TK_CO FROM TCS_MAP_TK_CTU ORDER BY TK_NO, TK_CO"
                Else
                    strSQL = "Select TK_NO, TK_CO FROM TCS_MAP_TK_CTU WHERE TYPE = '" & strType & "' ORDER BY TK_NO, TK_CO"
                End If

                cnTaiKhoan = New DataAccess
                dsTaiKhoan = cnTaiKhoan.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy thông tin tài khoản từ CSDL")
                Throw ex
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try

            Return dsTaiKhoan
        End Function
#End Region

#Region "Sửa chứng từ ngoài VP"
        Public Sub CTU_DTL_UpdateNVP(ByVal dtl As infChungTuDTL)
            '-----------------------------------------------------
            ' Mục đích: Cập nhật thông tin chi tiết chứng từ.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 31/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------   
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "update TCS_CTU_THOP_DTL set " & _
                    "CCH_ID = " & dtl.CCH_ID & ", " & _
                    "MA_CAP = '" & dtl.Ma_Cap & "', " & _
                    "MA_CHUONG = '" & dtl.Ma_Chuong & "', " & _
                    "LKH_ID = " & dtl.LKH_ID & ", " & _
                    "MA_LOAI = '" & dtl.Ma_Loai & "', " & _
                    "MA_KHOAN = '" & dtl.Ma_Khoan & "', " & _
                    "MTM_ID = " & dtl.MTM_ID & ", " & _
                    "MA_MUC = '" & dtl.Ma_Muc & "', " & _
                    "MA_TMUC = '" & dtl.Ma_TMuc & "', " & _
                    "DT_ID = " & dtl.DT_ID & ", " & _
                    "MA_TLDT = '" & dtl.Ma_TLDT & "', " & _
                    "NOI_DUNG = '" & dtl.Noi_Dung & "', " & _
                    "SOTIEN_NT = '" & dtl.SoTien_NT & "', " & _
                    "SOTIEN = " & dtl.SoTien & ", " & _
                    "KY_THUE = '" & dtl.Ky_Thue & "' " & _
                    "where ID = " & dtl.ID
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi cập nhật thông tin chi tiết chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub
#End Region

        '#Region "Huy KTKB"
        '        Public Function HuyKTKB(ByVal intMaNL As Integer, ByVal intSoBT As Integer, ByVal intNgayKS As Long) As Boolean
        '            Dim cnKS As New OracleConnect
        '            Dim p As IDbDataParameter
        '            Dim arrIn As IDbDataParameter()
        '            Dim strSql As String
        '            Try
        '                ReDim arrIn(3)
        '                p = cnKS.GetParameter("p_ngay", ParameterDirection.Input, intNgayKS, DbType.Int64)
        '                arrIn(0) = p
        '                p = cnKS.GetParameter("p_manv", ParameterDirection.Input, intMaNL, DbType.Int64)
        '                arrIn(1) = p
        '                p = cnKS.GetParameter("p_sbt", ParameterDirection.Input, intSoBT, DbType.Int64)
        '                arrIn(2) = p
        '                p = cnKS.GetParameter("p_ma_ks", ParameterDirection.Input, CInt(gstrTenND), DbType.Int64)
        '                arrIn(3) = p
        '                strSql = "TDTT_PCK_KTKB.prc_huy_ktkb"
        '                cnKS.ExecuteNonquery(strSql, arrIn)
        '                Return True
        '            Catch ex As Exception
        '                'LogDebug.Writelog("Lỗi trong quá  trình hủy chứng từ bên KTKB: " & ex.Message)
        '                Throw ex
        '            End Try
        '        End Function
        '        Public Function HuyKTKB_NgoaiVP(ByVal lngNgay As Long, ByVal intMaNV As Integer, _
        '                                ByVal intSo_Phieu_KT As Integer, ByVal intSBT_KTKB As Integer, _
        '                                ByVal intMaKS As Integer, ByRef strError As String) As Boolean
        '            Dim cnKS As New OracleConnect
        '            Dim p As IDbDataParameter
        '            Dim arrOut As ArrayList
        '            Dim arrIn As IDbDataParameter()
        '            Dim strSql As String
        '            Try
        '                ReDim arrIn(5)
        '                p = cnKS.GetParameter("p_ngay", ParameterDirection.Input, lngNgay, DbType.Int64)
        '                arrIn(0) = p
        '                p = cnKS.GetParameter("p_manv", ParameterDirection.Input, intMaNV, DbType.Int64)
        '                arrIn(1) = p
        '                p = cnKS.GetParameter("p_so_phieu_kt", ParameterDirection.Input, intSo_Phieu_KT, DbType.UInt64)
        '                arrIn(2) = p
        '                p = cnKS.GetParameter("p_sbt_ktkb", ParameterDirection.Input, intSBT_KTKB, DbType.Int64)
        '                arrIn(3) = p
        '                p = cnKS.GetParameter("p_ma_ks", ParameterDirection.Input, intMaKS, DbType.Int64)
        '                arrIn(4) = p
        '                p = cnKS.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String, 500)
        '                arrIn(5) = p
        '                strSql = "TDTT_PCK_KTKB.prc_huy_ctu_ngoai_VP"

        '                arrOut = cnKS.ExecuteNonquery(strSql, arrIn)
        '                strError = arrOut.Item(0).ToString

        '                Return True
        '            Catch ex As Exception
        '                'LogDebug.Writelog("Lỗi trong quá  trình hủy chứng từ bên KTKB: " & ex.Message)
        '                Throw ex
        '            End Try
        '        End Function
        '#End Region


        Public Function CTU_HDR_SOBT_KTKB(ByVal lngSoPKT As Long, ByVal lngNgay As Long) As String
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin số bút toán KTKB của chứng từ 
            ' Ngày viết: 21/09/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String = "select so_bt_ktkb from tcs_ctu_thop_hdr " & _
                                    "where (so_phieu_kt = " & lngSoPKT & ") " & _
                                    "and (ngay_kb = " & lngNgay & ") "
                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strReturn As String = "0"
                If (dr.Read()) Then
                    strReturn = GetString(dr(0))
                End If

                dr.Close()
                Return strReturn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi thông tin số bút toán KTKB của chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        'Public Function Thop_Kxuat_CQThu(ByVal strWhereNgay As String) As Boolean
        '    Dim cnChungTu As DataAccess
        '    Dim cnKS As New OracleConnect
        '    Dim strSQL As String
        '    Dim p As IDbDataParameter
        '    Dim arrIn As IDataParameter()
        '    Dim arrOut As ArrayList

        '    Try
        '        cnChungTu = New DataAccess

        '        ReDim arrIn(1)
        '        p = cnKS.GetParameter("p_where", ParameterDirection.Input, strWhereNgay, DbType.String, 1000)
        '        arrIn(0) = p
        '        p = cnKS.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String, 500)
        '        arrIn(1) = p

        '        strSQL = "TCS_PCK_TRAODOI.prc_thop_kxuat_cqthu"
        '        arrOut = cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn)
        '        Dim strError As String = GetString(arrOut.Item(0))
        '        If strError = "OK" Then Return True

        '        Return False
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
    End Class
End Namespace
