﻿Namespace ChungTu
    Public Class infBienLai
        Private strSHKB As String
        Private iNgay_KB As Integer
        Private iMa_NV As Integer
        Private iSo_BT As Integer  ' số bút toán
        Private strMa_DThu As String
        Private iSo_BThu As Integer
        Private strKyHieu_BL As String
        Private strSo_BL As String
        Private strMa_NNTien As String
        Private strTen_NNTien As String
        Private strDC_NNTien As String
        Private strLy_Do As String
        Private iMa_KS As Integer
        Private iMa_TQ As Integer
        Private strSo_QD As String
        Private strNgay_QD As String
        Private strCQ_QD As String
        Private iNgay_CT As Integer
        Private strNgay_HT As String
        Private strTK_Co As String
        Private strID_LH As String
        Private dblTTien As Double
        Private dblTT_TThu As Double
        Private intLan_In As Integer
        Private strTrang_Thai As String
        Private strMa_LH As String
        Private strMa_Huyen As String
        Private strMa_Tinh As String

        Public Sub New()

        End Sub
        Public Property Ma_LH() As String
            Get
                Return strMa_LH
            End Get
            Set(ByVal Value As String)
                strMa_LH = Value
            End Set
        End Property
        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property
        Public Property Ngay_KB() As Integer
            Get
                Return iNgay_KB
            End Get
            Set(ByVal Value As Integer)
                iNgay_KB = Value
            End Set
        End Property
        Public Property Ma_NV() As Integer
            Get
                Return iMa_NV
            End Get
            Set(ByVal Value As Integer)
                iMa_NV = Value
            End Set
        End Property
        Public Property So_BT() As Integer
            Get
                Return iSo_BT
            End Get
            Set(ByVal Value As Integer)
                iSo_BT = Value
            End Set
        End Property
        Public Property Ma_DThu() As String
            Get
                Return strMa_DThu
            End Get
            Set(ByVal Value As String)
                strMa_DThu = Value
            End Set
        End Property
        Public Property So_BThu() As Integer
            Get
                Return iSo_BThu
            End Get
            Set(ByVal Value As Integer)
                iSo_BThu = Value
            End Set
        End Property
        Public Property KyHieu_BL() As String
            Get
                Return strKyHieu_BL
            End Get
            Set(ByVal Value As String)
                strKyHieu_BL = Value
            End Set
        End Property
        Public Property So_BL() As String
            Get
                Return strSo_BL
            End Get
            Set(ByVal Value As String)
                strSo_BL = Value
            End Set
        End Property
        Public Property Ma_NNTien() As String
            Get
                Return strMa_NNTien
            End Get
            Set(ByVal Value As String)
                strMa_NNTien = Value
            End Set
        End Property
        Public Property Ten_NNTien() As String
            Get
                Return strTen_NNTien
            End Get
            Set(ByVal Value As String)
                strTen_NNTien = Value
            End Set
        End Property
        Public Property DC_NNTien() As String
            Get
                Return strDC_NNTien
            End Get
            Set(ByVal Value As String)
                strDC_NNTien = Value
            End Set
        End Property
        Public Property Ly_Do() As String
            Get
                Return strLy_Do
            End Get
            Set(ByVal Value As String)
                strLy_Do = Value
            End Set
        End Property
        Public Property Ma_KS() As Integer
            Get
                Return iMa_KS
            End Get
            Set(ByVal Value As Integer)
                iMa_KS = Value
            End Set
        End Property
        Public Property Ma_TQ() As Integer
            Get
                Return iMa_TQ
            End Get
            Set(ByVal Value As Integer)
                iMa_TQ = Value
            End Set
        End Property
        Public Property So_QD() As String
            Get
                Return strSo_QD
            End Get
            Set(ByVal Value As String)
                strSo_QD = Value
            End Set
        End Property
        Public Property Ngay_QD() As String
            Get
                Return strNgay_QD
            End Get
            Set(ByVal Value As String)
                strNgay_QD = Value
            End Set
        End Property
        Public Property CQ_QD() As String
            Get
                Return strCQ_QD
            End Get
            Set(ByVal Value As String)
                strCQ_QD = Value
            End Set
        End Property
        Public Property Ngay_CT() As Integer
            Get
                Return iNgay_CT
            End Get
            Set(ByVal Value As Integer)
                iNgay_CT = Value
            End Set
        End Property
        Public Property Ngay_HT() As String
            Get
                Return strNgay_HT
            End Get
            Set(ByVal Value As String)
                strNgay_HT = Value
            End Set
        End Property
        Public Property ID_LH() As String
            Get
                Return strID_LH
            End Get
            Set(ByVal Value As String)
                strID_LH = Value
            End Set
        End Property
        Public Property TK_Co() As String
            Get
                Return strTK_Co
            End Get
            Set(ByVal Value As String)
                strTK_Co = Value
            End Set
        End Property
        Public Property TTien() As Double
            Get
                Return dblTTien
            End Get
            Set(ByVal Value As Double)
                dblTTien = Value
            End Set
        End Property
        Public Property TT_TThu() As Double
            Get
                Return dblTT_TThu
            End Get
            Set(ByVal Value As Double)
                dblTT_TThu = Value
            End Set
        End Property
        Public Property Lan_In() As Integer
            Get
                Return intLan_In
            End Get
            Set(ByVal Value As Integer)
                intLan_In = Value
            End Set
        End Property
        Public Property Trang_Thai() As String
            Get
                Return strTrang_Thai
            End Get
            Set(ByVal Value As String)
                strTrang_Thai = Value
            End Set
        End Property
        Public Property Ma_Huyen() As String
            Get
                Return strMa_Huyen
            End Get
            Set(ByVal Value As String)
                strMa_Huyen = Value
            End Set
        End Property
        Public Property Ma_Tinh() As String
            Get
                Return strMa_Tinh
            End Get
            Set(ByVal Value As String)
                strMa_Tinh = Value
            End Set
        End Property
    End Class
End Namespace