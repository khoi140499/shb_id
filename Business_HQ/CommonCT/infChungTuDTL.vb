﻿Namespace NewChungTu

    Public Class infChungTuDTL
        Private strDTL_ID As String
        Private strSHKB As String
        Private iNgay_KB As Integer
        Private iMa_NV As Integer
        Private iSo_BT As Integer  ' số bút toán
        Private strMa_DThu As String
        Private iCCH_ID As String
        Private strMa_Cap As String
        Private strMa_Chuong As String
        Private iLKH_ID As String
        Private strMa_Loai As String
        Private strMa_Khoan As String
        Private iMTM_ID As String
        Private strMa_Muc As String
        Private strMa_TMuc As String
        Private strNoi_Dung As String
        Private iDT_ID As String
        Private strMa_TLDT As String
        Private strKy_Thue As String
        Private dblSoTien As Double
        Private dblSoTien_NT As Double
        Private strSo_TK As String
        Private dNgay_TK As String
        Private strLH_XNK As String
        Private strMa_HQ As String
        Private strLoai_TT As String
        Private strSacThue As String

        Private strMaQuy As String
        Private strMaDP As String
        Private _PT_TT As String

        'Thuoc tinh dung cho Bao lanh HQ
        Private strMa_nkt As String
        Private strMa_nkt_cha As String
        Private strMa_ndkt As String
        Private strMa_ndkt_cha As String
        Private strMa_tlpc As Integer
        Private strMa_khtk As String
        Private strMa_nt As String
        Private strLoai_thue_hq As String
        Private strSO_CT As String
        Private strTT_BT As String
        Private _MA_LT As String
        
        Public Property TT_BTOAN() As String
            Get
                Return strTT_BT
            End Get
            Set(ByVal value As String)
                strTT_BT = value
            End Set
        End Property
        Public Property Ma_nkt() As String
            Get
                Return strMa_nkt
            End Get
            Set(ByVal value As String)
                strMa_nkt = value
            End Set
        End Property

        Public Property Ma_nkt_cha() As String
            Get
                Return strMa_nkt_cha
            End Get
            Set(ByVal value As String)
                strMa_nkt_cha = value
            End Set
        End Property

        Public Property Ma_ndkt() As String
            Get
                Return strMa_ndkt
            End Get
            Set(ByVal value As String)
                strMa_ndkt = value
            End Set
        End Property

        Public Property Ma_ndkt_cha() As String
            Get
                Return strMa_ndkt_cha
            End Get
            Set(ByVal value As String)
                strMa_ndkt_cha = value
            End Set
        End Property

        Public Property Ma_tlpc() As Integer
            Get
                Return strMa_tlpc
            End Get
            Set(ByVal value As Integer)
                strMa_tlpc = value
            End Set
        End Property

        Public Property Ma_khtk() As String
            Get
                Return strMa_khtk
            End Get
            Set(ByVal value As String)
                strMa_khtk = value
            End Set
        End Property

        Public Property Ma_nt() As String
            Get
                Return strMa_nt
            End Get
            Set(ByVal value As String)
                strMa_nt = value
            End Set
        End Property

        Public Property Loai_thue_hq() As String
            Get
                Return strLoai_thue_hq
            End Get
            Set(ByVal value As String)
                strLoai_thue_hq = value
            End Set
        End Property


        Public Sub New()

        End Sub

        Public Property PT_TT() As String
            Get
                Return _PT_TT
            End Get
            Set(ByVal Value As String)
                _PT_TT = Value
            End Set
        End Property

        Public Property ID() As String
            Get
                Return strDTL_ID
            End Get
            Set(ByVal Value As String)
                strDTL_ID = Value
            End Set
        End Property
        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property
        Public Property Ngay_KB() As Integer
            Get
                Return iNgay_KB
            End Get
            Set(ByVal Value As Integer)
                iNgay_KB = Value
            End Set
        End Property
        Public Property Ma_NV() As Integer
            Get
                Return iMa_NV
            End Get
            Set(ByVal Value As Integer)
                iMa_NV = Value
            End Set
        End Property
        Public Property So_BT() As Integer
            Get
                Return iSo_BT
            End Get
            Set(ByVal Value As Integer)
                iSo_BT = Value
            End Set
        End Property
        Public Property Ma_DThu() As String
            Get
                Return strMa_DThu
            End Get
            Set(ByVal Value As String)
                strMa_DThu = Value
            End Set
        End Property
        Public Property CCH_ID() As String
            Get
                Return iCCH_ID
            End Get
            Set(ByVal Value As String)
                iCCH_ID = Value
            End Set
        End Property
        Public Property Ma_Cap() As String
            Get
                Return strMa_Cap
            End Get
            Set(ByVal Value As String)
                strMa_Cap = Value
            End Set
        End Property
        Public Property Ma_Chuong() As String
            Get
                Return strMa_Chuong
            End Get
            Set(ByVal Value As String)
                strMa_Chuong = Value
            End Set
        End Property
        Public Property LKH_ID() As String
            Get
                Return iLKH_ID
            End Get
            Set(ByVal Value As String)
                iLKH_ID = Value
            End Set
        End Property
        Public Property Ma_Loai() As String
            Get
                Return strMa_Loai
            End Get
            Set(ByVal Value As String)
                strMa_Loai = Value
            End Set
        End Property
        Public Property Ma_Khoan() As String
            Get
                Return strMa_Khoan
            End Get
            Set(ByVal Value As String)
                strMa_Khoan = Value
            End Set
        End Property
        Public Property MTM_ID() As String
            Get
                Return iMTM_ID
            End Get
            Set(ByVal Value As String)
                iMTM_ID = Value
            End Set
        End Property

        Public Property Ma_Muc() As String
            Get
                Return strMa_Muc
            End Get
            Set(ByVal Value As String)
                strMa_Muc = Value
            End Set
        End Property
        Public Property Ma_TMuc() As String
            Get
                Return strMa_TMuc
            End Get
            Set(ByVal Value As String)
                strMa_TMuc = Value
            End Set
        End Property
        Public Property Noi_Dung() As String
            Get
                Return strNoi_Dung
            End Get
            Set(ByVal Value As String)
                strNoi_Dung = Value
            End Set
        End Property
        Public Property DT_ID() As String
            Get
                Return iDT_ID
            End Get
            Set(ByVal Value As String)
                iDT_ID = Value
            End Set
        End Property
        Public Property Ma_TLDT() As String
            Get
                Return strMa_TLDT
            End Get
            Set(ByVal Value As String)
                strMa_TLDT = Value
            End Set
        End Property

        Public Property Ky_Thue() As String
            Get
                Return strKy_Thue
            End Get
            Set(ByVal Value As String)
                strKy_Thue = Value
            End Set
        End Property
        Public Property SoTien() As Double
            Get
                Return dblSoTien
            End Get
            Set(ByVal Value As Double)
                dblSoTien = Value
            End Set
        End Property
        Public Property SoTien_NT() As Double
            Get
                Return dblSoTien_NT
            End Get
            Set(ByVal Value As Double)
                dblSoTien_NT = Value
            End Set
        End Property
        Public Property MaQuy() As String
            Get
                Return strMaQuy
            End Get
            Set(ByVal Value As String)
                strMaQuy = Value
            End Set
        End Property

        Public Property Ma_DP() As String
            Get
                Return strMaDP
            End Get
            Set(ByVal Value As String)
                strMaDP = Value
            End Set
        End Property
        Public Property SO_CT() As String
            Get
                Return strSO_CT
            End Get
            Set(ByVal Value As String)
                strSO_CT = Value
            End Set
        End Property
        Public Property SO_TK() As String
            Get
                Return strSo_TK
            End Get
            Set(ByVal Value As String)
                strSo_TK = Value
            End Set
        End Property
        Public Property NGAY_TK() As String
            Get
                Return dNgay_TK
            End Get
            Set(ByVal Value As String)
                dNgay_TK = Value
            End Set
        End Property
        Public Property LH_XNK() As String
            Get
                Return strLH_XNK
            End Get
            Set(ByVal Value As String)
                strLH_XNK = Value
            End Set
        End Property
        Public Property MA_HQ() As String
            Get
                Return strMa_HQ
            End Get
            Set(ByVal Value As String)
                strMa_HQ = Value
            End Set
        End Property
        Public Property LOAI_TT() As String
            Get
                Return strLoai_TT
            End Get
            Set(ByVal Value As String)
                strLoai_TT = Value
            End Set
        End Property
        Public Property SAC_THUE() As String
            Get
                Return strSacThue
            End Get
            Set(ByVal Value As String)
                strSacThue = Value
            End Set
        End Property
        Public Property MA_LT() As String
            Get
                Return _MA_LT
            End Get
            Set(ByVal value As String)
                _MA_LT = value
            End Set
        End Property
    End Class

End Namespace