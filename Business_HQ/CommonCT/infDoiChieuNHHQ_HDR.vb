﻿Public Class infDoiChieuNHHQ_HDR
    Private m_id As Long
    Private m_transaction_id As String
    Private m_transaction_type As String
    Private m_shkb As String
    Private m_ten_kb As String
    Private m_ngay_kb As Long
    Private m_so_bt As Long
    Private m_kyhieu_ct As String
    Private m_so_ct As String
    Private m_ngay_ct As Long
    Private m_ngay_bn As String
    Private m_ngay_bc As String
    Private m_ma_nnthue As String
    Private m_ten_nnthue As String
    Private m_ma_cqthu As String
    Private m_ten_cqthu As String
    Private m_so_tk As String
    Private m_ngay_tk As String
    Private m_lhxnk As String
    Private m_vt_lhxnk As String
    Private m_ten_lhxnk As String
    Private m_ttien As Long
    Private m_ttien_nt As Long
    Private m_trang_thai As String
    Private m_tk_ns As String
    Private m_ten_tk_ns As String
    Private m_ma_nkt As String
    Private m_so_bl As String
    Private m_ngay_bl_batdau As String
    Private m_ngay_bl_ketthuc As String
    Private m_songay_bl As Integer
    Private m_kieu_bl As String
    Private m_dien_giai As String
    Private m_accept_yn As String
    Private m_parent_transaction_id As String
    Private m_ma_hq_ph As String
    Private m_ma_loaitien As String
    Private m_tk_ns_hq As String
    Private m_ma_nh_ph As String
    Private m_ten_nh_ph As String
    Private m_ma_hq_cqt As String
    Private m_error_code_hq As String
    Private m_so_bt_hq As Long
    Private m_ly_do_huy As String
    Private m_ma_hq As String
    Private m_ten_hq As String
    Private m_ten_hq_ph As String
    '22/09/2012-ManhNV
    Private m_ma_nh_th As String
    Private m_tkkb As String
    Private m_tkkb_ct As String
    '27/09/2012-ManhNV
    Private m_ma_nv As String
    '30-05-2013
    Private m_ty_gia As String
    Private m_tg_id As String
    Private m_ma_nt As String
    Private m_so_tn_ct As String
    Private m_ngay_tn_ct As String
    Private m_ten_nh_th As String
    Private m_ten_dv_dd As String
    Private m_ma_dv_dd As String
    Private m_ma_ntk As String
    Private m_so_hd As String
    Private m_ngay_hd As String
    Private m_vtd As String
    Private m_ngay_vtd As String
    Private m_vtd2 As String
    Private m_ngay_vtd2 As String
    Private m_vtd3 As String
    Private m_ngay_vtd3 As String
    Private m_vtd4 As String
    Private m_ngay_vtd4 As String
    Private m_vtd5 As String
    Private m_ngay_vtd5 As String
    Private m_KENHGD As String = "TQ"
    


    Public Property id() As Long
        Get
            Return m_id
        End Get
        Set(ByVal value As Long)
            m_id = value
        End Set
    End Property

    Public Property transaction_id() As String
        Get
            Return m_transaction_id
        End Get
        Set(ByVal value As String)
            m_transaction_id = value
        End Set
    End Property
    Public Property so_hd() As String
        Get
            Return m_so_hd
        End Get
        Set(ByVal value As String)
            m_so_hd = value
        End Set
    End Property
    Public Property ngay_hd() As String
        Get
            Return m_ngay_hd
        End Get
        Set(ByVal value As String)
            m_ngay_hd = value
        End Set
    End Property
    Public Property transaction_type() As String
        Get
            Return m_transaction_type
        End Get
        Set(ByVal value As String)
            m_transaction_type = value
        End Set
    End Property

    Public Property shkb() As String
        Get
            Return m_shkb
        End Get
        Set(ByVal value As String)
            m_shkb = value
        End Set
    End Property

    Public Property ten_kb() As String
        Get
            Return m_ten_kb
        End Get
        Set(ByVal value As String)
            m_ten_kb = value
        End Set
    End Property

    Public Property ngay_kb() As Long
        Get
            Return m_ngay_kb
        End Get
        Set(ByVal value As Long)
            m_ngay_kb = value
        End Set
    End Property

    Public Property so_bt() As Long
        Get
            Return m_so_bt
        End Get
        Set(ByVal value As Long)
            m_so_bt = value
        End Set
    End Property

    Public Property kyhieu_ct() As String
        Get
            Return m_kyhieu_ct
        End Get
        Set(ByVal value As String)
            m_kyhieu_ct = value
        End Set
    End Property

    Public Property so_ct() As String
        Get
            Return m_so_ct
        End Get
        Set(ByVal value As String)
            m_so_ct = value
        End Set
    End Property

    Public Property ngay_ct() As Long
        Get
            Return m_ngay_ct
        End Get
        Set(ByVal value As Long)
            m_ngay_ct = value
        End Set
    End Property

    Public Property ngay_bn() As String
        Get
            Return m_ngay_bn
        End Get
        Set(ByVal value As String)
            m_ngay_bn = value
        End Set
    End Property

    Public Property ngay_bc() As String
        Get
            Return m_ngay_bc
        End Get
        Set(ByVal value As String)
            m_ngay_bc = value
        End Set
    End Property

    Public Property ma_nnthue() As String
        Get
            Return m_ma_nnthue
        End Get
        Set(ByVal value As String)
            m_ma_nnthue = value
        End Set
    End Property

    Public Property ten_nnthue() As String
        Get
            Return m_ten_nnthue
        End Get
        Set(ByVal value As String)
            m_ten_nnthue = value
        End Set
    End Property

    Public Property ma_cqthu() As String
        Get
            Return m_ma_cqthu
        End Get
        Set(ByVal value As String)
            m_ma_cqthu = value
        End Set
    End Property

    Public Property ten_cqthu() As String
        Get
            Return m_ten_cqthu
        End Get
        Set(ByVal value As String)
            m_ten_cqthu = value
        End Set
    End Property

    Public Property so_tk() As String
        Get
            Return m_so_tk
        End Get
        Set(ByVal value As String)
            m_so_tk = value
        End Set
    End Property

    Public Property ngay_tk() As String
        Get
            Return m_ngay_tk
        End Get
        Set(ByVal value As String)
            m_ngay_tk = value
        End Set
    End Property

    Public Property lhxnk() As String
        Get
            Return m_lhxnk
        End Get
        Set(ByVal value As String)
            m_lhxnk = value
        End Set
    End Property

    Public Property vt_lhxnk() As String
        Get
            Return m_vt_lhxnk
        End Get
        Set(ByVal value As String)
            m_vt_lhxnk = value
        End Set
    End Property

    Public Property ten_lhxnk() As String
        Get
            Return m_ten_lhxnk
        End Get
        Set(ByVal value As String)
            m_ten_lhxnk = value
        End Set
    End Property

    Public Property ttien() As Long
        Get
            Return m_ttien
        End Get
        Set(ByVal value As Long)
            m_ttien = value
        End Set
    End Property

    Public Property ttien_nt() As Long
        Get
            Return m_ttien_nt
        End Get
        Set(ByVal value As Long)
            m_ttien_nt = value
        End Set
    End Property

    Public Property trang_thai() As String
        Get
            Return m_trang_thai
        End Get
        Set(ByVal value As String)
            m_trang_thai = value
        End Set
    End Property

    Public Property tk_ns() As String
        Get
            Return m_tk_ns
        End Get
        Set(ByVal value As String)
            m_tk_ns = value
        End Set
    End Property

    Public Property ten_tk_ns() As String
        Get
            Return m_ten_tk_ns
        End Get
        Set(ByVal value As String)
            m_ten_tk_ns = value
        End Set
    End Property

    Public Property ma_nkt() As String
        Get
            Return m_ma_nkt
        End Get
        Set(ByVal value As String)
            m_ma_nkt = value
        End Set
    End Property

    Public Property so_bl() As String
        Get
            Return m_so_bl
        End Get
        Set(ByVal value As String)
            m_so_bl = value
        End Set
    End Property

    Public Property ngay_bl_batdau() As String
        Get
            Return m_ngay_bl_batdau
        End Get
        Set(ByVal value As String)
            m_ngay_bl_batdau = value
        End Set
    End Property

    Public Property ngay_bl_ketthuc() As String
        Get
            Return m_ngay_bl_ketthuc
        End Get
        Set(ByVal value As String)
            m_ngay_bl_ketthuc = value
        End Set
    End Property

    Public Property songay_bl() As Integer
        Get
            Return m_songay_bl
        End Get
        Set(ByVal value As Integer)
            m_songay_bl = value
        End Set
    End Property

    Public Property kieu_bl() As String
        Get
            Return m_kieu_bl
        End Get
        Set(ByVal value As String)
            m_kieu_bl = value
        End Set
    End Property

    Public Property dien_giai() As String
        Get
            Return m_dien_giai
        End Get
        Set(ByVal value As String)
            m_dien_giai = value
        End Set
    End Property

    Public Property accept_yn() As String
        Get
            Return m_accept_yn
        End Get
        Set(ByVal value As String)
            m_accept_yn = value
        End Set
    End Property

    Public Property parent_transaction_id() As String
        Get
            Return m_parent_transaction_id
        End Get
        Set(ByVal value As String)
            m_parent_transaction_id = value
        End Set
    End Property

    Public Property ma_hq_ph() As String
        Get
            Return m_ma_hq_ph
        End Get
        Set(ByVal value As String)
            m_ma_hq_ph = value
        End Set
    End Property

    Public Property ma_loaitien() As String
        Get
            Return m_ma_loaitien
        End Get
        Set(ByVal value As String)
            m_ma_loaitien = value
        End Set
    End Property

    Public Property tk_ns_hq() As String
        Get
            Return m_tk_ns_hq
        End Get
        Set(ByVal value As String)
            m_tk_ns_hq = value
        End Set
    End Property

    Public Property ma_nh_ph() As String
        Get
            Return m_ma_nh_ph
        End Get
        Set(ByVal value As String)
            m_ma_nh_ph = value
        End Set
    End Property

    Public Property ten_nh_ph() As String
        Get
            Return m_ten_nh_ph
        End Get
        Set(ByVal value As String)
            m_ten_nh_ph = value
        End Set
    End Property

    Public Property ma_hq_cqt() As String
        Get
            Return m_ma_hq_cqt
        End Get
        Set(ByVal value As String)
            m_ma_hq_cqt = value
        End Set
    End Property

    Public Property error_code_hq() As String
        Get
            Return m_error_code_hq
        End Get
        Set(ByVal value As String)
            m_error_code_hq = value
        End Set
    End Property

    Public Property so_bt_hq() As Long
        Get
            Return m_so_bt_hq
        End Get
        Set(ByVal value As Long)
            m_so_bt_hq = value
        End Set
    End Property

    Public Property ly_do_huy() As String
        Get
            Return m_ly_do_huy
        End Get
        Set(ByVal value As String)
            m_ly_do_huy = value
        End Set
    End Property
    Public Property ma_hq() As String
        Get
            Return m_ma_hq
        End Get
        Set(ByVal value As String)
            m_ma_hq = value
        End Set
    End Property
    Public Property ten_hq() As String
        Get
            Return m_ten_hq
        End Get
        Set(ByVal value As String)
            m_ten_hq = value
        End Set
    End Property
    Public Property ten_hq_ph() As String
        Get
            Return m_ten_hq_ph
        End Get
        Set(ByVal value As String)
            m_ten_hq_ph = value
        End Set
    End Property
    '22/09/20120 - ManhNV
    Public Property ma_nh_th() As String
        Get
            Return m_ma_nh_th
        End Get
        Set(ByVal value As String)
            m_ma_nh_th = value
        End Set
    End Property
    Public Property tkkb() As String
        Get
            Return m_tkkb
        End Get
        Set(ByVal value As String)
            m_tkkb = value
        End Set
    End Property
    Public Property tkkb_ct() As String
        Get
            Return m_tkkb_ct
        End Get
        Set(ByVal value As String)
            m_tkkb_ct = value
        End Set
    End Property
    Public Property ma_nv() As String
        Get
            Return m_ma_nv
        End Get
        Set(ByVal value As String)
            m_ma_nv = value
        End Set
    End Property
    Public Property ty_gia() As String
        Get
            Return m_ty_gia
        End Get
        Set(ByVal value As String)
            m_ty_gia = value
        End Set
    End Property
    Public Property tg_id() As String
        Get
            Return m_tg_id
        End Get
        Set(ByVal value As String)
            m_tg_id = value
        End Set
    End Property
    Public Property ma_nt() As String
        Get
            Return m_ma_nt
        End Get
        Set(ByVal value As String)
            m_ma_nt = value
        End Set
    End Property
    Public Property so_tn_ct() As String
        Get
            Return m_so_tn_ct
        End Get
        Set(ByVal value As String)
            m_so_tn_ct = value
        End Set
    End Property
    Public Property ngay_tn_ct() As String
        Get
            Return m_ngay_tn_ct
        End Get
        Set(ByVal value As String)
            m_ngay_tn_ct = value
        End Set
    End Property
    Public Property ten_nh_th() As String
        Get
            Return m_ten_nh_th
        End Get
        Set(ByVal value As String)
            m_ten_nh_th = value
        End Set
    End Property
    Public Property ma_dv_dd() As String
        Get
            Return m_ma_dv_dd
        End Get
        Set(ByVal value As String)
            m_ma_dv_dd = value
        End Set
    End Property
    Public Property ten_dv_dd() As String
        Get
            Return m_ten_dv_dd
        End Get
        Set(ByVal value As String)
            m_ten_dv_dd = value
        End Set
    End Property
    Public Property ma_ntk() As String
        Get
            Return m_ma_ntk
        End Get
        Set(ByVal value As String)
            m_ma_ntk = value
        End Set
    End Property
    Public Property vtd() As String
        Get
            Return m_vtd
        End Get
        Set(ByVal value As String)
            m_vtd = value
        End Set
    End Property
    Public Property ngay_vtd() As String
        Get
            Return m_ngay_vtd
        End Get
        Set(ByVal value As String)
            m_ngay_vtd = value
        End Set
    End Property
    Public Property vtd2() As String
        Get
            Return m_vtd2
        End Get
        Set(ByVal value As String)
            m_vtd2 = value
        End Set
    End Property
    Public Property ngay_vtd2() As String
        Get
            Return m_ngay_vtd2
        End Get
        Set(ByVal value As String)
            m_ngay_vtd2 = value
        End Set
    End Property
    Public Property vtd3() As String
        Get
            Return m_vtd3
        End Get
        Set(ByVal value As String)
            m_vtd3 = value
        End Set
    End Property
    Public Property ngay_vtd3() As String
        Get
            Return m_ngay_vtd3
        End Get
        Set(ByVal value As String)
            m_ngay_vtd3 = value
        End Set
    End Property
    Public Property vtd4() As String
        Get
            Return m_vtd4
        End Get
        Set(ByVal value As String)
            m_vtd4 = value
        End Set
    End Property
    Public Property ngay_vtd4() As String
        Get
            Return m_ngay_vtd4
        End Get
        Set(ByVal value As String)
            m_ngay_vtd4 = value
        End Set
    End Property
    Public Property vtd5() As String
        Get
            Return m_vtd5
        End Get
        Set(ByVal value As String)
            m_vtd5 = value
        End Set
    End Property
    Public Property ngay_vtd5() As String
        Get
            Return m_ngay_vtd5
        End Get
        Set(ByVal value As String)
            m_ngay_vtd5 = value
        End Set
    End Property
    Public Property KENHGD() As String
        Get
            Return m_KENHGD
        End Get
        Set(ByVal value As String)
            m_KENHGD = value
        End Set
    End Property
End Class
