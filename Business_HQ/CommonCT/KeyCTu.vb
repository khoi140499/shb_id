﻿Namespace NewChungTu

    Public Class KeyCTu
        Private p_SHKB As String
        Private p_Ngay_KB As Integer
        Private p_Ma_NV As Integer
        Private p_So_BT As Integer
        Private p_Ma_Dthu As String
        Private p_Kyhieu_CT As String
        Private p_So_CT As String
        Private p_TT_BDS As String
        Private p_TT_NSTT As String
        Private p_TT_TRUYVAN As String

        Public Property SHKB() As String
            Get
                Return p_SHKB
            End Get
            Set(ByVal Value As String)
                p_SHKB = Value
            End Set
        End Property

        Public Property Ngay_KB() As Integer
            Get
                Return p_Ngay_KB
            End Get
            Set(ByVal Value As Integer)
                p_Ngay_KB = Value
            End Set
        End Property
        Public Property Ma_NV() As Integer
            Get
                Return p_Ma_NV
            End Get
            Set(ByVal Value As Integer)
                p_Ma_NV = Value
            End Set
        End Property
        Public Property So_BT() As Integer
            Get
                Return p_So_BT
            End Get
            Set(ByVal Value As Integer)
                p_So_BT = Value
            End Set
        End Property
        Public Property Ma_Dthu() As String
            Get
                Return p_Ma_Dthu
            End Get
            Set(ByVal Value As String)
                p_Ma_Dthu = Value
            End Set
        End Property
        Public Property Kyhieu_CT() As String
            Get
                Return p_Kyhieu_CT
            End Get
            Set(ByVal Value As String)
                p_Kyhieu_CT = Value
            End Set
        End Property
        Public Property So_CT() As String
            Get
                Return p_So_CT
            End Get
            Set(ByVal Value As String)
                p_So_CT = Value
            End Set
        End Property
        Public Property TT_BDS() As String
            Get
                Return p_TT_BDS
            End Get
            Set(ByVal Value As String)
                p_TT_BDS = Value
            End Set
        End Property


        Public Property TT_NSTT() As String
            Get
                Return p_TT_NSTT
            End Get
            Set(ByVal Value As String)
                p_TT_NSTT = Value
            End Set
        End Property
        Public Property TT_TRUYVAN() As String
            Get
                Return p_TT_TRUYVAN
            End Get
            Set(ByVal Value As String)
                p_TT_TRUYVAN = Value
            End Set
        End Property
    End Class
End Namespace