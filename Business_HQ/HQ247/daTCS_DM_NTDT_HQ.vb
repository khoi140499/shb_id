﻿Imports System.Data.OracleClient
Namespace HQ247
    Public Class daTCS_DM_NTDT_HQ
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Shared Function TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA(ByVal pvStrID As String, ByVal pStrLyDoChuyenTra As String, ByVal pvStrTenDN As String) As Decimal
            Try
                'lay lai thong tin yeu cau va loai phan hoi la chap nhan yeu cau hay tu choi
                Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                strSQL &= "  id_nv_ck213='" & pvStrTenDN & "'"
                strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= " ,TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA & "'"
                strSQL &= " ,LY_DO_CHUYEN_TRA='" & pStrLyDoChuyenTra & "'"
                strSQL &= "  WHERE  ID='" & pvStrID & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA", "")
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Sub UpdateTCS_DM_NTDT_HQ_MSG311_DK_NH(ByVal pvStrSo_HS As String, ByVal pvStrSerialNumber As String, ByVal pvStrNoi_Cap As String, ByVal pvStrNgay_HL As String, _
                                                            ByVal pvStrNgay_HHL As String, ByVal pvStrPublicKey As String, ByVal pvStrMSGID311 As String, ByVal pvStrID As String)
            Dim conn As OracleConnection = VBOracleLib.DataAccess.GetConnection() ' New OracleConnection(VBOracleLib.DataAccess.strConnection)
            Dim myCMD As OracleCommand = New OracleCommand()
            Dim strSQL As String = ""
            Try
                'Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ SET "
                'huynt 
                strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                'huynt end
                strSQL &= " TRANGTHAI='13', "
                strSQL &= " SerialNumber='" & pvStrSerialNumber & "', "
                strSQL &= " NOICAP='" & pvStrNoi_Cap & "',"
                strSQL &= "  Ngay_HL=TO_DATE( '" & pvStrNgay_HL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "  Ngay_HHL=TO_DATE( '" & pvStrNgay_HHL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "  IDMSG311='" & pvStrMSGID311 & "',"
                ' ,to_date('" +  obj.Document.Data.ThongTin_NNT.ChungThuSo.Ngay_HL.Replace("T"," ") + "','RRRR-MM-DD HH24:MI:SS')
                'huynt
                strSQL &= "  NGAY_NHAN311 = to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS'),"
                strSQL &= "  NGAY_HT = to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS'),"
                strSQL &= "  ETAX_SERVICE ='Bổ sung TT NNT từ Msg 311',"
                'huynt end
                strSQL &= " PublicKey=:PublicKey "
                strSQL &= "  WHERE  So_HS='" & pvStrSo_HS & "'  AND TRANGTHAI='12' "

                ' conn.Open()
                myCMD.Connection = conn
                myCMD.CommandText = strSQL
                myCMD.CommandType = CommandType.Text
                myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pvStrPublicKey
                myCMD.ExecuteNonQuery()
                If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                    myCMD.Dispose()
                    conn.Dispose()
                    conn.Close()
                End If
            Catch ex As Exception
                Throw ex
            Finally
                If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                    conn.Close()
                End If
                myCMD.Dispose()
                conn.Dispose()
                'Insert sang bang HQ
                strSQL = "INSERT INTO TCS_DM_NTDT_HQ (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK)"
                strSQL &= " SELECT ID,SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN , CIFNO,TK_MA_CN,KENH_DK,IS_BK "
                strSQL &= " FROM TCS_DM_NTDT_HQ_311"
                strSQL &= " WHERE So_HS='" & pvStrSo_HS & "'  AND TRANGTHAI='13' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'Delete trong bang HQ_311
                strSQL = " DELETE FROM TCS_DM_NTDT_HQ_311"
                strSQL &= " WHERE So_HS = '" & pvStrSo_HS & "'  AND TRANGTHAI = '13' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
            End Try
        End Sub
        Public Shared Function UpdateMSG311_HUY_DK_HQ_NH(ByVal pvStrSo_HS As String, ByVal pvStrMSGID311 As String, ByVal pvStrID As String) As Decimal
            Try
                'kiem tra xem co o trang thai duoc phep thuc hien ko
                'neu dung thi cap nhat
                'cap nhat trang thai ve 01
                'cap nhat ma id 213 ve rong
                'cap nhat loaimsg213 ve 0 chua phan hoi 213
                'cap nhat id 312 rong
                'cap nhat 313 rong
                'cap nhat 311 theo thong tin nhan duoc
                'cap nhat loai ho so
                Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ SET "
                strSQL &= " trangthai='01',LoaiMSG213=0, "
                strSQL &= " Loai_HS='3',idmsg213='',idmsg312='',idmsg313='',id_nv_mk213='',id_nv_ck213='',id_nv_mk312='',id_nv_ck312='' , "
                strSQL &= "  MSGID311='" & pvStrMSGID311 & "'"
                strSQL &= " , NGAY_NHAN311=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= "  WHERE  So_HS='" & pvStrSo_HS & "' AND ID='" & pvStrID & "' AND TRANGTHAI='13' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UpdateMSG311_HUY_DK_HQ_NH", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        Public Shared Function UPDATE_TCS_HQ247_SEND213(ByVal pvStrTenDN As String, ByVal pvStrIDMSG213 As String, ByVal pvStrID As String) As Decimal
            Try
                'lay lai thong tin yeu cau va loai phan hoi la chap nhan yeu cau hay tu choi
                Dim strSQL As String = "SELECT * FROM  TCS_DM_NTDT_HQ_311 WHERE  ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim strLoai213 As String = "0"
                Dim strTrangThai As String = "01"
                Dim strLoaiHS As String = "1"
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            strLoai213 = ds.Tables(0).Rows(0)("LOAIMSG213").ToString()
                            strTrangThai = ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                            strLoaiHS = ds.Tables(0).Rows(0)("LOAI_HS").ToString()
                        End If
                    End If
                End If
                'huynt
                'Đoạn lệnh dưới kiểm tra điều kiện nhưng không đúng nghiệp vụ
                'If strLoai213.Equals("0") Or Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213) _
                'Or Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213_TUCHOI) Then
                '    VBOracleLib.LogDebug.WriteLog(New Exception("Lỗi không tìm thấy hồ sơ hoặc trạng thái hồ sơ không hợp lệ id[" & pvStrID & "][" & pvStrTenDN & "]"), "daTCS_DM_NTDT_HQ.UPDATE_TCS_HQ247_SEND213", "")
                '    Return Common.mdlCommon.TCS_HQ247_OK
                'End If
                'huynt
                'Sửa lại đoạn lệnh bên trên
                If strLoai213.Equals("0") Or (Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213) _
                And Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213_TUCHOI)) Then
                    log.Error("Lỗi không tìm thấy hồ sơ hoặc trạng thái hồ sơ không hợp lệ id[" & pvStrID & "][" & pvStrTenDN & "]")
                    ' VBOracleLib.LogDebug.WriteLog(New Exception("Lỗi không tìm thấy hồ sơ hoặc trạng thái hồ sơ không hợp lệ id[" & pvStrID & "][" & pvStrTenDN & "]"), "daTCS_DM_NTDT_HQ.UPDATE_TCS_HQ247_SEND213", "")
                    Return Common.mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE.ToString()
                End If

                strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                If strLoai213.Equals("1") Then
                    strSQL &= " trangthai='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_HQ_RESPONSE213 & "', "
                Else
                    strSQL &= " trangthai='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "', "
                End If
                strSQL &= "  id_nv_ck213='" & pvStrTenDN & "',idmsg213='" & pvStrIDMSG213 & "'"
                strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213 & "','" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213_TUCHOI & "') "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'doan nay danh cho tu choi yeu cau voi loai ho so la 2 va 3
                If strLoai213.Equals("2") And (strLoaiHS.Equals("2") Or strLoaiHS.Equals("3")) Then
                    'xoa dữ liệu trên bang HQ 311
                    strSQL = "DELETE FROM TCS_DM_NTDT_HQ_311 WHERE ID='" & pvStrID & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                    'cap nhat lai trang thai HQ ve active
                    strSQL = "UPDATE  TCS_DM_NTDT_HQ SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "' WHERE ID='" & pvStrID & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                End If
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_TCS_HQ247_SEND213", "")
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        'huynt
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="pvStrTenDN"></param>
        ''' <param name="pvStrIDMSG312"></param>
        ''' <param name="pvStrID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function UPDATE_TCS_HQ247_SEND312(ByVal pvStrTenDN As String, ByVal pvStrIDMSG312 As String, ByVal pvStrID As String, Optional ByVal strSoHS_HQ As String = "") As Decimal
            Try
                'lay lai thong tin yeu cau va loai phan hoi la chap nhan yeu cau hay tu choi
                Dim strSQL As String = "SELECT * FROM  TCS_DM_NTDT_HQ_311 WHERE  ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim strLoai213 As String = "0"
                Dim strTrangThai As String = "01"
                Dim strLoaiHS As String = "1"
                Dim strSo_HS As String = ""
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            strLoai213 = ds.Tables(0).Rows(0)("LOAIMSG213").ToString()
                            strTrangThai = ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                            strLoaiHS = ds.Tables(0).Rows(0)("LOAI_HS").ToString().Trim()
                            strSo_HS = ds.Tables(0).Rows(0)("SO_HS").ToString().Trim()
                        End If
                    End If
                Else
                    Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK 'Nếu xảy ra trường hợp này thì do không truy vấn được dữ liệu chứ không phải do không tồn tại số hồ sơ
                End If
                'Msg 313 nhận về có 3 trường hợp là của Khai mới, Khai sửa và Khai hủy
                'Ứng với mỗi trường hợp sẽ có xử lý khác nhau, do đó cần xác định được msg 313 là của trường hợp nào
                Select Case strLoaiHS
                    Case "1"
                        'Trường hợp 1: Msg 313 là của trường hợp Khai mới

                        'kiểm tra lại đoạn điều kiện này
                        'If strLoai213.Equals("0") Or (Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213) _
                        'And Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213_TUCHOI)) Then
                        '    VBOracleLib.LogDebug.WriteLog(New Exception("Lỗi không tìm thấy hồ sơ hoặc trạng thái hồ sơ không hợp lệ id[" & pvStrID & "][" & pvStrTenDN & "]"), "daTCS_DM_NTDT_HQ.UPDATE_TCS_HQ247_SEND213", "")
                        '    Return Common.mdlCommon.TCS_HQ247_OK
                        'End If

                        'Kiểm tra So_HS mà TCHQ gửi về xem nó đã có chưa, nếu có rồi chứng tỏ đây là Msg 313 TCHQ trả lời cho đăng ký mới tại TCHQ
                        If Not strSo_HS.Trim.Equals("") Then
                            'Trường hợp 313 TCHQ trả lời cho đăng ký mới tại TCHQ
                            'Chuyen trang thai len Active (13)
                            strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                            strSQL &= "  TRANGTHAI = '" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "',"
                            strSQL &= "  IDMSG313 = '" & pvStrID & "',TAIKHOAN_TH=CUR_TAIKHOAN_TH,TEN_TAIKHOAN_TH=CUR_TEN_TAIKHOAN_TH, "
                            strSQL &= "  id_nv_ck312='" & pvStrTenDN & "',idmsg312='" & pvStrIDMSG312 & "'"
                            strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                            strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI = ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312 & "') "
                            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                            'Insert sang bang HQ
                            'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
                            strSQL = "INSERT INTO TCS_DM_NTDT_HQ (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK)"
                            strSQL &= " SELECT '" & pvStrID & "',SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK "
                            strSQL &= " FROM TCS_DM_NTDT_HQ_311"
                            strSQL &= " WHERE ID = '" & pvStrID & "'"
                            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                            'Delete trong bang HQ_311
                            strSQL = " DELETE FROM TCS_DM_NTDT_HQ_311"
                            strSQL &= "  WHERE ID = '" & pvStrID & "'"
                            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                            ''doan nay danh cho tu choi yeu cau voi loai ho so la 2 va 3
                            'If strLoai213.Equals("2") And (strLoaiHS.Equals("2") Or strLoaiHS.Equals("3")) Then
                            '    'xoa dữ liệu trên bang HQ 311
                            '    strSQL = "DELETE FROM TCS_DM_NTDT_HQ_311 WHERE ID='" & pvStrID & "'"
                            '    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                            '    'cap nhat lai trang thai HQ ve active
                            '    strSQL = "UPDATE  TCS_DM_NTDT_HQ SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "' WHERE ID='" & pvStrID & "'"
                            '    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                            'End If
                            Return Common.mdlCommon.TCS_HQ247_OK
                        Else
                            Try
                                'Chuyen TRANGTHAI của Đăng ký từ '11' (Đã kiểm soát - chờ xác thực của TCHQ) lên '12' (Đã kiểm soát chờ bổ sung thông tin NNT)
                                strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                                strSQL &= "  TRANGTHAI = '" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "',"
                                strSQL &= "  SO_HS = '" & strSoHS_HQ & "',TAIKHOAN_TH=CUR_TAIKHOAN_TH,TEN_TAIKHOAN_TH=CUR_TEN_TAIKHOAN_TH,"
                                strSQL &= "  IDMSG313 = '" & pvStrID & "',"
                                strSQL &= "  id_nv_ck312='" & pvStrTenDN & "',idmsg312='" & pvStrIDMSG312 & "'"
                                strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                                strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI = ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312 & "') "
                                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                            Catch ex As Exception
                                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_TCS_HQ247_SEND312", "")
                                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                            End Try
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If


                    Case "2"
                        'Trường hợp 2: Msg 313 là của trường hợp Khai sửa
                        'Chuyen trang thai len Active (13)
                        strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                        strSQL &= "  TRANGTHAI = '" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "',TAIKHOAN_TH=CUR_TAIKHOAN_TH,TEN_TAIKHOAN_TH=CUR_TEN_TAIKHOAN_TH,"
                        strSQL &= "  IDMSG313 = '" & pvStrID & "',"
                        strSQL &= "  id_nv_ck312='" & pvStrTenDN & "',idmsg312='" & pvStrIDMSG312 & "'"
                        strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI = ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312 & "') "
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        'Kiểm tra số HS này xem đã có trong bảng HQ chưa, nếu có thì delete trước khi insert record  mới
                        Dim dsHQ As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet("SELECT SO_HS FROM TCS_DM_NTDT_HQ WHERE SO_HS='" & strSo_HS & "' AND TRANGTHAI = '16' ", CommandType.Text)
                        If Not dsHQ Is Nothing Then
                            If dsHQ.Tables.Count > 0 Then
                                If dsHQ.Tables(0).Rows.Count > 0 Then
                                    VBOracleLib.DataAccess.ExecuteNonQuery("DELETE FROM TCS_DM_NTDT_HQ WHERE  SO_HS='" & strSo_HS & "' AND TRANGTHAI = '16' ", CommandType.Text)
                                End If
                            End If
                        End If
                        'Insert sang bang HQ
                        'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
                        strSQL = "INSERT INTO TCS_DM_NTDT_HQ (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK)"
                        strSQL &= " SELECT '" & pvStrID & "',SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK"
                        strSQL &= " FROM TCS_DM_NTDT_HQ_311"
                        strSQL &= " WHERE ID = '" & pvStrID & "'"
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                        'Delete trong bang HQ_311
                        strSQL = " DELETE FROM TCS_DM_NTDT_HQ_311"
                        strSQL &= "  WHERE ID = '" & pvStrID & "'"
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        Return Common.mdlCommon.TCS_HQ247_OK
                    Case "3"
                        'Trường hợp 3: Msg 313 là của trường hợp Khai hủy

                        'Chuyen trang thai len Active (13)
                        strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                        strSQL &= "  TRANGTHAI = '" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_NGUNG_DANGKY & "',"
                        strSQL &= "  IDMSG313 = '" & pvStrID & "',"
                        strSQL &= "  id_nv_ck312='" & pvStrTenDN & "',idmsg312='" & pvStrIDMSG312 & "'"
                        strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI = ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312 & "') "
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                        'Kiểm tra số HS này xem đã có trong bảng HQ chưa, nếu có thì delete trước khi insert record  mới
                        Dim dsHQ As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet("SELECT SO_HS FROM TCS_DM_NTDT_HQ WHERE SO_HS='" & strSo_HS & "' AND TRANGTHAI = '16' ", CommandType.Text)
                        If Not dsHQ Is Nothing Then
                            If dsHQ.Tables.Count > 0 Then
                                If dsHQ.Tables(0).Rows.Count > 0 Then
                                    VBOracleLib.DataAccess.ExecuteNonQuery("DELETE FROM TCS_DM_NTDT_HQ WHERE  SO_HS='" & strSo_HS & "' AND TRANGTHAI = '16' ", CommandType.Text)
                                End If
                            End If
                        End If

                        'Insert sang bang HQ
                        'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
                        strSQL = "INSERT INTO TCS_DM_NTDT_HQ (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK)"
                        strSQL &= " SELECT '" & pvStrID & "',SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK"
                        strSQL &= " FROM TCS_DM_NTDT_HQ_311"
                        strSQL &= " WHERE ID = '" & pvStrID & "'"
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                        'Delete trong bang HQ_311
                        strSQL = " DELETE FROM TCS_DM_NTDT_HQ_311"
                        strSQL &= "  WHERE ID = '" & pvStrID & "'"
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        Return Common.mdlCommon.TCS_HQ247_OK
                    Case Else
                        Return Common.mdlCommon.TCS_HQ247_HQ_SAI_LOAI_HS 'Sai loại hồ sơ
                End Select

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_TCS_HQ247_SEND312", "")
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="htStrSoHS"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function UPDATE_TCS_HQ247_GET313(ByVal htStrSoHS As String) As Decimal
            Dim strSQL As String = ""
            Try
                strSQL = "UPDATE  TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "' WHERE SO_HS = '" & htStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_TCS_HQ247_GET313", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="pvStrSo_HS"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function UPDATE_HQ_GET311_LOAI_HS_2(ByVal pvStrSo_HS As String) As Decimal
            Try
                Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ SET "
                strSQL &= " TRANGTHAI = '16'"
                strSQL &= "  WHERE  So_HS = '" & pvStrSo_HS & "' AND TRANGTHAI='13' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_TCS_HQ_GET311_LOAI_HS_2", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="pvStrSo_HS"></param>
        ''' <param name="pvStrCUR_TEN_TAIKHOAN_TH"></param>
        ''' <param name="pvStrCUR_TAIKHOAN_TH"></param>
        ''' <param name="pvStrTRANGTHAI"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function UPDATE_HQ311_GET311_LOAI_HS_2(ByVal pvStrSo_HS As String, ByVal pvStrCUR_TEN_TAIKHOAN_TH As String, ByVal pvStrCUR_TAIKHOAN_TH As String, ByVal pvStrTRANGTHAI As String) As Decimal
            Try
                Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                strSQL &= " CUR_TEN_TAIKHOAN_TH = '" & pvStrCUR_TEN_TAIKHOAN_TH & "'"
                strSQL &= " ,CUR_TAIKHOAN_TH = '" & pvStrCUR_TAIKHOAN_TH & "'"
                strSQL &= " ,TRANGTHAI = '" & pvStrTRANGTHAI & "'"
                strSQL &= "  WHERE  So_HS = '" & pvStrSo_HS & "' AND LOAI_HS = '2'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_HQ311_GET311_LOAI_HS_2", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        ''' <summary>
        ''' Update TRANGTHAI của HS trong bảng TCS_DM_NTDT_HQ (HQ) từ 13 lên 16 (Tạm khóa - Chờ XL thay đổi của NNT)
        ''' </summary>
        ''' <param name="pvStrSo_HS">Số hồ sơ cần thay đổi</param>
        ''' <returns>0: nếu thành công, !0 nếu lỗi</returns>
        ''' <remarks></remarks>
        Public Shared Function UPDATE_HQ_GET311_LOAI_HS_3(ByVal pvStrSo_HS As String) As Decimal
            Try
                Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ SET "
                strSQL &= " TRANGTHAI = '16'"
                strSQL &= "  WHERE  So_HS = '" & pvStrSo_HS & "' AND TRANGTHAI='13' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_HQ_GET311_LOAI_HS_3", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="pvStrSo_HS"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function UPDATE_HQ311_GET311_LOAI_HS_3(ByVal pvStrSo_HS As String) As Decimal
            Try
                Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                strSQL &= " TRANGTHAI = '01'"
                strSQL &= "  WHERE  So_HS = '" & pvStrSo_HS & "' AND LOAI_HS = '3' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_HQ311_GET311_LOAI_HS_3", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        ''' <summary>
        ''' Đăng ký mới tại NHTM, Insert một Đăng ký mới vào bảng HQ_311 với TRANGTHAI = 09 (Chờ kiểm soát)
        ''' </summary>
        ''' <param name="strMaDV">Mã số thuế của người đại diện doanh nghiệp XNK (NNT)</param>
        ''' <param name="strTenDV">Tên doanh nghiệp XNK</param>
        ''' <param name="strDiaChi">Địa chỉ Doanh nghiệp XNK</param>
        ''' <param name="strSoCMT">Số CMTND của NNT</param>
        ''' <param name="strHoTen">Họ tên NNT</param>
        ''' <param name="strNgaySinh">Ngày sinh NNT</param>
        ''' <param name="strNguyenQuyan">Nguyên quán NNT</param>
        ''' <param name="strSoDT">Số điện thoại của NNT</param>
        ''' <param name="strEmal">Email NNT</param>
        ''' <param name="strSoDT1">Số ĐT bổ sung của NNT</param>
        ''' <param name="strEmail1">Email bổ sung của NNT</param>
        ''' <param name="strMaNH">Mã của NH - nơi NNT đã đăng ký tài khoản sử dụng NT</param>
        ''' <param name="strTenNganHang">Tên ngân hàng - nơi NNT đã đăng ký tài khoản sử dụng NT</param>
        ''' <param name="strTenDN">Tên đăng nhập của NSD</param>
        ''' <param name="strTaiKhoanChck">Số tài khoản NH mà NNT muốn đăng ký NTDT</param>
        ''' <param name="strTenTaiKhoanCheck">Tên tài khoản NH ứng với số tài khoản mà NNT muốn đăng ký NTDT</param>
        ''' <param name="strNgayKyHDUQ">Ngày ký ủy quyền (trong hợp đồng mà NNT đã ký với NH)</param>
        ''' <returns>0: Nếu Insert đăng ký mới thành công, !0 nếu thất bại</returns>
        ''' <remarks></remarks>
        Public Shared Function MakerMSG312(ByVal strMaDV As String, ByVal strTenDV As String, ByVal strDiaChi As String, ByVal strSoCMT As String, ByVal strHoTen As String, ByVal strNgaySinh As String, ByVal strNguyenQuyan As String, ByVal strSoDT As String, ByVal strEmal As String, ByVal strSoDT1 As String, ByVal strEmail1 As String, ByVal strTaiKhoan_TH As String, ByVal strTenTaiKhoan_TH As String, ByVal strMaNH As String, ByVal strTenNganHang As String, ByVal strTenDN As String, ByVal strTaiKhoanChck As String, ByVal strTenTaiKhoanCheck As String, ByVal strNgayKyHDUQ As String) As Decimal
            'Trường hợp xác định là đăng ký mới tại NHTM
            Try
                Dim strDateCover As String = strNgaySinh.Split("/")(2) & strNgaySinh.Split("/")(1) & strNgaySinh.Split("/")(0)
                Dim strNgayKyHDUQCover As String = strNgayKyHDUQ.Split("/")(2) & strNgayKyHDUQ.Split("/")(1) & strNgayKyHDUQ.Split("/")(0)
                Dim strSQL As String = " INSERT INTO TCS_DM_NTDT_HQ_311 ("
                strSQL &= " ID, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT"
                strSQL &= " ,HO_TEN, NGAYSINH, NGUYENQUAN, SO_DT"
                strSQL &= " ,EMAIL, SO_DT1, EMAIL1"
                strSQL &= " , TAIKHOAN_TH, TEN_TAIKHOAN_TH"
                strSQL &= " , MA_NH_TH, TEN_NH_TH"
                strSQL &= " , TRANGTHAI"
                strSQL &= " , ID_NV_MK213"
                strSQL &= " , CUR_TAIKHOAN_TH"
                strSQL &= " ,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY"
                strSQL &= " , NGAY_HT, LOAIMSG213, MA_CN, NGAY_KY_HDUQ,KENH_DK"
                strSQL &= " )"
                strSQL &= " VALUES (TO_CHAR(SYSDATE,'RRRRMMDD')||LPAD(SEQ_TCS_DM_NTDT_HQ.NEXTVAL,10,'0'), '1', '" & strMaDV & "', '" & strTenDV & "', '" & strDiaChi & "', '" & strSoCMT & "'"
                strSQL &= " ,'" & strHoTen & "', to_date('" & strDateCover & "','RRRRMMDDHH24MISS'), '" & strNguyenQuyan & "', '" & strSoDT & "'"
                strSQL &= " ,'" & strEmal & "', '" & strSoDT1 & "', '" & strEmail1 & "'"
                strSQL &= " , '" & strTaiKhoan_TH & "', '" & strTenTaiKhoan_TH & "'"
                strSQL &= " , '" & strMaNH & "', '" & strTenNganHang & "'"
                strSQL &= " , '09' "
                strSQL &= " , '" & strTenDN & "'"
                strSQL &= " , '" & strTaiKhoanChck & "'"
                strSQL &= " ,'" & strTenTaiKhoanCheck & "', ''"
                strSQL &= " , to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') , '1', '___',to_date('" & strNgayKyHDUQCover & "','RRRRMMDDHH24MISS'),'1'"
                strSQL &= " )"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            End Try
        End Function
        ''' <summary>
        ''' Update TRANGTHAI của bảng HQ hoặc HQ_311
        ''' </summary>
        ''' <param name="HQTableName">Tên bảng HQ hoặc HQ_311</param>
        ''' <param name="trangThai">Mã trạng thái</param>
        ''' <param name="soHS">Số hồ sơ</param>
        ''' <param name="ID">ID hồ sơ (từ Databse)</param>
        ''' <param name="tenDN">Tên đăng nhập của NSD</param>
        ''' <param name="whereClause">Thêm điều kiện nếu cần</param>
        ''' <returns>0: Nếu cập nhật thành công, !0 nếu không thành công</returns>
        ''' <remarks>ID của hồ sơ từ Database</remarks>
        Public Shared Function UPDATE_TRANGTHAI(ByVal HQTableName As String, ByVal trangThai As String, ByVal soHS As String, ByVal ID As String, ByVal tenDN As String, Optional ByVal whereClause As String = " (1=1) ") As Decimal
            Try
                Dim strSQL As String = ""
                strSQL = "SELECT ID, SO_HS, TRANGTHAI FROM " & HQTableName & " WHERE ID = '" & ID & "' AND SO_HS = '" & soHS & "'"
                'Kiểm tra trường hợp đăng ký từ TCHQ hay từ NHTM
                Dim dsHQ311 As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not dsHQ311 Is Nothing Then
                    If dsHQ311.Tables.Count > 0 Then
                        If dsHQ311.Tables(0).Rows.Count > 0 Then
                            strSQL = "UPDATE " & HQTableName & " SET "
                            strSQL &= " TRANGTHAI = '" & trangThai & "'"
                            ' strSQL &= " ,ID_NV_CK213='" & tenDN & "' "
                            ' strSQL &= " ,ID_NV = '" & tenDN & "' "
                            strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                            strSQL &= " WHERE SO_HS = '" & soHS & "' AND ID = '" & ID & "' AND "
                            strSQL &= whereClause
                            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If
                    Else
                        'Trường hợp xác định là Chuyển trả, khi đăng ký mới tại đầu NHTM
                        strSQL = "UPDATE " & HQTableName & " SET "
                        strSQL &= " TRANGTHAI = '" & trangThai & "'"
                        strSQL &= " ,ID_NV_CK312='" & tenDN & "' "
                        strSQL &= " ,ID_NV = '" & tenDN & "' "
                        strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                        strSQL &= " WHERE SO_HS IS NULL AND ID = '" & ID & "' AND "
                        strSQL &= whereClause
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                        Return Common.mdlCommon.TCS_HQ247_OK
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_TRANGTHAI", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        ''' <summary>
        ''' Kiểm tra số hồ sơ đã tồn tại hay chưa trên bảng HQ hoặc HQ_311 truyền vào
        ''' </summary>
        ''' <param name="strSoHS">Số hồ sơ cần kiểm tra</param>
        ''' <param name="strHQTableName">Tên bảng HQ hoặc HQ_311</param>
        ''' <param name="strWhereClause"> điều kiện kiểm tra</param>
        ''' <returns> boolRtn</returns>
        ''' <remarks></remarks>
        Public Shared Function checkSoHS(ByRef boolRtn As Boolean, ByVal strSoHS As String, ByVal strHQTableName As String, Optional ByVal strWhereClause As String = " (1=1) ") As Decimal
            Try
                Dim strSQL As String = "SELECT SO_HS FROM  " & strHQTableName & " "
                strSQL &= " WHERE SO_HS = '" & strSoHS & "' AND "
                strSQL &= strWhereClause
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If ds.Tables(0).Rows(0)("SO_HS").Equals("" & strSoHS & "") Then
                                boolRtn = True
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                boolRtn = False
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.checkSoHS", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        ''' <summary>
        ''' Trả về mã chi nhánh của nhân viên NH
        ''' </summary>
        ''' <param name="strCodeBranch">Mã chi nhánh trả về</param>
        ''' <param name="strUsername">Tên đăng nhập của nhân viên (Username)</param>
        ''' <returns>Trả về Mã chi nhánh của nhân viên có Username thông qua tham biến strCodeBranch, Hàm trả về 0 nếu truy vấn thành công và khác 0 nếu ngược lại </returns>
        ''' <remarks></remarks>
        Public Shared Function getCodebranch(ByRef strCodeBranch As String, ByVal strUsername As String) As Decimal
            Try
                Dim strSQL As String = "SELECT TEN_DN, MA_CN FROM TCS_DM_NHANVIEN "
                strSQL &= " WHERE TEN_DN = '" & strUsername & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            strCodeBranch = ds.Tables(0).Rows(0)("MA_CN").ToString()
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.getCodebranch", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        'huynt end
        Public Shared Function fnc_checkTrangThaiHS(ByVal pStrSoHS As String, ByVal pStrLoaiHS As String) As Decimal
            Dim decResult As Decimal = Common.mdlCommon.TCS_HQ247_OK
            Try
                'Dim strSQL As String = "select * from tcs_dm_ntdt_hq_311 where so_hs='" & pStrSoHS & "' and trangthai not in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "','" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "','" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312 & "') "
                'Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim strSQL As String = ""
                Dim ds As DataSet = New DataSet()

                'co lenh dang cho thanh toan
                strSQL = "select count(1) from tcs_hq247_304 where so_hs='" & pStrSoHS & "' AND  trangthai not in('" & Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_OK & "','" & Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI & "')"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)

                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            'Return Common.mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE
                            If Not ds.Tables(0).Rows(0)(0).ToString().Equals("0") Then
                                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                            End If
                        End If
                    End If
                End If
                'neu la loai ho so huy thi phai check lenh thanh toan
                'If pStrLoaiHS.Equals("3") Then
                '    'co lenh dang cho thanh toan
                '    strSQL = "select count(1) from tcs_hq247_304 where so_hs='" & pStrSoHS & "' where trangthai not in('" & Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_OK & "','" & Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI & "')"
                '    ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                '    If Not ds Is Nothing Then
                '        If ds.Tables.Count > 0 Then
                '            If ds.Tables(0).Rows.Count > 0 Then
                '                If ds.Tables(0).Rows(0)(0).ToString().Equals("0") Then
                '                    Return Common.mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE
                '                End If
                '            End If
                '        End If
                '    End If
                'End If
                ''kiem tra trong bang HQ
                If pStrLoaiHS.Equals("2") Then
                    strSQL = "select count(1) from  tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' AND trangthai  in('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "')"
                    ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                    decResult = Common.mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE
                    If Not ds Is Nothing Then
                        If ds.Tables.Count > 0 Then
                            If ds.Tables(0).Rows.Count > 0 Then
                                If Integer.Parse(ds.Tables(0).Rows(0)(0).ToString()) > 0 Then
                                    Return Common.mdlCommon.TCS_HQ247_OK
                                End If
                            End If
                        End If
                    End If
                    Return 2
                End If
                'If pStrLoaiHS.Equals("3") Then
                '    strSQL = "select count(1) from  tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' AND trangthai  in('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "','" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_NGUNG_DANGKY & "')"
                '    ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                '    decResult = Common.mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE
                '    If Not ds Is Nothing Then
                '        If ds.Tables.Count > 0 Then
                '            If ds.Tables(0).Rows.Count > 0 Then
                '                If Integer.Parse(ds.Tables(0).Rows(0)(0).ToString()) > 0 Then
                '                    decResult = Common.mdlCommon.TCS_HQ247_OK
                '                End If
                '            End If
                '        End If
                '    End If
                'End If
                'If pStrLoaiHS.Equals("2") Then
                '    strSQL = "select sum(x) from "
                '    strSQL &= " (select count(1) x from  tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' AND trangthai  in('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "','" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH & "')"
                '    strSQL &= " union all"
                '    strSQL &= " select count(1) x from  tcs_dm_ntdt_hq_311 where so_hs='" & pStrSoHS & "' AND trangthai  in('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213 & "','" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "'))"
                '    ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                '    decResult = Common.mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE
                '    If Not ds Is Nothing Then
                '        If ds.Tables.Count > 0 Then
                '            If ds.Tables(0).Rows.Count > 0 Then
                '                If Integer.Parse(ds.Tables(0).Rows(0)(0).ToString()) > 0 Then
                '                    decResult = Common.mdlCommon.TCS_HQ247_OK
                '                End If
                '            End If
                '        End If
                '    End If
                'End If
            Catch ex As Exception

            End Try
            Return decResult
        End Function

        Public Shared Function fnc_UpdateHuy311_fromHQ(ByVal pStrSoHS As String, ByVal pStrLoaiHS As String, ByVal pStrMSG311ID As String, ByVal pStrMSGContent As String, ByVal AccountHQ247 As String) As Decimal
            Try
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ WHERE SO_HS='" & pStrSoHS & "'"
                'em kiem tra xem bang HQ co khong
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim strCheck As Decimal = 0
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        strCheck = ds.Tables(0).Rows.Count
                    End If

                End If
                If strCheck > 0 Then
                    strSQL = "UPDATE tcs_dm_ntdt_hq set "
                    strSQL &= " LOAI_HS='" & pStrLoaiHS & "' "
                    strSQL &= ", TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_NGUNG_DANGKY & "' "
                    strSQL &= ", IDMSG312='' "
                    strSQL &= ", IDMSG313='' "
                    strSQL &= ", IDMSG213='' "
                    strSQL &= ", ID_NV_MK213='' "
                    strSQL &= ", ID_NV_CK213='' "
                    strSQL &= ", ID_NV_MK312='" & AccountHQ247 & "' "
                    strSQL &= ", ID_NV_CK312='" & AccountHQ247 & "' "
                    strSQL &= ", IDMSG311='" & pStrMSG311ID & "' "
                    strSQL &= ", NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS') "
                    strSQL &= ", NGAY_HT=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS') "
                    strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                    VBOracleLib.DataAccess.ExecuteNonQuery("DELETE FROM tcs_dm_ntdt_hq_311 WHERE SO_HS='" & pStrSoHS & "'", CommandType.Text)
                Else
                    strSQL = "UPDATE tcs_dm_ntdt_hq_311 set "
                    strSQL &= " LOAI_HS='" & pStrLoaiHS & "' "
                    strSQL &= ", TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_NGUNG_DANGKY & "' "
                    strSQL &= ", IDMSG312='' "
                    strSQL &= ", IDMSG313='' "
                    strSQL &= ", IDMSG213='' "
                    strSQL &= ", ID_NV_MK213='' "
                    strSQL &= ", ID_NV_CK213='' "
                    strSQL &= ", ID_NV_MK312='" & AccountHQ247 & "' "
                    strSQL &= ", ID_NV_CK312='" & AccountHQ247 & "' "
                    strSQL &= ", IDMSG311='" & pStrMSG311ID & "' "
                    strSQL &= ", NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS') "
                    strSQL &= ", NGAY_HT=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS') "
                    strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                    'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
                    strSQL = "INSERT INTO TCS_DM_NTDT_HQ (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK)"
                    strSQL &= " SELECT ID,SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK "
                    strSQL &= " FROM TCS_DM_NTDT_HQ_311"
                    strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)


                End If
                'Delete trong bang HQ_311
                strSQL = " DELETE FROM TCS_DM_NTDT_HQ_311"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'neu khong co thi 
                Return Common.mdlCommon.TCS_HQ247_OK

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_UpdateHuy311_fromHQ: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_CheckSua311_DKNH(ByVal pStrSoHS As String) As Decimal
            Try
                Dim strSQL As String = "select * from tcs_dm_ntdt_hq_311 where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "') "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_CheckSua311_DKNH: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_CheckSua311_CoSuaTK(ByVal pStrSoHS As String, ByVal pStrTAIKHOAN_TH As String) As Decimal
            Try
                Dim strSQL As String = "select * from tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "') and TAIKHOAN_TH<>'" & pStrTAIKHOAN_TH & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog("fnc_CheckSua311_CoSuaTK: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_CheckSua311_CoSuaTK_TamKhoa(ByVal pStrSoHS As String, ByVal pStrTAIKHOAN_TH As String) As Decimal
            Try
                Dim strSQL As String = "select * from tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH & "') and TAIKHOAN_TH<>'" & pStrTAIKHOAN_TH & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_CheckSua311_CoSuaTK: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function

        'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
        '20180122_Nhamlt them ham
        Public Shared Function fnc_getKenhDK_BySoHS_KhaiSua(ByVal pStrSoHS As String) As String
            Try
                Dim strSQL As String = "select KENH_DK from tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return ds.Tables(0).Rows(0)("KENH_DK").ToString
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_CheckSua311_CoSuaTK: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return "-1"
        End Function
        'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
        '20180122_Nhamlt them ham 
        Public Shared Function fnc_getKenhDK_BySoHS_KhaiHuyTuTCHQ(ByVal pStrSoHS As String) As String
            Try
                Dim strSQL As String = " select KENH_DK from tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' "
                strSQL &= " UNION ALL"
                strSQL &= " select KENH_DK from tcs_dm_ntdt_hq_311 where so_hs='" & pStrSoHS & "' "

                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return ds.Tables(0).Rows(0)("KENH_DK").ToString
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_CheckSua311_CoSuaTK: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return "-1"
        End Function

        Public Shared Function fnc_UpdateSua311_DKNH(ByVal pStrSoHS As String, ByVal pStrLoaiHS As String, _
                                                        ByVal pStrMA_DV As String, ByVal pStrTEN_DV As String, ByVal pStrDIACHI As String, ByVal pStrSO_CMT As String, _
                                                        ByVal pStrHO_TEN As String, ByVal pStrNGAYSINH As String, ByVal pStrNGUYENQUAN As String, ByVal pStrTHONGTINLIENHE As String, _
                                                        ByVal pStrSO_DT As String, ByVal pStrEMAIL As String, ByVal pStrSERIALNUMBER As String, ByVal pStrNOICAP As String, _
                                                        ByVal pStrNGAY_HL As String, ByVal pStrNGAY_HHL As String, ByVal pStrPUBLICKEY As String, ByVal pStrMA_NH_TH As String, _
                                                        ByVal pStrTEN_NH_TH As String, ByVal pStrTAIKHOAN_TH As String, ByVal pStrTEN_TAIKHOAN_TH As String, ByVal pStrIDMSG311 As String) As Decimal

            Try
                Dim conn As OracleConnection = VBOracleLib.DataAccess.GetConnection() 'New OracleConnection(VBOracleLib.DataAccess.strConnection)
                Dim myCMD As OracleCommand = New OracleCommand()
                Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                'cap nhat bang HQ_311
                strSQL &= " MA_DV='" & pStrMA_DV & "'"
                strSQL &= ",TEN_DV='" & pStrTEN_DV.Replace("'", "''") & "'"
                strSQL &= ",DIACHI='" & pStrDIACHI & "'"
                strSQL &= ",SO_CMT='" & pStrSO_CMT & "'"
                strSQL &= ",HO_TEN='" & pStrHO_TEN.Replace("'", "''") & "'"
                strSQL &= ",NGAYSINH=to_date('" & pStrNGAYSINH & "','RRRR-MM-DD')"
                strSQL &= ",NGUYENQUAN ='" & pStrNGUYENQUAN & "'"
                strSQL &= ",    THONGTINLIENHE=:THONGTINLIENHE"
                strSQL &= ",SO_DT='" & pStrSO_DT & "'"
                strSQL &= ",EMAIL='" & pStrEMAIL & "'"
                strSQL &= ",SERIALNUMBER='" & pStrSERIALNUMBER & "'"
                strSQL &= ",NOICAP ='" & pStrNOICAP & "'"
                strSQL &= ",NGAY_HL =to_date('" & pStrNGAY_HL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                strSQL &= ",NGAY_HHL=to_date('" & pStrNGAY_HHL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                strSQL &= ",    PUBLICKEY=:PUBLICKEY"
                strSQL &= ",MA_NH_TH='" & pStrMA_NH_TH & "'"
                strSQL &= ",TEN_NH_TH ='" & pStrTEN_NH_TH & "'"
                strSQL &= ",TAIKHOAN_TH='" & pStrTAIKHOAN_TH & "'"
                strSQL &= ",TEN_TAIKHOAN_TH='" & pStrTEN_TAIKHOAN_TH.Replace("'", "''") & "'"
                strSQL &= ",TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "'"
                strSQL &= ",IDMSG311='" & pStrIDMSG311 & "'"
                strSQL &= ",NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= ",NGAY_HT =to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                'insert bang HQ
                'xoa bang HQ_311
                Try
                    ' conn.Open()
                    myCMD.Connection = conn
                    myCMD.CommandText = strSQL
                    myCMD.CommandType = CommandType.Text
                    myCMD.Parameters.Add("THONGTINLIENHE", OracleType.Clob).Value = pStrTHONGTINLIENHE
                    myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pStrPUBLICKEY
                    myCMD.ExecuteNonQuery()
                    If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                        myCMD.Dispose()
                        conn.Dispose()
                        conn.Close()
                    End If
                Catch ex As Exception
                Finally
                    If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                        conn.Close()
                    End If
                    myCMD.Dispose()
                    conn.Dispose()
                End Try

                'Insert sang bang HQ
                'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
                strSQL = "INSERT INTO TCS_DM_NTDT_HQ (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK)"
                strSQL &= " SELECT ID,SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,KENH_DK,IS_BK "
                strSQL &= " FROM TCS_DM_NTDT_HQ_311"
                strSQL &= " WHERE So_HS='" & pStrSoHS & "'  "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'Delete trong bang HQ_311
                strSQL = " DELETE FROM TCS_DM_NTDT_HQ_311"
                strSQL &= " WHERE So_HS = '" & pStrSoHS & "'  "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception

            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_UpdateSua311_HQ(ByVal pStrSoHS As String, ByVal pStrLoaiHS As String, _
                                                        ByVal pStrMA_DV As String, ByVal pStrTEN_DV As String, ByVal pStrDIACHI As String, ByVal pStrSO_CMT As String, _
                                                        ByVal pStrHO_TEN As String, ByVal pStrNGAYSINH As String, ByVal pStrNGUYENQUAN As String, ByVal pStrTHONGTINLIENHE As String, _
                                                        ByVal pStrSO_DT As String, ByVal pStrEMAIL As String, ByVal pStrSERIALNUMBER As String, ByVal pStrNOICAP As String, _
                                                        ByVal pStrNGAY_HL As String, ByVal pStrNGAY_HHL As String, ByVal pStrPUBLICKEY As String, ByVal pStrMA_NH_TH As String, _
                                                        ByVal pStrTEN_NH_TH As String, ByVal pStrTAIKHOAN_TH As String, ByVal pStrTEN_TAIKHOAN_TH As String, ByVal pStrIDMSG311 As String, ByVal pStrAccountHQ247 As String) As Decimal

            Try
                Dim conn As OracleConnection = VBOracleLib.DataAccess.GetConnection() ' New OracleConnection(VBOracleLib.DataAccess.strConnection)
                Dim myCMD As OracleCommand = New OracleCommand()
                Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ SET "
                'cap nhat bang HQ_311
                strSQL &= " LOAI_HS='" & pStrLoaiHS & "' "
                strSQL &= ",MA_DV='" & pStrMA_DV & "'"
                strSQL &= ",TEN_DV='" & pStrTEN_DV.Replace("'", "''") & "'"
                strSQL &= ",DIACHI='" & pStrDIACHI & "'"
                strSQL &= ",SO_CMT='" & pStrSO_CMT & "'"
                strSQL &= ",HO_TEN='" & pStrHO_TEN.Replace("'", "''") & "'"
                strSQL &= ",NGAYSINH=to_date('" & pStrNGAYSINH & "','RRRR-MM-DD')"
                strSQL &= ",NGUYENQUAN ='" & pStrNGUYENQUAN & "'"
                strSQL &= ",    THONGTINLIENHE=:THONGTINLIENHE"
                strSQL &= ",SO_DT='" & pStrSO_DT & "'"
                strSQL &= ",EMAIL='" & pStrEMAIL & "'"
                strSQL &= ",SERIALNUMBER='" & pStrSERIALNUMBER & "'"
                strSQL &= ",NOICAP ='" & pStrNOICAP & "'"
                strSQL &= ",NGAY_HL =to_date('" & pStrNGAY_HL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                strSQL &= ",NGAY_HHL=to_date('" & pStrNGAY_HHL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                strSQL &= ",    PUBLICKEY=:PUBLICKEY"
                strSQL &= ",TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "'"
                strSQL &= ",IDMSG311='" & pStrIDMSG311 & "'"
                strSQL &= ", ID_NV_MK312='" & pStrAccountHQ247 & "' "
                strSQL &= ", ID_NV_CK312='" & pStrAccountHQ247 & "' "
                strSQL &= ",NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= ",NGAY_HT =to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                'insert bang HQ
                'xoa bang HQ_311
                Try
                    'conn.Open()
                    myCMD.Connection = conn
                    myCMD.CommandText = strSQL
                    myCMD.CommandType = CommandType.Text
                    myCMD.Parameters.Add("THONGTINLIENHE", OracleType.Clob).Value = pStrTHONGTINLIENHE
                    myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pStrPUBLICKEY
                    myCMD.ExecuteNonQuery()
                    If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                        myCMD.Dispose()
                        conn.Dispose()
                        conn.Close()
                    End If
                Catch ex As Exception
                Finally
                    If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                        conn.Close()
                    End If
                    myCMD.Dispose()
                    conn.Dispose()
                End Try
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_UpdateSua311_HQ: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_CheckDieuChinhTaiNH(ByVal pStrSoHS As String) As Decimal
            Try
                Dim strSQL As String = "select * from tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "') "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_CheckDieuChinhTaiNH: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_Maker312_SuaTaiNH(ByVal pStrSoHS As String, ByVal pStrSoTK As String, ByVal pStrTenTK As String, ByVal pStrNGAY_KY_HDUQ As String, ByVal pStrTenDN As String, Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMA_CNTK As String = "") As Decimal
            Try
                Dim strSQL As String = "select * from tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "') "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim decResult As Decimal = Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            decResult = Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                'neu khong ton tai thi tra ve loi
                If decResult <> 0 Then
                    Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                End If
                'insert sang bang HQ 311
                'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
                'Nhamlt 20180123: Them NGAY_NHAN311
                strSQL = "INSERT INTO TCS_DM_NTDT_HQ_311 (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH, TRANGTHAI, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH, LOAIMSG213, MA_CN,KENH_DK,NGAY_NHAN311)"
                strSQL &= " SELECT ID, SO_HS,'2' LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH,'04' TRANGTHAI, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH,'1' LOAIMSG213, MA_CN,KENH_DK,NGAY_NHAN311"
                strSQL &= " FROM TCS_DM_NTDT_HQ A WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'cap nhat trang thai HQ
                strSQL = "UPDATE TCS_DM_NTDT_HQ SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH & "' WHERE SO_HS='" & pStrSoHS & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'cap nhat lai trang thai thong tin tai khoan dang ky va thong tin nguoi lap 312
                strSQL = "UPDATE TCS_DM_NTDT_HQ_311 set "
                strSQL &= " NGAY_HT=TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= ",ID_NV_MK312=upper('" & pStrTenDN & "') "
                strSQL &= ",CUR_TAIKHOAN_TH='" & pStrSoTK & "'"
                strSQL &= ",CIFNO='" & pvStrCIFNO & "'"
                strSQL &= ",TK_MA_CN='" & pvStrMA_CNTK & "'"
                strSQL &= ",CUR_TEN_TAIKHOAN_TH='" & pStrTenTK.Replace("'", "''") & "'"
                strSQL &= ",TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312 & "'"
                strSQL &= ", NGAY_KY_HDUQ=TO_DATE('" & pStrNGAY_KY_HDUQ & "','DD/MM/RRRR')"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_Maker312_SuaTaiNH: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_checkChuyenTra(ByVal pStrSoHS As String, ByVal pStrTenDN As String) As Decimal
            Dim strSQL As String = "select * from tcs_dm_ntdt_hq_311 where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA & "') "
            Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Dim decResult As Decimal = Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
            If Not ds Is Nothing Then
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        decResult = Common.mdlCommon.TCS_HQ247_OK
                    End If
                End If
            End If
            'neu khong ton tai thi tra ve loi
            If decResult <> 0 Then
                Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
            End If
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_Maker312_HuyTaiNH(ByVal pStrSoHS As String, ByVal pStrTenDN As String) As Decimal
            Try
                Dim strSQL As String = "select * from tcs_dm_ntdt_hq_311 where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA & "') "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim decResult As Decimal = Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            decResult = Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                If decResult = 0 Then
                    'Neu chuyen tra của khai huy chi can update lai trang thai 311 la duoc
                    'cap nhat lai trang thai thong tin tai khoan dang ky va thong tin nguoi lap 312
                    strSQL = "UPDATE TCS_DM_NTDT_HQ_311 set "
                    strSQL &= " NGAY_HT=TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                    strSQL &= ",ID_NV_MK312=upper('" & pStrTenDN & "') "
                    strSQL &= ",TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312 & "'"
                    strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                    Return Common.mdlCommon.TCS_HQ247_OK
                End If
                'check trang thai active
                strSQL = "select * from tcs_dm_ntdt_hq where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "') "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                decResult = Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            decResult = Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                'neu khong ton tai thi tra ve loi
                If decResult <> 0 Then
                    Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                End If
                'check bang hq 311 xem co ko
                strSQL = "select * from tcs_dm_ntdt_hq_311 where so_hs='" & pStrSoHS & "'"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            decResult = Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                'neu khong ton tai thi tra ve loi
                If decResult <> 0 Then
                    Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                End If
                'check chung tu thanh toan xem co khong
                strSQL = "SELECT * FROM TCS_HQ247_304 WHERE TRANGTHAI NOT IN ('07','04') AND SO_HS='" & pStrSoHS & "'"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            decResult = Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                'neu khong ton tai thi tra ve loi
                If decResult <> 0 Then
                    Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                End If

                'insert sang bang HQ 311
                'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
                strSQL = "INSERT INTO TCS_DM_NTDT_HQ_311 (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH, TRANGTHAI, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH, LOAIMSG213, MA_CN,NGAY_KY_HDUQ,KENH_DK,NGAY_NHAN311)"
                strSQL &= " SELECT ID, SO_HS,'3' LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH,'04' TRANGTHAI, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH,'1' LOAIMSG213, MA_CN,NGAY_KY_HDUQ,KENH_DK,NGAY_NHAN311"
                strSQL &= " FROM TCS_DM_NTDT_HQ A WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'cap nhat trang thai HQ
                strSQL = "UPDATE TCS_DM_NTDT_HQ SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH & "' WHERE SO_HS='" & pStrSoHS & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'cap nhat lai trang thai thong tin tai khoan dang ky va thong tin nguoi lap 312
                strSQL = "UPDATE TCS_DM_NTDT_HQ_311 set "
                strSQL &= " NGAY_HT=TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= ",ID_NV_MK312=upper('" & pStrTenDN & "') "
                strSQL &= ",TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312 & "'"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog("fnc_Maker312_HuyTaiNH: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_Update311InfoHeader(ByVal pStrSoHS As String, ByVal pStrLoaiHS As String, _
                                                      ByVal pStrMA_DV As String, ByVal pStrTEN_DV As String, ByVal pStrDIACHI As String, ByVal pStrSO_CMT As String, _
                                                      ByVal pStrHO_TEN As String, ByVal pStrNGAYSINH As String, ByVal pStrNGUYENQUAN As String, ByVal pStrTHONGTINLIENHE As String, _
                                                      ByVal pStrSO_DT As String, ByVal pStrEMAIL As String, ByVal pStrSERIALNUMBER As String, ByVal pStrNOICAP As String, _
                                                      ByVal pStrNGAY_HL As String, ByVal pStrNGAY_HHL As String, ByVal pStrPUBLICKEY As String, _
                                                      ByVal pStrTAIKHOAN_TH As String, ByVal pStrIDMSG311 As String, ByVal pStrAccountHQ247 As String) As Decimal
            Dim conn As OracleConnection = VBOracleLib.DataAccess.GetConnection() ' New OracleConnection(VBOracleLib.DataAccess.strConnection)
            Dim myCMD As OracleCommand = New OracleCommand()
            Try

                'kiem tra xem bang HQ_311 co khong
                Dim strSQL As String = "SELECT ID,SO_HS,TRANGTHAI FROM TCS_DM_NTDT_HQ_311 WHERE SO_HS='" & pStrSoHS & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        ' conn.Open()
                        strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                        'cap nhat bang HQ_311
                        strSQL &= " MA_DV='" & pStrMA_DV & "'"
                        strSQL &= ",TEN_DV='" & pStrTEN_DV.Replace("'", "''") & "'"
                        strSQL &= ",DIACHI='" & pStrDIACHI & "'"
                        strSQL &= ",SO_CMT='" & pStrSO_CMT & "'"
                        strSQL &= ",HO_TEN='" & pStrHO_TEN.Replace("'", "''") & "'"
                        strSQL &= ",NGAYSINH=to_date('" & pStrNGAYSINH & "','RRRR-MM-DD')"
                        strSQL &= ",NGUYENQUAN ='" & pStrNGUYENQUAN & "'"
                        strSQL &= ",    THONGTINLIENHE=:THONGTINLIENHE"
                        strSQL &= ",SO_DT='" & pStrSO_DT & "'"
                        strSQL &= ",EMAIL='" & pStrEMAIL & "'"
                        strSQL &= ",SERIALNUMBER='" & pStrSERIALNUMBER & "'"
                        strSQL &= ",NOICAP ='" & pStrNOICAP & "'"
                        strSQL &= ",NGAY_HL =to_date('" & pStrNGAY_HL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                        strSQL &= ",NGAY_HHL=to_date('" & pStrNGAY_HHL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                        strSQL &= ",    PUBLICKEY=:PUBLICKEY"
                        'strSQL &= ",IDMSG311='" & pStrIDMSG311 & "'"
                        strSQL &= ",TAIKHOAN_TH=(CASE WHEN TRANGTHAI IN ('09','11','12') THEN TAIKHOAN_TH ELSE   '" & pStrTAIKHOAN_TH & "' END)"
                        'strSQL &= ",NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= ",NGAY_HT =to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                        myCMD.Connection = conn
                        myCMD.CommandText = strSQL
                        myCMD.CommandType = CommandType.Text
                        myCMD.Parameters.Add("THONGTINLIENHE", OracleType.Clob).Value = pStrTHONGTINLIENHE
                        myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pStrPUBLICKEY
                        myCMD.ExecuteNonQuery()
                        If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                            myCMD.Dispose()
                            conn.Dispose()
                            conn.Close()
                        End If
                    End If
                End If
                strSQL = "SELECT ID,SO_HS,TRANGTHAI FROM TCS_DM_NTDT_HQ WHERE SO_HS='" & pStrSoHS & "' "
                'kiem tra xem ban HQ co khong
                Dim dshq As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If dshq.Tables.Count > 0 Then
                    If dshq.Tables(0).Rows.Count > 0 Then
                        conn = New OracleConnection(VBOracleLib.DataAccess.strConnection)
                        myCMD = New OracleCommand()
                        '  conn.Open()
                        strSQL = "UPDATE TCS_DM_NTDT_HQ SET "
                        'cap nhat bang HQ_311
                        strSQL &= " MA_DV='" & pStrMA_DV & "'"
                        strSQL &= ",TEN_DV='" & pStrTEN_DV.Replace("'", "''") & "'"
                        strSQL &= ",DIACHI='" & pStrDIACHI & "'"
                        strSQL &= ",SO_CMT='" & pStrSO_CMT & "'"
                        strSQL &= ",HO_TEN='" & pStrHO_TEN.Replace("'", "''") & "'"
                        strSQL &= ",NGAYSINH=to_date('" & pStrNGAYSINH & "','RRRR-MM-DD')"
                        strSQL &= ",NGUYENQUAN ='" & pStrNGUYENQUAN & "'"
                        strSQL &= ",    THONGTINLIENHE=:THONGTINLIENHE"
                        strSQL &= ",SO_DT='" & pStrSO_DT & "'"
                        strSQL &= ",EMAIL='" & pStrEMAIL & "'"
                        strSQL &= ",SERIALNUMBER='" & pStrSERIALNUMBER & "'"
                        strSQL &= ",NOICAP ='" & pStrNOICAP & "'"
                        strSQL &= ",NGAY_HL =to_date('" & pStrNGAY_HL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                        strSQL &= ",NGAY_HHL=to_date('" & pStrNGAY_HHL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                        strSQL &= ",    PUBLICKEY=:PUBLICKEY"
                        'strSQL &= ",IDMSG311='" & pStrIDMSG311 & "'"
                        strSQL &= ",NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        'strSQL &= ",NGAY_HT =to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                        myCMD.Connection = conn
                        myCMD.CommandText = strSQL
                        myCMD.CommandType = CommandType.Text
                        myCMD.Parameters.Add("THONGTINLIENHE", OracleType.Clob).Value = pStrTHONGTINLIENHE
                        myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pStrPUBLICKEY
                        myCMD.ExecuteNonQuery()
                        If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                            myCMD.Dispose()
                            conn.Dispose()
                            conn.Close()
                        End If
                    End If
                End If

                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog("fnc_UpdateSua311_HQ: " & ex.StackTrace, EventLogEntryType.Error)
            Finally
                If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                    conn.Close()
                End If
                myCMD.Dispose()
                conn.Dispose()
            End Try

            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
    End Class
End Namespace

