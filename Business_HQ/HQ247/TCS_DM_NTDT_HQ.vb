﻿Imports System.Data
Namespace HQ247
    Public Class TCS_DM_NTDT_HQ
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Shared Function getSoHOSO(ByVal pvMa_DV As String, ByVal pvTaiKhoan_TH As String) As String
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT SO_HS FROM TCS_DM_NTDT_HQ WHERE TRANGTHAI='13' "
                strSQL &= " AND MA_DV = '" & pvMa_DV & "' AND TAIKHOAN_TH ='" & pvTaiKhoan_TH & "' "

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return ds.Tables(0).Rows(0)("SO_HS").ToString
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "", "")
            End Try
            Return "HS_NOTFOUND"
        End Function
        Public Shared Function getMSG311FORTRACUUVAXULY(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID,A.SO_HS,A.MA_DV,A.LOAI_HS,C.MOTA TENLOAIHS,A.TEN_DV,A.TAIKHOAN_TH,TO_CHAR(A.NGAY_NHAN311,'DD/MM/RRRR') "
                strSQL &= " NGAY_NHAN311,A.TRANGTHAI,B.mota TRANG_THAI, 'JchkGridNO' URL "
                strSQL &= " FROM TCS_DM_NTDT_HQ A, TCS_DM_TRANGTHAI_YEUCAU B , TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.LOAI_HS = C.LOAI_HS"
                strSQL &= " AND A.trangthai IN ('13','15')" & pvStrWhereClause
                strSQL &= "     UNION ALL "
                strSQL &= " SELECT A.ID,A.SO_HS,A.MA_DV,A.LOAI_HS,C.MOTA TENLOAIHS,A.TEN_DV,A.TAIKHOAN_TH,TO_CHAR(A.NGAY_NHAN311,'DD/MM/RRRR') "
                strSQL &= " NGAY_NHAN311,A.TRANGTHAI,B.mota TRANG_THAI,(case when A.TRANGTHAI ='07' THEN 'JchkGridNO' ELSE 'JchkGrid' END) URL "
                strSQL &= " FROM TCS_DM_NTDT_HQ_311 A, TCS_DM_TRANGTHAI_YEUCAU B , TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.LOAI_HS = C.LOAI_HS " & pvStrWhereClause
                strSQL &= "     UNION ALL "
                strSQL &= " SELECT A.ID,A.SO_HS,A.MA_DV,A.LOAI_HS,C.MOTA TENLOAIHS,A.TEN_DV,A.TAIKHOAN_TH,TO_CHAR(A.NGAY_NHAN311,'DD/MM/RRRR') "
                strSQL &= " NGAY_NHAN311,A.TRANGTHAI,B.MOTA TRANG_THAI,(CASE WHEN A.TRANGTHAI ='07' THEN 'JCHKGRIDNO' ELSE 'JCHKGRID' END) URL "
                strSQL &= " FROM TCS_HQ_311_TMP A, TCS_DM_TRANGTHAI_YEUCAU B , TCS_DM_LOAI_HS_NNT C"
                strSQL &= " WHERE(A.TRANGTHAI = B.TRANG_THAI And A.LOAI_HS = C.LOAI_HS)"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getMSG311(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS,C.MOTA TENLOAIHS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH"

                strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('01','02','03','04','05','06','07','08') THEN "
                strSQL &= "  A.TAIKHOAN_TH ELSE"
                strSQL &= "  A.CUR_TAIKHOAN_TH"
                strSQL &= "  END ) TAIKHOAN_TH, "

                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, A.NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI, "
                strSQL &= "  NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME "
                strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('01','02','03','05','06','07','08') THEN "
                strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.21')),'#') ELSE"
                strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.22')),'#')"
                strSQL &= "  END ) NAVIGATEURL "
                strSQL &= " FROM TCS_DM_NTDT_HQ_311 A,TCS_DM_TRANGTHAI_YEUCAU B, TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI And A.LOAI_HS = C.LOAI_HS " & pvStrWhereClause


                strSQL &= " UNION ALL "
                strSQL &= "SELECT A.ID, A.SO_HS, A.LOAI_HS,C.MOTA TENLOAIHS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH"

                strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('01','02','03','04','05','06','07','08') THEN "
                strSQL &= "  A.TAIKHOAN_TH ELSE"
                strSQL &= "  A.CUR_TAIKHOAN_TH"
                strSQL &= "  END ) TAIKHOAN_TH, "

                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, A.NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI, "
                strSQL &= "  NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME "
                strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('01','02','03','05','06','07','08') THEN "
                strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.21')),'#') ELSE"
                strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.22')),'#')"
                strSQL &= "  END ) NAVIGATEURL "
                strSQL &= " FROM TCS_DM_NTDT_HQ A,TCS_DM_TRANGTHAI_YEUCAU B, TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.TRANGTHAI  IN ('13','15') And A.LOAI_HS = C.LOAI_HS " & pvStrWhereClause
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function

        Public Shared Function getMakerMSG213(ByVal pvStrID As String, Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,THONGTINLIENHE "
                strSQL &= "  FROM TCS_DM_NTDT_HQ_311 A WHERE  1=1 "
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND ID='" & pvStrID & "' "
                End If
                If Not pvStrWhereClause.Equals("") Then
                    strSQL &= "  " & pvStrWhereClause & " "
                End If
                If Not pvStrOrderBy.Equals("") Then
                    strSQL &= "  ORDER BY " & pvStrOrderBy & " "
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getMakerMSG213_AndHoso15_16InDay(ByVal pvStrID As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,THONGTINLIENHE "
                strSQL &= "  FROM TCS_DM_NTDT_HQ_311 A WHERE  1=1 "
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND ID='" & pvStrID & "' "
                End If
                strSQL &= " UNION ALL "
                strSQL &= "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,THONGTINLIENHE "
                strSQL &= "  FROM TCS_DM_NTDT_HQ A WHERE  1=1 AND A.TRANGTHAI IN ('13','15')"
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND ID='" & pvStrID & "' "
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getDangKyNNT_HQ_LOG(ByVal pvStrID As String, Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS "
                strSQL &= "  FROM TCS_DM_NTDT_HQ_LOG A WHERE  1=1 "
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND LOG_ID='" & pvStrID & "' "
                End If
                If Not pvStrWhereClause.Equals("") Then
                    strSQL &= "  " & pvStrWhereClause & " "
                End If
                If Not pvStrOrderBy.Equals("") Then
                    strSQL &= "  ORDER BY " & pvStrOrderBy & " "
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getDangKyNNT_HQ(ByVal pvStrID As String, Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS "
                strSQL &= "  FROM TCS_DM_NTDT_HQ A WHERE  1=1 "
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND ID='" & pvStrID & "' "
                End If
                If Not pvStrWhereClause.Equals("") Then
                    strSQL &= "  " & pvStrWhereClause & " "
                End If
                If Not pvStrOrderBy.Equals("") Then
                    strSQL &= "  ORDER BY " & pvStrOrderBy & " "
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getMaker312(Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "", Optional ByVal usernameName As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String
                strSQL &= "  SELECT B.MOTA TRANGTHAI,A.MA_DV ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,A.ID, A.CUR_TAIKHOAN_TH"
                strSQL &= "  FROM TCS_DM_NTDT_HQ_311 A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_NHANVIEN C, TCS_DM_NHANVIEN D WHERE A.TRANGTHAI=B.TRANG_THAI "
                strSQL &= "  AND (A.TRANGTHAI IN ('04','09','10','11','12','13','15') ) AND UPPER(A.ID_NV_MK312) = UPPER(C.TEN_DN) AND C.MA_CN=D.MA_CN "
                strSQL &= "  AND UPPER(D.TEN_DN)=UPPER('" + usernameName + "') "
                If pvStrWhereClause <> "" Then
                    strSQL &= pvStrWhereClause
                End If
                'Nhamlt sua hien thi thanh cong va ngung dang ky trong ngay hom do
                strSQL &= " UNION ALL "
                strSQL &= "  SELECT B.MOTA TRANGTHAI,A.MA_DV ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,A.ID, A.CUR_TAIKHOAN_TH"
                strSQL &= "  FROM TCS_DM_NTDT_HQ A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_NHANVIEN C, TCS_DM_NHANVIEN D WHERE A.TRANGTHAI=B.TRANG_THAI "
                strSQL &= "  AND (A.TRANGTHAI IN ('13','15') ) AND UPPER(A.ID_NV_MK312) = UPPER(C.TEN_DN) AND C.MA_CN=D.MA_CN "
                strSQL &= "  AND UPPER(D.TEN_DN)=UPPER('" + usernameName + "') AND  TO_CHAR(A.NGAY_HT,'DD/MM/YYYY')=(SELECT GIATRI_TS FROM TCS_THAMSO  WHERE TEN_TS ='NGAY_LV' AND ROWNUM=1)"
                If pvStrWhereClause <> "" Then
                    strSQL &= pvStrWhereClause
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getCheckerMSG213(Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "", Optional ByVal usernameName As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String
                strSQL &= "  SELECT B.MOTA TRANGTHAI,A.MA_DV ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,A.ID, A.TAIKHOAN_TH"
                strSQL &= "  FROM TCS_DM_NTDT_HQ_311 A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_NHANVIEN C, TCS_DM_NHANVIEN D WHERE A.TRANGTHAI=B.TRANG_THAI "
                strSQL &= "  AND (A.TRANGTHAI IN ('02','03','05','06') OR (A.trangthai='07' AND TRUNC(A.ngay_ht)=TRUNC(SYSDATE)) ) AND UPPER(A.ID_NV_MK213) = UPPER(C.TEN_DN) AND C.MA_CN=D.MA_CN "
                strSQL &= "  AND UPPER(D.TEN_DN)=UPPER('" + usernameName + "') "
                If pvStrWhereClause <> "" Then
                    strSQL &= pvStrWhereClause
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
            End Try
            Return ds
        End Function
        Public Shared Function MakerMSG213(ByVal pvStrID As String, ByVal pvStrNoidungXL As String, ByVal pvStrLoai213 As String, Optional ByVal pvStrTenDN As String = "") As Decimal
            Try
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH) Then
                                Return Business_HQ.Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'CAP NHAT THONG TIN VAO HQ 311
                If pvStrLoai213.Equals("1") Then
                    strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER213 & "'"
                Else
                    strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER213_TUCHOI & "'"
                End If

                strSQL &= " ,LY_DO_HUY='" & pvStrNoidungXL & "' "
                strSQL &= " ,LOAIMSG213='" & pvStrLoai213 & "' "
                strSQL &= " ,ID_NV_MK213='" & pvStrTenDN & "' "
                strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                strSQL &= " WHERE ID='" & pvStrID & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function CheckerMSG213(ByVal pvStrID As String, ByVal pvStrNoidungXL As String, ByVal pvStrLoai213 As String, Optional ByVal pvStrTenDN As String = "") As Decimal
            Try
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH) Then
                                Return Business_HQ.Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'CAP NHAT THONG TIN VAO HQ 311
                If pvStrLoai213.Equals("1") Then
                    strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213 & "'"
                Else
                    strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213_TUCHOI & "'"
                End If
                strSQL &= " ,ID_NV_CK213='" & pvStrTenDN & "' "
                strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                strSQL &= " WHERE ID='" & pvStrID & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
#Region "HQ247 huynt"
        'HQ247 huynt begin
        ''' <summary>
        ''' GDV (Maker) hoàn thiện thông tin NNT:
        ''' Nếu thành công: trạng thái chứng từ được chuyển thành Chờ kiểm soát
        ''' Nếu lỗi: không thay đổi trạng thái
        ''' </summary>
        ''' <param name="pvStrID">Mã chứng từ (ID)</param>
        ''' <param name="pvStrNgay_Ky_HDUQ">Ngày ký hợp đồng (từ hợp đồng ủy quyền)</param>
        ''' <param name="pvStrTenDN">Tên đăng nhập của GDV</param>
        ''' <returns>
        ''' 10004: nếu Đăng ký đang ở trạng thái Active (13) hoặc trạng thái Hold (16);
        ''' 0: nếu thành công;
        ''' 1: nếu lỗi hệ thống
        ''' </returns>
        ''' <remarks></remarks>
        Public Shared Function MakerMSG312(ByVal pvStrID As String, ByVal pvStrNgay_Ky_HDUQ As String, ByVal pvStrCurTaiKhoanTH As String, ByVal pvStrCurTenTaiKhoanTH As String, ByVal pvStrThongtinLienHe As String, Optional ByVal pvStrTenDN As String = "", Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMaCNTK As String = "") As Decimal
            Try
                'Kiểm tra trong bảng chính, nếu đã có chứng từ ở trạng thái khác active (13) và khác hold (14) thì return
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_HOLD) Then
                                Return Business_HQ.Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'Lấy thông tin tài khoản NNT đăng ký để Nộp thuế điện tử
                'CAP NHAT THONG TIN VAO HQ 311
                'Do có cả 2 phương thức: đăng ký mới tại TCHQ và đăng ký mới tại NHTM đều sử dụng chung Form này, do đó để xác đinh
                'được đâu là đăng ký mới tại TCHQ và đâu là tại NHTM ta kiểm tra ID
                'Trường hợp xác định là đăng ký tại TCHQ
                If Not pvStrID.Equals("") Then
                    Dim maChiNhanh As String = "___"
                    If (Business_HQ.HQ247.daTCS_DM_NTDT_HQ.getCodebranch(maChiNhanh, pvStrTenDN)) = 0 Then
                        strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312 & "'"
                        'Định dạng lại pvStrNgay_Ky_HDUQ theo chuỗi yyyyMMddHHmmss
                        Dim strDateCover As String = pvStrNgay_Ky_HDUQ.Split("/")(2) & pvStrNgay_Ky_HDUQ.Split("/")(1) & pvStrNgay_Ky_HDUQ.Split("/")(0)
                        strSQL &= " ,NGAY_KY_HDUQ=to_date('" & strDateCover & "','RRRRMMDDHH24MISS') "
                        strSQL &= " ,CIFNO = '" & pvStrCIFNO & "' "
                        strSQL &= " ,TK_MA_CN = '" & pvStrMaCNTK & "' "
                        strSQL &= " ,CUR_TEN_TAIKHOAN_TH = '" & pvStrCurTenTaiKhoanTH & "' "
                        strSQL &= " ,CUR_TAIKHOAN_TH = '" & pvStrCurTaiKhoanTH & "' "
                        strSQL &= " ,MA_CN = '" & maChiNhanh & "' "
                        strSQL &= " ,THONGTINLIENHE = '" & pvStrThongtinLienHe & "' "
                        strSQL &= " ,ID_NV_MK312='" & pvStrTenDN & "' "
                        strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                        strSQL &= " WHERE ID='" & pvStrID & "' "
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                    Else
                        Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                    End If

                End If
            Catch ex As Exception
            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        ''' <summary>
        ''' Đăng ký mới tại NHTM, Insert một Đăng ký mới vào bảng HQ_311 với TRANGTHAI = 09 (Chờ kiểm soát)
        ''' </summary>
        ''' <param name="strMaDV">Mã số thuế của người đại diện doanh nghiệp XNK (NNT)</param>
        ''' <param name="strTenDV">Tên doanh nghiệp XNK</param>
        ''' <param name="strDiaChi">Địa chỉ Doanh nghiệp XNK</param>
        ''' <param name="strSoCMT">Số CMTND của NNT</param>
        ''' <param name="strHoTen">Họ tên NNT</param>
        ''' <param name="strNgaySinh">Ngày sinh NNT</param>
        ''' <param name="strNguyenQuyan">Nguyên quán NNT</param>
        ''' <param name="strSoDT">Số điện thoại của NNT</param>
        ''' <param name="strEmal">Email NNT</param>
        ''' <param name="strSoDT1">Số ĐT bổ sung của NNT</param>
        ''' <param name="strEmail1">Email bổ sung của NNT</param>
        ''' <param name="strMaNH">Mã của NH - nơi NNT đã đăng ký tài khoản sử dụng NT</param>
        ''' <param name="strTenNganHang">Tên ngân hàng - nơi NNT đã đăng ký tài khoản sử dụng NT</param>
        ''' <param name="strTenDN">Tên đăng nhập của NSD</param>
        ''' <param name="strTaiKhoanChck">Số tài khoản NH mà NNT muốn đăng ký NTDT</param>
        ''' <param name="strTenTaiKhoanCheck">Tên tài khoản NH ứng với số tài khoản mà NNT muốn đăng ký NTDT</param>
        ''' <param name="strNgayKyHDUQ">Ngày ký ủy quyền (trong hợp đồng mà NNT đã ký với NH)</param>
        ''' <returns>0: Nếu Insert đăng ký mới thành công, !0 nếu thất bại</returns>
        ''' <remarks></remarks>
        Public Shared Function MakerMSG312(ByVal strMaDV As String, ByVal strTenDV As String, ByVal strDiaChi As String, ByVal strSoCMT As String, ByVal strHoTen As String, ByVal strNgaySinh As String, ByVal strNguyenQuyan As String, ByVal strSoDT As String, ByVal strEmal As String, ByVal strSoDT1 As String, ByVal strEmail1 As String, ByVal strTaiKhoan_TH As String, ByVal strTenTaiKhoan_TH As String, ByVal strMaNH As String, ByVal strTenNganHang As String, ByVal strTenDN As String, ByVal strTaiKhoanChck As String, ByVal strTenTaiKhoanCheck As String, ByVal strNgayKyHDUQ As String, Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMaCNTK As String = "") As Decimal
            'Trường hợp xác định là đăng ký mới tại NHTM
            Try
                Dim strDateCover As String = strNgaySinh.Split("/")(2) & strNgaySinh.Split("/")(1) & strNgaySinh.Split("/")(0)
                Dim strNgayKyHDUQCover As String = strNgayKyHDUQ.Split("/")(2) & strNgayKyHDUQ.Split("/")(1) & strNgayKyHDUQ.Split("/")(0)
                Dim maChiNhanh As String = "___"
                If (Business_HQ.HQ247.daTCS_DM_NTDT_HQ.getCodebranch(maChiNhanh, strTenDN)) = 0 Then
                    Dim strSQL As String = " INSERT INTO TCS_DM_NTDT_HQ_311 ("
                    strSQL &= " ID, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT"
                    strSQL &= " ,HO_TEN, NGAYSINH, NGUYENQUAN, SO_DT"
                    strSQL &= " ,EMAIL, SO_DT1, EMAIL1"
                    strSQL &= " , TAIKHOAN_TH, TEN_TAIKHOAN_TH"
                    strSQL &= " , MA_NH_TH, TEN_NH_TH"
                    strSQL &= " , TRANGTHAI"
                    strSQL &= " , ID_NV_MK312"
                    strSQL &= " , CUR_TAIKHOAN_TH"
                    strSQL &= " ,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY"
                    strSQL &= " , NGAY_HT, LOAIMSG213, MA_CN, NGAY_KY_HDUQ,CIFNO,TK_MA_CN,KENH_DK"
                    strSQL &= " )"
                    strSQL &= " VALUES (TO_CHAR(SYSDATE,'RRRRMMDD')||LPAD(SEQ_TCS_DM_NTDT_HQ.NEXTVAL,10,'0'), '1', '" & strMaDV & "', '" & strTenDV & "', '" & strDiaChi & "', '" & strSoCMT & "'"
                    strSQL &= " ,'" & strHoTen & "', to_date('" & strDateCover & "','RRRRMMDDHH24MISS'), '" & strNguyenQuyan & "', '" & strSoDT & "'"
                    strSQL &= " ,'" & strEmal & "', '" & strSoDT1 & "', '" & strEmail1 & "'"
                    strSQL &= " , '" & strTaiKhoan_TH & "', '" & strTenTaiKhoan_TH & "'"
                    strSQL &= " , '" & strMaNH & "', '" & strTenNganHang & "'"
                    strSQL &= " , '09' "
                    strSQL &= " , '" & strTenDN & "'"
                    strSQL &= " , '" & strTaiKhoanChck & "'"
                    strSQL &= " ,'" & strTenTaiKhoanCheck & "', ''"
                    strSQL &= " , to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') , '1', '" & maChiNhanh & "',to_date('" & strNgayKyHDUQCover & "','RRRRMMDDHH24MISS')"
                    strSQL &= " , '" & pvStrCIFNO & "'"
                    strSQL &= " , '" & pvStrMaCNTK & "','1'"
                    strSQL &= " )"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                    Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                Else
                    Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                End If
            Catch ex As Exception
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            End Try
        End Function

        Public Shared Function MakerMSG312H(ByVal strMaDV As String, ByVal strTenDV As String, ByVal strDiaChi As String, ByVal strSoCMT As String, ByVal strHoTen As String, ByVal strNgaySinh As String, ByVal strNguyenQuyan As String, ByVal strSoDT As String, ByVal strEmal As String, ByVal strSoDT1 As String, ByVal strEmail1 As String, ByVal strTaiKhoan_TH As String, ByVal strTenTaiKhoan_TH As String, ByVal strMaNH As String, ByVal strTenNganHang As String, ByVal strTenDN As String, ByVal strTaiKhoanChck As String, ByVal strTenTaiKhoanCheck As String, ByVal strNgayKyHDUQ As String, ByVal strThongTinLienHe As String, Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMaCNTK As String = "") As Decimal
            'Trường hợp xác định là đăng ký mới tại NHTM
            Try
                Dim strDateCover As String = strNgaySinh.Split("/")(2) & strNgaySinh.Split("/")(1) & strNgaySinh.Split("/")(0)
                Dim strNgayKyHDUQCover As String = strNgayKyHDUQ.Split("/")(2) & strNgayKyHDUQ.Split("/")(1) & strNgayKyHDUQ.Split("/")(0)
                Dim maChiNhanh As String = "___"
                If (Business_HQ.HQ247.daTCS_DM_NTDT_HQ.getCodebranch(maChiNhanh, strTenDN)) = 0 Then
                    'Nhamlt 20180122: Them KENH_DK: MST được đăng ký từ kênh nào (0-TCHQ, 1-NHTM)/
                    Dim strSQL As String = " INSERT INTO TCS_DM_NTDT_HQ_311 ("
                    strSQL &= " ID, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT"
                    strSQL &= " ,HO_TEN, NGAYSINH, NGUYENQUAN, SO_DT"
                    strSQL &= " ,EMAIL, SO_DT1, EMAIL1"
                    strSQL &= " , TAIKHOAN_TH, TEN_TAIKHOAN_TH"
                    strSQL &= " , MA_NH_TH, TEN_NH_TH"
                    strSQL &= " , TRANGTHAI"
                    strSQL &= " , ID_NV_MK312"
                    strSQL &= " , CUR_TAIKHOAN_TH"
                    strSQL &= " ,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY"
                    strSQL &= " , NGAY_HT, LOAIMSG213, MA_CN, NGAY_KY_HDUQ,THONGTINLIENHE,CIFNO,TK_MA_CN,KENH_DK"
                    strSQL &= " )"
                    strSQL &= " VALUES (TO_CHAR(SYSDATE,'RRRRMMDD')||LPAD(SEQ_TCS_DM_NTDT_HQ.NEXTVAL,10,'0'), '1', '" & strMaDV & "', '" & strTenDV & "', '" & strDiaChi & "', '" & strSoCMT & "'"
                    strSQL &= " ,'" & strHoTen & "', to_date('" & strDateCover & "','RRRRMMDDHH24MISS'), '" & strNguyenQuyan & "', '" & strSoDT & "'"
                    strSQL &= " ,'" & strEmal & "', '" & strSoDT1 & "', '" & strEmail1 & "'"
                    strSQL &= " , '" & strTaiKhoan_TH & "', '" & strTenTaiKhoan_TH & "'"
                    strSQL &= " , '" & strMaNH & "', '" & strTenNganHang & "'"
                    strSQL &= " , '09' "
                    strSQL &= " , '" & strTenDN & "'"
                    strSQL &= " , '" & strTaiKhoanChck & "'"
                    strSQL &= " ,'" & strTenTaiKhoanCheck & "', ''"
                    strSQL &= " , to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') , '1', '" & maChiNhanh & "',to_date('" & strNgayKyHDUQCover & "','RRRRMMDDHH24MISS')"
                    strSQL &= ",'" & strThongTinLienHe & "'"
                    strSQL &= " , '" & pvStrCIFNO & "'"
                    strSQL &= " , '" & pvStrMaCNTK & "'"
                    strSQL &= " ,'1')"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                    Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                Else
                    Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                End If
            Catch ex As Exception
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            End Try
        End Function
        Public Shared Function InsertNewHQ311(ByVal strTrangThai As String, ByVal strLoai_HS As String, ByVal strSo_HS As String, ByVal strMaDV As String, ByVal strTenDV As String, ByVal strDiaChi As String, ByVal strSoCMT As String, ByVal strHoTen As String, ByVal strNgaySinh As String, ByVal strNguyenQuyan As String, ByVal strSoDT As String, ByVal strEmal As String, ByVal strSoDT1 As String, ByVal strEmail1 As String, ByVal strTaiKhoan_TH As String, ByVal strTenTaiKhoan_TH As String, ByVal strMaNH As String, ByVal strTenNganHang As String, ByVal strTenDN As String, ByVal strTaiKhoanChck As String, ByVal strTenTaiKhoanCheck As String, ByVal strNgayKyHDUQ As String) As Decimal
            'Trường hợp xác định là đăng ký mới tại NHTM
            Try
                Dim strDateCover As String = strNgaySinh.Split("/")(2) & strNgaySinh.Split("/")(1) & strNgaySinh.Split("/")(0)
                Dim strNgayKyHDUQCover As String = strNgayKyHDUQ.Split("/")(2) & strNgayKyHDUQ.Split("/")(1) & strNgayKyHDUQ.Split("/")(0)
                Dim strSQL As String = " INSERT INTO TCS_DM_NTDT_HQ_311 ("
                strSQL &= " ID, LOAI_HS, SO_HS, MA_DV, TEN_DV, DIACHI, SO_CMT"
                strSQL &= " ,HO_TEN, NGAYSINH, NGUYENQUAN, SO_DT"
                strSQL &= " ,EMAIL, SO_DT1, EMAIL1"
                strSQL &= " , TAIKHOAN_TH, TEN_TAIKHOAN_TH"
                strSQL &= " , MA_NH_TH, TEN_NH_TH"
                strSQL &= " , TRANGTHAI"
                strSQL &= " , ID_NV_MK213"
                strSQL &= " , CUR_TAIKHOAN_TH"
                strSQL &= " ,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY"
                strSQL &= " , NGAY_HT, LOAIMSG213, MA_CN, NGAY_KY_HDUQ,KENH_DK"
                strSQL &= " )"
                strSQL &= " VALUES (TO_CHAR(SYSDATE,'RRRRMMDD')||LPAD(SEQ_TCS_DM_NTDT_HQ.NEXTVAL,10,'0'), '" & strLoai_HS & "','" & strSo_HS & "', '" & strMaDV & "', '" & strTenDV & "', '" & strDiaChi & "', '" & strSoCMT & "'"
                strSQL &= " ,'" & strHoTen & "', to_date('" & strDateCover & "','RRRRMMDDHH24MISS'), '" & strNguyenQuyan & "', '" & strSoDT & "'"
                strSQL &= " ,'" & strEmal & "', '" & strSoDT1 & "', '" & strEmail1 & "'"
                strSQL &= " , '" & strTaiKhoan_TH & "', '" & strTenTaiKhoan_TH & "'"
                strSQL &= " , '" & strMaNH & "', '" & strTenNganHang & "'"
                strSQL &= " , '" & strTrangThai & "' "
                strSQL &= " , '" & strTenDN & "'"
                strSQL &= " , '" & strTaiKhoanChck & "'"
                strSQL &= " ,'" & strTenTaiKhoanCheck & "', ''"
                strSQL &= " , to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') , '1', '___',to_date('" & strNgayKyHDUQCover & "','RRRRMMDDHH24MISS')"
                strSQL &= " ,'1')"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            End Try
        End Function
        Public Shared Function InsertNewHQ311ByCopyHQ(ByVal strID As String, ByVal strTrangThai As String, ByVal strLoai_HS As String, ByVal strSo_HS As String) As Decimal
            'Trường hợp xác định là đăng ký mới tại NHTM
            Try
                Dim strSQL As String = ""
                strSQL &= " "
                strSQL &= " INSERT INTO TCS_DM_NTDT_HQ_311 ("
                strSQL &= " ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,"
                strSQL &= " IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,"
                strSQL &= " ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,"
                strSQL &= " IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN,KENH_DK"
                strSQL &= " )"

                strSQL &= " SELECT ID, '" & strSo_HS & "', '" & strLoai_HS & "', MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH, '" & strTrangThai & "', IDMSG312, IDMSG313,"
                strSQL &= " IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,"
                strSQL &= " ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,"
                strSQL &= " IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN,KENH_DK"
                strSQL &= " "
                strSQL &= " FROM TCS_DM_NTDT_HQ "
                strSQL &= " WHERE ID = '" & strID & "' AND SO_HS = '" & strSo_HS & "'"

                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            End Try
        End Function
        Public Shared Function Update_HQ311(ByVal ID_HoSo As String, ByVal strMaDV As String, ByVal strTenDV As String, ByVal strDiaChi As String, ByVal strSoCMT As String, ByVal strHoTen As String, ByVal strNgaySinh As String, ByVal strNguyenQuyan As String, ByVal strSoDT As String, ByVal strEmail As String, ByVal strSoDT1 As String, ByVal strEmail1 As String, ByVal strMaNH As String, ByVal strTenNganHang As String, ByVal strTenDN As String, ByVal strTaiKhoanChck As String, ByVal strTenTaiKhoanCheck As String, ByVal strNgayKyHDUQ As String, ByVal strThongTinLienHe As String, Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMA_CNTK As String = "") As Decimal
            Try
                Dim strDateCover As String = strNgaySinh.Split("/")(2) & strNgaySinh.Split("/")(1) & strNgaySinh.Split("/")(0)
                Dim strNgayKyHDUQCover As String = strNgayKyHDUQ.Split("/")(2) & strNgayKyHDUQ.Split("/")(1) & strNgayKyHDUQ.Split("/")(0)
                Dim strSQL As String = ""
                strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET "
                strSQL &= "  TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312 & "'"
                strSQL &= "  ,MA_DV = '" & strMaDV & "'"
                strSQL &= "  ,TEN_DV = '" & strTenDV & "'"
                strSQL &= "  ,DIACHI = '" & strDiaChi & "'"
                strSQL &= "  ,SO_CMT = '" & strSoCMT & "'"
                strSQL &= "  ,HO_TEN = '" & strHoTen & "'"
                strSQL &= "  ,NGAYSINH = to_date('" & strDateCover & "','RRRRMMDDHH24MISS')"
                strSQL &= "  ,NGUYENQUAN = '" & strNguyenQuyan & "'"
                strSQL &= "  ,SO_DT = '" & strSoDT & "'"
                strSQL &= "  ,THONGTINLIENHE = '" & strThongTinLienHe & "'"
                strSQL &= "  ,EMAIL = '" & strEmail & "'"
                strSQL &= "  ,SO_DT1 = '" & strSoDT1 & "'"
                strSQL &= "  ,EMAIL1 = '" & strEmail1 & "'"
                strSQL &= "  ,CIFNO = '" & pvStrCIFNO & "'"
                strSQL &= "  ,TK_MA_CN = '" & pvStrMA_CNTK & "'"

                strSQL &= "  ,MA_NH_TH = '" & strMaNH & "'"
                strSQL &= "  ,TEN_NH_TH = '" & strTenNganHang & "'"
                strSQL &= "  ,ID_NV_MK312 = '" & strTenDN & "'"
                strSQL &= "  ,TAIKHOAN_TH = '" & strTaiKhoanChck & "'"
                strSQL &= "  ,TEN_TAIKHOAN_TH = '" & strTenTaiKhoanCheck & "'"
                strSQL &= "  ,CUR_TAIKHOAN_TH = '" & strTaiKhoanChck & "'"
                strSQL &= "  ,CUR_TEN_TAIKHOAN_TH = '" & strTenTaiKhoanCheck & "'"
                strSQL &= "  ,NGAY_KY_HDUQ = to_date('" & strNgayKyHDUQCover & "','RRRRMMDDHH24MISS')"
                strSQL &= " ,NGAY_HT = to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                strSQL &= " WHERE ID = '" & ID_HoSo & "' AND TRANGTHAI = '" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA.ToString() & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            End Try
        End Function
        ''' <summary>
        ''' KSV (Maker)Kiểm soát thông tin đăng ký NNT
        ''' </summary>
        ''' <param name="pvStrID">ID của Đăng ký</param>
        ''' <param name="pvStrNoidungXL">Lý do Chuyển trả trong trường hợp thực hiện chuyển trả</param>
        ''' <param name="pvStrBtnDuyet"> Mã chức năng: truyền 1: nếu thực hiện Duyệt, truyền 0 nếu thực hiện chuyển trả</param>
        ''' <param name="pvStrTenDN">Tên đăng nhập của KSV</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CheckerMSG312(ByVal pvStrID As String, ByVal pvStrNoidungXL As String, ByVal pvStrBtnDuyet As String, Optional ByVal pvStrTenDN As String = "") As Decimal
            Try
                'Kiểm tra NNT, nếu đã đăng ký thành công và đang hoạt động hoặc đang bị tạm ngưng (hold) hoạt động thì báo lỗi cho Checker
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_HOLD) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH) Then
                                Return Business_HQ.Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'update trạng thái của Đăng ký trong bảngTCS_DM_NTDT_HQ_311
                'Nếu nhận được yêu cầu Duyệt
                If pvStrBtnDuyet.Equals("1") Then
                    strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312 & "'"
                Else
                    'Nếu nhận được yêu cầu là Chuyển trả
                    strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA & "'"
                End If
                strSQL &= " ,ID_NV_CK312='" & pvStrTenDN & "' "
                strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                strSQL &= " WHERE ID='" & pvStrID & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function CheckerMSG312_SuaTK_NH(ByVal pvStrID As String, ByVal pvStrNoidungXL As String, ByVal pvStrBtnDuyet As String, Optional ByVal pvStrTenDN As String = "") As Decimal
            Try
                Dim result_HQ As Decimal = 1
                Dim result_HQ311 As Integer = 1
                'Kiểm tra NNT, nếu đã đăng ký thành công và đang hoạt động hoặc đang bị tạm ngưng (hold) hoạt động thì báo lỗi cho Checker
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH) Then
                                result_HQ = Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                            End If
                        End If
                    End If
                End If

                strSQL = "select * from TCS_DM_NTDT_HQ_311 where ID='" & pvStrID & "' "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312) Then
                                result_HQ311 = Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                            End If
                        End If
                    End If
                End If


                Return result_HQ + result_HQ311

            Catch ex As Exception
            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function

        Public Shared Function CheckNhoThu(ByVal pvSo_HS As String) As Decimal
            Try

                Dim strSQL As String = "select * from tcs_hq247_314 where so_hs='" & pvSo_HS & "' and TRANGTHAI NOT IN ('" & Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "','" & Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "')"
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If

            Catch ex As Exception

            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function CheckerMSG312_SuaTK_NH_Ngung(ByVal pvStrID As String, ByVal pvStrNoidungXL As String, ByVal pvStrBtnDuyet As String, Optional ByVal pvStrTenDN As String = "") As Decimal
            Try
                Dim result_HQ As Decimal = 1
                Dim result_HQ311 As Integer = 1
                'Kiểm tra NNT, nếu đã đăng ký thành công và đang hoạt động hoặc đang bị tạm ngưng (hold) hoạt động thì báo lỗi cho Checker
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH) Then
                                result_HQ = Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                            End If
                        End If
                    End If
                End If

                strSQL = "select * from TCS_DM_NTDT_HQ_311 where ID='" & pvStrID & "' "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312) Then
                                result_HQ311 = Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                            End If
                        End If
                    End If
                End If


                Return result_HQ + result_HQ311

            Catch ex As Exception
            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function CheckNhoThuHS2(ByVal pvSo_HS As String) As Decimal
            Try

                Dim strSQL As String = "select * from tcs_hq247_314 where so_hs='" & pvSo_HS & "' and TRANGTHAI NOT IN ('" & Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "','" & Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "')"
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If

                strSQL = "select * from tcs_hq247_314_tmp where so_hs='" & pvSo_HS & "' and TRANGTHAI NOT IN ('" & Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "','" & Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "','" & Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213_TUCHOI & "')"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function

        ''' <summary>
        ''' Kiểm tra xem Số hồ có tồn tại chứng từ nào đang trong quá trình xử lý hay không ?
        ''' </summary>
        ''' <param name="boolRtn">Kết quả kiểm tra, True: Số hồ sơ này TỒN TẠI Chứng từ đang trong quá trình xử lý, False: Số hồ sơ này KHÔNG tồn tại Chứng từ đang trong quá trình xử lý</param>
        ''' <param name="strSo_HS">Số hồ sơ cần kiểm tra</param>
        ''' <param name="strWhereClause">Điều kiện bổ sung nếu có</param>
        ''' <returns>Hàm trả về 0: nếu truy vấn thành công, != 0 nếu lỗi truy vấn. Tham biến boolRtn trả về kết quả kiểm tra</returns>
        ''' <remarks>Tham biến boolRtn là kết quả trả về của việc kiểm tra</remarks>
        Public Shared Function CheckReceipts(ByRef boolRtn As Boolean, ByVal strSo_HS As String, ByVal strWhereClause As String) As Decimal
            Try
                'Mặc định trước lúc kiểm tra là không có chứng từ nào đang xử lý
                boolRtn = False
                Dim strSQL As String = "SELECT SO_HS FROM  TCS_HQ247_304 "
                strSQL &= " WHERE SO_HS = '" & strSo_HS & "' "
                'Nếu tồn tại chứng từ có TRANGTHAI khác 04 và 07 và 09 có nghĩa là Số hồ sơ đang trong quá trình xử lý
                strSQL &= " AND TRANGTHAI NOT IN ('" & Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_OK & "','" & Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI & "','" & Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER301_ERROR & "') AND "
                strSQL &= strWhereClause
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If ds.Tables(0).Rows(0)("SO_HS").Equals("" & strSo_HS & "") Then
                                boolRtn = True
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.checkSoHS", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        Public Shared Function MakerMSG312Sua(ByVal pvStrID As String, ByVal pvStrNgay_Ky_HDUQ As String, ByVal pvStrCurTaiKhoanTH As String, ByVal pvStrCurTenTaiKhoanTH As String, ByVal pvStrThongtinLienHe As String, Optional ByVal pvStrTenDN As String = "", Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMA_CNTK As String = "") As Decimal
            Try
                'Kiểm tra trong bảng chính, nếu đã có chứng từ ở trạng thái khác active (13) và khác hold (14) thì return
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_HOLD) Then
                                Return Business_HQ.Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'Lấy thông tin tài khoản NNT đăng ký để Nộp thuế điện tử
                'CAP NHAT THONG TIN VAO HQ 311
                'Do có cả 2 phương thức: đăng ký mới tại TCHQ và đăng ký mới tại NHTM đều sử dụng chung Form này, do đó để xác đinh
                'được đâu là đăng ký mới tại TCHQ và đâu là tại NHTM ta kiểm tra ID
                'Trường hợp xác định là đăng ký tại TCHQ
                If Not pvStrID.Equals("") Then
                    Dim maChiNhanh As String = "___"
                    If (Business_HQ.HQ247.daTCS_DM_NTDT_HQ.getCodebranch(maChiNhanh, pvStrTenDN)) = 0 Then
                        strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312 & "'"
                        'Định dạng lại pvStrNgay_Ky_HDUQ theo chuỗi yyyyMMddHHmmss
                        Dim strDateCover As String = pvStrNgay_Ky_HDUQ.Split("/")(2) & pvStrNgay_Ky_HDUQ.Split("/")(1) & pvStrNgay_Ky_HDUQ.Split("/")(0)
                        strSQL &= " ,NGAY_KY_HDUQ=to_date('" & strDateCover & "','RRRRMMDDHH24MISS') ,TAIKHOAN_TH=CUR_TAIKHOAN_TH, TEN_TAIKHOAN_TH=CUR_TEN_TAIKHOAN_TH "
                        strSQL &= " ,CUR_TEN_TAIKHOAN_TH = '" & pvStrCurTenTaiKhoanTH & "' "
                        strSQL &= " ,CUR_TAIKHOAN_TH = '" & pvStrCurTaiKhoanTH & "' "
                        strSQL &= " ,MA_CN = '" & maChiNhanh & "' "
                        strSQL &= " ,CIFNO = '" & pvStrCIFNO & "' "
                        strSQL &= " ,TK_MA_CN = '" & pvStrMA_CNTK & "' "
                        strSQL &= " ,THONGTINLIENHE = '" & pvStrThongtinLienHe & "' "
                        strSQL &= " ,ID_NV_MK312='" & pvStrTenDN & "' "
                        strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                        strSQL &= " WHERE ID='" & pvStrID & "' "
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                    Else
                        Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                    End If

                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function getMSG311Log(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = ""
                strSQL &= "SELECT  A.ID, A.SO_HS, A.LOAI_HS,C.MOTA TENLOAIHS,A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH"
                strSQL &= "  ,A.TAIKHOAN_TH, "
                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311,TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI, "
                strSQL &= "  NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME "
                strSQL &= " , NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.24')),'#') ||'?ID='||A.LOG_ID NAVIGATEURL "

                ' strSQL &= " NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.24')),'#') NAVIGATEURL "
                strSQL &= " FROM TCS_DM_NTDT_HQ_LOG A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_LOAI_HS_NNT C  WHERE A.TRANGTHAI=B.TRANG_THAI And A.LOAI_HS = C.LOAI_HS " & pvStrWhereClause

                strSQL &= " ORDER BY A.NGAY_HT DESC "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getMSG311View(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH"

                strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('01','02','03','04','05','06','07','08') THEN "
                strSQL &= "  A.TAIKHOAN_TH ELSE"
                strSQL &= "  A.CUR_TAIKHOAN_TH"
                strSQL &= "  END ) TAIKHOAN_TH, "

                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, A.NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI, "
                strSQL &= "  NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME "
                strSQL &= " , NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.23')),'#') ||'?ID='||A.ID NAVIGATEURL "
                ' strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('01','02','03','05','06','07','08') THEN "
                'strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.21')),'#') ELSE"
                ' strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.22')),'#')"
                'strSQL &= "  END ) NAVIGATEURL "
                'Nhamlt 20180123: Thêm kênh đăng ký
                strSQL &= ",(CASE WHEN KENH_DK='0' THEN 'TCHQ' WHEN KENH_DK='1' THEN 'NHTM' END) KENH_DK "
                strSQL &= " FROM TCS_DM_NTDT_HQ_311 A,TCS_DM_TRANGTHAI_YEUCAU B WHERE A.TRANGTHAI=B.TRANG_THAI  " & pvStrWhereClause


                strSQL &= " UNION ALL "
                strSQL &= "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH"

                strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('01','02','03','04','05','06','07','08') THEN "
                strSQL &= "  A.TAIKHOAN_TH ELSE"
                strSQL &= "  A.CUR_TAIKHOAN_TH"
                strSQL &= "  END ) TAIKHOAN_TH, "

                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, A.NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI, "
                strSQL &= "  NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME "
                strSQL &= " , NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.23')),'#') ||'?ID='||A.ID NAVIGATEURL "
                'strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('01','02','03','05','06','07','08') THEN "
                'strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.21')),'#') ELSE"
                'strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11%' AND X.NODEID IN ('11.1.22')),'#')"
                'strSQL &= "  END ) NAVIGATEURL "
                'Nhamlt 20180123: Thêm kênh đăng ký
                strSQL &= ",(CASE WHEN A.KENH_DK='0' THEN 'TCHQ' WHEN A.KENH_DK='1' THEN 'NHTM' END) KENH_DK  "
                strSQL &= " FROM TCS_DM_NTDT_HQ A,TCS_DM_TRANGTHAI_YEUCAU B WHERE A.TRANGTHAI=B.TRANG_THAI AND A.TRANGTHAI  IN ('13','15') " & pvStrWhereClause
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
#End Region
        Public Shared Function MakerMSG312(ByVal pvStrID As String, ByVal pvStrNgay_Ky_HDUQ As String, ByVal pvStrCurTaiKhoanTH As String, ByVal pvStrCurTenTaiKhoanTH As String, ByVal pvStrThongtinLienHe As String, Optional ByVal pvStrTenDN As String = "", Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMaCNTK As String = "", Optional ByVal ma_nv As String = "") As Decimal
            Try
                'Kiểm tra trong bảng chính, nếu đã có chứng từ ở trạng thái khác active (13) và khác hold (14) thì return
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_HOLD) Then
                                Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'Lấy thông tin tài khoản NNT đăng ký để Nộp thuế điện tử
                'CAP NHAT THONG TIN VAO HQ 311
                'Do có cả 2 phương thức: đăng ký mới tại TCHQ và đăng ký mới tại NHTM đều sử dụng chung Form này, do đó để xác đinh
                'được đâu là đăng ký mới tại TCHQ và đâu là tại NHTM ta kiểm tra ID
                'Trường hợp xác định là đăng ký tại TCHQ
                If Not pvStrID.Equals("") Then
                    Dim maChiNhanh As String = "___"
                    If (HQ247.daTCS_DM_NTDT_HQ.getCodebranch(maChiNhanh, pvStrTenDN)) = 0 Then
                        strSQL = "UPDATE TCS_DM_NTDT_HQ_311 SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312 & "'"
                        'Định dạng lại pvStrNgay_Ky_HDUQ theo chuỗi yyyyMMddHHmmss
                        Dim strDateCover As String = pvStrNgay_Ky_HDUQ.Split("/")(2) & pvStrNgay_Ky_HDUQ.Split("/")(1) & pvStrNgay_Ky_HDUQ.Split("/")(0)
                        strSQL &= " ,NGAY_KY_HDUQ=to_date('" & strDateCover & "','RRRRMMDDHH24MISS') "
                        strSQL &= " ,CIFNO = '" & pvStrCIFNO & "' "
                        strSQL &= " ,TK_MA_CN = '" & pvStrMaCNTK & "' "
                        strSQL &= " ,CUR_TEN_TAIKHOAN_TH =:CUR_TEN_TAIKHOAN_TH  "
                        strSQL &= " ,CUR_TAIKHOAN_TH = '" & pvStrCurTaiKhoanTH & "' "
                        strSQL &= " ,MA_NV_TH = '" & ma_nv & "' "
                        strSQL &= " ,MA_CN = '" & maChiNhanh & "' "
                        '    strSQL &= " ,THONGTINLIENHE =:THONGTINLIENHE  "
                        strSQL &= " ,ID_NV_MK312='" & pvStrTenDN & "' "
                        strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                        strSQL &= " WHERE ID='" & pvStrID & "' "

                        Dim lsParam As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                        lsParam.Add(VBOracleLib.DataAccess.NewDBParameter("CUR_TEN_TAIKHOAN_TH", ParameterDirection.Input, pvStrCurTenTaiKhoanTH, DbType.String))
                        'lsParam.Add(VBOracleLib.DataAccess.NewDBParameter("THONGTINLIENHE", ParameterDirection.Input, pvStrThongtinLienHe, DbType.String))



                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lsParam.ToArray())
                        Return Common.mdlCommon.TCS_HQ247_OK
                    Else
                        Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                    End If

                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
    End Class
End Namespace

