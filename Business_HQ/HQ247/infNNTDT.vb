Namespace HQ247.NNTDT
    Public Class infNNTDT
        Private strID As String
        Private strSo_HS As String
        Private strLoai_HS As String
        Private strMa_DV As String
        Private strTen_DV As String
        Private strDiaChi As String
        Private strHo_Ten As String
        Private strSo_CMT As String
        Private strNgaySinh As String
        Private strNguyenQuan As String
        Private strSo_DT As String
        Private strEmail As String
        Private strSerialNumber As String
        Private strNoi_Cap As String
        Private strNgay_HL As String
        Private strNgay_HHL As String
        Private strMa_NH_TH As String
        Private strTen_NH_TH As String
        Private strTaiKhoan_TH As String
        Private strTransaction_Date As String
        Private strNoiDung_XL As String
        Private strChk_SoTaiKhoan As String
        Private strtxtTenTaiKhoan As String
        Private txtSo_TK_NTDT As String
        Private txtLyDo_Huy As String
        Private txtLydo_ChuyenTra As String



        Sub New()
            strID = ""
            strSo_HS = ""
            strLoai_HS = ""
            strMa_DV = ""
            strTen_DV = ""
            strDiaChi = ""
            strHo_Ten = ""
            strSo_CMT = ""
            strNgaySinh = ""
            strNguyenQuan = ""
            strSo_DT = ""
            strEmail = ""
            strSerialNumber = ""
            strNoi_Cap = ""
            strNgay_HL = ""
            strNgay_HHL = ""
            strMa_NH_TH = ""
            strTen_NH_TH = ""
            strTaiKhoan_TH = ""
            strTransaction_Date = ""
            strNoiDung_XL = ""
            strChk_SoTaiKhoan = ""
            strtxtTenTaiKhoan = ""
            txtSo_TK_NTDT = ""
            txtLyDo_Huy = ""
            txtLydo_ChuyenTra = ""
        End Sub

        Sub New(ByVal drwNNTDT As DataRow)
            strID = drwNNTDT("ID").ToString().Trim()
            strSo_HS = drwNNTDT("SO_HS").ToString().Trim()
            strLoai_HS = drwNNTDT("LOAI_HS").ToString().Trim()
            strMa_DV = drwNNTDT("MA_DV").ToString().Trim()
            strTen_DV = drwNNTDT("TEN_DV").ToString().Trim()
            strDiaChi = drwNNTDT("DIACHI").ToString().Trim()
            strHo_Ten = drwNNTDT("HO_TEN").ToString().Trim()
            strSo_CMT = drwNNTDT("SO_CMT").ToString().Trim()
            strNgaySinh = drwNNTDT("NGAYSINH").ToString().Trim()
            strNguyenQuan = drwNNTDT("NGUYENQUAN").ToString().Trim()
            strSo_DT = drwNNTDT("SO_DT").ToString().Trim()
            strEmail = drwNNTDT("EMAIL").ToString().Trim()
            strSerialNumber = drwNNTDT("SERIALNUMBER").ToString().Trim()
            strNoi_Cap = drwNNTDT("NOICAP").ToString().Trim()
            strNgay_HL = drwNNTDT("NGAY_HL").ToString().Trim()
            strNgay_HHL = drwNNTDT("NGAY_HHL").ToString().Trim()
            strMa_NH_TH = drwNNTDT("MA_NH_TH").ToString().Trim()
            strTen_NH_TH = drwNNTDT("CUR_TEN_TAIKHOAN_TH").ToString().Trim()
            strTaiKhoan_TH = drwNNTDT("CUR_TAIKHOAN_TH").ToString().Trim()
            'strTransaction_Date = drwNNTDT("NNTDT_ID").ToString().Trim()
            'strNoiDung_XL = drwNNTDT("NNTDT_ID").ToString().Trim()
            'strChk_SoTaiKhoan = drwNNTDT("NNTDT_ID").ToString().Trim()
            'strtxtTenTaiKhoan = drwNNTDT("NNTDT_ID").ToString().Trim()
            'txtSo_TK_NTDT = drwNNTDT("NNTDT_ID").ToString().Trim()
            txtLyDo_Huy = drwNNTDT("LY_DO_HUY").ToString().Trim()
            txtLydo_ChuyenTra = drwNNTDT("LY_DO_CHUYEN_TRA").ToString().Trim()
        End Sub
        Public Property ID() As String
            Get
                Return strID
            End Get
            Set(ByVal Value As String)
                strID = Value
            End Set
        End Property
        Public Property SO_HS() As String
            Get
                Return strSo_HS
            End Get
            Set(ByVal Value As String)
                strSo_HS = Value
            End Set
        End Property
        Public Property LOAI_HS() As String
            Get
                Return strLoai_HS
            End Get
            Set(ByVal Value As String)
                strLoai_HS = Value
            End Set
        End Property
        Public Property MA_DV() As String
            Get
                Return strMa_DV
            End Get
            Set(ByVal Value As String)
                strMa_DV = Value
            End Set
        End Property
        Public Property TEN_DV() As String
            Get
                Return strTen_DV
            End Get
            Set(ByVal Value As String)
                strTen_DV = Value
            End Set
        End Property
        Public Property DIACHI() As String
            Get
                Return strDiaChi
            End Get
            Set(ByVal Value As String)
                strDiaChi = Value
            End Set
        End Property
        Public Property HO_TEN() As String
            Get
                Return strHo_Ten
            End Get
            Set(ByVal Value As String)
                strHo_Ten = Value
            End Set
        End Property
        Public Property SO_CMT() As String
            Get
                Return strSo_CMT
            End Get
            Set(ByVal Value As String)
                strSo_CMT = Value
            End Set
        End Property
        Public Property NGAYSINH() As String
            Get
                Return strNgaySinh
            End Get
            Set(ByVal Value As String)
                strNgaySinh = Value
            End Set
        End Property
        Public Property NGUYENQUAN() As String
            Get
                Return strNguyenQuan
            End Get
            Set(ByVal Value As String)
                strNguyenQuan = Value
            End Set
        End Property
        Public Property SO_DT() As String
            Get
                Return strSo_DT
            End Get
            Set(ByVal Value As String)
                strSo_DT = Value
            End Set
        End Property
        Public Property EMAIL() As String
            Get
                Return strEmail
            End Get
            Set(ByVal Value As String)
                strEmail = Value
            End Set
        End Property
        Public Property SERIALNUMBER() As String
            Get
                Return strSerialNumber
            End Get
            Set(ByVal Value As String)
                strSerialNumber = Value
            End Set
        End Property
        Public Property NOICAP() As String
            Get
                Return strNoi_Cap
            End Get
            Set(ByVal Value As String)
                strNoi_Cap = Value
            End Set
        End Property
        Public Property NGAY_HL() As String
            Get
                Return strNgay_HL
            End Get
            Set(ByVal Value As String)
                strNgay_HL = Value
            End Set
        End Property
        Public Property NGAY_HHL() As String
            Get
                Return strNgay_HHL
            End Get
            Set(ByVal Value As String)
                strNgay_HHL = Value
            End Set
        End Property
        Public Property MA_NH_TH() As String
            Get
                Return strMa_NH_TH
            End Get
            Set(ByVal Value As String)
                strMa_NH_TH = Value
            End Set
        End Property
        Public Property CUR_TEN_TAIKHOAN_TH() As String
            Get
                Return strTen_NH_TH
            End Get
            Set(ByVal Value As String)
                strTen_NH_TH = Value
            End Set
        End Property
        Public Property CUR_TAIKHOAN_TH() As String
            Get
                Return strTaiKhoan_TH
            End Get
            Set(ByVal Value As String)
                strTaiKhoan_TH = Value
            End Set
        End Property
        Public Property LY_DO_HUY() As String
            Get
                Return txtLyDo_Huy
            End Get
            Set(ByVal Value As String)
                txtLyDo_Huy = Value
            End Set
        End Property
        Public Property LY_DO_CHUYEN_TRA() As String
            Get
                Return txtLydo_ChuyenTra
            End Get
            Set(ByVal Value As String)
                txtLydo_ChuyenTra = Value
            End Set
        End Property


    End Class

End Namespace
