﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Xml.Serialization
Imports System.Xml
Imports System.IO
'Imports CustomsV3.MSG.MSGHeaderCustoms
'Imports CustomsV3.MSG.SECURITY
'Imports CustomsV3.MSG.MSGError
Imports Business_HQ.HQ247
Imports System.Data

Namespace HQ247
    <Serializable()> _
    <XmlRootAttribute("Customs", [Namespace]:="http://www.cpandl.com", IsNullable:=False)> _
    Public Class MSG3041
        Public Sub New()
            [Error] = New [Error]()
        End Sub
        <XmlElement("Header")> _
        Public Header As HeaderCustomshq
        <XmlElement("Data")> _
        Public Data As Data
        <XmlElement("Error")> _
        Public [Error] As [Error]
        <XmlElement("Signature")> _
        Public Signature As Signature


        Public Function MSGToObject(ByVal p_XML As String) As MSG3041
            Dim serializer As New XmlSerializer(GetType(MSG3041))


            Dim reader As XmlReader = XmlReader.Create(New StringReader(p_XML))
            Dim LoadedObjTmp As MSG3041 = DirectCast(serializer.Deserialize(reader), MSG3041)
            Return LoadedObjTmp


        End Function

        Public Sub SETDATA(ByVal p_XML As String)
            Dim serializer As New XmlSerializer(GetType(MSG3041))


            Dim reader As XmlReader = XmlReader.Create(New StringReader(p_XML))
            Dim LoadedObjTmp As MSG3041 = DirectCast(serializer.Deserialize(reader), MSG3041)
            Header = LoadedObjTmp.Header
            Data = LoadedObjTmp.Data

            strXML = p_XML


        End Sub

        Public Property strXML() As String
            Get
                Return m_strXML
            End Get
            Set(ByVal value As String)
                m_strXML = value
            End Set
        End Property
        Private m_strXML As String
        Public Function ObjectMSG304DC(ByVal strID As String) As MSG304DC_HDR
            Dim objhdr As New MSG304DC_HDR()
            Try
                objhdr.NGAYLAP_CT = Data.ThongTinChungTu.NgayLap_CT
                objhdr.NGAYTRUYEN_CT = Data.ThongTinChungTu.NgayTruyen_CT
                objhdr.MA_DV = Data.ThongTinChungTu.Ma_DV
                objhdr.MA_CHUONG = Data.ThongTinChungTu.Ma_Chuong
                objhdr.TEN_DV = Data.ThongTinChungTu.Ten_DV
                objhdr.MA_KB = Data.ThongTinChungTu.Ma_KB
                objhdr.TEN_KB = Data.ThongTinChungTu.Ten_KB
                objhdr.TKKB = Data.ThongTinChungTu.TKKB
                objhdr.MA_NTK = Data.ThongTinChungTu.Ma_NTK
                objhdr.MA_HQ_PH = Data.ThongTinChungTu.Ma_HQ_PH
                objhdr.MA_HQ_CQT = Data.ThongTinChungTu.Ma_HQ_CQT
                objhdr.KYHIEU_CT = Data.ThongTinChungTu.KyHieu_CT
                objhdr.SO_CT = Data.ThongTinChungTu.So_CT
                objhdr.LOAI_CT = Data.ThongTinChungTu.Loai_CT
                objhdr.NGAY_BN = Data.ThongTinChungTu.Ngay_BN
                objhdr.NGAY_CT = Data.ThongTinChungTu.Ngay_CT
                objhdr.MA_NT = Data.ThongTinChungTu.Ma_NT
                objhdr.TY_GIA = Data.ThongTinChungTu.Ty_Gia
                objhdr.SOTIEN_TO = Data.ThongTinChungTu.SoTien_TO
                objhdr.DIENGIAI = Data.ThongTinChungTu.DienGiai
                objhdr.MA_ST = Data.ThongTinGiaoDich.NguoiNopTien.Ma_ST
                objhdr.SO_CMT = Data.ThongTinGiaoDich.NguoiNopTien.So_CMT
                objhdr.TEN_NNT = Data.ThongTinGiaoDich.NguoiNopTien.Ten_NNT
                objhdr.DIACHI = Data.ThongTinGiaoDich.NguoiNopTien.DiaChi
                objhdr.TT_KHAC = Data.ThongTinGiaoDich.NguoiNopTien.TT_Khac
                objhdr.MA_NH_TH = Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ma_NH_TH
                objhdr.TEN_NH_TH = Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_NH_TH
                objhdr.TAIKHOAN_TH = Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH
                objhdr.TEN_TAIKHOAN_TH = Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH
                For Each objGNT_CT As GNT_CT In Data.ThongTinChungTu.GNT_CT
                    For Each objToKhai_CT As ToKhai_CT In objGNT_CT.ToKhai_CT
                        Dim objdtl As New MSG304DC_DTL()
                        objdtl.ID = strID
                        objdtl.ID_HS = objGNT_CT.IS_HS
                        objdtl.MA_HQ = objGNT_CT.Ma_HQ
                        objdtl.MA_LH = objGNT_CT.Ma_LH
                        objdtl.MA_LT = objGNT_CT.Ma_LT
                        objdtl.SO_TK = objGNT_CT.So_TK
                        objdtl.NAM_DK = objGNT_CT.Nam_DK
                        objdtl.TTBUTTOAN = objGNT_CT.TTButToan
                        objdtl.MA_ST = objToKhai_CT.Ma_ST
                        objdtl.NDKT = objToKhai_CT.NDKT
                        objdtl.SOTIEN_NT = objToKhai_CT.SoTien_NT
                        objdtl.SOTIEN_VND = objToKhai_CT.SoTien_VND
                        objhdr.MSG304DC_DTL.Add(objdtl)
                    Next
                Next
                Return objhdr
            Catch ex As Exception
            End Try
            Return Nothing
        End Function

    End Class

    Public Class ThongTinChungTu
        Public Sub New()
            GNT_CT = New List(Of GNT_CT)()
        End Sub
        <XmlElement("NgayLap_CT")> _
        Public Property NgayLap_CT() As String
            Get
                Return m_NgayLap_CT
            End Get
            Set(ByVal value As String)
                m_NgayLap_CT = value
            End Set
        End Property
        Private m_NgayLap_CT As String
        <XmlElement("NgayTruyen_CT")> _
        Public Property NgayTruyen_CT() As String
            Get
                Return m_NgayTruyen_CT
            End Get
            Set(ByVal value As String)
                m_NgayTruyen_CT = value
            End Set
        End Property
        Private m_NgayTruyen_CT As String
        <XmlElement("Ma_DV")> _
        Public Property Ma_DV() As String
            Get
                Return m_Ma_DV
            End Get
            Set(ByVal value As String)
                m_Ma_DV = value
            End Set
        End Property
        Private m_Ma_DV As String
        <XmlElement("Ma_Chuong")> _
        Public Property Ma_Chuong() As String
            Get
                Return m_Ma_Chuong
            End Get
            Set(ByVal value As String)
                m_Ma_Chuong = value
            End Set
        End Property
        Private m_Ma_Chuong As String
        <XmlElement("Ten_DV")> _
        Public Property Ten_DV() As String
            Get
                Return m_Ten_DV
            End Get
            Set(ByVal value As String)
                m_Ten_DV = value
            End Set
        End Property
        Private m_Ten_DV As String
        <XmlElement("Ma_KB")> _
        Public Property Ma_KB() As String
            Get
                Return m_Ma_KB
            End Get
            Set(ByVal value As String)
                m_Ma_KB = value
            End Set
        End Property
        Private m_Ma_KB As String
        <XmlElement("Ten_KB")> _
        Public Property Ten_KB() As String
            Get
                Return m_Ten_KB
            End Get
            Set(ByVal value As String)
                m_Ten_KB = value
            End Set
        End Property
        Private m_Ten_KB As String
        <XmlElement("TKKB")> _
        Public Property TKKB() As String
            Get
                Return m_TKKB
            End Get
            Set(ByVal value As String)
                m_TKKB = value
            End Set
        End Property
        Private m_TKKB As String
        <XmlElement("Ma_NTK")> _
        Public Property Ma_NTK() As String
            Get
                Return m_Ma_NTK
            End Get
            Set(ByVal value As String)
                m_Ma_NTK = value
            End Set
        End Property
        Private m_Ma_NTK As String
        <XmlElement("Ma_HQ_PH")> _
        Public Property Ma_HQ_PH() As String
            Get
                Return m_Ma_HQ_PH
            End Get
            Set(ByVal value As String)
                m_Ma_HQ_PH = value
            End Set
        End Property
        Private m_Ma_HQ_PH As String
        <XmlElement("Ma_HQ_CQT")> _
        Public Property Ma_HQ_CQT() As String
            Get
                Return m_Ma_HQ_CQT
            End Get
            Set(ByVal value As String)
                m_Ma_HQ_CQT = value
            End Set
        End Property
        Private m_Ma_HQ_CQT As String

        <XmlElement("KyHieu_CT")> _
        Public Property KyHieu_CT() As String
            Get
                Return m_KyHieu_CT
            End Get
            Set(ByVal value As String)
                m_KyHieu_CT = value
            End Set
        End Property
        Private m_KyHieu_CT As String
        <XmlElement("So_CT")> _
        Public Property So_CT() As String
            Get
                Return m_So_CT
            End Get
            Set(ByVal value As String)
                m_So_CT = value
            End Set
        End Property
        Private m_So_CT As String
        <XmlElement("Loai_CT")> _
        Public Property Loai_CT() As String
            Get
                Return m_Loai_CT
            End Get
            Set(ByVal value As String)
                m_Loai_CT = value
            End Set
        End Property
        Private m_Loai_CT As String
        <XmlElement("Ngay_BN")> _
        Public Property Ngay_BN() As String
            Get
                Return m_Ngay_BN
            End Get
            Set(ByVal value As String)
                m_Ngay_BN = value
            End Set
        End Property
        Private m_Ngay_BN As String
        <XmlElement("Ngay_CT")> _
        Public Property Ngay_CT() As String
            Get
                Return m_Ngay_CT
            End Get
            Set(ByVal value As String)
                m_Ngay_CT = value
            End Set
        End Property
        Private m_Ngay_CT As String
        <XmlElement("Ma_NT")> _
        Public Property Ma_NT() As String
            Get
                Return m_Ma_NT
            End Get
            Set(ByVal value As String)
                m_Ma_NT = value
            End Set
        End Property
        Private m_Ma_NT As String
        <XmlElement("Ty_Gia")> _
        Public Property Ty_Gia() As String
            Get
                Return m_Ty_Gia
            End Get
            Set(ByVal value As String)
                m_Ty_Gia = value
            End Set
        End Property
        Private m_Ty_Gia As String
        <XmlElement("SoTien_TO")> _
        Public Property SoTien_TO() As String
            Get
                Return m_SoTien_TO
            End Get
            Set(ByVal value As String)
                m_SoTien_TO = value
            End Set
        End Property
        Private m_SoTien_TO As String
        <XmlElement("DienGiai")> _
        Public Property DienGiai() As String
            Get
                Return m_DienGiai
            End Get
            Set(ByVal value As String)
                m_DienGiai = value
            End Set
        End Property
        Private m_DienGiai As String
        <XmlElement("GNT_CT")> _
        Public GNT_CT As List(Of GNT_CT)

        Public Function convertGNT_CTToDS() As DataSet
            Dim ds As New DataSet()

            Try
                Dim tbl As New DataTable()
                tbl.Columns.Add("ID_HS")
                tbl.Columns.Add("TTButToan")
                tbl.Columns.Add("Ma_HQ")
                tbl.Columns.Add("Ma_LH")
                tbl.Columns.Add("Nam_DK")
                tbl.Columns.Add("So_TK")
                tbl.Columns.Add("Ma_LT")
                tbl.Columns.Add("Ma_ST")
                tbl.Columns.Add("MTMUC")
                tbl.Columns.Add("NDKT")
                tbl.Columns.Add("SoTien_NT")
                tbl.Columns.Add("SoTien_VND")
                ds.Tables.Add(tbl)

                For i As Integer = 0 To GNT_CT.Count - 1
                    For x As Integer = 0 To GNT_CT(i).ToKhai_CT.Count - 1
                        Dim vrow As DataRow = ds.Tables(0).NewRow()
                        vrow("ID_HS") = GNT_CT(i).IS_HS
                        vrow("TTButToan") = GNT_CT(i).TTButToan
                        vrow("Ma_HQ") = GNT_CT(i).Ma_HQ
                        vrow("Ma_LH") = GNT_CT(i).Ma_LH
                        vrow("So_TK") = GNT_CT(i).So_TK
                        vrow("Nam_DK") = GNT_CT(i).Nam_DK
                        vrow("Ma_LT") = GNT_CT(i).Ma_LT
                        vrow("Ma_ST") = GNT_CT(i).ToKhai_CT(x).Ma_ST
                        vrow("MTMUC") = GNT_CT(i).ToKhai_CT(x).NDKT
                        vrow("NDKT") = Business_HQ.Common.mdlCommon.Get_TenMTM(GNT_CT(i).ToKhai_CT(x).NDKT)
                        vrow("SoTien_NT") = GNT_CT(i).ToKhai_CT(x).SoTien_NT
                        vrow("SoTien_VND") = GNT_CT(i).ToKhai_CT(x).SoTien_VND
                        ds.Tables(0).Rows.Add(vrow)
                    Next

                Next
            Catch ex As Exception
            End Try
            Return ds
        End Function

    End Class

    Public Class GNT_CT
        Public Sub New()
            ToKhai_CT = New List(Of ToKhai_CT)()
        End Sub
        <XmlElement("ID_HS")> _
        Public Property IS_HS() As String
            Get
                Return m_IS_HS
            End Get
            Set(ByVal value As String)
                m_IS_HS = value
            End Set
        End Property
        Private m_IS_HS As String
        <XmlElement("TTButToan")> _
        Public Property TTButToan() As String
            Get
                Return m_TTButToan
            End Get
            Set(ByVal value As String)
                m_TTButToan = value
            End Set
        End Property
        Private m_TTButToan As String
        <XmlElement("Ma_HQ")> _
        Public Property Ma_HQ() As String
            Get
                Return m_Ma_HQ
            End Get
            Set(ByVal value As String)
                m_Ma_HQ = value
            End Set
        End Property
        Private m_Ma_HQ As String

        <XmlElement("Ma_LH")> _
        Public Property Ma_LH() As String
            Get
                Return m_Ma_LH
            End Get
            Set(ByVal value As String)
                m_Ma_LH = value
            End Set
        End Property
        Private m_Ma_LH As String
        <XmlElement("Nam_DK")> _
        Public Property Nam_DK() As String
            Get
                Return m_Nam_DK
            End Get
            Set(ByVal value As String)
                m_Nam_DK = value
            End Set
        End Property
        Private m_Nam_DK As String
        <XmlElement("So_TK")> _
        Public Property So_TK() As String
            Get
                Return m_So_TK
            End Get
            Set(ByVal value As String)
                m_So_TK = value
            End Set
        End Property
        Private m_So_TK As String
        <XmlElement("Ma_LT")> _
        Public Property Ma_LT() As String
            Get
                Return m_Ma_LT
            End Get
            Set(ByVal value As String)
                m_Ma_LT = value
            End Set
        End Property
        Private m_Ma_LT As String
        <XmlElement("ToKhai_CT")> _
        Public ToKhai_CT As List(Of ToKhai_CT)
    End Class
    Public Class ToKhai_CT
        Public Sub New()
        End Sub
        <XmlElement("Ma_ST")> _
        Public Property Ma_ST() As String
            Get
                Return m_Ma_ST
            End Get
            Set(ByVal value As String)
                m_Ma_ST = value
            End Set
        End Property
        Private m_Ma_ST As String
        <XmlElement("NDKT")> _
        Public Property NDKT() As String
            Get
                Return m_NDKT
            End Get
            Set(ByVal value As String)
                m_NDKT = value
            End Set
        End Property
        Private m_NDKT As String
        <XmlElement("SoTien_NT")> _
        Public Property SoTien_NT() As String
            Get
                Return m_SoTien_NT
            End Get
            Set(ByVal value As String)
                m_SoTien_NT = value
            End Set
        End Property
        Private m_SoTien_NT As String
        <XmlElement("SoTien_VND")> _
        Public Property SoTien_VND() As String
            Get
                Return m_SoTien_VND
            End Get
            Set(ByVal value As String)
                m_SoTien_VND = value
            End Set
        End Property
        Private m_SoTien_VND As String

    End Class
    Public Class ThongTinGiaoDich
        Public Sub New()
            NguoiNopTien = New NguoiNopTien()

            TaiKhoan_NopTien = New TaiKhoan_NopTien()
        End Sub
        <XmlElement("NguoiNopTien")> _
        Public NguoiNopTien As NguoiNopTien
        <XmlElement("TaiKhoan_NopTien")> _
        Public TaiKhoan_NopTien As TaiKhoan_NopTien

    End Class
    Public Class NguoiNopTien

        Public Sub New()
        End Sub
        <XmlElement("Ma_ST")> _
        Public Property Ma_ST() As String
            Get
                Return m_Ma_ST
            End Get
            Set(ByVal value As String)
                m_Ma_ST = value
            End Set
        End Property
        Private m_Ma_ST As String
        <XmlElement("So_CMT")> _
        Public Property So_CMT() As String
            Get
                Return m_So_CMT
            End Get
            Set(ByVal value As String)
                m_So_CMT = value
            End Set
        End Property
        Private m_So_CMT As String
        <XmlElement("Ten_NNT")> _
        Public Property Ten_NNT() As String
            Get
                Return m_Ten_NNT
            End Get
            Set(ByVal value As String)
                m_Ten_NNT = value
            End Set
        End Property
        Private m_Ten_NNT As String
        <XmlElement("DiaChi")> _
        Public Property DiaChi() As String
            Get
                Return m_DiaChi
            End Get
            Set(ByVal value As String)
                m_DiaChi = value
            End Set
        End Property
        Private m_DiaChi As String
        <XmlElement("TT_Khac")> _
        Public Property TT_Khac() As String
            Get
                Return m_TT_Khac
            End Get
            Set(ByVal value As String)
                m_TT_Khac = value
            End Set
        End Property
        Private m_TT_Khac As String

    End Class
    Public Class TaiKhoan_NopTien

        Public Sub New()
        End Sub
        <XmlElement("Ma_NH_TH")> _
        Public Property Ma_NH_TH() As String
            Get
                Return m_Ma_NH_TH
            End Get
            Set(ByVal value As String)
                m_Ma_NH_TH = value
            End Set
        End Property
        Private m_Ma_NH_TH As String
        <XmlElement("Ten_NH_TH")> _
        Public Property Ten_NH_TH() As String
            Get
                Return m_Ten_NH_TH
            End Get
            Set(ByVal value As String)
                m_Ten_NH_TH = value
            End Set
        End Property
        Private m_Ten_NH_TH As String
        <XmlElement("TaiKhoan_TH")> _
        Public Property TaiKhoan_TH() As String
            Get
                Return m_TaiKhoan_TH
            End Get
            Set(ByVal value As String)
                m_TaiKhoan_TH = value
            End Set
        End Property
        Private m_TaiKhoan_TH As String
        <XmlElement("Ten_TaiKhoan_TH")> _
        Public Property Ten_TaiKhoan_TH() As String
            Get
                Return m_Ten_TaiKhoan_TH
            End Get
            Set(ByVal value As String)
                m_Ten_TaiKhoan_TH = value
            End Set
        End Property
        Private m_Ten_TaiKhoan_TH As String

    End Class
    Public Class Data
        Public Sub New()
            ThongTinChungTu = New ThongTinChungTu()
            ThongTinGiaoDich = New ThongTinGiaoDich()
        End Sub
        <XmlElement("ThongTinChungTu")> _
        Public ThongTinChungTu As ThongTinChungTu
        <XmlElement("ThongTinGiaoDich")> _
        Public ThongTinGiaoDich As ThongTinGiaoDich
    End Class
    Public Class [Error]
        Public Sub New()
        End Sub
        Public Property ErrorNumber() As String
            Get
                Return m_ErrorNumber
            End Get
            Set(ByVal value As String)
                m_ErrorNumber = value
            End Set
        End Property
        Private m_ErrorNumber As String
        Public Property ErrorMessage() As String
            Get
                Return m_ErrorMessage
            End Get
            Set(ByVal value As String)
                m_ErrorMessage = value
            End Set
        End Property
        Private m_ErrorMessage As String
    End Class

    Public Class Signature
        Public Sub New()
        End Sub

    End Class
    Public Class HeaderCustomshq
        Public Sub New()
        End Sub

    End Class


End Namespace

