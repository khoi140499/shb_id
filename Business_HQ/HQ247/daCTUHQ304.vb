﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.Common
Imports System.Data.OracleClient
Imports System.Configuration

Namespace HQ247
    Public Class daCTUHQ304
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Shared Function CheckValidSoCTU(ByVal p_strSoCTU As String, ByVal p_StrkyhieuCTU As String, ByVal p_StrNgayLapCT As String) As Decimal
            Try
                'kiem tra so chung tu
                'tam dong doan nay do BA yêu cau chi chekc so CTU
                'Dim strSQL As String = "select * from TCS_HQ247_304 where HQ_SO_CTU='" & p_strSoCTU & "' AND HQ_KYHIEU_CTU='" & p_StrkyhieuCTU & "' AND NGAY_LAP=TO_DATE('" & p_StrNgayLapCT & "','RRRR-MM-DD')"
                Dim strSQL As String = "select * from TCS_HQ247_304 where HQ_SO_CTU='" & p_strSoCTU & "' and HQ_KYHIEU_CTU ='" & p_StrkyhieuCTU & "'"
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return mdlCommon.TCS_HQ247_HQ_TRUNGSO_HQ_CTU
                        End If
                    End If
                End If
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("CheckValidSoCTU  p_strSoCTU[" & p_strSoCTU & "] p_StrkyhieuCTU[" & p_StrkyhieuCTU & "] p_StrNgayLapCT[" & p_StrNgayLapCT & "] ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "CheckValidSoCTU  p_strSoCTU[" & p_strSoCTU & "] p_StrkyhieuCTU[" & p_StrkyhieuCTU & "] p_StrNgayLapCT[" & p_StrNgayLapCT & "] ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        'Nhamlt 20180123_ them ham
        Public Shared Function CheckValidNgayTruyenCT(ByVal p_NgayTruyenCT As String, ByVal p_Ma_DV As String, ByVal p_TAIKHOAN As String) As Decimal
            Try
                'kiem tra so chung tu
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where TRANGTHAI='" & mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "' "
                strSQL &= " AND TO_DATE('" & p_NgayTruyenCT & "','DD/MM/YYYY') <= TRUNC(NGAY_KY_HDUQ)"
                strSQL &= " AND MA_DV='" & p_Ma_DV & "' AND TAIKHOAN_TH ='" & p_TAIKHOAN & "'"
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                Return mdlCommon.TCS_HQ247_HQ_SAI_TAIKHOAN_TH
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function CheckValidTAIKHOAN_TH(ByVal p_StrMa_DV As String, ByVal p_StrTaiKhoan_TH As String, Optional ByVal p_StrTenTK As String = "") As Decimal
            Try
                'kiem tra so chung tu
                Dim strSQL As String = "select * from TCS_DM_NTDT_HQ where TRANGTHAI='" & mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "' "
                strSQL &= " AND MA_DV='" & p_StrMa_DV & "'"
                strSQL &= " AND TAIKHOAN_TH='" & p_StrTaiKhoan_TH & "'"
                If p_StrTenTK.Length > 0 Then
                    strSQL &= " AND UPPER(TEN_TAIKHOAN_TH)=UPPER('" & p_StrTenTK & "')"
                End If
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                Return mdlCommon.TCS_HQ247_HQ_SAI_TAIKHOAN_TH
            Catch ex As Exception
                log.Error("CheckValidTAIKHOAN_TH  p_StrMa_DV[" & p_StrMa_DV & "] p_StrTaiKhoan_TH[" & p_StrTaiKhoan_TH & "] p_StrTenTK[" & p_StrTenTK & "] ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "CheckValidTAIKHOAN_TH  p_StrMa_DV[" & p_StrMa_DV & "] p_StrTaiKhoan_TH[" & p_StrTaiKhoan_TH & "] p_StrTenTK[" & p_StrTenTK & "] ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function InsertMSG304(ByVal pStrSoCT As String, ByVal pStrKyHieuCT As String, ByVal pStrMaNT As String, ByVal pStrSoTienTO As String, _
                                            ByVal pStrMaDV As String, ByVal pStrTenDV As String, ByVal pStrNgayLapCT As String, ByVal pStrNgayTruyenCT As String, _
                                            ByVal pStrTyGia As String, ByVal pStrTaiKhoanTH As String, ByVal pStrTenTaiKhoanTH As String, ByVal pStrMSG As String, ByVal PSTRMSG_ID304 As String, ByVal pStrNgayCT As String) As Decimal
            Dim strSQL As String = "SELECT TO_CHAR(SYSDATE,'RRRRMMDD')||LPAD(SEQ_TCS_HQ247_304.NEXTVAL,10,'0') FROM DUAL"
            Dim ds As DataSet = Nothing
            Dim StrID As String = ""
            Dim _cmd As OracleCommand = New OracleCommand()
            Dim _conn As OracleConnection = DataAccess.GetConnection() 'New OracleConnection(VBOracleLib.DataAccess.strConnection)
            Try
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                StrID = ds.Tables(0).Rows(0)(0).ToString()
                strSQL = "INSERT INTO TCS_HQ247_304 (ID, HQ_SO_CTU, HQ_KYHIEU_CTU,SO_HS, MA_NT,HQ_SOTIEN_TO, MA_DV, TEN_DV, TRANGTHAI, HQ_NGAYLAP_CT,"
                strSQL &= " HQ_NGAYTRUYEN_CT, TY_GIA, TAIKHOAN_TH, TEN_TAIKHOAN_TH,"
                strSQL &= " MSG_CONTENT, LOAI213,MSG_ID304,NGAY_HT,HQ_NGAY_CT,ma_CN,NGAYNHANMSG) VALUES("
                strSQL &= "'" & StrID & "',"
                strSQL &= "'" & pStrSoCT & "',"
                strSQL &= "'" & pStrKyHieuCT & "',"
                strSQL &= "nvl((select max(so_hs) from tcs_dm_ntdt_hq where trangthai='" & mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "'  AND MA_DV='" & pStrMaDV & "' AND TAIKHOAN_TH='" & pStrTaiKhoanTH & "' ),'____'),"
                strSQL &= "'" & pStrMaNT & "',"
                strSQL &= "" & pStrSoTienTO & ","
                strSQL &= "'" & pStrMaDV & "',"
                strSQL &= "'" & pStrTenDV & "',"
                strSQL &= "'" & mdlCommon.TCS_HQ247_TRANGTHAI_304_MOINHAN & "',"
                strSQL &= "TO_DATE('" & pStrNgayLapCT.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "TO_DATE('" & pStrNgayTruyenCT.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                If pStrTyGia.Length > 0 Then
                    strSQL &= "" & pStrTyGia & ","
                Else
                    strSQL &= "0,"
                End If
                strSQL &= "'" & pStrTaiKhoanTH & "',"
                strSQL &= "'" & pStrTenTaiKhoanTH & "',"
                'NOI DUNG 304 NHAN VE
                strSQL &= ":MSG_CONTENT,"
                'LOAI 213
                strSQL &= "0,"
                strSQL &= "'" & PSTRMSG_ID304 & "',"
                strSQL &= "TO_DATE('" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "TO_DATE('" & pStrNgayLapCT.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "nvl((select max(ma_CN) from tcs_dm_ntdt_hq where trangthai='" & mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "'  AND MA_DV='" & pStrMaDV & "' AND TAIKHOAN_TH='" & pStrTaiKhoanTH & "' ),'____')"
                strSQL &= ", TO_DATE('" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & "','RRRR-MM-DD HH24:MI:SS')"
                strSQL &= ")"

                ' _conn.Open()
                _cmd.Connection = _conn
                _cmd.CommandText = strSQL
                _cmd.CommandType = CommandType.Text
                _cmd.Parameters.Add("MSG_CONTENT", OracleType.Clob).Value = pStrMSG
                _cmd.ExecuteNonQuery()
                If Not _conn Is Nothing And _conn.State = ConnectionState.Closed Then
                    _cmd.Dispose()
                    _conn.Dispose()
                    _conn.Close()
                End If
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("insert msg 304 idmsg[" & PSTRMSG_ID304 & "] mst[" & pStrMaDV & "] soctuHQ[" & pStrSoCT & "] ngaylapctu[" & pStrNgayLapCT & "] ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "insert msg 304 idmsg[" & PSTRMSG_ID304 & "] mst[" & pStrMaDV & "] soctuHQ[" & pStrSoCT & "] ngaylapctu[" & pStrNgayLapCT & "] ")
            Finally
                _cmd.Dispose()
                _conn.Dispose()
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function InsertMSG304(ByVal pStrSoCT As String, ByVal pStrKyHieuCT As String, ByVal pStrMaNT As String, ByVal pStrSoTienTO As String, _
                                         ByVal pStrMaDV As String, ByVal pStrTenDV As String, ByVal pStrNgayLapCT As String, ByVal pStrNgayTruyenCT As String, _
                                         ByVal pStrTyGia As String, ByVal pStrTaiKhoanTH As String, ByVal pStrTenTaiKhoanTH As String, ByVal pStrMSG As String, ByVal PSTRMSG_ID304 As String, ByVal pStrNgayCT As String, ByVal pStrDienGiai As String) As Decimal
            Dim strSQL As String = "SELECT TO_CHAR(SYSDATE,'RRRRMMDD')||LPAD(SEQ_TCS_HQ247_304.NEXTVAL,10,'0') FROM DUAL"
            Dim ds As DataSet = Nothing
            Dim StrID As String = ""
            Dim _cmd As OracleCommand = New OracleCommand()
            Dim _conn As OracleConnection = DataAccess.GetConnection() ' New OracleConnection(VBOracleLib.DataAccess.strConnection)
            Try
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                StrID = ds.Tables(0).Rows(0)(0).ToString()
                strSQL = "INSERT INTO TCS_HQ247_304 (ID, HQ_SO_CTU, HQ_KYHIEU_CTU,SO_HS, MA_NT,HQ_SOTIEN_TO, MA_DV, TEN_DV, TRANGTHAI, HQ_NGAYLAP_CT,"
                strSQL &= " HQ_NGAYTRUYEN_CT, TY_GIA, TAIKHOAN_TH, TEN_TAIKHOAN_TH,"
                strSQL &= " MSG_CONTENT, LOAI213,MSG_ID304,NGAY_HT,HQ_NGAY_CT,ma_CN,NGAYNHANMSG,DIEN_GIAI) VALUES("
                strSQL &= "'" & StrID & "',"
                strSQL &= "'" & pStrSoCT & "',"
                strSQL &= "'" & pStrKyHieuCT & "',"
                strSQL &= "nvl((select max(so_hs) from tcs_dm_ntdt_hq where trangthai='" & mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "'  AND MA_DV='" & pStrMaDV & "' AND TAIKHOAN_TH='" & pStrTaiKhoanTH & "' ),'____'),"
                strSQL &= "'" & pStrMaNT & "',"
                strSQL &= "" & pStrSoTienTO & ","
                strSQL &= "'" & pStrMaDV & "',"
                strSQL &= "'" & pStrTenDV & "',"
                strSQL &= "'" & mdlCommon.TCS_HQ247_TRANGTHAI_304_MOINHAN & "',"
                strSQL &= "TO_DATE('" & pStrNgayLapCT.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "TO_DATE('" & pStrNgayTruyenCT.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                If pStrTyGia.Length > 0 Then
                    strSQL &= "" & pStrTyGia & ","
                Else
                    strSQL &= "0,"
                End If
                strSQL &= "'" & pStrTaiKhoanTH & "',"
                strSQL &= "'" & pStrTenTaiKhoanTH & "',"
                'NOI DUNG 304 NHAN VE
                strSQL &= ":MSG_CONTENT,"
                'LOAI 213
                strSQL &= "0,"
                strSQL &= "'" & PSTRMSG_ID304 & "',"
                strSQL &= "TO_DATE('" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "TO_DATE('" & pStrNgayLapCT.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "nvl((select max(ma_CN) from tcs_dm_ntdt_hq where trangthai='" & mdlCommon.TCS_HQ247_HS_TRANGTHAI_ACTIVE & "'  AND MA_DV='" & pStrMaDV & "' AND TAIKHOAN_TH='" & pStrTaiKhoanTH & "' ),'____')"
                strSQL &= ", TO_DATE('" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & "','RRRR-MM-DD HH24:MI:SS')"
                strSQL &= ",'" & pStrDienGiai & "'"
                strSQL &= ")"

                ' _conn.Open()
                _cmd.Connection = _conn
                _cmd.CommandText = strSQL
                _cmd.CommandType = CommandType.Text
                _cmd.Parameters.Add("MSG_CONTENT", OracleType.Clob).Value = pStrMSG
                _cmd.ExecuteNonQuery()
                If Not _conn Is Nothing And _conn.State = ConnectionState.Closed Then
                    _cmd.Dispose()
                    _conn.Dispose()
                    _conn.Close()
                End If
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("insert msg 304 idmsg[" & PSTRMSG_ID304 & "] mst[" & pStrMaDV & "] soctuHQ[" & pStrSoCT & "] ngaylapctu[" & pStrNgayLapCT & "] ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "insert msg 304 idmsg[" & PSTRMSG_ID304 & "] mst[" & pStrMaDV & "] soctuHQ[" & pStrSoCT & "] ngaylapctu[" & pStrNgayLapCT & "] ")
            Finally
                _cmd.Dispose()
                _conn.Dispose()
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function checkTrangthaiMSG304(ByVal pStrID As String, ByVal pStrTrangthai As String) As Decimal
            Try
                Dim strSQL As String = "SELECT * FROM TCS_HQ247_304 WHERE ID='" & pStrID & "' AND TRANGTHAI='" & pStrTrangthai & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
            Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_304_MAKER213(ByVal pStrID304 As String, ByVal pStrLyDo As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213 & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", LOAI213=1 "
                StrSQL &= ", LYDO='" & pStrLyDo & "' "
                StrSQL &= ", ID_MK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_MAKER213  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_MAKER213  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_304_CHUYENTRA(ByVal pStrID304 As String, ByVal pStrLyDo As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_CHUYENTRA & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", LYDOCHUYENTRA='" & pStrLyDo & "' "
                StrSQL &= ", ID_MK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_CHUYENTRA  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_CHUYENTRA  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function


        Public Shared Function TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(ByVal pStrID304 As String, ByVal pStrLyDo As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", LOAI213=2 "
                StrSQL &= ", LYDO='" & pStrLyDo & "' "
                StrSQL &= ", ID_MK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function

        Public Shared Function TCS_HQ247_TRANGTHAI_304_CHEKER213(ByVal pStrID304 As String, ByVal pStrMSI213ID As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_CHEKER213 & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", MSG_ID213='" & pStrMSI213ID & "'  "
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_CHEKER213  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_CHEKER213  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_304_CHEKER213_TUCHOI(ByVal pStrID304 As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_CHEKER213_TUCHOI & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_CHEKER213_TUCHOI  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_CHEKER213_TUCHOI  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function


        Public Shared Sub TCS_HQ247_TRANGTHAI_304_MAKER301_ERROR(ByVal pStrID304 As String, Optional ByVal pStrTenDN As String = "NTDTHQ")
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER301_ERROR & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)

            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_MAKER301_ERROR  pStrID304[" & pStrID304 & "]   ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_MAKER301_ERROR  pStrID304[" & pStrID304 & "]   ")
            End Try

        End Sub

        Public Shared Function TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI(ByVal pStrID304 As String, ByVal pStrMSG213ID As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", MSG_ID213='" & pStrMSG213ID & "'  "
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI  pStrID304[" & pStrID304 & "]   ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI  pStrID304[" & pStrID304 & "]   ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Sub TCS_HQ247_TRANGTHAI_304_SEND301_ERROR(ByVal pStrID304 As String, Optional ByVal pStrTenDN As String = "NTDTHQ")
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)

            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_SEND301_ERROR  pStrID304[" & pStrID304 & "]   ")
                log.Error(ex.Message & "-" & ex.StackTrace) 'VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_SEND301_ERROR  pStrID304[" & pStrID304 & "]   ")
            End Try

        End Sub

        Public Shared Function TCS_HQ247_TRANGTHAI_304_SEND301_OK(ByVal pStrID304 As String, ByVal pStrMSG213ID As String, ByVal pStrSo_TN_CT As String, ByVal pStrNgay_TN_CT As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_OK & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", MSG_ID301='" & pStrMSG213ID & "'  "
                StrSQL &= ", id_send_301='" & pStrTenDN & "' "
                StrSQL &= ", SO_TN_CT='" & pStrSo_TN_CT & "' "
                StrSQL &= ", Ngay_TN_CT='" & pStrNgay_TN_CT & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                'CAP NHAT TRANG THAI CHUNG TU
                StrSQL = "UPDATE TCS_CTU_NTDT_HQ247_HDR SET TRANG_THAI='01' WHERE SO_CT IN (SELECT SO_CTU FROM TCS_HQ247_304 where ID='" & pStrID304 & "'  )"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)

                'INSERT VAO BANG DOI CHIEU
                Try
                    'Dim hdr As New infDoiChieuNHHQ_HDR
                    'Dim dtls() As infDoiChieuNHHQ_DTL
                    'SetObjectToDoiChieuNHHQ(pStrID304, hdr, dtls, pStrTenDN)
                    'InsertDoiChieuNHHQ(hdr, dtls, "1")
                    '===================insert đối chiếu mới ==================
                    Dim hdr As New MSG304DC_HDR
                    SetObjectToDoiChieuNHHQ_NTDT(pStrID304, hdr, pStrTenDN)
                    InsertDoiChieuNHHQ_NTDT(hdr, "1")
                Catch ex As Exception
                    log.Error("TCS_HQ247_TRANGTHAI_304_SEND301_OK  INSERT VAO BANG DOI CHIEU pStrID304[" & pStrID304 & "]   ")
                    log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_SEND301_OK  INSERT VAO BANG DOI CHIEU pStrID304[" & pStrID304 & "]   ")
                    Return mdlCommon.TCS_HQ247_HQ_LOIINSERT_DOICHIEU_NH_HQ
                End Try

                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_SEND301_OK  pStrID304[" & pStrID304 & "]   ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_SEND301_OK  pStrID304[" & pStrID304 & "]   ")
                Return mdlCommon.TCS_HQ247_HQ_CAPNHAT_TRANGTHAI304_SAUKHISEN_301
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function InsertDoiChieuNHHQ_NTDT_TEST(ByVal pStrID304 As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal

            'INSERT VAO BANG DOI CHIEU
            Try
                'Dim hdr As New infDoiChieuNHHQ_HDR
                'Dim dtls() As infDoiChieuNHHQ_DTL
                'SetObjectToDoiChieuNHHQ(pStrID304, hdr, dtls, pStrTenDN)
                'InsertDoiChieuNHHQ(hdr, dtls, "1")
                '===================insert đối chiếu mới ==================
                Dim hdr As New MSG304DC_HDR
                SetObjectToDoiChieuNHHQ_NTDT(pStrID304, hdr, pStrTenDN)
                InsertDoiChieuNHHQ_NTDT(hdr, "1")
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_SEND301_OK  INSERT VAO BANG DOI CHIEU pStrID304[" & pStrID304 & "]   ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_SEND301_OK  INSERT VAO BANG DOI CHIEU pStrID304[" & pStrID304 & "]   ")
                Return mdlCommon.TCS_HQ247_HQ_LOIINSERT_DOICHIEU_NH_HQ
            End Try

            Return mdlCommon.TCS_HQ247_OK

        End Function
        Public Shared Function getMSG304(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "  SELECT ROWNUM STT,X.* FROM (   SELECT A.ID, A.HQ_SO_CTU, A.HQ_KYHIEU_CTU, A.MA_NT,  A.TAIKHOAN_TH, "
                strSQL &= " A.HQ_SOTIEN_TO, A.MA_DV, A.TEN_DV, A.TRANGTHAI,TO_CHAR( A.HQ_NGAYLAP_CT,'DD/MM/RRRR')  HQ_NGAYLAP_CT ,"
                strSQL &= "  B.MOTA TRANG_THAI, C.TENLOAI LOAI_THUE,"
                strSQL &= "  (CASE WHEN A.LOAIMSG='304' THEN"
                strSQL &= "       NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11.2%' AND X.NODEID IN ('11.2.2')),'#')"
                strSQL &= "  ELSE"
                strSQL &= "       NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11.2%' AND X.NODEID IN ('11.2.3')),'#')"
                strSQL &= "  END) NAVIGATEURL"
                strSQL &= " ,A.SO_CTU, TO_CHAR( A.HQ_NGAY_CT,'DD/MM/RRRR')  HQ_NGAY_CT, A.SO_CT_NH,TO_CHAR(A.NGAYNHANMSG,'DD/MM/RRRR') NGAYNHANMSG  "
                strSQL &= " FROM TCS_HQ247_304 A, TCS_DM_TRANGTHAI_TT_HQ247 B,TCS_HQ247_LOAITHUE C"
                strSQL &= "  WHERE A.TRANGTHAI = B.TRANGTHAI AND A.LOAIMSG = C.MALOAI  " & pvStrWhereClause
                strSQL &= " ORDER BY A.ID) X ORDER BY X.ID "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getMSG304_TracuuXuly(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "  SELECT ROWNUM STT,X.* FROM (   SELECT A.ID, A.HQ_SO_CTU, A.HQ_KYHIEU_CTU, A.MA_NT,  A.TAIKHOAN_TH, "
                strSQL &= " A.HQ_SOTIEN_TO, A.MA_DV, A.TEN_DV, A.TRANGTHAI,TO_CHAR( A.HQ_NGAYLAP_CT,'DD/MM/RRRR')  HQ_NGAYLAP_CT ,"
                strSQL &= "  B.MOTA TRANG_THAI, C.TENLOAI LOAI_THUE,"
                strSQL &= "  (CASE WHEN A.LOAIMSG='304' THEN"
                strSQL &= "       NVL((SELECT MAX(X.NAVIGATEURL)||'?ID='||A.ID FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11.3%' AND X.NODEID IN ('11.3.7')),'#')"
                strSQL &= "  ELSE"
                strSQL &= "       NVL((SELECT MAX(X.NAVIGATEURL)||'?ID='||A.ID FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11.3%' AND X.NODEID IN ('11.2.7')),'#')"
                strSQL &= "  END) NAVIGATEURL"
                strSQL &= " ,A.SO_CTU, TO_CHAR( A.HQ_NGAY_CT,'DD/MM/RRRR')  HQ_NGAY_CT, A.SO_CT_NH,TO_CHAR(A.NGAYNHANMSG,'DD/MM/RRRR') NGAYNHANMSG  "
                strSQL &= " FROM TCS_HQ247_304 A, TCS_DM_TRANGTHAI_TT_HQ247 B,TCS_HQ247_LOAITHUE C"
                strSQL &= "  WHERE A.TRANGTHAI = B.TRANGTHAI AND A.LOAIMSG = C.MALOAI  " & pvStrWhereClause
                strSQL &= " ORDER BY A.ID) X ORDER BY X.ID "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getMSG304_Tracuu(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "  SELECT ROWNUM STT,X.* FROM (   SELECT A.ID, A.HQ_SO_CTU, A.HQ_KYHIEU_CTU, A.MA_NT,  A.TAIKHOAN_TH, "
                strSQL &= " A.HQ_SOTIEN_TO, A.MA_DV, A.TEN_DV, A.TRANGTHAI,TO_CHAR( A.HQ_NGAYLAP_CT,'DD/MM/RRRR')  HQ_NGAYLAP_CT ,"
                strSQL &= "  B.MOTA TRANG_THAI, C.TENLOAI LOAI_THUE,"
                strSQL &= "  (CASE WHEN A.LOAIMSG='304' THEN"
                strSQL &= "       NVL((SELECT MAX(X.NAVIGATEURL)||'?ID='||A.ID FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11.3%' AND X.NODEID IN ('11.3.6')),'#')"
                strSQL &= "  ELSE"
                strSQL &= "       NVL((SELECT MAX(X.NAVIGATEURL)||'?ID='||A.ID FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '11.3%' AND X.NODEID IN ('11.2.7')),'#')"
                strSQL &= "  END) NAVIGATEURL"
                strSQL &= " ,A.SO_CTU, TO_CHAR( A.HQ_NGAY_CT,'DD/MM/RRRR')  HQ_NGAY_CT, A.SO_CT_NH,TO_CHAR(A.NGAYNHANMSG,'DD/MM/RRRR') NGAYNHANMSG  "
                strSQL &= " FROM TCS_HQ247_304 A, TCS_DM_TRANGTHAI_TT_HQ247 B,TCS_HQ247_LOAITHUE C"
                strSQL &= "  WHERE A.TRANGTHAI = B.TRANGTHAI AND A.LOAIMSG = C.MALOAI  " & pvStrWhereClause
                strSQL &= " ORDER BY A.ID) X ORDER BY X.ID "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getMSG304Detail(ByVal pvStrID As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "  SELECT A.*,B.MOTA TRANG_THAI "
                strSQL &= " FROM TCS_HQ247_304 A, TCS_DM_TRANGTHAI_TT_HQ247 B,TCS_HQ247_LOAITHUE C"
                strSQL &= "  WHERE A.TRANGTHAI = B.TRANGTHAI AND A.LOAIMSG = C.MALOAI AND A.ID='" & pvStrID & "'"

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function UpdateMSG304_IDMSG213(ByVal pStrID As String, ByVal pStrIDMSG213 As String) As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET MSG_ID213='" & pStrIDMSG213 & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= " WHERE ID='" & pStrID & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function UpdateMSG304_TrangThaiChoDuyet(ByVal pStrID As String, ByVal pStrIDMSG213 As String) As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET MSG_ID213='" & pStrIDMSG213 & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= " WHERE ID='" & pStrID & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function Insert301(ByVal HDR As NewChungTu.infChungTuHDR, ByVal ListDTL As List(Of NewChungTu.infChungTuDTL), ByVal pStrID As String, ByVal pStrtenDN As String) As Decimal
            Try
                If HDR Is Nothing Then
                    Return mdlCommon.TCS_HQ247_HQ_HACHTOAN_LOI_CTU
                End If
                Dim blnVND As Boolean = True
                If (HDR.Ma_NT <> "VND") Then blnVND = False
                For Each dtl As NewChungTu.infChungTuDTL In ListDTL
                    If (dtl.SoTien = 0) Then
                        log.Error("Insert301  pStrID304[" & pStrID & "] pStrTenDN[" & pStrtenDN & "] loi chi tiet chung tu  ") 'VBOracleLib.LogDebug.WriteLog(New Exception("Lỗi valid chứng từ DTL"), "Insert301  pStrID304[" & pStrID & "] pStrTenDN[" & pStrtenDN & "] loi chi tiet chung tu  ")
                        Return mdlCommon.TCS_HQ247_HQ_HACHTOAN_LOI_CTU
                    End If
                Next
                HDR.Trang_Thai = "05"
                If String.IsNullOrEmpty(HDR.Ngay_QD) Then
                    HDR.Ngay_QD = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                If String.IsNullOrEmpty(HDR.Ngay_HT) Then
                    HDR.Ngay_HT = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                If String.IsNullOrEmpty(HDR.Ngay_TK) Then
                    HDR.Ngay_TK = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                If String.IsNullOrEmpty(HDR.NGAY_KH_NH) Then
                    HDR.NGAY_KH_NH = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                If String.IsNullOrEmpty(HDR.Ngay_BK) Then
                    HDR.Ngay_BK = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                For Each dtl In ListDTL
                    If String.IsNullOrEmpty(dtl.NGAY_TK) Then
                        dtl.NGAY_TK = DateTime.Now.ToString("dd/MM/yyyy")
                    End If
                Next
                If Insert_Chungtu(HDR, ListDTL) Then
                    Dim strSQLUpdate As String = "UPDATE TCS_HQ247_304 SET SO_CTU='" & HDR.So_CT & "' WHERE ID='" & pStrID & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQLUpdate, CommandType.Text)
                    Return mdlCommon.TCS_HQ247_OK
                End If
            Catch ex As Exception
                log.Error("Insert301  pStrID304[" & pStrID & "] pStrTenDN[" & pStrtenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "Insert301  pStrID304[" & pStrID & "] pStrTenDN[" & pStrtenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_HQ_HACHTOAN_LOI_CTU
        End Function
        Public Shared Function Insert_Chungtu(ByVal HDR As NewChungTu.infChungTuHDR, ByVal listDTL As List(Of NewChungTu.infChungTuDTL)) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If HDR IsNot Nothing Then
                    'Try
                    '    HDR.KyHieu_CT = Business_HQ.HaiQuan.HaiQuanController.GetKyHieuChungTu(HDR.SHKB, HDR.Ma_NV, "01")
                    'Catch ex As Exception
                    '    bRet = False
                    '    Throw New Exception("Có lỗi trong quá trình sinh ký hiệu chứng từ")
                    '    Return bRet
                    'End Try

                    Try
                        'nên chuyển vào trong câu query

                        Dim sSo_BT As String = String.Empty

                        sSo_BT = GetSoBT(HDR.Ngay_KB, HDR.Ma_NV)
                        If Not String.IsNullOrEmpty(sSo_BT) Then HDR.So_BT = CInt(sSo_BT + 1)

                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh số chứng từ")
                        Return bRet
                    End Try

                    'Using conn1 As IDbConnection = VBOracleLib.DataAccess.GetConnection()
                    '    Using tran1 As IDbTransaction = conn.BeginTransaction()

                    '    End Using
                    'End Using
                    conn = VBOracleLib.DataAccess.GetConnection()
                    '  conn.Open()
                    tran = conn.BeginTransaction()

                    sSQL = "Insert into TCS_CTU_NTDT_HQ247_HDR(SHKB,Ngay_KB,Ma_NV," & _
                              "So_BT,Ma_DThu,So_BThu,KyHieu_CT,So_CT_NH,Ma_NNTien,Ten_NNTien,DC_NNTien," & _
                              "Ma_NNThue,Ten_NNThue,DC_NNThue,Ly_Do,Ma_KS,Ma_TQ,So_QD,Ngay_QD,CQ_QD,Ngay_CT," & _
                              "Ma_CQThu,XA_ID,Ma_Tinh,Ma_Huyen,Ma_Xa,TK_No,TK_Co,So_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
                              "Ma_NT,Ty_Gia,TG_ID, Ma_LThue,So_Khung,So_May,TK_KH_NH,MA_NH_A,MA_NH_B,Ten_NH_B,Ten_NH_TT,TTien,TT_TThu," & _
                              "Lan_In,Trang_Thai, TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT,So_BK, CTU_GNT_BL,So_CT," & _
                              "TK_GL_NH,TEN_KH_NH,TK_KB_NH, ISACTIVE,PHI_GD,PHI_VAT,PT_TINHPHI,SEQ_NO,REF_NO,RM_REF_NO,QUAN_HUYENNNTIEN," & _
                              "TINH_TPNNTIEN,MA_HQ,LOAI_TT,TEN_HQ,MA_HQ_PH,TEN_HQ_PH, MA_NH_TT,MA_SANPHAM,TT_CITAD,MA_NTK, KENH_CT, " & _
                              "MA_CN,partition_id,transaction_id,save_type, tran_type, " & _
                              "value_date,so_lo,note,address,remarks,TEN_CQTHU,NAME_TEN_CQTHU,SO_TIEN_CHU,TEN_LTHUE, " & _
                              "TEN_LHXNK,TEN_KB,MA_CUC,TEN_CUC, LOAI_NNT, PO_Date, TT_THU, " & _
                              "Ma_Huyen_NNThue, Ma_Tinh_NNThue, MA_QUAN_HUYENNNTIEN, MA_TINH_TPNNTIEN, Ngay_HT, Ngay_TK," & _
                              "NGAY_KH_NH, Ngay_BK, LOAI_CTU,SO_CMND,SO_FONE,NGAY_BAONO,NGAY_BAOCO)" & _
                              "Values(:SHKB, :Ngay_KB, :Ma_NV, " & _
                              ":So_BT, :Ma_DThu, :So_BThu, :KyHieu_CT, :So_CT_NH, :Ma_NNTien, :Ten_NNTien, :DC_NNTien, " & _
                              ":Ma_NNThue, :Ten_NNThue, :DC_NNThue, :Ly_Do, :Ma_KS, :Ma_TQ, :So_QD, TO_DATE (:Ngay_QD, 'dd/MM/yyyy HH24:MI:SS'), :CQ_QD, :Ngay_CT, " & _
                              ":Ma_CQThu, :XA_ID, :Ma_Tinh, :Ma_Huyen, :Ma_Xa, :TK_No, :TK_Co, :So_TK,  :LH_XNK, :DVSDNS, :Ten_DVSDNS, " & _
                              ":Ma_NT, :Ty_Gia, :TG_ID, :Ma_LThue, :So_Khung, :So_May, :TK_KH_NH,  :MA_NH_A, :MA_NH_B, :Ten_NH_B, :Ten_NH_TT, :TTien, :TT_TThu, " & _
                              ":Lan_In, :Trang_Thai, :TK_KH_Nhan, :Ten_KH_Nhan, :Diachi_KH_Nhan, :TTien_NT, :PT_TT, :So_BK,  :CTU_GNT_BL, :So_CT, " & _
                              ":TK_GL_NH, :TEN_KH_NH, :TK_KB_NH, :ISACTIVE, :PHI_GD, :PHI_VAT, :PT_TINHPHI, :SEQ_NO, :REF_NO, :RM_REF_NO, :QUAN_HUYENNNTIEN, " & _
                              ":TINH_TPNNTIEN, :MA_HQ, :LOAI_TT, :TEN_HQ, :MA_HQ_PH, :TEN_HQ_PH, :MA_NH_TT, :MA_SANPHAM, :TT_CITAD, :MA_NTK, :KENH_CT, " & _
                              ":MA_CN, :partition_id, :transaction_id, :save_type, :tran_type, " & _
                              ":value_date, :so_lo, :note, :address, :remarks, :TEN_CQTHU, :NAME_TEN_CQTHU, :SO_TIEN_CHU, :TEN_LTHUE, " & _
                              ":TEN_LHXNK, :TEN_KB, :MA_CUC, :TEN_CUC, :LOAI_NNT, TO_DATE (:PO_Date, 'dd/MM/yyyy HH24:MI:SS'), :TT_THU, " & _
                              ":Ma_Huyen_NNThue, :Ma_Tinh_NNThue, :MA_QUAN_HUYENNNTIEN, :MA_TINH_TPNNTIEN, TO_DATE(:Ngay_HT, 'dd/MM/yyyy HH24:MI:SS'), TO_DATE(:Ngay_TK, 'dd/MM/yyyy HH24:MI:SS')," & _
                              "TO_DATE (:NGAY_KH_NH, 'dd/MM/yyyy HH24:MI:SS'), TO_DATE (:Ngay_BK, 'dd/MM/yyyy HH24:MI:SS'), :LOAI_CTU, :SO_CMND, :SO_FONE,TO_DATE (:NGAY_BAONO, 'dd/MM/yyyy'),TO_DATE (:NGAY_BAOCO, 'dd/MM/yyyy'))"

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(HDR.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_KB", ParameterDirection.Input, CStr(HDR.Ngay_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, CStr(HDR.Ma_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BT", ParameterDirection.Input, CStr(HDR.So_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_DThu", ParameterDirection.Input, CStr(HDR.Ma_DThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BThu", ParameterDirection.Input, CStr(HDR.So_BThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KyHieu_CT", ParameterDirection.Input, CStr(HDR.KyHieu_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_CT_NH", ParameterDirection.Input, CStr(HDR.So_CT_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NNTien", ParameterDirection.Input, CStr(HDR.Ma_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NNTien", ParameterDirection.Input, CStr(HDR.Ten_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTien", ParameterDirection.Input, CStr(HDR.DC_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NNThue", ParameterDirection.Input, CStr(HDR.Ma_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NNThue", ParameterDirection.Input, CStr(HDR.Ten_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNThue", ParameterDirection.Input, CStr(HDR.DC_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ly_Do", ParameterDirection.Input, CStr(HDR.Ly_Do), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_KS", ParameterDirection.Input, CStr(HDR.Ma_KS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_TQ", ParameterDirection.Input, CStr(HDR.Ma_TQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_QD", ParameterDirection.Input, CStr(HDR.So_QD), DbType.String))
                    'If String.IsNullOrEmpty(HDR.Ngay_QD) Then HDR.Ngay_QD = DateTime.Now
                    'Dim today As Date = DateTime.ParseExact(HDR.Ngay_QD, "dd/MM/yyyy HH:mm tt", CultureInfo.CurrentCulture)
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_QD", ParameterDirection.Input, HDR.Ngay_QD, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":CQ_QD", ParameterDirection.Input, CStr(HDR.CQ_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_CT", ParameterDirection.Input, CStr(HDR.Ngay_CT), DbType.String))
                    'Dim dNgay_HT As DateTime = DateTime.ParseExact(HDR.Ngay_HT, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_HT", ParameterDirection.Input, HDR.Ngay_HT, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_CQThu", ParameterDirection.Input, CStr(HDR.Ma_CQThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":XA_ID", ParameterDirection.Input, CStr(HDR.XA_ID), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Tinh", ParameterDirection.Input, CStr(HDR.Ma_Tinh), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Huyen", ParameterDirection.Input, CStr(HDR.Ma_Huyen), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Xa", ParameterDirection.Input, CStr(HDR.Ma_Xa), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_No", ParameterDirection.Input, CStr(HDR.TK_No), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_Co", ParameterDirection.Input, CStr(HDR.TK_Co), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_TK", ParameterDirection.Input, CStr(HDR.So_TK), DbType.String))
                    'If String.IsNullOrEmpty(HDR.Ngay_TK) Then HDR.Ngay_TK = DateTime.Now
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_TK", ParameterDirection.Input, HDR.Ngay_TK, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":LH_XNK", ParameterDirection.Input, CStr(HDR.LH_XNK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DVSDNS", ParameterDirection.Input, CStr(HDR.DVSDNS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_DVSDNS", ParameterDirection.Input, CStr(HDR.Ten_DVSDNS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NT", ParameterDirection.Input, CStr(HDR.Ma_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ty_Gia", ParameterDirection.Input, CStr(HDR.Ty_Gia), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TG_ID", ParameterDirection.Input, CStr(HDR.TG_ID), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_LThue", ParameterDirection.Input, CStr(HDR.Ma_LThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_Khung", ParameterDirection.Input, CStr(HDR.So_Khung), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_May", ParameterDirection.Input, CStr(HDR.So_May), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(HDR.TK_KH_NH), DbType.String))
                    'If String.IsNullOrEmpty(HDR.NGAY_KH_NH) Then HDR.NGAY_KH_NH = DateTime.Now
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, HDR.NGAY_KH_NH, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, CStr(HDR.MA_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_B", ParameterDirection.Input, CStr(HDR.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NH_B", ParameterDirection.Input, CStr(HDR.Ten_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NH_TT", ParameterDirection.Input, CStr(HDR.TEN_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTien", ParameterDirection.Input, CStr(HDR.TTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_TThu", ParameterDirection.Input, CStr(HDR.TT_TThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Lan_In", ParameterDirection.Input, CStr(HDR.Lan_In), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Trang_Thai", ParameterDirection.Input, CStr(HDR.Trang_Thai), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_Nhan", ParameterDirection.Input, CStr(HDR.TK_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_KH_Nhan", ParameterDirection.Input, CStr(HDR.Ten_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Diachi_KH_Nhan", ParameterDirection.Input, CStr(HDR.Diachi_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTien_NT", ParameterDirection.Input, CStr(HDR.TTien_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(HDR.PT_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BK", ParameterDirection.Input, CStr(HDR.So_BK), DbType.String))
                    'If String.IsNullOrEmpty(HDR.Ngay_BK) Then HDR.Ngay_BK = DateTime.Now
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_BK", ParameterDirection.Input, HDR.Ngay_BK, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":CTU_GNT_BL", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_CT", ParameterDirection.Input, CStr(HDR.So_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_GL_NH", ParameterDirection.Input, CStr(HDR.TK_GL_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(HDR.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KB_NH", ParameterDirection.Input, CStr(HDR.TK_KB_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ISACTIVE", ParameterDirection.Input, 1, DbType.Int32))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD", ParameterDirection.Input, CStr(HDR.PHI_GD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT", ParameterDirection.Input, CStr(HDR.PHI_VAT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TINHPHI", ParameterDirection.Input, CStr(HDR.PT_TINHPHI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SEQ_NO", ParameterDirection.Input, CStr(HDR.SEQ_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":REF_NO", ParameterDirection.Input, CStr(HDR.REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":RM_REF_NO", ParameterDirection.Input, CStr(HDR.RM_REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(HDR.QUAN_HUYEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TINH_TPNNTIEN", ParameterDirection.Input, CStr(HDR.TINH_TPHO_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(HDR.MA_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_TT", ParameterDirection.Input, CStr(HDR.LOAI_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, CStr(HDR.TEN_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, CStr(HDR.Ma_hq_ph), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, CStr(HDR.TEN_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(HDR.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_SANPHAM", ParameterDirection.Input, CStr(HDR.SAN_PHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_CITAD", ParameterDirection.Input, CStr(HDR.TT_CITAD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(HDR.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KENH_CT", ParameterDirection.Input, CStr(HDR.Kenh_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(HDR.Ma_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":partition_id", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":transaction_id", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":save_type", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":record_type", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":tran_type", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":gl_glsub", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":method", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":dup_allowflg", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":mod_allowflg", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":ac", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":file_import", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":debit_method", ParameterDirection.Input, Nothing, DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":value_date", ParameterDirection.Input, CInt(DateTime.Now.ToString("yyyyMMdd")), DbType.Int32))
                    listParam.Add(DataAccess.NewDBParameter(":so_lo", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":note", ParameterDirection.Input, CStr(HDR.Ghi_chu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":address", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":remarks", ParameterDirection.Input, CStr(HDR.Ghi_chu), DbType.String))

                    HDR.Ghi_chu = Globals.RemoveSign4VietnameseString(HDR.Ghi_chu)
                    Dim strRemarks As String = HDR.Ghi_chu
                    If strRemarks.Length > 210 Then
                        strRemarks = strRemarks.Substring(0, 210)
                    End If

                    listParam.Add(DataAccess.NewDBParameter(":remarks", ParameterDirection.Input, CStr(strRemarks), DbType.String))


                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(HDR.Ten_cqthu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NAME_TEN_CQTHU", ParameterDirection.Input, CStr(HDR.Ten_cqthu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_TIEN_CHU", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LTHUE", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHXNK", ParameterDirection.Input, CStr(HDR.Ten_lhxnk), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(HDR.Ten_kb), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CUC", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CUC", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":BENE_SHORT_NAME", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_NNT", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":IMPORT_FILE", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PO_Date", ParameterDirection.Input, DateTime.Now.ToString("dd/MM/yyyy"), DbType.Date))
                    'listParam.Add(DataAccess.NewDBParameter(":HILOWVALUE", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_THU", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Huyen_NNThue", ParameterDirection.Input, CStr(HDR.Ma_Huyen_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Tinh_NNThue", ParameterDirection.Input, CStr(HDR.Ma_Tinh_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(HDR.MA_QUAN_HUYENNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_TPNNTIEN", ParameterDirection.Input, CStr(HDR.MA_TINH_TPNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_CTU", ParameterDirection.Input, CStr(HDR.LOAI_CTU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":SO_CMND", ParameterDirection.Input, CStr(HDR.SO_CMND), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_FONE", ParameterDirection.Input, CStr(HDR.SO_FONE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_BAONO", ParameterDirection.Input, CStr(HDR.NGAY_BAONO.ToString("dd/MM/yyyy")), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_BAOCO", ParameterDirection.Input, CStr(HDR.NGAY_BAOCO.ToString("dd/MM/yyyy")), DbType.String))



                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....


                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If listDTL IsNot Nothing AndAlso listDTL.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each DTL In listDTL
                                DTL.ID = Business_HQ.HaiQuan.HaiQuanController.GenSequenceKeyTable("TCS_CTU_NTDT_HQ247_DTL_SEQ.NEXTVAL")
                                DTL.SO_CT = HDR.So_CT
                                DTL.SHKB = HDR.SHKB
                                DTL.Ngay_KB = HDR.Ngay_KB
                                DTL.Ma_NV = HDR.Ma_NV
                                DTL.So_BT = HDR.So_BT
                                DTL.Ma_DThu = HDR.Ma_DThu

                                iRet = 0
                                sSQL = "Insert into TCS_CTU_NTDT_HQ247_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                                        "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                                        "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,SO_CT,ma_dp, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ,SAC_THUE,TT_BTOAN) " & _
                                        "Values (:ID, :SHKB, :Ngay_KB, :Ma_NV, :So_BT, :Ma_DThu, :CCH_ID, :Ma_Cap, " & _
                                        ":Ma_Chuong, :LKH_ID, :Ma_Loai, :Ma_Khoan, :MTM_ID, :Ma_Muc, :Ma_TMuc, :Noi_Dung," & _
                                        ":DT_ID, :Ma_TLDT, :Ky_Thue, :SoTien, :SoTien_NT, :maquy, :SO_CT, :ma_dp, :SO_TK, TO_DATE (:NGAY_TK, 'dd/MM/yyyy HH24:MI:SS'), :MA_LHXNK, :MA_LT, :MA_HQ, :SAC_THUE, :TT_BTOAN) "

                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":ID", ParameterDirection.Input, DTL.ID, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(DTL.SHKB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ngay_KB", ParameterDirection.Input, CStr(DTL.Ngay_KB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, CStr(DTL.Ma_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":So_BT", ParameterDirection.Input, CStr(DTL.So_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_DThu", ParameterDirection.Input, CStr(DTL.Ma_DThu), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":CCH_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Cap", ParameterDirection.Input, CStr(DTL.Ma_Cap), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Chuong", ParameterDirection.Input, CStr(DTL.Ma_Chuong), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":LKH_ID", ParameterDirection.Input, CStr(DTL.LKH_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Loai", ParameterDirection.Input, CStr(DTL.Ma_Loai), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Khoan", ParameterDirection.Input, CStr(DTL.Ma_Khoan), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MTM_ID", ParameterDirection.Input, CStr(DTL.MTM_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Muc", ParameterDirection.Input, CStr(DTL.Ma_Muc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TMuc", ParameterDirection.Input, CStr(DTL.Ma_TMuc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Noi_Dung", ParameterDirection.Input, CStr(DTL.Noi_Dung), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":DT_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TLDT", ParameterDirection.Input, CStr(DTL.Ma_TLDT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ky_Thue", ParameterDirection.Input, CStr(DTL.Ky_Thue), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien", ParameterDirection.Input, CStr(DTL.SoTien), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien_NT", ParameterDirection.Input, CStr(DTL.SoTien_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":maquy", ParameterDirection.Input, CStr(DTL.MaQuy), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(DTL.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":ma_dp", ParameterDirection.Input, CStr(DTL.Ma_DP), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(DTL.SO_TK), DbType.String))

                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, DTL.NGAY_TK, DbType.Date))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LHXNK", ParameterDirection.Input, CStr(DTL.LH_XNK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LT", ParameterDirection.Input, CStr(DTL.MA_LT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(DTL.MA_HQ), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(DTL.SAC_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":TT_BTOAN", ParameterDirection.Input, CStr(DTL.TT_BTOAN), DbType.String))

                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function

        Public Shared Function TCS_HQ247_TRANGTHAI_304_CHO_CORE_RESPONSE(ByVal pStrID304 As String) As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_CHO_CORE_RESPONSE & "'"
                StrSQL &= ",NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_CHO_CORE_RESPONSE  pStrID304[" & pStrID304 & "]")
                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_CHO_CORE_RESPONSE  pStrID304[" & pStrID304 & "]")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function

        Public Shared Function UpdateTTSP(ByVal pStrSoCT As String, ByVal strMA_NH_TT As String, ByVal strTEN_NH_TT As String, ByVal strMA_NH_B As String, ByVal strTEN_NH_B As String, ByVal strSo_ct_nh As String, ByVal strRemarks As String)
            Try
                'ma_nh_b,ten_nh_b,ma_nh_tt,ten_nh_tt
                Dim strSQL As String = "UPDATE TCS_CTU_NTDT_HQ247_HDR SET ma_nh_tt='" & strMA_NH_TT & "',ten_nh_tt='" & strTEN_NH_TT & "',ma_nh_b='" & strMA_NH_B & "',ten_nh_b='" & strTEN_NH_B & "', so_ct_nh='" & strSo_ct_nh & "',remarks='" & strRemarks & "', ngay_kh_nh=sysdate     WHERE SO_CT='" & pStrSoCT & "';"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Function Duyet301(ByVal pStrSoCT As String, ByVal pStrID As String, ByVal pStrMSGID301 As String)
            Try
                Dim strSQL As String = "UPDATE TCS_CTU_NTDT_HQ247_HDR SET TRANG_THAI='01' WHERE SO_CT='" & pStrSoCT & "';"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)


            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function getNHTrucTiepGianTiep(ByVal pStrSHKB As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "select * from TCS_DM_NH_GIANTIEP_TRUCTIEP where SHKB='" & pStrSHKB & "'"
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 
            End Try
            Return ds
        End Function
        Public Shared Function getNHTrucTiepGianTiepKB_SHB(ByVal pStrSHKB As String, ByVal pStrMa_GT As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "select * from TCS_DM_NH_GIANTIEP_TRUCTIEP where SHKB='" & pStrSHKB & "' and ma_giantiep='" & pStrMa_GT & "'"
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 
            End Try
            Return ds
        End Function
        Public Shared Function getTenHQ_CQT(ByVal pStrMaHQ_CQT As String) As String
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "select TEN from tcs_dm_cqthu where MA_CQTHU='" & pStrMaHQ_CQT & "'"
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds.Tables(0).Rows(0)(0).ToString()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 
            End Try
            Return ""
        End Function
        Public Shared Function getTenHQ_CQT(ByVal pStrMaHQ_CQT As String, ByVal pStrMaHQ_PH As String) As String
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "select TEN from tcs_dm_cqthu where MA_CQTHU='" & pStrMaHQ_CQT & "' and MA_HQ = '" & pStrMaHQ_PH & "'"
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds.Tables(0).Rows(0)(0).ToString()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 
            End Try
            Return ""
        End Function
        Public Shared Function getTenHQ_PH(ByVal pStrMaHQ_CQT As String) As String
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "select TEN from tcs_dm_cqthu where MA_HQ='" & pStrMaHQ_CQT & "'"
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds.Tables(0).Rows(0)(0).ToString()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 
            End Try
            Return ""
        End Function
        Public Shared Function GetSoBT(ByVal iNgayLV As Integer, ByVal iMaNV As Integer) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select MAX(So_BT) AS So_BT From TCS_CTU_NTDT_HQ247_HDR Where ngay_kb = :ngay_kb AND Ma_NV = :Ma_NV"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("ngay_kb", ParameterDirection.Input, iNgayLV, DbType.Int32))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, iMaNV, DbType.Int32))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Sub SetObjectToDoiChieuNHHQ(ByVal pStrID As String, ByRef hdr As infDoiChieuNHHQ_HDR, ByRef dtls() As infDoiChieuNHHQ_DTL, Optional ByVal pStrTenDN As String = "NTDTHQ")

            Dim strSender_Code As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
            Dim strSender_Name As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString()

            Dim hdrBL As New DataTable
            Dim dtlsBL As New DataTable
            Dim objDTL As infDoiChieuNHHQ_DTL
            Dim strSO_CT As String = ""
            Dim strHQSO_CTU As String = ""
            Dim strHQNGAY_CT As String = ""
            Dim strSO_TN_CT As String = ""
            Dim strNGAY_TN_CT As String = ""
            Dim strTransaction_id As String
            Dim k As Integer = 0
            Try
                'Tao key chung tu
                Dim strSQL As String = "SELECT ID, HQ_SO_CTU, HQ_KYHIEU_CTU, SO_CTU, SO_HS, MA_NT,"
                strSQL &= "  HQ_SOTIEN_TO, MA_DV, TEN_DV, TRANGTHAI,TO_CHAR( HQ_NGAYLAP_CT,'DD/MM/RRRR') HQ_NGAYLAP_CT,"
                strSQL &= "  TO_CHAR( HQ_NGAYTRUYEN_CT,'DD/MM/RRRR') HQ_NGAYTRUYEN_CT, TY_GIA, TAIKHOAN_TH, TEN_TAIKHOAN_TH,"
                strSQL &= "  TO_CHAR( HQ_NGAY_CT,'DD/MM/RRRR') HQ_NGAY_CT,SO_TN_CT ,NGAY_TN_CT,MSG_ID301 "
                strSQL &= "  FROM TCS_HQ247_304 A WHERE ID='" & pStrID & "'"
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            strSO_CT = ds.Tables(0).Rows(0)("SO_CTU").ToString()
                            strHQSO_CTU = ds.Tables(0).Rows(0)("HQ_SO_CTU").ToString()
                            strHQNGAY_CT = ds.Tables(0).Rows(0)("HQ_NGAY_CT").ToString()
                            strSO_TN_CT = ds.Tables(0).Rows(0)("SO_TN_CT").ToString()
                            strNGAY_TN_CT = ds.Tables(0).Rows(0)("NGAY_TN_CT").ToString()
                            strTransaction_id = ds.Tables(0).Rows(0)("MSG_ID301").ToString()
                        End If
                    End If
                End If
                'Lay thong tin ChungTu

                hdrBL = SelectCT_HDRHQ247(strSO_CT).Tables(0)
                dtlsBL = SelectCT_DTLHQ247(strSO_CT).Tables(0)



                If hdrBL.Rows.Count > 0 Then
                    hdr.shkb = hdrBL.Rows(0)("shkb").ToString()
                    hdr.ngay_kb = hdrBL.Rows(0)("ngay_kb").ToString()
                    hdr.so_bt = hdrBL.Rows(0)("so_bt").ToString()
                    hdr.kyhieu_ct = hdrBL.Rows(0)("kyhieu_ct").ToString()
                    hdr.so_ct = hdrBL.Rows(0)("so_ct").ToString()
                    hdr.ngay_ct = hdrBL.Rows(0)("ngay_ct").ToString()
                    hdr.ma_nnthue = hdrBL.Rows(0)("ma_nnthue").ToString()
                    hdr.ten_nnthue = hdrBL.Rows(0)("ten_nnthue").ToString()
                    hdr.ma_cqthu = hdrBL.Rows(0)("ma_cqthu").ToString()
                    hdr.ma_hq_cqt = hdrBL.Rows(0)("ma_cqthu").ToString()
                    hdr.so_tk = hdrBL.Rows(0)("so_tk").ToString()

                    hdr.ttien = hdrBL.Rows(0)("ttien").ToString()
                    hdr.ttien_nt = hdrBL.Rows(0)("ttien_nt").ToString()
                    hdr.trang_thai = hdrBL.Rows(0)("trang_thai").ToString()
                    hdr.ly_do_huy = hdrBL.Rows(0)("ly_do_huy").ToString()
                    hdr.ma_nt = hdrBL.Rows(0)("ma_nt").ToString()
                    hdr.ten_nh_ph = strSender_Name
                    If hdrBL.Columns.Contains("ma_dv_dd") Then
                        hdr.ma_dv_dd = hdrBL.Rows(0)("ma_dv_dd").ToString()
                    End If
                    If hdrBL.Columns.Contains("ty_gia") Then
                        hdr.ty_gia = hdrBL.Rows(0)("ty_gia").ToString()
                    End If
                    If hdrBL.Columns.Contains("ttien_nt") Then
                        hdr.ttien_nt = hdrBL.Rows(0)("ttien_nt").ToString()
                    End If
                    If hdrBL.Columns.Contains("ten_dv_dd") Then
                        hdr.ten_dv_dd = hdrBL.Rows(0)("ten_dv_dd").ToString()
                    End If
                    If hdrBL.Columns.Contains("ngay_hdon") Then
                        hdr.ngay_hd = "to_date('" & hdrBL.Rows(0)("ngay_hdon").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_hd = "''"
                    End If
                    If hdrBL.Columns.Contains("hoa_don") Then
                        hdr.so_hd = hdrBL.Rows(0)("hoa_don").ToString()
                    End If
                    If hdrBL.Rows(0)("ngay_tk").ToString() <> "" Then
                        hdr.ngay_tk = "to_date('" & hdrBL.Rows(0)("ngay_tk").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_tk = "''"
                    End If
                    '21/09/2012-manhnv
                    If hdrBL.Columns.Contains("lh_xnk") Then
                        hdr.lhxnk = hdrBL.Rows(0)("lh_xnk").ToString()
                    End If
                    '***
                    '27/09/2012-manhnv
                    If hdrBL.Columns.Contains("ma_nv") Then
                        hdr.ma_nv = hdrBL.Rows(0)("ma_nv").ToString()
                    End If
                    '***
                    If hdrBL.Columns.Contains("lhxnk") Then
                        hdr.lhxnk = hdrBL.Rows(0)("lhxnk").ToString()
                    End If
                    If hdrBL.Columns.Contains("vt_lhxnk") Then
                        hdr.vt_lhxnk = hdrBL.Rows(0)("vt_lhxnk").ToString()
                    End If
                    If hdrBL.Columns.Contains("ten_lhxnk") Then
                        hdr.ten_lhxnk = hdrBL.Rows(0)("ten_lhxnk").ToString()
                    End If
                    If hdrBL.Columns.Contains("tk_ns") Then
                        hdr.tk_ns = hdrBL.Rows(0)("tk_ns").ToString()
                    End If
                    If hdrBL.Columns.Contains("ten_tk_ns") Then
                        hdr.ten_tk_ns = hdrBL.Rows(0)("ten_tk_ns").ToString()
                    End If
                    If hdrBL.Columns.Contains("so_bl") Then
                        hdr.so_bl = hdrBL.Rows(0)("so_bl").ToString()
                    End If
                    If hdrBL.Columns.Contains("ngay_batdau") Then
                        If hdrBL.Rows(0)("ngay_batdau").ToString() <> "" Then
                            hdr.ngay_bl_batdau = "to_date('" & hdrBL.Rows(0)("ngay_batdau").ToString() & "','dd/mm/yyyy')"
                        Else
                            hdr.ngay_bl_batdau = "''"
                        End If
                    Else
                        hdr.ngay_bl_batdau = "''"
                    End If
                    If hdrBL.Columns.Contains("ngay_ketthuc") Then
                        If hdrBL.Rows(0)("ngay_ketthuc").ToString() <> "" Then
                            hdr.ngay_bl_ketthuc = "to_date('" & hdrBL.Rows(0)("ngay_ketthuc").ToString() & "','dd/mm/yyyy')"
                        Else
                            hdr.ngay_bl_ketthuc = "''"
                        End If
                    Else
                        hdr.ngay_bl_ketthuc = "''"
                    End If
                    If hdrBL.Columns.Contains("songay_bl") Then
                        hdr.songay_bl = hdrBL.Rows(0)("songay_bl").ToString()
                    End If
                    If hdrBL.Columns.Contains("kieu_bl") Then
                        hdr.kieu_bl = hdrBL.Rows(0)("kieu_bl").ToString()
                    End If
                    If hdrBL.Columns.Contains("dien_giai") Then
                        hdr.dien_giai = hdrBL.Rows(0)("dien_giai").ToString()
                    End If
                    If hdrBL.Columns.Contains("ma_hq_ph") Then
                        hdr.ma_hq_ph = hdrBL.Rows(0)("ma_hq_ph").ToString()
                    End If

                    '21/09/2012-manhnv
                    If hdrBL.Columns.Contains("ten_hq_ph") Then
                        hdr.ten_hq_ph = hdrBL.Rows(0)("ten_hq_ph").ToString()
                    End If
                    If hdrBL.Columns.Contains("ten_hq") Then
                        hdr.ten_hq = hdrBL.Rows(0)("ten_hq").ToString()
                    End If
                    If hdrBL.Columns.Contains("ma_hq") Then
                        hdr.ma_hq = hdrBL.Rows(0)("ma_hq").ToString()
                    End If
                    '******
                    If hdrBL.Columns.Contains("ma_loaitien") Then
                        hdr.ma_loaitien = hdrBL.Rows(0)("ma_loaitien").ToString()
                    End If
                    If hdrBL.Columns.Contains("ma_hq_cqt") Then
                        hdr.ma_hq_cqt = hdrBL.Rows(0)("ma_hq_cqt").ToString()
                    End If
                    If hdrBL.Columns.Contains("so_bt_hq") Then
                        If hdrBL.Rows(0)("so_bt_hq").ToString().Trim() <> "" Then
                            hdr.so_bt_hq = hdrBL.Rows(0)("so_bt_hq").ToString()
                        End If
                    End If
                    If hdrBL.Columns.Contains("ma_ntk") Then
                        If hdrBL.Rows(0)("ma_ntk").ToString().Trim() <> "" Then
                            hdr.ma_ntk = hdrBL.Rows(0)("ma_ntk").ToString()
                        End If
                    End If
                    'Cac truong khac
                    hdr.error_code_hq = "0"
                    hdr.transaction_type = mdlCommon.TransactionHQ_Type.M47.ToString()
                    If hdr.error_code_hq = "0" Then
                        hdr.accept_yn = "Y"
                    Else
                        hdr.accept_yn = "N"
                    End If
                    hdr.ten_kb = Globals.RemoveSign4VietnameseString(mdlCommon.Get_TenKhoBac(hdr.shkb))
                    hdr.ten_cqthu = mdlCommon.Get_TenCQThu(hdr.ma_cqthu)
                    hdr.ngay_bc = "to_date('" & hdrBL.Rows(0)("NGAY_BAONO").ToString() & "','dd/mm/yyyy')"
                    hdr.ngay_bn = "to_date('" & hdrBL.Rows(0)("NGAY_BAOCO").ToString() & "','dd/mm/yyyy')"
                    hdr.parent_transaction_id = strTransaction_id
                    hdr.tk_ns_hq = ""
                    hdr.so_tn_ct = strSO_TN_CT
                    hdr.ngay_tn_ct = strNGAY_TN_CT
                    '22/09/2012-manhnv----------------------------------------------------------
                    If hdrBL.Columns.Contains("ma_nh_a") Then
                        hdr.ma_nh_ph = hdrBL.Rows(0)("ma_nh_a").ToString()
                    End If
                    If hdrBL.Columns.Contains("ma_nh_b") Then
                        hdr.ma_nh_th = hdrBL.Rows(0)("ma_nh_b").ToString()
                    End If
                    If hdrBL.Columns.Contains("tk_co") Then
                        hdr.tkkb_ct = hdrBL.Rows(0)("tk_co").ToString()
                    End If
                    '----------------------------------------------------------------------------
                    '13/09/14
                    If hdrBL.Columns.Contains("ngay_hdon") Then
                        hdr.ngay_hd = "to_date('" & hdrBL.Rows(0)("ngay_hdon").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_hd = "''"
                    End If
                    If hdrBL.Columns.Contains("hoa_don") Then
                        hdr.so_hd = hdrBL.Rows(0)("hoa_don").ToString()
                    End If

                    If hdrBL.Columns.Contains("ngay_vtd") Then
                        hdr.ngay_vtd = "to_date('" & hdrBL.Rows(0)("ngay_vtd").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_vtd = "''"
                    End If
                    If hdrBL.Columns.Contains("vt_don") Then
                        hdr.vtd = hdrBL.Rows(0)("vt_don").ToString()
                    End If
                    If hdrBL.Columns.Contains("ngay_vtd2") Then
                        hdr.ngay_vtd2 = "to_date('" & hdrBL.Rows(0)("ngay_vtd2").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_vtd2 = "''"
                    End If
                    If hdrBL.Columns.Contains("vt_don2") Then
                        hdr.vtd2 = hdrBL.Rows(0)("vt_don2").ToString()
                    End If
                    If hdrBL.Columns.Contains("ngay_vtd3") Then
                        hdr.ngay_vtd3 = "to_date('" & hdrBL.Rows(0)("ngay_vtd3").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_vtd3 = "''"
                    End If
                    If hdrBL.Columns.Contains("vt_don3") Then
                        hdr.vtd3 = hdrBL.Rows(0)("vt_don3").ToString()
                    End If
                    If hdrBL.Columns.Contains("ngay_vtd4") Then
                        hdr.ngay_vtd4 = "to_date('" & hdrBL.Rows(0)("ngay_vtd4").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_vtd4 = "''"
                    End If
                    If hdrBL.Columns.Contains("vt_don4") Then
                        hdr.vtd4 = hdrBL.Rows(0)("vt_don4").ToString()
                    End If
                    If hdrBL.Columns.Contains("ngay_vtd5") Then
                        hdr.ngay_vtd5 = "to_date('" & hdrBL.Rows(0)("ngay_vtd5").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_vtd5 = "''"
                    End If
                    If hdrBL.Columns.Contains("vt_don5") Then
                        hdr.vtd5 = hdrBL.Rows(0)("vt_don5").ToString()
                    End If

                End If

                If dtlsBL.Rows.Count > 0 Then
                    For Each row As DataRow In dtlsBL.Rows
                        Try
                            objDTL = New infDoiChieuNHHQ_DTL
                            objDTL.ma_cap = row("ma_cap").ToString()
                            objDTL.ma_chuong = row("ma_chuong").ToString()
                            objDTL.noi_dung = row("noi_dung").ToString()
                            objDTL.ma_dp = row("ma_dp").ToString()
                            objDTL.ky_thue = row("ky_thue").ToString()
                            objDTL.sotien = row("sotien").ToString()

                            If dtlsBL.Columns.Contains("ma_quy") Then
                                objDTL.ma_quy = row("ma_quy").ToString()
                            End If
                            If dtlsBL.Columns.Contains("ma_nkt") Then
                                objDTL.ma_nkt = row("ma_nkt").ToString()
                            End If
                            If dtlsBL.Columns.Contains("ma_tmuc") Then
                                objDTL.ma_ndkt = row("ma_tmuc").ToString()
                            End If
                            If dtlsBL.Columns.Contains("ma_nkt_cha") Then
                                objDTL.ma_nkt_cha = row("ma_nkt_cha").ToString()
                            End If
                            If dtlsBL.Columns.Contains("ma_ndkt") Then
                                objDTL.ma_ndkt = row("ma_ndkt").ToString()
                            End If
                            If dtlsBL.Columns.Contains("ma_khoan") Then
                                objDTL.ma_nkt = row("ma_khoan").ToString()
                            End If
                            If dtlsBL.Columns.Contains("ma_ndkt_cha") Then
                                objDTL.ma_ndkt_cha = row("ma_ndkt_cha").ToString()
                            End If
                            If dtlsBL.Columns.Contains("ma_tlpc") Then
                                If row("ma_tlpc").ToString().Trim() <> "" Then
                                    objDTL.ma_tlpc = row("ma_tlpc").ToString()
                                End If
                            End If
                            If dtlsBL.Columns.Contains("ma_khtk") Then
                                objDTL.ma_khtk = row("ma_khtk").ToString()
                            End If
                            If dtlsBL.Columns.Contains("SoTien_NT") Then
                                objDTL.sotien_nt = row("SoTien_NT").ToString()
                            End If

                            ReDim Preserve dtls(k)
                            dtls(k) = objDTL
                            k = k + 1
                        Catch ex As Exception
                            log.Error("daCTUHQ304.SetObjectToDoiChieuNHHQ  pStrID304[" & pStrID & "] pStrTenDN[" & pStrTenDN & "]  ")
                            log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daCTUHQ304.SetObjectToDoiChieuNHHQ  pStrID304[" & pStrID & "] pStrTenDN[" & pStrTenDN & "]  ")
                            Throw ex

                        End Try
                    Next
                End If
            Catch ex As Exception
                log.Error("daCTUHQ304.SetObjectToDoiChieuNHHQ  pStrID304[" & pStrID & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daCTUHQ304.SetObjectToDoiChieuNHHQ  pStrID304[" & pStrID & "] pStrTenDN[" & pStrTenDN & "]  ")
                Throw ex

            End Try
        End Sub
        Public Shared Sub SetObjectToDoiChieuNHHQ_NTDT(ByVal pStrID As String, ByRef hdr As MSG304DC_HDR, Optional ByVal pStrTenDN As String = "NTDTHQ")

            Dim strSender_Code As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
            Dim strSender_Name As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString()

            Dim hdrBL As New DataTable
            Dim dtlsBL As New DataTable
            Dim objDTL As infDoiChieuNHHQ_DTL
            Dim strSO_CT As String = ""
            Dim strHQSO_CTU As String = ""
            Dim strHQNGAY_CT As String = ""
            Dim strSO_TN_CT As String = ""
            Dim strNGAY_TN_CT As String = ""
            Dim strTransaction_id As String
            Dim msg3041 As MSG3041 = New MSG3041()
            Dim strNGAY_BN As String = ""
            Dim k As Integer = 0
            Try
                'Tao key chung tu
                Dim strSQL As String = "SELECT ID, HQ_SO_CTU, HQ_KYHIEU_CTU, SO_CTU, SO_HS, MA_NT,"
                strSQL &= "  HQ_SOTIEN_TO, MA_DV, TEN_DV, TRANGTHAI,TO_CHAR( HQ_NGAYLAP_CT,'DD/MM/RRRR') HQ_NGAYLAP_CT,"
                strSQL &= "  TO_CHAR( HQ_NGAYTRUYEN_CT,'DD/MM/RRRR') HQ_NGAYTRUYEN_CT, TY_GIA, TAIKHOAN_TH, TEN_TAIKHOAN_TH,"
                strSQL &= "  TO_CHAR( HQ_NGAY_CT,'DD/MM/RRRR') HQ_NGAY_CT,SO_TN_CT ,NGAY_TN_CT,MSG_ID301, TO_CHAR( NGAY_TT,'RRRR-MM-DD') NGAY_TT,MSG_CONTENT "
                strSQL &= "  FROM TCS_HQ247_304 A WHERE ID='" & pStrID & "'"
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            strSO_CT = ds.Tables(0).Rows(0)("SO_CTU").ToString()
                            strHQSO_CTU = ds.Tables(0).Rows(0)("HQ_SO_CTU").ToString()
                            strHQNGAY_CT = ds.Tables(0).Rows(0)("HQ_NGAY_CT").ToString()
                            strSO_TN_CT = ds.Tables(0).Rows(0)("SO_TN_CT").ToString()
                            strNGAY_TN_CT = ds.Tables(0).Rows(0)("NGAY_TN_CT").ToString()
                            strTransaction_id = ds.Tables(0).Rows(0)("MSG_ID301").ToString()
                            msg3041.SETDATA(ds.Tables(0).Rows(0)("MSG_CONTENT").ToString())
                            strNGAY_BN = ds.Tables(0).Rows(0)("NGAY_TT").ToString()
                        End If
                    End If
                End If
                'Lay thong tin ChungTu
                hdr = msg3041.ObjectMSG304DC(pStrID)
                hdr.NGAY_BN = strNGAY_BN
                hdr.ETAX_SO_CT = strSO_CT
                hdr.NGAY_TT = strNGAY_BN
            Catch ex As Exception
                log.Error("daCTUHQ304.SetObjectToDoiChieuNHHQ  pStrID304[" & pStrID & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '   VBOracleLib.LogDebug.WriteLog(ex, "daCTUHQ304.SetObjectToDoiChieuNHHQ  pStrID304[" & pStrID & "] pStrTenDN[" & pStrTenDN & "]  ")
                Throw ex

            End Try
        End Sub
        Public Shared Sub InsertDoiChieuNHHQ(ByVal hdr As infDoiChieuNHHQ_HDR, ByVal dtls() As infDoiChieuNHHQ_DTL, Optional ByVal strTT_CT As String = "", Optional ByVal DescErr As String = "")
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim objDTL As infDoiChieuNHHQ_DTL
            Dim strSql As String
            Dim strID As String
            Try
                strID = Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_NTDT_HDR_ID.NextVal")
                conn = New DataAccess
                transCT = conn.BeginTransaction


                strSql = " Insert into TCS_DCHIEU_NHHQ_NTDT_HDR( " & _
                             " id, transaction_id, transaction_type, shkb, ten_kb, " & _
                             " ngay_kb, so_bt, kyhieu_ct, so_ct, ngay_ct, ngay_bn, " & _
                             " ngay_bc, ma_nnthue, ten_nnthue, ma_cqthu, ten_cqthu, " & _
                             " so_tk, ngay_tk, lhxnk, vt_lhxnk, ten_lhxnk, " & _
                             " ttien, ttien_nt, trang_thai, tk_ns, ten_tk_ns, ma_ntk, " & _
                             " so_bl, ngay_bl_batdau, ngay_bl_ketthuc, songay_bl, " & _
                             " kieu_bl, diengiai, accept_yn, parent_transaction_id, " & _
                             " ma_hq_ph, ma_loaitien, tk_ns_hq, ma_nh_ph, ten_nh_ph, " & _
                             " ma_hq_cqt, error_code_hq, so_bt_hq, ma_hq, ten_hq, ten_hq_ph,ly_do_huy," & _
                             " vt_don, ngay_vtd,vt_don2, ngay_vtd2, vt_don3, ngay_vtd3,vt_don4, ngay_vtd4,vt_don5, ngay_vtd5," & _
                             " ma_nh_th, tkkb, tkkb_ct, ma_nv, ma_nt,ty_gia,so_tn_ct,ngay_tn_ct,ten_nh_th,ma_dv_dd,ten_dv_dd, tg_id, so_hd, ngay_hd)" & _
                         " Values( " & _
                             strID & " , '" & _
                             hdr.transaction_id & "','" & _
                             hdr.transaction_type & "','" & _
                             hdr.shkb & "','" & _
                             hdr.ten_kb & "'," & _
                             hdr.ngay_kb & "," & _
                             hdr.so_bt & ",'" & _
                             hdr.kyhieu_ct & "','" & _
                             hdr.so_ct & "'," & _
                             hdr.ngay_ct & "," & _
                             hdr.ngay_bn & "," & _
                             hdr.ngay_bc & ",'" & _
                             hdr.ma_nnthue & "','" & _
                             hdr.ten_nnthue & "','" & _
                             hdr.ma_cqthu & "','" & _
                             hdr.ten_cqthu & "','" & _
                             hdr.so_tk & "'," & _
                             hdr.ngay_tk & ",'" & _
                             hdr.lhxnk & "','" & _
                             hdr.vt_lhxnk & "','" & _
                             hdr.ten_lhxnk & "'," & _
                             hdr.ttien & "," & _
                             hdr.ttien_nt & ",'" & _
                             hdr.trang_thai & "','" & _
                             hdr.tk_ns & "','" & _
                             hdr.ten_tk_ns & "','" & _
                             hdr.ma_ntk & "','" & _
                             hdr.so_bl & "', " & _
                             hdr.ngay_bl_batdau & ", " & _
                             hdr.ngay_bl_ketthuc & "," & _
                             hdr.songay_bl & ",'" & _
                             hdr.kieu_bl & "','" & _
                             hdr.dien_giai & "','" & _
                             hdr.accept_yn & "','" & _
                             hdr.parent_transaction_id & "','" & _
                             hdr.ma_hq_ph & "','" & _
                             hdr.ma_loaitien & "','" & _
                             hdr.tk_ns_hq & "','" & _
                             hdr.ma_nh_ph & "','" & _
                             hdr.ten_nh_ph & "','" & _
                             hdr.ma_hq_cqt & "','" & _
                             hdr.error_code_hq & "'," & _
                             hdr.so_bt_hq & ",'" & _
                             hdr.ma_hq & "','" & _
                             hdr.ten_hq & "','" & _
                             hdr.ten_hq_ph & "','" & _
                             hdr.ly_do_huy & "','" & _
                             hdr.vtd & "'," & _
                             hdr.ngay_vtd & ",'" & _
                             hdr.vtd2 & "'," & _
                             hdr.ngay_vtd2 & ",'" & _
                             hdr.vtd3 & "'," & _
                             hdr.ngay_vtd3 & ",'" & _
                             hdr.vtd4 & "'," & _
                             hdr.ngay_vtd4 & ",'" & _
                             hdr.vtd5 & "'," & _
                             hdr.ngay_vtd5 & ",'" & _
                             hdr.ma_nh_th & "','" & _
                             hdr.tkkb & "','" & _
                             hdr.tkkb_ct & "','" & _
                             hdr.ma_nv & "','" & _
                             hdr.ma_nt & "','" & _
                             hdr.ty_gia & "','" & _
                             hdr.so_tn_ct & "','" & _
                             hdr.ngay_tn_ct & "','" & _
                             hdr.ten_nh_th & "','" & _
                             hdr.ma_dv_dd & "','" & _
                             hdr.ten_dv_dd & "','" & _
                             hdr.tg_id & "','" & _
                             hdr.so_hd & "'," & _
                             hdr.ngay_hd & ")"

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)


                ' Insert vào bảng DOICHIEU_NHHQ_DTL
                If Not dtls Is Nothing Then
                    For Each objDTL In dtls
                        strSql = " Insert into TCS_DCHIEU_NHHQ_NTDT_DTL(" & _
                                    " hdr_id, ma_quy, ma_cap, ma_chuong, ma_nkt, " & _
                                    " ma_nkt_cha, ma_ndkt, ma_ndkt_cha, ma_tlpc, " & _
                                    " ma_khtk, noi_dung, ma_dp, ky_thue, sotien,sotien_nt ) " & _
                                 " Values (" & strID & ", '" & _
                                    objDTL.ma_quy & "','" & _
                                    objDTL.ma_cap & "','" & _
                                    objDTL.ma_chuong & "','" & _
                                    objDTL.ma_nkt & "','" & _
                                    objDTL.ma_nkt_cha & "','" & _
                                    objDTL.ma_ndkt & "','" & _
                                    objDTL.ma_ndkt_cha & "'," & _
                                    objDTL.ma_tlpc & ",'" & _
                                    objDTL.ma_khtk & "','" & _
                                    objDTL.noi_dung & "','" & _
                                    objDTL.ma_dp & "','" & _
                                    objDTL.ky_thue & "'," & _
                                    objDTL.sotien & "," & _
                                    objDTL.sotien_nt & ")"
                        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                    Next
                End If
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error("daCTUHQ304.InsertDoiChieuNHHQ  So_CT[" & hdr.so_ct & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daCTUHQ304.InsertDoiChieuNHHQ  So_CT[" & hdr.so_ct & "]  ")

                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
        Public Shared Sub InsertDoiChieuNHHQ_NTDT(ByVal hdr As MSG304DC_HDR, Optional ByVal strTT_CT As String = "", Optional ByVal DescErr As String = "")
            Dim conn As DataAccess
            Dim transCT As IDbTransaction
            Dim strSql As String
            Dim strSqldtl As String
            Dim strID As String
            Try
                strID = Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_NTDT_HDR_ID.NextVal")
                conn = New DataAccess
                transCT = conn.BeginTransaction


                strSql = " INSERT INTO TCS_HQ247_304_DCHQ_HDR(DIACHI"
                strSql &= ",DIENGIAI,ETAX_SO_CT,ID,KYHIEU_CT,LOAI_CT,MA_CHUONG,MA_CN,MA_DV,MA_HQ_CQT"
                strSql &= ",MA_HQ_PH,MA_KB,MA_NH_TH,MA_NT,MA_NTK,MA_ST,NGAYLAP_CT,NGAYTRUYEN_CT,NGAY_BN"
                strSql &= ",NGAY_CT,NGAY_TT,SOTIEN_TO,SO_CMT,SO_CT,TAIKHOAN_TH,TEN_DV,TEN_KB,TEN_NH_TH,TEN_NNT"
                strSql &= ",TEN_TAIKHOAN_TH,TKKB,TT_KHAC,TY_GIA)VALUES( "
                strSql &= "'" & hdr.DIACHI & "'"
                strSql &= ",'" & hdr.DIENGIAI & "'"
                strSql &= ",'" & hdr.ETAX_SO_CT & "'"
                strSql &= ",'" & hdr.ID & "'"
                strSql &= ",'" & hdr.KYHIEU_CT & "'"
                strSql &= ",'" & hdr.LOAI_CT & "'"
                strSql &= ",'" & hdr.MA_CHUONG & "'"
                strSql &= ",'" & hdr.MA_CN & "'"
                strSql &= ",'" & hdr.MA_DV & "'"
                strSql &= ",'" & hdr.MA_HQ_CQT & "'"
                strSql &= ",'" & hdr.MA_HQ_PH & "'"
                strSql &= ",'" & hdr.MA_KB & "'"
                strSql &= ",'" & hdr.MA_NH_TH & "'"
                strSql &= ",'" & hdr.MA_NT & "'"
                strSql &= ",'" & hdr.MA_NTK & "'"
                strSql &= ",'" & hdr.MA_ST & "'"
                strSql &= ",'" & hdr.NGAYLAP_CT & "'"
                strSql &= ",'" & hdr.NGAYTRUYEN_CT & "'"
                strSql &= ",'" & hdr.NGAY_BN & "'"
                strSql &= ",'" & hdr.NGAY_CT & "'"
                strSql &= ",'" & hdr.NGAY_TT & "'"
                strSql &= ",'" & hdr.SOTIEN_TO & "'"
                strSql &= ",'" & hdr.SO_CMT & "'"
                strSql &= ",'" & hdr.SO_CT & "'"
                strSql &= ",'" & hdr.TAIKHOAN_TH & "'"
                strSql &= ",'" & hdr.TEN_DV & "'"
                strSql &= ",'" & hdr.TEN_KB & "'"
                strSql &= ",'" & hdr.TEN_NH_TH & "'"
                strSql &= ",'" & hdr.TEN_NNT & "'"
                strSql &= ",'" & hdr.TEN_TAIKHOAN_TH & "'"
                strSql &= ",'" & hdr.TKKB & "'"
                strSql &= ",'" & hdr.TT_KHAC & "'"
                strSql &= ",'" & hdr.TY_GIA & "'"
                strSql &= ")"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                For Each objdtl As MSG304DC_DTL In hdr.MSG304DC_DTL
                    strSqldtl = " INSERT INTO TCS_HQ247_304_DCHQ_DTL(ID" & _
                                ",ID_HS,MA_HQ,MA_LH,MA_LT,MA_ST,NAM_DK,NDKT,SOTIEN_NT,SOTIEN_VND,SO_TK,TTBUTTOAN)" & _
                                "VALUES( '" & _
                                objdtl.ID & "','" & _
                                objdtl.ID_HS & "','" & _
                                objdtl.MA_HQ & "','" & _
                                objdtl.MA_LH & "','" & _
                                objdtl.MA_LT & "','" & _
                                objdtl.MA_ST & "','" & _
                                objdtl.NAM_DK & "','" & _
                                objdtl.NDKT & "','" & _
                                objdtl.SOTIEN_NT & "','" & _
                                objdtl.SOTIEN_VND & "','" & _
                                objdtl.SO_TK & "','" & _
                                objdtl.TTBUTTOAN & "')"
                    conn.ExecuteNonQuery(strSqldtl, CommandType.Text, transCT)
                Next
                ' Insert vào bảng DOICHIEU_NHHQ_DTL
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                log.Error("daCTUHQ304.InsertDoiChieuNHHQ  So_CT[" & hdr.SO_CT & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daCTUHQ304.InsertDoiChieuNHHQ  So_CT[" & hdr.SO_CT & "]  ")
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub

        Public Shared Function SelectCT_HDRHQ247(ByVal pStrSo_CT As String) As DataSet
            ' ****************************************************************************
            ' Mục đích: lấy ra một chứng từ trong bảng TCS_CTU_HDR với các điều kiện đầu vào
            ' Người viết: XXX
            ' Ngày viết: 23/09/2017
            '*****************************************************************************
            Dim dsCT As New DataSet
            Dim strSql As String

            Try

                strSql = " Select " & _
                           " shkb, ngay_kb, ma_nv, so_bt, ma_dthu, so_bthu, " & _
                           " kyhieu_ct, so_ct, so_ct_nh, ma_nntien, ten_nntien, " & _
                           " dc_nntien, ma_nnthue, ten_nnthue, dc_nnthue, ly_do_huy, " & _
                           " ma_ks, ma_tq, so_qd, ngay_qd, cq_qd, ngay_ct, " & _
                           " ngay_ht, ma_cqthu, xa_id, ma_tinh, ma_huyen, ma_xa, " & _
                           " tk_no, tk_co, so_tk, to_char(ngay_tk,'dd/MM/yyyy') ngay_tk, lh_xnk, dvsdns, " & _
                           " ten_dvsdns, ma_nt, ty_gia, tg_id, ma_lthue, so_khung, " & _
                           " so_may, tk_kh_nh, ngay_kh_nh, ma_nh_a, ma_nh_b, " & _
                           " ttien, tt_tthu, lan_in, trang_thai, tk_kh_nhan, " & _
                           " ten_kh_nhan, diachi_kh_nhan, ttien_nt, so_bt_ktkb, " & _
                           " pt_tt, tt_in, tt_in_bk, so_bk, ngay_bk, ctu_gnt_bl, " & _
                           " ref_no, tt_thu, ten_kh_nh, tk_gl_nh, tk_kb_nh, " & _
                           " isactive, seq_no, phi_gd, phi_vat, pt_tinhphi, " & _
                           " tt_cthue, rm_ref_no, ngay_ks, quan_huyennntien, " & _
                           " tinh_tpnntien, tkht, loai_tt,loai_tt ma_loaitien, ma_ntk, ma_hq, ten_hq, ma_hq_ph, ten_hq_ph,to_char( NGAY_BAONO,'DD/MM/RRRR') NGAY_BAONO,to_char( NGAY_BAOCO,'DD/MM/RRRR') NGAY_BAOCO,DIENGIAI_HQ,ten_nh_b,ten_cqthu " & _
                         " From TCS_CTU_NTDT_HQ247_HDR " & _
                         " Where so_ct = '" & pStrSo_CT & "' "
                dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return dsCT
            Catch ex As Exception
                log.Error("daCTUHQ304.SelectCT_HDRHQ247  So_CT[" & pStrSo_CT & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "daCTUHQ304.SelectCT_HDRHQ247  So_CT[" & pStrSo_CT & "]  ")

                Throw ex
            Finally

            End Try
        End Function
        Public Shared Function SelectCT_DTLHQ247(ByVal pStrSo_CT As String) As DataSet
            ' ****************************************************************************
            ' Mục đích: lấy ra một chứng từ chi tiết trong bảng TCS_CTU_DTL với các điều kiện đầu vào
            ' Người viết: XXX
            ' Ngày viết: 23/09/2017
            '*****************************************************************************

            Dim dsCT As New DataSet
            Dim strSql As String
            Try
                strSql = " Select SHKB, Ngay_KB, Ma_NV, So_BT, Ma_DThu, CCH_ID, Ma_Cap," & _
                         " Ma_Chuong, LKH_ID, Ma_Loai, Ma_Khoan, MTM_ID, Ma_Muc, Ma_TMuc, Noi_Dung," & _
                         " DT_ID, Ma_TLDT, Ma_dp, Ky_Thue, SoTien, SoTien_NT " & _
                         " From TCS_CTU_NTDT_HQ247_DTL " & _
                         " Where so_ct = '" & pStrSo_CT & "' "
                dsCT = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return dsCT
            Catch ex As Exception
                log.Error("daCTUHQ304.SelectCT_DTL  So_CT[" & pStrSo_CT & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "daCTUHQ304.SelectCT_DTL  So_CT[" & pStrSo_CT & "]  ")
                Throw ex

            Finally

            End Try
        End Function

        Public Shared Function Update304_HachToan(ByVal pStrID304 As String, ByVal pSO_CT_NH As String, ByVal pDienGiai As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213 & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", SO_CT_NH='" & pSO_CT_NH & "', DIEN_GIAI='" & pDienGiai & "'"
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)

                StrSQL = "UPDATE TCS_CTU_NTDT_HQ247_HDR SET so_ct_nh='" & pSO_CT_NH & "',ngay_kh_nh = sysdate,remarks='" & pDienGiai & "'  WHERE SO_CT IN (SELECT SO_CTU FROM tcs_hq247_304 WHERE ID='" & pStrID304 & "' )"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)

                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("Update304_SoChungTuCore  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "Update304_SoChungTuCore  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function Update304_ResetSoCtuEtax(ByVal pStrID304 As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_304 SET SO_CTU=NULL WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("Update304_ResetSoCtuEtax  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "Update304_ResetSoCtuEtax  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function

    End Class

End Namespace
