Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon
Namespace TDDMDC
    Public Class daMucTMuc
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objMucTMuc As infMucTMuc) As Boolean
            Dim cnMucTMuc As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO TCS_DM_MUC_TMUC (MTM_ID, MA_MUC, MA_TMUC, TEN, NGAY_BD, " & IIf(objMucTMuc.NGAY_KT = "", "", "NGAY_KT, ") & "GHI_CHU) " & _
                                "VALUES (" & getDataKey("TCS_MTM_ID_SEQ.NEXTVAL") & ", '" & objMucTMuc.MA_MUC & "'," & _
                                "'" & objMucTMuc.MA_TMUC & "','" & objMucTMuc.Ten & " '," & _
                                objMucTMuc.NGAY_BD & ", " & IIf(objMucTMuc.NGAY_KT = "", "", objMucTMuc.NGAY_KT & ", ") & _
                                "'" & objMucTMuc.GHICHU & "')"
            Try
                cnMucTMuc = New DataAccess
                cnMucTMuc.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới mục - tiểu mục")
                'LogDebug.WriteLog("Có lỗi xảy ra khi thêm mới mục - tiểu mục: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
                Throw ex
            Finally
                If Not cnMucTMuc Is Nothing Then cnMucTMuc.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objMucTMuc As infMucTMuc) As Boolean
            Dim cnMucTMuc As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_MUC_TMUC SET TEN='" & objMucTMuc.Ten & "', " & _
                   "NGAY_BD=" & objMucTMuc.NGAY_BD & ",NGAY_KT=" & IIf(objMucTMuc.NGAY_KT = "", "NULL", objMucTMuc.NGAY_KT) & ",GHI_CHU='" & _
                   objMucTMuc.GHICHU & "', MA_MUC='" & objMucTMuc.MA_MUC & "', MA_TMUC='" & objMucTMuc.MA_TMUC & "' WHERE MTM_ID=" & objMucTMuc.MTM_ID
            Try
                cnMucTMuc = New DataAccess
                cnMucTMuc.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật mục - tiểu mục")
                'LogDebug.WriteLog("Có lỗi xảy ra khi cập nhật mục - tiểu mục: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
                Throw ex
            Finally
                If Not cnMucTMuc Is Nothing Then cnMucTMuc.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objMucTMuc As infMucTMuc) As Boolean
            Dim cnMucTMuc As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_MUC_TMUC " & _
                    "WHERE MTM_ID=" & objMucTMuc.MTM_ID & ""
            Try
                cnMucTMuc = New DataAccess
                cnMucTMuc.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa mục - tiểu mục")
                'LogDebug.WriteLog("Có lỗi xảy ra khi xoá  mục - tiểu mục: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
            Finally
                If Not cnMucTMuc Is Nothing Then cnMucTMuc.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strMTM_ID As String) As infMucTMuc
            Dim objMucTMuc As New infMucTMuc
            Dim cnMucTMuc As DataAccess
            Dim drMucTMuc As IDataReader
            Dim strSQL As String = "Select MTM_ID, MA_MUC, MA_TMUC, TEN, NGAY_BD, NGAY_KT, GHI_CHU From TCS_DM_MUC_TMUC " & _
                        "Where MTM_ID=" & strMTM_ID
            Try
                cnMucTMuc = New DataAccess
                drMucTMuc = cnMucTMuc.ExecuteDataReader(strSQL, CommandType.Text)
                If drMucTMuc.Read Then
                    objMucTMuc = New infMucTMuc
                    objMucTMuc.MTM_ID = strMTM_ID
                    objMucTMuc.MA_MUC = drMucTMuc("MA_MUC").ToString()
                    objMucTMuc.MA_TMUC = drMucTMuc("MA_TMUC").ToString()
                    objMucTMuc.Ten = drMucTMuc("TEN").ToString()
                    objMucTMuc.NGAY_BD = drMucTMuc("NGAY_BD").ToString()
                    objMucTMuc.NGAY_KT = drMucTMuc("NGAY_KT").ToString()
                    objMucTMuc.GHICHU = drMucTMuc("GHI_CHU").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin mục - tiểu mục")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin mục - tiểu mục từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drMucTMuc Is Nothing Then drMucTMuc.Close()
                If Not cnMucTMuc Is Nothing Then cnMucTMuc.Dispose()
            End Try
            Return objMucTMuc
        End Function

        Public Function Load(ByVal tmpMucTMuc As infMucTMuc) As infMucTMuc
            Dim objMucTMuc As New infMucTMuc
            Dim cnMucTMuc As DataAccess
            Dim drMucTMuc As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc

            Dim strSQL As String = "Select MTM_ID, MA_MUC, MA_TMUC, TEN, NGAY_BD, NGAY_KT, GHI_CHU From TCS_DM_MUC_TMUC " & _
                "Where " & IIf(tmpMucTMuc.MTM_ID <> "", "MTM_ID <> " & tmpMucTMuc.MTM_ID & " and ", "") & "MA_MUC='" & tmpMucTMuc.MA_MUC & "' and MA_TMUC='" & tmpMucTMuc.MA_TMUC & "' and (" & _
                "(NGAY_BD <= " & tmpMucTMuc.NGAY_BD & " and (NGAY_KT IS NULL or NGAY_KT >= " & tmpMucTMuc.NGAY_BD & ")) or " & _
                "(" & IIf(tmpMucTMuc.NGAY_KT = "", "NGAY_KT IS NULL or NGAY_KT >= " & tmpMucTMuc.NGAY_BD, "NGAY_BD <= " & tmpMucTMuc.NGAY_KT & " and (NGAY_KT IS NULL or NGAY_KT >= " & tmpMucTMuc.NGAY_KT & ")") & "))"

            Try
                cnMucTMuc = New DataAccess
                drMucTMuc = cnMucTMuc.ExecuteDataReader(strSQL, CommandType.Text)
                If drMucTMuc.Read Then
                    objMucTMuc = New infMucTMuc
                    objMucTMuc.MTM_ID = drMucTMuc("MTM_ID").ToString()
                    objMucTMuc.MA_MUC = drMucTMuc("MA_MUC").ToString()
                    objMucTMuc.MA_TMUC = drMucTMuc("MA_TMUC").ToString()
                    objMucTMuc.Ten = drMucTMuc("TEN").ToString()
                    objMucTMuc.NGAY_BD = drMucTMuc("NGAY_BD").ToString()
                    objMucTMuc.NGAY_KT = drMucTMuc("NGAY_KT").ToString()
                    objMucTMuc.GHICHU = drMucTMuc("GHI_CHU").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin mục - tiểu mục")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin mục - tiểu mục từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drMucTMuc Is Nothing Then drMucTMuc.Close()
                If Not cnMucTMuc Is Nothing Then cnMucTMuc.Dispose()
            End Try
            Return objMucTMuc
        End Function

        Public Function Load() As Object
            Dim strSQL As String = "Select MTM_ID, MA_MUC, MA_TMUC, TEN, NGAY_BD, NGAY_KT, GHI_CHU from TCS_DM_MUC_TMUC Order by MA_MUC,MA_TMUC"
            Dim cnMucTMuc As DataAccess
            Dim dsMucTMuc As DataSet

            Try
                strSQL = "Select MTM_ID, MA_MUC, MA_TMUC, TEN, " & _
                         "DECODE(NGAY_BD, Null,'',SUBSTR(NGAY_BD, 7, 2) || '/' || SUBSTR (NGAY_BD, 5, 2) || '/' || SUBSTR (NGAY_BD, 1, 4)) NGAY_BD, " & _
                         "DECODE(NGAY_KT, Null,'',SUBSTR(NGAY_KT, 7, 2) || '/' || SUBSTR (NGAY_KT, 5, 2) || '/' || SUBSTR (NGAY_KT, 1, 4)) NGAY_KT, " & _
                         "GHI_CHU from " & _
                         "TCS_DM_MUC_TMUC Order by MA_MUC,MA_TMUC"

                cnMucTMuc = New DataAccess
                dsMucTMuc = cnMucTMuc.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin mục - tiểu mục")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin mục - tiểu mục từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnMucTMuc Is Nothing Then cnMucTMuc.Dispose()
            End Try

            Return dsMucTMuc
        End Function

        Public Function CheckExist(ByVal objMucTMuc As infMucTMuc) As Boolean
            Return Load(objMucTMuc).MTM_ID <> ""
        End Function
    End Class
End Namespace
