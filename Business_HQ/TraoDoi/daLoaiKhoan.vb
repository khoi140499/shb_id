Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon
Namespace TDDMDC
    Public Class daLoaiKhoan
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        'Public Function Insert(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
        '    Dim cnLoaiKhoan As DataAccess
        '    Dim blnResult As Boolean = True
        '    Dim strSql As String = "INSERT INTO TCS_DM_LOAI_KHOAN (LKH_ID, MA_LOAI, MA_KHOAN, TEN, NGAY_BD, " & IIf(objLoaiKhoan.NGAY_KT = "", "", "NGAY_KT, ") & "GHI_CHU) " & _
        '                        "VALUES (" & getDataKey("TCS_LKH_ID_SEQ.NEXTVAL") & ", '" & objLoaiKhoan.MA_LOAI & "'," & _
        '                        "'" & objLoaiKhoan.MA_KHOAN & "','" & objLoaiKhoan.Ten & "'," & _
        '                        objLoaiKhoan.NGAY_BD & "," & IIf(objLoaiKhoan.NGAY_KT = "", "", objLoaiKhoan.NGAY_KT & ", ") & _
        '                        "'" & objLoaiKhoan.GHICHU & "')"
        '    Try
        '        cnLoaiKhoan = New DataAccess
        '        cnLoaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
        '        blnResult = False
        '        Throw ex
        '    Finally
        '        If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
        '    End Try
        '    Return blnResult
        'End Function

        Public Function Update(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
            Dim cnLoaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_LOAI_KHOAN SET MA_LOAI='" & objLoaiKhoan.MA_LOAI & "', MA_KHOAN='" & objLoaiKhoan.MA_KHOAN & "', TEN='" & objLoaiKhoan.Ten & "', " & _
                   "NGAY_BD=" & objLoaiKhoan.NGAY_BD & ",NGAY_KT=" & IIf(objLoaiKhoan.NGAY_KT = "", "NULL", objLoaiKhoan.NGAY_KT) & ",GHI_CHU='" & _
                   objLoaiKhoan.GHICHU & "' WHERE LKH_ID=" & objLoaiKhoan.LKH_ID
            Try
                cnLoaiKhoan = New DataAccess
                cnLoaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi cập nhật loại khoản")
                Throw ex
                'LogDebug.WriteLog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
            Finally
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
            Dim cnLoaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_LOAI_KHOAN " & _
                    "WHERE LKH_ID=" & objLoaiKhoan.LKH_ID
            Try
                cnLoaiKhoan = New DataAccess
                cnLoaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xoá cơ mục lục loại - khoản")
                Throw ex
                'LogDebug.WriteLog("Có lỗi xảy ra khi xoá cơ mục lục loại - khoản: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
            Finally
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strLKH_ID As String) As infLoaiKhoan
            Dim objLoaiKhoan As New infLoaiKhoan
            Dim cnLoaiKhoan As DataAccess
            Dim drLoaiKhoan As IDataReader
            Dim strSQL As String = "Select LKH_ID, MA_LOAI, MA_KHOAN, TEN, NGAY_BD, NGAY_KT, GHI_CHU From TCS_DM_LOAI_KHOAN " & _
                        "Where LKH_ID=" & strLKH_ID
            Try
                cnLoaiKhoan = New DataAccess
                drLoaiKhoan = cnLoaiKhoan.ExecuteDataReader(strSQL, CommandType.Text)
                If drLoaiKhoan.Read Then
                    objLoaiKhoan.LKH_ID = strLKH_ID
                    objLoaiKhoan.MA_LOAI = drLoaiKhoan("MA_LOAI").ToString()
                    objLoaiKhoan.MA_KHOAN = drLoaiKhoan("MA_KHOAN").ToString()
                    objLoaiKhoan.Ten = drLoaiKhoan("TEN").ToString()
                    objLoaiKhoan.NGAY_BD = drLoaiKhoan("NGAY_BD").ToString()
                    objLoaiKhoan.NGAY_KT = drLoaiKhoan("NGAY_KT").ToString()
                    objLoaiKhoan.GHICHU = drLoaiKhoan("GHI_CHU").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin loại - khoản từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin loại - khoản từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drLoaiKhoan Is Nothing Then drLoaiKhoan.Close()
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try
            Return objLoaiKhoan
        End Function

        Public Function Load(ByVal tmpLoaiKhoan As infLoaiKhoan) As infLoaiKhoan
            Dim objLoaiKhoan As New infLoaiKhoan
            Dim cnLoaiKhoan As DataAccess
            Dim drLoaiKhoan As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc

            Dim strSQL As String = "Select LKH_ID, MA_LOAI, MA_KHOAN, TEN, NGAY_BD, NGAY_KT, GHI_CHU From TCS_DM_LOAI_KHOAN " & _
                "Where " & IIf(tmpLoaiKhoan.LKH_ID <> "", "LKH_ID <> " & tmpLoaiKhoan.LKH_ID & " and ", "") & "MA_LOAI='" & tmpLoaiKhoan.MA_LOAI & "' and MA_KHOAN='" & tmpLoaiKhoan.MA_KHOAN & "' and (" & _
                "(NGAY_BD <= " & tmpLoaiKhoan.NGAY_BD & " and (NGAY_KT IS NULL or NGAY_KT >= " & tmpLoaiKhoan.NGAY_BD & ")) or " & _
                "(" & IIf(tmpLoaiKhoan.NGAY_KT = "", "NGAY_KT IS NULL or NGAY_KT >= " & tmpLoaiKhoan.NGAY_BD, "NGAY_BD <= " & tmpLoaiKhoan.NGAY_KT & " and (NGAY_KT IS NULL or NGAY_KT >= " & tmpLoaiKhoan.NGAY_KT & ")") & "))"

            Try
                cnLoaiKhoan = New DataAccess
                drLoaiKhoan = cnLoaiKhoan.ExecuteDataReader(strSQL, CommandType.Text)
                If drLoaiKhoan.Read Then
                    objLoaiKhoan = New infLoaiKhoan
                    objLoaiKhoan.LKH_ID = drLoaiKhoan("LKH_ID").ToString()
                    objLoaiKhoan.MA_LOAI = drLoaiKhoan("MA_LOAI").ToString()
                    objLoaiKhoan.MA_KHOAN = drLoaiKhoan("MA_KHOAN").ToString()
                    objLoaiKhoan.Ten = drLoaiKhoan("TEN").ToString()
                    objLoaiKhoan.NGAY_BD = drLoaiKhoan("NGAY_BD").ToString()
                    objLoaiKhoan.NGAY_KT = drLoaiKhoan("NGAY_KT").ToString()
                    objLoaiKhoan.GHICHU = drLoaiKhoan("GHI_CHU").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin loại - khoản từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin loại - khoản từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drLoaiKhoan Is Nothing Then drLoaiKhoan.Close()
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try
            Return objLoaiKhoan
        End Function

        Public Function Load() As Object
            Dim strSQL As String
            Dim cnLoaiKhoan As DataAccess
            Dim dsLoaiKhoan As DataSet

            Try
                strSQL = "Select LKH_ID, MA_LOAI, MA_KHOAN, TEN, " & _
                         "DECODE(NGAY_BD, Null,'',SUBSTR(NGAY_BD, 7, 2) || '/' || SUBSTR (NGAY_BD, 5, 2) || '/' || SUBSTR (NGAY_BD, 1, 4)) NGAY_BD, " & _
                         "DECODE(NGAY_KT, Null,'',SUBSTR(NGAY_KT, 7, 2) || '/' || SUBSTR (NGAY_KT, 5, 2) || '/' || SUBSTR (NGAY_KT, 1, 4)) NGAY_KT, " & _
                         "GHI_CHU from " & _
                         "TCS_DM_LOAI_KHOAN Order by MA_LOAI,MA_KHOAN"
                cnLoaiKhoan = New DataAccess
                dsLoaiKhoan = cnLoaiKhoan.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin loại - khoản từ CSDL")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu loại khoản từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnLoaiKhoan Is Nothing Then cnLoaiKhoan.Dispose()
            End Try

            Return dsLoaiKhoan
        End Function

        Public Function CheckExist(ByVal objLoaiKhoan As infLoaiKhoan) As Boolean
            Return Load(objLoaiKhoan).LKH_ID <> ""
        End Function

        Public Function CheckExist(ByVal strLKH_ID As String) As Boolean
            Return Load(strLKH_ID).LKH_ID <> ""
        End Function

    End Class

End Namespace
