Namespace TDDMDC
    Public Class infLoaiKhoan
        Private strLKH_ID As String
        Private strMA_LOAI As String
        Private strMA_KHOAN As String
        Private strTen As String
        Private strNGAY_BD As String
        Private strNGAY_KT As String
        Private strGHICHU As String

        Sub New()
        End Sub

        Sub New(ByVal drwLoaiKhoan As DataRow)
            strLKH_ID = drwLoaiKhoan("LKH_ID").ToString().Trim()
            strMA_LOAI = drwLoaiKhoan("MA_LOAI").ToString().Trim()
            strMA_KHOAN = drwLoaiKhoan("MA_KHOAN").ToString().Trim()
            strTen = drwLoaiKhoan("Ten").ToString().Trim()
            strNGAY_BD = drwLoaiKhoan("NGAY_BD").ToString().Trim()
            strNGAY_KT = drwLoaiKhoan("NGAY_KT").ToString().Trim()
            strGHICHU = drwLoaiKhoan("GHI_CHU").ToString().Trim()
        End Sub

        Public Property LKH_ID() As String
            Get
                Return strLKH_ID
            End Get
            Set(ByVal Value As String)
                strLKH_ID = Value
            End Set
        End Property

        Public Property MA_LOAI() As String
            Get
                Return strMA_LOAI
            End Get
            Set(ByVal Value As String)
                strMA_LOAI = Value
            End Set
        End Property

        Public Property MA_KHOAN() As String
            Get
                Return strMA_KHOAN
            End Get
            Set(ByVal Value As String)
                strMA_KHOAN = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property

        Public Property NGAY_BD() As String
            Get
                Return strNGAY_BD
            End Get
            Set(ByVal Value As String)
                strNGAY_BD = Value
            End Set
        End Property

        Public Property NGAY_KT() As String
            Get
                Return strNGAY_KT
            End Get
            Set(ByVal Value As String)
                strNGAY_KT = Value
            End Set
        End Property

        Public Property GHICHU() As String
            Get
                Return strGHICHU
            End Get
            Set(ByVal Value As String)
                strGHICHU = Value
            End Set
        End Property
    End Class
End Namespace
