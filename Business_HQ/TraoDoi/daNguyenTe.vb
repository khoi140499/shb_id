Imports VBOracleLib
Namespace TDDMDC
    Public Class daNguyenTe
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objNguyenTe As infNguyenTe) As Boolean
            Dim cnNguyenTe As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO TCS_DM_NGUYENTE (Ma_NT,Ten) " & _
                                "VALUES ('" & objNguyenTe.Ma_NT.ToUpper & "','" & _
                                objNguyenTe.Ten & "')"

            Try
                cnNguyenTe = New DataAccess
                cnNguyenTe.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới nguyên tệ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi thêm mới nguyên tệ: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
                Throw ex
            Finally
                If Not cnNguyenTe Is Nothing Then cnNguyenTe.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objNguyenTe As infNguyenTe) As Boolean
            Dim cnNguyenTe As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_NGUYENTE SET TEN='" & objNguyenTe.Ten & "' WHERE Ma_NT='" & objNguyenTe.Ma_NT.ToUpper & "'"

            Try
                cnNguyenTe = New DataAccess
                cnNguyenTe.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật nguyên tệ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi cập nhật nguyên tệ: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
                Throw ex
            Finally
                If Not cnNguyenTe Is Nothing Then cnNguyenTe.Dispose()
            End Try
            Return blnResult
        End Function

        Public Sub Update(ByVal objNguyenTe As infNguyenTe, ByRef cnNguyenTe As DataAccess, ByRef tranUpdate As IDbTransaction)
            Dim strSql As String = "UPDATE TCS_DM_NGUYENTE SET TEN='" & objNguyenTe.Ten & "' WHERE Ma_NT='" & objNguyenTe.Ma_NT.ToUpper & "'"

            Try
                cnNguyenTe = New DataAccess
                cnNguyenTe.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật nguyên tệ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi cập nhật nguyên tệ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
            End Try
        End Sub

        Public Function Delete(ByVal objNguyenTe As infNguyenTe) As Boolean
            Dim cnNguyenTe As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_NGUYENTE " & _
                           "WHERE " & "Ma_NT='" & _
                           objNguyenTe.Ma_NT.ToUpper & "'"
            Try
                cnNguyenTe = New DataAccess
                cnNguyenTe.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '     CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa nguyên tệ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi xoá nguyên tệ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
                blnResult = False
            Finally
                If Not cnNguyenTe Is Nothing Then cnNguyenTe.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strMa_NT As String) As infNguyenTe
            Dim objNguyenTe As New infNguyenTe
            Dim cnNguyenTe As DataAccess
            Dim drNguyenTe As IDataReader
            Dim strSQL As String = "Select Ma_NT, TEN From TCS_DM_NGUYENTE " & _
                        "Where Ma_NT='" & strMa_NT.ToUpper() & "'"
            Try
                cnNguyenTe = New DataAccess
                drNguyenTe = cnNguyenTe.ExecuteDataReader(strSQL, CommandType.Text)
                If drNguyenTe.Read Then
                    objNguyenTe = New infNguyenTe
                    objNguyenTe.Ma_NT = strMa_NT
                    objNguyenTe.Ten = drNguyenTe("Ten").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin nguyên tệ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin nguyên tệ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drNguyenTe Is Nothing Then drNguyenTe.Close()
                If Not cnNguyenTe Is Nothing Then cnNguyenTe.Dispose()
            End Try
            Return objNguyenTe
        End Function

        Public Function Load() As Object
            Dim strSQL As String = "Select Ma_NT,Ten from TCS_DM_NGUYENTE Order by Ma_NT ASC"
            Dim cnNguyenTe As DataAccess
            Dim dsNguyenTe As DataSet

            Try
                cnNguyenTe = New DataAccess
                dsNguyenTe = cnNguyenTe.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin nguyên tệ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin nguyên tệ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnNguyenTe Is Nothing Then cnNguyenTe.Dispose()
            End Try

            Return dsNguyenTe
        End Function

        Public Function CheckExist(ByVal objNguyenTe As infNguyenTe) As Boolean
            Return Load(objNguyenTe.Ma_NT).Ma_NT <> ""
        End Function

        Public Shared Function Valid2Del(ByVal objNguyenTe As infNguyenTe) As Byte
            Dim cnNguyenTe As DataAccess
            Dim drNguyenTe As IDataReader
            Dim strSQL As String = "Select TG_ID from TCS_DM_TYGIA where MA_NT = '" & objNguyenTe.Ma_NT & "'"
            Dim bytResult As Byte = 0

            Try
                cnNguyenTe = New DataAccess
                drNguyenTe = cnNguyenTe.ExecuteDataReader(strSQL, CommandType.Text)
                If drNguyenTe.Read() Then
                    bytResult = 1
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin nguyên tệ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin nguyên tệ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drNguyenTe Is Nothing Then drNguyenTe.Close()
                If Not cnNguyenTe Is Nothing Then cnNguyenTe.Dispose()
            End Try
            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function
    End Class
End Namespace
