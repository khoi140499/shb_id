Namespace TDDMDC
    Public Class infCapChuong
        Private strCCH_ID As String
        Private strMA_CAP As String
        Private strMA_CHUONG As String
        Private strTen As String
        Private strNGAY_BD As String
        Private strNGAY_KT As String
        Private strGHICHU As String

        Sub New()
        End Sub

        Sub New(ByVal drwCapChuong As DataRow)
            strCCH_ID = drwCapChuong("CCH_ID").ToString().Trim()
            strMA_CAP = drwCapChuong("MA_CAP").ToString().Trim()
            strMA_CHUONG = drwCapChuong("MA_CHUONG").ToString().Trim()
            strTen = drwCapChuong("Ten").ToString().Trim()
            strNGAY_BD = drwCapChuong("NGAY_BD").ToString().Trim()
            strNGAY_KT = drwCapChuong("NGAY_KT").ToString().Trim()
            strGHICHU = drwCapChuong("GHI_CHU").ToString().Trim()
        End Sub

        Public Property CCH_ID() As String
            Get
                Return strCCH_ID
            End Get
            Set(ByVal Value As String)
                strCCH_ID = Value
            End Set
        End Property

        Public Property MA_CAP() As String
            Get
                Return strMA_CAP
            End Get
            Set(ByVal Value As String)
                strMA_CAP = Value
            End Set
        End Property

        Public Property MA_CHUONG() As String
            Get
                Return strMA_CHUONG
            End Get
            Set(ByVal Value As String)
                strMA_CHUONG = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property

        Public Property NGAY_BD() As String
            Get
                Return strNGAY_BD
            End Get
            Set(ByVal Value As String)
                strNGAY_BD = Value
            End Set
        End Property

        Public Property NGAY_KT() As String
            Get
                Return strNGAY_KT
            End Get
            Set(ByVal Value As String)
                strNGAY_KT = Value
            End Set
        End Property

        Public Property GHICHU() As String
            Get
                Return strGHICHU
            End Get
            Set(ByVal Value As String)
                strGHICHU = Value
            End Set
        End Property
    End Class
End Namespace
