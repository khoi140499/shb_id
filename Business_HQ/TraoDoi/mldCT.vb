﻿'Imports VBOracleLib
'Imports Business.Common.mdlSystemVariables
'Imports Business.Common.mdlCommon
'Imports Business.Common
'Imports Business.ChungTu
'Namespace TraoDoi
'    Public Module mldCT

'#Region "Biến toàn cục"
'        Public mstrMaDBHC As String = gstrMaDBHC
'        Public mblnBatBuocMLNS As Boolean = True        'Ghi nhận việc có bắt buộc nhập MLNS hay không?
'        Public mblnBatBuocTK As Boolean = True          'Bắt buộc nhập tờ khai, không bắt buộc nhập số bảng kê
'        Public mblnNhapThucThu As Boolean = True        'Cho phép nhập số thực thu
'        Public mblnTLDT As Boolean = True               'Bắt buộc nhập Tỷ Lệ Điều Tiết
'        Private mstrCurrentNT As String                 'Lưu mã nguyên tệ hiện thời
'        Public mstrMaQuyMacDinh As String = "01"        'Biến lưu mã quỹ mặc định
'        Public mintRowChiTiet As Integer               'Biến lưu dòng hiện tại trên grdChiTiet

'        Public mdblMaxTien As Double = 999999999999999     '15

'        Public mkeyCT As New Business.ChungTu.KeyCTu()               'Lưu bộ key của chứng từ
'        Public mblnCTOnDate As Boolean = False              'Đánh dấu chứng từ có lập trong ngày hay không?

'        Public Enum NguonNNT As Byte
'            Khac = 0
'            NNThue_CTN = 1
'            NNThue_VL = 2
'            TruocBa = 3
'            HaiQuan = 4
'            SoThue_CTN = 5
'            VangLai = 6
'            Loi = 7
'        End Enum
'        'Biến Enum eRowHeader để lưu thứ tự Header của chứng từ
'        Public Enum eRowHeader As Byte
'            NgayLV = 0
'            SoCT = 1
'            MaNNT = 2
'            TenNNT = 2
'            DChiNNT = 3
'            MaNNTien = 4
'            TenNNTien = 4
'            DChiNNTien = 5
'            MaDBHC = 6
'            TenDBHC = 6
'            TKNo = 7
'            TKCo = 8
'            MaCQThu = 9
'            TenCQThu = 9
'            LoaiThue = 10
'            MaNT = 11
'            TiGia = 12
'            ToKhaiSo = 13
'            NgayDK = 14
'            LHXNK = 15
'            SoQD = 16
'            NgayBHQD = 17
'            CQBH = 18
'            MaDVSDNS = 19
'            SoKhung = 20
'            SoMay = 21
'            SoCTNH = 22
'            TK_KH_NH = 23
'            NGAY_KH_NH = 24
'            MA_NH_A = 25
'            MA_NH_B = 26
'            TKKHNhan = 27
'            TenKHNhan = 28
'            DCKHNhan = 29
'            So_BK = 30
'            Ngay_BK = 31
'        End Enum

'        'Biến Enum eColDTL để lưu thứ tự Detail của chứng từ
'        Public Enum eColDTL As Byte
'            MaQuy = 0
'            CChuong = 1
'            LKhoan = 2
'            MTieuMuc = 3
'            TLDT = 4
'            NoiDung = 5
'            TienNT = 6
'            Tien = 7
'            KyThue = 8
'            MaDP = 9
'            ID = 10
'        End Enum
'        Public Enum CTEnum As Byte
'            TienMat
'            CK_NganHang
'            CK_KhoBac
'        End Enum
'#End Region

'#Region "Xử lý xâu"
'        Public Function GetTTBL(ByVal strMaTT As String, ByVal strLanIn As String) As Integer
'            '------------------------------------------------------------------
'            ' Mục đích: Lấy thứ tự của trạng thái chứng từ khi đã biết mã (dùng hiển thị ảnh)
'            ' Tham số: 
'            ' Giá trị trả về: 
'            ' Ngày viết: 14/11/2007
'            ' Người viết: Lê Hồng Hà
'            ' -----------------------------------------------------------------
'            Select Case strMaTT
'                Case "00"   ' Chưa chuyển thành chứng từ
'                    Return 0
'                Case "02"   ' Đã chuyển thành chứng từ
'                    Return 1
'                Case "03"   'Đã hủy
'                    Return 4
'            End Select
'        End Function
'        Public Function GetTTTrangThai(ByVal strMaTT As String, ByVal strLanIn As String) As Integer
'            '------------------------------------------------------------------
'            ' Mục đích: Lấy thứ tự của trạng thái chứng từ khi đã biết mã (dùng hiển thị ảnh)
'            ' Tham số: 
'            ' Giá trị trả về: 
'            ' Ngày viết: 14/11/2007
'            ' Người viết: Lê Hồng Hà
'            ' -----------------------------------------------------------------
'            Select Case strMaTT
'                Case "00"   'Chưa kiểm soát
'                    Return 0
'                Case "01"   'Đã kiểm soát
'                    Return 1
'                Case "03"   'Kiểm soát lỗi
'                    Return 3
'                Case "04"   'Đã hủy
'                    Return 4
'                Case "05"   'Đã chuyển sang KTKB
'                    Return 2
'            End Select
'        End Function
'        Public Function GetKyThue(ByVal strNgayLV As String) As String
'            '------------------------------------------------------------------
'            ' Mục đích: Đưa từ dạng ngày yyyyMMdd -> dạng 'MM/yyyy'
'            ' Tham số: 
'            ' Giá trị trả về: 
'            ' Ngày viết: 07/11/2007
'            ' Người viết: Lê Hồng Hà
'            ' -----------------------------------------------------------------
'            Try
'                If (strNgayLV.Length <> 8) Then Return "" 'Không đúng định dạng đầu vào

'                Dim strThang, strNam As String
'                strNam = strNgayLV.Substring(0, 4)
'                strThang = strNgayLV.Substring(4, 2)

'                Return (strThang & "/" & strNam)
'            Catch ex As Exception
'                Throw ex
'            End Try
'        End Function
'        Public Function ChuanHoa_KyThue(ByVal strKyThue As String) As String
'            '-------------------------------------------------------------------------------------
'            ' Mục đích: Chuẩn hóa thông tin kỳ thuế trong trước bạ
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 07/11/2007
'            '--------------------------------------------------------------------------------------
'            Try
'                strKyThue = Trim(strKyThue)
'                If (Valid_KyThue(strKyThue)) Then Return strKyThue

'                Dim intYear As Integer = Convert.ToInt16(strKyThue)
'                Return ("01/" & strKyThue)
'            Catch ex As Exception
'                Return GetKyThue(gdtmNgayLV)
'            End Try
'        End Function
'#End Region

'#Region "Valid _DTL"
'        Public Function Valid_DTL(ByVal dtl As infChungTuDTL, _
'                    ByVal fblnBatBuocMLNS As Boolean, _
'                    ByVal fblnVND As Boolean, _
'                    ByVal fblnTLDT As Boolean) As Integer
'            Try
'                If (fblnBatBuocMLNS = True) Then
'                    If (Not Valid_MaQuy(dtl.MaQuy, True)) Then
'                        'MessageBox.Show("Thông tin 'Mã quỹ' không hợp lệ", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                        Return eColDTL.MaQuy
'                    End If
'                    If (Not Valid_Chuong(dtl.Ma_Chuong, True)) Then
'                        'MessageBox.Show("Thông tin 'Chương' không hợp lệ", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                        Return eColDTL.CChuong
'                    End If
'                    If (Not Valid_Khoan(dtl.Ma_Khoan, True)) Then
'                        'MessageBox.Show("Thông tin 'Ngành kinh tế' không hợp lệ", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                        Return eColDTL.LKhoan
'                    End If
'                    If (Not Valid_TMuc(dtl.Ma_TMuc, True)) Then
'                        'MessageBox.Show("Thông tin 'Nội dung kinh tế' không hợp lệ", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                        Return eColDTL.MTieuMuc
'                    End If

'                    'Ngân hàng ko check tỷ lệ điều tiết
'                    'If ((Not Valid_TLDT(dtl.Ma_TLDT, True)) And (fblnTLDT = True)) Then
'                    '    'MessageBox.Show("Thông tin Tỷ lệ phân chia không hợp lệ", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                    '    Return eColDTL.TLDT
'                    'End If
'                End If

'                If (dtl.SoTien = 0) Then
'                    'MessageBox.Show("Số tiền không được để 0", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                    If (fblnVND) Then
'                        Return eColDTL.Tien
'                    Else
'                        Return eColDTL.TienNT
'                    End If
'                End If
'                If (Not Valid_KyThue(dtl.Ky_Thue)) Then
'                    'MessageBox.Show("Thông tin Kỳ thuế không hợp lệ (MM/yyyy)", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                    Return eColDTL.KyThue
'                End If
'                Return -1
'            Catch ex As Exception
'                Return -1
'            End Try
'        End Function
'        Public Function Valid_DTL(ByVal dtl() As infChungTuDTL, _
'                ByVal fblnBatBuocMLNS As Boolean, _
'                ByVal fblnVND As Boolean) As Integer()
'            Dim intReturn(1) As Integer
'            Try
'                Dim i, iCol As Integer
'                For i = 0 To dtl.Length - 1
'                    iCol = Valid_DTL(dtl(i), fblnBatBuocMLNS, fblnVND, mblnTLDT)
'                    If (iCol <> -1) Then
'                        intReturn(0) = i
'                        intReturn(1) = iCol
'                        Return intReturn
'                    End If
'                Next
'                Return Nothing
'            Catch ex As Exception
'                Return Nothing
'            End Try
'        End Function
'#End Region

'#Region "Valid"
'        Public Function Valid_Char_SoCT(ByVal c As Char) As Boolean
'            '-------------------------------------------
'            ' Mục đích: Tính hợp lệ của Ký tự số chứng từ
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 06/08/2008
'            '-------------------------------------------

'            Try
'                Dim intKeyCode As Integer = Asc(c)

'                If ((intKeyCode >= 48) And (intKeyCode <= 57)) _
'                    Or (intKeyCode = 8) Then
'                    Return True
'                End If

'                Return False
'            Catch ex As Exception

'            End Try
'        End Function

'        Public Function Valid_Char_ToKhai(ByVal c As Char) As Boolean
'            '-------------------------------------------
'            ' Mục đích: Tính hợp lệ của Ký tự số tờ khai
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 06/08/2008
'            '-------------------------------------------
'            Try
'                Dim intKeyCode As Integer = Asc(c)

'                If ((intKeyCode >= 48) And (intKeyCode <= 57)) _
'                    Or (c.ToString() = ",") _
'                    Or (intKeyCode = 8) Then
'                    Return True
'                End If

'                Return False
'            Catch ex As Exception

'            End Try
'        End Function

'        Public Function Valid_Char_So(ByVal c As Char) As Boolean
'            '-------------------------------------------
'            ' Mục đích: Tính hợp lệ của Ký tự số 
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 06/08/2008
'            '-------------------------------------------
'            Try
'                Dim intKeyCode As Integer = Asc(c)

'                If ((intKeyCode >= 48) And (intKeyCode <= 57)) _
'                    Or (c.ToString() = "-") _
'                    Or (c.ToString() = " ") _
'                    Or (c.ToString() = ".") _
'                    Or (intKeyCode = 8) Then
'                    Return True
'                End If

'                Return False
'            Catch ex As Exception

'            End Try
'        End Function

'        Public Function Valid_ToKhai(ByVal strToKhai As String) As Boolean
'            '-------------------------------------------
'            ' Mục đích: Tính hợp lệ của tờ khai
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 06/08/2008
'            '-------------------------------------------
'            Try
'                If (strToKhai.Trim() = "") Then
'                    Return True
'                End If

'                Dim c As String
'                c = strToKhai.Replace(",", "")
'                c = c.Replace(" ", "")
'                If Not IsNumeric(c) Then
'                    Return False
'                End If

'                Return True
'            Catch ex As Exception
'                Return False
'            End Try
'        End Function
'#End Region

'#Region "Others"
'        'ICB: lấy danh sách Địa bàn thu
'        'Hoàng Văn Anh
'        Public Function GetDB_Thu() As DataSet
'            Dim strSQL As String
'            Dim dsDB_Thu As New DataSet
'            Dim KetNoi As DataAccess
'            Try
'                KetNoi = New DataAccess

'                Dim arrDBThu As String() = ParmValues_Load("DB_THU").Split(";"c)
'                Dim strDK_DBHC As String = ""
'                Dim strDK_DBHC_Cha As String = ""
'                If arrDBThu.Length > 0 Then
'                    Dim i As Integer
'                    For i = 0 To arrDBThu.Length - 1
'                        If (arrDBThu(i).Length >= 0) And (arrDBThu(i).Trim() <> gstrMaDBHC) Then
'                            strDK_DBHC_Cha &= Globals.EscapeQuote(arrDBThu(i).ToString()) & ","
'                        End If
'                    Next
'                    '/// Xóa bỏ dấu "," cuối cùng ở xâu điều kiện
'                    If strDK_DBHC_Cha <> "" Then
'                        strDK_DBHC_Cha = strDK_DBHC_Cha.Substring(0, strDK_DBHC_Cha.Length - 1)
'                    End If
'                End If
'                Dim strTinhTP As String = ParmValues_Load("MA_TTP")
'                If (strTinhTP <> "") And (IsTinh_TP() = False) Then
'                    '/// Trường hợp này là thu hộ cả Tỉnh (TP)
'                    If strDK_DBHC_Cha <> "" Then
'                        strDK_DBHC = strDK_DBHC_Cha & "," & Globals.EscapeQuote(strTinhTP)
'                    Else
'                        strDK_DBHC = Globals.EscapeQuote(strTinhTP)
'                    End If
'                Else
'                    '/// Trường hợp này không thu hộ Tỉnh (TP)
'                    strDK_DBHC = strDK_DBHC_Cha
'                End If
'                If strDK_DBHC_Cha <> "" Then
'                    strDK_DBHC_Cha += "," & Globals.EscapeQuote(gstrMaDBHC)
'                Else
'                    strDK_DBHC_Cha = Globals.EscapeQuote(gstrMaDBHC)
'                End If

'                strSQL = "Select ma_xa, ten " & _
'                    " From " & _
'                    " (SELECT DISTINCT ma_xa,  ten, 1 as TT  FROM tcs_dm_xa WHERE (ma_xa = " & Globals.EscapeQuote(gstrMaDBHC) & ") " & _
'                    " UNION " & _
'                    " SELECT DISTINCT ma_xa,  ten, 2 as TT  FROM tcs_dm_xa WHERE (ma_xa IN (" & strDK_DBHC & ")) " & _
'                    " UNION " & _
'                    " SELECT DISTINCT ma_xa, ten, 3 as TT FROM tcs_dm_xa WHERE (ma_cha IN (" & strDK_DBHC_Cha & ")) " & _
'                    " ) DBHC " & _
'                    " order by TT, ma_xa "
'                dsDB_Thu = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
'            Catch ex As Exception
'                'LogDebug.Writelog("Lỗi trong Hàm GetDB_Thu" & ex.ToString())
'            Finally
'                If Not KetNoi Is Nothing Then
'                    KetNoi.Dispose()
'                    KetNoi = Nothing
'                End If
'            End Try

'            Return dsDB_Thu
'        End Function
'        Public Function GetTongTien(ByVal ds As DataSet, ByVal strFieldName As String) As Double
'            '-------------------------------------------------------------------------------------
'            ' Mục đích: Tính tổng tiền
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 07/11/2007
'            '--------------------------------------------------------------------------------------

'            Try
'                If (IsEmptyDataSet(ds)) Then Return 0

'                Dim dblTongTien, dblTien As Double
'                Dim intCount As Integer = ds.Tables(0).Rows.Count

'                Dim i As Integer
'                For i = 0 To intCount - 1
'                    Try
'                        dblTien = Convert.ToDouble(ds.Tables(0).Rows(i).Item(strFieldName).ToString())
'                    Catch ex As Exception
'                        dblTien = 0
'                    End Try
'                    dblTongTien += dblTien
'                Next
'                Return dblTongTien
'            Catch ex As Exception
'                Return 0
'            End Try
'        End Function
'        'Public Function GetGridIndex(ByVal grdDSCT As C1FlexGrid, _
'        '     ByVal intVitriHienTai As Integer) As Integer
'        '    '-------------------------------------------------------------------------------------
'        '    ' Mục đích: Lấy vị trí chứng từ trên grid
'        '    ' Người viết: Lê Hồng Hà
'        '    ' Ngày viết: 07/11/2007
'        '    '--------------------------------------------------------------------------------------
'        '    If (intVitriHienTai > 1) Then
'        '        If (intVitriHienTai < grdDSCT.Rows.Count) Then
'        '            Return intVitriHienTai
'        '        Else
'        '            Return intVitriHienTai - 1
'        '        End If
'        '    End If

'        '    Return 1
'        'End Function

'        Public Function ConvertKey(ByVal intKey As Integer) As Char
'            '-------------------------------------------------------------------------------------
'            ' Mục đích: Convert mã phím Number Numlock về thành phím Number thường
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 15/05/2008
'            '--------------------------------------------------------------------------------------

'            Try
'                Dim intKeyReturn As Integer = 0

'                Select Case intKey
'                    Case 96 To 105
'                        intKeyReturn = intKey - 48
'                    Case Else
'                        intKeyReturn = intKey
'                End Select

'                Return Chr(intKeyReturn)
'            Catch ex As Exception
'                Return ""
'            End Try
'        End Function

'        Public Function CQThu_TK(ByVal strTK As String) As String
'            '-------------------------------------------------------------------------------------
'            ' Mục đích: Lấy mã cơ quan thu tương ứng với mã tài khoản
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 15/05/2008
'            '--------------------------------------------------------------------------------------

'            Dim conn As DataAccess
'            Dim dr As IDataReader

'            Try
'                conn = New DataAccess
'                Dim strSQL As String = "select ma_cqthu from tcs_dm_taikhoan where tk='" & strTK.Replace(".", "") & "'"

'                dr = conn.ExecuteDataReader(strSQL, CommandType.Text)

'                Dim strReturn As String = ""

'                If (dr.Read()) Then
'                    strReturn = GetString(dr(0)).Trim()
'                End If

'                Return strReturn
'            Catch ex As Exception
'                Throw ex
'            Finally
'                If (Not IsNothing(dr)) Then dr.Close()
'                If (Not IsNothing(conn)) Then conn.Dispose()
'            End Try
'        End Function

'        Public Function Get_MLNS_LT(ByVal strMa_LThue As String) As String()
'            '-------------------------------------------------------------------------------------
'            ' Mục đích: Lấy các MLNS tương ứng với mã loại thuế
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 15/05/2008
'            '--------------------------------------------------------------------------------------

'            Dim cnLoaiThue As DataAccess
'            Dim ds As DataSet

'            Try
'                cnLoaiThue = New DataAccess

'                Dim strSQL As String = "SELECT lt_id, ma_muc, ma_tmuc, ma_lthue, " & _
'                                       "ma_cap, ma_chuong, ma_loai, ma_khoan " & _
'                                       "FROM tcs_xdinh_lthue " & _
'                                       "Where Ma_LThue='" & strMa_LThue & "'"
'                ds = cnLoaiThue.ExecuteReturnDataSet(strSQL, CommandType.Text)

'                If (IsEmptyDataSet(ds)) Then Return Nothing

'                Dim s(5) As String

'                s(0) = GetString(ds.Tables(0).Rows(0).Item("Ma_cap"))
'                s(1) = GetString(ds.Tables(0).Rows(0).Item("Ma_chuong"))
'                s(2) = GetString(ds.Tables(0).Rows(0).Item("Ma_loai"))
'                s(3) = GetString(ds.Tables(0).Rows(0).Item("Ma_khoan"))
'                s(4) = GetString(ds.Tables(0).Rows(0).Item("Ma_muc"))
'                s(5) = GetString(ds.Tables(0).Rows(0).Item("Ma_tmuc"))

'                Return s
'            Catch ex As Exception
'                Throw ex
'            Finally
'                If Not cnLoaiThue Is Nothing Then cnLoaiThue.Dispose()
'                If (Not IsNothing(ds)) Then ds.Dispose()
'            End Try
'        End Function

'        Public Function Get_CtuDetail(ByVal strMa_LThue As String) As CTUDetail
'            '-------------------------------------------------------------------------------------
'            ' Mục đích: Lấy các MLNS tương ứng với mã loại thuế
'            ' Người viết: Hoàng Văn Anh
'            ' Ngày viết: 20/12/2008
'            '--------------------------------------------------------------------------------------

'            Dim cnLoaiThue As DataAccess
'            Dim ds As DataSet

'            Try
'                cnLoaiThue = New DataAccess

'                Dim strSQL As String = "SELECT lt_id, ma_muc, ma_tmuc, ma_lthue, " & _
'                                       "ma_cap, ma_chuong, ma_loai, ma_khoan " & _
'                                       "FROM tcs_xdinh_lthue " & _
'                                       "Where Ma_LThue='" & strMa_LThue & "'"
'                ds = cnLoaiThue.ExecuteReturnDataSet(strSQL, CommandType.Text)

'                If (IsEmptyDataSet(ds)) Then Return Nothing
'                Dim CtuDetail As New CTUDetail

'                ' s(0) = GetString(ds.Tables(0).Rows(0).Item("Ma_cap"))
'                CtuDetail.Ma_Chuong = GetString(ds.Tables(0).Rows(0).Item("Ma_chuong"))
'                's(2) = GetString(ds.Tables(0).Rows(0).Item("Ma_loai"))
'                CtuDetail.Ma_NganhKT = GetString(ds.Tables(0).Rows(0).Item("Ma_khoan"))
'                's(4) = GetString(ds.Tables(0).Rows(0).Item("Ma_muc"))
'                CtuDetail.ND_NganhKT = GetString(ds.Tables(0).Rows(0).Item("Ma_tmuc"))

'                Return CtuDetail

'            Catch ex As Exception
'                Throw ex
'            Finally
'                If Not cnLoaiThue Is Nothing Then cnLoaiThue.Dispose()
'                If (Not IsNothing(ds)) Then ds.Dispose()
'            End Try
'        End Function

'        Public Function ChuanHoa_MaNNT(ByVal strMaNNT As String) As String
'            Try
'                Dim strReturn As String = strMaNNT

'                'If (strMaNNT.Length > 10) And (strMaNNT.IndexOf("-") < 0) Then
'                '    strReturn = strReturn.Insert(10, "-")
'                'End If

'                strReturn = strReturn.Replace("_", "")
'                strReturn = strReturn.Trim("-").Trim()

'                Return strReturn
'            Catch ex As Exception
'                Throw ex
'            End Try
'        End Function

'        Public Function Get_LHXNK(ByVal strMaCQThu As String, ByVal strSoTK As String, ByVal strNgayTK As String) As String
'            '-------------------------------------------------------------------------------------
'            ' Mục đích: Lấy loại hình xuất nhập khẩu 
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 21/09/2008
'            '--------------------------------------------------------------------------------------

'            Dim conn As DataAccess
'            Dim dr As IDataReader

'            Try
'                If (strMaCQThu.Trim() = "") Or (strSoTK.Trim() = "") Or (strNgayTK.Trim() = "") Then
'                    Return ""
'                End If

'                conn = New DataAccess
'                Dim strSQL As String = "select lh_xnk from tcs_ds_tokhai " & _
'                                       "where ma_cqthu = '" & strMaCQThu & _
'                                       "' and so_tk = '" & strSoTK & _
'                                       "' and ngay_tk = " & ToValue(strNgayTK, "DATE", "dd/MM/yyyy")
'                dr = conn.ExecuteDataReader(strSQL, CommandType.Text)

'                Dim strReturn As String = ""

'                If (dr.Read()) Then
'                    strReturn = GetString(dr(0)).Trim()
'                End If

'                Return strReturn
'            Catch ex As Exception
'                Throw ex
'            Finally
'                If (Not IsNothing(dr)) Then dr.Close()
'                If (Not IsNothing(conn)) Then conn.Dispose()
'            End Try
'        End Function
'#End Region

'#Region "In chứng từ"
'        Public Function Exists_VangLai(ByVal strMaNNT As String) As Boolean
'            '-----------------------------------------------------
'            ' Mục đích: Kiểm tra sự tồn tại của NNThuế trong sổ thuế
'            ' Tham số: mã người nộp thuế
'            ' Giá trị trả về:
'            ' Ngày viết: 28/11/2007
'            ' Người viết: Lê Hồng Hà
'            ' ----------------------------------------------------  
'            Dim cnCT As DataAccess
'            Try
'                cnCT = New DataAccess
'                Dim strSQL As String
'                strSQL = "select 1 " & _
'                    "from tcs_dm_nnt_kb " & _
'                    "where ma_nnt = '" & strMaNNT & "'"
'                Dim blnReturn As Boolean = False
'                Dim drNNT As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
'                If (drNNT.Read()) Then
'                    blnReturn = True
'                End If
'                drNNT.Close()
'                Return blnReturn
'            Catch ex As Exception
'                Throw ex
'            Finally
'                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
'            End Try
'        End Function

'        Private Function strDTU_Cat_Xau(ByRef inputStr As String, ByVal len As Integer) As String
'            '---------------------------------------------------------------------------
'            'Mục đích: nếu dòng chữ nộp tiền quá dài thì cắt thành 2 dòng
'            'dau vao:
'            '               - inputStr: xau cat cat
'            '               - len     : do dai can dua vao de cat
'            'giá trị trả về: 
'            '               - inputStr: dòng cắt thứ nhất
'            '               - tsTien  : dòng cắt thứ 2
'            'ngày viết: 19/09/2005
'            'Người viết: Nguyễn Hữu Hoan
'            '---------------------------------------------------------------------------
'            Dim strResult As String = ""
'            Dim i As Integer

'            Try
'                If inputStr.Length > len Then
'                    i = len
'                    While ((i < inputStr.Length - 1) And (inputStr.Chars(i) <> " "))
'                        i = i + 1
'                    End While
'                    strResult = inputStr.Substring(i)
'                    inputStr = inputStr.Substring(0, i)
'                End If
'            Catch ex As Exception
'                MsgBox("Lỗi trong quá trình cắt xâu !", MsgBoxStyle.Critical, "Chú ý")
'                'LogDebug.Writelog("Lỗi trong quá trình cắt xâu !")
'            End Try
'            ' Tra lai ket qua cho ham
'            Return strResult
'        End Function

'        'ICB-16/10/2008: Load tài khoản ngân hàng A,B
'        'Hoàng Văn Anh
'        Public Function MapTK_Load(ByVal strMa_DBHC As String) As ArrayList
'            Dim strSQL As String
'            Dim KetNoi As DataAccess
'            Dim arr As New ArrayList
'            Try
'                If strMa_DBHC.IndexOf("TTT") <> -1 Then
'                    strMa_DBHC = gstrMaDBHC
'                End If
'                KetNoi = New DataAccess
'                strSQL = "SELECT  a.ma_nh as NH_A, a.ma_kb as NH_B, a.tk_kb as TK_NO " _
'                    & " FROM tcs_map_tk_nh_kb a " _
'                    & "where a.dbhc =" & Globals.EscapeQuote(strMa_DBHC.Trim())
'                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
'                If (Not ds Is Nothing) And (ds.Tables(0).Rows.Count > 0) Then
'                    arr.Add(ds.Tables(0).Rows(0)(0)) 'NH
'                    arr.Add(ds.Tables(0).Rows(0)(1)) 'KB
'                    arr.Add(ds.Tables(0).Rows(0)(2)) 'Ma tk KB
'                End If
'            Catch ex As Exception
'                'LogDebug.Writelog("Lỗi trong hàmICB: MapTK_Load - " & ex.ToString())
'            Finally
'                If Not KetNoi Is Nothing Then
'                    KetNoi.Dispose()
'                    KetNoi = Nothing
'                End If
'            End Try
'            Return arr
'        End Function
'        'ICB-16/10/2008: Load tài khoản GL của ngân hàng 
'        'Hoàng Văn Anh
'        Public Function Get_TK_GL_NH(ByVal strMaDBHC As String) As String
'            Dim strSQL As String
'            Dim KetNoi As DataAccess
'            Dim strMaDB As String = ""
'            Dim strResult As String = ""
'            Try
'                '/// kiểm tra trường hợp ko phải là Tỉnh/TP nhưng thu hộ Tỉnh/TP
'                If (Not IsTinh_TP()) And (strMaDBHC.IndexOf("TTT") <> -1) Then
'                    strMaDBHC = gstrMaDBHC
'                End If
'                KetNoi = New DataAccess
'                strSQL = "SELECT  a.TK_GL_NH " _
'                    & " FROM tcs_map_tk_nh_kb a " _
'                    & "where a.dbhc =" & Globals.EscapeQuote(strMaDBHC.Trim())
'                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
'                If (Not ds Is Nothing) And (ds.Tables(0).Rows.Count > 0) Then
'                    strResult = CStr(ds.Tables(0).Rows(0)(0))
'                End If
'            Catch ex As Exception
'                'LogDebug.Writelog("Lỗi trong hàmICB: Get_TK_GL_NH - " & ex.ToString())
'            Finally
'                If Not KetNoi Is Nothing Then
'                    KetNoi.Dispose()
'                    KetNoi = Nothing
'                End If
'            End Try

'            Return strResult
'        End Function

'        'Public Sub TCS_IN_CTU_LASER(ByVal key As KeyCTu, Optional ByVal bInTrucTiep As Boolean = False)
'        '    Try
'        '        Dim ds As DataSet = buChungTu.CTU_IN_LASER(key)
'        '        Dim hdr As infChungTuHDR = buChungTu.CTU_Header(key)

'        '        gLoaiBaoCao = Report_Type.TCS_CTU_LASER

'        '        Dim frm As New frmShowReports
'        '        frm.TCS_DS = ds

'        '        frm.KHCT = hdr.KyHieu_CT
'        '        frm.SoCT = ParmValues_Load("MA_CN_NH") & " - " & hdr.So_CT
'        '        frm.Ten_NNThue = hdr.Ten_NNThue

'        '        ' Mã số thuế NNT: Mã NNT
'        '        If Exists_VangLai(hdr.Ma_NNThue) Then  ' Neu la ma DTNT dac biet se khong in ra GNT
'        '            frm.Ma_NNThue = ""
'        '        Else ' Nguoc lai van in binh thuong
'        '            frm.Ma_NNThue = hdr.Ma_NNThue
'        '        End If

'        '        frm.DC_NNThue = hdr.DC_NNThue
'        '        frm.Ten_NNTien = hdr.Ten_NNTien
'        '        frm.Ma_NNTien = hdr.Ma_NNTien
'        '        frm.DC_NNTien = hdr.DC_NNTien
'        '        frm.Huyen_NNTien = ""
'        '        frm.Tinh_NNTien = ""

'        '        frm.NHA = Get_TenNganHang(hdr.MA_NH_A)
'        '        frm.NHB = Get_TenNganHang(hdr.MA_NH_B)
'        '        If hdr.PT_TT.Trim() = "00" Then
'        '            frm.TKNo = ParmValues_Load("NH_TKTM")
'        '        Else
'        '            frm.TKNo = hdr.TK_KH_NH
'        '        End If
'        '        'frm.TKNo = strFormatTK(hdr.TK_No)
'        '        frm.TKCo = strFormatTK(hdr.TK_Co)
'        '        frm.Ma_Quy = buChungTu.CTU_ChiTiet(key)(0).MaQuy()
'        '        frm.TK_GL_NH = Get_TK_GL_NH(hdr.Ma_Xa.Trim())

'        '        frm.NgayNH = hdr.NGAY_KH_NH

'        '        If hdr.Lan_In = "0" Then
'        '            frm.Copy = "Bản Chính"
'        '        Else
'        '            frm.Copy = "Bản Sao"
'        '        End If

'        '        frm.Ten_KB = gstrTenKB
'        '        frm.Tinh_KB = gstrTenTinhTP
'        '        frm.Ma_CQThu = hdr.Ma_CQThu
'        '        frm.Ten_CQThu = Get_TenCQThu(hdr.Ma_CQThu)

'        '        frm.So_TK = hdr.So_TK
'        '        frm.Ngay_TK = hdr.Ngay_TK
'        '        frm.LHXNK = hdr.LH_XNK

'        '        frm.So_BK = ""
'        '        frm.Ngay_BK = ""
'        '        frm.Tien_Bang_Chu = TCS_Dich_So(hdr.TTien, "đồng")
'        '        frm.SHKB = hdr.SHKB
'        '        frm.DBHC = hdr.Ma_Xa

'        '        frm.Ten_ThuQuy = Get_HoTenNV(hdr.Ma_KS)
'        '        frm.Ten_KeToan = Get_HoTenNV(hdr.Ma_NV)
'        '        frm.Ten_KTT = gstrTen_KTT

'        '        If hdr.PT_TT.Trim() = "00" Then
'        '            'Tiền Mặt
'        '            frm.Key_MauCT = "0"
'        '        Else
'        '            'Chuyển khoản
'        '            frm.Key_MauCT = "1"
'        '        End If
'        '        Dim intKieuCT, intKieuMH As Integer
'        '        Get_Map_TK_Form(hdr.TK_No, hdr.TK_Co, intKieuMH, intKieuCT)
'        '        'Xét xem có phải là chứng từ tạm thu hay không
'        '        If (IsTamThu(intKieuCT)) Then
'        '            frm.IsTamThu = "1"
'        '        Else
'        '            frm.IsTamThu = "0"
'        '        End If

'        '        'Kiểm tra xem in trực tiếp hay hiển thị in
'        '        If bInTrucTiep = True Then
'        '            'In Trực tiếp ra máy in
'        '            Dim i As Integer = 0
'        '            For i = 1 To gintSoLienInCT
'        '                frm.PrintToPrinter()
'        '            Next
'        '        Else
'        '            'Hiển thị ra màn hình trước khi in
'        '            frm.Show()
'        '        End If
'        '    Catch ex As Exception
'        '        Throw (ex)
'        '    End Try
'        'End Sub
'#End Region

'#Region "In chứng từ phục hồi"
'        'Public Sub TCS_CTU_PHUCHOI(ByVal strKHSoCT As String, ByVal blnChuyenKhoan As Boolean)
'        '    '-------------------------------------------------------------------------------------
'        '    ' Mục đích: In các chứng từ phục hồi chuyển khoản hoặc tiền mặt
'        '    ' Đầu vào: Một dãy các KHCT ghép với SoosCT, phân cách bằng dấu phẩy (,)
'        '    ' Người viết: Lê Hồng Hà
'        '    ' Ngày viết: 15/05/2008
'        '    '--------------------------------------------------------------------------------------

'        '    Try
'        '        If (blnChuyenKhoan) Then
'        '            gLoaiBaoCao = Report_Type.CTU_PHUCHOI_CK
'        '        Else
'        '            gLoaiBaoCao = Report_Type.CTU_PHUCHOI_TM
'        '        End If

'        '        Dim frm As New BaoCao.frmShowReports

'        '        frm.TCS_Ten_KB = gstrTenKB

'        '        Dim ds As DataSet
'        '        ds = buBaoCao.CTU_PhucHoi_Chitiet(strKHSoCT)
'        '        ds.Tables(0).TableName = "TCS_CTU"

'        '        Dim i As Integer
'        '        For i = 0 To ds.Tables(0).Rows.Count - 1
'        '            ds.Tables(0).Rows(i).Item("Tien") = TCS_Dich_So(StoN(ds.Tables(0).Rows(i).Item("Tien").ToString()), "đồng")
'        '        Next

'        '        frm.TCS_DS = ds

'        '        frm.Show()
'        '    Catch ex As Exception
'        '        Throw ex
'        '    End Try
'        'End Sub
'#End Region

'        Public Function GetKieuCT(ByVal intKieuCT As Integer) As CTEnum
'            Try
'                'Kiểm tra chuyển khoản tại kho bạc hay ngân hàng?
'                Select Case intKieuCT
'                    Case 2, 5, 8, 11, 14, 17, 22, 25, 28  'Chuyển khoản tại Ngân hàng
'                        Return CTEnum.CK_NganHang
'                    Case 3, 6, 9, 12, 15, 18, 23, 26, 29   'Chuyển khoản tại Kho Bạc
'                        Return CTEnum.CK_KhoBac
'                End Select

'                Return CTEnum.TienMat
'            Catch ex As Exception
'                Throw ex
'            End Try
'        End Function


'    End Module
'End Namespace