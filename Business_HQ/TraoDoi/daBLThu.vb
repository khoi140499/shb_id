Imports VBOracleLib

Namespace BLThu
    Public Class daBLTHu
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function LoadData(ByVal strWhere As String) As Object
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Dim strSQL As String

            Try
                strSQL = "SELECT KYHIEU_BL, SO_BL, TEN_NNTIEN, DC_NNTIEN, " & _
                        "SUBSTR(NGAY_CT, 7, 2) || '/' || SUBSTR (NGAY_CT, 5, 2) || '/' || SUBSTR (NGAY_CT, 1, 4) NGAY_CT, " & _
                        "LH_ID, LY_DO, TTIEN " & _
                        "FROM TCS_CTBL " & _
                        "WHERE " & strWhere & _
                        " ORDER BY so_bl ASC"
                cnChungTu = New DataAccess
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách header của chứng từ từ CSDL")
                Throw ex
                'LogDebug.WriteLog("Có lỗi sảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try

            Return dsChungTu
        End Function

    End Class
End Namespace
