Namespace TDDMDC
    Public Class infNguyenTe
        Private strMa_NT As String
        Private strTen As String

        Sub New()
        End Sub

        Sub New(ByVal drwNguyenTe As DataRow)
            strMa_NT = drwNguyenTe("Ma_NT").ToString().Trim()
            strTen = drwNguyenTe("Ten").ToString().Trim()
        End Sub

        Public Property Ma_NT() As String
            Get
                Return strMa_NT
            End Get
            Set(ByVal Value As String)
                strMa_NT = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property

    End Class
End Namespace
