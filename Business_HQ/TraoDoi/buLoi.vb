Imports VBOracleLib
Namespace DMDC.Loi
    Public Class buLoi
        Private strTNSCode As String

        Sub New(ByVal strTmpTNSCode As String)
            '-----------------------------------------------------------------
            ' Mục đích: Khởi tạo lỗi theo TNS_CODE
            ' Tham số: 
            ' Giá trị trả về: 
            ' Ngày viết: 22/10/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            strTNSCode = strTmpTNSCode
        End Sub

        Public Function Title() As String
            Dim tmpTitle As String
            Select Case strTNSCode
                Case "01"
                    tmpTitle = " Cấp-Chương"
                Case "02"
                    tmpTitle = " Loại-Khoản"
                Case "03"
                    tmpTitle = " Mục-Tiểu mục"
                Case "04"
                    tmpTitle = " Nguyên tệ"
                Case "05"
                    tmpTitle = " Địa bàn hành chính"
                Case "09"
                    tmpTitle = " NNT"
                Case "62"
                    tmpTitle = "  Thông báo lỗi chi tiết khi nhận chứng từ"
                    Return tmpTitle
                Case "21"
                    tmpTitle = "  Thông báo lỗi chi tiết khi nhận sổ thuế"
                    Return tmpTitle
                Case "25"
                    tmpTitle = "  Thông báo lỗi chi tiết khi nhận danh sách tờ khai"
                    Return tmpTitle
                Case Else
                    tmpTitle = ""
            End Select
            tmpTitle = "  Thông báo lỗi chi tiết khi nhận danh mục" & tmpTitle
            Return tmpTitle
        End Function

        Public Shared Function Load(ByVal strImExportID As String, ByVal strTSNCode As String) As DataSet
            Dim objLoi As New daLoi
            Return objLoi.Load(strImExportID, strTSNCode)
        End Function

        Public Shared Sub InsertErr(ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, ByVal fstrErrCode As String)
            '-----------------------------------------------------------------
            ' Mục đích: Đưa lỗi vào CSDL trong quá trình thao tác trao đổi
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objLoi As New daLoi
            objLoi.Insert(fstrId_ImEx, fstrId_Mess, fstrErrCode)
        End Sub
    End Class
End Namespace
