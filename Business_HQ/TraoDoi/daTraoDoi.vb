Imports VBOracleLib
Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.Common.mdlCommon
'Imports TraoDoi.DMDC.Loi

Public Class daTraoDoi
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    'Public Sub ExportPkg(ByVal fstrTsn_Code As String, ByVal fstrLcn_Recv As String, _
    '     ByVal fstrWhere As String, ByVal strLcn_Owner As String, ByRef fintTongso As Int64, ByRef fintSoloi As Int64)
    '    '---------------------------------------------------------------------------------
    '    ' Mục đích: gọi thủ tục export viết bằng package để kết xuất dữ liệu cho cqthu
    '    ' Đầu vào : fstrTsn_code - Mã giao dịch
    '    '           fstrLcn_Recv - Nơi nhận
    '    '           fstrWhere    - Đk
    '    '           fintTongso   - Tổng số thành công
    '    '           fintSoloi    - Số lỗi
    '    ' Người viết: HoanNH
    '    ' Ngày viết: 28/12/2007
    '    '-------------------------------------------------------------------------------

    '    Dim cnTD As DataAccess
    '    Dim strSql As String
    '    Dim p As IDbDataParameter
    '    Dim arrIn As IDataParameter()
    '    Dim arrOut As ArrayList
    '    Try
    '        cnTD = New DataAccess
    '        ReDim arrIn(6)
    '        p = cnTD.GetParameter("p_tsn_code", ParameterDirection.Input, fstrTsn_Code, DbType.String)
    '        p.Size = 50
    '        arrIn(0) = p
    '        p = cnTD.GetParameter("p_nsd_name", ParameterDirection.Input, gstrTenND, DbType.String)
    '        p.Size = 50
    '        arrIn(1) = p
    '        p = cnTD.GetParameter("p_lcn_owner", ParameterDirection.Input, strLcn_Owner, DbType.String)
    '        p.Size = 50
    '        arrIn(2) = p
    '        p = cnTD.GetParameter("p_noi_nhan", ParameterDirection.Input, fstrLcn_Recv, DbType.String)
    '        p.Size = 50
    '        arrIn(3) = p
    '        p = cnTD.GetParameter("p_where", ParameterDirection.Input, fstrWhere, DbType.String)
    '        p.Size = 5000
    '        arrIn(4) = p
    '        p = cnTD.GetParameter("p_sobggui", ParameterDirection.Output, Nothing, DbType.Int64)
    '        p.Size = 10
    '        arrIn(5) = p
    '        p = cnTD.GetParameter("p_sobgloi", ParameterDirection.Output, Nothing, DbType.Int64)
    '        p.Size = 10
    '        arrIn(6) = p
    '        strSql = "EXC_PKG_IMEXPORT.Prc_export_data_tcs"

    '        arrOut = cnTD.ExecuteNonQuery(strSql, CommandType.StoredProcedure, arrIn)
    '        If IsNumeric(arrOut.Item(0).ToString()) Then
    '            fintTongso = CLng(arrOut.Item(0).ToString().Trim)
    '        End If
    '        If IsNumeric(arrOut.Item(1).ToString()) Then
    '            fintSoloi = CLng(arrOut.Item(1).ToString().Trim)
    '        End If
    '    Catch ex As Exception
    '        'LogDebug.Writelog("Lỗi trong quá trình kết xuất chứng từ cho cơ quan thu: " & ex.ToString)
    '        Throw ex
    '    Finally
    '        If Not cnTD Is Nothing Then cnTD.Dispose()
    '    End Try
    'End Sub
    Public Function GoiDL(ByVal strTSNCode As String, ByVal strWhere As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách các gói dữ liệu đang chờ nhận
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim strSQL As String
        Dim cnDataPkg As DataAccess
        Dim dsDataPkg As DataSet

        Try
            cnDataPkg = New DataAccess
            strSQL = "SELECT data_pkg.ID, data_pkg.crea_date, data_pkg.tran_num, " & _
                     "tran_lst.tsn_name, data_pkg.tsn_code, loca_lst.lcn_name " & _
                     "FROM tran_lst, data_pkg, loca_lst " & _
                     "WHERE (tran_lst.tsn_code = data_pkg.tsn_code) " & _
                     "and (data_pkg.lcn_send = loca_lst.lcn_code) " & _
                     "And (data_pkg.pkg_type='1') AND (data_pkg.TRAN_NUM <> data_pkg.RCV_NUM) " & _
                     "AND (tran_lst.tsn_code = '" & strTSNCode & "') " & strWhere & _
                     " AND " & WhereLcn_Recv(strTSNCode) & _
                     " ORDER BY crea_date DESC"
            dsDataPkg = cnDataPkg.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin các gói dữ liệu từ CSDL")
            'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin các gói dữ liệu từ CSDL: " & ex.ToString)
            Throw ex
        Finally
            If Not cnDataPkg Is Nothing Then cnDataPkg.Dispose()
        End Try

        Return dsDataPkg
    End Function
    Public Function LoadData(ByVal strTSNCode As String, ByVal strPkgID As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách các bản ghi của một gói dữ liệu
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' Nguời sửa: Nguyễn Bá Cường
        ' Thêm điều kiện check điều kiện nơi gửi
        ' ----------------------------------------------------------------
        Dim cnDMDC As DataAccess
        Dim drData As IDataReader
        Dim strResult As String = ""
        Dim dsDMDC As DataSet
        Dim strSQL As String = "SELECT fld_code, fld_name, val_type, xtg_name, map_form, val_fmt, val_len, tab_name " & _
            "FROM tab_lst tab, fld_lst fld, xml_tag xml " & _
            "WHERE(fld.tbl_code = TAB.tab_code) AND (FLD.fld_tn ='Y') " & _
            "AND tab.tsn_code = '" & strTSNCode & "' " & _
            "AND fld.xtg_code = xml.xtg_code " & _
            "ORDER BY fld.fld_order"
        Try
            cnDMDC = New DataAccess
            drData = cnDMDC.ExecuteDataReader(strSQL, CommandType.Text)
            Do While drData.Read
                If strResult <> "" Then strResult = strResult & ", "
                strResult = strResult & drData.GetValue(4)
            Loop
            drData.Close()
            If strResult <> "" Then
                strResult = "Select " & strResult & " From TRAN_UP, MESS_UP " & _
                "Where " & IIf(strPkgID.Trim = "", "", "(TRAN_UP.PKG_ID = '" & strPkgID & "') And ") & _
                "(TRAN_UP.Tran_No = MESS_UP.Tran_No) And (TRAN_UP.Tsn_Code='" & strTSNCode & "') And" & _
                "((TRAN_UP.Sts_Code='" & gstrSSN & "') Or " & _
                "((TRAN_UP.Sts_Code = '" & gstrDN & "') And " & _
                "(TRAN_UP.Resu_Code = '01'))) And " & WhereLcn_Recv(strTSNCode) & _
                " Order by Tran_up.Tran_no "
                dsDMDC = cnDMDC.ExecuteReturnDataSet(strResult, CommandType.Text)
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin các gói dữ liệu từ CSDL")
            'LogDebug.Writelog("Có lỗi xảy ra khi xây dựng câu lệnh SQL lấy danh mục dùng chung: " & ex.ToString)
            Throw ex
        Finally
            If Not drData Is Nothing Then drData.Close()
            If Not cnDMDC Is Nothing Then cnDMDC.Dispose()
        End Try

        Return dsDMDC
    End Function
    Private Sub UpdateTransUp(ByVal strSTS_CODE As String, ByVal strRESU_CODE As String, ByVal strTRAN_NO As String)
        '-----------------------------------------------------------------
        ' Mục đích: Đưa lỗi vào CSDL trong quá trình thao tác truyền tin
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim cUpdate As DataAccess
        Try
            cUpdate = New DataAccess
            Dim strSQL = "Update TRAN_UP Set STS_CODE = '" & strSTS_CODE & "',RESU_CODE = '" & strRESU_CODE & "'" & _
                    " Where TRAN_NO = '" & strTRAN_NO & "'"
            cUpdate.ExecuteNonQuery(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật lại trạng thái của bảng TransUp")
            'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật lại trạng thái của bảng TransUp: " & ex.ToString)
            Throw ex
        Finally
            If Not cUpdate Is Nothing Then cUpdate.Dispose()
        End Try
    End Sub

    Public Sub Import(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
        ByVal fstrWhere As String, ByRef fintTongso As Integer, _
            ByRef fintSoloi As Integer, ByRef fstrImExportKey As String)
        '-----------------------------------------------------------------
        ' Mục đích: Thủ tục Import lấy dữ liệu trong CSDL trung gian đổ vào
        '           CSDL tác nghiệp
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 12/02/2004
        ' Người viết: Nguyễn Quốc Việt
        ' Hạn chế:  Thủ tục này sẽ bị hạn chế nếu CSDL tác nghiệp có quan hệ
        '           Master+Detail song nằm trong cùng một bảng và tham số 
        '           Is_Master = 'N' trong bảng Tab_Lst
        'Người sửa: Nguyễn Bá Cường/ Ngô Minh Trọng
        'Thêm điều kiện: nơi gửi/ Đưa hàm ghi lỗi; Hàm cập nhật trạng thái
        ' ----------------------------------------------------------------
        Dim cSelect As DataAccess
        Dim ketnoiToInsert As DataAccess
        Dim ketnoiToUpdate As DataAccess
        Dim tranInsert As IDbTransaction
        Dim drParalist As IDataReader
        Dim drDatalist As IDataReader
        Dim arrFldIndex As New ArrayList
        Dim arrTblName As New ArrayList
        Dim arrIsMaster As New ArrayList
        Dim arrFldname As New ArrayList
        Dim arrFldtype As New ArrayList
        Dim strMessmapString As String = ""
        Dim strTblname As String
        Dim strIsMaster As String
        Dim strNewTblCode As String
        Dim strFldNameList As String
        Dim strOldTblCode As String = " " ' Một ký tự trống
        Dim strSQL As String
        Dim intNumFld As Integer = 0
        Dim strTmpID As String ' = getID(fstrTsn_Code)

        ' Sinh khoá cho bảng ImExport
        fstrImExportKey = getIMEXKey("EXC_IMEXPORT_SEQ")

        ' Hình thành câu lệnh select dữ liệu từ các bảng tham số để lấy ra:
        ' tên bảng, tên trường, danh sách mess_map,...
        strSQL = "SELECT  tab_code,tab_name,is_master,fld_name, map_form,fld_type,tab_alias " _
                & "FROM tran_lst, tab_lst, fld_lst " _
                & "WHERE  (TRAN_LST.tsn_code ='" & fstrTsn_Code _
                & "') And (TRAN_LST.tsn_code = TAB_LST.tsn_code) And " _
                & "(TRAN_LST.tsn_type='N') And (FLD_LST.fld_tn ='Y') " _
                & "And (TAB_LST.tab_code = FLD_LST.tbl_code) " _
                & "ORDER BY TAB_LST.tab_order,FLD_LST.fld_order"
        Try
            ketnoiToInsert = New DataAccess
            ketnoiToUpdate = New DataAccess
            cSelect = New DataAccess
            drParalist = cSelect.ExecuteDataReader(strSQL, CommandType.Text)
            While drParalist.Read
                strNewTblCode = drParalist.GetValue(0)
                If strNewTblCode <> strOldTblCode And intNumFld > 0 Then
                    arrTblName.Add(strTblname)
                    strFldNameList = strFldNameList.Substring(0, strFldNameList.Length - 1)
                    arrFldname.Add(strFldNameList)
                    arrFldIndex.Add(intNumFld)
                    arrIsMaster.Add(strIsMaster)
                    strFldNameList = ""
                End If
                strMessmapString &= drParalist.GetValue(4) & "," 'mess_map
                strTblname = drParalist.GetValue(1)
                strFldNameList &= drParalist.GetValue(3) & ","
                arrFldtype.Add(drParalist.GetValue(5))
                If Not drParalist.IsDBNull(2) Then
                    strIsMaster = drParalist.GetValue(2) 'Is_master
                Else
                    strIsMaster = "" 'Is_Detail
                End If
                strOldTblCode = strNewTblCode
                intNumFld += 1
            End While
            drParalist.Close()
            drParalist = Nothing
            ' Insert vào phần tử cuối cùng lưu chỉ số của trường cuối cùng
            arrTblName.Add(strTblname)
            strFldNameList = strFldNameList.Substring(0, strFldNameList.Length - 1)
            arrFldname.Add(strFldNameList)
            arrFldIndex.Add(intNumFld)
            arrIsMaster.Add(strIsMaster)
            strMessmapString = strMessmapString.Substring(0, strMessmapString.Length - 1)
            ' Insert một bản ghi vào trong bảng ImExport trước khi đổ dữ liệu từ
            ' CSLD trung gian(Tran_up, Mess_up) vào CSDL tác nghiệp
            strSQL = "Insert into ImExport(ID,TSN_CODE,IEP_TYPE,IEP_DATE,IEP_USER,IEP_ROWNO,IEP_ERRNO)" _
                    & "  VALUES('" & fstrImExportKey & "','" & fstrTsn_Code & "' ,'N'," & ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") & ",'" _
                    & gstrTenND & "',0,0)"
            ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text)
            ' Bắt đầu đọc dữ liệu từ CSDL trung gian(Tran_up,Mess_up) để đổ vào bảng tác nghiệp (bao gồm dữ liệu không lỗi)
            Dim strOldTranNo As String = ""
            Dim strNewTranNo As String
            Dim blnUpdate As Boolean
            Dim strIdMess_Up As String
            'Cuongnb sửa            
            strSQL = "SELECT Mess_Up.Tran_no,Mess_Up.Id," & strMessmapString _
                                & "  FROM Tran_Up,Mess_up " _
                                & "  WHERE (Tran_Up.Tsn_code='" & fstrTsn_Code & "') And" _
                                & "((Tran_Up.Sts_Code='" & gstrSSN & "') Or " _
                                & "((Tran_Up.Sts_Code = '" & gstrDN & "') And (Tran_Up.Resu_Code = '01'))) And " _
                                & " (Tran_up.Tran_no = Mess_up.Tran_no) And " & WhereLcn_Recv(fstrTsn_Code) _
                                & " And " & fstrWhere & " " _
                                & " ORDER BY Mess_up.Tran_no"
            drDatalist = cSelect.ExecuteDataReader(strSQL, CommandType.Text)
            ' Khởi tạo bắt đầu giao dịch
            tranInsert = ketnoiToInsert.BeginTransaction
            While drDatalist.Read
                Dim intFldiIndex As Integer = 0
                Dim intTblIndex As Integer = 0
                Dim arrValues As New ArrayList
                ' Lấy dữ liệu từ các trường của bản ghi vừa đọc và lần lựơt Insert vào
                ' Các bảng nhận theo thứ tự danh sách các bảng lưu ở trên
                Try
                    While intTblIndex <= arrTblName.Count - 1
                        Dim strSQLInsString As String
                        Dim strValueString As String = ""
                        While intFldiIndex <= arrFldIndex.Item(intTblIndex) - 1
                            Dim strFldType As String = arrFldtype.Item(intFldiIndex)
                            If Not drDatalist.IsDBNull(intFldiIndex + 2) Then
                                Select Case UCase(strFldType)
                                    Case "VARCHAR2"
                                        strValueString &= "'" & drDatalist.GetValue(intFldiIndex + 2) & "',"
                                    Case "NUMBER"
                                        strValueString &= "" & drDatalist.GetValue(intFldiIndex + 2) & ","
                                    Case "DATE"
                                        ' Vì danh mục MLNS để trường ngày là number nên phải convert
                                        If "01,02,03".IndexOf(fstrTsn_Code) >= 0 Then
                                            strValueString &= drDatalist.GetDateTime(intFldiIndex + 2).ToString("yyyyMMdd") & ","
                                        Else
                                            strValueString &= ToValue(drDatalist.GetDateTime(intFldiIndex + 2).ToString("dd/MM/yyyy HH:mm:ss"), "DATE") & ","
                                        End If
                                End Select
                            Else
                                strValueString &= "Null" & ","
                            End If
                            ' Lấy giá trị cua cac truong để kiểm tra
                            arrValues.Add(drDatalist.GetValue(intFldiIndex + 2))
                            intFldiIndex += 1
                        End While
                        strValueString = strValueString.Substring(0, strValueString.Length - 1)
                        strFldNameList = arrFldname.Item(intTblIndex)
                        strTmpID = getID(arrTblName.Item(intTblIndex))
                        If strTmpID <> "" Then
                            strSQLInsString = "INSERT INTO  " & arrTblName.Item(intTblIndex) & "(" & strTmpID & "," _
                                                            & strFldNameList & ") VALUES(" & getDataKey("TCS_" & strTmpID & "_SEQ.NEXTVAL") & "," & strValueString & ")"
                        Else
                            strSQLInsString = "INSERT INTO  " & arrTblName.Item(intTblIndex) & "(" _
                                                            & strFldNameList & ") VALUES(" & strValueString & ")"
                        End If
                        strIsMaster = arrIsMaster.Item(intTblIndex)
                        If strIsMaster.ToUpper = "Y" Then
                            strNewTranNo = drDatalist.GetValue(0)
                            If strNewTranNo <> strOldTranNo Then
                                gblnLoiDetail = False ' Bắt đầu một giao dịch khởi tạo trạng thái không bị lỗi
                                blnUpdate = False ' Trạng thái cập nhât khi trùng khoá (hiện tại không cập nhật)
                                ' Cập nhật tổng số bản ghi Insert và lỗi
                                strSQL = "Update IMEXPORT Set IEP_ROWNO = " & fintTongso & "," & _
                                        "IEP_ERRNO = " & fintSoloi & " Where ID = '" & fstrImExportKey & "'"
                                ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, tranInsert)
                                ' Commit dữ liệu khi chuyển sang dòng Master+Detail mới
                                tranInsert.Commit()
                                ' Bắt đầu theo dõi quá trình cập nhật
                                tranInsert = ketnoiToInsert.BeginTransaction
                                strIdMess_Up = drDatalist.GetValue(1)
                                If CheckValues(fstrTsn_Code, fblnGhide, "Y", arrValues, fstrImExportKey, strIdMess_Up, strNewTranNo, blnUpdate, ketnoiToInsert, tranInsert) Then
                                    ketnoiToInsert.ExecuteNonQuery(strSQLInsString, CommandType.Text, tranInsert)
                                    fintTongso += 1
                                Else
                                    If blnUpdate Then
                                        ' Khong duoc Insert va blnUpdate = True thi so loi khong duoc tang len va so ban ghi
                                        ' da nhan vao phai tang len.
                                        fintTongso += 1
                                    Else
                                        ' Truong hop bi loi va khong phai do cap nhat(blnUpdate=False). Tong so loi tang len
                                        fintSoloi += 1
                                        ' Neu Master bi loi khong duoc phep Insert du lieu vao bang Detail
                                        ' mac du kiem tra Bảng Detail không có lỗi
                                        gblnLoiDetail = True
                                    End If
                                End If
                                strOldTranNo = strNewTranNo
                            End If
                        Else
                            ' Neu khong phai la bang master thi khong can so sanh Tran_No, moi ban ghi trong 
                            ' Mess_up se duoc tuong ung voi mot ban ghi trong bang tac nghiep
                            strIdMess_Up = drDatalist.GetValue(1)
                            If CheckValues(fstrTsn_Code, fblnGhide, "N", arrValues, fstrImExportKey, strIdMess_Up, strNewTranNo, blnUpdate, ketnoiToInsert, tranInsert) Then
                                If Not gblnLoiDetail Then
                                    ' Co loi Detail tu truoc do do khi Detail tiep theo khong bi loi
                                    ' thi cung khong duoc Insert vao tiep nua
                                    ketnoiToInsert.ExecuteNonQuery(strSQLInsString, CommandType.Text, tranInsert)
                                End If
                            Else
                                If Not tranInsert Is Nothing Then
                                    ' Doan lenh nay chi thuc hien khi gap mot Detail loi dau tien
                                    tranInsert.Rollback()
                                    tranInsert = ketnoiToInsert.BeginTransaction
                                    fintTongso -= 1
                                    fintSoloi += 1
                                End If
                            End If
                        End If
                        intTblIndex += 1
                        strValueString = ""
                        strFldNameList = ""
                    End While
                Catch ex As Exception
                    If Not tranInsert Is Nothing Then
                        tranInsert.Rollback()
                        tranInsert.Dispose()
                        tranInsert = Nothing
                        fintTongso -= 1
                        fintSoloi += 1
                        ' Cập nhật vào bảng ImExport tổng số bản ghi đã kết xuất
                        strSQL = "Update IMEXPORT Set IEP_ROWNO = " & fintTongso & "," & _
                                "IEP_ERRNO = " & fintSoloi & " Where ID = '" & fstrImExportKey & "'"
                        ketnoiToUpdate.ExecuteNonQuery(strSQL, CommandType.Text)
                        ' TDTT-3333 - Lỗi không xác định
                        ''buLoi.InsertErr(fstrImExportKey, strIdMess_Up, "TDTT-3333")
                    End If
                Finally
                End Try
            End While
            drDatalist.Close()
            drDatalist = Nothing
            ' Truong hop cuoi cung phai commit cho ca Master+Detail cuoi cung
            If Not tranInsert Is Nothing Then
                tranInsert.Commit()
            End If
            ' Cap nhat tong so ban ghi Insert va loi
            strSQL = "Update IMEXPORT Set IEP_ROWNO = " & fintTongso & "," & _
                    "IEP_ERRNO = " & fintSoloi & " Where ID = '" & fstrImExportKey & "'"
            ketnoiToUpdate.ExecuteNonQuery(strSQL, CommandType.Text)
            ' Cập nhật vào data_pkg số bản ghi đã nhận vào tác nghiệp
            If fintSoloi = 0 Then
                strSQL = "Update Data_pkg Set RCV_NUM=Tran_num where id in (Select distinct pkg_id From Tran_up " & _
                         "WHERE Tran_Up.Tsn_code='" & fstrTsn_Code & "' And " & _
                         "Tran_Up.Sts_Code = '" & gstrDN & "' And Tran_Up.Resu_Code = '00' " & _
                         "And " & WhereLcn_Recv(fstrTsn_Code) & _
                         " And " & fstrWhere & " )"
                ketnoiToUpdate.ExecuteNonQuery(strSQL, CommandType.Text)
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra trong quá trình import dữ liệu")
            'LogDebug.Writelog("Lỗi trong quá trình import dữ liệu: " & ex.ToString)
            fintTongso = 0
            fintSoloi = 999999999 ' ba so 9
            ' Cập nhật vào bảng ImExport tổng số bản ghi đã kết xuất
            strSQL = "Update IMEXPORT Set IEP_ROWNO =" & fintTongso & ", IEP_ERRNO =" & fintSoloi & ",IEP_ERR = '" & _
                    ChuanHoaXau(ex.ToString, 255) & "' Where ID = '" & fstrImExportKey & "'"
            ketnoiToUpdate.ExecuteNonQuery(strSQL, CommandType.Text)
        Finally
            If Not drDatalist Is Nothing Then
                drDatalist.Close()
            End If
            If Not cSelect Is Nothing Then
                cSelect.Dispose()
            End If
            If Not tranInsert Is Nothing Then
                tranInsert.Dispose()
            End If
            If Not ketnoiToUpdate Is Nothing Then
                ketnoiToUpdate.Dispose()
            End If
            If Not ketnoiToInsert Is Nothing Then
                ketnoiToInsert.Dispose()
            End If
        End Try
    End Sub

    Public Sub ExportData(ByVal pv_lngNgayKS As Long)
        Dim cnTD As DataAccess
        Dim strSql As String
        Dim p As IDbDataParameter
        Dim arrIn As IDataParameter()
        Dim arrOut As ArrayList

        Try
            cnTD = New DataAccess
            ReDim arrIn(0)

            p = cnTD.GetParameter("p_ngayks", ParameterDirection.Input, pv_lngNgayKS, DbType.Double)
            p.Size = 10
            arrIn(0) = p

            'p = cnTD.GetParameter("p_lstdiemthu", ParameterDirection.Input, pv_lngNgayKS, DbType.String)
            'p.Size = 200
            'arrIn(1) = p

            strSql = "EXC_PKG_IMEXPORT.prc_ketxuat_cqthu"
            arrOut = cnTD.ExecuteNonQuery(strSql, CommandType.StoredProcedure, arrIn)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi export dữ liệu")
            Throw ex
        Finally
            If Not cnTD Is Nothing Then cnTD.Dispose()
        End Try
    End Sub

    Public Sub Export(ByVal fstrTsn_Code As String, ByVal fstrLcn_Recv As String, _
        ByVal fstrWhere As String, ByRef fintTongso As Integer, ByRef fintSoloi As Integer)
        '-----------------------------------------------------------------
        ' Mục đích: Thủ tục Export lấy dữ liệu trong CSDL tác nghiệp đổ ra
        '           CSDL trung gian (tran_up,mess_up,data_pkg)
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 12/02/2004
        ' Người viết: Nguyễn Quốc Việt
        ' ----------------------------------------------------------------
        Dim cSelect As New DataAccess
        Dim ketnoiToInsert As New DataAccess
        Dim tranInsert As IDbTransaction
        Dim dParalist As IDataReader
        Dim dDatalist As IDataReader
        Dim arrKeyCode As New ArrayList
        Dim arrFldtypelist As New ArrayList
        Dim strFieldString As String = ""
        Dim strValueString As String = ""
        Dim strTableString As String = ""
        Dim strWhereString As String = ""
        Dim strMessmapString As String = ""
        Dim strFldOrder As String = ""
        Dim strNewTblcode As String
        Dim strOldTblcode As String = " " ' một ký tự trống
        Dim strImExportKey As String = Common.mdlCommon.getIMEXKey("EXC_IMEXPORT_SEQ")
        Dim strSQL As String
        Dim intIndexField As Integer = 0

        'Hinh thanh cau lenh Select du lieu tu cac bang tham so de lay ra: ten bang,
        'ten truong,...
        strSQL = "SELECT  tab_code,tab_name,where_cls,is_master,fld_form," & _
                "mess_map,fld_key,fld_type,tab_alias " & _
                "FROM tran_lst,tab_lst,fld_lst " & _
                "WHERE (tran_lst.tsn_code ='" & fstrTsn_Code & "')  And " & _
                "(tran_lst.tsn_type='Y') And (tran_lst.tsn_code = tab_lst.tsn_code) " & _
                "And (tab_lst.tab_code = fld_lst.tbl_code) And (fld_lst.fld_tn='Y') " & _
                "ORDER BY tab_lst.tab_order,fld_lst.fld_order"
        Try
            dParalist = cSelect.ExecuteDataReader(strSQL, CommandType.Text)
            While dParalist.Read
                Dim strFieldkey As String
                Dim strIsmaster As String
                Dim strTblname As String
                '
                strNewTblcode = dParalist.GetValue(0).Trim
                If strNewTblcode <> strOldTblcode Then
                    '
                    strTblname = dParalist.GetValue(1) 'tab_name
                    strTblname = strTblname.Trim
                    'Neu tab_alias <>null thi ghep tab_alias vao voi tab_name
                    If Not dParalist.IsDBNull(8) Then
                        strTableString &= strTblname & "  " & dParalist.GetValue(8).trim & "," 'tab_alias
                    Else
                        strTableString &= strTblname & ","
                    End If
                    If Not dParalist.IsDBNull(2) AndAlso dParalist.GetValue(2).trim <> "" Then
                        strWhereString &= dParalist.GetValue(2).trim & " AND " 'where_cls
                    End If
                End If
                strFieldString &= dParalist.GetValue(4).trim & "," 'fld_form
                strMessmapString &= dParalist.GetValue(5).trim & "," 'mess_map
                'Dua vi tri cac truong key trong bang master vao mang arrKeyCode
                If Not dParalist.IsDBNull(3) And Not dParalist.IsDBNull(6) Then
                    strIsmaster = dParalist.GetValue(3).trim 'is_master
                    strFieldkey = dParalist.GetValue(6).trim 'fld_key
                    If strIsmaster.ToUpper = "Y" And strFieldkey.ToUpper = "Y" Then
                        arrKeyCode.Add(intIndexField)
                        strFldOrder &= dParalist.GetValue(4).trim & ","
                    End If
                End If
                arrFldtypelist.Add(dParalist.GetValue(7).trim) 'fld_type
                strOldTblcode = strNewTblcode
                intIndexField += 1
            End While
            ' Cat ky tu thua "," va "AND" trong cac xau vua xay dung duoc 
            strTableString = strTableString.Substring(0, strTableString.Length - 1)
            strFieldString = strFieldString.Substring(0, strFieldString.Length - 1)
            strMessmapString = strMessmapString.Substring(0, strMessmapString.Length - 1)
            If strFldOrder.Trim <> "" Then
                strFldOrder = "Order by " & strFldOrder.Substring(0, strFldOrder.Length - 1)
            End If
            dParalist.Close()
            cSelect.Dispose()
            '
            Dim strDataPkgKey As String = gstrMaNoiLamViec & Common.mdlCommon.getIMEXKey("EXC_DATA_PKG_SEQ") ' ham lay gia tri khoa cho bang DATA_PKG
            Dim strOldKeyValue As String = " " 'một ký tự trống
            Dim strNewKeyValue As String = ""
            Dim strTran_No, strMessID As String
            ' Insert mot ban ghi vao trong bang ImExport truoc nham muc dich neu co say
            ' ra loi, ta co the ghi loi vao bang LOI_DL thi da co san ID cua bang IMEXPORT
            strSQL = " Insert into IMEXPORT(ID,TSN_CODE,IEP_TYPE,IEP_DATE,IEP_USER,IEP_ROWNO,IEP_ERRNO)" _
                    & "  VALUES('" & strImExportKey & "','" & fstrTsn_Code & "' ,'Y'," & ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") & _
                    ",'" & gstrTenND & "'," & fintTongso & ",0)"
            ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text)
            ' Bat dau theo doi qua trinh cap nhat cho cac bang: IMEXPORT,DATA_PKG,TRAN_UP,MESS_UP. Neu gap loi
            ' tat ca du lieu cua cac bang tren deu bi Roll back lai du lieu va ghi loi vao bang LOI_DL
            tranInsert = ketnoiToInsert.BeginTransaction
            ' Truoc khi doc du lieu tu bang that ta chen mot ban ghi vao bang Data_Pkg
            strSQL = "Insert Into DATA_PKG(ID,PARENT_ID,TSN_CODE,CREA_DATE,TRAN_NUM,ERR_NUM,LCN_SEND," & _
                     "LCN_RECV,PKG_TYPE,SEND_DATE,LCN_OWNER,CURR_STA,Exp_Date) VALUES(" & _
                     "'" & strDataPkgKey & "','" & strDataPkgKey & "','" & _
                     fstrTsn_Code & "'," & ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") & ",0,0,'" & _
                     gstrMaNoiLamViec & "','" & fstrLcn_Recv & "','0'," & _
                     ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") & ",'" & gstrMaNoiLamViec & "','00'," & _
                     ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") & ")"


            ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, tranInsert)
            'Hinh thanh cau lenh select du lieu tu bang that
            If strWhereString <> "" Then
                strWhereString = strWhereString.Substring(0, strWhereString.Length - 5)
                strSQL = "Select " & strFieldString & "  From  " _
                        & strTableString & "  Where  " & strWhereString & _
                        " And " & fstrWhere & " " & _
                        strFldOrder
            Else
                strSQL = "Select " & strFieldString & "  From  " _
                        & strTableString & " Where " & fstrWhere & " " & _
                        strFldOrder
            End If
            dDatalist = cSelect.ExecuteDataReader(strSQL, CommandType.Text)
            'Bat dau doc du lieu tu bang that
            While dDatalist.Read
                Dim intfieldnum As Integer
                intfieldnum = 0
                While intfieldnum <= arrKeyCode.Count - 1
                    strNewKeyValue &= dDatalist.GetValue(arrKeyCode.Item(intfieldnum))
                    intfieldnum += 1
                End While
                'Neu gia tri truong khoa thay doi thi Insert mot ban ghi vao trong bang tran_up   
                If strOldKeyValue <> strNewKeyValue Then
                    fintTongso += 1
                    ' Lay gia tri khoa cho bang TUP_EXC
                    strTran_No = gstrMaNoiLamViec & Common.mdlCommon.getIMEXKey("EXC_TRAN_UP_SEQ")
                    'Hinh thanh len cau lenh Insert vao TUP_EXC                   
                    strSQL = "INSERT INTO tup_exc(tran_no,parent,tsn_code,sts_code,sts_exc,lcn_owner," _
                            & " lcn_send,lcn_recV,pkg_ID,crea_date,resu_code,resu_name)" _
                            & "  VALUES('" & strTran_No & "','" & strTran_No & "','" & fstrTsn_Code _
                            & "','" & gstrTND & "','" & gstrTND & "','" & gstrMaNoiLamViec & "','" _
                            & gstrMaNoiLamViec & "','" & fstrLcn_Recv & "','" & strDataPkgKey & "'," & _
                            ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") & ",'00','')"
                    ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, tranInsert)
                End If
                intfieldnum = 0
                ' Doc du lieu tu bang that, ghep vao thanh mot xau cac gia tri sau do dung thu tu
                ' cac truong. Truoc khi ghep vao xau gia tri can dua vao ham ToValue de ep kieu cho dung
                While intfieldnum <= dDatalist.FieldCount - 1
                    Dim strFldType As String = arrFldtypelist.Item(intfieldnum).trim
                    If Not dDatalist.IsDBNull(intfieldnum) Then
                        Select Case UCase(strFldType)
                            Case "VARCHAR2"
                                strValueString &= "'" & dDatalist.GetValue(intfieldnum) & "',"
                            Case "NUMBER"
                                strValueString &= "" & dDatalist.GetValue(intfieldnum) & ","
                            Case "DATE"
                                strValueString &= "" & ToValue(dDatalist.GetDateTime(intfieldnum).ToString("dd/MM/yyyy HH:mm:ss"), "DATE") & ","
                        End Select
                    Else
                        strValueString &= "null" & ","
                    End If
                    intfieldnum += 1
                End While

                ' Lay khoa cho bang MESS_UP
                strMessID = gstrMaNoiLamViec & Common.mdlCommon.getIMEXKey("EXC_MESS_UP_SEQ")

                strValueString = strValueString.Substring(0, strValueString.Length - 1)
                'Hinh thanh len cau lenh Insert vao bang Mess_up
                strSQL = "INSERT INTO mup_exc(id,tran_no," _
                        & strMessmapString & ") VALUES('" & strMessID & "','" _
                        & strTran_No & "'," & strValueString & ")"
                ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, tranInsert)
                '
                strValueString = ""
                strOldKeyValue = strNewKeyValue
                strNewKeyValue = ""
            End While
            ' Cập nhật vào bảng Data_Pkg tổng số bản ghi đã kết xuất
            strSQL = "Update DATA_PKG Set TRAN_NUM = " & fintTongso & " Where ID = '" & strDataPkgKey & "'"
            ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, tranInsert)
            ' Cập nhật vào bảng ImExport tổng số bản ghi đã kết xuất
            strSQL = "Update IMEXPORT Set IEP_ROWNO = " & fintTongso & " Where ID = '" & strImExportKey & "'"
            ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, tranInsert)
            '
            dDatalist.Dispose()
            cSelect.Dispose()
            '
            tranInsert.Commit()
            tranInsert.Dispose()
            ketnoiToInsert.Dispose()
            '
        Catch ex As Exception
            If Not tranInsert Is Nothing Then
                tranInsert.Rollback()
                tranInsert.Dispose()
                Dim ketnoiToUpdate As New DataAccess
                fintTongso = 0
                fintSoloi = 999999999 'chin so 9
                ' Cập nhật vào bảng ImExport tổng số bản ghi đã kết xuất
                strSQL = "Update IMEXPORT Set IEP_ROWNO =" & fintTongso & ", IEP_ERRNO =" & fintSoloi & ",IEP_ERR = '" & _
                        ChuanHoaXau(ex.ToString, 255) & "' Where ID = '" & strImExportKey & "'"
                ketnoiToUpdate.ExecuteNonQuery(strSQL, CommandType.Text)
                ketnoiToUpdate.Dispose()
                'LogDebug.Writelog(strSQL & " - Lỗi trong quá trình kết xuất dữ liệu ! - " & ex.ToString)
            End If
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi kết xuất dữ liệu")
        End Try
    End Sub

    Private Function CheckValue_CapChuong(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
               ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
                   ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                       ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                           ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
        ' Tham số:
        ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
        '                   + False:Không cho phép Insert dữ liệu
        ' Người viết: Nguyễn Bá Cường
        ' Ngày viết: 10/11/2007
        ' Người Sửa: Ngô Minh Trọng: Kiểm tra ngày bắt đầu
        ' ----------------------------------------------------------------
        Dim blnTmpCheckValues As Boolean = True
        Dim strSQL As String
        If fstrIsMaster = "Y" Then ' Kiểm tra lỗi bảng Master
            ' Kiểm tra mã cấp
            If blnTmpCheckValues Then
                If farrTmp_Values(0) Is System.DBNull.Value Then
                    ' TDTT-3012 - Trường mã cấp không được để trống(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3012")
                    blnTmpCheckValues = False
                Else
                    If Len(farrTmp_Values(0)) <> 1 Then
                        ' TDTT-3000 - Mã cấp không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3000")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            ' Kiểm mã chương
            If blnTmpCheckValues Then
                If farrTmp_Values(1) Is System.DBNull.Value Then
                    ' TDTT-3013 - Trường mã chương không được để trống(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3013")
                    blnTmpCheckValues = False
                Else
                    If Len(farrTmp_Values(1)) <> 3 Then
                        ' TDTT-3001 - Mã chương không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3001")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            ' Kiểm tra tên gọi cấp chương
            If blnTmpCheckValues Then
                If farrTmp_Values(2) Is System.DBNull.Value Then
                    ' TDTT-3011 - Tên cấp chương không tồn tại
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3011")
                    blnTmpCheckValues = False
                End If
            End If

            ' Kiểm tra ngày bắt đầu
            If blnTmpCheckValues Then
                If farrTmp_Values(3) Is System.DBNull.Value Then
                    ' TDTT-3016 - Trường ngày bắt đầu không được để trống (null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3016")
                    blnTmpCheckValues = False
                Else
                    Dim tmpDate As String = ""
                    If Not IsDate(farrTmp_Values(3)) Then
                        ' TDTT-3017 - Ngày bắt đầu không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3017")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            ' Kiểm tra ngày kết thúc
            If blnTmpCheckValues Then
                If Not farrTmp_Values(4) Is System.DBNull.Value Then
                    If Not IsDate(farrTmp_Values(4)) Then
                        ' TDTT-3018 - Ngày kết thúc không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3018")
                        blnTmpCheckValues = False
                    Else
                        If farrTmp_Values(3) > farrTmp_Values(4) Then
                            ' TDTT-3019 - Ngày bắt đầu không được sau ngày kết thúc
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3019")
                            blnTmpCheckValues = False
                        End If
                    End If
                End If
            End If
        End If
        'Kiểm tra khoá cho bảng Master
        If blnTmpCheckValues Then  ' Không có lỗi chi tiết ở Master
            If fstrIsMaster = "Y" Then ' Nếu không lỗi kiểm tra trùng khoá
                Dim objCapChuong As New TDDMDC.infCapChuong
                objCapChuong.MA_CAP = farrTmp_Values(0)
                objCapChuong.MA_CHUONG = farrTmp_Values(1)
                Dim strTmpNayBD As String = ""
                Dim strTmpNayKT As String = ""
                strTmpNayBD = farrTmp_Values(3).Year & _
                                farrTmp_Values(3).Month.ToString.PadLeft(2, "0") & _
                                    farrTmp_Values(3).Day.ToString.PadLeft(2, "0")
                objCapChuong.NGAY_BD = strTmpNayBD
                If Not (farrTmp_Values(4) Is System.DBNull.Value) Then
                    objCapChuong.NGAY_KT = farrTmp_Values(4).Year & _
                                                farrTmp_Values(4).Month.ToString.PadLeft(2, "0") & _
                                                    farrTmp_Values(4).Day.ToString.PadLeft(2, "0")
                    strTmpNayKT = objCapChuong.NGAY_KT
                End If

                Dim tmpCapChuong As New TDDMDC.daCapChuong
                objCapChuong = tmpCapChuong.Load(objCapChuong)
                Dim cTonTai As New DataAccess
                Dim drTonTai As IDataReader
                strSQL = "Select * From TCS_DM_CAP_CHUONG Where (MA_CAP = '" & farrTmp_Values(0) & "') And (MA_CHUONG = '" & farrTmp_Values(1) & "')"
                drTonTai = cTonTai.ExecuteDataReader(strSQL, CommandType.Text)
                If objCapChuong.CCH_ID <> "" Then ' Tìm thấy khoá
                    If fblnGhide Then ' Cho phép ghi đè
                        strSQL = "Update TCS_DM_CAP_CHUONG Set TEN = " & oToString(farrTmp_Values(2), "C") & _
                           ",NGAY_BD  = " & strTmpNayBD & _
                          ",NGAY_KT=" & IIf(strTmpNayKT = "", "NULL", strTmpNayKT) & _
                        " WHERE CCH_ID=" & objCapChuong.CCH_ID
                        '",GHI_CHU=" & oToString(farrTmp_Values(5), "C") & _

                        fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)
                        blnTmpCheckValues = False ' Đã cập nhật rồi không cần Insert dữ liệu
                        fblnUpdate = True ' Dùng kết hợp với hàm CheckValues và không được tăng số lỗi lên
                        ' Cập nhật lại trạng thái của bảng Tran_Up
                        UpdateTransUp(gstrDN, "00", fstrTran_No)
                    Else ' Trùng khoa và không cho phép ghi đè
                        ' TDTT-3008 - Mã cấp chương đã tồn tại trong danh mục
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3008")
                        blnTmpCheckValues = False ' Bỏ qua không được Insert dữ liệu
                    End If
                Else ' Không bị trùng khoá, không lỗi và cho phép Insert dữ liệu
                    ' Cập nhật lại trạng thái của bảng Tran_Up
                    Dim cInsert As New DataAccess
                    strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                            "Where TRAN_NO = '" & fstrTran_No & "'"
                    cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                    cInsert.Dispose()
                End If
                drTonTai.Dispose()
                cTonTai.Dispose()
            Else ' Bảng Detail không có lỗi
                ' Giao dịch này không có bảng Detail
            End If
        Else ' Kiểm tra có lỗi chi tiết trong bảng Master hoặc Detail
            blnTmpCheckValues = False ' Không cho phép Insert dữ liệu
            ' Cập nhật lại trạng thái của bảng Tran_Up
            Dim cInsert As New DataAccess
            strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                    "Where TRAN_NO = '" & fstrTran_No & "'"
            cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
            cInsert.Dispose()
        End If
        Return blnTmpCheckValues
    End Function

    Private Function CheckValue_Loai_Khoan(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
               ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
                   ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                       ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                           ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
        ' Tham số:
        ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
        '                   + False:Không cho phép Insert dữ liệu
        ' Người viết: Nguyễn Bá Cường
        ' Ngày viết: 10/11/2007
        ' ----------------------------------------------------------------
        Dim blnTmpCheckValues As Boolean = True
        Dim strSQL As String
        If fstrIsMaster = "Y" Then ' Kiểm tra lỗi bảng Master
            ' Kiểm tra mã loại
            If blnTmpCheckValues Then
                If farrTmp_Values(0) Is System.DBNull.Value Then
                    ' TDTT-3032 - Trường mã loại không được để trống (null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3032")
                    blnTmpCheckValues = False
                Else
                    If Len(farrTmp_Values(0)) > 3 Or Not IsNumeric(Len(farrTmp_Values(0))) Then
                        ' TDTT-3020 - Mã loại không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3020")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            ' Kiểm mã khoản
            If blnTmpCheckValues Then
                If farrTmp_Values(1) Is System.DBNull.Value Then
                    ' TDTT-3033 - Trường mã khoản không được để trống (null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3033")
                    blnTmpCheckValues = False
                Else
                    If Len(farrTmp_Values(1)) > 2 Or Not IsNumeric(Len(farrTmp_Values(1))) Then
                        ' TDTT-3021 - Mã khoản không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3021")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            ' Kiểm tra tên gọi loại khoản
            If blnTmpCheckValues Then
                If farrTmp_Values(2) Is System.DBNull.Value Then
                    ' TDTT-3031 - Tên loại khoản không tồn tại
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3031")
                    blnTmpCheckValues = False
                End If
            End If

            ' Kiểm tra ngày bắt đầu
            If blnTmpCheckValues Then
                If farrTmp_Values(3) Is System.DBNull.Value Then
                    ' TDTT-3036 - Trường ngày bắt đầu không được để trống (null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3036")
                    blnTmpCheckValues = False
                Else
                    Dim tmpDate As String = ""
                    If Not IsDate(farrTmp_Values(3)) Then
                        ' TDTT-3037 - Ngày bắt đầu không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3037")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            ' Kiểm tra ngày kết thúc
            If blnTmpCheckValues Then
                If Not farrTmp_Values(4) Is System.DBNull.Value Then
                    If Not IsDate(farrTmp_Values(4)) Then
                        ' TDTT-3038 - Ngày kết thúc không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3038")
                        blnTmpCheckValues = False
                    Else
                        If farrTmp_Values(3) > farrTmp_Values(4) Then
                            ' TDTT-3039 - Ngày bắt đầu không được sau ngày kết thúc
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3039")
                            blnTmpCheckValues = False
                        End If
                    End If
                End If
            End If
        End If

        'Kiểm tra khoá cho bảng Master
        If blnTmpCheckValues Then  ' Không có lỗi chi tiết ở Master
            If fstrIsMaster = "Y" Then ' Nếu không lỗi kiểm tra trùng khoá
                Dim objLoaiKhoan As New TDDMDC.infLoaiKhoan
                objLoaiKhoan.MA_LOAI = farrTmp_Values(0)
                objLoaiKhoan.MA_KHOAN = farrTmp_Values(1)

                Dim strTmpNayBD As String = ""
                Dim strTmpNayKT As String = ""
                strTmpNayBD = farrTmp_Values(3).Year & _
                                                farrTmp_Values(3).Month.ToString.PadLeft(2, "0") & _
                                                    farrTmp_Values(3).Day.ToString.PadLeft(2, "0")
                objLoaiKhoan.NGAY_BD = strTmpNayBD
                If Not (farrTmp_Values(4) Is System.DBNull.Value) Then
                    objLoaiKhoan.NGAY_KT = farrTmp_Values(4).Year & _
                                                farrTmp_Values(4).Month.ToString.PadLeft(2, "0") & _
                                                    farrTmp_Values(4).Day.ToString.PadLeft(2, "0")
                    strTmpNayKT = objLoaiKhoan.NGAY_KT
                End If

                Dim tmpLoaiKhoan As New TDDMDC.daLoaiKhoan
                objLoaiKhoan = tmpLoaiKhoan.Load(objLoaiKhoan)

                If objLoaiKhoan.LKH_ID <> "" Then ' Tìm thấy khoá
                    If fblnGhide Then ' Cho phép ghi đè
                        strSQL = "Update TCS_DM_LOAI_KHOAN Set TEN = " & oToString(farrTmp_Values(2), "C") & _
                           ",NGAY_BD  = " & strTmpNayBD & _
                          ",NGAY_KT=" & IIf(strTmpNayKT = "", "NULL", strTmpNayKT) & _
                        " WHERE LKH_ID=" & objLoaiKhoan.LKH_ID
                        '",GHI_CHU=" & oToString(farrTmp_Values(5), "C") & _

                        fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)
                        blnTmpCheckValues = False ' Đã cập nhật rồi không cần Insert dữ liệu
                        fblnUpdate = True ' Dùng kết hợp với hàm CheckValues và không được tăng số lỗi lên
                        ' Cập nhật lại trạng thái của bảng Tran_Up
                        UpdateTransUp(gstrDN, "00", fstrTran_No)
                    Else ' Trùng khoa và không cho phép ghi đè
                        blnTmpCheckValues = False ' Bỏ qua không được Insert dữ liệu
                        ' TDTT-3028 - Mã loại khoản đã tồn tại trong danh mục
                        Dim cInsert As New DataAccess
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3028")
                        ' Cập nhật lại trạng thái của bảng Tran_Up
                        UpdateTransUp(gstrDN, "01", fstrTran_No)
                    End If
                Else ' Không bị trùng khoá, không lỗi và cho phép Insert dữ liệu
                    ' Cập nhật lại trạng thái của bảng Tran_Up
                    UpdateTransUp(gstrDN, "00", fstrTran_No)
                End If
            Else ' Không có lỗi ở bảng Detail
                ' Giao dịch này không có bảng Detail
            End If
        Else ' Kiểm tra có lỗi chi tiết trong bảng Master hoặc Detail
            blnTmpCheckValues = False ' Không cho phép Insert dữ liệu
            ' Cập nhật lại trạng thái của bảng Tran_Up
            UpdateTransUp(gstrDN, "01", fstrTran_No)
        End If
        Return blnTmpCheckValues
    End Function

    Private Function CheckValue_Muc_Tmuc(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
                       ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
                           ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                               ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                                   ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
        ' Tham số:
        ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
        '                   + False:Không cho phép Insert dữ liệu
        ' Người viết: Nguyễn Bá Cường
        ' Ngày viết: 10/11/2007
        ' ----------------------------------------------------------------
        Dim blnTmpCheckValues As Boolean = True
        Dim strSQL As String
        '
        If fstrIsMaster = "Y" Then ' Kiểm tra lỗi bảng Master
            ' Kiểm mã mục
            If blnTmpCheckValues Then
                If farrTmp_Values(0) Is System.DBNull.Value Then
                    ' TDTT-3072 - Trường mã mục không được để trống (null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3072")
                    blnTmpCheckValues = False
                Else
                    If Len(farrTmp_Values(0)) > 4 Then
                        ' TDTT-3060 - Mã mục không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3060")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            ' Kiểm mã tiểu mục
            If blnTmpCheckValues Then
                If farrTmp_Values(1) Is System.DBNull.Value Then
                    ' TDTT-3073 - Trường mã tiểu mục không được để trống (null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3073")
                    blnTmpCheckValues = False
                Else
                    If Len(farrTmp_Values(1)) > 2 Then
                        ' TDTT-3061 - Mã tiểu mục không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3061")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            ' Kiểm tra tên gọi mục tiểu mục
            If blnTmpCheckValues Then
                If farrTmp_Values(2) Is System.DBNull.Value Then
                    ' TDTT-3071 - Tên mục tiểu mục không tồn tại
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3071")
                    blnTmpCheckValues = False
                End If
            End If

            ' Kiểm tra ngày bắt đầu
            If blnTmpCheckValues Then
                If farrTmp_Values(3) Is System.DBNull.Value Then
                    ' TDTT-3076 - Trường ngày bắt đầu không được để trống (null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3076")
                    blnTmpCheckValues = False
                Else
                    Dim tmpDate As String = ""
                    If Not IsDate(farrTmp_Values(3)) Then
                        ' TDTT-3077 - Ngày bắt đầu không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3077")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            ' Kiểm tra ngày kết thúc
            If blnTmpCheckValues Then
                If Not farrTmp_Values(4) Is System.DBNull.Value Then
                    If Not IsDate(farrTmp_Values(4)) Then
                        ' TDTT-3078 - Ngày kết thúc không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3078")
                        blnTmpCheckValues = False
                    Else
                        If farrTmp_Values(3) > farrTmp_Values(4) Then
                            ' TDTT-3079 - Ngày bắt đầu không được sau ngày kết thúc
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3079")
                            blnTmpCheckValues = False
                        End If
                    End If
                End If
            End If
        Else ' Kiểm tra lỗi bảng Detail
            ' Giao dịch này không có bảng Detail
        End If
        'Kiểm tra khoá cho bảng Master
        If blnTmpCheckValues Then  ' Không có lỗi chi tiết ở Master
            If fstrIsMaster = "Y" Then ' Nếu không lỗi kiểm tra trùng khoá
                Dim objMucTmuc As New TDDMDC.infMucTMuc
                objMucTmuc.MA_MUC = farrTmp_Values(0)
                objMucTmuc.MA_TMUC = farrTmp_Values(1)

                Dim strTmpNayBD As String = ""
                Dim strTmpNayKT As String = ""
                strTmpNayBD = farrTmp_Values(3).Year & _
                                                farrTmp_Values(3).Month.ToString.PadLeft(2, "0") & _
                                                    farrTmp_Values(3).Day.ToString.PadLeft(2, "0")
                objMucTmuc.NGAY_BD = strTmpNayBD
                If Not (farrTmp_Values(4) Is System.DBNull.Value) Then
                    objMucTmuc.NGAY_KT = farrTmp_Values(4).Year & _
                                                farrTmp_Values(4).Month.ToString.PadLeft(2, "0") & _
                                                    farrTmp_Values(4).Day.ToString.PadLeft(2, "0")
                    strTmpNayKT = objMucTmuc.NGAY_KT
                End If

                Dim tmpMucTmuc As New TDDMDC.daMucTMuc
                objMucTmuc = tmpMucTmuc.Load(objMucTmuc)

                If objMucTmuc.MTM_ID <> "" Then ' Tìm thấy khoá
                    If fblnGhide Then ' Cho phép ghi đè
                        strSQL = "Update TCS_DM_MUC_TMUC Set TEN = " & oToString(farrTmp_Values(2), "C") & _
                           ",NGAY_BD  = " & strTmpNayBD & _
                          ",NGAY_KT=" & IIf(strTmpNayKT = "", "NULL", strTmpNayKT) & _
                        " WHERE MTM_ID=" & objMucTmuc.MTM_ID
                        '",GHI_CHU=" & oToString(farrTmp_Values(5), "C") & _

                        fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)
                        blnTmpCheckValues = False ' Đã cập nhật rồi không cần Insert dữ liệu
                        fblnUpdate = True ' Dùng kết hợp với hàm CheckValues và không được tăng số lỗi lên
                        ' Cập nhật lại trạng thái của bảng Tran_Up
                        UpdateTransUp(gstrDN, "00", fstrTran_No)
                    Else ' Trùng khoa và không cho phép ghi đè
                        blnTmpCheckValues = False ' Bỏ qua không được Insert dữ liệu
                        ' TDTT-3028 - Mã loại khoản đã tồn tại trong danh mục
                        Dim cInsert As New DataAccess
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3068")
                        ' Cập nhật lại trạng thái của bảng Tran_Up
                        UpdateTransUp(gstrDN, "01", fstrTran_No)
                    End If
                Else ' Không bị trùng khoá, không lỗi và cho phép Insert dữ liệu
                    ' Cập nhật lại trạng thái của bảng Tran_Up
                    UpdateTransUp(gstrDN, "00", fstrTran_No)
                End If
            Else ' Không có lỗi ở bảng Detail
                ' Giao dịch này không có bảng Detail
            End If
        Else ' Kiểm tra có lỗi chi tiết trong bảng Master hoặc Detail
            blnTmpCheckValues = False ' Không cho phép Insert dữ liệu
            ' Cập nhật lại trạng thái của bảng Tran_Up
            UpdateTransUp(gstrDN, "01", fstrTran_No)
        End If
        Return blnTmpCheckValues
    End Function

    Private Function CheckValue_NNT(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
                        ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
                            ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                                ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                                    ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
        ' Tham số:
        ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
        '                   + False:Không cho phép Insert dữ liệu
        ' Người viết: Nguyễn Bá Cường
        ' Ngày viết: 10/11/2007
        ' ----------------------------------------------------------------
        Dim blnTmpCheckValues As Boolean = True
        Dim strSQL As String

        Try
            If fstrIsMaster = "Y" Then ' kiem tra loi bang Master
                ' Kiểm tra Ma nguoi nop thue
                If blnTmpCheckValues Then
                    If farrTmp_Values(0) Is System.DBNull.Value Then
                        'TDTT-3088 - Truong mã NNT khong duoc de trong(null)
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3088")
                        blnTmpCheckValues = False
                    End If
                End If

                '' Kiểm tra mã cấp chương                
                'If blnTmpCheckValues AndAlso Not (farrTmp_Values(3) Is System.DBNull.Value) AndAlso Not (farrTmp_Values(4) Is System.DBNull.Value) Then
                '    If Not Valid_CCh(farrTmp_Values(3), farrTmp_Values(4)) Then
                '        'TDTT-3005 - Mã cấp chương không tồn tại trong danh mục
                '        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3005")
                '        blnTmpCheckValues = False
                '    End If
                'End If
                '' Kiểm tra mã loại khoản
                'If blnTmpCheckValues AndAlso Not (farrTmp_Values(5) Is System.DBNull.Value) AndAlso Not (farrTmp_Values(6) Is System.DBNull.Value) Then
                '    If Not Valid_LK(farrTmp_Values(5), farrTmp_Values(6)) Then
                '        'TDTT-3025 - Mã loại khoản không tồn tại trong danh mục
                '        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3025")
                '        blnTmpCheckValues = False
                '    End If
                'End If
            End If
            'Kiem tra khoa cho bang Master
            If blnTmpCheckValues Then  ' không co loi chi tiet o Master
                If fstrIsMaster = "Y" Then ' nếu không lỗi kiểm tra trùng khoá
                    Dim cTonTai As New DataAccess
                    Dim drTonTai As IDataReader

                    strSQL = "Select MA_NNT " & _
                        "From TCS_DM_NNT " & _
                        "Where (MA_NNT = '" & farrTmp_Values(0) & "')"
                    drTonTai = cTonTai.ExecuteDataReader(strSQL, CommandType.Text)
                    If drTonTai.Read Then ' tim thay khoa
                        If fblnGhide Then ' cho phep ghi de
                            'strSQL = "Update TCS_DM_NNT Set TEN_NNT = " & oToString(farrTmp_Values(1), "C") & _
                            '        ",DIA_CHI   = " & oToString(farrTmp_Values(2), "C") & _
                            '        ",MA_CAP   = " & oToString(farrTmp_Values(3), "C") & _
                            '        ",MA_CHUONG= " & oToString(farrTmp_Values(4), "C") & _
                            '        ",MA_LOAI  = " & oToString(farrTmp_Values(5), "C") & _
                            '        ",MA_KHOAN = " & oToString(farrTmp_Values(6), "C") & _
                            '        ",MA_CQTHU= " & oToString(farrTmp_Values(7), "C") & _
                            '        ",MA_TINH  = " & oToString(farrTmp_Values(8), "C") & _
                            '        ",MA_HUYEN = " & oToString(farrTmp_Values(9), "C") & _
                            '        ",MA_XA = " & oToString(farrTmp_Values(10), "C") & _
                            '        " Where (MA_NNT = '" & farrTmp_Values(0) & "')"
                            strSQL = "Update TCS_DM_NNT Set TEN_NNT = " & oToString(farrTmp_Values(1), "C") & _
                                   ",DIA_CHI   = " & oToString(farrTmp_Values(2), "C") & _
                                   ",MA_CHUONG= " & oToString(farrTmp_Values(4), "C") & _
                                   ",MA_KHOAN = " & oToString(farrTmp_Values(6), "C") & _
                                   ",MA_CQTHU= " & oToString(farrTmp_Values(7), "C") & _
                                   ",MA_XA = " & oToString(farrTmp_Values(10), "C") & _
                                   " Where (MA_NNT = '" & farrTmp_Values(0) & "')"
                            fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)
                            blnTmpCheckValues = False ' da cap nhat roi khong can Insert du lieu
                            fblnUpdate = True ' dung de ket hop voi ham checkvalue va khong duoc tang so loi len
                            ' Cap nhat lai trang thai cua bang Tran_Up
                            Dim cInsert As New DataAccess

                            strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                                    " Where TRAN_NO = '" & fstrTran_No & "'"
                            cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                            ' Giải phóng Connection tới CSDL
                            cInsert.Dispose()
                        Else ' trung khoa va khong cho phep ghi de
                            blnTmpCheckValues = False ' bo qua không duoc Insert du lieu
                            'TDTT-3082 - Mã ĐTNT đã tồn tại trong danh mục
                            Dim cInsert As New DataAccess

                            strSQL = "Insert Into LOI_DL(id_imex,id_mess,ma_loi) Values " & _
                                    "('" & fstrId_ImEx & "','" & fstrId_Mess & "','TDTT-3082')"
                            cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                            ' Cap nhat lai trang thai cua bang Tran_Up
                            strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                                    " Where TRAN_NO = '" & fstrTran_No & "'"
                            cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                            ' Giải phóng Connection tới CSDL
                            cInsert.Dispose()
                        End If
                    Else ' khong bi trung khoa, khong loi va cho phep Insert du lieu
                        ' Cap nhat lai trang thai cua bang Tran_Up
                        Dim cInsert As New DataAccess

                        strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                                "Where TRAN_NO = '" & fstrTran_No & "'"
                        cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                        ' Giải phóng Connection tới CSDL
                        cInsert.Dispose()
                    End If
                    ' Giải phóng Connection tới CSDL
                    drTonTai.Dispose()
                    cTonTai.Dispose()
                Else ' khong co loi o bang Detail
                    ' Giao dich nay khong co bang Detail
                End If
            Else ' kiem tra co loi chi tiet trong Master hoac Detail
                blnTmpCheckValues = False ' khong cho phep Insert du lieu
                ' Cap nhat lai trang thai cua bang Tran_Up
                Dim cInsert As New DataAccess

                strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                        "Where TRAN_NO = '" & fstrTran_No & "'"
                cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                ' Giải phóng Connection tới CSDL
                cInsert.Dispose()
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi kiểm tra dữ liệu")
            Throw ex
            blnTmpCheckValues = False
        End Try
        Return blnTmpCheckValues
    End Function

    Private Function CheckValue_SoThue(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
                           ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
                               ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                                   ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                                       ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
        ' Tham số:
        ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
        '                   + False:Không cho phép Insert dữ liệu
        ' Người viết: Nguyễn Bá Cường
        ' Ngày viết: 10/11/2007
        ' Sửa 01/01/2009: bỏ các kiểm tra mã tỉnh, mã huyện, cấp, loại, mục.
        ' ----------------------------------------------------------------
        Dim blnTmpCheckValues As Boolean = True
        Dim strSQL As String
        ' Kiểm tra sổ thuế
        If fstrIsMaster = "Y" Then ' kiem tra loi bang Master
            ' Kiểm tra Mã NNT
            If blnTmpCheckValues Then
                If farrTmp_Values(0) Is System.DBNull.Value Then
                    'TDTT-3088 - Truong Mã ĐTNT không duoc de trong(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3088")
                    blnTmpCheckValues = False
                Else
                    If Not TCS_KT_NNT(farrTmp_Values(0)) Then
                        'TDTT-3081 - Mã ĐTNT không tồn tại trong danh mục 
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3081")
                        blnTmpCheckValues = False
                    End If
                End If
            End If

            '' Kiểm tra mã tinh
            'If blnTmpCheckValues Then
            '    If Not (farrTmp_Values(3) Is System.DBNull.Value) AndAlso farrTmp_Values(3).trim <> "" Then
            '        If Not TCS_KT_TINH_HUYEN_XA(farrTmp_Values(3), "00", "00") Then
            '            'TDTT-3083 - Ma tinh khong ton tai trong danh muc
            '            'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3102")
            '            blnTmpCheckValues = False
            '        End If

            '    Else
            '        'TDTT-3083 - Ma tinh khong duoc de trong
            '        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3108")
            '        blnTmpCheckValues = False
            '    End If
            'End If

            '' Kiểm tra mã huyen
            'If blnTmpCheckValues Then
            '    If Not (farrTmp_Values(4) Is System.DBNull.Value) AndAlso farrTmp_Values(4).trim <> "" Then
            '        If Not TCS_KT_TINH_HUYEN_XA(farrTmp_Values(3), farrTmp_Values(4), "00") Then
            '            'TDTT-3083 - Ma huyen khong ton tai trong danh muc
            '            'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3103")
            '            blnTmpCheckValues = False
            '        End If
            '    Else
            '        'TDTT-3083 - Ma huyen khong duoc de trong
            '        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3109")
            '        blnTmpCheckValues = False
            '    End If
            'End If


            '  Kiểm tra mã xa - need edit
            If blnTmpCheckValues Then
                If Not (farrTmp_Values(5) Is System.DBNull.Value) Then
                    'If Not TCS_KT_TINH_HUYEN_XA("", "", farrTmp_Values(5)) Then
                    If Not TCS_KT_XA(farrTmp_Values(5)) Then
                        'TDTT-3110 - Mã tỉnh mã huyện xã không có trong danh mục
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3112")
                        blnTmpCheckValues = False
                    End If
                Else
                    'TDTT-3083 - Ma xa khong duoc de trong
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-5005")
                    blnTmpCheckValues = False
                End If
            End If
            ' Kiểm tra mã CQ thu 
            If blnTmpCheckValues Then
                If Not (farrTmp_Values(13) Is System.DBNull.Value) Then
                    If Not TCS_KT_CQTHU(farrTmp_Values(13)) Then
                        'TDTT-3121 - Mã cơ quan thu không tồn tại trong danh mục
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3121")
                        blnTmpCheckValues = False
                    End If
                End If
            End If
            ' Kiểm tra cấp chương
            If blnTmpCheckValues Then
                If Not (farrTmp_Values(15) Is System.DBNull.Value) AndAlso farrTmp_Values(15).trim <> "" Then
                    ' If Not Valid_CCh(farrTmp_Values(14), farrTmp_Values(15)) Then
                    If Not Valid_Chuong_Nhan(farrTmp_Values(15)) Then
                        'TDTT-3005 - Mã cấp chương không tồn tại trong danh mục
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3005")
                        blnTmpCheckValues = False
                    End If
                End If
            End If
            ' Kiểm tra mã loại khoản
            If blnTmpCheckValues Then
                'If Not (farrTmp_Values(16) Is System.DBNull.Value) AndAlso farrTmp_Values(16).trim <> "" Then
                If Not (farrTmp_Values(17) Is System.DBNull.Value) AndAlso farrTmp_Values(17).trim <> "" Then
                    If Not Valid_LK(farrTmp_Values(16), farrTmp_Values(17)) Then
                        'TDTT-3025 - Mã loại khoản không tồn tại trong danh mục
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3025")
                        blnTmpCheckValues = False
                    End If
                End If
                'End If
            End If
            ' Kiểm tra mã mục tiểu mục
            If blnTmpCheckValues Then
                'If farrTmp_Values(18) Is System.DBNull.Value Then
                '    'TDTT-3072 - Trường mã mục không được để trống(null)
                '    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3072")
                '    blnTmpCheckValues = False
                'Else
                If farrTmp_Values(19) Is System.DBNull.Value Then
                    'TDTT-3073 - Trường mã tiểu mục không được để trống(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3073")
                    blnTmpCheckValues = False
                Else
                    If Not Valid_MTM(farrTmp_Values(18), farrTmp_Values(19)) Then
                        'TDTT-3065 - Mã mục tiểu mục không tồn tại trong danh mục
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3065")
                        blnTmpCheckValues = False
                    End If
                End If
                'End If
            End If
        Else ' kiem tra loi bang Detail
            ' So thue khong co bang Detail
        End If

        'Kiem tra khoa cho bang Master
        If blnTmpCheckValues Then ' không co loi chi tiet o Master
            If fstrIsMaster = "Y" Then
                ' Kiem tra trung khoa trong CSDL tac nghiep
                Dim strTmpWhere As String = ""

                'If farrTmp_Values(14) Is System.DBNull.Value Then
                '    strTmpWhere = strTmpWhere & "(MA_CAP is Null) And"
                'Else
                '    strTmpWhere = strTmpWhere & "(MA_CAP = '" & farrTmp_Values(14) & "') And"
                'End If

                If farrTmp_Values(15) Is System.DBNull.Value Then
                    strTmpWhere = strTmpWhere & "(MA_CHUONG is Null) And"
                Else
                    strTmpWhere = strTmpWhere & "(MA_CHUONG = '" & farrTmp_Values(15) & "') And"
                End If

                'If farrTmp_Values(16) Is System.DBNull.Value Then
                '    strTmpWhere = strTmpWhere & "(MA_LOAI is Null) And"
                'Else
                '    strTmpWhere = strTmpWhere & "(MA_LOAI = '" & farrTmp_Values(16) & "') And"
                'End If

                If farrTmp_Values(17) Is System.DBNull.Value Then
                    strTmpWhere = strTmpWhere & "(MA_KHOAN is Null) And"
                Else
                    strTmpWhere = strTmpWhere & "(MA_KHOAN = '" & farrTmp_Values(17) & "') And"
                End If
                ' Cat bo phep toan And o cuoi chuoi
                strTmpWhere = Mid(strTmpWhere, 1, Len(strTmpWhere) - 3)
                '
                Dim cTonTai As New DataAccess
                Dim drTonTai As IDataReader

                'strSQL = "Select 1 From TCS_SOTHUE Where (MA_NNT = '" & farrTmp_Values(0) & "') And " & _
                '        "(KYTHUE = '" & farrTmp_Values(3) & "') And " & strTmpWhere & " And (MA_MUC = '" & _
                '        farrTmp_Values(18) & "') And (MA_TMUC = '" & farrTmp_Values(19) & "')"

                strSQL = "Select 1 From TCS_SOTHUE Where (MA_NNT = '" & farrTmp_Values(0) & "') And " & _
                        "(KYTHUE = '" & farrTmp_Values(3) & "') And " & strTmpWhere & " And (MA_TMUC = '" & farrTmp_Values(19) & "')"

                drTonTai = cTonTai.ExecuteDataReader(strSQL, CommandType.Text)
                If drTonTai.Read Then ' tim thay khoa
                    If fblnGhide Then ' cho phep ghi de
                        'strSQL = "Update TCS_SOTHUE Set SO_PHAINOP = " & oToString(farrTmp_Values(7), "N") & _
                        '        " Where (MA_NNT = '" & farrTmp_Values(0) & "') And " & _
                        '        "(KYTHUE = '" & farrTmp_Values(2) & "') And " & _
                        '        strTmpWhere & " And (MA_MUC = '" & farrTmp_Values(18) & _
                        '        "') And (MA_TMUC = '" & farrTmp_Values(19) & "')"

                        strSQL = "Update TCS_SOTHUE Set SO_PHAINOP = " & oToString(farrTmp_Values(7), "N") & _
                             " Where (MA_NNT = '" & farrTmp_Values(0) & "') And " & _
                             "(KYTHUE = '" & farrTmp_Values(2) & "') And " & _
                             strTmpWhere & " And (MA_TMUC = '" & farrTmp_Values(19) & "')"

                        fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)
                        blnTmpCheckValues = False ' da cap nhat roi khong can Insert du lieu
                        fblnUpdate = True ' dung de ket hop voi ham checkvalue va khong duoc tang so loi len
                        ' Cap nhat lai trang thai cua bang Tran_Up
                        Dim cInsert As New DataAccess

                        strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                                " Where TRAN_NO = '" & fstrTran_No & "'"
                        cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                        ' Giải phóng Connection tới CSDL
                        cInsert.Dispose()
                    Else ' trung khoa va khong cho phep ghi de
                        blnTmpCheckValues = False ' bo qua không duoc Insert du lieu
                        'TDTT-3438 - Mã ĐTNT+Kỳ thuế+... đã tồn tại trong sổ thuế
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3438")

                        Dim cInsert As New DataAccess
                        ' Cap nhat lai trang thai cua bang Tran_Up
                        strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                                " Where TRAN_NO = '" & fstrTran_No & "'"
                        cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                        ' Giải phóng Connection tới CSDL
                        cInsert.Dispose()
                    End If
                Else ' khong bi trung khoa, khong loi va cho phep ghi de
                    ' Cap nhat lai trang thai cua bang Tran_Up
                    Dim cInsert As New DataAccess

                    strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                            "Where TRAN_NO = '" & fstrTran_No & "'"
                    cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                    ' Giải phóng Connection tới CSDL
                    cInsert.Dispose()
                End If
                ' Giải phóng Connection tới CSDL
                drTonTai.Dispose()
                cTonTai.Dispose()
            Else ' bang Detail khong bi loi
                ' Giao dich nay khong co bang Detail
            End If
        Else ' co loi chi tiet trong Master hoac Detail
            Dim cInsert As New DataAccess

            blnTmpCheckValues = False ' khong cho phep Insert du lieu
            ' Cap nhat lai trang thai cua bang Tran_Up
            strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                    "Where TRAN_NO = '" & fstrTran_No & "'"
            cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
            ' Giải phóng Connection tới CSDL
            cInsert.Dispose()
        End If
        Return blnTmpCheckValues
    End Function

    Private Function CheckValue_Diaban(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
                       ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
                           ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                               ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                                   ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
        ' Tham số:
        ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
        '                   + False:Không cho phép Insert dữ liệu
        ' Người viết: Nguyễn Bá Cường
        ' Ngày viết: 10/11/2007
        ' ----------------------------------------------------------------
        Dim blnTmpCheckValues As Boolean = True
        Dim strSQL As String

        Select Case farrTmp_Values.Count
            Case 2 ' Tinh
                If blnTmpCheckValues And fstrIsMaster = "Y" Then
                    If farrTmp_Values(0) Is System.DBNull.Value Then
                        ' TDTT-3012 - Trường tỉnh cấp không được để trống(null)
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3108")
                        blnTmpCheckValues = False
                    Else
                        If Len(farrTmp_Values(0)) <> 3 Then
                            ' TDTT-5010 - Mã tỉnh không đúng định dạng
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-5010")
                            blnTmpCheckValues = False
                        ElseIf Valid_Tinh(farrTmp_Values(0)) Then
                            'TDTT-5011 - Mã tỉnh đã tồn tại trong danh mục
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-5011")
                            blnTmpCheckValues = False
                        End If
                    End If
                End If
            Case 5 ' Huyen
                gblnLoiDetail = False
                If blnTmpCheckValues And fstrIsMaster = "N" Then
                    If farrTmp_Values(3) Is System.DBNull.Value Then
                        ' TDTT-3012 - Trường huyện cấp không được để trống(null)
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3109")
                        blnTmpCheckValues = False
                    Else
                        If Len(farrTmp_Values(3)) <> 2 Then
                            ' TDTT-5012 - Mã huyện không đúng định dạng
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-5012")
                            blnTmpCheckValues = False
                        ElseIf TCS_KT_TINH_HUYEN_XA(farrTmp_Values(2), farrTmp_Values(3), "00") Then
                            'TDTT-5013 - Mã huyện đã tồn tại trong danh mục
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-5013")
                            blnTmpCheckValues = False
                        End If
                    End If
                End If
            Case 9 ' Xa
                gblnLoiDetail = False
                If blnTmpCheckValues And fstrIsMaster = "N" Then
                    If farrTmp_Values(7) Is System.DBNull.Value Then
                        ' TDTT-3012 - Trường xã cấp không được để trống(null)
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3112")
                        blnTmpCheckValues = False
                    Else
                        If Len(farrTmp_Values(7)) <> 2 Then
                            ' TDTT-5014 - Mã xã không đúng định dạng
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-5014")
                            blnTmpCheckValues = False
                        ElseIf TCS_KT_TINH_HUYEN_XA(farrTmp_Values(5), farrTmp_Values(6), farrTmp_Values(7)) Then
                            'TDTT-5015 - Mã xã đã tồn tại trong danh mục
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-5015")
                            blnTmpCheckValues = False
                        End If
                    End If
                End If
        End Select

        Return blnTmpCheckValues
    End Function

    Private Function CheckValue_ChungTu(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
                       ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
                           ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                               ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                                   ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
        ' Tham số:
        ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
        '                   + False:Không cho phép Insert dữ liệu
        ' Người viết: Ngô Minh Trọng
        ' Ngày viết: 14/12/2007
        ' ----------------------------------------------------------------
        Dim blnTmpCheckValues As Boolean = True
        Dim strSQL As String
        ' Kiểm tra sổ thuế
        If fstrIsMaster = "Y" Then ' kiem tra loi bang Master
            ' Kiểm tra SHKB
            If blnTmpCheckValues Then
                If farrTmp_Values(0) Is System.DBNull.Value Then
                    'TDTT-3500 - Truong SHKB duoc de trong(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3500")
                    blnTmpCheckValues = False
                End If
            End If
            ' Kiểm tra NGAY_KB
            If blnTmpCheckValues Then
                If farrTmp_Values(1) Is System.DBNull.Value Then
                    'TDTT-3502 - Truong ngày KB không được trống (null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3502")
                    blnTmpCheckValues = False
                Else
                    Dim strDate As String
                    strDate = ConvertNumbertoString(farrTmp_Values(1))
                    If Not Valid_Date(strDate) Then
                        'TDTT-3503 - Ngày kho bạc không đúng định dạng
                        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3503")
                        blnTmpCheckValues = False
                    End If
                End If
            End If
            ' Kiểm tra mã nhân viên
            If blnTmpCheckValues Then
                If farrTmp_Values(2) Is System.DBNull.Value Then
                    'TDTT-3504 - Truong mã nhân viên không được trống (null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3504")
                    blnTmpCheckValues = False
                End If
            End If
            ' Kiểm tra số bút toán
            If blnTmpCheckValues Then
                If farrTmp_Values(3) Is System.DBNull.Value Then
                    'TDTT-3505 - Truong so but toan khong duoc de trong(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3505")
                    blnTmpCheckValues = False
                End If
            End If
            ' Kiểm tra mã điểm thu
            If blnTmpCheckValues Then
                If farrTmp_Values(4) Is System.DBNull.Value Then
                    'TDTT-3506 - Truong ma diem thu khong duoc de trong(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3506")
                    blnTmpCheckValues = False
                End If
            End If
            ' Kiểm tra số bàn thu
            If blnTmpCheckValues Then
                If farrTmp_Values(5) Is System.DBNull.Value Then
                    'TDTT-3507 - Truong so ban thu thu khong duoc de trong(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3507")
                    blnTmpCheckValues = False
                End If
            End If
            ' Kiểm tra ký hiệu chứng từ
            If blnTmpCheckValues Then
                If farrTmp_Values(8) Is System.DBNull.Value Then
                    'TDTT-3508 - Truong ky hieu chung tu khong duoc de trong(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3508")
                    blnTmpCheckValues = False
                End If
            End If
            ' Kiểm tra số chứng từ
            If blnTmpCheckValues Then
                If farrTmp_Values(9) Is System.DBNull.Value Then
                    'TDTT-3509 - Truong so chung tu khong duoc de trong(null)
                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3509")
                    blnTmpCheckValues = False
                End If
            End If
            'Kienvt :Check lai o day
            '' Kiểm tra chứng từ đã tồn tại trong hệ thống chưa
            'If blnTmpCheckValues Then
            '    If bytTCS_GNT_Exist(farrTmp_Values(9), farrTmp_Values(8)) Then
            '        'TDTT-3519 - chứng từ đã tồn tại
            '        ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3519")
            '        blnTmpCheckValues = False
            '    End If
        End If
        ' Kiểm tra mã cơ quan thu
        If blnTmpCheckValues Then
            If farrTmp_Values(17) Is System.DBNull.Value Then
                'TDTT-3510 - Truong ma co quan thu khong duoc de trong(null)
                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3510")
                blnTmpCheckValues = False
            End If
        End If
        ' Kiểm tra mã tỉnh
        If blnTmpCheckValues Then
            If farrTmp_Values(19) Is System.DBNull.Value Then
                'TDTT-3511 - Truong ma tinh khong duoc de trong(null)
                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-351")
                blnTmpCheckValues = False
            End If
        End If
        ' Kiểm tra mã huyện
        If blnTmpCheckValues Then
            If farrTmp_Values(20) Is System.DBNull.Value Then
                'TDTT-3512 - Truong ma huyen khong duoc de trong(null)
                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3512")
                blnTmpCheckValues = False
            End If
        End If
        ' Kiểm tra mã xã
        If blnTmpCheckValues Then
            If farrTmp_Values(21) Is System.DBNull.Value Then
                'TDTT-3513 - Truong ma xa khong duoc de trong(null)
                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3513")
                blnTmpCheckValues = False
            End If
        End If
        ' Kiểm tra Xa_ID
        If blnTmpCheckValues Then
            If farrTmp_Values(18) Is System.DBNull.Value Then
                'TDTT-3514 - Ma xa khong ton tai trong danh muc
                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3514")
                blnTmpCheckValues = False
            End If
        End If
        ' Kiểm tra tài khoản nợ
        If blnTmpCheckValues Then
            If farrTmp_Values(22) Is System.DBNull.Value Then
                'TDTT-3515 - Truong tai khoan no khong duoc de trong(null)
                'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3515")
                blnTmpCheckValues = False
                'ElseIf Not Valid_TaiKhoan(farrTmp_Values(22)) Then
                'TDTT-3516 - Ma tai khoan o khong ton tai trong danh muc
                'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3516")
                'blnTmpCheckValues = False
            End If
        End If
        ' Kiểm tra tài khoản có
        If blnTmpCheckValues Then
            If farrTmp_Values(23) Is System.DBNull.Value Then
                'TDTT-3517 - Truong tai khoan co khong duoc de trong(null)
                'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3517")
                blnTmpCheckValues = False
                'ElseIf Not Valid_TaiKhoan(farrTmp_Values(23)) Then
                'TDTT-3518 - Ma tai khoan co khong ton tai trong danh muc
                'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3518")
                blnTmpCheckValues = False
                'End If
            End If

        Else ' kiem tra loi bang Detail
            ' Kiểm tra cấp chương
            If blnTmpCheckValues Then
                If Not (farrTmp_Values(45) Is System.DBNull.Value) Then
                    If Not (farrTmp_Values(46) Is System.DBNull.Value) Then
                        If Not Valid_CCh(farrTmp_Values(45), farrTmp_Values(46)) Then
                            'TDTT-3005 - Mã cấp chương không tồn tại trong danh mục
                            'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3005")
                            blnTmpCheckValues = False
                        End If
                    End If
                End If
            End If
            ' Kiểm tra mã loại khoản
            If blnTmpCheckValues Then
                If Not (farrTmp_Values(48) Is System.DBNull.Value) Then
                    If Not (farrTmp_Values(49) Is System.DBNull.Value) Then
                        If Not Valid_LK(farrTmp_Values(48), farrTmp_Values(49)) Then
                            'TDTT-3025 - Mã loại khoản không tồn tại trong danh mục
                            'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3025")
                            blnTmpCheckValues = False
                        End If
                    End If
                End If
            End If
            ' Kiểm tra mã muc-tmuc
            If blnTmpCheckValues Then
                If Not (farrTmp_Values(51) Is System.DBNull.Value) Then
                    If Not (farrTmp_Values(52) Is System.DBNull.Value) Then
                        If Not Valid_MTM(farrTmp_Values(51), farrTmp_Values(52)) Then
                            'TDTT-3065 - Mã mục tiểu mục không tồn tại trong danh mục
                            'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3065")
                            blnTmpCheckValues = False
                        End If
                    End If
                End If
            End If
        End If

        'Kiem tra khoa cho bang Master
        If blnTmpCheckValues Then ' không co loi chi tiet o Master
            If fstrIsMaster = "Y" Then
                ' Kiem tra trung khoa trong CSDL tac nghiep
                If CheckExistCTuTHop(farrTmp_Values(0), farrTmp_Values(1), farrTmp_Values(2), _
                farrTmp_Values(3), farrTmp_Values(4)) Then ' tim thay khoa
                    If fblnGhide Then ' cho phep ghi de
                        strSQL = "DELETE TCS_CTU_THOP_DTL " & _
                        "WHERE SHKB='" & farrTmp_Values(0) & "' AND NGAY_KB = " & farrTmp_Values(1) & " AND MA_NV = " & farrTmp_Values(2) & " " & _
                        "AND SO_BT = " & farrTmp_Values(3) & " AND MA_DTHU = '" & farrTmp_Values(4) & "'"
                        fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)

                        strSQL = "DELETE TCS_CTU_THOP_HDR " & _
                        "WHERE SHKB='" & farrTmp_Values(0) & "' AND NGAY_KB = " & farrTmp_Values(1) & " AND MA_NV = " & farrTmp_Values(2) & " " & _
                        "AND SO_BT = " & farrTmp_Values(3) & " AND MA_DTHU = '" & farrTmp_Values(4) & "'"
                        fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)

                        blnTmpCheckValues = True ' da cap nhat roi khong can Insert du lieu
                        fblnUpdate = True ' dung de ket hop voi ham checkvalue va khong duoc tang so loi len
                        ' Cap nhat lai trang thai cua bang Tran_Up
                        Dim cInsert As New DataAccess

                        strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                                " Where TRAN_NO = '" & fstrTran_No & "'"
                        cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                        ' Giải phóng Connection tới CSDL
                        cInsert.Dispose()
                    Else ' trung khoa va khong cho phep ghi de
                        blnTmpCheckValues = False ' bo qua không duoc Insert du lieu
                        'TDTT-3519 - Chung tu da ton tai trong he thong
                        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3519")

                        Dim cInsert As New DataAccess
                        ' Cap nhat lai trang thai cua bang Tran_Up
                        strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                                " Where TRAN_NO = '" & fstrTran_No & "'"
                        cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                        ' Giải phóng Connection tới CSDL
                        cInsert.Dispose()
                    End If
                Else ' khong bi trung khoa, khong loi va cho phep ghi de
                    ' Cap nhat lai trang thai cua bang Tran_Up
                    Dim cInsert As New DataAccess

                    strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                            "Where TRAN_NO = '" & fstrTran_No & "'"
                    cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                    ' Giải phóng Connection tới CSDL
                    cInsert.Dispose()
                End If
            Else ' bang Detail khong bi loi

            End If
        Else ' co loi chi tiet trong Master hoac Detail
            Dim cInsert As New DataAccess

            blnTmpCheckValues = False ' khong cho phep Insert du lieu
            ' Cap nhat lai trang thai cua bang Tran_Up
            strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                    "Where TRAN_NO = '" & fstrTran_No & "'"
            cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
            ' Giải phóng Connection tới CSDL
            cInsert.Dispose()
        End If
        Return blnTmpCheckValues
    End Function

    Private Function CheckValue_ToKhai(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
                        ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
                            ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                                ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                                    ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
        ' Tham số:
        ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
        '                   + False:Không cho phép Insert dữ liệu
        ' Người viết: Nguyễn Bá Cường
        ' Ngày viết: 10/11/2007
        ' ----------------------------------------------------------------
        Dim blnTmpCheckValues As Boolean = True
        Dim strSQL As String
        ' Kiểm tra sổ thuế
        If fstrIsMaster = "Y" Then ' kiem tra loi bang Master
            ' Kiểm tra Mã NNT
            If blnTmpCheckValues Then
                If farrTmp_Values(0) Is System.DBNull.Value Then
                    'TDTT-3088 - Truong Mã ĐTNT không duoc de trong(null)
                    'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3088")
                    blnTmpCheckValues = False
                Else
                    If Not TCS_KT_NNT(farrTmp_Values(0)) Then
                        'TDTT-3081 - Mã ĐTNT không tồn tại trong danh mục 
                        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3081")
                        blnTmpCheckValues = False
                    End If
                End If
            End If
            ' Kiểm tra mã Kho bạc               
            If blnTmpCheckValues Then
                If Not (farrTmp_Values(6) Is System.DBNull.Value) Then
                    If Not TCS_KT_MAKBAC(farrTmp_Values(6)) Then
                        'TDTT-5002 - Mã cơ quan kho bạc không tồn tại trong danh mục
                        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-5002")
                        blnTmpCheckValues = False
                    End If
                End If
            End If


            '' Kiểm tra mã tinh
            'If blnTmpCheckValues Then
            '    If Not (farrTmp_Values(3) Is System.DBNull.Value) Then
            '        If Not TCS_KT_TINH_HUYEN_XA(farrTmp_Values(3), "00", "00") Then
            '            'TDTT-3083 - Ma tinh khong ton tai trong danh muc
            '            'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3102")
            '            blnTmpCheckValues = False
            '        End If

            '    Else
            '        'TDTT-3083 - Ma tinh khong duoc de trong
            '        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3108")
            '        blnTmpCheckValues = False
            '    End If
            'End If
            '' Kiểm tra mã huyen
            'If blnTmpCheckValues Then
            '    If Not (farrTmp_Values(4) Is System.DBNull.Value) Then
            '        If Not TCS_KT_TINH_HUYEN_XA(farrTmp_Values(3), farrTmp_Values(4), "00") Then
            '            'TDTT-3083 - Ma huyen khong ton tai trong danh muc
            '            'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3103")
            '            blnTmpCheckValues = False
            '        End If
            '    Else
            '        'TDTT-3083 - Ma huyen khong duoc de trong
            '        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3109")
            '        blnTmpCheckValues = False
            '    End If
            'End If


            '  Kiểm tra mã xa
            If blnTmpCheckValues Then
                If Not (farrTmp_Values(5) Is System.DBNull.Value) Then
                    If Not TCS_KT_XA(farrTmp_Values(5)) Then
                        'TDTT-3110 - Mã tỉnh mã huyện xã không có trong danh mục
                        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3112")
                        blnTmpCheckValues = False
                    End If
                Else
                    'TDTT-3083 - Ma xa khong duoc de trong
                    'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-5005")
                    blnTmpCheckValues = False
                End If
            End If
            ' Kiểm tra mã CQ thu               
            If blnTmpCheckValues Then
                If Not (farrTmp_Values(13) Is System.DBNull.Value) Then
                    If Not TCS_KT_CQTHU(farrTmp_Values(13)) Then
                        'TDTT-3121 - Mã cơ quan thu không tồn tại trong danh mục
                        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3121")
                        blnTmpCheckValues = False
                    End If
                End If
            End If
            ' Kiểm tra cấp chương
            If blnTmpCheckValues Then
                'If Not (farrTmp_Values(14) Is System.DBNull.Value) Then
                If Not (farrTmp_Values(15) Is System.DBNull.Value) Then
                    If Not Valid_CCh(farrTmp_Values(14), farrTmp_Values(15)) Then
                        'TDTT-3005 - Mã cấp chương không tồn tại trong danh mục
                        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3005")
                        blnTmpCheckValues = False
                    End If
                End If
                ' End If
            End If
            ' Kiểm tra mã loại khoản
            If blnTmpCheckValues Then
                'If Not (farrTmp_Values(16) Is System.DBNull.Value) Then
                If Not (farrTmp_Values(17) Is System.DBNull.Value) Then
                    If Not Valid_LK("", farrTmp_Values(17)) Then
                        'TDTT-3025 - Mã loại khoản không tồn tại trong danh mục
                        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3025")
                        blnTmpCheckValues = False
                    End If
                End If
                'End If
            End If
            ' Kiểm tra mã mục tiểu mục
            If blnTmpCheckValues Then
                'If farrTmp_Values(18) Is System.DBNull.Value Then
                '    'TDTT-3072 - Trường mã mục không được để trống(null)
                '    'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3072")
                '    blnTmpCheckValues = False
                'Else
                If farrTmp_Values(19) Is System.DBNull.Value Then
                    'TDTT-3073 - Trường mã tiểu mục không được để trống(null)
                    'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3073")
                    blnTmpCheckValues = False
                Else
                    If Not Valid_MTM(farrTmp_Values(18), farrTmp_Values(19)) Then
                        'TDTT-3065 - Mã mục tiểu mục không tồn tại trong danh mục
                        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3065")
                        blnTmpCheckValues = False
                    End If
                End If
                'End If
            End If
        Else ' kiem tra loi bang Detail
            ' So thue khong co bang Detail
        End If

        'Kiem tra khoa cho bang Master
        If blnTmpCheckValues Then ' không co loi chi tiet o Master
            If fstrIsMaster = "Y" Then
                ' Kiem tra trung khoa trong CSDL tac nghiep
                Dim strTmpWhere As String = ""

                'If farrTmp_Values(14) Is System.DBNull.Value Then
                '    strTmpWhere = strTmpWhere & "(MA_CAP is Null) And"
                'Else
                '    strTmpWhere = strTmpWhere & "(MA_CAP = '" & farrTmp_Values(14) & "') And"
                'End If

                If farrTmp_Values(15) Is System.DBNull.Value Then
                    strTmpWhere = strTmpWhere & "(MA_CHUONG is Null) And"
                Else
                    strTmpWhere = strTmpWhere & "(MA_CHUONG = '" & farrTmp_Values(15) & "') And"
                End If
                'If farrTmp_Values(16) Is System.DBNull.Value Then
                '    strTmpWhere = strTmpWhere & "(MA_LOAI is Null) And"
                'Else
                '    strTmpWhere = strTmpWhere & "(MA_LOAI = '" & farrTmp_Values(16) & "') And"
                'End If
                If farrTmp_Values(17) Is System.DBNull.Value Then
                    strTmpWhere = strTmpWhere & "(MA_KHOAN is Null) And"
                Else
                    strTmpWhere = strTmpWhere & "(MA_KHOAN = '" & farrTmp_Values(17) & "') And"
                End If
                ' Cat bo phep toan And o cuoi chuoi
                strTmpWhere = Mid(strTmpWhere, 1, Len(strTmpWhere) - 3)
                '
                Dim cTonTai As New DataAccess
                Dim drTonTai As IDataReader
                '
                'strSQL = "Select 1 From TCS_DS_TOKHAI Where (MA_NNT = '" & farrTmp_Values(0) & "') And " & _
                '        "(SO_TK = '" & farrTmp_Values(4) & "') And (NGAY_TK = '" & farrTmp_Values(5) & "') And " & _
                '        strTmpWhere & " And (MA_MUC = '" & _
                '        farrTmp_Values(18) & "') And (MA_TMUC = '" & farrTmp_Values(19) & "')"

                strSQL = "Select 1 From TCS_DS_TOKHAI Where (MA_NNT = '" & farrTmp_Values(0) & "') And " & _
                       "(SO_TK = '" & farrTmp_Values(4) & "') And (NGAY_TK = '" & farrTmp_Values(5) & "') And " & _
                       strTmpWhere & " And (MA_TMUC = '" & farrTmp_Values(19) & "')"
                '
                drTonTai = cTonTai.ExecuteDataReader(strSQL, CommandType.Text)
                If drTonTai.Read Then ' tim thay khoa
                    If fblnGhide Then ' cho phep ghi de
                        'strSQL = "Update TCS_DS_TOKHAI Set SO_PHAINOP = " & oToString(farrTmp_Values(7), "N") & _
                        '        " Where (MA_NNT = '" & farrTmp_Values(0) & "') And " & _
                        '        "(SO_TK = '" & farrTmp_Values(4) & "') And (NGAY_TK = '" & farrTmp_Values(5) & "') And " & _
                        '        strTmpWhere & " And (MA_MUC = '" & farrTmp_Values(18) & _
                        '        "') And (MA_TMUC = '" & farrTmp_Values(19) & "')"
                        strSQL = "Update TCS_DS_TOKHAI Set SO_PHAINOP = " & oToString(farrTmp_Values(7), "N") & _
                                " Where (MA_NNT = '" & farrTmp_Values(0) & "') And " & _
                                "(SO_TK = '" & farrTmp_Values(4) & "') And (NGAY_TK = '" & farrTmp_Values(5) & "') And " & _
                                strTmpWhere & " And (MA_TMUC = '" & farrTmp_Values(19) & "')"
                        fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)
                        blnTmpCheckValues = False ' da cap nhat roi khong can Insert du lieu
                        fblnUpdate = True ' dung de ket hop voi ham checkvalue va khong duoc tang so loi len
                        ' Cap nhat lai trang thai cua bang Tran_Up
                        Dim cInsert As New DataAccess

                        strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                                " Where TRAN_NO = '" & fstrTran_No & "'"
                        cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                        ' Giải phóng Connection tới CSDL
                        cInsert.Dispose()
                    Else ' trung khoa va khong cho phep ghi de
                        blnTmpCheckValues = False ' bo qua không duoc Insert du lieu
                        'TDTT-3438 - Mã ĐTNT+Kỳ thuế+... đã tồn tại trong sổ thuế
                        'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3438")

                        Dim cInsert As New DataAccess
                        ' Cap nhat lai trang thai cua bang Tran_Up
                        strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                                " Where TRAN_NO = '" & fstrTran_No & "'"
                        cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                        ' Giải phóng Connection tới CSDL
                        cInsert.Dispose()
                    End If
                Else ' khong bi trung khoa, khong loi va cho phep ghi de
                    ' Cap nhat lai trang thai cua bang Tran_Up
                    Dim cInsert As New DataAccess

                    strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                            "Where TRAN_NO = '" & fstrTran_No & "'"
                    cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                    ' Giải phóng Connection tới CSDL
                    cInsert.Dispose()
                End If
                ' Giải phóng Connection tới CSDL
                drTonTai.Dispose()
                cTonTai.Dispose()
            Else ' bang Detail khong bi loi
                ' Giao dich nay khong co bang Detail
            End If
        Else ' co loi chi tiet trong Master hoac Detail
            Dim cInsert As New DataAccess

            blnTmpCheckValues = False ' khong cho phep Insert du lieu
            ' Cap nhat lai trang thai cua bang Tran_Up
            strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                    "Where TRAN_NO = '" & fstrTran_No & "'"
            cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
            ' Giải phóng Connection tới CSDL
            cInsert.Dispose()
        End If
        Return blnTmpCheckValues
    End Function

    Private Function CheckValues(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
        ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
            ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                    ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
        ' Tham số:
        ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
        '                   + False:Không cho phép Insert dữ liệu
        ' Người viết: Nguyễn Bá Cường
        ' Ngày viết: 10/11/2007
        ' ----------------------------------------------------------------
        Dim blnTmpCheckValues As Boolean = True
        '
        Select Case fstrTsn_Code
            Case "01"  ' Kiem tra danh muc cap chuong
                blnTmpCheckValues = CheckValue_CapChuong(fstrTsn_Code, fblnGhide, fstrIsMaster, _
                                                    farrTmp_Values, fstrId_ImEx, fstrId_Mess, _
                                                    fstrTran_No, fblnUpdate, fketnoiToInsert, ftranInsert)
            Case "02" ' Kiem tra danh muc loai khoan
                blnTmpCheckValues = CheckValue_Loai_Khoan(fstrTsn_Code, fblnGhide, fstrIsMaster, _
                                                    farrTmp_Values, fstrId_ImEx, fstrId_Mess, _
                                                    fstrTran_No, fblnUpdate, fketnoiToInsert, ftranInsert)
            Case "03"  ' Kiem tra danh muc tieu muc
                blnTmpCheckValues = CheckValue_Muc_Tmuc(fstrTsn_Code, fblnGhide, fstrIsMaster, _
                                                    farrTmp_Values, fstrId_ImEx, fstrId_Mess, _
                                                    fstrTran_No, fblnUpdate, fketnoiToInsert, ftranInsert)
            Case "05"  ' Kiem tra danh muc tieu muc
                blnTmpCheckValues = CheckValue_Diaban(fstrTsn_Code, fblnGhide, fstrIsMaster, _
                                                    farrTmp_Values, fstrId_ImEx, fstrId_Mess, _
                                                    fstrTran_No, fblnUpdate, fketnoiToInsert, ftranInsert)
            Case "09"  ' Kiem tra danh muc nguoi nop thue
                blnTmpCheckValues = CheckValue_NNT(fstrTsn_Code, fblnGhide, fstrIsMaster, _
                                                    farrTmp_Values, fstrId_ImEx, fstrId_Mess, _
                                                    fstrTran_No, fblnUpdate, fketnoiToInsert, ftranInsert)
            Case "21"  ' Kiem tra so thue
                blnTmpCheckValues = CheckValue_SoThue(fstrTsn_Code, fblnGhide, fstrIsMaster, _
                                                    farrTmp_Values, fstrId_ImEx, fstrId_Mess, _
                                                    fstrTran_No, fblnUpdate, fketnoiToInsert, ftranInsert)
            Case "25"  ' Kiem tra danh sach to khai
                blnTmpCheckValues = CheckValue_ToKhai(fstrTsn_Code, fblnGhide, fstrIsMaster, _
                                                    farrTmp_Values, fstrId_ImEx, fstrId_Mess, _
                                                    fstrTran_No, fblnUpdate, fketnoiToInsert, ftranInsert)
            Case "62"  ' Kiem chung tu nhan
                blnTmpCheckValues = CheckValue_ChungTu(fstrTsn_Code, fblnGhide, fstrIsMaster, _
                                                    farrTmp_Values, fstrId_ImEx, fstrId_Mess, _
                                                    fstrTran_No, fblnUpdate, fketnoiToInsert, ftranInsert)

        End Select
        Return blnTmpCheckValues
    End Function

    Public Function CheckExistCTuTHop(ByVal strSHKB As String, ByVal strNGAY_KB _
        As String, ByVal strMA_NV As String, ByVal strSO_BT As String, ByVal strMA_DTHU As String) As Boolean
        Dim cnChungTu As DataAccess
        Dim drChungTu As IDataReader
        Dim lbnResult As Boolean = False
        Dim strSQL As String = "Select SHKB, NGAY_KB, MA_NV, SO_BT, MA_DTHU " & _
            "FROM TCS_CTU_THOP_HDR " & _
            "WHERE SHKB='" & strSHKB & "' AND NGAY_KB = " & strNGAY_KB & " AND MA_NV = " & strMA_NV & " " & _
            "AND SO_BT = " & strSO_BT & " AND MA_DTHU = '" & strMA_DTHU & "'"
        Try
            cnChungTu = New DataAccess
            drChungTu = cnChungTu.ExecuteDataReader(strSQL, CommandType.Text)
            If drChungTu.Read Then lbnResult = True
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi kiểm tra trùng lặp chứng từ")
            Throw ex
            'LogDebug.Writelog("Có lỗi xảy ra khi kiểm tra trùng lặp chứng từ: " & ex.ToString)
        Finally
            If Not drChungTu Is Nothing Then drChungTu.Close()
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return lbnResult
    End Function

    Public Function CheckExistCTuTHopDtl(ByVal strSHKB As String, ByVal strNGAY_KB _
        As String, ByVal strMA_NV As String, ByVal strSO_BT As String, ByVal strMA_DTHU As String, _
            ByVal strMA_CAP As String, ByVal strMA_CHUONG _
        As String, ByVal strMA_LOAI As String, ByVal strMA_KHOAN As String, ByVal strMA_MUC As String, ByVal strMA_TMUC As String) As Boolean
        Dim cnChungTu As DataAccess
        Dim drChungTu As IDataReader
        Dim lbnResult As Boolean = False
        Dim strSQL As String = "Select SHKB, NGAY_KB, MA_NV, SO_BT, MA_DTHU " & _
            "FROM TCS_CTU_THOP_DTL " & _
            "WHERE SHKB='" & strSHKB & "' AND NGAY_KB = " & strNGAY_KB & " AND MA_NV = " & strMA_NV & " " & _
            "AND SO_BT = " & strSO_BT & " AND MA_DTHU = '" & strMA_DTHU & "' AND " & _
            "MA_CAP='" & strMA_CAP & "' AND MA_CHUONG = '" & strMA_CHUONG & "' AND MA_LOAI = '" & strMA_LOAI & "' " & _
            "AND MA_KHOAN = '" & strMA_KHOAN & "' AND MA_MUC = '" & strMA_MUC & "' AND MA_TMUC = '" & strMA_TMUC & "'"
        Try
            cnChungTu = New DataAccess
            drChungTu = cnChungTu.ExecuteDataReader(strSQL, CommandType.Text)
            If drChungTu.Read Then lbnResult = True
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi kiểm tra trùng lặp chứng từ chi tiết")
            Throw ex
            'LogDebug.Writelog("Có lỗi xảy ra khi kiểm tra trùng lặp chứng từ chi tiết: " & ex.ToString)
        Finally
            If Not drChungTu Is Nothing Then drChungTu.Close()
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return lbnResult
    End Function

    Public Function WhereLcn_Recv(ByVal strTSNCode As String) As String
        Dim strtmp As String
        If "01,02,03,04,05,32,33".IndexOf(strTSNCode) >= 0 Then Return " (1=1) "
        strtmp = "((Substr(LCN_OWNER,4,5) ='" & _
                             Mid(gstrMaNoiLamViec, 4, 5) & "') Or (Substr(lcn_recv,4,5) ='" & _
                             Mid(gstrMaNoiLamViec, 4, 5) & "')) "
        Return strtmp
    End Function
    Private Function getID(ByVal strTabName As String) As String
        '-----------------------------------------------------------------
        ' Mục đích: Xác định các trường khoá phụ thuộc vào strTSNCode
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Select Case strTabName.ToUpper
            Case "TCS_DM_CAP_CHUONG"
                Return "CCH_ID"
            Case "TCS_DM_LOAI_KHOAN"
                Return "LKH_ID"
            Case "TCS_DM_MUC_TMUC"
                Return "MTM_ID"
            Case "TCS_DM_TINH"
                Return "TINH_ID"
            Case "TCS_DM_HUYEN"
                Return "HUYEN_ID"
            Case "TCS_DM_XA"
                Return "XA_ID"
            Case Else
                Return ""
        End Select

    End Function
#Region "TDTT-Tham so"
    Public Sub GetPreFix()
        '-----------------------------------------------------------------
        ' Mục đích: Thu tuc lay gia tri tien to cho cac truong khoa
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 12/02/2004
        ' Người viết: Nguyễn Quốc Việt
        ' ----------------------------------------------------------------
        Dim Conn As New DataAccess
        Dim drData As IDataReader
        Dim strSQL As String
        Try
            ' Xây dựng câu lệnh SQL theo điều kiện
            strSQL = "Select parm.prm_value,loca_lst.lcn_name " & _
                    "From parm parm,loca_lst loca_lst " & _
                    "Where (loca_lst.lcn_code = parm.prm_value) " & _
                    "And (parm.prm_name='NOI_LAM_VIEC')"
            drData = Conn.ExecuteDataReader(strSQL, CommandType.Text)
            If drData.Read Then
                gstrMaNoiLamViec = drData.GetValue(0)
                gstrTenNoiLamViec = drData.GetValue(1)
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy mã nơi làm việc")
            Throw ex
            'LogDebug.Writelog("Lỗi trong quá trình lấy mã nơi làm việc: " & ex.ToString)
        Finally
            If Not drData Is Nothing Then drData.Close()
            If Not Conn Is Nothing Then Conn.Dispose()
        End Try
    End Sub
    Public Sub GetTSTD()
        '-----------------------------------------------------------------
        ' Mục đích: Lấy các tham số phục vụ trao đổi
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 12/02/2008
        ' Người viết: Nguyễn Hữu Hoan
        ' ----------------------------------------------------------------
        Dim Conn As New DataAccess
        Dim strSql As String
        Dim dsTD As New DataSet
        Dim strTen As String
        Dim strGT As String
        Dim dr As DataRow
        Try
            strSql = "Select prm_name,prm_value From parm " & _
                     "where prm_name='SSG' or prm_name = 'DG' or prm_name='TTD' or " & _
                     "prm_name ='TND' Or prm_name ='FTN' or prm_name ='FTT' or prm_name ='SSN' " & _
                     "prm_name ='DN'"
            dsTD = Conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            If Not IsEmptyDataSet(dsTD) Then
                For Each dr In dsTD.Tables(0).Rows
                    strTen = UCase(dr(0).ToString.Trim())
                    If Not IsDBNull(dr(1)) Then strGT = dr(0).ToString().Trim
                    Select Case strTen
                        Case "SSG"
                            gstrSSG = strGT
                        Case "DG"
                            ' gstrDG = strGT
                        Case "SSG"
                            'gstrTTD = strGT
                        Case "SSN"
                            gstrSSN = strGT
                        Case "TND"
                            gstrTND = strGT
                        Case "DN"
                            gstrDN = strGT
                    End Select
                Next
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy tham số phục vụ trao đổi")
            Throw ex
        End Try
    End Sub
    Public Function GetTT_FLAG() As String
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra trạng thái truyền tin đang làm việc?
        ' Tham số:
        ' Giá trị trả về: 1 Nếu truyền tin đang làm việc
        '                 0 Nếu truyền tin không làm việc  
        ' Ngày viết: 01/06/2004
        ' Người viết: Nguyễn Quốc Việt
        ' ----------------------------------------------------------------
        Dim strSQL As String
        Dim drParameter As IDataReader
        Dim strTT As String = "0"
        Dim Ketnoi As New DataAccess
        Try
            strSQL = "SELECT prm_value " _
                           & " FROM parm" _
                           & " WHERE prm_name='TT_FLAG' "
            drParameter = Ketnoi.ExecuteDataReader(strSQL, CommandType.Text)
            If drParameter.Read Then
                strTT = drParameter.GetValue(0).trim
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi kiểm tra trạng thái truyền tin đang làm việc")
            Throw ex
            'LogDebug.Writelog("Lỗi khi kiểm tra trạng thái truyền tin đang làm việc: " & ex.ToString)
        Finally
            drParameter.Close()
            Ketnoi.Dispose()
        End Try
        Return strTT
    End Function

    Public Function GetTN_FLAG() As String
        '-----------------------------------------------------------------
        ' Mục đích: Hàm kiểm tra trạng thái tác nghiệp đang làm việc?
        ' Tham số:
        ' Giá trị trả về: 1 Nếu tác nghiệp đang làm việc
        '                 0 Nếu tác nghiệp không làm việc 
        ' Ngày viết: 01/06/2004
        ' Người viết: Nguyễn Quốc Việt
        ' ----------------------------------------------------------------
        Dim strSQL As String
        Dim drParameter As IDataReader
        Dim strTN As String = "0"
        Dim Ketnoi As New DataAccess
        Try
            strSQL = "SELECT prm_value " _
                           & " FROM parm" _
                           & " WHERE prm_name='TN_FLAG' "
            drParameter = Ketnoi.ExecuteDataReader(strSQL, CommandType.Text)
            If drParameter.Read Then
                strTN = drParameter.GetValue(0).trim
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi kiểm tra trạng thái tác nghiệp đang làm việc")
            Throw ex
            'LogDebug.Writelog("Lỗi khi kiểm tra trạng thái tác nghiệp đang làm việc: " & ex.ToString)
        Finally
            drParameter.Close()
            Ketnoi.Dispose()
        End Try
        Return strTN
    End Function

    Public Sub SetTN_FLAG(ByVal fblnLogOut As Boolean)
        '-----------------------------------------------------------------
        ' Mục đích: Thủ tục đặt trạng thái làm việc hoặc không làm việc
        ' Tham số: fblnLogOut
        ' Giá trị trả về: 1 Nếu tác nghiệp đang làm việc
        '                 0 Nếu tác không làm việc
        ' Ngày viết: 01/06/2004
        ' Người viết: Nguyễn Quốc Việt
        ' ----------------------------------------------------------------
        Dim strSQL As String
        Dim Ketnoi As New DataAccess
        Try
            If fblnLogOut Then
                strSQL = "UPDATE parm SET prm_value ='0' WHERE prm_name='TN_FLAG'"
            Else
                strSQL = "UPDATE parm SET prm_value ='1' WHERE prm_name='TN_FLAG'"
            End If
            Ketnoi.ExecuteNonQuery(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi đặt trạng thái làm việc hoặc không làm việc")
            Throw ex
            'LogDebug.Writelog("Lỗi khi đặt trạng thái làm việc hoặc không làm việc: " & ex.ToString)
        Finally
            Ketnoi.Dispose()
        End Try
    End Sub

    Public Function Lcn_CodeValue(ByVal strTsn_Code As String) As String
        '-----------------------------------------------------------------
        ' Mục đích:
        ' Tham số:
        ' Giá trị trả về:
        '                
        ' Ngày viết: 12/02/2004
        ' Người viết: Nguyễn Quốc Việt
        ' ----------------------------------------------------------------
        Dim ConnLcn As DataAccess
        Dim drDataLcn As IDataReader
        Dim strSQL As String
        Dim strTmp As String

        Try
            ConnLcn = New DataAccess

            strSQL = "Select LCN_CODE From RCV_LST Where (TSN_CODE = '" & strTsn_Code & "')"
            drDataLcn = ConnLcn.ExecuteDataReader(strSQL, CommandType.Text)
            If drDataLcn.Read Then
                strTmp = drDataLcn.GetValue(0)
            Else
                strTmp = ""
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin")
            Throw ex
        Finally
            If Not drDataLcn Is Nothing Then drDataLcn.Dispose()
            If Not ConnLcn Is Nothing Then ConnLcn.Dispose()
        End Try

        Return strTmp
    End Function
#End Region
#Region "Xu ly dieu kien tai khoan"
    Public Function DKTKhoan(ByVal strTKNO As String, ByVal strTKCo As String, _
                             ByVal strUser As String, ByVal strChucnang As String) As Boolean
        Dim cnTK As DataAccess
        Dim strSql As String
        Try
            cnTK = New DataAccess
            strSql = "Insert Into tcs_tk_check(tk_no,tk_co,chuc_nang,session_id) " & _
                     "Values('" & strTKNO & "','" & strTKCo & "','" & strChucnang & "','" & strUser & "')"
            cnTK.ExecuteNonQuery(strSql, CommandType.Text)
            Return True
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert điều kiện map tài khoản")
            Throw ex
            'LogDebug.Writelog("Lỗi trong quá trình insert điều kiện map tài khoản: " & ex.ToString)
            Return False
        Finally
            If Not cnTK Is Nothing Then cnTK.Dispose()
        End Try
    End Function
    Public Function XoaDKTkhoan(ByVal strUser As String, ByVal strChucnang As String) As Boolean
        Dim cnTK As DataAccess
        Dim strSql As String
        Try
            cnTK = New DataAccess
            strSql = "Delete From tcs_tk_check where chuc_nang='" & strChucnang & "' and session_id='" & strUser & "'"
            cnTK.ExecuteNonQuery(strSql, CommandType.Text)
            Return True
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình xóa điều kiện map tài khoản")
            Throw ex
            'LogDebug.Writelog("Lỗi trong quá trình xóa điều kiện map tài khoản: " & ex.ToString)
            Return False
        Finally
            If Not cnTK Is Nothing Then cnTK.Dispose()
        End Try
    End Function
#End Region
End Class
