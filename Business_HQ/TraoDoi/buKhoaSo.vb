﻿Namespace TraoDoi

    Public Class buKhoaSo
        Public Shared Function getChungTu(ByVal strTThai As String, ByVal strMaNV As String) As DataSet
            Dim daKS As New daKhoaSo
            Return daKS.getChungTu(strTThai, strMaNV)
        End Function

        Public Shared Function KhoaSo(ByVal strNgayLV As String, ByVal strNgayKS As String, _
                           ByVal strNgayDC As String, ByVal strMa_Dthu As String)
            Dim daKS As New daKhoaSo
            Return daKS.KhoaSo(strNgayLV, strNgayKS, strNgayDC, strMa_Dthu)
        End Function

        'Public Shared Function KhoaSo_DThu(ByVal strNgayLV As Long,  ByVal strMa_Dthu As String)
        '    Dim daKS As New daKhoaSo
        '    Return daKS.KhoaSo_Dthu(strNgayLV, strMa_Dthu)
        'End Function
        Public Shared Sub KhoaSo_DThu(ByVal strMa_Dthu As String)
            Dim daKS As New daKhoaSo
            daKS.KhoaSo_Dthu(strMa_Dthu)
        End Sub
        Public Shared Sub KX_CQThu(ByVal lngNgayKX As Long)
            Dim daKX As New daKhoaSo
            daKX.KX_CQThu(lngNgayKX)
        End Sub
        Public Shared Function KiemTraKS() As Boolean
            Dim daKS As New daKhoaSo
            Return daKS.KiemTraKS()
        End Function
        'ICB: Hàm kiểm tra xem còn tồn tại chứng từ đã kiểm soát mà chưa được in thành bảng kê hay ko?
        'Hoàng Văn Anh
        'Public Shared Function Is_DaIn_BK() As Boolean
        '    Dim daKS As New daKhoaSo
        '    Return daKS.Is_DaIn_BK()
        'End Function
        ''Public Function Get_BL(ByVal strTrangThai As String) As DataSet
        'Public Shared Function Get_BL(ByVal strTrangThai As String) As DataSet
        '    Dim daKS As New daKhoaSo
        '    Return daKS.Get_BL(strTrangThai)
        'End Function
    End Class

End Namespace