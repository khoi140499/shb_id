﻿Imports System.Data.OracleClient
Imports Business_HQ.Common
Imports VBOracleLib
Namespace nhothu
    Public Class daTCS_HQ247_314
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Shared Function TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA(ByVal pvStrID As String, ByVal pStrLyDoChuyenTra As String, ByVal pvStrTenDN As String) As Decimal
            Try
                'lay lai thong tin yeu cau va loai phan hoi la chap nhan yeu cau hay tu choi
                Dim strSQL As String = "UPDATE TCS_HQ247_314_TMP SET "
                strSQL &= "  id_nv_ck213='" & pvStrTenDN & "'"
                strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= " ,TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA & "'"
                strSQL &= " ,LY_DO_CHUYEN_TRA='" & pStrLyDoChuyenTra & "'"
                strSQL &= "  WHERE  ID='" & pvStrID & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA", "")
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        ''' <summary>
        ''' Kiểm tra số hồ sơ đã tồn tại hay chưa trên bảng HQ hoặc HQ_311 truyền vào
        ''' </summary>
        ''' <param name="strSoHS">Số hồ sơ cần kiểm tra</param>
        ''' <param name="strHQTableName">Tên bảng HQ hoặc HQ_311</param>
        ''' <param name="strWhereClause"> điều kiện kiểm tra</param>
        ''' <returns> boolRtn</returns>
        ''' <remarks></remarks>
        Public Shared Function checkSoHS(ByRef boolRtn As Boolean, ByVal strSoHS As String, ByVal strHQTableName As String, Optional ByVal strWhereClause As String = " (1=1) ") As Decimal
            Try
                Dim strSQL As String = "SELECT SO_HS FROM  " & strHQTableName & " "
                strSQL &= " WHERE SO_HS = '" & strSoHS & "' AND "
                strSQL &= strWhereClause
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If ds.Tables(0).Rows(0)("SO_HS").Equals("" & strSoHS & "") Then
                                boolRtn = True
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                boolRtn = False
                log.Error(ex.Message & "-" & ex.StackTrace) '    LogDebug.WriteLog(ex, "daTCS_HQ247_314.checkSoHS", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        Public Shared Function checkSoHSNew(ByVal strMa_DV As String) As Decimal
            Dim kq As Integer = 0
            Try
                Dim strSQL As String = "select * from TCS_HQ247_314 where MA_DV='" & strMa_DV & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("checkSoHSNew: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            'TCS_HQ247_OK
            'TCS_HQ247_SYSTEM_NOT_OK
            Try
                Dim strSQL As String = "select * from TCS_HQ247_314_TMP where MA_DV='" & strMa_DV & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog("checkSoHSNew: " & ex.StackTrace, EventLogEntryType.Error)
            End Try

            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        Public Shared Sub UpdateTCS_DM_NTDT_HQ_MSG314_DK_NH(ByVal pvStrSo_HS As String, ByVal pvStrSerialNumber As String, ByVal pvStrNoi_Cap As String, ByVal pvStrNgay_HL As String, _
                                                            ByVal pvStrNgay_HHL As String, ByVal pvStrPublicKey As String, ByVal pvStrMSGID311 As String, ByVal pvStrID As String)
            Dim conn As OracleConnection = VBOracleLib.DataAccess.GetConnection() ' New OracleConnection(VBOracleLib.DataAccess.strConnection)
            Dim myCMD As OracleCommand = New OracleCommand()
            Dim strSQL As String = ""
            Try
                'Dim strSQL As String = "UPDATE TCS_DM_NTDT_HQ SET "
                'huynt 
                strSQL = "UPDATE TCS_HQ247_314_TMP SET "
                'huynt end
                strSQL &= " TRANGTHAI='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "', "
                strSQL &= " SerialNumber='" & pvStrSerialNumber & "', "
                strSQL &= " NOICAP='" & pvStrNoi_Cap & "',"
                strSQL &= "  Ngay_HL=TO_DATE( '" & pvStrNgay_HL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "  Ngay_HHL=TO_DATE( '" & pvStrNgay_HHL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "  IDMSG311='" & pvStrMSGID311 & "',"
                ' ,to_date('" +  obj.Document.Data.ThongTin_NNT.ChungThuSo.Ngay_HL.Replace("T"," ") + "','RRRR-MM-DD HH24:MI:SS')
                'huynt
                strSQL &= "  NGAY_NHAN311 = to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS'),"
                strSQL &= "  NGAY_HT = to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS'),"
                strSQL &= "  ETAX_SERVICE ='Bổ sung TT NNT từ Msg 311',"
                'huynt end
                strSQL &= " PublicKey=:PublicKey "
                strSQL &= "  WHERE  So_HS='" & pvStrSo_HS & "'  AND TRANGTHAI='12' "

                ' conn.Open()
                myCMD.Connection = conn
                myCMD.CommandText = strSQL
                myCMD.CommandType = CommandType.Text
                myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pvStrPublicKey
                myCMD.ExecuteNonQuery()
                If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                    myCMD.Dispose()
                    conn.Dispose()
                    conn.Close()
                End If
            Catch ex As Exception
                Throw ex
            Finally
                If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                    conn.Close()
                End If
                myCMD.Dispose()
                conn.Dispose()
                'Insert sang bang HQ
                strSQL = "INSERT INTO TCS_HQ247_314 (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN, Ngay_HL_UQ  , Ngay_HHL_UQ)"
                strSQL &= " SELECT ID,SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN , CIFNO,TK_MA_CN , Ngay_HL_UQ  , Ngay_HHL_UQ"
                strSQL &= " FROM TCS_HQ247_314_TMP"
                strSQL &= " WHERE So_HS='" & pvStrSo_HS & "'  AND TRANGTHAI='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'Delete trong bang HQ_311
                strSQL = " DELETE FROM TCS_HQ247_314_TMP"
                strSQL &= " WHERE So_HS = '" & pvStrSo_HS & "'  AND TRANGTHAI = '" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
            End Try
        End Sub
        Public Shared Function fnc_CheckSua314_DKNH(ByVal pStrSoHS As String) As Decimal
            Try
                Dim strSQL As String = "select * from TCS_HQ247_314_TMP where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "') "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog("fnc_CheckSua314_DKNH: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_UpdateSua314_DKNH(ByVal pStrSoHS As String, ByVal pStrLoaiHS As String, _
                                                        ByVal pStrMA_DV As String, ByVal pStrTEN_DV As String, ByVal pStrDIACHI As String, ByVal pStrSO_CMT As String, _
                                                        ByVal pStrHO_TEN As String, ByVal pStrNGAYSINH As String, ByVal pStrNGUYENQUAN As String, ByVal pStrTHONGTINLIENHE As String, _
                                                        ByVal pStrSO_DT As String, ByVal pStrEMAIL As String, ByVal pStrSERIALNUMBER As String, ByVal pStrNOICAP As String, _
                                                        ByVal pStrNGAY_HL As String, ByVal pStrNGAY_HHL As String, ByVal pStrPUBLICKEY As String, ByVal pStrMA_NH_TH As String, _
                                                        ByVal pStrTEN_NH_TH As String, ByVal pStrTAIKHOAN_TH As String, ByVal pStrTEN_TAIKHOAN_TH As String, ByVal pStrIDMSG311 As String) As Decimal

            Try
                Dim conn As OracleConnection = VBOracleLib.DataAccess.GetConnection() 'New OracleConnection(VBOracleLib.DataAccess.strConnection)
                Dim myCMD As OracleCommand = New OracleCommand()
                Dim strSQL As String = "UPDATE TCS_HQ247_314_TMP SET "
                'cap nhat bang HQ_311
                strSQL &= " MA_DV='" & pStrMA_DV & "'"
                strSQL &= ",TEN_DV=:TEN_DV " '" & pStrTEN_DV.Replace("'", "''") & "'"
                strSQL &= ",DIACHI=:DIACHI " '" & pStrDIACHI & "'"
                strSQL &= ",SO_CMT='" & pStrSO_CMT & "'"
                strSQL &= ",HO_TEN=:HO_TEN " '" & pStrHO_TEN.Replace("'", "''") & "'"
                strSQL &= ",NGAYSINH=to_date('" & pStrNGAYSINH & "','RRRR-MM-DD')"
                strSQL &= ",NGUYENQUAN =:NGUYENQUAN " '" & pStrNGUYENQUAN & "'"
                strSQL &= ",    THONGTINLIENHE=:THONGTINLIENHE"
                strSQL &= ",SO_DT='" & pStrSO_DT & "'"
                strSQL &= ",EMAIL='" & pStrEMAIL & "'"
                strSQL &= ",SERIALNUMBER='" & pStrSERIALNUMBER & "'"
                strSQL &= ",NOICAP =:NOICAP " '" & pStrNOICAP & "'"
                strSQL &= ",NGAY_HL =to_date('" & pStrNGAY_HL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                strSQL &= ",NGAY_HHL=to_date('" & pStrNGAY_HHL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                strSQL &= ",    PUBLICKEY=:PUBLICKEY"
                strSQL &= ",MA_NH_TH='" & pStrMA_NH_TH & "'"
                strSQL &= ",TEN_NH_TH =:TEN_NH_TH " '" & pStrTEN_NH_TH & "'"
                strSQL &= ",TAIKHOAN_TH='" & pStrTAIKHOAN_TH & "'"
                strSQL &= ",TEN_TAIKHOAN_TH=:TEN_TAIKHOAN_TH " '" & pStrTEN_TAIKHOAN_TH.Replace("'", "''") & "'"
                strSQL &= ",TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "'"
                strSQL &= ",IDMSG311='" & pStrIDMSG311 & "'"
                strSQL &= ",NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= ",NGAY_HT =to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                'insert bang HQ
                'xoa bang HQ_311
                Try
                    ' conn.Open()
                    myCMD.Connection = conn
                    myCMD.CommandText = strSQL
                    myCMD.CommandType = CommandType.Text
                    myCMD.Parameters.Add("TEN_DV", OracleType.VarChar).Value = pStrTEN_DV
                    myCMD.Parameters.Add("DIACHI", OracleType.VarChar).Value = pStrDIACHI
                    myCMD.Parameters.Add("HO_TEN", OracleType.VarChar).Value = pStrHO_TEN
                    myCMD.Parameters.Add("NGUYENQUAN", OracleType.VarChar).Value = pStrNGUYENQUAN
                    myCMD.Parameters.Add("THONGTINLIENHE", OracleType.Clob).Value = pStrTHONGTINLIENHE
                    myCMD.Parameters.Add("NOICAP", OracleType.VarChar).Value = pStrNOICAP
                    myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pStrPUBLICKEY

                    myCMD.Parameters.Add("TEN_NH_TH", OracleType.VarChar).Value = pStrTEN_NH_TH
                    myCMD.Parameters.Add("TEN_TAIKHOAN_TH", OracleType.VarChar).Value = pStrTEN_TAIKHOAN_TH

                    myCMD.ExecuteNonQuery()
                    If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                        myCMD.Dispose()
                        conn.Dispose()
                        conn.Close()
                    End If
                Catch ex As Exception
                Finally
                    If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                        conn.Close()
                    End If
                    myCMD.Dispose()
                    conn.Dispose()
                End Try

                'Insert sang bang HQ
                strSQL = "INSERT INTO TCS_HQ247_314 (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN, Ngay_HL_UQ  , Ngay_HHL_UQ)"
                strSQL &= " SELECT ID,SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN , Ngay_HL_UQ  , Ngay_HHL_UQ "
                strSQL &= " FROM TCS_HQ247_314_TMP "
                strSQL &= " WHERE So_HS='" & pStrSoHS & "'  "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'Delete trong bang HQ_311
                strSQL = " DELETE FROM TCS_HQ247_314_TMP"
                strSQL &= " WHERE So_HS = '" & pStrSoHS & "'  "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_UpdateSua314_DKNH: " & ex.Message & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_Update314InfoHeader(ByVal pStrSoHS As String, ByVal pStrLoaiHS As String, _
                                                       ByVal pStrMA_DV As String, ByVal pStrTEN_DV As String, ByVal pStrDIACHI As String, ByVal pStrSO_CMT As String, _
                                                       ByVal pStrHO_TEN As String, ByVal pStrNGAYSINH As String, ByVal pStrNGUYENQUAN As String, ByVal pStrTHONGTINLIENHE As String, _
                                                       ByVal pStrSO_DT As String, ByVal pStrEMAIL As String, ByVal pStrSERIALNUMBER As String, ByVal pStrNOICAP As String, _
                                                       ByVal pStrNGAY_HL As String, ByVal pStrNGAY_HHL As String, ByVal pStrPUBLICKEY As String, _
                                                       ByVal pStrTAIKHOAN_TH As String, ByVal pStrIDMSG311 As String, ByVal pStrAccountHQ247 As String, ByVal pStrNgay_HL_UQ As String, ByVal pStrNgay_HHL_UQ As String) As Decimal
            Dim conn As OracleConnection = VBOracleLib.DataAccess.GetConnection() ' New OracleConnection(VBOracleLib.DataAccess.strConnection)
            Dim myCMD As OracleCommand = New OracleCommand()
            Try
                '---------------------------------------------------------------------------------
                'ngay_hl_uq,ngay_hhl_uq
                Dim strNgayHL_UQ As String = "null"
                Dim strNgayHHL_UQ As String = "null"
                If Not String.IsNullOrEmpty(pStrNgay_HL_UQ) Then
                    strNgayHL_UQ = "TO_DATE('" + pStrNgay_HL_UQ + "','RRRR-MM-DD')"
                End If
                If Not String.IsNullOrEmpty(pStrNgay_HHL_UQ) Then
                    strNgayHHL_UQ = "TO_DATE('" & pStrNgay_HHL_UQ & "','RRRR-MM-DD')"
                End If
                '---------------------------------------------------------------------------------
                'kiem tra xem bang HQ_314 co khong
                Dim strSQL As String = "SELECT ID,SO_HS,TRANGTHAI FROM TCS_HQ247_314_TMP WHERE SO_HS='" & pStrSoHS & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        'conn.Open()
                        strSQL = "UPDATE TCS_HQ247_314_TMP SET "
                        'cap nhat bang HQ_314
                        strSQL &= "loai_hs='" & pStrLoaiHS & "'"
                        strSQL &= ",MA_DV='" & pStrMA_DV & "'"
                        strSQL &= ",TEN_DV=:TEN_DV" '" & pStrTEN_DV.Replace("'", "''") & "'"
                        strSQL &= ",DIACHI=:DIACHI " '" & pStrDIACHI & "'"
                        strSQL &= ",SO_CMT='" & pStrSO_CMT & "'"
                        strSQL &= ",HO_TEN=:HO_TEN " '" & pStrHO_TEN.Replace("'", "''") & "'"
                        strSQL &= ",NGAYSINH=to_date('" & pStrNGAYSINH & "','RRRR-MM-DD')"
                        strSQL &= ",NGUYENQUAN =:NGUYENQUAN " '" & pStrNGUYENQUAN & "'"
                        strSQL &= ",THONGTINLIENHE=:THONGTINLIENHE"
                        strSQL &= ",SO_DT='" & pStrSO_DT & "'"
                        strSQL &= ",EMAIL='" & pStrEMAIL & "'"
                        strSQL &= ",SERIALNUMBER='" & pStrSERIALNUMBER & "'"
                        strSQL &= ",NOICAP =:NOICAP " '" & pStrNOICAP & "'"
                        strSQL &= ",NGAY_HL =to_date('" & pStrNGAY_HL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                        strSQL &= ",NGAY_HHL=to_date('" & pStrNGAY_HHL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                        strSQL &= ",PUBLICKEY=:PUBLICKEY"
                        ' strSQL &= ",IDMSG311='" & pStrIDMSG311 & "'"
                        strSQL &= ",TAIKHOAN_TH=(CASE WHEN TRANGTHAI IN ('09','11','12') THEN TAIKHOAN_TH ELSE   '" & pStrTAIKHOAN_TH & "' END)"
                        ' strSQL &= ",NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= ",ngay_hl_uq=" & strNgayHL_UQ
                        strSQL &= ",ngay_hhl_uq=" & strNgayHHL_UQ
                        strSQL &= ",NGAY_HT =to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                        If ((Not conn Is Nothing) And conn.State = ConnectionState.Closed) Then
                            conn.Open()
                        End If
                        myCMD.Connection = conn
                        myCMD.CommandText = strSQL
                        myCMD.CommandType = CommandType.Text
                        myCMD.Parameters.Add("TEN_DV", OracleType.VarChar).Value = pStrTEN_DV
                        myCMD.Parameters.Add("DIACHI", OracleType.VarChar).Value = pStrDIACHI
                        myCMD.Parameters.Add("HO_TEN", OracleType.VarChar).Value = pStrHO_TEN
                        myCMD.Parameters.Add("NGUYENQUAN", OracleType.VarChar).Value = pStrNGUYENQUAN
                        myCMD.Parameters.Add("THONGTINLIENHE", OracleType.VarChar).Value = pStrTHONGTINLIENHE
                        myCMD.Parameters.Add("NOICAP", OracleType.VarChar).Value = pStrNOICAP
                        myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pStrPUBLICKEY
                        myCMD.ExecuteNonQuery()
                        If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                            myCMD.Dispose()
                            conn.Dispose()
                            conn.Close()
                        End If
                    End If
                End If
                strSQL = "SELECT ID,SO_HS,TRANGTHAI FROM TCS_HQ247_314 WHERE SO_HS='" & pStrSoHS & "' "
                'kiem tra xem ban HQ co khong
                Dim dshq As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If dshq.Tables.Count > 0 Then
                    If dshq.Tables(0).Rows.Count > 0 Then
                        conn = New OracleConnection(VBOracleLib.DataAccess.strConnection)
                        myCMD = New OracleCommand()
                        'conn.Open()
                        strSQL = "UPDATE TCS_HQ247_314 SET "
                        'cap nhat bang HQ_311
                        strSQL &= "loai_hs='" & pStrLoaiHS & "'"
                        strSQL &= ",MA_DV='" & pStrMA_DV & "'"
                        strSQL &= ",TEN_DV=:TEN_DV " '" & pStrTEN_DV.Replace("'", "''") & "'"
                        strSQL &= ",DIACHI=:DIACHI " '" & pStrDIACHI & "'"
                        strSQL &= ",SO_CMT='" & pStrSO_CMT & "'"
                        strSQL &= ",HO_TEN=:HO_TEN " '" & pStrHO_TEN.Replace("'", "''") & "'"
                        strSQL &= ",NGAYSINH=to_date('" & pStrNGAYSINH & "','RRRR-MM-DD')"
                        strSQL &= ",NGUYENQUAN =:NGUYENQUAN " '" & pStrNGUYENQUAN & "'"
                        strSQL &= ",THONGTINLIENHE=:THONGTINLIENHE"
                        strSQL &= ",SO_DT='" & pStrSO_DT & "'"
                        strSQL &= ",EMAIL='" & pStrEMAIL & "'"
                        strSQL &= ",SERIALNUMBER='" & pStrSERIALNUMBER & "'"
                        strSQL &= ",NOICAP =:NOICAP " '" & pStrNOICAP & "'"
                        strSQL &= ",NGAY_HL =to_date('" & pStrNGAY_HL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                        strSQL &= ",NGAY_HHL=to_date('" & pStrNGAY_HHL.Replace("T", " ") & "','RRRR-MM-DD HH24:MI:SS')"
                        strSQL &= ",PUBLICKEY=:PUBLICKEY"
                        strSQL &= ",IDMSG311='" & pStrIDMSG311 & "'"
                        strSQL &= ",NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= ",ngay_hl_uq=" & strNgayHL_UQ
                        strSQL &= ",ngay_hhl_uq=" & strNgayHHL_UQ
                        strSQL &= ",NGAY_HT =to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                        If ((Not conn Is Nothing) And conn.State = ConnectionState.Closed) Then
                            conn.Open()
                        End If
                        myCMD.Connection = conn
                        myCMD.CommandText = strSQL
                        myCMD.CommandType = CommandType.Text
                        'myCMD.Parameters.Add("THONGTINLIENHE", OracleType.Clob).Value = pStrTHONGTINLIENHE
                        'myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pStrPUBLICKEY

                        myCMD.Parameters.Add("TEN_DV", OracleType.VarChar).Value = pStrTEN_DV
                        myCMD.Parameters.Add("DIACHI", OracleType.VarChar).Value = pStrDIACHI
                        myCMD.Parameters.Add("HO_TEN", OracleType.VarChar).Value = pStrHO_TEN
                        myCMD.Parameters.Add("NGUYENQUAN", OracleType.VarChar).Value = pStrNGUYENQUAN
                        myCMD.Parameters.Add("THONGTINLIENHE", OracleType.VarChar).Value = pStrTHONGTINLIENHE
                        myCMD.Parameters.Add("NOICAP", OracleType.VarChar).Value = pStrNOICAP
                        myCMD.Parameters.Add("PUBLICKEY", OracleType.Clob).Value = pStrPUBLICKEY
                        myCMD.ExecuteNonQuery()
                        If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                            myCMD.Dispose()
                            conn.Dispose()
                            conn.Close()
                        End If
                    End If
                End If

                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog("fnc_Update314InfoHeader: " & ex.StackTrace, EventLogEntryType.Error)
            Finally
                If ((Not conn Is Nothing) And conn.State <> ConnectionState.Closed) Then
                    conn.Close()
                End If
                myCMD.Dispose()
                conn.Dispose()
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_CheckSua314_CoSuaTK(ByVal pStrSoHS As String, ByVal pStrTAIKHOAN_TH As String) As Decimal
            Dim kq As Integer = 0
            Try
                Dim strSQL As String = "select * from TCS_HQ247_314 where so_hs='" & pStrSoHS & "' and TAIKHOAN_TH<>'" & pStrTAIKHOAN_TH & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_CheckSua314_CoSuaTK: " & ex.StackTrace, EventLogEntryType.Error)
            End Try


            Try
                Dim strSQL As String = "select * from TCS_HQ247_314_TMP where so_hs='" & pStrSoHS & "'  and cur_taikhoan_th<>'" & pStrTAIKHOAN_TH & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_CheckSua314_CoSuaTK: " & ex.StackTrace, EventLogEntryType.Error)
            End Try


            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_checkTrangThaiHS(ByVal pStrSoHS As String, ByVal pStrLoaiHS As String) As Decimal
            Dim decResult As Decimal = Common.mdlCommon.TCS_HQ247_OK
            Try
                Dim strSQL As String = ""
                Dim ds As DataSet = New DataSet()

                'co lenh dang cho thanh toan
                strSQL = "select count(1) from tcs_hq247_201 where so_hs='" & pStrSoHS & "' AND  trangthai not in('" & Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_SEND301_OK & "','" & Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213_RESPONSE_TUCHOI & "')"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)(0).ToString().Equals("0") Then
                                Return 1
                                'Return Common.mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE
                            End If
                        End If
                    End If
                End If

                ''kiem tra trong bang HQ
                If pStrLoaiHS.Equals("2") Then
                    strSQL = "select count(1) from  TCS_HQ247_314 where so_hs='" & pStrSoHS & "' AND trangthai  in('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "')"
                    ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                    decResult = Common.mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE
                    If Not ds Is Nothing Then
                        If ds.Tables.Count > 0 Then
                            If ds.Tables(0).Rows.Count > 0 Then
                                If Integer.Parse(ds.Tables(0).Rows(0)(0).ToString()) > 0 Then
                                    Return Common.mdlCommon.TCS_HQ247_OK
                                End If
                            End If
                        End If
                    End If
                    Return 2
                End If

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return decResult
        End Function
        Public Shared Function UPDATE_HQ_GET314_LOAI_HS_2(ByVal pvStrSo_HS As String) As Decimal
            Try
                Dim strSQL As String = "UPDATE TCS_HQ247_314 SET "
                strSQL &= " TRANGTHAI = '16'"
                strSQL &= "  WHERE  So_HS = '" & pvStrSo_HS & "' AND TRANGTHAI='13' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "daTCS_HQ247_314.UPDATE_HQ_GET314_LOAI_HS_2", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        Public Shared Function UPDATE_HQ314_GET314_LOAI_HS_2(ByVal pvStrSo_HS As String, ByVal pvStrCUR_TEN_TAIKHOAN_TH As String, ByVal pvStrCUR_TAIKHOAN_TH As String, ByVal pvStrTRANGTHAI As String) As Decimal
            Try
                Dim strSQL As String = "UPDATE TCS_HQ247_314_TMP SET "
                strSQL &= " CUR_TEN_TAIKHOAN_TH = '" & pvStrCUR_TEN_TAIKHOAN_TH & "'"
                strSQL &= " ,CUR_TAIKHOAN_TH = '" & pvStrCUR_TAIKHOAN_TH & "'"
                strSQL &= " ,TRANGTHAI = '" & pvStrTRANGTHAI & "'"
                strSQL &= "  WHERE  So_HS = '" & pvStrSo_HS & "' AND LOAI_HS = '2'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog(ex, "UPDATE_HQ314_GET314_LOAI_HS_2", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_OK
        End Function
        Public Shared Function fnc_UpdateHuy314_fromHQ(ByVal pStrSoHS As String, ByVal pStrLoaiHS As String, ByVal pStrMSG311ID As String, ByVal pStrMSGContent As String, ByVal AccountHQ247 As String) As Decimal
            Try
                Dim strSQL As String = "select * from TCS_HQ247_314 WHERE SO_HS='" & pStrSoHS & "'"
                'em kiem tra xem bang HQ co khong
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim strCheck As Decimal = 0
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        strCheck = ds.Tables(0).Rows.Count
                    End If

                End If
                If strCheck > 0 Then
                    strSQL = "UPDATE TCS_HQ247_314 set "
                    strSQL &= " LOAI_HS='" & pStrLoaiHS & "' "
                    strSQL &= ", TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "' "
                    strSQL &= ", IDMSG312='' "
                    strSQL &= ", IDMSG313='' "
                    strSQL &= ", IDMSG213='' "
                    strSQL &= ", ID_NV_MK213='' "
                    strSQL &= ", ID_NV_CK213='' "
                    strSQL &= ", ID_NV_MK312='" & AccountHQ247 & "' "
                    strSQL &= ", ID_NV_CK312='" & AccountHQ247 & "' "
                    strSQL &= ", IDMSG311='" & pStrMSG311ID & "' "
                    strSQL &= ", NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS') "
                    strSQL &= ", NGAY_HT=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS') "
                    strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                    VBOracleLib.DataAccess.ExecuteNonQuery("DELETE FROM TCS_HQ247_314_TMP WHERE SO_HS='" & pStrSoHS & "'", CommandType.Text)
                Else
                    strSQL = "UPDATE TCS_HQ247_314_TMP set "
                    strSQL &= " LOAI_HS='" & pStrLoaiHS & "' "
                    strSQL &= ", TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "' "
                    strSQL &= ", IDMSG312='' "
                    strSQL &= ", IDMSG313='' "
                    strSQL &= ", IDMSG213='' "
                    strSQL &= ", ID_NV_MK213='' "
                    strSQL &= ", ID_NV_CK213='' "
                    strSQL &= ", ID_NV_MK312='" & AccountHQ247 & "' "
                    strSQL &= ", ID_NV_CK312='" & AccountHQ247 & "' "
                    strSQL &= ", IDMSG311='" & pStrMSG311ID & "' "
                    strSQL &= ", NGAY_NHAN311=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS') "
                    strSQL &= ", NGAY_HT=to_date('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS') "
                    strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                    strSQL = "INSERT INTO TCS_HQ247_314 (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,Ngay_HL_UQ,Ngay_HHL_UQ)"
                    strSQL &= " SELECT ID,SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN ,Ngay_HL_UQ,Ngay_HHL_UQ "
                    strSQL &= " FROM TCS_HQ247_314_TMP"
                    strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                End If
                'Delete trong bang HQ_311
                strSQL = " DELETE FROM TCS_HQ247_314_TMP"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                strSQL = "INSERT INTO tcs_hq247_314_log (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,Ngay_HL_UQ,Ngay_HHL_UQ,TT_NGUNGDK)"
                strSQL &= " SELECT ID,SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN ,Ngay_HL_UQ,Ngay_HHL_UQ,'15' "
                strSQL &= " FROM tcs_hq247_314"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'Delete trong bang tcs_hq247_314
                strSQL = " DELETE FROM tcs_hq247_314"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'neu khong co thi 
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_UpdateHuy314_fromHQ: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function getMSG314FORTRACUUVAXULY(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID,A.SO_HS,A.MA_DV,A.LOAI_HS,C.MOTA TENLOAIHS,A.TEN_DV,A.TAIKHOAN_TH,TO_CHAR(A.NGAY_NHAN311,'DD/MM/RRRR') "
                strSQL &= " NGAY_NHAN311,A.TRANGTHAI,B.mota TRANG_THAI, 'JchkGridNO' URL,A.diachi,TO_CHAR( A.NGAY_HL_UQ,'DD/MM/RRRR') NGAY_HL_UQ,TO_CHAR( A.NGAY_HHL_UQ,'DD/MM/RRRR') NGAY_HHL_UQ,to_char(A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') as ngay_ht,"
                'strSQL &= " NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE  X.navigateurl like '%frmChitietTracuuXuly_DK.aspx'),'#') ||'?ID='||A.ID NAVIGATEURL,"
                strSQL &= " (CASE WHEN nvl(IDMSG311,'__')<> '__' then 'Export' else ''end) EXPORT311 ,IDMSG311 MSG_ID311, "
                strSQL &= " (CASE WHEN nvl(IDMSG213,'__')<> '__' then 'Export' else ''end) EXPORT213,IDMSG213 MSG_ID213 "
                strSQL &= " FROM TCS_HQ247_314 A, TCS_DM_TRANGTHAI_YEUCAU B , TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.LOAI_HS = C.LOAI_HS"
                strSQL &= " AND A.trangthai IN ('" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "')" & pvStrWhereClause
                strSQL &= "     UNION ALL "
                strSQL &= " SELECT A.ID,A.SO_HS,A.MA_DV,A.LOAI_HS,C.MOTA TENLOAIHS,A.TEN_DV,(case when a.trangthai in ("
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER312 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_CHUYENTRA & "',"
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "'"
                strSQL &= ",'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HOLD & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "') then nvl(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH  ) else A.TAIKHOAN_TH end )TAIKHOAN_TH,TO_CHAR(A.NGAY_NHAN311,'DD/MM/RRRR') "
                strSQL &= " NGAY_NHAN311,A.TRANGTHAI,B.mota TRANG_THAI,(case when A.TRANGTHAI in ("
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER312 & "'"
                strSQL &= ",'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_CHUYENTRA & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "',"
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "',"
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HOLD & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "',"
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "'"
                strSQL &= ") THEN 'JchkGridNO' ELSE 'JchkGrid' END) URL,A.diachi,TO_CHAR( A.NGAY_HL_UQ,'DD/MM/RRRR') NGAY_HL_UQ,TO_CHAR( A.NGAY_HHL_UQ,'DD/MM/RRRR') NGAY_HHL_UQ,to_char(A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') as ngay_ht,"
                'strSQL &= " NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.navigateurl like '%frmChitietTracuuXuly_DK.aspx'),'#') ||'?ID='||A.ID NAVIGATEURL,"
                strSQL &= " (CASE WHEN nvl(IDMSG311,'__')<> '__' then 'Export' else ''end) EXPORT311 ,IDMSG311 MSG_ID311, "
                strSQL &= " (CASE WHEN nvl(IDMSG213,'__')<> '__' then 'Export' else ''end) EXPORT213,IDMSG213 MSG_ID213 "
                strSQL &= " FROM TCS_HQ247_314_TMP A, TCS_DM_TRANGTHAI_YEUCAU B , TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.LOAI_HS = C.LOAI_HS " & pvStrWhereClause
                'strSQL &= "     UNION ALL "
                'strSQL &= " SELECT A.ID,A.SO_HS,A.MA_DV,A.LOAI_HS,C.MOTA TENLOAIHS,A.TEN_DV,A.TAIKHOAN_TH,TO_CHAR(A.NGAY_NHAN311,'DD/MM/RRRR') "
                'strSQL &= " NGAY_NHAN311,A.TRANGTHAI,B.MOTA TRANG_THAI,(CASE WHEN A.TRANGTHAI ='07' THEN 'JCHKGRIDNO' ELSE 'JCHKGRID' END) URL "
                'strSQL &= " FROM TCS_HQ_311_TMP A, TCS_DM_TRANGTHAI_YEUCAU B , TCS_DM_LOAI_HS_NNT C"
                'strSQL &= " WHERE(A.TRANGTHAI = B.TRANG_THAI And A.LOAI_HS = C.LOAI_HS)"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function MakerMSG213(ByVal pvStrID As String, ByVal pvStrNoidungXL As String, ByVal pvStrLoai213 As String, Optional ByVal pvStrTenDN As String = "") As Decimal
            Try
                Dim strSQL As String = "select * from TCS_HQ247_314 where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_TAMKHOA_CHODIEUCHINH) Then
                                Return mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'CAP NHAT THONG TIN VAO HQ 311
                If pvStrLoai213.Equals("1") Then
                    strSQL = "UPDATE TCS_HQ247_314_TMP SET TRANGTHAI='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213 & "'"
                Else
                    strSQL = "UPDATE TCS_HQ247_314_TMP SET TRANGTHAI='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213_TUCHOI & "'"
                End If

                strSQL &= " ,LY_DO_HUY=:LY_DO_HUY "
                strSQL &= " ,LOAIMSG213='" & pvStrLoai213 & "' "
                strSQL &= " ,ID_NV_MK213='" & pvStrTenDN & "' "
                strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                strSQL &= " WHERE ID='" & pvStrID & "' "
                Dim lst As List(Of IDataParameter) = New List(Of IDataParameter)
                lst.Add(DataAccess.NewDBParameter("LY_DO_HUY", ParameterDirection.Input, pvStrNoidungXL, DbType.String))
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray())
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function CheckerMSG213(ByVal pvStrID As String, ByVal pvStrNoidungXL As String, ByVal pvStrLoai213 As String, Optional ByVal pvStrTenDN As String = "") As Decimal
            Try
                Dim strSQL As String = "select * from TCS_HQ247_314 where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_TAMKHOA_CHODIEUCHINH) Then
                                Return mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'CAP NHAT THONG TIN VAO HQ 311
                If pvStrLoai213.Equals("1") Then
                    strSQL = "UPDATE TCS_HQ247_314_TMP SET TRANGTHAI='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213 & "'"
                Else
                    strSQL = "UPDATE TCS_HQ247_314_TMP SET TRANGTHAI='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213_TUCHOI & "'"
                End If
                strSQL &= " ,ID_NV_CK213='" & pvStrTenDN & "' "
                strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                strSQL &= " WHERE ID='" & pvStrID & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function UPDATE_TCS_HQ247_SEND213(ByVal pvStrTenDN As String, ByVal pvStrIDMSG213 As String, ByVal pvStrID As String) As Decimal
            Try
                'lay lai thong tin yeu cau va loai phan hoi la chap nhan yeu cau hay tu choi
                Dim strSQL As String = "SELECT * FROM  TCS_HQ247_314_TMP WHERE  ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim strLoai213 As String = "0"
                Dim strTrangThai As String = "01"
                Dim strLoaiHS As String = "1"
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            strLoai213 = ds.Tables(0).Rows(0)("LOAIMSG213").ToString()
                            strTrangThai = ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                            strLoaiHS = ds.Tables(0).Rows(0)("LOAI_HS").ToString()
                        End If
                    End If
                End If

                'Sửa lại đoạn lệnh bên trên
                If strLoai213.Equals("0") Or (Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213) _
                And Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213_TUCHOI)) Then
                    log.Error("Lỗi không tìm thấy hồ sơ hoặc trạng thái hồ sơ không hợp lệ id[" & pvStrID & "][" & pvStrTenDN & "]")
                    Return Common.mdlCommon.TCS_HQ247_HQ_HS_KHONG_HOPLE.ToString()
                End If

                strSQL = "UPDATE TCS_HQ247_314_TMP SET "
                If strLoai213.Equals("1") Then
                    strSQL &= " trangthai='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213 & "', "
                Else
                    strSQL &= " trangthai='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "', "
                End If
                strSQL &= "  id_nv_ck213='" & pvStrTenDN & "',idmsg213='" & pvStrIDMSG213 & "'"
                strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI in ('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213 & "','" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213_TUCHOI & "') "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                'doan nay danh cho tu choi yeu cau voi loai ho so la 2 va 3
                If strLoai213.Equals("2") And (strLoaiHS.Equals("2") Or strLoaiHS.Equals("3")) Then
                    'xoa dữ liệu trên bang HQ 311
                    strSQL = "DELETE FROM TCS_HQ247_314_TMP WHERE ID='" & pvStrID & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                    'cap nhat lai trang thai HQ ve active
                    strSQL = "UPDATE  TCS_HQ247_314 SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "' WHERE ID='" & pvStrID & "'"
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                End If
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog(ex, "daTCS_HQ247_314.UPDATE_TCS_HQ247_SEND213", "")
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function getMSG314(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, TO_CHAR( A.NGAY_HL_UQ,'DD/MM/RRRR') NGAY_HL_UQ,TO_CHAR( A.NGAY_HHL_UQ,'DD/MM/RRRR') NGAY_HHL_UQ,"

                strSQL &= "(case when a.trangthai in ("
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER312 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_CHUYENTRA & "',"
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "'"
                strSQL &= ",'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HOLD & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "') then nvl(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH  ) else A.TAIKHOAN_TH end ) TAIKHOAN_TH, "
                strSQL &= " C.MOTA TEN_LOAI_HS, "
                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311,  to_char(A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') as NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI, "
                strSQL &= "  NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME "
                strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MOINHAN & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213_TUCHOI & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213_TUCHOI & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "','08') THEN "
                strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '12%' AND X.NODEID IN ('12.1.21')),'#') ELSE"
                strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '12%' AND X.NODEID IN ('12.1.22')),'#')"
                strSQL &= "  END ) NAVIGATEURL "
                strSQL &= " FROM TCS_HQ247_314_TMP A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.LOAI_HS=C.LOAI_HS AND TRANGTHAI IN ('04','07','08','09','10','11','12')  " & pvStrWhereClause

                strSQL &= " UNION ALL "
                strSQL &= "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH,TO_CHAR( A.NGAY_HL_UQ,'DD/MM/RRRR') NGAY_HL_UQ,TO_CHAR( A.NGAY_HHL_UQ,'DD/MM/RRRR') NGAY_HHL_UQ,"

                strSQL &= "(case when a.trangthai in ("
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER312 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_CHUYENTRA & "',"
                strSQL &= "'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "'"
                strSQL &= ",'" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HOLD & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "') then nvl(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH  ) else A.TAIKHOAN_TH end ) TAIKHOAN_TH, "
                strSQL &= " C.MOTA TEN_LOAI_HS, "
                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311,to_char(A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') as NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI, "
                strSQL &= "  NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME "
                strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MOINHAN & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213_TUCHOI & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213_TUCHOI & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "','08') THEN "
                strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '12%' AND X.NODEID IN ('12.1.21')),'#') ELSE"
                strSQL &= "  NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '12%' AND X.NODEID IN ('12.1.22')),'#')"
                strSQL &= "  END ) NAVIGATEURL "
                strSQL &= " FROM TCS_HQ247_314 A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.LOAI_HS=C.LOAI_HS AND A.TRANGTHAI  IN ('13','15') " & pvStrWhereClause
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception

            End Try
            Return ds
        End Function
        Public Shared Function getMakerMSG213(ByVal pvStrID As String, Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP,TO_CHAR( A.NGAY_HL_UQ,'DD/MM/RRRR') NGAY_HL_UQ,TO_CHAR( A.NGAY_HHL_UQ,'DD/MM/RRRR') NGAY_HHL_UQ, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,THONGTINLIENHE "
                strSQL &= "  FROM TCS_HQ247_314_TMP A WHERE  1=1 "
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND ID='" & pvStrID & "' "
                End If
                If Not pvStrWhereClause.Equals("") Then
                    strSQL &= "  " & pvStrWhereClause & " "
                End If
                If Not pvStrOrderBy.Equals("") Then
                    strSQL &= "  ORDER BY " & pvStrOrderBy & " "
                End If
                strSQL &= " UNION ALL "
                strSQL &= "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP,TO_CHAR( A.NGAY_HL_UQ,'DD/MM/RRRR') NGAY_HL_UQ,TO_CHAR( A.NGAY_HHL_UQ,'DD/MM/RRRR') NGAY_HHL_UQ, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,THONGTINLIENHE "
                strSQL &= "  FROM TCS_HQ247_314 A WHERE  1=1 AND TRANGTHAI NOT IN ('15','16')"
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND ID='" & pvStrID & "' "
                End If
                If Not pvStrWhereClause.Equals("") Then
                    strSQL &= "  " & pvStrWhereClause & " "
                End If
                If Not pvStrOrderBy.Equals("") Then
                    strSQL &= "  ORDER BY " & pvStrOrderBy & " "
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception

            End Try
            Return ds
        End Function
        Public Shared Function getTTNgungDK(ByVal pvStrID As String, Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP,TO_CHAR( A.NGAY_HL_UQ,'DD/MM/RRRR') NGAY_HL_UQ,TO_CHAR( A.NGAY_HHL_UQ,'DD/MM/RRRR') NGAY_HHL_UQ, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,THONGTINLIENHE "
                strSQL &= "  FROM tcs_hq247_314_log A WHERE  1=1 "
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND ID='" & pvStrID & "' "
                End If
                If Not pvStrWhereClause.Equals("") Then
                    strSQL &= "  " & pvStrWhereClause & " "
                End If
                If Not pvStrOrderBy.Equals("") Then
                    strSQL &= "  ORDER BY " & pvStrOrderBy & " "
                End If

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception

            End Try
            Return ds
        End Function
        Public Shared Function MakerMSG312Sua(ByVal pvStrID As String, ByVal pvStrNgay_Ky_HDUQ As String, ByVal pvStrCurTaiKhoanTH As String, ByVal pvStrCurTenTaiKhoanTH As String, ByVal pvStrThongtinLienHe As String, Optional ByVal pvStrTenDN As String = "", Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMA_CNTK As String = "") As Decimal
            Try
                'Kiểm tra trong bảng chính, nếu đã có chứng từ ở trạng thái khác active (13) và khác hold (14) thì return
                Dim strSQL As String = "select * from TCS_HQ247_314 where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_TAMKHOA_CHODIEUCHINH) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_HOLD) Then
                                Return Business_HQ.Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'Lấy thông tin tài khoản NNT đăng ký để Nộp thuế điện tử
                'CAP NHAT THONG TIN VAO HQ 311
                'Do có cả 2 phương thức: đăng ký mới tại TCHQ và đăng ký mới tại NHTM đều sử dụng chung Form này, do đó để xác đinh
                'được đâu là đăng ký mới tại TCHQ và đâu là tại NHTM ta kiểm tra ID
                'Trường hợp xác định là đăng ký tại TCHQ
                If Not pvStrID.Equals("") Then
                    Dim maChiNhanh As String = "___"
                    If (Business_HQ.HQ247.daTCS_DM_NTDT_HQ.getCodebranch(maChiNhanh, pvStrTenDN)) = 0 Then
                        strSQL = "UPDATE TCS_HQ247_314_TMP SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER312 & "'"
                        'Định dạng lại pvStrNgay_Ky_HDUQ theo chuỗi yyyyMMddHHmmss
                        Dim strDateCover As String = pvStrNgay_Ky_HDUQ.Split("/")(2) & pvStrNgay_Ky_HDUQ.Split("/")(1) & pvStrNgay_Ky_HDUQ.Split("/")(0)
                        strSQL &= " ,NGAY_KY_HDUQ=to_date('" & strDateCover & "','RRRRMMDDHH24MISS') ,TAIKHOAN_TH=CUR_TAIKHOAN_TH, TEN_TAIKHOAN_TH=CUR_TEN_TAIKHOAN_TH "
                        strSQL &= " ,CUR_TEN_TAIKHOAN_TH = '" & pvStrCurTenTaiKhoanTH & "' "
                        strSQL &= " ,CUR_TAIKHOAN_TH = '" & pvStrCurTaiKhoanTH & "' "
                        strSQL &= " ,MA_CN = '" & maChiNhanh & "' "
                        strSQL &= " ,CIFNO = '" & pvStrCIFNO & "' "
                        strSQL &= " ,TK_MA_CN = '" & pvStrMA_CNTK & "' "
                        strSQL &= " ,THONGTINLIENHE = '" & pvStrThongtinLienHe & "' "
                        strSQL &= " ,ID_NV_MK312='" & pvStrTenDN & "' "
                        strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                        strSQL &= " WHERE ID='" & pvStrID & "' "
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                    Else
                        Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                    End If

                End If
            Catch ex As Exception
            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function MakerMSG312(ByVal pvStrID As String, ByVal pvStrNgay_Ky_HDUQ As String, ByVal pvStrCurTaiKhoanTH As String, ByVal pvStrCurTenTaiKhoanTH As String, ByVal pvStrThongtinLienHe As String, ByVal pvStrNgay_HL_UQ As String, ByVal pvStrNgay_HHL_UQ As String, Optional ByVal pvStrTenDN As String = "", Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMaCNTK As String = "") As Decimal
            Try
                'Kiểm tra trong bảng chính, nếu đã có chứng từ ở trạng thái khác active (13) và khác hold (14) thì return
                Dim strSQL As String = "select * from TCS_HQ247_314 where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_TAMKHOA_CHODIEUCHINH) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_HOLD) Then
                                Return Business_HQ.Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'Lấy thông tin tài khoản NNT đăng ký để Nộp thuế điện tử
                'CAP NHAT THONG TIN VAO HQ 311
                'Do có cả 2 phương thức: đăng ký mới tại TCHQ và đăng ký mới tại NHTM đều sử dụng chung Form này, do đó để xác đinh
                'được đâu là đăng ký mới tại TCHQ và đâu là tại NHTM ta kiểm tra ID
                'Trường hợp xác định là đăng ký tại TCHQ
                If Not pvStrID.Equals("") Then
                    Dim maChiNhanh As String = "___"
                    If (Business_HQ.HQ247.daTCS_DM_NTDT_HQ.getCodebranch(maChiNhanh, pvStrTenDN)) = 0 Then
                        strSQL = "UPDATE TCS_HQ247_314_TMP SET TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER312 & "'"
                        'Định dạng lại pvStrNgay_Ky_HDUQ theo chuỗi yyyyMMddHHmmss
                        Dim strDateCover As String = pvStrNgay_Ky_HDUQ.Split("/")(2) & pvStrNgay_Ky_HDUQ.Split("/")(1) & pvStrNgay_Ky_HDUQ.Split("/")(0)
                        strSQL &= " ,NGAY_KY_HDUQ=to_date('" & strDateCover & "','RRRRMMDDHH24MISS') "
                        strSQL &= " ,NGAY_HL_UQ=to_date('" & pvStrNgay_HL_UQ & "','DD/MM/RRRR') "
                        strSQL &= " ,NGAY_HHL_UQ=to_date('" & pvStrNgay_HHL_UQ & "','DD/MM/RRRR') "
                        strSQL &= " ,CIFNO = '" & pvStrCIFNO & "' "
                        strSQL &= " ,TK_MA_CN = '" & pvStrMaCNTK & "' "
                        strSQL &= " ,CUR_TEN_TAIKHOAN_TH =:CUR_TEN_TAIKHOAN_TH  "
                        strSQL &= " ,CUR_TAIKHOAN_TH = '" & pvStrCurTaiKhoanTH & "' "
                        strSQL &= " ,MA_CN = '" & maChiNhanh & "' "
                        strSQL &= " ,THONGTINLIENHE =:THONGTINLIENHE  "
                        strSQL &= " ,ID_NV_MK312='" & pvStrTenDN & "' "
                        strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                        strSQL &= " WHERE ID='" & pvStrID & "' "
                        Dim lsParam As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                        lsParam.Add(VBOracleLib.DataAccess.NewDBParameter("CUR_TEN_TAIKHOAN_TH", ParameterDirection.Input, pvStrCurTenTaiKhoanTH, DbType.String))
                        lsParam.Add(VBOracleLib.DataAccess.NewDBParameter("THONGTINLIENHE", ParameterDirection.Input, pvStrThongtinLienHe, DbType.String))
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lsParam.ToArray())
                        Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK
                    Else
                        Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                    End If

                End If
            Catch ex As Exception
            End Try
            Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function getCheckerMSG213(Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "", Optional ByVal usernameName As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String
                strSQL &= "  SELECT B.MOTA TRANGTHAI,A.MA_DV ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,A.ID, A.TAIKHOAN_TH"
                strSQL &= "  FROM TCS_HQ247_314_TMP A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_NHANVIEN C, TCS_DM_NHANVIEN D WHERE A.TRANGTHAI=B.TRANG_THAI "
                strSQL &= "  AND (A.TRANGTHAI IN ('" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213_TUCHOI & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER213_TUCHOI & "') OR (A.trangthai='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI & "' AND TRUNC(A.ngay_ht)=TRUNC(SYSDATE)) ) AND UPPER(A.ID_NV_MK213) = UPPER(C.TEN_DN) AND C.MA_CN=D.MA_CN "
                strSQL &= "  AND UPPER(D.TEN_DN)=UPPER('" + usernameName + "') "
                If pvStrWhereClause <> "" Then
                    strSQL &= pvStrWhereClause
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
            End Try
            Return ds
        End Function

        Public Shared Function UPDATE_TRANGTHAI_TRUYENTRA_213(ByVal ID As String, ByVal tenDN As String) As Decimal
            Try
                Dim strSQL As String = ""
                strSQL = "SELECT ID, SO_HS, TRANGTHAI FROM  TCS_HQ247_314_TMP  WHERE ID = '" & ID & "' AND A.TRANGTHAI IN ('" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER213_TUCHOI & "') "
                'Kiểm tra trường hợp đăng ký từ TCHQ hay từ NHTM
                Dim dsHQ311 As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not dsHQ311 Is Nothing Then
                    If dsHQ311.Tables.Count > 0 Then
                        If dsHQ311.Tables(0).Rows.Count > 0 Then
                            strSQL = "UPDATE TCS_HQ247_314_TMP SET "
                            strSQL &= " TRANGTHAI = '08'" 'truyen tra
                            ' strSQL &= " ,ID_NV_CK213='" & tenDN & "' "
                            ' strSQL &= " ,ID_NV = '" & tenDN & "' "
                            strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                            strSQL &= " WHERE  ID = '" & ID & "' AND "

                            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If


                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog(ex, "daTCS_DM_NTDT_HQ.UPDATE_TRANGTHAI", "")
                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            Finally
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function getMaker312(Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "", Optional ByVal usernameName As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = ""
                strSQL &= "  SELECT B.MOTA TRANGTHAI,A.MA_DV ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,A.ID, A.CUR_TAIKHOAN_TH"
                strSQL &= "  FROM TCS_HQ247_314_TMP A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_NHANVIEN C, TCS_DM_NHANVIEN D WHERE A.TRANGTHAI=B.TRANG_THAI "
                strSQL &= "  AND (A.TRANGTHAI IN ('" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER312 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_CHUYENTRA & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_HOLD & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "') ) AND UPPER(A.ID_NV_MK312) = UPPER(C.TEN_DN) AND C.MA_CN=D.MA_CN "
                strSQL &= "  AND UPPER(D.TEN_DN)=UPPER('" + usernameName + "') "
                If pvStrWhereClause <> "" Then
                    strSQL &= pvStrWhereClause
                End If
                strSQL &= " UNION ALL "
                strSQL &= "  SELECT B.MOTA TRANGTHAI,A.MA_DV ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,A.ID, A.CUR_TAIKHOAN_TH"
                strSQL &= "  FROM TCS_HQ247_314 A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_NHANVIEN C, TCS_DM_NHANVIEN D WHERE A.TRANGTHAI=B.TRANG_THAI "
                strSQL &= "  AND (A.TRANGTHAI IN ('" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "','" & mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "') AND TRUNC(ngay_ht) = TRUNC(SYSDATE)  ) AND UPPER(A.ID_NV_MK312) = UPPER(C.TEN_DN) AND C.MA_CN=D.MA_CN "
                strSQL &= "  AND UPPER(D.TEN_DN)=UPPER('" + usernameName + "')  "
                If pvStrWhereClause <> "" Then
                    strSQL &= pvStrWhereClause
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
            End Try
            Return ds
        End Function
        ''' <summary>
        ''' KSV (Maker)Kiểm soát thông tin đăng ký NNT
        ''' </summary>
        ''' <param name="pvStrID">ID của Đăng ký</param>
        ''' <param name="pvStrNoidungXL">Lý do Chuyển trả trong trường hợp thực hiện chuyển trả</param>
        ''' <param name="pvStrBtnDuyet"> Mã chức năng: truyền 1: nếu thực hiện Duyệt, truyền 0 nếu thực hiện chuyển trả</param>
        ''' <param name="pvStrTenDN">Tên đăng nhập của KSV</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CheckerMSG312(ByVal pvStrID As String, ByVal pvStrNoidungXL As String, ByVal pvStrBtnDuyet As String, Optional ByVal pvStrTenDN As String = "") As Decimal
            Try
                'Kiểm tra NNT, nếu đã đăng ký thành công và đang hoạt động hoặc đang bị tạm ngưng (hold) hoạt động thì báo lỗi cho Checker
                Dim strSQL As String = "select * from TCS_HQ247_314 where ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_HOLD) _
                            And Not ds.Tables(0).Rows(0)("TRANGTHAI").Equals(mdlCommon.TCS_HQ247_314_TRANGTHAI_TAMKHOA_CHODIEUCHINH) Then
                                Return mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                            End If
                        End If
                    End If
                End If
                'update trạng thái của Đăng ký trong bảngTCS_DM_NTDT_HQ_311
                'Nếu nhận được yêu cầu Duyệt
                If pvStrBtnDuyet.Equals("1") Then
                    strSQL = "UPDATE TCS_HQ247_314_TMP SET TRANGTHAI='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "'"
                Else
                    'Nếu nhận được yêu cầu là Chuyển trả
                    strSQL = "UPDATE TCS_HQ247_314_TMP SET TRANGTHAI='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_CHUYENTRA & "'"
                End If
                strSQL &= " ,ID_NV_CK312='" & pvStrTenDN & "' "
                strSQL &= " ,NGAY_HT=to_date('" & Date.Now.ToString("yyyyMMddHHmmss") & "','RRRRMMDDHH24MISS') "
                strSQL &= " WHERE ID='" & pvStrID & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function UPDATE_TCS_HQ247_SEND314(ByVal pvStrTenDN As String, ByVal pvStrIDMSG312 As String, ByVal pvStrID As String, Optional ByVal strSoHS_HQ As String = "") As Decimal
            Try
                'lay lai thong tin yeu cau va loai phan hoi la chap nhan yeu cau hay tu choi
                Dim strSQL As String = "SELECT * FROM  TCS_HQ247_314_TMP WHERE  ID='" & pvStrID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim strLoai213 As String = "0"
                Dim strTrangThai As String = "01"
                Dim strLoaiHS As String = "1"
                Dim strSo_HS As String = ""
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            strLoai213 = ds.Tables(0).Rows(0)("LOAIMSG213").ToString()
                            strTrangThai = ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                            strLoaiHS = ds.Tables(0).Rows(0)("LOAI_HS").ToString().Trim()
                            strSo_HS = ds.Tables(0).Rows(0)("SO_HS").ToString().Trim()
                        End If
                    End If
                Else
                    Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK 'Nếu xảy ra trường hợp này thì do không truy vấn được dữ liệu chứ không phải do không tồn tại số hồ sơ
                End If
                'Msg 313 nhận về có 3 trường hợp là của Khai mới, Khai sửa và Khai hủy
                'Ứng với mỗi trường hợp sẽ có xử lý khác nhau, do đó cần xác định được msg 313 là của trường hợp nào
                Select Case strLoaiHS
                    Case "1"
                        'Trường hợp 1: Msg 313 là của trường hợp Khai mới

                        'kiểm tra lại đoạn điều kiện này
                        'If strLoai213.Equals("0") Or (Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213) _
                        'And Not strTrangThai.Equals(Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER213_TUCHOI)) Then
                        '    LogDebug.WriteLog(New Exception("Lỗi không tìm thấy hồ sơ hoặc trạng thái hồ sơ không hợp lệ id[" & pvStrID & "][" & pvStrTenDN & "]"), "daTCS_HQ247_314.UPDATE_TCS_HQ247_SEND213", "")
                        '    Return Common.mdlCommon.TCS_HQ247_OK
                        'End If

                        'Kiểm tra So_HS mà TCHQ gửi về xem nó đã có chưa, nếu có rồi chứng tỏ đây là Msg 313 TCHQ trả lời cho đăng ký mới tại TCHQ
                        If Not strSo_HS.Trim.Equals("") Then
                            'Trường hợp 313 TCHQ trả lời cho đăng ký mới tại TCHQ
                            'Chuyen trang thai len Active (13)
                            strSQL = "UPDATE TCS_HQ247_314_TMP SET "
                            strSQL &= "  TRANGTHAI = '" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "',"
                            strSQL &= "  IDMSG313 = '" & pvStrID & "',TAIKHOAN_TH=CUR_TAIKHOAN_TH,TEN_TAIKHOAN_TH=CUR_TEN_TAIKHOAN_TH, "
                            strSQL &= "  id_nv_ck312='" & pvStrTenDN & "',idmsg312='" & pvStrIDMSG312 & "'"
                            strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                            strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI = ('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "') "
                            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                            'Insert sang bang HQ
                            strSQL = "INSERT INTO TCS_HQ247_314 (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,NGAY_HL_UQ,NGAY_HHL_UQ)"
                            strSQL &= " SELECT '" & pvStrID & "',SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,NGAY_HL_UQ,NGAY_HHL_UQ "
                            strSQL &= " FROM TCS_HQ247_314_TMP"
                            strSQL &= " WHERE ID = '" & pvStrID & "'"
                            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                            'Delete trong bang HQ_311
                            strSQL = " DELETE FROM TCS_HQ247_314_TMP "
                            strSQL &= "  WHERE ID = '" & pvStrID & "'"
                            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                            ''doan nay danh cho tu choi yeu cau voi loai ho so la 2 va 3
                            'If strLoai213.Equals("2") And (strLoaiHS.Equals("2") Or strLoaiHS.Equals("3")) Then
                            '    'xoa dữ liệu trên bang HQ 311
                            '    strSQL = "DELETE FROM TCS_DM_NTDT_HQ_311 WHERE ID='" & pvStrID & "'"
                            '    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                            '    'cap nhat lai trang thai HQ ve active
                            '    strSQL = "UPDATE  TCS_DM_NTDT_HQ SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "' WHERE ID='" & pvStrID & "'"
                            '    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                            'End If
                            Return Common.mdlCommon.TCS_HQ247_OK
                        Else
                            Try
                                'Chuyen TRANGTHAI của Đăng ký từ '11' (Đã kiểm soát - chờ xác thực của TCHQ) lên '12' (Đã kiểm soát chờ bổ sung thông tin NNT)
                                strSQL = "UPDATE TCS_HQ247_314_TMP SET "
                                strSQL &= "  TRANGTHAI = '" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM & "',"
                                strSQL &= "  SO_HS = '" & strSoHS_HQ & "',TAIKHOAN_TH=CUR_TAIKHOAN_TH,TEN_TAIKHOAN_TH=CUR_TAIKHOAN_TH,"
                                strSQL &= "  IDMSG313 = '" & pvStrID & "',"
                                strSQL &= "  id_nv_ck312='" & pvStrTenDN & "',idmsg312='" & pvStrIDMSG312 & "'"
                                strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                                strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI = ('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "') "
                                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                            Catch ex As Exception
                                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog(ex, "daTCS_HQ247_314.UPDATE_TCS_HQ247_SEND312", "")
                                Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
                            End Try
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If


                    Case "2"
                        'Trường hợp 2: Msg 313 là của trường hợp Khai sửa
                        'Chuyen trang thai len Active (13)
                        strSQL = "UPDATE TCS_HQ247_314_TMP SET "
                        strSQL &= "  TRANGTHAI = '" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "',TAIKHOAN_TH=CUR_TAIKHOAN_TH,TEN_TAIKHOAN_TH=CUR_TEN_TAIKHOAN_TH,"
                        strSQL &= "  IDMSG313 = '" & pvStrID & "',"
                        strSQL &= "  id_nv_ck312='" & pvStrTenDN & "',idmsg312='" & pvStrIDMSG312 & "'"
                        strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI = ('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "') "
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        'Kiểm tra số HS này xem đã có trong bảng HQ chưa, nếu có thì delete trước khi insert record  mới
                        Dim dsHQ As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet("SELECT SO_HS FROM TCS_HQ247_314 WHERE SO_HS='" & strSo_HS & "' AND TRANGTHAI = '16' ", CommandType.Text)
                        If Not dsHQ Is Nothing Then
                            If dsHQ.Tables.Count > 0 Then
                                If dsHQ.Tables(0).Rows.Count > 0 Then
                                    VBOracleLib.DataAccess.ExecuteNonQuery("DELETE FROM TCS_HQ247_314 WHERE  SO_HS='" & strSo_HS & "' AND TRANGTHAI = '16' ", CommandType.Text)
                                End If
                            End If
                        End If
                        'Insert sang bang HQ
                        strSQL = "INSERT INTO TCS_HQ247_314 (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,NGAY_HL_UQ,NGAY_HHL_UQ)"
                        strSQL &= " SELECT '" & pvStrID & "',SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,NGAY_HL_UQ,NGAY_HHL_UQ "
                        strSQL &= " FROM TCS_HQ247_314_TMP"
                        strSQL &= " WHERE ID = '" & pvStrID & "'"
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                        'Delete trong bang HQ_311
                        strSQL = " DELETE FROM TCS_HQ247_314_TMP "
                        strSQL &= "  WHERE ID = '" & pvStrID & "'"
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        Return Common.mdlCommon.TCS_HQ247_OK
                    Case "3"
                        'Trường hợp 3: Msg 313 là của trường hợp Khai hủy

                        'Chuyen trang thai len Active (13)
                        strSQL = "UPDATE TCS_HQ247_314_TMP SET "
                        strSQL &= "  TRANGTHAI = '" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY & "',"
                        strSQL &= "  IDMSG313 = '" & pvStrID & "',"
                        strSQL &= "  id_nv_ck312='" & pvStrTenDN & "',idmsg312='" & pvStrIDMSG312 & "'"
                        strSQL &= " , NGAY_HT=to_date('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                        strSQL &= "  WHERE  ID='" & pvStrID & "' AND TRANGTHAI = ('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_CHECKER312 & "') "
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                        'Kiểm tra số HS này xem đã có trong bảng HQ chưa, nếu có thì delete trước khi insert record  mới
                        Dim dsHQ As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet("SELECT SO_HS FROM TCS_HQ247_314 WHERE SO_HS='" & strSo_HS & "' AND TRANGTHAI = '16' ", CommandType.Text)
                        If Not dsHQ Is Nothing Then
                            If dsHQ.Tables.Count > 0 Then
                                If dsHQ.Tables(0).Rows.Count > 0 Then
                                    VBOracleLib.DataAccess.ExecuteNonQuery("DELETE FROM TCS_HQ247_314 WHERE  SO_HS='" & strSo_HS & "' AND TRANGTHAI = '16' ", CommandType.Text)
                                End If
                            End If
                        End If

                        'Insert sang bang HQ
                        strSQL = "INSERT INTO TCS_HQ247_314 (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,NGAY_HL_UQ,NGAY_HHL_UQ)"
                        strSQL &= " SELECT '" & pvStrID & "',SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,TEN_TAIKHOAN_TH, TRANGTHAI, IDMSG312, IDMSG313,IDMSG213, NGAY_KY_HDUQ, ID_NV_MK213, ID_NV_CK213,ID_NV_MK312, ID_NV_CK312, CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH, LY_DO_HUY, LY_DO_CHUYEN_TRA,IDMSG311, NGAY_NHAN311, NGAY_HT, LOAIMSG213, MA_CN, CIFNO,TK_MA_CN,NGAY_HL_UQ,NGAY_HHL_UQ "
                        strSQL &= " FROM TCS_HQ247_314_TMP "
                        strSQL &= " WHERE ID = '" & pvStrID & "'"
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

                        'Delete trong bang HQ_311
                        strSQL = " DELETE FROM TCS_HQ247_314_TMP "
                        strSQL &= "  WHERE ID = '" & pvStrID & "'"
                        VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                        Return Common.mdlCommon.TCS_HQ247_OK
                    Case Else
                        Return Common.mdlCommon.TCS_HQ247_HQ_SAI_LOAI_HS 'Sai loại hồ sơ
                End Select

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex, "daTCS_HQ247_314.UPDATE_TCS_HQ247_SEND314", "")
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function gf_get314InfoToObj(ByVal pvStrSO_HS As String) As DataSet
            Dim ds As DataSet = Nothing
            Try
                Dim strSQL As String = "SELECT a.id, a.so_hs, a.loai_hs, a.ma_dv, a.ten_dv, a.diachi, a.so_cmt,"
                strSQL &= " a.ho_ten,to_char(a.ngaysinh,'DD-MM-RRRR') NGAYSINH , a.nguyenquan, a.thongtinlienhe, a.so_dt,"
                strSQL &= " a.email, a.so_dt1, a.email1, a.serialnumber, a.noicap,TO_CHAR( a.ngay_hl,'DD-MM-RRRR HH24:MI:SS') NGAY_HL,"
                strSQL &= " TO_CHAR( a.ngay_hhl,'DD-MM-RRRR HH24:MI:SS') ngay_hhl, a.publickey, a.ma_nh_th, a.ten_nh_th, a.taikhoan_th,"
                strSQL &= " a.ten_taikhoan_th, a.trangthai,to_char(a.ngay_ky_hduq,'DD-MM-RRRR') ngay_ky_hduq,to_char(a.ngay_hl_uq,'DD-MM-RRRR') ngay_hl_uq,to_char(a.ngay_hhl_uq,'DD-MM-RRRR') ngay_hhl_uq "
                strSQL &= "  FROM tcs_hq247_314 a WHERE SO_HS='" & pvStrSO_HS & "'"
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)

            Catch ex As Exception

            End Try
            Return ds
        End Function
        Public Shared Function gf_CheckValidHS201(ByVal pvStrSO_HS As String) As Decimal
            Try
                Dim strSQL As String = "select * from tcs_HQ247_314 where So_HS='" & pvStrSO_HS & "' and trangthai='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "' and loai_HS<>'3'  AND TO_CHAR( NGAY_HHL_UQ,'RRRRMMDD')>='" + DateTime.Now.ToString("yyyyMMdd") + "' AND TO_CHAR( NGAY_HL_UQ,'RRRRMMDD') <= '" + DateTime.Now.ToString("yyyyMMdd") + "' "
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
            End Try
            Return mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
        End Function
        Public Shared Function getMSG311View(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH"

                strSQL &= "  ,(CASE WHEN A.TRANGTHAI IN ('01','02','03','04','05','06','07','08') THEN "
                strSQL &= "  A.TAIKHOAN_TH ELSE"
                strSQL &= "  A.CUR_TAIKHOAN_TH"
                strSQL &= "  END ) TAIKHOAN_TH, C.MOTA TEN_LOAI_HS, "

                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311,  to_char(A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS')as NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI, "
                strSQL &= " NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME,"
                strSQL &= " to_char(A.ngay_hl_uq,'DD/MM/RRRR') ngay_hl_uq, to_char(A.ngay_hhl_uq,'DD/MM/RRRR') ngay_hhl_uq,(trunc(A.ngay_hhl_uq)-trunc(A.ngay_hl_uq)) as soNgayHL," '" A.ngay_hl_uq, A.ngay_hhl_uq,"
                strSQL &= " NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '12%' AND X.NODEID IN ('12.1.23')),'#') ||'?ID='||A.ID NAVIGATEURL "
                strSQL &= " FROM TCS_HQ247_314_TMP A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.LOAI_HS =C.LOAI_HS " & pvStrWhereClause
                strSQL &= " UNION ALL "
                strSQL &= "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH"

                strSQL &= "  ,NVL(A.CUR_TAIKHOAN_TH,  A.TAIKHOAN_TH)"
                strSQL &= "   TAIKHOAN_TH,C.MOTA TEN_LOAI_HS, "

                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311,  to_char(A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') as NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI,"
                strSQL &= " NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME,"
                strSQL &= " to_char(A.ngay_hl_uq,'DD/MM/RRRR') ngay_hl_uq, to_char(A.ngay_hhl_uq,'DD/MM/RRRR') ngay_hhl_uq, (trunc(A.ngay_hhl_uq)-trunc(A.ngay_hl_uq)) as soNgayHL," '" A.ngay_hl_uq, A.ngay_hhl_uq,"
                strSQL &= " NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '12%' AND X.NODEID IN ('12.1.23')),'#') ||'?ID='||A.ID NAVIGATEURL "
                strSQL &= " FROM TCS_HQ247_314 A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.LOAI_HS =C.LOAI_HS AND A.TRANGTHAI  IN ('13','15') " & pvStrWhereClause
                strSQL = "Select rownum as STT, B.* from (" & strSQL & ") B"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception

            End Try
            Return ds
        End Function
        Public Shared Function getMSGDKNgung(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT  A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= " A.HO_TEN, A.NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= " A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, A.NGAY_HL,"
                strSQL &= " A.NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH,"
                strSQL &= " A.TAIKHOAN_TH, C.MOTA TEN_LOAI_HS, "
                strSQL &= " A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= " A.IDMSG213, A.NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= " A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= " A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= " A.IDMSG311,TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311,  to_char(A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS')as NGAY_HT, A.LOAIMSG213, B.MOTA TRANG_THAI, "
                strSQL &= " NVL((SELECT MAX(NAME) FROM TCS_DM_CHINHANH WHERE ID=A.MA_CN  ),'____')CN_NAME,"
                strSQL &= " to_char(A.ngay_hl_uq,'DD/MM/RRRR') ngay_hl_uq, to_char(A.ngay_hhl_uq,'DD/MM/RRRR') ngay_hhl_uq,(trunc(A.ngay_hhl_uq)-trunc(A.ngay_hl_uq)) as soNgayHL,"
                strSQL &= " NVL((SELECT MAX(X.NAVIGATEURL) FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '12%' AND X.NODEID IN ('12.1.31')),'#') ||'?ID='||A.ID NAVIGATEURL "
                strSQL &= " FROM tcs_hq247_314_log A,TCS_DM_TRANGTHAI_YEUCAU B,TCS_DM_LOAI_HS_NNT C WHERE A.TRANGTHAI=B.TRANG_THAI AND A.LOAI_HS =C.LOAI_HS AND TT_NGUNGDK='15' " & pvStrWhereClause
                strSQL &= " ORDER BY NGAY_HT DESC"

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception

            End Try
            Return ds
        End Function
        Public Shared Function getDangKyNNT_HQ_LOG(ByVal pvStrID As String, Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS "
                strSQL &= "  FROM TCS_HQ247_314_LOG A WHERE  1=1 "
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND LOG_ID='" & pvStrID & "' "
                End If
                If Not pvStrWhereClause.Equals("") Then
                    strSQL &= "  " & pvStrWhereClause & " "
                End If
                If Not pvStrOrderBy.Equals("") Then
                    strSQL &= "  ORDER BY " & pvStrOrderBy & " "
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception

            End Try
            Return ds
        End Function
        Public Shared Function getDangKyNNT_HQ(ByVal pvStrID As String, Optional ByVal pvStrWhereClause As String = "", Optional ByVal pvStrOrderBy As String = "") As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.ID, A.SO_HS, A.LOAI_HS, A.MA_DV, A.TEN_DV, A.DIACHI, A.SO_CMT,"
                strSQL &= "  A.HO_TEN,TO_CHAR( A.NGAYSINH,'DD/MM/RRRR') NGAYSINH, A.NGUYENQUAN, A.THONGTINLIENHE, A.SO_DT,"
                strSQL &= "  A.EMAIL, A.SO_DT1, A.EMAIL1, A.SERIALNUMBER, A.NOICAP, TO_CHAR( A.NGAY_HL,'DD/MM/RRRR') NGAY_HL,"
                strSQL &= "  TO_CHAR( A.NGAY_HHL,'DD/MM/RRRR')  NGAY_HHL, A.PUBLICKEY, A.MA_NH_TH, A.TEN_NH_TH, A.TAIKHOAN_TH,"
                strSQL &= "  A.TEN_TAIKHOAN_TH, A.TRANGTHAI, A.IDMSG312, A.IDMSG313,"
                strSQL &= "  A.IDMSG213, TO_CHAR( A.NGAY_KY_HDUQ,'DD/MM/RRRR') NGAY_KY_HDUQ, A.ID_NV_MK213, A.ID_NV_CK213,"
                strSQL &= "  A.ID_NV_MK312, A.ID_NV_CK312, A.CUR_TAIKHOAN_TH,"
                strSQL &= "  A.CUR_TEN_TAIKHOAN_TH, A.LY_DO_HUY, A.LY_DO_CHUYEN_TRA,"
                strSQL &= "  A.IDMSG311, TO_CHAR( A.NGAY_NHAN311,'DD/MM/RRRR') NGAY_NHAN311, TO_CHAR( A.NGAY_HT,'DD/MM/RRRR HH24:MI:SS') NGAY_HT, A.LOAIMSG213, A.MA_CN"
                strSQL &= " ,(SELECT MAX(MOTA) FROM TCS_DM_LOAI_HS_NNT WHERE LOAI_HS = A.LOAI_HS) TENLOAI_HS,to_char(a.ngay_hl_uq,'DD-MM-RRRR') ngay_hl_uq,to_char(a.ngay_hhl_uq,'DD-MM-RRRR') ngay_hhl_uq  "
                strSQL &= "  FROM TCS_HQ247_314 A WHERE  1=1 "
                If Not pvStrID.Equals("") Then
                    strSQL &= " AND ID='" & pvStrID & "' "
                End If
                If Not pvStrWhereClause.Equals("") Then
                    strSQL &= "  " & pvStrWhereClause & " "
                End If
                If Not pvStrOrderBy.Equals("") Then
                    strSQL &= "  ORDER BY " & pvStrOrderBy & " "
                End If
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception

            End Try
            Return ds
        End Function

        Public Shared Function fnc_CheckDieuChinhTaiNH(ByVal pStrSoHS As String) As Decimal
            Try
                Dim strSQL As String = "select * from TCS_HQ247_314 where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "') "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' VBOracleLib.LogDebug.WriteLog("fnc_CheckDieuChinhTaiNH: " & ex.StackTrace, EventLogEntryType.Error)
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_Maker312_SuaTaiNH(ByVal pStrSoHS As String, ByVal pStrSoTK As String, ByVal pStrTenTK As String, ByVal pStrNGAY_KY_HDUQ As String, ByVal pStrNGAY_HL_UQ As String, ByVal pStrNGAY_HHL_UQ As String, ByVal pStrTenDN As String, Optional ByVal pvStrCIFNO As String = "", Optional ByVal pvStrMA_CNTK As String = "") As Decimal
            Dim vConn As IDbConnection = Nothing
            Dim vTran As IDbTransaction = Nothing
            Try
                vConn = DataAccess.GetConnection()
                vTran = vConn.BeginTransaction()
                Dim strSQL As String = "select * from TCS_HQ247_314 where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "') "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim decResult As Decimal = Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            decResult = Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                'neu khong ton tai thi tra ve loi
                If decResult <> 0 Then
                    Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                End If
                'insert sang bang HQ 311
                strSQL = "INSERT INTO TCS_HQ247_314_TMP (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH, TRANGTHAI, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH, LOAIMSG213, MA_CN,NGAY_HL_UQ,NGAY_HHL_UQ)"
                strSQL &= " SELECT ID, SO_HS,'2' LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH,'04' TRANGTHAI, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH,'1' LOAIMSG213, MA_CN,NGAY_HL_UQ,NGAY_HHL_UQ"
                strSQL &= " FROM TCS_HQ247_314 A WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, vTran)
                'cap nhat trang thai HQ
                strSQL = "UPDATE TCS_HQ247_314 SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_TAMKHOA_CHODIEUCHINH & "' WHERE SO_HS='" & pStrSoHS & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, vTran)
                'cap nhat lai trang thai thong tin tai khoan dang ky va thong tin nguoi lap 312
                strSQL = "UPDATE TCS_HQ247_314_TMP set "
                strSQL &= " NGAY_HT=TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= ",ID_NV_MK312=upper('" & pStrTenDN & "') "
                strSQL &= ",CUR_TAIKHOAN_TH='" & pStrSoTK & "'"
                strSQL &= ",CIFNO='" & pvStrCIFNO & "'"
                strSQL &= ",TK_MA_CN='" & pvStrMA_CNTK & "'"
                strSQL &= ",CUR_TEN_TAIKHOAN_TH=:CUR_TEN_TAIKHOAN_TH " '" & pStrTenTK.Replace("'", "''") & "'"
                strSQL &= ",TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_MAKER312 & "'"
                strSQL &= ", NGAY_KY_HDUQ=TO_DATE('" & pStrNGAY_KY_HDUQ & "','DD/MM/RRRR')"
                strSQL &= ", NGAY_HL_UQ=TO_DATE('" & pStrNGAY_HL_UQ & "','DD/MM/RRRR')"
                strSQL &= ", NGAY_HHL_UQ=TO_DATE('" & pStrNGAY_HHL_UQ & "','DD/MM/RRRR')"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                Dim lsParam As List(Of IDataParameter) = New List(Of IDataParameter)
                lsParam.Add(VBOracleLib.DataAccess.NewDBParameter("CUR_TEN_TAIKHOAN_TH", ParameterDirection.Input, pStrTenTK, DbType.String))
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lsParam.ToArray(), vTran)
                vTran.Commit()
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                vTran.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_Maker312_SuaTaiNH: " & ex.StackTrace, EventLogEntryType.Error)
            Finally
                If Not vTran Is Nothing Then
                    vTran.Dispose()
                End If
                If Not vConn Is Nothing Then
                    vConn.Dispose()
                End If
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function fnc_Maker312_HuyTaiNH(ByVal pStrSoHS As String, ByVal pStrTenDN As String) As Decimal
            Dim vConn As IDbConnection = Nothing
            Dim vTran As IDbTransaction = Nothing
            Try
                vConn = DataAccess.GetConnection()
                vTran = vConn.BeginTransaction()
                Dim strSQL As String = "select * from TCS_HQ247_314 where so_hs='" & pStrSoHS & "' and trangthai  in ('" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "') "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim decResult As Decimal = Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            decResult = Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                'neu khong ton tai thi tra ve loi
                If decResult <> 0 Then
                    Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                End If
                'check bang hq 311 xem co ko
                strSQL = "select * from TCS_HQ247_314_TMP where so_hs='" & pStrSoHS & "'"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            decResult = Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                'neu khong ton tai thi tra ve loi
                If decResult <> 0 Then
                    Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                End If
                'check chung tu thanh toan xem co khong
                strSQL = "SELECT * FROM TCS_HQ247_314 WHERE TRANGTHAI NOT IN ('07','04') AND SO_HS='" & pStrSoHS & "'"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            decResult = Common.mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
                'neu khong ton tai thi tra ve loi
                If decResult <> 0 Then
                    Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                End If

                'insert sang bang HQ 311
                strSQL = "INSERT INTO TCS_HQ247_314_TMP (ID, SO_HS, LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH, TRANGTHAI, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH, LOAIMSG213, MA_CN,NGAY_KY_HDUQ,NGAY_HL_UQ,NGAY_HHL_UQ)"
                strSQL &= " SELECT ID, SO_HS,'3' LOAI_HS, MA_DV, TEN_DV, DIACHI, SO_CMT,"
                strSQL &= " HO_TEN, NGAYSINH, NGUYENQUAN, THONGTINLIENHE, SO_DT,"
                strSQL &= " EMAIL, SO_DT1, EMAIL1, SERIALNUMBER, NOICAP, NGAY_HL,"
                strSQL &= " NGAY_HHL, PUBLICKEY, MA_NH_TH, TEN_NH_TH, TAIKHOAN_TH,"
                strSQL &= " TEN_TAIKHOAN_TH,'04' TRANGTHAI, CUR_TAIKHOAN_TH,"
                strSQL &= " CUR_TEN_TAIKHOAN_TH,'1' LOAIMSG213, MA_CN,NGAY_KY_HDUQ,NGAY_HL_UQ,NGAY_HHL_UQ"
                strSQL &= " FROM TCS_HQ247_314 A WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, vTran)
                'cap nhat trang thai HQ
                strSQL = "UPDATE TCS_HQ247_314 SET TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_314_TRANGTHAI_TAMKHOA_CHODIEUCHINH & "' WHERE SO_HS='" & pStrSoHS & "' "
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, vTran)
                'cap nhat lai trang thai thong tin tai khoan dang ky va thong tin nguoi lap 312
                strSQL = "UPDATE TCS_HQ247_314_TMP set "
                strSQL &= " NGAY_HT=TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                strSQL &= ",ID_NV_MK312=upper('" & pStrTenDN & "') "
                strSQL &= ",TRANGTHAI='" & Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER312 & "'"
                strSQL &= " WHERE SO_HS='" & pStrSoHS & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, vTran)
                vTran.Commit()
                Return Common.mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                vTran.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog("fnc_Maker312_HuyTaiNH: " & ex.StackTrace, EventLogEntryType.Error)
            Finally
                If Not vTran Is Nothing Then
                    vTran.Dispose()
                End If
                If Not vConn Is Nothing Then
                    vConn.Dispose()
                End If
            End Try
            Return Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function Get_TT_HS(Id As String) As String
            Dim sql As String = ""
            sql = "select trangthai from TCS_HQ247_314_TMP where id='" & Id & "'"
            Dim tb As DataTable = VBOracleLib.DataAccess.ExecuteToTable(sql)
            If tb.Rows.Count > 0 Then
                Return tb.Rows(0)(0).ToString
            Else
                sql = "select trangthai from TCS_HQ247_314 where id='" & Id & "'"
                tb = VBOracleLib.DataAccess.ExecuteToTable(sql)
                If tb.Rows.Count > 0 Then
                    Return tb.Rows(0)(0).ToString
                Else
                    Return ""
                End If
            End If
        End Function
    End Class
End Namespace
