﻿Imports VBOracleLib
Namespace NHOTHU
    Public Class daBaoCaoNHOTHU
        Public Shared Function TCS_BC_TONG_CN(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = " SELECT   cn.id ma_cn,MAX (cn.name) ten_cn,MAX (XA.ten) quanhuyen," & _
                         " COUNT (DISTINCT hdr.ma_nnthue) soluong_khachhang,COUNT (p.rm_ref_no) soluong_giaodich, " & _
                         " SUM (hdr.ttien + hdr.phi_gd + hdr.phi_vat)giatri_gd," & _
                         " SUM (hdr.ttien) giatri_gd_gl,    " & _
                         " SUM (NVL (hdr.ttien, 0)) amt,SUM (NVL (hdr.phi_gd, 0)) fee,SUM (NVL (hdr.phi_vat, 0)) vat," & _
                         " (ROUND (ratio_to_report(SUM(  NVL (hdr.ttien, 0)+ NVL (hdr.phi_gd, 0)+ NVL (hdr.phi_vat, 0)))OVER (),4))* 100 tytrong_gd" & _
                         " FROM   tcs_ctu_ntdt_hq247_hdr hdr,tcs_dm_chinhanh cn,tcs_dm_xa xa,tcs_log_payment p" & _
                         " WHERE   (    (hdr.ma_cn = cn.id) " & _
                         " AND (hdr.trang_thai = '01') AND (p.response_code is not null) " & _
                         "" & _
                         strWhere & _
                        " AND (hdr.so_ct = p.rm_ref_no)" & _
                        " AND (cn.provice_code = xa.ma_tinh))" & _
                        " GROUP BY cn.id"
                'sonmt bo " AND p.acct_no = hdr.tk_kh_nh " & _
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                'bỏ " AND TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks)" & _
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Shared Function TCS_BC_TONG_DC(ByVal strWhere As String, ByVal strWhereNhan As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                'strSQL = " SELECT ROWNUM STT,A.MA_HQ_CQT MCQTHU,A.SO_CT SMONTRUYEN,A.SOTIEN_TO STIENTRUYEN,A.HQ_SO_CT SMONNHAN,A.HQ_SOTIEN_TO STIENNHAN, A.SOTIEN_TO-A.HQ_SOTIEN_TO CLECH"
                'strSQL &= " FROM ("
                'strSQL &= " SELECT A.MA_HQ_CQT,NVL(A.SOTIEN_TO,0) SOTIEN_TO,NVL( A.SO_CT,0) SO_CT,NVL( B.HQ_SOTIEN_TO,0) HQ_SOTIEN_TO"
                'strSQL &= " ,NVL( B.HQ_SO_CT,0) HQ_SO_CT"
                'strSQL &= " FROM   (SELECT A.MA_HQ_CQT, SUM(A.SOTIEN_TO)SOTIEN_TO,SUM(1) SO_CT  FROM TCS_HQ247_304_DCNHHQ_HDR A, TCS_HQ247_304 B WHERE A.TRANSACTION_ID = B.MSG_ID304 AND  1=1 "
                'strSQL &= strWhere
                'strSQL &= " GROUP BY A.MA_HQ_CQT) A,"
                'strSQL &= " (SELECT A.MA_HQ_CQT, SUM(A.SOTIEN_TO) HQ_SOTIEN_TO,SUM(1) HQ_SO_CT  FROM TCS_HQ247_304_DCHQNH_HDR A, TCS_HQ247_304 B WHERE A.TRANSACTION_ID = B.MSG_ID304 AND 1=1 "
                'strSQL &= strWhereNhan
                'strSQL &= " GROUP BY A.MA_HQ_CQT) B  WHERE B.MA_HQ_CQT(+)= A.MA_HQ_CQT"
                'strSQL &= " ORDER BY A.MA_HQ_CQT) A"


                strSQL = " SELECT ROWNUM STT,A.MA_HQ_CQT MCQTHU,A.SO_CT SMONTRUYEN,A.SOTIEN_TO STIENTRUYEN,A.HQ_SO_CT SMONNHAN,A.HQ_SOTIEN_TO STIENNHAN, A.SOTIEN_TO-A.HQ_SOTIEN_TO CLECH"
                strSQL &= " FROM ("
                strSQL &= " SELECT A.MA_HQ_CQT,NVL(A.SOTIEN_TO,0) SOTIEN_TO,NVL( A.SO_CT,0) SO_CT,NVL( B.HQ_SOTIEN_TO,0) HQ_SOTIEN_TO"
                strSQL &= " ,NVL( B.HQ_SO_CT,0) HQ_SO_CT"
                strSQL &= " FROM   (SELECT A.MA_HQ_CQT, SUM(A.SOTIEN_TO)SOTIEN_TO,SUM(1) SO_CT  FROM TCS_HQ247_304_DCNHHQ_HDR A, TCS_HQ247_304 B WHERE A.TRANSACTION_ID = B.MSG_ID304 AND  1=1 "
                strSQL &= strWhere
                strSQL &= " GROUP BY A.MA_HQ_CQT) A,"
                strSQL &= " (SELECT A.MA_HQ_CQT, SUM(A.SOTIEN_TO) HQ_SOTIEN_TO,SUM(1) HQ_SO_CT  FROM TCS_HQ247_304_DCHQNH_HDR A  WHERE  1=1 "
                strSQL &= strWhereNhan
                strSQL &= " GROUP BY A.MA_HQ_CQT) B  WHERE B.MA_HQ_CQT(+)= A.MA_HQ_CQT"
                strSQL &= " ORDER BY A.MA_HQ_CQT) A"

                'sonmt bo " AND p.acct_no = hdr.tk_kh_nh " & _
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                'bỏ " AND TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks)" & _
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Shared Function TCS_BC_CHITIET_DC(ByVal strWhere As String) As DataSet
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String = "SELECT ROWNUM STT, A.* from (SELECT A.KYHIEU_CT||'-'|| A.SO_CT KHSCTU,A.TEN_DV TDVNTHUE,A.MA_DV MSTHUE,A.MA_HQ_PH MHQUANPH,B.SO_TK STKHAI,"
                strSQL &= " C.NGAY_DK NDKTKHAI, B.MA_LH LHINH,A.MA_KB MKBAC,A.TKKB TKTHU,A.TKKB  TKTNS,"
                strSQL &= " NVL(a.KQ_DC,'Thành công') TTRANG,A.MA_HQ_CQT MCQT , B.NDKT MA_TMUC, B.SOTIEN_NT TTIEN ,"
                strSQL &= " NVL((SELECT MAX(SO_CT_NH) FROM TCS_HQ247_304 WHERE HQ_SO_CTU =A.SO_CT),'') REFNO "
                strSQL &= " FROM TCS_HQ247_304_DCHQNH_HDR A,TCS_HQ247_304_DCHQNH_DTL B , TCS_HQ247_304_DCNHHQ_DTL C "
                strSQL &= "    WHERE(A.TRANSACTION_ID = B.TRANSACTION_ID)  "
                'strSQL &= " AND B.transaction_id = C.transaction_id(+)"
                'strSQL &= " AND B.ndkt(+) = C.ndkt AND B.so_tk=C.so_tk(+)"
                strSQL &= " AND B.transaction_id = C.transaction_id(+)"
                strSQL &= " AND B.ndkt = C.ndkt(+) AND B.so_tk=C.so_tk(+)"
                strSQL &= strWhere & "  ORDER BY A.NGAYTRUYEN_CT ) A"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                'bỏ " AND TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks)" & _
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

    End Class
End Namespace
