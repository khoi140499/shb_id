﻿Imports System.Data.OracleClient
Imports Business_HQ.Common
Imports VBOracleLib
Imports CtcCommon
Namespace nhothu
    Public Class daCTUHQ201
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Shared Function CheckValidSoCTU(ByVal p_strTranID As String) As Decimal
            Try
                'kiem tra so chung tu
                Dim strSQL As String = "select * from TCS_HQ247_201 where MSG_ID201='" & p_strTranID & "' "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return mdlCommon.TCS_HQ247_HQ_TRUNGSO_HQ_CTU
                        End If
                    End If
                End If
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("CheckValidSoCTU  MSG_ID201[" & p_strTranID & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex, "CheckValidSoCTU  MSG_ID201[" & p_strTranID & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function InsertMSG201(ByVal pStrSoHS As String, ByVal pStrMaNT As String, ByVal pStrSoTienTO As String, _
                                            ByVal pStrMaDV As String, ByVal pStrTenDV As String, _
                                            ByVal pStrTyGia As String, ByVal pStrMSG As String, _
                                            ByVal PSTRMSG_ID304 As String) As Decimal
            Dim strSQL As String = "SELECT TO_CHAR(SYSDATE,'RRRRMMDD')||LPAD(SEQ_TCS_HQ247_304.NEXTVAL,10,'0') FROM DUAL"
            Dim ds As DataSet = Nothing
            Dim StrID As String = ""

            Try
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                StrID = ds.Tables(0).Rows(0)(0).ToString()
                strSQL = "INSERT INTO TCS_HQ247_201 (ID, SO_HS, MA_NT,TY_GIA,HQ_SOTIEN_TO,"
                strSQL &= "MA_DV,TEN_DV,TRANGTHAI,TAIKHOAN_TH,TEN_TAIKHOAN_TH,"
                strSQL &= "MSG_CONTENT, MSG_ID201,"
                strSQL &= "NGAY_HT, NGAYNHANMSG, MST_NOP) VALUES("
                strSQL &= "'" & StrID & "',"
                strSQL &= "'" & pStrSoHS & "',"
                strSQL &= "'" & pStrMaNT & "',"
                strSQL &= "" & pStrTyGia & ","
                strSQL &= "" & pStrSoTienTO & ","
                strSQL &= "'" & pStrMaDV & "',"
                strSQL &= ":TEN_DV,"
                strSQL &= "'" & mdlCommon.TCS_HQ247_TRANGTHAI_201_MOINHAN & "',"
                strSQL &= "nvl((select max(TAIKHOAN_TH) from TCS_HQ247_314 where trangthai='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "'  AND so_HS='" & pStrSoHS & "'  ),'____'),"
                strSQL &= "nvl((select max(TEN_TAIKHOAN_TH) from TCS_HQ247_314 where trangthai='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "'  AND so_HS='" & pStrSoHS & "'  ),'____'),"
                strSQL &= ":MSG_CONTENT,"
                strSQL &= "'" & PSTRMSG_ID304 & "',"
                strSQL &= "TO_DATE('" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "TO_DATE('" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & "','RRRR-MM-DD HH24:MI:SS'),"
                strSQL &= "nvl((select max(MA_DV) from TCS_HQ247_314 where trangthai='" & mdlCommon.TCS_HQ247_314_TRANGTHAI_ACTIVE & "'  AND so_HS='" & pStrSoHS & "'  ),'____')"
                strSQL &= ")"
                Dim lst As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                lst.Add(DataAccess.NewDBParameter("TEN_DV", ParameterDirection.Input, pStrTenDV, DbType.String))
                lst.Add(DataAccess.NewDBParameter("MSG_CONTENT", ParameterDirection.Input, pStrMSG, DbType.String))
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray())
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("insert msg 201 idmsg[" & PSTRMSG_ID304 & "] mst[" & pStrMaDV & "] so_HS[" & pStrSoHS & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) 'LogDebug.WriteLog(ex, "insert msg 201 idmsg[" & PSTRMSG_ID304 & "] mst[" & pStrMaDV & "] so_HS[" & pStrSoHS & "]  ")
            Finally

            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function getMSG201_TracuuXuly(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT ROWNUM STT,X.* FROM (SELECT A.ID,  A.MA_NT,A.TAIKHOAN_TH,"
                strSQL &= " A.HQ_SOTIEN_TO, A.MA_DV, A.TEN_DV,nvl(A.MST_NOP,A.MA_DV) MST_NOP,A.TRANGTHAI,"
                strSQL &= " B.MOTA TRANG_THAI,"
                strSQL &= " TO_CHAR( TO_DATE(ngay_tn_ct,'RRRR-MM-DD""T""HH24:MI:SS'),'DD/MM/RRRR HH24:MI:SS') as ngay_tn_ct,"
                strSQL &= " NVL((SELECT MAX(X.NAVIGATEURL)||'?ID='||A.ID FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '12.3%' AND X.NODEID IN ('12.3.7')),'#') NAVIGATEURL,"
                strSQL &= " (CASE WHEN nvl(MSG_ID201,'__')<> '__' then 'Export' else ''end) EXPORT304 ,MSG_ID201, "
                strSQL &= " (CASE WHEN nvl(MSG_ID301,'__')<> '__' then 'Export' else ''end) EXPORT213,MSG_ID213,MSG_ID301 "
                strSQL &= ",A.SO_CTU, A.SO_CT_NH,TO_CHAR(A.NGAYNHANMSG,'DD/MM/RRRR HH24:MI:SS') NGAYNHANMSG  "
                strSQL &= " FROM tcs_hq247_201 A, TCS_DM_TRANGTHAI_TT_HQ247 B"
                strSQL &= " WHERE A.TRANGTHAI = B.TRANGTHAI " & pvStrWhereClause
                strSQL &= " ORDER BY A.ID) X ORDER BY X.ID "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                '  log.Error()
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function gf_getMSG201Detail(ByVal pvStrID As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = " SELECT A.*,B.MOTA TRANG_THAI FROM TCS_HQ247_201 A, TCS_DM_TRANGTHAI_TT_HQ247 B  "
                strSQL &= "  WHERE A.TRANGTHAI = B.TRANGTHAI AND A.ID='" & pvStrID & "'"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                ' log.Error()
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI(ByVal pStrID304 As String, ByVal pStrLyDo As String, Optional ByVal pStrTenDN As String = "NTDTHQ", Optional ByVal strMa_kq_xl As String = "2") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", LOAI213='" & strMa_kq_xl & "' "
                StrSQL &= ", LYDO=:LYDO "
                StrSQL &= ", ID_MK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                'VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Dim lsParam As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                lsParam.Add(DataAccess.NewDBParameter("LYDO", ParameterDirection.Input, pStrLyDo, DbType.String))
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text, lsParam.ToArray())
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI  pStrID201[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI  pStrID201[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_201_KQXL(ByVal pStrID304 As String, ByVal pStrMaKQXL As String, ByVal pStrLyDo As String) As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET LOAI213= '" & pStrMaKQXL & "' "
                StrSQL &= ", LYDO=:LYDO "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"

                Dim lsParam As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                lsParam.Add(DataAccess.NewDBParameter("LYDO", ParameterDirection.Input, pStrLyDo, DbType.String))
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text, lsParam.ToArray())
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_201_KQXL  pStrID201[" & pStrID304 & "] LOAI213[" & pStrMaKQXL & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_201_KQXL  pStrID201[" & pStrID304 & "] LOAI213[" & pStrMaKQXL & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function

        Public Shared Function checkTrangthaiMSG201(ByVal pStrID As String, ByVal pStrTrangthai As String) As Decimal
            Try
                Dim strSQL As String = "SELECT * FROM TCS_HQ247_201 WHERE ID='" & pStrID & "' AND TRANGTHAI='" & pStrTrangthai & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
        End Function
        Public Shared Function Insert301(ByVal HDR As NewChungTu.infChungTuHDR, ByVal ListDTL As List(Of NewChungTu.infChungTuDTL), ByVal pStrID As String, ByVal pStrtenDN As String) As Decimal
            Try
                If HDR Is Nothing Then
                    Return mdlCommon.TCS_HQ247_HQ_HACHTOAN_LOI_CTU
                End If
                Dim blnVND As Boolean = True
                If (HDR.Ma_NT <> "VND") Then blnVND = False
                For Each dtl As NewChungTu.infChungTuDTL In ListDTL
                    If (dtl.SoTien = 0) Then
                        log.Error("Insert301  pStrID201[" & pStrID & "] pStrTenDN[" & pStrtenDN & "] loi chi tiet chung tu  ")
                        '  LogDebug.WriteLog(New Exception("Lỗi valid chứng từ DTL"), "Insert301  pStrID201[" & pStrID & "] pStrTenDN[" & pStrtenDN & "] loi chi tiet chung tu  ")
                        Return mdlCommon.TCS_HQ247_HQ_HACHTOAN_LOI_CTU
                    End If
                Next
                HDR.Trang_Thai = "05"
                If String.IsNullOrEmpty(HDR.Ngay_QD) Then
                    HDR.Ngay_QD = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                If String.IsNullOrEmpty(HDR.Ngay_HT) Then
                    HDR.Ngay_HT = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                If String.IsNullOrEmpty(HDR.Ngay_TK) Then
                    HDR.Ngay_TK = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                If String.IsNullOrEmpty(HDR.NGAY_KH_NH) Then
                    HDR.NGAY_KH_NH = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                If String.IsNullOrEmpty(HDR.Ngay_BK) Then
                    HDR.Ngay_BK = DateTime.Now.ToString("dd/MM/yyyy")
                End If
                For Each dtl In ListDTL
                    If String.IsNullOrEmpty(dtl.NGAY_TK) Then
                        dtl.NGAY_TK = DateTime.Now.ToString("dd/MM/yyyy")
                    End If
                Next
                If Insert_Chungtu(HDR, ListDTL) Then
                    ' Dim strSQLUpdate As String = "UPDATE TCS_HQ247_304 SET SO_CTU='" & HDR.So_CT & "' WHERE ID='" & pStrID & "'"
                    '  VBOracleLib.DataAccess.ExecuteNonQuery(strSQLUpdate, CommandType.Text)
                    Return mdlCommon.TCS_HQ247_OK
                End If
            Catch ex As Exception
                log.Error("Insert201  pStrID201[" & pStrID & "] pStrTenDN[" & pStrtenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex, "Insert201  pStrID201[" & pStrID & "] pStrTenDN[" & pStrtenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_HQ_HACHTOAN_LOI_CTU
        End Function
        Public Shared Function Insert_Chungtu(ByVal HDR As NewChungTu.infChungTuHDR, ByVal listDTL As List(Of NewChungTu.infChungTuDTL)) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If HDR IsNot Nothing Then
                    'Try
                    '    HDR.KyHieu_CT = Business_HQ.HaiQuan.HaiQuanController.GetKyHieuChungTu(HDR.SHKB, HDR.Ma_NV, "01")
                    'Catch ex As Exception
                    '    bRet = False
                    '    Throw New Exception("Có lỗi trong quá trình sinh ký hiệu chứng từ")
                    '    Return bRet
                    'End Try
                    Try
                        'nên chuyển vào trong câu query

                        Dim sSo_BT As String = String.Empty
                        sSo_BT = GetSoBT(HDR.Ngay_KB, HDR.Ma_NV)
                        If Not String.IsNullOrEmpty(sSo_BT) Then HDR.So_BT = CInt(sSo_BT + 1)
                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh số chứng từ")
                        Return bRet
                    End Try
                    'Using conn1 As IDbConnection = VBOracleLib.DataAccess.GetConnection()
                    '    Using tran1 As IDbTransaction = conn.BeginTransaction()

                    '    End Using
                    'End Using
                    conn = VBOracleLib.DataAccess.GetConnection()
                    '   conn.Open()
                    tran = conn.BeginTransaction()
                    sSQL = "Insert into TCS_CTU_HQ_NHOTHU_HDR(SHKB,Ngay_KB,Ma_NV," & _
                              "So_BT,Ma_DThu,So_BThu,KyHieu_CT,So_CT_NH,Ma_NNTien,Ten_NNTien,DC_NNTien," & _
                              "Ma_NNThue,Ten_NNThue,DC_NNThue,Ly_Do,Ma_KS,Ma_TQ,So_QD,Ngay_QD,CQ_QD,Ngay_CT," & _
                              "Ma_CQThu,XA_ID,Ma_Tinh,Ma_Huyen,Ma_Xa,TK_No,TK_Co,So_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
                              "Ma_NT,Ty_Gia,TG_ID, Ma_LThue,So_Khung,So_May,TK_KH_NH,MA_NH_A,MA_NH_B,Ten_NH_B,Ten_NH_TT,TTien,TT_TThu," & _
                              "Lan_In,Trang_Thai, TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT,So_BK, CTU_GNT_BL,So_CT," & _
                              "TK_GL_NH,TEN_KH_NH,TK_KB_NH, ISACTIVE,PHI_GD,PHI_VAT,PT_TINHPHI,SEQ_NO,REF_NO,RM_REF_NO,QUAN_HUYENNNTIEN," & _
                              "TINH_TPNNTIEN,MA_HQ,LOAI_TT,TEN_HQ,MA_HQ_PH,TEN_HQ_PH, MA_NH_TT,MA_SANPHAM,TT_CITAD,MA_NTK, KENH_CT, " & _
                              "MA_CN,partition_id,transaction_id,save_type, tran_type, " & _
                              "value_date,so_lo,note,address,remarks,TEN_CQTHU,NAME_TEN_CQTHU,SO_TIEN_CHU,TEN_LTHUE, " & _
                              "TEN_LHXNK,TEN_KB,MA_CUC,TEN_CUC, LOAI_NNT, PO_Date, TT_THU, " & _
                              "Ma_Huyen_NNThue, Ma_Tinh_NNThue, MA_QUAN_HUYENNNTIEN, MA_TINH_TPNNTIEN, Ngay_HT, Ngay_TK," & _
                              "NGAY_KH_NH, Ngay_BK, LOAI_CTU,SO_CMND,SO_FONE,NGAY_BAONO,NGAY_BAOCO,diengiai_hq,Ten_NH_A)" & _
                              "Values(:SHKB, :Ngay_KB, :Ma_NV, " & _
                              ":So_BT, :Ma_DThu, :So_BThu, :KyHieu_CT, :So_CT_NH, :Ma_NNTien, :Ten_NNTien, :DC_NNTien, " & _
                              ":Ma_NNThue, :Ten_NNThue, :DC_NNThue, :Ly_Do, :Ma_KS, :Ma_TQ, :So_QD, TO_DATE (:Ngay_QD, 'dd/MM/yyyy HH24:MI:SS'), :CQ_QD, :Ngay_CT, " & _
                              ":Ma_CQThu, :XA_ID, :Ma_Tinh, :Ma_Huyen, :Ma_Xa, :TK_No, :TK_Co, :So_TK,  :LH_XNK, :DVSDNS, :Ten_DVSDNS, " & _
                              ":Ma_NT, :Ty_Gia, :TG_ID, :Ma_LThue, :So_Khung, :So_May, :TK_KH_NH,  :MA_NH_A, :MA_NH_B, :Ten_NH_B, :Ten_NH_TT, :TTien, :TT_TThu, " & _
                              ":Lan_In, :Trang_Thai, :TK_KH_Nhan, :Ten_KH_Nhan, :Diachi_KH_Nhan, :TTien_NT, :PT_TT, :So_BK,  :CTU_GNT_BL, :So_CT, " & _
                              ":TK_GL_NH, :TEN_KH_NH, :TK_KB_NH, :ISACTIVE, :PHI_GD, :PHI_VAT, :PT_TINHPHI, :SEQ_NO, :REF_NO, :RM_REF_NO, :QUAN_HUYENNNTIEN, " & _
                              ":TINH_TPNNTIEN, :MA_HQ, :LOAI_TT, :TEN_HQ, :MA_HQ_PH, :TEN_HQ_PH, :MA_NH_TT, :MA_SANPHAM, :TT_CITAD, :MA_NTK, :KENH_CT, " & _
                              ":MA_CN, :partition_id, :transaction_id, :save_type, :tran_type, " & _
                              ":value_date, :so_lo, :note, :address, :remarks, :TEN_CQTHU, :NAME_TEN_CQTHU, :SO_TIEN_CHU, :TEN_LTHUE, " & _
                              ":TEN_LHXNK, :TEN_KB, :MA_CUC, :TEN_CUC, :LOAI_NNT, TO_DATE (:PO_Date, 'dd/MM/yyyy HH24:MI:SS'), :TT_THU, " & _
                              ":Ma_Huyen_NNThue, :Ma_Tinh_NNThue, :MA_QUAN_HUYENNNTIEN, :MA_TINH_TPNNTIEN, TO_DATE(:Ngay_HT, 'dd/MM/yyyy HH24:MI:SS'), TO_DATE(:Ngay_TK, 'dd/MM/yyyy HH24:MI:SS')," & _
                              "TO_DATE (:NGAY_KH_NH, 'dd/MM/yyyy HH24:MI:SS'), TO_DATE (:Ngay_BK, 'dd/MM/yyyy HH24:MI:SS'), :LOAI_CTU, :SO_CMND, :SO_FONE,TO_DATE (:NGAY_BAONO, 'dd/MM/yyyy'),TO_DATE (:NGAY_BAOCO, 'dd/MM/yyyy'),:diengiai_hq,:Ten_NH_A)"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(HDR.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_KB", ParameterDirection.Input, CStr(HDR.Ngay_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, CStr(HDR.Ma_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BT", ParameterDirection.Input, CStr(HDR.So_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_DThu", ParameterDirection.Input, CStr(HDR.Ma_DThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BThu", ParameterDirection.Input, CStr(HDR.So_BThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KyHieu_CT", ParameterDirection.Input, CStr(HDR.KyHieu_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_CT_NH", ParameterDirection.Input, CStr(HDR.So_CT_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NNTien", ParameterDirection.Input, CStr(HDR.Ma_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NNTien", ParameterDirection.Input, CStr(HDR.Ten_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTien", ParameterDirection.Input, CStr(HDR.DC_NNTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NNThue", ParameterDirection.Input, CStr(HDR.Ma_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NNThue", ParameterDirection.Input, CStr(HDR.Ten_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNThue", ParameterDirection.Input, CStr(HDR.DC_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ly_Do", ParameterDirection.Input, CStr(HDR.Ly_Do), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_KS", ParameterDirection.Input, CStr(HDR.Ma_KS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_TQ", ParameterDirection.Input, CStr(HDR.Ma_TQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_QD", ParameterDirection.Input, CStr(HDR.So_QD), DbType.String))
                    'If String.IsNullOrEmpty(HDR.Ngay_QD) Then HDR.Ngay_QD = DateTime.Now
                    'Dim today As Date = DateTime.ParseExact(HDR.Ngay_QD, "dd/MM/yyyy HH:mm tt", CultureInfo.CurrentCulture)
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_QD", ParameterDirection.Input, HDR.Ngay_QD, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":CQ_QD", ParameterDirection.Input, CStr(HDR.CQ_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_CT", ParameterDirection.Input, CStr(HDR.Ngay_CT), DbType.String))
                    'Dim dNgay_HT As DateTime = DateTime.ParseExact(HDR.Ngay_HT, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_HT", ParameterDirection.Input, HDR.Ngay_HT, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_CQThu", ParameterDirection.Input, CStr(HDR.Ma_CQThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":XA_ID", ParameterDirection.Input, CStr(HDR.XA_ID), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Tinh", ParameterDirection.Input, CStr(HDR.Ma_Tinh), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Huyen", ParameterDirection.Input, CStr(HDR.Ma_Huyen), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Xa", ParameterDirection.Input, CStr(HDR.Ma_Xa), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_No", ParameterDirection.Input, CStr(HDR.TK_No), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_Co", ParameterDirection.Input, CStr(HDR.TK_Co), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_TK", ParameterDirection.Input, CStr(HDR.So_TK), DbType.String))
                    'If String.IsNullOrEmpty(HDR.Ngay_TK) Then HDR.Ngay_TK = DateTime.Now
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_TK", ParameterDirection.Input, HDR.Ngay_TK, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LH_XNK", ParameterDirection.Input, CStr(HDR.LH_XNK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DVSDNS", ParameterDirection.Input, CStr(HDR.DVSDNS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_DVSDNS", ParameterDirection.Input, CStr(HDR.Ten_DVSDNS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_NT", ParameterDirection.Input, CStr(HDR.Ma_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ty_Gia", ParameterDirection.Input, CStr(HDR.Ty_Gia), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TG_ID", ParameterDirection.Input, CStr(HDR.TG_ID), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_LThue", ParameterDirection.Input, CStr(HDR.Ma_LThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_Khung", ParameterDirection.Input, CStr(HDR.So_Khung), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_May", ParameterDirection.Input, CStr(HDR.So_May), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(HDR.TK_KH_NH), DbType.String))
                    'If String.IsNullOrEmpty(HDR.NGAY_KH_NH) Then HDR.NGAY_KH_NH = DateTime.Now
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, HDR.NGAY_KH_NH, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, CStr(HDR.MA_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_B", ParameterDirection.Input, CStr(HDR.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NH_B", ParameterDirection.Input, CStr(HDR.Ten_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_NH_TT", ParameterDirection.Input, CStr(HDR.TEN_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTien", ParameterDirection.Input, CStr(HDR.TTien), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_TThu", ParameterDirection.Input, CStr(HDR.TT_TThu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Lan_In", ParameterDirection.Input, CStr(HDR.Lan_In), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Trang_Thai", ParameterDirection.Input, CStr(HDR.Trang_Thai), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_Nhan", ParameterDirection.Input, CStr(HDR.TK_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ten_KH_Nhan", ParameterDirection.Input, CStr(HDR.Ten_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Diachi_KH_Nhan", ParameterDirection.Input, CStr(HDR.Diachi_KH_Nhan), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTien_NT", ParameterDirection.Input, CStr(HDR.TTien_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(HDR.PT_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_BK", ParameterDirection.Input, CStr(HDR.So_BK), DbType.String))
                    'If String.IsNullOrEmpty(HDR.Ngay_BK) Then HDR.Ngay_BK = DateTime.Now
                    listParam.Add(DataAccess.NewDBParameter(":Ngay_BK", ParameterDirection.Input, HDR.Ngay_BK, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":CTU_GNT_BL", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":So_CT", ParameterDirection.Input, CStr(HDR.So_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_GL_NH", ParameterDirection.Input, CStr(HDR.TK_GL_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(HDR.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KB_NH", ParameterDirection.Input, CStr(HDR.TK_KB_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ISACTIVE", ParameterDirection.Input, 1, DbType.Int32))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_GD", ParameterDirection.Input, CStr(HDR.PHI_GD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PHI_VAT", ParameterDirection.Input, CStr(HDR.PHI_VAT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TINHPHI", ParameterDirection.Input, CStr(HDR.PT_TINHPHI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SEQ_NO", ParameterDirection.Input, CStr(HDR.SEQ_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":REF_NO", ParameterDirection.Input, CStr(HDR.REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":RM_REF_NO", ParameterDirection.Input, CStr(HDR.RM_REF_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(HDR.QUAN_HUYEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TINH_TPNNTIEN", ParameterDirection.Input, CStr(HDR.TINH_TPHO_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(HDR.MA_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_TT", ParameterDirection.Input, CStr(HDR.LOAI_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, CStr(HDR.TEN_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, CStr(HDR.Ma_hq_ph), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, CStr(HDR.TEN_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(HDR.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_SANPHAM", ParameterDirection.Input, CStr(HDR.SAN_PHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_CITAD", ParameterDirection.Input, CStr(HDR.TT_CITAD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(HDR.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KENH_CT", ParameterDirection.Input, CStr(HDR.Kenh_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(HDR.Ma_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":partition_id", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":transaction_id", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":save_type", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":record_type", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":tran_type", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":gl_glsub", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":method", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":dup_allowflg", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":mod_allowflg", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":ac", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":file_import", ParameterDirection.Input, Nothing, DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":debit_method", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":value_date", ParameterDirection.Input, CInt(DateTime.Now.ToString("yyyyMMdd")), DbType.Int32))
                    listParam.Add(DataAccess.NewDBParameter(":so_lo", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":note", ParameterDirection.Input, CStr(HDR.Ghi_chu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":address", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":remarks", ParameterDirection.Input, CStr(HDR.Ghi_chu), DbType.String))
                    HDR.Ghi_chu = Globals.RemoveSign4VietnameseString(HDR.Ghi_chu)
                    Dim strRemarks As String = HDR.Ghi_chu
                    'If strRemarks.Length > 210 Then
                    '    strRemarks = strRemarks.Substring(0, 210)
                    'End If
                    listParam.Add(DataAccess.NewDBParameter(":remarks", ParameterDirection.Input, CStr(strRemarks), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(HDR.Ten_cqthu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NAME_TEN_CQTHU", ParameterDirection.Input, CStr(HDR.Ten_cqthu), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_TIEN_CHU", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LTHUE", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHXNK", ParameterDirection.Input, CStr(HDR.Ten_lhxnk), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(HDR.Ten_kb), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CUC", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CUC", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":BENE_SHORT_NAME", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_NNT", ParameterDirection.Input, "", DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":IMPORT_FILE", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PO_Date", ParameterDirection.Input, DateTime.Now.ToString("dd/MM/yyyy"), DbType.Date))
                    'listParam.Add(DataAccess.NewDBParameter(":HILOWVALUE", ParameterDirection.Input, Nothing, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_THU", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Huyen_NNThue", ParameterDirection.Input, CStr(HDR.Ma_Huyen_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":Ma_Tinh_NNThue", ParameterDirection.Input, CStr(HDR.Ma_Tinh_NNThue), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_QUAN_HUYENNNTIEN", ParameterDirection.Input, CStr(HDR.MA_QUAN_HUYENNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_TINH_TPNNTIEN", ParameterDirection.Input, CStr(HDR.MA_TINH_TPNNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAI_CTU", ParameterDirection.Input, CStr(HDR.LOAI_CTU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CMND", ParameterDirection.Input, CStr(HDR.SO_CMND), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_FONE", ParameterDirection.Input, CStr(HDR.SO_FONE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_BAONO", ParameterDirection.Input, CStr(HDR.NGAY_BAONO.ToString("dd/MM/yyyy")), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_BAOCO", ParameterDirection.Input, CStr(HDR.NGAY_BAOCO.ToString("dd/MM/yyyy")), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":diengiai_hq", ParameterDirection.Input, HDR.DIENGIAI_HQ, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_nh_a", ParameterDirection.Input, HDR.Ten_nh_a, DbType.String))
                    Dim v_sqlT As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)
                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....
                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If listDTL IsNot Nothing AndAlso listDTL.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each DTL In listDTL
                                DTL.ID = Business_HQ.HaiQuan.HaiQuanController.GenSequenceKeyTable("seq_tcs_ctu_hq_nhothu_dtl.NEXTVAL")
                                DTL.ID = DateTime.Now.ToString("yyyyMMdd") & DTL.ID.PadLeft(10, "0")
                                DTL.SO_CT = HDR.So_CT
                                DTL.SHKB = HDR.SHKB
                                DTL.Ngay_KB = HDR.Ngay_KB
                                DTL.Ma_NV = HDR.Ma_NV
                                DTL.So_BT = HDR.So_BT
                                DTL.Ma_DThu = HDR.Ma_DThu
                                iRet = 0
                                sSQL = "Insert into TCS_CTU_HQ_NHOTHU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                                        "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                                        "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT,maquy,SO_CT,ma_dp, SO_TK, NGAY_TK, MA_LHXNK, MA_LT, MA_HQ,SAC_THUE,TT_BTOAN) " & _
                                        "Values (:ID, :SHKB, :Ngay_KB, :Ma_NV, :So_BT, :Ma_DThu, :CCH_ID, :Ma_Cap, " & _
                                        ":Ma_Chuong, :LKH_ID, :Ma_Loai, :Ma_Khoan, :MTM_ID, :Ma_Muc, :Ma_TMuc, :Noi_Dung," & _
                                        ":DT_ID, :Ma_TLDT, :Ky_Thue, :SoTien, :SoTien_NT, :maquy, :SO_CT, :ma_dp, :SO_TK, TO_DATE (:NGAY_TK, 'RRRR-MM-DD'), :MA_LHXNK, :MA_LT, :MA_HQ, :SAC_THUE, :TT_BTOAN) "
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":ID", ParameterDirection.Input, DTL.ID, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(DTL.SHKB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ngay_KB", ParameterDirection.Input, CStr(DTL.Ngay_KB), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_NV", ParameterDirection.Input, CStr(DTL.Ma_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":So_BT", ParameterDirection.Input, CStr(DTL.So_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_DThu", ParameterDirection.Input, CStr(DTL.Ma_DThu), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":CCH_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Cap", ParameterDirection.Input, CStr(DTL.Ma_Cap), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Chuong", ParameterDirection.Input, CStr(DTL.Ma_Chuong), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":LKH_ID", ParameterDirection.Input, CStr(DTL.LKH_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Loai", ParameterDirection.Input, CStr(DTL.Ma_Loai), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Khoan", ParameterDirection.Input, CStr(DTL.Ma_Khoan), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MTM_ID", ParameterDirection.Input, CStr(DTL.MTM_ID), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_Muc", ParameterDirection.Input, CStr(DTL.Ma_Muc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TMuc", ParameterDirection.Input, CStr(DTL.Ma_TMuc), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Noi_Dung", ParameterDirection.Input, CStr(DTL.Noi_Dung), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":DT_ID", ParameterDirection.Input, DBNull.Value, DbType.Int32))
                                listParam.Add(DataAccess.NewDBParameter(":Ma_TLDT", ParameterDirection.Input, CStr(DTL.Ma_TLDT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":Ky_Thue", ParameterDirection.Input, CStr(DTL.Ky_Thue), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien", ParameterDirection.Input, CStr(DTL.SoTien), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SoTien_NT", ParameterDirection.Input, CStr(DTL.SoTien_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":maquy", ParameterDirection.Input, CStr(DTL.MaQuy), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(DTL.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":ma_dp", ParameterDirection.Input, CStr(DTL.Ma_DP), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(DTL.SO_TK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, DTL.NGAY_TK, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LHXNK", ParameterDirection.Input, CStr(DTL.LH_XNK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_LT", ParameterDirection.Input, CStr(DTL.MA_LT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(DTL.MA_HQ), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(DTL.SAC_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":TT_BTOAN", ParameterDirection.Input, CStr(DTL.TT_BTOAN), DbType.String))
                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next
                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If
                    If bRet = True Then
                        Dim strSQLUpdate As String = "UPDATE TCS_HQ247_201 SET SO_CTU='" & HDR.So_CT & "' WHERE ID='" & HDR.REF_NO & "' AND SO_CTU IS NULL  "
                        iRet = DataAccess.ExecuteNonQuery(strSQLUpdate, CommandType.Text, tran)
                        If iRet > 0 Then
                            bRet = True
                        Else
                            bRet = False
                        End If
                    End If
                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try
            Return bRet
        End Function
        Public Shared Function UpdateTTSP(ByVal pStrSoCT As String, ByVal strMA_NH_TT As String, ByVal strTEN_NH_TT As String, ByVal strMA_NH_B As String, ByVal strTEN_NH_B As String, ByVal strSo_ct_nh As String, ByVal strRemarks As String)
            Try
                'ma_nh_b,ten_nh_b,ma_nh_tt,ten_nh_tt
                Dim strSQL As String = "UPDATE TCS_CTU_HQ_NHOTHU_HDR SET ma_nh_tt='" & strMA_NH_TT & "',ten_nh_tt='" & strTEN_NH_TT & "',ma_nh_b='" & strMA_NH_B & "',ten_nh_b='" & strTEN_NH_B & "', so_ct_nh='" & strSo_ct_nh & "',remarks='" & strRemarks & "', ngay_kh_nh=sysdate     WHERE SO_CT='" & pStrSoCT & "';"
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function GetSoBT(ByVal iNgayLV As Integer, ByVal iMaNV As Integer) As String
            Dim sRet As String = "0"
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select MAX(So_BT) AS So_BT From TCS_CTU_HQ_NHOTHU_HDR Where ngay_kb = :ngay_kb AND Ma_NV = :Ma_NV"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("ngay_kb", ParameterDirection.Input, iNgayLV, DbType.Int32))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, iMaNV, DbType.Int32))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                sRet = "0"
            End Try

            Return sRet
        End Function
        Public Shared Function checkTrangthaiMSG201CTU(ByVal pStrID As String, ByVal pStrTrangthai As String, Optional ByVal pvStrSOCTU As String = "") As Decimal
            Try
                Dim strSQL As String = "SELECT * FROM TCS_HQ247_201 WHERE ID='" & pStrID & "' AND TRANGTHAI='" & pStrTrangthai & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If pvStrSOCTU.Length > 0 Then
                                If pvStrSOCTU.Length = 1 Then
                                    If ds.Tables(0).Rows(0)("SO_CTU").ToString().Length > 0 Then
                                        Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                                    End If
                                Else
                                    If Not ds.Tables(0).Rows(0)("SO_CTU").ToString().Equals(pvStrSOCTU) Then
                                        Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                                    End If
                                End If

                            End If
                            Return mdlCommon.TCS_HQ247_OK
                        End If
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
        End Function
        Public Shared Function checkTrangthaiMSG201CTUNH(ByVal pStrID As String, ByVal pStrTrangthai As String, ByVal pvStrSOCTU As String, Optional ByVal pvStrSOCTU_NH As String = "") As Decimal
            Try
                Dim strSQL As String = "SELECT * FROM TCS_HQ247_201 WHERE ID='" & pStrID & "' AND TRANGTHAI='" & pStrTrangthai & "' and so_ctu='" & pvStrSOCTU & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            If pvStrSOCTU_NH.Length > 0 Then
                                If ds.Tables(0).Rows(0)("so_ct_nh").ToString().Length > 0 Then
                                    Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
                                End If
                            End If
                            Return mdlCommon.TCS_HQ247_OK
                        End If

                    End If
                End If

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return Common.mdlCommon.TCS_HQ247_HQ_SAI_TRANGTHAI
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_201_MAKER213(ByVal pStrID304 As String, ByVal pStrLyDo As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Try
                conn = DataAccess.GetConnection()
                'conn.Open()
                tran = conn.BeginTransaction()
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213 & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", LOAI213=1 "
                StrSQL &= ", LYDO=:LYDO "
                StrSQL &= ", ID_MK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                ' VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Dim lsParam As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                lsParam.Add(DataAccess.NewDBParameter("LYDO", ParameterDirection.Input, pStrLyDo, DbType.String))
                Dim iret = VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, lsParam.ToArray(), tran)
                If iret > 0 Then
                    tran.Commit()
                    Return mdlCommon.TCS_HQ247_OK
                Else
                    tran.Rollback()
                End If

            Catch ex As Exception
                tran.Rollback()
                log.Error("TCS_HQ247_TRANGTHAI_201_MAKER213  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_201_MAKER213  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Sub TCS_HQ247_TRANGTHAI_201_SEND301_ERROR(ByVal pStrID304 As String, Optional ByVal pStrTenDN As String = "NTDTHQ")
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_201_SEND301_ERROR & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_201_SEND301_ERROR  pStrID201[" & pStrID304 & "]   ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_201_SEND301_ERROR  pStrID201[" & pStrID304 & "]   ")
            End Try

        End Sub
        Public Shared Function TCS_HQ247_TRANGTHAI_201_SEND301_OK(ByVal pStrID304 As String, ByVal pStrMSG213ID As String, ByVal pStrSo_TN_CT As String, ByVal pStrNgay_TN_CT As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_OK & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", MSG_ID301='" & pStrMSG213ID & "'  "
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= ", SO_TN_CT='" & pStrSo_TN_CT & "' "
                StrSQL &= ", Ngay_TN_CT='" & pStrNgay_TN_CT & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                'CAP NHAT TRANG THAI CHUNG TU
                StrSQL = "UPDATE TCS_CTU_HQ_NHOTHU_HDR SET TRANG_THAI='01' WHERE SO_CT IN (SELECT SO_CTU FROM TCS_HQ247_201 where ID='" & pStrID304 & "'  )"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_201_SEND301_OK  pStrID201[" & pStrID304 & "]   ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_201_SEND301_OK  pStrID201[" & pStrID304 & "]   ")
                Return mdlCommon.TCS_HQ247_HQ_CAPNHAT_TRANGTHAI304_SAUKHISEN_301
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Sub getSotiepNhan(ByVal pv_str201ID As String, ByVal pv_strso_ct As String, ByRef pv_so_tn_ct As String, ByRef pv_ngay_tn_ct As String, ByRef v_strTransaction_id As String)
            Try
                Dim strSQL As String = "SELECT * FROM TCS_HQ247_201 WHERE ID='" & pv_str201ID & "' AND so_ctu='" & pv_strso_ct & "' "
                pv_so_tn_ct = ""
                pv_ngay_tn_ct = ""
                v_strTransaction_id = ""
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            pv_so_tn_ct = ds.Tables(0).Rows(0)("so_tn_ct").ToString()
                            pv_ngay_tn_ct = ds.Tables(0).Rows(0)("ngay_tn_ct").ToString()
                            v_strTransaction_id = ds.Tables(0).Rows(0)("msg_id301").ToString()
                        End If
                    End If
                End If

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' 
            End Try
        End Sub
        Public Shared Function SelectCT_HDR(ByVal pvStrSO_CTU As String) As DataSet
            ' ****************************************************************************
            ' Mục đích: lấy ra một chứng từ trong bảng TCS_CTU_HDR với các điều kiện đầu vào
            ' Người viết: HoanNH
            ' Ngày viết: 16/10/2007
            '*****************************************************************************
            Dim dsCT As New DataSet
            Dim strSql As String
            Try
                strSql = " Select " & _
                           " shkb,ten_kb, ngay_kb, ma_nv, so_bt, ma_dthu, so_bthu, " & _
                           " kyhieu_ct, so_ct, so_ct_nh, ma_nntien, ten_nntien, " & _
                           " dc_nntien, ma_nnthue, ten_nnthue, dc_nnthue, ly_do_huy, " & _
                           " ma_ks, ma_tq, so_qd, ngay_qd, cq_qd, ngay_ct, " & _
                           " ngay_ht, ma_cqthu,ten_cqthu, xa_id, ma_tinh, ma_huyen, ma_xa, " & _
                           " tk_no, tk_co, so_tk, to_char(ngay_tk,'dd/MM/yyyy') ngay_tk, lh_xnk, dvsdns, " & _
                           " ten_dvsdns, ma_nt, ty_gia, tg_id, ma_lthue, so_khung, " & _
                           " so_may, tk_kh_nh, ngay_kh_nh, ma_nh_a, ma_nh_b, " & _
                           " ttien, tt_tthu, lan_in, trang_thai, tk_kh_nhan, " & _
                           " ten_kh_nhan, diachi_kh_nhan, ttien_nt, so_bt_ktkb, " & _
                           " pt_tt, tt_in, tt_in_bk, so_bk, ngay_bk, ctu_gnt_bl, " & _
                           " ref_no, tt_thu, ten_kh_nh, tk_gl_nh, tk_kb_nh, " & _
                           " isactive, seq_no, phi_gd, phi_vat, pt_tinhphi, " & _
                           " tt_cthue, rm_ref_no, ngay_ks, quan_huyennntien,ten_nh_tt,ten_nh_a,ma_nh_tt, " & _
                           " tinh_tpnntien, tkht, loai_tt,loai_tt ma_loaitien, ma_ntk, ma_hq, ten_hq, ma_hq_ph, ten_hq_ph,ten_nh_b,DIENGIAI_HQ, to_char(ngay_baono,'DD/MM/RRRR') ngay_baono, to_char(ngay_baoco,'DD/MM/RRRR') ngay_baoco,SO_CMND " & _
                         " From TCS_CTU_HQ_NHOTHU_HDR WHERE SO_CT='" & pvStrSO_CTU & "' "
                dsCT = DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return dsCT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu bảng TCS_CTU_HDR!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng kết nối
            End Try
        End Function
        Public Shared Function SelectCT_DTL(ByVal pvStrSO_CTU As String) As DataSet
            Dim dsCT As New DataSet
            Dim strSql As String
            Try
                strSql = " Select SHKB, Ngay_KB, Ma_NV, So_BT, Ma_DThu, CCH_ID, Ma_Cap," & _
                         " Ma_Chuong, LKH_ID, Ma_Loai, Ma_Khoan, MTM_ID, Ma_Muc, Ma_TMuc, Noi_Dung," & _
                         " DT_ID, Ma_TLDT, Ma_dp, Ky_Thue, SoTien, SoTien_NT " & _
                         " From TCS_CTU_HQ_NHOTHU_DTL " & _
                         " Where SO_CT='" & pvStrSO_CTU & "' "
                dsCT = DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return dsCT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin dữ liệu chứng từ")
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_201_CHEKER213(ByVal pStrID304 As String, ByVal pStrMSI213ID As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213 & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", MSG_ID213='" & pStrMSI213ID & "'  "
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_201_CHEKER213  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '   LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_201_CHEKER213  pStrID304[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_201_CHEKER213_TUCHOI(ByVal pStrID304 As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213_TUCHOI & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_201_CHEKER213_TUCHOI  pStrID201[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_201_CHEKER213_TUCHOI  pStrID201[" & pStrID304 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_201_CHEKER213_RESPONSE_TUCHOI(ByVal pStrID304 As String, ByVal pStrMSG213ID As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213_RESPONSE_TUCHOI & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", MSG_ID213='" & pStrMSG213ID & "'  "
                StrSQL &= ", ID_CK_213='" & pStrTenDN & "' "
                StrSQL &= " WHERE ID='" & pStrID304 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI  pStrID304[" & pStrID304 & "]   ")
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI  pStrID304[" & pStrID304 & "]   ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function getMSG201_Tracuu(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT ROWNUM STT, X.*"
                strSQL &= " FROM (SELECT A.ID,"
                strSQL &= " A.MA_NT,"
                strSQL &= " A.TAIKHOAN_TH,"
                strSQL &= " A.HQ_SOTIEN_TO,"
                strSQL &= " A.MA_DV,"
                strSQL &= " A.TEN_DV,"
                strSQL &= " TO_CHAR( TO_DATE(ngay_tn_ct,'RRRR-MM-DD""T""HH24:MI:SS'),'DD/MM/RRRR HH24:MI:SS') as ngay_tn_ct,"
                strSQL &= " A.TRANGTHAI,"
                strSQL &= " B.MOTA TRANG_THAI,"
                strSQL &= " NVL ("
                strSQL &= " (SELECT MAX (X.NAVIGATEURL) || '?ID=' || A.ID"
                strSQL &= " FROM TCS_SITE_MAP X"
                strSQL &= " WHERE X.NODEID LIKE '12.3%' AND X.NODEID IN ('12.3.6')),"
                strSQL &= " '#')"
                strSQL &= " NAVIGATEURL,"
                strSQL &= " A.SO_CTU,"
                strSQL &= " TO_CHAR (a.ngay_ht, 'DD/MM/RRRR HH24:MI:SS') NGAY_HT,"
                strSQL &= " A.SO_CT_NH,"
                strSQL &= " TO_CHAR (A.NGAYNHANMSG, 'DD/MM/RRRR HH24:MI:SS') NGAYNHANMSG,"
                strSQL &= " (case when A.trangthai<>'04' then A.lydo else '' end)  as Lydo"
                strSQL &= " FROM TCS_HQ247_201 A, TCS_DM_TRANGTHAI_TT_HQ247 B"
                strSQL &= " WHERE A.TRANGTHAI = B.TRANGTHAI " & pvStrWhereClause
                strSQL &= " ORDER BY A.ID) X"
                strSQL &= " ORDER BY X.ID"
                'Dim strSQL As String = "  SELECT ROWNUM STT,X.* FROM (   SELECT A.ID, A.HQ_SO_CTU, A.HQ_KYHIEU_CTU, A.MA_NT,  A.TAIKHOAN_TH, "
                'strSQL &= " A.HQ_SOTIEN_TO, A.MA_DV, A.TEN_DV, A.TRANGTHAI,TO_CHAR( A.HQ_NGAYLAP_CT,'DD/MM/RRRR')  HQ_NGAYLAP_CT ,"
                'strSQL &= "  B.MOTA TRANG_THAI, C.TENLOAI LOAI_THUE,"
                'strSQL &= "   NVL((SELECT MAX(X.NAVIGATEURL)||'?ID='||A.ID FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '12.3%' AND X.NODEID IN ('12.3.6')),'#') NAVIGATEURL"
                'strSQL &= " ,A.SO_CTU, TO_CHAR( A.HQ_NGAY_CT,'DD/MM/RRRR')  HQ_NGAY_CT, A.SO_CT_NH,TO_CHAR(A.NGAYNHANMSG,'DD/MM/RRRR') NGAYNHANMSG  "
                'strSQL &= " FROM TCS_HQ247_201 A, TCS_DM_TRANGTHAI_TT_HQ247 B,TCS_HQ247_LOAITHUE C"
                'strSQL &= "  WHERE A.TRANGTHAI = B.TRANGTHAI AND A.LOAIMSG = C.MALOAI  " & pvStrWhereClause
                'strSQL &= " ORDER BY A.ID) X ORDER BY X.ID "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function

        Public Shared Function TCS_HQ247_TRANGTHAI_201_CHO_CORE_RESPONSE(ByVal pStrID201 As String) As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_201_CHO_CORE_RESPONSE & "'"
                StrSQL &= ",NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= " WHERE ID='" & pStrID201 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_201_CHO_CORE_RESPONSE  pStrID201[" & pStrID201 & "]")
                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_201_CHO_CORE_RESPONSE  pStrID201[" & pStrID201 & "]")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function TCS_HQ247_TRANGTHAI_201_TUCHOI(ByVal pStrID201 As String, ByVal pStrTrangThai As String) As Decimal
            Try
                Dim StrSQL As String = "UPDATE TCS_HQ247_201 SET TRANGTHAI='" & pStrTrangThai & "'"
                StrSQL &= ",NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= " WHERE ID='" & pStrID201 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("TCS_HQ247_TRANGTHAI_201_TUCHOI  pStrID201[" & pStrID201 & "]")
                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog(ex, "TCS_HQ247_TRANGTHAI_201_TUCHOI  pStrID201[" & pStrID201 & "]")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function

        Public Shared Function Update201_HachToan(ByVal pStrID201 As String, ByVal pSO_CT_NH As String, ByVal pDienGiai As String, Optional ByVal pStrTenDN As String = "NTDTHQ") As Decimal
            Try
                Dim StrSQL As String = "UPDATE tcs_hq247_201 SET TRANGTHAI='" & mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213 & "'"
                StrSQL &= ", NGAY_HT=TO_DATE('" & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & "','DD/MM/RRRR HH24:MI:SS')"
                StrSQL &= ", SO_CT_NH='" & pSO_CT_NH & "'"
                'StrSQL &=", DIEN_GIAI='" & pDienGiai & "'"
                StrSQL &= " WHERE ID='" & pStrID201 & "'"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                StrSQL = "UPDATE tcs_ctu_hq_nhothu_hdr SET so_ct_nh='" & pSO_CT_NH & "',ngay_kh_nh = sysdate,remarks='" & pDienGiai & "'  WHERE SO_CT IN (SELECT SO_CTU FROM tcs_hq247_201 WHERE ID='" & pStrID201 & "' )"
                VBOracleLib.DataAccess.ExecuteNonQuery(StrSQL, CommandType.Text)
                Return mdlCommon.TCS_HQ247_OK
            Catch ex As Exception
                log.Error("Update201_HachToan  pStrID201[" & pStrID201 & "] pStrTenDN[" & pStrTenDN & "]  ")
                log.Error(ex.Message & "-" & ex.StackTrace) '  VBOracleLib.LogDebug.WriteLog(ex, "Update201_HachToan  pStrID201[" & pStrID201 & "] pStrTenDN[" & pStrTenDN & "]  ")
            End Try
            Return mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
        End Function
        Public Shared Function getMSG201Detail(ByVal pvStrID As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT A.*,B.MOTA TRANG_THAI "
                strSQL &= " FROM TCS_HQ247_201 A, TCS_DM_TRANGTHAI_TT_HQ247 B"
                strSQL &= "  WHERE A.TRANGTHAI IN ('03','04','10') AND A.TRANGTHAI = B.TRANGTHAI AND A.ID='" & pvStrID & "'"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
        Public Shared Function getMSG201_resend(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = "SELECT ROWNUM STT,X.* FROM ( SELECT A.ID, A.SO_CTU, A.SO_CT_NH,A.MA_DV,A.TEN_DV, A.TAIKHOAN_TH,"
                strSQL &= " TO_CHAR(A.ngaynhanmsg,'DD/MM/RRRR HH24:MI:SS') ngaynhan201,"
                strSQL &= " TO_CHAR(TO_DATE(A.ngay_tn_ct,'RRRR-MM-DD""T""HH24:MI:SS'),'DD/MM/RRRR HH24:MI:SS') as ngay_hq_ct,"
                strSQL &= " A.hq_sotien_to as sotien,"
                strSQL &= " A.TRANGTHAI, B.MOTA TRANG_THAI"
                strSQL &= " FROM tcs_hq247_201 A, TCS_DM_TRANGTHAI_TT_HQ247 B"
                strSQL &= " WHERE A.TRANGTHAI = B.TRANGTHAI " & pvStrWhereClause
                strSQL &= " ORDER BY A.ID) X ORDER BY X.ID"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function

        Public Shared Function getGNT_HQNhothu(ByVal pvIDCT As String) As DataSet
            Dim ds As DataSet = Nothing
            Try
                Dim sSQL As String = "select a.SHKB,a.ten_kb,a.Ngay_KB,a.Ma_NV, a.Ly_do_huy, " & _
                       "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                       "a.KyHieu_CT,a.So_CT,a.So_CT_NH,a.SEQ_NO,a.so_xcard," & _
                       "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                       "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
                       "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD,a.TK_GL_NH," & _
                       "a.Ngay_QD ,a.CQ_QD,a.Ngay_CT," & _
                       "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
                       "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co,a.ngay_ht," & _
                       "a.So_TK, to_char(a.Ngay_TK,'dd/MM/RRRR') as Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                       "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                       "a.So_Khung,a.So_May,a.TK_KH_NH,a.NGAY_KH_NH,a.TEN_KH_NH as TEN_KH_NH," & _
                       "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu, a.MA_HUYEN_NNTHUE, a.MA_TINH_NNTHUE, " & _
                       "(select xa1.TEN from TCS_DM_XA xa1 where xa1.MA_XA = a.MA_HUYEN_NNTHUE) HUYEN_NNTHUE, " & _
                       "(select xa2.TEN from TCS_DM_XA xa2 where xa2.MA_XA = a.MA_TINH_NNTHUE) TINH_NNTHUE, " & _
                       "a.Lan_In,a.Trang_Thai,a.so_bt_ktkb, a.TT_CTHUE,a.ma_sanpham, a.MA_QUAN_HUYENNNTIEN, a.MA_TINH_TPNNTIEN, " & _
                       "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT,a.TT_CITAD,A.SO_CMND,A.SO_FONE,A.MA_CUC,A.TEN_CUC, " & _
                       " a.ten_nh_b, " & _
                       " a.MA_NTK,a.time_begin,a.ngay_ks, a.PO_DATE, a.NGAY_BAONO, a.NGAY_BAOCO, " & _
                       "a.Ten_CQThu,a.PT_TT as PT_TT,a.isactive,a.tt_tthu,a.ma_tq, " & _
                       "a.So_BK, a.Ngay_BK,a.PHI_GD,A.PHI_VAT,a.PHI_GD2,A.PHI_VAT2,a.PT_TINHPHI,a.RM_REF_NO,a.REF_NO, " & _
                       "a.QUAN_HUYENNNTIEN, a.TINH_TPNNTIEN, a.MA_HQ, a.LOAI_TT, a.TEN_HQ,a.REMARKS,a.MA_HQ_PH, a.TEN_HQ_PH, a.MA_NH_TT, a.ten_nh_tt,a.MA_SANPHAM, a.MSG_LOI, a.LOAI_CTU,a.diengiai_hq as dien_giai_hq, a.ma_cn " & _
                       "from tcs_ctu_hq_nhothu_hdr a " & _
                       "Where a.ref_no = '" & pvIDCT & "'"
                ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi in chứng từ HQNHOTHU")
                Throw ex
            End Try
            Return ds
        End Function
        Public Shared Function getChitietGNT_HQNhothu(ByVal pvSo_ct As String) As DataSet
            Try
                Dim strSQL As String = "select rownum as stt,noi_dung as NoiDung, (ma_cap||ma_chuong) as CC, " & _
                        "so_tk as LK, (ma_muc||ma_tmuc) as MTM, " & _
                        "ky_thue as KyThue, sotien as SoTien, sotien_nt as SoTienNT " & _
                        "from tcs_ctu_hq_nhothu_dtl " & _
                        "where so_ct = '" & pvSo_ct & "'" & _
                        " order by rownum "
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi in chứng từ HQNHOTHU")
                Throw ex
            End Try
        End Function
        Public Shared Function getHdr201_UNT(ByVal pvStrID As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String
                strSQL = "SELECT a.*,to_char(a.ngay_baono,'dd/MM/RRRR') ngay_thanhtoan from tcs_ctu_hq_nhothu_hdr a "
                strSQL &= "WHERE a.ref_no ='" & pvStrID & "'"
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return ds
        End Function
    End Class
End Namespace

