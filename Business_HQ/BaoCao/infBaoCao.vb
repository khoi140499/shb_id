﻿Namespace BaoCao

    Public Class infBaoCao

#Region "Biến toàn cục"
        Public FblnPrintSuccess As Boolean
#End Region

#Region "Các thuộc tính của Bảng kê chứng từ"
        Public Fds As New DataSet
        Public TK_GL_NH As String
        Public Ma_Quy As String
        Public NgayNH As String
        Public Copy As String

        Private FTen_KB As String = ""
        Private FMa_KB As String
        Private FMaDiemThu As String = ""
        Private FBanThu As String = ""
        Private FNgay_CT As String = ""
        Private FDichSo As String = ""
        Private FThuquy As String = ""
        Private FTungay As String = ""
        Private FDenngay As String = ""
        Private FTrangThai As String = ""
        Private FTen_KTT As String = ""
        Private FTenDN As String = ""
        Private FTien_tronggio As String = ""
        Private FTien_ngoaiGio As String = ""
        Private Fsomon_trongGio As String = ""
        Private Fsomon_ngoaiGio As String = ""
        Private FNgay_NgoaiGio As String = ""
        Private FNgay_TrongGio As String = ""
        Private FMau_BaoCao As String = ""
        Private F_TT_Ctu As String = ""
        Private F_Loai_Thue As String = ""

        Public Property TT_Ctu() As String
            Get
                Return F_TT_Ctu
            End Get
            Set(ByVal Value As String)
                F_TT_Ctu = Value
            End Set
        End Property
        Public Property Loai_Thue() As String
            Get
                Return F_Loai_Thue
            End Get
            Set(ByVal Value As String)
                F_Loai_Thue = Value
            End Set
        End Property
        Public Property Mau_BaoCao() As String
            Get
                Return FMau_BaoCao
            End Get
            Set(ByVal Value As String)
                FMau_BaoCao = Value
            End Set
        End Property
        Public Property TienTrongGio() As String
            Get
                Return FTien_tronggio
            End Get
            Set(ByVal Value As String)
                FTien_tronggio = Value
            End Set
        End Property
        Public Property TienNgoaiGio() As String
            Get
                Return FTien_ngoaiGio
            End Get
            Set(ByVal Value As String)
                FTien_ngoaiGio = Value
            End Set
        End Property
        Public Property SoMon_TrongGio() As String
            Get
                Return Fsomon_trongGio
            End Get
            Set(ByVal Value As String)
                Fsomon_trongGio = Value
            End Set
        End Property
        Public Property SoMon_NgoaiGio() As String
            Get
                Return Fsomon_ngoaiGio
            End Get
            Set(ByVal Value As String)
                Fsomon_ngoaiGio = Value
            End Set
        End Property
        Public Property Ngay_TrongGio() As String
            Get
                Return FNgay_TrongGio
            End Get
            Set(ByVal Value As String)
                FNgay_TrongGio = Value
            End Set
        End Property
        Public Property Ngay_NgoaiGio() As String
            Get
                Return FNgay_NgoaiGio
            End Get
            Set(ByVal Value As String)
                FNgay_NgoaiGio = Value
            End Set
        End Property
        Public Property TenDN() As String
            Get
                Return FTenDN
            End Get
            Set(ByVal Value As String)
                FTenDN = Value
            End Set
        End Property

        Public Property TCS_Tungay() As String
            Get
                Return FTungay
            End Get
            Set(ByVal Value As String)
                FTungay = Value
            End Set
        End Property
        Public Property TCS_Denngay() As String
            Get
                Return FDenngay
            End Get
            Set(ByVal Value As String)
                FDenngay = Value
            End Set
        End Property
        Public Property TCS_PrintSuccess() As Boolean
            Get
                Return FblnPrintSuccess
            End Get
            Set(ByVal Value As Boolean)
                FblnPrintSuccess = Value
            End Set
        End Property
        Public Property TCS_Ma_KB() As String
            Get
                Return FMa_KB
            End Get
            Set(ByVal Value As String)
                FMa_KB = Value
            End Set
        End Property
        Public Property TCS_Ten_KB() As String
            Get
                Return FTen_KB
            End Get
            Set(ByVal Value As String)
                FTen_KB = Value
            End Set
        End Property
        Public Property TCS_MaDiemThu() As String
            Get
                Return FMaDiemThu
            End Get
            Set(ByVal Value As String)
                FMaDiemThu = Value
            End Set
        End Property
        Public Property TCS_BanThu() As String
            Get
                Return FBanThu
            End Get
            Set(ByVal Value As String)
                FBanThu = Value
            End Set
        End Property
        Public Property TCS_Ngay_CT() As String
            Get
                Return FNgay_CT
            End Get
            Set(ByVal Value As String)
                FNgay_CT = Value
            End Set
        End Property
        Public Property TCS_DichSo() As String
            Get
                Return FDichSo
            End Get
            Set(ByVal Value As String)
                FDichSo = Value
            End Set
        End Property
        Public Property TCS_DS() As DataSet
            Get
                Return Fds
            End Get
            Set(ByVal Value As DataSet)
                Fds = Value
            End Set
        End Property
        Public Property TCS_Thuquy() As String
            Get
                Return FThuquy
            End Get
            Set(ByVal Value As String)
                FThuquy = Value
            End Set
        End Property
        Public Property TCS_TrangThai() As String
            Get
                Return FTrangThai
            End Get
            Set(ByVal Value As String)
                FTrangThai = Value
            End Set
        End Property

        Public Property TCS_Ten_KTT() As String
            Get
                Return FTen_KTT
            End Get
            Set(ByVal Value As String)
                FTen_KTT = Value
            End Set
        End Property

#End Region

#Region "Các thuộc tính của Phiếu xác nhận chứng từ"
        Private mHDR_So_CT As String = ""
        Private mHDR_Ma_DTNT As String = ""
        Private mHDR_TenDTNT As String = ""
        Private mHDR_KH_CT As String = ""
        Private mHDR_DiaChi As String = ""

        Public Property HDR_So_CT() As String
            Get
                Return mHDR_So_CT
            End Get
            Set(ByVal Value As String)
                mHDR_So_CT = Value
            End Set
        End Property
        Public Property HDR_Ma_DTNT() As String
            Get
                Return mHDR_Ma_DTNT
            End Get
            Set(ByVal Value As String)
                mHDR_Ma_DTNT = Value
            End Set
        End Property

        Public Property HDR_Ten_DTNT() As String
            Get
                Return mHDR_TenDTNT
            End Get
            Set(ByVal Value As String)
                mHDR_TenDTNT = Value
            End Set
        End Property
        Public Property HDR_KH_CT() As String
            Get
                Return mHDR_KH_CT
            End Get
            Set(ByVal Value As String)
                mHDR_KH_CT = Value
            End Set
        End Property
        Public Property HDR_DiaChi() As String
            Get
                Return mHDR_DiaChi
            End Get
            Set(ByVal Value As String)
                mHDR_DiaChi = Value
            End Set
        End Property
#End Region

#Region "Các thuộc tính của GNT từ BLT"
        Private mTen_KB_Tinh As String = ""
        Private mTen_VP As String = ""
        Private mNgay As String = ""
        Private mThang As String = ""
        Private mNam As String = ""
        Private mTen_Nguoi_Nop As String = ""
        Private mTen_TKNo As String = ""
        Private mTK_No As String = ""
        Private mTen_TKCo As String = ""
        Private mTK_Co As String = ""
        Private mNoi_dung As String = ""
        Private mSHKB As String = ""
        Private mMLNS As String = ""
        Private mSo_Tien As Double = 0
        Private mTien_Bang_Chu As String = ""

        Public Property Ten_KB_Tinh() As String
            Get
                Return mTen_KB_Tinh
            End Get
            Set(ByVal Value As String)
                mTen_KB_Tinh = Value
            End Set
        End Property
        Public Property Ten_VP() As String
            Get
                Return mTen_VP
            End Get
            Set(ByVal Value As String)
                mTen_VP = Value
            End Set
        End Property
        Public Property Ngay() As String
            Get
                Return mNgay
            End Get
            Set(ByVal Value As String)
                mNgay = Value
            End Set
        End Property
        Public Property Thang() As String
            Get
                Return mThang
            End Get
            Set(ByVal Value As String)
                mThang = Value
            End Set
        End Property
        Public Property Nam() As String
            Get
                Return mNam
            End Get
            Set(ByVal Value As String)
                mNam = Value
            End Set
        End Property
        Public Property Ten_Nguoi_Nop() As String
            Get
                Return mTen_Nguoi_Nop
            End Get
            Set(ByVal Value As String)
                mTen_Nguoi_Nop = Value
            End Set
        End Property
        Public Property Ten_TKNo() As String
            Get
                Return mTen_TKNo
            End Get
            Set(ByVal Value As String)
                mTen_TKNo = Value
            End Set
        End Property
        Public Property TK_No() As String
            Get
                Return mTK_No
            End Get
            Set(ByVal Value As String)
                mTK_No = Value
            End Set
        End Property
        Public Property Ten_TKCo() As String
            Get
                Return mTen_TKCo
            End Get
            Set(ByVal Value As String)
                mTen_TKCo = Value
            End Set
        End Property
        Public Property TK_Co() As String
            Get
                Return mTK_Co
            End Get
            Set(ByVal Value As String)
                mTK_Co = Value
            End Set
        End Property
        Public Property Noi_dung() As String
            Get
                Return mNoi_dung
            End Get
            Set(ByVal Value As String)
                mNoi_dung = Value
            End Set
        End Property
        Public Property SHKB() As String
            Get
                Return mSHKB
            End Get
            Set(ByVal Value As String)
                mSHKB = Value
            End Set
        End Property
        Public Property MLNS() As String
            Get
                Return mMLNS
            End Get
            Set(ByVal Value As String)
                mMLNS = Value
            End Set
        End Property
        Public Property So_Tien() As Double
            Get
                Return mSo_Tien
            End Get
            Set(ByVal Value As Double)
                mSo_Tien = Value
            End Set
        End Property
        Public Property Tien_Bang_Chu() As String
            Get
                Return mTien_Bang_Chu
            End Get
            Set(ByVal Value As String)
                mTien_Bang_Chu = Value
            End Set
        End Property

#End Region

#Region "Các thuộc tính cua tien gia"
        Private mdtFrom As Date
        Public Property dtFromDate()
            Get
                Return mdtFrom
            End Get
            Set(ByVal Value)
                mdtFrom = Value
            End Set
        End Property

        Private mdtTo As Date
        Public Property dtToDate()
            Get
                Return mdtTo
            End Get
            Set(ByVal Value)
                mdtTo = Value
            End Set
        End Property
#End Region

#Region "Các thuộc tính của Chi Tiết Lỗi"
        Private mstrTSNCode As String = ""

        Public Property TNSCode() As String
            Get
                Return mstrTSNCode
            End Get
            Set(ByVal Value As String)
                mstrTSNCode = Value
            End Set
        End Property
#End Region

#Region "Các thuộc tính của chứng từ"
        Private mKHCT As String = ""
        Private mSoCT As String = ""
        Private mKHBL As String = ""
        Private mSoBL As String = ""
        Private mMa_NNThue As String = ""
        Private mTen_NNThue As String = ""
        Private mDC_NNThue As String = ""
        Private mHuyen_NNThue As String = ""
        Private mTinh_NNThue As String = ""

        Private mMa_NNTien As String = ""
        Private mTen_NNTien As String = ""
        Private mDC_NNTien As String = ""
        Private mHuyen_NNTien As String = ""
        Private mTinh_NNTien As String = ""

        Private mNHA As String = ""
        Private mNHB As String = ""
        Private mTKNo As String = ""
        Private mTKCo As String = ""
        Private mTK_CO_KB As String = ""

        Private mTen_KB As String = ""
        Private mTinh_KB As String = ""
        Private mTen_CQThu As String = ""
        Private mMa_CQThu As String = ""

        Private mSo_TK As String = ""
        Private mNgay_TK As String = ""
        Private mLHXNK As String = ""
        Private mSo_BK As String = ""
        Private mNgay_BK As String = ""
        Private mDBHC As String = ""

        Private mTen_ThuQuy As String = ""
        Private mTen_KeToan As String = ""
        Private mTen_KTT As String = ""
        Private mKey_MauCT As String = "0"
        Private mIsTamThu As String = "0"

        Private mSoQD As String = ""
        Private mNgayQD As String = ""
        Private mCQQD As String = ""

        Private mLyDo As String = ""
        Private mTien As String = ""
        Private mTen_Tinh As String = ""
        Private mTen_Huyen As String = ""

        Private mMaQuy As String = ""
        Private mTienMat As String = ""
        Private mSoLien As String = ""
        Private mTyGia As String = ""
        Private mTKKH As String = ""
        Private mblnTienNT As Boolean = False

        Public Property TK_KH_NH() As String
            Get
                Return mTKKH
            End Get
            Set(ByVal Value As String)
                mTKKH = Value
            End Set
        End Property
        Public Property TK_CO_KB() As String
            Get
                Return mTK_CO_KB
            End Get
            Set(ByVal Value As String)
                mTK_CO_KB = Value
            End Set
        End Property
        Public Property KHCT() As String
            Get
                Return mKHCT
            End Get
            Set(ByVal Value As String)
                mKHCT = Value
            End Set
        End Property
        Public Property SoCT() As String
            Get
                Return mSoCT
            End Get
            Set(ByVal Value As String)
                mSoCT = Value
            End Set
        End Property
        Public Property KHBL() As String
            Get
                Return mKHBL
            End Get
            Set(ByVal Value As String)
                mKHBL = Value
            End Set
        End Property
        Public Property SoBL() As String
            Get
                Return mSoBL
            End Get
            Set(ByVal Value As String)
                mSoBL = Value
            End Set
        End Property
        Public Property Ma_NNThue() As String
            Get
                Return mMa_NNThue
            End Get
            Set(ByVal Value As String)
                mMa_NNThue = Value
            End Set
        End Property
        Public Property Ten_NNThue() As String
            Get
                Return mTen_NNThue
            End Get
            Set(ByVal Value As String)
                mTen_NNThue = Value
            End Set
        End Property
        Public Property DC_NNThue() As String
            Get
                Return mDC_NNThue
            End Get
            Set(ByVal Value As String)
                mDC_NNThue = Value
            End Set
        End Property
        Public Property Huyen_NNThue() As String
            Get
                Return mHuyen_NNThue
            End Get
            Set(ByVal Value As String)
                mHuyen_NNThue = Value
            End Set
        End Property
        Public Property Tinh_NNThue() As String
            Get
                Return mTinh_NNThue
            End Get
            Set(ByVal Value As String)
                mTinh_NNThue = Value
            End Set
        End Property

        Public Property Ma_NNTien() As String
            Get
                Return mMa_NNTien
            End Get
            Set(ByVal Value As String)
                mMa_NNTien = Value
            End Set
        End Property
        Public Property Ten_NNTien() As String
            Get
                Return mTen_NNTien
            End Get
            Set(ByVal Value As String)
                mTen_NNTien = Value
            End Set
        End Property
        Public Property DC_NNTien() As String
            Get
                Return mDC_NNTien
            End Get
            Set(ByVal Value As String)
                mDC_NNTien = Value
            End Set
        End Property
        Public Property Huyen_NNTien() As String
            Get
                Return mHuyen_NNTien
            End Get
            Set(ByVal Value As String)
                mHuyen_NNTien = Value
            End Set
        End Property
        Public Property Tinh_NNTien() As String
            Get
                Return mTinh_NNTien
            End Get
            Set(ByVal Value As String)
                mTinh_NNTien = Value
            End Set
        End Property

        Public Property NHA() As String
            Get
                Return mNHA
            End Get
            Set(ByVal Value As String)
                mNHA = Value
            End Set
        End Property
        Public Property NHB() As String
            Get
                Return mNHB
            End Get
            Set(ByVal Value As String)
                mNHB = Value
            End Set
        End Property
        Public Property TKNo() As String
            Get
                Return mTKNo
            End Get
            Set(ByVal Value As String)
                mTKNo = Value
            End Set
        End Property
        Public Property TKCo() As String
            Get
                Return mTKCo
            End Get
            Set(ByVal Value As String)
                mTKCo = Value
            End Set
        End Property

        Public Property Ten_KB() As String
            Get
                Return mTen_KB
            End Get
            Set(ByVal Value As String)
                mTen_KB = Value
            End Set
        End Property
        Public Property Tinh_KB() As String
            Get
                Return mTinh_KB
            End Get
            Set(ByVal Value As String)
                mTinh_KB = Value
            End Set
        End Property
        Public Property Ten_CQThu() As String
            Get
                Return mTen_CQThu
            End Get
            Set(ByVal Value As String)
                mTen_CQThu = Value
            End Set
        End Property
        Public Property Ma_CQThu() As String
            Get
                Return mMa_CQThu
            End Get
            Set(ByVal Value As String)
                mMa_CQThu = Value
            End Set
        End Property

        Public Property So_TK() As String
            Get
                Return mSo_TK
            End Get
            Set(ByVal Value As String)
                mSo_TK = Value
            End Set
        End Property
        Public Property Ngay_TK() As String
            Get
                Return mNgay_TK
            End Get
            Set(ByVal Value As String)
                mNgay_TK = Value
            End Set
        End Property
        Public Property LHXNK() As String
            Get
                Return mLHXNK
            End Get
            Set(ByVal Value As String)
                mLHXNK = Value
            End Set
        End Property
        Public Property So_BK() As String
            Get
                Return mSo_BK
            End Get
            Set(ByVal Value As String)
                mSo_BK = Value
            End Set
        End Property
        Public Property Ngay_BK() As String
            Get
                Return mNgay_BK
            End Get
            Set(ByVal Value As String)
                mNgay_BK = Value
            End Set
        End Property
        Public Property DBHC() As String
            Get
                Return mDBHC
            End Get
            Set(ByVal Value As String)
                mDBHC = Value
            End Set
        End Property

        Public Property Ten_ThuQuy() As String
            Get
                Return mTen_ThuQuy
            End Get
            Set(ByVal Value As String)
                mTen_ThuQuy = Value
            End Set
        End Property
        Public Property Ten_KeToan() As String
            Get
                Return mTen_KeToan
            End Get
            Set(ByVal Value As String)
                mTen_KeToan = Value
            End Set
        End Property
        Public Property Ten_KTT() As String
            Get
                Return mTen_KTT
            End Get
            Set(ByVal Value As String)
                mTen_KTT = Value
            End Set
        End Property
        Public Property Key_MauCT() As String
            Get
                Return mKey_MauCT
            End Get
            Set(ByVal Value As String)
                mKey_MauCT = Value
            End Set
        End Property
        Public Property IsTamThu() As String
            Get
                Return mIsTamThu
            End Get
            Set(ByVal Value As String)
                mIsTamThu = Value
            End Set
        End Property
        Public Property SoQD() As String
            Get
                Return mSoQD
            End Get
            Set(ByVal Value As String)
                mSoQD = Value
            End Set
        End Property
        Public Property NgayQD() As String
            Get
                Return mNgayQD
            End Get
            Set(ByVal Value As String)
                mNgayQD = Value
            End Set
        End Property
        Public Property CQQD() As String
            Get
                Return mCQQD
            End Get
            Set(ByVal Value As String)
                mCQQD = Value
            End Set
        End Property
        Public Property LyDo() As String
            Get
                Return mLyDo
            End Get
            Set(ByVal Value As String)
                mLyDo = Value
            End Set
        End Property
        Public Property Tien() As String
            Get
                Return mTien
            End Get
            Set(ByVal Value As String)
                mTien = Value
            End Set
        End Property
        Public Property Ten_Tinh() As String
            Get
                Return mTen_Tinh
            End Get
            Set(ByVal Value As String)
                mTen_Tinh = Value
            End Set
        End Property
        Public Property Ten_Huyen() As String
            Get
                Return mTen_Huyen
            End Get
            Set(ByVal Value As String)
                mTen_Huyen = Value
            End Set
        End Property
        Public Property MaQuy() As String
            Get
                Return mMaQuy
            End Get
            Set(ByVal Value As String)
                mMaQuy = Value
            End Set
        End Property
        Public Property TienMat() As String
            Get
                Return mTienMat
            End Get
            Set(ByVal Value As String)
                mTienMat = Value
            End Set
        End Property
        Public Property SoLien() As String
            Get
                Return mSoLien
            End Get
            Set(ByVal Value As String)
                mSoLien = Value
            End Set
        End Property

        Public Property IsTienNT() As Boolean
            Get
                Return mblnTienNT
            End Get
            Set(ByVal Value As Boolean)
                mblnTienNT = Value
            End Set
        End Property
        Public Property TyGia() As String
            Get
                Return mTyGia
            End Get
            Set(ByVal Value As String)
                mTyGia = Value
            End Set
        End Property
#End Region

#Region "Các thuộc tính của Kho Quỹ"
        Private mstrTenNV As String = ""
        Private mdblTongNop As Double = 0

        Public Property TenNV() As String
            Get
                Return mstrTenNV
            End Get
            Set(ByVal Value As String)
                mstrTenNV = Value
            End Set
        End Property

        Public Property TongNop() As Double
            Get
                Return mdblTongNop
            End Get
            Set(ByVal Value As Double)
                mdblTongNop = Value
            End Set
        End Property
#End Region

    End Class

End Namespace