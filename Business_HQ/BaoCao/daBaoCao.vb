﻿Imports VBOracleLib
Imports Business_HQ.Common.mdlSystemVariables

Namespace BaoCao
    Public Class daBaoCao
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "Danh Mục Hệ thống"
        Public Function TCS_DM_CAPCHUONG() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục cấp chương.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 25/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Dim dsResult As New DataSet
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "Select MA_CAP, MA_CHUONG, TEN " & _
                    "From TCS_DM_CAP_CHUONG " & _
                    "Order By MA_CAP, MA_CHUONG"
                dsResult = conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi load danh mục cấp chương")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
            Return dsResult
        End Function
        Public Function TCS_DM_LOAIKHOAN() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục loại khoản.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 25/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "Select MA_LOAI, MA_KHOAN, TEN " & _
                    "From TCS_DM_LOAI_KHOAN " & _
                    "Order By MA_LOAI, MA_KHOAN "
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi load danh mục Tài khoản")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_MUCTMUC() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục Mục - Tiểu mục.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 25/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "Select Ma_MUC, Ma_TMUC, TEN " & _
                    "From TCS_DM_MUC_TMUC " & _
                    "Order By Ma_MUC, Ma_TMUC"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi load danh mục mục - tiểu mục")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_DBHC() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục địa bàn hành chính
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 13/11/2007
            ' Người viết: Lê Hồng Hà
            ' Người sửa: Vũ Khắc Tiếp
            ' Sửa: lệnh truy vấn lấy dữ liệu ĐBHC
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = " select distinct tt.ma_xa as Ma_Tinh, tt.ten as Ten_Tinh, hh.ma_xa as Ma_Huyen, hh.ten as Ten_Huyen, xx.ma_xa as Ma_Xa, xx.ten as Ten_Xa   " & _
                        "from   tcs_dm_xa xx, tcs_dm_xa hh, tcs_dm_xa tt " & _
                        "where  xx.ma_cha = hh.ma_xa and hh.ma_cha=tt.ma_xa " & _
                        "order by ma_tinh, ma_huyen, ma_xa "

                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi load danh mục địa bàn hành chính")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_CQQD() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục cơ quan quyết định
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 13/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "select ma_cqqd as Ma_CQQD, ten_cqqd as Ten_CQQD, cap as Cap " & _
                        "from TCS_DM_CQQD " & _
                        "order by ma_cqqd"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục Cơ quan quyết định")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_CQThu() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục cơ quan thu
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 13/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "select ma_cqthu as Ma_CQThu, ten as Ten_CQThu " & _
                        "from TCS_DM_CQTHU " & _
                        "order by ma_cqthu"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục Cơ quan thu")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_LOAITIEN() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục loại tiền
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 13/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "select ma_loaitien as Ma_LoaiTien, menh_gia as Menh_Gia ,ghi_chu as Ghi_Chu " & _
                        "from TCS_DM_LOAITIEN " & _
                        "order by menh_gia"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi load danh mục loại tiền")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_NGUYENTE() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục nguyên tệ
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 13/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "select ma_nt as Ma_NT, ten as Ten_NT " & _
                        "from TCS_DM_NGUYENTE " & _
                        "order by ma_nt"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục nguyên tệ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_TYGIA() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục nguyên tệ
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 13/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "select tg.ma_nt as Ma_NT, tg.ty_gia as Ty_Gia, tg.ngay_hl as Ngay_HL " & _
                        "from TCS_DM_TYGIA tg " & _
                        "order by tg.ma_nt, tg.ngay_hl"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục tỷ giá")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_DM_TAIKHOAN() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục tài khoản
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 04/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "SELECT tk as Ma, ten_tk as Ten, ma_cqthu as Ma_CQThu, tinh_trang as Tinh_Trang " & _
                        "FROM TCS_DM_TAIKHOAN " & _
                        "order by tk"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục tài khoản")
                'LogDebug.WriteLog("Lỗi trong quá trình Lấy danh mục địa bàn hành chính: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_NNT() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục người nộp thuế
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 05/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "SELECT ma_nnt as Ma_NNT, ten_nnt as Ten, dia_chi as DiaChi, " & _
                        "ma_tinh as Ma_Tinh, ma_huyen as Ma_Huyen, ma_xa as Ma_Xa, " & _
                        "ma_cqthu as Ma_CQThu, ma_cap  as Ma_Cap, ma_chuong as Ma_Chuong, " & _
                        "ma_loai as Ma_Loai, ma_khoan as Ma_Khoan " & _
                        "FROM tcs_dm_nnt " & _
                        "order by ma_nnt "
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục người nộp thuế")
                'LogDebug.WriteLog("Lỗi trong quá trình Lấy danh mục địa bàn hành chính: " & ex.ToString, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_LHTHU() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục loại hình thu
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 05/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim strSQL As String
                strSQL = "SELECT lh.ten_lh as Ten_LHThu, mlns.ma_lh as Ma_LHThu, " & _
                        "mlns.ma_cap as Ma_Cap, mlns.ma_chuong as Ma_Chuong, mlns.ma_loai as Ma_Loai, " & _
                        "mlns.ma_khoan as Ma_Khoan, mlns.ma_muc as Ma_Muc, mlns.ma_tmuc as Ma_TMuc " & _
                        "FROM tcs_dm_lhthu_mlns mlns, tcs_dm_lhthu lh " & _
                        "where lh.ma_lh = mlns.ma_lh " & _
                        "order by lh.ma_lh "
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi load danh mục loại hình thu")
                'LogDebug.WriteLog("Lỗi trong quá trình Lấy danh mục địa bàn hành chính: " & ex.ToString, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_DM_LOAITHUE() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục loại hình thu
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 05/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "select lt.ma_lthue as Ma_LThue, lt.ten as Ten_LThue, " & _
                                "mtm.ma_muc as Ma_Muc,mtm.ma_tmuc as Ma_TMuc, mtm.ten as Ten_MTM " & _
                                "from tcs_dm_lthue lt, tcs_xdinh_lthue xdlt, tcs_dm_muc_tmuc mtm " & _
                                "where (lt.ma_lthue = xdlt.ma_lthue) and " & _
                                "(xdlt.ma_muc = mtm.ma_muc) and (xdlt.ma_tmuc = mtm.ma_tmuc) " & _
                                "union " & _
                                "select lt.ma_lthue as Ma_LThue, lt.ten as Ten_LThue, " & _
                                "mtm.ma_muc as Ma_Muc ,'' as Ma_Tmuc, max(mtm.Ten) as Ten_MTM " & _
                                "from tcs_dm_lthue lt, tcs_xdinh_lthue xdlt, tcs_dm_muc_tmuc mtm " & _
                                "where  lt.ma_lthue=xdlt.ma_lthue and xdlt.ma_muc=mtm.ma_muc " & _
                                "and xdlt.ma_tmuc is null " & _
                                "group by lt.ma_lthue, lt.ten, mtm.ma_muc " & _
                                "union " & _
                                "select lt.ma_lthue as Ma_LThue, lt.ten as Ten_LThue, " & _
                                "'' as Ma_Muc,'' as Ma_TMuc,'' as Ten_MTM " & _
                                "from tcs_dm_lthue lt where lt.ma_lthue not in " & _
                                "(select xdlt.ma_lthue from tcs_xdinh_lthue xdlt) "

                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi load danh mục loại thuế")
                'LogDebug.WriteLog("Lỗi trong quá trình Lấy danh mục địa bàn hành chính: " & ex.ToString, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_DM_TLDT() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục loại hình thu
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 05/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "select tl.tw as TW, tl.tt as TT, tl.qh as QH, tl.px, " & _
                                "tl.ma_tldt as Ma_TLDT, dt.ma_huyen as Ma_Huyen, " & _
                                "dt.ma_xa as Ma_Xa, dt.mlns as MLNS, dt.ma_cqthu as Ma_CQThu " & _
                                "from tcs_dm_tldt tl, tcs_dieutiet dt " & _
                                "where tl.dt_id = dt.dt_id (+) "

                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục tỉ lệ điều tiết")
                'LogDebug.WriteLog("Lỗi trong quá trình Lấy danh mục địa bàn hành chính: " & ex.ToString, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_DM_NGANHANG() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục ngân hàng
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 21/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "SELECT ma as Ma_NH, ma_tinh as Ma_Tinh, ma_huyen as Ma_Huyen, ten as Ten_NH " & _
                        "FROM tcs_dm_nganhang " & _
                        "order by ma"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục ngân hàng")
                'LogDebug.WriteLog("Lỗi trong quá trình Lấy danh mục địa bàn hành chính: " & ex.ToString, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_DM_KHOBAC() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục ngân hàng
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 21/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "SELECT SHKB as SHKB, ma_tinh as Ma_Tinh, ma_huyen as Ma_Huyen, ten as Ten_KB " & _
                        "FROM tcs_dm_khobac " & _
                        "order by SHKB"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục kho bạc")
                'LogDebug.WriteLog("Lỗi trong quá trình Lấy danh mục địa bàn hành chính: " & ex.ToString, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Function TCS_DM_DIEMTHU() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục điểm thu        
            ' Ngày viết: 24/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "SELECT ma_dthu as Ma_DThu, ten as Ten_DThu " & _
                        "FROM tcs_dm_diemthu " & _
                        "order by ma_dthu"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi load danh mục điểm thu")
                'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_DM_MAQUY() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục mã quỹ     
            ' Ngày viết: 20/12/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------     
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "SELECT ma, ten, tinh_trang " & _
                        "FROM tcs_dm_maquy " & _
                        "order by ma"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi load danh mục mã quỹ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

#End Region

#Region "Tiền giả"       
        Public Function BC_TinhHinhTG(ByVal dtFromDate As Date, ByVal dtToDate As Date) As DataSet
            Dim Ket_Noi As DataAccess
            Dim strSQL As String = ""
            Dim iFromDate As Integer = 0
            Dim iToDate As Integer = 0
            Dim ds As New DataSet
            Try
                iFromDate = Integer.Parse(ConvertDateToString(dtFromDate))
                iToDate = Integer.Parse(ConvertDateToString(dtToDate))
                Ket_Noi = New DataAccess
                strSQL = "SELECT TG.loai_tien  as loai_tien, LT.MENH_GIA as menh_gia,TG.SO_TO as so_to,TG.so_serial as so_serial, TG.dac_diem  as dacdiem " _
                          & " FROM tcs_tiengia_dtl TG, TCS_DM_LOAITIEN  LT" _
                          & " WHERE(TG.ma_ltien = LT.ma_loaitien)" _
                          & " And TG.ngay_bb>=" & iFromDate _
                          & " And TG.ngay_bb<=" & iToDate _
                          & " ORDER BY TG.loai_tien ASC , LT.menh_gia DESC"
                Dim dtTMP As DataTable = Ket_Noi.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
                If (dtTMP.Rows.Count > 0) Then
                    Dim typeString As System.Type = System.Type.GetType("System.String")
                    Dim typeInt As System.Type = System.Type.GetType("System.Int32")
                    '/// Khai báo 1 table mới
                    Dim dtTG As New DataTable
                    dtTG.TableName = "TCS_TIENGIA1"
                    dtTG.Columns.Add("loai_tien", typeString)
                    dtTG.Columns.Add("menh_gia", typeInt)
                    dtTG.Columns.Add("so_to", typeInt)
                    dtTG.Columns.Add("so_serial", typeString)
                    dtTG.Columns.Add("dacdiem", typeString)

                    Dim strLoaiTien_Pre As String = "0"
                    Dim strMenhGia_Pre As Integer = 0
                    Dim iSoTo_Pre As Integer = 0
                    Dim strSerial_Pre As String = " "
                    Dim strDacDiem_Pre As String = " "
                    Dim i As Integer = 0
                    Dim objTMP As Object() = New Object(4) {strLoaiTien_Pre, strMenhGia_Pre, iSoTo_Pre, strSerial_Pre, strDacDiem_Pre}
                    dtTMP.Rows.Add(objTMP)
                    While (i < dtTMP.Rows.Count)
                        Dim strLoaiTien_Cur As String = Convert.ToString(dtTMP.Rows(i)(0)).Trim()
                        Dim strMenhGia_Cur As String = Convert.ToInt32(dtTMP.Rows(i)(1))
                        Dim iSoTo_Cur As Integer = Convert.ToInt32(dtTMP.Rows(i)(2))
                        Dim strSerial_Cur As String = Convert.ToString(dtTMP.Rows(i)(3)).Trim()
                        Dim strDacDiem_Cur As String = Convert.ToString(dtTMP.Rows(i)(4)).Trim()
                        If ((Not strMenhGia_Pre = strMenhGia_Cur) Or ((strMenhGia_Pre = strMenhGia_Cur) And (Not strLoaiTien_Pre = strLoaiTien_Cur))) Then
                            If (strLoaiTien_Pre = "1") Then
                                objTMP(0) = "2-Tiền Polyme"
                            Else
                                objTMP(0) = "1-Tiền Coton"
                            End If
                            objTMP(2) = iSoTo_Pre
                            objTMP(3) = strSerial_Pre
                            objTMP(4) = strDacDiem_Pre
                            dtTG.Rows.Add(objTMP)
                            strLoaiTien_Pre = strLoaiTien_Cur
                            strMenhGia_Pre = strMenhGia_Cur
                            iSoTo_Pre = iSoTo_Cur
                            strSerial_Pre = strSerial_Cur
                            strDacDiem_Pre = strDacDiem_Cur

                            objTMP(0) = strLoaiTien_Pre
                            objTMP(1) = strMenhGia_Pre
                            objTMP(2) = iSoTo_Pre
                            objTMP(3) = strSerial_Pre
                            objTMP(4) = strDacDiem_Pre
                        ElseIf (strMenhGia_Pre = strMenhGia_Cur) Then
                            iSoTo_Pre += iSoTo_Cur
                            If (Not strSerial_Cur = "") Then
                                If (Not strSerial_Pre = "") Then
                                    strSerial_Pre &= "," & strSerial_Cur
                                Else
                                    strSerial_Pre = strSerial_Cur
                                End If
                            End If
                            If (Not strDacDiem_Cur = "") Then
                                If (Not strDacDiem_Pre = "") Then
                                    strDacDiem_Pre &= "," & strDacDiem_Cur
                                Else
                                    strDacDiem_Pre = strDacDiem_Cur
                                End If
                            End If
                        End If
                        i += 1
                    End While
                    dtTG.Rows.RemoveAt(0)

                    '/// đảo ngược Rows-Collum
                    Dim objCotton As Object() = New Object(11) {500000, 200000, 100000, 50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100}
                    Dim objPolime As Object() = New Object(5) {500000, 200000, 100000, 50000, 20000, 10000}
                    Dim objValue As Object() = New Object(4) {"", 0, 0, "", ""}
                    i = 0
                    While (i < objCotton.Length)
                        Dim j As Integer = 0
                        Dim bAvailable As Boolean = True
                        While (j < dtTG.Rows.Count)
                            Dim strLoaiTien As String = Convert.ToString(dtTG.Rows(j)(0)).Trim()
                            Dim strMenhGia As Integer = Convert.ToInt32(dtTG.Rows(j)(1))
                            If (strLoaiTien = "1-Tiền Coton") Then
                                If (strMenhGia = Convert.ToInt32(objCotton(i))) Then
                                    bAvailable = False
                                    Exit While
                                End If
                            End If
                            j += 1
                        End While
                        If (bAvailable = True) Then
                            objValue(0) = "1-Tiền Coton"
                            objValue(1) = objCotton(i)
                            dtTG.Rows.Add(objValue)
                        End If
                        i += 1
                    End While

                    i = 0
                    While (i < objPolime.Length)
                        Dim j As Integer = 0
                        Dim bAvailable As Boolean = True
                        While (j < dtTG.Rows.Count)
                            Dim strLoaiTien As String = Convert.ToString(dtTG.Rows(j)(0)).Trim()
                            Dim strMenhGia As String = Convert.ToInt32(dtTG.Rows(j)(1))
                            If (strLoaiTien = "2-Tiền Polyme") Then
                                If (strMenhGia = Convert.ToInt32(objPolime(i))) Then
                                    bAvailable = False
                                    Exit While
                                End If
                            End If
                            j += 1
                        End While
                        If (bAvailable = True) Then
                            objValue(0) = "2-Tiền Polyme"
                            objValue(1) = objPolime(i)
                            dtTG.Rows.Add(objValue)
                        End If
                        i += 1
                    End While
                    '/// sắp xếp lại bảng
                    Dim dr As DataRow()
                    dr = dtTG.Select(Nothing, "menh_gia DESC")
                    Dim dtTGResult As New DataTable
                    dtTGResult.TableName = "TCS_TIENGIA"
                    dtTGResult.Columns.Add("loai_tien", typeString)
                    dtTGResult.Columns.Add("menh_gia", typeInt)
                    dtTGResult.Columns.Add("so_to", typeInt)
                    dtTGResult.Columns.Add("so_serial", typeString)
                    dtTGResult.Columns.Add("dacdiem", typeString)
                    i = 0
                    While (i < dr.Length)
                        Dim oTMP As Object() = New Object(4) {dr(i)(0), dr(i)(1), dr(i)(2), dr(i)(3), dr(i)(4)}
                        dtTGResult.Rows.Add(oTMP)
                        i += 1
                    End While
                    ds.Tables.Add(dtTGResult)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi báo cáo tình hình tiền giả")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            End Try
            Return ds
        End Function
                
        Public Function BC_TheoDoiTG(ByVal dtFromDate As Date, ByVal dtToDate As Date) As DataSet
            Dim Ket_Noi As DataAccess
            Dim strSQL As String = ""
            Dim iFromDate As Integer = 0
            Dim iToDate As Integer = 0
            Dim ds As New DataSet
            Try
                iFromDate = Integer.Parse(ConvertDateToString(dtFromDate))
                iToDate = Integer.Parse(ConvertDateToString(dtToDate))
                Ket_Noi = New DataAccess
                strSQL = "SELECT tg.ngay_bb as ngay_bb, LT.MENH_GIA as menh_gia,TG.SO_TO as so_to,TG.so_serial as so_serial " _
                          & " FROM tcs_tiengia_dtl TG, TCS_DM_LOAITIEN  LT" _
                          & " WHERE(TG.ma_ltien = LT.ma_loaitien)" _
                          & " And TG.ngay_bb>=" & iFromDate _
                          & " And TG.ngay_bb<=" & iToDate _
                          & " ORDER BY TG.ngay_bb ASC , LT.menh_gia DESC"
                Dim dtTMP As DataTable = Ket_Noi.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
                If (dtTMP.Rows.Count > 0) Then
                    Dim typeString As System.Type = System.Type.GetType("System.String")
                    Dim typeInt As System.Type = System.Type.GetType("System.Int32")
                    '/// Khai báo 1 table mới
                    Dim dtTG As New DataTable
                    dtTG.TableName = "TCS_TIENGIA1"
                    dtTG.Columns.Add("ngay_bb", typeString)
                    dtTG.Columns.Add("menh_gia", typeString)
                    dtTG.Columns.Add("so_to", typeInt)
                    dtTG.Columns.Add("so_serial", typeString)

                    Dim strNgayBB_Pre As String = "11111111"
                    Dim strMenhGia_Pre As String = "0"
                    Dim iSoTo_Pre As Integer = 0
                    Dim strSerial_Pre As String = " "

                    Dim i As Integer = 0
                    Dim objTMP As Object() = New Object(3) {strNgayBB_Pre, strMenhGia_Pre, iSoTo_Pre, strSerial_Pre}
                    dtTMP.Rows.Add(objTMP)
                    While (i < dtTMP.Rows.Count)
                        Dim strNgayBB_Cur As String = Convert.ToString(dtTMP.Rows(i)(0)).Trim()
                        Dim strMenhGia_Cur As String = Convert.ToString(dtTMP.Rows(i)(1)).Trim()
                        Dim iSoTo_Cur As Integer = Convert.ToInt32(dtTMP.Rows(i)(2))
                        Dim strSerial_Cur As String = Convert.ToString(dtTMP.Rows(i)(3)).Trim()

                        If ((strNgayBB_Pre = strNgayBB_Cur) And (strMenhGia_Pre = strMenhGia_Cur)) Then
                            iSoTo_Pre += iSoTo_Cur
                            If (Not strSerial_Cur = "") Then
                                If (Not strSerial_Pre = "") Then
                                    strSerial_Pre &= "," & strSerial_Cur
                                Else
                                    strSerial_Pre = strSerial_Cur
                                End If
                            End If
                        Else
                            objTMP(0) = FormatDate(strNgayBB_Pre)
                            objTMP(2) = iSoTo_Pre
                            objTMP(3) = strSerial_Pre
                            '/// them vao 1 row
                            dtTG.Rows.Add(objTMP)
                            strNgayBB_Pre = strNgayBB_Cur
                            strMenhGia_Pre = strMenhGia_Cur
                            iSoTo_Pre = iSoTo_Cur
                            strSerial_Pre = strSerial_Cur

                            objTMP(0) = FormatDate(strNgayBB_Pre)
                            objTMP(1) = strMenhGia_Pre
                            objTMP(2) = iSoTo_Pre
                            objTMP(3) = strSerial_Pre
                        End If
                        i += 1
                    End While
                    dtTG.Rows.RemoveAt(0)

                    '/// tao cau truc moi cua bang
                    Dim dtTG_NEW As New DataTable
                    dtTG_NEW.TableName = "TCS_TIENGIA"
                    dtTG_NEW.Columns.Add("ngay_bb", typeString)
                    dtTG_NEW.Columns.Add("500000", typeInt) '/// chứa số tờ tiền
                    dtTG_NEW.Columns.Add("500000S", typeString) '/// chứa seri của tiền
                    dtTG_NEW.Columns.Add("200000", typeInt)
                    dtTG_NEW.Columns.Add("200000S", typeString)
                    dtTG_NEW.Columns.Add("100000", typeInt)
                    dtTG_NEW.Columns.Add("100000S", typeString)
                    dtTG_NEW.Columns.Add("50000", typeInt)
                    dtTG_NEW.Columns.Add("50000S", typeString)
                    dtTG_NEW.Columns.Add("20000", typeInt)
                    dtTG_NEW.Columns.Add("20000S", typeString)
                    dtTG_NEW.Columns.Add("10000", typeInt)
                    dtTG_NEW.Columns.Add("10000S", typeString)
                    dtTG_NEW.Columns.Add("5000", typeInt)
                    dtTG_NEW.Columns.Add("5000S", typeString)
                    dtTG_NEW.Columns.Add("2000", typeInt)
                    dtTG_NEW.Columns.Add("2000S", typeString)
                    dtTG_NEW.Columns.Add("1000", typeInt)
                    dtTG_NEW.Columns.Add("1000S", typeString)
                    dtTG_NEW.Columns.Add("500", typeInt)
                    dtTG_NEW.Columns.Add("500S", typeString)
                    dtTG_NEW.Columns.Add("200", typeInt)
                    dtTG_NEW.Columns.Add("200S", typeString)
                    dtTG_NEW.Columns.Add("100", typeInt)
                    dtTG_NEW.Columns.Add("100S", typeString)

                    strNgayBB_Pre = "11111111"
                    strMenhGia_Pre = "0"
                    iSoTo_Pre = 0
                    strSerial_Pre = " "
                    Dim obj1 As Object() = New Object(3) {strNgayBB_Pre, strMenhGia_Pre, iSoTo_Pre, strSerial_Pre}
                    Dim obj2 As Object() = New Object(24) {strNgayBB_Pre, 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, ""}
                    dtTG.Rows.Add(obj1)
                    i = 0
                    While (i < dtTG.Rows.Count)
                        Dim strNgayBB_Cur As String = Convert.ToString(dtTG.Rows(i)("ngay_bb")).Trim()
                        Dim strMenhGia_Cur As String = Convert.ToString(dtTG.Rows(i)("menh_gia")).Trim()
                        Dim iSoTo_Cur As Integer = Convert.ToInt32(dtTG.Rows(i)("so_to"))
                        Dim strSerial_Cur As String = Convert.ToString(dtTG.Rows(i)("so_serial")).Trim()
                        If (strNgayBB_Pre = strNgayBB_Cur) Then
                            'Dim strValue As String = iSoTo_Cur.ToString() & "/" & strSerial_Cur
                            Select Case (strMenhGia_Cur)
                                Case "500000"
                                    obj2(1) = iSoTo_Cur
                                    obj2(2) = strSerial_Cur
                                Case "200000"
                                    obj2(3) = iSoTo_Cur
                                    obj2(4) = strSerial_Cur
                                Case "100000"
                                    obj2(5) = iSoTo_Cur
                                    obj2(6) = strSerial_Cur
                                Case "50000"
                                    obj2(7) = iSoTo_Cur
                                    obj2(8) = strSerial_Cur
                                Case "20000"
                                    obj2(9) = iSoTo_Cur
                                    obj2(10) = strSerial_Cur
                                Case "10000"
                                    obj2(11) = iSoTo_Cur
                                    obj2(12) = strSerial_Cur
                                Case "5000"
                                    obj2(13) = iSoTo_Cur
                                    obj2(14) = strSerial_Cur
                                Case "2000"
                                    obj2(15) = iSoTo_Cur
                                    obj2(16) = strSerial_Cur
                                Case "1000"
                                    obj2(17) = iSoTo_Cur
                                    obj2(18) = strSerial_Cur
                                Case "500"
                                    obj2(19) = iSoTo_Cur
                                    obj2(20) = strSerial_Cur
                                Case "200"
                                    obj2(21) = iSoTo_Cur
                                    obj2(22) = strSerial_Cur
                                Case "100"
                                    obj2(23) = iSoTo_Cur
                                    obj2(24) = strSerial_Cur
                            End Select

                        Else
                            obj2(0) = strNgayBB_Pre
                            dtTG_NEW.Rows.Add(obj2)
                            strNgayBB_Pre = strNgayBB_Cur
                            obj2 = New Object(24) {strNgayBB_Pre, 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0, ""}
                            ' Dim strValue As String = iSoTo_Cur.ToString() & "/" & strSerial_Cur
                            Select Case (strMenhGia_Cur)
                                Case "500000"
                                    obj2(1) = iSoTo_Cur
                                    obj2(2) = strSerial_Cur
                                Case "200000"
                                    obj2(3) = iSoTo_Cur
                                    obj2(4) = strSerial_Cur
                                Case "100000"
                                    obj2(5) = iSoTo_Cur
                                    obj2(6) = strSerial_Cur
                                Case "50000"
                                    obj2(7) = iSoTo_Cur
                                    obj2(8) = strSerial_Cur
                                Case "20000"
                                    obj2(9) = iSoTo_Cur
                                    obj2(10) = strSerial_Cur
                                Case "10000"
                                    obj2(11) = iSoTo_Cur
                                    obj2(12) = strSerial_Cur
                                Case "5000"
                                    obj2(13) = iSoTo_Cur
                                    obj2(14) = strSerial_Cur
                                Case "2000"
                                    obj2(15) = iSoTo_Cur
                                    obj2(16) = strSerial_Cur
                                Case "1000"
                                    obj2(17) = iSoTo_Cur
                                    obj2(18) = strSerial_Cur
                                Case "500"
                                    obj2(19) = iSoTo_Cur
                                    obj2(20) = strSerial_Cur
                                Case "200"
                                    obj2(21) = iSoTo_Cur
                                    obj2(22) = strSerial_Cur
                                Case "100"
                                    obj2(23) = iSoTo_Cur
                                    obj2(24) = strSerial_Cur
                            End Select

                        End If
                        i += 1
                    End While
                    dtTG_NEW.Rows.RemoveAt(0)
                    '///
                    ds.Tables.Add(dtTG_NEW)

                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi báo cáo theo dõi tiền giả")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

            End Try
            Return ds
        End Function
        '-----------------------------------------------------
        ' Mục đích: Lấy dữ liệu về việc theo dõi nhập, xuất tiền giả       
        ' Ngày viết: 07/07/2008
        ' Người viết: Hoàng Văn Anh
        ' ---------------------------------------------------- 
        '/// Sổ theo dõi Xuất nhập tiền giả
        Public Function BC_XuatNhapTG(ByVal dtFromDate As Date, ByVal dtToDate As Date) As DataSet
            Dim Ket_Noi As DataAccess
            Dim strSQL As String = ""
            Dim iFromDate As Integer = 0
            Dim iToDate As Integer = 0
            Dim ds As New DataSet
            Try
                iFromDate = Integer.Parse(ConvertDateToString(dtFromDate))
                iToDate = Integer.Parse(ConvertDateToString(dtToDate))
                Ket_Noi = New DataAccess
                strSQL = "SELECT tg.ngay_bb as ngay_bb, LT.MENH_GIA as menh_gia,TG.SO_TO as so_to,TG.so_serial as so_serial,hd.so_bb AS so_bb, hd.ma_tq AS ma_tq" _
                          & " FROM tcs_tiengia_hdr hd,tcs_tiengia_dtl TG, TCS_DM_LOAITIEN  LT" _
                          & " WHERE(TG.ma_ltien = LT.ma_loaitien)" _
                          & " AND (tg.so_bb =hd.so_bb ) AND(tg.ngay_bb =hd.ngay_bb )" _
                          & " And TG.ngay_bb>=" & iFromDate _
                          & " And TG.ngay_bb<=" & iToDate _
                          & " ORDER BY TG.ngay_bb ASC , LT.menh_gia DESC"
                Dim dtTMP As DataTable = Ket_Noi.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
                If (dtTMP.Rows.Count > 0) Then
                    Dim typeString As System.Type = System.Type.GetType("System.String")
                    Dim typeInt As System.Type = System.Type.GetType("System.Int32")
                    '/// Khai báo 1 table mới
                    Dim dtTG As New DataTable
                    dtTG.TableName = "TCS_TIENGIA1"
                    dtTG.Columns.Add("ngay_bb", typeString)
                    dtTG.Columns.Add("menh_gia", typeString)
                    dtTG.Columns.Add("so_to", typeInt)
                    dtTG.Columns.Add("so_serial", typeString)
                    dtTG.Columns.Add("so_bb", typeString)
                    dtTG.Columns.Add("ma_tq", typeString)

                    Dim strNgayBB_Pre As String = "11111111"
                    Dim strMenhGia_Pre As String = "0"
                    Dim iSoTo_Pre As Integer = 0
                    Dim strSerial_Pre As String = " "
                    Dim strSoBB_pre As String = "0"
                    Dim strMaTQ_pre As String = "0"
                    Dim i As Integer = 0
                    Dim objTMP As Object() = New Object(5) {strNgayBB_Pre, strMenhGia_Pre, iSoTo_Pre, strSerial_Pre, strSoBB_pre, strMaTQ_pre}
                    dtTMP.Rows.Add(objTMP)
                    While (i < dtTMP.Rows.Count)
                        Dim strNgayBB_Cur As String = Convert.ToString(dtTMP.Rows(i)(0)).Trim()
                        Dim strMenhGia_Cur As String = Convert.ToString(dtTMP.Rows(i)(1)).Trim()
                        Dim iSoTo_Cur As Integer = Convert.ToInt32(dtTMP.Rows(i)(2))
                        Dim strSerial_Cur As String = Convert.ToString(dtTMP.Rows(i)(3)).Trim()
                        Dim strSoBB_Cur As String = Convert.ToString(dtTMP.Rows(i)(4)).Trim()
                        Dim strMaTQ_Cur As String = Convert.ToString(dtTMP.Rows(i)(5)).Trim()

                        If ((strNgayBB_Pre = strNgayBB_Cur) And (strSoBB_pre = strSoBB_Cur) And (strMaTQ_pre = strMaTQ_Cur) And (strMenhGia_Pre = strMenhGia_Cur)) Then
                            iSoTo_Pre += iSoTo_Cur
                            If (Not strSerial_Cur = "") Then
                                If (Not strSerial_Pre = "") Then
                                    strSerial_Pre &= "," & strSerial_Cur
                                Else
                                    strSerial_Pre = strSerial_Cur
                                End If
                            End If
                        Else
                            objTMP(0) = FormatDate(strNgayBB_Pre)
                            objTMP(2) = iSoTo_Pre
                            objTMP(3) = strSerial_Pre
                            objTMP(4) = strSoBB_pre
                            objTMP(5) = strMaTQ_pre

                            '/// them vao 1 row
                            dtTG.Rows.Add(objTMP)
                            strNgayBB_Pre = strNgayBB_Cur
                            strMenhGia_Pre = strMenhGia_Cur
                            iSoTo_Pre = iSoTo_Cur
                            strSerial_Pre = strSerial_Cur
                            strSoBB_pre = strSoBB_Cur
                            strMaTQ_pre = strMaTQ_Cur

                            objTMP(0) = FormatDate(strNgayBB_Pre)
                            objTMP(1) = strMenhGia_Pre
                            objTMP(2) = iSoTo_Pre
                            objTMP(3) = strSerial_Pre
                            objTMP(4) = strSoBB_pre
                            objTMP(5) = strMaTQ_pre

                        End If
                        i += 1
                    End While
                    dtTG.Rows.RemoveAt(0)

                    '/// tao cau truc moi cua bang
                    Dim dtTG_NEW As New DataTable
                    dtTG_NEW.TableName = "TCS_TIENGIA"
                    dtTG_NEW.Columns.Add("ngay_bb", typeString)
                    dtTG_NEW.Columns.Add("500000", typeInt) '/// chứa số tờ tiền
                    dtTG_NEW.Columns.Add("200000", typeInt)
                    dtTG_NEW.Columns.Add("100000", typeInt)
                    dtTG_NEW.Columns.Add("50000", typeInt)
                    dtTG_NEW.Columns.Add("20000", typeInt)
                    dtTG_NEW.Columns.Add("10000", typeInt)
                    dtTG_NEW.Columns.Add("5000", typeInt)
                    dtTG_NEW.Columns.Add("2000", typeInt)
                    dtTG_NEW.Columns.Add("1000", typeInt)
                    dtTG_NEW.Columns.Add("500", typeInt)
                    dtTG_NEW.Columns.Add("200", typeInt)
                    dtTG_NEW.Columns.Add("100", typeInt)
                    dtTG_NEW.Columns.Add("so_bb", typeString)
                    dtTG_NEW.Columns.Add("ma_tq", typeString)

                    strNgayBB_Pre = "11111111"
                    strMenhGia_Pre = "0"
                    iSoTo_Pre = 0
                    strSerial_Pre = " "
                    strSoBB_pre = "0"
                    strMaTQ_pre = "0"

                    Dim obj1 As Object() = New Object(3) {strNgayBB_Pre, strMenhGia_Pre, iSoTo_Pre, strSerial_Pre}
                    Dim obj2 As Object() = New Object(14) {strNgayBB_Pre, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", ""}
                    dtTG.Rows.Add(obj1)
                    i = 0
                    While (i < dtTG.Rows.Count)
                        Dim strNgayBB_Cur As String = Convert.ToString(dtTG.Rows(i)("ngay_bb")).Trim()
                        Dim strMenhGia_Cur As String = Convert.ToString(dtTG.Rows(i)("menh_gia")).Trim()
                        Dim iSoTo_Cur As Integer = Convert.ToInt32(dtTG.Rows(i)("so_to"))
                        Dim strSerial_Cur As String = Convert.ToString(dtTG.Rows(i)("so_serial")).Trim()
                        Dim strSoBB_Cur As String = Convert.ToString(dtTG.Rows(i)("so_bb")).Trim()
                        Dim strMaTQ_Cur As String = Convert.ToString(dtTG.Rows(i)("ma_tq")).Trim()

                        If (strNgayBB_Pre = strNgayBB_Cur) And (strSoBB_pre = strSoBB_Cur) And (strMaTQ_pre = strMaTQ_Cur) Then
                            Select Case (strMenhGia_Cur)
                                Case "500000"
                                    obj2(1) = iSoTo_Cur
                                Case "200000"
                                    obj2(2) = iSoTo_Cur
                                Case "100000"
                                    obj2(3) = iSoTo_Cur
                                Case "50000"
                                    obj2(4) = iSoTo_Cur
                                Case "20000"
                                    obj2(5) = iSoTo_Cur
                                Case "10000"
                                    obj2(6) = iSoTo_Cur
                                Case "5000"
                                    obj2(7) = iSoTo_Cur
                                Case "2000"
                                    obj2(8) = iSoTo_Cur
                                Case "1000"
                                    obj2(9) = iSoTo_Cur
                                Case "500"
                                    obj2(10) = iSoTo_Cur
                                Case "200"
                                    obj2(11) = iSoTo_Cur
                                Case "100"
                                    obj2(12) = iSoTo_Cur
                            End Select
                        Else
                            obj2(0) = strNgayBB_Pre
                            obj2(13) = strSoBB_pre
                            obj2(14) = strMaTQ_pre
                            dtTG_NEW.Rows.Add(obj2)
                            strNgayBB_Pre = strNgayBB_Cur
                            strSoBB_pre = strSoBB_Cur
                            strMaTQ_pre = strMaTQ_Cur

                            obj2 = New Object(14) {strNgayBB_Pre, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, strSoBB_pre, strMaTQ_pre}
                            ' Dim strValue As String = iSoTo_Cur.ToString() & "/" & strSerial_Cur
                            Select Case (strMenhGia_Cur)
                                Case "500000"
                                    obj2(1) = iSoTo_Cur
                                Case "200000"
                                    obj2(2) = iSoTo_Cur
                                Case "100000"
                                    obj2(3) = iSoTo_Cur
                                Case "50000"
                                    obj2(4) = iSoTo_Cur
                                Case "20000"
                                    obj2(5) = iSoTo_Cur
                                Case "10000"
                                    obj2(6) = iSoTo_Cur
                                Case "5000"
                                    obj2(7) = iSoTo_Cur
                                Case "2000"
                                    obj2(8) = iSoTo_Cur
                                Case "1000"
                                    obj2(9) = iSoTo_Cur
                                Case "500"
                                    obj2(10) = iSoTo_Cur
                                Case "200"
                                    obj2(11) = iSoTo_Cur
                                Case "100"
                                    obj2(12) = iSoTo_Cur
                            End Select
                        End If
                        i += 1
                    End While
                    dtTG_NEW.Rows.RemoveAt(0)
                    '///
                    ds.Tables.Add(dtTG_NEW)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi theo dõi nhập, xuất tiền giả")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            End Try
            Return ds
        End Function
        Public Function ConvertDateToString(ByVal dt As Date) As String
            Return dt.ToString("yyyyMMdd")
        End Function
        Public Function FormatDate(ByVal strTMP As String) As String
            Dim strResult As String = ""
            strResult = strTMP.Substring(6, 2) + "/" + strTMP.Substring(4, 2) + "/" + strTMP.Substring(0, 4)
            Return strResult
        End Function
#End Region

#Region "Bảng kê tiền mặt"
        '-----------------------------------------------------
        ' Mục đích: Lấy dữ liệu về Sổ thu các loại tiền      
        ' Ngày viết: 17/07/2008
        ' Người viết: Hoàng Văn Anh
        ' ---------------------------------------------------- 
        '/// Sổ thu các loại tiền
        Public Function SoThuCLTien(ByVal strWhere As String) As DataSet
            Dim Ket_Noi As DataAccess
            Dim strSQL As String = ""
            Dim ds As New DataSet
            '/// loai du lieu
            Dim typeString As System.Type = System.Type.GetType("System.String")
            Dim typeInt As System.Type = System.Type.GetType("System.Int32")
            Dim typeDec As System.Type = System.Type.GetType("System.Decimal")
            Try

                strSQL = "SELECT (kq.shkb || kq.ngay_kb || kq.ma_nv || kq.so_bt || kq.ma_dthu) AS ID, " _
                        & "ct.so_ct AS soct, " _
                        & "(nvl(ct.ten_nntien,ct.ten_nnthue) || '-' || nvl(ct.dc_nntien,ct.dc_nnthue)) AS noidung," _
                        & "lt.menh_gia, kq.so_to " _
                        & " from tcs_ctu_hdr ct, tcs_khoquy_dtl kq , tcs_dm_loaitien lt " _
                        & " where (ct.so_ct =kq.so_ct) and (ct.kyhieu_ct =kq.kyhieu_ct) " _
                        & " and (kq.ma_loaitien =lt.ma_loaitien) and (kq.so_to <> 0) " _
                        & strWhere & _
                        " and (ct.trang_thai <> '04') " & _
                        " ORDER BY (kq.shkb || kq.ngay_kb || kq.ma_nv || kq.so_bt || kq.ma_dthu), ct.so_ct, lt.menh_gia DESC "
                '// Khoi tao ket noi
                Ket_Noi = New DataAccess
                '/// lấy ra các giá trị mà có số tờ <>0
                Dim dt1 As DataTable = Ket_Noi.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
                strSQL = "SELECT (kq.shkb || kq.ngay_kb || kq.ma_nv || kq.so_bt || kq.ma_dthu) AS ID, ct.so_ct AS soct, " _
                        & "(nvl(ct.ten_nntien,ct.ten_nnthue) || '-' || nvl(ct.dc_nntien,ct.dc_nnthue)) AS noidung," _
                        & "lt.menh_gia, kq.so_to " _
                        & " from tcs_ctu_hdr ct, tcs_khoquy_dtl kq , tcs_dm_loaitien lt " _
                        & " where (ct.so_ct =kq.so_ct) and (ct.kyhieu_ct =kq.kyhieu_ct) " _
                        & " and (kq.ma_loaitien =lt.ma_loaitien) and (kq.so_to = 0) " _
                        & strWhere & _
                        " and (ct.trang_thai <> '04') " & _
                        " ORDER BY (kq.shkb || kq.ngay_kb || kq.ma_nv || kq.so_bt || kq.ma_dthu), " & _
                        " ct.so_ct, lt.menh_gia DESC "
                '/// lấy ra các giá trị mà có số tờ = 0
                Dim dt2 As DataTable = Ket_Noi.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
                '/// ghép 2 bảng dt1 và dt2 = dtTMP
                Dim dtTMP As New DataTable
                dtTMP.Columns.Add("SoCT", typeString)
                dtTMP.Columns.Add("NoiDung", typeString)
                dtTMP.Columns.Add("MenhGia", typeString)
                dtTMP.Columns.Add("SoTo", typeInt)
                If (dt1.Rows.Count > 0) Then
                    Dim u As Integer = 0
                    While (u < dt1.Rows.Count)
                        Dim strID_1 As String = Convert.ToString(dt1.Rows(u)(0)).Trim()
                        Dim strSO_CT_1 As String = Convert.ToString(dt1.Rows(u)(1)).Trim()
                        Dim strNoiDung_1 As String = Convert.ToString(dt1.Rows(u)(2)).Trim()
                        Dim strMenh_Gia_1 As String = Convert.ToString(dt1.Rows(u)(3)).Trim()
                        Dim iSoTo_1 As Integer = Convert.ToInt32(dt1.Rows(u)(4))

                        If (dt2.Rows.Count > 0) Then
                            Dim v As Integer = 0
                            While (v < dt2.Rows.Count)
                                Dim strID_2 As String = Convert.ToString(dt2.Rows(v)(0)).Trim()
                                Dim strSO_CT_2 As String = Convert.ToString(dt2.Rows(v)(1)).Trim()
                                Dim strNoiDung_2 As String = Convert.ToString(dt2.Rows(v)(2)).Trim()
                                Dim strMenh_Gia_2 As String = Convert.ToString(dt2.Rows(v)(3)).Trim()
                                If (strID_1 = strID_2) And (strMenh_Gia_1 = strMenh_Gia_2) Then
                                    'strSO_CT_1 = strSO_CT_1 & ", " & strSO_CT_2
                                    'strNoiDung_1 = strNoiDung_1 & ", " & strNoiDung_2                                
                                    Exit While
                                End If
                                v += 1
                            End While
                        End If
                        '/// thêm vào bảng mới
                        Dim objTmp As Object() = New Object(3) {strSO_CT_1, strNoiDung_1, strMenh_Gia_1, iSoTo_1}
                        dtTMP.Rows.Add(objTmp)
                        u += 1
                    End While
                End If
                '/// Chuyển row thành collum

                'Dim dtTMP As New DataTable
                'dtTMP = dt1

                If (dtTMP.Rows.Count > 0) Then
                    '/// tao cau truc moi cua bang
                    Dim dtSoCLTien As New DataTable
                    dtSoCLTien.TableName = "STTM"

                    dtSoCLTien.Columns.Add("SoCT", typeString) '// số chứng từ
                    dtSoCLTien.Columns.Add("TongTien", typeDec) '// tổng tiền
                    dtSoCLTien.Columns.Add("NoiDung", typeString) '// nội dung
                    dtSoCLTien.Columns.Add("500000", typeInt) '/// chứa số tờ tiền
                    dtSoCLTien.Columns.Add("200000", typeInt)
                    dtSoCLTien.Columns.Add("100000", typeInt)
                    dtSoCLTien.Columns.Add("50000", typeInt)
                    dtSoCLTien.Columns.Add("20000", typeInt)
                    dtSoCLTien.Columns.Add("10000", typeInt)
                    dtSoCLTien.Columns.Add("5000", typeInt)
                    dtSoCLTien.Columns.Add("2000", typeInt)
                    dtSoCLTien.Columns.Add("1000", typeInt)
                    dtSoCLTien.Columns.Add("500", typeInt)
                    dtSoCLTien.Columns.Add("200", typeInt)
                    dtSoCLTien.Columns.Add("100", typeInt)

                    Dim strSO_CT_Pre As String = "0"
                    Dim dTongTien_Pre As Decimal = 0
                    Dim iSoTo_pre As Integer = 0
                    Dim strMenh_Gia_pre As String = "0"
                    Dim strNoiDung_pre As String = ""

                    Dim obj1 As Object() = New Object(3) {strSO_CT_Pre, strNoiDung_pre, 0, 0}
                    Dim obj2 As Object() = New Object(14) {strSO_CT_Pre, dTongTien_Pre, strNoiDung_pre, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                    dtTMP.Rows.Add(obj1)
                    Dim i As Integer = 0
                    While (i < dtTMP.Rows.Count)
                        Dim strSO_CT_Cur As String = Convert.ToString(dtTMP.Rows(i)(0)).Trim()
                        Dim strNoiDung_Cur As String = Convert.ToString(dtTMP.Rows(i)(1)).Trim()
                        Dim strMenh_Gia_Cur As String = Convert.ToString(dtTMP.Rows(i)(2)).Trim()
                        Dim iSoTo_Cur As Integer = Convert.ToInt32(dtTMP.Rows(i)(3))
                        If (strSO_CT_Pre = strSO_CT_Cur) Then
                            Select Case (strMenh_Gia_Cur)
                                Case "500000"
                                    obj2(3) = iSoTo_Cur
                                Case "200000"
                                    obj2(4) = iSoTo_Cur
                                Case "100000"
                                    obj2(5) = iSoTo_Cur
                                Case "50000"
                                    obj2(6) = iSoTo_Cur
                                Case "20000"
                                    obj2(7) = iSoTo_Cur
                                Case "10000"
                                    obj2(8) = iSoTo_Cur
                                Case "5000"
                                    obj2(9) = iSoTo_Cur
                                Case "2000"
                                    obj2(10) = iSoTo_Cur
                                Case "1000"
                                    obj2(11) = iSoTo_Cur
                                Case "500"
                                    obj2(12) = iSoTo_Cur
                                Case "200"
                                    obj2(13) = iSoTo_Cur
                                Case "100"
                                    obj2(14) = iSoTo_Cur
                            End Select
                        Else
                            obj2(0) = strSO_CT_Pre
                            obj2(1) = dTongTien_Pre
                            obj2(2) = strNoiDung_pre
                            dtSoCLTien.Rows.Add(obj2)

                            strSO_CT_Pre = strSO_CT_Cur
                            strNoiDung_pre = strNoiDung_Cur

                            obj2 = New Object(14) {strSO_CT_Pre, dTongTien_Pre, strNoiDung_pre, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                            ' Dim strValue As String = iSoTo_Cur.ToString() & "/" & strSerial_Cur
                            Select Case (strMenh_Gia_Cur)
                                Case "500000"
                                    obj2(3) = iSoTo_Cur
                                Case "200000"
                                    obj2(4) = iSoTo_Cur
                                Case "100000"
                                    obj2(5) = iSoTo_Cur
                                Case "50000"
                                    obj2(6) = iSoTo_Cur
                                Case "20000"
                                    obj2(7) = iSoTo_Cur
                                Case "10000"
                                    obj2(8) = iSoTo_Cur
                                Case "5000"
                                    obj2(9) = iSoTo_Cur
                                Case "2000"
                                    obj2(10) = iSoTo_Cur
                                Case "1000"
                                    obj2(11) = iSoTo_Cur
                                Case "500"
                                    obj2(12) = iSoTo_Cur
                                Case "200"
                                    obj2(13) = iSoTo_Cur
                                Case "100"
                                    obj2(14) = iSoTo_Cur
                            End Select
                        End If
                        i += 1
                    End While
                    dtSoCLTien.Rows.RemoveAt(0)
                    ds.Tables.Add(dtSoCLTien)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin sổ thu các loại tiền")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            End Try
            Return ds
        End Function

        Public Function BKE_TIENMAT(ByVal strWhere As String) As DataSet
            ' Mục đích: Lấy dữ liệu bảng kê số tiền thực thu trongngày theo mệnh giá
            ' người viết: Lê Hồng Hà
            ' Ngày viết: 17/07/2008
            '---------------------------
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim sql As String
                sql = "SELECT lt.menh_gia as MenhGia, SUM (kq.so_to) AS so_to,(lt.menh_gia * SUM (kq.so_to)) AS tien " & _
                      "FROM tcs_dm_loaitien lt, tcs_khoquy_dtl kq, tcs_ctu_hdr ct " & _
                      "WHERE (kq.ma_loaitien = lt.ma_loaitien) " & _
                      "And (kq.shkb=ct.shkb) And (kq.ngay_kb=ct.ngay_kb) And (kq.ma_nv=ct.ma_nv) " & _
                      "And (kq.ma_dthu=ct.ma_dthu) And (kq.so_bt=ct.so_bt) " & strWhere & _
                      " and (ct.trang_thai <> '04') " & _
                      " Group by menh_gia"
                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi truy vấn bảng kê số tiền thực thu trong ngày theo mệnh giá")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        'Kienvt : ???Thay ham cu
        'Public Function BKE_TIENMAT(ByVal strWhere As String) As DataSet
        '    ' Mục đích: Lấy dữ liệu bảng kê số tiền thực thu trongngày theo mệnh giá
        '    ' người viết: Lê Hồng Hà
        '    ' Ngày viết: 17/07/2008

        '    ' Người sửa: Hoàng Văn Anh
        '    ' Mục đích: Sửa theo COA bên Ngân Hàng
        '    '---------------------------
        '    Dim conn As DataAccess
        '    Try
        '        conn = New DataAccess

        '        Dim sql As String
        '        sql = "SELECT lt.menh_gia as MenhGia, SUM (kq.so_to) AS so_to,(lt.menh_gia * SUM (kq.so_to)) AS tien " & _
        '              " FROM tcs_dm_loaitien lt, tcs_khoquy_dtl kq, tcs_ctu_hdr ct " & _
        '              " WHERE (kq.ma_loaitien = lt.ma_loaitien) " & _
        '              " And (kq.shkb=ct.shkb) And (kq.ngay_kb=ct.ngay_kb) And (kq.ma_nv=ct.ma_nv) " & _
        '              " And (kq.ma_dthu=ct.ma_dthu) And (kq.so_bt=ct.so_bt) " & strWhere & _
        '              " and (ct.trang_thai <> '04') AND ct.MA_NV=" & gstrTenND & _
        '              " Group by menh_gia"
        '        Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try
        'End Function

        Public Function GetTongTienNop(ByVal strWhere As String) As Double
            ' Mục đích: Lấy dữ liệu bảng kê số tiền thực thu trongngày theo mệnh giá
            ' người viết: Lê Hồng Hà
            ' Ngày viết: 17/07/2008
            '---------------------------
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim sql As String

                sql = "SELECT SUM (ct.ttien) AS tongtien " & _
                        "  FROM tcs_ctu_hdr ct " & _
                        " WHERE ((ct.kyhieu_ct || ct.so_ct) IN (SELECT (kq.kyhieu_ct || kq.so_ct) " & _
                        "FROM tcs_khoquy_dtl kq)) " & strWhere & _
                        " and (ct.trang_thai <> '04') "

                Dim dr As IDataReader = conn.ExecuteDataReader(sql, CommandType.Text)
                Dim dblTongTien As Double

                If (dr.Read()) Then
                    dblTongTien = CDbl(dr(0))
                End If

                Return dblTongTien
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi truy vấn bảng kê số tiền thực thu trong ngày theo mệnh giá")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        'Public Function SOQUY_TM_CHITIET(ByVal strWhere As String) As DataSet
        '    ' Mục đích: Lấy dữ liệu bảng kê số tiền thực thu trongngày theo mệnh giá
        '    ' người viết: Lê Hồng Hà
        '    ' Ngày viết: 17/07/2008
        '    '---------------------------
        '    Dim conn As DataAccess
        '    Try
        '        conn = New DataAccess

        '        Dim sql As String
        '        sql = "SELECT DISTINCT LPAD (ct.ma_tq, 3, '0') AS ma_tq, ct.so_ct AS so_ct, " & _
        '                    " (nvl(ct.ten_nntien,ct.ten_nnthue) || ' - ' || nvl(ct.dc_nntien,ct.dc_nnthue)) AS noi_dung, " & _
        '                    " ct.tt_tthu AS thuc_thu, 0 AS thua, " & _
        '                    " ct.ma_nv, ct.so_bt " & _
        '                    " FROM tcs_ctu_hdr ct, tcs_khoquy_dtl kq " & _
        '                    " WHERE (ct.trang_thai <> '04') " & _
        '                    " AND (ct.kyhieu_ct = kq.kyhieu_ct) " & _
        '                    " AND ct.Ma_NV=" & gstrTenND & _
        '                    " AND (ct.shkb || ct.ngay_kb || ct.ma_nv || ct.so_bt || ct.ma_dthu) =" & _
        '                    "(kq.shkb || kq.ngay_kb || kq.ma_nv || kq.so_bt || kq.ma_dthu)" & _
        '                    strWhere & _
        '                    " order by ct.ma_nv, ct.so_bt"

        '        Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try
        'End Function

        Public Function SOQUY_TM_CHITIET(ByVal strWhere As String) As DataSet
            ' Mục đích: Lấy dữ liệu bảng kê số tiền thực thu trongngày theo mệnh giá
            ' người viết: Lê Hồng Hà
            ' Ngày viết: 17/07/2008
            '---------------------------
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim sql As String


                gblnNhapPLT = True
                If (gblnNhapPLT) Then
                    sql = "SELECT DISTINCT LPAD (ct.ma_tq, 3, '0') AS ma_tq, ct.so_ct AS so_ct, " & _
                        "(nvl(ct.ten_nntien,ct.ten_nnthue) || ' - ' || nvl(ct.dc_nntien,ct.dc_nnthue)) AS noi_dung, " & _
                        "ct.tt_tthu AS thuc_thu, 0 AS thua, " & _
                        "ct.ma_nv, ct.so_bt " & _
                        "FROM tcs_ctu_hdr ct, tcs_khoquy_dtl kq " & _
                        "WHERE (ct.trang_thai <> '04') " & _
                        "AND (ct.kyhieu_ct = kq.kyhieu_ct) " & _
                        "AND (ct.so_ct = kq.so_ct) " & strWhere & _
                        " order by ct.ma_nv, ct.so_bt"
                Else
                    sql = "SELECT DISTINCT LPAD (ct.ma_tq, 3, '0') AS ma_tq, ct.so_ct AS so_ct, " & _
                        "(nvl(ct.ten_nntien,ct.ten_nnthue) || ' - ' || nvl(ct.dc_nntien,ct.dc_nnthue)) AS noi_dung, " & _
                        "ct.tt_tthu AS thuc_thu, 0 AS thua, " & _
                        "ct.ma_nv, ct.so_bt " & _
                        "FROM tcs_ctu_hdr ct " & _
                        "WHERE (ct.trang_thai <> '04') and (ct.tt_tthu <> 0) and (ct.ma_tq <> 0) " & strWhere & _
                        " order by ct.ma_nv, ct.so_bt"
                End If

                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi truy vấn bảng kê số tiền thực thu trong ngày theo mệnh giá")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        Public Function SOQUY_TM_TONGHOP(ByVal strWhere As String) As DataSet
            Dim conn As DataAccess
            Try
                conn = New DataAccess

                Dim sql As String
                sql = "SELECT  LPAD (ct.ma_tq, 3, '0') AS ma_tq, ct.so_ct AS so_ct, " & _
                            "(nvl(ct.ten_nntien,ct.ten_nnthue) || ' - ' || nvl(ct.dc_nntien,ct.dc_nnthue)) AS noi_dung, " & _
                            " SUM (kq.so_to*lt.menh_gia) AS thuc_thu, (ct.tt_tthu - ct.ttien) AS thua, " & _
                            " ct.ma_nv, ct.so_bt " & _
                            " FROM tcs_ctu_hdr ct, tcs_khoquy_dtl kq,tcs_dm_loaitien lt " & _
                            " WHERE (ct.trang_thai <> '04') " & _
                            " AND (ct.kyhieu_ct = kq.kyhieu_ct)" & _
                            " AND (kq.ma_loaitien=lt.ma_loaitien) " & _
                            " AND (ct.shkb || ct.ngay_kb || ct.ma_nv || ct.so_bt || ct.ma_dthu) = " & _
                            " (kq.shkb || kq.ngay_kb || kq.ma_nv || kq.so_bt || kq.ma_dthu) " & _
                            strWhere & _
                            "GROUP BY ct.ma_tq,ct.so_ct, " & _
                            " (nvl(ct.ten_nntien,ct.ten_nnthue) || ' - ' || nvl(ct.dc_nntien,ct.dc_nnthue)), " & _
                            " ct.ma_nv, ct.so_bt, " & _
                            " (ct.tt_tthu - ct.ttien) " & _
                            " ORDER BY ct.ma_nv, ct.so_bt "

                Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

#End Region

#Region "Kết xuất Cơ quan thu"
        Public Function TCS_CTU_CQTHU(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy dữ liệu kết xuất chứng từ để báo cáo
            ' Tham số: dãy KHCT ghép với SoCT
            ' Giá trị trả về: DataSet dữ liệu
            ' Ngày viết: 26/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------------------
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select hdr.kyhieu_ct as kyhieu_ct, hdr.so_ct as so_ct, " & _
                        "hdr.ngay_kb as ngay_ct, hdr.so_tk as so_tk,  " & _
                        "to_char(hdr.ngay_tk, 'dd/MM/yyyy') as ngay_tk, hdr.lh_xnk as lhxnk, " & _
                        "hdr.ma_nnthue as ma_nnthue, hdr.ten_nnthue as ten_nnthue, " & _
                        "(dtl.ma_cap||dtl.ma_chuong) as CC, (dtl.ma_loai||dtl.ma_khoan) as LK, " & _
                        "(dtl.ma_muc||dtl.ma_tmuc) as MTM, dtl.ky_thue as ky_thue, " & _
                        "(hdr.ma_tinh||'.'||hdr.ma_huyen||'.'||hdr.ma_xa) as DBHC, " & _
                        "hdr.tk_no as tk_no, hdr.tk_co as tk_co, " & _
                        "dtl.sotien as so_tien,  " & _
                        "decode (substr(hdr.tk_co, 1, 3), " & _
                        "        '741', '741', " & _
                        "        '920', '920', " & _
                        "        '921', '921', " & _
                        "        '999') as Dau_TK_Co " & _
                        "from tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " & _
                        "where (hdr.shkb = dtl.shkb(+)) and (hdr.ngay_kb = dtl.ngay_kb(+)) and " & _
                        "(hdr.ma_nv = dtl.ma_nv(+)) and (hdr.so_bt = dtl.so_bt(+)) and " & _
                        "(hdr.ma_dthu = dtl.ma_dthu(+)) " & _
                        strWhere & _
                        "order by dau_tk_co, hdr.ngay_kb, hdr.ma_nv, hdr.so_bt "
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu kết xuất chứng từ để báo cáo")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo: " & ex.ToString, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BKE_CTU_CHITIET(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy dữ liệu kết xuất chứng từ để báo cáo
            ' Tham số: dãy KHCT ghép với SoCT
            ' Giá trị trả về: DataSet dữ liệu
            ' Ngày viết: 09/05/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------------------
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select hdr.ma_nnthue as MaSoThue, hdr.ten_nnthue as Ten_NNT, " & _
                        "hdr.so_ct as So_CT, hdr.kyhieu_ct as kh_ct, hdr.ngay_kb as Ngay_CT, dtl.ky_thue as Ky_Thue, " & _
                        "(dtl.ma_cap||'.'||dtl.ma_chuong||'.'||dtl.ma_loai||'.'||dtl.ma_khoan||'.'||dtl.ma_muc||'.'||dtl.ma_tmuc) as MLNS, " & _
                        "dtl.sotien as So_Tien , hdr.so_tk as So_TK, to_char(hdr.ngay_tk,'dd/MM/rrrr') as Ngay_TK, " & _
                        "(hdr.ma_tinh||'.'||hdr.ma_huyen||'.'||hdr.ma_xa) as DBHC,  " & _
                        "(hdr.kyhieu_ct||hdr.so_ct) as MaNV_SoBT  " & _
                        "from KX_CQT_HDR hdr, KX_CQT_DTL dtl " & _
                        "where (hdr.shkb =dtl.shkb (+)) and (hdr.ngay_kb =dtl.ngay_kb (+)) and (hdr.ma_nv =dtl.ma_nv (+)) " & _
                        "and (hdr.so_bt =dtl.so_bt (+)) and (hdr.ma_dthu =dtl.ma_dthu (+)) " & _
                        strWhere & _
                        "order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt asc"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu kết xuất chứng từ để báo cáo")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BKE_CTU_CQTHU(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy dữ liệu kết xuất chứng từ để báo cáo
            ' Tham số: dãy KHCT ghép với SoCT
            ' Giá trị trả về: DataSet dữ liệu
            ' Ngày viết: 26/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------------------
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select hdr.so_ct as SO_CTU, to_char(hdr.ngay_kb) as NGAY_KB, hdr.ma_nnthue as MA_NNT, " & _
                        "hdr.ten_nnthue as Ten_NNT, (hdr.ma_tinh||'.'||hdr.ma_huyen||'.'||hdr.ma_xa) as Ma_DBHC, " & _
                        "hdr.so_tk as SO_TK, hdr.ngay_tk as NGAY_TK, hdr.so_ct_nh as GHI_CHU, " & _
                        "(lpad(to_char(hdr.ma_nv), 3, '0')||'/'||lpad(to_char(hdr.so_bt), 3, '0')) as SO_BT, hdr.tk_no as TK_NO, hdr.tk_co as TK_CO, " & _
                        "dtl.ma_cap as Cap,dtl.ma_chuong as Chuong, dtl.ma_loai as Loai, " & _
                        "dtl.ma_khoan as Khoan, dtl.ma_muc as Muc, dtl.ma_tmuc as TMUC, " & _
                        "dtl.sotien as So_Tien, dtl.ky_thue as Ky_Thue " & _
                        "from tcs_ctu_thop_hdr hdr, tcs_ctu_thop_dtl dtl " & _
                        "where (hdr.shkb=dtl.shkb) and (hdr.ngay_kb=dtl.ngay_kb) and (hdr.ma_nv=dtl.ma_nv) " & _
                        "and (hdr.so_bt=dtl.so_bt) and (hdr.ma_dthu=dtl.ma_dthu) " & _
                        strWhere & _
                        "order by hdr.so_ct"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu kết xuất chứng từ để báo cáo")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
#End Region

#Region "Chứng từ phục hồi"
        Public Function CTU_PhucHoi_Chitiet(ByVal strKHSoCT As String) As DataSet
            Dim cnCT As New DataAccess
            Dim strSQL As String
            Try
                strSQL = "SELECT distinct dtl.noi_dung AS noi_dung, (dtl.ma_cap||'.'||dtl.ma_chuong ) AS chuong, dtl.ma_loai AS loai, " & _
                        "dtl.ma_khoan AS khoan, dtl.ma_muc AS muc, dtl.ma_tmuc AS tmuc, " & _
                        "dtl.ky_thue AS kythue, dtl.sotien AS sotien, dtl.ma_tldt AS madieutiet, " & _
                        "(hdr.kyhieu_ct || hdr.so_ct) AS khsoct, hdr.ma_nnthue AS ma_nnthue, " & _
                        "hdr.ten_nnthue AS ten_nnthue, hdr.dc_nnthue AS dc_nnthue, " & _
                        "hdr.ma_nntien AS ma_nntien, hdr.ten_nntien AS ten_nntien, " & _
                        "hdr.dc_nntien AS dc_nntien, hdr.tk_no AS tk_no, hdr.tk_co AS tk_co, " & _
                        "nha.ten AS nh_a, nhb.ten AS nh_b, hdr.ma_cqthu AS ma_cqthu, " & _
                        "cqthu.ten AS ten_cqthu, hdr.so_tk AS so_tk, hdr.shkb as SHKB, " & _
                        "TO_CHAR (hdr.ngay_tk, 'dd/MM/yyyy') AS ngay_tk,  " & _
                        "'' AS tinh, " & _
                        "'' as huyen,  " & _
                        "'' AS xa, " & _
                        "'' AS tinh2, " & _
                        "'' AS huyen2, " & _
                        "'' AS xa2, " & _
                        "x.ten AS xa2, to_char(hdr.ttien) AS tien, " & _
                        "(x.ma_tinh||'.'||x.ma_huyen||'.'||x.ma_xa) as Ma_DBHC, " & _
                        "kt.ten as ketoan, ks.ten as kiemsoat " & _
                        "FROM tcs_ctu_thop_hdr hdr, " & _
                        "tcs_ctu_thop_dtl dtl, " & _
                        "tcs_dm_nganhang nha, " & _
                        "tcs_dm_nganhang nhb, " & _
                        "tcs_dm_cqthu cqthu, " & _
                        "tcs_dm_tinh t, " & _
                        "tcs_dm_huyen h, " & _
                        "tcs_dm_xa x, " & _
                        "tcs_dm_nhanvien kt, " & _
                        "tcs_dm_nhanvien ks " & _
                        " WHERE (hdr.shkb = dtl.shkb) " & _
                        "   AND (hdr.ngay_kb = dtl.ngay_kb) " & _
                        "   AND (hdr.ma_nv = dtl.ma_nv) " & _
                        "   AND (hdr.so_bt = dtl.so_bt) " & _
                        "   AND (hdr.ma_dthu = hdr.ma_dthu) " & _
                        "   AND (hdr.ma_nh_a = nha.ma(+)) " & _
                        "   AND (hdr.ma_nh_b = nhb.ma(+)) " & _
                        "   AND (hdr.ma_cqthu = cqthu.ma_cqthu) " & _
                        "   AND (hdr.ma_tinh = t.ma_tinh) " & _
                        "   AND (hdr.ma_tinh = h.ma_tinh) " & _
                        "   AND (hdr.ma_huyen = h.ma_huyen) " & _
                        "   AND (hdr.xa_id = x.xa_id) " & _
                        "   AND (hdr.ma_nv = kt.ma_nv(+)) " & _
                        "   AND (hdr.ma_ks = ks.ma_nv(+)) " & _
                        "and ((hdr.kyhieu_ct||hdr.so_ct) in (" & strKHSoCT & "))"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi phục hồi chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
#End Region

#Region "Tra cứu chứng từ"
        Public Function TCS_BK_CTU_TRACUU(ByVal strSoCTList As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report       
            ' Ngày viết: 16/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess
                strSQL = "select hdr.tk_co as TK_Co , hdr.ma_nnthue as Ma_NNT, hdr.ten_nnthue as Ten_NNT, " & _
                                "hdr.kyhieu_ct as Kyhieu_CT, hdr.so_ct as So_CT, " & _
                                "(hdr.ma_tinh||'.'||hdr.ma_huyen||'.'||hdr.ma_xa) as Ma_DB, " & _
                                "hdr.ngay_kb as Ngay_KB, " & _
                                "hdr.so_bthu as Ma_BT, hdr.ttien as Tien , hdr.tt_tthu as So_TThu, " & _
                                "hdr.tk_no as TK_No " & _
                                "from TCS_CTU_HDR hdr " & _
                                "where (hdr.kyhieu_ct||hdr.so_ct||hdr.trang_thai) in (" & strSoCTList & ") " & _
                                "order by hdr.ma_nv, hdr.so_bt "
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi truy vấn BK CTU đưa ra DataSet để gắn vào report")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Function TCS_BKE_MAU10_1068_TC(ByVal strKeyLists As String, Optional ByVal blnTHOP As Boolean = False) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy dữ liệu chứng từ theo bảng kê mẫu 10 - QD 1068        
            ' Ngày viết: 17/09/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------------------
            Dim cnCT As DataAccess
            Try
                Dim strHDR As String = "tcs_ctu_hdr"
                Dim strDTL As String = "tcs_ctu_dtl"

                If (blnTHOP) Then
                    strHDR = "tcs_ctu_thop_hdr"
                    strDTL = "tcs_ctu_thop_dtl"
                End If

                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = " SELECT   TCS_PCK_TRAODOI.fnc_formattk(hdr.tk_no) AS tk_no, TCS_PCK_TRAODOI.fnc_formattk(hdr.tk_co) AS tk_co, dtl.maquy AS maquy, " & _
                                    "        hdr.shkb AS ma_kb, (hdr.ma_xa) AS ma_db, (dtl.ma_chuong) AS chuong, " & _
                                    "       dtl.ma_khoan AS khoan, dtl.ma_tmuc AS tmuc, (hdr.kyhieu_ct||'/'||hdr.so_ct) AS so_ct, " & _
                                    "      hdr.ngay_kb AS ngay_kb, hdr.ten_nnthue AS ten_nguoinop, " & _
                                        "     dtl.sotien AS tien, hdr.ngay_ct As Ngay_CT, hdr.ma_dthu As Ma_DThu, dthu.ten As Ten_DThu " & _
                                    " FROM " & strHDR & " hdr, " & strDTL & " dtl, tcs_dm_diemthu dthu " & _
                                    " WHERE (hdr.shkb = dtl.shkb) " & _
                                    " AND (hdr.ngay_kb = dtl.ngay_kb) " & _
                                    " AND (hdr.ma_nv = dtl.ma_nv) " & _
                                    " AND (hdr.so_bt = dtl.so_bt) " & _
                                    " AND (hdr.ma_dthu = dtl.ma_dthu) " & _
                                    " AND (hdr.ma_dthu = dthu.ma_dthu) " & _
                                    " and ((hdr.shkb || hdr.ngay_kb || hdr.ma_nv || hdr.so_bt || hdr.ma_dthu) in  (" & strKeyLists & ")) " & _
                                    " ORDER BY hdr.ma_dthu, hdr.ngay_kb, dtl.maquy, hdr.tk_no, hdr.tk_co, hdr.ma_xa, hdr.ma_nv, hdr.so_bt "

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ theo bảng kê mẫu 10 - QD 1068")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
#End Region

#Region "Chứng từ từ biên lai thu"
        Public Function CTU_BLT_TM(ByVal strKHCT As String, ByVal strSoCT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Truy vấn dữ liệu chứng từ tổng hợp từ BLT        
            ' Ngày viết: 16/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnCT As New DataAccess
            Dim strSQL As String
            Try
                strSQL = "SELECT dtl.noi_dung AS noi_dung, (dtl.ma_cap||'.'||dtl.ma_chuong ) AS chuong, dtl.ma_loai AS loai, " & _
                            "dtl.ma_khoan AS khoan, dtl.ma_muc AS muc, dtl.ma_tmuc AS tmuc, " & _
                            "dtl.ky_thue AS kythue, dtl.sotien AS sotien, dtl.ma_tldt AS madieutiet, " & _
                            "(hdr.kyhieu_ct || hdr.so_ct) AS khsoct, hdr.ma_nnthue AS ma_nnthue, " & _
                            "hdr.ten_nnthue AS ten_nnthue, hdr.dc_nnthue AS dc_nnthue, " & _
                            "hdr.ma_nntien AS ma_nntien, hdr.ten_nntien AS ten_nntien, " & _
                            "hdr.dc_nntien AS dc_nntien, hdr.tk_no AS tk_no, hdr.tk_co AS tk_co, " & _
                            "nha.ten AS nh_a, nhb.ten AS nh_b, hdr.ma_cqthu AS ma_cqthu, " & _
                            "cqthu.ten AS ten_cqthu, hdr.so_tk AS so_tk, " & _
                            "TO_CHAR (hdr.ngay_tk, 'dd/MM/yyyy') AS ngay_tk,  " & _
                            "replace(replace(t.ten , 'Tỉnh', ''),'Thành phố','') AS tinh, " & _
                            "replace(replace(decode (h.ma_huyen, '00', '', h.ten), 'Huyện', ''),'Quận','') AS huyen,  " & _
                            "replace(replace(decode (x.ma_xa, '00', '', x.ten), 'Phường', ''),'Xã','') AS xa, " & _
                            "DECODE (hdr.dc_nntien, NULL, '', replace(replace(t.ten , 'Tỉnh', ''),'Thành phố','')) AS tinh2, " & _
                            "DECODE (hdr.dc_nntien, NULL, '', replace(replace(decode (h.ma_huyen, '00', '', h.ten), 'Huyện', ''),'Quận','')) AS huyen2, " & _
                            "DECODE (hdr.dc_nntien, NULL, '', replace(replace(decode (x.ma_xa, '00', '', x.ten), 'Phường', ''),'Xã','')) AS xa2, " & _
                            "x.ten AS xa2, to_char(hdr.ttien) AS tien, " & _
                            "(x.ma_tinh||'.'||x.ma_huyen||'.'||x.ma_xa) as Ma_DBHC, " & _
                            "kt.ten as ketoan, ks.ten as kiemsoat " & _
                            "FROM tcs_ctu_hdr hdr, " & _
                            "tcs_ctu_dtl dtl, " & _
                            "tcs_dm_nganhang nha, " & _
                            "tcs_dm_nganhang nhb, " & _
                            "tcs_dm_cqthu cqthu, " & _
                            "tcs_dm_tinh t, " & _
                            "tcs_dm_huyen h, " & _
                            "tcs_dm_xa x, " & _
                            "tcs_dm_nhanvien kt, " & _
                            "tcs_dm_nhanvien ks " & _
                            " WHERE (hdr.shkb = dtl.shkb) " & _
                            "   AND (hdr.ngay_kb = dtl.ngay_kb) " & _
                            "   AND (hdr.ma_nv = dtl.ma_nv) " & _
                            "   AND (hdr.so_bt = dtl.so_bt) " & _
                            "   AND (hdr.ma_dthu = hdr.ma_dthu) " & _
                            "   AND (hdr.ma_nh_a = nha.ma(+)) " & _
                            "   AND (hdr.ma_nh_b = nhb.ma(+)) " & _
                            "   AND (hdr.ma_cqthu = cqthu.ma_cqthu) " & _
                            "   AND (hdr.ma_tinh = t.ma_tinh) " & _
                            "   AND (hdr.ma_tinh = h.ma_tinh) " & _
                            "   AND (hdr.ma_huyen = h.ma_huyen) " & _
                            "   AND (hdr.xa_id = x.xa_id) " & _
                            "   AND (hdr.ma_nv = kt.ma_nv(+)) " & _
                            "   AND (hdr.ma_ks = ks.ma_nv(+)) " & _
                            "   AND (hdr.kyhieu_ct='" & strKHCT & "') " & _
                            "   AND (hdr.so_ct='" & strSoCT & "') "
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi truy vấn dữ liệu chứng từ tổng hợp từ BLT")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
#End Region

#Region "Biên lai thu"
        Public Function BLT_TC(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Tra cứu dữ liệu biên lại        
            ' Ngày viết: 07/08/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnCT As New DataAccess
            Dim strSQL As String
            Try
                strSQL = "select bl.ten_nntien as Ho_ten, bl.kyhieu_bl as KH_BL, bl.so_bl as So_BL, " & _
                            "bl.ngay_kb as Ngay_nop, bl.ttien as So_tien, bl.trang_thai as Ghi_chu " & _
                            "from tcs_ctbl bl " & _
                            "where (bl.kyhieu_bl ||bl.so_bl || bl.trang_thai) in (" & strWhere & ") " & _
                            "order by bl.kyhieu_bl, bl.so_bl "
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi tra cứu dữ liệu biên lai")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
#End Region

#Region "Bảng kê chứng từ cuối ngày"
        Public Function TCS_BKE_MAU10(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy dữ liệu chứng từ theo bảng kê mẫu 10
            ' Tham số: 
            ' Giá trị trả về: DataSet dữ liệu
            ' Ngày viết: 09/09/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------------------
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select hdr.so_ct as soct, hdr.ngay_kb as ngayct, hdr.tk_no as tkno, hdr.tk_co as tkco, " & _
                                "(dtl.ma_cap||dtl.ma_chuong) as CC, (dtl.ma_loai||dtl.ma_khoan) as LK, " & _
                                "(dtl.ma_muc||dtl.ma_tmuc) as MT, (hdr.ma_tinh||'.'||hdr.ma_huyen||'.'||hdr.ma_xa) as DBHC, " & _
                                "dtl.sotien as tien, ' ' as MaNH, (hdr.ngay_kb||hdr.ma_nv||hdr.so_bt) as Key " & _
                                "from tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " & _
                                "where (hdr.shkb = dtl.shkb) and (hdr.ngay_kb = dtl.ngay_kb) and  " & _
                                "(hdr.ma_nv = dtl.ma_nv) and (hdr.so_bt = dtl.so_bt) and  " & _
                                "(hdr.ma_dthu = dtl.ma_dthu) and (hdr.trang_thai = '01') " & _
                                strWhere & _
                                "order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt "

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ theo bảng kê mẫu 10")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        'Public Function TCS_BKE_MAU10_1068(ByVal strWhere As String) As DataSet
        '    '-----------------------------------------------------------------
        '    ' Mục đích: Lấy dữ liệu chứng từ theo bảng kê mẫu 10 - QD 1068        
        '    ' Ngày viết: 17/09/2008
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------------------
        '    Dim cnCT As DataAccess
        '    Try
        '        cnCT = New DataAccess
        '        Dim strSQL As String
        '        strSQL = "SELECT   hdr.tk_no AS tkno, hdr.tk_co AS tkco, " & _
        '                        " (hdr.ma_tinh || '.' || hdr.ma_huyen || '.' || hdr.ma_xa) AS dbhc, " & _
        '                        " hdr.shkb AS shkb, (dtl.ma_cap || dtl.ma_chuong) AS chuong, " & _
        '                        " dtl.ma_loai AS loai, dtl.ma_khoan AS khoan, dtl.ma_muc AS muc, " & _
        '                        " dtl.ma_tmuc AS tieumuc, hdr.so_ct AS soct, hdr.ngay_kb AS ngay, " & _
        '                        " dtl.sotien AS tien " & _
        '                        " FROM tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " & _
        '                        " WHERE (hdr.shkb = dtl.shkb) AND (hdr.ngay_kb = dtl.ngay_kb) " & _
        '                        " AND (hdr.ma_nv = dtl.ma_nv) AND (hdr.so_bt = dtl.so_bt) " & _
        '                        " AND (hdr.ma_dthu = dtl.ma_dthu) AND (hdr.trang_thai <> '04') " & _
        '                        strWhere & _
        '                        "order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt "

        '        Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo: " & ex.ToString)
        '        Throw ex
        '    Finally
        '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        '    End Try
        'End Function

        Public Function TCS_BKE_MAU10_1068(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy dữ liệu chứng từ theo bảng kê mẫu 10 - QD 1068        
            ' Ngày viết: 17/09/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------------------
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                'strSQL = "SELECT   hdr.tk_no AS tkno, hdr.tk_co AS tkco, " & _
                '                " (hdr.ma_tinh || '.' || hdr.ma_huyen || '.' || hdr.ma_xa) AS dbhc, " & _
                '                " hdr.shkb AS shkb, (dtl.ma_cap || dtl.ma_chuong) AS chuong, " & _
                '                " dtl.ma_loai AS loai, dtl.ma_khoan AS khoan, dtl.ma_muc AS muc, " & _
                '                " dtl.ma_tmuc AS tieumuc, hdr.so_ct AS soct, hdr.ngay_kb AS ngay, " & _
                '                " dtl.sotien AS tien " & _
                '                " FROM tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " & _
                '                " WHERE (hdr.shkb = dtl.shkb) AND (hdr.ngay_kb = dtl.ngay_kb) " & _
                '                " AND (hdr.ma_nv = dtl.ma_nv) AND (hdr.so_bt = dtl.so_bt) " & _
                '                " AND (hdr.ma_dthu = dtl.ma_dthu) AND (hdr.trang_thai <> '04') " & _
                '                strWhere & _
                '                "order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt "

                strSQL = " SELECT   TCS_PCK_TRAODOI.fnc_formattk(hdr.tk_no) AS tk_no, TCS_PCK_TRAODOI.fnc_formattk(hdr.tk_co) AS tk_co, dtl.maquy AS maquy, " & _
                                "        hdr.shkb AS ma_kb, (hdr.ma_xa) AS ma_db, (dtl.ma_chuong) AS chuong, " & _
                                "       dtl.ma_khoan AS khoan, dtl.ma_tmuc AS tmuc, (hdr.kyhieu_ct||'/'||hdr.so_ct) AS so_ct, " & _
                                "      hdr.ngay_kb AS ngay_kb,hdr.ngay_kh_nh ngayKH_NH, hdr.ten_nnthue AS ten_nguoinop, " & _
                                    "     dtl.sotien AS tien, hdr.ngay_ct As Ngay_CT, hdr.ma_dthu As Ma_DThu, dthu.ten As Ten_DThu " & _
                                    " FROM tcs_ctu_hdr hdr, tcs_ctu_dtl dtl, tcs_dm_diemthu dthu " & _
                                " WHERE (hdr.shkb = dtl.shkb) " & _
                                    " AND (hdr.ngay_kb = dtl.ngay_kb) " & _
                                    " AND (hdr.ma_nv = dtl.ma_nv) " & _
                                    " AND (hdr.so_bt = dtl.so_bt) " & _
                                    " AND (hdr.ma_dthu = dtl.ma_dthu) " & _
                                    " AND (hdr.ma_dthu = dthu.ma_dthu) " & _
                                    " AND (hdr.trang_thai = '01') " & _
                                    strWhere & _
                                " ORDER BY hdr.ma_dthu, hdr.ngay_kb, dtl.maquy, hdr.tk_no, hdr.tk_co, hdr.ma_xa, hdr.ma_nv, hdr.so_bt "

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ theo bảng kê mẫu 10 - QD 1068 ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function TCS_CTU_HUY(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy dữ liệu chứng từ theo bảng kê mẫu 10 - QD 1068        
            ' Ngày viết: 17/09/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------------------
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select hdr.ngay_kb as Ngay_CT, hdr.so_ct as So_CT, hdr.MA_NNTHUE,hdr.TEN_NNTHUE,hdr.ma_nv, " & _
                                 "N.TEN_DN, hdr.ttien , (case hdr.PT_TT when '00' then 'TM' when '01' then 'CK' when '03' then 'GL' else 'BC' end ) PTTT  " & _
                                 "from tcs_ctu_hdr hdr, tcs_dm_nhanvien N  " & _
                                 "where (hdr.ma_nv =n.ma_nv)" & _
                                 "and hdr.trang_thai = '04' and (hdr.ma_ks is not null or hdr.ma_ks <>'' or hdr.ma_ks <>0) " & strWhere & _
                                 " Order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt asc"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ theo bảng kê mẫu 10 - QD 1068 ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BKE_CT_TrongGio(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select hdr.so_ct as so_ctu,hdr.ma_nv,d.id ma_dthu,D.Name ten, hdr.ngay_kb as ngayct,hdr.ma_nnthue ma_NNT, hdr.ten_nnthue ten_nnt, " & _
                               " hdr.Ngay_KS Ngay_HT, hdr.ngay_kh_nh, hdr.ttien so_tien, p.remark_desc NOIDUNG,hdr.seq_no SO_FT, (CASE  WHEN   hdr.tt_cthue = 1  and (hdr.trang_thai='01' or hdr.trang_thai='02') " & _
               " THEN 'Đã chuyển'" & _
                "WHEN   hdr.tt_cthue = 0  and hdr.trang_thai='04'" & _
                "THEN 'Đã chuyển'" & _
                "WHEN   hdr.tt_cthue = 0  and (hdr.trang_thai='01' or hdr.trang_thai='02') " & _
                "THEN 'Chưa chuyển'" & _
            " ELSE 'Chưa chuyển'  END )   trangthai,N.TEN_DN username,  n_ks.ten_dn username_ks, p.response_code, " & _
            " (case  when hdr.trang_thai='01' then 'Đã duyệt' when hdr.trang_thai='04' then 'Hủy duyệt' else 'Hủy - Chờ duyệt' end )trang_thai_CT " & _
               "from tcs_ctu_hdr hdr,TCS_DM_NhanVien N, tcs_dm_chinhanh D, tcs_dm_nhanvien n_ks, tcs_log_payment p " & _
               " where N.ma_nv=hdr.ma_nv and d.id= n.ma_cn and n_ks.ma_nv=hdr.ma_ks   and p.rm_ref_no= hdr.rm_ref_no and p.response_code='0' " & _
               strWhere & _
               "order by hdr.ngay_ks"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                ' bỏ điều kiện
                'and  TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks) 
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ theo bảng kê ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BC_THUE_PGB(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select  hdr.ma_nnthue,hdr.ten_nnthue,hdr.ttien,hdr.so_ct, " & _
                         " p.response_msg as rm_ref_no,hdr.ma_lthue,(hdr.phi_gd+hdr.phi_vat) as PHI_VAT," & _
                         " p.tran_type as LoaiKH, p.cert_code as NhomKH,to_char(to_date(hdr.ngay_ct, 'yyyymmdd'), 'dd-mm-yyyy') ngay_ct," & _
                         " hdr.trang_thai,cn.id ma_cn,cn.name ten_cn,nv.ten_dn user_lap,nv_ks.ten_dn user_ks," & _
                         " cqt.ten ten_cqthu,p.tran_date,hdr.tt_cthue trang_thai_nsnn" & _
                         " from tcs_ctu_hdr hdr,tcs_dm_nhanvien nv,tcs_dm_nhanvien nv_ks,tcs_dm_chinhanh cn,tcs_log_payment p, tcs_dm_cqthu cqt" & _
                         " where hdr.ma_nv=nv.ma_nv" & _
                         " and hdr.ma_ks=nv_ks.ma_nv and nv.ma_cn = cn.id" & _
                         " and hdr.rm_ref_no = p.rm_ref_no and hdr.ma_cqthu=cqt.ma_cqthu " & _
                         strWhere & _
                         "order by hdr.ngay_ks"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                ' bỏ điều kiện
                'and  TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks) 
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ theo bảng kê ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BC_DC_ETAX_FCC(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = " select to_char(hdr.ngay_ks,'dd/MM/yyyy') as ngay_th, cn.id || '-' || cn.name as ma_cn,to_char(hdr.ngay_ks,'dd/MM/yyyy hh24:mm:ss') as gio_th,nv.ten_dn as user_macker, " & _
                         " hdr.so_ct as so_ctu,hdr.seq_no as so_fcc,hdr.tk_kh_nh as tk_kh_nh,hdr.ttien as ttien, " & _
                         " hdr.trang_thai as tt_etax, hdr.tt_cthue as tt_cthue,ci.tt_chuyen as tt_citad, " & _
                         " Case when TO_CHAR(hdr.ngay_ks,'hh24') <(select GIATRI_TS From tcs_thamso_ht where ten_ts= 'GIO_CUTOFF_CITAD') then 'Phát sinh trước giờ cut-off-time'" & _
                         " ELSE 'Phát sinh sau giờ cut-off-time' END AS TRUOC_SAU from (select * from " & _
                         " (select a.seq_no ,a.ma_nv,a.so_ct,a.so_ct_nh,a.tk_kh_nh,a.ttien,a.trang_thai,a.tt_cthue,a.ma_lthue,a.ma_cn,a.ngay_ks from " & _
                         " tcs_ctu_hdr a )a " & _
                         " FULL JOIN " & _
                         " (select b.created_by,b.tran_ref from tcs_dchieu_etax b)b " & _
                         " on b.tran_ref= a.seq_no) hdr, " & _
                         " tcs_dm_nhanvien nv,tcs_dm_chinhanh cn,tcs_ds_citad ci " & _
                         " where hdr.ma_nv=nv.ma_nv(+)    and hdr.ma_cn=cn.id(+)    and hdr.so_ct=ci.so_ct(+) " & strWhere & _
                         " order by cn.id,hdr.ngay_ks"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                ' bỏ điều kiện
                'and  TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks) 
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ theo bảng kê ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_DC_LENH_CITAD(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "SELECT  LG.SO_CT AS SO_CTU,LG.RELATION_NO AS REF_CITAD, LG.USER_NAME AS USER_MAKER,LG.MA_CN,LG.TEN_NH_TT AS TEN_NH, " & _
                         " LG.AMOUNT AS TTIEN,LG.TT_CHUYEN,LG.TT_NHAN,LG.NGUOI_NHAN,(SELECT COUNT(*) FROM TCS_LOG_NHAN_CITAD LG_NHAN WHERE LG.TT_NHAN=LG_NHAN.TT_NHAN AND LG.RELATION_NO=LG_NHAN.RELATION_NO AND LG.TRX_DATE=LG_NHAN.TRX_DATE) AS COUNT_NUMBER " & _
                         " ,(SELECT LG_NHAN.GHI_CHU FROM TCS_LOG_NHAN_CITAD LG_NHAN WHERE LG.TT_NHAN=LG_NHAN.TT_NHAN AND LG.RELATION_NO=LG_NHAN.RELATION_NO AND LG.TRX_DATE=LG_NHAN.TRX_DATE) AS GHI_CHU FROM TCS_LOG_DS_CITAD LG, TCS_CTU_HDR HDR WHERE LG.SO_CT = HDR.SO_CT " & strWhere & _
                         " ORDER BY LG.SO_CT,LG.RELATION_NO "
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                ' bỏ điều kiện
                'and  TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks) 
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ theo bảng kê ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BKE_CT_TrongGio_MHB(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select l.ref_seq_no so_tham_so,hdr.so_ct, hdr.ten_nnthue ten_nnt,hdr.ma_nnthue ma_nnthue,hdr.ttien THUE,hdr.phi_gd PHI,hdr.phi_vat VAT,(NVL (hdr.ttien, 0) + NVL (hdr.phi_gd, 0) + NVL (hdr.phi_vat, 0)) sotien,hdr.ngay_ks gio_gd,hdr.pt_tt KENH_CTIEN,k.ten_kenh,(d.name || '-' || d.id) chi_nhanh,b.ten ten_kb,c.ten_nh_chuyen ma_nh_a,nh.ten_giantiep ma_nh_b,hdr.trang_thai tt_ctu,d1.name dv_thuchien " & _
                         "from tcs_ctu_hdr hdr left JOIN tcs_log_payment l  on hdr.rm_ref_no = l.rm_ref_no LEFT join tcs_dm_kenh_tt k on hdr.kenh_tt = k.id  LEFT join tcs_dm_chinhanh d1 on trim(d1.id) = trim (l.branch_no) join  tcs_dm_nhanvien n on hdr.ma_nv=n.ma_nv JOIN tcs_dm_chinhanh d on n.ma_cn = d.id join tcs_dm_khobac b on hdr.shkb = b.shkb " & _
                         "join tcs_nganhang_chuyen c on hdr.ma_nh_a= c.ma_nh_chuyen and c.ma_truc_tiep=1 join  tcs_dm_nh_giantiep_tructiep nh on hdr.ma_nh_b = nh.ma_giantiep where  hdr.shkb = nh.shkb " & _
                         strWhere & " order by D.branch_id,hdr.so_ct"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BKE_CT_TrongGio_New(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = " select hdr.so_ct,l.ref_seq_no SO_THAM_SO,hdr.ten_nnthue TEN_NNT,hdr.tk_kh_nh TK_GHINO,hdr.ttien thue,hdr.phi_gd phi,hdr.phi_vat vat," & _
                        " (NVL (hdr.ttien, 0) + NVL (hdr.phi_gd, 0) + NVL (hdr.phi_vat, 0)) TONG_TIEN,hdr.ngay_ks GIO_GD,k.ten_kenh KENH_CTIEN" & _
                        " from tcs_ctu_hdr hdr,tcs_log_payment l,tcs_dm_kenh_tt k,tcs_dm_nhanvien n,tcs_dm_chinhanh d " & _
                        " where hdr.rm_ref_no(+) = l.rm_ref_no and hdr.kenh_tt=k.id and hdr.ma_nv=n.ma_nv and n.ma_cn=d.id  " & _
                         strWhere & " order by D.branch_id,hdr.so_ct"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
              log.Error(ex.Message & "-" & ex.StackTrace)'
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BKE_CT_TrongGio_Goc(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select hdr.so_ct as so_ctu,hdr.ma_nv,d.id ma_dthu,D.Name ten, hdr.ngay_kb as ngayct,hdr.ma_nnthue ma_NNT, hdr.ten_nnthue ten_nnt, " & _
                               " hdr.Ngay_KS Ngay_HT, hdr.ngay_kh_nh, hdr.ttien so_tien, p.remark_desc NOIDUNG,p.response_msg SO_FT, (CASE  WHEN   hdr.tt_cthue = 1  and hdr.trang_thai='01'" & _
               " THEN 'Da chuyen'" & _
                "WHEN   hdr.tt_cthue = 0  and hdr.trang_thai='04'" & _
                "THEN 'Da chuyen'" & _
                "WHEN   hdr.tt_cthue = 0  and hdr.trang_thai='01'" & _
                "THEN 'Chua chuyen'" & _
            " ELSE 'Chua chuyen'  END )   trangthai,N.TEN_DN username,  n_ks.ten_dn username_ks, p.response_code, " & _
            " (case hdr.trang_thai when '01' then 'Da duyet' else ' Huy Duyet' end )trang_thai_CT " & _
               "from tcs_ctu_hdr hdr,TCS_DM_NhanVien N, tcs_dm_chinhanh D, tcs_dm_nhanvien n_ks, tcs_log_payment p " & _
               " where N.ma_nv=hdr.ma_nv and d.id= n.ma_cn and n_ks.ma_nv=hdr.ma_ks   and p.rm_ref_no= hdr.rm_ref_no and p.response_code='00' and  TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks) " & _
               strWhere & _
               "order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ theo bảng kê ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BKE_DOICHIEU(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select hdr.so_ct as so_ctu,hdr.ma_nv,d.id ma_dthu,D.name ten, hdr.ngay_kb as ngayct,hdr.ma_nnthue ma_NNT, hdr.ten_nnthue ten_nnt," & _
                " decode( hdr.ma_lthue,'04','04','01') ma_lthue, " & _
                " hdr.Ngay_HT, hdr.ngay_kh_nh, hdr.ttien so_tien,  (CASE  WHEN   hdr.tt_cthue = 1  and hdr.trang_thai='01'" & _
               " THEN 'Da chuyen'" & _
                "WHEN   hdr.tt_cthue = 0  and hdr.trang_thai='04'" & _
                "THEN 'Da chuyen'" & _
                "WHEN   hdr.tt_cthue = 0  and hdr.trang_thai='01'" & _
                "THEN 'Chua chuyen'" & _
            " ELSE 'Chua chuyen'  END )  trangthai,hdr.rm_ref_no SORM , (case hdr.trang_thai when '01' then 'Da duyet' else ' Huy Duyet' end )trang_thai_CT " & _
                               "from tcs_ctu_hdr hdr, tcs_dm_chinhanh D,TCS_DM_NhanVien N" & _
                            " where d.id= n.ma_cn and N.ma_nv=hdr.ma_nv " & _
                               strWhere & _
                               "order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu chứng từ đối chiếu ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BKE_CT_ThuHo(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select hdr.so_ct as soct, hdr.ngay_kb as ngayct, hdr.ten_nnthue , " & _
                                "(dtl.ma_cap||dtl.ma_chuong) as CC, (dtl.ma_loai||dtl.ma_khoan) as LK, " & _
                                "(dtl.ma_muc||dtl.ma_tmuc) as MT, (case hdr.PT_TT when '00' then 'TM' when '01' then 'CK' when '03' then 'GL' else 'BC' end ) PTTT, " & _
                                "dtl.sotien as tien, ' ' as MaNH,  Substr(hdr.tk_kb_nh,0,3) BDF " & _
                                "from tcs_ctu_hdr hdr, tcs_ctu_dtl dtl,TCS_DM_NHANVIEN N " & _
                                "where (hdr.shkb = dtl.shkb) and (hdr.ngay_kb = dtl.ngay_kb) and  " & _
                                "(hdr.ma_nv = dtl.ma_nv) and (hdr.so_bt = dtl.so_bt) and  " & _
                                "(hdr.ma_dthu = dtl.ma_dthu)  and hdr.trang_thai = '01' and hdr.ma_dthu <> N.BAN_LV AND hdr.ma_nv=n.ma_nv " & _
                                strWhere & _
                                "order by hdr.ngay_kb, hdr.ma_nv, hdr.so_bt "
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu bang kê chứng từ thu hộ ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BC_TONG_CN(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = " SELECT   cn.branch_id ma_cn,MAX (cn2.name) ten_cn,MAX (XA.ten) quanhuyen," & _
                         " COUNT (DISTINCT hdr.tk_kh_nh) soluong_khachhang,COUNT (p.rm_ref_no) soluong_giaodich, " & _
                         " SUM(DECODE (NVL2(p.cif_no,'Y','N'),'Y',NVL (hdr.ttien, 0),0))giatri_gd," & _
                         " SUM(DECODE (NVL2(p.cif_no,'Y','N'),'N',NVL (hdr.ttien, 0),0))giatri_gd_gl,    " & _
                         " SUM (NVL (hdr.ttien, 0)) amt,SUM (NVL (hdr.phi_gd, 0)) fee,SUM (NVL (hdr.phi_vat, 0)) vat," & _
                         " (ROUND (ratio_to_report(SUM(  NVL (hdr.ttien, 0)+ NVL (hdr.phi_gd, 0)+ NVL (hdr.phi_vat, 0)))OVER (),4))* 100 tytrong_gd" & _
                         " FROM   tcs_ctu_hdr hdr,tcs_dm_chinhanh cn,tcs_dm_chinhanh cn2,tcs_dm_nhanvien nv,tcs_dm_xa xa,tcs_log_payment p" & _
                         " WHERE   (    (nv.ma_cn = cn.id) " & _
                         " AND (hdr.ma_nv = nv.ma_nv)" & _
                         " AND (hdr.trang_thai = '01') AND (p.response_code is not null) " & _
                         " AND p.acct_no = hdr.tk_kh_nh " & _
                         strWhere & _
                        " AND (hdr.rm_ref_no = p.rm_ref_no)" & _
                        " AND (cn.provice_code = xa.ma_tinh)" & _
                        " AND (cn.branch_id=cn2.id))" & _
                        " GROUP BY cn.branch_id"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                'bỏ " AND TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks)" & _
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BC_DC_02_BKDC(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = " select a.so_bt so_seri,a.so_ct so_ctu,to_char(a.ngay_ht,'dd/MM/rrrr') ngay_ht, a.ngay_kh_nh ngay_nt," & _
                         " a.so_tk, a.ngay_tk ngay_tk, a.ma_nnthue ma_nnt, a.ten_nnthue ten_nnt, " & _
                         " a.shkb ma_kbnn, a.ma_cqthu ma_tctd, dtl.ma_chuong || '-' || dtl.ma_tmuc ch_tm," & _
                         " a.ma_nt ngoai_te, a.ttien_nt sotien_nt, a.ty_gia ty_gia, a.ttien, a.ma_ntk ma_ntk    " & _
                         " from tcs_ctu_hdr a,tcs_ctu_dtl dtl,tcs_dm_chinhanh cn,tcs_dm_nhanvien nv, tcs_dchieu_nhhq_hdr b, tcs_dchieu_hqnh_hdr c " & _
                         " where a.ma_nv=dtl.ma_nv " & _
                         " and a.ngay_kb=dtl.ngay_kb" & _
                         " and a.shkb=dtl.shkb " & _
                         " and a.so_bt=dtl.so_bt and a.ma_nv=nv.ma_nv and nv.ma_cn = cn.id" & _
                         " and a.ma_lthue='04' " & _
                         " and (a.trang_thai='01' or a.trang_thai='02') " & _
                         " and a.so_ct= b.so_ct " & _
                         " and b.so_tn_ct= c.so_tn_ct " & _
                         " and a.tt_cthue='1' " & _
                         " and (c.kq_dc<>'22' or c.kq_dc<>'21') " & _
                         strWhere & _
                        " order BY   a.ma_ntk, a.so_ct"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                'bỏ " AND TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks)" & _
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function TCS_BC_TONG_NHCD(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "SELECT   bc.ma_cn,MAX (bc.ten_cn) ten_cn,SUM (bc.soluong_khachhang) soluong_khachhang," & _
                         " SUM (bc.soluong_giaodich) soluong_giaodich,SUM (bc.giatri_gd) giatri_gd," & _
                         "SUM (bc.amt) amt,SUM (bc.fee) fee,SUM (bc.vat) vat," & _
                         "(ROUND (ratio_to_report (SUM (NVL (bc.amt, 0)))OVER (),4))* 100 tytrong_gd" & _
                         " FROM   (  SELECT   k.INDICATOR ma_cn,DECODE (k.INDICATOR,'F', 'FI','S', 'SME','L', 'LC','R', 'RB',NULL)ten_cn," & _
                         " COUNT (DISTINCT p.acct_no) soluong_khachhang,COUNT (p.rm_ref_no) soluong_giaodich," & _
                         " SUM(  NVL (p.amt, 0))giatri_gd," & _
                         " SUM (NVL (p.amt, 0)) amt,SUM (NVL (p.fee, 0)) fee,SUM (NVL (p.vat_charge, 0)) vat" & _
                         " FROM   tcs_kh_nh k, tcs_log_payment p, tcs_ctu_hdr h" & _
                         " WHERE       p.response_code = 0" & _
                         " AND h.trang_thai = '01'" & _
                         strWhere & _
                         " AND TRUNC (p.tran_date) = TRUNC (h.ngay_ks)" & _
                         " AND p.acct_no = h.tk_kh_nh" & _
                         " AND h.rm_ref_no = p.rm_ref_no" & _
                         " AND p.cif_no = k.cif_number" & _
                         " GROUP BY   k.INDICATOR" & _
                         " UNION ALL" & _
                         " SELECT   'GL' ma_cn,'GL' ten_cn," & _
                         " COUNT (DISTINCT p.acct_no) soluong_khachhang,COUNT (p.rm_ref_no) soluong_giaodich," & _
                         " SUM(  NVL (p.amt, 0))giatri_gd," & _
                         " SUM (NVL (p.amt, 0)) amt,SUM (NVL (p.fee, 0)) fee,SUM (NVL (p.vat_charge, 0)) vat" & _
                         " FROM   tcs_log_payment p, tcs_ctu_hdr h" & _
                         " WHERE       p.response_code = 0" & _
                         " AND h.trang_thai = '01'" & _
                         strWhere & _
                         " AND TRUNC (p.tran_date) = TRUNC (h.ngay_ks)" & _
                         " AND p.acct_no = lpad(h.tk_kh_nh, 14, '0')" & _
                         " AND h.rm_ref_no = p.rm_ref_no" & _
                         " AND p.cif_no IS NULL" & _
                         " AND NOT EXISTS " & _
                         " (SELECT   1 FROM   tcs_kh_nh k WHERE   (k.cif_number = p.cif_no))) bc" & _
                         " GROUP BY   bc.ma_cn"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy dữ liệu báo cáo tổng")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function checkHSC_MaCN(ByVal strMa_CN As String) As String

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "Select cn.phan_cap from tcs_dm_chinhanh cn where cn.id ='" & strMa_CN & "' "
                Return DataAccess.ExecuteSQLScalar(strSQL)
            Catch ex As Exception
               log.Error(ex.Message & "-" & ex.StackTrace)'
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function checkHSC(ByVal Ma_NV As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "Select cn.phan_cap from TCS_DM_NhanVien nv,tcs_dm_chinhanh cn where cn.id=nv.ma_cn and ma_nv='" & Ma_NV & "' "
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi kiểm tra thông tin HSC")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Lỗi trong quá trình lấy dữ liệu kết xuất bảng kê báo cáo!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function BC_THOIGIANXULYGD(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "  SELECT HDR.REF_NO , HDR.MA_NV,TO_CHAR(HDR.TIME_BEGIN,'dd/MM/yyyy hh:mm:ss') AS NGAY_LAP,TO_CHAR(HDR.NGAY_HT,'dd/MM/yyyy hh:mm:ss') AS NGAY_GHI,TO_CHAR(HDR.NGAY_KS,'dd/MM/yyyy hh:mm:ss') AS NGAY_DUYET," & _
                         "  ROUND((HDR.NGAY_KS-HDR.NGAY_HT)*(60*24),2) AS THOIGIAN_DUYET, " & _
                         " (CASE  WHEN HDR.MA_NV=0 THEN '' ELSE (SELECT NV1.TEN FROM TCS_DM_NHANVIEN NV1 WHERE NV1.MA_NV=HDR.MA_NV) END) AS NGUOI_LAP, " & _
                         " (CASE  WHEN HDR.MA_KS=0 THEN '' ELSE (SELECT NV1.TEN FROM TCS_DM_NHANVIEN NV1 WHERE NV1.MA_NV=HDR.MA_KS) END) AS NGUOI_DUYET, " & _
                         "  (CASE  WHEN HDR.MA_HUYKS=0 THEN '' ELSE (SELECT NV2.TEN FROM TCS_DM_NHANVIEN NV2 WHERE NV2.MA_NV=HDR.MA_HUYKS) END) AS NGUOI_DUYET_HUY," & _
                         "  TO_CHAR(HDR.NGAY_GDV_HUY,'dd/MM/yyyy hh:mm:ss') AS NGAY_HUY, TO_CHAR(HDR.NGAY_KSV_HUY,'dd/MM/yyyy hh:mm:ss') AS NGAY_DUYET_HUY,ROUND((HDR.NGAY_KSV_HUY-HDR.NGAY_GDV_HUY)*(60*24),2) AS THOIGIAN_HUY " & _
                         " FROM TCS_CTU_HDR HDR,TCS_DM_NHANVIEN NV,TCS_DM_CHINHANH CN,TCS_DM_LTHUE LT " & _
                         " WHERE HDR.MA_NV=NV.MA_NV AND NV.MA_CN = CN.ID AND HDR.MA_LTHUE=LT.MA_LTHUE " & _
                         strWhere & _
                         " ORDER BY HDR.so_ct DESC "
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                'bỏ " AND TRUNC (p.tran_date) = TRUNC (hdr.ngay_ks)" & _
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

#End Region

        Public Function GetTenDN(ByVal maNV As String) As String
            Dim ds As DataSet
            Dim strSQL = "Select ten from TSC_DM_NhanVien where ma_nv=" & maNV
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    Return ds.Tables(0).Rows(0)("Ten").ToString()
                End If

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin đăng nhập")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)

                Throw ex
            Finally
                If (Not IsNothing(ds)) Then ds.Dispose()
            End Try
        End Function

        'sonmt
        Public Function BC_ChiTietHQBLC(ByVal strWhere As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = " SELECT HDR.TEN_NNTHUE , HQ.MA_NNTHUE, HDR.CIF, NV.MA_CN, HDR.SO_CT, HDR.TTIEN, HDR.NGAY_BATDAU, HDR.NGAY_KETTHUC, HQ.SODU, HQ.SO_TK , HQ.NGAY_DK, HQ.SO_SD, HDR.MA_HQ, HDR.TEN_HQ, HQ.SO_PH, NV.TEN " & _
                         " FROM TCS_BAOLANH_HDR HDR, TCS_BLC_HQ HQ, TCS_DM_NHANVIEN NV " & _
                         " WHERE HQ.MA_NNTHUE = HDR.MA_NNTHUE AND HDR.MA_NV = NV.MA_NV " & _
                         strWhere & _
                         " ORDER BY HDR.SO_TK ASC"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        'sonmt_end

    End Class

End Namespace