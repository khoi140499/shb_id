﻿Option Explicit On 
Option Strict Off
Imports VBOracleLib
Imports System.Data.OleDb
Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.NewChungTu
Imports System.Text
Imports System.IO
Imports System.IO.Compression

Namespace Common
    Public Class mdlCommon
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "phan danh sach dinh nghia ma loi"
        Public Shared ReadOnly TCS_HQ247_OK As Decimal = 0
        Public Shared ReadOnly TCS_HQ247_SYSTEM_NOT_OK As Decimal = 1
        Public Shared ReadOnly TCS_HQ247_HQ As Decimal = 10000
        Public Shared ReadOnly TCS_HQ247_HQ_SAICSK As Decimal = TCS_HQ247_HQ + 1
        Public Shared ReadOnly TCS_HQ247_HQ_SAI_NH_TH As Decimal = TCS_HQ247_HQ + 2
        Public Shared ReadOnly TCS_HQ247_HQ_SAI_TAIKHOAN_TH As Decimal = TCS_HQ247_HQ + 3
        Public Shared ReadOnly TCS_HQ247_HQ_SAI_TRANGTHAI As Decimal = TCS_HQ247_HQ + 4
        Public Shared ReadOnly TCS_HQ247_HQ_KHONGDUSODU As Decimal = TCS_HQ247_HQ + 5
        Public Shared ReadOnly TCS_HQ247_HQ_TRUNGSO_HQ_CTU As Decimal = TCS_HQ247_HQ + 6
        'huynt
        Public Shared ReadOnly TCS_HQ247_HQ_SAI_LOAI_HS As Decimal = TCS_HQ247_HQ + 7 'Sai Loại hồ sơ
        Public Shared ReadOnly TCS_HQ247_HQ_SAI_LOAI_TD_TRA_LOI As Decimal = TCS_HQ247_HQ + 8 'Sai Loại thông điệp trả lời
        Public Shared ReadOnly TCS_HQ247_HQ_SAI_FORMAT_XML As Decimal = TCS_HQ247_HQ + 9 'Sai định dạng XML, dẫn đến việc đọc XML bị lỗi
        Public Shared ReadOnly TCS_HQ247_HQ_SAI_MSG_NHAN_VE_TU_HQ As Decimal = TCS_HQ247_HQ + 10 'HQ trả về loại msg sai với msg của quy trình
        'huynt end
        Public Shared ReadOnly TCS_HQ247_HQ_HACHTOAN_LOI_CTU As Decimal = TCS_HQ247_HQ + 11
        Public Shared ReadOnly TCS_HQ247_HQ_SEND_CTU_LOI As Decimal = TCS_HQ247_HQ + 12
        Public Shared ReadOnly TCS_HQ247_HQ_NGANHANGTT_GIANTIEP_LOI As Decimal = TCS_HQ247_HQ + 13
        Public Shared ReadOnly TCS_HQ247_HQ_LOIINSERT_DOICHIEU_NH_HQ As Decimal = TCS_HQ247_HQ + 14
        Public Shared ReadOnly TCS_HQ247_HQ_CAPNHAT_TRANGTHAI304_SAUKHISEN_301 As Decimal = TCS_HQ247_HQ + 15
        'huynt
        Public Shared ReadOnly TCS_HQ247_HQ_LOI_SUA_HOSO_CO_CHUNGTU_DANGXULY As Decimal = TCS_HQ247_HQ + 16 'Lỗi Sửa Hồ Sơ có Chứng từ đang trong quá trình xử lý
        Public Shared ReadOnly TCS_HQ247_HQ_LOI_SUA_HOSO_DANGXULY As Decimal = TCS_HQ247_HQ + 17 'Lỗi Sửa Hồ Sơ đang trong quá trình xử lý của NH
        Public Shared ReadOnly TCS_HQ247_HQ_LOI_HOSO_KHONGTONTAI As Decimal = TCS_HQ247_HQ + 18 'Lỗi Sửa Hồ Sơ mà nó chưa tồn tại tại NH
        Public Shared ReadOnly TCS_HQ247_HQ_LOI_THEMMOI_HOSO_DANGXULY As Decimal = TCS_HQ247_HQ + 19 'Lỗi Thêm mới Hồ Sơ mà nó đang được xử lý tại NH
        Public Shared ReadOnly TCS_HQ247_HQ_LOI_CHUKY_NNT_KHONGHOPLE As Decimal = TCS_HQ247_HQ + 19 'Lỗi Chữ ký của NNT không hợp lệ
        Public Shared ReadOnly TCS_HQ247_HQ_LOI_CHUKY_HQ_KHONGHOPLE As Decimal = TCS_HQ247_HQ + 19 'Lỗi Chữ ký của HQ không hợp lệ
        Public Shared ReadOnly TCS_HQ247_HQ_LOI_CAUTRUC_MSG_KHONGHOPLE As Decimal = TCS_HQ247_HQ + 19 'Lỗi cấu trúc Thông điệp 311 không hợp lệ
        Public Shared ReadOnly TCS_HQ247_HQ_TRALOI_MSG_ERRCODE_KHAC_0_VA_RONG As Decimal = TCS_HQ247_HQ + 20 'HQ trả về msg có ErrCode khác 0 và khác rỗng
        Public Shared ReadOnly TCS_HQ247_HQ_HS_KHONG_HOPLE As Decimal = TCS_HQ247_HQ + 21 'Lỗi không tìm thấy hồ sơ hoặc trạng thái hồ sơ không hợp lệ
        'huynt end
        Public Shared ReadOnly TCS_HQ247_MSG213_TITLE As String = "Thông điệp NHTM thông báo kết quả xử lý yêu cầu của NNT"
        'huynt
        Public Shared ReadOnly TCS_HQ247_MSG312_TITLE As String = "Thông điệp NHTM thông báo cho TCHQ về việc NNT đã ký ủy quyền"
        Public Shared ReadOnly TCS_HQ247_MSG313_TITLE As String = "Thông điệp TCHQ thông báo cho NHTM về việc đã nhận được thông tin đăng ký ủy quyền của DN tại NHTM"
        'huynt end
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_MOINHAN As String = "01"
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_MAKER213 As String = "02"
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_CHECKER213 As String = "03"
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_HQ_RESPONSE213 As String = "04"
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_MAKER213_TUCHOI As String = "05"
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_CHECKER213_TUCHOI As String = "06"
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_HQ_RESPONSE213_TUCHOI As String = "07"
        'HQ247 huynt begin
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_MAKER312 As String = "09" 'Chờ kiểm soát
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA As String = "10" 'Chuyển trả
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_CHECKER312 As String = "11" 'Đã kiểm soát - Chờ xác nhận của TCHQ
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM As String = "12" 'Đã kiểm soát - chờ bổ sung TT của NNT
        'HQ247 huynt end
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_ACTIVE As String = "13" 'Active: ready, đang hoạt động, đã được nộp thuế điện tử
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_HOLD As String = "14" 'Điều chỉnh
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_NGUNG_DANGKY As String = "15" 'Ngừng đăng ký
        Public Shared ReadOnly TCS_HQ247_HS_TRANGTHAI_TAMKHOA_CHODIEUCHINH As String = "16" 'Ngừng đăng ký
        'HQ247 TRANG THAI MSG304
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_MOINHAN As String = "01"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_MAKER213 As String = "02"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_CHEKER213 As String = "03"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI As String = "05"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_CHEKER213_TUCHOI As String = "06"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI As String = "07"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_CHUYENTRA As String = "08"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_MAKER301_ERROR As String = "09"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_SEND301_ERROR As String = "10"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_CHO_CORE_RESPONSE As String = "11"     'vinhvv new
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_304_SEND301_OK As String = "04"
        '------------------
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_MOINHAN As String = "01"
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_MAKER213 As String = "02"
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_CHECKER213 As String = "03"
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213 As String = "04"
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_MAKER213_TUCHOI As String = "05"
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_CHECKER213_TUCHOI As String = "06"
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_HQ_RESPONSE213_TUCHOI As String = "07"
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_CHUYENTRA As String = "08"
        'HQ247 huynt begin
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_MAKER312 As String = "09" 'Chờ kiểm soát
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_CHECKER312_CHUYENTRA As String = "10" 'Chuyển trả
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_CHECKER312 As String = "11" 'Đã kiểm soát - Chờ xác nhận của TCHQ
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_CHECKER312_DK_MOI_TAI_NHTM As String = "12" 'Đã kiểm soát - chờ bổ sung TT của NNT
        'HQ247 huynt end
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_ACTIVE As String = "13" 'Active: ready, đang hoạt động, đã được nộp thuế điện tử
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_HOLD As String = "14" 'Điều chỉnh
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_NGUNG_DANGKY As String = "15" 'Ngừng đăng ký
        Public Shared ReadOnly TCS_HQ247_314_TRANGTHAI_TAMKHOA_CHODIEUCHINH As String = "16" 'Ngừng đăng ký

        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_MOINHAN As String = "01"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_MAKER213 As String = "02"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_CHEKER213 As String = "03"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_MAKER213_TUCHOI As String = "05"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_CHEKER213_TUCHOI As String = "06"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_CHEKER213_RESPONSE_TUCHOI As String = "07"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_CHUYENTRA As String = "08"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_MAKER301_ERROR As String = "09"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_SEND301_ERROR As String = "10"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_CHO_CORE_RESPONSE As String = "11"
        Public Shared ReadOnly TCS_HQ247_TRANGTHAI_201_SEND301_OK As String = "04"
        'hoang lv 20200312
        Public Shared ReadOnly TCS_HQ247_HQ_EXCEPTION As Decimal = 1001
        Public Shared ReadOnly TCS_HQ247_HQ_SENDED213_FAIl_STATUS_UPDATE As Decimal = 1002
#End Region
#Region "--- Enum Phần Chứng Từ ---"

        Public Enum NguonNNT As Byte
            Khac = 0
            NNThue_CTN = 1
            NNThue_VL = 2
            TruocBa = 3
            HaiQuan = 4
            SoThue_CTN = 5
            VangLai = 6
            Loi = 7
        End Enum

        'Biến Enum eRowHeader để lưu thứ tự Header của chứng từ
        Public Enum eRowHeader As Byte
            NgayLV = 0
            SoCT = 1
            MaNNT = 2
            TenNNT = 2
            DChiNNT = 3
            MaNNTien = 4
            TenNNTien = 4
            DChiNNTien = 5
            MaDBHC = 6
            TenDBHC = 6
            TKNo = 7
            TKCo = 8
            MaCQThu = 9
            TenCQThu = 9
            LoaiThue = 10
            MaNT = 11
            TiGia = 12
            ToKhaiSo = 13
            NgayDK = 14
            LHXNK = 15
            SoQD = 16
            NgayBHQD = 17
            CQBH = 18
            MaDVSDNS = 19
            SoKhung = 20
            SoMay = 21
            SoCTNH = 22
            TK_KH_NH = 23
            NGAY_KH_NH = 24
            MA_NH_A = 25
            MA_NH_B = 26
            TKKHNhan = 27
            TenKHNhan = 28
            DCKHNhan = 29
            So_BK = 30
            Ngay_BK = 31
        End Enum

        'Biến Enum eColDTL để lưu thứ tự Detail của chứng từ
        Public Enum eColDTL As Byte
            MaQuy = 0
            CChuong = 1
            LKhoan = 2
            MTieuMuc = 3
            TLDT = 4
            NoiDung = 5
            TienNT = 6
            Tien = 7
            KyThue = 8
            MaDP = 9
            ID = 10
        End Enum

        Public Enum CTEnum As Byte
            TienMat
            CK_NganHang
            CK_KhoBac
        End Enum


#End Region

#Region "Enum"
        Public Enum TransactionHQ_Type As Byte
            M41 = 1 'Gui CTU
            M51 = 2 'Huy CTU
            M81 = 3 'Gui Bao lanh
            M91 = 4 'Huy Bao lanh
            M45 = 5 'Huy Bao lanh
            M47 = 6 'Huy Bao lanh
            M49 = 7 'Huy Bao lanh
        End Enum

        Public Enum LOV_Type As Byte
            CapChuong = 1
            LoaiKhoan = 2
            MucTMuc = 3
            MaDVThu = 4
            MaXa = 5
            MaHuyen = 6
            MaCQThu = 7
            MaTLDT = 8
            MaCQThue = 9
            MaDTNT = 10
            MaDTNT_KB = 11
            MaDBHC = 12
        End Enum
        Public Enum Report_Type As Byte
            'Danh mục hệ thống
            DM_NNT
            DM_CAPCHUONG
            DM_LOAIKHOAN
            DM_MUCTMUC
            DM_CQQD
            DM_CQTHU
            DM_CQTHUE
            DM_LOAITIEN
            DM_LOAITHUE
            DM_NGUYENTE
            DM_TYGIA
            DM_TAIKHOAN
            DM_DBHC
            DM_LHTHU
            DM_NGANHANG
            DM_KHOBAC
            DM_TLDT
            DM_DIEMTHU
            DM_MAQUY

            'Chi tiết lỗi
            ERR_TDTT
            ERR_TDTT_09

            'Phiếu kế toán
            PHIEUKT_TKCO
            PHIEUKT_TKNO
            PHIEUKT_DBTHU

            'Sổ thu tiền mặt
            SOTHUTM

            'Kho quỹ
            KQ_SOTHU_TIENMAT
            KQ_SOQUY_TIENMAT
            KQ_BKE_TIENMAT

            'Bảng kê chứng từ
            CTU_CQTHU
            CTU_CHITIET
            CTU_SOCT
            CTU_TTSOCT
            CTU_MANNT
            CTU_MLNS
            CTU_TKNO
            CTU_TKCO
            CTU_CQTTONG
            CTU_CQTCHITIET
            CTU_DBTONG
            CTU_DBCHITIET
            CTU_TLDT
            CTU_LOAITHUE
            CTU_HUY
            CTU_XACNHAN
            CTU_BKE_CQTHU
            CTU_BKE
            CTU_BKE_TB
            CTU_PHUCHOI_CK
            CTU_PHUCHOI_TM
            CTU_LIETKE
            CTU_MAU10
            CTU_MAU10_1068
            CTU_MAU10_1068_tong

            TCS_CTU_TC      'Tra cứu chứng từ

            TCS_BLT_TC      'Tra cứu biên lai

            TCS_CTU_LASER   'In chứng từ Laser

            'Tổng hợp Giấy nộp tiền từ Biên Lai
            GNT_BLT
            TCS_BKBLT_CT
            TCS_BK_DVQD
            TCS_THOP_TPHAT
            TCS_BK_PLTM
            TCS_TC_STTHU

            'Bảng kê chứng từ
            CTU_CQTHU_THUE
            CTU_CQTHU_HAIQUAN

            ' Tien gia
            TCS_BBTG
            ' Tình hình tiền giả
            TCS_TinhHinhTG
            ' theo dõi xuất nhập tiền giả
            TCS_XuatNhapTG
            ' Theo dõi thu giữ tiền giả
            TCS_TheoDoiTG
            TCS_BLTHU

            'Bảng kê chứng từ sửa theo COA
            TCS_BK_CTU_COA

            CTU_TRONGGIO 'Chung tu trong h
            CTU_NGOAIGIO 'Chung tu ngoai h
            CTU_ThuHo
            'bao cao doi chieu
            BKE_DOICHIEU
            TCS_BC_TONG
            TCS_BC_NHCD
            TCS_BC_TONG_CT
            TCS_BC_NHCD_CT

            DOICHIEU_1
            DOICHIEU_2
            DOICHIEU_3
            DOICHIEU_4
            DOICHIEU_5
            DOICHIEU_6
            DOICHIEU_7
            DOICHIEU_8

            Thu_Bao_Lanh

            '********************************************'
            'Người viết: ANHLD
            'Ngày viết: 27.09.2012
            'Mục đích: Thêm 2 báo cáo chi tiết bảo lãnh số dư và doanh số

            TCS_BC_BL_SODU
            TCS_BC_BL_DOANHSO

            TCS_BC_TH_BL_SODU
            TCS_BC_TH_BL_DOANHSO
            '********************************************'
            TCS_BC_DC_02BKDC
            CTU_TRONGGIO_NEW
            CTU_BCTHUE_PGB
            DOICHIEU_9
            DOICHIEU_10
            DOICHIEU_11

            KetQua_DoiChieu
            KetQua_DoiChieu_Huy
            TCS_BC_DC_ETAX_FCC_ND
            TCS_BC_DC_ETAX_FCC_XNK
            TCS_DC_LENH_CITAD
            BC_THOIGIANXULYGD
            'DC_TONGHOP_PBBN
            'DC_CHITIET_PBBN

        End Enum
        Public Enum Data_Type As Byte
            DM_DTNT = 1
            SO_THUE = 2
            DM_DBTHU = 3
        End Enum

#End Region

#Region "Hệ thống"
        'ICB-15/10/2008
        'AnhHV4
        '/// Xét xem điểm làm việc có phải là tỉnh hoặc Tp ko?
        '/// Nếu là Tỉnh (TP) thì mã DBHC kết thúc = "TTT"
        'Public Shared Function IsTinh_TP() As Boolean
        '    If mdlSystemVariables.gstrMaDBHC.IndexOf("TTT") <> -1 Then
        '        Return True
        '    End If
        '    Return False
        'End Function

        'ICB-15/10/2008
        'AnhHV4
        '/// lấy giá trị trong bảng tham số
        Public Shared Function ParmValues_Load(ByVal strParmName As String, ByVal str_MaDthu As String) As String
            Dim strSQL As String
            Dim strResult As String = ""
            Try
                strSQL = "SELECT a.giatri_ts  FROM tcs_thamso a  where a.ten_ts ='" & strParmName & "' and a.ma_dthu='" & str_MaDthu & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If (Not ds Is Nothing) And (ds.Tables(0).Rows.Count > 0) Then
                    strResult = Convert.ToString(ds.Tables(0).Rows(0)(0))
                End If
            Catch ex As Exception

            End Try
            Return strResult
        End Function
        'ICB-15/10/2008
        'AnhHV4
        '/// lấy mô tả trong bảng tham số
        Public Shared Function ParmDescript_Load(ByVal strParmName As String) As String
            Dim strSQL As String

            Dim strResult As String = ""
            Try

                strSQL = "SELECT a.MO_TA  FROM tcs_thamso a  where a.ten_ts ='" & strParmName & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If (Not ds Is Nothing) And (ds.Tables(0).Rows.Count > 0) Then
                    strResult = Convert.ToString(ds.Tables(0).Rows(0)(0))
                End If
            Catch ex As Exception
            Finally

            End Try
            Return strResult
        End Function

        'ICB: UnLock NV
        'Hoàng Văn Anh
        Public Shared Function UnLock_NV(ByVal strMaNV As String)

            Try

                Dim str As String = "Update tcs_dm_nhanvien Set tinh_trang='0' Where ma_nv=" & CInt(strMaNV)
                DataAccess.ExecuteNonQuery(str, CommandType.Text)
            Catch ex As Exception
                ''LogDebug.Writelog("Lỗi trong quá trình kích hoạt nhân viên ex:" & ex.ToString())
            Finally

            End Try
        End Function
        'ICB: kiểm tra tình trạng của NV xem có bị lock hay không
        'Hoàng Văn Anh
        Public Shared Function Get_TrangThaiNV(ByVal strMaNV As String) As String
            Dim KetNoi As DataAccess
            Dim strResult As String
            Try
                KetNoi = New DataAccess
                Dim str As String = "SELECT a.tinh_trang  FROM tcs_dm_nhanvien a Where a.ma_nv=" & CInt(strMaNV)
                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(str, CommandType.Text)
                If Not IsEmptyDataSet(ds) Then
                    strResult = CStr(ds.Tables(0).Rows(0)(0)).Trim()
                End If
            Catch ex As Exception
                strResult = "1"
                'LogDebug.Writelog("Lỗi trong việc lấy trạng thái của NV ex:" & ex.ToString())
            Finally
                If Not KetNoi Is Nothing Then
                    KetNoi.Dispose()
                    KetNoi = Nothing
                End If
            End Try

            Return strResult
        End Function
        'ICB: Lấy bàn thu của 1 NV 
        'Hoàng Văn Anh
        Public Shared Function Get_BanLV_NV(ByVal iMaNV As Integer) As Integer
            Dim KetNoi As DataAccess
            Dim iBanLV As Integer
            Try
                KetNoi = New DataAccess
                Dim strSQL = "SELECT a.ban_lv " _
                    & " FROM tcs_dm_nhanvien a " _
                    & " WHERE a.ma_nv=" & iMaNV
                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not IsEmptyDataSet(ds) Then
                    iBanLV = CInt(ds.Tables(0).Rows(0)(0))
                Else
                    iBanLV = 1
                End If
            Catch ex As Exception
                iBanLV = 0
                MsgBox("Lỗi trong quá trình select bàn làm việc của NV!", MsgBoxStyle.Critical, "Chú ý")
                'LogDebug.Writelog("Lỗi trong quá trình select bàn làm việc của NV ! - " & ex.ToString)
            Finally
                If Not KetNoi Is Nothing Then
                    KetNoi.Dispose()
                    KetNoi = Nothing
                End If
            End Try
            Return iBanLV
        End Function
        Public Shared Sub TCS_QuyenMacDinh()
            '-----------------------------------------------------
            ' Mục đích: Khởi tạo các quyền mặc định khi lần đầu tiên
            '           đăng nhập hoặc thoát khỏi hệ thống
            ' Tham số: 
            ' Giá trị trả về: gintSo_CN, garrTenMenu,garrTenButton
            ' Ngày viết: 13/10/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            mdlSystemVariables.gintSoCN = 3  '= so phan tu mang+1
            ReDim mdlSystemVariables.garrTenMenu(mdlSystemVariables.gintSoCN - 1)
            ReDim mdlSystemVariables.garrTenButton(mdlSystemVariables.gintSoCN - 1)
            ' Danh sách các menu chức năng không được phép sử dụng
            ' He thong
            mdlSystemVariables.garrTenMenu(0) = "&a. Đăng nhập vào hệ thống"
            mdlSystemVariables.garrTenMenu(1) = "&a. Giới thiệu"
            mdlSystemVariables.garrTenMenu(2) = "&j. Kết thúc"
            ' Danh sách các button chức năng không được phép sử dụng
            mdlSystemVariables.garrTenButton(0) = "Login"
            mdlSystemVariables.garrTenButton(1) = ""
            mdlSystemVariables.garrTenButton(2) = "KT"
        End Sub
        'Public Shared Function TCS_KT_DangNhap() As Byte
        '    '-----------------------------------------------------
        '    ' Mục đích: Kiểm tra xem các bàn khác đã LogOut hết chưa
        '    '           Nếu chưa LogOut hết thì không được: Khoá sổ,
        '    '           đổi ngày làm việc, các thao tác tiện ích khác
        '    ' Tham số: 
        '    ' Giá trị trả về:   - True  Cho phép  Khoá sổ,đổi ngày làm việc,
        '    '                           các thao tác tiện ích khác
        '    '                   - False Không cho phép làm các thao tác trên
        '    '                           và phải nhắc nhở tất cả các máy khác
        '    ' Ngày viết: 23/10/2003
        '    ' Người viết: Nguyễn Quốc Việt
        '    ' Người sửa: Nguyễn Bá Cường (sửa tên trường,theo chuẩn lập trình)

        '    ' Người sửa: Hoàng Văn Anh - 30/12/2008
        '    ' Mục đích: Sửa theo COA bên ngân hàng
        '    ' ----------------------------------------------------
        '    Dim cnQDangnhap As DataAccess
        '    Dim drDangnhap As IDataReader
        '    Dim strSQL As String
        '    Dim bytResult As Byte

        '    Try
        '        cnQDangnhap = New DataAccess
        '        strSQL = "Select Count(BAN_LV) As MOTBANLV " & _
        '                 "From TCS_DM_NHANVIEN " & _
        '                 "Where LOG_ON ='1' AND MA_NV<>" & CInt(mdlSystemVariables.gstrTenND)
        '        drDangnhap = cnQDangnhap.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drDangnhap.Read Then
        '            If drDangnhap.GetValue(0) = 0 Then
        '                bytResult = 1
        '            Else
        '                bytResult = CByte(drDangnhap.GetValue(0))
        '            End If
        '        End If
        '    Catch ex As Exception
        '        MsgBox("Lỗi trong quá trình kiểm tra đăng nhập hệ thống !", MsgBoxStyle.Critical, "Chú ý")
        '        'LogDebug.Writelog("Lỗi trong quá trình kiểm tra đăng nhập hệ thống ! - " & ex.ToString)
        '        ' Lỗi
        '        bytResult = 0
        '    Finally
        '        ' Giải phóng Connection tới CSDL
        '        If Not drDangnhap Is Nothing Then
        '            drDangnhap.Close()
        '        End If
        '        If Not cnQDangnhap Is Nothing Then
        '            cnQDangnhap.Dispose()
        '        End If
        '    End Try
        '    ' Trả lại kết quả cho hàm
        '    Return bytResult
        'End Function
        Public Shared Function TCS_FREE_BLV(ByVal strTenND As String) As Boolean
            '-------------------------------------------------------------------
            ' Mục đích: giải phóng bàn làm việc cho tất cả các máy (trừ máy đang gp)
            '-------------------------------------------------------------------
            Dim cnND As DataAccess
            Dim strSql As String
            Dim blnResult As Boolean = False
            Try
                cnND = New DataAccess
                strSql = "Update TCS_DM_NHANVIEN Set LOG_ON ='0' Where Ma_NV <> " & strTenND
                cnND.ExecuteNonQuery(strSql, CommandType.Text)
                blnResult = True
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình giải phóng bàn làm việc: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnND Is Nothing Then cnND.Dispose()
            End Try
            Return blnResult
        End Function

        Public Shared Function TCS_KT_NSD_DN(ByVal fstrTenND As String) As Boolean
            '    -----------------------------------------------------
            '     Mục đích: Kiểm tra xem NSD này còn làm việc không? các bàn khác đã LogOut hết chưa
            '               Nếu chưa LogOut thì QTHT không được phép thay đổi quyền, xoá, đổi tên NSD này
            'Tham(số)
            '     Giá trị trả về:   - True  Cho phép  Khoá sổ,đổi ngày làm việc,
            '                               các thao tác tiện ích khác
            '                       - False Không cho phép làm các thao tác trên
            '                               và phải nhắc nhở tất cả các máy khác
            '     Ngày viết: 16/10/2007
            '     Người viết: Nguyen Ba Cuong
            '     ----------------------------------------------------
            Dim Conn As DataAccess
            Dim drData As IDataReader
            Dim blnResult As Boolean
            Dim strSQL As String

            strSQL = "Select LOG_ON From TCS_DM_NHANVIEN " & _
                     "Where LOG_ON <>'0' AND Ma_NV=" & fstrTenND
            Try
                Conn = New DataAccess

                drData = Conn.ExecuteDataReader(strSQL, CommandType.Text)
                If drData.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                blnResult = True
            Finally
                If Not drData Is Nothing Then
                    drData.Dispose()
                End If
                If Not Conn Is Nothing Then
                    Conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        'Public Shared Function Get_HoTenNV(ByVal MaNV As String) As String
        '    '-----------------------------------------------------
        '    ' Mục đích: Lay ten nhan vien
        '    ' Tham số: 
        '    ' Giá trị trả về:   Ten nhan vien
        '    ' Ngày viết: 16/10/2007
        '    ' Người viết: Nguyen Ba Cuong
        '    ' ----------------------------------------------------
        '    Dim cnNV As DataAccess
        '    Dim drNV As IDataReader
        '    Dim strSql As String
        '    Dim strResult As String = ""
        '    Try
        '        cnNV = New DataAccess
        '        strSql = "Select TEN From TCS_DM_NHANVIEN Where MA_NV=" & MaNV
        '        drNV = cnNV.ExecuteDataReader(strSql, CommandType.Text)
        '        If drNV.Read Then
        '            strResult = drNV.GetString(0)
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tên người dùng: " + ex.ToString)
        '    Finally
        '        If Not drNV Is Nothing Then
        '            drNV.Dispose()
        '        End If
        '        If Not cnNV Is Nothing Then
        '            cnNV.Dispose()
        '        End If
        '    End Try
        '    Return strResult
        'End Function
        Public Shared Sub TCS_KT_BoDKBanThu(ByVal fstrCurrUser As String, ByVal fstrPass As String, ByVal CurrUser As Boolean)
            '-----------------------------------------------------
            ' Mục đích: Kế toán thoát khỏi hệ thống hoặc chương trình
            '           mã bàn thu sẽ được trả lại tự do
            ' Tham số: 
            ' Giá trị trả về: 
            ' Ngày viết: 16/10/2007
            ' Người viết: Nguyen Ba Cuong
            ' ----------------------------------------------------
            Dim Conn As DataAccess
            Dim strSQL As String

            If fstrCurrUser = "" Then
                Exit Sub
            End If
            If CurrUser Then
                strSQL = "Update TCS_DM_NHANVIEN " & _
                        "Set LOG_ON ='0',local_ip = ''  " & _
                        "Where MA_NV =" & fstrCurrUser & _
                        " And Mat_Khau='" & fstrPass & "'"
            Else
                strSQL = "Update TCS_DM_NHANVIEN " & _
                                    "Set  LOG_ON ='0',local_ip = ''  " & _
                                    "Where MA_NV =" & fstrCurrUser
            End If
            Try
                Conn = New DataAccess
                Conn.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Cập nhật dữ liệu")
            Finally
                If Not Conn Is Nothing Then
                    Conn.Dispose()
                End If
            End Try
        End Sub
        Public Shared Sub Update_User_MaNhom(ByVal pv_UserName As String, ByVal pv_Ma_NHOM As String)
            Dim Conn As DataAccess
            Dim strSQL As String
            strSQL = "UPDATE TCS_DM_NHANVIEN SET MA_NHOM = '" & pv_Ma_NHOM & "'" & _
                    " WHERE TEN_DN = '" & pv_UserName & "' "
            Try
                Conn = New DataAccess
                Conn.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Cập nhật dữ liệu")
            Finally
                If Not Conn Is Nothing Then
                    Conn.Dispose()
                End If
            End Try
        End Sub
        Public Shared Sub Update_User_FullName(ByVal pv_UserName As String, ByVal pv_Ma_CN As String, ByVal pv_TEN_DN As String)
            Dim Conn As DataAccess
            Dim strSQL As String
            strSQL = "UPDATE TCS_DM_NHANVIEN SET MA_CN = '" & pv_Ma_CN & "', TEN = '" & pv_TEN_DN & "'" & _
                    " WHERE TEN_DN = '" & pv_UserName & "' "
            Try
                Conn = New DataAccess
                Conn.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Cập nhật dữ liệu")
            Finally
                If Not Conn Is Nothing Then
                    Conn.Dispose()
                End If
            End Try
        End Sub
        Public Shared Sub Insert_User_FullName(ByVal pv_UserName As String, ByVal pv_Ma_CN As String, ByVal pv_TEN_DN As String)
            Dim Conn As DataAccess
            Dim strSQL As String
            strSQL = "INSERT INTO TCS_DM_NHANVIEN(TEN_DN,MA_NV,MA_NHOM,TEN,MA_CN,teller_id) " & _
                    " VALUES('" & pv_UserName & "', TCS_NHANVIEN_ID_SEQ.NEXTVAL,'02','" & pv_TEN_DN & "'," & _
                    "  '" & pv_Ma_CN & "','" & pv_UserName & "') "
            Try
                Conn = New DataAccess
                Conn.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Cập nhật dữ liệu")
            Finally
                If Not Conn Is Nothing Then
                    Conn.Dispose()
                End If
            End Try
        End Sub
        Public Shared Sub Insert_User_FullName_Log(ByVal pv_UserName As String, ByVal pv_Ma_CN As String, ByVal pv_TEN_DN As String)
            Dim Conn As DataAccess
            Dim strSQL As String
            strSQL = "INSERT INTO TCS_LOG_DM_NHANVIEN(TEN_DN,TEN_DN,MA_CN) " & _
                    " VALUES('" & pv_UserName & "','" & pv_TEN_DN & "', " & _
                    "  '" & pv_Ma_CN & "') "
            Try
                Conn = New DataAccess
                Conn.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                MsgBox(ex.ToString, MsgBoxStyle.Critical, "Cập nhật dữ liệu")
            Finally
                If Not Conn Is Nothing Then
                    Conn.Dispose()
                End If
            End Try
        End Sub
        Public Shared Function TestConnection() As Boolean
            '-----------------------------------------------------------------
            ' Mục đích: Kiểm tra đường dẫn đến CSDL NSD chọn có hợp lệ không.
            ' Tham số: 
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 23/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' Người sửa: Nguyễn Quốc Việt
            ' Ngày sửa: 20/08/2004
            ' ----------------------------------------------------------------
            Dim cnTest As New DataAccess
            Dim blnConn As Boolean
            blnConn = cnTest.CheckConnect
            Return blnConn
        End Function

#End Region

#Region "Xử lý số --> Chữ, Date"
        Public Shared Function ToValue(ByVal value As String, ByVal typefield As String _
                                , Optional ByVal strFormat As String = "") As String
            Dim tmp As String = ""
            'convert to from Unicode to TCVN before insert DB
            If (IsDBNull(value)) Then
                value = "' '"
                Return value
            End If
            value = value.Trim
            If UCase(Trim(typefield)) = "NUMBER" Then
                If value <> "" Then
                    tmp = value.Replace(",", ".")
                    'Neu dau vao cho Value la Nothing thi ham tra ve chuoi "null"
                Else
                    tmp = "null"
                End If
            ElseIf UCase(Trim(typefield)) = "DATE" Then
                If value <> "" Then
                    If strFormat = "" Then
                        strFormat = "dd/MM/rrrr HH24:MI:ss"
                    End If
                    tmp = "to_date('" & value & "','" & strFormat & "')"
                    'Neu dau vao cho Value la Nothing thi ham tra ve chuoi "null"
                Else
                    tmp = "null"
                End If
            End If

            Return tmp
        End Function
        '*************************************************************
        'S01 Nguoi sua doi: Tran Ngoc Diep
        '   Ngay sua doi:   08/11/2003
        '   Ly do sua:      Sua lai ham trong truong hop dau vao la nothing

        '*************************************************************
        Public Shared Function CorrectFormat(ByVal inString As String) As String
            ' Luu y format co truong hop HH:mm
            Dim outString As String = ""
            Dim i As Integer
            Dim strChar As Char

            For i = 0 To inString.Length - 1
                strChar = inString.Chars(i)
                Select Case strChar
                    Case "m"
                        If i > 0 AndAlso inString.Chars(i - 1) <> "m" AndAlso inString.Chars(i + 1) <> "m" Then
                            strChar = "M"
                        End If
                    Case "Y"
                        strChar = "y"
                    Case "R"
                        strChar = "y"
                    Case "r"
                        strChar = "y"
                    Case "D"
                        strChar = "d"
                End Select
                outString = outString & strChar
            Next
            Return outString
        End Function
        Public Shared Function NtoS(ByVal Number As Double) As String
            '-----------------------------------------------------------------
            ' Mục đích: Đổi 1 số thành 1 chuỗi theo dạng "# ###".
            ' Tham số: Số cần đổi.
            ' Giá trị trả về: Xâu ký tự sau khi đổi.
            ' Ngày viết: 23/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' Ngày sửa: 28/04/2004
            ' Người sủa: Nguyễn Quốc Việt
            ' Mục đích sửa: Bỏ đi dấu phảy thập phân(".")
            ' ----------------------------------------------------------------
            If Number = 0 Then
                Return "0"
            Else
                Dim s As String = Number.ToString("### ### ### ### ### ##0").Trim()

                While s.IndexOf("  ") >= 0
                    s = s.Replace("  ", " ")
                End While

                Return s
            End If
        End Function
        Public Shared Function NtoS2(ByVal Number As Double) As String
            '-----------------------------------------------------------------
            ' Mục đích: Đổi 1 số thành 1 chuỗi theo dạng "# ###".
            ' Tham số: Số cần đổi.
            ' Giá trị trả về: Xâu ký tự sau khi đổi.
            ' Ngày viết: 23/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' Ngày sửa: 28/04/2004
            ' Người sủa: Nguyễn Quốc Việt
            ' Mục đích sửa: Bỏ đi dấu phảy thập phân(".")
            ' ----------------------------------------------------------------
            If Number = 0 Then
                Return "0"
            Else
                Dim s As String = Number.ToString("### ### ### ### ### ##0.00").Trim()

                While s.IndexOf("  ") >= 0
                    s = s.Replace("  ", " ")
                End While

                Return s
            End If
        End Function
        Public Shared Function StoN(ByVal strNum As String) As Double
            '-----------------------------------------------------------------
            ' Mục đích: Đổi 1 xâu số dạng "# ###" thành 1 số double.
            ' Tham số: Xâu cần đổi.
            ' Giá trị trả về: Số double sau khi đổi.
            ' Ngày viết: 23/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------------------
            strNum = Trim(strNum)
            Dim strResult As String = ""
            Dim i As Integer
            Try
                strResult = strNum.Replace(" ", "")
                If strResult <> "" Then
                    Return CDbl(strResult)
                Else
                    Return 0
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function RemoveApostrophe(ByVal strInput As String) As String
            '-----------------------------------------------------------------
            ' Mục đích: Kiểm tra nếu trong 1 xâu có ký tự nháy đơn thì xoá đi.
            ' Tham số: Xâu cần kiểm tra.
            ' Giá trị trả về: Xâu sau khi kiểm tra.
            ' Ngày viết: 04/12/2007
            ' Người viết: Nguyễn Hữu Hoan
            ' ----------------------------------------------------------------
            Dim strResult As String = ""
            Dim i As Integer
            Try
                If Len(strInput) > 0 Then
                    strResult = strInput.Replace("'", "")
                End If
            Catch ex As Exception
            End Try
            Return strResult
        End Function
        Public Shared Function FilterSQLi(ByVal strInput As String) As String
            '-----------------------------------------------------------------
            ' Mục đích: Xóa các ký tự, chuỗi khi đưa giá trị vào câu lệnh SQL.
            ' Tham số: Xâu cần kiểm tra, Chuỗi ký tự
            ' Giá trị trả về: True-False.
            ' Ngày viết: 12/12/2014
            ' Người viết: Nguyễn Kỷ Tỵ
            ' ----------------------------------------------------------------
            Dim strSQLiChar As String
            strSQLiChar = TCS_GETTS("CHAR_SQLI")
            Dim strSQLi() As String
            strSQLi = strSQLiChar.Split(",")
            Dim i As Integer
            Dim i_index As Integer = 0
            Try
                For i = 0 To strSQLi.Length - 1
                    i_index = strInput.LastIndexOf(strSQLi(i).Trim)
                    If i_index > 0 Then
                        strInput = strInput.Replace(strSQLi(i).Trim, "")
                    End If
                Next
            Catch ex As Exception
            End Try
            Return strInput
        End Function
        Public Shared Function TCS_GETTS(ByVal p_strTENTS As String) As String
            Try
                Dim strSQL As String = String.Empty
                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='" & p_strTENTS & "' "
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
            Catch ex As Exception
                'clsCommon.WriteLog(ex, "Lỗi lấy giá trị tham số", HttpContext.Current.Session.Item("TEN_DN"))
                Throw ex
            End Try
        End Function
        Public Shared Function RemoveChar(ByVal strInput As String, ByVal strFromChar As String, ByVal strToChar As String) As String
            'ICB
            '-----------------------------------------------------------------
            ' Mục đích: Thay thế một số ký tự này = 1 số ký tự khác trong 1 xâu
            ' Tham số: Xâu cần kiểm tra.
            ' Giá trị trả về: Xâu sau khi kiểm tra.
            ' Ngày viết: 17/10/2008
            ' Người viết: Hoàng Văn Anh
            ' ----------------------------------------------------------------
            Dim strResult As String = ""
            Dim i As Integer
            Try
                If Len(strInput) > 0 Then
                    strResult = strInput.Replace(strFromChar, strToChar)
                End If
            Catch ex As Exception
            End Try
            Return strResult
        End Function
        Private Shared Function NumberToWord1(ByVal Words_Number1 As String) As String
            Dim Words_result As String, Number123 As String
            Dim Words_Len As Double

            If CDbl(Words_Number1) = 0 Then Exit Function

            Words_Number1 = Trim(Words_Number1)
            Words_Len = Len(Words_Number1)
            If Words_Len > 9 Then Exit Function

            Number123 = Right$(Words_Number1, 3)
            Words_result = NumberToWord2(Number123)

            Words_Len = Words_Len - 3
            If Words_Len <= 0 Then
                NumberToWord1 = Words_result
                Exit Function
            End If
            Words_Number1 = Left$(Words_Number1, Words_Len)
            Number123 = Right$(Words_Number1, 3)
            If CInt(Number123) > 0 Then
                Words_result = NumberToWord2(Number123) & " nghìn" & Words_result
            End If

            Words_Len = Words_Len - 3
            If Words_Len <= 0 Then
                NumberToWord1 = Words_result
                Exit Function
            End If
            Words_Number1 = Left$(Words_Number1, Words_Len)
            Number123 = Right$(Words_Number1, 3)
            If CInt(Number123) > 0 Then
                Words_result = NumberToWord2(Number123) & " triệu" & Words_result
            End If
            NumberToWord1 = Words_result
        End Function
        Private Shared Function NumberToWord2(ByVal Words_Number2 As String) As String
            Dim Hundreds_Name As String, Tens_Name As String, Units_Name As String
            Dim Word_Number1 As String, Word_Number2 As String, Word_Number3 As String
            Dim Words_Len As Long

            If CInt(Words_Number2) = 0 Then Exit Function

            Words_Len = Len(Words_Number2)
            Hundreds_Name = ""
            Tens_Name = ""
            Units_Name = ""

            Select Case Words_Len
                Case 3
                    Word_Number1 = Mid(Words_Number2, 1, 1)
                    Word_Number2 = Mid(Words_Number2, 2, 1)
                    Word_Number3 = Mid(Words_Number2, 3, 1)
                Case 2
                    Word_Number2 = Mid(Words_Number2, 1, 1)
                    Word_Number3 = Mid(Words_Number2, 2, 1)
                Case 1
                    Word_Number3 = Mid(Words_Number2, 1, 1)
            End Select

            If Word_Number1 <> "" Then
                Hundreds_Name = NumberToWord3(Word_Number1) & " trăm"
            End If

            If Word_Number2 <> "" Then
                Select Case Word_Number2
                    Case "0"
                        If Word_Number3 <> "0" And Words_Len > 2 Then
                            Tens_Name = " lẻ"
                        Else
                            Tens_Name = ""
                        End If
                    Case "1"
                        Tens_Name = " mười"
                    Case Else
                        Tens_Name = NumberToWord3(Word_Number2) + " mươi"
                End Select
            End If

            Select Case Word_Number3
                Case "0"
                    Units_Name = ""
                Case "1"
                    If Words_Len = 1 Then
                        Units_Name = " một"
                    Else
                        Units_Name = " mốt"
                    End If
                Case "5"
                    If Word_Number2 <> "0" And Words_Len > 1 Then
                        Units_Name = " lăm"
                    Else
                        Units_Name = " năm"
                    End If
                Case Else
                    Units_Name = NumberToWord3(Word_Number3)
            End Select
            NumberToWord2 = Hundreds_Name & Tens_Name & Units_Name
        End Function
        Private Shared Function NumberToWord3(ByVal Word_Number As String) As String
            Select Case Word_Number
                Case "0"
                    NumberToWord3 = " không"
                Case "1"
                    NumberToWord3 = " một"
                Case "2"
                    NumberToWord3 = " hai"
                Case "3"
                    NumberToWord3 = " ba"
                Case "4"
                    NumberToWord3 = " bốn"
                Case "5"
                    NumberToWord3 = " năm"
                Case "6"
                    NumberToWord3 = " sáu"
                Case "7"
                    NumberToWord3 = " bảy"
                Case "8"
                    NumberToWord3 = " tám"
                Case "9"
                    NumberToWord3 = " chín"
            End Select
        End Function
        Public Shared Function TCS_Dich_So(ByVal Number As Double, ByVal Unit_Name As String) As String
            '-----------------------------------------------------
            ' Mục đích: Dịch một số thành một chuỗi dịch
            ' Tham số: Number
            ' Giá trị trả về: TCS_Dich_So
            ' Ngày viết: 15/10/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            ' Mục đích sửa: "mười mốt" --> "mười một"
            ' Ngày viết: 17/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim Words_Dec As String, Number9 As String, Words_Number As String
            Dim Word_Sign As String, Words_result As String
            Dim Words_Len As Long, pos As Long
            ' Khai báo động
            'Dim Unit_Name As String = "đồng"
            Dim SubUnit_name As String = ""
            Dim Curr As String = ""
            '

            If Number = 0 Then Exit Function
            Words_Number = Format(Number, "##0.00")

            If Number < 0 Then
                Word_Sign = "Trừ "
                Words_Number = Format(Math.Abs(Number), "##0.00")
            End If

            If InStr(Words_Number, ".") = 0 Then ' Phu thuoc vao Regional
                pos = InStr(Words_Number, ",")
            Else
                pos = InStr(Words_Number, ".")
            End If
            Words_Dec = Mid(Words_Number, pos + 1)
            Words_Number = Left(Words_Number, pos - 1)
            Words_Len = Len(Words_Number)

            Number9 = Right(Words_Number, 9)
            Words_result = NumberToWord1(Number9)

            Words_Len = Words_Len - 9
            Do While Words_Len > 0
                Words_Number = Left$(Words_Number, Words_Len)
                Number9 = Right$(Words_Number, 9)
                Words_result = NumberToWord1(Number9) & " tỷ" & Words_result
                Words_Len = Words_Len - 9
            Loop

            If CInt(Words_Dec) = 0 Then
                Words_result = Trim(Words_result & " " & Unit_Name & Curr & " chẵn./.")
            Else
                If SubUnit_name <> "" Then
                    Words_result = Trim(Words_result & " " & Unit_Name & " và" & NumberToWord1(Words_Dec) & " " & SubUnit_name & "" & Curr)
                Else
                    Words_result = Trim$(Words_result & " " & Unit_Name & " và " & "0." & Words_Dec & "" & Curr)
                End If
            End If
            Dim s As String = Word_Sign & UCase(Left(Words_result, 1)) & Right(Words_result, Len(Words_result) - 1)

            s = s.Replace("mười mốt", "mười một")
            s = s.Replace("Mười mốt", "Mười một")


            Return s
        End Function
#End Region

#Region "Xử lý MLNS - DM"

#Region "Valid_Other"
        Public Shared Function Valid_KyThue(ByVal strText As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra string có đúng biểu diễn ngày tháng dạng 'MM/yyyy' ?.
            ' Tham số: string đầu vào
            ' Giá trị trả về: 
            ' Ngày viết: 23/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Try
                Dim sep As String = "/"
                If (strText.IndexOf("-") > 0) Then
                    sep = "-"
                ElseIf strText.IndexOf(".") > 0 Then
                    sep = "."
                ElseIf strText.IndexOf("*") > 0 Then
                    sep = "*"
                End If

                strText = strText.Trim()
                Dim i, iLen As Integer
                iLen = Len(strText)

                If (iLen <> 7 Or Mid(strText, 3, 1) <> sep) Then
                    ''MessageBox.Show("Lỗi, không đúng độ dài")
                    Return False
                End If

                Dim strMonth, strYear As String
                strMonth = Mid(strText, 1, 2)
                strYear = Mid(strText, 4, 4)

                If ((Not IsNumeric(strMonth)) Or (Not IsNumeric(strYear))) Then
                    ''MessageBox.Show("Lỗi, Ngày tháng năm không phải kiểu số")
                    Return False
                End If

                Dim iMonth, iYear As Integer
                iMonth = Convert.ToInt16(strMonth)
                iYear = Convert.ToInt16(strYear)

                If ((iMonth < 1) Or (iMonth > 12) Or _
                    (iYear < 1000) Or (iYear > 9999)) Then
                    ''MessageBox.Show("Lỗi, Ngày tháng năm vượt quá khoảng giá trị cho phép")
                    Return False
                End If
            Catch ex As Exception
                ''MessageBox.Show("Lỗi : " & ex.Message)
                Return False
            End Try
            Return True
        End Function
        Public Shared Function Valid_Date(ByVal strText As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra string có đúng biểu diễn ngày tháng dạng 'dd/MM/yyyy' ?.
            ' Tham số: string đầu vào
            ' Giá trị trả về: 
            ' Ngày viết: 16/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Try
                Dim sep As String = "/"
                sep = FindSeprate(strText)
                If "".Equals(sep) Then Return False
                strText = strText.Trim()
                Dim i, iLen As Integer
                iLen = Len(strText)

                If (iLen <> 10 Or Mid(strText, 3, 1) <> sep Or Mid(strText, 6, 1) <> sep) Then
                    Return False
                End If

                Dim strDate, strMonth, strYear As String
                strDate = Mid(strText, 1, 2)
                strMonth = Mid(strText, 4, 2)
                strYear = Mid(strText, 7, 4)

                If ((Not IsNumeric(strDate)) Or (Not IsNumeric(strMonth)) Or (Not IsNumeric(strYear))) Then
                    Return False
                End If

                Dim iDate, iMonth, iYear As Integer
                iDate = CInt(strDate)
                iMonth = CInt(strMonth)
                iYear = CInt(strYear)

                If (iDate <> 32) Then
                    Dim d As New Date(iYear, iMonth, iDate)
                Else
                    If ((iDate < 1) Or (iDate > 32) Or _
                        (iMonth < 1) Or (iMonth > 12) Or _
                        (iYear < 1000) Or (iYear > 9999)) Then
                        Return False
                    End If
                End If

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Shared Function Valid_DateExact(ByVal strText As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra string có đúng biểu diễn ngày tháng dạng 'dd/MM/yyyy' ?.
            ' Tham số: string đầu vào
            ' Giá trị trả về: 
            ' Ngày viết: 16/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Try
                Dim sep As String = "/"
                If (strText.IndexOf("-") > 0) Then
                    sep = "-"
                ElseIf strText.IndexOf(".") > 0 Then
                    sep = "."
                ElseIf strText.IndexOf("*") > 0 Then
                    sep = "*"
                End If

                strText = strText.Trim()
                Dim i, iLen As Integer
                iLen = Len(strText)

                If (iLen <> 10 Or Mid(strText, 3, 1) <> sep Or Mid(strText, 6, 1) <> sep) Then
                    Return False
                End If

                Dim strDate, strMonth, strYear As String
                strDate = Mid(strText, 1, 2)
                strMonth = Mid(strText, 4, 2)
                strYear = Mid(strText, 7, 4)

                If ((Not IsNumeric(strDate)) Or (Not IsNumeric(strMonth)) Or (Not IsNumeric(strYear))) Then
                    Return False
                End If


                Dim iDate, iMonth, iYear As Integer
                iDate = Convert.ToInt16(strDate)
                iMonth = Convert.ToInt16(strMonth)
                iYear = Convert.ToInt16(strYear)

                Dim d As New Date(iYear, iMonth, iDate)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
        'Public Shared Function Valid_DiaBan(ByVal strDiaBan As String) As Boolean
        '    '-----------------------------------------------------
        '    ' Mục đích: Kiểm tra Mã địa bàn có hợp lệ
        '    ' Tham số: Mã địa bàn
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 08/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim cnDiaBan As DataAccess
        '    Try
        '        If (IsNothing(strDiaBan)) Then Return False
        '        Dim strValue() As String = strDiaBan.Split(".")
        '        If (strValue.Length <> 3) Then Return False

        '        cnDiaBan = New DataAccess
        '        Dim strSQL As String
        '        strSQL = "Select 1 From TCS_DM_XA " & _
        '                "Where (MA_TINH = '" & strValue(0) & "') " & _
        '                "and (MA_HUYEN = '" & strValue(1) & "') " & _
        '                "and (MA_XA = '" & strValue(2) & "')"

        '        Dim blnReturn As Boolean = False

        '        Dim drDiaBan As IDataReader = cnDiaBan.ExecuteDataReader(strSQL, CommandType.Text)
        '        If (drDiaBan.Read()) Then
        '            blnReturn = True
        '        End If
        '        drDiaBan.Close()

        '        If (Not IsNothing(drDiaBan)) Then drDiaBan.Dispose()
        '        Return blnReturn
        '    Catch ex As Exception
        '        Return False
        '    Finally
        '        If (Not IsNothing(cnDiaBan)) Then cnDiaBan.Dispose()
        '    End Try
        'End Function
        Public Shared Function Valid_TaiKhoan(ByVal strTK As String, Optional ByVal blnDangHoatDong As Boolean = False) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra Tài khoản có hợp lệ
            ' Tham số: Tài Khoản
            ' Giá trị trả về: 
            ' Ngày viết: 08/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnTK As DataAccess
            Try
                If (IsNothing(strTK)) Then Return False

                strTK = strTK.Replace(".", "")

                Dim strWhereTT As String = ""
                If (blnDangHoatDong) Then
                    strWhereTT = " and (tinh_trang = '1')"
                End If

                cnTK = New DataAccess
                Dim strSQL As String

                strSQL = "Select 1 From TCS_DM_TAIKHOAN " & _
                        "Where (TK = '" & strTK & "') " & strWhereTT

                Dim blnReturn As Boolean = False

                Dim drTK As IDataReader = cnTK.ExecuteDataReader(strSQL, CommandType.Text)
                If (drTK.Read()) Then
                    blnReturn = True
                End If
                drTK.Close()

                If (Not IsNothing(drTK)) Then drTK.Dispose()
                Return blnReturn
            Catch ex As Exception
                Return False
            Finally
                If (Not IsNothing(cnTK)) Then cnTK.Dispose()
            End Try
        End Function
        ''Public Shared Function Valid_TaiKhoan(ByVal strTK As String) As Boolean
        '    '-----------------------------------------------------
        '    ' Mục đích: Kiểm tra Tài khoản có hợp lệ
        '    ' Tham số: Tài Khoản
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 08/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim cnTK As DataAccess
        '    Try
        '        If (IsNothing(strTK)) Then Return False

        '        strTK = strTK.Replace(".", "")

        '        cnTK = New DataAccess
        '        Dim strSQL As String
        '        strSQL = "Select 1 From TCS_DM_TAIKHOAN " & _
        '                "Where (TK = '" & strTK & "') "

        '        Dim blnReturn As Boolean = False

        '        Dim drTK As IDataReader = cnTK.ExecuteDataReader(strSQL, CommandType.Text)
        '        If (drTK.Read()) Then
        '            blnReturn = True
        '        End If
        '        drTK.Close()

        '        If (Not IsNothing(drTK)) Then drTK.Dispose()
        '        Return blnReturn
        '    Catch ex As Exception
        '        Return False
        '    Finally
        '        If (Not IsNothing(cnTK)) Then cnTK.Dispose()
        '    End Try
        'End Function
        'Public Shared Function Valid_LoaiThue(ByVal strMa_LThue As String) As Boolean
        '    '-----------------------------------------------------
        '    ' Mục đích: Kiểm tra Mã Loại Thuế có hợp lệ
        '    ' Tham số: Mã Loại Thuế
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 08/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim cnLThue As DataAccess
        '    Try
        '        If (IsNothing(strMa_LThue)) Then Return False

        '        cnLThue = New DataAccess
        '        Dim strSQL As String
        '        strSQL = "Select 1 From TCS_DM_LTHUE " & _
        '                "Where (MA_LTHUE = '" & strMa_LThue & "') "

        '        Dim blnReturn As Boolean = False

        '        Dim drLThue As IDataReader = cnLThue.ExecuteDataReader(strSQL, CommandType.Text)
        '        If (drLThue.Read()) Then
        '            blnReturn = True
        '        End If
        '        drLThue.Close()

        '        If (Not IsNothing(drLThue)) Then drLThue.Dispose()
        '        Return blnReturn
        '    Catch ex As Exception
        '        Return False
        '    Finally
        '        If (Not IsNothing(cnLThue)) Then cnLThue.Dispose()
        '    End Try
        'End Function

        Public Shared Function Valid_LHThu(ByVal strMa_LH As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra Mã Loại hình thu có hợp lệ
            ' Tham số: Mã Loại hình
            ' Giá trị trả về: 
            ' Ngày viết: 12/12/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnLHThu As DataAccess
            Try
                If (IsNothing(strMa_LH)) Then Return False

                cnLHThu = New DataAccess
                Dim strSQL As String
                strSQL = "Select 1 From TCS_DM_LHTHU " & _
                        "Where (MA_LH = '" & strMa_LH & "') "

                Dim blnReturn As Boolean = False

                Dim drLThue As IDataReader = DataAccess.ExecuteDataReader(strSQL, CommandType.Text)
                If (drLThue.Read()) Then
                    blnReturn = True
                End If
                drLThue.Close()

                If (Not IsNothing(drLThue)) Then drLThue.Dispose()
                Return blnReturn
            Catch ex As Exception
                Return False
            Finally
                If (Not IsNothing(cnLHThu)) Then cnLHThu.Dispose()
            End Try
        End Function

        'Public Shared Function Valid_MaCha_DBHC(ByVal strMaDBHC As String, ByVal strMaCha As String) As Boolean
        '    '---------------------------------------------------------------
        '    ' Mục đích: Kiểm tra một mã DBHC có là mã cha của mã DBHC khác không?
        '    ' Tham số: Mã địa bàn hành chính
        '    ' Ngày viết: 26/11/2008
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim cnQHuyen As New DataAccess
        '    Dim drHuyen As IDataReader
        '    Dim strSQL As String
        '    Dim blnResult As Boolean

        '    Try
        '        strSQL = "Select 1 From TCS_DM_XA " & _
        '               "Where (MA_XA = '" & strMaDBHC & "') and (MA_CHA = '" & strMaCha & "')"

        '        drHuyen = cnQHuyen.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drHuyen.Read Then
        '            blnResult = True
        '        Else
        '            blnResult = False
        '        End If
        '        ' Giải phóng Connection tới CSDL
        '        drHuyen.Close()
        '        cnQHuyen.Dispose()
        '    Catch ex As Exception
        '        If Not drHuyen Is Nothing Then
        '            drHuyen.Close()
        '        End If
        '        If Not cnQHuyen Is Nothing Then
        '            cnQHuyen.Dispose()
        '        End If
        '        'LogDebug.Writelog("Lỗi trong quá trình kiểm tra mã cha của mã địa bàn hành chính ! ! - " & ex.ToString)
        '        ' Lỗi
        '        blnResult = False
        '    End Try
        '    ' Trả lại kết quả cho hàm
        '    Return blnResult
        'End Function

        'Public Shared Function Get_MaCha_DBHC(ByVal strMaDBHC As String) As String
        '    '---------------------------------------------------------------
        '    ' Mục đích: Lấy mã cha của mã địa bàn hành chính
        '    ' Tham số: Mã địa bàn hành chính
        '    ' Giá trị trả về: Mã cha
        '    ' Ngày viết: 26/11/2008
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim cnQHuyen As New DataAccess
        '    Dim drHuyen As IDataReader
        '    Dim strSQL As String
        '    Dim strResult As String

        '    Try
        '        strSQL = "Select MA_CHA From TCS_DM_XA " & _
        '               "Where (MA_XA = '" & strMaDBHC & "') "

        '        drHuyen = cnQHuyen.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drHuyen.Read Then
        '            strResult = GetString(drHuyen(0))
        '        Else
        '            strResult = ""
        '        End If
        '        ' Giải phóng Connection tới CSDL
        '        drHuyen.Close()
        '        cnQHuyen.Dispose()
        '    Catch ex As Exception
        '        If Not drHuyen Is Nothing Then
        '            drHuyen.Close()
        '        End If
        '        If Not cnQHuyen Is Nothing Then
        '            cnQHuyen.Dispose()
        '        End If
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy mã cha của mã địa bàn hành chính ! ! - " & ex.ToString)
        '        ' Lỗi
        '        strResult = ""
        '    End Try
        '    ' Trả lại kết quả cho hàm
        '    Return strResult
        'End Function

        'Public Shared Function Valid_NgoaiTe(ByVal strMa_NT As String) As Boolean
        '    '-----------------------------------------------------
        '    ' Mục đích: Kiểm tra Mã Ngoại Tệ có hợp lệ
        '    ' Tham số: Mã Ngoại Tệ
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 08/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim cnNT As DataAccess
        '    Try
        '        If (IsNothing(strMa_NT)) Then Return False

        '        cnNT = New DataAccess
        '        Dim strSQL As String
        '        strSQL = "Select 1 From TCS_DM_NGUYENTE " & _
        '                "Where (MA_NT = '" & strMa_NT.ToUpper() & "')"

        '        Dim blnReturn As Boolean = False

        '        Dim drNT As IDataReader = cnNT.ExecuteDataReader(strSQL, CommandType.Text)
        '        If (drNT.Read()) Then
        '            blnReturn = True
        '        End If
        '        drNT.Close()

        '        If (Not IsNothing(drNT)) Then drNT.Dispose()
        '        Return blnReturn
        '    Catch ex As Exception
        '        Return False
        '    Finally
        '        If (Not IsNothing(cnNT)) Then cnNT.Dispose()
        '    End Try
        'End Function
#End Region

#Region "Get Tên by ID"
        Public Shared Function Get_XaID(ByVal strMaXa As String) As String
            Dim strResult As String = "null"
            Dim conn As DataAccess
            Dim drNT As IDataReader
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Select XA_ID From TCS_DM_XA Where Ma_Xa ='" & strMaXa & "'"
                strResult = conn.ExecuteSQLScalar(strSql)
                'If drNT.Read Then
                '    strResult = drNT("XA_ID").ToString()
                'End If
            Catch ex As Exception
                Throw ex
                'LogDebug.Writelog("Lỗi trong quá trình lấy xã ID: " & ex.ToString)
            Finally
                If Not drNT Is Nothing Then
                    drNT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function Get_MaTenTinh(ByVal strMaXa As String) As DataTable
            'Dim strResult As String = "null"
            Dim conn As DataAccess
            Dim drNT As DataTable
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "SELECT a.xa_id, a.ma_xa, a.ma_huyen, a.ma_tinh, a.ten, a.tinh_trang " & _
               "  FROM tcs_dm_xa a WHERE A.ma_xa=tcs_pck_util.fnc_get_ma_xa_cha ('" & strMaXa & "')"
                drNT = conn.ExecuteToTable(strSql)
                Return drNT
            Catch ex As Exception

                log.Error(ex.Message & "-" & ex.StackTrace) '    LogDebug.WriteLog("Lỗi trong quá trình lấy xã tỉnh: " & ex.ToString, EventLogEntryType.Error)
                Return Nothing
            Finally
                If Not drNT Is Nothing Then
                    drNT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try

        End Function
        Public Shared Function Get_TenTinh(ByVal strMaXa As String) As String
            Dim cnTenMTM As New DataAccess
            Dim drTenMTM As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "Select TEN From TCS_DM_xa where ma_tinh='" & strMaXa & "'"
                drTenMTM = cnTenMTM.ExecuteDataReader(strSql, CommandType.Text)
                If drTenMTM IsNot Nothing And drTenMTM.Read Then
                    strResult = Trim(drTenMTM.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên mục tiểu muc của 1 cặp mã MỤC,TIỂU MỤC bất kỳ: " & ex.ToString)
            Finally
                If Not drTenMTM Is Nothing Then drTenMTM.Close()
                If Not cnTenMTM Is Nothing Then cnTenMTM.Dispose()
            End Try
            Return strResult
        End Function
        Public Shared Function GetString_DBThu_NNT(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As String
            Dim strSQL As String
            Dim strDK_DBHC As String = ""
            Dim strDK_DBHC_Cha As String = ""
            Dim strMaTinhTP As String = ""
            Dim strMaDBHC As String = ""
            Dim arrDBThu As String()
            Dim strDBThu As String = ""
            Dim strMaDiemThu As String
            Try
                strSQL = " SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE upper(b.ma_xa) like upper('___HH') and upper(b.ma_xa) like upper('%" & strMaDanhMuc & "%') and upper(ten) like upper('%" & strTenDanhMuc & "%')"
            Catch ex As Exception
                Throw ex
            End Try
            Return strSQL
        End Function
        Public Shared Function GetString_DBThu_NNTHUE(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As String
            Dim strSQL As String
            Dim strDK_DBHC As String = ""
            Dim strDK_DBHC_Cha As String = ""
            Dim strMaTinhTP As String = ""
            Dim strMaDBHC As String = ""
            Dim arrDBThu As String()
            Dim strDBThu As String = ""
            Dim strMaDiemThu As String
            Try
                strSQL = " SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE upper(b.ma_xa) like upper('___HH') and upper(b.ma_xa) like upper('%" & strMaDanhMuc & "%') and upper(ten) like upper('%" & strTenDanhMuc & "%')"
            Catch ex As Exception
                Throw ex
            End Try
            Return strSQL
        End Function

        Public Shared Function Get_TenQuan(ByVal strMaXa As String) As DataTable
            'Dim strResult As String = "null"
            Dim conn As DataAccess
            Dim drNT As DataTable
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "SELECT a.xa_id, a.ma_xa, a.ma_huyen, a.ma_tinh, a.ten, a.tinh_trang" & _
               "  FROM tcs_dm_xa a WHERE A.ma_xa='" & strMaXa & "'"
                drNT = conn.ExecuteToTable(strSql)
                Return drNT
            Catch ex As Exception

                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog("Lỗi trong quá trình lấy xã tỉnh: " & ex.ToString, EventLogEntryType.Error)
                Return Nothing
            Finally
                If Not drNT Is Nothing Then
                    drNT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        Public Shared Function getDiaChi(ByVal strMaXa As String) As String
            Dim conn As DataAccess
            Dim drNT As DataTable
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "SELECT  a.TEN FROM tcs_dm_xa a WHERE A.ma_xa='" & strMaXa & "'"
                drNT = conn.ExecuteToTable(strSql)
                If drNT IsNot Nothing Then
                    If drNT.Rows.Count > 0 Then
                        Return drNT.Rows(0)("TEN")
                    Else
                        Return ""
                    End If
                Else
                    Return ""
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog("Lỗi trong quá trình lấy địa chỉ: " & ex.ToString, EventLogEntryType.Error)
                Return Nothing
            Finally
                If Not drNT Is Nothing Then
                    drNT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try

        End Function

        Public Shared Function Get_CCHID(ByVal strMaChuong As String) As String
            '-----------------------------------------------------
            ' Mục đích: Lấy CCH_ID từ Mã chương     
            ' Ngày viết: 24/11/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim strResult As String = "null"
            Dim conn As DataAccess
            Dim drNT As IDataReader
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Select CCH_ID From TCS_DM_CAP_CHUONG Where Ma_Chuong = '" & strMaChuong & "'"
                drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
                If drNT.Read Then
                    strResult = drNT("CCH_ID").ToString()
                End If
                Return strResult
            Catch ex As Exception
                Throw ex
                'LogDebug.Writelog("Lỗi trong quá trình lấy Cấp - Chương ID: " & ex.ToString)
            Finally
                If Not drNT Is Nothing Then
                    drNT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Shared Function Get_LKHID(ByVal strMaKhoan As String) As String
            '    '-----------------------------------------------------
            '    ' Mục đích: Lấy LKH_ID từ Mã khoản    
            '    ' Ngày viết: 24/11/2008
            '    ' Người viết: Lê Hồng Hà
            '    ' ----------------------------------------------------
            Dim strResult As String = "null"
            Dim conn As DataAccess
            Dim drNT As IDataReader
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Select LKH_ID From TCS_DM_LOAI_KHOAN Where Ma_Khoan = '" & strMaKhoan & "'"
                drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
                If drNT.Read Then
                    strResult = drNT("LKH_ID").ToString()
                End If
                Return strResult
            Catch ex As Exception
                Throw ex
                'LogDebug.Writelog("Lỗi trong quá trình lấy Loại - Khoản ID: " & ex.ToString)
            Finally
                If Not drNT Is Nothing Then
                    drNT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try

        End Function
        Public Shared Function Get_MTMucID(ByVal strMaTMuc As String) As String
            '    '---------------------------------------------------------------
            '    ' Mục đích: Get MTMucID của 1 cặp TIỂU MỤC.
            '    ' Ngày viết: 24/11/2004
            '    ' Người viết: Lê Hồng Hà
            '    ' ----------------------------------------------------
            Dim strResult As String = "null"
            Dim conn As DataAccess
            Dim drNT As IDataReader
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Select MTM_ID From TCS_DM_MUC_TMUC Where Ma_TMuc = '" & strMaTMuc & "'"
                drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
                If drNT.Read Then
                    strResult = drNT("MTM_ID").ToString()
                End If
                Return strResult
            Catch ex As Exception
                Throw ex
                'LogDebug.Writelog("Lỗi trong quá trình lấy Mục tiểu mục ID: " & ex.ToString)
            Finally
                If Not drNT Is Nothing Then
                    drNT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try

        End Function

        Public Shared Function Get_TenMTM(ByVal MaMuc As String, ByVal MaTMuc As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên mục tiểu muc của 1 cặp mã MỤC,TIỂU MỤC bất kỳ.
            ' Tham số: MaMuc, MaTMuc
            ' Giá trị trả về: Tên mục tiểu mục
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenMTM As New DataAccess
            Dim drTenMTM As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "Select TEN From TCS_DM_MUC_TMUC Where MA_MUC='" & _
                       MaMuc & "' And MA_TMUC='" & MaTMuc & "'"
                drTenMTM = cnTenMTM.ExecuteDataReader(strSql, CommandType.Text)
                If drTenMTM.Read Then
                    strResult = GetString(drTenMTM(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên mục tiểu muc của 1 cặp mã MỤC,TIỂU MỤC bất kỳ: " & ex.ToString)
            Finally
                If Not drTenMTM Is Nothing Then drTenMTM.Close()
                If Not cnTenMTM Is Nothing Then cnTenMTM.Dispose()
            End Try
            Return strResult
        End Function

        Public Shared Function Get_TenMTM(ByVal MaTMuc As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên mục tiểu

            Dim cnTenMTM As New DataAccess
            Dim drTenMTM As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "Select TEN From TCS_DM_MUC_TMUC Where MA_TMUC='" & MaTMuc & "' And TINH_TRANG='1'"
                drTenMTM = cnTenMTM.ExecuteDataReader(strSql, CommandType.Text)
                If drTenMTM.Read Then
                    strResult = GetString(drTenMTM(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên mục tiểu muc của 1 cặp mã MỤC,TIỂU MỤC bất kỳ: " & ex.ToString)
            Finally
                If Not drTenMTM Is Nothing Then drTenMTM.Close()
                If Not cnTenMTM Is Nothing Then cnTenMTM.Dispose()
            End Try
            Return strResult
        End Function

        'Public Shared Function Get_LoaiThue(ByVal strMaMuc As String, ByVal strMaTMuc As String)
        '    '------------------------------------------------------------------
        '    ' Mục đích: Lấy mã Loại Thuế từ mã mục và mã tiểu mục
        '    ' Tham số: 
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 28/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' -----------------------------------------------------------------
        '    Dim strResult As String = ""
        '    Dim conn As DataAccess
        '    Dim drNT As IDataReader
        '    Dim strSql As String
        '    Try
        '        conn = New DataAccess
        '        strSql = "Select Ma_Lthue From TCS_XDINH_LTHUE " & _
        '            "Where ((Ma_Muc = '" & strMaMuc & "') and (Ma_TMuc = '" & strMaTMuc & "'))" & _
        '            "or ((Ma_Muc = '" & strMaMuc & "') and ((Ma_TMuc = '') or (Ma_TMuc is null))) " & _
        '            "or (((Ma_Muc = '') or (Ma_Muc is null)) and (Ma_TMuc = '" & strMaTMuc & "'))"
        '        drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
        '        If drNT.Read Then
        '            strResult = drNT.GetString(0)
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy mã LThuế: " & ex.ToString)
        '        Return ""
        '    Finally
        '        If Not drNT Is Nothing Then
        '            drNT.Dispose()
        '        End If
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try
        '    Return strResult
        'End Function
        Public Shared Function Get_IDLH(ByVal strMaLH As String, ByVal strMaCqqd As String) As String
            Dim conn As DataAccess
            Dim strSql As String
            Dim strResult As String = "null"
            Dim strCap As String
            Dim drLH As IDataReader
            Dim drMaCap As IDataReader
            Try
                conn = New DataAccess
                strSql = "select cap from tcs_dm_cqqd where ma_cqqd ='" & strMaCqqd & "'"
                drMaCap = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drMaCap.Read Then
                    strCap = drMaCap.GetValue(0).ToString()
                End If
                strSql = "Select Id from tcs_dm_lhthu_mlns Where ma_lh='" & strMaLH & "' " & _
                         "And ma_cap ='" & strCap & "'"
                drLH = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drLH.Read() Then
                    strResult = drLH.GetValue(0).ToString()
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ID loại hình MLNS: " & ex.ToString)
            Finally
                If Not drLH Is Nothing Then
                    drLH.Close()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function GetTyGia(ByVal strMA_NT As String, ByVal strNGAY_LV As String) As String
            '------------------------------------------------------------------
            ' Mục đích: Lấy tỷ giá 
            ' Tham số: 
            ' Giá trị trả về: 
            ' Ngày viết: 03/11/2007
            ' Người viết: Lê Hồng Hà
            ' -----------------------------------------------------------------
            Dim cnTyGia As DataAccess
            Dim drTyGia As IDataReader
            Dim strSQL As String = "Select TG_ID, MA_NT, TY_GIA, NGAY_HL From TCS_DM_TYGIA " & _
            "Where MA_NT='" & strMA_NT.ToUpper & "' and NGAY_HL<=" & strNGAY_LV & " Order by NGAY_HL DESC"

            Try
                Dim strTyGia As String = "0"
                cnTyGia = New DataAccess
                drTyGia = cnTyGia.ExecuteDataReader(strSQL, CommandType.Text)
                If drTyGia.Read Then
                    strTyGia = drTyGia("TY_GIA").ToString()
                End If
                Return strTyGia
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi sảy ra khi lấy thông tin tỷ giá từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drTyGia Is Nothing Then drTyGia.Close()
                If Not cnTyGia Is Nothing Then cnTyGia.Dispose()
            End Try
        End Function
        Public Shared Function GetTyGiaID(ByVal strMA_NT As String, ByVal strNGAY_LV As String) As String
            '------------------------------------------------------------------
            ' Mục đích: Lấy tỷ giá ID
            ' Tham số: 
            ' Giá trị trả về: 
            ' Ngày viết: 03/11/2007
            ' Người viết: Lê Hồng Hà
            ' -----------------------------------------------------------------
            Dim cnTyGia As DataAccess
            Dim drTyGia As IDataReader
            Dim strSQL As String
            Dim strTyGiaID As String = "null"
            Try
                strSQL = "Select TG_ID, MA_NT, TY_GIA, NGAY_HL From TCS_DM_TYGIA " & _
                         "Where MA_NT='" & strMA_NT.ToUpper & "' and NGAY_HL<=" & strNGAY_LV & " Order by NGAY_HL DESC"
                cnTyGia = New DataAccess
                drTyGia = cnTyGia.ExecuteDataReader(strSQL, CommandType.Text)
                If drTyGia.Read Then
                    strTyGiaID = drTyGia("TG_ID").ToString()
                End If
                Return strTyGiaID
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tỷ giá từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drTyGia Is Nothing Then drTyGia.Close()
                If Not cnTyGia Is Nothing Then cnTyGia.Dispose()
            End Try
        End Function
        'Public Shared Function Get_TenNganHang(ByVal strMaNH As String)
        '    '------------------------------------------------------------------
        '    ' Mục đích: Lấy tên ngân hàng
        '    ' Tham số: 
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 03/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' -----------------------------------------------------------------
        '    Dim strResult As String = ""
        '    Dim conn As DataAccess
        '    Dim drNT As IDataReader
        '    Dim strSql As String
        '    Try
        '        conn = New DataAccess
        '        strSql = "Select TEN From TCS_DM_NGANHANG Where Ma = '" & strMaNH & "'"
        '        drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
        '        If drNT.Read Then
        '            strResult = drNT.GetString(0)
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tên ngân hàng: " & ex.ToString)
        '        Return ""
        '    Finally
        '        If Not drNT Is Nothing Then
        '            drNT.Dispose()
        '        End If
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try
        '    Return strResult
        'End Function
        'Public Shared Function Get_TenCQQD(ByVal strMaCQQD As String) As String
        '    Dim strResult As String = ""
        '    Dim conn As DataAccess
        '    Dim drNT As IDataReader
        '    Dim strSql As String
        '    Try
        '        conn = New DataAccess
        '        strSql = "Select Ten_CQQD From TCS_DM_CQQD Where Ma_CQQD = '" & strMaCQQD & "'"
        '        drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
        '        If drNT.Read Then
        '            strResult = drNT.GetString(0)
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tên cơ quan quyết định: " & ex.ToString)
        '    Finally
        '        If Not drNT Is Nothing Then
        '            drNT.Dispose()
        '        End If
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try
        '    Return strResult
        'End Function
        'Public Shared Function Get_XaID(ByVal strMaXa As String) As String
        '    Dim strResult As String = "null"
        '    Dim conn As DataAccess
        '    Dim drNT As IDataReader
        '    Dim strSql As String
        '    Try
        '        conn = New DataAccess
        '        strSql = "Select XA_ID From TCS_DM_XA Where Ma_Xa ='" & strMaXa & "'"
        '        drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
        '        If drNT.Read Then
        '            strResult = drNT("XA_ID").ToString()
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy xã ID: " & ex.ToString)
        '    Finally
        '        If Not drNT Is Nothing Then
        '            drNT.Dispose()
        '        End If
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try
        '    Return strResult
        'End Function
        'Public Shared Function Get_DTietID(ByVal strMaTLDT As String) As String
        '    Dim strResult As String = "null"
        '    Dim conn As DataAccess
        '    Dim drNT As IDataReader
        '    Dim strSql As String
        '    Try
        '        conn = New DataAccess
        '        strSql = "Select DT_ID From TCS_DM_TLDT Where Ma_TLDT = '" & strMaTLDT & "'"
        '        drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
        '        If drNT.Read Then
        '            strResult = drNT("DT_ID").ToString()
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tỉ lệ điều tiết ID: " & ex.ToString)
        '    Finally
        '        If Not drNT Is Nothing Then
        '            drNT.Dispose()
        '        End If
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try
        '    Return strResult
        'End Function
        'Public Shared Function Get_CCHID(ByVal strMaCap As String, ByVal strMaChuong As String) As String
        '    '-----------------------------------------------------
        '    ' Mục đích: Lấy CCH_ID từ Mã cấp  Mã chương
        '    ' Tham số: 
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 02/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim strResult As String = "null"
        '    Dim conn As DataAccess
        '    Dim drNT As IDataReader
        '    Dim strSql As String
        '    Try
        '        conn = New DataAccess
        '        strSql = "Select CCH_ID From TCS_DM_CAP_CHUONG Where Ma_Cap = '" & strMaCap & "' " & _
        '        "And Ma_Chuong = '" & strMaChuong & "'"
        '        drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
        '        If drNT.Read Then
        '            strResult = drNT("CCH_ID").ToString()
        '        End If
        '        Return strResult
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy Cấp - Chương ID: " & ex.ToString)
        '    Finally
        '        If Not drNT Is Nothing Then
        '            drNT.Dispose()
        '        End If
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try
        'End Function
        'Public Shared Function Get_LKHID(ByVal strMaLoai As String, ByVal strMaKhoan As String) As String
        '    '-----------------------------------------------------
        '    ' Mục đích: Lấy LKH_ID từ Mã loại - Mã khoản
        '    ' Tham số: 
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 02/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim strResult As String = "null"
        '    Dim conn As DataAccess
        '    Dim drNT As IDataReader
        '    Dim strSql As String
        '    Try
        '        conn = New DataAccess
        '        strSql = "Select LKH_ID From TCS_DM_LOAI_KHOAN Where Ma_Loai = '" & strMaLoai & "' " & _
        '        "And Ma_Khoan = '" & strMaKhoan & "'"
        '        drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
        '        If drNT.Read Then
        '            strResult = drNT("LKH_ID").ToString()
        '        End If
        '        Return strResult
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy Loại - Khoản ID: " & ex.ToString)
        '    Finally
        '        If Not drNT Is Nothing Then
        '            drNT.Dispose()
        '        End If
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try

        'End Function
        'Public Shared Function Get_MTMucID(ByVal strMaMuc As String, ByVal strMaTMuc As String) As String
        '    '---------------------------------------------------------------
        '    ' Mục đích: Get MTMucID của 1 cặp mã MỤC,TIỂU MỤC bất kỳ.
        '    ' Ngày viết: 28/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim strResult As String = "null"
        '    Dim conn As DataAccess
        '    Dim drNT As IDataReader
        '    Dim strSql As String
        '    Try
        '        conn = New DataAccess
        '        strSql = "Select MTM_ID From TCS_DM_MUC_TMUC Where Ma_Muc = '" & strMaMuc & "' " & _
        '        "And Ma_TMuc = '" & strMaTMuc & "'"
        '        drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
        '        If drNT.Read Then
        '            strResult = drNT("MTM_ID").ToString()
        '        End If
        '        Return strResult
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy Mục tiểu mục ID: " & ex.ToString)
        '    Finally
        '        If Not drNT Is Nothing Then
        '            drNT.Dispose()
        '        End If
        '        If Not conn Is Nothing Then
        '            conn.Dispose()
        '        End If
        '    End Try

        'End Function
        'Public Shared Function Get_TenTinh(ByVal MaTinh As String) As String
        '    '---------------------------------------------------------------
        '    ' Mục đích: Lấy ra tên huyện của 1 mã huyện bất kỳ.
        '    ' Tham số: MaHuyen: mã huyện cần lấy tên
        '    ' Giá trị trả về: Tên huyện
        '    ' Ngày viết: 02/11/2003
        '    ' Người viết: Nguyễn Quốc Việt
        '    ' ----------------------------------------------------
        '    Dim cnTenTinh As New DataAccess
        '    Dim drTenTinh As IDataReader
        '    Dim strSql As String
        '    Dim strResult As String = ""
        '    Try
        '        strSql = "Select TEN From TCS_DM_TINH Where MA_TINH='" & MaTinh & "'"
        '        drTenTinh = cnTenTinh.ExecuteDataReader(strSql, CommandType.Text)
        '        If drTenTinh.Read Then
        '            strResult = Trim(drTenTinh.GetString(0))
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tên tỉnh: " + ex.ToString)
        '    Finally
        '        drTenTinh.Close()
        '        cnTenTinh.Dispose()
        '    End Try
        '    Return strResult
        'End Function
        'Public Shared Function Get_TenHuyen(ByVal MaHuyen As String) As String
        '    '---------------------------------------------------------------
        '    ' Mục đích: Lấy ra tên huyện của 1 mã huyện bất kỳ.
        '    ' Tham số: MaHuyen: mã huyện cần lấy tên
        '    ' Giá trị trả về: Tên huyện
        '    ' Ngày viết: 02/11/2003
        '    ' Người viết: Nguyễn Quốc Việt
        '    ' ----------------------------------------------------
        '    Dim cnTenHuyen As New DataAccess
        '    Dim drTenHuyen As IDataReader
        '    Dim strSql As String
        '    Dim strResult As String = ""
        '    Try
        '        strSql = "Select TEN From TCS_DM_XA Where MA_TINH='" & _
        '                mdlSystemVariables.gstrMaTTP & "' And MA_HUYEN='" & MaHuyen & "'"
        '        drTenHuyen = cnTenHuyen.ExecuteDataReader(strSql, CommandType.Text)
        '        If drTenHuyen.Read Then
        '            strResult = Trim(drTenHuyen.GetString(0))
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tên huyện: " + ex.ToString)
        '    Finally
        '        drTenHuyen.Close()
        '        cnTenHuyen.Dispose()
        '    End Try
        '    Return strResult
        'End Function
        'Public Shared Function Get_TenHuyen(ByVal MaHuyen As String, ByVal MaTinh As String) As String
        '    '---------------------------------------------------------------
        '    ' Mục đích: Lấy ra tên huyện của 1 mã huyện bất kỳ.
        '    ' Tham số: MaHuyen: mã huyện cần lấy tên
        '    ' Giá trị trả về: Tên huyện
        '    ' Ngày viết: 02/11/2003
        '    ' Người viết: Nguyễn Quốc Việt
        '    ' ----------------------------------------------------
        '    Dim cnTenHuyen As New DataAccess
        '    Dim drTenHuyen As IDataReader
        '    Dim strSql As String
        '    Dim strResult As String = ""
        '    Try
        '        strSql = "Select TEN From TCS_DM_XA Where MA_TINH='" & _
        '                MaTinh & "' And MA_HUYEN='" & MaHuyen & "'"
        '        drTenHuyen = cnTenHuyen.ExecuteDataReader(strSql, CommandType.Text)
        '        If drTenHuyen.Read Then
        '            strResult = Trim(drTenHuyen.GetString(0))
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tên huyện: " + ex.ToString)
        '    Finally
        '        drTenHuyen.Close()
        '        cnTenHuyen.Dispose()
        '    End Try
        '    Return strResult
        'End Function

        Public Shared Function Get_TenKhoBac(ByVal strSHKB As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên xã của 1 mã XÃ bất kỳ.
            ' Tham số: MaXa: mã XÃ cần lấy tên
            ' Giá trị trả về: Tên xã
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenXa As New DataAccess
            Dim drTenXa As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "select ten from tcs_dm_khobac where shkb='" & strSHKB & "'" ' & Globals.EscapeQuote(strSHKB.Replace(".", "") & "'")
                drTenXa = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenXa.Read Then
                    strResult = Trim(drTenXa.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên xã của 1 mã XÃ bất kỳ: " & ex.ToString)
            Finally
                drTenXa.Close()
                cnTenXa.Dispose()
            End Try
            Return strResult
        End Function

        Public Shared Function Get_TenDBHC(ByVal strMaDBHC As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên xã của 1 mã XÃ bất kỳ.
            ' Tham số: MaXa: mã XÃ cần lấy tên
            ' Giá trị trả về: Tên xã
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenXa As New DataAccess
            Dim drTenXa As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "Select TEN From TCS_DM_XA Where MA_XA='" & strMaDBHC & "'" '& Globals.EscapeQuote(strMaDBHC.Replace(".", ""))
                drTenXa = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenXa.Read Then
                    strResult = Trim(drTenXa.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên xã của 1 mã XÃ bất kỳ: " & ex.ToString)
            Finally
                drTenXa.Close()
                cnTenXa.Dispose()
            End Try
            Return strResult
        End Function
        Public Shared Function Get_TenCN(ByVal strMaCN As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên xã của 1 mã XÃ bất kỳ.
            ' Tham số: MaXa: mã XÃ cần lấy tên
            ' Giá trị trả về: Tên xã
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenXa As New DataAccess
            Dim drTenXa As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "Select name From TCS_DM_CHINHANH Where ID='" & strMaCN & "'" '& Globals.EscapeQuote(strMaDBHC.Replace(".", ""))
                drTenXa = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenXa.Read Then
                    strResult = Trim(drTenXa.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên xã của 1 mã XÃ bất kỳ: " & ex.ToString)
            Finally
                drTenXa.Close()
                cnTenXa.Dispose()
            End Try
            Return strResult
        End Function
        Public Shared Function Get_PROVICE_CODE(ByVal strMaCN As String) As String
            Dim cnTenXa As New DataAccess
            Dim drTenXa As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "Select PROVICE_CODE From TCS_DM_CHINHANH Where ID='" & strMaCN & "'"
                drTenXa = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenXa.Read Then
                    strResult = Trim(drTenXa.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên xã của 1 mã XÃ bất kỳ: " & ex.ToString)
            Finally
                drTenXa.Close()
                cnTenXa.Dispose()
            End Try
            Return strResult
        End Function
        Public Shared Function Get_ProDuct_Type(ByVal strProvice_Code As String, ByVal strSmart_Card As String) As String
            Dim cnTenXa As New DataAccess
            Dim drTenXa As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                If strSmart_Card = "FCC" Then
                    strSql = "Select FCC_CODE From TCS_DM_MAP_PRODUCT Where ETAX_CODE='" & strProvice_Code & "'"
                Else
                    strSql = "Select XCARD_CODE From TCS_DM_MAP_PRODUCT Where ETAX_CODE='" & strProvice_Code & "'"
                End If
                drTenXa = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenXa.Read Then
                    strResult = Trim(drTenXa.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên xã của 1 mã XÃ bất kỳ: " & ex.ToString)
            Finally
                drTenXa.Close()
                cnTenXa.Dispose()
            End Try
            Return strResult
        End Function
        Public Shared Function Get_TenPTTT(ByVal strMaPTTT As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên xã của 1 mã XÃ bất kỳ.
            ' Tham số: MaXa: mã XÃ cần lấy tên
            ' Giá trị trả về: Tên xã
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenXa As New DataAccess
            Dim drTenXa As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "Select TEN_PTTT From TCS_DM_PTTT Where MA_PTTT='" & strMaPTTT & "'" '& Globals.EscapeQuote(strMaDBHC.Replace(".", ""))
                drTenXa = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenXa.Read Then
                    strResult = Trim(drTenXa.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên xã của 1 mã XÃ bất kỳ: " & ex.ToString)
            Finally
                drTenXa.Close()
                cnTenXa.Dispose()
            End Try
            Return strResult
        End Function
        'Public Shared Sub Get_LHXNK_ALL(ByVal Ma_LH As String, ByRef Ten_LH As String, ByRef Ten_VT As String)
        '    '---------------------------------------------------------------
        '    ' Mục đích: Lấy ra tên LH và Ten_VT của 1 mã loại hình bất kỳ.
        '    ' Tham số: Ma_LH: mã loại hình cần lấy tên
        '    ' Ngày viết: 14/10/2008
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim cnTenCQ As DataAccess
        '    Dim drTenCQ As IDataReader
        '    Dim strSql As String
        '    Try
        '        cnTenCQ = New DataAccess
        '        strSql = "Select TEN_LH, TEN_VT From TCS_DM_LHINH Where Ma_LH='" & Ma_LH & "'"
        '        drTenCQ = cnTenCQ.ExecuteDataReader(strSql, CommandType.Text)
        '        If drTenCQ.Read Then
        '            Ten_LH = Trim(drTenCQ.GetString(0))
        '            Ten_VT = Trim(drTenCQ.GetString(1))
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tên Loại hình xuất nhập khẩu: " & ex.ToString)
        '    Finally
        '        If Not drTenCQ Is Nothing Then
        '            drTenCQ.Close()
        '        End If
        '        If Not cnTenCQ Is Nothing Then
        '            cnTenCQ.Dispose()
        '        End If
        '    End Try
        'End Sub

        'Public Shared Sub Get_LHXNK_byTenVT(ByVal Ten_VT As String, ByRef Ma_LH As String, ByRef Ten_LH As String)
        '    '---------------------------------------------------------------
        '    ' Mục đích: Lấy ra tên LH và Ten_VT của 1 mã loại hình bất kỳ.
        '    ' Tham số: Ma_LH: mã loại hình cần lấy tên
        '    ' Ngày viết: 14/10/2008
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Dim cnTenCQ As DataAccess
        '    Dim drTenCQ As IDataReader
        '    Dim strSql As String
        '    Try
        '        cnTenCQ = New DataAccess
        '        strSql = "Select Ma_LH, TEN_LH From TCS_DM_LHINH Where TEN_VT='" & Ten_VT.ToUpper() & "' order by Ma_LH"
        '        drTenCQ = cnTenCQ.ExecuteDataReader(strSql, CommandType.Text)
        '        If drTenCQ.Read Then
        '            Ma_LH = Trim(drTenCQ.GetString(0))
        '            Ten_LH = Trim(drTenCQ.GetString(1))
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tên Loại hình xuất nhập khẩu: " & ex.ToString)
        '    Finally
        '        If Not drTenCQ Is Nothing Then
        '            drTenCQ.Close()
        '        End If
        '        If Not cnTenCQ Is Nothing Then
        '            cnTenCQ.Dispose()
        '        End If
        '    End Try
        'End Sub

        'Public Shared Function Get_TenNTe(ByVal MaNT As String) As String
        '    '---------------------------------------------------------------
        '    ' Mục đích: Lấy ra tên ngoại tệ của 1 mã NT bất kỳ.
        '    ' Giá trị trả về: Tên ngoại tệ
        '    ' Ngày viết: 26/20/2007
        '    ' Người viết: Nguyễn Hữu Hoan
        '    ' ----------------------------------------------------
        '    Dim cnTenNT As DataAccess
        '    Dim drTenNT As IDataReader
        '    Dim strSql As String
        '    Dim strResult As String = ""
        '    Try
        '        cnTenNT = New DataAccess
        '        strSql = "Select TEN From TCS_DM_NGUYENTE Where Ma_NT = '" & MaNT.ToUpper() & "'"
        '        drTenNT = cnTenNT.ExecuteDataReader(strSql, CommandType.Text)
        '        If drTenNT.Read Then
        '            strResult = Trim(drTenNT.GetString(0))
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình lấy tên nguyên tệ: " & ex.ToString)
        '    Finally
        '        If Not drTenNT Is Nothing Then
        '            drTenNT.Close()
        '        End If
        '        If Not cnTenNT Is Nothing Then
        '            cnTenNT.Dispose()
        '        End If
        '    End Try
        '    Return strResult
        'End Function
        Public Shared Function Get_TenCQThu(ByVal MaCQThu As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên CQ thu của 1 mã CQ thu bất kỳ.
            ' Tham số: MaCQThu: mã CQ cần lấy tên
            ' Giá trị trả về: Tên cơ quan thu
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenCQ As DataAccess
            Dim drTenCQ As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnTenCQ = New DataAccess
                strSql = "Select TEN From TCS_DM_CQTHU Where Ma_CQThu='" & _
                       MaCQThu & "'"
                drTenCQ = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenCQ.Read Then
                    strResult = Trim(drTenCQ.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên Cơ quan thu: " & ex.ToString)
            Finally
                If Not drTenCQ Is Nothing Then
                    drTenCQ.Close()
                End If
                If Not cnTenCQ Is Nothing Then
                    cnTenCQ.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function Get_TenCQThue_DAB(ByVal MaCQThu As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên CQ thu của 1 mã CQ thu bất kỳ.
            ' Tham số: MaCQThu: mã CQ cần lấy tên
            ' Giá trị trả về: Tên cơ quan thu
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenCQ As DataAccess
            Dim drTenCQ As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnTenCQ = New DataAccess
                strSql = "Select TEN From TCS_DM_CQTHU Where Ma_CQThu='" & _
                       MaCQThu & "' and rownum=1"
                drTenCQ = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenCQ.Read Then
                    strResult = Trim(drTenCQ.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên Cơ quan thu: " & ex.ToString)
            Finally
                If Not drTenCQ Is Nothing Then
                    drTenCQ.Close()
                End If
                If Not cnTenCQ Is Nothing Then
                    cnTenCQ.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function Get_TenHQ(ByVal MaHQ As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên CQ thu của 1 mã CQ thu bất kỳ.
            ' Tham số: MaCQThu: mã CQ cần lấy tên
            ' Giá trị trả về: Tên cơ quan thu
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenCQ As DataAccess
            Dim drTenCQ As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnTenCQ = New DataAccess
                strSql = "Select TEN From TCS_DM_CQTHU Where Ma_HQ='" & _
                       MaHQ & "'"
                drTenCQ = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenCQ.Read Then
                    strResult = Trim(drTenCQ.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên Cơ quan thu: " & ex.ToString)
            Finally
                If Not drTenCQ Is Nothing Then
                    drTenCQ.Close()
                End If
                If Not cnTenCQ Is Nothing Then
                    cnTenCQ.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function Get_TenKH(ByVal SHKB As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên CQ thu của 1 mã CQ thu bất kỳ.
            ' Tham số: MaCQThu: mã CQ cần lấy tên
            ' Giá trị trả về: Tên cơ quan thu
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenCQ As DataAccess
            Dim drTenCQ As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnTenCQ = New DataAccess
                strSql = "Select TEN From TCS_DM_KHOBAC Where SHKB='" & _
                       SHKB & "'"
                drTenCQ = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenCQ.Read Then
                    strResult = Trim(drTenCQ.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên Cơ quan thu: " & ex.ToString)
            Finally
                If Not drTenCQ Is Nothing Then
                    drTenCQ.Close()
                End If
                If Not cnTenCQ Is Nothing Then
                    cnTenCQ.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function Get_DBHC_KB(ByVal SHKB As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên CQ thu của 1 mã CQ thu bất kỳ.
            ' Tham số: MaCQThu: mã CQ cần lấy tên
            ' Giá trị trả về: Tên cơ quan thu
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenCQ As DataAccess
            Dim drTenCQ As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnTenCQ = New DataAccess
                strSql = "Select MA_DB From TCS_DM_KHOBAC Where SHKB='" & _
                       SHKB & "'"
                drTenCQ = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenCQ.Read Then
                    strResult = Trim(drTenCQ.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên Cơ quan thu: " & ex.ToString)
            Finally
                If Not drTenCQ Is Nothing Then
                    drTenCQ.Close()
                End If
                If Not cnTenCQ Is Nothing Then
                    cnTenCQ.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function Get_TenLHXNK(ByVal Ma_LH As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên Loại hình xuất nhập khẩu của 1 mã loại hình bất kỳ.
            ' Tham số: Ma_LH: mã loại hình cần lấy tên
            ' Giá trị trả về: Tên loại hình xuất nhập khẩu
            ' Ngày viết: 19/03/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnTenCQ As DataAccess
            Dim drTenCQ As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnTenCQ = New DataAccess
                strSql = "Select TEN_LH From TCS_DM_LHINH Where Ma_LH='" & Ma_LH & "'"
                drTenCQ = cnTenCQ.ExecuteDataReader(strSql, CommandType.Text)
                If drTenCQ.Read Then
                    strResult = Trim(drTenCQ.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên Loại hình xuất nhập khẩu: " & ex.ToString)
            Finally
                If Not drTenCQ Is Nothing Then
                    drTenCQ.Close()
                End If
                If Not cnTenCQ Is Nothing Then
                    cnTenCQ.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function Get_TenTKhoan(ByVal strMaTK) As String
            Dim cnTenTK As DataAccess
            Dim drTenTK As IDataReader
            Dim strSQL As String
            Dim strResult As String = ""
            Try
                cnTenTK = New DataAccess
                strSQL = "Select Ten_TK From TCS_DM_TAIKHOAN Where TK='" & strMaTK & "'"
                drTenTK = cnTenTK.ExecuteDataReader(strSQL, CommandType.Text)
                If drTenTK.Read Then
                    strResult = Trim(drTenTK.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên tài khoản: " & ex.ToString)
            Finally
                If Not drTenTK Is Nothing Then
                    drTenTK.Close()
                End If
                If Not cnTenTK Is Nothing Then
                    cnTenTK.Dispose()
                End If
            End Try
            Return strResult
        End Function

        'ICB: Lấy tên TK TM của ngân hàng
        ' Hoàng Văn Anh
        Public Shared Function Get_TenTKTM_NH() As String
            Return ParmDescript_Load("NH_TKTM")
        End Function
        'ICB: Lấy tên TK CK của ngân hàng
        ' Hoàng Văn Anh
        Public Shared Function Get_TenTKCK_NH() As String
            Return ParmDescript_Load("NH_TKTH_KB")
        End Function

        Public Shared Function Get_TenLoaithue(ByVal fstrMaloaithue As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên loại thuế của 1 mã loại thuế bất kỳ.
            ' Tham số: Maloaithue: mã loại thuế cần lấy tên
            ' Giá trị trả về: Tên loại thuế
            ' Ngày viết: 07/04/2005
            ' Người viết: Phan Nhật Kiên
            ' ----------------------------------------------------
            Dim cnTenLT As DataAccess
            Dim drTenLT As IDataReader
            Dim strSQL As String
            Dim strResult As String = ""
            Try
                cnTenLT = New DataAccess
                strSQL = "Select TEN From TCS_DM_LTHUE Where Ma_LThue='" & fstrMaloaithue & "'"
                drTenLT = cnTenLT.ExecuteDataReader(strSQL, CommandType.Text)
                If drTenLT.Read Then
                    strResult = Trim(drTenLT.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên loại thuế: " & ex.ToString)
            Finally
                If Not drTenLT Is Nothing Then
                    drTenLT.Close()
                End If
                If Not cnTenLT Is Nothing Then
                    cnTenLT.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function Get_TenNNThue(ByVal strMaNTThue As String) As String
            '-----------------------------------------------------
            ' Mục đích: Lấy tên người nộp thuế.
            ' Tham số: Mã Người nộp Thuế
            ' Giá trị trả về:
            ' Ngày viết: 25/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------        
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "Select TEN_NNT " & _
                    "From TCS_DM_NNT " & _
                    "Where MA_NNT = '" & strMaNTThue & "' "

                Dim strReturn As String = ""
                Dim drNNT As IDataReader = conn.ExecuteDataReader(strSQL, CommandType.Text)
                If (drNNT.Read()) Then
                    strReturn = drNNT("TEN_NNT").ToString()
                End If
                drNNT.Close()

                Return strReturn
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        ''' ***************BEGIN****************************************'
        ''' ---------------CREATE--------------------------
        '''  Tên:			Get_s the cif number.
        '''  Người viết:	ANHLD
        '''  Ngày viết:		10/02/2012 00:00:00
        '''  Mục đích:	    lấy số cif theo mst
        ''' ---------------MODIFY--------------------------
        '''  Người sửa:	
        '''  Ngày sửa: 	
        '''  Mục đích:	
        ''' ---------------PARAM--------------------------
        ''' <param name="strMaNTThue">The STR ma NT thue.</param>
        ''' <returns>System.String.</returns>
        Public Shared Function Get_CifNumber(ByVal strMaNTThue As String) As String
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "Select CIF " & _
                    "From TCS_DM_CIF " & _
                    "Where MST = '" & strMaNTThue & "' AND ROWNUM=1"

                Dim strReturn As String = ""
                Dim drNNT As IDataReader = conn.ExecuteDataReader(strSQL, CommandType.Text)
                If (drNNT.Read()) Then
                    strReturn = drNNT("CIF").ToString()
                End If
                drNNT.Close()

                Return strReturn
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Shared Function Get_TS_DC() As String
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "Select NVL(substr(GIO_BAT_DAU,0,2),'16') || '|' || TS_NHAN_DC" & _
                            " || '|' || to_char(GUI_LAN_CUOI, 'dd/MM/yyyy')" & _
                            " || '|' || to_char(NHAN_LAN_CUOI, 'dd/MM/yyyy')" & _
                            " || '|' || TT_GUI_CUOI || '|' || TT_NHAN_CUOI" & _
                            " || '|' || GIO_KET_THUC || '|' || AUTO_RUN || '|' || NVL(substr(GIO_BAT_DAU,4,2),'00') AS DC_VALUE" & _
                    " From TCS_THAMSO_DC "

                Dim strReturn As String = ""
                Dim drNNT As IDataReader = conn.ExecuteDataReader(strSQL, CommandType.Text)
                If (drNNT.Read()) Then
                    strReturn = drNNT("DC_VALUE").ToString()
                End If
                drNNT.Close()

                Return strReturn
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Shared Function GET_URL_HQ() As String
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "Select GIATRI_TS AS URL_HQ" & _
                    " From TCS_THAMSO_HT WHERE TEN_TS='URL_HQ'"

                Dim strReturn As String = ""
                strReturn = conn.ExecuteSQLScalar(strSQL)
                Return strReturn
                'Return strReturn
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Shared Function GET_URL_HQ_DC() As String
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "Select GIATRI_TS AS URL_HQ_DC" & _
                    " From TCS_THAMSO_HT WHERE TEN_TS='URL_HQ_DC'"

                Dim strReturn As String = ""
                strReturn = conn.ExecuteSQLScalar(strSQL)

                Return strReturn
                'Return strReturn
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Shared Function GET_URL_HQ247() As String
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "Select GIATRI_TS AS URL_HQ" & _
                    " From TCS_THAMSO_HT WHERE TEN_TS='URL_HQ247'"

                Dim strReturn As String = ""
                strReturn = conn.ExecuteSQLScalar(strSQL)
                Return strReturn
                'Return strReturn
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Shared Function GET_URL_HQ247_DC() As String
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "Select GIATRI_TS AS URL_HQ_DC" & _
                    " From TCS_THAMSO_HT WHERE TEN_TS='URL_HQ247_DC'"

                Dim strReturn As String = ""
                strReturn = conn.ExecuteSQLScalar(strSQL)

                Return strReturn
                'Return strReturn
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Shared Function Get_TenLHThu(ByVal Ma_LH As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên Loại hình thu.
            ' Tham số: Ma_LH: mã loại hình cần lấy tên
            ' Giá trị trả về: Tên loại hình thu
            ' Ngày viết: 12/12/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnTenCQ As DataAccess
            Dim drTenCQ As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnTenCQ = New DataAccess
                strSql = "Select TEN_LH From TCS_DM_LHTHU Where Ma_LH='" & Ma_LH & "'"
                drTenCQ = cnTenCQ.ExecuteDataReader(strSql, CommandType.Text)
                If drTenCQ.Read Then
                    strResult = Trim(drTenCQ.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên Loại hình thu: " & ex.ToString)
            Finally
                If Not drTenCQ Is Nothing Then
                    drTenCQ.Close()
                End If
                If Not cnTenCQ Is Nothing Then
                    cnTenCQ.Dispose()
                End If
            End Try
            Return strResult
        End Function

        Public Shared Function TimMa_TLDT(ByVal MaHuyen As String, ByVal MaXa As String, _
           ByVal MaCQThu As String, ByVal MLNS As String) As String
            '-----------------------------------------------------------------
            ' Mục đích: Tìm mã điều tiết cho 1 khoản thu sau khi đã biết mã xã
            '           hoặc mã huỵện, mã CQThu và MLNS.
            ' Tham số: Mã xã (nếu có), Mã Huyện (nếu có), Mã CQThu (nếu có), MLNS
            ' Giá trị trả về: Xâu ký tự chứa mã ĐT, nếu không tìm thấy thì trả
            '                 về xâu rỗng.
            ' Ngày viết: 13/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------------------
            Dim cnTLDT As New DataAccess
            Dim drTLDT As IDataReader
            Dim strSql As String
            Dim strCap As String = Mid(MLNS, 1, 1)
            Dim strCh As String = Mid(MLNS, 2, 3)
            Dim strLK As String = Mid(MLNS, 5, 4)
            Dim strMuc As String = Mid(MLNS, 9, 3)
            Dim strTM As String = Mid(MLNS, 12, 2)
            Dim strResult As String = " "

            Try
                strSql = "Select " & _
                                 "MA_HUYEN," & _
                                 "MA_XA," & _
                                 "MA_CQTHU," & _
                                 "substr(MLNS,1,1) As CAP," & _
                                 "substr(MLNS,2,3) As CH," & _
                                 "substr(MLNS,5,4) As LK," & _
                                 "substr(MLNS,9,3) As MUC," & _
                                 "substr(MLNS,12,2) As TM," & _
                                 "MA_TLDT As MADT " & _
                                 "From TCS_DM_DIEUTIET " & _
                                 "Order By " & _
                                 "MA_HUYEN Desc," & _
                                 "MA_XA Desc," & _
                                 "MA_CQTHU Desc," & _
                                 "substr(MLNS,1,1) Desc," & _
                                 "substr(MLNS,2,3) Desc," & _
                                 "substr(MLNS,5,4) Desc," & _
                                 "substr(MLNS,9,3) Desc," & _
                                 "substr(MLNS,12,2) Desc"
                drTLDT = cnTLDT.ExecuteDataReader(strSql, CommandType.Text)
                Do While drTLDT.Read
                    If (drTLDT.IsDBNull(0) OrElse Trim(drTLDT.GetString(0)) = "" OrElse MaHuyen = drTLDT.GetString(0)) _
                        And (drTLDT.IsDBNull(1) OrElse Trim(drTLDT.GetString(1)) = "" OrElse MaXa = drTLDT.GetString(1)) _
                        And (drTLDT.IsDBNull(2) OrElse Trim(drTLDT.GetString(2)) = "" OrElse MaCQThu = drTLDT.GetString(2)) _
                        And (drTLDT.IsDBNull(3) OrElse strCap = drTLDT.GetString(3) OrElse Trim(drTLDT.GetString(3)) = "") _
                        And (drTLDT.IsDBNull(4) OrElse strCh = drTLDT.GetString(4) OrElse Trim(drTLDT.GetString(4)) = "") _
                        And (drTLDT.IsDBNull(5) OrElse strLK = drTLDT.GetString(5) OrElse Trim(drTLDT.GetString(5)) = "") _
                        And (drTLDT.IsDBNull(6) OrElse strMuc = drTLDT.GetString(6) OrElse Trim(drTLDT.GetString(6)) = "") _
                        And (drTLDT.IsDBNull(7) OrElse strTM = drTLDT.GetString(7) OrElse Trim(drTLDT.GetString(7)) = "") Then
                        '
                        strResult = drTLDT.GetString(8)
                        '
                        Exit Do
                    End If
                Loop
                drTLDT.Close()
                cnTLDT.Dispose()
            Catch ex As Exception
                MsgBox(ex.ToString)
                If Not drTLDT Is Nothing OrElse Not drTLDT.IsClosed Then
                    drTLDT.Close()
                End If
                If Not cnTLDT Is Nothing Then
                    cnTLDT.Dispose()
                End If
                strResult = ""
            End Try
            Return Trim(strResult)
        End Function
#End Region

#Region "Valid_MLNS"
        Public Shared Function Valid_CCh(ByVal MaCap As String, ByVal MaChuong As String, ByVal strNgayLV As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 cặp mã CẤP CHƯƠNG bất kỳ theo MLNS   
            ' Ngày viết: 28/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnCCh As New DataAccess
            Dim drCCh As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select TEN From TCS_DM_CAP_CHUONG " & _
                        "Where (MA_CAP='" & MaCap & "') And (MA_CHUONG='" & MaChuong & "') " & _
                        "and (ngay_bd <= " & strNgayLV & ") and " & _
                        "((ngay_kt is null) or (ngay_kt >= " & strNgayLV & "))"

                drCCh = cnCCh.ExecuteDataReader(strSql, CommandType.Text)
                If drCCh.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tồn tại của mã cấp: " & ex.ToString)
            Finally
                drCCh.Close()
                cnCCh.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function Valid_LK(ByVal MaLoai As String, ByVal MaKhoan As String, ByVal strNgayLV As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 cặp mã LOẠI KHOẢN bất kỳ theo MLNS     
            ' Ngày viết: 28/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnLK As New DataAccess
            Dim drLK As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select TEN From TCS_DM_LOAI_KHOAN " & _
                           "Where MA_LOAI='" & MaLoai & "' And MA_KHOAN='" & MaKhoan & "'" & _
                           "and (ngay_bd <= " & strNgayLV & ") and " & _
                           "((ngay_kt is null) or (ngay_kt >= " & strNgayLV & "))"
                drLK = cnLK.ExecuteDataReader(strSql, CommandType.Text)
                If drLK.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 cặp mã LOẠI KHOẢN bất kỳ theo MLNS: " & ex.ToString)
            Finally
                drLK.Close()
                cnLK.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function Valid_MTM(ByVal MaMuc As String, ByVal MaTMuc As String, ByVal strNgayLV As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 cặp mã MỤC,TIỂU MỤC bất kỳ theo MLNS
            ' Ngày viết: 28/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnMTM As New DataAccess
            Dim drMTM As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select TEN From TCS_DM_MUC_TMUC " & _
                         "Where MA_MUC='" & MaMuc & "' And MA_TMUC='" & MaTMuc & "'" & _
                        "and (ngay_bd <= " & strNgayLV & ") and " & _
                        "((ngay_kt is null) or (ngay_kt >= " & strNgayLV & "))"

                drMTM = cnMTM.ExecuteDataReader(strSql, CommandType.Text)
                If drMTM.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 cặp mã MỤC,TIỂU MỤC bất kỳ theo MLNS: " & ex.ToString)
            Finally
                drMTM.Close()
                cnMTM.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function Valid_TLDT(ByVal MaDT As String, ByVal strNgayLV As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã TLĐT bất kỳ theo danh
            '           mục tỷ lệ điều tiết.
            ' Ngày viết: 28/12/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnCCh As New DataAccess
            Dim drCCh As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select TW From TCS_DM_TLDT Where MA_TLDT='" & MaDT & "'" & _
                        "and (ngay_bd <= " & strNgayLV & ") and " & _
                        "((ngay_kt is null) or (ngay_kt >= " & strNgayLV & "))"

                drCCh = cnCCh.ExecuteDataReader(strSql, CommandType.Text)
                If drCCh.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã TLĐT bất kỳ: " & ex.ToString)
            Finally
                drCCh.Close()
                cnCCh.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function Valid_TLDT(ByVal MaDT As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã TLĐT bất kỳ theo danh
            '           mục tỷ lệ điều tiết.
            ' Tham số: MaDT: mã TLDT cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnCCh As New DataAccess
            Dim drCCh As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select TW From TCS_DM_TLDT Where MA_TLDT='" & MaDT & "'"
                drCCh = cnCCh.ExecuteDataReader(strSql, CommandType.Text)
                If drCCh.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã TLĐT bất kỳ: " & ex.ToString)
            Finally
                drCCh.Close()
                cnCCh.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function Valid_Cap(ByVal MaCap As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã CẤP bất kỳ theo MLNS
            ' Tham số: MaCap: mã cấp cần kiểm tra
            '          Restricted: Xác định toán tử so sánh: "=" hoặc "Like" 
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnCCh As New DataAccess
            Dim drCCh As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select MA_CHUONG From TCS_DM_CAP_CHUONG " & _
                       "Where MA_CAP='" & MaCap & "'"
                drCCh = cnCCh.ExecuteDataReader(strSql, CommandType.Text)
                If drCCh.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra mã cấp: " & ex.ToString)
            Finally
                drCCh.Close()
                cnCCh.Dispose()
            End Try
            Return blnResult
        End Function
        'Public Shared Function Valid_Chuong(ByVal MaChuong As String) As Boolean
        '    '---------------------------------------------------------------
        '    ' Mục đích: Kiểm tra tính hợp lệ của 1 mã CHƯƠNG bất kỳ theo MLNS
        '    ' Tham số: MaCap: mã chương cần kiểm tra
        '    ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
        '    ' Ngày viết: 02/11/2003
        '    ' Người viết: Nguyễn Quốc Việt
        '    ' ----------------------------------------------------
        '    Dim cnCCh As New DataAccess
        '    Dim drCCh As IDataReader
        '    Dim strSql As String
        '    Dim blnResult As Boolean
        '    Try
        '        strSql = "Select MA_CAP From TCS_DM_CAP_CHUONG " & _
        '                        "Where MA_CHUONG='" & MaChuong & "'"
        '        drCCh = cnCCh.ExecuteDataReader(strSql, CommandType.Text)
        '        If drCCh.Read Then
        '            blnResult = True
        '        Else
        '            blnResult = False
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình kiểm tra mã chương: " & ex.ToString)
        '    Finally
        '        drCCh.Close()
        '        cnCCh.Dispose()
        '    End Try
        '    Return blnResult
        'End Function


        Public Shared Function Valid_CCh(ByVal MaCap As String, ByVal MaChuong As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 cặp mã CẤP CHƯƠNG bất kỳ theo MLNS
            ' Tham số: MaCap,MaChuong: mã cấp, mã chương cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnCCh As New DataAccess
            Dim drCCh As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select TEN From TCS_DM_CAP_CHUONG " & _
                        "Where MA_CAP='" & MaCap & "' And MA_CHUONG='" & _
                        MaChuong & "'"
                drCCh = cnCCh.ExecuteDataReader(strSql, CommandType.Text)
                If drCCh.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tồn tại của mã cấp: " & ex.ToString)
            Finally
                drCCh.Close()
                cnCCh.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function Valid_Chuong_Nhan(ByVal MaChuong As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 cặp mã CHƯƠNG bất kỳ theo MLNS
            ' Tham số: MaCap,MaChuong: mã cấp, mã chương cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' Người sửa : TiếpVK
            ' ----------------------------------------------------
            Dim cnCCh As New DataAccess
            Dim drCCh As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select TEN From TCS_DM_CAP_CHUONG " & _
                        "Where MA_CHUONG='" & MaChuong & "'"
                drCCh = cnCCh.ExecuteDataReader(strSql, CommandType.Text)
                If drCCh.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tồn tại của mã cấp: " & ex.ToString)
            Finally
                drCCh.Close()
                cnCCh.Dispose()
            End Try
            Return blnResult
        End Function

        Public Shared Function Valid_Loai(ByVal MaLoai As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã LOẠI bất kỳ theo MLNS
            ' Tham số: MaLoai: mã LOẠI cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnLK As New DataAccess
            Dim drLK As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select MA_KHOAN From TCS_DM_LOAI_KHOAN " & _
                        "Where MA_LOAI='" & MaLoai & "'"
                drLK = cnLK.ExecuteDataReader(strSql, CommandType.Text)
                If drLK.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của mã Loại: " & ex.ToString)
            Finally
                drLK.Close()
                cnLK.Dispose()
            End Try
            Return blnResult
        End Function
        'Public Shared Function Valid_Khoan(ByVal MaKhoan As String) As Boolean
        '    '---------------------------------------------------------------
        '    ' Mục đích: Kiểm tra tính hợp lệ của 1 mã KHOẢN bất kỳ theo MLNS
        '    ' Tham số: MaCap: mã KHOẢN cần kiểm tra
        '    ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
        '    ' Ngày viết: 02/11/2003
        '    ' Người viết: Nguyễn Quốc Việt
        '    ' ----------------------------------------------------
        '    Dim cnLK As New DataAccess
        '    Dim drLK As IDataReader
        '    Dim strSql As String
        '    Dim blnResult As Boolean
        '    Try
        '        strSql = "Select MA_LOAI From TCS_DM_LOAI_KHOAN " & _
        '               "Where MA_KHOAN='" & MaKhoan & "'"
        '        drLK = cnLK.ExecuteDataReader(strSql, CommandType.Text)
        '        If drLK.Read Then
        '            blnResult = True
        '        Else
        '            blnResult = False
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã KHOẢN bất kỳ theo MLNS: " & ex.ToString)
        '    Finally
        '        drLK.Close()
        '        cnLK.Dispose()
        '    End Try
        '    Return blnResult
        'End Function

        Public Shared Function Valid_DBHC(ByVal strDiaBan As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra Mã địa bàn có hợp lệ
            ' Tham số: Mã địa bàn
            ' Ngày viết: 08/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnDiaBan As DataAccess
            Try
                If (IsNothing(strDiaBan)) Then Return False

                cnDiaBan = New DataAccess
                Dim strSQL As String
                strSQL = "Select 1 From TCS_DM_XA " & _
                        "Where (MA_XA = '" & strDiaBan & "')"

                Dim blnReturn As Boolean = False

                Dim drDiaBan As IDataReader = cnDiaBan.ExecuteDataReader(strSQL, CommandType.Text)
                If (drDiaBan.Read()) Then
                    blnReturn = True
                End If
                drDiaBan.Close()

                If (Not IsNothing(drDiaBan)) Then drDiaBan.Dispose()
                Return blnReturn
            Catch ex As Exception
                Return False
            Finally
                If (Not IsNothing(cnDiaBan)) Then cnDiaBan.Dispose()
            End Try
        End Function
        Public Shared Function Valid_LK(ByVal MaLoai As String, ByVal MaKhoan As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 cặp mã LOẠI KHOẢN bất kỳ theo MLNS
            ' Tham số: MaLoai,MaKhoan: mã LOẠI, mã KHOẢN cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnLK As New DataAccess
            Dim drLK As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select TEN From TCS_DM_LOAI_KHOAN " & _
                        "Where MA_LOAI='" & MaLoai & "' And MA_KHOAN='" & _
                        MaKhoan & "'"
                drLK = cnLK.ExecuteDataReader(strSql, CommandType.Text)
                If drLK.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 cặp mã LOẠI KHOẢN bất kỳ theo MLNS: " & ex.ToString)
            Finally
                drLK.Close()
                cnLK.Dispose()
            End Try
            Return blnResult
        End Function

        Public Shared Function Valid_Chuong(ByVal MaChuong As String, Optional ByVal blnDangHoatDong As Boolean = False) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã CHƯƠNG bất kỳ theo MLNS
            ' Tham số: MaCap: mã chương cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnCCh As New DataAccess
            Dim drCCh As IDataReader
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                Dim strWhereTT As String = ""
                If (blnDangHoatDong) Then
                    strWhereTT = " and (tinh_trang = '1')"
                End If

                strSql = "Select 1 From TCS_DM_CAP_CHUONG " & _
                         " Where MA_CHUONG='" & MaChuong & "' " & strWhereTT
                drCCh = cnCCh.ExecuteDataReader(strSql, CommandType.Text)
                If drCCh.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra mã chương: " & ex.ToString)
            Finally
                drCCh.Close()
                cnCCh.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function Valid_Khoan(ByVal MaKhoan As String, Optional ByVal blnDangHoatDong As Boolean = False) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã KHOẢN bất kỳ theo MLNS
            ' Tham số: MaCap: mã KHOẢN cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnLK As New DataAccess
            Dim drLK As String
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                Dim strWhereTT As String = ""
                If (blnDangHoatDong) Then
                    strWhereTT = " and (tinh_trang = '1')"
                End If

                strSql = "Select 1 From TCS_DM_LOAI_KHOAN " & _
                       " Where MA_KHOAN='" & MaKhoan & "' " & strWhereTT
                drLK = cnLK.ExecuteSQLScalar(strSql)
                If Not drLK.ToString() Is Nothing Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã KHOẢN bất kỳ theo MLNS: " & ex.ToString)
            Finally
                cnLK.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function Valid_MaQuy(ByVal MaQuy As String, Optional ByVal blnDangHoatDong As Boolean = False) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã quỹ
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 21/11/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnMTM As New DataAccess
            Dim drMTM As String
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                Dim strWhereTT As String = ""
                If (blnDangHoatDong) Then
                    strWhereTT = " and (tinh_trang = '1')"
                End If

                strSql = "Select Ma From TCS_DM_MAQUY " & _
                        " Where (Ma='" & MaQuy & "') " & strWhereTT
                drMTM = cnMTM.ExecuteSQLScalar(strSql)
                If Not drMTM.ToString() Is Nothing Then
                    blnResult = True
                Else
                    blnResult = False
                End If

                Return blnResult
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã quỹ: " & ex.ToString)
            Finally
                cnMTM.Dispose()
            End Try
        End Function
        Public Shared Function Valid_TMuc(ByVal MaTMuc As String, Optional ByVal blnDangHoatDong As Boolean = False) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã TIỂU MỤC bất kỳ theo MLNS
            ' Tham số: MaTMuc: mã TIỂU MỤC cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnMTM As New DataAccess
            Dim drMTM As String
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                Dim strWhereTT As String = ""
                If (blnDangHoatDong) Then
                    strWhereTT = " and (tinh_trang = '1')"
                End If

                strSql = "Select 1 From TCS_DM_MUC_TMUC " & _
                        "Where (MA_TMUC='" & MaTMuc & "') " & strWhereTT
                drMTM = cnMTM.ExecuteSQLScalar(strSql)
                If Not drMTM.ToString() Is Nothing Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã TIỂU MỤC bất kỳ theo MLNS: " & ex.ToString)
            Finally
                cnMTM.Dispose()
            End Try
            Return blnResult
        End Function

        Public Shared Function Valid_Muc(ByVal MaMuc As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã MỤC bất kỳ theo MLNS
            ' Tham số: MaMuc: mã MỤC cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnMTM As New DataAccess
            Dim drMTM As String
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select MA_TMUC From TCS_DM_MUC_TMUC " & _
                        "Where MA_MUC='" & MaMuc & "'"
                drMTM = cnMTM.ExecuteSQLScalar(strSql)
                If Not drMTM.ToString() Is Nothing Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã MỤC bất kỳ theo MLNS: " & ex.ToString)
            Finally
                cnMTM.Dispose()
            End Try
            Return blnResult
        End Function
        'Public Shared Function Valid_TMuc(ByVal MaTMuc As String) As Boolean
        '    '---------------------------------------------------------------
        '    ' Mục đích: Kiểm tra tính hợp lệ của 1 mã TIỂU MỤC bất kỳ theo MLNS
        '    ' Tham số: MaTMuc: mã TIỂU MỤC cần kiểm tra
        '    ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
        '    ' Ngày viết: 02/11/2003
        '    ' Người viết: Nguyễn Quốc Việt
        '    ' ----------------------------------------------------
        '    Dim cnMTM As New DataAccess
        '    Dim drMTM As IDataReader
        '    Dim strSql As String
        '    Dim blnResult As Boolean
        '    Try
        '        strSql = "Select MA_MUC From TCS_DM_MUC_TMUC " & _
        '                "Where MA_TMUC='" & MaTMuc & "'"
        '        drMTM = cnMTM.ExecuteDataReader(strSql, CommandType.Text)
        '        If drMTM.Read Then
        '            blnResult = True
        '        Else
        '            blnResult = False
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã TIỂU MỤC bất kỳ theo MLNS: " & ex.ToString)
        '    Finally
        '        drMTM.Close()
        '        cnMTM.Dispose()
        '    End Try
        '    Return blnResult
        'End Function
        Public Shared Function Valid_MTM(ByVal MaMuc As String, ByVal MaTMuc As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 cặp mã MỤC,TIỂU MỤC bất kỳ theo MLNS
            ' Tham số: MaMuc,MaTMuc: mã MỤC, mã TIỂU MỤC cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnMTM As New DataAccess
            Dim drMTM As String
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                strSql = "Select TEN From TCS_DM_MUC_TMUC " & _
                        "Where MA_MUC='" & MaMuc & "' And MA_TMUC='" & _
                        MaTMuc & "'"
                drMTM = cnMTM.ExecuteSQLScalar(strSql)
                If Not drMTM.ToString() Is Nothing Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 cặp mã MỤC,TIỂU MỤC bất kỳ theo MLNS: " & ex.ToString)
            Finally
                cnMTM.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function TCS_KT_NNT(ByVal strMaNNT As String) As Boolean
            '-----------------------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của Mã ĐTNT trong bảng DTU_DM_DTNT
            ' Tham số: strMa_DTNT
            ' Giá trị trả về:   + True: Nếu có tồn tại
            '                   + False: Nếu không tồn tại
            ' Ngày viết: 12/02/2004
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------------------
            Dim ConnKT As New DataAccess
            Dim drDataKT As String
            Dim strSQLKT As String
            Dim blnTmp As Boolean
            '
            strSQLKT = "Select Ma_NNT From TCS_DM_NNT Where Ma_NNT = '" & strMaNNT & "'"
            Try
                drDataKT = ConnKT.ExecuteSQLScalar(strSQLKT)
                If Not drDataKT.ToString() Is Nothing Then
                    blnTmp = True
                Else
                    blnTmp = False
                End If
                ConnKT.Dispose()
            Catch ex As Exception
                blnTmp = False
            End Try
            Return blnTmp
        End Function
#End Region

#Region "Valid Địa bàn"
        Public Shared Function Valid_CQThu(ByVal MaCQThu As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã cơ quan thu bất kỳ theo
            '           danh mục hệ thống.
            ' Tham số: MaCQThu: mã cơ quan cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnCQ As New DataAccess
            Dim drCQ As String
            Dim strSQL As String
            Dim blnResult As Boolean

            Try
                strSQL = "Select MA_CQTHU From TCS_DM_CQTHU Where MA_CQTHU='" & MaCQThu & "'"
                drCQ = cnCQ.ExecuteSQLScalar(strSQL)
                If Not drCQ.ToString() Is Nothing Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã cơ quan thu bất kỳ theo danh mục hệ thống: " & ex.ToString)
            Finally
                cnCQ.Dispose()
            End Try
            Return blnResult
        End Function
        Public Shared Function Valid_LHXNK(ByVal Ma_LH As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã loại hình xuất nhập khẩu
            '           bất kỳ theo danh mục hệ thống.
            ' Tham số: Ma_LH: mã cơ quan cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 19/03/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnCQ As New DataAccess
            Dim drCQ As String
            Dim strSQL As String
            Dim blnResult As Boolean

            Try
                strSQL = "Select ma_lh From TCS_DM_LHINH Where Ma_LH ='" & Ma_LH & "'"
                drCQ = cnCQ.ExecuteSQLScalar(strSQL)
                If Not drCQ.ToString() Is Nothing Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã cơ quan thu bất kỳ theo danh mục hệ thống: " & ex.ToString)
            Finally
                cnCQ.Dispose()
            End Try
            Return blnResult
        End Function

        Public Shared Function Valid_LHXNK_TenVT(ByVal Ten_VT As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 tên viết tắt loại hình xuất nhập khẩu
            '           bất kỳ theo danh mục hệ thống.
            ' Tham số: Ten_VT: Tên viết tắt
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 14/10/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnCQ As New DataAccess
            Dim drCQ As IDataReader
            Dim strSQL As String

            Try
                If (Ten_VT.Trim() = "") Then Return False

                strSQL = "Select * From TCS_DM_LHINH Where Ten_VT ='" & Ten_VT.ToUpper() & "'"
                drCQ = cnCQ.ExecuteDataReader(strSQL, CommandType.Text)
                Dim blnResult As Boolean = False
                If drCQ.Read Then blnResult = True

                Return blnResult
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra tính hợp lệ của 1 mã cơ quan thu bất kỳ theo danh mục hệ thống: " & ex.ToString)
            Finally
                If (Not IsNothing(drCQ)) Then drCQ.Close()
                If (Not IsNothing(cnCQ)) Then cnCQ.Dispose()
            End Try

        End Function

        Public Shared Function Valid_Tinh(ByVal fstrMaTinh As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã Tỉnh đã được khai báo
            '           trong tham số hệ thống.
            ' Tham số: 
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/08/2005
            ' Người viết: Vũ Hoàng Long
            ' ----------------------------------------------------
            Dim cnQTinh As New DataAccess
            Dim drTinh As IDataReader
            Dim strSQL As String
            Dim blnResult As Boolean

            Try
                strSQL = "Select MA_TINH " & _
                    "From TCS_DM_TINH " & _
                    "Where MA_TINH='" & fstrMaTinh & "'"
                drTinh = cnQTinh.ExecuteDataReader(strSQL, CommandType.Text)
                If drTinh.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
                ' Giải phóng Connection tới CSDL
                drTinh.Close()
                cnQTinh.Dispose()
            Catch ex As Exception
                If Not drTinh Is Nothing Then
                    drTinh.Close()
                End If
                If Not cnQTinh Is Nothing Then
                    cnQTinh.Dispose()
                End If
                MsgBox("Lỗi trong qúa trình kiểm tra mã tỉnh !", MsgBoxStyle.Critical, "Chú ý")
                'LogDebug.Writelog("Lỗi trong qúa trình kiểm tra mã tỉnh ! - " & ex.ToString)
                ' Lỗi
                blnResult = False
            End Try
            ' Trả lại kết quả cho hàm
            Return blnResult
        End Function
        'Public Shared Function Valid_Huyen(ByVal fstrMaHuyen As String) As Boolean
        '    '---------------------------------------------------------------
        '    ' Mục đích: Kiểm tra tính hợp lệ của 1 mã HUYỆN bất kỳ theo mã
        '    '           Tỉnh/TP đã được khai báo trong tham số hệ thống.
        '    ' Tham số: MaHuyen: mã HUYỆN cần kiểm tra
        '    ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
        '    ' Ngày viết: 02/11/2003
        '    ' Người viết: Nguyễn Quốc Việt
        '    ' ----------------------------------------------------
        '    Dim cnQHuyen As New DataAccess
        '    Dim drHuyen As IDataReader
        '    Dim strSQL As String
        '    Dim blnResult As Boolean

        '    Try
        '        strSQL = "Select MA_HUYEN,TEN " & _
        '            "From TCS_DM_HUYEN " & _
        '            "Where MA_TINH='" & mdlSystemVariables.gstrMaTTP & _
        '                "' And MA_HUYEN='" & fstrMaHuyen & "'"
        '        drHuyen = cnQHuyen.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drHuyen.Read Then
        '            blnResult = True
        '        Else
        '            blnResult = False
        '        End If
        '        ' Giải phóng Connection tới CSDL
        '        drHuyen.Close()
        '        cnQHuyen.Dispose()
        '    Catch ex As Exception
        '        If Not drHuyen Is Nothing Then
        '            drHuyen.Close()
        '        End If
        '        If Not cnQHuyen Is Nothing Then
        '            cnQHuyen.Dispose()
        '        End If
        '        MsgBox("Lỗi trong qúa trình kiểm tra mã huyện !", MsgBoxStyle.Critical, "Chú ý")
        '        'LogDebug.Writelog("Lỗi trong qúa trình kiểm tra mã huyện ! - " & ex.ToString)
        '        ' Lỗi
        '        blnResult = False
        '    End Try
        '    ' Trả lại kết quả cho hàm
        '    Return blnResult
        'End Function
        Public Shared Function Valid_KT_Xa(ByVal fstrMaTinh As String, ByVal fstrMaHuyen As String, ByVal strMaXa As String) As Boolean
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tính hợp lệ của 1 mã XÃ bất kỳ theo mã
            '           Tỉnh/TP, mã Huyện đã được khai báo trong tham số hệ thống.
            ' Tham số: MaXa: mã XÃ cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnQXa As New DataAccess
            Dim drXa As IDataReader
            Dim strSQL As String
            Dim blnResult As Boolean

            Try
                strSQL = "Select MA_XA " & _
                    "From TCS_DM_XA " & _
                    "Where  MA_TINH = '" & _
                        fstrMaTinh & "' And MA_HUYEN = '" & _
                        fstrMaHuyen & "' And MA_XA = '" & strMaXa & "'"
                drXa = cnQXa.ExecuteDataReader(strSQL, CommandType.Text)
                If drXa.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
                ' Giải phóng Connection tới CSDL
                drXa.Close()
                cnQXa.Dispose()
            Catch ex As Exception
                If Not drXa Is Nothing Then
                    drXa.Close()
                End If
                If Not cnQXa Is Nothing Then
                    cnQXa.Dispose()
                End If
                MsgBox("Lỗi trong qúa trình kiểm tra mã xã !", MsgBoxStyle.Critical, "Chú ý")
                'LogDebug.Writelog("Lỗi trong qúa trình kiểm tra mã xã ! - " & ex.ToString)
                ' Lỗi
                blnResult = False
            End Try
            ' Trả lại kết quả cho hàm
            Return blnResult
        End Function
        Public Shared Function Valid_NganHang(ByVal strMaNH As String)
            '------------------------------------------------------------------
            ' Mục đích: Kiểm tra mã ngân hàng
            ' Tham số: 
            ' Giá trị trả về: 
            ' Ngày viết: 13/11/2007
            ' Người viết: Lê Hồng Hà
            ' -----------------------------------------------------------------
            If (IsNothing(strMaNH)) Then Return False

            Dim strResult As String = ""
            Dim conn As DataAccess
            Dim drNH As IDataReader
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Select 1 From TCS_DM_NGANHANG Where Ma = '" & strMaNH & "'"
                drNH = conn.ExecuteDataReader(strSql, CommandType.Text)

                Dim blnReturn As Boolean = False
                If drNH.Read Then
                    blnReturn = True
                End If
                drNH.Close()

                Return blnReturn
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra sự tồn tại của mã ngân hàng: " & ex.ToString)
                Return ""
            Finally
                If Not drNH Is Nothing Then
                    drNH.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return strResult
        End Function

#End Region

#Region "Các hàm khác"
        'Public Shared Function Get_MaTLDT(ByVal strMaHuyen As String, ByVal strMaXa As String, _
        '    ByVal strMLNS As String, ByVal strMaCQThu As String) As String
        '    '------------------------------------------------------------------
        '    ' Mục đích: Lấy mã tỉ lệ điều tiết
        '    ' Tham số: 
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 27/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' -----------------------------------------------------------------
        '    Dim cnTLDT As DataAccess
        '    Dim drTLDT As IDataReader
        '    Dim strSQL As String

        '    strSQL = "Select MA_TLDT From TCS_DIEUTIET dt, TCS_DM_TLDT tldt " & _
        '                    "Where (MA_HUYEN='" & strMaHuyen & "') and (MA_XA='" & strMaXa & "') " & _
        '                    "and ('" & strMLNS & "' like MLNS) AND (mlns <> '_____________') and (MA_CQTHU='" & strMaCQThu & "') " & _
        '                    "and (dt.DT_ID=tldt.DT_ID) " & _
        '                    "and (ngay_bd <= " & mdlSystemVariables.gdtmNgayLV & ") and " & _
        '                    "((ngay_kt is null) or (ngay_kt >= " & mdlSystemVariables.gdtmNgayLV & ")) " & _
        '                    "order by MLNS asc"
        '    Try
        '        Dim strMaTLDT As String = ""
        '        cnTLDT = New DataAccess
        '        drTLDT = cnTLDT.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drTLDT.Read Then
        '            strMaTLDT = drTLDT("MA_TLDT").ToString()
        '        Else
        '            drTLDT.Close()
        '            strSQL = "Select MA_TLDT From TCS_DIEUTIET dt, TCS_DM_TLDT tldt " & _
        '                    "Where ('" & strMLNS & "' like MLNS) AND (mlns <> '_____________') " & _
        '                    "and (dt.DT_ID=tldt.DT_ID) " & _
        '                    "and (ngay_bd <= " & mdlSystemVariables.gdtmNgayLV & ") and " & _
        '                    "((ngay_kt is null) or (ngay_kt >= " & mdlSystemVariables.gdtmNgayLV & ")) " & _
        '                    "order by MLNS asc"

        '            drTLDT = cnTLDT.ExecuteDataReader(strSQL, CommandType.Text)

        '            If (drTLDT.Read) Then
        '                strMaTLDT = drTLDT("MA_TLDT").ToString()
        '            End If
        '        End If
        '        Return strMaTLDT
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin mã tldt: " & ex.ToString)
        '        Return ""
        '    Finally
        '        If Not drTLDT Is Nothing Then drTLDT.Close()
        '        If Not cnTLDT Is Nothing Then cnTLDT.Dispose()
        '    End Try
        'End Function
        'Public Shared Function Get_MaTLDT(ByVal strMLNS As String) As String
        '    '------------------------------------------------------------------
        '    ' Mục đích: Lấy mã tỉ lệ điều tiết
        '    ' Tham số: 
        '    ' Giá trị trả về: 
        '    ' Ngày viết: 27/11/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' -----------------------------------------------------------------
        '    Dim cnTLDT As DataAccess
        '    Dim drTLDT As IDataReader
        '    Dim strSQL As String

        '    strSQL = "Select MA_TLDT From TCS_DIEUTIET dt, TCS_DM_TLDT tldt " & _
        '                    "Where ('" & strMLNS & "' like MLNS) " & _
        '                    "and (dt.DT_ID=tldt.DT_ID) " & _
        '                    "and (ngay_bd <= " & mdlSystemVariables.gdtmNgayLV & ") and " & _
        '                    "((ngay_kt is null) or (ngay_kt >= " & mdlSystemVariables.gdtmNgayLV & ")) " & _
        '                    "order by MLNS asc"

        '    Try
        '        Dim strMaTLDT As String = ""
        '        cnTLDT = New DataAccess
        '        drTLDT = cnTLDT.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drTLDT.Read Then
        '            strMaTLDT = drTLDT("MA_TLDT").ToString()
        '        End If
        '        Return strMaTLDT
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin mã tldt: " & ex.ToString)
        '        Return ""
        '    Finally
        '        If Not drTLDT Is Nothing Then drTLDT.Close()
        '        If Not cnTLDT Is Nothing Then cnTLDT.Dispose()
        '    End Try
        'End Function
        Public Shared Function Get_MaTLDT(ByVal strMaDBHC As String, ByVal strMaCQThu As String, _
       ByVal strMaChuong As String, ByVal strMaKhoan As String, ByVal strMaTMuc As String, _
       Optional ByVal strDefaultTLDT As String = "") As String
            '------------------------------------------------------------------
            ' Mục đích: Lấy mã tỉ lệ điều tiết
            ' Tham số: 
            ' Giá trị trả về: 
            ' Ngày viết: 27/11/2007
            ' Người viết: Lê Hồng Hà
            ' -----------------------------------------------------------------
            Dim cnTLDT As DataAccess
            Dim drTLDT As IDataReader
            Dim strSQL As String

            strSQL = "select tldt.madt as MaDT " & _
                    "from tcs_dm_tldt tldt, tcs_dm_dieutiet dt " & _
                    "where (tldt.madt = dt.madt) and (tldt.tinh_trang = '1') " & _
                    "and (dt.ma_db = '" & strMaDBHC & "') and (dt.ma_cqthu = '" & strMaCQThu & "') " & _
                    "and (dt.tu_chuong <= '" & strMaChuong & "') and (dt.den_chuong >= '" & strMaChuong & "') " & _
                    "and (dt.tu_lk <= '" & strMaKhoan & "') and (dt.den_lk >= '" & strMaKhoan & "')  " & _
                    "and (dt.tu_muc <= '" & strMaTMuc & "') and (dt.den_muc >= '" & strMaTMuc & "') " & _
                    "order by tldt.madt "
            Try
                Dim strMaTLDT As String = ""
                cnTLDT = New DataAccess
                drTLDT = cnTLDT.ExecuteDataReader(strSQL, CommandType.Text)
                If drTLDT.Read Then
                    strMaTLDT = drTLDT("MADT").ToString()
                Else
                    drTLDT.Close()
                    strSQL = "select tldt.madt as MaDT " & _
                        "from tcs_dm_tldt tldt, tcs_dm_dieutiet dt " & _
                        "where (tldt.madt = dt.madt) and (tldt.tinh_trang = '1') " & _
                        "and (dt.ma_cqthu = '" & strMaCQThu & "') " & _
                        "and (dt.tu_chuong <= '" & strMaChuong & "') and (dt.den_chuong >= '" & strMaChuong & "') " & _
                        "and (dt.tu_lk <= '" & strMaKhoan & "') and (dt.den_lk >= '" & strMaKhoan & "')  " & _
                        "and (dt.tu_muc <= '" & strMaTMuc & "') and (dt.den_muc >= '" & strMaTMuc & "') " & _
                        "order by tldt.madt "

                    drTLDT = cnTLDT.ExecuteDataReader(strSQL, CommandType.Text)

                    If (drTLDT.Read) Then
                        strMaTLDT = drTLDT("MaDT").ToString()
                    Else
                        drTLDT.Close()
                        strSQL = "select tldt.madt as MaDT " & _
                            "from tcs_dm_tldt tldt, tcs_dm_dieutiet dt " & _
                            "where (tldt.madt = dt.madt) and (tldt.tinh_trang = '1') " & _
                            "and (dt.tu_chuong <= '" & strMaChuong & "') and (dt.den_chuong >= '" & strMaChuong & "') " & _
                            "and (dt.tu_lk <= '" & strMaKhoan & "') and (dt.den_lk >= '" & strMaKhoan & "')  " & _
                            "and (dt.tu_muc <= '" & strMaTMuc & "') and (dt.den_muc >= '" & strMaTMuc & "') " & _
                            "order by tldt.madt "
                        drTLDT = cnTLDT.ExecuteDataReader(strSQL, CommandType.Text)

                        If (drTLDT.Read) Then
                            strMaTLDT = drTLDT("MaDT").ToString()
                        Else
                            strMaTLDT = strDefaultTLDT
                        End If
                    End If
                End If
                Return strMaTLDT
            Catch ex As Exception
                Return ""
            Finally
                If Not drTLDT Is Nothing Then drTLDT.Close()
                If Not cnTLDT Is Nothing Then cnTLDT.Dispose()
            End Try
        End Function
        Public Shared Function Get_TenCQQDCap(ByVal strMaCQQD As String) As DataSet
            Dim strSql As String
            Dim conn As DataAccess
            Dim ds As DataSet
            Try
                conn = New DataAccess
                strSql = "Select ten_cqqd, cap,ma_lh,ghi_chu From tcs_dm_cqqd Where ma_cqqd ='" & strMaCQQD & "'"
                ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên cơ quan quyết định: " & ex.ToString)
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return ds
        End Function
        Public Shared Function TCS_KT_CQTHU(ByVal strMa_CQThu As String) As Boolean
            '-----------------------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của mã cơ quan Thu trong bảng TCS_DM_CQTHU
            ' Tham số: strMa_CQThu
            ' Giá trị trả về:   + True: Nếu có tồn tại
            '                   + False: Nếu không tồn tại
            ' Ngày viết: 19/08/2004
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------------------
            Dim ConnKT As New DataAccess
            Dim drDataKT As IDataReader
            Dim strSQLKT As String
            Dim blnTmp As Boolean
            '
            strSQLKT = "Select Ma_CQThu From TCS_DM_CQTHU Where Ma_CQThu = '" & strMa_CQThu & "'"
            Try
                drDataKT = ConnKT.ExecuteDataReader(strSQLKT, CommandType.Text)
                If drDataKT.Read Then
                    blnTmp = True
                Else
                    blnTmp = False
                End If
                drDataKT.Close()
                ConnKT.Dispose()
            Catch ex As Exception
                blnTmp = False
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra sự tồn tại của mã cơ quan Thu trong bảng TCS_DM_CQTHU: " & ex.ToString)
            End Try
            Return blnTmp
        End Function
        Public Shared Function TCS_KT_MAKBAC(ByVal strMaKBac As String) As Boolean
            '-----------------------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của mã cơ quan Thu trong bảng TCS_DM_CQTHU
            ' Tham số:strMaKbac
            ' Giá trị trả về:   + True: Nếu có tồn tại
            '                   + False: Nếu không tồn tại
            ' Ngày viết: 21/11/2007
            ' Người viết: Nguyễn Bá Cường
            ' ----------------------------------------------------------------
            Dim ConnKT As New DataAccess
            Dim drDataKT As IDataReader
            Dim strSQLKT As String
            Dim blnTmp As Boolean
            '
            strSQLKT = "Select SHKB From TCS_DM_KHOBAC Where SHKB = '" & strMaKBac & "'"
            Try
                drDataKT = ConnKT.ExecuteDataReader(strSQLKT, CommandType.Text)
                If drDataKT.Read Then
                    blnTmp = True
                Else
                    blnTmp = False
                End If
                drDataKT.Close()
                ConnKT.Dispose()
            Catch ex As Exception
                blnTmp = False
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra sự tồn tại của mã cơ quan Kbạc trong bảng TCS_DM_KHOBAC: " & ex.ToString)
            End Try
            Return blnTmp
        End Function


        Public Shared Function TCS_KT_TINH_HUYEN_XA(ByVal fstrMa_Tinh As String, _
        ByVal fstrMa_Huyen As String, ByVal fstrMa_Xa As String) As Boolean
            '-----------------------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của mã Tỉnh, mã Huyện, mã Xã trong hệ thống
            ' Tham số: fstrMa_Tinh, fstrMa_Huyen, fstrMa_Xa
            ' Giá trị trả về:   + True: Nếu có tồn tại
            '                   + False: Nếu không tồn tại
            ' Ngày viết: 12/02/2004
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------------------
            Dim ConnKT As New DataAccess
            Dim drDataKT As IDataReader
            Dim strSQLKT As String
            Dim blnTmp As Boolean
            '
            strSQLKT = "Select Ma_Tinh,Ma_Huyen,Ma_Xa From TCS_DM_XA " & _
                       "Where (Ma_Tinh = '" & Trim(fstrMa_Tinh) & "') And " & _
                       "(Ma_Huyen = '" & Trim(fstrMa_Huyen) & "') And  " & _
                       "(Ma_Xa = '" & Trim(fstrMa_Xa) & "')"
            '
            Try
                drDataKT = ConnKT.ExecuteDataReader(strSQLKT, CommandType.Text)
                If drDataKT.Read Then
                    blnTmp = True
                Else
                    blnTmp = False
                End If
                drDataKT.Close()
                ConnKT.Dispose()
            Catch ex As Exception
                blnTmp = False
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra sự tồn tại của mã Tỉnh, mã Huyện, mã Xã trong hệ thống: " & ex.ToString)
            End Try
            Return blnTmp
        End Function
        Public Shared Function TCS_KT_XA(ByVal fstrMa_Xa As String) As Boolean
            '-----------------------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của mã Xã trong hệ thống
            ' Tham số: fstrMa_Tinh, fstrMa_Huyen, fstrMa_Xa
            ' Giá trị trả về:   + True: Nếu có tồn tại
            '                   + False: Nếu không tồn tại
            ' Ngày viết: 12/02/2004
            ' Người viết: Nguyễn Quốc Việt
            ' Người sửa: TiếpVK
            ' ----------------------------------------------------------------
            Dim ConnKT As New DataAccess
            Dim drDataKT As IDataReader
            Dim strSQLKT As String
            Dim blnTmp As Boolean
            '
            strSQLKT = "Select Ma_Tinh,Ma_Huyen,Ma_Xa From TCS_DM_XA " & _
                       "Where (Ma_Xa = '" & Trim(fstrMa_Xa) & "')"
            '
            Try
                drDataKT = ConnKT.ExecuteDataReader(strSQLKT, CommandType.Text)
                If drDataKT.Read Then
                    blnTmp = True
                Else
                    blnTmp = False
                End If
                drDataKT.Close()
                ConnKT.Dispose()
            Catch ex As Exception
                blnTmp = False
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra sự tồn tại của mã Tỉnh, mã Huyện, mã Xã trong hệ thống: " & ex.ToString)
            End Try
            Return blnTmp
        End Function

#End Region

#Region "Danh mục"
        Public Shared Function Get_DM_LoaiThue() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục loại thuế.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 16/10/2007
            ' Người viết: Lê Hồng Hà
            ' ---------------------------------------------------- 
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "Select MA_LTHUE,TEN " & _
                    "From TCS_DM_LTHUE " & _
                    "Order By MA_LTHUE"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                ''LogDebug.Writelog("Lỗi trong quá trình lấy danh mục loại thuế: " & ex.ToString)
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
        Public Shared Function Get_DM_NhanVien() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy danh mục Nhân viên.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 22/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------        
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSQL As String
                strSQL = "Select a.ma_nv, a.ten " & _
                "From TCS_DM_NHANVIEN a " & _
                "order by a.ten"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function
#End Region
#End Region

#Region "Xử lý string"
        Public Shared Function GetString(ByVal objInput As Object) As String
            '-------------------------------------------------------------------------------------
            ' Mục đích: Chuyển một đối tượng thành string
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 15/05/2008
            '--------------------------------------------------------------------------------------
            Try
                If (IsNothing(objInput)) Then Return ""
                If (IsDBNull(objInput)) Then Return ""
                Return objInput.ToString()
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function ChuanHoa_SoCT(ByVal InputStr As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Chuẩn hoá số chứng từ về dạng xâu có đúng 7 ký tự
            ' Tham số: InputStr: số CT chưa chuẩn hoá
            ' Giá trị trả về: Số CT đã chuẩn hoá
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Try
                If InputStr.Length = 7 Then
                    Return InputStr
                ElseIf InputStr.Length < 7 Then
                    Dim strTemp As String = InputStr
                    Do While strTemp.Length < 7
                        strTemp = "0" + strTemp
                    Loop
                    Return strTemp
                ElseIf InputStr.Length > 7 Then
                    Return Mid(InputStr, 1, 7)
                End If
            Catch ex As Exception
                Return "0000001"
            End Try
        End Function
        Public Shared Function ChuanHoa_SoTK(ByVal InputStr As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Chuẩn hoá số tai khoan về dạng xâu khong có ký tự "."
            ' Tham số: InputStr: số TK chưa chuẩn hoá
            ' Giá trị trả về: Số TK đã chuẩn hoá
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            If InputStr.Length = 0 Then
                Return ""
            Else
                Return InputStr.Replace(".", "")
            End If
        End Function
        Public Shared Function ChuanHoaXau(ByVal strXauBD As String, ByVal intMaxLen As Integer) As String
            '---------------------------------------------------------------
            ' Mục đích: Chuẩn hoá xâu ký tự về dạng xâu đã đựơc chuẩn hoá.
            '           Xâu chuẩn hoá: Dấu nháy đơn(') bằng dấu nháy kép("),
            '           có chiều dài không vượt quá intMaxLen
            ' Tham số: strXauBD, intMaxLen
            ' Giá trị trả về: Xâu đã được chuẩn hoá
            ' Ngày viết: 08/04/2004
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            If strXauBD.Length = 0 Then
                Return ""
            Else
                Dim strTemp As String = ""
                Dim i As Integer
                For i = 0 To strXauBD.Length - 1
                    If strXauBD.Chars(i) = "'" Then
                        strTemp += """"
                    Else
                        strTemp += strXauBD.Chars(i)
                    End If
                    If i = intMaxLen - 1 Then
                        Exit For
                    End If
                Next
                Return strTemp
            End If
        End Function
        Public Shared Sub TachXau(ByVal InputStr As String, ByRef OutputArray() As String)
            '---------------------------------------------------------------
            ' Mục đích: Phân tích 1 xâu gồm nhiều giá trị phân cách nhau
            '           bởi dấu ";" và đưa các giá trị này vào 1 mảng.
            ' Tham số: InputStr - Xâu cần phân tích
            '          OutputArray: Mảng rỗng. 
            ' Giá trị trả về: OutputArray: Mảng chứa các giá trị tách được.
            ' Ngày viết: 30/10/2003
            ' Người viết: Nguyễn Quốc Việt
            ' --------------------------------------------------------------
            If InStr(InputStr, ";") = 1 Then
                InputStr = Mid(InputStr, 2)
            End If
            Dim i As Integer = 1
            Do While InputStr <> ""
                ReDim Preserve OutputArray(i)
                If InStr(InputStr, ";") = 0 Then
                    OutputArray(i) = InputStr
                    Exit Do
                Else
                    OutputArray(i) = Mid(InputStr, 1, InputStr.IndexOf(";"))
                    InputStr = Mid(InputStr, InputStr.IndexOf(";") + 2)
                End If
                i += 1
            Loop
        End Sub
        Public Shared Function oToString(ByVal strValue As Object, ByVal strType As String) As String
            '-----------------------------------------------------------------
            ' Mục đích: Đổi các kiểu giá trị thành kiểu xâu ký tự. Dung cho Update
            ' Tham số: strValue,strType
            ' Giá trị trả về:
            ' Ngày viết: 12/02/2004
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------------------
            Dim strTmp As String

            If strValue Is System.DBNull.Value Then
                strTmp = "Null"
            Else
                If strType = "D" Then
                    strTmp = Format(CDate(strValue), "dd/MM/yyyy")
                ElseIf strType = "N" Then
                    strTmp = Format(Val(strValue), "###") ' khong can dinh dang vi tinh tong quat
                    If strTmp.Trim = "" Then
                        strTmp = "0"
                    End If
                ElseIf strType = "C" Then
                    strTmp = "'" & strValue & "'"
                End If
            End If
            Return strTmp
        End Function
        Public Shared Function strFormatTK(ByVal strSoTK As String) As String
            '-----------------------------------------------------------------
            ' Mục đích: Format số tài khoản theo định dạng 999.99.99.99999
            ' Tham số:   strSoTK : Chuỗi ký tự cần định dạng
            ' Giá trị trả về:
            ' Ngày viết: 26/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------------------
            If (strSoTK.Length < 12) Then Return strSoTK
            Dim strtmpSoTK As String = strSoTK.Replace(".", "")

            strtmpSoTK = strtmpSoTK.Insert(7, ".")
            strtmpSoTK = strtmpSoTK.Insert(5, ".")
            strtmpSoTK = strtmpSoTK.Insert(3, ".")

            Return strtmpSoTK
        End Function
        Public Shared Function strFormatDBHC(ByVal strMaDBHC As String) As String
            '-----------------------------------------------------------------
            ' Mục đích: Format mã địa bàn hành chính theo định dạng 999.99.99
            ' Tham số:   strMaDBHC : Chuỗi ký tự cần định dạng
            ' Giá trị trả về:
            ' Ngày viết: 26/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------------------
            If (strMaDBHC.Length < 7) Then Return strMaDBHC
            Dim i As Integer
            Dim strtmpMaDBHC As String = strMaDBHC

            strtmpMaDBHC = strtmpMaDBHC.Insert(5, ".")
            strtmpMaDBHC = strtmpMaDBHC.Insert(3, ".")

            Return strtmpMaDBHC
        End Function
#End Region

#Region "Xử lý ngày - tháng"
        Public Shared Function Get_NgayDieuChinh(ByVal strNgayLV As String) As Long
            '------------------------------------------------------------------
            ' Mục đích: Lấy ngày ĐC      
            ' Ngày viết: 18/17/2008
            ' Người viết: Lê Hồng Hà
            ' -----------------------------------------------------------------
            Dim lngNgayDC As Long
            Dim conn As DataAccess
            Dim drDC As IDataReader
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Select ngay_dc FROM tcs_khoaso Where ngay_lv = " & strNgayLV
                drDC = conn.ExecuteDataReader(strSql, CommandType.Text)
                If drDC.Read() Then
                    lngNgayDC = drDC.GetValue(0)
                End If

                Return lngNgayDC
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ngày điều chỉnh: " & ex.ToString)
                Throw ex
            Finally
                If Not drDC Is Nothing Then
                    drDC.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try

        End Function
        Public Shared Function ChuanHoa_Date(ByVal strText As String) As String
            '-----------------------------------------------------
            ' Mục đích: Chuyển định dạng ngày tháng "MM/dd/yyyy hh:mm:ss" từ database ra dạng "dd/MM/yyyy"
            ' Tham số: string đầu vào
            ' Giá trị trả về: 
            ' Ngày viết: 06/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Try
                If (strText.Trim() = "") Then Return ""

                Dim strValue() As String = strText.Split(" ")
                If (strValue.Length < 1) Then Return ""

                strValue = strValue(0).Split("/")

                If (strValue.Length <> 3) Then Return ""

                Return (strValue(1).PadLeft(2, "0") & "/" & strValue(0).PadLeft(2, "0") & "/" & strValue(2).PadLeft(4, "0"))
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function ChuanHoa_NgayToKhai(ByVal strText As String) As String
            '-----------------------------------------------------
            ' Mục đích: Chuyển định dạng ngày tháng "yyyy-MM-dd hh:mm:ss" từ danh sách tờ khai ra dạng "dd/MM/yyyy"
            ' Tham số: string đầu vào
            ' Giá trị trả về: 
            ' Ngày viết: 06/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Try
                If (strText.Trim() = "") Then Return ""

                Dim strValue() As String = strText.Split(" ")
                If (strValue.Length < 1) Then Return ""

                strValue = strValue(0).Split("-")

                If (strValue.Length <> 3) Then Return ""

                Return (strValue(2) & "/" & strValue(1) & "/" & strValue(0))
            Catch ex As Exception
                Return ""
            End Try
        End Function

        Public Shared Function FindSeprate(ByVal strInput As String) As String
            Dim i As Integer
            Dim strResult As String = ""
            If strInput.Length < 1 Then Return ""
            Try
                For i = 0 To strInput.Length - 1
                    If Not IsNumeric(strInput.Chars(i)) Then
                        strResult = strInput.Chars(i)
                        Exit For
                    End If
                Next
                Return strResult
            Catch ex As Exception
                Return ""
            End Try
        End Function

        Public Shared Function DateString_ChangeFormat(ByVal sInputFormat As String, ByVal sInput As String, ByVal sOutputFormat As String, ByRef sResult As String) As Boolean
            Dim bRet As Boolean = False
            Dim sDay As String = ""
            Dim sMonth As String = ""
            Dim sYear As String = ""
            Dim inputType As Integer = 0
            Try
                sResult = ""
                If Not String.IsNullOrEmpty(sInput) Then
                    If sInputFormat.ToUpper() = sOutputFormat.ToUpper() Then
                        bRet = True
                        sResult = sInput
                        Return bRet
                    End If

                    Select Case sInputFormat.ToUpper()
                        Case "dd/MM/yyyy".ToUpper()
                            inputType = 1
                        Case "dd-MM-yyyy".ToUpper()
                            inputType = 1
                        Case "MM/dd/yyyy".ToUpper()
                            inputType = 2
                        Case "MM-dd-yyyy".ToUpper()
                            inputType = 2
                        Case "yyyy/MM/dd".ToUpper()
                            inputType = 3
                        Case "yyyy-MM-dd".ToUpper()
                            inputType = 3
                        Case "yyyy/dd/MM".ToUpper()
                            inputType = 4
                        Case "yyyy-dd-MM".ToUpper()
                            inputType = 4
                        Case "ddMMyyyy".ToUpper()
                            inputType = 5
                        Case "MMddyyyy".ToUpper()
                            inputType = 6
                        Case "yyyyMMdd".ToUpper()
                            inputType = 7
                        Case "yyyyddMM".ToUpper()
                            inputType = 8
                    End Select

                    Select Case inputType
                        Case 1
                            If sInput.Length = 10 Then
                                sDay = sInput.Substring(0, 1)
                                sMonth = sInput.Substring(3, 4)
                                sYear = sInput.Substring(6, 9)
                            End If
                        Case 2
                            If sInput.Length = 10 Then
                                sDay = sInput.Substring(3, 4)
                                sMonth = sInput.Substring(0, 1)
                                sYear = sInput.Substring(6, 9)
                            End If
                        Case 3
                            If sInput.Length = 10 Then
                                sDay = sInput.Substring(8, 9)
                                sMonth = sInput.Substring(5, 6)
                                sYear = sInput.Substring(0, 3)
                            End If
                        Case 4
                            If sInput.Length = 10 Then
                                sDay = sInput.Substring(5, 6)
                                sMonth = sInput.Substring(8, 9)
                                sYear = sInput.Substring(0, 3)
                            End If
                        Case 5
                            If sInput.Length = 8 Then
                                sDay = sInput.Substring(0, 1)
                                sMonth = sInput.Substring(2, 3)
                                sYear = sInput.Substring(4, 7)
                            End If
                        Case 6
                            If sInput.Length = 8 Then
                                sDay = sInput.Substring(2, 3)
                                sMonth = sInput.Substring(0, 1)
                                sYear = sInput.Substring(4, 7)
                            End If
                        Case 7
                            If sInput.Length = 8 Then
                                sDay = sInput.Substring(6, 7)
                                sMonth = sInput.Substring(4, 5)
                                sYear = sInput.Substring(0, 3)
                            End If
                        Case 8
                            If sInput.Length = 8 Then
                                sDay = sInput.Substring(4, 5)
                                sMonth = sInput.Substring(6, 7)
                                sYear = sInput.Substring(0, 3)
                            End If
                    End Select

                    If IsNumeric(sDay) AndAlso IsNumeric(sMonth) AndAlso IsNumeric(sYear) Then
                        Select Case sOutputFormat.ToUpper()
                            Case "dd/MM/yyyy".ToUpper()
                                bRet = True
                                sResult = sDay & "/" & sMonth & "/" & sYear
                            Case "dd-MM-yyyy".ToUpper()
                                bRet = True
                                sResult = sDay & "-" & sMonth & "-" & sYear
                            Case "MM/dd/yyyy".ToUpper()
                                bRet = True
                                sResult = sMonth & "/" & sDay & "/" & sYear
                            Case "MM-dd-yyyy".ToUpper()
                                bRet = True
                                sResult = sMonth & "-" & sDay & "-" & sYear
                            Case "yyyy/MM/dd".ToUpper()
                                bRet = True
                                sResult = sYear & "/" & sMonth & "/" & sDay
                            Case "yyyy-MM-dd".ToUpper()
                                bRet = True
                                sResult = sYear & "-" & sMonth & "-" & sDay
                            Case "yyyy/dd/MM".ToUpper()
                                bRet = True
                                sResult = sYear & "/" & sDay & "/" & sMonth
                            Case "yyyy-dd-MM".ToUpper()
                                bRet = True
                                sResult = sYear & "-" & sDay & "-" & sMonth
                            Case "ddMMyyyy".ToUpper()
                                bRet = True
                                sResult = sDay & sMonth & sYear
                            Case "MMddyyyy".ToUpper()
                                bRet = True
                                sResult = sMonth & sDay & sYear
                            Case "yyyyMMdd".ToUpper()
                                bRet = True
                                sResult = sYear & sMonth & sDay
                            Case "yyyyddMM".ToUpper()
                                bRet = True
                                sResult = sYear & sDay & sMonth
                        End Select
                    End If
                End If

            Catch ex As Exception
                bRet = False
                sResult = ""
                Throw ex
            End Try

            Return bRet
        End Function

        Public Shared Function ConvertNumbertoString(ByVal Lstr As Long) As String
            '-----------------------------------------------------
            ' Mục đích: Chuyển 1 chuoi Long  yyyyMMdd  --> dạng số 'dd/MM/yyyy'.
            ' Tham số: string đầu vào
            ' Giá trị trả về: 
            ' Ngày viết: 18/10/2007
            ' Người viết: Nguyen Ba Cuong
            ' ----------------------------------------------------
            Dim strDate, strMonth, strYear As String
            Dim strInput As String
            Try
                strInput = Lstr.ToString()
                If Len(strInput = 8) Then
                    strDate = Mid(strInput, 7, 2)
                    strMonth = Mid(strInput, 5, 2)
                    strYear = Mid(strInput, 1, 4)
                End If
            Catch ex As Exception

            End Try
            Return (strDate & "/" & strMonth & "/" & strYear)
        End Function

        Public Shared Function ConvertNumbertoString_HQ(ByVal strText As String) As String
            '-----------------------------------------------------
            ' Mục đích: Chuyển 1 chuoi Long  yyyyMMdd  --> dạng số 'ddMMyyyy'.
            ' Tham số: string đầu vào
            ' Giá trị trả về: 
            ' Ngày viết: 18/10/2007
            ' Người viết: Nguyen Ba Cuong
            ' ----------------------------------------------------
            Dim strDate, strMonth, strYear As String
            Dim strInput As String
            Try
                strInput = strText
                If Len(strInput = 8) Then
                    strDate = Mid(strInput, 7, 2)
                    strMonth = Mid(strInput, 5, 2)
                    strYear = Mid(strInput, 1, 4)
                End If
            Catch ex As Exception

            End Try
            Return (strDate.ToString() & strMonth.ToString() & strYear.ToString())
        End Function

        Public Shared Function ConvertNumberToDate(ByVal strText As String) As DateTime
            '-----------------------------------------------------
            ' Mục đích: Chuyển một string ngày tháng 'yyyyMMdd' --> dạng DateTime
            ' Tham số: string đầu vào
            ' Giá trị trả về: 
            ' Ngày viết: 18/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim dt As Date
            Dim strDate, strMonth, strYear As String
            Try
                strText = strText.Trim()
                If Len(strText) < 8 Then
                    Return ""
                End If
                strDate = strText.Substring(6, 2)
                strMonth = strText.Substring(4, 2)
                strYear = strText.Substring(0, 4)

                Dim intDate, intMonth, intYear As Integer
                intDate = CInt(strDate)
                intMonth = CInt(strMonth)
                intYear = CInt(strYear)

                dt = New DateTime(intYear, intMonth, intDate)
            Catch ex As Exception
                dt = Now()
            End Try
            Return dt
        End Function
#End Region

#Region "Read Variables"
        Public Shared Sub GetGlobalVars(ByVal strDiemThu As String)
            Try
                'Lấy các giá trị trong bảng TCS_THAMSO:
                ReadDBVars(strDiemThu)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub ReadDBVars(ByVal strMaDiemThu As String)
            Dim cnQTSHT As New DataAccess
            Dim dsTSHT As DataSet
            Dim dsTSC As DataSet
            Dim dr As DataRow
            Dim strSQL As String
            Dim strTenTS As String
            Dim strGTTS As String
            Try
                strSQL = "Select TEN_TS,GIATRI_TS From TCS_THAMSO WHERE MA_DTHU='00' AND TEN_TS='MODE'"
                dsTSC = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not dsTSC Is Nothing Then
                    mdlSystemVariables.gblnTTBDS = dsTSC.Tables(0).Rows(0)("GIATRI_TS").ToString.ToUpper
                End If

                strSQL = "SELECT (CASE WHEN TO_DATE(GIATRI_TS,'DD/MM/YYYY') >TRUNC(SYSDATE) THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE GIATRI_TS END) GIATRI_TS FROM TCS_THAMSO WHERE MA_DTHU='00' AND TEN_TS='NGAY_LV'"
                dsTSC = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not dsTSC Is Nothing Then
                    mdlSystemVariables.gdtmNgayNH = ConvertDateToNumber(dsTSC.Tables(0).Rows(0)("GIATRI_TS").ToString.ToUpper)
                End If


                strSQL = "Select TEN_TS,GIATRI_TS From TCS_THAMSO WHERE MA_DTHU='" & strMaDiemThu & "'"
                dsTSHT = cnQTSHT.ExecuteReturnDataSet(strSQL, CommandType.Text)


                'mdlSystemVariables.gstrMaDiemThu = strMaDiemThu

                If Not IsEmptyDataSet(dsTSHT) Then
                    For Each dr In dsTSHT.Tables(0).Rows
                        strTenTS = UCase(dr("TEN_TS").ToString().Trim)
                        If Not IsDBNull(dr("GIATRI_TS")) Then
                            strGTTS = GetString(dr("GIATRI_TS")).Trim()
                        Else
                            strGTTS = ""
                        End If
                        Select Case strTenTS
                            'Case "SO_BANTHU"
                            '    ' Tham số bàn thu của điểm thu
                            '    If IsNumeric(strGTTS) Then
                            '        mdlSystemVariables.gbytSoBanLV = CByte(strGTTS)
                            '    Else
                            '        mdlSystemVariables.gbytSoBanLV = 1
                            '    End If
                            Case "EXP_PATH"
                                ' Lấy đường dẫn kết xuất dữ liệu
                                mdlSystemVariables.gstrExportPath = strGTTS
                                ' Định dạng lại đường dẫn
                                If Len(mdlSystemVariables.gstrExportPath) > 0 Then
                                    If mdlSystemVariables.gstrExportPath.Chars(mdlSystemVariables.gstrExportPath.Length - 1) <> "\" Then
                                        mdlSystemVariables.gstrExportPath += "\"
                                    End If
                                    mdlSystemVariables.gstrExportPath = mdlSystemVariables.gstrExportPath.Replace("/", "\")
                                End If
                            Case "FTP_EXP_PATH"
                                ' Lấy đường dẫn FTP
                                mdlSystemVariables.gstrFTPExpPath = strGTTS
                                ' Định dạng lại đường dẫn
                                If Len(mdlSystemVariables.gstrFTPExpPath) > 0 Then
                                    If mdlSystemVariables.gstrFTPExpPath.Chars(0) <> "/" Then
                                        mdlSystemVariables.gstrFTPExpPath = "/" + mdlSystemVariables.gstrFTPExpPath
                                    End If
                                    mdlSystemVariables.gstrFTPExpPath = mdlSystemVariables.gstrFTPExpPath.Replace("\", "/")
                                End If
                            Case "IMP_PATH"
                                ' Lấy đường dẫn nhận dữ liệu
                                mdlSystemVariables.gstrImportPath = strGTTS
                                ' Định dạng lại đường dẫn
                                If Len(mdlSystemVariables.gstrImportPath) > 0 Then
                                    If mdlSystemVariables.gstrImportPath.Chars(mdlSystemVariables.gstrImportPath.Length - 1) <> "\" Then
                                        mdlSystemVariables.gstrImportPath += "\"
                                    End If
                                    mdlSystemVariables.gstrImportPath = mdlSystemVariables.gstrImportPath.Replace("/", "\")
                                End If
                            Case "FTP_IMP_PATH"
                                ' Lấy đường dẫn FTP
                                mdlSystemVariables.gstrFTPImpPath = strGTTS
                                ' Định dạng lại đường dẫn FTP
                                If Len(mdlSystemVariables.gstrFTPImpPath) > 0 Then
                                    If mdlSystemVariables.gstrFTPImpPath.Chars(0) <> "/" Then
                                        mdlSystemVariables.gstrFTPImpPath = "/" + mdlSystemVariables.gstrFTPImpPath
                                    End If
                                    mdlSystemVariables.gstrFTPImpPath = mdlSystemVariables.gstrFTPImpPath.Replace("\", "/")
                                End If
                            Case "BK_PATH"
                                ' Lấy đường dẫn sao lưu dữ liệu
                                mdlSystemVariables.gstrBackupPath = strGTTS
                                ' Định dạng lại đường dẫn
                                If Len(mdlSystemVariables.gstrBackupPath) > 0 Then
                                    If mdlSystemVariables.gstrBackupPath.Chars(mdlSystemVariables.gstrBackupPath.Length - 1) <> "\" Then
                                        mdlSystemVariables.gstrBackupPath += "\"
                                    End If
                                    mdlSystemVariables.gstrBackupPath = mdlSystemVariables.gstrBackupPath.Replace("/", "\")
                                End If
                            Case "NGAY_LV"
                                ' Lấy ngày làm việc
                                Dim d As Date
                                ' mdlSystemVariables.gdtmNgayLV = ConvertDateToNumber(strGTTS)
                                ' Lay ngay dieu chinh
                                ' d = ConvertNumberToDate(mdlSystemVariables.gdtmNgayLV.ToString)
                                mdlSystemVariables.gdtmNgayDC = CLng(d.AddMonths(-1).ToString("yyyyMM") & "32")
                                ' Lấy mdlSystemVariables. theo ngày làm việc 
                                If Len(strGTTS) > 4 Then
                                    mdlSystemVariables.gstrKyThue = Mid(strGTTS, 4)
                                End If
                            Case "TB_PATH"
                                ' Lấy đường dẫn thu thuế trước bạ
                                mdlSystemVariables.gstrTB_Path = strGTTS
                                ' Định dạng lại đường dẫn
                                If Len(mdlSystemVariables.gstrTB_Path) > 0 Then
                                    If mdlSystemVariables.gstrTB_Path.Chars(mdlSystemVariables.gstrTB_Path.Length - 1) <> "\" Then
                                        mdlSystemVariables.gstrTB_Path += "\"
                                    End If
                                    mdlSystemVariables.gstrTB_Path = mdlSystemVariables.gstrTB_Path.Replace("/", "\")
                                End If
                            Case "FTP_TB_PATH"
                                ' Lấy đường dẫn FTP trước bạ
                                mdlSystemVariables.gstrFTPTB_Path = strGTTS
                                ' Định dạng lại đường dẫn FTP
                                If Len(mdlSystemVariables.gstrFTPTB_Path) > 0 Then
                                    If mdlSystemVariables.gstrFTPTB_Path.Chars(0) <> "/" Then
                                        mdlSystemVariables.gstrFTPTB_Path = "/" + mdlSystemVariables.gstrFTPTB_Path
                                    End If
                                    mdlSystemVariables.gstrFTPTB_Path = mdlSystemVariables.gstrFTPTB_Path.Replace("\", "/")
                                End If
                            Case "KT_MATIN"
                                ' Lấy điều kiện kiểm tra mã ĐTNT khi lập GNT
                                If UCase(strGTTS) <> "C" Then
                                    mdlSystemVariables.gblnKT_MaTIN = False
                                Else
                                    mdlSystemVariables.gblnKT_MaTIN = True
                                End If
                                'Case "MA_DBHC"
                                '    ' Lấy mã địa bàn hành chính
                                '    mdlSystemVariables.gstrMaDBHC = strGTTS
                                'Case "TEN_KB"
                                '    ' Lấy tên văn phòng Kho bạc
                                '    mdlSystemVariables.gstrTenKB = strGTTS
                                'Case "MA_NHKB"
                                '    ' Lấy mã NHKB
                                '    mdlSystemVariables.gstrMaNHKB = strGTTS
                                'Case "MA_CQTHUE"
                                '    ' Lấy mã cơ quan thuế tương ứng với Kho bạc
                                '    mdlSystemVariables.gstrMaCQThue = strGTTS
                                'Case "TEN_TINH"
                                '    ' Lấy tên Tỉnh/TP
                                '    mdlSystemVariables.gstrTenTinhTP = strGTTS
                                'Case "MA_TTP"
                                '    ' Lấy mã Tỉnh/TP
                                '    mdlSystemVariables.gstrMaTTP = strGTTS
                                'Case "MA_QH"
                                '    ' Lấy mã Quận/Huyện
                                '    mdlSystemVariables.gstrMaQH = strGTTS
                            Case "MA_DT"
                                ' Lấy mã điểm thu
                                'Kienvt: lay ban thu lam ma diem thu
                                'mdlSystemVariables.gstrMaDiemThu = strMaDiemThu 'strGTTS
                                'Case "TEN_DTHU"
                                '    ' Lấy tên điểm thu
                                '    mdlSystemVariables.gstrTenDiemThu = strGTTS
                            Case "TEN_KTT"
                                ' Lấy tên kế toán trưởng
                                mdlSystemVariables.gstrTen_KTT = strGTTS
                            Case "SAI_THUCTHU"
                                ' Lấy số tiền cho phép thu thừa khi lập GNT
                                If IsNumeric(strGTTS) Then
                                    mdlSystemVariables.gdblSaiThucThu = CDbl(strGTTS)
                                Else
                                    mdlSystemVariables.gdblSaiThucThu = 100
                                End If
                                'Case "KSCT"
                                '    ' Lấy điều kiện cho phép kiểm soát chứng từ hay không
                                '    If UCase(strGTTS) = "C" Then
                                '        mdlSystemVariables.gbytKSCT = 1
                                '    Else
                                '        mdlSystemVariables.gbytKSCT = 0
                                '    End If
                            Case "A5_WIDTH"
                                ' Lấy Width của khổ giấy A5
                                If IsNumeric(strGTTS) Then
                                    mdlSystemVariables.gdblA5_Width = CDbl(strGTTS)
                                Else
                                    mdlSystemVariables.gdblA5_Width = 21.59
                                End If
                            Case "A5_HEIGHT"
                                ' Lấy Height của khổ giấy A5
                                If IsNumeric(strGTTS) Then
                                    mdlSystemVariables.gdblA5_Height = CDbl(strGTTS)
                                Else
                                    mdlSystemVariables.gdblA5_Height = 14.8
                                End If
                            Case "A5_TOP"
                                ' Lấy Top của khổ giấy A5
                                If IsNumeric(strGTTS) Then
                                    mdlSystemVariables.gdblA5_Top = CDbl(strGTTS)
                                Else
                                    mdlSystemVariables.gdblA5_Top = 18
                                End If
                            Case "A5_LEFT"
                                ' Lấy Left của khổ giấy A5
                                If IsNumeric(strGTTS) Then
                                    mdlSystemVariables.gdblA5_Left = CDbl(strGTTS)
                                Else
                                    mdlSystemVariables.gdblA5_Left = 12
                                End If
                                'Case "DB_THU"
                                '    ' Lấy địa bàn thu
                                '    mdlSystemVariables.gstrDBThu = strGTTS
                                'Case "SHKB"
                                '    ' Lấy số hiệu kho bạc
                                '    mdlSystemVariables.gstrSHKB = strGTTS
                            Case "PASS_KTKB"
                                ' Lấy số hiệu kho bạc
                                'mdlSystemVariables.gstrPassKTKB = Security.DescryptStr(strGTTS)
                            Case "NHAP_BKPLT"
                                If "B".Equals(UCase(strGTTS)) Then
                                    ' Nhập theo loại tiền
                                    mdlSystemVariables.gblnNhapPLT = True
                                Else
                                    ' Nhập theo tổng tiền
                                    mdlSystemVariables.gblnNhapPLT = False
                                End If
                            Case "SLCT" ' Số Chứng Từ
                                mdlSystemVariables.gintSoLienInCT = strGTTS
                            Case "SLBL" ' Số Biên Lai
                                mdlSystemVariables.gintSoLienInBL = strGTTS
                                'Case "KHCT" ' Ký hiệu chứng từ
                                '    mdlSystemVariables.gstrKHCT = strGTTS
                            Case "KHBL" ' Ký hiệu biên lai
                                mdlSystemVariables.gstrKHBL = strGTTS
                            Case "NH_TKTM" ' Tài khoản tiền mặt của NH
                                mdlSystemVariables.gstrTK_TM_NH = strGTTS
                            Case "NH_TKTH_KB" ' Tài khoản Thi chi hộ KB của NH
                                mdlSystemVariables.gstrTK_CK_NH = strGTTS
                            Case "TEN_NH" ' Tên ngân hàng
                                mdlSystemVariables.gstrTenNH = strGTTS
                            Case "MODE" ' Tên ngân hàng
                                mdlSystemVariables.gsMode = strGTTS
                        End Select
                    Next
                    dsTSHT.Dispose()
                End If

            Catch ex As Exception
                'MsgBox("Lỗi trong quá lấy tham số hệ thống !", MsgBoxStyle.Critical, "Chú ý")
            Finally
                If Not cnQTSHT Is Nothing Then cnQTSHT.Dispose()
            End Try
        End Sub
#End Region

#Region "Xử lý File"

        'Public Shared Function SaveFile(ByVal fstrFilePath As String, ByVal fbytToSave As Byte())
        '    Dim mFileStream As FileStream
        '    Dim blnResult As Boolean

        '    Try
        '        If File.Exists(fstrFilePath) Then
        '            mFileStream = New FileStream(fstrFilePath, FileMode.Open, FileAccess.Write)
        '        Else
        '            mFileStream = New FileStream(fstrFilePath, FileMode.CreateNew)
        '        End If
        '        Dim mWriter As New BinaryWriter(mFileStream)

        '        mWriter.Write(fbytToSave)

        '        mWriter.Close()
        '        mFileStream.Close()
        '        ' Thành công
        '        blnResult = True
        '    Catch ex As Exception
        '        ' Lỗi
        '        blnResult = False
        '    End Try
        'End Function
        Public Shared Sub CreatIniFile()
            ''---------------------------------------------------------------
            '' Mục đích: Tạo mới 1 file Initial.xml để khởi tạo các
            ''           biến hệ thống trong trường hợp không tìm thấy file này.
            '' Tham số: 
            '' Giá trị trả về:
            '' Ngày viết: 30/10/2003
            '' Người viết: Nguyễn Quốc Việt
            '' Nguoi sua: Nguyen Ba Cuong 29/10/2007
            '' --------------------------------------------------------------
            'Dim MyDom As New Xml.XmlDocument
            'Dim ProcessInstruct, RootNode, VarNode As Xml.XmlNode
            'ProcessInstruct = MyDom.CreateProcessingInstruction( _
            '    "xml", "version = '1.0' encoding='UTF-8'")
            'ProcessInstruct = MyDom.AppendChild(ProcessInstruct)
            'RootNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "Variables", "")
            'RootNode = MyDom.AppendChild(RootNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "TenND", "")
            'VarNode.InnerText = ""
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "MaNhom", "")
            'VarNode.InnerText = ""
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "BanLV", "")
            'VarNode.InnerText = "1"
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "KyHieuCT", "")
            'VarNode.InnerText = gstrKHCT
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "SoBL", "")
            'VarNode.InnerText = "0000001"
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "SoCT", "")
            'VarNode.InnerText = "0000001"
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "KyHieuBL", "")
            'VarNode.InnerText = gstrKHBL
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "TT_NNT", "")
            'VarNode.InnerText = "0"
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "MAU_GNT", "")
            'VarNode.InnerText = "1"
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "DB_KT38i", "")
            'VarNode.InnerText = "KT38i"
            'VarNode = RootNode.AppendChild(VarNode)
            'MyDom.Save("Initial.xml")
        End Sub
        Public Shared Sub CreatIniFile_FTP()
            '---------------------------------------------------------------
            ' Mục đích: Tạo mới 1 file Initial_FTP.xml để khởi tạo các
            '           giá trị của form login FTP.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 19/04/2005
            ' Người viết: Nguyễn Quốc Việt
            ' --------------------------------------------------------------
            Dim MyDom As New Xml.XmlDocument
            Dim ProcessInstruct, RootNode, VarNode As Xml.XmlNode

            ProcessInstruct = MyDom.CreateProcessingInstruction( _
                "xml", "version = '1.0' encoding='UTF-8'")
            ProcessInstruct = MyDom.AppendChild(ProcessInstruct)
            RootNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "Variables", "")
            RootNode = MyDom.AppendChild(RootNode)
            VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "TenMaychu", "")
            VarNode.InnerText = ""
            VarNode = RootNode.AppendChild(VarNode)
            VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "TenNSD", "")
            VarNode.InnerText = ""
            VarNode = RootNode.AppendChild(VarNode)
            VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "Matkhau", "")
            VarNode.InnerText = ""
            VarNode = RootNode.AppendChild(VarNode)
            MyDom.Save("Initial_FTP.xml")
        End Sub
        Public Shared Sub ReadIniFile()
            ''---------------------------------------------------------------
            '' Mục đích: Đọc các giá trị khởi tạo ban đầu trong file
            ''           Initial.xml và đưa vào các biến hệ thống tương ứng.
            '' Tham số: 
            '' Giá trị trả về:
            '' Ngày viết: 30/10/2003
            '' Người viết: Nguyễn Quốc Việt
            '' Nguoi sua: Nguyen Ba Cuong 29/10/2007
            '' --------------------------------------------------------------
            'Dim MyDom As New Xml.XmlDocument
            'Dim VarNodeList As Xml.XmlNodeList
            'MyDom.Load("Initial.xml")
            '' Tên ND
            'VarNodeList = MyDom.GetElementsByTagName("TenND")
            'gstrTenND = VarNodeList.Item(0).InnerText
            '' Mã nhóm
            'VarNodeList = MyDom.GetElementsByTagName("MaNhom")
            'gstrMaNhom = VarNodeList.Item(0).InnerText
            '' Bàn làm việc
            'VarNodeList = MyDom.GetElementsByTagName("BanLV")
            'If IsNumeric(VarNodeList.Item(0).InnerText.Trim()) Then
            '    gbytBanLV = CByte(VarNodeList.Item(0).InnerText.Trim())
            'Else
            '    gbytBanLV = 1
            'End If
            '' Ký hiệu chứng từ
            'VarNodeList = MyDom.GetElementsByTagName("KyHieuCT")
            'gstrKHCT = VarNodeList.Item(0).InnerText
            '' Số Biên lai
            'VarNodeList = MyDom.GetElementsByTagName("SoBL")
            'gstrSoBL = ChuanHoa_SoCT(Trim(VarNodeList.Item(0).InnerText))
            '' Số CT
            'VarNodeList = MyDom.GetElementsByTagName("SoCT")
            'gstrSoCT = ChuanHoa_SoCT(Trim(VarNodeList.Item(0).InnerText))
            '' Ký hiệu biên lai
            'VarNodeList = MyDom.GetElementsByTagName("KyHieuBL")
            'gstrKHBL = VarNodeList.Item(0).InnerText

            '' Ưu tiên nộp thuế
            'VarNodeList = MyDom.GetElementsByTagName("TT_NNT")
            'If IsNumeric(VarNodeList.Item(0).InnerText.Trim()) Then
            '    gbytUutien = CByte(VarNodeList.Item(0).InnerText)
            'Else
            '    gbytUutien = 0
            'End If
            '' Kiem tra ban thu hien thoi phai nam trong gioi han 1..gbytSoBanLV
            'If gbytBanLV <= 0 Then
            '    gbytBanLV = 1
            'ElseIf gbytBanLV > gbytSoBanLV Then
            '    gbytBanLV = gbytSoBanLV
            'End If
            '' Mẫu GNT
            'VarNodeList = MyDom.GetElementsByTagName("MAU_GNT")
            'If IsNumeric(VarNodeList.Item(0).InnerText.Trim()) Then
            '    gbytMauGNT = CByte(VarNodeList.Item(0).InnerText)
            'Else
            '    gbytMauGNT = 1
            'End If
            '' Kiem tra ban thu hien thoi phai nam trong gioi han 1..gbytSoBanLV
            'If gbytMauGNT < 0 Then
            '    gbytMauGNT = 1
            'End If
            '' Đọc tên database KTKB
            'VarNodeList = MyDom.GetElementsByTagName("DB_KT38i")
            'gstrKT38i = VarNodeList.Item(0).InnerText
        End Sub
        Public Shared Sub ReadIniFile_FTP(ByRef fstrTenmaychu As String, ByRef fstrTenNSD As String, _
            ByRef fstrMatkhau As String)
            '---------------------------------------------------------------
            ' Mục đích: Đọc các giá trị khởi tạo ban đầu trong file
            '           Initial_FTP.xml và đưa vào form login FTP.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 19/04/2005
            ' Người viết: Nguyễn Quốc Việt
            ' --------------------------------------------------------------
            Dim MyDom As New Xml.XmlDocument
            Dim VarNodeList As Xml.XmlNodeList

            MyDom.Load("Initial_FTP.xml")

            VarNodeList = MyDom.GetElementsByTagName("TenMaychu")
            fstrTenmaychu = VarNodeList.Item(0).InnerText
            VarNodeList = MyDom.GetElementsByTagName("TenNSD")
            fstrTenNSD = VarNodeList.Item(0).InnerText
            VarNodeList = MyDom.GetElementsByTagName("Matkhau")
            'fstrMatkhau = Security.DescryptStr(VarNodeList.Item(0).InnerText)
        End Sub
        Public Shared Sub UpdateIniFile()
            '---------------------------------------------------------------
            ' Mục đích: Cập nhật giá trị các biến hệ thống vào file
            '           Initial.xml (xoá file cũ đi và ghi lại 1 file mới).
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 30/10/2003
            ' Người viết: Nguyễn Quốc Việt
            ' Nguoi sua: Nguyen Ba Cuong 29/10/2007
            ' --------------------------------------------------------------
            'Dim MyDom As New Xml.XmlDocument
            'Dim ProcessInstruct, RootNode, VarNode As Xml.XmlNode

            'If System.IO.File.Exists("Initial.xml") Then Kill("Initial.xml")

            'ProcessInstruct = MyDom.CreateProcessingInstruction( _
            '    "xml", "version = '1.0' encoding='UTF-8'")
            'ProcessInstruct = MyDom.AppendChild(ProcessInstruct)
            'RootNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "Variables", "")
            'RootNode = MyDom.AppendChild(RootNode)
            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "TenND", "")
            'VarNode.InnerText = gstrTenND
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "MaNhom", "")
            'VarNode.InnerText = gstrMaNhom
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "BanLV", "")
            'VarNode.InnerText = CStr(gbytBanLV)
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "KyHieuCT", "")
            'VarNode.InnerText = gstrKHCT
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "SoCT", "")
            'VarNode.InnerText = gstrSoCT
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "SoBL", "")
            'VarNode.InnerText = gstrSoBL
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "KyHieuBL", "")
            'VarNode.InnerText = gstrKHBL
            'VarNode = RootNode.AppendChild(VarNode)

            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "TT_NNT", "")
            'VarNode.InnerText = CStr(gbytUutien)
            'VarNode = RootNode.AppendChild(VarNode)
            ''Mẫu GNT
            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "MAU_GNT", "")
            'VarNode.InnerText = CStr(gbytMauGNT)
            'VarNode = RootNode.AppendChild(VarNode)
            '' KTKB
            'VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "DB_KT38i", "")
            'VarNode.InnerText = gstrKT38i
            'VarNode = RootNode.AppendChild(VarNode)
            'MyDom.Save("Initial.xml")
        End Sub
        Public Shared Sub UpdateIniFile_FTP(ByVal fstrTenmaychu As String, ByVal fstrTenNSD As String, _
            ByVal fstrMatkhau As String)
            '---------------------------------------------------------------
            ' Mục đích: Cập nhật giá trị các biến đăng nhập FTP.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 30/10/2003
            ' Người viết: Nguyễn Quốc Việt
            ' --------------------------------------------------------------
            Dim MyDom As New Xml.XmlDocument
            Dim ProcessInstruct, RootNode, VarNode As Xml.XmlNode

            If System.IO.File.Exists("Initial_FTP.xml") Then Kill("Initial_FTP.xml")

            ProcessInstruct = MyDom.CreateProcessingInstruction( _
                "xml", "version = '1.0' encoding='UTF-8'")
            ProcessInstruct = MyDom.AppendChild(ProcessInstruct)
            RootNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "Variables", "")
            RootNode = MyDom.AppendChild(RootNode)
            VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "TenMaychu", "")
            VarNode.InnerText = fstrTenmaychu
            VarNode = RootNode.AppendChild(VarNode)
            VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "TenNSD", "")
            VarNode.InnerText = fstrTenNSD
            VarNode = RootNode.AppendChild(VarNode)
            VarNode = MyDom.CreateNode(Xml.XmlNodeType.Element, "Matkhau", "")
            'VarNode.InnerText = Security.EncryptStr(fstrMatkhau)
            VarNode = RootNode.AppendChild(VarNode)
            MyDom.Save("Initial_FTP.xml")
        End Sub
#End Region

#Region "Sinh ID cho bảng"
        Public Shared Function getDataKey(ByVal strSeqName As String) As String
            '-----------------------------------------------------------------------------------------------
            ' Mục đích: sinh khóa cho các bảng
            ' Đầu vào : strSeqname - Tên seq cần lấy
            ' Người viết: HoanNH
            ' Ngày viết: 12/11/2007
            '-----------------------------------------------------------------------------------------------
            Dim strDataKey As String = "0"
            Dim strSQL As String
            Dim conPK As DataAccess
            Dim drParameter As IDataReader
            Try
                conPK = New DataAccess

                strSQL = "SELECT " & strSeqName & " FROM dual"
                drParameter = conPK.ExecuteDataReader(strSQL, CommandType.Text)
                If drParameter.Read Then
                    strDataKey = drParameter.GetValue(0)
                End If
            Catch ex As Exception
                strDataKey = "0"
                'LogDebug.Writelog("Lỗi trong quá trình sinh ID: " & ex.ToString)
            Finally
                If Not drParameter Is Nothing Then
                    drParameter.Close()
                End If
                If Not conPK Is Nothing Then
                    conPK.Dispose()
                End If
            End Try
            Return strDataKey
        End Function
        Public Shared Function getIMEXKey(ByVal strSeqName As String) As String
            '-------------------------------------------------------------------
            ' Mục đích: sinh khóa cho các giao dịch khi trao đổi
            ' Đầu vào : strSeqname - Tên seq cần lấy
            ' Người viết: HoanNH
            ' Ngày viết: 12/11/2007
            '-------------------------------------------------------------------
            Dim strIMEXKey As String = "0"
            Dim strSQL As String
            Dim drParameter As IDataReader
            Dim Ketnoi As New DataAccess
            Try
                Ketnoi = New DataAccess
                strSQL = "SELECT " & strSeqName & ".NEXTVAL   " _
                               & " FROM dual "
                drParameter = Ketnoi.ExecuteDataReader(strSQL, CommandType.Text)
                If drParameter.Read Then
                    strIMEXKey = drParameter.GetValue(0)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình sinh khóa cho trao đổi: " & ex.ToString)
            Finally
                If Not drParameter Is Nothing Then drParameter.Close()
                If Not Ketnoi Is Nothing Then Ketnoi.Dispose()
            End Try
            Return strIMEXKey.Trim
        End Function
#End Region

#Region "Control"

        'Public Shared Sub Valid_NumberTextBox(ByVal e As KeyPressEventArgs)
        '    '--------------------------------------------------------------------
        '    ' Mục đích: Chặn những ký tự không được phép trên Textbox
        '    ' Người viết: Lê Hồng Hà
        '    ' Ngày viết: 03/12/2009
        '    '---------------------------------------------------------------------
        '    Try
        '        Select Case AscW(e.KeyChar)
        '            Case 13         'Enter
        '            Case 3          'Ctrl+C
        '            Case 22         'Ctrl+V
        '            Case 8          'Back space
        '            Case 48 To 57   'Number
        '            Case Else
        '                e.Handled = True
        '        End Select
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        'Public Shared Sub Valid_TextBox(ByVal e As KeyPressEventArgs, ByVal strValidKey As String)
        '    '--------------------------------------------------------------------
        '    ' Mục đích: Chặn những ký tự không được phép trên Textbox
        '    ' Người viết: Lê Hồng Hà
        '    ' Ngày viết: 03/12/2009
        '    '---------------------------------------------------------------------
        '    Try
        '        Select Case e.KeyChar.ToString()
        '            Case 13         'Enter
        '            Case 3          'Ctrl+C
        '            Case 22         'Ctrl+V
        '            Case 8          'Back space
        '            Case Else
        '                If (strValidKey.IndexOf(e.KeyChar.ToString()) < 0) Then
        '                    e.Handled = True
        '                End If
        '        End Select
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Shared Sub ComboBoxWithValue(ByVal cbo As System.Windows.Forms.ComboBox, ByVal ds As DataSet, _
        'ByVal colDisplay As Integer, ByVal colValue As Integer)
        '    '-----------------------------------------------------
        '    ' Mục đích: Load dữ liệu từ DataSet vào ComboBox với 2 trường "Text" và "Value".
        '    ' Tham số: 
        '    ' Giá trị trả về:
        '    ' Ngày viết: 31/10/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Try
        '        cbo.Items.Clear()

        '        If (ds.Tables.Count = 0) Then Return
        '        cbo.BeginUpdate()
        '        cbo.DataSource = ds.Tables(0)
        '        cbo.DisplayMember = ds.Tables(0).Columns(colDisplay).ColumnName
        '        cbo.ValueMember = ds.Tables(0).Columns(colValue).ColumnName
        '        cbo.EndUpdate()
        '        cbo.SelectedIndex = 0
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        'Public Shared Sub ComboBoxWithValue(ByVal cbo As System.Windows.Forms.ComboBox, ByVal ds As DataSet, _
        'ByVal colDisplay As Integer, ByVal colValue As Integer, ByVal strTitle As String)
        '    '-----------------------------------------------------
        '    ' Mục đích: Load dữ liệu từ DataSet vào ComboBox với 2 trường "Text" và "Value".
        '    ' Tham số: 
        '    ' Giá trị trả về:
        '    ' Ngày viết: 31/10/2007
        '    ' Người viết: Lê Hồng Hà
        '    ' ----------------------------------------------------
        '    Try
        '        cbo.Items.Clear()

        '        If (ds.Tables.Count = 0) Then Return

        '        'Dim r As New DataRow
        '        ds.Tables(0).Rows.Add(New Object() {"0", ""})
        '        Dim row As DataRow = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1)
        '        ds.Tables(0).Rows.Remove(row)
        '        ds.Tables(0).Rows.InsertAt(row, 0)

        '        ds.Tables(0).Rows(0).Item(colDisplay) = strTitle

        '        cbo.BeginUpdate()
        '        cbo.DataSource = ds.Tables(0)
        '        cbo.DisplayMember = ds.Tables(0).Columns(colDisplay).ColumnName
        '        cbo.ValueMember = ds.Tables(0).Columns(colValue).ColumnName
        '        cbo.EndUpdate()
        '        cbo.SelectedIndex = 0
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        Public Shared Function IsEmptyDataSet(ByVal ds As DataSet) As Boolean
            Try
                If (IsNothing(ds)) Then Return True
                If (ds.Tables.Count = 0) Then Return True
                If (ds.Tables(0).Rows.Count = 0) Then Return True

                Return False
            Catch ex As Exception
                Return True
            End Try
        End Function
        'Public Shared Function IsChar(ByVal intKeyCode As Keys) As Boolean
        '    Try
        '        Select Case intKeyCode
        '            Case 48 To 57
        '                Return True
        '            Case 65 To 90
        '                Return True
        '            Case 96 To 105
        '                Return True
        '            Case Else
        '                Return False
        '        End Select
        '    Catch ex As Exception
        '        Return False
        '    End Try
        'End Function
        Public Shared Function GetTongTien(ByVal ds As DataSet, ByVal strFieldName As String) As Double
            '-----------------------------
            'Mục đích: Tính tổng tiền từ 1 trường trong DataSet
            'Người viết: Lê Hồng Hà
            '-----------------------------
            Try
                If (IsEmptyDataSet(ds)) Then Return 0

                Dim dblTongTien, dblTien As Double
                Dim intCount As Integer = ds.Tables(0).Rows.Count

                Dim i As Integer
                For i = 0 To intCount - 1
                    Try
                        dblTien = Convert.ToDouble(ds.Tables(0).Rows(i).Item(strFieldName).ToString())
                    Catch ex As Exception
                        dblTien = 0
                    End Try
                    dblTongTien += dblTien
                Next
                Return dblTongTien
            Catch ex As Exception
                Return 0
            End Try
        End Function
#End Region

#Region "KTKB"
        Public Shared Function GetNgayLV_KTKB() As Long
            '-------------------------------------------------------
            ' Mục đích: lấy ngày làm việc của KTKB
            '           để so sánh khi kiểm soát chứng từ
            ' Người viết: HoanNH
            ' Ngày viết: 18/06/2008
            '-------------------------------------------------------
            'Dim cnKTKB As New OracleConnect
            'Dim p As IDbDataParameter
            'Dim arrIn As IDataParameter()
            'Dim arrOut As ArrayList
            'Dim strSql As String
            'Dim lngNgayKTKB As Long = 0
            'Try
            '    ' Lấy ngày làm việc của KTKB
            '    ReDim arrIn(0)
            '    p = cnKTKB.GetParameter("p_ngay_ktkb", ParameterDirection.Output, Nothing, DbType.Int64, 10)
            '    arrIn(0) = p
            '    strSql = "TDTT_PCK_KTKB.prc_get_ngayktkb"
            '    arrOut = cnKTKB.ExecuteNonquery(strSql, arrIn)
            '    lngNgayKTKB = CLng(arrOut.Item(0).ToString)
            'Catch ex As Exception
            '    'LogDebug.Writelog("Lỗi trong quá trình lấy ngày làm việc của KTKB: " & ex.Message)
            '    Throw ex
            'End Try
            'Return lngNgayKTKB
        End Function
        Public Shared Function GetNgayLV(ByVal strMa_NV As String) As String
            Try
                Dim strSQL As String = String.Empty
                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
                Return ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString)
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Shared Function fnc_Check_KSThang() As Integer
            '-------------------------------------------------------
            ' Mục đích: Kiểm tra KTKB đã khóa sổ tháng chưa?
            ' Người viết: HaLH
            ' Ngày viết: 17/07/2008
            '-------------------------------------------------------
            'Dim cnKTKB As New OracleConnect
            'Dim p As IDbDataParameter
            'Dim arrIn As IDataParameter()
            'Dim arrOut As ArrayList
            'Dim strSql As String
            'Dim lngNgayKTKB As Long = 0
            'Try
            '    ReDim arrIn(0)
            '    p = cnKTKB.GetParameter("p_ksthang", ParameterDirection.Output, Nothing, DbType.Int16, 1)
            '    arrIn(0) = p
            '    strSql = "TDTT_PCK_KTKB.prc_get_ksthang"
            '    arrOut = cnKTKB.ExecuteNonquery(strSql, arrIn)
            '    Dim ksthang As Integer = CInt(arrOut.Item(0).ToString)

            '    If (ksthang = 1) Then
            '        Return 1 'Da khoa so
            '    Else
            '        Return 0    'Chua khoa so
            '    End If
            'Catch ex As Exception
            '    'LogDebug.Writelog("Lỗi trong quá trình lấy ngày làm việc của KTKB: " & ex.Message)
            '    Throw ex
            'End Try
            'Return lngNgayKTKB
        End Function
#End Region

#Region "Lay SoBT"
        Public Shared Function TCS_NSD_SOBT(ByVal lngNgayLV As Long, ByVal fstrMa_NV As String) As Integer
            '-----------------------------------------------------
            ' Ngày viết: 16/10/2007
            ' Người viết: Nguyen Ba Cuong
            ' Người sửa : Nguyễn Hữu Hoan
            ' Mục đích  : gộp 2 số bút toán ct và bl thành 1 số
            ' ----------------------------------------------------
            Dim Conn As DataAccess
            Dim drData As IDataReader
            Dim strSQL As String
            Dim intSoBTCT As Integer = 1
            Dim intSoBTBL As Integer = 1
            Dim intResult As Integer = 1
            Try
                Conn = New DataAccess
                strSQL = "Select MAX(So_BT) AS So_BT From TCS_CTU_HDR " & _
                         "Where NGAY_KB=" & lngNgayLV & " AND Ma_NV=" & fstrMa_NV
                drData = Conn.ExecuteDataReader(strSQL, CommandType.Text)
                If drData.Read Then
                    If Not drData.IsDBNull(0) Then
                        intSoBTCT = CInt(drData.GetValue(0)) + 1
                    Else
                        intSoBTCT = 1
                    End If
                End If
                drData.Close()
                strSQL = "Select MAX(So_BT) AS So_BT From TCS_CTBL " & _
                         "Where NGAY_KB=" & lngNgayLV & " AND Ma_NV=" & fstrMa_NV
                drData = Conn.ExecuteDataReader(strSQL, CommandType.Text)
                If drData.Read Then
                    If Not drData.IsDBNull(0) Then
                        intSoBTBL = CInt(drData.GetValue(0)) + 1
                    Else
                        intSoBTBL = 1
                    End If
                End If
                intResult = Math.Max(intSoBTCT, intSoBTBL)
                drData.Close()
            Catch ex As Exception
                ' Lỗi không đọc CSDL
                ' MsgBox("Lỗi trong quá trình lấy số bút toán.", MsgBoxStyle.Critical, "Chú ý")
                ' LogDebug.Writelog("Lỗi trong quá trình lấy số bút toán: " & ex.ToString)
                intResult = 1
            Finally
                If Not drData Is Nothing Then
                    drData.Dispose()
                End If
                If Not Conn Is Nothing Then
                    Conn.Dispose()
                End If
            End Try
            Return intResult
        End Function
        Public Shared Function Get_SOBT_CTU(ByVal lngNgayLV As Long, ByVal fstrMa_NV As String) As Integer
            '-----------------------------------------------------
            ' Ngày viết: 16/10/2007
            ' Người viết: Nguyen Ba Cuong
            ' Người sửa : Nguyễn Hữu Hoan
            ' Mục đích  : gộp 2 số bút toán ct và bl thành 1 số

            ' Người Sửa: Hoàng Văn Anh
            ' Sửa theo COA
            ' ----------------------------------------------------
            Dim Conn As DataAccess
            Dim drData As IDataReader
            Dim strSQL As String
            Dim intSoBTCT As Integer = 1
            Dim intResult As Integer = 1
            Try
                Conn = New DataAccess
                strSQL = "Select MAX(So_BT) AS So_BT From TCS_CTU_HDR " & _
                         "Where NGAY_KB=" & lngNgayLV & " AND Ma_NV=" & fstrMa_NV
                drData = Conn.ExecuteDataReader(strSQL, CommandType.Text)
                If drData.Read Then
                    If Not drData.IsDBNull(0) Then
                        intSoBTCT = CInt(drData.GetValue(0)) + 1
                    Else
                        intSoBTCT = 1
                    End If
                End If
                drData.Close()

                intResult = intSoBTCT
            Catch ex As Exception
                ' Lỗi không đọc CSDL
                MsgBox("Lỗi trong quá trình lấy số bút toán chứng từ.", MsgBoxStyle.Critical, "Chú ý")
                'LogDebug.Writelog("Lỗi trong quá trình lấy số bút toán chứng từ: " & ex.ToString)
                intResult = 1
            Finally
                If Not drData Is Nothing Then
                    drData.Dispose()
                End If
                If Not Conn Is Nothing Then
                    Conn.Dispose()
                End If
            End Try
            Return intResult
        End Function

        Public Shared Function Get_SOBT_BL(ByVal lngNgayLV As Long, ByVal fstrMa_NV As String) As Integer
            '-----------------------------------------------------
            ' Người Viết: Hoàng Văn Anh
            ' Mục đích: Lấy số bút toán Biên Lai
            ' ----------------------------------------------------
            Dim Conn As DataAccess
            Dim drData As IDataReader
            Dim strSQL As String
            Dim intSoBTBL As Integer = 1
            Dim intResult As Integer = 1
            Try
                Conn = New DataAccess
                strSQL = "Select MAX(So_BT) AS So_BT From TCS_CTBL " & _
                    "Where NGAY_KB=" & lngNgayLV & " AND Ma_NV=" & fstrMa_NV
                drData = Conn.ExecuteDataReader(strSQL, CommandType.Text)
                If drData.Read Then
                    If Not drData.IsDBNull(0) Then
                        intSoBTBL = CInt(drData.GetValue(0)) + 1
                    Else
                        intSoBTBL = 1
                    End If
                End If
                intResult = intSoBTBL
                drData.Close()
            Catch ex As Exception
                ' Lỗi không đọc CSDL
                MsgBox("Lỗi trong quá trình lấy số bút toán Biên Lai.", MsgBoxStyle.Critical, "Chú ý")
                'LogDebug.Writelog("Lỗi trong quá trình lấy số bút toán Biên Lai: " & ex.ToString)
                intResult = 1
            Finally
                If Not drData Is Nothing Then
                    drData.Dispose()
                End If
                If Not Conn Is Nothing Then
                    Conn.Dispose()
                End If
            End Try
            Return intResult
        End Function

        '-----------------------------------------------------
        ' Người Viết: Hoàng Văn Anh
        ' Mục đích: Lấy số chứng từ
        ' ----------------------------------------------------
        'Public Shared Function Get_SoCTU(ByVal strMaNV As String, ByVal lSoBT As Long) As String
        '    Dim strResult As String = ""
        '    Try
        '        strResult = strMaNV.PadLeft(3, "0") & lSoBT.ToString().PadLeft(5, "0")
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong hàm lấy số chứng từ: " & ex.ToString())
        '    End Try

        '    Return strResult
        'End Function

        '-----------------------------------------------------------------------------------------------
        ' Mục đích: Lấy SoCT hiện tại theo Sequence TCS_CTU_ID_SEQ.CURRVAL
        ' Người viết: Hoàng Văn Anh
        ' Ngày viết: 11/02/2009
        '-----------------------------------------------------------------------------------------------
        Public Shared Function GetSoCT_Current() As String
            Dim strResult As String = ""
            Try
                strResult = GetData_Seq("TCS_CTU_ID_SEQ.CURRVAL")
            Catch ex As Exception
                ' 'LogDebug.Writelog("Lỗi trong hàm lấy số chứng từ hiện tại GetSoCT_Current: " & ex.ToString())
            End Try

            Return strResult.PadLeft(7, "0")
        End Function
        '-----------------------------------------------------------------------------------------------
        ' Mục đích: Lấy SoCT tiếp theo của Sequence TCS_CTU_ID_SEQ.NEXTVAL
        ' Người viết: Hoàng Văn Anh
        ' Ngày viết: 11/02/2009
        '-----------------------------------------------------------------------------------------------
        Public Shared Function GetSoCT_Next() As String
            Dim strResult As String = ""
            Try
                strResult = GetData_Seq("TCS_CTU_ID_SEQ.NEXTVAL")
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong hàm lấy  số chứng từ tiếp theo GetSoCT_Next : " & ex.ToString())
            End Try

            Return strResult.PadLeft(5, "0")
        End Function

        '-----------------------------------------------------------------------------------------------
        ' Mục đích: Lấy SoBL theo Sequence TCS_CTUBL_ID_SEQ.CURRVAL
        ' Người viết: Hoàng Văn Anh
        ' Ngày viết: 11/02/2009
        '-----------------------------------------------------------------------------------------------
        Public Shared Function GetSoBL_Current() As String
            Dim strResult As String = ""
            Try
                strResult = GetData_Seq("TCS_CTUBL_ID_SEQ.CURRVAL")
            Catch ex As Exception
                ''LogDebug.Writelog("Lỗi trong hàm lấy số Biên Lai: " & ex.ToString())
            End Try

            Return strResult.PadLeft(7, "0")
        End Function
        '-----------------------------------------------------------------------------------------------
        ' Mục đích: Lấy SoBL tiếp theo Sequence TCS_CTUBL_ID_SEQ.NEXTVAL
        ' Người viết: Hoàng Văn Anh
        ' Ngày viết: 11/02/2009
        '-----------------------------------------------------------------------------------------------
        Public Shared Function GetSoBL_Next() As String
            Dim strResult As String = ""
            Try
                strResult = GetData_Seq("TCS_CTUBL_ID_SEQ.NEXTVAL")
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong hàm lấy số Biên Lai: " & ex.ToString())
            End Try

            Return strResult.PadLeft(5, "0")
        End Function

        '-----------------------------------------------------------------------------------------------
        ' Mục đích: Lấy giá trị của Sequence
        ' Đầu vào : strSeqname + .NEXTVAL hoặc .CURRVAL - Tên seq cần lấy
        ' Người viết: Hoàng Văn Anh
        ' Ngày viết: 11/02/2009
        '-----------------------------------------------------------------------------------------------
        Public Shared Function GetData_Seq(ByVal strSeqName As String) As String
            Dim strDataKey As String = "1"
            Dim strSQL As String = ""
            Dim KetNoi As DataAccess
            Try
                KetNoi = New DataAccess
                strSeqName = strSeqName.Trim()
                strSQL = "SELECT " & strSeqName & " FROM DUAL"
                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If (Not IsEmptyDataSet(ds)) And (ds.Tables(0).Rows.Count > 0) Then
                    strDataKey = GetString(ds.Tables(0).Rows(0)(0))
                Else
                    strDataKey = "1"
                End If
            Catch ex As Exception
                strDataKey = "1"
                ''LogDebug.Writelog("Lỗi trong quá trình sinh ID: " & ex.ToString)
            Finally
                If Not KetNoi Is Nothing Then
                    KetNoi.Dispose()
                    KetNoi = Nothing
                End If
            End Try

            Return strDataKey
        End Function

        'Hàm kiểm tra và lấy Số CT duy nhất
        'Hoàng Văn Anh
        Public Shared Function Valid_SoCT_GNTExisted(ByVal strSoCT, ByVal strKHCT) As String
            Dim strResult As String = strSoCT
            Try
                Dim bCheck As Boolean = Check_GNT_Existed(strResult, strKHCT)
                'Nếu đã tồn tại GNT(gồm có Số CT và Kí hiệu CT) thì lấy số chứng từ tăng lên tiếp
                While (bCheck = True)
                    'Lấy Số CT tiếp theo
                    strResult = GetSoCT_Next()
                    bCheck = Check_GNT_Existed(strResult, strKHCT)
                End While
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong hàm chuẩn hóa số chứng từ Valid_SoCT_GNTExited: " & ex.ToString())
            End Try
            Return strResult
        End Function
        'Kiểm tra sự tồn tại của GNT
        'Hoàng Văn Anh
        Public Shared Function Check_GNT_Existed(ByVal strSoCT As String, ByVal strKHCT As String) As Boolean
            Dim KetNoi As New DataAccess
            Dim drCT As IDataReader
            Dim strSQL As String
            Dim bResult As Boolean = False
            Try
                strSQL = "SELECT 1 FROM TCS_CTU_HDR WHERE " _
                    & " KyHieu_CT=" & Globals.EscapeQuote(strKHCT) _
                    & " AND So_CT=" & Globals.EscapeQuote(strSoCT)
                drCT = KetNoi.ExecuteDataReader(strSQL, CommandType.Text)
                If drCT.Read Then
                    'Số CT đã lập thì trả về giá trị TRUE
                    bResult = True
                Else
                    'Số CT chưa lập thì trả về giá trị FALSE
                    bResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra chứng từ CT đã tồn tại chưa Check_GNT_Existed: " & ex.ToString)
                bResult = True
            Finally
                If Not drCT Is Nothing Then
                    drCT.Dispose()
                End If
                If Not KetNoi Is Nothing Then
                    KetNoi.Dispose()
                    KetNoi = Nothing
                End If
            End Try
            ' Trả lại kết quả cho hàm
            Return bResult
        End Function

        'Hàm kiểm tra và lấy Số BL duy nhất
        'Hoàng Văn Anh
        Public Shared Function Valid_SoBL_Existed(ByVal strSoBL, ByVal strKHBL) As String
            Dim strResult As String = strSoBL
            Try
                Dim bCheck As Boolean = Check_BL_Existed(strResult, strKHBL)
                'Nếu đã tồn tại GNT(gồm có Số CT và Kí hiệu CT) thì lấy số chứng từ tăng lên tiếp
                While (bCheck = True)
                    'Lấy Số BL tiếp theo
                    strResult = GetSoBL_Next()
                    bCheck = Check_BL_Existed(strResult, strKHBL)
                End While
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong hàm chuẩn hóa số chứng từ Valid_SoBL_GNTExited: " & ex.ToString())
            End Try
            Return strResult
        End Function
        'Kiểm tra sự tồn tại của GNT
        'Hoàng Văn Anh
        Public Shared Function Check_BL_Existed(ByVal strSoBL As String, ByVal strKHBL As String) As Boolean
            Dim KetNoi As New DataAccess
            Dim drCT As IDataReader
            Dim strSQL As String
            Dim bResult As Boolean = False
            Try
                strSQL = "SELECT 1 FROM TCS_CTBL WHERE " _
                    & " KYHIEU_BL=" & Globals.EscapeQuote(strKHBL) _
                    & " AND SO_BL=" & Globals.EscapeQuote(strSoBL)
                drCT = KetNoi.ExecuteDataReader(strSQL, CommandType.Text)
                If drCT.Read Then
                    'Số CT đã lập thì trả về giá trị TRUE
                    bResult = True
                Else
                    'Số CT chưa lập thì trả về giá trị FALSE
                    bResult = False
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra Biên Lai đã tồn tại chưa Check_BL_Existed: " & ex.ToString)
                bResult = True
            Finally
                If Not drCT Is Nothing Then
                    drCT.Dispose()
                End If
                If Not KetNoi Is Nothing Then
                    KetNoi.Dispose()
                    KetNoi = Nothing
                End If
            End Try
            ' Trả lại kết quả cho hàm
            Return bResult
        End Function

#End Region



#Region "--- Một số chức năng bên Ngân Hàng ---"

        Public Shared Function Get_Map_TK_CQThu(ByVal strTKNo As String, ByVal strTKCo As String) As String
            '--------------------------------------------------------------------
            ' Mục đích: Get mã cơ quan thu theo tài khoản nợ - có
            ' Tham số : Tài khoản nợ - có
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 29/11/2009
            '---------------------------------------------------------------------
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strResult As String = ""

                Dim strSql As String = "select Ma_CQThu from TCS_MAP_TK_CTU where (instr('" & strTKNo.Replace(".", "") & "',trim(TK_No))=1) and (instr('" & strTKCo.Replace(".", "") & "',trim(TK_Co))=1)"
                Dim dr As IDataReader
                dr = conn.ExecuteDataReader(strSql, CommandType.Text)

                If dr.Read() Then
                    strResult = GetString(dr.GetString(0))
                End If
                dr.Dispose()

                Return strResult
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Function

        Public Shared Function Get_Map_BatBuocMLNS(ByVal intKieuCT As Integer) As Boolean
            Try
                'Kiểm tra điều kiện bắt buộc nhập MLNS
                Select Case intKieuCT
                    Case 1, 5, 10, 45, 50, 55, 105, 110, 115, 120, 125, 160, 165, 170, 175, 180
                        Return True
                    Case Else
                        Return False
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Get_Map_BatBuocTLDT(ByVal intKieuCT As Integer) As Boolean
            Try
                'Kiểm tra điều kiện bắt buộc nhập TLDT
                Select Case intKieuCT
                    Case 1, 5, 10, 105, 110, 115, 120, 125, 160, 165, 170, 175, 180
                        Return True
                    Case Else
                        Return False
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Get_Map_String(ByVal intMaMau As Integer) As String
            '--------------------------------------------------------------------
            ' Mục đích: Get các trường hiển thị theo mẫu nhập chứng từ
            ' Tham số : mã mẫu
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 29/11/2009
            '---------------------------------------------------------------------
            Try
                Select Case intMaMau
                    Case 0, 1
                        Return gstrMAP_LTSTTM
                    Case 2
                        Return gstrMAP_LTSTCK
                    Case 3
                        Return gstrMAP_TTTGTM
                    Case 4
                        Return gstrMAP_TTTGCK
                    Case 5
                        Return gstrMAP_CTTC
                    Case 6
                        Return gstrMAP_CTVATTMCK
                    Case Else
                        Return gstrMAP_LTSTTM
                End Select
                Return gstrMAP_LTSTTM
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetKieuCT(ByVal intKieuCT As Integer) As CTEnum
            Try
                'Kiểm tra chuyển khoản tại kho bạc hay ngân hàng?
                Select Case intKieuCT
                    Case 2, 5, 8, 11, 14, 17, 22, 25, 28  'Chuyển khoản tại Ngân hàng
                        Return CTEnum.CK_NganHang
                    Case 3, 6, 9, 12, 15, 18, 23, 26, 29   'Chuyển khoản tại Kho Bạc
                        Return CTEnum.CK_KhoBac
                End Select

                Return CTEnum.TienMat
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub Get_Map_TK_Form(ByVal strTKNo As String, ByVal strTKCo As String, ByRef intKieuMH As Integer, ByRef intKieuCT As Integer)
            '--------------------------------------------------------------------
            ' Mục đích: Get mẫu nhập chứng từ theo tài khoản nợ - có
            ' Tham số : Tài khoản nợ - có
            ' Người viết: Lê Hồng Hà
            ' Ngày viết: 29/11/2009
            '---------------------------------------------------------------------
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim dr As IDataReader
                Dim strSql As String = "select Kieu_MH, Kieu_CT from TCS_MAP_TK_CTU where (instr('" & strTKNo.Replace(".", "") & "',trim(TK_No))=1) and (instr('" & strTKCo.Replace(".", "") & "',trim(TK_Co))=1) and (TYPE = 'CT') order by tk_no asc, tk_co desc"
                dr = conn.ExecuteDataReader(strSql, CommandType.Text)
                If dr.Read() Then
                    intKieuMH = CInt(dr.GetString(0))
                    intKieuCT = CInt(dr.GetString(1))
                End If
                dr.Dispose()
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(conn)) Then conn.Dispose()
            End Try
        End Sub

        Public Shared Function IsTamThu(ByVal intKieuCT As Integer) As Boolean
            Try
                'Tạm thu + tạm giữ
                Select Case intKieuCT
                    Case 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70
                        Return True
                End Select
                Return False
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        '-----------------------------------------------------
        ' Người Viết: Hoàng Văn Anh
        ' Mục đích: Lấy trạng thái của biên lai
        ' ----------------------------------------------------
        Public Shared Function Get_TrangThai_BL(ByVal iSoBL As String, ByVal iSoBT As Integer) As String
            Dim strSQL As String = ""
            Dim KetNoi As DataAccess
            Dim strTrangThai As String = ""
            Try
                KetNoi = New DataAccess
                strSQL = "SELECT Trang_Thai FROM TCS_CTBL " _
                            & " Where (so_bl = '" & iSoBL & "') " & _
                            " And (so_bt = " & iSoBT & ") "
                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not IsEmptyDataSet(ds) Then
                    strTrangThai = GetString(ds.Tables(0).Rows(0)(0))
                Else
                    strTrangThai = ""
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy trạng thái của Biên Lai: " & ex.ToString())
            Finally
                If Not KetNoi Is Nothing Then
                    KetNoi.Dispose()
                    KetNoi = Nothing
                End If
            End Try

            Return strTrangThai
        End Function

        Public Shared Function Get_TenNganHang(ByVal strMaNH As String)
            '------------------------------------------------------------------
            ' Mục đích: Lấy tên ngân hàng
            ' Tham số: 
            ' Giá trị trả về: 
            ' Ngày viết: 03/11/2007
            ' Người viết: Lê Hồng Hà
            ' -----------------------------------------------------------------
            Dim strResult As String = ""
            Dim conn As DataAccess
            Dim drNT As IDataReader
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Select TEN From TCS_DM_NGANHANG Where Ma = '" & strMaNH & "'"
                drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
                If drNT.Read Then
                    strResult = drNT.GetString(0)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên ngân hàng: " & ex.ToString)
                Return ""
            Finally
                If Not drNT Is Nothing Then
                    drNT.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return strResult
        End Function

        Public Shared Sub Get_LHXNK_ALL(ByVal Ma_LH As String, ByRef Ten_LH As String, ByRef Ten_VT As String)
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên LH và Ten_VT của 1 mã loại hình bất kỳ.
            ' Tham số: Ma_LH: mã loại hình cần lấy tên
            ' Ngày viết: 14/10/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnTenCQ As DataAccess
            Dim drTenCQ As IDataReader
            Dim strSql As String
            Try
                cnTenCQ = New DataAccess
                strSql = "Select TEN_LH, TEN_VT From TCS_DM_LHINH Where Ma_LH='" & Ma_LH & "'"
                drTenCQ = cnTenCQ.ExecuteDataReader(strSql, CommandType.Text)
                If drTenCQ.Read Then
                    Ten_LH = Trim(drTenCQ.GetString(0))
                    Ten_VT = Trim(drTenCQ.GetString(1))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên Loại hình xuất nhập khẩu: " & ex.ToString)
            Finally
                If Not drTenCQ Is Nothing Then
                    drTenCQ.Close()
                End If
                If Not cnTenCQ Is Nothing Then
                    cnTenCQ.Dispose()
                End If
            End Try
        End Sub
        Public Shared Function Get_LTIEN_THUE(ByVal Ma_LT As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên LH và Ten_VT của 1 mã loại hình bất kỳ.
            ' Tham số: Ma_LH: mã loại hình cần lấy tên
            ' Ngày viết: 14/10/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim cnTenCQ As DataAccess
            Dim drTenCQ As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnTenCQ = New DataAccess
                strSql = "SELECT MA_LT ||'-'||ten_lt ten_lt from tcs_dm_loaitien_thue Where Ma_LT='" & Ma_LT & "'"
                drTenCQ = cnTenCQ.ExecuteDataReader(strSql, CommandType.Text)
                If drTenCQ.Read Then
                    strResult = Trim(drTenCQ.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên Loại hình xuất nhập khẩu: " & ex.ToString)
            Finally
                If Not drTenCQ Is Nothing Then
                    drTenCQ.Close()
                End If
                If Not cnTenCQ Is Nothing Then
                    cnTenCQ.Dispose()
                End If
            End Try
            Return strResult
        End Function

        Public Shared Function Get_HoTenNV(ByVal MaNV As String) As String
            '-----------------------------------------------------
            ' Mục đích: Lay ten nhan vien
            ' Tham số: 
            ' Giá trị trả về:   Ten nhan vien
            ' Ngày viết: 16/10/2007
            ' Người viết: Nguyen Ba Cuong
            ' ----------------------------------------------------
            Dim cnNV As DataAccess
            Dim drNV As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnNV = New DataAccess
                strSql = "Select TEN From TCS_DM_NHANVIEN Where MA_NV=" & MaNV
                drNV = cnNV.ExecuteDataReader(strSql, CommandType.Text)
                If drNV.Read Then
                    strResult = drNV.GetString(0)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên người dùng: " + ex.ToString)
            Finally
                If Not drNV Is Nothing Then
                    drNV.Dispose()
                End If
                If Not cnNV Is Nothing Then
                    cnNV.Dispose()
                End If
            End Try
            Return strResult
        End Function
        Public Shared Function Get_TellerNV(ByVal MaNV As String) As String
            '-----------------------------------------------------
            ' Mục đích: Lay ten nhan vien
            ' Tham số: 
            ' Giá trị trả về:   Ten nhan vien
            ' Ngày viết: 16/10/2007
            ' Người viết: Nguyen Ba Cuong
            ' ----------------------------------------------------
            Dim cnNV As DataAccess
            Dim drNV As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                cnNV = New DataAccess
                strSql = "Select teller_id From TCS_DM_NHANVIEN Where MA_NV=" & MaNV
                drNV = cnNV.ExecuteDataReader(strSql, CommandType.Text)
                If drNV.Read Then
                    strResult = drNV.GetString(0)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy tên người dùng: " + ex.ToString)
            Finally
                If Not drNV Is Nothing Then
                    drNV.Dispose()
                End If
                If Not cnNV Is Nothing Then
                    cnNV.Dispose()
                End If
            End Try
            Return strResult
        End Function

        Public Shared Function Get_Map_IsTamThu(ByVal intKieuCT As Integer) As Boolean
            Try

                'Tạm thu + tạm giữ
                Select Case intKieuCT
                    Case 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70
                        Return True
                End Select

                Return False
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Get_Map_KieuCT(ByVal intKieuCT As Integer) As CTEnum
            Try
                'Kiểm tra chuyển khoản tại kho bạc hay ngân hàng?
                Select Case intKieuCT
                    Case 5, 20, 35, 50, 65, 80, 95, 120, 135, 150 'Chuyển khoản tại Ngân hàng
                        Return CTEnum.CK_NganHang
                    Case 10, 25, 40, 55, 70, 85, 100, 125, 140, 155 'Chuyển khoản tại Kho Bạc
                        Return CTEnum.CK_KhoBac
                End Select

                Return CTEnum.TienMat
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        '-----------------------------------------------------
        ' Người Viết: Hoàng Văn Anh
        ' Mục đích: Lấy trạng thái của Chứng Từ
        ' ----------------------------------------------------
        Public Shared Function Get_TrangThai_CTU(ByVal key As KeyCTu) As String
            Dim strResult As String = ""
            Dim KetNoi As DataAccess
            Try
                KetNoi = New DataAccess
                Dim strSQL As String = "select a.Trang_Thai from TCS_CTU_HDR a " & _
                                "where (a.ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                                "and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _
                                "and (a.so_bt = " & key.So_BT.ToString() & ") " & _
                                "and (a.shkb = '" & key.SHKB.ToString() & "') " & _
                                "and (a.ma_dthu = '" & key.Ma_Dthu & "') "
                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not IsEmptyDataSet(ds) Then
                    strResult = GetString(ds.Tables(0).Rows(0)(0))
                Else
                    strResult = ""
                End If
            Catch ex As Exception
                strResult = ""
                'LogDebug.Writelog("Lỗi trong quá trình lấy trạng thái của chứng từ ex:" & ex.ToString())
            Finally
                If Not KetNoi Is Nothing Then
                    KetNoi.Dispose()
                    KetNoi = Nothing
                End If
            End Try

            Return strResult
        End Function


        'ICB-16/10/2008: Load tài khoản ngân hàng A,B
        'Hoàng Văn Anh
        'Public Shared Function MapTK_Load(ByVal strMa_DBHC As String) As ArrayList
        '    Dim strSQL As String
        '    Dim KetNoi As DataAccess
        '    Dim arr As New ArrayList
        '    Dim strWhere As String = " Where a.dbhc IN ("
        '    Try
        '        'Trường hợp Mã DBHC là tỉnh
        '        If strMa_DBHC.IndexOf("TTT") <> -1 Then
        '            strMa_DBHC = gstrMaDBHC
        '            strWhere += Globals.EscapeQuote(strMa_DBHC) & ")"
        '        Else
        '            'Nếu không phải thu hộ Tỉnh
        '            'Nếu trường hợp chỉ thu cho 1 DB thu
        '            If gstrDBThu.IndexOf(";") = -1 Then
        '                strMa_DBHC = gstrMaDBHC
        '                strWhere += Globals.EscapeQuote(strMa_DBHC) & ")"
        '            Else
        '                'Trường hợp thu cho nhiều DB thu
        '                'Trường hợp mã DBHC chọn là chính địa bàn thu đó
        '                If gstrDBThu.IndexOf(strMa_DBHC) <> -1 Then
        '                    strWhere += Globals.EscapeQuote(strMa_DBHC) & ")"
        '                Else
        '                    strWhere += "SELECT DISTINCT ma_cha FROM tcs_dm_xa WHERE ma_xa=" & Globals.EscapeQuote(strMa_DBHC) & ")"
        '                End If
        '            End If
        '        End If
        '        KetNoi = New DataAccess
        '        strSQL = "SELECT  a.ma_nh as NH_A, a.ma_kb as NH_B, a.tk_kb as TK_NO " _
        '            & " FROM tcs_map_tk_nh_kb a " & strWhere
        '        Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '        If (Not ds Is Nothing) And (ds.Tables(0).Rows.Count > 0) Then
        '            arr.Add(ds.Tables(0).Rows(0)(0)) 'NH
        '            arr.Add(ds.Tables(0).Rows(0)(1)) 'KB
        '            arr.Add(ds.Tables(0).Rows(0)(2)) 'Ma tk KB
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong hàmICB: MapTK_Load - " & ex.ToString())
        '    Finally
        '        If Not KetNoi Is Nothing Then
        '            KetNoi.Dispose()
        '            KetNoi = Nothing
        '        End If
        '    End Try
        '    Return arr
        'End Function
        'ICB-16/10/2008: Load tài khoản GL của ngân hàng 
        'Hoàng Văn Anh
        'Public Shared Function Get_TK_GL_NH(ByVal strMaDBHC As String) As String
        '    Dim strSQL As String
        '    Dim KetNoi As DataAccess
        '    Dim strMaDB As String = ""
        '    Dim strResult As String = ""
        '    Try
        '        '/// kiểm tra trường hợp ko phải là Tỉnh/TP nhưng thu hộ Tỉnh/TP
        '        If (Not IsTinh_TP()) And (strMaDBHC.IndexOf("TTT") <> -1) Then
        '            strMaDBHC = gstrMaDBHC
        '        End If
        '        KetNoi = New DataAccess
        '        strSQL = "SELECT tk.tk_gl_nh, a.ma_xa, a.ten " _
        '            & " FROM tcs_dm_xa a, tcs_map_tk_nh_kb tk" _
        '            & " WHERE (tk.dbhc = a.ma_cha OR tk.dbhc = a.ma_xa) AND a.ma_xa = " & Globals.EscapeQuote(strMaDBHC)

        '        Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '        If (Not ds Is Nothing) And (ds.Tables(0).Rows.Count > 0) Then
        '            strResult = CStr(ds.Tables(0).Rows(0)(0))
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Lỗi trong hàmICB: Get_TK_GL_NH - " & ex.ToString())
        '    Finally
        '        If Not KetNoi Is Nothing Then
        '            KetNoi.Dispose()
        '            KetNoi = Nothing
        '        End If
        '    End Try

        '    Return strResult
        'End Function

        Public Shared Function Exists_VangLai(ByVal strMaNNT As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của NNThuế trong sổ thuế
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' Ngày viết: 28/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select 1 " & _
                    "from tcs_dm_nnt_kb " & _
                    "where ma_nnt = '" & strMaNNT & "'"
                Dim blnReturn As Boolean = False
                Dim drNNT As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                If (drNNT.Read()) Then
                    blnReturn = True
                End If
                drNNT.Close()
                Return blnReturn
            Catch ex As Exception
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        'ICB-20/10/2008: Định dạng lại TK_NO xxx.xxx.xxx
        'Hoàng Văn Anh
        Public Shared Function Format_TK_NO(ByVal strInput As String) As String
            If strInput.Length > 3 Then
                Dim i As Integer = strInput.Length
                While i > 4
                    i -= 3
                    strInput = strInput.Insert(i, ".")
                End While
            End If
            Return strInput
        End Function

        'ICB: kiểm tra mã kiểm soát của chứng từ
        'Hoàng Văn Anh
        Public Shared Function Get_KSV_CTU(ByVal key As KeyCTu) As String
            Dim KetNoi As DataAccess
            Dim strTMP As String = ""
            Try
                KetNoi = New DataAccess
                Dim strSQL As String
                strSQL = "select MA_KS from tcs_ctu_hdr " & _
                        " where shkb = '" & key.SHKB.ToString() & "' and " & _
                        " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                        " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                        " so_bt = " & key.So_BT.ToString() & " and " & _
                        " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not IsEmptyDataSet(ds) Then
                    strTMP = CStr(ds.Tables(0).Rows(0)(0)).Trim()
                End If
            Catch ex As Exception
                Throw ex
            Finally
                If Not KetNoi Is Nothing Then
                    KetNoi.Dispose()
                    KetNoi = Nothing
                End If
            End Try

            Return strTMP
        End Function
        'ICB: lấy tên Nhân Viên
        'Hoàng Văn Anh
        Public Shared Function Get_NV(ByVal strMaNV As String) As String
            Dim KetNoi As DataAccess
            Dim strTMP As String = ""
            Try
                KetNoi = New DataAccess
                Dim strSQL As String
                strSQL = "SELECT a.ten" _
                    & " FROM tcs_dm_nhanvien a" _
                    & " WHERE a.ma_nv=" & CInt(strMaNV)
                Dim ds As DataSet = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not IsEmptyDataSet(ds) Then
                    strTMP = CStr(ds.Tables(0).Rows(0)(0)).Trim()
                End If
            Catch ex As Exception
                Throw ex
            Finally
                If Not KetNoi Is Nothing Then
                    KetNoi.Dispose()
                    KetNoi = Nothing
                End If
            End Try

            Return strTMP
        End Function

#End Region

        '/// Lấy giá trị của Tham số
        Public Shared Function Get_ParmValues(ByVal strParmName As String, ByVal pv_strMaDiemThu As String) As String
            Dim strSQL As String
            Dim strResult As String = ""
            Try
                strSQL = "SELECT a.giatri_ts  FROM tcs_thamso a  where a.ten_ts ='" & strParmName & "' and a.ma_dthu='" & pv_strMaDiemThu & "'"
                Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    strResult = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
            Finally
            End Try
            Return strResult
        End Function

        '/// lấy mô tả trong bảng tham số
        Public Shared Function Get_ParmDescript(ByVal strParmName As String) As String
            Dim strSQL As String

            Dim strResult As String = ""
            Try

                strSQL = "SELECT a.MO_TA  FROM tcs_thamso a  where a.ten_ts ='" & strParmName & "'"
                Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    strResult = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
            Finally

            End Try
            Return strResult
        End Function

        Public Shared Function ConvertDateToNumber(ByVal strDate As String) As Long
            '-----------------------------------------------------
            ' Mục đích: Chuyển một string ngày tháng 'dd/MM/yyyy' --> dạng số 'yyyyMMdd'.
            ' Tham số: string đầu vào
            ' Giá trị trả về: 
            ' Ngày viết: 18/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------
            Dim arr() As String

            Try
                Dim sep As String = "/"
                strDate = strDate.Trim()
                If Len(strDate) < 10 Then Return 0
                arr = strDate.Split(sep)
                sep = "/"
                arr(0) = arr(0).PadLeft(2, "0")
                arr(1) = arr(1).PadLeft(2, "0")
                If arr(2).Length = 2 Then
                    arr(2) = "20" & arr(2)
                End If

            Catch ex As Exception
                Return CLng(Now().ToString("yyyyMMdd"))
            End Try

            Return Long.Parse(arr(2) & arr(1) & arr(0))
        End Function

        Public Shared Function ConvertDateToNumber2(ByVal strDate As String) As Long
            Dim arr() As String

            Try
                Dim sep As String = "/"
                strDate = strDate.Trim()
                If Len(strDate) < 10 Then Return 0
                arr = strDate.Split(sep)
                sep = "/"
                arr(0) = arr(0).PadLeft(2, "0")
                arr(1) = arr(1).PadLeft(2, "0")
                arr(2) = arr(2).Substring(2, 2)

            Catch ex As Exception
                Return CLng(Now().ToString("yyMMdd"))
            End Try

            Return Long.Parse(arr(2) & arr(1) & arr(0))
        End Function

        Public Shared Function ConvertDateToNumber3(ByVal strDate As String) As Long
            Dim arr() As String

            Try
                Dim sep As String = "/"
                strDate = strDate.Trim()
                If Len(strDate) < 10 Then Return 0
                arr = strDate.Split(sep)
                sep = "/"
                arr(0) = arr(0).PadLeft(2, "0")
                arr(1) = arr(1).PadLeft(2, "0")
                arr(2) = arr(2).Substring(2, 2)

            Catch ex As Exception
                Return CLng(Now().ToString("ddMMyy"))
            End Try

            Return Long.Parse(arr(0) & arr(1) & arr(2))
        End Function

        '// Lấy tên thể hiện trạng thái
        Public Shared Function Get_TrangThai(ByVal strIDTrangThai) As String
            Dim strResult As String
            Select Case (strIDTrangThai)
                Case "00" '// chưa KS
                    strResult = "Chưa Kiểm Soát"
                Case "01" '// đã KS
                    strResult = "Đã Kiểm Soát"
                Case "03" '// KS lỗi
                    strResult = "Kiểm Soát Lỗi"
                Case "04" '// đã Hủy
                    strResult = "Đã Hủy"
                Case Else
                    strResult = strIDTrangThai
            End Select
            Return strResult
        End Function
        Public Shared Function Get_TrangThaiBL(ByVal strIDTrangThai) As String
            Dim strResult As String
            Select Case (strIDTrangThai)
                Case "00" '// chưa KS
                    strResult = "Chưa Lập CT"
                Case "01" '// đã KS
                    strResult = "Đã kiểm soát"
                Case "02" '// đã KS
                    strResult = "Đã Lập CT"
                Case "03" '// KS lỗi
                    strResult = "Đã Hủy"
                Case "04" '// KS lỗi
                    strResult = "Đã chuyển trả"
                Case "05" '// KS lỗi
                    strResult = "Đã chuyển KS"
                Case Else
                    strResult = strIDTrangThai
            End Select
            Return strResult
        End Function

        '// Lấy Ảnh thể hiện trạng thái
        Public Shared Function Get_ImgTrangThai(ByVal strIDTrangThai) As String
            Dim strResult As String
            Select Case (strIDTrangThai)
                Case "00" '// chưa KS
                    strResult = "Images/icons/ChuaKS.ico"
                Case "02" '// đã KS
                    strResult = "Images/icons/DaKS.ico"
                Case "03" '// KS lỗi
                    strResult = "Images/icons/KSLoi.ico"
                Case "04" '// đã Hủy
                    strResult = "Images/icons/Huy.ico"
                Case Else
                    strResult = strIDTrangThai
            End Select
            Return strResult
        End Function
        Public Shared Function Get_ImgTrangThaiBL(ByVal strIDTrangThai) As String
            Dim strResult As String
            Select Case (strIDTrangThai)
                Case "00" '// chưa KS
                    strResult = "../../images/icons/ChuaKS.ico"
                Case "01" '// Kiểm soát
                    strResult = "../../images/icons/DaKS.ico"
                Case "02" '// Đã lập thành chưng từ
                    strResult = "../../images/icons/DaIN.ico"
                Case "03" '// Hủy
                    strResult = "../../images/icons/Huy.ico"
                Case "04" '// Chuyển trả
                    strResult = "../../images/icons/KSLoi.ico"
                Case "05" '// Chuyển kiểm soát
                    strResult = "../../images/icons/lock_on.png"
                Case Else
                    strResult = strIDTrangThai
            End Select
            Return strResult
        End Function



        Public Shared Function Get_BanLV(ByVal iMaNV As Integer) As Integer
            Dim iBanLV As Integer
            Try
                Dim strSQL = "SELECT a.ban_lv " _
                    & " FROM tcs_dm_nhanvien a " _
                    & " WHERE a.ma_nv=" & iMaNV
                Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    iBanLV = CInt(dt.Rows(0)(0).ToString())
                Else
                    iBanLV = 0
                End If
            Catch ex As Exception
                iBanLV = 0
            Finally

            End Try
            Return iBanLV
        End Function

        Public Shared Function Get_TenNV(ByVal iMaNV As Integer) As String
            Dim strTenNV As String
            Try
                Dim strSQL = "SELECT a.TEN " _
                    & " FROM tcs_dm_nhanvien a " _
                    & " WHERE a.ma_nv=" & iMaNV
                Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    strTenNV = dt.Rows(0)(0).ToString()
                Else
                    strTenNV = "GUEST"
                End If
            Catch ex As Exception
                strTenNV = "GUEST"
            Finally

            End Try
            Return strTenNV
        End Function

        Public Shared Function Get_ChucVuNV(ByVal iMaNV As Integer) As String
            Dim strTenNV As String
            Try
                Dim strSQL = "SELECT a.CHUC_DANH " _
                    & " FROM tcs_dm_nhanvien a " _
                    & " WHERE a.ma_nv=" & iMaNV
                Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    strTenNV = dt.Rows(0)(0).ToString()
                Else
                    strTenNV = "GUEST"
                End If
            Catch ex As Exception
                strTenNV = "GUEST"
            Finally

            End Try
            Return strTenNV
        End Function
        Public Shared Function Get_DThu(ByVal strSHKB As String) As String
            Dim strSQL As String
            Dim strResult As String = ""
            Try
                strSQL = "SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "'"
                Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    strResult = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
            Finally
            End Try
            Return strResult
        End Function

        Public Shared Function GetMaDBHC_KB(ByVal strSHKB As String) As String
            Try
                Dim strSQL As String = String.Empty
                strSQL = "SELECT  ma_db FROM tcs_dm_khobac where shkb= '" & strSHKB & "'"
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Shared Function GetMaDBHCAll(ByVal strMa_Xa As String, ByVal strTen_Xa As String) As String
            Try
                Dim strSQL As String = String.Empty
                strSQL = "SELECT ma_xa, ten  FROM  TCS_DM_Xa" _
                                & " WHERE ma_xa LIKE '" & strMa_Xa & "%'  and UPPER(TEN) like '%" & strTen_Xa.ToUpper() & "%'  " _
                                & " ORDER BY  ma_xa "
                Return strSQL
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Shared Function GetMaDBHC_TINH(ByVal strMa_Xa As String, ByVal strTen_Xa As String) As String
            Try
                Dim strSQL As String = String.Empty
                strSQL = "SELECT ma_xa as MA_DM, ten as TEN_DM  FROM  TCS_DM_Xa" _
                                & " WHERE ma_xa like '%TT' and ma_xa LIKE '" & strMa_Xa & "%'  and UPPER(TEN) like '%" & strTen_Xa.ToUpper() & "%'  " _
                                & " ORDER BY  ma_xa "
                Return strSQL
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Shared Function GetMaDBHC_HUYEN(ByVal strMa_Xa As String, ByVal strTen_Xa As String) As String
            Try
                Dim strSQL As String = String.Empty
                strSQL = "SELECT ma_xa as MA_DM, ten as TEN_DM FROM  TCS_DM_Xa" _
                                & " WHERE ma_xa like '%HH' and ma_xa LIKE '" & strMa_Xa & "%'  and UPPER(TEN) like '%" & strTen_Xa.ToUpper() & "%'  " _
                                & " ORDER BY  ma_xa "
                Return strSQL
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        '------------------------------------------------------------------------------
        'ICB: Hoàng Văn Anh
        'Mục đích: lấy String select danh sách Địa bàn thu
        '/// Chú ý: trường hợp thu hộ tỉnh/Tp và trường hợp là Tỉnh/TP
        '------------------------------------------------------------------------------
        Public Shared Function GetString_DBThu(ByVal strInput As String, ByVal strTenDBHC As String, ByVal strSHKB As String) As String
            Dim strSQL As String
            Dim strDK_DBHC As String = ""
            Dim strDK_DBHC_Cha As String = ""
            Dim strMaTinhTP As String = ""
            Dim strMaDBHC As String = ""
            Dim arrDBThu As String()
            Dim strDBThu As String = ""
            Dim strMaDiemThu As String
            Try
                'strMaDiemThu = Get_DThu(strSHKB)
                strMaDBHC = GetMaDBHC_KB(strSHKB)
                strSQL = " SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE b.ma_cha ='" & strMaDBHC & "' union SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE b.ma_xa ='" & strMaDBHC & "' "
                ''/// Lấy mã tỉnh thành phố
                'strMaTinhTP = Get_ParmValues("MA_TTP", strMaDiemThu)
                ''/// Lấy biến tham số Địa Bàn Thu
                'strDBThu = Get_ParmValues("DB_THU", strMaDiemThu).Trim()
                'arrDBThu = strDBThu.Split(";"c)
                'If arrDBThu.Length > 0 Then
                '    Dim i As Integer
                '    For i = 0 To arrDBThu.Length - 1
                '        If (arrDBThu(i).Length >= 0) And (arrDBThu(i).Trim() <> strMaDBHC) Then
                '            strDK_DBHC &= Globals.EscapeQuote(arrDBThu(i).ToString()) & ","
                '        End If
                '    Next
                '    '/// Xóa bỏ dấu "," cuối cùng ở xâu điều kiện
                '    If strDK_DBHC <> "" Then
                '        strDK_DBHC = strDK_DBHC.Substring(0, strDK_DBHC.Length - 1)
                '    Else
                '        strDK_DBHC = "''"
                '    End If
                'End If

                'strDK_DBHC_Cha = strDK_DBHC & "," & Globals.EscapeQuote(strMaDBHC)
                ''/// Xét trường hợp DHBC này là tỉnh/TP
                'If IsTinh_TP(strMaDBHC) Then
                '    '/// không thu hộ Địa Bàn nào khác 
                '    If strDK_DBHC = "''" Then
                '        strSQL = "SELECT ma_xa, ten  FROM " _
                '                & " (SELECT DISTINCT ma_xa,  ten, 1 as TT  FROM tcs_dm_xa WHERE ma_xa = " & Globals.EscapeQuote(strMaDBHC) & ") DBHC  " _
                '                & " WHERE ma_xa LIKE '" & strInput & "%'  and UPPER(TEN) like '%" & strTenDBHC.ToUpper() & "%' " _
                '                & " ORDER BY  ma_xa "
                '    Else
                '        '/// Trường hợp thu hộ địa bàn khác
                '        strSQL = "SELECT ma_xa, ten  FROM " & _
                '                " (SELECT DISTINCT ma_xa,  ten, 1 as TT  FROM tcs_dm_xa WHERE (ma_xa = " & Globals.EscapeQuote(strMaDBHC) & ") " & _
                '                " UNION " & _
                '                " SELECT DISTINCT ma_xa,  ten, 2 as TT  FROM tcs_dm_xa WHERE (ma_xa IN (" & strDK_DBHC & ")) " & _
                '                " UNION " & _
                '                " SELECT DISTINCT ma_xa, ten, 3 as TT FROM tcs_dm_xa WHERE (ma_cha IN (" & strDK_DBHC_Cha & ")) " & _
                '                " ) DBHC " & _
                '                " WHERE ma_xa LIKE '" & strInput & "%' " & _
                '                " ORDER BY TT, ma_xa "
                '    End If
                'Else
                '    '/// Trường hợp DHBC này không phải là tỉnh/TP 
                '    '/// Và không thu hộ địa bàn nào khác
                '    If strDK_DBHC = "''" Then
                '        strSQL = "SELECT ma_xa, ten  FROM " & _
                '                " (SELECT DISTINCT ma_xa,  ten, 1 as TT  FROM tcs_dm_xa WHERE (ma_xa = " & Globals.EscapeQuote(strMaDBHC) & ") " & _
                '                " UNION " & _
                '                " SELECT DISTINCT ma_xa, ten, 2 as TT FROM tcs_dm_xa WHERE (ma_cha IN (" & Globals.EscapeQuote(strMaDBHC) & ")) " & _
                '                " ) DBHC " & _
                '                " WHERE ma_xa LIKE '" & strInput & "%'  and UPPER(TEN) like '%" & strTenDBHC.ToUpper() & "%'  " & _
                '                " order by TT, ma_xa "
                '    Else
                '        '/// Trường hợp này là thu hộ địa bàn khác nữa (loại bỏ các điểm con của Tỉnh/TP)
                '        strSQL = "SELECT ma_xa, ten  FROM " & _
                '                " (SELECT DISTINCT ma_xa,  ten, 1 as TT  FROM tcs_dm_xa WHERE (ma_xa = " & Globals.EscapeQuote(strMaDBHC) & ") " & _
                '                " UNION " & _
                '                " SELECT DISTINCT ma_xa,  ten, 2 as TT  FROM tcs_dm_xa WHERE (ma_xa IN (" & strDK_DBHC & ")) " & _
                '                " UNION " & _
                '                " SELECT DISTINCT ma_xa, ten, 3 as TT FROM tcs_dm_xa WHERE (ma_cha IN (" & strDK_DBHC_Cha & ") AND ma_cha <> " & Globals.EscapeQuote(strMaTinhTP) & "  ) " & _
                '                " ) DBHC " & _
                '                " WHERE ma_xa LIKE '" & strInput & "%' " & _
                '                " order by TT, ma_xa "
                '    End If
                'End If
            Catch ex As Exception
                Throw ex
            End Try
            Return strSQL
        End Function
        Public Shared Function GetString_DBThu_NNT() As String
            Dim strSQL As String
            Dim strDK_DBHC As String = ""
            Dim strDK_DBHC_Cha As String = ""
            Dim strMaTinhTP As String = ""
            Dim strMaDBHC As String = ""
            Dim arrDBThu As String()
            Dim strDBThu As String = ""
            Dim strMaDiemThu As String
            Try
                strSQL = " SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE b.ma_xa like '___HH'"
            Catch ex As Exception
                Throw ex
            End Try
            Return strSQL
        End Function

        Public Shared Function IsTinh_TP(ByVal gstrMaDBHC As String) As Boolean
            If gstrMaDBHC.IndexOf("TTT") <> -1 Then
                Return True
            End If
            Return False
        End Function
        'Public Shared Function GetNgayLV(ByVal strMa_NV As String) As String
        '    Try
        '        Dim strSQL As String = String.Empty
        '        strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
        '        Return ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString)
        '    Catch ex As Exception
        '        Return Nothing
        '    End Try
        'End Function
        Public Shared Function check_esixst_SHKB(ByVal shkb As String) As Boolean
            Dim connect As DataAccess
            Try
                connect = New DataAccess
                Dim strSql As String = "select * from tcs_dm_cqthu where shkb='" & shkb & "'"
                Dim ds As DataSet = connect.ExecuteReturnDataSet(strSql, CommandType.Text)
                If IsEmptyDataSet(ds) Then
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Shared Function check_esixst_DBHC(ByVal maDBHC As String) As Boolean
            Dim connect As DataAccess
            Try
                connect = New DataAccess
                Dim strSql As String = "select * from tcs_dm_xa where ma_xa='" & maDBHC & "'"
                Dim ds As DataSet = connect.ExecuteReturnDataSet(strSql, CommandType.Text)
                If IsEmptyDataSet(ds) Then
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                Return False
            Finally
                connect.Dispose()
            End Try
        End Function
        Public Shared Function check_and_province(ByVal maDBHC As String, ByVal shkb As String) As Boolean
            Dim connect As DataAccess
            Try

            Catch ex As Exception
                Return False
            Finally
                connect.Dispose()
            End Try
        End Function
        Public Shared Function GetSO_FT(ByVal key As KeyCTu) As DataSet
            Try
                Dim strSQL As String = String.Empty
                strSQL = "SELECT p.response_msg,p.branch_no from tcs_ctu_hdr hdr, tcs_log_payment p where p.rm_ref_no=hdr.rm_ref_no and " & _
                " shkb = '" & key.SHKB.ToString() & "' and " & _
                    " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                    " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                    " so_bt = " & key.So_BT.ToString()
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Shared Function CheckHanMuc_KSV(ByVal strMa_KSV As String, ByVal str_TongTrichNo As Decimal) As Boolean
            Dim ds As DataSet
            Dim boResult As Boolean = False
            Try
                Dim strSQL As String = String.Empty
                strSQL = "Select nvl(hanmuc_tu,0) hanmuc_tu,nvl(hanmuc_den,0) hanmuc_den from tcs_dm_nhanvien where  " & _
                " ma_nv = '" & strMa_KSV & "' "
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Dim tien_hanmuc_tu As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0)("hanmuc_tu"))
                Dim tien_hanmuc_den As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0)("hanmuc_den"))
                If tien_hanmuc_tu = 0 And tien_hanmuc_den = 0 Then
                    Return True
                End If
                If str_TongTrichNo > tien_hanmuc_tu And str_TongTrichNo < tien_hanmuc_den Then
                    boResult = True
                End If
            Catch ex As Exception
                Return Nothing
            End Try
            Return boResult
        End Function
        Public Shared Function Get_NH_A_NH_TT(ByVal strSHKB As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên xã của 1 mã XÃ bất kỳ.
            ' Tham số: MaXa: mã XÃ cần lấy tên
            ' Giá trị trả về: Tên xã
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenXa As New DataAccess
            Dim drTenXa As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "select a.ma_tructiep, a.ten_tructiep,a.ma_giantiep,a.ten_giantiep from tcs_dm_nh_giantiep_tructiep a where shkb='" & strSHKB & "'" ' & Globals.EscapeQuote(strSHKB.Replace(".", "") & "'")
                drTenXa = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenXa.Read Then
                    strResult = Trim(drTenXa.GetString(0)) & ";" & Trim(drTenXa.GetString(1)) & ";" & Trim(drTenXa.GetString(2)) & ";" & Trim(drTenXa.GetString(3))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên xã của 1 mã XÃ bất kỳ: " & ex.ToString)
            Finally
                drTenXa.Close()
                cnTenXa.Dispose()
            End Try
            Return strResult
        End Function
        Public Shared Function Get_MA_TINH_THANH_SHKB(ByVal strSHKB As String) As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên xã của 1 mã XÃ bất kỳ.
            ' Tham số: MaXa: mã XÃ cần lấy tên
            ' Giá trị trả về: Tên xã
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenXa As New DataAccess
            Dim drTenXa As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "select c.Ten from tcs_dm_khobac a,tcs_dm_xa c where a.ma_db=c.ma_xa and a.shkb='" & strSHKB & "'" ' & Globals.EscapeQuote(strSHKB.Replace(".", "") & "'")
                drTenXa = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenXa.Read Then
                    strResult = Trim(drTenXa.GetString(0))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên xã của 1 mã XÃ bất kỳ: " & ex.ToString)
            Finally
                drTenXa.Close()
                cnTenXa.Dispose()
            End Try
            Return strResult
        End Function
        Public Shared Function Get_NH_A() As String
            '---------------------------------------------------------------
            ' Mục đích: Lấy ra tên xã của 1 mã XÃ bất kỳ.
            ' Tham số: MaXa: mã XÃ cần lấy tên
            ' Giá trị trả về: Tên xã
            ' Ngày viết: 02/11/2003
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------
            Dim cnTenXa As New DataAccess
            Dim drTenXa As IDataReader
            Dim strSql As String
            Dim strResult As String = ""
            Try
                strSql = "select a.ma_nh_chuyen,a.Ten_nh_chuyen from tcs_nganhang_chuyen a" ' & Globals.EscapeQuote(strSHKB.Replace(".", "") & "'")
                drTenXa = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If drTenXa.Read Then
                    strResult = Trim(drTenXa.GetString(0)) & ";" & Trim(drTenXa.GetString(1))
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên xã của 1 mã XÃ bất kỳ: " & ex.ToString)
            Finally
                drTenXa.Close()
                cnTenXa.Dispose()
            End Try
            Return strResult
        End Function



        Public Shared Function compress(ByVal text As String) As String
            Try
                Dim buffer As Byte() = Encoding.UTF8.GetBytes(text)
                Dim ms As New MemoryStream()
                Using zip As New GZipStream(ms, CompressionMode.Compress, True)
                    zip.Write(buffer, 0, buffer.Length)
                End Using
                Return Convert.ToBase64String(ms.ToArray())
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog("Error source: " + ex.Source + vbLf + "Error code: System error!" + vbLf + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.[Error])
                Return ""
            End Try
        End Function

        Public Shared Function decompress(ByVal compressedText As String) As String

            Try
                Dim gzBuffer As Byte() = Convert.FromBase64String(compressedText)

                Using ms As New MemoryStream()
                    Dim msgLength As Integer = BitConverter.ToInt32(gzBuffer, 0)
                    'gzBuffer.Length;
                    ms.Write(gzBuffer, 0, gzBuffer.Length)

                    Dim buffer As Byte() = New Byte(msgLength - 1) {}

                    ms.Position = 0
                    Using zip As New GZipStream(ms, CompressionMode.Decompress)
                        zip.Read(buffer, 0, buffer.Length)
                    End Using

                    Return Encoding.UTF8.GetString(buffer)
                End Using
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   LogDebug.WriteLog("Error source: " + ex.Source + vbLf + "Error code: System error!" + vbLf + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.[Error])
                Return ""
            End Try

        End Function

        '=======================================================
        'Service provided by Telerik (www.telerik.com)
        'Conversion powered by NRefactory.
        'Twitter: @telerik
        'Facebook: facebook.com/telerik
        '=======================================================
        Public Shared Function Get_ErrorDesciption(ByVal strParmName As String) As String
            Dim strSQL As String
            Dim strResult As String = ""
            Try
                strSQL = "SELECT a.Desciption  FROM tcs_DefError a  where a.ErrCode ='" & strParmName & "' "
                Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    strResult = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
                strResult = "Lỗi chưa được định nghĩa"
            Finally
            End Try
            Return strResult
        End Function
        Public Shared Function Get_ErrorDesciptionCoreBank(ByVal strParmName As String) As String
            Dim strSQL As String
            Dim strResult As String = ""
            Try
                Select Case strParmName
                    Case "002"
                        Return "Timeout from Back End"

                    Case "001"
                        Return "Error in SOA system"
                    Case "201"
                        Return "Invalid input data"
                    Case "301"
                        Return "Invalid output data"
                    Case "400"
                        Return "Error in Backend"
                    Case "005"
                        Return "Not connect to backend"
                    Case "006"
                        Return "SOA(Timeout)"

                End Select

            Catch ex As Exception
                strResult = "Lỗi chưa được định nghĩa"
            Finally
            End Try
            Return strResult
        End Function
        Public Shared Function Get_ImgTrangThai304(ByVal strIDTrangThai As String) As String
            Dim strResult As String
            Select Case (strIDTrangThai)

                Case TCS_HQ247_TRANGTHAI_304_MOINHAN
                    strResult = "../../images/icons/icon_daimport.gif"
                Case TCS_HQ247_TRANGTHAI_304_MAKER213
                    strResult = "../../images/icons/ChuaKS.png"
                Case TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI
                    strResult = "../../images/icons/ChuaKS.png"

                Case TCS_HQ247_TRANGTHAI_304_CHEKER213
                    strResult = "../../images/icons/HuyHs.gif"
                    ' strResult = "../../images/icons/HuyHs.gif"
                Case "03" '// Chuyển trả
                    strResult = "../../images/icons/KSLoi.png"
                Case "04" '// Hủy KSV đã duyệt - Hủy bởi GDV (tùy tình huống)  => Handx Tuy ngan hang
                    strResult = "../../images/icons/Huy.png"
                Case "05" '// Chuyển kiểm soát
                    strResult = "../../images/icons/ChuyenKS.png"
                Case "06" '// Chuyển kiểm soát
                    strResult = "../../images/icons/ChuaNhapBDS.png"
                Case "07" '// Chuyển kiểm soát
                    strResult = "../../images/icons/Huy_KS.png"
                Case "08" '// Chuyển kiểm soát
                    strResult = "../../images/icons/Huy_CT_Loi.png"
                Case "19" 'điều chỉnh chờ kiểm soát
                    strResult = "../../images/icons/ChuaNhapBDS.png"
            End Select
            Return strResult
        End Function
        Public Shared Function ConvertStringToDate(ByVal pStrDate As String, ByVal pStrFormat As String) As Date
            Dim strChar As String = "/"
            Dim strDD As String = "DD"
            Dim strMM As String = "MM"
            Dim strYYYY As String = "YYYY"
            Dim strday As String = ""
            Dim strMonth As String = ""
            Dim strYear As String = ""
            Try
                If pStrFormat.IndexOf(strChar) > 0 Then
                    Dim strTemp As String() = pStrFormat.Split(strChar)
                    Dim strDatetem As String() = pStrDate.Split(strChar)
                    For i As Integer = 0 To strTemp.Count - 1
                        If strTemp(i).ToUpper().Equals(strDD) Then
                            strday = strDatetem(i)
                        ElseIf strTemp(i).ToUpper().Equals(strMM) Then
                            strMonth = strDatetem(i)
                        ElseIf strTemp(i).ToUpper().Equals(strYYYY) Then
                            strYear = strDatetem(i)
                        End If
                    Next
                Else
                    strChar = "-"
                End If
                If pStrFormat.IndexOf(strChar) > 0 Then
                    Dim strTemp As String() = pStrFormat.Split(strChar)
                    Dim strDatetem As String() = pStrDate.Split(strChar)
                    For i As Integer = 0 To strTemp.Count - 1
                        If strTemp(i).ToUpper().Equals(strDD) Then
                            strday = strDatetem(i)
                        ElseIf strTemp(i).ToUpper().Equals(strMM) Then
                            strMonth = strDatetem(i)
                        ElseIf strTemp(i).ToUpper().Equals(strYYYY) Then
                            strYear = strDatetem(i)
                        End If
                    Next
                Else
                    strday = pStrDate.Substring(pStrFormat.ToUpper().IndexOf(strDD), 2)
                    strMonth = pStrDate.Substring(pStrFormat.ToUpper().IndexOf(strMM), 2)
                    strYear = pStrDate.Substring(pStrFormat.ToUpper().IndexOf(strYYYY), 4)
                End If
                Return New Date(Integer.Parse(strYear), Integer.Parse(strMonth), Integer.Parse(strday))
            Catch ex As Exception

            End Try

            Return Date.Now
        End Function
        Public Shared Function NumberToStringVN(ByVal strData As String)
            Try
                If strData.IndexOf(".") > 0 Then
                    Dim strArr As String() = strData.Split(".")
                    If strArr.Length > 1 Then
                        Return Decimal.Parse(strArr(0)).ToString("#,###").Replace(",", ".") & "," & strArr(1)
                    Else
                        Return Decimal.Parse(strData).ToString("#,###").Replace(",", ".")
                    End If
                Else
                    Return Decimal.Parse(strData).ToString("#,###").Replace(",", ".")
                End If
            Catch ex As Exception
                Return strData
            End Try
            Return strData
        End Function
        Public Shared Function NumberToStringVN2(ByVal strData As String)
            Try
                If Decimal.Parse(strData) = 0 Then
                    Return "0"
                End If
                If strData.IndexOf(".") > 0 Then
                    Dim strArr As String() = strData.Split(".")
                    If strArr.Length > 1 Then
                        Return Decimal.Parse(strArr(0)).ToString("#,###").Replace(",", ".") & "," & strArr(1)
                    Else
                        Return Decimal.Parse(strData).ToString("#,###").Replace(",", ".")
                    End If
                Else
                    Return Decimal.Parse(strData).ToString("#,###").Replace(",", ".")
                End If
            Catch ex As Exception
                Return strData
            End Try
            Return strData
        End Function
        Public Shared Function fnc_GET_TENTRANGTHAI_HS(ByVal strTRANGTHAI As String)
            Try
                Dim strSQL As String = "SELECT MOTA FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI='" & strTRANGTHAI & "' "
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return ds.Tables(0).Rows(0)(0).ToString()
                        End If
                    End If
                End If
            Catch ex As Exception
                Return ""
            End Try
            Return ""
        End Function
        Public Shared Function fnc_getMessageHQ(ByVal strID As String, ByVal strIndex As Integer)
            Try
                Dim strSQL As String = "SELECT MSG_IN,MSG_OUT FROM TCS_LOG_MSG_HQ WHERE TRANSACTION_ID='" & strID & "' "
                If strIndex = 0 Then
                    strSQL = "SELECT MSG_IN,MSG_OUT FROM TCS_LOG_MSG_HQ WHERE TRANSACTION_ID='" & strID & "' "
                Else
                    strSQL = "SELECT MSG_IN,MSG_OUT FROM TCS_LOG_MSG_HQ WHERE REQUEST_ID='" & strID & "' "
                End If
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return ds.Tables(0).Rows(0)(strIndex).ToString()
                        End If
                    End If
                End If
            Catch ex As Exception
                Return ""
            End Try
            Return ""
        End Function
        Public Enum DTLColumn As Byte
            ChkRemove = 0
            MaQuy = 1
            CChuong = 2
            LKhoan = 3
            MTieuMuc = 4
            NoiDung = 5
            Tien = 6
            Tien_NT = 7
            SoDT = 8
            KyThue = 9
            SoTK = 10
            LoaiTT = 11
            SacThue = 12
            NgayTK = 13
            MaLHXNK = 14
            MaHQ = 15
        End Enum

        Public Shared Function DataSet2Table(ByVal ds As DataSet) As DataTable
            Dim dt As DataTable = Nothing
            Try
                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            Catch ex As Exception
                dt = Nothing
            End Try

            Return dt
        End Function

        Public Shared Function GetKyThue(ByVal strNgayLV As String) As String
            Dim sRet As String = ""
            Try
                If Not String.IsNullOrEmpty(strNgayLV) AndAlso strNgayLV.Length = 8 Then
                    sRet = strNgayLV.Substring(4, 2) & "/" & strNgayLV.Substring(0, 4)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        ''' <summary>
        ''' HungVD: Convert a string to date. This's not goood idea and also not good performance, it's for temporary solution
        ''' </summary>
        ''' <param name="str">input string</param>
        ''' <param name="sInputFormat">input format</param>
        ''' <param name="dOutputDate">output date</param>
        ''' <returns>Success: return converted date. Otherwise: return current date</returns>
        ''' <remarks>Only support for some input format. You can extend more input format if you want</remarks>
        Public Shared Function ConvertStringToDate(ByVal str As String, ByVal sInputFormat As String, ByRef dOutputDate As Date) As Boolean
            Dim bRet As Boolean = False
            Dim iDay, iMonth, iYear As Integer
            Try
                dOutputDate = DateTime.Now
                If Not String.IsNullOrEmpty(str) Then
                    Select Case sInputFormat
                        Case "dd/MM/yyyy"
                            If Not Integer.TryParse(str.Substring(0, 2), iDay) Then
                                Throw New Exception("Error get Day")
                            End If
                            If Not Integer.TryParse(str.Substring(3, 2), iMonth) Then
                                Throw New Exception("Error get Month")
                            End If
                            If Not Integer.TryParse(str.Substring(6, 4), iYear) Then
                                Throw New Exception("Error get Year")
                            End If
                        Case "ddMMyyy"
                            If Not Integer.TryParse(str.Substring(0, 2), iDay) Then
                                Throw New Exception("Error get Day")
                            End If
                            If Not Integer.TryParse(str.Substring(2, 2), iMonth) Then
                                Throw New Exception("Error get Month")
                            End If
                            If Not Integer.TryParse(str.Substring(4, 4), iYear) Then
                                Throw New Exception("Error get Year")
                            End If
                        Case "MM/dd/yyyy"
                            If Not Integer.TryParse(str.Substring(0, 2), iMonth) Then
                                Throw New Exception("Error get Day")
                            End If
                            If Not Integer.TryParse(str.Substring(3, 2), iDay) Then
                                Throw New Exception("Error get Month")
                            End If
                            If Not Integer.TryParse(str.Substring(6, 4), iYear) Then
                                Throw New Exception("Error get Year")
                            End If
                        Case "MMddyyyy"
                            If Not Integer.TryParse(str.Substring(0, 2), iMonth) Then
                                Throw New Exception("Error get Day")
                            End If
                            If Not Integer.TryParse(str.Substring(2, 2), iDay) Then
                                Throw New Exception("Error get Month")
                            End If
                            If Not Integer.TryParse(str.Substring(4, 4), iYear) Then
                                Throw New Exception("Error get Year")
                            End If
                        Case "yyyy/MM/dd"
                            If Not Integer.TryParse(str.Substring(8, 2), iDay) Then
                                Throw New Exception("Error get Day")
                            End If
                            If Not Integer.TryParse(str.Substring(5, 2), iMonth) Then
                                Throw New Exception("Error get Month")
                            End If
                            If Not Integer.TryParse(str.Substring(0, 4), iYear) Then
                                Throw New Exception("Error get Year")
                            End If
                        Case "yyyyMMdd"
                            If Not Integer.TryParse(str.Substring(6, 2), iDay) Then
                                Throw New Exception("Error get Day")
                            End If
                            If Not Integer.TryParse(str.Substring(4, 2), iMonth) Then
                                Throw New Exception("Error get Month")
                            End If
                            If Not Integer.TryParse(str.Substring(0, 4), iYear) Then
                                Throw New Exception("Error get Year")
                            End If
                        Case "yyyy/dd/MM"
                            If Not Integer.TryParse(str.Substring(8, 2), iMonth) Then
                                Throw New Exception("Error get Day")
                            End If
                            If Not Integer.TryParse(str.Substring(5, 2), iDay) Then
                                Throw New Exception("Error get Month")
                            End If
                            If Not Integer.TryParse(str.Substring(0, 4), iYear) Then
                                Throw New Exception("Error get Year")
                            End If
                        Case "yyyyddMM"
                            If Not Integer.TryParse(str.Substring(4, 2), iDay) Then
                                Throw New Exception("Error get Day")
                            End If
                            If Not Integer.TryParse(str.Substring(6, 2), iMonth) Then
                                Throw New Exception("Error get Month")
                            End If
                            If Not Integer.TryParse(str.Substring(0, 4), iYear) Then
                                Throw New Exception("Error get Year")
                            End If
                        Case Else
                            Throw New Exception("Not support this format")
                    End Select
                End If

                If IsNumeric(iDay) AndAlso IsNumeric(iMonth) AndAlso IsNumeric(iYear) Then
                    If iDay > 0 AndAlso iDay < 32 AndAlso iMonth > 0 AndAlso iMonth < 13 AndAlso iYear > 0 Then
                        dOutputDate = New Date(iYear, iMonth, iDay) 'if 31/02... => throw exception
                        Dim sRet As String = dOutputDate.ToString("dd/MM/yyyy")
                        bRet = True
                    End If
                End If
            Catch ex As Exception
                bRet = False
                dOutputDate = DateTime.Now
                'Throw ex
            End Try

            Return bRet
        End Function
    End Class





End Namespace