Imports System.Data
Imports VBOracleLib
Imports Business_HQ
Imports System.Text

''' <summary>
''' 
''' 
''' </summary>
''' <remarks></remarks>
Public Class CTuCommon
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    '/// Lấy giá trị của Tham số hệ thống
    Public Shared Function GetValue_THAMSOHT(ByVal strParmName As String) As String
        Dim strSQL As String
        Dim strResult As String = ""
        Try
            strSQL = "SELECT a.giatri_ts  FROM tcs_thamso_ht a  where upper(a.ten_ts) =upper('" & strParmName & "')"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                strResult = dt.Rows(0)(0).ToString()
            End If
        Catch ex As Exception
        Finally

        End Try
        Return strResult
    End Function

    Public Shared Function DataSet2Table(ByVal ds As DataSet) As DataTable
        Dim dt As DataTable = Nothing
        If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt
    End Function


    '/// Lấy giá trị của Tham số
    Public Shared Function Get_ParmValues(ByVal strParmName As String, ByVal strMa_DThu As String) As String
        Dim strSQL As String
        Dim strResult As String = ""
        Try
            strSQL = "SELECT a.giatri_ts  FROM tcs_thamso a  where a.ten_ts ='" & strParmName & "' And Ma_DTHU = '" & strMa_DThu & "'"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                strResult = dt.Rows(0)(0).ToString()
            End If
        Catch ex As Exception
        Finally

        End Try
        Return strResult
    End Function

    '/// lấy mô tả trong bảng tham số
    Public Shared Function Get_ParmDescript(ByVal strParmName As String) As String
        Dim strSQL As String

        Dim strResult As String = ""
        Try

            strSQL = "SELECT a.MO_TA  FROM tcs_thamso a  where a.ten_ts ='" & strParmName & "'"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                strResult = dt.Rows(0)(0).ToString()
            End If
        Catch ex As Exception
        Finally

        End Try
        Return strResult
    End Function

    Public Shared Function ConvertDateToNumber(ByVal strDate As String) As Long
        '-----------------------------------------------------
        ' Mục đích: Chuyển một string ngày tháng 'dd/MM/yyyy' --> dạng số 'yyyyMMdd'.
        ' Tham số: string đầu vào
        ' Giá trị trả về: 
        ' Ngày viết: 18/10/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------
        Dim arr() As String

        Try
            Dim sep As String = "/"
            strDate = strDate.Trim()
            If Len(strDate) < 10 Then Return 0
            arr = strDate.Split(sep)
            sep = "/"
            arr(0) = arr(0).PadLeft(2, "0")
            arr(1) = arr(1).PadLeft(2, "0")
            If arr(2).Length = 2 Then
                arr(2) = "20" & arr(2)
            End If

        Catch ex As Exception
            Return CLng(Now().ToString("yyyyMMdd"))
        End Try

        Return Long.Parse(arr(2) & arr(1) & arr(0))
    End Function

    '// Lấy tên thể hiện trạng thái
    Public Shared Function Get_TrangThai(ByVal strIDTrangThai) As String
        Dim strResult As String
        Select Case (strIDTrangThai)
            Case "00" '// chưa KS
                strResult = "Chưa Kiểm Soát"
            Case "01" '// đã KS
                strResult = "Đã Kiểm Soát"
            Case "03" '// KS lỗi
                strResult = "Kiểm Soát Lỗi"
            Case "04" '// đã Hủy
                strResult = "Đã Hủy"
            Case "05" '// đã Hủy
                strResult = "Chờ kiểm soát"
            Case "06" '// đã Hủy
                strResult = "Chưa nhập BDS"
            Case "07"
                strResult = "Đã thanh toán"
            Case "02"
                strResult = "Hủy bởi GDV"
            Case Else
                strResult = strIDTrangThai
        End Select
        Return strResult
    End Function

    '// Lấy tên thể hiện trạng thái
    Public Shared Function Get_TrangThai_KS(ByVal strIDTrangThai) As String
        Dim strResult As String
        Select Case (strIDTrangThai)
            Case "00" '// chưa KS
                strResult = "Chưa kiểm soát"
            Case "01" '// đã KS
                strResult = "Đã Kiểm Soát"
            Case "03" '// KS lỗi
                strResult = "Chuyển trả"
            Case "04" '// đã Hủy
                strResult = "Đã hủy bởi GDV"
            Case "05" '// đã Hủy
                strResult = "Chờ kiểm soát"
            Case "06" '// đã Hủy
                strResult = "Chuyển Thuế/HQ lỗi"
            Case "07"
                strResult = "Đã hủy bởi KSV"
            Case "02"
                strResult = "Hủy bởi GDV chờ duyệt"
            Case "08"
                strResult = "Duyệt hủy chuyển thuế HQuan lỗi"
            Case Else
                strResult = strIDTrangThai
        End Select
        Return strResult
    End Function

    Public Shared Function Get_ImgTrangThai(ByVal strIDTrangThai As String, _
                Optional ByVal strTT_CThue As String = "0", Optional ByVal strTT_KS As String = "0", Optional ByVal strTT_TT As String = "0") As String
        Dim strResult As String
        Select Case (strIDTrangThai)

            Case "00" '// chưa KS
                strResult = "../../images/icons/ChuaKS.png"
            Case "01" '// đã KS
                If (strTT_CThue.Equals("0")) Then
                    strResult = "../../images/icons/TT_CThueLoi.gif"
                Else
                    If strTT_TT.Equals("1") Then
                        strResult = "../../images/icons/Da_TT.png"
                    Else
                        strResult = "../../images/icons/DaKS.png"
                    End If
                End If
            Case "02" ' Hủy chứng từ sau bước KS 1 bởi GDV - chưa duyệt (sau bước KSV duyệt CTu lúc lập) ???
                strResult = "../../images/icons/HuyHs.gif"
            Case "03" '// Chuyển trả
                strResult = "../../images/icons/KSLoi.png"
            Case "04" '// Hủy KSV đã duyệt - Hủy bởi GDV (tùy tình huống)  => Handx Tuy ngan hang

                'If strTT_CThue.Equals("1") Then
                '    strResult = "../../images/icons/Huy_CT_Loi.png"
                'Else
                '    If strTT_KS.Equals("0") Then
                '        strResult = "../../images/icons/Huy.png"
                '    Else
                '        strResult = "../../images/icons/Huy_KS.png"
                '    End If
                'End If

                strResult = "../../images/icons/Huy.png"
            Case "05" '// Chuyển kiểm soát
                strResult = "../../images/icons/ChuyenKS.png"
            Case "06" '// Chuyển kiểm soát
                strResult = "../../images/icons/ChuaNhapBDS.png"
            Case "07" '// Chuyển kiểm soát
                strResult = "../../images/icons/Huy_KS.png"
            Case "08" '// Chuyển kiểm soát
                strResult = "../../images/icons/Huy_CT_Loi.png"
            Case "19" 'điều chỉnh chờ kiểm soát
                strResult = "../../images/icons/ChuaNhapBDS.png"
        End Select
        Return strResult
    End Function

    Public Shared Function Get_ImgTrangThai_KS(ByVal strIDTrangThai As String, _
                Optional ByVal strTT_CThue As String = "0", Optional ByVal strTT_KS As String = "0", Optional ByVal strTT_TT As String = "0") As String
        Dim strResult As String
        Select Case (strIDTrangThai)


            Case "00" '// chưa KS
                strResult = "../../images/icons/ChuaKS.png"
            Case "01" '// đã KS
                If (strTT_CThue.Equals("0")) Then
                    strResult = "../../images/icons/TT_CThueLoi.gif"
                Else
                    If strTT_TT.Equals("1") Then
                        strResult = "../../images/icons/DaKS.png"
                    Else
                        strResult = "../../images/icons/DaKS.png"
                    End If
                End If
            Case "02" ' Hủy chứng từ sau bước KS 1 bởi GDV - chưa duyệt (sau bước KSV duyệt CTu lúc lập) ???
                strResult = "../../images/icons/HuyHs.gif"
            Case "03" '// Chuyển trả
                strResult = "../../images/icons/KSLoi.png"
            Case "04" '// Hủy KSV đã duyệt - Hủy bởi GDV (tùy tình huống)  => Handx Tuy ngan hang

                strResult = "../../images/icons/Huy.png"
            Case "05" '// Chuyển kiểm soát
                strResult = "../../images/icons/ChuyenKS.png"
            Case "06" '// Chuyển kiểm soát
                strResult = "../../images/icons/ChuaKS.png"
            Case "07" '// Chuyển kiểm soát
                strResult = "../../images/icons/Huy_KS.png"
            Case "08" '// Chuyển kiểm soát
                strResult = "../../images/icons/Huy_CT_Loi.png"
            Case "19" 'điều chỉnh chờ kiểm soát
                strResult = "../../images/icons/ChuaNhapBDS.png"
        End Select
        Return strResult
    End Function

    Public Shared Function Get_ImgTrangThai_frm_HOME(ByVal strIDTrangThai As String, _
                Optional ByVal strTT_CThue As String = "0", Optional ByVal strTT_KS As String = "0", Optional ByVal strTT_TT As String = "0") As String
        Dim strResult As String
        Select Case (strIDTrangThai)
            Case "00" '// chưa KS
                strResult = "../images/icons/ChuaKS.png"
            Case "01" '// đã KS
                If (strTT_CThue.Equals("0")) Then
                    strResult = "../images/icons/TT_CThueLoi.gif"
                Else
                    If strTT_TT.Equals("1") Then
                        strResult = "../images/icons/Da_TT.png"
                    Else
                        strResult = "../images/icons/DaKS.png"
                    End If
                End If
            Case "03" '// KS lỗi
                strResult = "../images/icons/KSLoi.png"
            Case "04" '// đã Hủy
                If strTT_CThue.Equals("1") Then
                    strResult = "../images/icons/Huy_CT_Loi.png"
                Else
                    If strTT_KS.Equals("0") Then
                        strResult = "../images/icons/Huy.png"
                    Else
                        strResult = "../images/icons/Huy_KS.png"
                    End If
                End If
            Case "05" '// da thu 1 phan
                strResult = "../images/icons/ChuyenKS.png"
            Case "06" '// chua nhap bds
                strResult = "../images/icons/ChuaNhapBDS.png"
            Case "07" '// da thu 1 phan
                strResult = "../images/icons/ChuyenKsHS.gif"

            Case "02" '// da huy boi GDV
                strResult = "../images/icons/HuyHs.gif"
            Case Else
                'strResult = strIDTrangThai
                strResult = "../images/icons/DaKS.png"
        End Select
        Return strResult
    End Function

    '// Lấy Ảnh thể hiện trạng thái

    '// Lấy tên trạng thái
    Public Shared Function Get_TenTrangThai(ByVal strIDTrangThai As String) As String
        Dim strResult As String
        Select Case (strIDTrangThai)
            Case "00" '// chưa KS
                strResult = "Chưa kiểm soát"
            Case "01" '// đã KS
                strResult = "Đã kiểm soát"
            Case "03" '// KS lỗi
                strResult = "Kiểm soát lỗi"
            Case "04" '// đã Hủy
                strResult = "Đã hủy"
            Case "05" '// da thu 1 phan
                strResult = "Chờ kiểm soát"
            Case "06" '// chua nhap bds
                strResult = "Chưa nhập PDS"
            Case "07" '// chua nhap bds
                strResult = "Tu chỉnh chuyển KS"
            Case Else
                'strResult = strIDTrangThai
                strResult = ""
        End Select
        Return strResult
    End Function

    Public Shared Function Get_ChucVuNV(ByVal iMaNV As Integer) As String
        Dim strTenNV As String
        Try
            Dim strSQL = "SELECT a.CHUC_DANH " _
                & " FROM tcs_dm_nhanvien a " _
                & " WHERE a.ma_nv=" & iMaNV
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                strTenNV = dt.Rows(0)(0).ToString()
            Else
                strTenNV = "GUEST"
            End If
        Catch ex As Exception
            strTenNV = "GUEST"
        Finally

        End Try
        Return strTenNV
    End Function


    '------------------------------------------------------------------------------
    'ICB: Hoàng Văn Anh
    'Mục đích: lấy String select danh sách Địa bàn thu
    '/// Chú ý: trường hợp thu hộ tỉnh/Tp và trường hợp là Tỉnh/TP
    '------------------------------------------------------------------------------
    Public Shared Function GetString_DBThu(ByVal gstrMaDBHC As String, ByVal strInput As String, Optional ByVal strMA_DTHU As String = "") As String
        Dim strSQL As String = ""
        Dim strDK_DBHC As String = ""
        Dim strDK_DBHC_Cha As String = ""
        Dim strMaTinhTP As String = ""
        Dim arrDBThu As String()
        Dim strDBThu As String = ""
        Try
            '/// Lấy mã tỉnh thành phố
            strMaTinhTP = CTuCommon.Get_ParmValues("MA_TTP", strMA_DTHU)
            '/// Lấy biến tham số Địa Bàn Thu
            strDBThu = CTuCommon.Get_ParmValues("DB_THU", strMA_DTHU).Trim()
            arrDBThu = strDBThu.Split(";"c)
            If arrDBThu.Length > 0 Then
                Dim i As Integer
                For i = 0 To arrDBThu.Length - 1
                    If (arrDBThu(i).Length >= 0) And (arrDBThu(i).Trim() <> gstrMaDBHC) Then
                        strDK_DBHC &= Globals.EscapeQuote(arrDBThu(i).ToString()) & ","
                    End If
                Next
                '/// Xóa bỏ dấu "," cuối cùng ở xâu điều kiện
                If strDK_DBHC <> "" Then
                    strDK_DBHC = strDK_DBHC.Substring(0, strDK_DBHC.Length - 1)
                Else
                    strDK_DBHC = "''"
                End If
            End If

            strDK_DBHC_Cha = strDK_DBHC & "," & Globals.EscapeQuote(gstrMaDBHC)
            '/// Xét trường hợp DHBC này là tỉnh/TP
            If IsTinh_TP(gstrMaDBHC) Then
                '/// không thu hộ Địa Bàn nào khác 
                If strDK_DBHC = "''" Then
                    strSQL = "SELECT ma_xa, ten  FROM " _
                            & " (SELECT DISTINCT b.ma_xa,  b.ten, 1 as TT  FROM tcs_dm_xa b,tcs_map_tk_nh_kb a WHERE b.ma_xa = " & Globals.EscapeQuote(gstrMaDBHC) & "  and a.dbhc = b.ma_xa) DBHC  " _
                            & " WHERE ma_xa LIKE '" & strInput & "%' " _
                            & " ORDER BY  ma_xa "
                Else
                    '/// Trường hợp thu hộ địa bàn khác
                    strSQL = "SELECT ma_xa, ten  FROM " & _
                            " (SELECT DISTINCT ma_xa,  ten, 1 as TT  FROM tcs_dm_xa b,tcs_map_tk_nh_kb a WHERE (b.ma_xa = " & Globals.EscapeQuote(gstrMaDBHC) & " and a.dbhc = b.ma_xa) " & _
                            " UNION " & _
                            " SELECT DISTINCT ma_xa,  ten, 2 as TT  FROM tcs_dm_xa WHERE (ma_xa IN (" & strDK_DBHC & ")) " & _
                            " UNION " & _
                            " SELECT DISTINCT ma_xa, ten, 3 as TT FROM tcs_dm_xa WHERE (ma_cha IN (" & strDK_DBHC_Cha & ")) " & _
                            " ) DBHC " & _
                            " WHERE ma_xa LIKE '" & strInput & "%' " & _
                            " ORDER BY TT, ma_xa "
                End If
            Else
                '/// Trường hợp DHBC này không phải là tỉnh/TP 
                '/// Và không thu hộ địa bàn nào khác
                If strDK_DBHC = "''" Then
                    strSQL = "SELECT ma_xa, ten  FROM " & _
                            " (SELECT DISTINCT b.ma_xa,  b.ten, 1 as TT  FROM tcs_dm_xa b,tcs_map_tk_nh_kb a WHERE (b.ma_xa = " & Globals.EscapeQuote(gstrMaDBHC) & ")  and a.dbhc = b.ma_xa" & _
                            " UNION " & _
                            " SELECT DISTINCT ma_xa, ten, 2 as TT FROM tcs_dm_xa WHERE (ma_cha IN (" & Globals.EscapeQuote(gstrMaDBHC) & ")) " & _
                            " ) DBHC " & _
                            " WHERE ma_xa LIKE '" & strInput & "%' " & _
                            " order by TT, ma_xa "
                Else
                    '/// Trường hợp này là thu hộ địa bàn khác nữa (loại bỏ các điểm con của Tỉnh/TP)
                    strSQL = "SELECT ma_xa, ten  FROM " & _
                            " (SELECT DISTINCT b.ma_xa,  b.ten, 1 as TT  FROM  tcs_dm_xa b,tcs_map_tk_nh_kb a WHERE (b.ma_xa = " & Globals.EscapeQuote(gstrMaDBHC) & ")  and a.dbhc = b.ma_xa" & _
                            " UNION " & _
                            " SELECT DISTINCT b.ma_xa,  b.ten, 2 as TT  FROM  tcs_dm_xa b,tcs_map_tk_nh_kb a WHERE (ma_xa IN (" & strDK_DBHC & ")) and a.dbhc = b.ma_xa " & _
                            " UNION " & _
                            " SELECT DISTINCT ma_xa, ten, 3 as TT FROM tcs_dm_xa WHERE (ma_cha IN (" & strDK_DBHC_Cha & ") AND ma_cha <> " & Globals.EscapeQuote(strMaTinhTP) & "  ) " & _
                            " ) DBHC " & _
                            " WHERE ma_xa LIKE '" & strInput & "%' " & _
                            " order by TT, ma_xa "
                End If
            End If
        Catch ex As Exception
            'LogDebug.Writelog("Lỗi trong Hàm GetString_DBThu" & ex.ToString())
        Finally
        End Try
        '///Trả lại kết quả cho Hàm
        Return strSQL
    End Function

    Public Shared Function IsTinh_TP(ByVal gstrMaDBHC As String) As Boolean
        If gstrMaDBHC.IndexOf("TTT") <> -1 Then
            Return True
        End If
        Return False
    End Function

    'Lấy tên Nguyên Tệ
    'Public Shared Function Get_TenNT(ByVal strIDNT As String) As String
    '    Dim strReturn As String = ""
    '    Try
    '        Dim strSQL As String = "SELECT a.ten  FROM tcs_dm_nguyente a WHERE a.ma_nt=" & Globals.EscapeQuote(strIDNT)
    '        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
    '        If Not Globals.IsNullOrEmpty(dt) Then
    '            strReturn = dt.Rows(0)(0).ToString().ToUpper()
    '        End If
    '    Catch ex As Exception

    '    End Try
    '    Return strReturn
    'End Function

    'Lấy Tên Loại Thuế
    Public Shared Function Get_TenLoaiThue(ByVal strIDLoaiThue As String) As String
        Dim strReturn As String = ""
        Try
            Dim strSQL As String = "SELECT a.ten TITLE  FROM tcs_dm_lthue a WHERE a.ma_lthue=" & Globals.EscapeQuote(strIDLoaiThue) _
            & " ORDER BY a.ma_lthue"

            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                strReturn = dt.Rows(0)(0).ToString().ToUpper()
            End If
        Catch ex As Exception

        End Try

        Return strReturn
    End Function

    'Lấy Tên tài khoản của Ngân Hang
    'Public Shared Function Get_TenTKNH(ByVal strIDTK As String) As String
    '    Dim strReturn As String = ""
    '    Try
    '        Dim strSQL As String = "select  a.mo_ta as Title from tcs_thamso a where a.ten_ts =" & Globals.EscapeQuote(strIDTK)
    '        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
    '        If Not Globals.IsNullOrEmpty(dt) Then
    '            strReturn = dt.Rows(0)(0).ToString().ToUpper()
    '        End If
    '    Catch ex As Exception

    '    End Try

    '    Return strReturn
    'End Function

    'Public Shared Function Get_HeaderCTu(ByVal keyCT As CTuKey) As CTuHeader
    '    Dim ctuHeader As CTuHeader = Nothing
    '    Try
    '        Dim strSQL As String = ""
    '        ctuHeader = New CTuHeader
    '        strSQL = "select a.SHKB,kb.ten tenkb,a.Ngay_KB,a.Ma_NV," & _
    '               "a.So_BT,a.Ma_DThu,a.So_BThu," & _
    '               "a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
    '               "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
    '               "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
    '               "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
    '               "to_char(a.Ngay_QD,'dd/MM/rrrr') as Ngay_QD ,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
    '               "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
    '               "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
    '               "a.So_TK,to_char(a.Ngay_TK,'dd/MM/rrrr') as Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
    '               "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
    '               "a.So_Khung,a.So_May,a.TK_KH_NH,to_char(a.NGAY_KH_NH,'dd/MM/rrrr') as NGAY_KH_NH," & _
    '               "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
    '               "a.Lan_In,a.Trang_Thai,a.so_bt_ktkb, " & _
    '               "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, " & _
    '               "nha.ten as TEN_NH_A, nhb.ten as TEN_NH_B, " & _
    '               "xa.ten as TenXa, tkno.ten_tk as Ten_TKNo, tkco.ten_tk as Ten_TKCo, " & _
    '               "cqthu.ten as Ten_CQThu, nt.ten as Ten_NT, lt.ten as Ten_LThue,a.PT_TT as PT_TT, " & _
    '               "a.So_BK, to_char(a.Ngay_BK,'dd/MM/yyyy') as Ngay_BK " & _
    '               "from TCS_CTU_HDR a,TCS_DM_KHOBAC kb, TCS_DM_XA xa, TCS_DM_TAIKHOAN tkno, TCS_DM_TAIKHOAN tkco, " & _
    '               "TCS_DM_CQTHU cqthu, TCS_DM_NGUYENTE nt, TCS_DM_LTHUE lt, TCS_DM_NGANHANG nha, TCS_DM_NGANHANG nhb " & _
    '               "where (a.xa_id = xa.xa_id(+))" & _
    '               "and (a.tk_no = tkno.tk (+)) and (a.tk_co = tkco.tk(+)) and (a.ma_nh_a = nha.ma(+)) and (a.ma_nh_b = nhb.ma(+)) " & _
    '               "and (a.ma_cqthu = cqthu.ma_cqthu(+)) and (a.ma_nt = nt.ma_nt(+)) and (a.ma_lthue = lt.ma_lthue(+)) " & _
    '               "and (a.shkb = kb.shkb) " & _
    '               "and (a.ngay_kb = " & keyCT.NgayKB & ") " & _
    '               "and (a.ma_nv = " & keyCT.MaNV & ") " & _
    '               "and (a.so_bt = " & keyCT.SoBT & ") " & _
    '               "and (a.shkb = '" & keyCT.SHKB & "') " & _
    '               "and (a.ma_dthu = '" & keyCT.MaDT & "') "
    '        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
    '        If Not Globals.IsNullOrEmpty(dt) Then
    '            '// lấy bộ key
    '            ctuHeader.SHKB = keyCT.SHKB
    '            ctuHeader.Ngay_KB = keyCT.NgayKB
    '            ctuHeader.Ma_DThu = keyCT.MaDT
    '            ctuHeader.So_BT = keyCT.SoBT
    '            ctuHeader.Ma_NV = keyCT.MaNV
    '            ctuHeader.KyHieu_CT = keyCT.KHCT
    '            '// lấy các thông tin khác
    '            ctuHeader.TenKB = dt.Rows(0)("TenKB").ToString()

    '            ctuHeader.So_CT = dt.Rows(0)("So_CT").ToString()
    '            ctuHeader.So_CTNH = dt.Rows(0)("So_CT_NH").ToString()
    '            ctuHeader.Ma_NNThue = dt.Rows(0)("Ma_NNThue").ToString()
    '            ctuHeader.Ten_NNThue = dt.Rows(0)("Ten_NNThue").ToString()
    '            ctuHeader.DC_NNThue = dt.Rows(0)("DC_NNThue").ToString()
    '            ctuHeader.Ma_NNTien = dt.Rows(0)("Ma_NNTien").ToString()
    '            ctuHeader.Ten_NNTien = dt.Rows(0)("Ten_NNTien").ToString()
    '            ctuHeader.DC_NNTien = dt.Rows(0)("DC_NNTien").ToString()

    '            ctuHeader.LyDo = dt.Rows(0)("Ly_Do").ToString()
    '            ctuHeader.Ma_KS = dt.Rows(0)("Ma_KS").ToString()
    '            ctuHeader.Ma_TQ = dt.Rows(0)("Ma_TQ").ToString()

    '            ctuHeader.SoQD = dt.Rows(0)("So_QD").ToString()
    '            ctuHeader.NgayQD = dt.Rows(0)("Ngay_QD").ToString()
    '            ctuHeader.CoQuanQD = dt.Rows(0)("CQ_QD").ToString()
    '            ctuHeader.NgayCT = CInt(dt.Rows(0)("Ngay_CT").ToString())
    '            ctuHeader.NgayHT = dt.Rows(0)("Ngay_HT").ToString()
    '            ctuHeader.MaCQThu = dt.Rows(0)("Ma_CQThu").ToString()
    '            ctuHeader.Xa_ID = dt.Rows(0)("Xa_ID").ToString()
    '            ctuHeader.MaXa = dt.Rows(0)("Ma_Xa").ToString()
    '            'ctuHeader.TK_NO = dt.Rows(0)("TK_No").ToString()
    '            ctuHeader.TK_CO = dt.Rows(0)("TK_Co").ToString()
    '            ctuHeader.PT_ThanhToan = dt.Rows(0)("PT_TT").ToString() 'PT_TT
    '            If ctuHeader.PT_ThanhToan.Trim() = "01" Then
    '                ctuHeader.TK_NO = Globals.Format_Number(Get_TKCK_NH(dt.Rows(0)("Ma_DThu").ToString()), ".")
    '            Else
    '                ctuHeader.TK_NO = Globals.Format_Number(Get_TKTM_NH(dt.Rows(0)("Ma_DThu").ToString()), ".")
    '            End If
    '            ctuHeader.SoToKhai = dt.Rows(0)("So_TK").ToString()
    '            ctuHeader.NgayToKhai = dt.Rows(0)("Ngay_TK").ToString()
    '            ctuHeader.LH_XNK = dt.Rows(0)("LH_XNK").ToString()
    '            ctuHeader.SoKhung = dt.Rows(0)("So_Khung").ToString()
    '            ctuHeader.SoMay = dt.Rows(0)("So_May").ToString()
    '            ctuHeader.TK_KH_NH = dt.Rows(0)("TK_KH_NH").ToString()
    '            ctuHeader.Ngay_KH_NH = dt.Rows(0)("NGAY_KH_NH").ToString()
    '            ctuHeader.MaNH_A = dt.Rows(0)("MA_NH_A").ToString()
    '            ctuHeader.MaNH_B = dt.Rows(0)("MA_NH_B").ToString()

    '            ctuHeader.TongTien = dt.Rows(0)("TTien").ToString()
    '            ctuHeader.TongTien_ThucThu = dt.Rows(0)("TT_TThu").ToString()

    '            ctuHeader.LanIn = dt.Rows(0)("Lan_In").ToString()
    '            ctuHeader.TrangThai = dt.Rows(0)("Trang_Thai").ToString()
    '            ctuHeader.So_BT_KTKB = dt.Rows(0)("so_bt_ktkb").ToString()

    '            ctuHeader.TongTien_NT = dt.Rows(0)("TTien_NT").ToString()
    '            ctuHeader.TK_KH_Nhan = dt.Rows(0)("TK_KH_Nhan").ToString()
    '            ctuHeader.Ten_KH_Nhan = dt.Rows(0)("Ten_KH_Nhan").ToString()
    '            ctuHeader.DC_KH_Nhan = dt.Rows(0)("Diachi_KH_Nhan").ToString()

    '            ctuHeader.Ma_LThue = dt.Rows(0)("Ma_LThue").ToString()
    '            ctuHeader.Ma_NguyenTe = dt.Rows(0)("Ma_NT").ToString()

    '            ctuHeader.So_BK = dt.Rows(0)("So_BK").ToString()
    '            ctuHeader.Ngay_BK = dt.Rows(0)("Ngay_BK").ToString()

    '        End If
    '    Catch ex As Exception

    '    End Try

    '    Return ctuHeader
    'End Function


    ''// lấy phần chi tiết của chứng từ
    'Public Shared Function Get_DetailCTU(ByVal keyCT As CTuKey) As DataTable
    '    Dim dt As New DataTable
    '    Try
    '        Dim strSQL As String = "select ID,CCH_ID,MaQuy,Ma_Chuong,LKH_ID,Ma_Khoan,MTM_ID,Ma_TMuc,Noi_Dung," & _
    '           " Ky_Thue,SoTien,SoTien_NT, Ma_DP from TCS_CTU_DTL " & _
    '           " where (shkb = " & Globals.EscapeQuote(keyCT.SHKB) & ") and (ngay_kb = " & keyCT.NgayKB & ") " & _
    '           " and (ma_nv = " & keyCT.MaNV & ") and (so_bt = " & keyCT.SoBT & ") and (ma_dthu = " & Globals.EscapeQuote(keyCT.MaDT) & ") "
    '        dt = DatabaseHelp.ExecuteToTable(strSQL)
    '    Catch ex As Exception
    '    End Try
    '    Return dt
    'End Function

    '// lấy tên dbhc
    Public Shared Function Get_TenDBHC(ByVal strMaDBHC As String) As String
        Dim strResult As String = ""
        Try
            Dim strSql As String = "Select TEN From TCS_DM_XA Where MA_XA=" & Globals.EscapeQuote(strMaDBHC)
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                strResult = dt.Rows(0)(0).ToString()
            End If
        Catch ex As Exception

        End Try
        Return strResult
    End Function

    '// Lấy tên tài khoản
    Public Shared Function Get_TenTaiKhoan(ByVal strID As String) As String
        Dim strResult As String = ""
        Try
            Dim strSql As String = "Select Ten_TK From TCS_DM_TAIKHOAN Where TK=" & Globals.EscapeQuote(strID)
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                strResult = dt.Rows(0)(0).ToString()
            End If
        Catch ex As Exception

        End Try
        Return strResult
    End Function

    '// Lấy tên cơ quan thu
    Public Shared Function Get_TenCQThu(ByVal strID As String) As String
        Dim strResult As String = ""
        Try
            Dim strSql As String = "Select TEN From TCS_DM_CQTHU Where Ma_CQThu=" & Globals.EscapeQuote(strID)
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                strResult = dt.Rows(0)(0).ToString()
            End If
        Catch ex As Exception

        End Try
        Return strResult

    End Function
    '// lấy số bút toán
    Public Shared Function Get_SoBT(ByVal iNgayLV As Integer, ByVal iMaNV As Integer) As Integer
        Dim iResult As Integer = 0
        Try
            Dim strSql As String = "Select MAX(So_BT) AS So_BT From TCS_CTU_HDR Where ngay_kb=" & iNgayLV & " AND Ma_NV=" & iMaNV
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString()) + 1
            End If
            'LogDebug.WriteLog("SoBT: " & iMaNV & "-" & iResult, EventLogEntryType.Information)
            log.Info("SoBT: " & iMaNV & "-" & iResult)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            iResult = 1
        End Try
        Return iResult
    End Function

    Public Shared Function GetNSTT(ByVal pv_strNgay_KB As String, ByVal pv_strMa_NV As String, ByVal pv_strSo_CT As String, ByVal pv_strSo_BT As String, ByVal pv_strMa_DThu As String)
        Try
            Dim strSql As String = "Select TT_TTHU From TCS_CTU_HDR Where NGAY_KB=" & pv_strNgay_KB & " AND Ma_NV=" & pv_strMa_NV & " AND So_CT='" & pv_strSo_CT & "'"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return IIf(CDbl(dt.Rows(0)(0).ToString()) = 0, "N", "Y")
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
            Return "N"
        End Try
    End Function

    '/// lấy XaID
    Public Shared Function Get_XaID(ByVal strMaXa As String) As Integer
        Dim iResult As Integer = 0
        Try
            Dim strSql As String = "Select XA_ID From TCS_DM_XA Where Ma_Xa =" & Globals.EscapeQuote(strMaXa)
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString())
            End If
        Catch ex As Exception
            iResult = 1
        End Try
        Return iResult
    End Function

    'Public Shared Function GetCTuID(ByVal headerCTu As CTuHeader) As Integer
    '    Dim iResult As Integer = 0
    '    Try
    '        Dim strSql As String = "Select ID From TCS_CTU_DLT Where SHKB='" & headerCTu.SHKB & "' AND NGAY_KB=" & headerCTu.Ngay_KB & " AND MA_NV=" & headerCTu.Ma_NV & " AND SO_BT=" & headerCTu.So_BT & " AND MA_DTHU='" & headerCTu.Ma_DThu & "'"
    '        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
    '        If Not Globals.IsNullOrEmpty(dt) Then
    '            iResult = CInt(dt.Rows(0)(0).ToString())
    '        End If
    '    Catch ex As Exception
    '        iResult = 1
    '    End Try
    '    Return iResult
    'End Function

    '// lấy bàn làm việc
    Public Shared Function Get_BanLV(ByVal iMaNV As Integer) As String
        Dim iResult As String = ""
        Try
            Dim strSQL As String = "SELECT a.ban_lv " _
                                   & " FROM tcs_dm_nhanvien a " _
                                   & " WHERE a.ma_nv=" & iMaNV
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = dt.Rows(0)(0).ToString()
            End If
        Catch ex As Exception
            iResult = 1
        End Try
        Return iResult
    End Function

    '/// lấy số chứng từ tiếp theo
    Public Shared Function Get_SoCT() As String
        Dim strResult As String
        Try
            strResult = GetData_Seq("TCS_CTU_ID_SEQ.NEXTVAL")
        Catch ex As Exception
            Throw ex
        End Try

        'Kienvt : So chung tu them ky tu B vao dau de phan biet BIDV
        Return strResult.PadLeft(5, "0")
    End Function
    Public Shared Function Get_SoRelation_No() As String
        Dim strResult As String
        Try
            strResult = GetData_Seq("tcs_relation_no_seq.NEXTVAL")
        Catch ex As Exception
            Throw ex
        End Try

        'Kienvt : So chung tu them ky tu B vao dau de phan biet BIDV
        Return strResult
    End Function
    Public Shared Function Get_SoBL() As String
        Dim strResult As String = ""
        Try
            strResult = GetData_Seq("TCS_BLANH_ID_SEQ.NEXTVAL")
        Catch ex As Exception
        End Try

        'Kienvt : So chung tu them ky tu B vao dau de phan biet BIDV
        Return strResult.PadLeft(5, "0")
    End Function
    '-----------------------------------------------------------------------------------------------
    ' Mục đích: Lấy giá trị của Sequence
    ' Đầu vào : strSeqname + .NEXTVAL hoặc .CURRVAL - Tên seq cần lấy
    ' Người viết: Hoàng Văn Anh
    ' Ngày viết: 11/02/2009
    '-----------------------------------------------------------------------------------------------
    Public Shared Function GetData_Seq(ByVal strSeqName As String) As String
        Dim strDataKey As String = "1"
        Dim strSQL As String = ""
        Try
            strSeqName = strSeqName.Trim()
            strSQL = "SELECT " & strSeqName & " FROM DUAL"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                strDataKey = dt.Rows(0)(0).ToString()
            Else
                strDataKey = "1"
            End If
        Catch ex As Exception
            strDataKey = "1"
            'LogDebug.Writelog("Lỗi trong quá trình sinh ID: " & ex.ToString)
        Finally
        End Try

        Return strDataKey
    End Function

    ''/// Lấy Tk tiền mặt của NH
    'Public Shared Function Get_TKTM_NH(ByVal strNH_TKTM As String) As String
    '    Return Get_ParmValues("NH_TKTM", strNH_TKTM)
    'End Function
    ''// lấy Tk chuyển khoản của NH
    'Public Shared Function Get_TKCK_NH(ByVal strNH_TKTH_KB As String) As String
    '    Return Get_ParmValues("NH_TKTH_KB", strNH_TKTH_KB)
    'End Function

    'ICB-16/10/2008: Load tài khoản ngân hàng A,B
    'Hoàng Văn Anh
    Public Shared Function MapTK_Load(ByVal strMa_DBHC As String, ByVal strSHKB As String, ByVal strDBThu As String, ByVal strMaNH As String) As ArrayList
        Dim strSQL As String
        Dim arr As New ArrayList
        Dim strWhere As String
        Try
            strWhere += " Where a.dbhc = '" & strMa_DBHC & "' And a.Ma_Kb='" & strSHKB & "'" 'And a.Ma_NH ='" & strMaNH & "'"
            strSQL = "SELECT  a.ma_nh as NH_A, a.ma_kb as NH_B, a.tk_kb as TK_NO,a.tk_gl_nh,a.tk_kb_nh " _
                & " FROM tcs_map_tk_nh_kb a " & strWhere
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                arr.Add(dt.Rows(0)("NH_A").ToString()) 'NH
                arr.Add(dt.Rows(0)("NH_B").ToString()) 'KB
                arr.Add(dt.Rows(0)("TK_NO").ToString()) 'Ma tk KB
                arr.Add(dt.Rows(0)("tk_gl_nh").ToString()) 'Ma tk KB
                arr.Add(dt.Rows(0)("tk_kb_nh").ToString()) 'Ma tk KB
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return arr
    End Function

    Public Shared Function ToValue(ByVal value As String, ByVal typefield As String _
                                , Optional ByVal strFormat As String = "") As String
        Dim tmp As String = ""
        'convert to from Unicode to TCVN before insert DB
        If (IsDBNull(value)) Then
            value = "' '"
            Return value
        End If
        value = value.Trim
        If UCase(Trim(typefield)) = "NUMBER" Then
            If value <> "" Then
                tmp = value.Replace(",", ".")
                'Neu dau vao cho Value la Nothing thi ham tra ve chuoi "null"
            Else
                tmp = "null"
            End If
        ElseIf UCase(Trim(typefield)) = "DATE" Then
            If value <> "" Then
                If strFormat = "" Then
                    strFormat = "dd/MM/rrrr HH24:MI:ss"
                End If
                tmp = "to_date('" & value & "','" & strFormat & "')"
                'Neu dau vao cho Value la Nothing thi ham tra ve chuoi "null"
            Else
                tmp = "null"
            End If
        End If

        Return tmp
    End Function

    ''// Inser chứng từ với nhiều Detail
    'Public Shared Function Insert_CT(ByVal headerCTU As CTuHeader, ByVal detailCTu As CTuDetail()) As Boolean
    '    Dim bResult As Boolean = True
    '    Dim strSQL As String = ""
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction
    '        strSQL = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV," & _
    '         "So_BT,Ma_DThu,So_BThu," & _
    '         "KyHieu_CT,So_CT_NH," & _
    '         "Ma_NNTien,Ten_NNTien,DC_NNTien," & _
    '         "Ma_NNThue,Ten_NNThue,DC_NNThue," & _
    '         "Ly_Do,Ma_KS,Ma_TQ,So_QD," & _
    '         "Ngay_QD,CQ_QD,Ngay_CT," & _
    '         "Ma_CQThu,XA_ID,Ma_Tinh," & _
    '         "Ma_Huyen,Ma_Xa,TK_No,TK_Co," & _
    '         "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
    '         "Ma_NT,Ty_Gia,TG_ID, Ma_LThue," & _
    '         "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH," & _
    '         "MA_NH_A,MA_NH_B,TTien,TT_TThu," & _
    '         "Lan_In,Trang_Thai, " & _
    '         "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT,So_BK, Ngay_BK,CTU_GNT_BL,So_CT) " & _
    '         "Values('" & headerCTU.SHKB & "'," & headerCTU.Ngay_KB & "," & headerCTU.Ma_NV & "," & _
    '         headerCTU.So_BT & ",'" & headerCTU.Ma_DThu & "'," & headerCTU.So_BanThu & ",'" & _
    '         headerCTU.KyHieu_CT & "','" & headerCTU.So_CTNH & "','" & _
    '         headerCTU.Ma_NNTien & "','" & headerCTU.Ten_NNTien & "','" & headerCTU.DC_NNTien & "','" & _
    '         headerCTU.Ma_NNThue & "','" & headerCTU.Ten_NNThue & "','" & headerCTU.DC_NNThue & "','" & _
    '         headerCTU.LyDo & "'," & headerCTU.Ma_KS & "," & headerCTU.Ma_TQ & ",'" & headerCTU.SoQD & "',null,'" & _
    '         headerCTU.CoQuanQD & "'," & headerCTU.NgayCT & _
    '         ",'" & headerCTU.MaCQThu & "'," & _
    '         headerCTU.Xa_ID & ",'" & headerCTU.MaTinh & "','" & headerCTU.MaHuyen & "','" & _
    '         headerCTU.MaXa & "','" & headerCTU.TK_NO & "','" & headerCTU.TK_CO & "','" & _
    '         headerCTU.SoToKhai & "'," & headerCTU.NgayToKhai & ",'" & headerCTU.LH_XNK & "','','','" & headerCTU.Ma_NguyenTe & "'," & _
    '         headerCTU.TyGia & "," & headerCTU.TyGiaID & ",'" & headerCTU.Ma_LThue & "','" & _
    '         headerCTU.SoKhung & "','" & headerCTU.SoMay & "','" & headerCTU.TK_KH_NH & "'," & _
    '         headerCTU.Ngay_KH_NH & ",'" & headerCTU.MaNH_A & "','" & headerCTU.MaNH_B & "'," & _
    '         headerCTU.TongTien & "," & headerCTU.TongTien_ThucThu & "," & headerCTU.LanIn & ",'" & _
    '         headerCTU.TrangThai & "','" & headerCTU.TK_KH_Nhan & "','" & headerCTU.Ten_KH_Nhan & "','" & _
    '         headerCTU.DC_KH_Nhan & "'," & headerCTU.TongTien_NT & ",'" & headerCTU.PT_ThanhToan & "'," & Globals.EscapeQuote(headerCTU.So_BK) & "," & _
    '         headerCTU.Ngay_BK & "," & Globals.EscapeQuote("00") & "," & Globals.EscapeQuote(headerCTU.So_CT) & ")"

    '        'DatabaseHelp.Execute(strSQL)
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        Dim ctuDetail As CTuDetail
    '        For Each ctuDetail In detailCTu
    '            strSQL = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu," & _
    '                "Ma_Chuong,Ma_Khoan,Ma_TMuc,Noi_Dung," & _
    '                "Ky_Thue,SoTien,SoTien_NT,maquy,ma_dp) " & _
    '                "Values (" & ctuDetail.ID & ",'" & headerCTU.SHKB & "'," & headerCTU.Ngay_KB & "," & headerCTU.Ma_NV & "," & _
    '                headerCTU.So_BT & ",'" & headerCTU.Ma_DThu & "','" & ctuDetail.Ma_Chuong & "','" & ctuDetail.Ma_NganhKT & "','" & _
    '                ctuDetail.ND_NganhKT & "','" & ctuDetail.Noi_Dung & "','" & ctuDetail.Ky_Thue & "'," & _
    '                ctuDetail.Tien & "," & ctuDetail.TienNT & "," & Globals.EscapeQuote(ctuDetail.Ma_Quy) & "," & Globals.EscapeQuote(ctuDetail.MaDP) & ")"
    '            'DatabaseHelp.Execute(strSQL)
    '            conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)
    '        Next
    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '        bResult = False
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Is Nothing Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Is Nothing Then
    '            conn.Dispose()
    '        End If
    '    End Try
    '    Return bResult
    'End Function

    ''// Inser chứng từ với 1 Detail
    'Public Shared Function Insert_CT(ByVal headerCTU As CTuHeader, ByVal ctuDetail As CTuDetail) As Boolean
    '    Dim bResult As Boolean = True
    '    Dim strSQL As String = ""
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction

    '        strSQL = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV," & _
    '         "So_BT,Ma_DThu,So_BThu," & _
    '         "KyHieu_CT,So_CT_NH," & _
    '         "Ma_NNTien,Ten_NNTien,DC_NNTien," & _
    '         "Ma_NNThue,Ten_NNThue,DC_NNThue," & _
    '         "Ly_Do,Ma_KS,Ma_TQ,So_QD," & _
    '         "Ngay_QD,CQ_QD,Ngay_CT," & _
    '         "Ma_CQThu,XA_ID,Ma_Tinh," & _
    '         "Ma_Huyen,Ma_Xa,TK_No,TK_Co," & _
    '         "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
    '         "Ma_NT,Ty_Gia,TG_ID, Ma_LThue," & _
    '         "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH," & _
    '         "MA_NH_A,MA_NH_B,TTien,TT_TThu," & _
    '         "Lan_In,Trang_Thai, " & _
    '         "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT,PT_TT,So_BK, Ngay_BK,CTU_GNT_BL,So_CT) " & _
    '         "Values('" & headerCTU.SHKB & "'," & headerCTU.Ngay_KB & "," & headerCTU.Ma_NV & "," & _
    '         headerCTU.So_BT & ",'" & headerCTU.Ma_DThu & "'," & headerCTU.So_BanThu & ",'" & _
    '         headerCTU.KyHieu_CT & "','" & headerCTU.So_CTNH & "','" & _
    '         headerCTU.Ma_NNTien & "','" & headerCTU.Ten_NNTien & "','" & headerCTU.DC_NNTien & "','" & _
    '         headerCTU.Ma_NNThue & "','" & headerCTU.Ten_NNThue & "','" & headerCTU.DC_NNThue & "','" & _
    '         headerCTU.LyDo & "'," & headerCTU.Ma_KS & "," & headerCTU.Ma_TQ & ",'" & headerCTU.SoQD & "',null,'" & _
    '         headerCTU.CoQuanQD & "'," & headerCTU.NgayCT & _
    '         ",'" & headerCTU.MaCQThu & "'," & _
    '         headerCTU.Xa_ID & ",'" & headerCTU.MaTinh & "','" & headerCTU.MaHuyen & "','" & _
    '         headerCTU.MaXa & "','" & headerCTU.TK_NO & "','" & headerCTU.TK_CO & "','" & _
    '         headerCTU.SoToKhai & "'," & headerCTU.NgayToKhai & ",'" & headerCTU.LH_XNK & "','','','" & headerCTU.Ma_NguyenTe & "'," & _
    '         headerCTU.TyGia & "," & headerCTU.TyGiaID & ",'" & headerCTU.Ma_LThue & "','" & _
    '         headerCTU.SoKhung & "','" & headerCTU.SoMay & "','" & headerCTU.TK_KH_NH & "'," & _
    '         headerCTU.Ngay_KH_NH & ",'" & headerCTU.MaNH_A & "','" & headerCTU.MaNH_B & "'," & _
    '         headerCTU.TongTien & "," & headerCTU.TongTien_ThucThu & "," & headerCTU.LanIn & ",'" & _
    '         headerCTU.TrangThai & "','" & headerCTU.TK_KH_Nhan & "','" & headerCTU.Ten_KH_Nhan & "','" & _
    '         headerCTU.DC_KH_Nhan & "'," & headerCTU.TongTien_NT & ",'" & headerCTU.PT_ThanhToan & "'," & Globals.EscapeQuote(headerCTU.So_BK) & "," & _
    '         headerCTU.Ngay_BK & "," & Globals.EscapeQuote("00") & "," & Globals.EscapeQuote(headerCTU.So_CT) & ")"

    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        strSQL = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu," & _
    '             "Ma_Chuong,Ma_Khoan,Ma_TMuc,Noi_Dung," & _
    '             "Ky_Thue,SoTien,SoTien_NT,maquy,ma_dp) " & _
    '             "Values (" & ctuDetail.ID & ",'" & headerCTU.SHKB & "'," & headerCTU.Ngay_KB & "," & headerCTU.Ma_NV & "," & _
    '             headerCTU.So_BT & ",'" & headerCTU.Ma_DThu & "','" & ctuDetail.Ma_Chuong & "','" & ctuDetail.Ma_NganhKT & "','" & _
    '             ctuDetail.ND_NganhKT & "','" & ctuDetail.Noi_Dung & "','" & ctuDetail.Ky_Thue & "'," & _
    '             ctuDetail.Tien & "," & ctuDetail.TienNT & "," & Globals.EscapeQuote(ctuDetail.Ma_Quy) & "," & Globals.EscapeQuote(ctuDetail.MaDP) & ")"

    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)
    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '        bResult = False
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Is Nothing Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Is Nothing Then
    '            conn.Dispose()
    '        End If
    '    End Try
    '    Return bResult
    'End Function

    'Public Shared Sub Update_CT(ByVal hdr As CTuHeader, ByVal v_arrDetailCTu() As CTuDetail, Optional ByVal blnXoaThucThu As Boolean = True)
    '    '----------------------------------------------------------------------
    '    ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
    '    ' Đầu vào: đối tượng HDR và mảng DTL
    '    ' Người viết: Lê Hồng Hà
    '    ' Ngày viết: 09/11/2007
    '    '----------------------------------------------------------------------
    '    Dim strSql As String
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction
    '        'Cập nhật thông tin HDR của chứng từ
    '        strSql = "Update TCS_CTU_HDR SET " & _
    '                 "kyhieu_ct = '" & hdr.KyHieu_CT & "', " & _
    '                 "So_CT= '" & hdr.So_CT & "', So_BThu=" & hdr.So_BanThu & ",So_CT_NH='" & hdr.So_CTNH & "'," & _
    '                 "Ma_NNTien='" & hdr.Ma_NNTien & "',Ten_NNTien='" & hdr.Ten_NNTien & "'," & _
    '                 "DC_NNTien='" & hdr.DC_NNTien & "',Ma_NNThue='" & hdr.Ma_NNThue & "'," & _
    '                 "Ten_NNThue='" & hdr.Ten_NNThue & "',DC_NNThue='" & hdr.DC_NNThue & "'," & _
    '                 "Ma_KS=0,So_QD='" & hdr.SoQD & "'," & _
    '                 "Ngay_QD='" & hdr.NgayQD & "',CQ_QD='" & hdr.CoQuanQD & "',Ngay_CT=" & hdr.NgayCT & "," & _
    '                 "Ngay_HT='" & hdr.NgayHT & "',Ma_CQThu='" & hdr.MaCQThu & "'," & _
    '                 "XA_ID=" & hdr.Xa_ID & ",Ma_Tinh='" & hdr.MaTinh & "',Ma_Huyen='" & hdr.MaHuyen & "'," & _
    '                 "Ma_Xa='" & hdr.MaXa & "',TK_No='" & hdr.TK_NO & "',TK_Co='" & hdr.TK_CO & "'," & _
    '                 "So_TK='" & hdr.SoToKhai & "',Ngay_TK=" & hdr.NgayToKhai & ",LH_XNK='" & hdr.LH_XNK & "'," & _
    '                 "Ma_NT='" & hdr.Ma_NguyenTe & "',Ty_Gia=" & hdr.TyGia & "," & _
    '                 "TG_ID = " & hdr.TyGiaID & ",MA_LTHUE = '" & hdr.Ma_LThue & "'," & _
    '                 "So_Khung='" & hdr.SoKhung & "',So_May='" & hdr.SoMay & "'," & _
    '                 "TK_KH_NH='" & hdr.TK_KH_NH & "',NGAY_KH_NH=" & hdr.Ngay_KH_NH & "," & _
    '                 "MA_NH_A='" & hdr.MaNH_A & "',MA_NH_B='" & hdr.MaNH_B & "'," & _
    '                 "TTien=" & hdr.TongTien & ", Lan_In=0, Trang_Thai='" & hdr.TrangThai & "', " & _
    '                 "TK_KH_Nhan='" & hdr.TK_KH_Nhan & "', Ten_KH_Nhan='" & hdr.Ten_KH_Nhan & "'," & _
    '                 "Diachi_KH_Nhan='" & hdr.DC_KH_Nhan & "', TTien_NT=" & hdr.TongTien_NT & ", " & _
    '                 "So_BK = '" & hdr.So_BK & "', Ngay_BK = " & hdr.Ngay_BK & " " & _
    '                 " Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
    '                 ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
    '                 ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"
    '        ' Insert vào bảng TCS_CT_HDR
    '        'DatabaseHelp.Execute(strSql)
    '        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

    '        'Nếu tham số cho phép xóa thực thu thì mới xóa
    '        'Xử dụng khi cập nhật chứng từ mà không thay đổi tổng tiền
    '        If (blnXoaThucThu) Then
    '            'Cập nhật thông tin số thực thu của chứng từ
    '            strSql = "Update TCS_CTU_HDR SET " & _
    '                     "Ma_TQ=" & hdr.Ma_TQ & ", TT_TThu=" & hdr.TongTien_ThucThu & _
    '                     " Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
    '                     ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
    '                     ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"
    '            ' Insert vào bảng TCS_CT_HDR
    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

    '            'Xóa phần chi tiết về số thực thu
    '            strSql = "delete from tcs_khoquy_dtl " & _
    '                    "Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
    '                    ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
    '                    ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"

    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

    '            'Xóa phần Header thực thu
    '            strSql = "delete from tcs_khoquy_hdr hdr " & _
    '                    "Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
    '                    ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
    '                    ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"

    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        End If

    '        'Xóa chứng từ chi tiết - để cập nhật lại
    '        strSql = "delete from TCS_CTU_DTL " & _
    '                "where SHKB='" & hdr.SHKB & "' and Ngay_KB=" & hdr.Ngay_KB & " and " & _
    '                "Ma_NV=" & hdr.Ma_NV & "  and So_BT=" & hdr.So_BT & "  and " & _
    '                "Ma_DThu='" & hdr.Ma_DThu & "'"
    '        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

    '        ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
    '        For Each detailCTu As CTuDetail In v_arrDetailCTu
    '            strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu," & _
    '                 "Ma_Chuong,Ma_Khoan,Ma_TMuc,Noi_Dung," & _
    '                 "Ky_Thue,SoTien,SoTien_NT,maquy,ma_dp) " & _
    '                 "Values (" & detailCTu.ID & ",'" & hdr.SHKB & "'," & hdr.Ngay_KB & "," & hdr.Ma_NV & "," & _
    '                 hdr.So_BT & ",'" & hdr.Ma_DThu & "','" & detailCTu.Ma_Chuong & "','" & detailCTu.Ma_NganhKT & "','" & _
    '                 detailCTu.ND_NganhKT & "','" & detailCTu.Noi_Dung & "','" & detailCTu.Ky_Thue & "'," & _
    '                 detailCTu.Tien & "," & detailCTu.TienNT & "," & Globals.EscapeQuote(detailCTu.Ma_Quy) & "," & Globals.EscapeQuote(detailCTu.MaDP) & ")"
    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        Next
    '        ' Cập nhật dữ liệu vào CSDL

    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Is Nothing Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Is Nothing Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    ''Update thong tin chung tu
    'Public Shared Sub Update_CT(ByVal hdr As CTuHeader, ByVal detailCTu As CTuDetail, Optional ByVal blnXoaThucThu As Boolean = True)
    '    '----------------------------------------------------------------------
    '    ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
    '    ' Đầu vào: đối tượng HDR và mảng DTL
    '    ' Người viết: Lê Hồng Hà
    '    ' Ngày viết: 09/11/2007
    '    '----------------------------------------------------------------------
    '    Dim strSql As String
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction
    '        'Cập nhật thông tin HDR của chứng từ
    '        strSql = "Update TCS_CTU_HDR SET " & _
    '                 "kyhieu_ct = '" & hdr.KyHieu_CT & "', " & _
    '                 "So_CT= '" & hdr.So_CT & "', So_BThu=" & hdr.So_BanThu & ",So_CT_NH='" & hdr.So_CTNH & "'," & _
    '                 "Ma_NNTien='" & hdr.Ma_NNTien & "',Ten_NNTien='" & hdr.Ten_NNTien & "'," & _
    '                 "DC_NNTien='" & hdr.DC_NNTien & "',Ma_NNThue='" & hdr.Ma_NNThue & "'," & _
    '                 "Ten_NNThue='" & hdr.Ten_NNThue & "',DC_NNThue='" & hdr.DC_NNThue & "'," & _
    '                 "Ma_KS=0,So_QD='" & hdr.SoQD & "'," & _
    '                 "Ngay_QD='" & hdr.NgayQD & "',CQ_QD='" & hdr.CoQuanQD & "',Ngay_CT=" & hdr.NgayCT & "," & _
    '                 "Ngay_HT='" & hdr.NgayHT & "',Ma_CQThu='" & hdr.MaCQThu & "'," & _
    '                 "XA_ID=" & hdr.Xa_ID & ",Ma_Tinh='" & hdr.MaTinh & "',Ma_Huyen='" & hdr.MaHuyen & "'," & _
    '                 "Ma_Xa='" & hdr.MaXa & "',TK_No='" & hdr.TK_NO & "',TK_Co='" & hdr.TK_CO & "'," & _
    '                 "So_TK='" & hdr.SoToKhai & "',Ngay_TK=" & hdr.NgayToKhai & ",LH_XNK='" & hdr.LH_XNK & "'," & _
    '                 "Ma_NT='" & hdr.Ma_NguyenTe & "',Ty_Gia=" & hdr.TyGia & "," & _
    '                 "TG_ID = " & hdr.TyGiaID & ",MA_LTHUE = '" & hdr.Ma_LThue & "'," & _
    '                 "So_Khung='" & hdr.SoKhung & "',So_May='" & hdr.SoMay & "'," & _
    '                 "TK_KH_NH='" & hdr.TK_KH_NH & "',NGAY_KH_NH=" & hdr.Ngay_KH_NH & "," & _
    '                 "MA_NH_A='" & hdr.MaNH_A & "',MA_NH_B='" & hdr.MaNH_B & "'," & _
    '                 "TTien=" & hdr.TongTien & ", Lan_In=0, Trang_Thai='" & hdr.TrangThai & "', " & _
    '                 "TK_KH_Nhan='" & hdr.TK_KH_Nhan & "', Ten_KH_Nhan='" & hdr.Ten_KH_Nhan & "'," & _
    '                 "Diachi_KH_Nhan='" & hdr.DC_KH_Nhan & "', TTien_NT=" & hdr.TongTien_NT & ", " & _
    '                 "So_BK = '" & hdr.So_BK & "', Ngay_BK = " & hdr.Ngay_BK & " " & _
    '                 " Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
    '                 ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
    '                 ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"
    '        ' Insert vào bảng TCS_CT_HDR
    '        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

    '        'Nếu tham số cho phép xóa thực thu thì mới xóa
    '        'Xử dụng khi cập nhật chứng từ mà không thay đổi tổng tiền
    '        If (blnXoaThucThu) Then
    '            'Cập nhật thông tin số thực thu của chứng từ
    '            strSql = "Update TCS_CTU_HDR SET " & _
    '                     "Ma_TQ=" & hdr.Ma_TQ & ", TT_TThu=" & hdr.TongTien_ThucThu & _
    '                     " Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
    '                     ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
    '                     ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"
    '            ' Insert vào bảng TCS_CT_HDR
    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

    '            'Xóa phần chi tiết về số thực thu
    '            strSql = "delete from tcs_khoquy_dtl " & _
    '                    "Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
    '                    ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
    '                    ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"

    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

    '            'Xóa phần Header thực thu
    '            strSql = "delete from tcs_khoquy_hdr hdr " & _
    '                    "Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
    '                    ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
    '                    ") And (Ma_DThu = '" & hdr.Ma_DThu & "')"

    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        End If

    '        'Xóa chứng từ chi tiết - để cập nhật lại
    '        strSql = "delete from TCS_CTU_DTL " & _
    '                "where SHKB='" & hdr.SHKB & "' and Ngay_KB=" & hdr.Ngay_KB & " and " & _
    '                "Ma_NV=" & hdr.Ma_NV & "  and So_BT=" & hdr.So_BT & "  and " & _
    '                "Ma_DThu='" & hdr.Ma_DThu & "'"
    '        'DatabaseHelp.Execute(strSql)
    '        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

    '        ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
    '        strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu," & _
    '             "Ma_Chuong,Ma_Khoan,Ma_TMuc,Noi_Dung," & _
    '             "Ky_Thue,SoTien,SoTien_NT,maquy,ma_dp) " & _
    '             "Values (" & detailCTu.ID & ",'" & hdr.SHKB & "'," & hdr.Ngay_KB & "," & hdr.Ma_NV & "," & _
    '             hdr.So_BT & ",'" & hdr.Ma_DThu & "','" & detailCTu.Ma_Chuong & "','" & detailCTu.Ma_NganhKT & "','" & _
    '             detailCTu.ND_NganhKT & "','" & detailCTu.Noi_Dung & "','" & detailCTu.Ky_Thue & "'," & _
    '             detailCTu.Tien & "," & detailCTu.TienNT & "," & Globals.EscapeQuote(detailCTu.Ma_Quy) & "," & Globals.EscapeQuote(detailCTu.MaDP) & ")"
    '        'DatabaseHelp.Execute(strSql)
    '        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        ' Cập nhật dữ liệu vào CSDL
    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Is Nothing Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Is Nothing Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    '// Cap nhat trang thai cua chung tu
    'Public Shared Function Update_TrangThaiCTU(ByVal keyCTu As CTuKey, ByVal strTrangThai As String, Optional ByVal strREFNo As String = "") As Integer
    '    Dim iResult As Integer = 0
    '    Try
    '        Dim strSQL As String = "UPDATE tcs_ctu_hdr a SET a.trang_thai=" & Globals.EscapeQuote(strTrangThai) _
    '                & " , REF_NO='" & strREFNo & "' WHERE a.shkb=" & Globals.EscapeQuote(keyCTu.SHKB) & " AND a.ngay_kb=" & keyCTu.NgayKB _
    '                & "  AND a.ma_nv=" & keyCTu.MaNV & " AND a.so_bt=" & keyCTu.SoBT & " AND a.ma_dthu=" & Globals.EscapeQuote(keyCTu.MaDT)
    '        iResult = DatabaseHelp.Execute(strSQL)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    '    Return iResult
    'End Function

    'Public Shared Function Get_TongTienCT(ByVal keyCTu As CTuKey) As Decimal
    '    Dim d As Decimal = 0
    '    Try
    '        Dim strSQL As String = "SELECT  ttien, tt_tthu  FROM tcs_ctu_hdr " & _
    '          " WHERE shkb=" & Globals.EscapeQuote(keyCTu.SHKB) & _
    '          " AND ngay_kb=" & keyCTu.NgayKB & _
    '          " AND ma_nv=" & keyCTu.MaNV & _
    '          " AND so_bt=" & keyCTu.SoBT & _
    '          " AND ma_dthu=" & Globals.EscapeQuote(keyCTu.MaDT)
    '        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
    '        If Not Globals.IsNullOrEmpty(dt) Then
    '            d = Decimal.Parse(dt.Rows(0)(0))
    '        End If
    '    Catch ex As Exception

    '    End Try

    '    Return d
    'End Function

    'Public Shared Function Valid_ToKhai(ByVal strToKhai As String) As Boolean
    '    '-------------------------------------------
    '    ' Mục đích: Tính hợp lệ của tờ khai
    '    ' Người viết: Lê Hồng Hà
    '    ' Ngày viết: 06/08/2008
    '    '-------------------------------------------
    '    Try
    '        If (strToKhai.Trim() = "") Then
    '            Return True
    '        End If

    '        Dim c As String
    '        c = strToKhai.Replace(",", "")
    '        c = c.Replace(" ", "")
    '        If Not IsNumeric(c) Then
    '            Return False
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Return False
    '    End Try
    'End Function

    Public Shared Function Get_LHXNK(ByVal strMaCQThu As String, ByVal strSoTK As String, ByVal strNgayTK As String) As String
        '-------------------------------------------------------------------------------------
        ' Mục đích: Lấy loại hình xuất nhập khẩu 
        ' Người viết: Lê Hồng Hà
        ' Ngày viết: 21/09/2008
        '--------------------------------------------------------------------------------------

        Dim conn As DataAccess
        Dim dr As IDataReader

        Try
            If (strMaCQThu.Trim() = "") Or (strSoTK.Trim() = "") Or (strNgayTK.Trim() = "") Then
                Return ""
            End If

            conn = New DataAccess
            Dim strSQL As String = "select lh_xnk from tcs_ds_tokhai " & _
                                   "where ma_cqthu = '" & strMaCQThu & _
                                   "' and so_tk = '" & strSoTK & _
                                   "' and ngay_tk = " & ToValue(strNgayTK, "DATE", "dd/MM/yyyy")
            dr = conn.ExecuteDataReader(strSQL, CommandType.Text)

            Dim strReturn As String = ""

            If (dr.Read()) Then
                strReturn = Common.mdlCommon.GetString(dr(0)).Trim()
            End If

            Return strReturn
        Catch ex As Exception
            Throw ex
        Finally
            If (Not IsNothing(dr)) Then dr.Close()
            If (Not IsNothing(conn)) Then conn.Dispose()
        End Try
    End Function

    Public Shared Sub Get_LHXNK_byTenVT(ByVal Ten_VT As String, ByRef Ma_LH As String, ByRef Ten_LH As String)
        '---------------------------------------------------------------
        ' Mục đích: Lấy ra tên LH và Ten_VT của 1 mã loại hình bất kỳ.
        ' Tham số: Ma_LH: mã loại hình cần lấy tên
        ' Ngày viết: 14/10/2008
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------
        Dim cnTenCQ As DataAccess
        Dim drTenCQ As IDataReader
        Dim strSql As String
        Try
            cnTenCQ = New DataAccess
            strSql = "Select Ma_LH, TEN_LH From TCS_DM_LHINH Where TEN_VT='" & Ten_VT.ToUpper() & "' order by Ma_LH"
            drTenCQ = cnTenCQ.ExecuteDataReader(strSql, CommandType.Text)
            If drTenCQ.Read Then
                Ma_LH = Trim(drTenCQ.GetString(0))
                Ten_LH = Trim(drTenCQ.GetString(1))
            End If
        Catch ex As Exception
            Throw ex
            'LogDebug.Writelog("Lỗi trong quá trình lấy tên Loại hình xuất nhập khẩu: " & ex.ToString)
        Finally
            If Not drTenCQ Is Nothing Then
                drTenCQ.Close()
            End If
            If Not cnTenCQ Is Nothing Then
                cnTenCQ.Dispose()
            End If
        End Try
    End Sub


    Public Shared Function GetKyThue(ByVal strNgayLV As String) As String
        '------------------------------------------------------------------
        ' Mục đích: Đưa từ dạng ngày yyyyMMdd -> dạng 'MM/yyyy'
        ' Tham số: 
        ' Giá trị trả về: 
        ' Ngày viết: 07/11/2007
        ' Người viết: Lê Hồng Hà
        ' -----------------------------------------------------------------
        Try
            If (strNgayLV.Length <> 8) Then Return "" 'Không đúng định dạng đầu vào

            Dim strThang, strNam As String
            strNam = strNgayLV.Substring(0, 4)
            strThang = strNgayLV.Substring(4, 2)

            Return (strThang & "/" & strNam)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function Get_TenTieuMuc(ByVal MaTMuc As String) As String
        '---------------------------------------------------------------
        ' Mục đích: Lấy ra tên mục tiểu muc của TIỂU MỤC .
        ' Ngày viết: 21/11/2008
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------
        Dim cnTenMTM As New DataAccess
        Dim drTenMTM As IDataReader
        Dim strSql As String
        Dim strResult As String = ""
        Try
            strSql = "Select TEN From TCS_DM_MUC_TMUC Where MA_TMUC='" & MaTMuc & "' and tinh_trang = '1'"

            drTenMTM = cnTenMTM.ExecuteDataReader(strSql, CommandType.Text)
            If drTenMTM.Read Then
                strResult = Trim(drTenMTM.GetString(0))
            End If
        Catch ex As Exception
            'LogDebug.Writelog("Lỗi trong quá trình lấy ra tên tiểu muc" & ex.ToString)
        Finally
            If Not drTenMTM Is Nothing Then drTenMTM.Close()
            If Not cnTenMTM Is Nothing Then cnTenMTM.Dispose()
        End Try
        Return strResult
    End Function


    Public Shared Function CQThu_TK(ByVal strTK As String) As String
        '-------------------------------------------------------------------------------------
        ' Mục đích: Lấy mã cơ quan thu tương ứng với mã tài khoản
        ' Người viết: Lê Hồng Hà
        ' Ngày viết: 15/05/2008
        '--------------------------------------------------------------------------------------

        Dim conn As DataAccess
        Dim dr As IDataReader

        Try
            conn = New DataAccess
            Dim strSQL As String = "select ma_cqthu from tcs_dm_taikhoan where tk='" & strTK.Replace(".", "") & "'"

            dr = conn.ExecuteDataReader(strSQL, CommandType.Text)

            Dim strReturn As String = ""

            If (dr.Read()) Then
                strReturn = dr(0).ToString.Trim()
            End If

            Return strReturn
        Catch ex As Exception
            Throw ex
        Finally
            If (Not IsNothing(dr)) Then dr.Close()
            If (Not IsNothing(conn)) Then conn.Dispose()
        End Try
    End Function
    'Public Shared Function Get_DetailCTU_BL(ByVal keyCT As CTuKey) As DataTable
    '    Dim dt As New DataTable
    '    Try
    '        Dim strSQL As String = "select ID,CCH_ID,MaQuy,Ma_Chuong,LKH_ID,Ma_Khoan,MTM_ID,Ma_TMuc,Noi_Dung," & _
    '           " Ky_Thue,SoTien TTien,SoTien_NT, Ma_DP from TCS_CTU_DTL " & _
    '           " where (shkb = " & Globals.EscapeQuote(keyCT.SHKB) & ") and (ngay_kb = " & keyCT.NgayKB & ") " & _
    '           " and (ma_nv = " & keyCT.MaNV & ") and (so_bt = " & keyCT.SoBT & ") and (ma_dthu = " & Globals.EscapeQuote(keyCT.MaDT) & ") "
    '        dt = DatabaseHelp.ExecuteToTable(strSQL)
    '    Catch ex As Exception
    '    End Try
    '    Return dt
    'End Function
    Public Shared Function Get_RefNo(ByVal strSoCT As String, ByVal strSoBT As String) As String
        Dim strSql As String = "Select REF_NO FROM TCS_CTU_HDR WHERE SO_CT='" & strSoCT & "' AND SO_BT='" & strSoBT & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("REF_NO").ToString
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetNgayNghi() As Boolean
        Dim ngaynghi As String = ""
        ngaynghi = Date.Now.Date.ToString("dd/MM/yyyy")
        Dim strSql As String = "Select ngay_nghi FROM TCS_DM_NgayNghi WHERE ngay_nghi=to_date('" & ngaynghi & "','DD/MM/RRRR')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return False
        Else
            Return True
        End If
    End Function

  
   
End Class


