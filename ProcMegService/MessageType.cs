﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcMegService
{
   public class MessageType
    {
        public const  string strMSG01 = "00001";
        public const  string strMSG03 = "00003";
        public const  string strMSG05 = "00005";
        public const  string strMSG07 = "00007";
        public const  string strMSG08 = "00008";
        public const  string strMSG09 = "00009";
        public const  string strMSG10 = "00010";
        public const  string strMSG11 = "00011";
        public const  string strMSG12 = "00012";
        public const  string strMSG13 = "00013";
        public const  string strMSG14 = "00014";
        public const  string strMSG15 = "00015";
        public const  string strMSG16 = "00016";

        public const  string strMSG17 = "00017";
        public const  string strMSG18 = "00018";
        public const  string strMSG19 = "00019";
        public const  string strMSG20 = "00020";
        public const  string strMSG21 = "00021";
        public const  string strMSG22 = "00022";
        public const  string strMSG23 = "00023";
        public const  string strMSG24 = "00024";
        public const  string strMSG25 = "00025";
        public const  string strMSG26 = "00026";
        public const  string strMSG27 = "00027";

        //sonmt
        public const string strMSG28 = "00028";
        public const string strMSG29 = "00029";
        public const string strMSG32 = "00032";
        public const string strMSG33 = "00033";

       //vinhvv
        public const string strMSG05010 = "05010";


       //vinhvv phan moi cho GIP
       //danh mục cơ quan thu
        public const string strMSG009 = "009";
       //danh mục địa bàn hành chính
        public const string strMSG010 = "010";
        //danh mục kho bạc
        public const string strMSG011 = "011";
        //danh mục chương
        public const string strMSG012 = "012";
        //danh mục khoản
        public const string strMSG013 = "013";
        //danh mục khoản
        public const string strMSG014 = "014";
        //danh mục khoản
        public const string strMSG015 = "015";

        //truy vấn sổ thuế
        public const string strMSG005 = "00005";
    }
}
