﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace ProcMegService.MSG.MSG33
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG33
    {
        public MSG33()
        {
        }
         [XmlElement("HEADER")]
         public HEADER Header;
         [XmlElement("BODY")]
         public BODY Body;
         [XmlElement("SECURITY")]
         public SECURITY Security;



         public MSG33 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ProcMegService.MSG.MSG33.MSG33));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            ProcMegService.MSG.MSG33.MSG33 LoadedObjTmp = (ProcMegService.MSG.MSG33.MSG33)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
         public string MSG33toXML(MSG33 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG33));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }
    public class BODY
    {
        public BODY()
        {
        }

        [XmlElement("ROW")]
        public ROW Row;
    }
    public class ROW
    {
        public ROW()
        { }
        public string TYPE { get; set; }
        public string NAME { get; set; }
        [XmlElement("PHANHOI")]
        public PHANHOI Phanhoi;
    }
    public class PHANHOI
    {
        public PHANHOI()
        { }
        public string TRANG_THAI { get; set; }
        public string MOTA_TRANGTHAI { get; set; }

    }

    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }

    }
    public class SECURITY
    {
        public SECURITY()
        {
        }
        [XmlElement("SIGNATURE")]
        public SIGNATURE Signature;
    }
    public class SIGNATURE
    {
        public SIGNATURE()
        { }
        public string SignatureValue { get; set; }
    }
}
