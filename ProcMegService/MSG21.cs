﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace ProcMegService.MSG.MSG21
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG21
    {
        public MSG21()
        {
        }
        [XmlElement("HEADER")]
        public HEADER Header;
        [XmlElement("BODY")]
        public BODY Body;
        [XmlElement("SECURITY")]
        public SECURITY Security;

        public string MSG21toXML(MSG21 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG21));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG21 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ProcMegService.MSG.MSG21.MSG21));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            ProcMegService.MSG.MSG21.MSG21 LoadedObjTmp = (ProcMegService.MSG.MSG21.MSG21)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW Row;
    }
    public class ROW
    {
        public ROW()
        { }
        [XmlElement(Order = 1)]
        public string TYPE { get; set; }
        [XmlElement(Order = 2)]
        public string NAME { get; set; }
        [XmlElement(Order = 3)]
        public CHUNGTU CHUNGTU;

    }

    public class CHUNGTU
    {
        public CHUNGTU()
        {
        }
        [XmlElement(Order = 1)]
        public string MST { get; set; }
        [XmlElement(Order = 2)]
        public string TEN_NNT { get; set; }
        [XmlElement(Order = 3)]
        public string DIACHI_NNT { get; set; }
        [XmlElement(Order = 4)]
        public string MST_NNTHAY { get; set; }
        [XmlElement(Order = 5)]
        public string TEN_NNTHAY { get; set; }
        [XmlElement(Order = 6)]
        public string DIACHI_NNTHAY { get; set; }
        [XmlElement(Order = 7)]
        public string SO_TK_NHKBNN { get; set; }
        [XmlElement(Order = 8)]
        public string SH_KBNN { get; set; }
        [XmlElement(Order = 9)]
        public string CQ_QLY_THU { get; set; }
        [XmlElement(Order = 10)]
        public string SO_CHUNGTU { get; set; }
        [XmlElement(Order = 11)]
        public string NGAY_CHUNGTU { get; set; }
        [XmlElement(Order = 12)]
        public string TINHCHAT_KHOAN_NOP { get; set; }
        [XmlElement(Order = 13)]
        public string CQUAN_THAMQUYEN { get; set; }
        [XmlElement(Order = 14)]
        public string MA_NHTM { get; set; }
        [XmlElement(Order = 15)]
        public string HUYEN { get; set; }
        [XmlElement(Order = 16)]
        public string TINH { get; set; }
        [XmlElement(Order = 17)]
        public string LOAI_TIEN { get; set; }
        [XmlElement(Order = 18)]
        public string MA_HTHUC_NOP { get; set; }
        [XmlElement(Order = 19)]
        public string DIENGIAI_HTHUC_NOP { get; set; }
        [XmlElement(Order = 20)]
        public string LHINH_NNT { get; set; }
        [XmlElement(Order = 21)]
        public string MATHAMCHIEU { get; set; }
        [XmlElement(Order = 22)]
        public string SO_QUYET_DINH { get; set; }
        [XmlElement(Order = 23)]
        public string DIA_CHI_TS { get; set; }
        [XmlElement(Order = 24)]
        public string SO_KHUNG { get; set; }
        [XmlElement(Order = 25)]
        public string SO_MAY { get; set; }
        [XmlElement(Order = 26)]
        public string NGAY_NTIEN { get; set; }
        [XmlElement(Order = 27)]
        public string DAC_DIEM_PTIEN { get; set; }
        [XmlElement(Order = 28)]
        public List<CHUNGTU_CHITIET> CHUNGTU_CHITIET;
    }
    public class CHUNGTU_CHITIET
    {
        public CHUNGTU_CHITIET()
        {
        }
        public string CHUONG { get; set; }
        public string TIEUMUC { get; set; }
        public string THONGTIN_KHOANNOP { get; set; }
        public string SO_TIEN { get; set; }
        public string KY_THUE { get; set; }
    }
    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }

    }
    public class SECURITY
    {
        public SECURITY()
        {
        }
        [XmlElement("SIGNATURE")]
        public SIGNATURE Signature;
    }
    public class SIGNATURE
    {
        public SIGNATURE()
        { }
        public string SignatureValue { get; set; }
    }
}
