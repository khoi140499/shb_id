﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcMegService.DataService;
using VBOracleLib;
using System.Data;
using System.Net;
using System.IO;
using System.Security.Authentication;
using System.Net.Security;
using System.Data.OracleClient;
using Newtonsoft.Json;
using System.Globalization;

namespace ProcMegService
{
    public class ProMsgJson
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region khai bao
        private static String strConnection = ConfigurationManager.AppSettings.Get("CTC_DB_CONN").ToString();
        //private static String strConnection = VBOracleLib.Security.DescryptStr(strConnection1);
        private static String strSender_Code = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
        private static String strSender_Name = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
        private static String gipURL = ConfigurationManager.AppSettings.Get("GIP.URL").ToString();
        private static String strMsg_Id = "";
        #endregion

        public ProMsgJson()
        {

        }

        public static infoTRUYVANTHUE getMSG05(string ID_KHOANNOP, string MST, string LOAI_DL, string SO_QDINH_TBAO, string MA_HS, string SO_GIAYTO)
        {
            try
            {
                string strURL = "";
                if (MST.Trim().Length >= 0)
                {
                    LOAI_DL = "00";  //00: Còn phải nộp - 01: Nộp thừa - 02: Được hoàn 
                }

                infoTRUYVANTHUE infoSoThue = new infoTRUYVANTHUE();

                strMsg_Id = TCS_GetSequenceValue();
                TruyVanSoThueReq request = new TruyVanSoThueReq();
                request.header = new Header(MessageType.strMSG005, strMsg_Id);

                request.data = new DataTruyVanSoThue();
                request.data.ID_KHOANNOP = ID_KHOANNOP;
                request.data.MST = MST;
                request.data.LOAI_DL = LOAI_DL;
                request.data.SO_QDINH_TBAO = SO_QDINH_TBAO;
                request.data.MA_HS = MA_HS;
                request.data.SO_GIAYTO = SO_GIAYTO;
                //ký số ở đây
                request.signature = new DataService.Signature();
                request.signature.value = "";
                request.signature.certificates = "";
                //send TCT ở đây
                string objSoThueJson = JsonUtils.objSoThue_ToJSON(request);
                string result = MessageRequesTCT(strMsg_Id, strURL, objSoThueJson, MessageType.strMSG005);
                if (result.Trim().Length > 0)
                {
                    if (verifySignJson(result))
                    {
                        try
                        {
                            TruyVanSoThueResponse objResponse = JsonConvert.DeserializeObject<TruyVanSoThueResponse>(result);

                            if (objResponse.data != null && objResponse.data.Count > 0)
                            {
                                if (objResponse.data[0].THONGTIN_NNT != null)
                                {
                                    infoSoThue.MST = objResponse.data[0].THONGTIN_NNT.MST;
                                    infoSoThue.TEN_NNT = objResponse.data[0].THONGTIN_NNT.TEN_NNT;
                                    infoSoThue.LOAI_NNT = objResponse.data[0].THONGTIN_NNT.LOAI_NNT;
                                    infoSoThue.SO_GIAYTO = objResponse.data[0].THONGTIN_NNT.SO_GIAYTO;
                                    infoSoThue.LOAI_GIAYTO = objResponse.data[0].THONGTIN_NNT.LOAI_GIAYTO;
                                    infoSoThue.MA_CQT_QLY = objResponse.data[0].THONGTIN_NNT.MA_CQT_QLY;
                                    infoSoThue.DIA_CHI_NNT = objResponse.data[0].THONGTIN_NNT.DIA_CHI_NNT;
                                    infoSoThue.MA_TINH_NNT = objResponse.data[0].THONGTIN_NNT.MA_TINH_NNT;
                                    infoSoThue.MA_HUYEN_NNT = objResponse.data[0].THONGTIN_NNT.MA_HUYEN_NNT;
                                    infoSoThue.MA_XA_NNT = objResponse.data[0].THONGTIN_NNT.MA_XA_NNT;
                                    infoSoThue.TEN_TINH_NNT = objResponse.data[0].THONGTIN_NNT.TEN_TINH_NNT;
                                    infoSoThue.TEN_HUYEN_NNT = objResponse.data[0].THONGTIN_NNT.TEN_HUYEN_NNT;
                                    infoSoThue.TEN_XA_NNT = objResponse.data[0].THONGTIN_NNT.TEN_XA_NNT;

                                    //add nợ thuế vào cùng 1 list
                                    infoSoThue.ROW_SOTHUE = new List<CHITIET_SOTHUE>();
                                    foreach (CHITIET_SOTHUE itdata in objResponse.data[0].KHOAN_THUE)
                                    {
                                        infoSoThue.ROW_SOTHUE.Add(itdata);
                                    }
                                    foreach (CHITIET_SOTHUE itdata in objResponse.data[0].KHOAN_THU_KHAC)
                                    {
                                        infoSoThue.ROW_SOTHUE.Add(itdata);
                                    }
                                }
                            }

                            capnhatSothue(ID_KHOANNOP, MST, LOAI_DL, SO_QDINH_TBAO, MA_HS, SO_GIAYTO, objResponse);
                        }
                        catch (Exception ex)
                        {
                            log.Error("1.getMSG05: " + ex.Message + " \n  " + ex.StackTrace);
                        }
                    }
                }
                return infoSoThue;
            }
            catch (Exception ex)
            {
                log.Error("2.getMSG05: " + ex.Message + " \n  " + ex.StackTrace);
                //throw ex;
            }
            return null;
        }
        private static void capnhatSothue(string ID_KHOANNOP, string MST, string LOAI_DL, string SO_QDINH_TBAO, string MA_HS, string SO_GIAYTO, TruyVanSoThueResponse msg06)
        {
            OracleConnection conn = default(OracleConnection);
            IDbTransaction transCT = null;

            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_sothue_id (");
            sbSqlInsert.Append("mst,");
            sbSqlInsert.Append("ten_nnt,");
            sbSqlInsert.Append("dia_chi_nnt,");
            sbSqlInsert.Append("so_qdinh_tbao,");
            sbSqlInsert.Append("ngay_quyet_dinh,");
            sbSqlInsert.Append("cq_qd,");
            sbSqlInsert.Append("lcn_owner,");
            sbSqlInsert.Append("so_giayto,");
            sbSqlInsert.Append("ma_cqt_qly,");
            sbSqlInsert.Append("ma_nt,");
            sbSqlInsert.Append("chuong_nnt,");
            sbSqlInsert.Append("thutu_uutien,");
            sbSqlInsert.Append("ma_nhom,");
            sbSqlInsert.Append("thutu_nhom,");
            sbSqlInsert.Append("id_khoannop,");
            sbSqlInsert.Append("ma_ddkd,");
            sbSqlInsert.Append("ten_ddkd,");
            sbSqlInsert.Append("ky_thue,");
            sbSqlInsert.Append("cquan_tquyen,");
            sbSqlInsert.Append("ten_cquan_tquyen,");
            sbSqlInsert.Append("ma_cqthu,");
            sbSqlInsert.Append("ten_cqthu,");
            sbSqlInsert.Append("ma_tinh_nnt,");
            sbSqlInsert.Append("ten_tinh_nnt,");
            sbSqlInsert.Append("ma_huyen_nnt,");
            sbSqlInsert.Append("ten_huyen_nnt,");
            sbSqlInsert.Append("ma_xa_nnt,");
            sbSqlInsert.Append("ten_xa_nnt,");
            sbSqlInsert.Append("loai_nnt,");
            sbSqlInsert.Append("dbhc_ma_tinh,");
            sbSqlInsert.Append("dbhc_ten_tinh,");
            sbSqlInsert.Append("dbhc_ma_huyen,");
            sbSqlInsert.Append("dbhc_ten_huyen,");
            sbSqlInsert.Append("dbhc_ma_xa,");
            sbSqlInsert.Append("dbhc_ten_xa,");
            sbSqlInsert.Append("shkb,");
            sbSqlInsert.Append("ten_kb,");
            sbSqlInsert.Append("ma_pnn,");
            sbSqlInsert.Append("chuong,");
            sbSqlInsert.Append("ma_muc,");
            sbSqlInsert.Append("tieu_muc,");
            sbSqlInsert.Append("ten_tmuc,");
            sbSqlInsert.Append("kyhieu_gd,");
            sbSqlInsert.Append("ten_gd,");
            sbSqlInsert.Append("tk_nsnn,");
            sbSqlInsert.Append("loai_tien,");
            sbSqlInsert.Append("ty_gia,");
            sbSqlInsert.Append("ma_loai_nvu,");
            sbSqlInsert.Append("ten_loai_nvu,");
            sbSqlInsert.Append("so_tien,");
            sbSqlInsert.Append("so_danop_nhtm,");
            sbSqlInsert.Append("ngay_tcn_den,");
            sbSqlInsert.Append("han_nop,");
            sbSqlInsert.Append("loai_thue,");
            sbSqlInsert.Append("ten_loai_thue,");
            sbSqlInsert.Append("ma_hs,");
            sbSqlInsert.Append("ma_gcn,");
            sbSqlInsert.Append("so_thuadat,");
            sbSqlInsert.Append("so_to_bando,");
            sbSqlInsert.Append("so_giayto_hskt,");
            sbSqlInsert.Append("dia_chi_hskt,");
            sbSqlInsert.Append("ma_tinh_hskt,");
            sbSqlInsert.Append("ten_tinh_hskt,");
            sbSqlInsert.Append("ma_huyen_hskt,");
            sbSqlInsert.Append("ten_huyen_hskt,");
            sbSqlInsert.Append("ma_xa_hskt,");
            sbSqlInsert.Append("ten_xa_hskt,");
            sbSqlInsert.Append("ma_tinhchatkhoannop,");
            sbSqlInsert.Append("ten_tinhchatkhoannop,");
            sbSqlInsert.Append("ma_trangthai,");
            sbSqlInsert.Append("ten_trangthai,");
            sbSqlInsert.Append("so_khung,");
            sbSqlInsert.Append("so_may,");
            sbSqlInsert.Append("dacdiem_ptien,");
            sbSqlInsert.Append("ngay_gia_han,");
            sbSqlInsert.Append("ma_tchieu_goc,");
            sbSqlInsert.Append("ma_tchieu_ctu)");
            sbSqlInsert.Append("  VALUES   (");
            sbSqlInsert.Append(":mst,");
            sbSqlInsert.Append(":ten_nnt,");
            sbSqlInsert.Append(":dia_chi_nnt,");
            sbSqlInsert.Append(":so_qdinh_tbao,");
            sbSqlInsert.Append(":ngay_quyet_dinh,");
            sbSqlInsert.Append(":cq_qd,");
            sbSqlInsert.Append(":lcn_owner,");
            sbSqlInsert.Append(":so_giayto,");
            sbSqlInsert.Append(":ma_cqt_qly,");
            sbSqlInsert.Append(":ma_nt,");
            sbSqlInsert.Append(":chuong_nnt,");
            sbSqlInsert.Append(":thutu_uutien,");
            sbSqlInsert.Append(":ma_nhom,");
            sbSqlInsert.Append(":thutu_nhom,");
            sbSqlInsert.Append(":id_khoannop,");
            sbSqlInsert.Append(":ma_ddkd,");
            sbSqlInsert.Append(":ten_ddkd,");
            sbSqlInsert.Append(":ky_thue,");
            sbSqlInsert.Append(":cquan_tquyen,");
            sbSqlInsert.Append(":ten_cquan_tquyen,");
            sbSqlInsert.Append(":ma_cqthu,");
            sbSqlInsert.Append(":ten_cqthu,");
            sbSqlInsert.Append(":ma_tinh_nnt,");
            sbSqlInsert.Append(":ten_tinh_nnt,");
            sbSqlInsert.Append(":ma_huyen_nnt,");
            sbSqlInsert.Append(":ten_huyen_nnt,");
            sbSqlInsert.Append(":ma_xa_nnt,");
            sbSqlInsert.Append(":ten_xa_nnt,");
            sbSqlInsert.Append(":loai_nnt,");
            sbSqlInsert.Append(":dbhc_ma_tinh,");
            sbSqlInsert.Append(":dbhc_ten_tinh,");
            sbSqlInsert.Append(":dbhc_ma_huyen,");
            sbSqlInsert.Append(":dbhc_ten_huyen,");
            sbSqlInsert.Append(":dbhc_ma_xa,");
            sbSqlInsert.Append(":dbhc_ten_xa,");
            sbSqlInsert.Append(":shkb,");
            sbSqlInsert.Append(":ten_kb,");
            sbSqlInsert.Append(":ma_pnn,");
            sbSqlInsert.Append(":chuong,");
            sbSqlInsert.Append(":ma_muc,");
            sbSqlInsert.Append(":tieu_muc,");
            sbSqlInsert.Append(":ten_tmuc,");
            sbSqlInsert.Append(":kyhieu_gd,");
            sbSqlInsert.Append(":ten_gd,");
            sbSqlInsert.Append(":tk_nsnn,");
            sbSqlInsert.Append(":loai_tien,");
            sbSqlInsert.Append(":ty_gia,");
            sbSqlInsert.Append(":ma_loai_nvu,");
            sbSqlInsert.Append(":ten_loai_nvu,");
            sbSqlInsert.Append(":so_tien,");
            sbSqlInsert.Append(":so_danop_nhtm,");
            sbSqlInsert.Append(":ngay_tcn_den,");
            sbSqlInsert.Append(":han_nop,");
            sbSqlInsert.Append(":loai_thue,");
            sbSqlInsert.Append(":ten_loai_thue,");
            sbSqlInsert.Append(":ma_hs,");
            sbSqlInsert.Append(":ma_gcn,");
            sbSqlInsert.Append(":so_thuadat,");
            sbSqlInsert.Append(":so_to_bando,");
            sbSqlInsert.Append(":so_giayto_hskt,");
            sbSqlInsert.Append(":dia_chi_hskt,");
            sbSqlInsert.Append(":ma_tinh_hskt,");
            sbSqlInsert.Append(":ten_tinh_hskt,");
            sbSqlInsert.Append(":ma_huyen_hskt,");
            sbSqlInsert.Append(":ten_huyen_hskt,");
            sbSqlInsert.Append(":ma_xa_hskt,");
            sbSqlInsert.Append(":ten_xa_hskt,");
            sbSqlInsert.Append(":ma_tinhchatkhoannop,");
            sbSqlInsert.Append(":ten_tinhchatkhoannop,");
            sbSqlInsert.Append(":ma_trangthai,");
            sbSqlInsert.Append(":ten_trangthai,");
            sbSqlInsert.Append(":so_khung,");
            sbSqlInsert.Append(":so_may,");
            sbSqlInsert.Append(":dacdiem_ptien,");
            sbSqlInsert.Append(":ngay_gia_han,");
            sbSqlInsert.Append(":ma_tchieu_goc,");
            sbSqlInsert.Append(":ma_tchieu_ctu)");
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                // 
                if (msg06.data.Count <= 0)
                {
                    return;
                }
                foreach (ResponseDataSoThue datasothue in msg06.data)
                {
                    if (datasothue.THONGTIN_NNT == null)
                    {
                        return;
                    }
                }
                //Xoa du lieu truoc
                //Khai bao cac cau lenh
                StringBuilder sbSqlDelete = new StringBuilder("");
                sbSqlDelete.Append("DELETE FROM TCS_SOTHUE_ID WHERE MST = :MST");

                IDbDataParameter[] p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "MST";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = MST;

                DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);


                //Lay thong tin so thue phai nop dua vao CSDL
                //Insert vao bang So thue
                string v_mst = DBNull.Value.ToString();
                string v_ten_nnt = DBNull.Value.ToString();
                string v_dia_chi_nnt = DBNull.Value.ToString();
                string v_so_qdinh_tbao = DBNull.Value.ToString();
                string v_ngay_quyet_dinh = DBNull.Value.ToString();
                string v_cq_qd = DBNull.Value.ToString();
                string v_lcn_owner = DBNull.Value.ToString();
                string v_so_giayto = DBNull.Value.ToString();
                string v_ma_cqt_qly = DBNull.Value.ToString();
                string v_ma_nt = DBNull.Value.ToString();
                string v_chuong_nnt = DBNull.Value.ToString();

                string v_thutu_uutien = DBNull.Value.ToString();
                string v_ma_nhom = DBNull.Value.ToString();
                string v_thutu_nhom = DBNull.Value.ToString();
                string v_id_khoannop = DBNull.Value.ToString();
                string v_ma_ddkd = DBNull.Value.ToString();
                string v_ten_ddkd = DBNull.Value.ToString();
                string v_ky_thue = DBNull.Value.ToString();
                string v_cquan_tquyen = DBNull.Value.ToString();
                string v_ten_cquan_tquyen = DBNull.Value.ToString();
                string v_ma_cqthu = DBNull.Value.ToString();
                string v_ten_cqthu = DBNull.Value.ToString();
                string v_ma_tinh_nnt = DBNull.Value.ToString();
                string v_ten_tinh_nnt = DBNull.Value.ToString();
                string v_ma_huyen_nnt = DBNull.Value.ToString();
                string v_ten_huyen_nnt = DBNull.Value.ToString();
                string v_ma_xa_nnt = DBNull.Value.ToString();
                string v_ten_xa_nnt = DBNull.Value.ToString();
                string v_loai_nnt = DBNull.Value.ToString();
                string v_dbhc_ma_tinh = DBNull.Value.ToString();
                string v_dbhc_ten_tinh = DBNull.Value.ToString();
                string v_dbhc_ma_huyen = DBNull.Value.ToString();
                string v_dbhc_ten_huyen = DBNull.Value.ToString();
                string v_dbhc_ma_xa = DBNull.Value.ToString();
                string v_dbhc_ten_xa = DBNull.Value.ToString();
                string v_shkb = DBNull.Value.ToString();
                string v_ten_kb = DBNull.Value.ToString();
                string v_ma_pnn = DBNull.Value.ToString();
                string v_chuong = DBNull.Value.ToString();
                string v_ma_muc = DBNull.Value.ToString();
                string v_tieu_muc = DBNull.Value.ToString();
                string v_ten_tmuc = DBNull.Value.ToString();
                string v_kyhieu_gd = DBNull.Value.ToString();
                string v_ten_gd = DBNull.Value.ToString();
                string v_tk_nsnn = DBNull.Value.ToString();
                string v_loai_tien = DBNull.Value.ToString();
                string v_ty_gia = DBNull.Value.ToString();
                string v_ma_loai_nvu = DBNull.Value.ToString();
                string v_ten_loai_nvu = DBNull.Value.ToString();
                string v_so_tien = DBNull.Value.ToString();
                string v_so_danop_nhtm = DBNull.Value.ToString();
                string v_ngay_tcn_den = DBNull.Value.ToString();
                string v_han_nop = DBNull.Value.ToString();
                string v_loai_thue = DBNull.Value.ToString();
                string v_ten_loai_thue = DBNull.Value.ToString();
                string v_ma_hs = DBNull.Value.ToString();
                string v_ma_gcn = DBNull.Value.ToString();
                string v_so_thuadat = DBNull.Value.ToString();
                string v_so_to_bando = DBNull.Value.ToString();
                string v_so_giayto_hskt = DBNull.Value.ToString();
                string v_dia_chi_hskt = DBNull.Value.ToString();
                string v_ma_tinh_hskt = DBNull.Value.ToString();
                string v_ten_tinh_hskt = DBNull.Value.ToString();
                string v_ma_huyen_hskt = DBNull.Value.ToString();
                string v_ten_huyen_hskt = DBNull.Value.ToString();
                string v_ma_xa_hskt = DBNull.Value.ToString();
                string v_ten_xa_hskt = DBNull.Value.ToString();
                string v_ma_tinhchatkhoannop = DBNull.Value.ToString();
                string v_ten_tinhchatkhoannop = DBNull.Value.ToString();
                string v_ma_trangthai = DBNull.Value.ToString();
                string v_ten_trangthai = DBNull.Value.ToString();
                string v_so_khung = DBNull.Value.ToString();
                string v_so_may = DBNull.Value.ToString();
                string v_dacdiem_ptien = DBNull.Value.ToString();
                string v_ngay_gia_han = DBNull.Value.ToString();
                string v_ma_tchieu_goc = DBNull.Value.ToString();
                string v_ma_tchieu_ctu = DBNull.Value.ToString();

                foreach (ResponseDataSoThue datasothue in msg06.data)
                {
                    v_mst = datasothue.THONGTIN_NNT.MST;
                    v_ten_nnt = datasothue.THONGTIN_NNT.TEN_NNT;
                    v_dia_chi_nnt = datasothue.THONGTIN_NNT.DIA_CHI_NNT;
                    if (datasothue.THONGTIN_NNT.LOAI_NNT != null)
                    {
                        if (!datasothue.THONGTIN_NNT.LOAI_NNT.Equals(string.Empty))
                        {
                            v_loai_nnt = datasothue.THONGTIN_NNT.LOAI_NNT;
                        }
                    }

                    if (datasothue.THONGTIN_NNT.SO_GIAYTO != null)
                    {
                        if (!datasothue.THONGTIN_NNT.SO_GIAYTO.Equals(string.Empty))
                        {
                            v_so_giayto = datasothue.THONGTIN_NNT.SO_GIAYTO;
                        }
                    }
                    v_ma_cqt_qly = datasothue.THONGTIN_NNT.MA_CQT_QLY;
                    v_cq_qd = "";
                    v_lcn_owner = "";
                    v_ma_nt = "";
                    v_chuong_nnt = datasothue.THONGTIN_NNT.CHUONG_NNT;

                    if (datasothue.THONGTIN_NNT.MA_TINH_NNT != null)
                    {
                        if (!datasothue.THONGTIN_NNT.MA_TINH_NNT.Equals(string.Empty))
                        {
                            v_ma_tinh_nnt = datasothue.THONGTIN_NNT.MA_TINH_NNT;
                        }
                    }
                    if (datasothue.THONGTIN_NNT.TEN_TINH_NNT != null)
                    {
                        if (!datasothue.THONGTIN_NNT.TEN_TINH_NNT.Equals(string.Empty))
                        {
                            v_ten_tinh_nnt = datasothue.THONGTIN_NNT.TEN_TINH_NNT;
                        }
                    }
                    if (datasothue.THONGTIN_NNT.MA_HUYEN_NNT != null)
                    {
                        if (!datasothue.THONGTIN_NNT.MA_HUYEN_NNT.Equals(string.Empty))
                        {
                            v_ma_huyen_nnt = datasothue.THONGTIN_NNT.MA_HUYEN_NNT;
                        }
                    }
                    if (datasothue.THONGTIN_NNT.TEN_HUYEN_NNT != null)
                    {
                        if (!datasothue.THONGTIN_NNT.TEN_HUYEN_NNT.Equals(string.Empty))
                        {
                            v_ten_huyen_nnt = datasothue.THONGTIN_NNT.TEN_HUYEN_NNT;
                        }
                    }
                    if (datasothue.THONGTIN_NNT.MA_XA_NNT != null)
                    {
                        if (!datasothue.THONGTIN_NNT.MA_XA_NNT.Equals(string.Empty))
                        {
                            v_ma_xa_nnt = datasothue.THONGTIN_NNT.MA_XA_NNT;
                        }
                    }
                    if (datasothue.THONGTIN_NNT.TEN_XA_NNT != null)
                    {
                        if (!datasothue.THONGTIN_NNT.TEN_XA_NNT.Equals(string.Empty))
                        {
                            v_ten_xa_nnt = datasothue.THONGTIN_NNT.TEN_XA_NNT;
                        }
                    }

                    //add nợ thuế vào cùng 1 list
                    List<CHITIET_SOTHUE> listSOTHUE = new List<CHITIET_SOTHUE>();
                    foreach (CHITIET_SOTHUE itdata in datasothue.KHOAN_THUE)
                    {
                        listSOTHUE.Add(itdata);
                    }
                    foreach (CHITIET_SOTHUE itdata in datasothue.KHOAN_THU_KHAC)
                    {
                        listSOTHUE.Add(itdata);
                    }

                    //insert data vào database
                    foreach (CHITIET_SOTHUE sothue in listSOTHUE)
                    {
                        if (sothue.SO_QDINH_TBAO != null)
                        {
                            if (!sothue.SO_QDINH_TBAO.Equals(string.Empty))
                            {
                                v_so_qdinh_tbao = sothue.SO_QDINH_TBAO;
                            }
                        }
                        if (sothue.LOAI_TIEN != null)
                        {
                            if (!sothue.LOAI_TIEN.Equals(string.Empty))
                            {
                                v_ma_nt = sothue.LOAI_TIEN;
                            }
                        }
                        if (sothue.ID_KHOANNOP != null)
                        {
                            if (!sothue.ID_KHOANNOP.Equals(string.Empty))
                            {
                                v_id_khoannop = sothue.ID_KHOANNOP;
                            }
                        }
                        if (sothue.MA_DDKD != null)
                        {
                            if (!sothue.MA_DDKD.Equals(string.Empty))
                            {
                                v_ma_ddkd = sothue.MA_DDKD;
                            }
                        }
                        if (sothue.TEN_DDKD != null)
                        {
                            if (!sothue.TEN_DDKD.Equals(string.Empty))
                            {
                                v_ten_ddkd = sothue.TEN_DDKD;
                            }
                        }
                        if (sothue.KY_THUE != null)
                        {
                            if (!sothue.KY_THUE.Equals(string.Empty))
                            {
                                v_ky_thue = sothue.KY_THUE;
                            }
                        }
                        if (sothue.CQUAN_TQUYEN != null)
                        {
                            if (!sothue.CQUAN_TQUYEN.Equals(string.Empty))
                            {
                                v_cquan_tquyen = sothue.CQUAN_TQUYEN;
                            }
                        }
                        if (sothue.TEN_CQUAN_TQUYEN != null)
                        {
                            if (!sothue.TEN_CQUAN_TQUYEN.Equals(string.Empty))
                            {
                                v_ten_cquan_tquyen = sothue.TEN_CQUAN_TQUYEN;
                            }
                        }
                        if (sothue.MA_CQTHU != null)
                        {
                            if (!sothue.MA_CQTHU.Equals(string.Empty))
                            {
                                v_ma_cqthu = sothue.MA_CQTHU;
                            }
                        }
                        if (sothue.TEN_CQTHU != null)
                        {
                            if (!sothue.TEN_CQTHU.Equals(string.Empty))
                            {
                                v_ten_cqthu = sothue.TEN_CQTHU;
                            }
                        }
                        if (sothue.DBHC_MA_TINH != null)
                        {
                            if (!sothue.DBHC_MA_TINH.Equals(string.Empty))
                            {
                                v_dbhc_ma_tinh = sothue.DBHC_MA_TINH;
                            }
                        }
                        if (sothue.DBHC_TEN_TINH != null)
                        {
                            if (!sothue.DBHC_TEN_TINH.Equals(string.Empty))
                            {
                                v_dbhc_ten_tinh = sothue.DBHC_TEN_TINH;
                            }
                        }
                        if (sothue.DBHC_MA_HUYEN != null)
                        {
                            if (!sothue.DBHC_MA_HUYEN.Equals(string.Empty))
                            {
                                v_dbhc_ma_huyen = sothue.DBHC_MA_HUYEN;
                            }
                        }
                        if (sothue.DBHC_TEN_HUYEN != null)
                        {
                            if (!sothue.DBHC_TEN_HUYEN.Equals(string.Empty))
                            {
                                v_dbhc_ten_huyen = sothue.DBHC_TEN_HUYEN;
                            }
                        }
                        if (sothue.DBHC_MA_XA != null)
                        {
                            if (!sothue.DBHC_MA_XA.Equals(string.Empty))
                            {
                                v_dbhc_ma_xa = sothue.DBHC_MA_XA;
                            }
                        } if (sothue.DBHC_TEN_XA != null)
                        {
                            if (!sothue.DBHC_TEN_XA.Equals(string.Empty))
                            {
                                v_dbhc_ten_xa = sothue.DBHC_TEN_XA;
                            }
                        } if (sothue.SHKB != null)
                        {
                            if (!sothue.SHKB.Equals(string.Empty))
                            {
                                v_shkb = sothue.SHKB;
                            }
                        }
                        if (sothue.TEN_KB != null)
                        {
                            if (!sothue.TEN_KB.Equals(string.Empty))
                            {
                                v_ten_kb = sothue.TEN_KB;
                            }
                        }
                        if (sothue.MA_PNN != null)
                        {
                            if (!sothue.MA_PNN.Equals(string.Empty))
                            {
                                v_ma_pnn = sothue.MA_PNN;
                            }
                        }
                        if (sothue.CHUONG != null)
                        {
                            if (!sothue.CHUONG.Equals(string.Empty))
                            {
                                v_chuong = sothue.CHUONG;
                            }
                        }
                        //v_ma_muc = "";
                        if (sothue.TIEU_MUC != null)
                        {
                            if (!sothue.TIEU_MUC.Equals(string.Empty))
                            {
                                v_tieu_muc = sothue.TIEU_MUC;
                            }
                        }
                        if (sothue.TEN_TMUC != null)
                        {
                            if (!sothue.TEN_TMUC.Equals(string.Empty))
                            {
                                v_ten_tmuc = sothue.TEN_TMUC;
                            }
                        }
                        if (sothue.KYHIEU_GD != null)
                        {
                            if (!sothue.KYHIEU_GD.Equals(string.Empty))
                            {
                                v_kyhieu_gd = sothue.KYHIEU_GD;
                            }
                        } if (sothue.TEN_GD != null)
                        {
                            if (!sothue.TEN_GD.Equals(string.Empty))
                            {
                                v_ten_gd = sothue.TEN_GD;
                            }
                        } if (sothue.TK_NSNN != null)
                        {
                            if (!sothue.TK_NSNN.Equals(string.Empty))
                            {
                                v_tk_nsnn = sothue.TK_NSNN;
                            }
                        } if (sothue.LOAI_TIEN != null)
                        {
                            if (!sothue.LOAI_TIEN.Equals(string.Empty))
                            {
                                v_loai_tien = sothue.LOAI_TIEN;
                            }
                        }
                        if (sothue.TY_GIA != null)
                        {
                            if (!sothue.TY_GIA.Equals(string.Empty))
                            {
                                v_ty_gia = sothue.TY_GIA;
                            }
                        }
                        if (sothue.MA_LOAI_NVU != null)
                        {
                            if (!sothue.MA_LOAI_NVU.Equals(string.Empty))
                            {
                                v_ma_loai_nvu = sothue.MA_LOAI_NVU;
                            }
                        }
                        if (sothue.TEN_LOAI_NVU != null)
                        {
                            if (!sothue.TEN_LOAI_NVU.Equals(string.Empty))
                            {
                                v_ten_loai_nvu = sothue.TEN_LOAI_NVU;
                            }
                        }

                        if (sothue.SO_TIEN != null)
                        {
                            if (!sothue.SO_TIEN.Equals(string.Empty))
                            {
                                v_so_tien = sothue.SO_TIEN;
                            }
                        }
                        if (sothue.SO_DANOP_NHTM != null)
                        {
                            if (!sothue.SO_DANOP_NHTM.Equals(string.Empty))
                            {
                                v_so_danop_nhtm = sothue.SO_DANOP_NHTM;
                            }
                        }


                        if (sothue.LOAI_THUE != null)
                        {
                            if (!sothue.LOAI_THUE.Equals(string.Empty))
                            {
                                v_loai_thue = sothue.LOAI_THUE;
                            }
                        }
                        if (sothue.TEN_LOAI_THUE != null)
                        {
                            if (!sothue.TEN_LOAI_THUE.Equals(string.Empty))
                            {
                                v_ten_loai_thue = sothue.TEN_LOAI_THUE;
                            }
                        }
                        if (sothue.MA_HS != null)
                        {
                            if (!sothue.MA_HS.Equals(string.Empty))
                            {
                                v_ma_hs = sothue.MA_HS;
                            }
                        }
                        if (sothue.MA_GCN != null)
                        {
                            if (!sothue.MA_GCN.Equals(string.Empty))
                            {
                                v_ma_gcn = sothue.MA_GCN;
                            }
                        }
                        if (sothue.SO_THUADAT != null)
                        {
                            if (!sothue.SO_THUADAT.Equals(string.Empty))
                            {
                                v_so_thuadat = sothue.SO_THUADAT;
                            }
                        }
                        if (sothue.SO_TO_BANDO != null)
                        {
                            if (!sothue.SO_TO_BANDO.Equals(string.Empty))
                            {
                                v_so_to_bando = sothue.SO_TO_BANDO;
                            }
                        }
                        if (sothue.SO_GIAYTO_HSKT != null)
                        {
                            if (!sothue.SO_GIAYTO_HSKT.Equals(string.Empty))
                            {
                                v_so_giayto_hskt = sothue.SO_GIAYTO_HSKT;
                            }
                        }
                        if (sothue.DIA_CHI_HSKT != null)
                        {
                            if (!sothue.DIA_CHI_HSKT.Equals(string.Empty))
                            {
                                v_dia_chi_hskt = sothue.DIA_CHI_HSKT;
                            }
                        }
                        if (sothue.MA_TINH_HSKT != null)
                        {
                            if (!sothue.MA_TINH_HSKT.Equals(string.Empty))
                            {
                                v_ma_tinh_hskt = sothue.MA_TINH_HSKT;
                            }
                        }
                        if (sothue.TEN_TINH_HSKT != null)
                        {
                            if (!sothue.TEN_TINH_HSKT.Equals(string.Empty))
                            {
                                v_ten_tinh_hskt = sothue.TEN_TINH_HSKT;
                            }
                        }
                        if (sothue.MA_HUYEN_HSKT != null)
                        {
                            if (!sothue.MA_HUYEN_HSKT.Equals(string.Empty))
                            {
                                v_ma_huyen_hskt = sothue.MA_HUYEN_HSKT;
                            }
                        }
                        if (sothue.TEN_HUYEN_HSKT != null)
                        {
                            if (!sothue.TEN_HUYEN_HSKT.Equals(string.Empty))
                            {
                                v_ten_huyen_hskt = sothue.TEN_HUYEN_HSKT;
                            }
                        }
                        if (sothue.MA_XA_HSKT != null)
                        {
                            if (!sothue.MA_XA_HSKT.Equals(string.Empty))
                            {
                                v_ma_xa_hskt = sothue.MA_XA_HSKT;
                            }
                        }
                        if (sothue.TEN_XA_HSKT != null)
                        {
                            if (!sothue.TEN_XA_HSKT.Equals(string.Empty))
                            {
                                v_ten_xa_hskt = sothue.TEN_XA_HSKT;
                            }
                        }
                        if (sothue.MA_TINHCHATKHOANNOP != null)
                        {
                            if (!sothue.MA_TINHCHATKHOANNOP.Equals(string.Empty))
                            {
                                v_ma_tinhchatkhoannop = sothue.MA_TINHCHATKHOANNOP;
                            }
                        }
                        if (sothue.TEN_TINHCHATKHOANNOP != null)
                        {
                            if (!sothue.TEN_TINHCHATKHOANNOP.Equals(string.Empty))
                            {
                                v_ten_tinhchatkhoannop = sothue.TEN_TINHCHATKHOANNOP;
                            }
                        }
                        if (sothue.MA_TRANGTHAI != null)
                        {
                            if (!sothue.MA_TRANGTHAI.Equals(string.Empty))
                            {
                                v_ma_trangthai = sothue.MA_TRANGTHAI;
                            }
                        }
                        if (sothue.TEN_TRANGTHAI != null)
                        {
                            if (!sothue.TEN_TRANGTHAI.Equals(string.Empty))
                            {
                                v_ten_trangthai = sothue.TEN_TRANGTHAI;
                            }
                        }
                        if (sothue.SO_KHUNG != null)
                        {
                            if (!sothue.SO_KHUNG.Equals(string.Empty))
                            {
                                v_so_khung = sothue.SO_KHUNG;
                            }
                        }
                        if (sothue.SO_MAY != null)
                        {
                            if (!sothue.SO_MAY.Equals(string.Empty))
                            {
                                v_so_may = sothue.SO_MAY;
                            }
                        }
                        if (sothue.DACDIEM_PTIEN != null)
                        {
                            if (!sothue.DACDIEM_PTIEN.Equals(string.Empty))
                            {
                                v_dacdiem_ptien = sothue.DACDIEM_PTIEN;
                            }
                        }
                        if (sothue.MA_TCHIEU_GOC != null)
                        {
                            if (!sothue.MA_TCHIEU_GOC.Equals(string.Empty))
                            {
                                v_ma_tchieu_goc = sothue.MA_TCHIEU_GOC;
                            }
                        }
                        if (sothue.MA_TCHIEU_CTU != null)
                        {
                            if (!sothue.MA_TCHIEU_CTU.Equals(string.Empty))
                            {
                                v_ma_tchieu_ctu = sothue.MA_TCHIEU_CTU;
                            }
                        }

                        p = new IDbDataParameter[77];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "mst";
                        p[0].DbType = DbType.String;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = v_mst;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ten_nnt";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = v_ten_nnt;

                        p[2] = new OracleParameter();
                        p[2].ParameterName = "dia_chi_nnt";
                        p[2].DbType = DbType.Date;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = v_dia_chi_nnt;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "so_qdinh_tbao";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = v_so_qdinh_tbao;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "ngay_quyet_dinh";
                        p[4].DbType = DbType.String;
                        p[4].Direction = ParameterDirection.Input;
                        if (sothue.NGAY_QUYET_DINH != null && sothue.NGAY_QUYET_DINH.Trim().Length > 0)
                        {
                            p[4].Value = DateTime.ParseExact(sothue.NGAY_QUYET_DINH, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            p[4].Value = v_ngay_quyet_dinh;
                        }


                        p[5] = new OracleParameter();
                        p[5].ParameterName = "cq_qd";
                        p[5].DbType = DbType.String;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = v_cq_qd;

                        p[6] = new OracleParameter();
                        p[6].ParameterName = "lcn_owner";
                        p[6].DbType = DbType.String;
                        p[6].Direction = ParameterDirection.Input;
                        p[6].Value = v_lcn_owner;

                        p[7] = new OracleParameter();
                        p[7].ParameterName = "so_giayto";
                        p[7].DbType = DbType.String;
                        p[7].Direction = ParameterDirection.Input;
                        p[7].Value = v_so_giayto;

                        p[8] = new OracleParameter();
                        p[8].ParameterName = "ma_cqt_qly";
                        p[8].DbType = DbType.String;
                        p[8].Direction = ParameterDirection.Input;
                        p[8].Value = v_ma_cqt_qly;

                        p[9] = new OracleParameter();
                        p[9].ParameterName = "ma_nt";
                        p[9].DbType = DbType.String;
                        p[9].Direction = ParameterDirection.Input;
                        p[9].Value = v_ma_nt;

                        p[10] = new OracleParameter();
                        p[10].ParameterName = "chuong_nnt";
                        p[10].DbType = DbType.String;
                        p[10].Direction = ParameterDirection.Input;
                        p[10].Value = v_chuong_nnt;

                        p[11] = new OracleParameter();
                        p[11].ParameterName = "thutu_uutien";
                        p[11].DbType = DbType.Int32;
                        p[11].Direction = ParameterDirection.Input;
                        p[11].Value = ConvertInt(sothue.THUTU_UUTIEN);

                        p[12] = new OracleParameter();
                        p[12].ParameterName = "ma_nhom";
                        p[12].DbType = DbType.String;
                        p[12].Direction = ParameterDirection.Input;
                        p[12].Value = v_ma_nhom;

                        p[13] = new OracleParameter();
                        p[13].ParameterName = "thutu_nhom";
                        p[13].DbType = DbType.String;
                        p[13].Direction = ParameterDirection.Input;
                        p[13].Value = ConvertInt(sothue.THUTU_NHOM);

                        p[14] = new OracleParameter();
                        p[14].ParameterName = "id_khoannop";
                        p[14].DbType = DbType.String;
                        p[14].Direction = ParameterDirection.Input;
                        p[14].Value = v_id_khoannop;

                        p[15] = new OracleParameter();
                        p[15].ParameterName = "ma_ddkd";
                        p[15].DbType = DbType.String;
                        p[15].Direction = ParameterDirection.Input;
                        p[15].Value = v_ma_ddkd;

                        p[16] = new OracleParameter();
                        p[16].ParameterName = "ten_ddkd";
                        p[16].DbType = DbType.String;
                        p[16].Direction = ParameterDirection.Input;
                        p[16].Value = v_ten_ddkd;

                        p[17] = new OracleParameter();
                        p[17].ParameterName = "ky_thue";
                        p[17].DbType = DbType.String;
                        p[17].Direction = ParameterDirection.Input;
                        p[17].Value = v_ky_thue;

                        p[18] = new OracleParameter();
                        p[18].ParameterName = "cquan_tquyen";
                        p[18].DbType = DbType.String;
                        p[18].Direction = ParameterDirection.Input;
                        p[18].Value = v_cquan_tquyen;

                        p[19] = new OracleParameter();
                        p[19].ParameterName = "ten_cquan_tquyen";
                        p[19].DbType = DbType.String;
                        p[19].Direction = ParameterDirection.Input;
                        p[19].Value = v_ten_cquan_tquyen;

                        p[20] = new OracleParameter();
                        p[20].ParameterName = "ma_cqthu";
                        p[20].DbType = DbType.String;
                        p[20].Direction = ParameterDirection.Input;
                        p[20].Value = v_ma_cqthu;

                        p[21] = new OracleParameter();
                        p[21].ParameterName = "ten_cqthu";
                        p[21].DbType = DbType.String;
                        p[21].Direction = ParameterDirection.Input;
                        p[21].Value = v_ten_cqthu;

                        p[22] = new OracleParameter();
                        p[22].ParameterName = "ma_tinh_nnt";
                        p[22].DbType = DbType.String;
                        p[22].Direction = ParameterDirection.Input;
                        p[22].Value = v_ma_tinh_nnt;

                        p[23] = new OracleParameter();
                        p[23].ParameterName = "ten_tinh_nnt";
                        p[23].DbType = DbType.String;
                        p[23].Direction = ParameterDirection.Input;
                        p[23].Value = v_ten_tinh_nnt;

                        p[24] = new OracleParameter();
                        p[24].ParameterName = "ma_huyen_nnt";
                        p[24].DbType = DbType.String;
                        p[24].Direction = ParameterDirection.Input;
                        p[24].Value = v_ma_huyen_nnt;

                        p[25] = new OracleParameter();
                        p[25].ParameterName = "ten_huyen_nnt";
                        p[25].DbType = DbType.String;
                        p[25].Direction = ParameterDirection.Input;
                        p[25].Value = v_ten_huyen_nnt;

                        p[26] = new OracleParameter();
                        p[26].ParameterName = "ma_xa_nnt";
                        p[26].DbType = DbType.String;
                        p[26].Direction = ParameterDirection.Input;
                        p[26].Value = v_ma_xa_nnt;

                        p[27] = new OracleParameter();
                        p[27].ParameterName = "ten_xa_nnt";
                        p[27].DbType = DbType.String;
                        p[27].Direction = ParameterDirection.Input;
                        p[27].Value = v_ten_xa_nnt;

                        p[28] = new OracleParameter();
                        p[28].ParameterName = "loai_nnt";
                        p[28].DbType = DbType.String;
                        p[28].Direction = ParameterDirection.Input;
                        p[28].Value = v_loai_nnt;

                        p[29] = new OracleParameter();
                        p[29].ParameterName = "dbhc_ma_tinh";
                        p[29].DbType = DbType.String;
                        p[29].Direction = ParameterDirection.Input;
                        p[29].Value = v_dbhc_ma_tinh;

                        p[30] = new OracleParameter();
                        p[30].ParameterName = "dbhc_ten_tinh";
                        p[30].DbType = DbType.String;
                        p[30].Direction = ParameterDirection.Input;
                        p[30].Value = v_dbhc_ten_tinh;

                        p[31] = new OracleParameter();
                        p[31].ParameterName = "dbhc_ma_huyen";
                        p[31].DbType = DbType.String;
                        p[31].Direction = ParameterDirection.Input;
                        p[31].Value = v_dbhc_ma_huyen;

                        p[32] = new OracleParameter();
                        p[32].ParameterName = "dbhc_ten_huyen";
                        p[32].DbType = DbType.String;
                        p[32].Direction = ParameterDirection.Input;
                        p[32].Value = v_dbhc_ten_huyen;

                        p[33] = new OracleParameter();
                        p[33].ParameterName = "dbhc_ma_xa";
                        p[33].DbType = DbType.String;
                        p[33].Direction = ParameterDirection.Input;
                        p[33].Value = v_dbhc_ma_xa;

                        p[34] = new OracleParameter();
                        p[34].ParameterName = "dbhc_ten_xa";
                        p[34].DbType = DbType.String;
                        p[34].Direction = ParameterDirection.Input;
                        p[34].Value = v_dbhc_ten_xa;

                        p[35] = new OracleParameter();
                        p[35].ParameterName = "shkb";
                        p[35].DbType = DbType.String;
                        p[35].Direction = ParameterDirection.Input;
                        p[35].Value = v_shkb;

                        p[36] = new OracleParameter();
                        p[36].ParameterName = "ten_kb";
                        p[36].DbType = DbType.String;
                        p[36].Direction = ParameterDirection.Input;
                        p[36].Value = v_ten_kb;

                        p[37] = new OracleParameter();
                        p[37].ParameterName = "ma_pnn";
                        p[37].DbType = DbType.String;
                        p[37].Direction = ParameterDirection.Input;
                        p[37].Value = v_ma_pnn;

                        p[38] = new OracleParameter();
                        p[38].ParameterName = "chuong";
                        p[38].DbType = DbType.String;
                        p[38].Direction = ParameterDirection.Input;
                        p[38].Value = v_chuong;

                        p[39] = new OracleParameter();
                        p[39].ParameterName = "ma_muc";
                        p[39].DbType = DbType.String;
                        p[39].Direction = ParameterDirection.Input;
                        p[39].Value = v_ma_muc;

                        p[40] = new OracleParameter();
                        p[40].ParameterName = "tieu_muc";
                        p[40].DbType = DbType.String;
                        p[40].Direction = ParameterDirection.Input;
                        p[40].Value = v_tieu_muc;

                        p[41] = new OracleParameter();
                        p[41].ParameterName = "ten_tmuc";
                        p[41].DbType = DbType.String;
                        p[41].Direction = ParameterDirection.Input;
                        p[41].Value = v_ten_tmuc;

                        p[42] = new OracleParameter();
                        p[42].ParameterName = "kyhieu_gd";
                        p[42].DbType = DbType.String;
                        p[42].Direction = ParameterDirection.Input;
                        p[42].Value = v_kyhieu_gd;

                        p[43] = new OracleParameter();
                        p[43].ParameterName = "ten_gd";
                        p[43].DbType = DbType.String;
                        p[43].Direction = ParameterDirection.Input;
                        p[43].Value = v_ten_gd;

                        p[44] = new OracleParameter();
                        p[44].ParameterName = "tk_nsnn";
                        p[44].DbType = DbType.String;
                        p[44].Direction = ParameterDirection.Input;
                        p[44].Value = v_tk_nsnn;

                        p[45] = new OracleParameter();
                        p[45].ParameterName = "loai_tien";
                        p[45].DbType = DbType.String;
                        p[45].Direction = ParameterDirection.Input;
                        p[45].Value = v_loai_tien;

                        p[46] = new OracleParameter();
                        p[46].ParameterName = "ty_gia";
                        p[46].DbType = DbType.String;
                        p[46].Direction = ParameterDirection.Input;
                        p[46].Value = v_ty_gia;

                        p[47] = new OracleParameter();
                        p[47].ParameterName = "ma_loai_nvu";
                        p[47].DbType = DbType.String;
                        p[47].Direction = ParameterDirection.Input;
                        p[47].Value = v_ma_loai_nvu;

                        p[48] = new OracleParameter();
                        p[48].ParameterName = "ten_loai_nvu";
                        p[48].DbType = DbType.String;
                        p[48].Direction = ParameterDirection.Input;
                        p[48].Value = v_ten_loai_nvu;

                        p[49] = new OracleParameter();
                        p[49].ParameterName = "so_tien";
                        p[49].DbType = DbType.String;
                        p[49].Direction = ParameterDirection.Input;
                        p[49].Value = v_so_tien;

                        p[50] = new OracleParameter();
                        p[50].ParameterName = "so_danop_nhtm";
                        p[50].DbType = DbType.String;
                        p[50].Direction = ParameterDirection.Input;
                        p[50].Value = v_so_danop_nhtm;

                        p[51] = new OracleParameter();
                        p[51].ParameterName = "ngay_tcn_den";
                        p[51].DbType = DbType.Date;
                        p[51].Direction = ParameterDirection.Input;
                        if (sothue.NGAY_TCN_DEN != null && sothue.NGAY_TCN_DEN.Trim().Length > 0)
                        {
                            p[51].Value = DateTime.ParseExact(sothue.NGAY_TCN_DEN, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            p[51].Value = v_ngay_tcn_den;
                        }


                        p[52] = new OracleParameter();
                        p[52].ParameterName = "han_nop";
                        p[52].DbType = DbType.Date;
                        p[52].Direction = ParameterDirection.Input;
                        if (sothue.HAN_NOP != null && sothue.HAN_NOP.Trim().Length > 0)
                        {
                            p[52].Value = DateTime.ParseExact(sothue.HAN_NOP, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            p[52].Value = v_han_nop;
                        }

                        p[53] = new OracleParameter();
                        p[53].ParameterName = "loai_thue";
                        p[53].DbType = DbType.String;
                        p[53].Direction = ParameterDirection.Input;
                        p[53].Value = v_loai_thue;

                        p[54] = new OracleParameter();
                        p[54].ParameterName = "ten_loai_thue";
                        p[54].DbType = DbType.String;
                        p[54].Direction = ParameterDirection.Input;
                        p[54].Value = v_ten_loai_thue;

                        p[55] = new OracleParameter();
                        p[55].ParameterName = "ma_hs";
                        p[55].DbType = DbType.String;
                        p[55].Direction = ParameterDirection.Input;
                        p[55].Value = v_ma_hs;

                        p[56] = new OracleParameter();
                        p[56].ParameterName = "ma_gcn";
                        p[56].DbType = DbType.String;
                        p[56].Direction = ParameterDirection.Input;
                        p[56].Value = v_ma_gcn;

                        p[57] = new OracleParameter();
                        p[57].ParameterName = "so_thuadat";
                        p[57].DbType = DbType.String;
                        p[57].Direction = ParameterDirection.Input;
                        p[57].Value = v_so_thuadat;

                        p[58] = new OracleParameter();
                        p[58].ParameterName = "so_to_bando";
                        p[58].DbType = DbType.String;
                        p[58].Direction = ParameterDirection.Input;
                        p[58].Value = v_so_to_bando;

                        p[59] = new OracleParameter();
                        p[59].ParameterName = "so_giayto_hskt";
                        p[59].DbType = DbType.String;
                        p[59].Direction = ParameterDirection.Input;
                        p[59].Value = v_so_giayto_hskt;

                        p[60] = new OracleParameter();
                        p[60].ParameterName = "dia_chi_hskt";
                        p[60].DbType = DbType.String;
                        p[60].Direction = ParameterDirection.Input;
                        p[60].Value = v_dia_chi_hskt;

                        p[61] = new OracleParameter();
                        p[61].ParameterName = "ma_tinh_hskt";
                        p[61].DbType = DbType.String;
                        p[61].Direction = ParameterDirection.Input;
                        p[61].Value = v_ma_tinh_hskt;

                        p[62] = new OracleParameter();
                        p[62].ParameterName = "ten_tinh_hskt";
                        p[62].DbType = DbType.String;
                        p[62].Direction = ParameterDirection.Input;
                        p[62].Value = v_ten_tinh_hskt;

                        p[63] = new OracleParameter();
                        p[63].ParameterName = "ma_huyen_hskt";
                        p[63].DbType = DbType.String;
                        p[63].Direction = ParameterDirection.Input;
                        p[63].Value = v_ma_huyen_hskt;

                        p[64] = new OracleParameter();
                        p[64].ParameterName = "ten_huyen_hskt";
                        p[64].DbType = DbType.String;
                        p[64].Direction = ParameterDirection.Input;
                        p[64].Value = v_ten_huyen_hskt;

                        p[65] = new OracleParameter();
                        p[65].ParameterName = "ma_xa_hskt";
                        p[65].DbType = DbType.String;
                        p[65].Direction = ParameterDirection.Input;
                        p[65].Value = v_ma_xa_hskt;

                        p[66] = new OracleParameter();
                        p[66].ParameterName = "ten_xa_hskt";
                        p[66].DbType = DbType.String;
                        p[66].Direction = ParameterDirection.Input;
                        p[66].Value = v_ten_xa_hskt;

                        p[67] = new OracleParameter();
                        p[67].ParameterName = "ma_tinhchatkhoannop";
                        p[67].DbType = DbType.String;
                        p[67].Direction = ParameterDirection.Input;
                        p[67].Value = v_ma_tinhchatkhoannop;

                        p[68] = new OracleParameter();
                        p[68].ParameterName = "ten_tinhchatkhoannop";
                        p[68].DbType = DbType.String;
                        p[68].Direction = ParameterDirection.Input;
                        p[68].Value = v_ten_tinhchatkhoannop;

                        p[69] = new OracleParameter();
                        p[69].ParameterName = "ma_trangthai";
                        p[69].DbType = DbType.String;
                        p[69].Direction = ParameterDirection.Input;
                        p[69].Value = v_ma_trangthai;

                        p[70] = new OracleParameter();
                        p[70].ParameterName = "ten_trangthai";
                        p[70].DbType = DbType.String;
                        p[70].Direction = ParameterDirection.Input;
                        p[70].Value = v_ten_trangthai;

                        p[71] = new OracleParameter();
                        p[71].ParameterName = "so_khung";
                        p[71].DbType = DbType.String;
                        p[71].Direction = ParameterDirection.Input;
                        p[71].Value = v_so_khung;

                        p[72] = new OracleParameter();
                        p[72].ParameterName = "so_may";
                        p[72].DbType = DbType.String;
                        p[72].Direction = ParameterDirection.Input;
                        p[72].Value = v_so_may;

                        p[73] = new OracleParameter();
                        p[73].ParameterName = "dacdiem_ptien";
                        p[73].DbType = DbType.String;
                        p[73].Direction = ParameterDirection.Input;
                        p[73].Value = v_dacdiem_ptien;

                        p[74] = new OracleParameter();
                        p[74].ParameterName = "ngay_gia_han";
                        p[74].DbType = DbType.Date;
                        p[74].Direction = ParameterDirection.Input;
                        if (sothue.NGAY_GIA_HAN != null && sothue.NGAY_GIA_HAN.Trim().Length > 0)
                        {
                            p[74].Value = DateTime.ParseExact(sothue.NGAY_GIA_HAN, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            p[74].Value = v_ngay_gia_han;
                        }

                        p[75] = new OracleParameter();
                        p[75].ParameterName = "ma_tchieu_goc";
                        p[75].DbType = DbType.String;
                        p[75].Direction = ParameterDirection.Input;
                        p[75].Value = v_ma_tchieu_goc;

                        p[76] = new OracleParameter();
                        p[76].ParameterName = "ma_tchieu_ctu";
                        p[76].DbType = DbType.String;
                        p[76].Direction = ParameterDirection.Input;
                        p[76].Value = v_ma_tchieu_ctu;

                        DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }


                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }

        }


        public static void getDanhMuc(string strType, string strMa, string tran_code)
        {
            string strURL = "";
            strMsg_Id = TCS_GetSequenceValue();
            RequestDanhMuc request = new RequestDanhMuc();
            request.header = new Header(tran_code, strMsg_Id);

            request.data = new ReqDataDM();
            request.data.type = strType;
            request.data.ma = strMa;

            //ký số ở đây
            request.signature = new DataService.Signature();
            request.signature.value = "";
            request.signature.certificates = "";
            //send TCT ở đây
            string objDMJson = JsonUtils.objDanhMuc_ToJSON(request);
            string result = MessageRequesTCT(strMsg_Id, strURL, objDMJson, strType);

            if (result.Trim().Length > 0)
            {
                if (verifySignJson(result))
                {
                    try
                    {
                        ResponseDanhMuc objResponse = JsonConvert.DeserializeObject<ResponseDanhMuc>(result);

                        switch (strType)
                        {
                            case MessageType.strMSG009:
                                {
                                    //danh mục cơ quan thu
                                    capnhatdmCQThu(objResponse);
                                    break;
                                }
                            case MessageType.strMSG010:
                                {
                                    //danh mục địa bàn hành chính - đợi tài liệu chuẩn rồi làm tiếp
                                    capnhatdmDBHC(objResponse);
                                    break;
                                }
                            case MessageType.strMSG011:
                                {
                                    //danh mục kho bạc
                                    capnhatdmSHKB(objResponse);
                                    break;
                                }
                            case MessageType.strMSG012:
                                {
                                    // danh mục chương
                                    capnhatdmchuong(objResponse);
                                    break;
                                }
                            case MessageType.strMSG013:
                                {
                                    //danh mục khoản- đợi tài liệu chuẩn rồi làm tiếp
                                    capnhatdmKhoan(objResponse);
                                    break;
                                }
                            case MessageType.strMSG014:
                                {
                                    //danh mục tiểu mục
                                    capnhatdmTMuc(objResponse);
                                    break;
                                }
                            case MessageType.strMSG015:
                                {
                                    //danh mục tài khoản kho bạc- đợi tài liệu chuẩn rồi làm tiếp
                                    capnhatdmTKKB(objResponse);
                                    break;
                                }
                        }

                    }
                    catch (Exception ex)
                    {
                        log.Error("getDanhMuc: " + ex.Message + " \n  " + ex.StackTrace);
                    }
                }
                else
                {
                    throw new Exception("Sai chu ky");
                }

            }

        }


        private static void capnhatdmCQThu(ResponseDanhMuc msg009)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_dm_cqthu (ma_cqthu,");
            sbSqlInsert.Append("ID,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("dbhc_tinh,");
            sbSqlInsert.Append("dbhc_huyen,");
            sbSqlInsert.Append("ma_qlt,");
            sbSqlInsert.Append("ma_hq,");
            sbSqlInsert.Append("shkb,");
            sbSqlInsert.Append("ma_dthu,");
            sbSqlInsert.Append("ma_tinh,");
            sbSqlInsert.Append("ngay_capnhat)");
            sbSqlInsert.Append(" VALUES(:ma_cqthu,");
            sbSqlInsert.Append(":ID,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":dbhc_tinh,");
            sbSqlInsert.Append(":dbhc_huyen,");
            sbSqlInsert.Append(":ma_qlt,");
            sbSqlInsert.Append(":ma_hq,");
            sbSqlInsert.Append(":shkb,");
            sbSqlInsert.Append(":ma_dthu,");
            sbSqlInsert.Append(":ma_tinh,");
            sbSqlInsert.Append("SYSDATE)");
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg009.data == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }
                //Lay thong tin dang ky thue dua vao CSDL
                if (msg009.data.Count > 0)
                {
                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh
                    StringBuilder sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("DELETE FROM TCS_DM_CQTHU_BK");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("INSERT INTO TCS_DM_CQTHU_BK ( ma_cqthu, ten, ma_dthu, shkb, ma_cqthu_cu, ma_hq,ma_qlt, dbhc_tinh, dbhc_huyen, ma_tinh, trang_thai, id,ngay_bk)");
                    sbSqlDelete.Append(" SELECT ma_cqthu, ten, ma_dthu, shkb, ma_cqthu_cu, ma_hq,ma_qlt, dbhc_tinh, dbhc_huyen, ma_tinh, trang_thai, id, sysdate FROM TCS_DM_CQTHU WHERE MA_HQ IS NULL ");
                    p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("DELETE FROM TCS_DM_CQTHU WHERE MA_HQ IS NULL");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                    p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    //Gan cac thong tin
                    string vMa_cqthu = DBNull.Value.ToString();
                    string vMa_qlt = DBNull.Value.ToString();
                    string vTen_cqthu = DBNull.Value.ToString();
                    string vMaTinh = DBNull.Value.ToString();
                    string vMaHuyen = DBNull.Value.ToString();
                    string vMaHQ = DBNull.Value.ToString();
                    string vSHKB = DBNull.Value.ToString();
                    string vMaDT = DBNull.Value.ToString();
                    string vMaTinh_CITAD = DBNull.Value.ToString();
                    Int64 vID = 0;
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    Int64 vcch_id = 0;

                    foreach (ResponseDataDM danhmuc_ct in msg009.data)
                    {
                        if (danhmuc_ct.MA_CQTHU != null)
                        {
                            if (!danhmuc_ct.MA_CQTHU.Equals(string.Empty))
                            {
                                vMa_cqthu = danhmuc_ct.MA_CQTHU;
                            }
                        }
                        if (danhmuc_ct.CQT_QLT != null)
                        {
                            if (!danhmuc_ct.CQT_QLT.Equals(string.Empty))
                            {
                                vMa_qlt = danhmuc_ct.CQT_QLT;
                            }
                        }
                        if (danhmuc_ct.TEN_CQTHU != null)
                        {
                            if (!danhmuc_ct.TEN_CQTHU.Equals(string.Empty))
                            {
                                vTen_cqthu = danhmuc_ct.TEN_CQTHU;
                            }
                        }
                        if (danhmuc_ct.MA_TINH != null)
                        {
                            if (!danhmuc_ct.MA_TINH.Equals(string.Empty))
                            {
                                vMaTinh = danhmuc_ct.MA_TINH;
                                vMaTinh_CITAD = danhmuc_ct.MA_TINH.ToString().Substring(0, 2);
                            }
                        }
                        if (danhmuc_ct.MA_HUYEN != null)
                        {
                            if (!danhmuc_ct.MA_HUYEN.Equals(string.Empty))
                            {
                                vMaHuyen = danhmuc_ct.MA_HUYEN;
                            }
                        }
                        if (danhmuc_ct.SHKB != null)
                        {
                            if (!danhmuc_ct.SHKB.Equals(string.Empty))
                            {
                                vSHKB = danhmuc_ct.SHKB;
                                vMaDT = danhmuc_ct.SHKB;
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_TU != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_TU.Equals(string.Empty))
                            {
                                vNgayBD = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_TU);
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_DEN != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_DEN.Equals(string.Empty))
                            {
                                vNgayKT = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_DEN);
                            }
                        }
                        vID = TCS_GetSequence("tcs_cqthu_id_seq");


                        p = new IDbDataParameter[10];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "ma_cqthu";
                        p[0].DbType = DbType.Int64;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = vMa_cqthu;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ten";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = vTen_cqthu.Replace("'", ""); ;


                        p[2] = new OracleParameter();
                        p[2].ParameterName = "ID";
                        p[2].DbType = DbType.Int64;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = vID;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "dbhc_tinh";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = vMaTinh;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "dbhc_huyen";
                        p[4].DbType = DbType.String;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = vMaHuyen;

                        p[5] = new OracleParameter();
                        p[5].ParameterName = "ma_qlt";
                        p[5].DbType = DbType.String;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = vMa_qlt;

                        p[6] = new OracleParameter();
                        p[6].ParameterName = "ma_hq";
                        p[6].DbType = DbType.String;
                        p[6].Direction = ParameterDirection.Input;
                        p[6].Value = vMaHQ;

                        p[7] = new OracleParameter();
                        p[7].ParameterName = "shkb";
                        p[7].DbType = DbType.String;
                        p[7].Direction = ParameterDirection.Input;
                        p[7].Value = vSHKB;

                        p[8] = new OracleParameter();
                        p[8].ParameterName = "ma_dthu";
                        p[8].DbType = DbType.String;
                        p[8].Direction = ParameterDirection.Input;
                        p[8].Value = vMaDT;

                        p[9] = new OracleParameter();
                        p[9].ParameterName = "ma_tinh";
                        p[9].DbType = DbType.String;
                        p[9].Direction = ParameterDirection.Input;
                        p[9].Value = vMaTinh_CITAD;
                        DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {

                conn.Close();
                conn.Dispose();
            }

        }

        private static void capnhatdmDBHC(ResponseDanhMuc msg010)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_dm_xa (xa_id,");
            sbSqlInsert.Append("ma_xa,");
            sbSqlInsert.Append("ma_cha,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("tinh_trang,");
            sbSqlInsert.Append("ngay_capnhat)");
            sbSqlInsert.Append(" VALUES(:xa_id,");
            sbSqlInsert.Append(":ma_xa,");
            sbSqlInsert.Append(":ma_cha,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":tinh_trang,");
            sbSqlInsert.Append("SYSDATE)");

            string loi = "";

            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg010.data == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }
                //Lay thong tin dang ky thue dua vao CSDL
                if (msg010.data.Count > 0)
                {

                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh

                    StringBuilder sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM tcs_dm_xa_bk");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);
                    // insert vào bảng bk trước khi xóa
                    StringBuilder sbSqlIinsertBK = new StringBuilder("");
                    sbSqlIinsertBK.Append("INSERT INTO tcs_dm_xa_bk (xa_id, ma_xa,ma_cha,ten,tinh_trang) SELECT xa_id, ma_xa,ma_cha,ten,tinh_trang FROM tcs_dm_xa ");
                    // p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlIinsertBK.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM tcs_dm_xa");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                    // p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);
                    //Gan cac thong tin
                    Int64 vxa_id = 0;
                    string vma_xa = DBNull.Value.ToString();
                    string vten = DBNull.Value.ToString();
                    string vtinhtrang = "1";
                    string vma_cha = DBNull.Value.ToString();
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    Int64 vmtm_id = 0;

                    string strSQL = "";
                    int No = 0;
                    DataTable dt = null;
                    bool Unique = true;

                    foreach (ResponseDataDM danhmuc_ct in msg010.data)
                    {
                        //    ++No;
                        //    for (int i = No; i < msg010.data.Count(); i++)
                        //    {
                        //        if (danhmuc_ct.MA_DBHC.Equals(body_dm.Row[i].Danhmuc.MA_DBHC))
                        //        {
                        //            Unique = false;
                        //        }
                        //    }

                        //    strSQL = "SELECT xa_id, ma_xa,ma_cha FROM tcs_dm_xa where ma_xa ='" + danhmuc_ct.MA_DBHC + "'  ";
                        //    dt = DataAccess.ExecuteToTable(strSQL);

                        //    if (dt.Rows.Count == 0 && Unique)
                        //    {
                        //        if (danhmuc_ct.MA_DBHC != null)
                        //        {
                        //            if (!danhmuc_ct.MA_DBHC.Equals(string.Empty))
                        //            {
                        //                vma_xa = danhmuc_ct.MA_DBHC;
                        //            }
                        //        }

                        //        if (danhmuc_ct.TEN_DBHC != null)
                        //        {
                        //            if (!danhmuc_ct.TEN_DBHC.Equals(string.Empty))
                        //            {
                        //                vten = danhmuc_ct.TEN_DBHC;
                        //            }
                        //        }
                        //        if (danhmuc_ct.MA_CHA != null)
                        //        {
                        //            if (!danhmuc_ct.MA_CHA.Equals(string.Empty))
                        //            {
                        //                vma_cha = danhmuc_ct.MA_CHA;
                        //            }
                        //        }

                        //        if (danhmuc_ct.TRANG_THAI != null)
                        //        {
                        //            if (!danhmuc_ct.TRANG_THAI.Equals(string.Empty))
                        //            {
                        //                vtinhtrang = danhmuc_ct.TRANG_THAI;
                        //            }
                        //        }

                        //        vxa_id = TCS_GetSequence("tcs_xa_id_seq");


                        //        p = new IDbDataParameter[5];
                        //        p[0] = new OracleParameter();
                        //        p[0].ParameterName = "xa_id";
                        //        p[0].DbType = DbType.Int64;
                        //        p[0].Direction = ParameterDirection.Input;
                        //        p[0].Value = vxa_id;

                        //        p[1] = new OracleParameter();
                        //        p[1].ParameterName = "ma_xa";
                        //        p[1].DbType = DbType.String;
                        //        p[1].Direction = ParameterDirection.Input;
                        //        p[1].Value = vma_xa;

                        //        p[2] = new OracleParameter();
                        //        p[2].ParameterName = "ma_cha";
                        //        p[2].DbType = DbType.String;
                        //        p[2].Direction = ParameterDirection.Input;
                        //        p[2].Value = vma_cha;

                        //        p[3] = new OracleParameter();
                        //        p[3].ParameterName = "ten";
                        //        p[3].DbType = DbType.String;
                        //        p[3].Direction = ParameterDirection.Input;
                        //        p[3].Value = vten.Replace("'", "");

                        //        p[4] = new OracleParameter();
                        //        p[4].ParameterName = "tinh_trang";
                        //        p[4].DbType = DbType.Int64;
                        //        p[4].Direction = ParameterDirection.Input;
                        //        p[4].Value = vtinhtrang;

                        //        loi = vma_xa;
                        //        DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                        //    }
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace + loi);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {

                conn.Close();
                conn.Dispose();
            }

        }

        private static void capnhatdmSHKB(ResponseDanhMuc msg011)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            /* Formatted on 20-Dec-2014 15:25:59 (QP5 v5.126) */
            sbSqlInsert.Append("INSERT INTO tcs_dm_khobac (shkb,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("ma_tinh,");
            sbSqlInsert.Append("ma_huyen,");
            sbSqlInsert.Append("ma_xa,");
            sbSqlInsert.Append("tinh_trang,");
            sbSqlInsert.Append("auto_khoaso,");
            sbSqlInsert.Append("ma_db,");
            sbSqlInsert.Append("ngay_capnhat)");
            sbSqlInsert.Append(" VALUES(:shkb,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":ma_tinh,");
            sbSqlInsert.Append(":ma_huyen,");
            sbSqlInsert.Append(":ma_xa,");
            sbSqlInsert.Append(":tinh_trang,");
            sbSqlInsert.Append(":auto_khoaso,");
            sbSqlInsert.Append(":ma_db,");
            sbSqlInsert.Append("SYSDATE)");
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg011.data == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }

                //Lay thong tin dang ky thue dua vao CSDL
                if (msg011.data.Count > 0)
                {

                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh
                    StringBuilder sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM tcs_dm_khobac_bk");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("INSERT INTO tcs_dm_khobac_bk ( shkb, ten, ma_tinh, ma_huyen, ma_xa, tinh_trang,auto_khoaso,ma_db)");
                    sbSqlDelete.Append(" SELECT shkb, ten, ma_tinh, ma_huyen, ma_xa, tinh_trang,auto_khoaso,ma_db FROM tcs_dm_khobac ");
                    p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("DELETE FROM tcs_dm_khobac");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                    p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    //Gan cac thong tin
                    string vshkb = DBNull.Value.ToString();
                    string vten = DBNull.Value.ToString();
                    string vma_tinh = DBNull.Value.ToString();
                    string vma_huyen = DBNull.Value.ToString();
                    string vma_xa = DBNull.Value.ToString();
                    string vtinh_trang = "1";
                    string vauto_khoaso = "1";
                    string vma_db = DBNull.Value.ToString();
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    //Int64 vcch_id = 0;
                    foreach (ResponseDataDM danhmuc_ct in msg011.data)
                    {
                        if (danhmuc_ct.MA_KBAC != null)
                        {
                            if (!danhmuc_ct.MA_KBAC.Equals(string.Empty))
                            {
                                vshkb = danhmuc_ct.MA_KBAC;
                            }
                        }
                        if (danhmuc_ct.TEN_KBAC != null)
                        {
                            if (!danhmuc_ct.TEN_KBAC.Equals(string.Empty))
                            {
                                vten = danhmuc_ct.TEN_KBAC;
                            }
                        }
                        if (danhmuc_ct.MA_TINH != null)
                        {
                            if (!danhmuc_ct.MA_TINH.Equals(string.Empty))
                            {
                                vma_tinh = danhmuc_ct.MA_TINH;
                            }
                        }
                        if (danhmuc_ct.MA_HUYEN != null)
                        {
                            if (!danhmuc_ct.MA_HUYEN.Equals(string.Empty))
                            {
                                vma_huyen = danhmuc_ct.MA_HUYEN;
                                vma_db = danhmuc_ct.MA_HUYEN;
                            }
                        }
                        if (danhmuc_ct.MA_XA != null)
                        {
                            if (!danhmuc_ct.MA_XA.Equals(string.Empty))
                            {
                                vma_xa = danhmuc_ct.MA_XA;
                            }
                        }

                        if (danhmuc_ct.HIEU_LUC_TU != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_TU.Equals(string.Empty))
                            {
                                vNgayBD = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_TU);
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_DEN != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_DEN.Equals(string.Empty))
                            {
                                vNgayKT = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_DEN);
                            }
                        }
                        //   vcch_id = TCS_GetSequence("TCS_CCH_ID_SEQ");


                        p = new IDbDataParameter[8];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "shkb";
                        p[0].DbType = DbType.String;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = vshkb;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ten";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = vten.Replace("'", ""); ;

                        p[2] = new OracleParameter();
                        p[2].ParameterName = "ma_tinh";
                        p[2].DbType = DbType.String;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = vma_tinh;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "ma_huyen";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = vma_huyen;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "ma_xa";
                        p[4].DbType = DbType.String;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = vma_xa;

                        p[5] = new OracleParameter();
                        p[5].ParameterName = "tinh_trang";
                        p[5].DbType = DbType.String;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = vtinh_trang;

                        p[6] = new OracleParameter();
                        p[6].ParameterName = "auto_khoaso";
                        p[6].DbType = DbType.String;
                        p[6].Direction = ParameterDirection.Input;
                        p[6].Value = vauto_khoaso;

                        p[7] = new OracleParameter();
                        p[7].ParameterName = "ma_db";
                        p[7].DbType = DbType.String;
                        p[7].Direction = ParameterDirection.Input;
                        p[7].Value = vma_db;
                        DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {

                conn.Close();
                conn.Dispose();
            }

        }

        private static void capnhatdmchuong(ResponseDanhMuc msg012)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_dm_cap_chuong (cch_id,");
            sbSqlInsert.Append("ma_cap,");
            sbSqlInsert.Append("ma_chuong,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("ngay_bd,");
            sbSqlInsert.Append("ngay_kt,ngay_capnhat)");
            sbSqlInsert.Append(" VALUES(:cch_id,");
            sbSqlInsert.Append(":ma_cap,");
            sbSqlInsert.Append(":ma_chuong,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":ngay_bd,");
            sbSqlInsert.Append(":ngay_kt,");
            sbSqlInsert.Append("SYSDATE)");
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg012.data == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }

                //Lay thong tin dang ky thue dua vao CSDL
                if (msg012.data.Count > 0)
                {

                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh
                    StringBuilder sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("DELETE FROM TCS_DM_CAP_CHUONG_BK");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("INSERT INTO TCS_DM_CAP_CHUONG_BK ( cch_id, ma_cap, ma_chuong, ten, ngay_bd, ngay_kt,ghi_chu, tinh_trang,ngay_bk)");
                    sbSqlDelete.Append(" SELECT cch_id, ma_cap, ma_chuong, ten, ngay_bd, ngay_kt,ghi_chu, tinh_trang, sysdate FROM TCS_DM_CAP_CHUONG ");
                    p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM TCS_DM_CAP_CHUONG");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                    p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    //Gan cac thong tin
                    string vMacap = DBNull.Value.ToString();
                    string vMachuong = DBNull.Value.ToString();
                    string vTenchuong = DBNull.Value.ToString();
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    Int64 vcch_id = 0;

                    foreach (ResponseDataDM danhmuc_ct in msg012.data)
                    {

                        if (danhmuc_ct.MA_CAP != null)
                        {
                            if (!danhmuc_ct.MA_CAP.Equals(string.Empty))
                            {
                                vMacap = danhmuc_ct.MA_CAP;
                            }
                        }
                        if (danhmuc_ct.MA_CHUONG != null)
                        {
                            if (!danhmuc_ct.MA_CHUONG.Equals(string.Empty))
                            {
                                vMachuong = danhmuc_ct.MA_CHUONG;
                            }
                        }
                        if (danhmuc_ct.TEN_CHUONG != null)
                        {
                            if (!danhmuc_ct.TEN_CHUONG.Equals(string.Empty))
                            {
                                vTenchuong = danhmuc_ct.TEN_CHUONG;
                            }
                        }

                        if (danhmuc_ct.HIEU_LUC_TU != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_TU.Equals(string.Empty))
                            {
                                vNgayBD = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_TU);
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_DEN != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_DEN.Equals(string.Empty))
                            {
                                vNgayKT = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_DEN);
                            }
                        }
                        vcch_id = TCS_GetSequence("TCS_CCH_ID_SEQ");


                        p = new IDbDataParameter[6];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "cch_id";
                        p[0].DbType = DbType.Int64;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = vcch_id;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ma_cap";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = vMacap;

                        p[2] = new OracleParameter();
                        p[2].ParameterName = "ma_chuong";
                        p[2].DbType = DbType.String;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = vMachuong;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "ten";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = vTenchuong.Replace("'", ""); ;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "ngay_bd";
                        p[4].DbType = DbType.Int64;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = vNgayBD;

                        p[5] = new OracleParameter();
                        p[5].ParameterName = "ngay_kt";
                        p[5].DbType = DbType.Int64;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = vNgayKT;
                        DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {

                conn.Close();
                conn.Dispose();
            }

        }
        private static void capnhatdmKhoan(ResponseDanhMuc msg013)
        {

        }

        private static void capnhatdmTMuc(ResponseDanhMuc msg014)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_dm_muc_tmuc (mtm_id,");
            sbSqlInsert.Append("ma_muc,");
            sbSqlInsert.Append("ma_tmuc,");
            sbSqlInsert.Append("sac_thue,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("ngay_bd,");
            sbSqlInsert.Append("ngay_kt,");
            sbSqlInsert.Append("tinh_trang,NGAY_CAPNHAT)");
            sbSqlInsert.Append(" VALUES(:mtm_id,");
            sbSqlInsert.Append(":ma_muc,");
            sbSqlInsert.Append(":ma_tmuc,");
            sbSqlInsert.Append(":sac_thue,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":ngay_bd,");
            sbSqlInsert.Append(":ngay_kt,");
            sbSqlInsert.Append(":tinh_trang,");
            sbSqlInsert.Append("SYSDATE)");
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg014.data == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }

                //Lay thong tin dang ky thue dua vao CSDL
                if (msg014.data.Count > 0)
                {

                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh
                    StringBuilder sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("INSERT INTO tcs_dm_muc_tmuc_bk ( mtm_id, ma_muc, ma_tmuc,sac_thue, ten, ngay_bd, ngay_kt, ghi_chu, tinh_trang,ngay_BK)");
                    sbSqlDelete.Append(" SELECT mtm_id, ma_muc, ma_tmuc,sac_thue, ten, ngay_bd, ngay_kt, ghi_chu, tinh_trang, sysdate FROM tcs_dm_muc_tmuc");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM tcs_dm_muc_tmuc");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                    p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);
                    //Gan cac thong tin
                    string vma_muc = DBNull.Value.ToString();
                    string vma_tmuc = DBNull.Value.ToString();
                    string vten = DBNull.Value.ToString();
                    string vsac_thue = DBNull.Value.ToString();
                    string vtinhtrang = "1";
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    Int64 vmtm_id = 0;

                    foreach (ResponseDataDM danhmuc_ct in msg014.data)
                    {

                        if (danhmuc_ct.MA_MUC != null)
                        {
                            if (!danhmuc_ct.MA_MUC.Equals(string.Empty))
                            {
                                vma_muc = danhmuc_ct.MA_MUC;
                            }
                        }
                        if (danhmuc_ct.MA_TMUC != null)
                        {
                            if (!danhmuc_ct.MA_TMUC.Equals(string.Empty))
                            {
                                vma_tmuc = danhmuc_ct.MA_TMUC;
                            }
                        }

                        if (vma_tmuc.Length > 0)
                        {
                            vsac_thue = GetSacThue(vma_tmuc);
                        }

                        if (danhmuc_ct.TEN_TMUC != null)
                        {
                            if (!danhmuc_ct.TEN_TMUC.Equals(string.Empty))
                            {
                                vten = danhmuc_ct.TEN_TMUC;
                            }
                        }

                        if (danhmuc_ct.HIEU_LUC_TU != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_TU.Equals(string.Empty))
                            {
                                vNgayBD = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_TU);
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_DEN != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_DEN.Equals(string.Empty))
                            {
                                vNgayKT = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_DEN);
                            }
                        }
                        vmtm_id = TCS_GetSequence("TCS_MTM_ID_SEQ");


                        p = new IDbDataParameter[8];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "mtm_id";
                        p[0].DbType = DbType.Int64;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = vmtm_id;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ma_muc";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = vma_muc;

                        p[2] = new OracleParameter();
                        p[2].ParameterName = "ma_tmuc";
                        p[2].DbType = DbType.String;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = vma_tmuc;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "sac_thue";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = vsac_thue;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "ten";
                        p[4].DbType = DbType.String;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = vten.Replace("'", ""); ;

                        p[5] = new OracleParameter();
                        p[5].ParameterName = "ngay_bd";
                        p[5].DbType = DbType.Int64;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = vNgayBD;

                        p[6] = new OracleParameter();
                        p[6].ParameterName = "ngay_kt";
                        p[6].DbType = DbType.Int64;
                        p[6].Direction = ParameterDirection.Input;
                        p[6].Value = vNgayKT;

                        p[7] = new OracleParameter();
                        p[7].ParameterName = "tinh_trang";
                        p[7].DbType = DbType.String;
                        p[7].Direction = ParameterDirection.Input;
                        p[7].Value = vtinhtrang;
                        DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {

                conn.Close();
                conn.Dispose();
            }

        }

        private static void capnhatdmTKKB(ResponseDanhMuc msg015)
        {

        }

        public static string MessageRequesTCT(string strID, string vStrURL, string pv_strMessage, string msgType)
        {
            string strResponse = "";
            try
            {
                const SslProtocols _Tls12 = (SslProtocols)0xC00;
                const SecurityProtocolType Tls12 = (SecurityProtocolType)_Tls12;
                ServicePointManager.SecurityProtocol = Tls12;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                //insert log ở đây
                insertLog(pv_strMessage, msgType, "", strID);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(vStrURL);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(AcceptAllCertifications);
                // Request 
                using (Stream requestStream = request.GetRequestStream())
                {
                    using (StreamWriter requestWriter = new StreamWriter(requestStream))
                    {
                        requestWriter.Write(pv_strMessage);
                    }
                }
                string id = Guid.NewGuid().ToString();

                using (WebResponse myWebResponse = request.GetResponse())
                {
                    using (Stream myResponseStream = myWebResponse.GetResponseStream())
                    {
                        using (StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.UTF8))
                        {
                            strResponse = myStreamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("MessageRequesTCT: " + ex.Message + " \n  " + ex.StackTrace);
                strResponse = "";
            }
            //update log ở đây
            UpdateLog(strResponse, "", strID);
            return strResponse;
        }
        public static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static bool verifySignJson(string strSignData)
        {

            bool checkCK = true;

            return checkCK;
        }
        public static string SignJson(string strSignData)
        {
            return "";
        }
        private static string TCS_GetSequenceValue()
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT '" + strSender_Code + "' || TO_CHAR (SYSDATE, 'RRRRMMDD') || tcs_ref_id_tct.NEXTVAL");
                sbSQL.Append(" FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                return "SHB" + strRet;
            }
            catch (Exception ex)
            {
                return strSender_Code + "00";
            }
        }
        private static Int64 TCS_GetSequence(string p_Seq)
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT ");
                sbSQL.Append(p_Seq);
                sbSQL.Append(".nextval FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                return Int64.Parse(strRet);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        private static void insertLog(string strMsgOut, String msg_type, String desc, string msg_id)
        {
            OracleConnection conn = DataAccess.GetConnection();
            OracleCommand myCMD = new OracleCommand();
            try
            {

                StringBuilder sbSqlInsert = new StringBuilder("");

                String strS = "INSERT INTO tcs_log_msg_tct(id, msg_out, date_send, description, msg_type,msg_id) values (:id, :msg_out, :date_send, :description, :msg_type, :msg_id)";

                myCMD.Connection = conn;
                myCMD.CommandText = strS;
                myCMD.CommandType = CommandType.Text;
                myCMD.Parameters.Add("id", OracleType.Int16, 1).Value = 1;
                myCMD.Parameters.Add("msg_out", OracleType.Clob, 32767).Value = strMsgOut;
                myCMD.Parameters.Add("date_send", OracleType.DateTime).Value = DateTime.Now;
                myCMD.Parameters.Add("description", OracleType.VarChar).Value = desc;
                myCMD.Parameters.Add("msg_type", OracleType.NVarChar).Value = msg_type;
                myCMD.Parameters.Add("msg_id", OracleType.NVarChar).Value = msg_id;
                myCMD.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                log.Error("insertLog: " + ex.Message + "-" + ex.StackTrace);
            }
            finally
            {

                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    myCMD.Dispose();


                    conn.Close();
                    conn.Dispose();
                }
            }
        }
        private static void UpdateLog(String strMsgIn, String desc, string msg_id)
        {
            OracleConnection conn = DataAccess.GetConnection();
            OracleCommand myCMD = new OracleCommand();
            try
            {

                StringBuilder sbSqlInsert = new StringBuilder("");

                String strS = "UPDATE tcs_log_msg_tct SET msg_in =: msg_in, description =: description where id =: id";

                myCMD.Connection = conn;
                myCMD.CommandText = strS;
                myCMD.CommandType = CommandType.Text;
                myCMD.Parameters.Add("id", OracleType.Int16, 1).Value = 1;
                myCMD.Parameters.Add("msg_in", OracleType.Clob, 32767).Value = strMsgIn;
                myCMD.Parameters.Add("description", OracleType.VarChar).Value = desc;
                myCMD.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
            }
            finally
            {

                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    myCMD.Dispose();


                    conn.Close();
                    conn.Dispose();
                }
            }
        }
        private static long ConvertDateToNumber(string strDate)
        {
            //-----------------------------------------------------
            // Mục đích: Chuyển một string ngày tháng 'dd/MM/yyyy' --> dạng số 'yyyyMMdd'.
            // Tham số: string đầu vào
            // Giá trị trả về: 
            // Ngày viết: 18/10/2007
            // Người viết: Lê Hồng Hà
            // ----------------------------------------------------
            string[] arr = null;

            try
            {
                string sep = "/";
                strDate = strDate.Trim();
                if (strDate.Length < 10)
                    return 0;
                arr = strDate.Split(sep.ToCharArray());
                sep = "/";
                arr[0] = arr[0].PadLeft(2, '0');
                arr[1] = arr[1].PadLeft(2, '0');
                if (arr[2].Length == 2)
                {
                    arr[2] = "20" + arr[2];
                }

            }
            catch (Exception ex)
            {
                return Convert.ToInt64(DateTime.Now.ToString("yyyyMMdd"));
            }

            return long.Parse(arr[2] + arr[1] + arr[0]);
        }
        public static string GetSacThue(string strMA_TMUC)
        {
            string Result = "";
            try
            {
                string strSQL = "SELECT sac_thue FROM tcs_map_tm_st   where ma_ndkt ='" + strMA_TMUC + "'  ";
                DataTable dt = null;
                dt = DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = dt.Rows[0]["sac_thue"].ToString();
                }
                return Result;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return Result;
            }
        }
        //private static DateTime ConvertStringToDate(string dateime)
        //{
        //    if (dateime != null && dateime.Trim().Length > 0)
        //    {
        //        try
        //        {
        //            return DateTime.ParseExact(dateime, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //        }
        //        catch (Exception ex)
        //        {
        //            return null;
        //        }
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        private static Int32 ConvertInt(string so)
        {
            if (so != null && so.Trim().Length > 0)
            {
                return Int32.Parse(so);
            }
            else
            {
                return 0;
            }
        }
    }
}
