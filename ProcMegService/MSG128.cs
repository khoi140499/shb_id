﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ProcMegService.MSG.MSG128
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG128
    {
        public MSG128()
        {
        }
        [XmlElement("HEADER")]
        public HEADER Header;
        [XmlElement("BODY")]
        public BODY Body;
        [XmlElement("SECURITY")]
        public SECURITY Security;

        public string MSG128toXML(MSG128 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG128));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG128 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ProcMegService.MSG.MSG128.MSG128));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            ProcMegService.MSG.MSG128.MSG128 LoadedObjTmp = (ProcMegService.MSG.MSG128.MSG128)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW Row;
    }

    public class ROW
    {
        public ROW()
        { }
        public string TYPE { get; set; }
        public string NAME { get; set; }
        [XmlElement("TRUYVAN")]
        public TRUYVAN TruyVan;
    }

    public class TRUYVAN
    {
        public TRUYVAN()
        {
        }
        public string MST { get; set; }
        public string MA_CQ_THU { get; set; }
        public string LOAITHUE { get; set; }

    }


    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }

    }
    public class SECURITY
    {
        public SECURITY()
        {
        }
        [XmlElement("SIGNATURE")]
        public SIGNATURE Signature;
    }
    public class SIGNATURE
    {
        public SIGNATURE()
        { }
        public string SignatureValue { get; set; }
    }
}
