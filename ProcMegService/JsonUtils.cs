﻿using Newtonsoft.Json;
using ProcMegService.DataService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ProcMegService
{
    public class JsonUtils
    {
        public static string objDanhMuc_ToJSON(RequestDanhMuc obj)
        {
            StringBuilder sb = new StringBuilder();
            JsonWriter jw = new JsonTextWriter(new StringWriter(sb));
            jw.Formatting = Newtonsoft.Json.Formatting.Indented;
            var output = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            return output;
        }

        public static string objSoThue_ToJSON(TruyVanSoThueReq obj)
        {
            StringBuilder sb = new StringBuilder();
            JsonWriter jw = new JsonTextWriter(new StringWriter(sb));
            jw.Formatting = Newtonsoft.Json.Formatting.Indented;
            var output = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            return output;
        }

        public static string objMSG9008_ToJSON(MSG90008OUT obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG90008OUT));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, obj);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
}
