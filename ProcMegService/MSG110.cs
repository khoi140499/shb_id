﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ProcMegService.MSG.MSG110
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG110
    {
        public MSG110()
        {
        }
        [XmlElement("HEADER")]
        public HEADER Header;
        [XmlElement("BODY")]
        public BODY Body;
        [XmlElement("SECURITY")]
        public SECURITY Security;



        public MSG110 MSGToObject(string p_XML)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ProcMegService.MSG.MSG110.MSG110));
                XmlReader reader = XmlReader.Create(new StringReader(p_XML));
                ProcMegService.MSG.MSG110.MSG110 LoadedObjTmp = (ProcMegService.MSG.MSG110.MSG110)serializer.Deserialize(reader);
                return LoadedObjTmp;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string MSG110toXML(MSG110 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG110));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }
    public class BODY
    {
        public BODY()
        {
        }

        [XmlElement("ROW")]
        public ROW Row;
    }
    public class ROW
    {
        public ROW()
        { }
        public string TYPE { get; set; }
        public string NAME { get; set; }
        [XmlElement("SOTHUE")]
        public SOTHUE SoThue;
    }
    public class SOTHUE
    {
        public SOTHUE()
        { }
        [XmlElement("TRANGTHAI")]
        public string TRANGTHAI { get; set; }
        [XmlElement("MOTA_TRANGTHAI")]
        public string MOTA_TRANGTHAI { get; set; }
        [XmlElement("TONG_TIEN")]
        public List<TONG_TIEN> row_tongtien;
    }
    public class TONG_TIEN
    {
        public TONG_TIEN()
        { }
        [XmlElement("MA_SO_THUE")]
        public string MA_SO_THUE { get; set; }
        [XmlElement("TT_NNT")]
        public string TT_NNT { get; set; }
        [XmlElement("ROW_SOTHUE")]
        public List<ROW_SOTHUE> Row_Sothue;
    }
    public class ROW_SOTHUE
    {
        public ROW_SOTHUE()
        { }
        public string MA_HS { get; set; }
        public string MA_GCN { get; set; }
        public string SO_THUADAT { get; set; }
        public string SO_TO_BANDO { get; set; }
        public string DIA_CHI_TS { get; set; }
        public string MA_TINH_TS { get; set; }
        public string TEN_TINH_TS { get; set; }
        public string MA_HUYEN_TS { get; set; }
        public string TEN_HUYEN_TS { get; set; }
        public string MA_XA_TS { get; set; }
        public string TEN_XA_TS { get; set; }
        public string MA_DBHC { get; set; }
        public string MA_SO_THUE { get; set; }
        public string TEN_NNT { get; set; }
        public string SO { get; set; }
        public string MOTA_DIACHI { get; set; }
        public string MA_TINH { get; set; }
        public string TEN_TINH { get; set; }
        public string MA_HUYEN { get; set; }
        public string TEN_HUYEN { get; set; }
        public string MA_XA { get; set; }
        public string TEN_XA { get; set; }
        public string SO_QUYET_DINH { get; set; }
        public string NGAY_QUYET_DINH { get; set; }
        public string MA_CHUONG { get; set; }
        public string MA_TMUC { get; set; }
        public string TEN_TMUC { get; set; }
        public string LOAI_TK_NSNN { get; set; }
        public string MA_CQUAN_THU { get; set; }
        public string SO_CON_PHAI_NOP { get; set; }
        public string HAN_NOP { get; set; }
        public string SHKB { get; set; }
    }

    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }

    }
    public class SECURITY
    {
        public SECURITY()
        {
        }
        [XmlElement("SIGNATURE")]
        public SIGNATURE Signature;
    }
    public class SIGNATURE
    {
        public SIGNATURE()
        { }
        public string SignatureValue { get; set; }
    }
}
