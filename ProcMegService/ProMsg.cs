﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VBOracleLib;
using System.Diagnostics;
using System.Configuration;
using System.Data.OracleClient;
using System.Data;
using System.Collections;
using Signature;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Runtime.InteropServices;
using ProcMegService.MSG.MSG01;
using ProcMegService.MSG.MSG02;
using ProcMegService.MSG.MSG03;
using ProcMegService.MSG.MSG05;
using ProcMegService.MSG.MSG06;
using ProcMegService.MSG.MSG07;
using ProcMegService.MSG.MSG08;
using ProcMegService.MSG.MSG10;
using ProcMegService.MSG.MSG12;
using ProcMegService.MSG.MSG14;
using ProcMegService.MSG.MSG16;
using ProcMegService.MSG.MSG18;
using ProcMegService.MSG.MSG20;
using ProcMegService.MSG.MSG22;
using ProcMegService.MSG.MSG23;
using ProcMegService.MSG.MSG24;
using ProcMegService.MSG.MSG25;
using ProcMegService.MSG.MSG26;
using ProcMegService.MSG.MSG27;
using ProcMegService.MSG.MSG21;
using System.Security.Cryptography.Xml;
using ProcMegService.MSG.MSG32;
using ProcMegService.MSG.MSG33;
using ProcMegService.MSG.MSG21MOBI;
using ProcMegService.MSG.MSG21IB;
using System.Security.Cryptography;
using log4net;
using ProcMegService.MSG.MSG128;
using ProcMegService.MSG.MSG109;
namespace ProcMegService
{

    public class ProMsg
    {
        private static readonly  log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region khai bao
        private static String strConnection = ConfigurationManager.AppSettings.Get("CTC_DB_CONN").ToString();
        //private static String strConnection = VBOracleLib.Security.DescryptStr(strConnection1);
        private static String strXMLdefout1 = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
        private static String strXMLdefout3 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        private static String strXMLdefout2 = " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\"";
        private static String strXMLdefout4 = "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.cpandl.com\"";
        private static String strXMLdefin1 = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
        private static String strXMLdefin2 = "<DATA xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
        private static String strXMLdefout = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        private static String strOpenTag = "<DATA>";
        private static String strCloseTag = "</DATA>";
        private static String strMessage_Version = "1.0";
        private static String strSender_Code = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
        private static String strSender_Name = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
        private static String gipURL = ConfigurationManager.AppSettings.Get("GIP.URL").ToString();
        private static String strReciver_Code = "GIP";
        private static String strReciver_Name = "GIP";
        //  private static String strTran_Code = "00001";
        private static String strMsg_Id = "";
        private static String strMsgRef_Id = "";//Guid.NewGuid().ToString()
        private static String strId_Link = "";
        private static String strSend_Date = "";
        private static String strOriginal_Code = "";
        private static String strOriginal_Name = "";
        private static String strOriginal_Date = "";
        private static string strError_Code = "";
        private static string strSpare1 = "";
        private static string strSpare2 = "";
        private static string strSpare3 = "";
        private static string strCoQuanTQ = "TCT";
        private static string strformatdatetime = "dd-MMM-yyyy HH:mm:ss";
        public static String strSerialGIP1 = GetValue_THAMSOHT("SHA.1.GIP.SERIAL");
        public static String strSerialGIP2 = GetValue_THAMSOHT("SHA.2.GIP.SERIAL");

        #endregion

        /// <summary>
        /// Ham nhan du lieu MSG02 
        /// </summary>        
        /// <remarks>
        ///</remarks>
        #region MSG01
        public static void getMSG01(string soCMND)
        {
            strMsg_Id = TCS_GetSequenceValue();
            MSG01 ms1 = new MSG01();
            ms1.Body = new ProcMegService.MSG.MSG01.BODY();
            ms1.Header = new ProcMegService.MSG.MSG01.HEADER();
            ms1.Body.Row = new ProcMegService.MSG.MSG01.ROW();
            ms1.Body.Row.TruyVan = new ProcMegService.MSG.MSG01.TRUYVAN();
            //Gan du lieu header
            ms1.Header.VERSION = strMessage_Version;
            ms1.Header.SENDER_CODE = strSender_Code;
            ms1.Header.SENDER_NAME = strSender_Name;
            ms1.Header.RECEIVER_CODE = strReciver_Code;
            ms1.Header.RECEIVER_NAME = strReciver_Name;
            ms1.Header.TRAN_CODE = MessageType.strMSG01;
            ms1.Header.MSG_ID = strMsg_Id;
            ms1.Header.MSG_REFID = strMsgRef_Id;
            ms1.Header.ID_LINK = strId_Link;
            ms1.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
            ms1.Header.ORIGINAL_CODE = strOriginal_Code;
            ms1.Header.ORIGINAL_NAME = strOriginal_Name;
            ms1.Header.ORIGINAL_DATE = strOriginal_Date;
            ms1.Header.ERROR_CODE = strError_Code;
            ms1.Header.ERROR_DESC = "";
            ms1.Header.SPARE1 = strSpare1;
            ms1.Header.SPARE2 = strSpare2;
            ms1.Header.SPARE3 = strSpare3;
            //Gan du lieu body
            ms1.Body.Row.TYPE = MessageType.strMSG01;
            ms1.Body.Row.NAME = "Truy vấn theo số CMND";
            ms1.Body.Row.TruyVan.SO = soCMND;
            //Chuyen thanh XML
            string strXMLOut = ms1.MSG01toXML(ms1);
            //strXMLdefout4
            strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout4, String.Empty);
            //Ky tren msg
            strXMLOut = SignXML(strXMLOut);
            //Goi den Web Service
            string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG01);
            if (verifySignXML(strXMLIn))
            {
                MSG02 msg02 = new MSG02();
                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                MSG02 objTemp = msg02.MSGToObject(strXMLIn);
                //Verify sign                

                //Cap nhan CSDL
                capnhatDangkythue(MessageType.strMSG01, soCMND, objTemp);
            }
            else
            {
                throw new Exception("Sai chu ky");
            }
        }
        #endregion
        #region MSG03
        public static void getMSG03(string MST)
        {
            strMsg_Id = TCS_GetSequenceValue();
            MSG03 ms3 = new MSG03();
            ms3.Body = new ProcMegService.MSG.MSG03.BODY();
            ms3.Header = new ProcMegService.MSG.MSG03.HEADER();
            ms3.Body.Row = new ProcMegService.MSG.MSG03.ROW();
            ms3.Body.Row.TruyVan = new ProcMegService.MSG.MSG03.TRUYVAN();
            //Gan du lieu header
            ms3.Header.VERSION = strMessage_Version;
            ms3.Header.SENDER_CODE = strSender_Code;
            ms3.Header.SENDER_NAME = strSender_Name;
            ms3.Header.RECEIVER_CODE = strReciver_Code;
            ms3.Header.RECEIVER_NAME = strReciver_Name;
            ms3.Header.TRAN_CODE = MessageType.strMSG03;
            ms3.Header.MSG_ID = strMsg_Id;
            ms3.Header.MSG_REFID = strMsgRef_Id;
            ms3.Header.ID_LINK = strId_Link;
            ms3.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
            ms3.Header.ORIGINAL_CODE = strOriginal_Code;
            ms3.Header.ORIGINAL_NAME = strOriginal_Name;
            ms3.Header.ORIGINAL_DATE = strOriginal_Date;
            ms3.Header.ERROR_CODE = strError_Code;
            ms3.Header.ERROR_DESC = "";
            ms3.Header.SPARE1 = strSpare1;
            ms3.Header.SPARE2 = strSpare2;
            ms3.Header.SPARE3 = strSpare3;
            //Gan du lieu body
            ms3.Body.Row.TYPE = MessageType.strMSG03;
            ms3.Body.Row.NAME = "Truy vấn theo MST";
            ms3.Body.Row.TruyVan.MST = MST;
            //Chuyen thanh XML
            string strXMLOut = ms3.MSG03toXML(ms3);

            strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout4, String.Empty);
            //LogDebug.WriteLog("1 Truoc ky getMSG03", EventLogEntryType.Error);
            strXMLOut = SignXML(strXMLOut);
            //LogDebug.WriteLog("2 sau ky getMSG03", EventLogEntryType.Error);
            //Goi den Web Service
            string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG03);
            if (verifySignXML(strXMLIn))
            {
                MSG02 msg02 = new MSG02();
                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                MSG02 objTemp = msg02.MSGToObject(strXMLIn);
                //Cap nhan CSDL
                capnhatDangkythue(MessageType.strMSG03, MST, objTemp);
            }
            else
            {
                throw new Exception("Sai chu ky");
            }
        }
        #endregion
        #region MSG05
        public static void getMSG05(string MST)
        {
            try
            {
                strMsg_Id = TCS_GetSequenceValue();
                MSG05 ms5 = new MSG05();
                ms5.Body = new ProcMegService.MSG.MSG05.BODY();
                ms5.Header = new ProcMegService.MSG.MSG05.HEADER();
                ms5.Body.Row = new ProcMegService.MSG.MSG05.ROW();
                ms5.Body.Row.TruyVan = new ProcMegService.MSG.MSG05.TRUYVAN();
                //Gan du lieu header
                ms5.Header.VERSION = strMessage_Version;
                ms5.Header.SENDER_CODE = strSender_Code;
                ms5.Header.SENDER_NAME = strSender_Name;
                ms5.Header.RECEIVER_CODE = strReciver_Code;
                ms5.Header.RECEIVER_NAME = strReciver_Name;
                ms5.Header.TRAN_CODE = MessageType.strMSG05;
                ms5.Header.MSG_ID = strMsg_Id;
                ms5.Header.MSG_REFID = strMsgRef_Id;
                ms5.Header.ID_LINK = strId_Link;
                ms5.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                ms5.Header.ORIGINAL_CODE = strOriginal_Code;
                ms5.Header.ORIGINAL_NAME = strOriginal_Name;
                ms5.Header.ORIGINAL_DATE = strOriginal_Date;
                ms5.Header.ERROR_CODE = strError_Code;
                ms5.Header.ERROR_DESC = "";
                ms5.Header.SPARE1 = strSpare1;
                ms5.Header.SPARE2 = strSpare2;
                ms5.Header.SPARE3 = strSpare3;
                //Gan du lieu body
                ms5.Body.Row.TYPE = MessageType.strMSG05;
                ms5.Body.Row.NAME = "Truy vấn thu nộp theo MST";
                ms5.Body.Row.TruyVan.MST = MST;
                //Chuyen thanh XML
                string strXMLOut = ms5.MSG05toXML(ms5);

                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout4, String.Empty);
                //Ky tren msg
                strXMLOut = SignXML(strXMLOut);
                //Goi den Web Service
                string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG05);
                if (verifySignXML(strXMLIn))
                {
                    MSG06 msg06 = new MSG06();
                    strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                    MSG06 objTemp = msg06.MSGToObject(strXMLIn);
                    //Verify sign                

                    //Cap nhan CSDL
                    capnhatSothue(MST, objTemp);
                }
                else
                {
                    throw new Exception("Sai chu ky");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static void capnhatSothue(string pTIN, MSG06 msg06)
        {
            OracleConnection conn = default(OracleConnection);
            IDbTransaction transCT = null;

            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_sothue (ma_nnt,");
            sbSqlInsert.Append("ten_nnt,");
            sbSqlInsert.Append("dia_chi,");
            sbSqlInsert.Append("so_qd,");
            sbSqlInsert.Append("ngay_qd,");
            sbSqlInsert.Append("cq_qd,");
            sbSqlInsert.Append("ma_tinh,");
            sbSqlInsert.Append("ma_huyen,");
            sbSqlInsert.Append("ma_xa,");
            sbSqlInsert.Append("ma_cqthu,");
            sbSqlInsert.Append("kythue,");
            sbSqlInsert.Append("tk_thu_ns,");
            sbSqlInsert.Append("ma_cap,");
            sbSqlInsert.Append("ma_chuong,");
            sbSqlInsert.Append("ma_loai,");
            sbSqlInsert.Append("ma_khoan,");
            sbSqlInsert.Append("ma_muc,");
            sbSqlInsert.Append("ma_tmuc,");
            sbSqlInsert.Append("so_phainop,");
            sbSqlInsert.Append("tt_nop,");
            sbSqlInsert.Append("SO_CMT,");
            sbSqlInsert.Append("MA_QLT,");
            sbSqlInsert.Append("MA_NT,");
            sbSqlInsert.Append("TY_GIA,");
            sbSqlInsert.Append("MA_CHUONG_NNT,");
            sbSqlInsert.Append("lcn_owner,");
            sbSqlInsert.Append("loai_nnt)");
            sbSqlInsert.Append("  VALUES   (:ma_nnt,");
            sbSqlInsert.Append(":ten_nnt,");
            sbSqlInsert.Append(":dia_chi,");
            sbSqlInsert.Append(":so_qd,");
            sbSqlInsert.Append(":ngay_qd,");
            sbSqlInsert.Append(":cq_qd,");
            sbSqlInsert.Append(":ma_tinh,");
            sbSqlInsert.Append(":ma_huyen,");
            sbSqlInsert.Append(":ma_xa,");
            sbSqlInsert.Append(":ma_cqthu,");
            sbSqlInsert.Append(":kythue,");
            sbSqlInsert.Append(":tk_thu_ns,");
            sbSqlInsert.Append(":ma_cap,");
            sbSqlInsert.Append(":ma_chuong,");
            sbSqlInsert.Append(":ma_loai,");
            sbSqlInsert.Append(":ma_khoan,");
            sbSqlInsert.Append(":ma_muc,");
            sbSqlInsert.Append(":ma_tmuc,");
            sbSqlInsert.Append(":so_phainop,");
            sbSqlInsert.Append(":tt_nop,");
            sbSqlInsert.Append(":SO_CMT,");
            sbSqlInsert.Append(":MA_QLT,");
            sbSqlInsert.Append(":MA_NT,");
            sbSqlInsert.Append(":TY_GIA,");
            sbSqlInsert.Append(":MA_CHUONG_NNT,");
            sbSqlInsert.Append(":lcn_owner,");
            sbSqlInsert.Append(":loai_nnt)");
            try
            {
                conn =DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                // 
                if (msg06.Body.Row.Thongtin_NNT == null)
                {
                    return;
                }
                //Xoa du lieu truoc
                //Khai bao cac cau lenh
                StringBuilder sbSqlDelete = new StringBuilder("");
                sbSqlDelete.Append("DELETE FROM TCS_SOTHUE WHERE MA_NNT = :ma_nnt");

                IDbDataParameter[] p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "MA_NNT";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = pTIN;

               DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);


                //Lay thong tin so thue phai nop dua vao CSDL
                ProcMegService.MSG.MSG06.THONGTINCHUNG thongTinDangKyThue_arr = new ProcMegService.MSG.MSG06.THONGTINCHUNG();
                thongTinDangKyThue_arr = msg06.Body.Row.Thongtin_NNT.Thongtinchung;
                ProcMegService.MSG.MSG06.DIACHI thongTinDiaChi_arr = new ProcMegService.MSG.MSG06.DIACHI();
                thongTinDiaChi_arr = msg06.Body.Row.Thongtin_NNT.Diachi;
                //Insert vao bang So thue
                string vMaNnt = DBNull.Value.ToString();
                string vTenNnt = DBNull.Value.ToString();
                string vSoCMTNnt = DBNull.Value.ToString();
                string vMaCQQLT = DBNull.Value.ToString();
                string vChuongNNT = DBNull.Value.ToString();
                string vDiaChi = DBNull.Value.ToString();
                string vMaTinh = DBNull.Value.ToString();
                string vMaHuyen = DBNull.Value.ToString();
                string vMaXa = DBNull.Value.ToString();
                string vMaCqthu = DBNull.Value.ToString();
                //sonmt
                string vLoai_NNT = DBNull.Value.ToString();
                foreach (ProcMegService.MSG.MSG06.ROW_NNT thongTinDangKyThue in thongTinDangKyThue_arr.row_nnt)
                {
                    // ProcMegService.MSG.MSG06.ROW_NNT thongTinDangKyThue = thongTinDangKyThue_ct[i];
                    if (thongTinDangKyThue.MST != null)
                    {
                        if (!thongTinDangKyThue.MST.Equals(string.Empty))
                        {
                            vMaNnt = thongTinDangKyThue.MST;
                        }
                    }
                    if (thongTinDangKyThue.TEN_NNT != null)
                    {
                        if (!thongTinDangKyThue.TEN_NNT.Equals(string.Empty))
                        {
                            vTenNnt = thongTinDangKyThue.TEN_NNT;
                        }
                    }
                    //sonmt
                    if (thongTinDangKyThue.LOAI_NNT != null)
                    {
                        if (!thongTinDangKyThue.LOAI_NNT.Equals(string.Empty))
                        {
                            vLoai_NNT = thongTinDangKyThue.LOAI_NNT;
                        }
                    }
                    if (thongTinDangKyThue.SO != null)
                    {
                        if (!thongTinDangKyThue.SO.Equals(string.Empty))
                        {
                            vSoCMTNnt = thongTinDangKyThue.SO;
                        }
                    }

                    if (thongTinDangKyThue.MA_CQT_QL != null)
                    {
                        if (!thongTinDangKyThue.MA_CQT_QL.Equals(string.Empty))
                        {
                            vMaCQQLT = thongTinDangKyThue.MA_CQT_QL;
                            vMaCqthu = thongTinDangKyThue.MA_CQT_QL;
                        }
                    }

                    if (thongTinDangKyThue.CHUONG != null)
                    {
                        if (!thongTinDangKyThue.CHUONG.Equals(string.Empty))
                        {
                            vChuongNNT = thongTinDangKyThue.CHUONG;
                        }
                    }
                    break;
                }

                foreach (ProcMegService.MSG.MSG06.ROW_DIACHI thongTinDiaChi in thongTinDiaChi_arr.Row_Diachi)
                {

                    if (thongTinDiaChi.MOTA_DIACHI != null)
                    {
                        if (!thongTinDiaChi.MOTA_DIACHI.Equals(string.Empty))
                        {
                            vDiaChi = thongTinDiaChi.MOTA_DIACHI;
                        }
                    }

                    if (thongTinDiaChi.MA_TINH != null)
                    {
                        if (!thongTinDiaChi.MA_TINH.Equals(string.Empty))
                        {
                            vMaTinh = thongTinDiaChi.MA_TINH;
                        }
                    }

                    if (thongTinDiaChi.MA_HUYEN != null)
                    {
                        if (!thongTinDiaChi.MA_HUYEN.Equals(string.Empty))
                        {
                            vMaHuyen = thongTinDiaChi.MA_HUYEN;
                            vMaXa = thongTinDiaChi.MA_HUYEN;
                        }
                    }
                    //if (thongTinDiaChi.MA_XA != null)
                    //{
                    //    if (!thongTinDiaChi.MA_XA.Equals(string.Empty))
                    //    {
                    //        vMaXa = thongTinDiaChi.MA_XA;
                    //    }
                    //}
                    break;
                }



                foreach (ProcMegService.MSG.MSG06.ROW_SOTHUE itdata in msg06.Body.Row.Thongtin_NNT.Sothue.Row_Sothue)
                {
                    
                    if (itdata.MA_CQ_THU != null)
                    {
                        if (!itdata.MA_CQ_THU.Equals(string.Empty))
                        {
                            vMaCqthu = itdata.MA_CQ_THU;
                        }
                    }
                    string vSoQd = DBNull.Value.ToString();
                    if (itdata.SO_QDINH != null)
                    {
                        if (!itdata.SO_QDINH.Equals(string.Empty))
                        {
                            vSoQd = itdata.SO_QDINH;
                        }
                    }
                    DateTime vNgayQd = DateTime.MinValue;
                    if (itdata.NGAY_QDINH != null)
                    {
                        if (!itdata.NGAY_QDINH.Equals(string.Empty))
                        {
                            vNgayQd = DateTime.ParseExact(itdata.NGAY_QDINH, "dd/MM/yyyy", null);
                        }
                    }
                    string vKythue = DBNull.Value.ToString();
                    string vTkThuNs = DBNull.Value.ToString();
                    if (itdata.SO_TAI_KHOAN_CO != null)
                    {
                        if (!itdata.SO_TAI_KHOAN_CO.Equals(string.Empty))
                        {
                            vTkThuNs = itdata.SO_TAI_KHOAN_CO;
                        }
                    }
                    string vMaCap = DBNull.Value.ToString();

                    string vMaChuong = DBNull.Value.ToString();
                    if (itdata.MA_CHUONG != null)
                    {
                        if (!itdata.MA_CHUONG.Equals(string.Empty))
                        {
                            vMaChuong = itdata.MA_CHUONG;
                        }
                    }

                    //Fix ma loai va ma khoan theo
                    //Công văn số 193/TCT-KK ngày 14/1/2011 của Tổng cục Thuế v/v hướng dẫn thực hiện MLNSNN theo Thông tư số 198/2010/TT-BTC và sửa chỉ tiêu báo cáo kế toán thuế
                    string vMaLoai = "000";
                    string vMaKhoan = "000";
                    string vMaMuc = DBNull.Value.ToString();
                    string vMaTmuc = DBNull.Value.ToString();
                    if (itdata.MA_TMUC != null)
                    {
                        if (!itdata.MA_TMUC.Equals(string.Empty))
                        {
                            vMaTmuc = itdata.MA_TMUC;
                        }
                    }
                    decimal vSoPhainop = new decimal(-99999999);
                    string vnocuoiky = DBNull.Value.ToString();
                    if (itdata.NO_CUOI_KY != null)
                    {
                        if (!itdata.NO_CUOI_KY.Equals(string.Empty))
                        {
                            vnocuoiky = itdata.NO_CUOI_KY;
                            vSoPhainop = decimal.Parse(itdata.NO_CUOI_KY);
                        }
                    }
                    string vCqQd = DBNull.Value.ToString();
                    //if (itdata.MA_CQ_THU != null)
                    //{
                    //    if (!itdata.MA_CQ_THU.Equals(string.Empty))
                    //    {
                    //        vCqQd = itdata.MA_CQ_THU;
                    //    }
                    //}
                    string vloaitien = DBNull.Value.ToString();
                    if (itdata.LOAI_TIEN != null)
                    {
                        if (!itdata.LOAI_TIEN.Equals(string.Empty))
                        {
                            vloaitien = itdata.LOAI_TIEN;
                        }
                    }
                    string vtygia = DBNull.Value.ToString();
                    if (itdata.TI_GIA != null)
                    {
                        if (!itdata.TI_GIA.Equals(string.Empty))
                        {
                            vtygia = itdata.TI_GIA;
                        }
                    }
                    string vTtNop = DBNull.Value.ToString();
                    //If thongTinDangKyThue.TtNop <> Nothing Then
                    //    If Not thongTinDangKyThue.TtNop.Equals(String.Empty) Then
                    //        vTtNop = thongTinDangKyThue.TtNop
                    //    End If
                    //End If
                    string vLcnOwner = DBNull.Value.ToString();
                    //If thongTinDangKyThue.LcnOwner <> Nothing Then
                    //    If Not thongTinDangKyThue.LcnOwner.Equals(String.Empty) Then
                    //        vLcnOwner = thongTinDangKyThue.LcnOwner
                    //    End If
                    //End If


                    p = new IDbDataParameter[27];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "MA_NNT";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = vMaNnt;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "TEN_NNT";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = vTenNnt;

                    p[2] = new OracleParameter();
                    p[2].ParameterName = "DIA_CHI";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = vDiaChi;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "SO_QD";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = vSoQd;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "NGAY_QD";
                    p[4].DbType = DbType.DateTime;
                    p[4].Direction = ParameterDirection.Input;
                    if (vNgayQd.Equals(DateTime.MinValue))
                    {
                        p[4].Value = DBNull.Value;
                    }
                    else
                    {
                        p[4].Value = vNgayQd;
                    }

                    p[5] = new OracleParameter();
                    p[5].ParameterName = "CQ_QD";
                    p[5].DbType = DbType.String;
                    p[5].Direction = ParameterDirection.Input;
                    p[5].Value = vCqQd;

                    p[6] = new OracleParameter();
                    p[6].ParameterName = "MA_TINH";
                    p[6].DbType = DbType.String;
                    p[6].Direction = ParameterDirection.Input;
                    p[6].Value = vMaTinh;

                    p[7] = new OracleParameter();
                    p[7].ParameterName = "MA_HUYEN";
                    p[7].DbType = DbType.String;
                    p[7].Direction = ParameterDirection.Input;
                    p[7].Value = vMaHuyen;

                    p[8] = new OracleParameter();
                    p[8].ParameterName = "MA_XA";
                    p[8].DbType = DbType.String;
                    p[8].Direction = ParameterDirection.Input;
                    p[8].Value = vMaXa;

                    p[9] = new OracleParameter();
                    p[9].ParameterName = "MA_CQTHU";
                    p[9].DbType = DbType.String;
                    p[9].Direction = ParameterDirection.Input;
                    p[9].Value = vMaCqthu;

                    p[10] = new OracleParameter();
                    p[10].ParameterName = "KYTHUE";
                    p[10].DbType = DbType.String;
                    p[10].Direction = ParameterDirection.Input;
                    p[10].Value = vKythue;

                    p[11] = new OracleParameter();
                    p[11].ParameterName = "TK_THU_NS";
                    p[11].DbType = DbType.String;
                    p[11].Direction = ParameterDirection.Input;
                    p[11].Value = vTkThuNs;

                    p[12] = new OracleParameter();
                    p[12].ParameterName = "MA_CAP";
                    p[12].DbType = DbType.String;
                    p[12].Direction = ParameterDirection.Input;
                    p[12].Value = vMaCap;

                    p[13] = new OracleParameter();
                    p[13].ParameterName = "MA_CHUONG";
                    p[13].DbType = DbType.String;
                    p[13].Direction = ParameterDirection.Input;
                    p[13].Value = vMaChuong;

                    p[14] = new OracleParameter();
                    p[14].ParameterName = "MA_LOAI";
                    p[14].DbType = DbType.String;
                    p[14].Direction = ParameterDirection.Input;
                    p[14].Value = vMaLoai;

                    p[15] = new OracleParameter();
                    p[15].ParameterName = "MA_KHOAN";
                    p[15].DbType = DbType.String;
                    p[15].Direction = ParameterDirection.Input;
                    p[15].Value = vMaKhoan;

                    p[16] = new OracleParameter();
                    p[16].ParameterName = "MA_MUC";
                    p[16].DbType = DbType.String;
                    p[16].Direction = ParameterDirection.Input;
                    p[16].Value = vMaMuc;

                    p[17] = new OracleParameter();
                    p[17].ParameterName = "MA_TMUC";
                    p[17].DbType = DbType.String;
                    p[17].Direction = ParameterDirection.Input;
                    p[17].Value = vMaTmuc;

                    p[18] = new OracleParameter();
                    p[18].ParameterName = "SO_PHAINOP";
                    p[18].DbType = DbType.Decimal;
                    p[18].Direction = ParameterDirection.Input;
                    if (vSoPhainop.Equals(new decimal(-99999999)))
                    {
                        p[18].Value = DBNull.Value;
                    }
                    else
                    {
                        p[18].Value = vSoPhainop;
                    }

                    p[19] = new OracleParameter();
                    p[19].ParameterName = "TT_NOP";
                    p[19].DbType = DbType.String;
                    p[19].Direction = ParameterDirection.Input;
                    p[19].Value = vTtNop;

                    p[20] = new OracleParameter();
                    p[20].ParameterName = "SO_CMT";
                    p[20].DbType = DbType.String;
                    p[20].Direction = ParameterDirection.Input;
                    p[20].Value = vSoCMTNnt;

                    p[21] = new OracleParameter();
                    p[21].ParameterName = "MA_QLT";
                    p[21].DbType = DbType.String;
                    p[21].Direction = ParameterDirection.Input;
                    p[21].Value = vMaCQQLT;

                    p[22] = new OracleParameter();
                    p[22].ParameterName = "MA_NT";
                    p[22].DbType = DbType.String;
                    p[22].Direction = ParameterDirection.Input;
                    p[22].Value = vloaitien;

                    p[23] = new OracleParameter();
                    p[23].ParameterName = "TY_GIA";
                    p[23].DbType = DbType.String;
                    p[23].Direction = ParameterDirection.Input;
                    p[23].Value = vtygia;

                    p[24] = new OracleParameter();
                    p[24].ParameterName = "MA_CHUONG_NNT";
                    p[24].DbType = DbType.String;
                    p[24].Direction = ParameterDirection.Input;
                    p[24].Value = vChuongNNT;

                    p[25] = new OracleParameter();
                    p[25].ParameterName = "LCN_OWNER";
                    p[25].DbType = DbType.String;
                    p[25].Direction = ParameterDirection.Input;
                    p[25].Value = vLcnOwner;

                    p[26] = new OracleParameter();
                    p[26].ParameterName = "loai_nnt";
                    p[26].DbType = DbType.String;
                    p[26].Direction = ParameterDirection.Input;
                    p[26].Value = vLoai_NNT;
                   DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                }



                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }

        }
        private static void capnhatDangkythue(string MSGTYPE, string pTIN, MSG02 msg02)
        {
            OracleConnection conn = default(OracleConnection);
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            //Insert vao bang dang ky thue
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_dm_nnt (ma_nnt,");
            sbSqlInsert.Append("ten_nnt,");
            sbSqlInsert.Append("dia_chi,");
            sbSqlInsert.Append("ma_tinh,");
            sbSqlInsert.Append("ma_huyen,");
            sbSqlInsert.Append("ma_xa,");
            sbSqlInsert.Append("ma_cqthu,");
            sbSqlInsert.Append("ma_chuong,");
            sbSqlInsert.Append("trang_thai,");
            sbSqlInsert.Append("ma_loai_nnt,");
            sbSqlInsert.Append("so_cmt,");
            sbSqlInsert.Append("ngay_cap_mst)");
            sbSqlInsert.Append("VALUES   (:ma_nnt,");
            sbSqlInsert.Append(":ten_nnt,");
            sbSqlInsert.Append(":dia_chi,");
            sbSqlInsert.Append(":ma_tinh,");
            sbSqlInsert.Append(":ma_huyen,");
            sbSqlInsert.Append(":ma_xa,");
            sbSqlInsert.Append(":ma_cqthu,");
            sbSqlInsert.Append(":ma_chuong,");
            sbSqlInsert.Append(":trang_thai,");
            sbSqlInsert.Append(":ma_loai_nnt,");
            sbSqlInsert.Append(":so_cmt,");
            sbSqlInsert.Append(":ngay_cap_mst)");
            try
            {
                conn =DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg02.Body.Row.Thongtin_NNT == null)
                {
                    return;
                }
                ProcMegService.MSG.MSG02.THONGTINCHUNG thongTinDangKyThue_arr = new ProcMegService.MSG.MSG02.THONGTINCHUNG();
                thongTinDangKyThue_arr = msg02.Body.Row.Thongtin_NNT.Thongtinchung;
                ProcMegService.MSG.MSG02.DIACHI thongTinDiaChi_arr = new ProcMegService.MSG.MSG02.DIACHI();
                thongTinDiaChi_arr = msg02.Body.Row.Thongtin_NNT.Diachi;
                //Lay thong tin dang ky thue dua vao CSDL
                if ((msg02.Body.Row.Thongtin_NNT.Thongtinchung != null))
                {
                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh
                    StringBuilder sbSqlDelete = new StringBuilder("");
                    if (MSGTYPE == MessageType.strMSG01)
                    {
                        sbSqlDelete.Append("DELETE FROM TCS_DM_NNT WHERE SO_CMT = :TIN");
                    }
                    else
                    {
                        sbSqlDelete.Append("DELETE FROM TCS_DM_NNT WHERE MA_NNT = :TIN");
                    }

                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                    IDbDataParameter[] p = new IDbDataParameter[1];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "TIN";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = pTIN;

                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);
                    //Gan cac thong tin
                    string vMaNnt = DBNull.Value.ToString();
                    string vTenNnt = DBNull.Value.ToString();
                    string vSoCMTNnt = DBNull.Value.ToString();
                    string vMaCQQLT = DBNull.Value.ToString();
                    string vMaChuong = DBNull.Value.ToString();
                    string vMaloai_nnt = DBNull.Value.ToString();
                    string vtrangthai = DBNull.Value.ToString();
                    string vngaycap = DBNull.Value.ToString();
                    string vDiaChi = DBNull.Value.ToString();
                    string vMaTinh = DBNull.Value.ToString();
                    string vMaHuyen = DBNull.Value.ToString();
                    string vMaXa = DBNull.Value.ToString();
                    foreach (ProcMegService.MSG.MSG02.ROW_NNT thongTinDangKyThue in thongTinDangKyThue_arr.row_nnt)
                    {
                        // ProcMegService.MSG.MSG06.ROW_NNT thongTinDangKyThue = thongTinDangKyThue_ct[i];
                        if (thongTinDangKyThue.MST != null)
                        {
                            if (!thongTinDangKyThue.MST.Equals(string.Empty))
                            {
                                vMaNnt = thongTinDangKyThue.MST;
                            }
                        }
                        if (thongTinDangKyThue.TEN_NNT != null)
                        {
                            if (!thongTinDangKyThue.TEN_NNT.Equals(string.Empty))
                            {
                                vTenNnt = thongTinDangKyThue.TEN_NNT;
                            }
                        }

                        if (thongTinDangKyThue.SO != null)
                        {
                            if (!thongTinDangKyThue.SO.Equals(string.Empty))
                            {
                                vSoCMTNnt = thongTinDangKyThue.SO;
                            }
                        }

                        if (thongTinDangKyThue.CQT_QL != null)
                        {
                            if (!thongTinDangKyThue.CQT_QL.Equals(string.Empty))
                            {
                                vMaCQQLT = thongTinDangKyThue.CQT_QL;
                            }
                        }

                        if (thongTinDangKyThue.CHUONG != null)
                        {
                            if (!thongTinDangKyThue.CHUONG.Equals(string.Empty))
                            {
                                vMaChuong = thongTinDangKyThue.CHUONG;
                            }
                        }
                        if (thongTinDangKyThue.LOAI_NNT != null)
                        {
                            if (!thongTinDangKyThue.LOAI_NNT.Equals(string.Empty))
                            {
                                vMaloai_nnt = thongTinDangKyThue.LOAI_NNT;
                            }
                        }
                        if (thongTinDangKyThue.TRANG_THAI != null)
                        {
                            if (!thongTinDangKyThue.TRANG_THAI.Equals(string.Empty))
                            {
                                vtrangthai = thongTinDangKyThue.TRANG_THAI;
                            }
                        }
                        if (thongTinDangKyThue.NGAYCAP_MST != null)
                        {
                            if (!thongTinDangKyThue.NGAYCAP_MST.Equals(string.Empty))
                            {
                                vngaycap = thongTinDangKyThue.NGAYCAP_MST;
                            }
                        }
                        break;
                    }

                    foreach (ProcMegService.MSG.MSG02.ROW_DIACHI thongTinDiaChi in thongTinDiaChi_arr.Row_Diachi)
                    {

                        if (thongTinDiaChi.MOTA_DIACHI != null)
                        {
                            if (!thongTinDiaChi.MOTA_DIACHI.Equals(string.Empty))
                            {
                                vDiaChi = thongTinDiaChi.MOTA_DIACHI;
                            }
                        }

                        if (thongTinDiaChi.MA_TINH != null)
                        {
                            if (!thongTinDiaChi.MA_TINH.Equals(string.Empty))
                            {
                                vMaTinh = thongTinDiaChi.MA_TINH;
                            }
                        }

                        if (thongTinDiaChi.MA_HUYEN != null)
                        {
                            if (!thongTinDiaChi.MA_HUYEN.Equals(string.Empty))
                            {
                                vMaHuyen = thongTinDiaChi.MA_HUYEN;
                                vMaXa = thongTinDiaChi.MA_HUYEN;
                            }
                        }
                        //if (thongTinDiaChi.MA_XA != null)
                        //{
                        //    if (!thongTinDiaChi.MA_XA.Equals(string.Empty))
                        //    {
                        //        vMaXa = thongTinDiaChi.MA_XA;
                        //    }
                        //}
                        break;
                    }


                    p = new IDbDataParameter[12];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "MA_NNT";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = vMaNnt;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "TEN_NNT";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = vTenNnt;

                    p[2] = new OracleParameter();
                    p[2].ParameterName = "DIA_CHI";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = vDiaChi;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "MA_TINH";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = vMaTinh;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "MA_HUYEN";
                    p[4].DbType = DbType.String;
                    p[4].Direction = ParameterDirection.Input;
                    p[4].Value = vMaHuyen;

                    p[5] = new OracleParameter();
                    p[5].ParameterName = "MA_XA";
                    p[5].DbType = DbType.String;
                    p[5].Direction = ParameterDirection.Input;
                    p[5].Value = vMaXa;

                    p[6] = new OracleParameter();
                    p[6].ParameterName = "MA_CQTHU";
                    p[6].DbType = DbType.String;
                    p[6].Direction = ParameterDirection.Input;
                    p[6].Value = vMaCQQLT;

                    p[7] = new OracleParameter();
                    p[7].ParameterName = "MA_CHUONG";
                    p[7].DbType = DbType.String;
                    p[7].Direction = ParameterDirection.Input;
                    p[7].Value = vMaChuong;

                    p[8] = new OracleParameter();
                    p[8].ParameterName = "TRANG_THAI";
                    p[8].DbType = DbType.String;
                    p[8].Direction = ParameterDirection.Input;
                    p[8].Value = vtrangthai;

                    p[9] = new OracleParameter();
                    p[9].ParameterName = "MA_LOAI_NNT";
                    p[9].DbType = DbType.String;
                    p[9].Direction = ParameterDirection.Input;
                    p[9].Value = vMaloai_nnt;

                    p[10] = new OracleParameter();
                    p[10].ParameterName = "SO_CMT";
                    p[10].DbType = DbType.String;
                    p[10].Direction = ParameterDirection.Input;
                    p[10].Value = vSoCMTNnt;

                    p[11] = new OracleParameter();
                    p[11].ParameterName = "NGAY_CAP_MST";
                    p[11].DbType = DbType.String;
                    p[11].Direction = ParameterDirection.Input;
                    p[11].Value = vngaycap;
                    DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);

                }

                transCT.Commit();
            }
            catch (Exception ex)
            {
                transCT.Rollback();
                // throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
                
            }

        }
        #endregion
        #region Đồng bộ danh mục




        public static void getMSG07(string strMSGCode, string strMSGName)
        {
            strMsg_Id = TCS_GetSequenceValue();
            MSG07 ms7 = new MSG07();
            ms7.Body = new ProcMegService.MSG.MSG07.BODY();
            ms7.Header = new ProcMegService.MSG.MSG07.HEADER();
            ms7.Body.Row = new ProcMegService.MSG.MSG07.ROW();
            //Gan du lieu header
            ms7.Header.VERSION = strMessage_Version;
            ms7.Header.SENDER_CODE = strSender_Code;
            ms7.Header.SENDER_NAME = strSender_Name;
            ms7.Header.RECEIVER_CODE = strReciver_Code;
            ms7.Header.RECEIVER_NAME = strReciver_Name;
            ms7.Header.TRAN_CODE = strMSGCode;
            ms7.Header.MSG_ID = strMsg_Id;
            ms7.Header.MSG_REFID = strMsgRef_Id;
            ms7.Header.ID_LINK = strId_Link;
            ms7.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
            ms7.Header.ORIGINAL_CODE = strOriginal_Code;
            ms7.Header.ORIGINAL_NAME = strOriginal_Name;
            ms7.Header.ORIGINAL_DATE = strOriginal_Date;
            ms7.Header.ERROR_CODE = strError_Code;
            ms7.Header.ERROR_DESC = "";
            ms7.Header.SPARE1 = strSpare1;
            ms7.Header.SPARE2 = strSpare2;
            ms7.Header.SPARE3 = strSpare3;
            //Gan du lieu body
            ms7.Body.Row.TYPE = strMSGCode;
            ms7.Body.Row.NAME = strMSGName;

            //Chuyen thanh XML
            string strXMLOut = ms7.MSG07toXML(ms7);

            strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout4, String.Empty);
            //Ky tren msg
            strXMLOut = SignXML(strXMLOut);
            //Goi den Web Service
            String strXMLIn = sendMsg(strXMLOut, strMsg_Id, strMSGCode);
            if (strXMLIn.Equals(String.Empty))
            {
                throw new Exception("Không lấy được dữ liệu từ thuế");
            }
            if (strMSGCode.Equals( MessageType.strMSG11))
            {
                MSG12 msg12 = new MSG12();
                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                MSG12 objTemp = msg12.MSGToObject(strXMLIn);
                capnhatdmDBHC(objTemp);
            }
            else
            {

                if (verifySignXML(strXMLIn))
                {

                    switch (strMSGCode)
                    {
                        case MessageType.strMSG07:
                            {
                                MSG08 msg08 = new MSG08();
                                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                                MSG08 objTemp = msg08.MSGToObject(strXMLIn);
                                capnhatdmchuong(objTemp);
                                break;
                            }
                        case MessageType.strMSG09:
                            {
                                MSG10 msg10 = new MSG10();
                                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                                MSG10 objTemp = msg10.MSGToObject(strXMLIn);
                                capnhatdmCQThu(objTemp);
                                break;
                            }
                        case MessageType.strMSG15:
                            {
                                MSG16 msg16 = new MSG16();
                                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                                MSG16 objTemp = msg16.MSGToObject(strXMLIn);
                                capnhatdmSHKB(objTemp);
                                break;
                            }
                        case MessageType.strMSG19:
                            {
                                MSG20 msg20 = new MSG20();
                                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                                MSG20 objTemp = msg20.MSGToObject(strXMLIn);
                                capnhatdmTMuc(objTemp);
                                break;
                            }
                    }

                }
                else
                {
                    throw new Exception("Sai chu ky");
                }
            }
        }
        private static void capnhatdmDBHC(MSG12 msg12)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_dm_xa (xa_id,");
            sbSqlInsert.Append("ma_xa,");
            sbSqlInsert.Append("ma_cha,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("tinh_trang,");
            sbSqlInsert.Append("ngay_capnhat)");
            sbSqlInsert.Append(" VALUES(:xa_id,");
            sbSqlInsert.Append(":ma_xa,");
            sbSqlInsert.Append(":ma_cha,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":tinh_trang,");
            sbSqlInsert.Append("SYSDATE)");

            string loi ="";

            try
            {
                conn =DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg12.Body.Row == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }
                ProcMegService.MSG.MSG12.BODY body_dm = new ProcMegService.MSG.MSG12.BODY();
                body_dm = msg12.Body;
                ProcMegService.MSG.MSG12.DANHMUC danhmuc_ct = new ProcMegService.MSG.MSG12.DANHMUC();
                //Lay thong tin dang ky thue dua vao CSDL
                if ((msg12.Body.Row != null))
                {

                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh

                    StringBuilder sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM tcs_dm_xa_bk");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                   DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);
                    // insert vào bảng bk trước khi xóa
                    StringBuilder sbSqlIinsertBK = new StringBuilder("");
                    sbSqlIinsertBK.Append("INSERT INTO tcs_dm_xa_bk (xa_id, ma_xa,ma_cha,ten,tinh_trang) SELECT xa_id, ma_xa,ma_cha,ten,tinh_trang FROM tcs_dm_xa ");
                    // p = new IDbDataParameter[0];
                   DataAccess.ExecuteNonQuery(sbSqlIinsertBK.ToString(), CommandType.Text, p, transCT);

                     sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM tcs_dm_xa");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                    // p = new IDbDataParameter[0];
                   DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);
                    //Gan cac thong tin
                    Int64 vxa_id = 0;
                    string vma_xa = DBNull.Value.ToString();
                    string vten = DBNull.Value.ToString();
                    string vtinhtrang = "1";
                    string vma_cha = DBNull.Value.ToString();
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    Int64 vmtm_id = 0;

                    string strSQL = "";
                    int No = 0;
                    DataTable dt = null;
                    bool Unique = true;

                    foreach (ProcMegService.MSG.MSG12.ROW row_dm in body_dm.Row)
                    {
                        ++No;
                        danhmuc_ct = row_dm.Danhmuc;

                        for (int i = No; i < body_dm.Row.Count(); i++)
                        {
                            if (danhmuc_ct.MA_DBHC.Equals(body_dm.Row[i].Danhmuc.MA_DBHC))
                            {
                                Unique = false;
                            }
                        }

                        strSQL = "SELECT xa_id, ma_xa,ma_cha FROM tcs_dm_xa where ma_xa ='" + danhmuc_ct.MA_DBHC + "'  ";
                        dt = DataAccess.ExecuteToTable(strSQL);

                        if (dt.Rows.Count == 0 && Unique)
                        {
                            if (danhmuc_ct.MA_DBHC != null)
                            {
                                if (!danhmuc_ct.MA_DBHC.Equals(string.Empty))
                                {
                                    vma_xa = danhmuc_ct.MA_DBHC;
                                }
                            }

                            if (danhmuc_ct.TEN_DBHC != null)
                            {
                                if (!danhmuc_ct.TEN_DBHC.Equals(string.Empty))
                                {
                                    vten = danhmuc_ct.TEN_DBHC;
                                }
                            }
                            if (danhmuc_ct.MA_CHA != null)
                            {
                                if (!danhmuc_ct.MA_CHA.Equals(string.Empty))
                                {
                                    vma_cha = danhmuc_ct.MA_CHA;
                                }
                            }

                            if (danhmuc_ct.TRANG_THAI != null)
                            {
                                if (!danhmuc_ct.TRANG_THAI.Equals(string.Empty))
                                {
                                    vtinhtrang = danhmuc_ct.TRANG_THAI;
                                }
                            }

                            vxa_id = TCS_GetSequence("tcs_xa_id_seq");


                            p = new IDbDataParameter[5];
                            p[0] = new OracleParameter();
                            p[0].ParameterName = "xa_id";
                            p[0].DbType = DbType.Int64;
                            p[0].Direction = ParameterDirection.Input;
                            p[0].Value = vxa_id;

                            p[1] = new OracleParameter();
                            p[1].ParameterName = "ma_xa";
                            p[1].DbType = DbType.String;
                            p[1].Direction = ParameterDirection.Input;
                            p[1].Value = vma_xa;

                            p[2] = new OracleParameter();
                            p[2].ParameterName = "ma_cha";
                            p[2].DbType = DbType.String;
                            p[2].Direction = ParameterDirection.Input;
                            p[2].Value = vma_cha;

                            p[3] = new OracleParameter();
                            p[3].ParameterName = "ten";
                            p[3].DbType = DbType.String;
                            p[3].Direction = ParameterDirection.Input;
                            p[3].Value = vten.Replace("'", "");

                            p[4] = new OracleParameter();
                            p[4].ParameterName = "tinh_trang";
                            p[4].DbType = DbType.Int64;
                            p[4].Direction = ParameterDirection.Input;
                            p[4].Value = vtinhtrang;

                            loi = vma_xa;
                            DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                        }                     
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace + loi);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
               
                conn.Close();
                conn.Dispose();
            }

        }
        private static void capnhatdmTMuc(MSG20 msg20)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_dm_muc_tmuc (mtm_id,");
            sbSqlInsert.Append("ma_muc,");
            sbSqlInsert.Append("ma_tmuc,");
            sbSqlInsert.Append("sac_thue,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("ngay_bd,");
            sbSqlInsert.Append("ngay_kt,");
            sbSqlInsert.Append("tinh_trang,NGAY_CAPNHAT)");
            sbSqlInsert.Append(" VALUES(:mtm_id,");
            sbSqlInsert.Append(":ma_muc,");
            sbSqlInsert.Append(":ma_tmuc,");
            sbSqlInsert.Append(":sac_thue,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":ngay_bd,");
            sbSqlInsert.Append(":ngay_kt,");
            sbSqlInsert.Append(":tinh_trang,");
            sbSqlInsert.Append("SYSDATE)");
            try
            {
                conn =DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg20.Body.Row == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }
                ProcMegService.MSG.MSG20.BODY body_dm = new ProcMegService.MSG.MSG20.BODY();
                body_dm = msg20.Body;
                ProcMegService.MSG.MSG20.DANHMUC danhmuc_ct = new ProcMegService.MSG.MSG20.DANHMUC();
                //Lay thong tin dang ky thue dua vao CSDL
                if ((msg20.Body.Row != null))
                {

                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh
                    StringBuilder sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("INSERT INTO tcs_dm_muc_tmuc_bk ( mtm_id, ma_muc, ma_tmuc,sac_thue, ten, ngay_bd, ngay_kt, ghi_chu, tinh_trang,ngay_BK)");
                    sbSqlDelete.Append(" SELECT mtm_id, ma_muc, ma_tmuc,sac_thue, ten, ngay_bd, ngay_kt, ghi_chu, tinh_trang, sysdate FROM tcs_dm_muc_tmuc");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM tcs_dm_muc_tmuc");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                    p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);
                    //Gan cac thong tin
                    string vma_muc = DBNull.Value.ToString();
                    string vma_tmuc = DBNull.Value.ToString();
                    string vten = DBNull.Value.ToString();
                    string vsac_thue = DBNull.Value.ToString();
                    string vtinhtrang = "1";
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    Int64 vmtm_id = 0;

                    foreach (ProcMegService.MSG.MSG20.ROW row_dm in body_dm.Row)
                    {

                        danhmuc_ct = row_dm.Danhmuc;
                        if (danhmuc_ct.MA_MUC != null)
                        {
                            if (!danhmuc_ct.MA_MUC.Equals(string.Empty))
                            {
                                vma_muc = danhmuc_ct.MA_MUC;
                            }
                        }
                        if (danhmuc_ct.MA_TIEU_MUC != null)
                        {
                            if (!danhmuc_ct.MA_TIEU_MUC.Equals(string.Empty))
                            {
                                vma_tmuc = danhmuc_ct.MA_TIEU_MUC;
                            }
                        }

                        if (vma_tmuc.Length > 0)
                        {
                            vsac_thue = GetSacThue(vma_tmuc);
                        }

                        if (danhmuc_ct.TENTIEUMUC != null)
                        {
                            if (!danhmuc_ct.TENTIEUMUC.Equals(string.Empty))
                            {
                                vten = danhmuc_ct.TENTIEUMUC;
                            }
                        }

                        if (danhmuc_ct.HIEU_LUC_TU != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_TU.Equals(string.Empty))
                            {
                                vNgayBD = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_TU);
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_DEN != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_DEN.Equals(string.Empty))
                            {
                                vNgayKT = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_DEN);
                            }
                        }
                        vmtm_id = TCS_GetSequence("TCS_MTM_ID_SEQ");


                        p = new IDbDataParameter[8];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "mtm_id";
                        p[0].DbType = DbType.Int64;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = vmtm_id;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ma_muc";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = vma_muc;

                        p[2] = new OracleParameter();
                        p[2].ParameterName = "ma_tmuc";
                        p[2].DbType = DbType.String;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = vma_tmuc;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "sac_thue";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = vsac_thue;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "ten";
                        p[4].DbType = DbType.String;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = vten.Replace("'", ""); ;

                        p[5] = new OracleParameter();
                        p[5].ParameterName = "ngay_bd";
                        p[5].DbType = DbType.Int64;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = vNgayBD;

                        p[6] = new OracleParameter();
                        p[6].ParameterName = "ngay_kt";
                        p[6].DbType = DbType.Int64;
                        p[6].Direction = ParameterDirection.Input;
                        p[6].Value = vNgayKT;

                        p[7] = new OracleParameter();
                        p[7].ParameterName = "tinh_trang";
                        p[7].DbType = DbType.String;
                        p[7].Direction = ParameterDirection.Input;
                        p[7].Value = vtinhtrang;
                       DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
               
                conn.Close();
                conn.Dispose();
            }

        }
        public static string GetSacThue(string strMA_TMUC)
        {
            string Result = "";
            try
            {
                string strSQL = "SELECT sac_thue FROM tcs_map_tm_st   where ma_ndkt ='" + strMA_TMUC + "'  ";
                DataTable dt = null;
                dt = DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = dt.Rows[0]["sac_thue"].ToString();
                }
                return Result;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return Result;
            }
        }
        private static void capnhatdmSHKB(MSG16 msg16)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            /* Formatted on 20-Dec-2014 15:25:59 (QP5 v5.126) */
            sbSqlInsert.Append("INSERT INTO tcs_dm_khobac (shkb,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("ma_tinh,");
            sbSqlInsert.Append("ma_huyen,");
            sbSqlInsert.Append("ma_xa,");
            sbSqlInsert.Append("tinh_trang,");
            sbSqlInsert.Append("auto_khoaso,");
            sbSqlInsert.Append("ma_db,");
            sbSqlInsert.Append("ngay_capnhat)");
            sbSqlInsert.Append(" VALUES(:shkb,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":ma_tinh,");
            sbSqlInsert.Append(":ma_huyen,");
            sbSqlInsert.Append(":ma_xa,");
            sbSqlInsert.Append(":tinh_trang,");
            sbSqlInsert.Append(":auto_khoaso,");
            sbSqlInsert.Append(":ma_db,");
            sbSqlInsert.Append("SYSDATE)");
            try
            {
                conn =DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg16.Body.Row == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }
                ProcMegService.MSG.MSG16.BODY body_dm = new ProcMegService.MSG.MSG16.BODY();
                body_dm = msg16.Body;
                ProcMegService.MSG.MSG16.DANHMUC danhmuc_ct = new ProcMegService.MSG.MSG16.DANHMUC();
                //Lay thong tin dang ky thue dua vao CSDL
                if ((msg16.Body.Row != null))
                {

                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh
                    StringBuilder sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM tcs_dm_khobac_bk");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("INSERT INTO tcs_dm_khobac_bk ( shkb, ten, ma_tinh, ma_huyen, ma_xa, tinh_trang,auto_khoaso,ma_db)");
                    sbSqlDelete.Append(" SELECT shkb, ten, ma_tinh, ma_huyen, ma_xa, tinh_trang,auto_khoaso,ma_db FROM tcs_dm_khobac ");
                     p = new IDbDataParameter[0];
                   DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("DELETE FROM tcs_dm_khobac");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                    p = new IDbDataParameter[0];
                   DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    //Gan cac thong tin
                    string vshkb = DBNull.Value.ToString();
                    string vten = DBNull.Value.ToString();
                    string vma_tinh = DBNull.Value.ToString();
                    string vma_huyen = DBNull.Value.ToString();
                    string vma_xa = DBNull.Value.ToString();
                    string vtinh_trang = "1";
                    string vauto_khoaso = "1";
                    string vma_db = DBNull.Value.ToString();
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    //Int64 vcch_id = 0;
                    foreach (ProcMegService.MSG.MSG16.ROW row_dm in body_dm.Row)
                    {

                        danhmuc_ct = row_dm.Danhmuc;
                        if (danhmuc_ct.MA_KHO_BAC != null)
                        {
                            if (!danhmuc_ct.MA_KHO_BAC.Equals(string.Empty))
                            {
                                vshkb = danhmuc_ct.MA_KHO_BAC;
                            }
                        }
                        if (danhmuc_ct.TEN_KHO_BAC != null)
                        {
                            if (!danhmuc_ct.TEN_KHO_BAC.Equals(string.Empty))
                            {
                                vten = danhmuc_ct.TEN_KHO_BAC;
                            }
                        }
                        if (danhmuc_ct.MA_TINH != null)
                        {
                            if (!danhmuc_ct.MA_TINH.Equals(string.Empty))
                            {
                                vma_tinh = danhmuc_ct.MA_TINH;
                            }
                        }
                        if (danhmuc_ct.MA_HUYEN != null)
                        {
                            if (!danhmuc_ct.MA_HUYEN.Equals(string.Empty))
                            {
                                vma_huyen = danhmuc_ct.MA_HUYEN;
                                vma_db = danhmuc_ct.MA_HUYEN;
                            }
                        }
                        if (danhmuc_ct.MA_XA != null)
                        {
                            if (!danhmuc_ct.MA_XA.Equals(string.Empty))
                            {
                                vma_xa = danhmuc_ct.MA_XA;
                            }
                        }

                        if (danhmuc_ct.HIEU_LUC_TU != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_TU.Equals(string.Empty))
                            {
                                vNgayBD = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_TU);
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_DEN != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_DEN.Equals(string.Empty))
                            {
                                vNgayKT = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_DEN);
                            }
                        }
                        //   vcch_id = TCS_GetSequence("TCS_CCH_ID_SEQ");


                        p = new IDbDataParameter[8];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "shkb";
                        p[0].DbType = DbType.String;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = vshkb;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ten";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = vten.Replace("'", ""); ;

                        p[2] = new OracleParameter();
                        p[2].ParameterName = "ma_tinh";
                        p[2].DbType = DbType.String;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = vma_tinh;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "ma_huyen";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = vma_huyen;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "ma_xa";
                        p[4].DbType = DbType.String;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = vma_xa;

                        p[5] = new OracleParameter();
                        p[5].ParameterName = "tinh_trang";
                        p[5].DbType = DbType.String;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = vtinh_trang;

                        p[6] = new OracleParameter();
                        p[6].ParameterName = "auto_khoaso";
                        p[6].DbType = DbType.String;
                        p[6].Direction = ParameterDirection.Input;
                        p[6].Value = vauto_khoaso;

                        p[7] = new OracleParameter();
                        p[7].ParameterName = "ma_db";
                        p[7].DbType = DbType.String;
                        p[7].Direction = ParameterDirection.Input;
                        p[7].Value = vma_db;
                       DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
               
                conn.Close();
                conn.Dispose();
            }

        }
        private static void capnhatdmCQThu(MSG10 msg10)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_dm_cqthu (ma_cqthu,");
            sbSqlInsert.Append("ID,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("dbhc_tinh,");
            sbSqlInsert.Append("dbhc_huyen,");
            sbSqlInsert.Append("ma_qlt,");
            sbSqlInsert.Append("ma_hq,");
            sbSqlInsert.Append("shkb,");
            sbSqlInsert.Append("ma_dthu,");
            sbSqlInsert.Append("ma_tinh,");
            sbSqlInsert.Append("ngay_capnhat)");
            sbSqlInsert.Append(" VALUES(:ma_cqthu,");
            sbSqlInsert.Append(":ID,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":dbhc_tinh,");
            sbSqlInsert.Append(":dbhc_huyen,");
            sbSqlInsert.Append(":ma_qlt,");
            sbSqlInsert.Append(":ma_hq,");
            sbSqlInsert.Append(":shkb,");
            sbSqlInsert.Append(":ma_dthu,");
            sbSqlInsert.Append(":ma_tinh,");
            sbSqlInsert.Append("SYSDATE)");
            try
            {
                conn =DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg10.Body.Row == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }
                ProcMegService.MSG.MSG10.BODY body_dm = new ProcMegService.MSG.MSG10.BODY();
                body_dm = msg10.Body;
                ProcMegService.MSG.MSG10.DANHMUC danhmuc_ct = new ProcMegService.MSG.MSG10.DANHMUC();
                //Lay thong tin dang ky thue dua vao CSDL
                if ((msg10.Body.Row != null))
                {

                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh
                    StringBuilder sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("DELETE FROM TCS_DM_CQTHU_BK");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                   DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("INSERT INTO TCS_DM_CQTHU_BK ( ma_cqthu, ten, ma_dthu, shkb, ma_cqthu_cu, ma_hq,ma_qlt, dbhc_tinh, dbhc_huyen, ma_tinh, trang_thai, id,ngay_bk)");
                    sbSqlDelete.Append(" SELECT ma_cqthu, ten, ma_dthu, shkb, ma_cqthu_cu, ma_hq,ma_qlt, dbhc_tinh, dbhc_huyen, ma_tinh, trang_thai, id, sysdate FROM TCS_DM_CQTHU WHERE MA_HQ IS NULL ");
                    p = new IDbDataParameter[0];
                   DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("DELETE FROM TCS_DM_CQTHU WHERE MA_HQ IS NULL");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                     p = new IDbDataParameter[0];
                   DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    //Gan cac thong tin
                    string vMa_cqthu = DBNull.Value.ToString();
                    string vMa_qlt = DBNull.Value.ToString();
                    string vTen_cqthu = DBNull.Value.ToString();
                    string vMaTinh = DBNull.Value.ToString();
                    string vMaHuyen = DBNull.Value.ToString();
                    string vMaHQ = DBNull.Value.ToString();
                    string vSHKB = DBNull.Value.ToString();
                    string vMaDT = DBNull.Value.ToString();
                    string vMaTinh_CITAD = DBNull.Value.ToString();
                    Int64 vID = 0;
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    Int64 vcch_id = 0;

                    foreach (ProcMegService.MSG.MSG10.ROW row_dm in body_dm.Row)
                    {

                        danhmuc_ct = row_dm.Danhmuc;
                        if (danhmuc_ct.MA_CQ_THU != null)
                        {
                            if (!danhmuc_ct.MA_CQ_THU.Equals(string.Empty))
                            {
                                vMa_cqthu = danhmuc_ct.MA_CQ_THU;
                            }
                        }
                        if (danhmuc_ct.CQT_QLT != null)
                        {
                            if (!danhmuc_ct.CQT_QLT.Equals(string.Empty))
                            {
                                vMa_qlt = danhmuc_ct.CQT_QLT;
                            }
                        }
                        if (danhmuc_ct.TEN_CQT != null)
                        {
                            if (!danhmuc_ct.TEN_CQT.Equals(string.Empty))
                            {
                                vTen_cqthu = danhmuc_ct.TEN_CQT;
                            }
                        }
                        if (danhmuc_ct.MA_TINH != null)
                        {
                            if (!danhmuc_ct.MA_TINH.Equals(string.Empty))
                            {
                                vMaTinh = danhmuc_ct.MA_TINH;
                                vMaTinh_CITAD = danhmuc_ct.MA_TINH.ToString().Substring(0, 2);
                            }
                        }
                        if (danhmuc_ct.MA_HUYEN != null)
                        {
                            if (!danhmuc_ct.MA_HUYEN.Equals(string.Empty))
                            {
                                vMaHuyen = danhmuc_ct.MA_HUYEN;
                            }
                        }
                        if (danhmuc_ct.SHKB != null)
                        {
                            if (!danhmuc_ct.SHKB.Equals(string.Empty))
                            {
                                vSHKB = danhmuc_ct.SHKB;
                                vMaDT = danhmuc_ct.SHKB;
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_TU != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_TU.Equals(string.Empty))
                            {
                                vNgayBD = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_TU);
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_DEN != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_DEN.Equals(string.Empty))
                            {
                                vNgayKT = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_DEN);
                            }
                        }
                        vID = TCS_GetSequence("tcs_cqthu_id_seq");


                        p = new IDbDataParameter[10];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "ma_cqthu";
                        p[0].DbType = DbType.Int64;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = vMa_cqthu;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ten";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = vTen_cqthu.Replace("'", ""); ;


                        p[2] = new OracleParameter();
                        p[2].ParameterName = "ID";
                        p[2].DbType = DbType.Int64;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value =vID ;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "dbhc_tinh";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = vMaTinh;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "dbhc_huyen";
                        p[4].DbType = DbType.String;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = vMaHuyen;

                        p[5] = new OracleParameter();
                        p[5].ParameterName = "ma_qlt";
                        p[5].DbType = DbType.String;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = vMa_qlt;

                        p[6] = new OracleParameter();
                        p[6].ParameterName = "ma_hq";
                        p[6].DbType = DbType.String;
                        p[6].Direction = ParameterDirection.Input;
                        p[6].Value = vMaHQ;

                        p[7] = new OracleParameter();
                        p[7].ParameterName = "shkb";
                        p[7].DbType = DbType.String;
                        p[7].Direction = ParameterDirection.Input;
                        p[7].Value =vSHKB ;

                        p[8] = new OracleParameter();
                        p[8].ParameterName = "ma_dthu";
                        p[8].DbType = DbType.String;
                        p[8].Direction = ParameterDirection.Input;
                        p[8].Value = vMaDT;

                        p[9] = new OracleParameter();
                        p[9].ParameterName = "ma_tinh";
                        p[9].DbType = DbType.String;
                        p[9].Direction = ParameterDirection.Input;
                        p[9].Value = vMaTinh_CITAD;
                       DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
               
                conn.Close();
                conn.Dispose();
            }

        }
        private static void capnhatdmchuong(MSG08 msg08)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_dm_cap_chuong (cch_id,");
            sbSqlInsert.Append("ma_cap,");
            sbSqlInsert.Append("ma_chuong,");
            sbSqlInsert.Append("ten,");
            sbSqlInsert.Append("ngay_bd,");
            sbSqlInsert.Append("ngay_kt,ngay_capnhat)");
            sbSqlInsert.Append(" VALUES(:cch_id,");
            sbSqlInsert.Append(":ma_cap,");
            sbSqlInsert.Append(":ma_chuong,");
            sbSqlInsert.Append(":ten,");
            sbSqlInsert.Append(":ngay_bd,");
            sbSqlInsert.Append(":ngay_kt,");
            sbSqlInsert.Append("SYSDATE)");
            try
            {
                conn =DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg08.Body.Row == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục từ thuế");
                }
                ProcMegService.MSG.MSG08.BODY body_dm = new ProcMegService.MSG.MSG08.BODY();
                body_dm = msg08.Body;
                ProcMegService.MSG.MSG08.DANHMUC danhmuc_ct = new ProcMegService.MSG.MSG08.DANHMUC();
                //Lay thong tin dang ky thue dua vao CSDL
                if ((msg08.Body.Row != null))
                {

                    //Xoa du lieu truoc
                    //Khai bao cac cau lenh
                    StringBuilder sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("DELETE FROM TCS_DM_CAP_CHUONG_BK");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                   DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("INSERT INTO TCS_DM_CAP_CHUONG_BK ( cch_id, ma_cap, ma_chuong, ten, ngay_bd, ngay_kt,ghi_chu, tinh_trang,ngay_bk)");
                    sbSqlDelete.Append(" SELECT cch_id, ma_cap, ma_chuong, ten, ngay_bd, ngay_kt,ghi_chu, tinh_trang, sysdate FROM TCS_DM_CAP_CHUONG ");
                    p = new IDbDataParameter[0];
                    DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    sbSqlDelete = new StringBuilder("");

                    sbSqlDelete.Append("DELETE FROM TCS_DM_CAP_CHUONG");


                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
                     p = new IDbDataParameter[0];
                     DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    //Gan cac thong tin
                    string vMacap = DBNull.Value.ToString();
                    string vMachuong = DBNull.Value.ToString();
                    string vTenchuong = DBNull.Value.ToString();
                    Int64 vNgayBD = 0;
                    Int64 vNgayKT = 0;
                    Int64 vcch_id = 0;

                    foreach (ProcMegService.MSG.MSG08.ROW row_dm in body_dm.Row)
                    {

                        danhmuc_ct = row_dm.Danhmuc;
                        if (danhmuc_ct.MA_CAP != null)
                        {
                            if (!danhmuc_ct.MA_CAP.Equals(string.Empty))
                            {
                                vMacap = danhmuc_ct.MA_CAP;
                            }
                        }
                        if (danhmuc_ct.MA_CHUONG != null)
                        {
                            if (!danhmuc_ct.MA_CHUONG.Equals(string.Empty))
                            {
                                vMachuong = danhmuc_ct.MA_CHUONG;
                            }
                        }
                        if (danhmuc_ct.TEN_CHUONG != null)
                        {
                            if (!danhmuc_ct.TEN_CHUONG.Equals(string.Empty))
                            {
                                vTenchuong = danhmuc_ct.TEN_CHUONG;
                            }
                        }

                        if (danhmuc_ct.HIEU_LUC_TU != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_TU.Equals(string.Empty))
                            {
                                vNgayBD = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_TU);
                            }
                        }
                        if (danhmuc_ct.HIEU_LUC_DEN != null)
                        {
                            if (!danhmuc_ct.HIEU_LUC_DEN.Equals(string.Empty))
                            {
                                vNgayKT = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_DEN);
                            }
                        }
                        vcch_id = TCS_GetSequence("TCS_CCH_ID_SEQ");


                        p = new IDbDataParameter[6];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "cch_id";
                        p[0].DbType = DbType.Int64;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = vcch_id;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ma_cap";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = vMacap;

                        p[2] = new OracleParameter();
                        p[2].ParameterName = "ma_chuong";
                        p[2].DbType = DbType.String;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = vMachuong;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "ten";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = vTenchuong.Replace("'", ""); ;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "ngay_bd";
                        p[4].DbType = DbType.Int64;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = vNgayBD;

                        p[5] = new OracleParameter();
                        p[5].ParameterName = "ngay_kt";
                        p[5].DbType = DbType.Int64;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = vNgayKT;
                        DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
             
                conn.Close();
                conn.Dispose();
            }

        }
        #endregion
        #region MSG21
        public static void getMSG21(string pv_strMaNTT, string pv_strSHKB, string pv_strSoCT,
                                    string pv_strSoBT, string pv_strMaNV, ref string pv_trangthai, ref string pv_mota_tt)
        {
            strMsg_Id = TCS_GetSequenceValue();
            MSG21 ms21 = new MSG21();
            ms21.Header = new ProcMegService.MSG.MSG21.HEADER();
            ms21.Body = new ProcMegService.MSG.MSG21.BODY();
            ms21.Body.Row = new ProcMegService.MSG.MSG21.ROW();
            ms21.Body.Row.CHUNGTU = new ProcMegService.MSG.MSG21.CHUNGTU();
            ms21.Header.VERSION = strMessage_Version;
            ms21.Header.SENDER_CODE = strSender_Code;
            ms21.Header.SENDER_NAME = strSender_Name;
            ms21.Header.RECEIVER_CODE = strReciver_Code;
            ms21.Header.RECEIVER_NAME = strReciver_Name;
            ms21.Header.TRAN_CODE = MessageType.strMSG21;
            ms21.Header.MSG_ID = strMsg_Id;
            ms21.Header.MSG_REFID = strMsgRef_Id;
            ms21.Header.ID_LINK = strId_Link;
            ms21.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
            ms21.Header.ORIGINAL_CODE = strOriginal_Code;
            ms21.Header.ORIGINAL_NAME = strOriginal_Name;
            ms21.Header.ORIGINAL_DATE = strOriginal_Date;
            ms21.Header.ERROR_CODE = strError_Code;
            ms21.Header.ERROR_DESC = "";
            ms21.Header.SPARE1 = strSpare1;
            ms21.Header.SPARE2 = strSpare2;
            ms21.Header.SPARE3 = strSpare3;
            //Gan du lieu body
            ms21.Body.Row.TYPE = MessageType.strMSG21;
            ms21.Body.Row.NAME = "Thông tin chứng từ";
            CTUToMSG21(pv_strMaNTT, pv_strSHKB,
                                   pv_strSoCT, pv_strSoBT,
                                   pv_strMaNV, ref ms21);
            //Chuyen thanh XML
            string strXMLOut = ms21.MSG21toXML(ms21);

            strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout4, String.Empty);
            //Ky tren msg
            strXMLOut = SignXML(strXMLOut);
            //Goi den Web Service
            string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG21);
            //if (verifySignXML(strXMLIn))
            //{
                MSG26 msg26 = new MSG26();
                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                MSG26 objTemp = msg26.MSGToObject(strXMLIn);
                if (objTemp != null)
                {
                    pv_trangthai = objTemp.Body.Row.Chungtu.TRANG_THAI;
                    pv_mota_tt = objTemp.Body.Row.Chungtu.MOTA_TRANGTHAI;
                }
                else
                {
                    pv_trangthai = "-1";
                    pv_mota_tt = "không load được object trả về";
                }
                //capnhatSothue(MST, objTemp);
            //}
            //else
            //{
            //    throw new Exception("Sai chu ky");
            //}
        }
        //sonmt: override msg21
        public static void getMSG21(string pv_strMaNTT, string pv_strSHKB, string pv_strSoCT,
                                    string pv_strSoBT, string pv_strMaNV, string ma_hthuc_nop, ref string pv_trangthai, ref string pv_mota_tt)
        {
            strMsg_Id = TCS_GetSequenceValue();
            MSG21 ms21 = new MSG21();
            ms21.Header = new ProcMegService.MSG.MSG21.HEADER();
            ms21.Body = new ProcMegService.MSG.MSG21.BODY();
            ms21.Body.Row = new ProcMegService.MSG.MSG21.ROW();
            ms21.Body.Row.CHUNGTU = new ProcMegService.MSG.MSG21.CHUNGTU();
            ms21.Header.VERSION = strMessage_Version;
            ms21.Header.SENDER_CODE = strSender_Code;
            ms21.Header.SENDER_NAME = strSender_Name;
            ms21.Header.RECEIVER_CODE = strReciver_Code;
            ms21.Header.RECEIVER_NAME = strReciver_Name;
            ms21.Header.TRAN_CODE = MessageType.strMSG21;
            ms21.Header.MSG_ID = strMsg_Id;
            ms21.Header.MSG_REFID = strMsgRef_Id;
            ms21.Header.ID_LINK = strId_Link;
            ms21.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
            ms21.Header.ORIGINAL_CODE = strOriginal_Code;
            ms21.Header.ORIGINAL_NAME = strOriginal_Name;
            ms21.Header.ORIGINAL_DATE = strOriginal_Date;
            ms21.Header.ERROR_CODE = strError_Code;
            ms21.Header.ERROR_DESC = "";
            ms21.Header.SPARE1 = strSpare1;
            ms21.Header.SPARE2 = strSpare2;
            ms21.Header.SPARE3 = strSpare3;
            //Gan du lieu body
            ms21.Body.Row.TYPE = MessageType.strMSG21;
            ms21.Body.Row.NAME = "Thông tin chứng từ";
            //LogDebug.WriteLog("0 getMSG21", EventLogEntryType.Error);
            CTUToMSG21(pv_strMaNTT, pv_strSHKB,
                                   pv_strSoCT, pv_strSoBT,
                                   pv_strMaNV, ma_hthuc_nop, ref ms21);
            //Chuyen thanh XML
            string strXMLOut = ms21.MSG21toXML(ms21);

            strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout4, String.Empty);
            //Ky tren msg
            //LogDebug.WriteLog("1 Truoc ky", EventLogEntryType.Error);
            strXMLOut = SignXML(strXMLOut);
            //Goi den Web Service
            //LogDebug.WriteLog("2 Sau ky", EventLogEntryType.Error);
            string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG21);
            //if (verifySignXML(strXMLIn))
            //{
                MSG26 msg26 = new MSG26();
                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                MSG26 objTemp = msg26.MSGToObject(strXMLIn);
                if (objTemp != null)
                {
                    pv_trangthai = objTemp.Body.Row.Chungtu.TRANG_THAI;
                    pv_mota_tt = objTemp.Body.Row.Chungtu.MOTA_TRANGTHAI;
                }
                else
                {
                    pv_trangthai = "-1";
                    pv_mota_tt = "không load được object trả về";
                }
                //capnhatSothue(MST, objTemp);
            //}
            //else
            //{
            //    throw new Exception("Sai chu ky");
            //}
        }
        #endregion
        #region MSG22
        public static void getMSG22(string MST, string MST_NNT, string SoCT, string NgayCT_Tungay, string NgayCT_Denngay, string Trangthai)
        {
            MSG22 ms22 = new MSG22();
            strMsg_Id = TCS_GetSequenceValue();
            //Gan du lieu header
            ms22.Header = new ProcMegService.MSG.MSG22.HEADER();
            ms22.Body = new ProcMegService.MSG.MSG22.BODY();
            ms22.Body.Row = new ProcMegService.MSG.MSG22.ROW();
            ms22.Body.Row.ChungTu = new ProcMegService.MSG.MSG22.CHUNGTU();
            ms22.Header.VERSION = strMessage_Version;
            ms22.Header.SENDER_CODE = strSender_Code;
            ms22.Header.SENDER_NAME = strSender_Name;
            ms22.Header.RECEIVER_CODE = strReciver_Code;
            ms22.Header.RECEIVER_NAME = strReciver_Name;
            ms22.Header.TRAN_CODE = MessageType.strMSG22;
            ms22.Header.MSG_ID = strMsg_Id;
            ms22.Header.MSG_REFID = strMsgRef_Id;
            ms22.Header.ID_LINK = strId_Link;
            ms22.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
            ms22.Header.ORIGINAL_CODE = strOriginal_Code;
            ms22.Header.ORIGINAL_NAME = strOriginal_Name;
            ms22.Header.ORIGINAL_DATE = strOriginal_Date;
            ms22.Header.ERROR_CODE = strError_Code;
            ms22.Header.ERROR_DESC = "";
            ms22.Header.SPARE1 = strSpare1;
            ms22.Header.SPARE2 = strSpare2;
            ms22.Header.SPARE3 = strSpare3;
            //Gan du lieu body
            ms22.Body.Row.TYPE = MessageType.strMSG22;
            ms22.Body.Row.NAME = "Tra cứu thông tin chứng từ";
            ms22.Body.Row.ChungTu.MST = MST;
            ms22.Body.Row.ChungTu.MST_NNTHAY = MST_NNT;

            ms22.Body.Row.ChungTu.SO_CHUNGTU = SoCT;
            ms22.Body.Row.ChungTu.NGAY_CHUNGTU_TUNGAY = NgayCT_Tungay;
            ms22.Body.Row.ChungTu.NGAY_CHUNGTU_DENNGAY = NgayCT_Denngay;
            ms22.Body.Row.ChungTu.TRANG_THAI = Trangthai;


            //Chuyen thanh XML
            string strXMLOut = ms22.MSG22toXML(ms22);

            strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout4, String.Empty);
            //Ky tren msg
            strXMLOut = SignXML(strXMLOut);
            string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG22);
            if (strXMLIn.Equals(String.Empty))
            {
                throw new Exception("Không lấy được dữ liệu từ thuế");
            }
            if (verifySignXML(strXMLIn))
            {
                MSG27 msg27 = new MSG27();
                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                MSG27 objTemp27 = msg27.MSGToObject(strXMLIn);
                if (objTemp27.Body.Row.Phanhoi != null)
                {
                    throw new Exception(objTemp27.Body.Row.Phanhoi.TRANG_THAI + " " + objTemp27.Body.Row.Phanhoi.MOTA_TRANGTHAI);
                }
                else
                {
                    MSG23 msg23 = new MSG23();
                    //strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                    MSG23 objTemp = msg23.MSGToObject(strXMLIn);
                    capnhatTracuuthue(objTemp);
                }
            }
            else
            {
                throw new Exception("Sai chu ky");
            }
        }
        private static void capnhatTracuuthue(MSG23 msg23)
        {
            OracleConnection conn = default(OracleConnection);
            IDbTransaction transCT = null;
            //Clear data TCS_CTU_HDR_THUE truoc khi insert moi
            StringBuilder sbSqlDeleteHDR = new StringBuilder("");
            sbSqlDeleteHDR.Append("DELETE FROM TCS_CTU_HDR_THUE WHERE MA_NNTHUE=:MA_NNTHUE");

            //Clear data TCS_CTU_DTL_THUE truoc khi insert moi
            StringBuilder sbSqlDeleteDTL = new StringBuilder("");
            sbSqlDeleteDTL.Append("DELETE FROM TCS_CTU_DTL_THUE WHERE SO_CT=:SO_CT");

            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO tcs_ctu_hdr_thue (so_ct,");
            sbSqlInsert.Append("ngay_ct,");
            sbSqlInsert.Append("ma_nntien,");
            sbSqlInsert.Append("ten_nntien,");
            sbSqlInsert.Append("dc_nntien,");
            sbSqlInsert.Append("ma_nnthue,");
            sbSqlInsert.Append("ten_nnthue,");
            sbSqlInsert.Append("dc_nnthue,");
            sbSqlInsert.Append("ma_cqthu,");
            sbSqlInsert.Append("tk_co,");
            sbSqlInsert.Append("ma_nh_a,");
            sbSqlInsert.Append("ma_nh_b,");
            sbSqlInsert.Append("ma_ntk,");
            sbSqlInsert.Append("ttien,");
            sbSqlInsert.Append("TRANGTHAI_CHUNGTU)");
            sbSqlInsert.Append("  VALUES   (:so_ct,");
            sbSqlInsert.Append(":ngay_ct,");
            sbSqlInsert.Append(":ma_nntien,");
            sbSqlInsert.Append(":ten_nntien,");
            sbSqlInsert.Append(":dc_nntien,");
            sbSqlInsert.Append(":ma_nnthue,");
            sbSqlInsert.Append(":ten_nnthue,");
            sbSqlInsert.Append(":dc_nnthue,");
            sbSqlInsert.Append(":ma_cqthu,");
            sbSqlInsert.Append(":tk_co,");
            sbSqlInsert.Append(":ma_nh_a,");
            sbSqlInsert.Append(":ma_nh_b,");
            sbSqlInsert.Append(":ma_ntk,");
            sbSqlInsert.Append(":ttien,");
            sbSqlInsert.Append(":TRANGTHAI_CHUNGTU)");

            StringBuilder sbSqlInsert_dtl = new StringBuilder("");
            sbSqlInsert_dtl.Append("INSERT INTO tcs_ctu_dtl_thue (so_ct,");
            sbSqlInsert_dtl.Append("ma_chuong,");
            sbSqlInsert_dtl.Append("ma_tmuc,");
            sbSqlInsert_dtl.Append("noi_dung,");
            sbSqlInsert_dtl.Append("sotien,");
            sbSqlInsert_dtl.Append("sotien_nt)");
            sbSqlInsert_dtl.Append("  VALUES   (:so_ct,");
            sbSqlInsert_dtl.Append(":ma_chuong,");
            sbSqlInsert_dtl.Append(":ma_tmuc,");
            sbSqlInsert_dtl.Append(":noi_dung,");
            sbSqlInsert_dtl.Append(":sotien,");
            sbSqlInsert_dtl.Append(":sotien_nt)");
            try
            {
                conn =DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                // 
                if (msg23.Body.Row == null)
                {
                    throw new Exception( "Không có thông tin chứng từ bên thuế");
                }
                //Xoa du lieu truoc
                //Khai bao cac cau lenh
                //StringBuilder sbSqlDelete = new StringBuilder("");
                //sbSqlDelete.Append("DELETE FROM TCS_SOTHUE WHERE MA_NNT = :ma_nnt");

                IDbDataParameter[] p = new IDbDataParameter[1];
                //p[0] = new OracleParameter();
                //p[0].ParameterName = "MA_NNT";
                //p[0].DbType = DbType.String;
                //p[0].Direction = ParameterDirection.Input;
                //p[0].Value = pTIN;

                //  ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);


                //Lay thong tin so thue phai nop dua vao CSDL
                ProcMegService.MSG.MSG23.ROW thongTinCTU_arr = new ProcMegService.MSG.MSG23.ROW();
                thongTinCTU_arr = msg23.Body.Row;
                if (thongTinCTU_arr.Chungtu == null)
                {
                    throw new Exception("Không có thông tin chứng từ bên thuế");
                }
                string strMA_NNTHUE = "";
                string strSo_Ctu_dtl = "";
                foreach (ProcMegService.MSG.MSG23.CHUNGTU thongTinCTU in thongTinCTU_arr.Chungtu)
                {
                    // ProcMegService.MSG.MSG06.ROW_NNT thongTinDangKyThue = thongTinDangKyThue_ct[i];
                    if (thongTinCTU.MST != null)
                    {
                        if (!thongTinCTU.MST.Equals(string.Empty))
                        {
                            strMA_NNTHUE = thongTinCTU.MST;
                        }
                    }
                    if (thongTinCTU.SO_CHUNGTU != null)
                    {
                        if (!thongTinCTU.SO_CHUNGTU.Equals(string.Empty))
                        {
                            strSo_Ctu_dtl = thongTinCTU.SO_CHUNGTU;
                        }
                    }
                    //Xoa du lieu bang TCS_CTU_DTL_THUE
                    p = new IDbDataParameter[1];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "SO_CT";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = strSo_Ctu_dtl;

                    DataAccess.ExecuteNonQuery(sbSqlDeleteDTL.ToString(), CommandType.Text, p, transCT);
                    //
                }
                //Xoa du lieu bang TCS_CTU_HDR_THUE
                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "MA_NNTHUE";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = strMA_NNTHUE;

                DataAccess.ExecuteNonQuery(sbSqlDeleteHDR.ToString(), CommandType.Text, p, transCT);
                //

                string vSo_ct = DBNull.Value.ToString();
                DateTime vNgay_CT = DateTime.MinValue;
                string vma_nntien = DBNull.Value.ToString();
                string vten_nntien = DBNull.Value.ToString();
                string vdc_nntien = DBNull.Value.ToString();
                string vma_nnthue = DBNull.Value.ToString();
                string vten_nnthue = DBNull.Value.ToString();
                string vdc_nnthue = DBNull.Value.ToString();
                string vma_cqthu = DBNull.Value.ToString();
                string vtk_co = DBNull.Value.ToString();
                string vma_nh_a = strSender_Code;
                string vma_nh_b = DBNull.Value.ToString();
                Int64 vttien = 0;
                string vma_ntk = DBNull.Value.ToString();

                string tthai_ct = DBNull.Value.ToString();


                string vma_chuong = DBNull.Value.ToString();
                string vma_tmuc = DBNull.Value.ToString();
                string vnoidung = DBNull.Value.ToString();
                Int64 vsotien = 0;
                Int64 vsotien_nt = 0;
                foreach (ProcMegService.MSG.MSG23.CHUNGTU thongTinCTU in thongTinCTU_arr.Chungtu)
                {
                    // ProcMegService.MSG.MSG06.ROW_NNT thongTinDangKyThue = thongTinDangKyThue_ct[i];
                    if (thongTinCTU.MST != null)
                    {
                        if (!thongTinCTU.MST.Equals(string.Empty))
                        {
                            vma_nnthue = thongTinCTU.MST;
                        }
                    }
                    if (thongTinCTU.TEN_NNT != null)
                    {
                        if (!thongTinCTU.TEN_NNT.Equals(string.Empty))
                        {
                            vten_nnthue = thongTinCTU.TEN_NNT;
                        }
                    }

                    if (thongTinCTU.MST_NNTHAY != null)
                    {
                        if (!thongTinCTU.MST_NNTHAY.Equals(string.Empty))
                        {
                            vma_nntien = thongTinCTU.MST_NNTHAY;
                        }
                    }

                    if (thongTinCTU.TEN_NNTHAY != null)
                    {
                        if (!thongTinCTU.TEN_NNTHAY.Equals(string.Empty))
                        {
                            vten_nntien = thongTinCTU.TEN_NNTHAY;
                        }
                    }

                    if (thongTinCTU.SO_CHUNGTU != null)
                    {
                        if (!thongTinCTU.SO_CHUNGTU.Equals(string.Empty))
                        {
                            vSo_ct = thongTinCTU.SO_CHUNGTU;
                        }
                    }
                    if (thongTinCTU.NGAY_CHUNGTU != null)
                    {
                        if (!thongTinCTU.NGAY_CHUNGTU.Equals(string.Empty))
                        {
                            vNgay_CT = DateTime.ParseExact(thongTinCTU.NGAY_CHUNGTU, "dd/MM/yyyy", null);
                        }
                    }
                    if (thongTinCTU.SO_CHUNGTU != null)
                    {
                        if (!thongTinCTU.SO_CHUNGTU.Equals(string.Empty))
                        {
                            vSo_ct = thongTinCTU.SO_CHUNGTU;
                        }
                    }
                    if (thongTinCTU.DIACHI_NNT != null)
                    {
                        if (!thongTinCTU.DIACHI_NNT.Equals(string.Empty))
                        {
                            vdc_nnthue = thongTinCTU.DIACHI_NNT;
                        }
                    }
                    if (thongTinCTU.DIACHI_NNTHAY != null)
                    {
                        if (!thongTinCTU.DIACHI_NNTHAY.Equals(string.Empty))
                        {
                            vdc_nntien = thongTinCTU.DIACHI_NNTHAY;
                        }
                    }
                    if (thongTinCTU.CQ_QLY_THU != null)
                    {
                        if (!thongTinCTU.CQ_QLY_THU.Equals(string.Empty))
                        {
                            vma_cqthu = thongTinCTU.CQ_QLY_THU;
                        }
                    }
                    if (thongTinCTU.MA_NHTM != null)
                    {
                        if (!thongTinCTU.MA_NHTM.Equals(string.Empty))
                        {
                            vma_nh_b = thongTinCTU.MA_NHTM;
                        }
                    }
                    if (thongTinCTU.SO_TK_NHKBNN != null)
                    {
                        if (!thongTinCTU.SO_TK_NHKBNN.Equals(string.Empty))
                        {
                            vtk_co = thongTinCTU.SO_TK_NHKBNN;
                        }
                    }
                    if (thongTinCTU.TINHCHAT_KHOAN_NOP != null)
                    {
                        if (!thongTinCTU.TINHCHAT_KHOAN_NOP.Equals(string.Empty))
                        {
                            vma_ntk = thongTinCTU.TINHCHAT_KHOAN_NOP;
                        }
                    }
                    if (thongTinCTU.TRANGTHAI_CHUNGTU != null)
                    {
                        if (!thongTinCTU.TRANGTHAI_CHUNGTU.Equals(string.Empty))
                        {
                            tthai_ct = thongTinCTU.TRANGTHAI_CHUNGTU;
                        }
                    }

                    p = new IDbDataParameter[15];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "so_ct";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = vSo_ct;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "NGAY_CT";
                    p[1].DbType = DbType.DateTime;
                    p[1].Direction = ParameterDirection.Input;
                    if (vNgay_CT.Equals(DateTime.MinValue))
                    {
                        p[1].Value = DBNull.Value;
                    }
                    else
                    {
                        p[1].Value = vNgay_CT;
                    }


                    p[2] = new OracleParameter();
                    p[2].ParameterName = "ma_nntien";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = vma_nntien;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "ten_nntien";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = vten_nntien;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "dc_nntien";
                    p[4].DbType = DbType.String;
                    p[4].Direction = ParameterDirection.Input;
                    p[4].Value = vdc_nntien;

                    p[5] = new OracleParameter();
                    p[5].ParameterName = "ma_nnthue";
                    p[5].DbType = DbType.String;
                    p[5].Direction = ParameterDirection.Input;
                    p[5].Value = vma_nnthue;

                    p[6] = new OracleParameter();
                    p[6].ParameterName = "ten_nnthue";
                    p[6].DbType = DbType.String;
                    p[6].Direction = ParameterDirection.Input;
                    p[6].Value = vten_nnthue;

                    p[7] = new OracleParameter();
                    p[7].ParameterName = "dc_nnthue";
                    p[7].DbType = DbType.String;
                    p[7].Direction = ParameterDirection.Input;
                    p[7].Value = vdc_nnthue;

                    p[8] = new OracleParameter();
                    p[8].ParameterName = "ma_cqthu";
                    p[8].DbType = DbType.String;
                    p[8].Direction = ParameterDirection.Input;
                    p[8].Value = vma_cqthu;

                    p[9] = new OracleParameter();
                    p[9].ParameterName = "tk_co";
                    p[9].DbType = DbType.String;
                    p[9].Direction = ParameterDirection.Input;
                    p[9].Value = vtk_co;

                    p[10] = new OracleParameter();
                    p[10].ParameterName = "ma_nh_a";
                    p[10].DbType = DbType.String;
                    p[10].Direction = ParameterDirection.Input;
                    p[10].Value = vma_nh_a;

                    p[11] = new OracleParameter();
                    p[11].ParameterName = "ma_nh_b";
                    p[11].DbType = DbType.String;
                    p[11].Direction = ParameterDirection.Input;
                    p[11].Value =vma_nh_b ;

                    p[12] = new OracleParameter();
                    p[12].ParameterName = "MA_NTK";
                    p[12].DbType = DbType.String;
                    p[12].Direction = ParameterDirection.Input;
                    p[12].Value = vma_ntk;

                    p[13] = new OracleParameter();
                    p[13].ParameterName = "ttien";
                    p[13].DbType = DbType.Int64;
                    p[13].Direction = ParameterDirection.Input;
                    p[13].Value = vttien;

                    p[14] = new OracleParameter();
                    p[14].ParameterName = "TRANGTHAI_CHUNGTU";
                    p[14].DbType = DbType.String;
                    p[14].Direction = ParameterDirection.Input;
                    p[14].Value = tthai_ct;

                    DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    foreach (ProcMegService.MSG.MSG23.CHUNGTU_CHITIET thongTinCTU_CT in thongTinCTU.Chungtu_Chitiet)
                    {

                        if (thongTinCTU_CT.CHUONG != null)
                        {
                            if (!thongTinCTU_CT.CHUONG.Equals(string.Empty))
                            {
                                vma_chuong = thongTinCTU_CT.CHUONG;
                            }
                        }
                        if (thongTinCTU_CT.SO_TIEN != null)
                        {
                            if (!thongTinCTU_CT.SO_TIEN.Equals(string.Empty))
                            {
                                vsotien =Int64.Parse( thongTinCTU_CT.SO_TIEN);
                                vsotien_nt = Int64.Parse(thongTinCTU_CT.SO_TIEN);
                            }
                        }
                        if (thongTinCTU_CT.TIEUMUC != null)
                        {
                            if (!thongTinCTU_CT.TIEUMUC.Equals(string.Empty))
                            {
                                vma_tmuc = thongTinCTU_CT.TIEUMUC;
                            }
                        }
                        if (thongTinCTU_CT.THONGTIN_KHOANNOP != null)
                        {
                            if (!thongTinCTU_CT.THONGTIN_KHOANNOP.Equals(string.Empty))
                            {
                                vnoidung = thongTinCTU_CT.THONGTIN_KHOANNOP;
                            }
                        }


                        p = new IDbDataParameter[6];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "so_ct";
                        p[0].DbType = DbType.String;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = vSo_ct;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ma_chuong";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = vma_chuong;

                        p[2] = new OracleParameter();
                        p[2].ParameterName = "ma_tmuc";
                        p[2].DbType = DbType.String;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = vma_tmuc;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "noi_dung";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = vnoidung;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "sotien";
                        p[4].DbType = DbType.Int64;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = vsotien;

                        p[5] = new OracleParameter();
                        p[5].ParameterName = "sotien_nt";
                        p[5].DbType = DbType.Int64;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = vsotien_nt;

                        DataAccess.ExecuteNonQuery(sbSqlInsert_dtl.ToString(), CommandType.Text, p, transCT);
                    }

                }

                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
                
                conn.Close();
                conn.Dispose();
            }

        }
        #endregion
        #region MSG24
        public static void getMSG24(string MST, string SoCT, ref string pv_trangthai, ref string pv_mota_tt)
        {
            MSG24 ms24 = new MSG24();
            strMsg_Id = TCS_GetSequenceValue();
            ms24.Body = new ProcMegService.MSG.MSG24.BODY();
            ms24.Header = new ProcMegService.MSG.MSG24.HEADER();
            ms24.Body.Row = new ProcMegService.MSG.MSG24.ROW();
            ms24.Body.Row.Huy_ChungTu = new ProcMegService.MSG.MSG24.HUY_CHUNGTU();
            //Gan du lieu header
            ms24.Header.VERSION = strMessage_Version;
            ms24.Header.SENDER_CODE = strSender_Code;
            ms24.Header.SENDER_NAME = strSender_Name;
            ms24.Header.RECEIVER_CODE = strReciver_Code;
            ms24.Header.RECEIVER_NAME = strReciver_Name;
            ms24.Header.TRAN_CODE = MessageType.strMSG24;
            ms24.Header.MSG_ID = strMsg_Id;
            ms24.Header.MSG_REFID = strMsgRef_Id;
            ms24.Header.ID_LINK = strId_Link;
            ms24.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
            ms24.Header.ORIGINAL_CODE = strOriginal_Code;
            ms24.Header.ORIGINAL_NAME = strOriginal_Name;
            ms24.Header.ORIGINAL_DATE = strOriginal_Date;
            ms24.Header.ERROR_CODE = strError_Code;
            ms24.Header.ERROR_DESC = "";
            ms24.Header.SPARE1 = strSpare1;
            ms24.Header.SPARE2 = strSpare2;
            ms24.Header.SPARE3 = strSpare3;
            //Gan du lieu body
            ms24.Body.Row.TYPE = MessageType.strMSG24;
            ms24.Body.Row.NAME = "Yêu cầu hủy thông tin chứng từ";
            ms24.Body.Row.Huy_ChungTu.MA_NHTM = strSender_Code;
            ms24.Body.Row.Huy_ChungTu.SO_CHUNGTU = SoCT;


            //Chuyen thanh XML
            string strXMLOut = ms24.MSG24toXML(ms24);

            strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout4, String.Empty);
            //Ky tren msg
            //Ky tren msg
            strXMLOut = SignXML(strXMLOut);
            //Goi den Web Service
            string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG24);
            if (verifySignXML(strXMLIn))
            {
                MSG25 msg25 = new MSG25();
                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                MSG25 objTemp = msg25.MSGToObject(strXMLIn);
                if (objTemp != null)
                {
                    pv_trangthai = objTemp.Body.Row.Huy_Chungtu.TRANGTHAI;
                    pv_mota_tt = objTemp.Body.Row.Huy_Chungtu.MOTA_TRANGTHAI;
                }
                else
                {
                    pv_trangthai = "-1";
                    pv_mota_tt = "không load được object trả về";
                }
                //capnhatSothue(MST, objTemp);
            }
            else
            {
                throw new Exception("Sai chu ky");
            }
        }
        #endregion

        #region MSG32 by sonmt
        public static void getMSG32(string ngay_bc, ref string pv_trangthai, ref string pv_mota_tt, ref string tong_gnt_ref, ref string tong_tien_ref, ref string tg_gui_ref, ref string tu_ngay_ref, ref string den_ngay_ref, ref DataTable dtKQ)
        {
            try
            {
                if (ngay_bc.Trim().Length > 0)
                {
                    //prepare data for message
                    string bc_tu_ngay, bc_den_ngay = "";
                    string tong_gnt = "0";
                    string tong_tiennop = "0";

                    IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);
                    DateTime ngay_ketthuc = Convert.ToDateTime(ngay_bc, culture);
                    DateTime ngay_batdau = ngay_ketthuc.AddDays(-1);
                    string ngay_batdau_dc, ngay_ketthuc_dc = "";
                    string ngay_batdau_dc1, ngay_ketthuc_dc1 = "";

                    string gio_dc = TCS_GetGIO_DC();

                    ngay_batdau_dc = ngay_batdau.ToString("yyyy") + ngay_batdau.DayOfYear.ToString().PadLeft(3,'0') + gio_dc.Replace(":", "");
                    ngay_ketthuc_dc = ngay_ketthuc.ToString("yyyy") + ngay_ketthuc.DayOfYear.ToString().PadLeft(3, '0') + gio_dc.Replace(":", "");
                    bc_tu_ngay = ngay_batdau.ToString("dd/MM/yyyy") + " " + gio_dc;
                    bc_den_ngay = ngay_ketthuc.ToString("dd/MM/yyyy") + " " + gio_dc;

                    ngay_batdau_dc1 = ngay_batdau.ToString("yyyyMMdd") + gio_dc.Replace(":", ""); ;
                    ngay_ketthuc_dc1 = ngay_ketthuc.ToString("yyyyMMdd") + gio_dc.Replace(":", ""); ;


                    DataTable dtNTDT = new DataTable();
                    string sl_gnt_ntdt = "0";
                    string so_tien_ntdt = "0";
                    dtNTDT = DataAccess.ExecuteReturnDataSet("select count(*) as sl_gnt, sum(tong_tien) as so_tien from tcs_ctu_ntdt_hdr where tthai_ctu = '11' and TO_NUMBER(ngay_gui_gd) >= " + ngay_batdau_dc + " and TO_NUMBER(ngay_gui_gd) <= " + ngay_ketthuc_dc, CommandType.Text).Tables[0];
                    if (dtNTDT.Rows.Count > 0)
                    {
                        sl_gnt_ntdt = dtNTDT.Rows[0]["sl_gnt"].ToString();
                        if (!dtNTDT.Rows[0]["so_tien"].ToString().Trim().Equals(""))
                        {
                            so_tien_ntdt = dtNTDT.Rows[0]["so_tien"].ToString();
                        }
                    }

                    DataTable dtTQ = new DataTable();
                    string sl_gnt_tq = "0";
                    string so_tien_tq = "0";
                    dtTQ = DataAccess.ExecuteReturnDataSet("select count(*) as sl_gnt , sum(ttien) as so_tien from tcs_ctu_hdr where ma_lthue <> '04' and trang_thai = '01' and  TO_NUMBER(TO_CHAR(ngay_ks, 'yyyyMMddHHmmss')) >= TO_NUMBER('" + ngay_batdau_dc1 + "') and  TO_NUMBER(TO_CHAR(ngay_ks, 'yyyyMMddHHmmss')) <= TO_NUMBER('" + ngay_ketthuc_dc1 + "')", CommandType.Text).Tables[0];
                    if (dtTQ.Rows.Count > 0)
                    {
                        sl_gnt_tq = dtTQ.Rows[0]["sl_gnt"].ToString();
                        if (!dtTQ.Rows[0]["so_tien"].ToString().Trim().Equals(""))
                        {
                            so_tien_tq = dtTQ.Rows[0]["so_tien"].ToString();
                        }
                    }

                    tong_gnt = (int.Parse(sl_gnt_ntdt) + int.Parse(sl_gnt_tq)).ToString();
                    tong_tiennop = (double.Parse(so_tien_ntdt) + double.Parse(so_tien_tq)).ToString();


                    MSG32 ms32 = new MSG32();
                    strMsg_Id = TCS_GetSequenceValue();
                    ms32.Body = new ProcMegService.MSG.MSG32.BODY();
                    ms32.Header = new ProcMegService.MSG.MSG32.HEADER();
                    ms32.Body.Row = new ProcMegService.MSG.MSG32.ROW();
                    ms32.Body.Row.Ctiet = new List<ProcMegService.MSG.MSG32.CTIET>();
                    //Gan du lieu header
                    ms32.Header.VERSION = strMessage_Version;
                    ms32.Header.SENDER_CODE = strSender_Code;
                    ms32.Header.SENDER_NAME = strSender_Name;
                    ms32.Header.RECEIVER_CODE = strReciver_Code;
                    ms32.Header.RECEIVER_NAME = strReciver_Name;
                    ms32.Header.TRAN_CODE = MessageType.strMSG32;
                    ms32.Header.MSG_ID = strMsg_Id;
                    ms32.Header.MSG_REFID = strMsgRef_Id;
                    ms32.Header.ID_LINK = strId_Link;
                    ms32.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                    ms32.Header.ORIGINAL_CODE = strOriginal_Code;
                    ms32.Header.ORIGINAL_NAME = strOriginal_Name;
                    ms32.Header.ORIGINAL_DATE = strOriginal_Date;
                    ms32.Header.ERROR_CODE = strError_Code;
                    ms32.Header.ERROR_DESC = "";
                    ms32.Header.SPARE1 = strSpare1;
                    ms32.Header.SPARE2 = strSpare2;
                    ms32.Header.SPARE3 = strSpare3;
                    //Gan du lieu body
                    ms32.Body.Row.TYPE = MessageType.strMSG32;
                    ms32.Body.Row.NAME = "Báo cáo tình hình nộp thuế điện tử";
                    ms32.Body.Row.TONG_GNT = tong_gnt;
                    ms32.Body.Row.TONG_TIENNOP = tong_tiennop;
                    ms32.Body.Row.TGIAN_GUI = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    ms32.Body.Row.BC_TU_NGAY = bc_tu_ngay;
                    ms32.Body.Row.BC_DEN_NGAY = bc_den_ngay;

                    tong_gnt_ref = tong_gnt;
                    tong_tien_ref = tong_tiennop;
                    tg_gui_ref = ms32.Body.Row.TGIAN_GUI;
                    tu_ngay_ref = bc_tu_ngay;
                    den_ngay_ref = bc_den_ngay;


                    //DataTable dtDetail = new DataTable();
                    //dtDetail = DataAccess.ExecuteReturnDataSet("select ma_lthue as ma_hthuc_nop, count(ma_lthue) as sl_gnt, sum(tong_tien) as so_tien from tcs_ctu_ntdt_hdr where tthai_ctu ='11' and TO_NUMBER(ngay_gui_gd) >= " + ngay_batdau_dc + " and TO_NUMBER(ngay_gui_gd) <= " + ngay_ketthuc_dc + " group by ma_lthue", CommandType.Text).Tables[0];
                    //if (dtDetail.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < dtDetail.Rows.Count; i++)
                    //    {
                    //        CTIET ctiet = new CTIET();
                    //        ctiet.MA_HTHUC_NOP = dtDetail.Rows[i]["MA_HTHUC_NOP"].ToString();
                    //        ctiet.SL_GNT = dtDetail.Rows[i]["SL_GNT"].ToString();
                    //        ctiet.SO_TIEN = dtDetail.Rows[i]["SO_TIEN"].ToString();
                    //        ms32.Body.Row.Ctiet.Add(ctiet);
                    //    }
                    //}

                    CTIET ctiet = new CTIET();
                    ctiet.MA_HTHUC_NOP = "00";
                    ctiet.SL_GNT = sl_gnt_ntdt;
                    ctiet.SO_TIEN = so_tien_ntdt;
                    ms32.Body.Row.Ctiet.Add(ctiet);

                    CTIET ctiet1 = new CTIET();
                    ctiet1.MA_HTHUC_NOP = "01";
                    ctiet1.SL_GNT = sl_gnt_tq;
                    ctiet1.SO_TIEN = so_tien_tq;
                    ms32.Body.Row.Ctiet.Add(ctiet1);


                    //tra lai ket qua chi tiet duoi dang datatable
                    DataTable dt = new DataTable();
                    dt.Columns.Add("kenh");
                    dt.Columns.Add("gnt");
                    dt.Columns.Add("tien");
                    DataRow dr = dt.NewRow();
                    dr["kenh"] = "01";
                    dr["gnt"] = sl_gnt_tq;
                    dr["tien"] = so_tien_tq;
                    dt.Rows.Add(dr);
                    dr = dt.NewRow();
                    dr["kenh"] = "00";
                    dr["gnt"] = sl_gnt_ntdt;
                    dr["tien"] = so_tien_ntdt;
                    dt.Rows.Add(dr);
                    dtKQ = dt;

                    //xoa du lieu doi chieu cu
                    string sql = "delete tcs_bcao_gip where ngay_dc='" + ngay_bc + "'";
                    DataAccess.ExecuteNonQuery(sql, CommandType.Text);
                    //luu ket qua doi chieu vao bang
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        sql = "insert into tcs_bcao_gip(ID,TONG_GNT,TONG_TIEN,TU_NGAY_DC,DEN_NGAY_DC,NGAY_DC,NGAY_GUI_BC,KENH,SO_GNT,SO_TIEN) values(1, " + tong_gnt + ", " + tong_tiennop + ", '" + bc_tu_ngay + "','" + bc_den_ngay + "','" + ngay_bc + "', '" + ms32.Body.Row.TGIAN_GUI + "' ,'" + dt.Rows[i]["kenh"].ToString() + "' , " + dt.Rows[i]["gnt"] + ", " + dt.Rows[i]["tien"] + ")";
                        DataAccess.ExecuteNonQuery(sql, CommandType.Text);
                    }

                    //Chuyen thanh XML
                    string strXMLOut = ms32.MSG32toXML(ms32);

                    strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                    strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                    strXMLOut = strXMLOut.Replace(strXMLdefout4, String.Empty);
                    //Ky tren msg
                    strXMLOut = SignXML(strXMLOut);
                    //Goi den Web Service
                    string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG32);
                    if (verifySignXML(strXMLIn))
                    {
                        MSG33 msg33 = new MSG33();
                        strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                        MSG33 objTemp = msg33.MSGToObject(strXMLIn);
                        if (objTemp != null)
                        {
                            pv_trangthai = objTemp.Body.Row.Phanhoi.TRANG_THAI;
                            pv_mota_tt = objTemp.Body.Row.Phanhoi.MOTA_TRANGTHAI;
                        }
                        else
                        {
                            pv_trangthai = "-1";
                            pv_mota_tt = "không load được object trả về";
                        }
                    }
                    else
                    {
                        throw new Exception("Sai chu ky");
                    }
                }
                else
                {
                    throw new Exception("Ngày báo không được để trống");
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }
        #endregion

        public static string signMsg(String msg)
        {
            msg = msg.Replace("\r\n        ", String.Empty);
            msg = msg.Replace("\r\n      ", String.Empty);
            msg = msg.Replace("\r\n    ", String.Empty);
            msg = msg.Replace("\r\n  ", String.Empty);
            msg = msg.Trim();
            Signature.SignatureProcess.ResultSign resultSign = Signature.SignatureProcess.Sign(msg);
            if (resultSign.SignCode == 0)
            {
                msg = "<DATA>" + msg + "<Security><Signature><SignatureValue>" + resultSign.SignMessage + "</SignatureValue></Signature></Security></DATA>";
                return msg;
            }
            else
            {
                throw new Exception("Loi ky tren msg: error_code: " + resultSign.SignCode + "; error_message: " + resultSign.SignMessage);
            }
        }
        public static String sendMsg(String msg, String MSG_ID, String MSG_TYPE)
        {
            try
            {
                string strGIPON = "";
                String strMsgRes = "";
                try
                {
                    strGIPON = ConfigurationManager.AppSettings["GIP.ON"];
                }
                catch (Exception ex)
                {
                    strGIPON = "";
                }
                log.Info("1.sendMsg");
                if (strGIPON.Equals("0"))
                {
                    GIPTEST.DPWS_GIP svr = new GIPTEST.DPWS_GIP();
                   
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    svr.Url = gipURL;
                    strMsgRes = svr.xuLyTruyVanMsg(msg);
                }
                else
                {
                    string GIPT2B = "0";
                    try
                    {
                        GIPT2B = ConfigurationManager.AppSettings.Get("GIP.T2B").ToString();
                    }
                    catch (Exception exc) { GIPT2B = "0"; }
                    if (GIPT2B.Equals("1"))
                    {
                        strMsgRes = SendMSGT2B(msg);
                    }
                    else if (GIPT2B.Equals("2"))
                    {
                        strMsgRes = SendMSGT2B_new(msg);
                    }
                    else
                    {
                        log.Info("2.sendMsg-gipURL: " + gipURL);
                        //LogDebug.WriteLog("3 Truoc gui thue", EventLogEntryType.Error);
                        GIP_WS ws = new GIP_WS(gipURL);
                        //string strPublicKey = Signature.SignatureProcess.getPublicKey();
                        string strPublicKey = Signature.SignatureProcess.getPublicKey();
                        log.Info("3.sendMsg");
                        try
                        { //try TLS 1.3
                            ServicePointManager.SecurityProtocol = (SecurityProtocolType)12288
                                                                 | (SecurityProtocolType)3072
                                                                 | (SecurityProtocolType)768
                                                                 | SecurityProtocolType.Tls;
                        }
                        catch (NotSupportedException)
                        {
                            try
                            { //try TLS 1.2
                                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072
                                                                     | (SecurityProtocolType)768
                                                                     | SecurityProtocolType.Tls;
                            }
                            catch (NotSupportedException)
                            {
                                try
                                { //try TLS 1.1
                                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)768
                                                                         | SecurityProtocolType.Tls;
                                }
                                catch (NotSupportedException)
                                { //TLS 1.0
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                                }
                            }
                        }
                        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                        strMsgRes = ws.xuLyTruyVanMsg(msg);
                        log.Info("4.sendMsg: " + strMsgRes);
                        //LogDebug.WriteLog("4 Sau gui thue", EventLogEntryType.Error);
                    }
                }
                //LogDebug.WriteLog("strMsgRes:" + strMsgRes, EventLogEntryType.Error);
                insertLog(msg, strMsgRes, MSG_ID, MSG_TYPE);
                return strMsgRes;
            }
            catch (Exception ex)
            {
                log.Error("Exception - sendMsg: " + ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("sendMsg_TCT: " + ex.Message, EventLogEntryType.Error);
                throw ex;
            }

        }

        /// <summary>
        /// ham nay dung cho ket noi T2B khong co user passs
        /// </summary>
        /// <param name="pv_msg"></param>
        /// <returns></returns>
        public static string SendMSGT2B(string pv_msg)
        {
            string str = "";
            try
            {

                Byte[] dataByte = Encoding.UTF8.GetBytes(getStringSendT2B(pv_msg));
                log.Info(getStringSendT2B(pv_msg));
                log.Info(gipURL);
              //  LogDebug.WriteLog(getStringSendT2B(pv_msg), EventLogEntryType.Information);
              //  LogDebug.WriteLog(gipURL, EventLogEntryType.Information);

                HttpWebRequest POSTRequest = WebRequest.Create(gipURL) as HttpWebRequest; //TryCast(WebRequest.Create(gipURL), HttpWebRequest)
                //Method type
                POSTRequest.Method = "POST";
                // Data type - message body coming in xml
                POSTRequest.ContentType = "text/xml";
                POSTRequest.KeepAlive = true;
                //POSTRequest.Timeout = WS_TIMEPOUT '5000
                //Content length of message body
                POSTRequest.ContentLength = dataByte.Length;

                // Get the request stream
                Stream POSTstream = POSTRequest.GetRequestStream();
                // Write the data bytes in the request stream
                POSTstream.Write(dataByte, 0, dataByte.Length);

                //Get response from server
                HttpWebResponse POSTResponse = POSTRequest.GetResponse() as HttpWebResponse;

                StreamReader reader = new StreamReader(POSTResponse.GetResponseStream(), Encoding.UTF8);
                string strRes = reader.ReadToEnd().ToString();
                XmlReaderSettings XSettings = new XmlReaderSettings();
                using (StringReader SReader = new StringReader(strRes))
                {

                    XmlReader X = XmlReader.Create(SReader, XSettings);
                    X.ReadToDescendant("return");
                    str = X.ReadElementString();

                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog(ex.Message, EventLogEntryType.Information);
                throw ex;
            }
            return str;

        }
        public static string SendMSGT2B_new(string pv_msg)
        {
            string str = "";
            try
            {

                Byte[] dataByte = Encoding.UTF8.GetBytes(getStringSendT2B_new(pv_msg));

                log.Info(getStringSendT2B(pv_msg));
                log.Info(gipURL);

                HttpWebRequest POSTRequest = WebRequest.Create(gipURL) as HttpWebRequest; //TryCast(WebRequest.Create(gipURL), HttpWebRequest)
                //Method type
                POSTRequest.Method = "POST";
                // Data type - message body coming in xml
                POSTRequest.ContentType = "text/xml";
                POSTRequest.KeepAlive = true;
                //POSTRequest.Timeout = WS_TIMEPOUT '5000
                //Content length of message body
                POSTRequest.ContentLength = dataByte.Length;

                // Get the request stream
                Stream POSTstream = POSTRequest.GetRequestStream();
                // Write the data bytes in the request stream
                POSTstream.Write(dataByte, 0, dataByte.Length);

                //Get response from server
                HttpWebResponse POSTResponse = POSTRequest.GetResponse() as HttpWebResponse;

                StreamReader reader = new StreamReader(POSTResponse.GetResponseStream(), Encoding.UTF8);
                string strRes = reader.ReadToEnd().ToString();
                XmlReaderSettings XSettings = new XmlReaderSettings();
                using (StringReader SReader = new StringReader(strRes))
                {

                    XmlReader X = XmlReader.Create(SReader, XSettings);
                    X.ReadToDescendant("return");
                    str = X.ReadElementString();

                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog(ex.Message, EventLogEntryType.Information);
                throw ex;
            }
            return str;

        }

        public static string getStringSendT2B(string strMSG)
        {
            StringBuilder strbuild = new StringBuilder();
            try
            {

                strbuild.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:gip=\"http://gip.request.com/\">");
                strbuild.Append("<soapenv:Header/><soapenv:Body>");
                strbuild.Append("<gip:xuLyTruyVanMsg>");
                strbuild.Append(" <in_msg><![CDATA[");
                strbuild.Append(strMSG);
                strbuild.Append("]]></in_msg>");
                strbuild.Append("</gip:xuLyTruyVanMsg>");
                strbuild.Append("</soapenv:Body>");
                strbuild.Append("</soapenv:Envelope>");

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                throw ex;
            }
            return strbuild.ToString();
        }

        public static string getStringSendT2B_new(string strMSG)
        {
            StringBuilder strbuild = new StringBuilder();
            try
            {
                string Created = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                string UserName = ConfigurationManager.AppSettings.Get("GIP.USERNAM");
                string Password = ConfigurationManager.AppSettings.Get("GIP.PASSWORD");
                byte[] nonce = GetNonce();

                string nonceStr = Convert.ToBase64String(nonce);


                strbuild.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:gip=\"http://gip.request.com/\">");

                strbuild.Append("<soapenv:Header>");
                strbuild.Append("<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" soapenv:mustUnderstand=\"1\"> ");
                strbuild.Append("<wsse:UsernameToken wsu:Id=\"UsernameToken-56BCA1C67B8F283DFA15434776645423\">");
                strbuild.Append("<wsse:Username>" + UserName + "</wsse:Username>");
                strbuild.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + CreateHashedPassword(nonce, Created, Password) + "</wsse:Password>");
                strbuild.Append("<wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonceStr + "</wsse:Nonce>");
                strbuild.Append("<wsu:Created>" + Created + "</wsu:Created>");
                strbuild.Append("</wsse:UsernameToken>");
                strbuild.Append("</wsse:Security>");
                strbuild.Append("</soapenv:Header><soapenv:Body>");
                strbuild.Append("<gip:xuLyTruyVanMsg>");
                strbuild.Append(" <in_msg><![CDATA[");
                strbuild.Append(strMSG);
                strbuild.Append("]]></in_msg>");
                strbuild.Append("</gip:xuLyTruyVanMsg>");
                strbuild.Append("</soapenv:Body>");
                strbuild.Append("</soapenv:Envelope>");

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strbuild.ToString();
        }
        private static byte[] GetNonce()
        {

            byte[] nonce = new byte[0x10];

            RandomNumberGenerator generator = new RNGCryptoServiceProvider();

            generator.GetBytes(nonce);

            return nonce;

        }

        private static string CreateHashedPassword(byte[] nonce, string created, string password)
        {

            byte[] createdBytes = Encoding.UTF8.GetBytes(created);

            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            byte[] combined = new byte[createdBytes.Length + nonce.Length + passwordBytes.Length];

            Buffer.BlockCopy(nonce, 0, combined, 0, nonce.Length);

            Buffer.BlockCopy(createdBytes, 0, combined, nonce.Length, createdBytes.Length);

            Buffer.BlockCopy(passwordBytes, 0, combined, nonce.Length + createdBytes.Length, passwordBytes.Length);



            return Convert.ToBase64String(SHA1.Create().ComputeHash(combined));

        }

        //public static String sendMsg(String msg, String MSG_ID, String MSG_TYPE)
        //{
        //    try
        //    {
        //        GIP_WS ws = new GIP_WS(gipURL);
        //        //string strPublicKey = Signature.SignatureProcess.getPublicKey();
        //        String strMsgRes = ws.xuLyTruyVanMsg(msg);
        //        insertLog(msg, strMsgRes, MSG_ID, MSG_TYPE);
        //        return strMsgRes;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}
        // xử lý chứng từ
        public static void CTUToMSG21(String pv_strMaNTT, String pv_strSHKB,
                                            String pv_strSoCT, String pv_strSoBT,
                                            String pv_strMaNV, ref MSG21 ms21)
        {

            //Lay thong tin Header CTU
            StringBuilder sbsql = new StringBuilder();
            DataSet ds = null;
            string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

            try
            {
                //Lay thong tin header
                sbsql.Append("SELECT   h.ten_nnthue ten_nnthue,");
                sbsql.Append("         h.ma_nnthue ma_nnt,");
                sbsql.Append("         h.dc_nnthue dc_nnthue,");
                sbsql.Append("         h.ma_nntien ma_nntien,");
                sbsql.Append("         h.ten_nntien ten_nntien,");
                sbsql.Append("         h.dc_nntien dc_nntien,");
                sbsql.Append("         h.tk_co tk_co,");
                sbsql.Append("         h.ma_ntk ma_ntk,");
                sbsql.Append("         h.ma_cqthu ma_cqthu,");
                sbsql.Append("         h.so_ct so_ct,");
                sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'dd/MM/yyyy') ngay_ct,");
                sbsql.Append("         h.ma_nh_b ma_nh_b");
                sbsql.Append("  FROM   tcs_ctu_hdr h");
                sbsql.Append(" WHERE   ((h.shkb = :shkb)");
                sbsql.Append("          AND (h.ma_nv = :ma_nv)");
                sbsql.Append("          AND (h.so_ct = '" + pv_strSoCT + "')");
                sbsql.Append("          AND (h.so_bt = :so_bt)");
                sbsql.Append("          AND (h.NGAY_KB = :ngay_kb))");
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[4];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SHKB";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = pv_strSHKB;

                p[1] = new OracleParameter();
                p[1].ParameterName = "MA_NV";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = pv_strMaNV;

                p[2] = new OracleParameter();
                p[2].ParameterName = "SO_BT";
                p[2].DbType = DbType.String;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = pv_strSoBT;
                p[3] = new OracleParameter();
                p[3].ParameterName = "ngay_kb";
                p[3].DbType = DbType.String;
                p[3].Direction = ParameterDirection.Input;
                p[3].Value = v_ngaylv;



                ds =DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không tìm thấy chứng từ!");
                }
                string strTinhChat_KhoanNop = "";
                ms21.Body.Row.CHUNGTU.MST = ds.Tables[0].Rows[0]["ma_nnt"].ToString();
                ms21.Body.Row.CHUNGTU.TEN_NNT = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                ms21.Body.Row.CHUNGTU.DIACHI_NNT = ds.Tables[0].Rows[0]["dc_nnthue"].ToString();
                ms21.Body.Row.CHUNGTU.MST_NNTHAY = ds.Tables[0].Rows[0]["ma_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.TEN_NNTHAY = ds.Tables[0].Rows[0]["ten_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.DIACHI_NNTHAY = ds.Tables[0].Rows[0]["dc_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.SO_TK_NHKBNN = ds.Tables[0].Rows[0]["tk_co"].ToString();
                ms21.Body.Row.CHUNGTU.CQ_QLY_THU = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
                ms21.Body.Row.CHUNGTU.SO_CHUNGTU = ds.Tables[0].Rows[0]["so_ct"].ToString();
                ms21.Body.Row.CHUNGTU.NGAY_CHUNGTU = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                if (ds.Tables[0].Rows[0]["ma_ntk"].ToString() == "1")
                {
                    strTinhChat_KhoanNop = "Nộp NSNN";
                }
                else
                {
                    strTinhChat_KhoanNop = "Nộp HT GTGT";
                }
                ms21.Body.Row.CHUNGTU.TINHCHAT_KHOAN_NOP = strTinhChat_KhoanNop;
                ms21.Body.Row.CHUNGTU.CQUAN_THAMQUYEN = strCoQuanTQ;
                ms21.Body.Row.CHUNGTU.MA_NHTM = ds.Tables[0].Rows[0]["ma_nh_b"].ToString();
                //Lay thong tin detail
                sbsql = new StringBuilder();
                sbsql.Append("SELECT   d.ky_thue,");
                sbsql.Append("         d.ma_chuong,");
                sbsql.Append("         d.ma_khoan,");
                sbsql.Append("         d.ma_tmuc,");
                sbsql.Append("        d.noi_dung,");
                sbsql.Append("         d.sotien");
                sbsql.Append("  FROM   tcs_ctu_dtl d");
                sbsql.Append(" WHERE   (    (d.shkb = :shkb)");
                sbsql.Append("          AND (d.ma_nv = :ma_nv)");
                // sbsql.Append("          AND (d.so_ct = :so_ct)");
                sbsql.Append("          AND (d.so_bt = :so_bt)");
                sbsql.Append("          AND (d.ngay_kb = :ngay_kb))");


                //Lay thong tin chi tiet chung tu

                ds =DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không tìm thấy chứng từ!");
                }
                List<ProcMegService.MSG.MSG21.CHUNGTU_CHITIET> listItems = new List<ProcMegService.MSG.MSG21.CHUNGTU_CHITIET>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ProcMegService.MSG.MSG21.CHUNGTU_CHITIET item = new ProcMegService.MSG.MSG21.CHUNGTU_CHITIET();
                    item.CHUONG = row["ma_chuong"].ToString();
                    item.TIEUMUC = row["ma_tmuc"].ToString();
                    item.THONGTIN_KHOANNOP = row["noi_dung"].ToString();
                    item.SO_TIEN = row["sotien"].ToString();
                    listItems.Add(item);
                }
                ms21.Body.Row.CHUNGTU.CHUNGTU_CHITIET = listItems;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                throw ex;

            }
            finally
            {
            }


        }

        //sonmt override xl chung tu
        public static void CTUToMSG21(String pv_strMaNTT, String pv_strSHKB,
                                            String pv_strSoCT, String pv_strSoBT,
                                            String pv_strMaNV, String ma_hthuc_nop, ref MSG21 ms21)
        {

            string loai_nnt = "";
            string dgiai_ht_nt = "";
            try
            {
                getMSG03(pv_strMaNTT);
                loai_nnt = DataAccess.ExecuteReturnDataSet("select * from tcs_dm_nnt where ma_nnt = '" + pv_strMaNTT + "' and rownum=1 order by TO_DATE(ngay_cap_mst, 'dd/MM/yyyy') desc", CommandType.Text).Tables[0].Rows[0]["ma_loai_nnt"].ToString();
            }
            catch (Exception e)
            {

                log.Error(e.Message + "-" + e.StackTrace);//
            }
            try
            {
                dgiai_ht_nt = DataAccess.ExecuteReturnDataSet("select * from tcs_dm_hthuc_nthue where ma_hthuc_nthue = '" + ma_hthuc_nop + "'", CommandType.Text).Tables[0].Rows[0]["dgiai_hthuc_nthue"].ToString();

            }
            catch (Exception e)
            {
                log.Error(e.Message + "-" + e.StackTrace);//
            }

            //Lay thong tin Header CTU
            StringBuilder sbsql = new StringBuilder();
            DataSet ds = null;
            string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

            try
            {
                //Lay thong tin header

                IDbDataParameter[] p = null;
                if (ma_hthuc_nop.Equals("01") || ma_hthuc_nop == "01")
                {
                    //Nop thue tai quay
                    sbsql.Append("SELECT   h.ten_nnthue ten_nnthue,");
                    sbsql.Append("         h.ma_nnthue ma_nnt,");
                    sbsql.Append("         h.dc_nnthue dc_nnthue,");
                    sbsql.Append("         h.ma_nntien ma_nntien,");
                    sbsql.Append("         h.ten_nntien ten_nntien,");
                    sbsql.Append("         h.dc_nntien dc_nntien,");
                    sbsql.Append("         h.tk_co tk_co,");
                    sbsql.Append("         h.ma_ntk ma_ntk,");
                    sbsql.Append("         h.ma_cqthu ma_cqthu,");
                    sbsql.Append("         h.so_ct so_ct,");
                    sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'dd/MM/yyyy') ngay_ct,");
                    sbsql.Append("         h.ma_nh_b ma_nh_b,");
                    sbsql.Append("         huyen.ten ten_huyen,");
                    sbsql.Append("         tinh.ten ten_tinh, h.shkb, h.ma_nt,h.ma_huyen,h.ma_tinh,");
                    sbsql.Append("         h.so_qd,h.dia_chi_ts,h.so_khung,h.so_may,to_char(h.ngay_kh_nh,'dd/MM/yyyy') as ngay_ntien,h.mathamchieu,h.dac_diem_ptien");
                    sbsql.Append("  FROM   tcs_ctu_hdr h, tcs_dm_xa huyen, tcs_dm_xa tinh");
                    sbsql.Append(" WHERE   ((h.shkb = :shkb)");
                    sbsql.Append("          AND (h.ma_nv = :ma_nv)");
                    sbsql.Append("          AND (h.so_ct = '" + pv_strSoCT + "')");
                    sbsql.Append("          AND (h.so_bt = :so_bt)");
                    sbsql.Append("          AND (h.ma_huyen = huyen.ma_xa(+))");
                    sbsql.Append("          AND (h.ma_tinh = tinh.ma_xa(+))");
                    sbsql.Append("          AND (h.NGAY_KB = :ngay_kb))");
                    p = new IDbDataParameter[4];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "SHKB";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = pv_strSHKB;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "MA_NV";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = pv_strMaNV;

                    p[2] = new OracleParameter();
                    p[2].ParameterName = "SO_BT";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = pv_strSoBT;
                    p[3] = new OracleParameter();
                    p[3].ParameterName = "ngay_kb";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = v_ngaylv;
                }
                else if (ma_hthuc_nop.Equals("00") || ma_hthuc_nop == "00")
                {
                    //nop thue dien tu
                    sbsql.Append("SELECT   h.ten_nnop ten_nnthue,");
                    sbsql.Append("         h.mst_nnop ma_nnt,");
                    sbsql.Append("         h.diachi_nnop dc_nnthue,");
                    sbsql.Append("         h.mst_nthay ma_nntien,");
                    sbsql.Append("         h.ten_nthay ten_nntien,");
                    sbsql.Append("         h.diachi_nthay dc_nntien,");
                    sbsql.Append("         h.stk_thu tk_co,");
                    sbsql.Append("         '' ma_ntk,");
                    sbsql.Append("         h.ma_cqthu ma_cqthu,");
                    sbsql.Append("         h.so_ctu so_ct,");
                    sbsql.Append("         to_char(ngay_lap,'dd/MM/yyyy') ngay_ct,");
                    sbsql.Append("         h.ma_nh_b ma_nh_b,");
                    sbsql.Append("         ten_huyen_nnop ten_huyen,");
                    sbsql.Append("         ten_tinh_nnop ten_tinh, ma_hieu_kbac shkb, '' ma_nt,'' ma_huyen,'' ma_tinh,");
                    sbsql.Append("         '' so_qd,'' dia_chi_ts,'' so_khung,'' so_may,to_char(sysdate,'dd/MM/yyyy') as ngay_ntien,'' mathamchieu,'' dac_diem_ptien");
                    sbsql.Append("  FROM   tcs_ctu_ntdt_hdr h");
                    sbsql.Append(" WHERE   ((h.so_ctu = :so_ct))");

                    p = new IDbDataParameter[1];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "so_ct";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = pv_strSoCT;
                }

                ds =DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không tìm thấy chứng từ!");
                }
                string strTinhChat_KhoanNop = "";
                ms21.Body.Row.CHUNGTU.MST = ds.Tables[0].Rows[0]["ma_nnt"].ToString();
                ms21.Body.Row.CHUNGTU.TEN_NNT = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                ms21.Body.Row.CHUNGTU.DIACHI_NNT = ds.Tables[0].Rows[0]["dc_nnthue"].ToString();
                ms21.Body.Row.CHUNGTU.MST_NNTHAY = ds.Tables[0].Rows[0]["ma_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.TEN_NNTHAY = ds.Tables[0].Rows[0]["ten_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.DIACHI_NNTHAY = ds.Tables[0].Rows[0]["dc_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.SO_TK_NHKBNN = ds.Tables[0].Rows[0]["tk_co"].ToString();
                ms21.Body.Row.CHUNGTU.CQ_QLY_THU = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
                ms21.Body.Row.CHUNGTU.SO_CHUNGTU = ds.Tables[0].Rows[0]["so_ct"].ToString();
                ms21.Body.Row.CHUNGTU.NGAY_CHUNGTU = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                if (ds.Tables[0].Rows[0]["ma_ntk"].ToString() == "1")
                {
                    strTinhChat_KhoanNop = "Nộp NSNN";
                }
                else if (ma_hthuc_nop.Equals("01"))
                {
                    strTinhChat_KhoanNop = "Nộp HT GTGT";
                }
                else
                {
                    //sonmt: nop thue dien tu
                    strTinhChat_KhoanNop = "";
                }
                ms21.Body.Row.CHUNGTU.TINHCHAT_KHOAN_NOP = strTinhChat_KhoanNop;
                ms21.Body.Row.CHUNGTU.CQUAN_THAMQUYEN = strCoQuanTQ;
                ms21.Body.Row.CHUNGTU.MA_NHTM = ds.Tables[0].Rows[0]["ma_nh_b"].ToString();

                //sonmt: bo sung truong moi
                //ms21.Body.Row.CHUNGTU.HUYEN = ds.Tables[0].Rows[0]["ten_huyen"].ToString();
                //ms21.Body.Row.CHUNGTU.TINH = ds.Tables[0].Rows[0]["ten_tinh"].ToString();
                ms21.Body.Row.CHUNGTU.HUYEN = ds.Tables[0].Rows[0]["ma_huyen"].ToString();
                ms21.Body.Row.CHUNGTU.TINH = ds.Tables[0].Rows[0]["ma_tinh"].ToString();

                ms21.Body.Row.CHUNGTU.SH_KBNN = ds.Tables[0].Rows[0]["shkb"].ToString();
                ms21.Body.Row.CHUNGTU.LOAI_TIEN = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                ms21.Body.Row.CHUNGTU.MA_HTHUC_NOP = ma_hthuc_nop;
                ms21.Body.Row.CHUNGTU.DIENGIAI_HTHUC_NOP = dgiai_ht_nt;
                ms21.Body.Row.CHUNGTU.LHINH_NNT = loai_nnt;


                //new
                ms21.Body.Row.CHUNGTU.MATHAMCHIEU = ds.Tables[0].Rows[0]["mathamchieu"].ToString();
                ms21.Body.Row.CHUNGTU.SO_QUYET_DINH = ds.Tables[0].Rows[0]["so_qd"].ToString();
                ms21.Body.Row.CHUNGTU.DIA_CHI_TS = ds.Tables[0].Rows[0]["dia_chi_ts"].ToString();
                ms21.Body.Row.CHUNGTU.SO_KHUNG = ds.Tables[0].Rows[0]["so_khung"].ToString();
                ms21.Body.Row.CHUNGTU.SO_MAY = ds.Tables[0].Rows[0]["so_may"].ToString();
                ms21.Body.Row.CHUNGTU.NGAY_NTIEN = ds.Tables[0].Rows[0]["ngay_ntien"].ToString();
                ms21.Body.Row.CHUNGTU.DAC_DIEM_PTIEN = ds.Tables[0].Rows[0]["dac_diem_ptien"].ToString(); 

                //Lay thong tin detail
                if (ma_hthuc_nop.Equals("01") || ma_hthuc_nop == "01")
                {
                    //Nop thue tai quay
                    sbsql = new StringBuilder();
                    sbsql.Append("SELECT   d.ky_thue,");
                    sbsql.Append("         d.ma_chuong,");
                    sbsql.Append("         d.ma_khoan,");
                    sbsql.Append("         d.ma_tmuc,");
                    sbsql.Append("        d.noi_dung,");
                    sbsql.Append("         d.sotien");
                    sbsql.Append("  FROM   tcs_ctu_dtl d");
                    sbsql.Append(" WHERE   (    (d.shkb = :shkb)");
                    sbsql.Append("          AND (d.ma_nv = :ma_nv)");
                    // sbsql.Append("          AND (d.so_ct = :so_ct)");
                    sbsql.Append("          AND (d.so_bt = :so_bt)");
                    sbsql.Append("          AND (d.ngay_kb = :ngay_kb))");
                }
                else if (ma_hthuc_nop.Equals("00") || ma_hthuc_nop == "00")
                {
                    //Nop thue dien tu
                    sbsql = new StringBuilder();
                    sbsql.Append("SELECT   d.kythue ky_thue,");
                    sbsql.Append("         d.ma_chuong,");
                    sbsql.Append("         '' ma_khoan,");
                    sbsql.Append("         d.ma_tmuc,");
                    sbsql.Append("        d.noi_dung,");
                    sbsql.Append("         d.sotien");
                    sbsql.Append("  FROM   tcs_ctu_ntdt_dtl d");
                    sbsql.Append(" WHERE   (    (d.so_ct = :so_ct) )");

                    p = new IDbDataParameter[1];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "so_ct";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = pv_strSoCT;

                }


                //Lay thong tin chi tiet chung tu

                ds =DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không tìm thấy chứng từ!");
                }
                List<ProcMegService.MSG.MSG21.CHUNGTU_CHITIET> listItems = new List<ProcMegService.MSG.MSG21.CHUNGTU_CHITIET>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ProcMegService.MSG.MSG21.CHUNGTU_CHITIET item = new ProcMegService.MSG.MSG21.CHUNGTU_CHITIET();
                    item.CHUONG = row["ma_chuong"].ToString();
                    item.TIEUMUC = row["ma_tmuc"].ToString();
                    item.THONGTIN_KHOANNOP = row["noi_dung"].ToString();
                    item.SO_TIEN = row["sotien"].ToString();
                    item.KY_THUE = row["ky_thue"].ToString();
                    listItems.Add(item);
                }
                ms21.Body.Row.CHUNGTU.CHUNGTU_CHITIET = listItems;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//

                throw ex;

            }
            finally
            {
            }


        }
        #region "Utility"
       
        /// <summary>
        /// Hàm chuyển từ dữ liệu kiểu ngày về dạng số (dd/MM/yyyy thành yyyyMMdd)
        /// </summary>
        /// <param name="strDate">Ngày cần chuyển với định dạng dd/MM/yyyy</param>
        /// <returns>Kết quả dạng yyyyMMdd</returns>
        /// <remarks>
        ///</remarks>
        private static long ConvertDateToNumber(string strDate)
        {
            //-----------------------------------------------------
            // Mục đích: Chuyển một string ngày tháng 'dd/MM/yyyy' --> dạng số 'yyyyMMdd'.
            // Tham số: string đầu vào
            // Giá trị trả về: 
            // Ngày viết: 18/10/2007
            // Người viết: Lê Hồng Hà
            // ----------------------------------------------------
            string[] arr = null;

            try
            {
                string sep = "/";
                strDate = strDate.Trim();
                if (strDate.Length < 10)
                    return 0;
                arr = strDate.Split(sep.ToCharArray());
                sep = "/";
                arr[0] = arr[0].PadLeft(2, '0');
                arr[1] = arr[1].PadLeft(2, '0');
                if (arr[2].Length == 2)
                {
                    arr[2] = "20" + arr[2];
                }

            }
            catch (Exception ex)
            {
                return Convert.ToInt64(DateTime.Now.ToString("yyyyMMdd"));
            }

            return long.Parse(arr[2] + arr[1] + arr[0]);
        }


        /// <summary>
        /// Hàm lấy thông tin ngày làm việc của hệ thống theo mã nhân viên
        /// </summary>
        /// <param name="strMa_NV">Mã nhân viên</param>
        /// <returns>Ngày làm việc của hệ thống</returns>
        /// <remarks>
        ///</remarks>
        private static string TCS_GetNgayLV(string strMa_NV)
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV'";
                return (ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString())).ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static string TCS_GetGIO_DC()
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='GIO_DC_CT'";
                return (DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString()).ToString();
            }
            catch (Exception ex)
            {
                return "Lỗi trong quá trình lấy giờ đối chiếu";
            }
        }


        /// <summary>
        /// Hàm lấy thông tin ngày làm việc của hệ thống theo mã nhân viên
        /// </summary>
        /// <param name="strMa_NV">Mã nhân viên</param>
        /// <returns>Ngày làm việc của hệ thống</returns>
        /// <remarks>
        ///</remarks>
        private static string TCS_GetSequenceValue()
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT '" + strSender_Code + "' || TO_CHAR (SYSDATE, 'RRRRMMDD') || tcs_ref_id_tct.NEXTVAL");
                sbSQL.Append(" FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                return strRet;
            }
            catch (Exception ex)
            {
                return strSender_Code + "00";
            }
        }
        private static Int64 TCS_GetSequence(string p_Seq)
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT ");
                sbSQL.Append(p_Seq);
                sbSQL.Append(".nextval FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                return Int64.Parse(strRet);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        #endregion "Utility"
        private static void insertLog(string strMsgOut, String strMsgIn, String msg_type, String desc)
        {
            OracleConnection conn =DataAccess.GetConnection();
            OracleCommand myCMD = new OracleCommand();
            try
            {

                StringBuilder sbSqlInsert = new StringBuilder("");

                String strS = "INSERT INTO tcs_log_msg_tct(id, msg_in, msg_out, date_send, description, msg_type) values (:id, :msg_in, :msg_out, :date_send, :description, :msg_type)";

                myCMD.Connection = conn;
                myCMD.CommandText = strS;
                myCMD.CommandType = CommandType.Text;
                myCMD.Parameters.Add("id", OracleType.Int16, 1).Value = 1;
                myCMD.Parameters.Add("msg_in", OracleType.Clob, 32767).Value = strMsgIn;
                myCMD.Parameters.Add("msg_out", OracleType.Clob, 32767).Value = strMsgOut;
                myCMD.Parameters.Add("date_send", OracleType.DateTime).Value = DateTime.Now;
                myCMD.Parameters.Add("description", OracleType.VarChar).Value = desc;
                myCMD.Parameters.Add("msg_type", OracleType.NVarChar).Value = msg_type;
                myCMD.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
            }
            finally
            {

                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    myCMD.Dispose();
                   

                    conn.Close();
                    conn.Dispose();
                }
            }
        }
        private static string GetValue_THAMSOHT(string strParmName)
        {
            string Result = "";
            try
            {
                string strSQL = "SELECT a.giatri_ts FROM tcs_thamso_ht a  where a.ten_ts ='" + strParmName + "' ";
                DataTable dt = null;
                dt = DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = dt.Rows[0]["GIATRI_TS"].ToString();
                }
                return Result;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return Result;
            }
        }
      
        public static bool verifySignXML(string strSignData)
        {
            string signatureType = GetValue_THAMSOHT("SHA.2.GIP");
            bool checkCK = true;
            if (signatureType.Equals("1"))
            {
                if (strSignData.Equals(String.Empty)) return false;
                checkCK = Signature.DigitalSignature.getVerify(strSignData, strSerialGIP1, strSerialGIP2);
            }
            else
            {
                checkCK = true;
            }
            return checkCK;
        }
        public static string SignXML(string strSignData)
        {
            string signatureType = GetValue_THAMSOHT("SIGN.TYPE.GIP");
            string xpathSign = "/DATA/SECURITY";
            if (signatureType.Equals("1"))
            {
                string result = Signature.DigitalSignature.getSign(strSignData, xpathSign);
                return result;
            }
            else
            {
                string result = Signature.SignatureProcess.SignXML(strSignData);
                return result;
            }
        }
        public static string getMSG05API(string MST)
        {
            try
            {
                strMsg_Id = TCS_GetSequenceValue();
                MSG05 ms5 = new MSG05();
                ms5.Body = new ProcMegService.MSG.MSG05.BODY();
                ms5.Header = new ProcMegService.MSG.MSG05.HEADER();
                ms5.Body.Row = new ProcMegService.MSG.MSG05.ROW();
                ms5.Body.Row.TruyVan = new ProcMegService.MSG.MSG05.TRUYVAN();
                //Gan du lieu header
                ms5.Header.VERSION = strMessage_Version;
                ms5.Header.SENDER_CODE = strSender_Code;
                ms5.Header.SENDER_NAME = strSender_Name;
                ms5.Header.RECEIVER_CODE = strReciver_Code;
                ms5.Header.RECEIVER_NAME = strReciver_Name;
                ms5.Header.TRAN_CODE = MessageType.strMSG05;
                ms5.Header.MSG_ID = strMsg_Id;
                ms5.Header.MSG_REFID = strMsgRef_Id;
                ms5.Header.ID_LINK = strId_Link;
                ms5.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                ms5.Header.ORIGINAL_CODE = strOriginal_Code;
                ms5.Header.ORIGINAL_NAME = strOriginal_Name;
                ms5.Header.ORIGINAL_DATE = strOriginal_Date;
                ms5.Header.ERROR_CODE = strError_Code;
                ms5.Header.ERROR_DESC = "";
                ms5.Header.SPARE1 = strSpare1;
                ms5.Header.SPARE2 = strSpare2;
                ms5.Header.SPARE3 = strSpare3;
                //Gan du lieu body
                ms5.Body.Row.TYPE = MessageType.strMSG05;
                ms5.Body.Row.NAME = "Truy vấn thu nộp theo MST";
                ms5.Body.Row.TruyVan.MST = MST;
                //Chuyen thanh XML
                string strXMLOut = ms5.MSG05toXML(ms5);

                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                //Ky tren msg

                //LogDebug.WriteLog(" SignXML  strXMLOut = " + strXMLOut, EventLogEntryType.Error);

                strXMLOut = SignXML(strXMLOut);

                //LogDebug.WriteLog(" End SignXML  strXMLOut = " + strXMLOut, EventLogEntryType.Error);

                string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG05);
                //string strXMLIn = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DATA><HEADER><VERSION>1</VERSION><SENDER_CODE>GIP</SENDER_CODE><SENDER_NAME>Hệ Thống GIP</SENDER_NAME><RECEIVER_CODE>01341001</RECEIVER_CODE><RECEIVER_NAME>NGAN HANG CITIBANK</RECEIVER_NAME><TRAN_CODE>05011</TRAN_CODE><MSG_ID>GIP201810182071852</MSG_ID><MSG_REFID/><ID_LINK/><SEND_DATE>10-18-2018</SEND_DATE><ORIGINAL_CODE>GIP</ORIGINAL_CODE><ORIGINAL_NAME>Hệ Thống GIP</ORIGINAL_NAME><ORIGINAL_DATE>10-18-2018</ORIGINAL_DATE><ERROR_CODE/><ERROR_DESC/><SPARE1/><SPARE2/><SPARE3/></HEADER><BODY><ROW><TYPE>00006</TYPE><NAME>Trả lời truy vấn thu nộp theo MST</NAME><THONGTIN_NNT><THONGTINCHUNG><ROW_NNT><MST>0123456789</MST><TEN_NNT>Công Ty Quế Ninh - (Tnhh)</TEN_NNT><LOAI_NNT>0100</LOAI_NNT><SO>125498944</SO><CHUONG>754</CHUONG><MA_CQT_QL>1054557</MA_CQT_QL></ROW_NNT></THONGTINCHUNG><DIACHI><ROW_DIACHI><MOTA_DIACHI>Xóm 01, Phường Đại Phúc</MOTA_DIACHI><MA_TINH>01TTT</MA_TINH><MA_HUYEN>009HH</MA_HUYEN><MA_XA>2230111</MA_XA></ROW_DIACHI></DIACHI><SOTHUE><ROW_SOTHUE><MA_CHUONG>754</MA_CHUONG><MA_CQ_THU>1054557</MA_CQ_THU><MA_TMUC>1701</MA_TMUC><NO_CUOI_KY>9000000000</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>03</LOAI_THUE></ROW_SOTHUE><ROW_SOTHUE><MA_CHUONG>754</MA_CHUONG><MA_CQ_THU>1054557</MA_CQ_THU><MA_TMUC>4911</MA_TMUC><NO_CUOI_KY>274500</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>13</LOAI_THUE></ROW_SOTHUE><ROW_SOTHUE><MA_CHUONG>757</MA_CHUONG><MA_CQ_THU>1054557</MA_CQ_THU><MA_TMUC>1701</MA_TMUC><NO_CUOI_KY>2600000</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>03</LOAI_THUE></ROW_SOTHUE></SOTHUE></THONGTIN_NNT></ROW></BODY><SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"><InclusiveNamespaces xmlns=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"#default\"/></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>N+LGMbFSFIOHLJ+ObkRrfGlemaYiedU6d8Mucf9YqM8=</DigestValue></Reference></SignedInfo><SignatureValue>KVksT5R/IwhAvWOogyNer0apgCMeLZzzTuIAzxLVlOsIEhL0m8gRSWtaG8ieyBGBY2Ph+rC3aBt2 xZ/K/yr82mdCgZ1P50Vok+TcgMnUKdle8nMRgUdp3Z3fFFdtITJzMeaAE5vRoSr0Y0Aq6oHNjxmF oC+GQHLeIlKHKb+3npE6EU2T45TbQBYTd248h0FH3Hv1HZD0Oq/jxPmEew8apNKgNrmEIPEcV+fC HBZ+VG62uWKokjib1pYvgxWGqXU4I/34lykScMLezrJazM5UVuw8LK8HM0NWZq48M7kprq/EhHqT aIwaetNGguRNJbifKGv+znhZKhVbeEly5OGxSA==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>rEBYcv+oGEwi54w4dEgbGHtTsExZEDeLxSU6Ql1XnX6xHsLh+IOpwXCT/NR02tl7V4Onfe7nmAgz WPFbObaV/2KL/IuV3sP/WWjHdXTme62Z4kAL9j00HBaJpt9+56BuSCxH9ZQyiARQq6vyOGVLlhnv Gj2vBCqcIphk3Y+F78AwgHE4VbbEVT4sggVUB+evH3HkZX8Ksh7RppiHIK2nxgsTg1ulqhV6IY3s uB14VSbOmevEZIp5u3Synz2zxINg4dtsvLxVia6DRChei4z8fiE9dUj3bei0+D2dPhjbJqVHnGVZ xqaV8hSF5QrPaKDW0HWA/HXVW+LlVtN/GJpTMQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIFEDCCA/igAwIBAgIQVAEBBQAAAABZpPu2HVQAXDANBgkqhkiG9w0BAQsFADA6MRMwEQYDVQQD DApWaWV0dGVsLUNBMRYwFAYDVQQKDA1WaWV0dGVsIEdyb3VwMQswCQYDVQQGEwJWTjAeFw0xODA2 MjUxMTE5MTBaFw0xOTA2MjUxMTE5MTBaMIGEMSIwIAYKCZImiZPyLGQBAQwSTVNUOjAxMDAyMzEy MjYtOTk4MSMwIQYDVQQDDBpU4buUTkcgQ+G7pEMgVEhV4bq+IFRFU1QwMTEcMBoGA1UECgwTVOG7 lE5HIEPhu6RDIFRIVeG6vjEOMAwGA1UECAwFSEFOT0kxCzAJBgNVBAYTAlZOMIIBIjANBgkqhkiG 9w0BAQEFAAOCAQ8AMIIBCgKCAQEArEBYcv+oGEwi54w4dEgbGHtTsExZEDeLxSU6Ql1XnX6xHsLh +IOpwXCT/NR02tl7V4Onfe7nmAgzWPFbObaV/2KL/IuV3sP/WWjHdXTme62Z4kAL9j00HBaJpt9+ 56BuSCxH9ZQyiARQq6vyOGVLlhnvGj2vBCqcIphk3Y+F78AwgHE4VbbEVT4sggVUB+evH3HkZX8K sh7RppiHIK2nxgsTg1ulqhV6IY3suB14VSbOmevEZIp5u3Synz2zxINg4dtsvLxVia6DRChei4z8 fiE9dUj3bei0+D2dPhjbJqVHnGVZxqaV8hSF5QrPaKDW0HWA/HXVW+LlVtN/GJpTMQIDAQABo4IB xTCCAcEwNgYIKwYBBQUHAQEEKjAoMCYGCCsGAQUFBzABhhpodHRwOi8vb2NzcDIudmlldHRlbC1j YS52bjAdBgNVHQ4EFgQUElhy2PvfweFHXx3hZpZa2ruD0eYwDAYDVR0TAQH/BAIwADAfBgNVHSME GDAWgBRMA8IT5aaiOoiPD1Uz/Osyde+qRDCBrAYDVR0gBIGkMIGhMIGeBgorBgEEAYHtAwEFMIGP MEoGCCsGAQUFBwICMD4ePABUAGgAaQBzACAAaQBzACAAYQBjAGMAcgBlAGQAaQB0AGUAZAAgAGMA ZQByAHQAaQBmAGkAYwBhAHQAZTBBBggrBgEFBQcCARY1aHR0cDovL3ZpZXR0ZWwtY2Eudm4vdXBs b2Fkcy9maWxlL2Rvd25sb2FkL0NQLUNQUy5wZGYwegYDVR0fBHMwcTBvoC2gK4YpaHR0cDovL2Ny bC52aWV0dGVsLWNhLnZuL1ZpZXR0ZWwtQ0EtMi5jcmyiPqQ8MDoxEzARBgNVBAMMClZpZXR0ZWwt Q0ExFjAUBgNVBAoMDVZpZXR0ZWwgR3JvdXAxCzAJBgNVBAYTAlZOMA4GA1UdDwEB/wQEAwIF4DAN BgkqhkiG9w0BAQsFAAOCAQEAriMfWZg/O/iDL+8ujF+DwYVBh60glVIxUy+UuTSZ9/t9YGuT3jCI a86/6LL9Ge4hI8tDAlrqYz79PXns6fTxxfzRCyLa/ChzoGjLblF+wuisXx1zp68RBonkC39VRCbS zRzDqeNwF7sE475w+1PG5ctssgJKqU0X3HF2JWkZn82Fe7SvopP/RplJOTNkxQp0X/32e6suQz4k SqgjAMEK1UPgX0g77lVvyZdcfhf0Z3N3I4fCgBetv7flAgV2D+t/f6jXWfcvZq9TKkDbZgdsHl04 4zSxcXZdA3KbDQjxxcxgyVEmN3mCP8pfSB1N98KxPZvFfNTr8CIqeWFs4c7YOw==</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";

                return strXMLIn;

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                throw ex;
            }
            return "";
        }

        public static MSG.MSG047.infoTRUYVANTHUE TruyVanSothue02(string MST, string Ma_LThue)
        {
            try
            {
                strMsg_Id = TCS_GetSequenceValue();
                MSG05 ms5 = new MSG05();
                ms5.Body = new ProcMegService.MSG.MSG05.BODY();
                ms5.Header = new ProcMegService.MSG.MSG05.HEADER();
                ms5.Body.Row = new ProcMegService.MSG.MSG05.ROW();
                ms5.Body.Row.TruyVan = new ProcMegService.MSG.MSG05.TRUYVAN();
                //Gan du lieu header
                ms5.Header.VERSION = strMessage_Version;
                ms5.Header.SENDER_CODE = strSender_Code;
                ms5.Header.SENDER_NAME = strSender_Name;
                ms5.Header.RECEIVER_CODE = strReciver_Code;
                ms5.Header.RECEIVER_NAME = strReciver_Name;
                //em code fix doan nay 
                ms5.Header.TRAN_CODE = "05010"; //MessageType.strMSG05;
                ms5.Header.MSG_ID = strMsg_Id;
                ms5.Header.MSG_REFID = strMsgRef_Id;
                ms5.Header.ID_LINK = strId_Link;
                ms5.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                ms5.Header.ORIGINAL_CODE = strOriginal_Code;
                ms5.Header.ORIGINAL_NAME = strOriginal_Name;
                ms5.Header.ORIGINAL_DATE = strOriginal_Date;
                ms5.Header.ERROR_CODE = strError_Code;
                ms5.Header.ERROR_DESC = "";
                ms5.Header.SPARE1 = strSpare1;
                ms5.Header.SPARE2 = strSpare2;
                ms5.Header.SPARE3 = strSpare3;
                //Gan du lieu body
                //doi sang truy van theo type 046
                ms5.Body.Row.TYPE = "00128";//MessageType.strMSG05;
                ms5.Body.Row.NAME = "Truy vấn sổ thuế cá nhân";
                ms5.Body.Row.TruyVan.MST = MST;
                ms5.Body.Row.TruyVan.MA_CQ_THU = "";
                ms5.Body.Row.TruyVan.LOAITHUE = "13";

                //Chuyen thanh XML
                string strXMLOut = ms5.MSG05toXML(ms5);

                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                strXMLOut = gf_MessageOut_Format(strXMLOut);
                //Ky tren msg

                strXMLOut = SignXML(strXMLOut);


                string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG05);
                //string strXMLIn = "<DATA><HEADER><VERSION>1</VERSION><SENDER_CODE>GIP</SENDER_CODE><SENDER_NAME>Hệ Thống GIP</SENDER_NAME><RECEIVER_CODE>DVCQG</RECEIVER_CODE><RECEIVER_NAME>Dịch vụ công Quốc gia</RECEIVER_NAME><TRAN_CODE>05011</TRAN_CODE><MSG_ID>GIP202205102505674</MSG_ID><MSG_REFID/><ID_LINK/><SEND_DATE>05-10-2022</SEND_DATE><ORIGINAL_CODE>GIP</ORIGINAL_CODE><ORIGINAL_NAME>Hệ Thống GIP</ORIGINAL_NAME><ORIGINAL_DATE>05-10-2022</ORIGINAL_DATE><ERROR_CODE/><ERROR_DESC/><SPARE1/><SPARE2/><SPARE3/></HEADER><BODY><ROW><TYPE>00129</TYPE><NAME>Trả lời truy vấn thu nộp theo MST</NAME><THONGTIN_NNT><THONGTINCHUNG><ROW_NNT><MST>0123456789</MST><TEN_NNT>Nguyễn Văn Thành</TEN_NNT><LOAI_NNT>0900</LOAI_NNT><SO_CMND>141564390</SO_CMND><SO_CCCD/><CHUONG>757</CHUONG><MA_CQT_QL>1054557</MA_CQT_QL></ROW_NNT></THONGTINCHUNG><DIACHI><ROW_DIACHI><MOTA_DIACHI>B8 240 Lê Lợi, p7</MOTA_DIACHI><MA_TINH>01TTT</MA_TINH><MA_HUYEN>009HH</MA_HUYEN><MA_XA/></ROW_DIACHI></DIACHI><SOTHUE><ROW_SOTHUE><MA_CHUONG>557</MA_CHUONG><MA_CQ_THU>1054557</MA_CQ_THU><TEN_CQ_THU>Cục thuế Thành phố Ha Noi</TEN_CQ_THU><SHKB>0111</SHKB><MA_DBHC/><MA_TMUC>4917</MA_TMUC><TEN_TMUC>Tiền chậm nộp thuế thu nhập cá nhân</TEN_TMUC><NO_CUOI_KY>748889</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>13</LOAI_THUE></ROW_SOTHUE></SOTHUE></THONGTIN_NNT></ROW></BODY><SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"><InclusiveNamespaces xmlns=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"#default\"/></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><DigestValue>XmfzvVvJK2oCFhjnZqNjwdrP12g=</DigestValue></Reference></SignedInfo><SignatureValue>Fp0h/PB5oqnJd4GVSBSXiWSxzAhs9zGumlBQVg3XzHvGI3iGukXQxeCMW1A6YBH487MDeHta2Yoy 6WZSfy8PKhbgzxIqm1pAg7PRo+aQWg8IbpIwUpa8WVc1j/YSxr8YTYICkpYqHUapEIIKwuOwqU7Q sLfc1tHxHlBnVRZzF5/sESwmJG1V84VDa7R/lx83PP6D+zG7VC1kacjqV+kuh7fipSiKovvlcW7K IAua7UResayUFgp2XGHzOujcIzMJphPuOjIUBX7ct5Tu7ECnmsVdyIuVShSqYStyFyV6GJg19DUM OJEPZ0NagM+HIsD06sx6o9pNAc3MBDGFMpXIZA==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>389R3V8CsclSouIjOk5VQHSMtUr74Bo5H2txfSmGowrmJcuo2oknyGG4Dv7lPU1M3V3GKtub3EJB gHscj2z51IOOR+j6zZq/7w9srrxFEg7jRAFZX5wfh/ut86rzaZSBU/2hwYYaWpy829AJ+GRfeSM+ AQZuN7wmS0+t4TwakGmE2ep8d4+jcXYmA+0FeYEAqwlhMJYPbgW+NjPMMB+DTe73BkPqLrHM+KNE lBOH/RIw2RnVYP56eW/RqB84XE9JhEbLleC7gj9p01Nm1zQknL8jwDexPpDJCsCOl2HLehGZeozk n/2fhF/IlcrjZ1uAZ0xhwfs5TJMS+yy3ZXQtYQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIFfjCCBGagAwIBAgIDazy1MA0GCSqGSIb3DQEBBQUAMFkxCzAJBgNVBAYTAlZOMR0wGwYDVQQK DBRCYW4gQ28geWV1IENoaW5oIHBodTErMCkGA1UEAwwiQ28gcXVhbiBjaHVuZyB0aHVjIHNvIEJv IFRhaSBjaGluaDAeFw0xNzA1MDQwODQ0NThaFw0yNzA1MDIwODQ0NThaMEQxCzAJBgNVBAYTAlZO MRcwFQYDVQQKDA5NU1Q6MDEwMDIzMTIyNjEcMBoGA1UEAwwTVOG7lW5nIGPhu6VjIFRodeG6vzCC ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAN/PUd1fArHJUqLiIzpOVUB0jLVK++AaOR9r cX0phqMK5iXLqNqJJ8hhuA7+5T1NTN1dxirbm9xCQYB7HI9s+dSDjkfo+s2av+8PbK68RRIO40QB WV+cH4f7rfOq82mUgVP9ocGGGlqcvNvQCfhkX3kjPgEGbje8JktPreE8GpBphNnqfHePo3F2JgPt BXmBAKsJYTCWD24FvjYzzDAfg03u9wZD6i6xzPijRJQTh/0SMNkZ1WD+enlv0agfOFxPSYRGy5Xg u4I/adNTZtc0JJy/I8A3sT6QyQrAjpdhy3oRmXqM5J/9n4RfyJXK42dbgGdMYcH7OUyTEvsst2V0 LWECAwEAAaOCAmIwggJeMAkGA1UdEwQCMAAwEQYJYIZIAYb4QgEBBAQDAgWgMAsGA1UdDwQEAwIE 8DApBgNVHSUEIjAgBggrBgEFBQcDAgYIKwYBBQUHAwQGCisGAQQBgjcUAgIwHwYJYIZIAYb4QgEN BBIWEFVzZXIgU2lnbiBvZiBCVEMwHQYDVR0OBBYEFCUeMA8n53T1rwdKoFXUxc0xquIcMIGVBgNV HSMEgY0wgYqAFJ44mtYplYlqBX8q/18Bl7RXMGayoW+kbTBrMQswCQYDVQQGEwJWTjEdMBsGA1UE CgwUQmFuIENvIHlldSBDaGluaCBwaHUxPTA7BgNVBAMMNENvIHF1YW4gY2h1bmcgdGh1YyBzbyBj aHV5ZW4gZHVuZyBDaGluaCBwaHUgKFJvb3RDQSmCAQMwCQYDVR0SBAIwADBfBggrBgEFBQcBAQRT MFEwHwYIKwYBBQUHMAGGE2h0dHA6Ly9vY3NwLmNhLmJ0Yy8wLgYIKwYBBQUHMAKGImh0dHA6Ly9j YS5idGMvcGtpL3B1Yi9jZXJ0L2J0Yy5jcnQwMAYJYIZIAYb4QgEEBCMWIWh0dHA6Ly9jYS5idGMv cGtpL3B1Yi9jcmwvYnRjLmNybDAwBglghkgBhvhCAQMEIxYhaHR0cDovL2NhLmJ0Yy9wa2kvcHVi L2NybC9idGMuY3JsMF4GA1UdHwRXMFUwJ6AloCOGIWh0dHA6Ly9jYS5idGMvcGtpL3B1Yi9jcmwv YnRjLmNybDAqoCigJoYkaHR0cDovL2NhLmdvdi52bi9wa2kvcHViL2NybC9idGMuY3JsMA0GCSqG SIb3DQEBBQUAA4IBAQAZaW+HR+tKHzW36+msqcSYjg+0F+fnEfz3xnpnfEzYYsdXieDOG/0Z7y0L prEQtj3mbbtAzWiF3DxCHGU1wrdJopvGuWV/LESNMnAJSlM8TZIPvW9ZcKEd4FNpMYBbTaY4pTPy JiPJJ7LAbUcmJfbswTHuYYZpDaXqKRDwDRE4scM6Gs9U8HawzDWVkGdU/8Fsrb9M9lSzlLDYTvgs +wdYyB4hRWkh9DMJshDsobb1OlFmSRNEpAbcCAx17uWnjGjsAL/Tx1FLuujspiWBYgnX1TcnRHeA hhCGDEFA4bYjn/c9sGNHTxEAEcCuxIcWpxEdmxqFNdaGYIHJzEQKi3Vb</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";

                if (verifySignXML(strXMLIn))
                {

                    MSG.MSG047.MSG047 msg06 = new MSG.MSG047.MSG047();
                    strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                    MSG.MSG047.MSG047 objTemp = msg06.MSGToObject(strXMLIn);
                    //Verify sign                
                    //doan nay em lay thong tin du lieu va loc bo cac anh thue truoc ba
                    //chay xoa sach cac phan thue truoc ba
                    MSG.MSG047.infoTRUYVANTHUE objReSult = new MSG.MSG047.infoTRUYVANTHUE();

                    objReSult.MST = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].MST;
                    objReSult.TEN_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].TEN_NNT;
                    objReSult.LOAI_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].LOAI_NNT;
                    objReSult.MA_CQT_QL = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].MA_CQT_QL;
                    objReSult.MOTA_DIACHI = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MOTA_DIACHI;
                    objReSult.MA_TINH = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MA_TINH;
                    objReSult.MA_HUYEN = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MA_HUYEN;
                    objReSult.MA_XA = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MA_XA;
                    objReSult.TEN_TINH = getTenDBHC(objReSult.MA_TINH);
                    objReSult.TEN_HUYEN = getTenDBHC(objReSult.MA_HUYEN);
                    objReSult.TEN_XA = getTenDBHC(objReSult.MA_XA);
                    objReSult.ROW_SOTHUE = new List<MSG.MSG047.ROW_SOTHUE>();
                    List<MSG.MSG047.ROW_SOTHUE> rows = new List<MSG.MSG047.ROW_SOTHUE>();
                    foreach (MSG.MSG047.ROW_SOTHUE vrow in objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue)
                    {
                        if (!vrow.LOAI_TIEN.Equals("VND"))
                            continue;
                        if (vrow.SO_KHUNG == null)
                            vrow.SO_KHUNG = "";
                        if (vrow.SO_MAY == null)
                            vrow.SO_MAY = "";
                        if (vrow.DAC_DIEM_PTIEN == null)
                            vrow.DAC_DIEM_PTIEN = "";
                        if (vrow.SO == null)
                            vrow.SO = "";
                        if (vrow.DIA_CHI_TS == null)
                            vrow.DIA_CHI_TS = "";
                        if (vrow.SO_KHUNG.Length > 0 || vrow.SO_MAY.Length > 0 || vrow.DAC_DIEM_PTIEN.Length > 0)
                            continue;

                        //check với thuế TB nhà đất
                        if (vrow.MA_HS == null)
                            vrow.MA_HS = "";
                        if (vrow.MA_GCN == null)
                            vrow.MA_GCN = "";
                        if (vrow.SO_THUADAT == null)
                            vrow.SO_THUADAT = "";
                        if (vrow.SO_TO_BANDO == null)
                            vrow.SO_TO_BANDO = "";
                        if (vrow.MA_HS.Length > 0 || vrow.MA_GCN.Length > 0 || vrow.SO_THUADAT.Length > 0 || vrow.SO_TO_BANDO.Length > 0)
                            continue;
                        //end

                        vrow.TEN_CQ_THU = getTEN_CQTHU(vrow.MA_CQ_THU);
                        if (vrow.SHKB == null)
                            vrow.SHKB = "";
                        if (vrow.SHKB.Length <= 0)
                            vrow.SHKB = getSHKB(vrow.MA_CQ_THU);
                        vrow.TEN_KB = getTEN_KBNN(vrow.SHKB);
                        vrow.NOI_DUNG = getNOIDUNGKT(vrow.MA_TMUC);
                        if (vrow.TEN_TMUC != null)
                        {
                            if (vrow.TEN_TMUC.Trim().Length > 0)
                            {
                                vrow.NOI_DUNG = vrow.TEN_TMUC;
                            }
                        }

                        if (vrow.KY_THUE == null)
                            vrow.KY_THUE = DateTime.Now.ToString("dd/MM/yyyy");
                        if (vrow.KY_THUE.Length <= 0)
                            vrow.KY_THUE = DateTime.Now.ToString("dd/MM/yyyy");
                        if (vrow.KY_THUE.Length < 10)
                            vrow.KY_THUE = "01/" + vrow.KY_THUE;
                        objReSult.ROW_SOTHUE.Add(vrow);
                    }
                    return objReSult;
                }
                else
                {
                    throw new Exception("Sai chu ky");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }
        public static MSG.MSG047.infoTRUYVANTHUE TruyVanSothue03(string MST, string So_ToKhai)
        {
            try
            {
                strMsg_Id = TCS_GetSequenceValue();
                MSG05 ms5 = new MSG05();
                ms5.Body = new ProcMegService.MSG.MSG05.BODY();
                ms5.Header = new ProcMegService.MSG.MSG05.HEADER();
                ms5.Body.Row = new ProcMegService.MSG.MSG05.ROW();
                ms5.Body.Row.TruyVan = new ProcMegService.MSG.MSG05.TRUYVAN();
                //Gan du lieu header
                ms5.Header.VERSION = strMessage_Version;
                ms5.Header.SENDER_CODE = strSender_Code;
                ms5.Header.SENDER_NAME = strSender_Name;
                ms5.Header.RECEIVER_CODE = strReciver_Code;
                ms5.Header.RECEIVER_NAME = strReciver_Name;
                //em code fix doan nay 
                ms5.Header.TRAN_CODE = "05010"; //MessageType.strMSG05;
                ms5.Header.MSG_ID = strMsg_Id;
                ms5.Header.MSG_REFID = strMsgRef_Id;
                ms5.Header.ID_LINK = strId_Link;
                ms5.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                ms5.Header.ORIGINAL_CODE = strOriginal_Code;
                ms5.Header.ORIGINAL_NAME = strOriginal_Name;
                ms5.Header.ORIGINAL_DATE = strOriginal_Date;
                ms5.Header.ERROR_CODE = strError_Code;
                ms5.Header.ERROR_DESC = "";
                ms5.Header.SPARE1 = strSpare1;
                ms5.Header.SPARE2 = strSpare2;
                ms5.Header.SPARE3 = strSpare3;
                //Gan du lieu body
                //doi sang truy van theo type 046
                ms5.Body.Row.TYPE = "00046";//MessageType.strMSG05;
                ms5.Body.Row.NAME = "Truy vấn thu nộp theo MST";
                ms5.Body.Row.TruyVan.MST = MST;
                //ms5.Body.Row.TruyVan.LOAITHUE = "12";
                ms5.Body.Row.TruyVan.SO_TKHAI = So_ToKhai;
                //Chuyen thanh XML
                string strXMLOut = ms5.MSG05toXML(ms5);

                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                strXMLOut = gf_MessageOut_Format(strXMLOut);
                //Ky tren msg

                strXMLOut = SignXML(strXMLOut);


                string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG05);
                //string strXMLIn = "<DATA><HEADER><VERSION>1</VERSION><SENDER_CODE>GIP</SENDER_CODE><SENDER_NAME>Hệ Thống GIP</SENDER_NAME><RECEIVER_CODE>01310001</RECEIVER_CODE><RECEIVER_NAME>Ngân hàng TMCP SHB</RECEIVER_NAME><TRAN_CODE>05011</TRAN_CODE><MSG_ID>GIP202209086596413</MSG_ID><MSG_REFID/><ID_LINK/><SEND_DATE>09-08-2022</SEND_DATE><ORIGINAL_CODE>GIP</ORIGINAL_CODE><ORIGINAL_NAME>Hệ Thống GIP</ORIGINAL_NAME><ORIGINAL_DATE>09-08-2022</ORIGINAL_DATE><ERROR_CODE/><ERROR_DESC/><SPARE1/><SPARE2/><SPARE3/></HEADER><BODY><ROW><TYPE>00047</TYPE><NAME>Kết quả truy vấn sổ thuế LPTB</NAME><THONGTIN_NNT><THONGTINCHUNG><ROW_NNT><MST>0123456789</MST><TEN_NNT>NGUYEN VAN A</TEN_NNT><LOAI_NNT/><SO_CMND>038097010828</SO_CMND><CHUONG>757</CHUONG><MA_CQT_QL/></ROW_NNT></THONGTINCHUNG><DIACHI><ROW_DIACHI><MOTA_DIACHI>THANH XUAN, HA NOI</MOTA_DIACHI><MA_TINH>01TTT</MA_TINH><MA_HUYEN>009HH</MA_HUYEN><MA_XA/></ROW_DIACHI></DIACHI><SOTHUE><ROW_SOTHUE><MST>0123456789</MST><MA_CHUONG>757</MA_CHUONG><MA_CQ_THU>1054557</MA_CQ_THU><SHKB>0011</SHKB><MA_TMUC>2824</MA_TMUC><NO_CUOI_KY>355800</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH>11522401110001176</SO_QDINH><NGAY_QDINH>22/07/2023</NGAY_QDINH><LOAI_TIEN>VND</LOAI_TIEN><TI_GIA/><LOAI_THUE>12</LOAI_THUE><DIA_CHI_TS/><SO_KHUNG>RLHJA392XNY247784</SO_KHUNG><SO_MAY>JA39E-2744760</SO_MAY><MA_DBHC>009HH</MA_DBHC><KY_THUE>09/2023</KY_THUE><DAC_DIEM_PTIEN>LoạiTS:Xe mô tô hai bánh, Nhãn hiệu:Honda, Số loại/Tên TM:JA392 WAVE a, Số BKS:111B</DAC_DIEM_PTIEN></ROW_SOTHUE></SOTHUE></THONGTIN_NNT></ROW></BODY></DATA>";

                if (verifySignXML(strXMLIn))
                {

                    MSG.MSG047.MSG047 msg06 = new MSG.MSG047.MSG047();
                    strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                    MSG.MSG047.MSG047 objTemp = msg06.MSGToObject(strXMLIn);
                    //Verify sign                
                    //doan nay em lay thong tin du lieu va loc bo cac anh thue truoc ba
                    //chay xoa sach cac phan thue truoc ba
                    MSG.MSG047.infoTRUYVANTHUE objReSult = new MSG.MSG047.infoTRUYVANTHUE();

                    objReSult.MST = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].MST;
                    objReSult.TEN_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].TEN_NNT;
                    objReSult.LOAI_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].LOAI_NNT;
                    objReSult.MA_CQT_QL = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].MA_CQT_QL;
                    objReSult.MOTA_DIACHI = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MOTA_DIACHI;
                    objReSult.MA_TINH = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MA_TINH;
                    objReSult.MA_HUYEN = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MA_HUYEN;
                    objReSult.MA_XA = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MA_XA;
                    objReSult.TEN_TINH = getTenDBHC(objReSult.MA_TINH);
                    objReSult.TEN_HUYEN = getTenDBHC(objReSult.MA_HUYEN);
                    objReSult.TEN_XA = getTenDBHC(objReSult.MA_XA);
                    objReSult.ROW_SOTHUE = new List<MSG.MSG047.ROW_SOTHUE>();
                    List<MSG.MSG047.ROW_SOTHUE> rows = new List<MSG.MSG047.ROW_SOTHUE>();
                    foreach (MSG.MSG047.ROW_SOTHUE vrow in objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue)
                    {
                        if (!vrow.LOAI_TIEN.Equals("VND"))
                            continue;
                        if (vrow.SO_KHUNG == null)
                            vrow.SO_KHUNG = "";
                        if (vrow.SO_MAY == null)
                            vrow.SO_MAY = "";
                        if (vrow.DAC_DIEM_PTIEN == null)
                            vrow.DAC_DIEM_PTIEN = "";
                        if (vrow.SO == null)
                            vrow.SO = "";
                        if (vrow.DIA_CHI_TS == null)
                            vrow.DIA_CHI_TS = "";
                        if (vrow.SO_KHUNG.Length <= 0 || vrow.SO_MAY.Length <= 0 || vrow.DAC_DIEM_PTIEN.Length <= 0)
                            continue;

                        //check với thuế TB nhà đất
                        if (vrow.MA_HS == null)
                            vrow.MA_HS = "";
                        if (vrow.MA_GCN == null)
                            vrow.MA_GCN = "";
                        if (vrow.SO_THUADAT == null)
                            vrow.SO_THUADAT = "";
                        if (vrow.SO_TO_BANDO == null)
                            vrow.SO_TO_BANDO = "";
                        if (vrow.MA_HS.Length > 0 || vrow.MA_GCN.Length > 0 || vrow.SO_THUADAT.Length > 0 || vrow.SO_TO_BANDO.Length > 0)
                            continue;
                        //end

                        vrow.TEN_CQ_THU = getTEN_CQTHU(vrow.MA_CQ_THU);
                        if (vrow.SHKB == null)
                            vrow.SHKB = "";
                        if (vrow.SHKB.Length <= 0)
                            vrow.SHKB = getSHKB(vrow.MA_CQ_THU);
                        vrow.TEN_KB = getTEN_KBNN(vrow.SHKB);
                        vrow.NOI_DUNG = getNOIDUNGKT(vrow.MA_TMUC);


                        if (vrow.KY_THUE == null)
                            vrow.KY_THUE = DateTime.Now.ToString("dd/MM/yyyy");
                        if (vrow.KY_THUE.Length <= 0)
                            vrow.KY_THUE = DateTime.Now.ToString("dd/MM/yyyy");
                        if (vrow.KY_THUE.Length < 10)
                            vrow.KY_THUE = "01/" + vrow.KY_THUE;
                        objReSult.ROW_SOTHUE.Add(vrow);
                    }
                    return objReSult;
                }
                else
                {
                    throw new Exception("Sai chu ky");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }
        public static MSG.MSG047.infoTRUYVANTHUE TruyVanSothue08(string ma_hs, string mst, string strCCCD, string strSOQD)
        {
            try
            {
                strMsg_Id = TCS_GetSequenceValue();
                MSG05 ms5 = new MSG05();
                ms5.Body = new ProcMegService.MSG.MSG05.BODY();
                ms5.Header = new ProcMegService.MSG.MSG05.HEADER();
                ms5.Body.Row = new ProcMegService.MSG.MSG05.ROW();
                ms5.Body.Row.TruyVan = new ProcMegService.MSG.MSG05.TRUYVAN();
                //Gan du lieu header
                ms5.Header.VERSION = strMessage_Version;
                ms5.Header.SENDER_CODE = strSender_Code;
                ms5.Header.SENDER_NAME = strSender_Name;
                ms5.Header.RECEIVER_CODE = strReciver_Code;
                ms5.Header.RECEIVER_NAME = strReciver_Name;
                //em code fix doan nay 
                ms5.Header.TRAN_CODE = "05010"; //MessageType.strMSG05;
                ms5.Header.MSG_ID = strMsg_Id;
                ms5.Header.MSG_REFID = strMsgRef_Id;
                ms5.Header.ID_LINK = strId_Link;
                ms5.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                ms5.Header.ORIGINAL_CODE = strOriginal_Code;
                ms5.Header.ORIGINAL_NAME = strOriginal_Name;
                ms5.Header.ORIGINAL_DATE = strOriginal_Date;
                ms5.Header.ERROR_CODE = strError_Code;
                ms5.Header.ERROR_DESC = "";
                ms5.Header.SPARE1 = strSpare1;
                ms5.Header.SPARE2 = strSpare2;
                ms5.Header.SPARE3 = strSpare3;
                //Gan du lieu body
                //doi sang truy van theo type 046
                ms5.Body.Row.TYPE = "00109";//MessageType.strMSG05;
                ms5.Body.Row.NAME = "Truy vấn sổ thuế nhà đất";

                //ms5.Body.Row.TruyVan.MST = mst;

                ms5.Body.Row.TruyVan.MA_HS = ma_hs;
                ms5.Body.Row.TruyVan.SO_QUYET_DINH = strSOQD;
                ms5.Body.Row.TruyVan.SO = strCCCD;
                //Chuyen thanh XML
                string strXMLOut = ms5.MSG05toXML(ms5);

                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                strXMLOut = gf_MessageOut_Format(strXMLOut);
                //Ky tren msg

                strXMLOut = SignXML(strXMLOut);


                string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG05);
                //string strXMLIn = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DATA><HEADER><VERSION>1</VERSION><SENDER_CODE>GIP</SENDER_CODE><SENDER_NAME>Hệ Thống GIP</SENDER_NAME><RECEIVER_CODE>DVCQG</RECEIVER_CODE><RECEIVER_NAME>Dịch vụ công Quốc gia</RECEIVER_NAME><TRAN_CODE>05011</TRAN_CODE><MSG_ID>GIP202205102590003</MSG_ID><MSG_REFID/><ID_LINK/><SEND_DATE>05-10-2022</SEND_DATE><ORIGINAL_CODE>GIP</ORIGINAL_CODE><ORIGINAL_NAME>Hệ Thống GIP</ORIGINAL_NAME><ORIGINAL_DATE>05-10-2022</ORIGINAL_DATE><ERROR_CODE/><ERROR_DESC/><SPARE1/><SPARE2/><SPARE3/></HEADER><BODY><ROW><TYPE>00110</TYPE><NAME>Trả lời truy vấn sổ thuế nhà đất</NAME><SOTHUE><TRANGTHAI>01</TRANGTHAI><MOTA_TRANGTHAI>Tồn tại mã hồ sơ và số CMND/CCCD và có còn nợ thuế đất</MOTA_TRANGTHAI><TONG_TIEN><MA_SO_THUE>0123456789</MA_SO_THUE><TT_NNT>1500000</TT_NNT><ROW_SOTHUE><MA_HS>047.10.13.H56-220221-0013</MA_HS><MA_GCN/><SO_THUADAT>756</SO_THUADAT><SO_TO_BANDO>1</SO_TO_BANDO><DIA_CHI_TS>Phường Phúc Tân</DIA_CHI_TS><MA_TINH_TS>01TTT</MA_TINH_TS><TEN_TINH_TS>Hà Nội</TEN_TINH_TS><MA_HUYEN_TS>002HH</MA_HUYEN_TS><TEN_HUYEN_TS>Quận Hoàn Kiếm</TEN_HUYEN_TS><MA_XA_TS>00037</MA_XA_TS><TEN_XA_TS>Xã Trường Tây</TEN_XA_TS><MA_DBHC>25648</MA_DBHC><MA_SO_THUE>0123456789</MA_SO_THUE><TEN_NNT>NGUYEN VAN A</TEN_NNT><SO>072201004300</SO><MOTA_DIACHI>Phường Phúc Tân</MOTA_DIACHI><MA_TINH>01TTT</MA_TINH><TEN_TINH>Hà Nội</TEN_TINH><MA_HUYEN>002HH</MA_HUYEN><TEN_HUYEN>Quận Hoàn Kiếm</TEN_HUYEN><MA_XA>00037</MA_XA><TEN_XA>Xã Trường Tây</TEN_XA><SO_QUYET_DINH>LTB2270911-TK0007800/TB-CCT</SO_QUYET_DINH><NGAY_QUYET_DINH>2022-05-10 15:20:23.0</NGAY_QUYET_DINH><MA_CHUONG>857</MA_CHUONG><MA_TMUC>2801</MA_TMUC><TEN_TMUC>Lệ phí trước bạ nhà đất</TEN_TMUC><LOAI_TK_NSNN>7111</LOAI_TK_NSNN><MA_CQUAN_THU>1054557</MA_CQUAN_THU><SO_CON_PHAI_NOP>1500000</SO_CON_PHAI_NOP><HAN_NOP>2022-06-08 00:00:00.0</HAN_NOP><SHKB>1911</SHKB></ROW_SOTHUE></TONG_TIEN><TONG_TIEN><MA_SO_THUE>8701682531</MA_SO_THUE><TT_NNT>6000000</TT_NNT><ROW_SOTHUE><MA_HS>047.10.13.H56-220221-0013</MA_HS><MA_GCN>DA 662574</MA_GCN><SO_THUADAT>756</SO_THUADAT><SO_TO_BANDO>1</SO_TO_BANDO><DIA_CHI_TS>Trường Phước</DIA_CHI_TS><MA_TINH_TS>709</MA_TINH_TS><TEN_TINH_TS>Tây Ninh</TEN_TINH_TS><MA_HUYEN_TS>70911</MA_HUYEN_TS><TEN_HUYEN_TS>Thị xã Hoà Thành</TEN_HUYEN_TS><MA_XA_TS>7091103</MA_XA_TS><TEN_XA_TS>Xã Trường Tây</TEN_XA_TS><MA_DBHC>25648</MA_DBHC><MA_SO_THUE>8701682531</MA_SO_THUE><TEN_NNT>Nguyễn Minh Hải Đăng</TEN_NNT><SO>072098000168</SO><MOTA_DIACHI>KP.Long Thới</MOTA_DIACHI><MA_TINH>709</MA_TINH><TEN_TINH>Tây Ninh</TEN_TINH><MA_HUYEN>70911</MA_HUYEN><TEN_HUYEN>Thị xã Hoà Thành</TEN_HUYEN><MA_XA>7091113</MA_XA><TEN_XA>Phường Long Thành Trung</TEN_XA><SO_QUYET_DINH>LTB2270911-TK0007799/TB-CCT</SO_QUYET_DINH><NGAY_QUYET_DINH>2022-05-10 15:20:23.0</NGAY_QUYET_DINH><MA_CHUONG>757</MA_CHUONG><MA_TMUC>1006</MA_TMUC><TEN_TMUC>Thuế thu nhập từ chuyển nhượng bất động sản, nhận thừa kế và nhận quà tặng là bất động sản</TEN_TMUC><LOAI_TK_NSNN>7111</LOAI_TK_NSNN><MA_CQUAN_THU>1054557</MA_CQUAN_THU><SO_CON_PHAI_NOP>6000000</SO_CON_PHAI_NOP><HAN_NOP>2022-06-08 00:00:00.0</HAN_NOP><SHKB>1911</SHKB></ROW_SOTHUE></TONG_TIEN></SOTHUE></ROW></BODY><SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"><InclusiveNamespaces xmlns=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"#default\"/></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><DigestValue>rsa/ajJRhQAWNa6KyUVKwXxLWN4=</DigestValue></Reference></SignedInfo><SignatureValue>M10nlzfcBwrsvXhgs0/knkIlLt3dXpscWhnt85YtV58SxcXfbXfUfw3xKc/xbeyTPl0VSf6VAu6d c16n9O9V41Ces/bCD9qTJZnTPvod/DswVaxFVhORuXppwVFWy+DtTuui/uUd9G0kQSNlCvBPPzkl JiY3uPBP3Ac0xFbbA4gOd2hflOeYJVG9W3nSMxkC76MXT1od9sjjf1iy3SeRapLOtIhc4HhHV4h8 eyLyTTOa4EihHu37+9e5Quc91kIQMXh3AUeykegup2IMuSrynD8G+7XSWI35c2ebuLSxAIdRwqQc Y0pCK7vUv3a08vfYq6DsP4n25yBE792ISszM+g==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>389R3V8CsclSouIjOk5VQHSMtUr74Bo5H2txfSmGowrmJcuo2oknyGG4Dv7lPU1M3V3GKtub3EJB gHscj2z51IOOR+j6zZq/7w9srrxFEg7jRAFZX5wfh/ut86rzaZSBU/2hwYYaWpy829AJ+GRfeSM+ AQZuN7wmS0+t4TwakGmE2ep8d4+jcXYmA+0FeYEAqwlhMJYPbgW+NjPMMB+DTe73BkPqLrHM+KNE lBOH/RIw2RnVYP56eW/RqB84XE9JhEbLleC7gj9p01Nm1zQknL8jwDexPpDJCsCOl2HLehGZeozk n/2fhF/IlcrjZ1uAZ0xhwfs5TJMS+yy3ZXQtYQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIFfjCCBGagAwIBAgIDazy1MA0GCSqGSIb3DQEBBQUAMFkxCzAJBgNVBAYTAlZOMR0wGwYDVQQK DBRCYW4gQ28geWV1IENoaW5oIHBodTErMCkGA1UEAwwiQ28gcXVhbiBjaHVuZyB0aHVjIHNvIEJv IFRhaSBjaGluaDAeFw0xNzA1MDQwODQ0NThaFw0yNzA1MDIwODQ0NThaMEQxCzAJBgNVBAYTAlZO MRcwFQYDVQQKDA5NU1Q6MDEwMDIzMTIyNjEcMBoGA1UEAwwTVOG7lW5nIGPhu6VjIFRodeG6vzCC ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAN/PUd1fArHJUqLiIzpOVUB0jLVK++AaOR9r cX0phqMK5iXLqNqJJ8hhuA7+5T1NTN1dxirbm9xCQYB7HI9s+dSDjkfo+s2av+8PbK68RRIO40QB WV+cH4f7rfOq82mUgVP9ocGGGlqcvNvQCfhkX3kjPgEGbje8JktPreE8GpBphNnqfHePo3F2JgPt BXmBAKsJYTCWD24FvjYzzDAfg03u9wZD6i6xzPijRJQTh/0SMNkZ1WD+enlv0agfOFxPSYRGy5Xg u4I/adNTZtc0JJy/I8A3sT6QyQrAjpdhy3oRmXqM5J/9n4RfyJXK42dbgGdMYcH7OUyTEvsst2V0 LWECAwEAAaOCAmIwggJeMAkGA1UdEwQCMAAwEQYJYIZIAYb4QgEBBAQDAgWgMAsGA1UdDwQEAwIE 8DApBgNVHSUEIjAgBggrBgEFBQcDAgYIKwYBBQUHAwQGCisGAQQBgjcUAgIwHwYJYIZIAYb4QgEN BBIWEFVzZXIgU2lnbiBvZiBCVEMwHQYDVR0OBBYEFCUeMA8n53T1rwdKoFXUxc0xquIcMIGVBgNV HSMEgY0wgYqAFJ44mtYplYlqBX8q/18Bl7RXMGayoW+kbTBrMQswCQYDVQQGEwJWTjEdMBsGA1UE CgwUQmFuIENvIHlldSBDaGluaCBwaHUxPTA7BgNVBAMMNENvIHF1YW4gY2h1bmcgdGh1YyBzbyBj aHV5ZW4gZHVuZyBDaGluaCBwaHUgKFJvb3RDQSmCAQMwCQYDVR0SBAIwADBfBggrBgEFBQcBAQRT MFEwHwYIKwYBBQUHMAGGE2h0dHA6Ly9vY3NwLmNhLmJ0Yy8wLgYIKwYBBQUHMAKGImh0dHA6Ly9j YS5idGMvcGtpL3B1Yi9jZXJ0L2J0Yy5jcnQwMAYJYIZIAYb4QgEEBCMWIWh0dHA6Ly9jYS5idGMv cGtpL3B1Yi9jcmwvYnRjLmNybDAwBglghkgBhvhCAQMEIxYhaHR0cDovL2NhLmJ0Yy9wa2kvcHVi L2NybC9idGMuY3JsMF4GA1UdHwRXMFUwJ6AloCOGIWh0dHA6Ly9jYS5idGMvcGtpL3B1Yi9jcmwv YnRjLmNybDAqoCigJoYkaHR0cDovL2NhLmdvdi52bi9wa2kvcHViL2NybC9idGMuY3JsMA0GCSqG SIb3DQEBBQUAA4IBAQAZaW+HR+tKHzW36+msqcSYjg+0F+fnEfz3xnpnfEzYYsdXieDOG/0Z7y0L prEQtj3mbbtAzWiF3DxCHGU1wrdJopvGuWV/LESNMnAJSlM8TZIPvW9ZcKEd4FNpMYBbTaY4pTPy JiPJJ7LAbUcmJfbswTHuYYZpDaXqKRDwDRE4scM6Gs9U8HawzDWVkGdU/8Fsrb9M9lSzlLDYTvgs +wdYyB4hRWkh9DMJshDsobb1OlFmSRNEpAbcCAx17uWnjGjsAL/Tx1FLuujspiWBYgnX1TcnRHeA hhCGDEFA4bYjn/c9sGNHTxEAEcCuxIcWpxEdmxqFNdaGYIHJzEQKi3Vb</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";

                if (verifySignXML(strXMLIn))
                {

                    MSG.MSG110.MSG110 msg110 = new MSG.MSG110.MSG110();
                    strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                    MSG.MSG110.MSG110 objTemp = msg110.MSGToObject(strXMLIn);
                    //Verify sign                
                    //doan nay gan du lieu vào msg 47
                    MSG.MSG047.infoTRUYVANTHUE objReSult = new MSG.MSG047.infoTRUYVANTHUE();

                    foreach (ProcMegService.MSG.MSG110.TONG_TIEN vrow in objTemp.Body.Row.SoThue.row_tongtien)
                    {
                        if (mst.Equals(vrow.MA_SO_THUE))
                        {

                            objReSult.MST = vrow.Row_Sothue[0].MA_SO_THUE;
                            objReSult.TEN_NNT = vrow.Row_Sothue[0].TEN_NNT;
                            objReSult.LOAI_NNT = "";
                            objReSult.MA_CQT_QL = ""; //vrow.Row_Sothue[0].MA_CQUAN_THU;
                            objReSult.MOTA_DIACHI = vrow.Row_Sothue[0].MOTA_DIACHI;
                            objReSult.MA_TINH = vrow.Row_Sothue[0].MA_TINH;
                            objReSult.MA_HUYEN = vrow.Row_Sothue[0].MA_HUYEN;
                            objReSult.MA_XA = vrow.Row_Sothue[0].MA_XA;

                            objReSult.TEN_TINH = getTenDBHC(objReSult.MA_TINH);
                            objReSult.TEN_HUYEN = getTenDBHC(objReSult.MA_HUYEN);
                            objReSult.TEN_XA = getTenDBHC(objReSult.MA_XA);

                            objReSult.ROW_SOTHUE = new List<MSG.MSG047.ROW_SOTHUE>();

                            MSG.MSG047.ROW_SOTHUE vrow_sothue = new MSG.MSG047.ROW_SOTHUE();

                            vrow_sothue.NO_CUOI_KY = vrow.Row_Sothue[0].SO_CON_PHAI_NOP;
                            vrow_sothue.MA_CHUONG = vrow.Row_Sothue[0].MA_CHUONG;
                            vrow_sothue.MA_CQ_THU = vrow.Row_Sothue[0].MA_CQUAN_THU;
                            vrow_sothue.MA_TMUC = vrow.Row_Sothue[0].MA_TMUC;
                            vrow_sothue.SHKB = vrow.Row_Sothue[0].SHKB;
                            vrow_sothue.SO_TAI_KHOAN_CO = vrow.Row_Sothue[0].LOAI_TK_NSNN;
                            vrow_sothue.LOAI_TIEN = "VND";
                            vrow_sothue.SO_QDINH = vrow.Row_Sothue[0].SO_QUYET_DINH;

                            string ngayQD = "";
                            if (vrow.Row_Sothue[0].NGAY_QUYET_DINH.Length > 10)
                            {
                                ngayQD = vrow.Row_Sothue[0].NGAY_QUYET_DINH.Substring(0, 10);
                                ngayQD = ngayQD.Replace("-", "");
                            }
                            vrow_sothue.NGAY_QDINH = FormatDateTime(ngayQD);

                            vrow_sothue.TI_GIA = "1";
                            vrow_sothue.TEN_CQ_THU = getTEN_CQTHU(vrow_sothue.MA_CQ_THU);
                            vrow_sothue.TEN_TMUC = vrow.Row_Sothue[0].TEN_TMUC;
                            vrow_sothue.TEN_KB = getTEN_KBNN(vrow_sothue.SHKB); ; //getTEN_KBNN
                            vrow_sothue.NOI_DUNG = vrow.Row_Sothue[0].TEN_TMUC;
                            vrow_sothue.LOAI_THUE = "08";
                            vrow_sothue.SO_KHUNG = "";
                            vrow_sothue.SO_MAY = "";
                            vrow_sothue.MA_DBHC = vrow.Row_Sothue[0].MA_DBHC;

                            if (vrow_sothue.KY_THUE == null)
                                vrow_sothue.KY_THUE = DateTime.Now.ToString("dd/MM/yyyy");
                            if (vrow_sothue.KY_THUE.Length <= 0)
                                vrow_sothue.KY_THUE = DateTime.Now.ToString("dd/MM/yyyy");
                            if (vrow_sothue.KY_THUE.Length < 10)
                                vrow_sothue.KY_THUE = "01/" + vrow_sothue.KY_THUE;

                            vrow_sothue.DAC_DIEM_PTIEN = "";

                            vrow_sothue.SO = vrow.Row_Sothue[0].SO;

                            vrow_sothue.DIA_CHI_TS = vrow.Row_Sothue[0].DIA_CHI_TS;
                            vrow_sothue.MA_HS = vrow.Row_Sothue[0].MA_HS;
                            vrow_sothue.MA_GCN = vrow.Row_Sothue[0].MA_GCN;
                            vrow_sothue.SO_THUADAT = vrow.Row_Sothue[0].SO_THUADAT;
                            vrow_sothue.SO_TO_BANDO = vrow.Row_Sothue[0].SO_TO_BANDO;

                            objReSult.ROW_SOTHUE.Add(vrow_sothue);
                        }

                    }

                    return objReSult;
                }
                else
                {
                    throw new Exception("Sai chu ky");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static MSG.MSG047.infoTRUYVANTHUE getMSG05NEW(string MST)
        {
            try
            {
                strMsg_Id = TCS_GetSequenceValue();

                log.Info("strMsg_Id getMSG05NEW: " + strMsg_Id);
                MSG05 ms5 = new MSG05();
                ms5.Body = new ProcMegService.MSG.MSG05.BODY();
                ms5.Header = new ProcMegService.MSG.MSG05.HEADER();
                ms5.Body.Row = new ProcMegService.MSG.MSG05.ROW();
                ms5.Body.Row.TruyVan = new ProcMegService.MSG.MSG05.TRUYVAN();
                //Gan du lieu header
                ms5.Header.VERSION = strMessage_Version;
                ms5.Header.SENDER_CODE = strSender_Code;
                ms5.Header.SENDER_NAME = strSender_Name;
                ms5.Header.RECEIVER_CODE = strReciver_Code;
                ms5.Header.RECEIVER_NAME = strReciver_Name;
                ms5.Header.TRAN_CODE = MessageType.strMSG05;
                ms5.Header.MSG_ID = strMsg_Id;
                ms5.Header.MSG_REFID = strMsgRef_Id;
                ms5.Header.ID_LINK = strId_Link;
                ms5.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                ms5.Header.ORIGINAL_CODE = strOriginal_Code;
                ms5.Header.ORIGINAL_NAME = strOriginal_Name;
                ms5.Header.ORIGINAL_DATE = strOriginal_Date;
                ms5.Header.ERROR_CODE = strError_Code;
                ms5.Header.ERROR_DESC = "";
                ms5.Header.SPARE1 = strSpare1;
                ms5.Header.SPARE2 = strSpare2;
                ms5.Header.SPARE3 = strSpare3;
                //Gan du lieu body
                ms5.Body.Row.TYPE = MessageType.strMSG05;
                ms5.Body.Row.NAME = "Truy vấn thu nộp theo MST";
                ms5.Body.Row.TruyVan.MST = MST;
                //Chuyen thanh XML
                string strXMLOut = ms5.MSG05toXML(ms5);
                log.Info("strXMLOut getMSG05NEW: " + strXMLOut);
                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                strXMLOut = gf_MessageOut_Format(strXMLOut);
                //Ky tren msg
                log.Info("strXMLOut1 getMSG05NEW: " + strXMLOut);
                strXMLOut = SignXML(strXMLOut);
                log.Info("strXMLOut2 getMSG05NEW: " + strXMLOut);

                string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG05);
                //string strXMLIn = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DATA><HEADER><VERSION>1</VERSION><SENDER_CODE>GIP</SENDER_CODE><SENDER_NAME>He Thong GIP</SENDER_NAME><RECEIVER_CODE>79305001</RECEIVER_CODE><RECEIVER_NAME>Ngan Hang TMCP EximBank</RECEIVER_NAME><TRAN_CODE>05011</TRAN_CODE><MSG_ID>GIP202201118270394</MSG_ID><MSG_REFID/><ID_LINK/><SEND_DATE>01-11-2022</SEND_DATE><ORIGINAL_CODE>GIP</ORIGINAL_CODE><ORIGINAL_NAME>He Thong GIP</ORIGINAL_NAME><ORIGINAL_DATE>01-11-2022</ORIGINAL_DATE><ERROR_CODE/><ERROR_DESC/><SPARE1/><SPARE2/><SPARE3/></HEADER><BODY><ROW><TYPE>00006</TYPE><NAME>Tr? l?i truy v?n thu n?p theo MST</NAME><THONGTIN_NNT><THONGTINCHUNG><ROW_NNT><MST>0123456789</MST><TEN_NNT>TONG CÔNG TY KHOÁNG SAN TKV-CTCP</TEN_NNT><LOAI_NNT>0100</LOAI_NNT><SO/><CHUONG>122</CHUONG><MA_CQT_QL>1054557</MA_CQT_QL></ROW_NNT></THONGTINCHUNG><DIACHI><ROW_DIACHI><MOTA_DIACHI>So 193 duong Nguyen Huy Tuong, Phuong Thanh Xuân Trung</MOTA_DIACHI><MA_TINH>01TTT</MA_TINH><MA_HUYEN>009HH</MA_HUYEN><MA_XA>1011107</MA_XA></ROW_DIACHI></DIACHI><SOTHUE><ROW_SOTHUE><MA_CHUONG>122</MA_CHUONG><MA_CQ_THU>1054557</MA_CQ_THU><MA_TMUC>4944</MA_TMUC><NO_CUOI_KY>358664595</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>13</LOAI_THUE></ROW_SOTHUE><ROW_SOTHUE><MA_CHUONG>122</MA_CHUONG><MA_CQ_THU>1054557</MA_CQ_THU><MA_TMUC>1603</MA_TMUC><NO_CUOI_KY>169314684</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>07</LOAI_THUE></ROW_SOTHUE><ROW_SOTHUE><MA_CHUONG>757</MA_CHUONG><MA_CQ_THU>1054449</MA_CQ_THU><MA_TMUC>1001</MA_TMUC><NO_CUOI_KY>37054000</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>02</LOAI_THUE></ROW_SOTHUE></SOTHUE></THONGTIN_NNT></ROW></BODY><SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"><InclusiveNamespaces xmlns=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"#default\"/></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><DigestValue>c8rsvYHQ5KcOaRvE2U3qd5lpFmU=</DigestValue></Reference></SignedInfo><SignatureValue>R6mERyrQ4iSxMLH+D4DCfJf/mfThtonuUiM9OY6tCVYc3GitVXu/noSHT+LJuZpJB8bdVxIjaLR/ GAHORkHkWGfGHqnK9avXjp0VRePl1uXDRWb3YueG0m27lwPh4RlHFIEHRfEekTALST5nL12rla1z nhzjXMmHOanFKvLwaq0UtxrCufgZOXlLvKxdG9J9Oh4g15I9aVmjx5pVePkyE30IpKGdmJDOakk6 bAdeaRmbWDNcFVuIRY2rF+W0kCnYbeertoGEeT8BbSeEnUlb+IHZO7heUG9WkeX1AScsVu0He9yS 7CoFDuFBgEm3x/lMlG1GChrhS9cwBUzneDoL6A==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>jCUIb7LZJ4D7xzAhSbImxFYc39MuN8OKcUsmZmKxjE1fle72nyNOAGO4NV8UanMHUDMvLoPgXFj9 J4OKeH9VoOOZ/UN3niRm66F2UmUDlOcNOzhNnsDex+/ypVT2LWQ/ex/lJA7nPDuKSkgnGdPBq2Wa dLOhhmlOorKk3YEuzs8hHznNOzQeERuOPI3MyKCXxwzj7BYQ3rCR6bHdBepwgVkxlmCdNjjcSGaz NjMwu+i/S6C40ZRLqXrtaHTfjo3Do/4VRGyP/sWP/o9qp5K9FTAKae5adUARjFrN3zta7yQ/pXPE uCKOwaAaUJw+7AgsB2X2wb34syj/P95BvG7A8Q==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIFOzCCBCOgAwIBAgIQVAEBBCSXdF+lr4pRDRgXXzANBgkqhkiG9w0BAQUFADBuMQswCQYDVQQG EwJWTjEYMBYGA1UEChMPRlBUIENvcnBvcmF0aW9uMR8wHQYDVQQLExZGUFQgSW5mb3JtYXRpb24g U3lzdGVtMSQwIgYDVQQDExtGUFQgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMjAxMDE5MTAx NzQ1WhcNMjQwNjAxMTEyNjE1WjCBgzELMAkGA1UEBhMCVk4xNjA0BgNVBAcMLVPhu5EgMTIzIEzD siDEkMO6YywgSGFpIELDoCBUcsawbmcsIEjDoCBO4buZaTEcMBoGA1UEAwwTVOG7lW5nIGPhu6Vj IFRodeG6vzEeMBwGCgmSJomT8ixkAQEMDk1TVDowMTAwMjMxMjI2MIIBIjANBgkqhkiG9w0BAQEF AAOCAQ8AMIIBCgKCAQEAjCUIb7LZJ4D7xzAhSbImxFYc39MuN8OKcUsmZmKxjE1fle72nyNOAGO4 NV8UanMHUDMvLoPgXFj9J4OKeH9VoOOZ/UN3niRm66F2UmUDlOcNOzhNnsDex+/ypVT2LWQ/ex/l JA7nPDuKSkgnGdPBq2WadLOhhmlOorKk3YEuzs8hHznNOzQeERuOPI3MyKCXxwzj7BYQ3rCR6bHd BepwgVkxlmCdNjjcSGazNjMwu+i/S6C40ZRLqXrtaHTfjo3Do/4VRGyP/sWP/o9qp5K9FTAKae5a dUARjFrN3zta7yQ/pXPEuCKOwaAaUJw+7AgsB2X2wb34syj/P95BvG7A8QIDAQABo4IBvTCCAbkw DAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSaphGlCw6MEi0SLfWW1uUXayA8LzCBqAYIKwYBBQUH AQEEgZswgZgwNwYIKwYBBQUHMAKGK2h0dHA6Ly9wdWJsaWMucm9vdGNhLmdvdi52bi9jcnQvbWlj bnJjYS5jcnQwOAYIKwYBBQUHMAKGLGh0dHA6Ly9kaWNodnVkaWVudHUuZnB0LmNvbS52bi9jcnQv ZnB0Y2EuY3J0MCMGCCsGAQUFBzABhhdodHRwOi8vb2NzcDIuZmlzLmNvbS52bjBPBgNVHSAESDBG MEQGCysGAQQBge0DAQQBMDUwMwYIKwYBBQUHAgEWJ2h0dHA6Ly9kaWNodnVkaWVudHUuZnB0LmNv bS52bi9jcHMuaHRtbDA0BgNVHSUELTArBggrBgEFBQcDAgYIKwYBBQUHAwQGCisGAQQBgjcKAwwG CSqGSIb3LwEBBTAnBgNVHR8EIDAeMBygGqAYhhZodHRwOi8vY3JsMi5maXMuY29tLnZuMB0GA1Ud DgQWBBToJK64+0KtnUhwFWL+iW2SF7eQETAOBgNVHQ8BAf8EBAMCBPAwDQYJKoZIhvcNAQEFBQAD ggEBADdI1XUQRm8SfCFRvK312m9lxH0dbFGbuRoxt3VvKT6TgLh8LKoon2zWwMBJPH7cjb9g31FC NAf/QI4aAcgUsFLJzxvCjW0LgxjsFL7mjJc0A/7fNr4jr0+Ov59Bz/u8OrLUaZ7fMwIqVsx/XfCM 1Op+te4+KIkWbbz7Fz+cKVEA9So+Y+UqSN/14Q8SALqWKjPXFYcxVd4wERSCyiE2DTEr+dgc9deB 1dcjGqmbfOG5knO4/abzIMW6bZHGYSCjTolanf+ORIhRRTbHRUinwqroNKqg4D2KyNqLLi2p6IMn S6eSU65TrOyyXbjveV7Bak/3pZu62tVWbP6+LjblHzQ=</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";

                if (verifySignXML(strXMLIn))
                {
                    log.Info("verifySignXML getMSG05NEW");
                    MSG.MSG047.MSG047 msg06 = new MSG.MSG047.MSG047();
                    strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);
                    MSG.MSG047.MSG047 objTemp = msg06.MSGToObject(strXMLIn);
                    //Verify sign                
                    //doan nay em lay thong tin du lieu va loc bo cac anh thue truoc ba
                    //chay xoa sach cac phan thue truoc ba
                    MSG.MSG047.infoTRUYVANTHUE objReSult = new MSG.MSG047.infoTRUYVANTHUE();

                    objReSult.MST = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].MST;
                    objReSult.TEN_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].TEN_NNT;
                    objReSult.LOAI_NNT = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].LOAI_NNT;
                    objReSult.MA_CQT_QL = objTemp.Body.Row.Thongtin_NNT.Thongtinchung.row_nnt[0].MA_CQT_QL;
                    objReSult.MOTA_DIACHI = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MOTA_DIACHI;
                    objReSult.MA_TINH = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MA_TINH;
                    objReSult.MA_HUYEN = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MA_HUYEN;
                    objReSult.MA_XA = objTemp.Body.Row.Thongtin_NNT.Diachi.Row_Diachi[0].MA_XA;
                    objReSult.TEN_TINH = getTenDBHC(objReSult.MA_TINH);
                    objReSult.TEN_HUYEN = getTenDBHC(objReSult.MA_HUYEN);
                    objReSult.TEN_XA = getTenDBHC(objReSult.MA_XA);
                    objReSult.ROW_SOTHUE = new List<MSG.MSG047.ROW_SOTHUE>();
                    List<MSG.MSG047.ROW_SOTHUE> rows = new List<MSG.MSG047.ROW_SOTHUE>();
                    foreach (MSG.MSG047.ROW_SOTHUE vrow in objTemp.Body.Row.Thongtin_NNT.Sothue.Row_Sothue)
                    {
                        if (!vrow.LOAI_TIEN.Equals("VND"))
                            continue;
                        if (vrow.SO_KHUNG == null)
                            vrow.SO_KHUNG = "";
                        if (vrow.SO_MAY == null)
                            vrow.SO_MAY = "";
                        if (vrow.DAC_DIEM_PTIEN == null)
                            vrow.DAC_DIEM_PTIEN = "";
                        if (vrow.SO == null)
                            vrow.SO = "";
                        if (vrow.DIA_CHI_TS == null)
                            vrow.DIA_CHI_TS = "";
                        if (vrow.SO_KHUNG.Length > 0 || vrow.SO_MAY.Length > 0 || vrow.DAC_DIEM_PTIEN.Length > 0)
                            continue;
                        vrow.TEN_CQ_THU = getTEN_CQTHU(vrow.MA_CQ_THU);
                        if (vrow.SHKB == null)
                            vrow.SHKB = "";
                        if (vrow.SHKB.Length <= 0)
                            vrow.SHKB = getSHKB(vrow.MA_CQ_THU);
                        vrow.TEN_KB = getTEN_KBNN(vrow.SHKB);
                        vrow.NOI_DUNG = getNOIDUNGKT(vrow.MA_TMUC);
                        if (vrow.KY_THUE == null)
                            vrow.KY_THUE = DateTime.Now.ToString("dd/MM/yyyy");
                        if (vrow.KY_THUE.Length <= 0)
                            vrow.KY_THUE = DateTime.Now.ToString("dd/MM/yyyy");
                        if (vrow.KY_THUE.Length < 10)
                            vrow.KY_THUE = "01/" + vrow.KY_THUE;
                        objReSult.ROW_SOTHUE.Add(vrow);
                    }
                    return objReSult;
                }
                else
                {
                    throw new Exception("Sai chu ky");
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception getMSG05NEW: " + ex.Message + "  - - -  " + ex.StackTrace);
                throw ex;
            }
        }

        public static string FormatDateTime(string strDateTime)
        {
            try
            {
                string year = strDateTime.Substring(0, 4);
                string month = strDateTime.Substring(4, 2);
                string day = strDateTime.Substring(6, 2);

                return day + "/" + month + "/" + year;

            }
            catch (Exception ex)
            {
                return strDateTime;
            }
            return "";
        }

        public static string getTEN_CQTHU(string strMaCQTHU, string strMaHQ = "")
        {
            try
            {
                string StrSQL = "SELECT A.TEN FROM TCS_DM_CQTHU A ";
                if (strMaHQ.Length > 0)
                {
                    StrSQL += " WHERE MA_HQ='" + strMaHQ + "'";
                }
                else
                {
                    StrSQL += " WHERE MA_CQTHU='" + strMaCQTHU + "'";
                }
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getSHKB(string strMaCQTHU, string strMaHQ = "")
        {
            try
            {
                string StrSQL = "SELECT A.SHKB FROM TCS_DM_CQTHU A ";
                if (strMaHQ.Length > 0)
                {
                    StrSQL += " WHERE MA_HQ='" + strMaHQ + "'";
                }
                else
                {
                    StrSQL += " WHERE MA_CQTHU='" + strMaCQTHU + "'";
                }
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getTEN_KBNN(string strSHKB)
        {
            try
            {
                string StrSQL = "SELECT A.TEN FROM TCS_DM_KHOBAC A WHERE SHKB='" + strSHKB + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getNOIDUNGKT(string strSHKB)
        {
            try
            {
                string StrSQL = "SELECT A.TEN FROM TCS_DM_MUC_TMUC A WHERE MA_TMUC='" + strSHKB + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }
        public static string getTenDBHC(string strSHKB)
        {
            try
            {
                string StrSQL = "Select Ten From TCS_DM_xa  where ma_xa='" + strSHKB + "'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(StrSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
            }
            catch (Exception ex)
            {
            }
            return "";
        }

        private static string gf_MessageOut_Format(string pvStrXML)
        {
            string vstrTMP = pvStrXML;// " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\"";
            try
            {
                vstrTMP = vstrTMP.Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", string.Empty);
                vstrTMP = vstrTMP.Replace(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", string.Empty);
                vstrTMP = vstrTMP.Replace(" xmlns=\"http://www.cpandl.com\"", string.Empty);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return vstrTMP;
        }

        public static void SendMSG21_MobiTCT(string pv_strMaThamChieu, string pv_strSoCTU, ref string pv_trangthai, ref string pv_mota_tt)
        {
            strMsg_Id = TCS_GetSequenceValue();
            MSG21MOBI ms21 = new MSG21MOBI();
            ms21.Header = new ProcMegService.MSG.MSG21MOBI.HEADER();
            ms21.Body = new ProcMegService.MSG.MSG21MOBI.BODY();
            ms21.Body.Row = new ProcMegService.MSG.MSG21MOBI.ROW();
            ms21.Body.Row.CHUNGTU = new ProcMegService.MSG.MSG21MOBI.CHUNGTU();
            ms21.Header.VERSION = strMessage_Version;
            ms21.Header.SENDER_CODE = strSender_Code;
            ms21.Header.SENDER_NAME = strSender_Name;
            ms21.Header.RECEIVER_CODE = strReciver_Code;
            ms21.Header.RECEIVER_NAME = strReciver_Name;
            ms21.Header.TRAN_CODE = MessageType.strMSG21;
            ms21.Header.MSG_ID = strMsg_Id;
            ms21.Header.MSG_REFID = strMsgRef_Id;
            ms21.Header.ID_LINK = strId_Link;
            ms21.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
            ms21.Header.ORIGINAL_CODE = strOriginal_Code;
            ms21.Header.ORIGINAL_NAME = strOriginal_Name;
            ms21.Header.ORIGINAL_DATE = strOriginal_Date;
            ms21.Header.ERROR_CODE = strError_Code;
            ms21.Header.ERROR_DESC = "";
            ms21.Header.SPARE1 = strSpare1;
            ms21.Header.SPARE2 = strSpare2;
            ms21.Header.SPARE3 = strSpare3;
            //Gan du lieu body
            ms21.Body.Row.TYPE = "00121";
            ms21.Body.Row.NAME = "Gửi thông tin chứng từ";
            log.Info("3.1. Prc_SendCTUToTCT - 1.SendMSG21_MobiTCT ");
            CTUToMSG21_MobiTCT(pv_strMaThamChieu, pv_strSoCTU, ref ms21);
            log.Info("3.1. Prc_SendCTUToTCT - 2.SendMSG21_MobiTCT ");
            //Chuyen thanh XML
            string strXMLOut = ms21.MSG21toXML(ms21);

            strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
            strXMLOut = gf_MessageOut_Format(strXMLOut);
            //Ky tren msg
            strXMLOut = SignXML(strXMLOut);
            log.Info("3.1. Prc_SendCTUToTCT - 3.SendMSG21_MobiTCT - SignXML " + strXMLOut);
            //Goi den Web Service
            string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG21);
            log.Info("3.1. Prc_SendCTUToTCT - 4.SendMSG21_MobiTCT - sendMsg " + strXMLIn);
            if (verifySignXML(strXMLIn))
            {
                log.Info("3.1. Prc_SendCTUToTCT - 5.verifySignXML ");
                MSG26 msg26 = new MSG26();
                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);   //inthe hotrack disable implement
                log.Info("3.1. Prc_SendCTUToTCT - 6.verifySignXML - strXMLIn " + strXMLIn);
                //strXMLIn = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DATA xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">\r\n   <HEADER>\r\n      <VERSION>1</VERSION>\r\n      <SENDER_CODE>GIP</SENDER_CODE>\r\n      <SENDER_NAME>H??? Th???ng GIP</SENDER_NAME>\r\n      <RECEIVER_CODE>01341001</RECEIVER_CODE>\r\n      <RECEIVER_NAME>01341001</RECEIVER_NAME>\r\n      <TRAN_CODE>05011</TRAN_CODE>\r\n      <MSG_ID>GIP202009292836298</MSG_ID>\r\n      <MSG_REFID/>\r\n      <ID_LINK/>\r\n      <SEND_DATE>09-29-2020</SEND_DATE>\r\n      <ORIGINAL_CODE>GIP</ORIGINAL_CODE>\r\n      <ORIGINAL_NAME>H??? Th???ng GIP</ORIGINAL_NAME>\r\n      <ORIGINAL_DATE>09-29-2020</ORIGINAL_DATE>\r\n      <ERROR_CODE/>\r\n      <ERROR_DESC/>\r\n      <SPARE1/>\r\n      <SPARE2/>\r\n      <SPARE3/>\r\n   </HEADER>\r\n   <BODY>\r\n      <ROW>\r\n         <TYPE>00027</TYPE>\r\n         <NAME>K???t qu??? Verify Ch??? k?? s???</NAME>\r\n         <CHUNGTU>\r\n            <TYPE_PHANHOI>00021</TYPE_PHANHOI>\r\n            <TRANG_THAI>01</TRANG_THAI>\r\n            <MOTA_TRANGTHAI>Thành công!!!</MOTA_TRANGTHAI>\r\n         </CHUNGTU>\r\n      </ROW>\r\n   </BODY>\r\n<SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"><InclusiveNamespaces xmlns=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"#default\"/></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>O+HHZWuoPIvU2FYZ/KP0MdcGF1LBUcAKdFc+rdiEcI0=</DigestValue></Reference></SignedInfo><SignatureValue>tpoV07oDG9DLEbkbPuUKjYZ7ksZt2rpZJWeeIm4m1PEJBtccf60d+Mf9TPFfbdPFeec7BwjQhUcb\r\nP4IEqwinTbY7S2QiaTCsqQmyQ7aVGWgeMDev+ZDr5Rkq+1Ttug6y9qpOzIFAJitYL0dxKCgmy85v\r\nw2RkOI34jn0dCpAVt9Q0JWoPR5k+33/ATXp8iY4KVU/c5bm/X7NYFSl0r22aE9GkpNAJZmAv0G9P\r\ngdlH42FCK6eQYVN3tiR6mO/FN26NAYnQ5lBgfkbK4UrlLJ76hU/OBlSW3X4/76M3WqvPdEngvqFB\r\nhPmcw+S60mtmMHb8a9xJPXR+gKTJkLi2iNYEzA==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>5Lb7DhwAfNbM1AfMyiz3oOAm71i0hLKyXqnXWU03pGMtkO+WISzBssGASia5kN2jnx0QguL2puKa\r\nm2Lnyt/L/pLyinIpMu4nOXm9hkijaRhmXIkVpWZJkDn0GkJ2x8MyU0QxG5E67Oby8iUepSViLicr\r\nCemHPSfOkMOeMnXsQdnyEAlmgNU/xIRMzW0Xm9xU0k9jSOGATr2A2A/IgT0z+dBcm67LArqsSfqB\r\nd/q8TFsh/7i9KAbH5vXSnB6h82kmNZHvxwGLt5meLXit2txyDdE0PQwayDUtEsroKd14vZ2ZmC2G\r\nRdFwR4zqUWeJ7gquXGOrZWkLPVxhs1vFsSGSgw==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIFdDCCBFygAwIBAgIQVAEBBAGPvYfN/dakG/hWSTANBgkqhkiG9w0BAQsFADBuMQswCQYDVQQG\r\nEwJWTjEYMBYGA1UEChMPRlBUIENvcnBvcmF0aW9uMR8wHQYDVQQLExZGUFQgSW5mb3JtYXRpb24g\r\nU3lzdGVtMSQwIgYDVQQDExtGUFQgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMjAwNjE1MDM1\r\nNzM1WhcNMjMwNjE1MDMyNzAwWjCBvDELMAkGA1UEBhMCVk4xEjAQBgNVBAgMCUjDoCBO4buZaTES\r\nMBAGA1UEBwwJSMOgIE7hu5lpMRwwGgYDVQQKDBNU4buVbmcgQ+G7pWMgVGh14bq/MSMwIQYDVQQD\r\nDBpU4buVbmcgQ+G7pWMgVGh14bq/IFRlc3QwMTEiMCAGCgmSJomT8ixkAQEMEk1TVDowMTAwMjMx\r\nMjI2LTk5ODEeMBwGCSqGSIb3DQEJARYPdGVzdEBmcHQuY29tLnZuMIIBIjANBgkqhkiG9w0BAQEF\r\nAAOCAQ8AMIIBCgKCAQEA5Lb7DhwAfNbM1AfMyiz3oOAm71i0hLKyXqnXWU03pGMtkO+WISzBssGA\r\nSia5kN2jnx0QguL2puKam2Lnyt/L/pLyinIpMu4nOXm9hkijaRhmXIkVpWZJkDn0GkJ2x8MyU0Qx\r\nG5E67Oby8iUepSViLicrCemHPSfOkMOeMnXsQdnyEAlmgNU/xIRMzW0Xm9xU0k9jSOGATr2A2A/I\r\ngT0z+dBcm67LArqsSfqBd/q8TFsh/7i9KAbH5vXSnB6h82kmNZHvxwGLt5meLXit2txyDdE0PQwa\r\nyDUtEsroKd14vZ2ZmC2GRdFwR4zqUWeJ7gquXGOrZWkLPVxhs1vFsSGSgwIDAQABo4IBvTCCAbkw\r\nDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSaphGlCw6MEi0SLfWW1uUXayA8LzCBqAYIKwYBBQUH\r\nAQEEgZswgZgwNwYIKwYBBQUHMAKGK2h0dHA6Ly9wdWJsaWMucm9vdGNhLmdvdi52bi9jcnQvbWlj\r\nbnJjYS5jcnQwOAYIKwYBBQUHMAKGLGh0dHA6Ly9kaWNodnVkaWVudHUuZnB0LmNvbS52bi9jcnQv\r\nZnB0Y2EuY3J0MCMGCCsGAQUFBzABhhdodHRwOi8vb2NzcDIuZmlzLmNvbS52bjBPBgNVHSAESDBG\r\nMEQGCysGAQQBge0DAQQBMDUwMwYIKwYBBQUHAgEWJ2h0dHA6Ly9kaWNodnVkaWVudHUuZnB0LmNv\r\nbS52bi9jcHMuaHRtbDA0BgNVHSUELTArBggrBgEFBQcDAgYIKwYBBQUHAwQGCisGAQQBgjcKAwwG\r\nCSqGSIb3LwEBBTAnBgNVHR8EIDAeMBygGqAYhhZodHRwOi8vY3JsMi5maXMuY29tLnZuMB0GA1Ud\r\nDgQWBBSH7GlEDJPDrAW7kDCOJbGkE4eKuTAOBgNVHQ8BAf8EBAMCBPAwDQYJKoZIhvcNAQELBQAD\r\nggEBAG6y+wbm68PLrfMEnQ4l/cRYoDCcECvE0ylYAJZRE0oiyk1UidBWJXcJBjd43BDpTURNZrBn\r\nNeIR0YgAc5gqTvBCQMCG77sDUOXNmaMmkbltLslMPSK2k1m8Hlh4XQWibS62C3EEdIPQ1Duizxlx\r\ngsfRec2liafVLimNZlArIqurGbx6wtUXjtjQkbt5uZ2akt6kHEjWUx9dLjcjloniyeTLZR6nMSIo\r\nJ2KltSYFq6kp7NV4ClZ7g7Hoa22hKFwSTUsQscHaYbsxj5V9r4BMR0N72F6pAJ/M4bzAYLwhMVJW\r\nEyd2ywChw+gUx5qc7EgtPcqojRqCm4Y13Oz7i9t04BU=</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";
                MSG26 objTemp = msg26.MSGToObject(strXMLIn);
                if (objTemp != null)
                {
                    pv_trangthai = objTemp.Body.Row.Chungtu.TRANG_THAI;
                    pv_mota_tt = objTemp.Body.Row.Chungtu.MOTA_TRANGTHAI;
                    log.Info("3.1. Prc_SendCTUToTCT - 7.verifySignXML - pv_trangthai " + pv_trangthai + "pv_mota_tt " + pv_mota_tt);
                }
                else
                {
                    log.Info("3.1. Prc_SendCTUToTCT - 7.verifySignXML - pv_trangthai -1 pv_mota_tt không load được object trả về");
                    pv_trangthai = "-1";
                    pv_mota_tt = "không load được object trả về";
                }
                //capnhatSothue(MST, objTemp);
            }
            else
            {
                log.Info("3.1. Prc_SendCTUToTCT - 7.verifySignXML - pv_trangthai -1 pv_mota_tt không load được object trả v");
                throw new Exception("Sai chu ky");
            }
        }
        public static void CTUToMSG21_MobiTCT(String pv_strMaThamChieu,
                                            String pv_strSoCT, ref MSG21MOBI ms21)
        {

            string loai_nnt = "";
            string dgiai_ht_nt = "";
            string ma_hthuc_nop = "08";
            try
            {
                //getMSG03(pv_strMaNTT);
                //mac dinh la ko xac dinh
                loai_nnt = "2";// DataAccess.ExecuteReturnDataSet("select * from tcs_dm_nnt where ma_nnt = '" + pv_strMaNTT + "' and rownum=1 order by TO_DATE(ngay_cap_mst, 'dd/MM/yyyy') desc", CommandType.Text).Tables[0].Rows[0]["ma_loai_nnt"].ToString();
                log.Info("3.1. Prc_SendCTUToTCT - 1.1.CTUToMSG21_MobiTCT - loai_nnt " + loai_nnt);
            }
            catch (Exception e)
            {


            }
            try
            {
                dgiai_ht_nt = DataAccess.ExecuteReturnDataSet("select * from tcs_dm_hthuc_nthue where ma_hthuc_nthue = '" + ma_hthuc_nop + "'", CommandType.Text).Tables[0].Rows[0]["dgiai_hthuc_nthue"].ToString();
                log.Info("3.1. Prc_SendCTUToTCT - 1.2.CTUToMSG21_MobiTCT - dgiai_ht_nt " + dgiai_ht_nt);
            }
            catch (Exception e)
            {
                log.Info("3.1. Prc_SendCTUToTCT - 1.2.CTUToMSG21_MobiTCT - error: " + e.StackTrace);
                dgiai_ht_nt = "Nộp qua eTax Mobile của TCT";
            }

            //Lay thong tin Header CTU
            StringBuilder sbsql = new StringBuilder();
            DataSet ds = null;
            string strNgayKB = "";
            //  string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

            try
            {
                //Lay thong tin header

                IDbDataParameter[] p = null;
                if (ma_hthuc_nop.Equals("08"))
                {
                    //Nop thue tai quay
                    sbsql.Append("SELECT   h.ten_nnthue ten_nnthue,");
                    sbsql.Append("         h.ma_nnthue ma_nnt,");
                    sbsql.Append("         h.dc_nnthue dc_nnthue,");
                    sbsql.Append("         h.ma_nntien ma_nntien,");
                    sbsql.Append("         h.ten_nntien ten_nntien,");
                    sbsql.Append("         h.dc_nntien dc_nntien,");
                    sbsql.Append("         h.tk_co tk_co,");
                    sbsql.Append("         h.ma_ntk ma_ntk,");
                    sbsql.Append("         h.ma_cqthu ma_cqthu,");
                    sbsql.Append("         h.so_ct so_ct,");
                    sbsql.Append("         to_char(h.ngay_kh_nh,'dd/MM/yyyy') ngay_ct,");
                    sbsql.Append("         h.ma_nh_b ma_nh_b,");
                    sbsql.Append("         tct.huyennguoinop ten_huyen,");
                    sbsql.Append("         tct.tinhnguoinop ten_tinh, h.shkb, h.ma_nt,");
                    sbsql.Append("         h.SO_KHUNG,h.so_may,h.so_qd,h.dac_diem_ptien,h.dia_chi_ts,h.ngay_kb,h.noiDung,tct.maloaihinhthue ");
                    sbsql.Append("  FROM   tcs_ctu_hdr_mobiTCT h, set_ctu_hdr_mobitct tct ");
                    sbsql.Append(" WHERE   h.so_ct = '" + pv_strSoCT + "'");
                    sbsql.Append("          AND (h.so_ct = tct.so_ct)");
                    log.Info("3.1. Prc_SendCTUToTCT - 1.3.CTUToMSG21_MobiTCT - ma_hthuc_nop_08 " + sbsql);

                }
                else
                { throw new Exception("Không tìm thấy chứng từ!"); }



                ds = DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    log.Info("3.1. Prc_SendCTUToTCT - 1.4.CTUToMSG21_MobiTCT - Không tìm thấy chứng từ!");
                    throw new Exception("Không tìm thấy chứng từ!");
                }

                string strTinhChat_KhoanNop = "";
                string maloaihinhthue = ds.Tables[0].Rows[0]["maloaihinhthue"].ToString();
                string noidung = ds.Tables[0].Rows[0]["noiDung"].ToString();
                ms21.Body.Row.CHUNGTU.MST = ds.Tables[0].Rows[0]["ma_nnt"].ToString();
                ms21.Body.Row.CHUNGTU.TEN_NNT = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                ms21.Body.Row.CHUNGTU.DIACHI_NNT = ds.Tables[0].Rows[0]["dc_nnthue"].ToString();
                ms21.Body.Row.CHUNGTU.MST_NNTHAY = ds.Tables[0].Rows[0]["ma_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.TEN_NNTHAY = ds.Tables[0].Rows[0]["ten_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.DIACHI_NNTHAY = ds.Tables[0].Rows[0]["dc_nntien"].ToString();

                //
                if (ms21.Body.Row.CHUNGTU.MST.Equals(ms21.Body.Row.CHUNGTU.MST_NNTHAY))
                {
                    ms21.Body.Row.CHUNGTU.MST_NNTHAY = "";
                    ms21.Body.Row.CHUNGTU.TEN_NNTHAY = "";
                    ms21.Body.Row.CHUNGTU.DIACHI_NNTHAY = "";
                }

                ms21.Body.Row.CHUNGTU.SO_TK_NHKBNN = ds.Tables[0].Rows[0]["tk_co"].ToString();
                ms21.Body.Row.CHUNGTU.CQ_QLY_THU = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
                ms21.Body.Row.CHUNGTU.SO_CHUNGTU = ds.Tables[0].Rows[0]["so_ct"].ToString();
                ms21.Body.Row.CHUNGTU.NGAY_CHUNGTU = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                if (ds.Tables[0].Rows[0]["ma_ntk"].ToString() == "1")
                {
                    strTinhChat_KhoanNop = "Nộp NSNN";
                }
                else if (ma_hthuc_nop.Equals("01"))
                {
                    strTinhChat_KhoanNop = "Nộp HT GTGT";
                }
                else
                {
                    //sonmt: nop thue dien tu
                    strTinhChat_KhoanNop = "";
                }
                ms21.Body.Row.CHUNGTU.TINHCHAT_KHOAN_NOP = strTinhChat_KhoanNop;
                ms21.Body.Row.CHUNGTU.CQUAN_THAMQUYEN = strCoQuanTQ;
                //ms21.Body.Row.CHUNGTU.MA_NHTM = ds.Tables[0].Rows[0]["ma_nh_b"].ToString();
                ms21.Body.Row.CHUNGTU.MA_DVTT = strSender_Code;

                //chỗ này điền mã, không điền tên -> cần lấy ra mã
                string ma_huyen = "";
                string ma_tinh = "";
                if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ten_huyen"].ToString().Trim()))
                {
                    ma_huyen = "";
                }
                else
                {
                    string huyen = ds.Tables[0].Rows[0]["ten_huyen"].ToString();
                    string[] arrhuyen = huyen.Split('-');
                    ma_huyen = arrhuyen[0];
                }

                //
                if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ten_tinh"].ToString().Trim()))
                {
                    ma_tinh = "";
                }
                else
                {
                    string tinh = ds.Tables[0].Rows[0]["ten_tinh"].ToString();
                    string[] arrtinh = tinh.Split('-');
                    ma_tinh = arrtinh[0];
                }

                //


                ms21.Body.Row.CHUNGTU.HUYEN = ma_huyen.Trim();
                ms21.Body.Row.CHUNGTU.TINH = ma_tinh.Trim();
                // end
                //ms21.Body.Row.CHUNGTU.SH_KBNN = ds.Tables[0].Rows[0]["shkb"].ToString();
                ms21.Body.Row.CHUNGTU.LOAI_TIEN = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                ms21.Body.Row.CHUNGTU.MA_HTHUC_NOP = ma_hthuc_nop;
                ms21.Body.Row.CHUNGTU.DIENGIAI_HTHUC_NOP = dgiai_ht_nt;
                ms21.Body.Row.CHUNGTU.LHINH_NNT = loai_nnt;
                ms21.Body.Row.CHUNGTU.SO_KHUNG = ds.Tables[0].Rows[0]["SO_KHUNG"].ToString();
                ms21.Body.Row.CHUNGTU.SO_MAY = ds.Tables[0].Rows[0]["SO_MAY"].ToString();
                ms21.Body.Row.CHUNGTU.SO_QUYET_DINH = ds.Tables[0].Rows[0]["SO_QD"].ToString();
                ms21.Body.Row.CHUNGTU.DIA_CHI_TS = ds.Tables[0].Rows[0]["DIA_CHI_TS"].ToString();
                if (maloaihinhthue.Equals("02"))
                {
                    ms21.Body.Row.CHUNGTU.DAC_DIEM_PTIEN = noidung;
                }
                else
                {
                    ms21.Body.Row.CHUNGTU.DAC_DIEM_PTIEN = ds.Tables[0].Rows[0]["DAC_DIEM_PTIEN"].ToString();
                }

                ms21.Body.Row.CHUNGTU.NGAY_NTIEN = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                ms21.Body.Row.CHUNGTU.MATHAMCHIEU = pv_strMaThamChieu;
                strNgayKB = ds.Tables[0].Rows[0]["ngay_kb"].ToString();

                //Check TH nhiều ID
                //StringBuilder sbsql1 = new StringBuilder();
                //DataSet ds1 = null;
                //sbsql1.Append("SELECT  so_ct,");
                //sbsql1.Append("so_tk,");
                //sbsql1.Append("COUNT(*) AS CountOf ");
                //sbsql1.Append("  FROM   tcs_ctu_dtl_mobitct ");
                //sbsql1.Append(" WHERE   so_ct = '" + pv_strSoCT + "' and ngay_kb='" + strNgayKB + "'");
                //sbsql1.Append("GROUP BY so_tk,so_ct");
                //sbsql1.Append("HAVING COUNT(*)>1");

                //log.Info("CTUToMSG21_MobiTCT - sql " + sbsql1);
                //ds1 = DataAccess.ExecuteReturnDataSet(sbsql1.ToString(), CommandType.Text);


                StringBuilder sbsql1 = new StringBuilder();
                DataSet ds1 = null;
                sbsql1.Append("SELECT distinct so_tk ");
                sbsql1.Append("  FROM   tcs_ctu_dtl_mobitct ");
                sbsql1.Append(" WHERE   so_ct = '" + pv_strSoCT + "' and ngay_kb='" + strNgayKB + "'");


                log.Info("CTUToMSG21_MobiTCT - sql1 " + sbsql1);
                ds1 = DataAccess.ExecuteReturnDataSet(sbsql1.ToString(), CommandType.Text);

                if (ds1.Tables[0].Rows.Count > 1)
                {
                    ms21.Body.Row.CHUNGTU.SO_QUYET_DINH = "";
                }
                else
                {
                    ms21.Body.Row.CHUNGTU.SO_QUYET_DINH = ds1.Tables[0].Rows[0]["so_tk"].ToString();
                }

                //if (checkID(pv_strSoCT, strNgayKB) == 1)
                //{
                //    ms21.Body.Row.CHUNGTU.SO_QUYET_DINH = "";
                //}

                //Lay thong tin detail

                //Nop thue tai quay
                sbsql = new StringBuilder();
                sbsql.Append("SELECT   d.ky_thue,");
                sbsql.Append("         d.ma_chuong,");
                sbsql.Append("         d.ma_khoan,");
                sbsql.Append("         d.ma_tmuc,");
                sbsql.Append("        d.noi_dung,");
                sbsql.Append("         d.sotien,");
                sbsql.Append("         d.so_tk");
                sbsql.Append("  FROM   tcs_ctu_dtl_mobitct d");
                sbsql.Append(" WHERE   d.so_ct = '" + pv_strSoCT + "' and d.ngay_kb='" + strNgayKB + "' ");





                //Lay thong tin chi tiet chung tu
                log.Info("3.1. Prc_SendCTUToTCT - 1.5.CTUToMSG21_MobiTCT - Lay thong tin chi tiet chung tu!");
                log.Info("3.1. Prc_SendCTUToTCT - 1.5.1. CTUToMSG21_MobiTCT - sql " + sbsql);
                ds = DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text);
                log.Info("3.1. Prc_SendCTUToTCT - 1.5.2. CTUToMSG21_MobiTCT - ds count " + ds.Tables[0].Rows.Count);
                if (ds.Tables[0].Rows.Count <= 0)
                {
                    log.Info("3.1. Prc_SendCTUToTCT - 1.6.CTUToMSG21_MobiTCT - Không tìm thấy chứng từ!");
                    throw new Exception("Không tìm thấy chứng từ!");
                }
                List<ProcMegService.MSG.MSG21MOBI.CHUNGTU_CHITIET> listItems = new List<ProcMegService.MSG.MSG21MOBI.CHUNGTU_CHITIET>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ProcMegService.MSG.MSG21MOBI.CHUNGTU_CHITIET item = new ProcMegService.MSG.MSG21MOBI.CHUNGTU_CHITIET();
                    item.CHUONG = row["ma_chuong"].ToString();
                    item.TIEUMUC = row["ma_tmuc"].ToString();
                    item.THONGTIN_KHOANNOP = row["noi_dung"].ToString();
                    if (maloaihinhthue.Equals("02"))
                    {
                        item.THONGTIN_KHOANNOP = noidung;
                    }
                    item.SO_TIEN = row["sotien"].ToString();
                    item.KY_THUE = row["ky_thue"].ToString().Substring(3);
                    listItems.Add(item);
                }
                ms21.Body.Row.CHUNGTU.CHUNGTU_CHITIET = listItems;
                log.Info("3.1. Prc_SendCTUToTCT - 1.7.CTUToMSG21_MobiTCT - success");
            }
            catch (Exception ex)
            {
                log.Info("3.1. Prc_SendCTUToTCT - error CTUToMSG21_MobiTCT - " + ex.StackTrace);
                StringBuilder sbErrMsg = default(StringBuilder);
                sbErrMsg = new StringBuilder();
                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUToMSG21_MobiTCT): ma tham chieu[" + pv_strMaThamChieu + "] so ctu[" + pv_strSoCT + "] ");
                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

                throw ex;

            }
            finally
            {
            }


        }
        public static string getMSG00128API(string MST, string MA_CQTHU, string LOAITHUE)
        {
            try
            {
                strMsg_Id = TCS_GetSequenceValue();
                MSG128 ms5 = new MSG128();
                ms5.Body = new ProcMegService.MSG.MSG128.BODY();
                ms5.Header = new ProcMegService.MSG.MSG128.HEADER();
                ms5.Body.Row = new ProcMegService.MSG.MSG128.ROW();
                ms5.Body.Row.TruyVan = new ProcMegService.MSG.MSG128.TRUYVAN();
                //Gan du lieu header
                ms5.Header.VERSION = strMessage_Version;
                ms5.Header.SENDER_CODE = strSender_Code;
                ms5.Header.SENDER_NAME = strSender_Name;
                ms5.Header.RECEIVER_CODE = strReciver_Code;
                ms5.Header.RECEIVER_NAME = strReciver_Name;
                ms5.Header.TRAN_CODE = MessageType.strMSG05010;
                ms5.Header.MSG_ID = strMsg_Id;
                ms5.Header.MSG_REFID = strMsgRef_Id;
                ms5.Header.ID_LINK = strId_Link;
                ms5.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                ms5.Header.ORIGINAL_CODE = strOriginal_Code;
                ms5.Header.ORIGINAL_NAME = strOriginal_Name;
                ms5.Header.ORIGINAL_DATE = strOriginal_Date;
                ms5.Header.ERROR_CODE = strError_Code;
                ms5.Header.ERROR_DESC = "";
                ms5.Header.SPARE1 = strSpare1;
                ms5.Header.SPARE2 = strSpare2;
                ms5.Header.SPARE3 = strSpare3;
                //Gan du lieu body
                ms5.Body.Row.TYPE = "00128"; //MessageType.MSG05010;
                ms5.Body.Row.NAME = "Truy vấn sổ thuế cá nhân";
                ms5.Body.Row.TruyVan.MST = MST;
                ms5.Body.Row.TruyVan.MA_CQ_THU = MA_CQTHU;
                ms5.Body.Row.TruyVan.LOAITHUE = LOAITHUE;
                //Chuyen thanh XML
                string strXMLOut = ms5.MSG128toXML(ms5);

                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                strXMLOut = gf_MessageOut_Format(strXMLOut);
                //Ky tren msg

                LogDebug.WriteLog(" SignXML  strXMLOut = " + strXMLOut, EventLogEntryType.Error);

                strXMLOut = SignXML(strXMLOut);
                log.Info("1. getMSG00128API - strXMLOut= " + strXMLOut);
                LogDebug.WriteLog(" End SignXML  strXMLOut = " + strXMLOut, EventLogEntryType.Error);

                string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG05010);
                //string strXMLIn = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DATA><HEADER><VERSION>1</VERSION><SENDER_CODE>GIP</SENDER_CODE><SENDER_NAME>Hệ Thống GIP</SENDER_NAME><RECEIVER_CODE>01341001</RECEIVER_CODE><RECEIVER_NAME>NGAN HANG CITIBANK</RECEIVER_NAME><TRAN_CODE>05011</TRAN_CODE><MSG_ID>GIP201810182071852</MSG_ID><MSG_REFID/><ID_LINK/><SEND_DATE>10-18-2018</SEND_DATE><ORIGINAL_CODE>GIP</ORIGINAL_CODE><ORIGINAL_NAME>Hệ Thống GIP</ORIGINAL_NAME><ORIGINAL_DATE>10-18-2018</ORIGINAL_DATE><ERROR_CODE/><ERROR_DESC/><SPARE1/><SPARE2/><SPARE3/></HEADER><BODY><ROW><TYPE>00006</TYPE><NAME>Trả lời truy vấn thu nộp theo MST</NAME><THONGTIN_NNT><THONGTINCHUNG><ROW_NNT><MST>2300157887</MST><TEN_NNT>Công Ty Quế Ninh - (Tnhh)</TEN_NNT><LOAI_NNT>0100</LOAI_NNT><SO>125498944</SO><CHUONG>754</CHUONG><MA_CQT_QL>1054022</MA_CQT_QL></ROW_NNT></THONGTINCHUNG><DIACHI><ROW_DIACHI><MOTA_DIACHI>Xóm 01, Phường Đại Phúc</MOTA_DIACHI><MA_TINH>27TTT</MA_TINH><MA_HUYEN>256HH</MA_HUYEN><MA_XA>2230111</MA_XA></ROW_DIACHI></DIACHI><SOTHUE><ROW_SOTHUE><MA_CHUONG>754</MA_CHUONG><MA_CQ_THU>1054022</MA_CQ_THU><MA_TMUC>1701</MA_TMUC><NO_CUOI_KY>9000000000</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>03</LOAI_THUE></ROW_SOTHUE><ROW_SOTHUE><MA_CHUONG>754</MA_CHUONG><MA_CQ_THU>1054022</MA_CQ_THU><MA_TMUC>4911</MA_TMUC><NO_CUOI_KY>274500</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>13</LOAI_THUE></ROW_SOTHUE><ROW_SOTHUE><MA_CHUONG>757</MA_CHUONG><MA_CQ_THU>1054022</MA_CQ_THU><MA_TMUC>1701</MA_TMUC><NO_CUOI_KY>2600000</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>03</LOAI_THUE></ROW_SOTHUE></SOTHUE></THONGTIN_NNT></ROW></BODY><SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"><InclusiveNamespaces xmlns=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"#default\"/></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>N+LGMbFSFIOHLJ+ObkRrfGlemaYiedU6d8Mucf9YqM8=</DigestValue></Reference></SignedInfo><SignatureValue>KVksT5R/IwhAvWOogyNer0apgCMeLZzzTuIAzxLVlOsIEhL0m8gRSWtaG8ieyBGBY2Ph+rC3aBt2 xZ/K/yr82mdCgZ1P50Vok+TcgMnUKdle8nMRgUdp3Z3fFFdtITJzMeaAE5vRoSr0Y0Aq6oHNjxmF oC+GQHLeIlKHKb+3npE6EU2T45TbQBYTd248h0FH3Hv1HZD0Oq/jxPmEew8apNKgNrmEIPEcV+fC HBZ+VG62uWKokjib1pYvgxWGqXU4I/34lykScMLezrJazM5UVuw8LK8HM0NWZq48M7kprq/EhHqT aIwaetNGguRNJbifKGv+znhZKhVbeEly5OGxSA==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>rEBYcv+oGEwi54w4dEgbGHtTsExZEDeLxSU6Ql1XnX6xHsLh+IOpwXCT/NR02tl7V4Onfe7nmAgz WPFbObaV/2KL/IuV3sP/WWjHdXTme62Z4kAL9j00HBaJpt9+56BuSCxH9ZQyiARQq6vyOGVLlhnv Gj2vBCqcIphk3Y+F78AwgHE4VbbEVT4sggVUB+evH3HkZX8Ksh7RppiHIK2nxgsTg1ulqhV6IY3s uB14VSbOmevEZIp5u3Synz2zxINg4dtsvLxVia6DRChei4z8fiE9dUj3bei0+D2dPhjbJqVHnGVZ xqaV8hSF5QrPaKDW0HWA/HXVW+LlVtN/GJpTMQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIFEDCCA/igAwIBAgIQVAEBBQAAAABZpPu2HVQAXDANBgkqhkiG9w0BAQsFADA6MRMwEQYDVQQD DApWaWV0dGVsLUNBMRYwFAYDVQQKDA1WaWV0dGVsIEdyb3VwMQswCQYDVQQGEwJWTjAeFw0xODA2 MjUxMTE5MTBaFw0xOTA2MjUxMTE5MTBaMIGEMSIwIAYKCZImiZPyLGQBAQwSTVNUOjAxMDAyMzEy MjYtOTk4MSMwIQYDVQQDDBpU4buUTkcgQ+G7pEMgVEhV4bq+IFRFU1QwMTEcMBoGA1UECgwTVOG7 lE5HIEPhu6RDIFRIVeG6vjEOMAwGA1UECAwFSEFOT0kxCzAJBgNVBAYTAlZOMIIBIjANBgkqhkiG 9w0BAQEFAAOCAQ8AMIIBCgKCAQEArEBYcv+oGEwi54w4dEgbGHtTsExZEDeLxSU6Ql1XnX6xHsLh +IOpwXCT/NR02tl7V4Onfe7nmAgzWPFbObaV/2KL/IuV3sP/WWjHdXTme62Z4kAL9j00HBaJpt9+ 56BuSCxH9ZQyiARQq6vyOGVLlhnvGj2vBCqcIphk3Y+F78AwgHE4VbbEVT4sggVUB+evH3HkZX8K sh7RppiHIK2nxgsTg1ulqhV6IY3suB14VSbOmevEZIp5u3Synz2zxINg4dtsvLxVia6DRChei4z8 fiE9dUj3bei0+D2dPhjbJqVHnGVZxqaV8hSF5QrPaKDW0HWA/HXVW+LlVtN/GJpTMQIDAQABo4IB xTCCAcEwNgYIKwYBBQUHAQEEKjAoMCYGCCsGAQUFBzABhhpodHRwOi8vb2NzcDIudmlldHRlbC1j YS52bjAdBgNVHQ4EFgQUElhy2PvfweFHXx3hZpZa2ruD0eYwDAYDVR0TAQH/BAIwADAfBgNVHSME GDAWgBRMA8IT5aaiOoiPD1Uz/Osyde+qRDCBrAYDVR0gBIGkMIGhMIGeBgorBgEEAYHtAwEFMIGP MEoGCCsGAQUFBwICMD4ePABUAGgAaQBzACAAaQBzACAAYQBjAGMAcgBlAGQAaQB0AGUAZAAgAGMA ZQByAHQAaQBmAGkAYwBhAHQAZTBBBggrBgEFBQcCARY1aHR0cDovL3ZpZXR0ZWwtY2Eudm4vdXBs b2Fkcy9maWxlL2Rvd25sb2FkL0NQLUNQUy5wZGYwegYDVR0fBHMwcTBvoC2gK4YpaHR0cDovL2Ny bC52aWV0dGVsLWNhLnZuL1ZpZXR0ZWwtQ0EtMi5jcmyiPqQ8MDoxEzARBgNVBAMMClZpZXR0ZWwt Q0ExFjAUBgNVBAoMDVZpZXR0ZWwgR3JvdXAxCzAJBgNVBAYTAlZOMA4GA1UdDwEB/wQEAwIF4DAN BgkqhkiG9w0BAQsFAAOCAQEAriMfWZg/O/iDL+8ujF+DwYVBh60glVIxUy+UuTSZ9/t9YGuT3jCI a86/6LL9Ge4hI8tDAlrqYz79PXns6fTxxfzRCyLa/ChzoGjLblF+wuisXx1zp68RBonkC39VRCbS zRzDqeNwF7sE475w+1PG5ctssgJKqU0X3HF2JWkZn82Fe7SvopP/RplJOTNkxQp0X/32e6suQz4k SqgjAMEK1UPgX0g77lVvyZdcfhf0Z3N3I4fCgBetv7flAgV2D+t/f6jXWfcvZq9TKkDbZgdsHl04 4zSxcXZdA3KbDQjxxcxgyVEmN3mCP8pfSB1N98KxPZvFfNTr8CIqeWFs4c7YOw==</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";

                return strXMLIn;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "";
        }
        public static string getMSG046API(string MST, string SO_TK)
        {
            try
            {
                strMsg_Id = TCS_GetSequenceValue();
                MSG05 ms5 = new MSG05();
                ms5.Body = new ProcMegService.MSG.MSG05.BODY();
                ms5.Header = new ProcMegService.MSG.MSG05.HEADER();
                ms5.Body.Row = new ProcMegService.MSG.MSG05.ROW();
                ms5.Body.Row.TruyVan = new ProcMegService.MSG.MSG05.TRUYVAN();
                //Gan du lieu header
                ms5.Header.VERSION = strMessage_Version;
                ms5.Header.SENDER_CODE = strSender_Code;
                ms5.Header.SENDER_NAME = strSender_Name;
                ms5.Header.RECEIVER_CODE = strReciver_Code;
                ms5.Header.RECEIVER_NAME = strReciver_Name;
                ms5.Header.TRAN_CODE = MessageType.strMSG05010;
                ms5.Header.MSG_ID = strMsg_Id;
                ms5.Header.MSG_REFID = strMsgRef_Id;
                ms5.Header.ID_LINK = strId_Link;
                ms5.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                ms5.Header.ORIGINAL_CODE = strOriginal_Code;
                ms5.Header.ORIGINAL_NAME = strOriginal_Name;
                ms5.Header.ORIGINAL_DATE = strOriginal_Date;
                ms5.Header.ERROR_CODE = strError_Code;
                ms5.Header.ERROR_DESC = "";
                ms5.Header.SPARE1 = strSpare1;
                ms5.Header.SPARE2 = strSpare2;
                ms5.Header.SPARE3 = strSpare3;
                //Gan du lieu body
                ms5.Body.Row.TYPE = "00046"; //MessageType.MSG05010;
                ms5.Body.Row.NAME = "Truy vấn thu nộp theo MST";
                ms5.Body.Row.TruyVan.MST = MST;
                ms5.Body.Row.TruyVan.SO_TKHAI = SO_TK;
                //Chuyen thanh XML
                string strXMLOut = ms5.MSG05toXML(ms5);

                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                strXMLOut = gf_MessageOut_Format(strXMLOut);
                //Ky tren msg

                LogDebug.WriteLog(" SignXML  strXMLOut = " + strXMLOut, EventLogEntryType.Error);

                strXMLOut = SignXML(strXMLOut);

                LogDebug.WriteLog(" End SignXML  strXMLOut = " + strXMLOut, EventLogEntryType.Error);

                string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG05010);
                //string strXMLIn = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DATA><HEADER><VERSION>1</VERSION><SENDER_CODE>GIP</SENDER_CODE><SENDER_NAME>Hệ Thống GIP</SENDER_NAME><RECEIVER_CODE>01341001</RECEIVER_CODE><RECEIVER_NAME>NGAN HANG CITIBANK</RECEIVER_NAME><TRAN_CODE>05011</TRAN_CODE><MSG_ID>GIP201810182071852</MSG_ID><MSG_REFID/><ID_LINK/><SEND_DATE>10-18-2018</SEND_DATE><ORIGINAL_CODE>GIP</ORIGINAL_CODE><ORIGINAL_NAME>Hệ Thống GIP</ORIGINAL_NAME><ORIGINAL_DATE>10-18-2018</ORIGINAL_DATE><ERROR_CODE/><ERROR_DESC/><SPARE1/><SPARE2/><SPARE3/></HEADER><BODY><ROW><TYPE>00006</TYPE><NAME>Trả lời truy vấn thu nộp theo MST</NAME><THONGTIN_NNT><THONGTINCHUNG><ROW_NNT><MST>2300157887</MST><TEN_NNT>Công Ty Quế Ninh - (Tnhh)</TEN_NNT><LOAI_NNT>0100</LOAI_NNT><SO>125498944</SO><CHUONG>754</CHUONG><MA_CQT_QL>1054022</MA_CQT_QL></ROW_NNT></THONGTINCHUNG><DIACHI><ROW_DIACHI><MOTA_DIACHI>Xóm 01, Phường Đại Phúc</MOTA_DIACHI><MA_TINH>27TTT</MA_TINH><MA_HUYEN>256HH</MA_HUYEN><MA_XA>2230111</MA_XA></ROW_DIACHI></DIACHI><SOTHUE><ROW_SOTHUE><MA_CHUONG>754</MA_CHUONG><MA_CQ_THU>1054022</MA_CQ_THU><MA_TMUC>1701</MA_TMUC><NO_CUOI_KY>9000000000</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>03</LOAI_THUE></ROW_SOTHUE><ROW_SOTHUE><MA_CHUONG>754</MA_CHUONG><MA_CQ_THU>1054022</MA_CQ_THU><MA_TMUC>4911</MA_TMUC><NO_CUOI_KY>274500</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>13</LOAI_THUE></ROW_SOTHUE><ROW_SOTHUE><MA_CHUONG>757</MA_CHUONG><MA_CQ_THU>1054022</MA_CQ_THU><MA_TMUC>1701</MA_TMUC><NO_CUOI_KY>2600000</NO_CUOI_KY><SO_TAI_KHOAN_CO>7111</SO_TAI_KHOAN_CO><SO_QDINH/><NGAY_QDINH/><TI_GIA/><LOAI_TIEN>VND</LOAI_TIEN><LOAI_THUE>03</LOAI_THUE></ROW_SOTHUE></SOTHUE></THONGTIN_NNT></ROW></BODY><SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"><InclusiveNamespaces xmlns=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"#default\"/></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>N+LGMbFSFIOHLJ+ObkRrfGlemaYiedU6d8Mucf9YqM8=</DigestValue></Reference></SignedInfo><SignatureValue>KVksT5R/IwhAvWOogyNer0apgCMeLZzzTuIAzxLVlOsIEhL0m8gRSWtaG8ieyBGBY2Ph+rC3aBt2 xZ/K/yr82mdCgZ1P50Vok+TcgMnUKdle8nMRgUdp3Z3fFFdtITJzMeaAE5vRoSr0Y0Aq6oHNjxmF oC+GQHLeIlKHKb+3npE6EU2T45TbQBYTd248h0FH3Hv1HZD0Oq/jxPmEew8apNKgNrmEIPEcV+fC HBZ+VG62uWKokjib1pYvgxWGqXU4I/34lykScMLezrJazM5UVuw8LK8HM0NWZq48M7kprq/EhHqT aIwaetNGguRNJbifKGv+znhZKhVbeEly5OGxSA==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>rEBYcv+oGEwi54w4dEgbGHtTsExZEDeLxSU6Ql1XnX6xHsLh+IOpwXCT/NR02tl7V4Onfe7nmAgz WPFbObaV/2KL/IuV3sP/WWjHdXTme62Z4kAL9j00HBaJpt9+56BuSCxH9ZQyiARQq6vyOGVLlhnv Gj2vBCqcIphk3Y+F78AwgHE4VbbEVT4sggVUB+evH3HkZX8Ksh7RppiHIK2nxgsTg1ulqhV6IY3s uB14VSbOmevEZIp5u3Synz2zxINg4dtsvLxVia6DRChei4z8fiE9dUj3bei0+D2dPhjbJqVHnGVZ xqaV8hSF5QrPaKDW0HWA/HXVW+LlVtN/GJpTMQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIFEDCCA/igAwIBAgIQVAEBBQAAAABZpPu2HVQAXDANBgkqhkiG9w0BAQsFADA6MRMwEQYDVQQD DApWaWV0dGVsLUNBMRYwFAYDVQQKDA1WaWV0dGVsIEdyb3VwMQswCQYDVQQGEwJWTjAeFw0xODA2 MjUxMTE5MTBaFw0xOTA2MjUxMTE5MTBaMIGEMSIwIAYKCZImiZPyLGQBAQwSTVNUOjAxMDAyMzEy MjYtOTk4MSMwIQYDVQQDDBpU4buUTkcgQ+G7pEMgVEhV4bq+IFRFU1QwMTEcMBoGA1UECgwTVOG7 lE5HIEPhu6RDIFRIVeG6vjEOMAwGA1UECAwFSEFOT0kxCzAJBgNVBAYTAlZOMIIBIjANBgkqhkiG 9w0BAQEFAAOCAQ8AMIIBCgKCAQEArEBYcv+oGEwi54w4dEgbGHtTsExZEDeLxSU6Ql1XnX6xHsLh +IOpwXCT/NR02tl7V4Onfe7nmAgzWPFbObaV/2KL/IuV3sP/WWjHdXTme62Z4kAL9j00HBaJpt9+ 56BuSCxH9ZQyiARQq6vyOGVLlhnvGj2vBCqcIphk3Y+F78AwgHE4VbbEVT4sggVUB+evH3HkZX8K sh7RppiHIK2nxgsTg1ulqhV6IY3suB14VSbOmevEZIp5u3Synz2zxINg4dtsvLxVia6DRChei4z8 fiE9dUj3bei0+D2dPhjbJqVHnGVZxqaV8hSF5QrPaKDW0HWA/HXVW+LlVtN/GJpTMQIDAQABo4IB xTCCAcEwNgYIKwYBBQUHAQEEKjAoMCYGCCsGAQUFBzABhhpodHRwOi8vb2NzcDIudmlldHRlbC1j YS52bjAdBgNVHQ4EFgQUElhy2PvfweFHXx3hZpZa2ruD0eYwDAYDVR0TAQH/BAIwADAfBgNVHSME GDAWgBRMA8IT5aaiOoiPD1Uz/Osyde+qRDCBrAYDVR0gBIGkMIGhMIGeBgorBgEEAYHtAwEFMIGP MEoGCCsGAQUFBwICMD4ePABUAGgAaQBzACAAaQBzACAAYQBjAGMAcgBlAGQAaQB0AGUAZAAgAGMA ZQByAHQAaQBmAGkAYwBhAHQAZTBBBggrBgEFBQcCARY1aHR0cDovL3ZpZXR0ZWwtY2Eudm4vdXBs b2Fkcy9maWxlL2Rvd25sb2FkL0NQLUNQUy5wZGYwegYDVR0fBHMwcTBvoC2gK4YpaHR0cDovL2Ny bC52aWV0dGVsLWNhLnZuL1ZpZXR0ZWwtQ0EtMi5jcmyiPqQ8MDoxEzARBgNVBAMMClZpZXR0ZWwt Q0ExFjAUBgNVBAoMDVZpZXR0ZWwgR3JvdXAxCzAJBgNVBAYTAlZOMA4GA1UdDwEB/wQEAwIF4DAN BgkqhkiG9w0BAQsFAAOCAQEAriMfWZg/O/iDL+8ujF+DwYVBh60glVIxUy+UuTSZ9/t9YGuT3jCI a86/6LL9Ge4hI8tDAlrqYz79PXns6fTxxfzRCyLa/ChzoGjLblF+wuisXx1zp68RBonkC39VRCbS zRzDqeNwF7sE475w+1PG5ctssgJKqU0X3HF2JWkZn82Fe7SvopP/RplJOTNkxQp0X/32e6suQz4k SqgjAMEK1UPgX0g77lVvyZdcfhf0Z3N3I4fCgBetv7flAgV2D+t/f6jXWfcvZq9TKkDbZgdsHl04 4zSxcXZdA3KbDQjxxcxgyVEmN3mCP8pfSB1N98KxPZvFfNTr8CIqeWFs4c7YOw==</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";
                // xml ko co du lieu
                //string strXMLIn = "<HEADER><VERSION>1</VERSION><SENDER_CODE>GIP</SENDER_CODE><SENDER_NAME>H??? Th???ng GIP</SENDER_NAME><RECEIVER_CODE>79305001</RECEIVER_CODE><RECEIVER_NAME>Ng??n ha??ng Eximbank</RECEIVER_NAME><TRAN_CODE>05011</TRAN_CODE><MSG_ID>GIP202209296638752</MSG_ID><MSG_REFID/><ID_LINK/><SEND_DATE>09-29-2022</SEND_DATE><ORIGINAL_CODE>GIP</ORIGINAL_CODE><ORIGINAL_NAME>H??? Th???ng GIP</ORIGINAL_NAME><ORIGINAL_DATE>09-29-2022</ORIGINAL_DATE><ERROR_CODE/><ERROR_DESC/><SPARE1/><SPARE2/><SPARE3/></HEADER><BODY><ROW><TYPE>00047</TYPE><NAME>K???t qu??? truy v???n s??? thu??? LPTB</NAME><PHANHOI><TYPE_PHANHOI>00046</TYPE_PHANHOI><TRANG_THAI>02</TRANG_THAI><MOTA_TRANGTHAI>Kh??ng t??m th???y k???t qu??? t??m ki???m</MOTA_TRANGTHAI></PHANHOI></ROW></BODY><SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>5qVez6c7c4x729/5GxQB7sh4haHi4u33UZMWrTq0C08=</DigestValue></Reference></SignedInfo><SignatureValue>hA9S0o6T7ZJSk8o4swZu1ChpWQO9ag3XewuW/ElLkXIF91u/JuB9DC9tn2Xb+LlxXBUvr0SdMS5B RSsik8vo/se0qH0hAJXb/utHgfuxZ0QgUb3DAqkz1KBrwt53J5d2n8UfDImKXpBYQN8gaAU5gJh6 XuvstvYS+8Ht11lFE6I=</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>rzQuv7EfIym5XRJyq7X+u5sfzUyRaJhbVuBPev7FXdSsVj57UndNnGEfOJHrprft2UP7GF4ZfLt0 dtvP/kpIt71T36QfaylPfcytq+zdMwG8SbRcIVgOr51dICzXaCuaVb4F8alOyuUhkw0JfCNEVQLa zJqVMESMm8xUcspHZ1k=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509SubjectName>C=VN,L=H?? N???i,O=T???ng c???c Thu???,CN=T???ng c???c Thu??? Test01,UID=MST:0100231226-998</X509SubjectName><X509Certificate>MIID4TCCAsmgAwIBAgIQVAT//rcDP7OhnZUkpVwADTANBgkqhkiG9w0BAQsFADA6MRMwEQYDVQQD DApWaWV0dGVsLUNBMRYwFAYDVQQKDA1WaWV0dGVsIEdyb3VwMQswCQYDVQQGEwJWTjAeFw0xNjEx MjkwMzI3MTdaFw0xNjEyMzAwOTQ1MDBaMIGIMSIwIAYKCZImiZPyLGQBAQwSTVNUOjAxMDAyMzEy MjYtOTk4MSMwIQYDVQQDDBpU4buVbmcgY+G7pWMgVGh14bq/IFRlc3QwMTEcMBoGA1UECgwTVOG7 lW5nIGPhu6VjIFRodeG6vzESMBAGA1UEBwwJSMOgIE7hu5lpMQswCQYDVQQGEwJWTjCBnzANBgkq hkiG9w0BAQEFAAOBjQAwgYkCgYEArzQuv7EfIym5XRJyq7X+u5sfzUyRaJhbVuBPev7FXdSsVj57 UndNnGEfOJHrprft2UP7GF4ZfLt0dtvP/kpIt71T36QfaylPfcytq+zdMwG8SbRcIVgOr51dICzX aCuaVb4F8alOyuUhkw0JfCNEVQLazJqVMESMm8xUcspHZ1kCAwEAAaOCARYwggESMDYGCCsGAQUF BwEBBCowKDAmBggrBgEFBQcwAYYaaHR0cDovL29jc3AyLnZpZXR0ZWwtY2Eudm4wHQYDVR0OBBYE FPejQcwnioLszft7ONAlIcJ3UARzMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUYrM5SPTc3FO1 qkyLTEZDK3V16BYwegYDVR0fBHMwcTBvoC2gK4YpaHR0cDovL2NybC52aWV0dGVsLWNhLnZuL1Zp ZXR0ZWwtQ0EtMi5jcmyiPqQ8MDoxEzARBgNVBAMMClZpZXR0ZWwtQ0ExFjAUBgNVBAoMDVZpZXR0 ZWwgR3JvdXAxCzAJBgNVBAYTAlZOMA4GA1UdDwEB/wQEAwIF4DANBgkqhkiG9w0BAQsFAAOCAQEA drdWuf8sIg3rt8kShKyiGB+LKElujhjDT7EiamJbqmqoj5g32lSelz7cHRndileLecjg+5WVhPly k+N9kZ3YuyI0oK/cfE23AZo9JrIrV6nce/sPnv1kg0H0mzyZF3FDjcGKxW50M9TS5IuCX1zuVUNE EAfRNPmdtvLdhH8JEISLhBuweW4w/MXRwKkXQzkoZA1CeSdNBkToa/G+a/503UKwq6HctqViVBlg VENcrFybFLgNVCZ7DqWIQ8c2HitYSwTJisHB3Od4pUyMsXCWU/WC990nlSaWzSr0y9A2qUeHsGh1 TKh6V6XHHaXbF+Ff9jvr/VnUtUs8zsKFkLxL9Q==</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";
                return strXMLIn;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "";
        }
        public static string getMSG109API(string MA_HS, string SO, string so_quyet_dinh)
        {
            try
            {
                strMsg_Id = TCS_GetSequenceValue();
                MSG109 ms109 = new MSG109();
                ms109.Body = new ProcMegService.MSG.MSG109.BODY();
                ms109.Header = new ProcMegService.MSG.MSG109.HEADER();
                ms109.Body.Row = new ProcMegService.MSG.MSG109.ROW();
                ms109.Body.Row.TruyVan = new ProcMegService.MSG.MSG109.TRUYVAN();
                //Gan du lieu header
                ms109.Header.VERSION = strMessage_Version;
                ms109.Header.SENDER_CODE = strSender_Code;
                ms109.Header.SENDER_NAME = strSender_Name;
                ms109.Header.RECEIVER_CODE = strReciver_Code;
                ms109.Header.RECEIVER_NAME = strReciver_Name;
                ms109.Header.TRAN_CODE = MessageType.strMSG05010;
                ms109.Header.MSG_ID = strMsg_Id;
                ms109.Header.MSG_REFID = strMsgRef_Id;
                ms109.Header.ID_LINK = strId_Link;
                ms109.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
                ms109.Header.ORIGINAL_CODE = strOriginal_Code;
                ms109.Header.ORIGINAL_NAME = strOriginal_Name;
                ms109.Header.ORIGINAL_DATE = strOriginal_Date;
                ms109.Header.ERROR_CODE = strError_Code;
                ms109.Header.ERROR_DESC = "";
                ms109.Header.SPARE1 = strSpare1;
                ms109.Header.SPARE2 = strSpare2;
                ms109.Header.SPARE3 = strSpare3;
                //Gan du lieu body
                ms109.Body.Row.TYPE = "00109"; //MessageType.MSG05010;
                ms109.Body.Row.NAME = "Truy vấn sổ thuế nhà đất";
                ms109.Body.Row.TruyVan.MA_HS = MA_HS;
                ms109.Body.Row.TruyVan.SO_QUYET_DINH = so_quyet_dinh;
                ms109.Body.Row.TruyVan.SO = SO;
                //Chuyen thanh XML
                string strXMLOut = ms109.MSG109toXML(ms109);

                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
                strXMLOut = gf_MessageOut_Format(strXMLOut);
                //Ky tren msg

                LogDebug.WriteLog(" SignXML  strXMLOut = " + strXMLOut, EventLogEntryType.Error);

                strXMLOut = SignXML(strXMLOut);

                LogDebug.WriteLog(" End SignXML  strXMLOut = " + strXMLOut, EventLogEntryType.Error);

                string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG05010);
                //string strXMLIn = "<DATA><HEADER><VERSION>1</VERSION><SENDER_CODE>GIP</SENDER_CODE><SENDER_NAME>Hệ Thống GIP</SENDER_NAME><RECEIVER_CODE>DVCQG</RECEIVER_CODE><RECEIVER_NAME>Dịch vụ công Quốc gia</RECEIVER_NAME><TRAN_CODE>05011</TRAN_CODE><MSG_ID>GIP202205102590003</MSG_ID><MSG_REFID/><ID_LINK/><SEND_DATE>05-10-2022</SEND_DATE><ORIGINAL_CODE>GIP</ORIGINAL_CODE><ORIGINAL_NAME>Hệ Thống GIP</ORIGINAL_NAME><ORIGINAL_DATE>05-10-2022</ORIGINAL_DATE><ERROR_CODE/><ERROR_DESC/><SPARE1/><SPARE2/><SPARE3/></HEADER><BODY><ROW><TYPE>00110</TYPE><NAME>Trả lời truy vấn sổ thuế nhà đất</NAME><SOTHUE><TRANGTHAI>01</TRANGTHAI><MOTA_TRANGTHAI>Tồn tại mã hồ sơ và số CMND/CCCD và có còn nợ thuế đất</MOTA_TRANGTHAI><TONG_TIEN><MA_SO_THUE>8754619163</MA_SO_THUE><TT_NNT>1500000</TT_NNT><ROW_SOTHUE><MA_HS>000.00.12.H53-220506-1697</MA_HS><MA_GCN/><SO_THUADAT>756</SO_THUADAT><SO_TO_BANDO>1</SO_TO_BANDO><DIA_CHI_TS>Trường Phước</DIA_CHI_TS><MA_TINH_TS>709</MA_TINH_TS><TEN_TINH_TS>Tây Ninh</TEN_TINH_TS><MA_HUYEN_TS>70911</MA_HUYEN_TS><TEN_HUYEN_TS>Thị xã Hoà Thành</TEN_HUYEN_TS><MA_XA_TS>7091103</MA_XA_TS><TEN_XA_TS>Xã Trường Tây</TEN_XA_TS><MA_DBHC>25648</MA_DBHC><MA_SO_THUE>8754619163</MA_SO_THUE><TEN_NNT>Phùng Nhật Nam</TEN_NNT><SO>072201004300</SO><MOTA_DIACHI>Trường Phước</MOTA_DIACHI><MA_TINH>709</MA_TINH><TEN_TINH>Tây Ninh</TEN_TINH><MA_HUYEN>70911</MA_HUYEN><TEN_HUYEN>Thị xã Hoà Thành</TEN_HUYEN><MA_XA>7091103</MA_XA><TEN_XA>Xã Trường Tây</TEN_XA><SO_QUYET_DINH>LTB2270911-TK0007800/TB-CCT</SO_QUYET_DINH><NGAY_QUYET_DINH>2022-05-10 15:20:23.0</NGAY_QUYET_DINH><MA_CHUONG>857</MA_CHUONG><MA_TMUC>2801</MA_TMUC><TEN_TMUC>Lệ phí trước bạ nhà đất</TEN_TMUC><LOAI_TK_NSNN>7111</LOAI_TK_NSNN><MA_CQUAN_THU>1054267</MA_CQUAN_THU><SO_CON_PHAI_NOP>1500000</SO_CON_PHAI_NOP><HAN_NOP>2022-06-08 00:00:00.0</HAN_NOP><SHKB>1911</SHKB></ROW_SOTHUE></TONG_TIEN><TONG_TIEN><MA_SO_THUE>8701682531</MA_SO_THUE><TT_NNT>6000000</TT_NNT><ROW_SOTHUE><MA_HS>000.00.12.H53-220506-1697</MA_HS><MA_GCN>DA 662574</MA_GCN><SO_THUADAT>756</SO_THUADAT><SO_TO_BANDO>1</SO_TO_BANDO><DIA_CHI_TS>Trường Phước</DIA_CHI_TS><MA_TINH_TS>709</MA_TINH_TS><TEN_TINH_TS>Tây Ninh</TEN_TINH_TS><MA_HUYEN_TS>70911</MA_HUYEN_TS><TEN_HUYEN_TS>Thị xã Hoà Thành</TEN_HUYEN_TS><MA_XA_TS>7091103</MA_XA_TS><TEN_XA_TS>Xã Trường Tây</TEN_XA_TS><MA_DBHC>25648</MA_DBHC><MA_SO_THUE>8701682531</MA_SO_THUE><TEN_NNT>Nguyễn Minh Hải Đăng</TEN_NNT><SO>072098000168</SO><MOTA_DIACHI>KP.Long Thới</MOTA_DIACHI><MA_TINH>709</MA_TINH><TEN_TINH>Tây Ninh</TEN_TINH><MA_HUYEN>70911</MA_HUYEN><TEN_HUYEN>Thị xã Hoà Thành</TEN_HUYEN><MA_XA>7091113</MA_XA><TEN_XA>Phường Long Thành Trung</TEN_XA><SO_QUYET_DINH>LTB2270911-TK0007799/TB-CCT</SO_QUYET_DINH><NGAY_QUYET_DINH>2022-05-10 15:20:23.0</NGAY_QUYET_DINH><MA_CHUONG>757</MA_CHUONG><MA_TMUC>1006</MA_TMUC><TEN_TMUC>Thuế thu nhập từ chuyển nhượng bất động sản, nhận thừa kế và nhận quà tặng là bất động sản</TEN_TMUC><LOAI_TK_NSNN>7111</LOAI_TK_NSNN><MA_CQUAN_THU>1054267</MA_CQUAN_THU><SO_CON_PHAI_NOP>6000000</SO_CON_PHAI_NOP><HAN_NOP>2022-06-08 00:00:00.0</HAN_NOP><SHKB>1911</SHKB></ROW_SOTHUE></TONG_TIEN></SOTHUE></ROW></BODY><SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"><InclusiveNamespaces xmlns=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"#default\"/></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><DigestValue>rsa/ajJRhQAWNa6KyUVKwXxLWN4=</DigestValue></Reference></SignedInfo><SignatureValue>M10nlzfcBwrsvXhgs0/knkIlLt3dXpscWhnt85YtV58SxcXfbXfUfw3xKc/xbeyTPl0VSf6VAu6d c16n9O9V41Ces/bCD9qTJZnTPvod/DswVaxFVhORuXppwVFWy+DtTuui/uUd9G0kQSNlCvBPPzkl JiY3uPBP3Ac0xFbbA4gOd2hflOeYJVG9W3nSMxkC76MXT1od9sjjf1iy3SeRapLOtIhc4HhHV4h8 eyLyTTOa4EihHu37+9e5Quc91kIQMXh3AUeykegup2IMuSrynD8G+7XSWI35c2ebuLSxAIdRwqQc Y0pCK7vUv3a08vfYq6DsP4n25yBE792ISszM+g==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>389R3V8CsclSouIjOk5VQHSMtUr74Bo5H2txfSmGowrmJcuo2oknyGG4Dv7lPU1M3V3GKtub3EJB gHscj2z51IOOR+j6zZq/7w9srrxFEg7jRAFZX5wfh/ut86rzaZSBU/2hwYYaWpy829AJ+GRfeSM+ AQZuN7wmS0+t4TwakGmE2ep8d4+jcXYmA+0FeYEAqwlhMJYPbgW+NjPMMB+DTe73BkPqLrHM+KNE lBOH/RIw2RnVYP56eW/RqB84XE9JhEbLleC7gj9p01Nm1zQknL8jwDexPpDJCsCOl2HLehGZeozk n/2fhF/IlcrjZ1uAZ0xhwfs5TJMS+yy3ZXQtYQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIFfjCCBGagAwIBAgIDazy1MA0GCSqGSIb3DQEBBQUAMFkxCzAJBgNVBAYTAlZOMR0wGwYDVQQK DBRCYW4gQ28geWV1IENoaW5oIHBodTErMCkGA1UEAwwiQ28gcXVhbiBjaHVuZyB0aHVjIHNvIEJv IFRhaSBjaGluaDAeFw0xNzA1MDQwODQ0NThaFw0yNzA1MDIwODQ0NThaMEQxCzAJBgNVBAYTAlZO MRcwFQYDVQQKDA5NU1Q6MDEwMDIzMTIyNjEcMBoGA1UEAwwTVOG7lW5nIGPhu6VjIFRodeG6vzCC ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAN/PUd1fArHJUqLiIzpOVUB0jLVK++AaOR9r cX0phqMK5iXLqNqJJ8hhuA7+5T1NTN1dxirbm9xCQYB7HI9s+dSDjkfo+s2av+8PbK68RRIO40QB WV+cH4f7rfOq82mUgVP9ocGGGlqcvNvQCfhkX3kjPgEGbje8JktPreE8GpBphNnqfHePo3F2JgPt BXmBAKsJYTCWD24FvjYzzDAfg03u9wZD6i6xzPijRJQTh/0SMNkZ1WD+enlv0agfOFxPSYRGy5Xg u4I/adNTZtc0JJy/I8A3sT6QyQrAjpdhy3oRmXqM5J/9n4RfyJXK42dbgGdMYcH7OUyTEvsst2V0 LWECAwEAAaOCAmIwggJeMAkGA1UdEwQCMAAwEQYJYIZIAYb4QgEBBAQDAgWgMAsGA1UdDwQEAwIE 8DApBgNVHSUEIjAgBggrBgEFBQcDAgYIKwYBBQUHAwQGCisGAQQBgjcUAgIwHwYJYIZIAYb4QgEN BBIWEFVzZXIgU2lnbiBvZiBCVEMwHQYDVR0OBBYEFCUeMA8n53T1rwdKoFXUxc0xquIcMIGVBgNV HSMEgY0wgYqAFJ44mtYplYlqBX8q/18Bl7RXMGayoW+kbTBrMQswCQYDVQQGEwJWTjEdMBsGA1UE CgwUQmFuIENvIHlldSBDaGluaCBwaHUxPTA7BgNVBAMMNENvIHF1YW4gY2h1bmcgdGh1YyBzbyBj aHV5ZW4gZHVuZyBDaGluaCBwaHUgKFJvb3RDQSmCAQMwCQYDVR0SBAIwADBfBggrBgEFBQcBAQRT MFEwHwYIKwYBBQUHMAGGE2h0dHA6Ly9vY3NwLmNhLmJ0Yy8wLgYIKwYBBQUHMAKGImh0dHA6Ly9j YS5idGMvcGtpL3B1Yi9jZXJ0L2J0Yy5jcnQwMAYJYIZIAYb4QgEEBCMWIWh0dHA6Ly9jYS5idGMv cGtpL3B1Yi9jcmwvYnRjLmNybDAwBglghkgBhvhCAQMEIxYhaHR0cDovL2NhLmJ0Yy9wa2kvcHVi L2NybC9idGMuY3JsMF4GA1UdHwRXMFUwJ6AloCOGIWh0dHA6Ly9jYS5idGMvcGtpL3B1Yi9jcmwv YnRjLmNybDAqoCigJoYkaHR0cDovL2NhLmdvdi52bi9wa2kvcHViL2NybC9idGMuY3JsMA0GCSqG SIb3DQEBBQUAA4IBAQAZaW+HR+tKHzW36+msqcSYjg+0F+fnEfz3xnpnfEzYYsdXieDOG/0Z7y0L prEQtj3mbbtAzWiF3DxCHGU1wrdJopvGuWV/LESNMnAJSlM8TZIPvW9ZcKEd4FNpMYBbTaY4pTPy JiPJJ7LAbUcmJfbswTHuYYZpDaXqKRDwDRE4scM6Gs9U8HawzDWVkGdU/8Fsrb9M9lSzlLDYTvgs +wdYyB4hRWkh9DMJshDsobb1OlFmSRNEpAbcCAx17uWnjGjsAL/Tx1FLuujspiWBYgnX1TcnRHeA hhCGDEFA4bYjn/c9sGNHTxEAEcCuxIcWpxEdmxqFNdaGYIHJzEQKi3Vb</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";

                return strXMLIn;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "";
        }
        public static void SendMSG21_CN_TB(string pv_strMaNTT, string pv_strSHKB, string pv_strSoCT,
                                   string pv_strSoBT, string pv_strMaNV, string ma_hthuc_nop, ref string pv_trangthai, ref string pv_mota_tt)
        {
            strMsg_Id = TCS_GetSequenceValue();
            MSG21IB ms21 = new MSG21IB();
            ms21.Header = new ProcMegService.MSG.MSG21IB.HEADER();
            ms21.Body = new ProcMegService.MSG.MSG21IB.BODY();
            ms21.Body.Row = new ProcMegService.MSG.MSG21IB.ROW();
            ms21.Body.Row.CHUNGTU = new ProcMegService.MSG.MSG21IB.CHUNGTU();
            ms21.Header.VERSION = strMessage_Version;
            ms21.Header.SENDER_CODE = strSender_Code;
            ms21.Header.SENDER_NAME = strSender_Name;
            ms21.Header.RECEIVER_CODE = strReciver_Code;
            ms21.Header.RECEIVER_NAME = strReciver_Name;
            ms21.Header.TRAN_CODE = MessageType.strMSG21;
            ms21.Header.MSG_ID = strMsg_Id;
            ms21.Header.MSG_REFID = strMsgRef_Id;
            ms21.Header.ID_LINK = strId_Link;
            ms21.Header.SEND_DATE = DateTime.Now.ToString(strformatdatetime);
            ms21.Header.ORIGINAL_CODE = strOriginal_Code;
            ms21.Header.ORIGINAL_NAME = strOriginal_Name;
            ms21.Header.ORIGINAL_DATE = strOriginal_Date;
            ms21.Header.ERROR_CODE = strError_Code;
            ms21.Header.ERROR_DESC = "";
            ms21.Header.SPARE1 = strSpare1;
            ms21.Header.SPARE2 = strSpare2;
            ms21.Header.SPARE3 = strSpare3;
            //Gan du lieu body
            ms21.Body.Row.TYPE = "00121";
            ms21.Body.Row.NAME = "Gửi thông tin chứng từ";
            log.Info("3.1. IB - 1.SendMSG21_CN_TB ");

            CTUToMSG21_CN_TB(pv_strMaNTT, pv_strSHKB,
                                  pv_strSoCT, pv_strSoBT,
                                  pv_strMaNV, ma_hthuc_nop, ref ms21);
            //CTUToMSG21_CN_TB(pv_strMaThamChieu, pv_strSoCTU, ref ms21);
            log.Info("3.1. IB - 2.SendMSG21_CN_TB ");
            //Chuyen thanh XML
            string strXMLOut = ms21.MSG21IBtoXML(ms21);

            strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
            strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
            strXMLOut = gf_MessageOut_Format(strXMLOut);
            //Ky tren msg
            strXMLOut = SignXML(strXMLOut);
            log.Info("3.1. IB - 3.SendMSG21_CN_TB - SignXML " + strXMLOut);
            //Goi den Web Service
            string strXMLIn = sendMsg(strXMLOut, strMsg_Id, MessageType.strMSG21);
            log.Info("3.1. IB - 4.SendMSG21_CN_TB - sendMsg " + strXMLIn);
            if (verifySignXML(strXMLIn))
            {
                log.Info("3.1. IB - 5.verifySignXML ");
                MSG26 msg26 = new MSG26();
                strXMLIn = strXMLIn.Replace("<DATA>", strXMLdefin2);   //inthe hotrack disable implement
                log.Info("3.1. IB - 6.verifySignXML - strXMLIn " + strXMLIn);
                //strXMLIn = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DATA xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">\r\n   <HEADER>\r\n      <VERSION>1</VERSION>\r\n      <SENDER_CODE>GIP</SENDER_CODE>\r\n      <SENDER_NAME>H??? Th???ng GIP</SENDER_NAME>\r\n      <RECEIVER_CODE>01341001</RECEIVER_CODE>\r\n      <RECEIVER_NAME>01341001</RECEIVER_NAME>\r\n      <TRAN_CODE>05011</TRAN_CODE>\r\n      <MSG_ID>GIP202009292836298</MSG_ID>\r\n      <MSG_REFID/>\r\n      <ID_LINK/>\r\n      <SEND_DATE>09-29-2020</SEND_DATE>\r\n      <ORIGINAL_CODE>GIP</ORIGINAL_CODE>\r\n      <ORIGINAL_NAME>H??? Th???ng GIP</ORIGINAL_NAME>\r\n      <ORIGINAL_DATE>09-29-2020</ORIGINAL_DATE>\r\n      <ERROR_CODE/>\r\n      <ERROR_DESC/>\r\n      <SPARE1/>\r\n      <SPARE2/>\r\n      <SPARE3/>\r\n   </HEADER>\r\n   <BODY>\r\n      <ROW>\r\n         <TYPE>00027</TYPE>\r\n         <NAME>K???t qu??? Verify Ch??? k?? s???</NAME>\r\n         <CHUNGTU>\r\n            <TYPE_PHANHOI>00021</TYPE_PHANHOI>\r\n            <TRANG_THAI>01</TRANG_THAI>\r\n            <MOTA_TRANGTHAI>Thành công!!!</MOTA_TRANGTHAI>\r\n         </CHUNGTU>\r\n      </ROW>\r\n   </BODY>\r\n<SECURITY><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#WithComments\"><InclusiveNamespaces xmlns=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"#default\"/></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\"><XPath>ancestor-or-self::HEADER or ancestor-or-self::BODY</XPath></Transform><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>O+HHZWuoPIvU2FYZ/KP0MdcGF1LBUcAKdFc+rdiEcI0=</DigestValue></Reference></SignedInfo><SignatureValue>tpoV07oDG9DLEbkbPuUKjYZ7ksZt2rpZJWeeIm4m1PEJBtccf60d+Mf9TPFfbdPFeec7BwjQhUcb\r\nP4IEqwinTbY7S2QiaTCsqQmyQ7aVGWgeMDev+ZDr5Rkq+1Ttug6y9qpOzIFAJitYL0dxKCgmy85v\r\nw2RkOI34jn0dCpAVt9Q0JWoPR5k+33/ATXp8iY4KVU/c5bm/X7NYFSl0r22aE9GkpNAJZmAv0G9P\r\ngdlH42FCK6eQYVN3tiR6mO/FN26NAYnQ5lBgfkbK4UrlLJ76hU/OBlSW3X4/76M3WqvPdEngvqFB\r\nhPmcw+S60mtmMHb8a9xJPXR+gKTJkLi2iNYEzA==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>5Lb7DhwAfNbM1AfMyiz3oOAm71i0hLKyXqnXWU03pGMtkO+WISzBssGASia5kN2jnx0QguL2puKa\r\nm2Lnyt/L/pLyinIpMu4nOXm9hkijaRhmXIkVpWZJkDn0GkJ2x8MyU0QxG5E67Oby8iUepSViLicr\r\nCemHPSfOkMOeMnXsQdnyEAlmgNU/xIRMzW0Xm9xU0k9jSOGATr2A2A/IgT0z+dBcm67LArqsSfqB\r\nd/q8TFsh/7i9KAbH5vXSnB6h82kmNZHvxwGLt5meLXit2txyDdE0PQwayDUtEsroKd14vZ2ZmC2G\r\nRdFwR4zqUWeJ7gquXGOrZWkLPVxhs1vFsSGSgw==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIFdDCCBFygAwIBAgIQVAEBBAGPvYfN/dakG/hWSTANBgkqhkiG9w0BAQsFADBuMQswCQYDVQQG\r\nEwJWTjEYMBYGA1UEChMPRlBUIENvcnBvcmF0aW9uMR8wHQYDVQQLExZGUFQgSW5mb3JtYXRpb24g\r\nU3lzdGVtMSQwIgYDVQQDExtGUFQgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMjAwNjE1MDM1\r\nNzM1WhcNMjMwNjE1MDMyNzAwWjCBvDELMAkGA1UEBhMCVk4xEjAQBgNVBAgMCUjDoCBO4buZaTES\r\nMBAGA1UEBwwJSMOgIE7hu5lpMRwwGgYDVQQKDBNU4buVbmcgQ+G7pWMgVGh14bq/MSMwIQYDVQQD\r\nDBpU4buVbmcgQ+G7pWMgVGh14bq/IFRlc3QwMTEiMCAGCgmSJomT8ixkAQEMEk1TVDowMTAwMjMx\r\nMjI2LTk5ODEeMBwGCSqGSIb3DQEJARYPdGVzdEBmcHQuY29tLnZuMIIBIjANBgkqhkiG9w0BAQEF\r\nAAOCAQ8AMIIBCgKCAQEA5Lb7DhwAfNbM1AfMyiz3oOAm71i0hLKyXqnXWU03pGMtkO+WISzBssGA\r\nSia5kN2jnx0QguL2puKam2Lnyt/L/pLyinIpMu4nOXm9hkijaRhmXIkVpWZJkDn0GkJ2x8MyU0Qx\r\nG5E67Oby8iUepSViLicrCemHPSfOkMOeMnXsQdnyEAlmgNU/xIRMzW0Xm9xU0k9jSOGATr2A2A/I\r\ngT0z+dBcm67LArqsSfqBd/q8TFsh/7i9KAbH5vXSnB6h82kmNZHvxwGLt5meLXit2txyDdE0PQwa\r\nyDUtEsroKd14vZ2ZmC2GRdFwR4zqUWeJ7gquXGOrZWkLPVxhs1vFsSGSgwIDAQABo4IBvTCCAbkw\r\nDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSaphGlCw6MEi0SLfWW1uUXayA8LzCBqAYIKwYBBQUH\r\nAQEEgZswgZgwNwYIKwYBBQUHMAKGK2h0dHA6Ly9wdWJsaWMucm9vdGNhLmdvdi52bi9jcnQvbWlj\r\nbnJjYS5jcnQwOAYIKwYBBQUHMAKGLGh0dHA6Ly9kaWNodnVkaWVudHUuZnB0LmNvbS52bi9jcnQv\r\nZnB0Y2EuY3J0MCMGCCsGAQUFBzABhhdodHRwOi8vb2NzcDIuZmlzLmNvbS52bjBPBgNVHSAESDBG\r\nMEQGCysGAQQBge0DAQQBMDUwMwYIKwYBBQUHAgEWJ2h0dHA6Ly9kaWNodnVkaWVudHUuZnB0LmNv\r\nbS52bi9jcHMuaHRtbDA0BgNVHSUELTArBggrBgEFBQcDAgYIKwYBBQUHAwQGCisGAQQBgjcKAwwG\r\nCSqGSIb3LwEBBTAnBgNVHR8EIDAeMBygGqAYhhZodHRwOi8vY3JsMi5maXMuY29tLnZuMB0GA1Ud\r\nDgQWBBSH7GlEDJPDrAW7kDCOJbGkE4eKuTAOBgNVHQ8BAf8EBAMCBPAwDQYJKoZIhvcNAQELBQAD\r\nggEBAG6y+wbm68PLrfMEnQ4l/cRYoDCcECvE0ylYAJZRE0oiyk1UidBWJXcJBjd43BDpTURNZrBn\r\nNeIR0YgAc5gqTvBCQMCG77sDUOXNmaMmkbltLslMPSK2k1m8Hlh4XQWibS62C3EEdIPQ1Duizxlx\r\ngsfRec2liafVLimNZlArIqurGbx6wtUXjtjQkbt5uZ2akt6kHEjWUx9dLjcjloniyeTLZR6nMSIo\r\nJ2KltSYFq6kp7NV4ClZ7g7Hoa22hKFwSTUsQscHaYbsxj5V9r4BMR0N72F6pAJ/M4bzAYLwhMVJW\r\nEyd2ywChw+gUx5qc7EgtPcqojRqCm4Y13Oz7i9t04BU=</X509Certificate></X509Data></KeyInfo></Signature></SECURITY></DATA>";
                MSG26 objTemp = msg26.MSGToObject(strXMLIn);
                if (objTemp != null)
                {
                    pv_trangthai = objTemp.Body.Row.Chungtu.TRANG_THAI;
                    pv_mota_tt = objTemp.Body.Row.Chungtu.MOTA_TRANGTHAI;
                    log.Info("3.1. IB - 7.verifySignXML - pv_trangthai " + pv_trangthai + "pv_mota_tt " + pv_mota_tt);
                }
                else
                {
                    log.Info("3.1. IB - 7.verifySignXML - pv_trangthai -1 pv_mota_tt không load được object trả về");
                    pv_trangthai = "-1";
                    pv_mota_tt = "không load được object trả về";
                }
                //capnhatSothue(MST, objTemp);
            }
            else
            {
                log.Info("3.1. IB - 7.verifySignXML - pv_trangthai -1 pv_mota_tt không load được object trả v");
                throw new Exception("Sai chu ky");
            }
        }
        public static void CTUToMSG21_CN_TB(String pv_strMaNTT, String pv_strSHKB,
                                            String pv_strSoCT, String pv_strSoBT,
                                            String pv_strMaNV, String ma_hthuc_nop, ref MSG21IB ms21)
        {

            //Lay thong tin Header CTU
            StringBuilder sbsql = new StringBuilder();
            DataSet ds = null;
            string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);
            //mac dinh la ko xac dinh
            string loai_nnt = "2";
            string dgiai_ht_nt = "01";
            try
            {
                dgiai_ht_nt = DataAccess.ExecuteReturnDataSet("select * from tcs_dm_hthuc_nthue where ma_hthuc_nthue = '" + ma_hthuc_nop + "'", CommandType.Text).Tables[0].Rows[0]["dgiai_hthuc_nthue"].ToString();
                log.Info("3.1. IB - 1.2.CTUToMSG21_CN_TB - dgiai_ht_nt " + dgiai_ht_nt);
            }
            catch (Exception e)
            {
                log.Info("3.1. IB - 1.2.CTUToMSG21_CN_TB - error: " + e.StackTrace);
                dgiai_ht_nt = "Nộp tại quầy";
            }
            try
            {
                //Lay thong tin header
                sbsql.Append("SELECT   h.ten_nnthue ten_nnthue,");
                sbsql.Append("         h.ma_nnthue ma_nnt,");
                sbsql.Append("         h.dc_nnthue dc_nnthue,");
                sbsql.Append("         h.ma_nntien ma_nntien,");
                sbsql.Append("         h.ten_nntien ten_nntien,");
                sbsql.Append("         h.dc_nntien dc_nntien,");
                sbsql.Append("         h.tk_co tk_co,");
                sbsql.Append("         h.ma_ntk ma_ntk,");
                sbsql.Append("         h.ma_cqthu ma_cqthu,");
                sbsql.Append("         h.so_ct so_ct,");
                sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'dd/MM/yyyy') ngay_ct,");
                sbsql.Append("         h.ma_nh_b ma_nh_b, h.shkb, h.ma_nt, h.SO_KHUNG, h.SO_MAY, h.SO_QD, h.DIA_CHI_TS, h.DAC_DIEM_PTIEN ");
                sbsql.Append("  FROM   tcs_ctu_hdr h");
                sbsql.Append(" WHERE   ((h.shkb = :shkb)");
                sbsql.Append("          AND (h.ma_nv = :ma_nv)");
                sbsql.Append("          AND (h.so_ct = '" + pv_strSoCT + "')");
                sbsql.Append("          AND (h.so_bt = :so_bt)");
                sbsql.Append("          AND (h.NGAY_KB = :ngay_kb))");
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[4];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SHKB";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = pv_strSHKB;

                p[1] = new OracleParameter();
                p[1].ParameterName = "MA_NV";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = pv_strMaNV;

                p[2] = new OracleParameter();
                p[2].ParameterName = "SO_BT";
                p[2].DbType = DbType.String;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = pv_strSoBT;
                p[3] = new OracleParameter();
                p[3].ParameterName = "ngay_kb";
                p[3].DbType = DbType.String;
                p[3].Direction = ParameterDirection.Input;
                p[3].Value = v_ngaylv;



                ds = DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không tìm thấy chứng từ!");
                }
                string strTinhChat_KhoanNop = "";
                ms21.Body.Row.CHUNGTU.MST = ds.Tables[0].Rows[0]["ma_nnt"].ToString();
                ms21.Body.Row.CHUNGTU.TEN_NNT = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                ms21.Body.Row.CHUNGTU.DIACHI_NNT = ds.Tables[0].Rows[0]["dc_nnthue"].ToString();
                ms21.Body.Row.CHUNGTU.MST_NNTHAY = ds.Tables[0].Rows[0]["ma_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.TEN_NNTHAY = ds.Tables[0].Rows[0]["ten_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.DIACHI_NNTHAY = ds.Tables[0].Rows[0]["dc_nntien"].ToString();
                ms21.Body.Row.CHUNGTU.SO_TK_NHKBNN = ds.Tables[0].Rows[0]["tk_co"].ToString();
                ms21.Body.Row.CHUNGTU.CQ_QLY_THU = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
                ms21.Body.Row.CHUNGTU.SO_CHUNGTU = ds.Tables[0].Rows[0]["so_ct"].ToString();
                ms21.Body.Row.CHUNGTU.NGAY_CHUNGTU = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                if (ds.Tables[0].Rows[0]["ma_ntk"].ToString() == "1")
                {
                    strTinhChat_KhoanNop = "Nộp NSNN";
                }
                else
                {
                    strTinhChat_KhoanNop = "Nộp HT GTGT";
                }
                ms21.Body.Row.CHUNGTU.TINHCHAT_KHOAN_NOP = strTinhChat_KhoanNop;
                ms21.Body.Row.CHUNGTU.CQUAN_THAMQUYEN = strCoQuanTQ;
                ms21.Body.Row.CHUNGTU.MA_NHTM = ds.Tables[0].Rows[0]["ma_nh_b"].ToString();
                //new for thue ca nhan, thue truoc ba
                ms21.Body.Row.CHUNGTU.MA_DVTT = strSender_Code;

                ////chỗ này điền mã, không điền tên -> cần lấy ra mã
                //string ma_huyen = "";
                //string ma_tinh = "";
                //if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ten_huyen"].ToString().Trim()))
                //{
                //    ma_huyen = "";
                //}
                //else
                //{
                //    string huyen = ds.Tables[0].Rows[0]["ten_huyen"].ToString();
                //    string[] arrhuyen = huyen.Split('-');
                //    ma_huyen = arrhuyen[0];
                //}

                ////
                //if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ten_tinh"].ToString().Trim()))
                //{
                //    ma_tinh = "";
                //}
                //else
                //{
                //    string tinh = ds.Tables[0].Rows[0]["ten_tinh"].ToString();
                //    string[] arrtinh = tinh.Split('-');
                //    ma_tinh = arrtinh[0];
                //}

                ////


                //ms21.Body.Row.CHUNGTU.HUYEN = ma_huyen.Trim();
                //ms21.Body.Row.CHUNGTU.TINH = ma_tinh.Trim();
                //tạm thời để trống
                ms21.Body.Row.CHUNGTU.HUYEN = "";
                ms21.Body.Row.CHUNGTU.TINH = "";
                // end

                ms21.Body.Row.CHUNGTU.SH_KBNN = ds.Tables[0].Rows[0]["shkb"].ToString();
                ms21.Body.Row.CHUNGTU.LOAI_TIEN = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                ms21.Body.Row.CHUNGTU.MA_HTHUC_NOP = ma_hthuc_nop;
                ms21.Body.Row.CHUNGTU.DIENGIAI_HTHUC_NOP = dgiai_ht_nt;
                ms21.Body.Row.CHUNGTU.LHINH_NNT = loai_nnt;
                ms21.Body.Row.CHUNGTU.SO_KHUNG = ds.Tables[0].Rows[0]["SO_KHUNG"].ToString();
                ms21.Body.Row.CHUNGTU.SO_MAY = ds.Tables[0].Rows[0]["SO_MAY"].ToString();
                ms21.Body.Row.CHUNGTU.SO_QUYET_DINH = ds.Tables[0].Rows[0]["SO_QD"].ToString();
                ms21.Body.Row.CHUNGTU.DIA_CHI_TS = ds.Tables[0].Rows[0]["DIA_CHI_TS"].ToString();
                ms21.Body.Row.CHUNGTU.DAC_DIEM_PTIEN = ds.Tables[0].Rows[0]["DAC_DIEM_PTIEN"].ToString();
                ms21.Body.Row.CHUNGTU.NGAY_NTIEN = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                ms21.Body.Row.CHUNGTU.MATHAMCHIEU = "";


                //Lay thong tin detail
                sbsql = new StringBuilder();
                sbsql.Append("SELECT   d.ky_thue,");
                sbsql.Append("         d.ma_chuong,");
                sbsql.Append("         d.ma_khoan,");
                sbsql.Append("         d.ma_tmuc,");
                sbsql.Append("        d.noi_dung,");
                sbsql.Append("         d.sotien");
                sbsql.Append("  FROM   tcs_ctu_dtl d");
                sbsql.Append(" WHERE   (    (d.shkb = :shkb)");
                sbsql.Append("          AND (d.ma_nv = :ma_nv)");
                sbsql.Append("          AND (d.so_bt = :so_bt)");
                sbsql.Append("          AND (d.ngay_kb = :ngay_kb))");


                //Lay thong tin chi tiet chung tu

                ds = DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không tìm thấy chứng từ!");
                }
                List<ProcMegService.MSG.MSG21IB.CHUNGTU_CHITIET> listItems = new List<ProcMegService.MSG.MSG21IB.CHUNGTU_CHITIET>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ProcMegService.MSG.MSG21IB.CHUNGTU_CHITIET item = new ProcMegService.MSG.MSG21IB.CHUNGTU_CHITIET();
                    item.CHUONG = row["ma_chuong"].ToString();
                    item.TIEUMUC = row["ma_tmuc"].ToString();
                    item.THONGTIN_KHOANNOP = row["noi_dung"].ToString();

                    item.SO_TIEN = row["sotien"].ToString();
                    item.KY_THUE = row["ky_thue"].ToString();
                    listItems.Add(item);
                }
                ms21.Body.Row.CHUNGTU.CHUNGTU_CHITIET = listItems;
            }
            catch (Exception ex)
            {
                StringBuilder sbErrMsg = default(StringBuilder);
                sbErrMsg = new StringBuilder();
                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUToMSG21IB): ĐTNT - ");
                sbErrMsg.Append(pv_strMaNTT);
                sbErrMsg.Append(", SHKB - ");
                sbErrMsg.Append(pv_strSHKB);
                sbErrMsg.Append(", So CT - ");
                sbErrMsg.Append(pv_strSoCT);
                sbErrMsg.Append(", So BT - ");
                sbErrMsg.Append(pv_strSoBT);
                sbErrMsg.Append(", Ma NV - ");
                sbErrMsg.Append(pv_strMaNV);
                sbErrMsg.Append("\n");
                sbErrMsg.Append(ex.Message);
                sbErrMsg.Append("\n");
                sbErrMsg.Append(ex.StackTrace);

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

                if ((ex.InnerException != null))
                {
                    sbErrMsg = new StringBuilder();
                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUToMSG21): ĐTNT - ");
                    sbErrMsg.Append(pv_strMaNTT);
                    sbErrMsg.Append(", SHKB - ");
                    sbErrMsg.Append(pv_strSHKB);
                    sbErrMsg.Append(", So CT - ");
                    sbErrMsg.Append(pv_strSoCT);
                    sbErrMsg.Append(", So BT - ");
                    sbErrMsg.Append(pv_strSoBT);
                    sbErrMsg.Append(", Ma NV - ");
                    sbErrMsg.Append(pv_strMaNV);
                    sbErrMsg.Append("\n");
                    sbErrMsg.Append(ex.InnerException.Message);
                    sbErrMsg.Append("\n");
                    sbErrMsg.Append(ex.InnerException.StackTrace);

                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
                }

                throw ex;

            }
            finally
            {
            }



        }
    }
}
