﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace ProcMegService.MSG.MSG047
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG047
    {
        public MSG047()
        {
        }
        [XmlElement("HEADER")]
        public HEADER Header;
        [XmlElement("BODY")]
        public BODY Body;
        [XmlElement("SECURITY")]
        public SECURITY Security;



        public MSG047 MSGToObject(string p_XML)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ProcMegService.MSG.MSG047.MSG047));
                XmlReader reader = XmlReader.Create(new StringReader(p_XML));
                ProcMegService.MSG.MSG047.MSG047 LoadedObjTmp = (ProcMegService.MSG.MSG047.MSG047)serializer.Deserialize(reader);
                return LoadedObjTmp;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string MSG06toXML(MSG047 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG047));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }
    public class BODY
    {
        public BODY()
        {
        }

        [XmlElement("ROW")]
        public ROW Row;
    }
    public class ROW
    {
        public ROW()
        { }
        public string TYPE { get; set; }
        public string NAME { get; set; }
        [XmlElement("THONGTIN_NNT")]
        public THONGTIN_NNT Thongtin_NNT;
    }
    public class THONGTIN_NNT
    {
        public THONGTIN_NNT()
        { }
        [XmlElement("THONGTINCHUNG")]
        public THONGTINCHUNG Thongtinchung;
        [XmlElement("DIACHI")]
        public DIACHI Diachi;
        [XmlElement("SOTHUE")]
        public SOTHUE Sothue;
    }
    public class THONGTINCHUNG
    {
        public THONGTINCHUNG()
        { }
        [XmlElement("ROW_NNT")]
        public List<ROW_NNT> row_nnt;
    }
    public class ROW_NNT
    {
        public ROW_NNT()
        { }
        public string MST { get; set; }
        public string TEN_NNT { get; set; }
        public string LOAI_NNT { get; set; }
        public string CHUONG { get; set; }
        public string SO { get; set; }
        public string SO_CMND { get; set; }
        public string MA_CQT_QL { get; set; }
    }
    public class DIACHI
    {
        public DIACHI()
        { }
        [XmlElement("ROW_DIACHI")]
        public List<ROW_DIACHI> Row_Diachi;
    }
    public class ROW_DIACHI
    {
        public ROW_DIACHI()
        { }
        public string MOTA_DIACHI { get; set; }
        public string MA_TINH { get; set; }
        public string MA_HUYEN { get; set; }
        public string MA_XA { get; set; }

    }
    public class SOTHUE
    {
        public SOTHUE()
        { }
        [XmlElement("ROW_SOTHUE")]
        public List<ROW_SOTHUE> Row_Sothue;
    }
    public class ROW_SOTHUE
    {
        public ROW_SOTHUE()
        { }
        public string NO_CUOI_KY { get; set; }
        //public string SO_PHAINOP { get; set; }
        public string MA_CHUONG { get; set; }
        public string MA_CQ_THU { get; set; }
        public string MA_TMUC { get; set; }
        public string SHKB { get; set; }
        public string SO_TAI_KHOAN_CO { get; set; }
        //public string SO_DAHOAN { get; set; }
        //public string TK_THUHOI_QHT { get; set; }
        public string LOAI_TIEN { get; set; }
        public string SO_QDINH { get; set; }
        public string NGAY_QDINH { get; set; }
        public string TI_GIA { get; set; }
        public string TEN_CQ_THU { get; set; }
        public string TEN_TMUC { get; set; }
        public string TEN_KB { get; set; }
        public string NOI_DUNG { get; set; }
        public string LOAI_THUE { get; set; }
        public string SO_KHUNG { get; set; }
        public string SO_MAY { get; set; }
        public string MA_DBHC { get; set; }
        public string KY_THUE { get; set; }
        public string DAC_DIEM_PTIEN { get; set; }

        public string SO { get; set; }
        public string DIA_CHI_TS { get; set; }
        public string MA_HS { get; set; }
        public string MA_GCN { get; set; }
        public string SO_THUADAT { get; set; }
        public string SO_TO_BANDO { get; set; }


    }
    public class infoTRUYVANTHUE
    {
        public string MST = "";
        public string TEN_NNT = "";
        public string LOAI_NNT = "";
        public string MA_CQT_QL = "";
        public string MOTA_DIACHI = "";
        public string MA_TINH = "";
        public string MA_HUYEN = "";
        public string MA_XA = "";
        public string TEN_TINH = "";
        public string TEN_HUYEN = "";
        public string TEN_XA = "";

        public List<ROW_SOTHUE> ROW_SOTHUE = new List<ROW_SOTHUE>();
    }


    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }

    }
    public class SECURITY
    {
        public SECURITY()
        {
        }
        [XmlElement("SIGNATURE")]
        public SIGNATURE Signature;
    }
    public class SIGNATURE
    {
        public SIGNATURE()
        { }
        public string SignatureValue { get; set; }
    }
}
