﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class RequestDanhMuc
    {
       public Header header;

       public ReqDataDM data;

       public Signature signature;

    }
    public class ReqDataDM
    {
        public string type { get; set; }
        public string ma { get; set; }
    }
}
