﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class HuyCTReq
    {
        public Header header;

        public ReqDataHuyCT data;

        public Signature signature;
    }
    public class ReqDataHuyCT
    {
        public string ma_nhtm { get; set; }
        public string so_chungtu { get; set; }
        public string ma_tham_chieu { get; set; }
    }
}
