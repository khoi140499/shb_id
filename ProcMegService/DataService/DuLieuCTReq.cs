﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class DuLieuCTReq
    {
        public Header header;

        public ReqDataDuLieuCT data;

        public Signature signature;
    }
    public class ReqDataDuLieuCT
    {
        public string mst { get; set; }
        public string mst_nnthay { get; set; }
        public string tungay_ctu { get; set; }
        public string denngay_ctu { get; set; }
        public string trangthai { get; set; }
    }
}
