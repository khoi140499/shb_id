﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class DangKyThueRes
    {
        public Header header;

        public string status { get; set; }
        public string errorMessage { get; set; }

        public List<ResponseDataDKT> data;

        public Signature signature;
    }
    public class ResponseDataDKT
    {
        public string MST_NNT { get; set; }
        public string TEN_NNT { get; set; }
        public string LOAI_NNT { get; set; }
        public string TRANG_THAI { get; set; }
        public string NGAY_CAP_MST { get; set; }
        public string SO_CMND { get; set; }
        public string CCCD { get; set; }
        public string CHUONG { get; set; }
        public string DIA_CHI { get; set; }
        public string TINH_TP { get; set; }
        public string QUAN_HUYEN { get; set; }
        public string PHUONG_XA { get; set; }
        public string NGAY_TDOI { get; set; }
    }
}
