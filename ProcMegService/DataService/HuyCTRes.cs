﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class HuyCTRes
    {
        public Header header;

        public string status { get; set; }
        public string errorMessage { get; set; }

        public List<ResponseDataHuyCT> data;

        public Signature signature;
    }
    public class ResponseDataHuyCT
    {
        public string ma_nhtm { get; set; }
        public string so_chungtu { get; set; }
        public string ma_thamchieu { get; set; }
        public string trang_thai { get; set; }
        public string ten_trang_thai { get; set; }
    }
}
