﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class TruyVanSoThueResponse
    {
        public Header header;

        public string status { get; set; }
        public string errorMessage { get; set; }

        public List<ResponseDataSoThue> data;

        public Signature signature;
    }
    public class ResponseDataSoThue
    {
        public THONGTIN_NNT THONGTIN_NNT;

        public TONG_TIEN_NNT TONG_TIEN_NNT;

        public List<CHITIET_SOTHUE> KHOAN_THUE;
        public List<CHITIET_SOTHUE> KHOAN_THU_KHAC;
    }
    public class THONGTIN_NNT
    {
        public string MST { get; set; }
        public string TEN_NNT { get; set; }
        public string LOAI_NNT { get; set; }
        public string SO_GIAYTO { get; set; }
        public string LOAI_GIAYTO { get; set; }
        public string CHUONG_NNT { get; set; }
        public string MA_CQT_QLY { get; set; }
        public string TEN_CQT_QLY { get; set; }
        public string DIA_CHI_NNT { get; set; }
        public string MA_TINH_NNT { get; set; }
        public string TEN_TINH_NNT { get; set; }
        public string MA_HUYEN_NNT { get; set; }
        public string TEN_HUYEN_NNT { get; set; }
        public string MA_XA_NNT { get; set; }
        public string TEN_XA_NNT { get; set; }
    }
    public class TONG_TIEN_NNT
    {
        public string TONG_TIEN { get; set; }
        public string LOAI_TIEN { get; set; }
    }
    public class CHITIET_SOTHUE
    {
        public string THUTU_UUTIEN { get; set; }
        public string MA_NHOM { get; set; }
        public string THUTU_NHOM { get; set; }
        public string ID_KHOANNOP { get; set; }
        public string MA_DDKD { get; set; }
        public string TEN_DDKD { get; set; }
        public string MA_TINH_DDKD { get; set; }
        public string TEN_TINH_DDKD { get; set; }
        public string MA_HUYEN_DDKD { get; set; }
        public string TEN_HUYEN_DDKD { get; set; }
        public string MA_XA_DDKD { get; set; }
        public string TEN_XA_DDKD { get; set; }
        public string KY_THUE { get; set; }
        public string SO_QDINH_TBAO { get; set; }
        public string NGAY_QUYET_DINH { get; set; }
        public string CQUAN_TQUYEN { get; set; }
        public string TEN_CQUAN_TQUYEN { get; set; }
        public string MA_CQTHU { get; set; }
        public string TEN_CQTHU { get; set; }
        public string DBHC_MA_TINH { get; set; }
        public string DBHC_TEN_TINH { get; set; }
        public string DBHC_MA_HUYEN { get; set; }
        public string DBHC_TEN_HUYEN { get; set; }
        public string DBHC_MA_XA { get; set; }
        public string DBHC_TEN_XA { get; set; }
        public string SHKB { get; set; }
        public string TEN_KB { get; set; }
        public string MA_PNN { get; set; }
        public string CHUONG { get; set; }
        public string TIEU_MUC { get; set; }
        public string TEN_TMUC { get; set; }
        public string KYHIEU_GD { get; set; }
        public string TEN_GD { get; set; }
        public string MA_TC_GD { get; set; }
        public string TEN_TC_GD { get; set; }
        public string MAU_TKHAI { get; set; }
        public string TEN_TKHAI { get; set; }
        public string NGAY_HTOAN { get; set; }
        public string MA_NAM_NS { get; set; }
        public string TEN_NAM_NS { get; set; }
        public string TK_NSNN { get; set; }
        public string LOAI_TIEN { get; set; }
        public string TY_GIA { get; set; }
        public string MA_LOAI_NVU { get; set; }
        public string TEN_LOAI_NVU { get; set; }
        public string SO_TIEN { get; set; }
        public string SO_DANOP_NHTM { get; set; }
        public string NGAY_TCN_DEN { get; set; }
        public string HAN_NOP { get; set; }
        public string LOAI_THUE { get; set; }
        public string TEN_LOAI_THUE { get; set; }
        public string MA_HS { get; set; }
        public string MA_GCN { get; set; }
        public string SO_THUADAT { get; set; }
        public string SO_TO_BANDO { get; set; }
        public string SO_GIAYTO_HSKT { get; set; }
        public string DIA_CHI_HSKT { get; set; }
        public string MA_TINH_HSKT { get; set; }
        public string TEN_TINH_HSKT { get; set; }
        public string MA_HUYEN_HSKT { get; set; }
        public string TEN_HUYEN_HSKT { get; set; }
        public string MA_XA_HSKT { get; set; }
        public string TEN_XA_HSKT { get; set; }
        public string MA_TINHCHATKHOANNOP { get; set; }
        public string TEN_TINHCHATKHOANNOP { get; set; }
        public string MA_TRANGTHAI { get; set; }
        public string TEN_TRANGTHAI { get; set; }
        public string SO_KHUNG { get; set; }
        public string SO_MAY { get; set; }
        public string DACDIEM_PTIEN { get; set; }
        public string NGAY_GIA_HAN { get; set; }
        public string MA_TCHIEU_GOC { get; set; }
        public string MA_TCHIEU_CTU { get; set; }

    }
    public class infoTRUYVANTHUE
    {
     
        public string MST = "";
        public string TEN_NNT = "";
        public string LOAI_NNT = "";
        public string SO_GIAYTO = "";
        public string LOAI_GIAYTO = "";
        public string MA_CQT_QLY = "";
        public string DIA_CHI_NNT = "";
        public string MA_TINH_NNT = "";
        public string MA_HUYEN_NNT = "";
        public string MA_XA_NNT = "";
        public string TEN_TINH_NNT = "";
        public string TEN_HUYEN_NNT = "";
        public string TEN_XA_NNT = "";

        public List<CHITIET_SOTHUE> ROW_SOTHUE = new List<CHITIET_SOTHUE>();
    }
}
