﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class TruyVanSoThueReq
    {
        public Header header;

        public DataTruyVanSoThue data;

        public Signature signature;

    }
    public class DataTruyVanSoThue
    {
        public string ID_KHOANNOP { get; set; }
        public string MST { get; set; }
        public string LOAI_DL { get; set; }
        public string SO_QDINH_TBAO { get; set; }
        public string MA_HS { get; set; }
        public string SO_GIAYTO { get; set; }
    }
   
}
