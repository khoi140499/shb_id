﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class ResponseDataDM
    {
        //danh mục cơ quan thu
        public string MA_CQTHU { get; set; }
        public string TEN_CQTHU { get; set; }
        public string CQT_QLT { get; set; }
        public string CQT_TMS { get; set; }
        public string MA_QUOC_GIA { get; set; }
        public string DIACHI { get; set; }
        public string HIEU_LUC_TU { get; set; }
        public string HIEU_LUC_DEN { get; set; }
        public string SHKB { get; set; }


        //DBHC
        public string MA_TINH { get; set; }
        public string TEN_TINH { get; set; }
        public string MA_HUYEN { get; set; }
        public string TEN_HUYEN { get; set; }
        public string MA_XA { get; set; }
        public string TEN_XA { get; set; }

        //Kho bac
        public string MA_KBAC { get; set; }
        public string TEN_KBAC { get; set; }
        public string NGAY_HLUC_TU { get; set; }
        public string NGAY_HLUC_DEN { get; set; }

        //chuong
        public string MA_CHUONG { get; set; }
        public string TEN_CHUONG { get; set; }
        public string MA_CAP { get; set; }
        //public string NGAY_HLUC_TU { get; set; }
        //public string NGAY_HLUC_DEN { get; set; }

        //danh mục khoan
        public string MA_KHOAN { get; set; }
        public string TEN_KHOAN { get; set; }
        //public string NGAY_HLUC_TU { get; set; }
        //public string NGAY_HLUC_DEN { get; set; }


        //danh tieu muc
        public string MA_TMUC { get; set; }
        public string TEN_TMUC { get; set; }
        public string MA_MUC { get; set; }
        //public string NGAY_HLUC_TU { get; set; }
        //public string NGAY_HLUC_DEN { get; set; }


        //danh tai khoan kho bac
        public string MA_TK_KBAC { get; set; }
        public string TEN_TK_KBAC { get; set; }
        //public string NGAY_HLUC_TU { get; set; }
        //public string NGAY_HLUC_DEN { get; set; 
   
    }
}
