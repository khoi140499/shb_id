﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class Signature
    {
        public string value { get; set; }
        public string certificates { get; set; }
    }
}
