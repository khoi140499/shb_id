﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class DuLieuCTRes
    {
        public Header header;

        public string status { get; set; }
        public string errorMessage { get; set; }

        public List<ResponseDataDuLieuCT> data;

        public Signature signature;
    }
    public class ResponseDataDuLieuCT
    {
        public string MST_NNT { get; set; }
        public string TEN_NNT { get; set; }
        public string DIACHI_NNT { get; set; }
        public string MST_NNTHAY { get; set; }
        public string TEN_NNTHAY { get; set; }
        public string DIACHI_NNTHAY { get; set; }
        public string SO_TK_NHKBNN { get; set; }
        public string CQ_QLY_THU { get; set; }
        public string SO_CHUNGTU { get; set; }
        public string NGAY_CHUNGTU { get; set; }
        public string TINHCHAT_KHOAN_NOP { get; set; }
        public string TRANGTHAI_CHUNGTU { get; set; }
        public string CQUAN_THAMQUYEN { get; set; }
        public string MA_NHTM { get; set; }
        public string TRANG_THAI { get; set; }
        public string DENNGAY_CHUNGTU { get; set; }
        public string TINH { get; set; }
        public string HUYEN { get; set; }
        public string SO_QUYET_DINH { get; set; }
        public string DIA_CHI_TS { get; set; }
        public string SO_KHUNG { get; set; }
        public string SO_MAY { get; set; }
        public string NGAY_NOP_TIEN { get; set; }
        public string CHUNGTU_CHITIET { get; set; }
        public string CHUONG { get; set; }
        public string TIEUMUC { get; set; }
        public string THONGTIN_KHOANNOP { get; set; }
        public string SO_TIEN { get; set; }
        public string KY_THUE { get; set; }
    }
}
