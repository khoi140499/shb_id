﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class DangKyThueReq
    {
        public Header header;

        public ReqDataDKT data;

        public Signature signature;
    }
    public class ReqDataDKT
    {
        public string mst { get; set; }
        public string so_giayto { get; set; }
    }
}
