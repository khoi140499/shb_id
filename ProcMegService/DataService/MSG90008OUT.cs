﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class MSG90008OUT
    {
        public Header header { get; set; }
        public Data data { get; set; }
        public Signature signature { get; set; }
    }
    public class Data
    {
        public string so_tchieu { get; set; }
    }
}
