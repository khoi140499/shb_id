﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class Header
    {
        public string sender_code { get; set; }
        public string sender_name { get; set; }
        public string receiver_code { get; set; }
        public string receiver_name { get; set; }
        public string tran_code { get; set; }
        public string msg_id { get; set; }
        public string msg_refid { get; set; }
        public string send_date { get; set; }

        public Header()
        {
            sender_code = ConfigurationManager.AppSettings.Get("ID.SENDER_CODE").ToString();
            sender_name = ConfigurationManager.AppSettings.Get("ID.SENDER_NAME").ToString();
            receiver_code = "Dataservice";
            receiver_name = "He thong Dataservice";
            tran_code = "";
            msg_id = "";
            msg_refid = "";
            send_date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
        }
        public Header(string pv_tran_code, string REQUEST_ID)
        {
            sender_code = ConfigurationManager.AppSettings.Get("ID.SENDER_CODE").ToString();
            sender_name = ConfigurationManager.AppSettings.Get("ID.SENDER_NAME").ToString();
            receiver_code = "Dataservice";
            receiver_name = "He thong Dataservice";
            tran_code = pv_tran_code;
            msg_id = REQUEST_ID;
            msg_refid = "";
            send_date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
        }
       
    }

}
