﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcMegService.DataService
{
    public class MSG90008IN
    {
        public Header header { get; set; }
        public int status { get; set; }
        public string type { get; set; }
        public string errorMessage { get; set; }
        public int total { get; set; }
        public DataIn data { get; set; }
        public Signature signature { get; set; }
    }
    public class DataIn
    {
        public THONGTIN_NNT_90008 THONGTIN_NNT { get; set; }
        public THONGTIN_KHOANNOP THONGTIN_KHOANNOP { get; set; }
    }
    public class THONGTIN_NNT_90008
    {
        public string MST_NNT { get; set; }
        public string TEN_NNT { get; set; }
    }
    public class THONGTIN_KHOANNOP
    {
        public CHUNGTU_TTIN_CHUNG CHUNGTU_TTIN_CHUNG { get; set; }
        public List<CTIET_CHUNGTU> CTIET_CHUNGTU { get; set; }
    }
    public class CHUNGTU_TTIN_CHUNG
    {
        public string MA_TCHIEU { get; set; }
        public string SO_CHUNGTU { get; set; }
        public string MA_CQT { get; set; }
        public string TEN_CQT { get; set; }
        public string LOAI_TIEN { get; set; }
        public string MA_TK_THU { get; set; }
        public string TEN_TK_THU { get; set; }
        public string MA_TK_KNGHI { get; set; }
        public string TEN_TK_KNGHI { get; set; }
        public string MA_CQTHU { get; set; }
        public string TEN_CQTHU { get; set; }
        public string MA_KBNN { get; set; }
        public string TEN_KBNN { get; set; }
        public string NGAY_CTU { get; set; }
    }
    public class CTIET_CHUNGTU
    {
        public string ID_KHOANNOP { get; set; }
        public string SO_QDINH { get; set; }
        public string NGAY_QDINH { get; set; }
        public string CHUONG { get; set; }
        public string MA_NDKT { get; set; }
        public string TEN_NDKT { get; set; }
        public string SO_TIEN { get; set; }
        public string DBHC_MA_TINH { get; set; }
        public string DBHC_TEN_TINH { get; set; }
        public string DBHC_MA_HUYEN { get; set; }
        public string DBHC_TEN_HUYEN { get; set; }
        public string DBHC_MA_XA { get; set; }
        public string DBHC_TEN_XA { get; set; }

    }
}
