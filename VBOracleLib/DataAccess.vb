

'Imports System.Configuration.ConfigurationSettings
Imports System.Data.OracleClient
Imports VBOracleLib.Globals
Imports log4net
Public Class DataAccess
    'bat buoc trong file app.config ta phai gan key="Main.Connection" cho xau ket noi
    Public Shared strConnection As String = Configuration.ConfigurationManager.AppSettings("CTC_DB_CONN").ToString
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    'Public Shared strConnection As String = VBOracleLib.Security.DescryptStr(strConnection1)
    '  Private Shared _connection As OracleConnection
    ' Private Shared _command As OracleCommand

    Public Shared Function GetConnectionOpen()

        Try

            Dim _conn As OracleConnection = New OracleConnection(strConnection)
            _conn.Open()
            Return _conn
        Catch ex As Exception
            log.Error(ex.Message + ex.StackTrace)
        End Try
        Return Nothing
    End Function
    Public Shared Function GetConnectionNo() As OracleConnection
        Dim conn As OracleConnection = Nothing
        If conn Is Nothing Then
            conn = New OracleConnection(strConnection)
            conn.Open()
        End If
        Return conn
    End Function

#Region "Utility"
    Public Shared Function GetCommand() As OracleCommand
        Dim _command As OracleCommand
        If _command Is Nothing Then
            _command = New OracleCommand
        End If
        Return _command
    End Function
    Public Shared Function GetConnection() As OracleConnection
        Dim _connection As OracleConnection
        If _connection Is Nothing Then
            _connection = New OracleConnection(strConnection)
        End If
        _connection.Open()
        Return _connection
    End Function
#End Region

#Region "ExecuteNonQuery"
    'ham nay thuc hien truy van tham doi la mot bien oracleCommand
    Public Shared Function Execute(ByVal oCommand As OracleCommand) As Integer
        Return oCommand.ExecuteNonQuery()
    End Function

    Public Shared Function ExecuteNonQuery(ByVal Statement As String, ByVal arrParamIn As IDataParameter()) As Integer
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Dim rows As Integer
        'Dim outList As New ArrayList

        Try
            Dim p As IDataParameter
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            oCommand.CommandText = Statement
            oCommand.CommandType = CommandType.Text
            oConnect.Open()
            If arrParamIn IsNot Nothing AndAlso arrParamIn.Length > 0 Then
                For Each p In arrParamIn
                    oCommand.Parameters.Add(p)
                Next
            End If
            oCommand.Connection = oConnect
            rows = oCommand.ExecuteNonQuery()
            '' Read the return values and output parameters
            'For Each p In oCommand.Parameters
            '    If p.Direction <> ParameterDirection.Input Then
            '        outList.Add(p.Value)
            '    End If
            'Next

            oCommand.Parameters.Clear()
        Catch ex As Exception
            Throw ex
        Finally
            If Not oCommand Is Nothing Then oCommand.Dispose()
            If Not oConnect Is Nothing Then oConnect.Dispose()
        End Try

        Return rows
    End Function

    Public Shared Function ExecuteNonQuery(ByVal Statement As String _
                                    , ByVal cmdType As CommandType _
                                    , ByVal arrParamIn As IDataParameter() _
            ) As ArrayList
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Dim rows As Integer
        Dim outList As New ArrayList

        Try
            Dim p As IDataParameter
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            oCommand.CommandText = Statement
            oCommand.CommandType = cmdType
            oConnect.Open()
            For Each p In arrParamIn
                oCommand.Parameters.Add(p)
            Next
            oCommand.Connection = oConnect
            rows = oCommand.ExecuteNonQuery()
            ' Read the return values and output parameters
            For Each p In oCommand.Parameters
                If p.Direction <> ParameterDirection.Input Then
                    outList.Add(p.Value)
                End If
            Next
            oCommand.Parameters.Clear()
            Return outList
        Catch ex As Exception
            Throw ex
        Finally
            If Not oCommand Is Nothing Then oCommand.Dispose()
            If Not oConnect Is Nothing Then oConnect.Dispose()
        End Try
    End Function

    Public Shared Function ExecuteNonQuery(ByVal Statement As String _
                                    , ByVal arrParamIn As IDataParameter() _
                                    , ByVal transaction As IDbTransaction) As Integer
        Dim oCommand As OracleCommand = Nothing
        Dim rows As Integer = -1
        Dim leaveOpen As Boolean = False
        Try

            Dim p As IDataParameter
            oCommand = GetCommand()
            oCommand.CommandText = Statement
            oCommand.CommandType = CommandType.Text
            oCommand.Connection = transaction.Connection
            oCommand.Transaction = transaction
            If oCommand.Connection.State = ConnectionState.Closed OrElse oCommand.Connection.State = ConnectionState.Broken Then
                oCommand.Connection.Open()
            Else
                leaveOpen = True
            End If

            If arrParamIn IsNot Nothing AndAlso arrParamIn.Length > 0 Then
                For Each p In arrParamIn
                    oCommand.Parameters.Add(p)
                Next
            End If

            rows = oCommand.ExecuteNonQuery()
            oCommand.Parameters.Clear()
        Catch ex As Exception
            oCommand.Parameters.Clear()
            Throw ex
        Finally
            If Not leaveOpen Then
                oCommand.Connection.Close()
                If Not oCommand Is Nothing Then
                    oCommand.Dispose()
                End If
            End If

        End Try

        Return rows
    End Function

    Public Shared Function ExecuteNonQuery(ByVal Statement As String _
                                    , ByVal cmdType As CommandType _
                                    , ByVal arrParamIn As IDataParameter() _
                                    , ByVal transaction As IDbTransaction _
        ) As ArrayList
        Dim oCommand As OracleCommand = Nothing
        Dim rows As Integer
        Dim outList As New ArrayList
        Dim leaveOpen As Boolean = False
        Try
            Dim p As IDataParameter
            oCommand = GetCommand()
            oCommand.CommandText = Statement
            oCommand.CommandType = cmdType
            oCommand.Connection = transaction.Connection
            oCommand.Transaction = transaction
            If oCommand.Connection.State = ConnectionState.Closed Then
                oCommand.Connection.Open()
            Else
                leaveOpen = True
            End If

            For Each p In arrParamIn
                oCommand.Parameters.Add(p)
            Next
            rows = oCommand.ExecuteNonQuery()
            ' Read the return values and output parameters
            For Each p In oCommand.Parameters
                If p.Direction <> ParameterDirection.Input Then
                    outList.Add(p.Value)
                End If
            Next
            oCommand.Parameters.Clear()
            Return outList
        Catch ex As Exception
            Throw ex
        Finally
            If Not leaveOpen Then oCommand.Connection.Close()
        End Try

    End Function
    'Public Shared Function test(ByVal Statement As String _
    '                                , ByVal cmdType As CommandType _
    '                                , ByVal arrParamIn As IDataParameter() _
    '                                , ByVal transaction As IDbTransaction _
    '    ) As ArrayList
    '    Dim oCommand As OracleCommand = Nothing
    '    Dim rows As Integer
    '    Dim outList As New ArrayList
    '    Dim leaveOpen As Boolean = False
    '    Try
    '        Dim p As IDataParameter
    '        oCommand = GetCommand()
    '        oCommand.CommandText = Statement
    '        oCommand.CommandType = cmdType
    '        oCommand.Connection = transaction.Connection
    '        oCommand.Transaction = transaction
    '        Dim cmd As OracleCommand = con.CreateCommand()
    '        cmd.CommandText = "test_pkg.my_func"
    '        cmd.CommandType = CommandType.StoredProcedure

    '        Dim parm As OracleParameter

    '        parm = New OracleParameter()
    '        parm.Direction = ParameterDirection.ReturnValue
    '        parm.OracleDbType = OracleDbType.Varchar2
    '        parm.Size = 5000
    '        cmd.Parameters.Add(parm)

    '        parm = New OracleParameter()
    '        parm.Direction = ParameterDirection.Input
    '        parm.Value = "abc"
    '        parm.OracleDbType = OracleDbType.Varchar2
    '        cmd.Parameters.Add(parm)

    '        parm = New OracleParameter()
    '        parm.Direction = ParameterDirection.Input
    '        parm.Value = 42
    '        parm.OracleDbType = OracleDbType.Int32
    '        cmd.Parameters.Add(parm)

    '        cmd.ExecuteNonQuery()
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        If Not leaveOpen Then oCommand.Connection.Close()
    '    End Try

    'End Function
    ' Thực hiện một Command trong Oracle
    Public Shared Function ExecuteNonQuery(ByVal Statement As String _
                                            , ByVal cmdType As CommandType) As Integer
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Try
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = Statement
            oCommand.CommandType = cmdType
            oConnect.Open()
            Return oCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            If Not oCommand Is Nothing Then
                oCommand.Dispose()
            End If
            If Not oConnect Is Nothing Then
                oConnect.Close()
                oConnect.Dispose()
            End If
        End Try
    End Function
    ' Thực hiện một Command trong Oracle
    Public Shared Function ExecuteNonQuery(ByVal Statement As String _
                                            , ByVal cmdType As CommandType _
                                            , ByVal transaction As IDbTransaction) As Integer
        Dim leaveOpen As Boolean = False
        Dim oCommand As OracleCommand = Nothing
        Try
            oCommand = GetCommand()
            oCommand.CommandText = Statement
            oCommand.CommandType = cmdType
            oCommand.Connection = transaction.Connection
            oCommand.Transaction = transaction
            If oCommand.Connection.State = ConnectionState.Closed Then
                oCommand.Connection.Open()
            Else
                leaveOpen = True
            End If
            Return oCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            If Not leaveOpen Then oCommand.Connection.Close()
        End Try
    End Function
#End Region

#Region "ExecuteDataReader"
    Public Shared Function ExecuteDataReader(ByVal Statement As String, ByVal arrParamIn As IDataParameter()) As String
        Dim sRet As String = String.Empty
        Dim oRet As IDataReader = Nothing
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Try
            Dim p As IDataParameter
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            oCommand.CommandText = Statement
            oCommand.CommandType = CommandType.Text
            oConnect.Open()
            If arrParamIn IsNot Nothing AndAlso arrParamIn.Length > 0 Then
                For Each p In arrParamIn
                    oCommand.Parameters.Add(p)
                Next
            End If
            oCommand.Connection = oConnect
            oRet = oCommand.ExecuteReader(CommandBehavior.CloseConnection)
            If oRet IsNot Nothing Then
                While oRet.Read
                    sRet = oRet.GetValue(0)
                End While
            End If

            oCommand.Parameters.Clear()
        Catch ex As Exception
            Throw ex
        Finally
            If Not oCommand Is Nothing Then oCommand.Dispose()
            If Not oConnect Is Nothing Then oConnect.Dispose()
        End Try

        Return sRet
    End Function

    'ham nay select data ra mot dataReader dung mot commandtext tu dinh nghia
    Public Shared Function ExecuteDataReader(ByVal Statement As String, ByVal cmdType As CommandType) As OracleDataReader
        Dim oCommand As OracleCommand = Nothing
        Dim oConnect As OracleConnection = Nothing
        Try

            oConnect = New OracleConnection(strConnection)
            oCommand = GetCommand()
            oCommand.CommandText = Statement
            oCommand.CommandType = cmdType
            oCommand.Connection = oConnect
            If oCommand.Connection.State = ConnectionState.Closed Then
                oCommand.Connection.Open()
            End If
            Return oCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
            'oCommand.Connection.Close()
            'Throw ex
        Finally
            'If Not oCommand.Connection Is Nothing Then
            '    oCommand.Connection.Close()
            '    oCommand.Connection.Dispose()
            'End If
        End Try
    End Function



    'ham nay select data ra mot dataReader 
    Public Shared Function ExecuteDataReader(ByVal FieldNames As String(), ByVal strTable As String, _
                    ByVal strCriterias As String, ByVal strProperty As String) As OracleDataReader

        Dim strCommandText As String
        'dim string
        Dim str0 As String ' = Microsoft.VisualBasic.IIf(IsNullOrEmpty(FieldNames), " * ", String.Join(", ", FieldNames))
        If IsNullOrEmpty(FieldNames) = True Then
            str0 = " * "
        Else
            str0 = String.Join(", ", FieldNames)
        End If
        Dim str1 As String = strTable
        Dim str2 As String = IfNotNullOrEmpty(strCriterias, "WHERE " & strCriterias)
        Dim str3 As String = IfNotNullOrEmpty(strProperty, "ORDER BY " & strProperty)
        strCommandText = String.Format("SELECT {0} FROM {1} {2} {3}", str0, str1, str2, str3)
        Return ExecuteDataReader(strCommandText, CommandType.Text)
    End Function
#End Region

    Public Function CheckConnect() As Boolean
        Dim oConnect As OracleConnection = Nothing
        Try
            oConnect = New OracleConnection(strConnection)
            If oConnect Is Nothing OrElse oConnect.State <> ConnectionState.Open Then
                oConnect.Open()
            End If
            Return True
        Catch ex As Exception
            Return False
        Finally
            If Not oConnect Is Nothing Then
                oConnect.Dispose()
            End If
        End Try
    End Function

    Public Shared Function ExecuteSQLScalar(ByVal sqlCommand As String) As String
        Dim v_cmd As New OracleCommand
        Dim oConnect As OracleConnection = Nothing

        Try
            oConnect = New OracleConnection(strConnection)
            v_cmd.CommandType = CommandType.Text
            v_cmd.CommandText = sqlCommand
            v_cmd.Connection = oConnect
            If v_cmd.Connection.State = ConnectionState.Closed Then
                v_cmd.Connection.Open()
            End If
            Dim v_obj As Object = v_cmd.ExecuteScalar()
            If Not v_obj Is Nothing Then
                Return v_obj.ToString
            Else
                Return ""
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not v_cmd.Connection Is Nothing Then
                v_cmd.Connection.Close()
            End If
            If Not oConnect Is Nothing Then oConnect.Dispose()
        End Try
    End Function


#Region "ExecuteDataSet"
    Public Shared Function NewDBParameter(ByVal paramName As String, ByVal paramDirection As ParameterDirection _
                , ByVal paramValue As Object, ByVal paramtype As DbType) As IDbDataParameter
        Dim param As IDbDataParameter

        param = New OracleParameter(paramName, paramtype)
        If paramDirection = ParameterDirection.Input Or paramDirection = ParameterDirection.InputOutput Then
            If paramValue Is Nothing Then
                param.Value = DBNull.Value
            Else
                param.Value = paramValue
            End If
        Else
            param.Value = Nothing
        End If
        param.Direction = paramDirection

        Return param
    End Function

    Public Shared Function ExecuteReturnDataSet(ByVal sSQL As String, ByVal arrParamIn As IDataParameter()) As DataSet
        Dim ds As DataSet = Nothing
        Try
            ds = ExecuteReturnDataSet(sSQL, CommandType.Text, arrParamIn)
        Catch ex As Exception
            ds = Nothing
        End Try

        Return ds
    End Function

    Public Shared Function ExecuteReturnDataSet(ByVal Statement As String _
                                          , ByVal cmdType As CommandType _
                                          , ByVal arrParamIn As IDataParameter() _
      ) As DataSet
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Dim outList As New ArrayList

        Try
            Dim ds As New DataSet
            Dim da As OracleDataAdapter

            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = Statement
            oCommand.CommandType = cmdType

            If arrParamIn IsNot Nothing AndAlso arrParamIn.Length > 0 Then
                For Each p In arrParamIn
                    oCommand.Parameters.Add(p)
                Next
            End If

            ' Get DataAdapter
            da = New OracleDataAdapter()
            oCommand.Connection.Open()
            da.SelectCommand = oCommand
            da.Fill(ds)
            oCommand.Parameters.Clear()
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            If Not oCommand Is Nothing Then oCommand.Dispose()
            If Not oConnect Is Nothing Then oConnect.Dispose()
        End Try
    End Function
    'ham nay select data ra mot dataset
    Public Shared Function ExecuteReturnDataSet(ByVal strCommandText As String _
                                                , ByVal strTable As String) As DataSet
        Dim oConnect As OracleConnection = Nothing
        Dim oDA As OracleDataAdapter = Nothing
        Dim ds As DataSet = Nothing
        Try
            oConnect = New OracleConnection(strConnection)
            oDA = New OracleDataAdapter(strCommandText, oConnect)
            ds = New DataSet
            oDA.Fill(ds, strTable)

        Catch ex As Exception
            'thong bao loi ra o day
        Finally
            If Not oDA Is Nothing Then oDA.Dispose()
            If Not oConnect Is Nothing Then oConnect.Dispose()
        End Try
        Return ds
    End Function

    'ham nay select data ra mot dataset
    Public Shared Function ExecuteReturnDataSet(ByVal strCommandText As String _
                                                , ByVal cmdType As CommandType) As DataSet
        Dim oConnect As OracleConnection = Nothing
        Dim oDA As OracleDataAdapter = Nothing
        Dim ds As DataSet = Nothing
        Try
            'LogDebug.WriteLog("ExecuteReturnDataSet: " & strConnection, EventLogEntryType.Error)
            oConnect = New OracleConnection(strConnection)
            oDA = New OracleDataAdapter(strCommandText, oConnect)
            ds = New DataSet
            oDA.Fill(ds)

        Catch ex As Exception
            'thong bao loi ra o day
            log.Error(ex.Message & "-" & ex.StackTrace)
            'LogDebug.WriteLog("ExecuteReturnDataSet: " & ex.Message, EventLogEntryType.Error)
        Finally
            If Not oDA Is Nothing Then oDA.Dispose()
            If Not oConnect Is Nothing Then oConnect.Dispose()
        End Try
        Return ds
    End Function
    'select data ra mot bang dung cau truy van truc tiep
    Public Shared Function ExecuteToTable(ByVal strCommandText As String) As DataTable
        Dim oConnect As OracleConnection = Nothing
        Dim oDA As OracleDataAdapter = Nothing
        Dim dt As New DataTable
        Try
            oConnect = New OracleConnection(strConnection)
            oDA = New OracleDataAdapter(strCommandText, oConnect)
            oDA.Fill(dt)
        Catch ex As Exception
            Throw ex
        Finally
            If Not oDA Is Nothing Then
                oDA.Dispose()
            End If
            If Not oConnect Is Nothing Then
                oConnect.Close()
                oConnect.Dispose()
            End If
        End Try
        Return dt
    End Function

    'ham nay select data dung procedure va tra ve mot dataTable
    Public Shared Function ExecuteToTable(ByVal oracleProceduce As String, ByVal strCursorOut As String) As DataTable
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Dim oDa As OracleDataAdapter = Nothing

        Try
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = oracleProceduce
            oCommand.CommandType = CommandType.StoredProcedure

            oCommand.Parameters.Add(strCursorOut, OracleType.Cursor).Direction = ParameterDirection.Output
            oConnect.Open()
            oDa = New OracleDataAdapter(oCommand)
            Dim dt As DataTable = New DataTable
            oDa.Fill(dt)
            Return dt
        Catch ex As Exception
            Throw ex
        Finally
            If Not oDa Is Nothing Then
                oDa.Dispose()
            End If
            If Not oCommand Is Nothing Then
                oCommand.Dispose()
            End If
            If Not oConnect Is Nothing Then
                oConnect.Close()
                oConnect.Dispose()
            End If
        End Try
    End Function

    Public Shared Function ExecuteReturnDataTable(ByVal Statement As String, ByVal cmdType As CommandType, ByVal arrParamIn As IDataParameter()) As DataTable
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Dim outList As New ArrayList

        Try
            Dim dt As New DataTable
            Dim da As OracleDataAdapter

            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = Statement
            oCommand.CommandType = cmdType

            If arrParamIn IsNot Nothing AndAlso arrParamIn.Length > 0 Then
                For Each p In arrParamIn
                    oCommand.Parameters.Add(p)
                Next
            End If

            ' Get DataAdapter
            da = New OracleDataAdapter()
            oCommand.Connection.Open()
            da.SelectCommand = oCommand
            da.Fill(dt)
            oCommand.Parameters.Clear()
            Return dt

        Catch ex As Exception
            Throw ex
        Finally
            If Not oCommand Is Nothing Then oCommand.Dispose()
            If Not oConnect Is Nothing Then oConnect.Dispose()
        End Try
    End Function
#End Region

#Region "Transaction"
    Private _Conn As OracleConnection = Nothing

    Public Function BeginTransaction() As IDbTransaction

        _Conn = New OracleConnection(strConnection)
        If _Conn.State = ConnectionState.Closed Then
            _Conn.Open()
        End If
        Return _Conn.BeginTransaction()
    End Function
#End Region

#Region "Dispose"
    Public Overloads Sub Dispose()
        Try
            If Not _Conn Is Nothing Then
                _Conn.Close()
                _Conn.Dispose()
            End If
            If Not _Conn Is Nothing Then
                If _Conn.State = ConnectionState.Open Then
                    ' Close the connection
                    _Conn.Close()
                End If
            End If
            If Not _Conn Is Nothing Then
                _Conn.Dispose()
            End If
            'If Not _command Is Nothing Then
            '    _command.Dispose()
            'End If

            '_connection = Nothing
            '_command = Nothing
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Public Shared Function ExecuteToTable(ByVal cmdType As CommandType, ByVal oCmd As OracleCommand) As DataTable
        Dim oConnect As OracleConnection = Nothing
        Dim oDa As OracleDataAdapter = Nothing
        Dim dt As DataTable = New DataTable
        Try
            oConnect = New OracleConnection(strConnection)
            oCmd.Connection = oConnect
            oConnect.Open()
            oDa = New OracleDataAdapter(oCmd)
            oDa.Fill(dt)
            oConnect.Close()
        Catch ex As Exception

        Finally
            If Not oDa Is Nothing Then
                oDa.Dispose()
            End If
            If Not oCmd Is Nothing Then
                oCmd.Dispose()
            End If
            If Not oConnect Is Nothing Then
                oConnect.Close()
                oConnect.Dispose()
            End If
        End Try
        Return dt
    End Function

    'HOAPT
    Public Function GetParameter(ByVal paramName As String, ByVal paramDirection As ParameterDirection _
                , ByVal paramValue As Object, ByVal paramtype As DbType) As IDbDataParameter
        Dim param As IDbDataParameter

        param = New OracleParameter(paramName, paramtype)
        If paramDirection = ParameterDirection.Input Or paramDirection = ParameterDirection.InputOutput Then
            param.Value = paramValue
        Else
            param.Value = Nothing
        End If
        param.Direction = paramDirection

        Return param
    End Function
#Region "Other"
    ' ham nay insert data vao mot bang dung truc tiep truy van oracle
    Public Shared Function InsertTable(ByVal FieldNames As String(), ByVal FieldValues As String(), ByVal strTable As String) As Integer
        Dim strCommandText As String = String.Format("INSERT INTO {2} ({0}) VALUES ({1})", _
        String.Join(", ", FieldNames), String.Join(", ", FieldValues), strTable)
        Return ExecuteNonQuery(strCommandText, CommandType.Text)
    End Function

    ' ham nay insert data vao mot bang dung proceduce va cac parameter
    Public Shared Function InsertTable(ByVal strProceduceOracle As String, ByVal FieldNames As String(), ByVal ListParameter As Object()) As Integer
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Try
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            'setup cac thuoc tinh cua doi tuong oCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = strProceduceOracle
            oCommand.CommandType = CommandType.StoredProcedure
            'them vao cac parameter
            oCommand = AddParameter(oCommand, FieldNames, ListParameter)
            oConnect.Open()
            Return oCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            oCommand.Dispose()
            oConnect.Close()
            oConnect.Dispose()
        End Try
    End Function

    ' ham nay de them vao cac tham doi cho doi tuong OracleCommand
    Public Shared Function AddParameter(ByVal oCommand As OracleCommand, ByVal FieldParameter As String(), ByVal FieldValues As Object()) As OracleCommand
        Dim iCount As Integer = FieldParameter.Length
        Dim i As Integer = 0
        While i < iCount
            oCommand.Parameters.Add(FieldParameter(i), FieldValues(i))
            i = i + 1
        End While
        Return oCommand
    End Function

    'ham cap nhat data dung cau lenh truy van truc tiep
    Public Shared Function Update(ByVal FieldNames As String(), ByVal strTable As String, ByVal strCriterias As String) As Integer
        Dim strCommandText As String = String.Format("UPDATE {1} SET {0} {2}", _
        String.Join(", ", FieldNames), _
        strTable, _
        IfNotNullOrEmpty(strCriterias, "WHERE " & strCriterias))
        Return ExecuteNonQuery(strCommandText, CommandType.Text)
    End Function

    ' ham cap nhat tu cau lenh truy van truc tiep
    Public Shared Function Update(ByVal FieldNames As String(), ByVal FieldValues As String(), ByVal strTable As String, ByVal strCriterias As String) As Integer
        Dim Field(FieldNames.Length - 1) As String
        Dim i As Integer = 0
        While i < FieldNames.Length
            Field(i) += FieldNames(i) & "=" & FieldValues(i)
            i += 1
        End While
        Return Update(Field, strTable, strCriterias)
    End Function

    'ham cap nhat su dung procedure
    Public Shared Function Update(ByVal strProcedureOracle As String, ByVal FieldParameters As String(), _
    ByVal FieldValues As Object()) As Integer
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Try
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            ' thiet lap cac thuoc tinh cua doi tuong oCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = strProcedureOracle
            oCommand.CommandType = CommandType.StoredProcedure
            'them vao cac parameter
            oCommand = AddParameter(oCommand, FieldParameters, FieldValues)
            oConnect.Open()
            Return oCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            If Not oCommand Is Nothing Then oCommand.Dispose()
            If Not oConnect Is Nothing Then
                oConnect.Close()
                oConnect.Dispose()
            End If
        End Try
    End Function

    'ham nay xoa data voi dieu kien di kem nao do
    Public Shared Function Delete(ByVal strTable As String, ByVal strCriterias As String) As Integer
        Dim strCommandText As String = String.Format("DELETE {0} {1}", _
                    strTable, IfNotNullOrEmpty(strCriterias, "WHERE " & strCriterias))
        Return ExecuteNonQuery(strCommandText, CommandType.Text)
    End Function

    'ham nay xoa mot bang trong co so du lieu
    Public Shared Function DropTable(ByVal strTableName As String) As Integer
        Dim strCommandText As String = String.Format("DROP TABLE {0}", strTableName)
        Return ExecuteNonQuery(strCommandText, CommandType.Text)
    End Function


    Public Shared Function ConvertToSql(ByVal vSql As String, ByVal vList As List(Of IDataParameter)) As String
        Dim v_return As String = vSql
        Try
            v_return = v_return.Replace(",", " , ")
            v_return = v_return.Replace(")", " ) ")
            For Each p As IDbDataParameter In vList
                Dim v_pName As String = p.ParameterName & " "
                Dim v_pValue As String = IIf(p.Value.Equals(DBNull.Value), "null", p.Value)
                Dim v_pType As String = p.SourceColumn

                v_pValue = "'" & v_pValue & "'"

                v_return = v_return.Replace(v_pName, v_pValue)
            Next
        Catch ex As Exception

        End Try
        Return v_return
    End Function

#End Region
End Class

