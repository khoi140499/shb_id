﻿Imports System.Security.Cryptography
Imports System.IO
Imports System.Text

Public Class Security
    Private Shared Key() As Byte = {26, 15, 5, 46, 98, 21, 2, 36, 19, 4, 22, 50, 42, 14, 24, 18, 34, 16, 82, 76, 65, 91, 54, 78}
    Private Shared InitializationVector() As Byte = {65, 110, 68, 26, 69, 178, 200, 219}
    Const sPrivateKey_1 As String = "<RSAKeyValue><Modulus>kCA8nVCxo/kPYdPY4g4cS2ClrJSoFolmXG+aurkrFG8qZSI99ZFARCR6sg0ALmFA7vQGNzi4LplQtvZOUcAXzgBxAepnrOuQfgFKQcOpmLrwymJM7k079kIEQmkdptmoyKJPx2jNEFbkIvz5orTlHGJunValE19+Iq0FXLJ213c=</Modulus><Exponent>AQAB</Exponent><P>yxhEOytNv/jUM4RM0UEOrFt2n5zrmFUaSkmb+1DD84x1Yt6dCK62FTmQ9t88smIrMIZFzfa+lyqJ4YrYfsPh5w==</P><Q>tauHFE2+OR8aKuDbgEiTe5U5pwbOi+9hlVzZR1nNEe6Vdhsn+6sfmmw+mBEq2zmF7ks92VcAeucJLmxTtQvL8Q==</Q><DP>YF4WWiEVFmdCgWNgc9Yz2TLZGamC3Nhczi0ZV2meYR0fwhcxWiJUgcO79ng8u8P7DF5aFv3it6XWoO+aBKR7tw==</DP><DQ>AButBecR8OjC1Dm0B7yDUfSBx0aqwsyOq08V1gQIqjURI7Pabzzn1OixlNeVxfGf0cd7rvBtUKVojP2FgzsRkQ==</DQ><InverseQ>VPEYTJrYq322IpHSrbewewv9Wrl2cESl27VJuQ6rgMFN+fTbPTK+OHa4lY5MeUvge83sfFTUD3+1VYYbdebJWg==</InverseQ><D>R0vrMFdOyTLX7SkDTTB1xsQUsP8I970Yqfl7rYsUSJOFgWojBGvKQutY1uOfh1IM8Sg+OusQEd0+FIRb0g0YuM0gPrb9/Rh1k3idpKzUKwIlNsUgOvFZG221wVqX6+B+kDgdGCng5s79AgCZCG4y36EfAim/JbD3V0P2VP8SaoE=</D></RSAKeyValue>"
    Const sPublicKey_1 As String = "<RSAKeyValue><Modulus>kCA8nVCxo/kPYdPY4g4cS2ClrJSoFolmXG+aurkrFG8qZSI99ZFARCR6sg0ALmFA7vQGNzi4LplQtvZOUcAXzgBxAepnrOuQfgFKQcOpmLrwymJM7k079kIEQmkdptmoyKJPx2jNEFbkIvz5orTlHGJunValE19+Iq0FXLJ213c=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>"
    Const sPrivateKey_2 As String = "<RSAKeyValue><Modulus>n963wL5CFia7V23WvUGrVuzyP1DhYhstdsLDpsomaI0+9qv38DrY9S4b46mtLm8M6LYBLcLNnvFHAgFCPYkRTnL+XX+4HrD62I+Pp/e1w6+whhD6ZcynvplzybDx53HYkGWXT48+imM2wFq+0vzYn7pmYxUDwnhTTtBEH6CDOcE=</Modulus><Exponent>AQAB</Exponent><P>1+Iq9rrkAa+Ib/wXk0MhGZj+9FdaCxIrnidAKAW35SV5R0W3s5TJJwpiGkEdvHBwguesGmgw8P46+/CSR9SPkQ==</P><Q>vZPsLF8DKlKi24lWBMzvuF/qqn7vDI/ZlEEq8UOu7+eu2hAl6ZxOpxQSJ/LaLHbFR7kcUwkWBXUWkSNt6SZPMQ==</Q><DP>DEiZKzrNGG91cvHEIaPzMTKsZxjFTdX112TiWKSipw4vzWdhIX24yFjl5hqIZ516OWiXOVVuW3gLwSQaHq4wkQ==</DP><DQ>dKXBCyhHJVKFfLnLYNbMpdD27oN+CWe1xEA48QzpZhpTfz+yZETWLH3wEf6e+f72soQXBoyvrRn3kcSQC4aT8Q==</DQ><InverseQ>CU1odTmNE61kS3nTBHMXymflC9PXHqWTJ667DeGm0nc7kA803YstQoTFC/i8xP5ydEaRYjgWlZldpw8Qm0Iz7Q==</InverseQ><D>VpLja0scuqYq2YOvwDlpsd8KqGMCSoBxNFjwXsgl2IR0zlv+HmefydY4YCcok3arxPGa3c8aTrJWQ62u02gAqGFnRMMvIt2tVRb9evME5kHPBreA+tLE0MwOiiESrOM7+7DKyAvOYHiA+eRofY7lMvsXRVLtrlh8lcfmrX9WlgE=</D></RSAKeyValue>"
    Const sPublicKey_2 As String = "<RSAKeyValue><Modulus>n963wL5CFia7V23WvUGrVuzyP1DhYhstdsLDpsomaI0+9qv38DrY9S4b46mtLm8M6LYBLcLNnvFHAgFCPYkRTnL+XX+4HrD62I+Pp/e1w6+whhD6ZcynvplzybDx53HYkGWXT48+imM2wFq+0vzYn7pmYxUDwnhTTtBEH6CDOcE=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>"
    Public Shared Function fnEncypt_RSA(ByVal sClearData As String, ByRef sData As String, ByRef sSign As String) As Boolean
        Dim result As Boolean = False
        sData = String.Empty
        sSign = String.Empty
        Try
            Dim byte_KeyDesc As Byte()
            Dim unc As New UTF8Encoding()
            byte_KeyDesc = unc.GetBytes(sClearData)
            Dim clsBank As New RSA2OJB.Encryption.Bank()
            Dim clsCustomer As New RSA2OJB.Encryption.Customer()

            clsCustomer.SetPrivateKey(sPrivateKey_1)
            clsBank.SetPublicKey(sPrivateKey_2)

            Dim byte_keyDesc_encryptArr As Byte() = clsCustomer.EncryptData(clsBank.get_PublicParameters(), byte_KeyDesc)
            sData = Convert.ToBase64String(byte_keyDesc_encryptArr)

            Dim byte_keyDesc_signArr As Byte() = clsCustomer.HashAndSign(byte_keyDesc_encryptArr)
            sSign = Convert.ToBase64String(byte_keyDesc_signArr)
            result = True
        Catch ex As Exception
            Throw ex
        End Try
        Return result
    End Function
    Public Shared Function fnDeCrypt_RSA(ByVal sData As String, ByVal sSign As String, ByRef clearData As String) As Boolean
        Dim result As Boolean = False
        Try
            Dim encrypted As Byte() = Convert.FromBase64String(sData)
            Dim signature As Byte() = Convert.FromBase64String(sSign)

            Dim clsBank As New RSABANK.Encryption.Bank()
            Dim clsCustomer As New RSABANK.Encryption.Customer()

            clsBank.SetPrivateKey(sPrivateKey_2)
            clsCustomer.SetPublicKey(sPublicKey_1)

            If clsBank.VerifyHash(clsCustomer.get_PublicParameters(), encrypted, signature) Then
                clearData = clsBank.DecryptData(encrypted)
                If clearData <> String.Empty Then
                    result = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return result
    End Function
    Public Shared Function fnSignClearText(ByVal sClearData As String, ByRef sSign As String) As Boolean
        Dim result As Boolean = False
        sSign = String.Empty
        Try
            Dim clsBank As New RSA2OJB.Encryption.Bank()
            Dim clsCustomer As New RSA2OJB.Encryption.Customer()

            clsCustomer.SetPrivateKey(sPrivateKey_1)
            clsBank.SetPublicKey(sPrivateKey_2)

            Dim utfEn As New UTF8Encoding()
            Dim byte_keyDesc_encryptArr As Byte() = utfEn.GetBytes(sClearData)

            Dim byte_keyDesc_signArr As Byte() = clsCustomer.HashAndSign(byte_keyDesc_encryptArr)
            sSign = Convert.ToBase64String(byte_keyDesc_signArr)
            result = True
        Catch ex As Exception
            result = False
        End Try
        Return result
    End Function
    Public Shared Function fnVerify(ByVal sData As String, ByVal sSign As String) As Boolean
        Dim result As Boolean ' = False
        Try
            Dim utfEn As New UTF8Encoding()
            Dim encrypted As Byte() = utfEn.GetBytes(sData)
            Dim signature As Byte() = Convert.FromBase64String(sSign)


            Dim clsBank As New RSABANK.Encryption.Bank()
            Dim clsCustomer As New RSABANK.Encryption.Customer()

            clsBank.SetPrivateKey(sPrivateKey_2)
            clsCustomer.SetPublicKey(sPublicKey_1)

            If clsBank.VerifyHash(clsCustomer.get_PublicParameters(), encrypted, signature) Then
                result = True
            End If
        Catch ex As Exception
            result = False
        End Try
        Return result
    End Function
    Public Shared Function EncryptStr(ByVal strText As String) As String
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim strEncrKey As String
        Try
            strEncrKey = "&%#@?,:*"
            Dim bykey() As Byte = System.Text.Encoding.UTF8.GetBytes(Left(strEncrKey, 8))
            Dim InputByteArray() As Byte = System.Text.Encoding.UTF8.GetBytes(strText)
            Dim des As New DESCryptoServiceProvider
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(bykey, IV), CryptoStreamMode.Write)
            cs.Write(InputByteArray, 0, InputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function DescryptStr(ByVal strText As String) As String
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim inputByteArray(strText.Length) As Byte
        Dim sDecrKey As String
        Try
            sDecrKey = "&%#@?,:*"
            Dim byKey() As Byte = System.Text.Encoding.UTF8.GetBytes(Left(sDecrKey, 8))
            Dim des As New DESCryptoServiceProvider
            inputByteArray = Convert.FromBase64String(strText)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Public Shared Function EncryptStr(ByVal strText As String, ByVal strKey As String, ByVal intType As Integer) As String
        Dim IV() As Byte = {&H87, &H88, &H91, &H12, &H36, &H49, &HAB, &HEF}
        Dim strEncrKey As String
        Try
            'Mac dinh 1: Database, 2 OSB, 3 PFX, 4 CITAD
            strEncrKey = "Se@" & intType & strKey
            Dim bykey() As Byte = System.Text.Encoding.UTF8.GetBytes(Left(strEncrKey, 8))
            Dim InputByteArray() As Byte = System.Text.Encoding.UTF8.GetBytes(strText)
            Dim des As New DESCryptoServiceProvider
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(bykey, IV), CryptoStreamMode.Write)
            cs.Write(InputByteArray, 0, InputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function DescryptStr(ByVal strText As String, ByVal strKey As String, ByVal intType As Integer) As String
        Dim IV() As Byte = {&H87, &H88, &H91, &H12, &H36, &H49, &HAB, &HEF}
        Dim inputByteArray(strText.Length) As Byte
        Dim sDecrKey As String
        Try
            'Mac dinh 1: Database, 2 OSB, 3 PFX, 4 CITAD
            sDecrKey = "Se@" & intType & strKey
            Dim byKey() As Byte = System.Text.Encoding.UTF8.GetBytes(Left(sDecrKey, 8))
            Dim des As New DESCryptoServiceProvider
            inputByteArray = Convert.FromBase64String(strText)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Shared Function Encrypt3Des(ByVal Text As String) As String

        ' Tạo mã hóa UTF8 để chuyển chuỗi thành mảng byte
        Dim UTF8Encoder As System.Text.UTF8Encoding = New System.Text.UTF8Encoding()
        Dim TextBytes() As Byte = UTF8Encoder.GetBytes(Text)


        ' Dim TextBytes() As Byte =  ' HexStringToBytes(Text)

        'Khởi tạo TripleDes service provider
        Dim TDESProvider As System.Security.Cryptography.TripleDESCryptoServiceProvider = New System.Security.Cryptography.TripleDESCryptoServiceProvider
        Dim CryptoTransform As System.Security.Cryptography.ICryptoTransform = TDESProvider.CreateEncryptor(Key, InitializationVector)

        'Khởi tạo bộ nhớ đệm để lưu dữ liệu mã hóa
        Dim EncryptionStream As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim CryptoStream As System.Security.Cryptography.CryptoStream = New System.Security.Cryptography.CryptoStream(EncryptionStream, CryptoTransform, System.Security.Cryptography.CryptoStreamMode.Write)

        'Viết các thông tin mã hóa cho các dòng
        CryptoStream.Write(TextBytes, 0, TextBytes.Length)
        CryptoStream.FlushFinalBlock()
        EncryptionStream.Position = 0

        'Đọc vào một mảng Byte và trả về
        Dim Result(EncryptionStream.Length - 1) As Byte
        EncryptionStream.Read(Result, 0, EncryptionStream.Length)
        CryptoStream.Close()
        'Return UTF8Encoder.GetString(Result)
        Return GetHexString(Result)

    End Function
    Public Shared Function Decrypt3Des(ByVal strTextBytes As String) As String
        Dim UTF8Encoder As System.Text.UTF8Encoding = New System.Text.UTF8Encoding()
        ' Dim TextBytes() As Byte = UTF8Encoder.GetBytes(strTextBytes)
        Dim TextBytes() As Byte = HexStringToBytes(strTextBytes)
        Dim TDESProvider As System.Security.Cryptography.TripleDESCryptoServiceProvider = New System.Security.Cryptography.TripleDESCryptoServiceProvider
        Dim CryptoTransform As System.Security.Cryptography.ICryptoTransform = TDESProvider.CreateDecryptor(Key, InitializationVector)

        Dim DecryptionStream As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim CryptoStream As System.Security.Cryptography.CryptoStream = New System.Security.Cryptography.CryptoStream(DecryptionStream, CryptoTransform, System.Security.Cryptography.CryptoStreamMode.Write)

        CryptoStream.Write(TextBytes, 0, TextBytes.Length)
        CryptoStream.FlushFinalBlock()
        DecryptionStream.Position = 0

        Dim result(DecryptionStream.Length - 1) As Byte
        DecryptionStream.Read(result, 0, DecryptionStream.Length)
        CryptoStream.Close()


        Return UTF8Encoder.GetString(result)
        'Return GetHexString(result)

    End Function
    Private Shared Function GetHexString(ByVal bytes() As Byte, Optional ByVal len As Integer = -1, Optional ByVal spaces As Boolean = False) As String
        If len = -1 Then len = bytes.Length
        Dim i As Integer
        Dim s As String = ""
        For i = 0 To len - 1
            s += bytes(i).ToString("x2")
            If spaces Then s += " "
        Next
        If spaces Then s = s.TrimEnd()
        Return s
    End Function

    Private Shared Function HexStringToBytes(ByVal hexstring As String) As Byte()
        Dim out((hexstring.Length / 2) - 1) As Byte
        For i = 0 To (hexstring.Length / 2) - 1
            out(i) = Convert.ToByte(hexstring.Substring(i * 2, 2), 16)
        Next
        Return out
    End Function
    Public Shared Function Base64EncodeSHA256(ByVal plainText As String) As String
        Try
            If plainText.Length > 0 Then
                Dim mySHA256 As SHA256 = SHA256Managed.Create()

                Dim plainTextBytes As Byte() = mySHA256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(plainText))
                'Return System.Convert.ToBase64String(plainTextBytes)

                'Dim strTemp As New StringBuilder(plainTextBytes.Length * 2)
                'For Each b As Byte In plainTextBytes
                '    strTemp.Append(Conversion.Hex(b))
                'Next
                'Return strTemp.ToString()

                Dim Result As New System.Text.StringBuilder(plainTextBytes.Length * 2)
                Dim Part As String
                For Each b As Byte In plainTextBytes
                    Part = Conversion.Hex(b)
                    If Part.Length = 1 Then Part = "0" & Part
                    Result.Append(Part)
                Next
                Return Result.ToString()
            End If
        Catch ex As Exception
        End Try
        Return ""
    End Function
End Class
