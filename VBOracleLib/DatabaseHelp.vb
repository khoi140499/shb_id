'----------------------------------------
'created by: Hoang Van Anh
'Date: 24/03/2009
'descript: access to oracle database
'----------------------------------------

Imports System.Configuration.ConfigurationSettings
Imports System.Data.OracleClient
Imports VBOracleLib.Globals


Public Class DatabaseHelp

    Public Shared strConnection As String = Configuration.ConfigurationManager.AppSettings("CTC_DB_CONN").ToString
    'Public Shared strConnection As String = VBOracleLib.Security.DescryptStr(strConnection1)

    Public Shared Function Execute(ByVal oCommand As OracleCommand) As Integer
        Return oCommand.ExecuteNonQuery()
    End Function


    ' Thực hiện một Command trong Oracle
    Public Shared Function Execute(ByVal strCommandText As String) As Integer
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Dim i As Integer = 0
        Try
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = strCommandText
            oCommand.CommandType = CommandType.Text
            oConnect.Open()
            i = oCommand.ExecuteNonQuery()
        Catch ex As Exception
        Finally
            If Not oCommand Is Nothing Then
                oCommand.Dispose()
            End If
            If Not oConnect Is Nothing Then
                oConnect.Close()
                oConnect.Dispose()
            End If
        End Try
        Return i
    End Function

    'ham nay select data ra mot dataReader dung mot commandtext tu dinh nghia
    Public Shared Function ExecuteToReader(ByVal strCommandText As String) As OracleDataReader
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Dim odReader As OracleDataReader = Nothing
        Try
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand(strCommandText, oConnect)
            oConnect.Open()
            odReader = oCommand.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
            'loi khai bao
            oConnect.Close()
            oConnect.Dispose()
        Finally
            oCommand.Dispose()
            '--------------------
            'Trong oracleDataReader ko duoc ngat ket noi khi dang doc. them 2 dong sau la sai
            ' oConnect.Close()
            'oConnect.Dispose()
            '--------------------
        End Try
        Return odReader
    End Function

    'ham nay select data ra mot dataReader 
    Public Shared Function ExecuteToReader(ByVal FieldNames As String(), ByVal strTable As String, _
                    ByVal strCriterias As String, ByVal strProperty As String) As OracleDataReader

        Dim strCommandText As String
        'dim string
        Dim str0 As String ' = Microsoft.VisualBasic.IIf(IsNullOrEmpty(FieldNames), " * ", String.Join(", ", FieldNames))
        If IsNullOrEmpty(FieldNames) = True Then
            str0 = " * "
        Else
            str0 = String.Join(", ", FieldNames)
        End If
        Dim str1 As String = strTable
        Dim str2 As String = IfNotNullOrEmpty(strCriterias, "WHERE " & strCriterias)
        Dim str3 As String = IfNotNullOrEmpty(strProperty, "ORDER BY " & strProperty)
        strCommandText = String.Format("SELECT {0} FROM {1} {2} {3}", str0, str1, str2, str3)
        Return ExecuteToReader(strCommandText)
    End Function

    'ham nay select data ra mot dataset
    Public Shared Function ExecuteToDataSet(ByVal strCommandText As String, ByVal strTable As String) As DataSet
        Dim oConnect As OracleConnection = Nothing
        Dim oDA As OracleDataAdapter = Nothing
        Dim ds As DataSet = Nothing
        Try
            oConnect = New OracleConnection(strConnection)
            oDA = New OracleDataAdapter(strCommandText, oConnect)
            ds = New DataSet
            oDA.Fill(ds, strTable)

        Catch ex As Exception
            'thong bao loi ra o day
        Finally
            oDA.Dispose()
            ' oConnect.Close()
            oConnect.Dispose()
        End Try
        Return ds
    End Function


    'select data ra mot bang dung cau truy van truc tiep
    Public Shared Function ExecuteToTable(ByVal strCommandText As String) As DataTable
        Dim oConnect As OracleConnection = New OracleConnection(strConnection)
        Dim oDA As OracleDataAdapter
        Dim dt As New DataTable
        Try
            oDA = New OracleDataAdapter(strCommandText, oConnect)
            oDA.Fill(dt)
        Catch ex As Exception
            'thong bao loi ra o day
        Finally
            oConnect.Close()
            oConnect.Dispose()
        End Try
        Return dt
    End Function

    'ham nay select data dung procedure va tra ve mot dataTable
    Public Shared Function ExecuteToTable(ByVal oracleProceduce As String, ByVal strCursorOut As String) As DataTable
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Dim oDa As OracleDataAdapter = Nothing
        Dim dt As DataTable = New DataTable
        Try
            oConnect = New OracleConnection(strConnection)
            oCommand = New OracleCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = oracleProceduce
            oCommand.CommandType = CommandType.StoredProcedure

            oCommand.Parameters.Add(strCursorOut, OracleType.Cursor).Direction = ParameterDirection.Output
            oConnect.Open()
            oDa = New OracleDataAdapter(oCommand)

            oDa.Fill(dt)
        Catch ex As Exception
            'oCommand.Dispose()
            'oConnect.Close()
            'oConnect.Dispose()
        Finally
            If Not oDa Is Nothing Then
                oDa.Dispose()
            End If
            If Not oCommand Is Nothing Then
                oCommand.Dispose()
            End If
            If Not oConnect Is Nothing Then
                oConnect.Close()
                oConnect.Dispose()
            End If
        End Try
        Return dt
    End Function

    ' ham nay insert data vao mot bang dung truc tiep truy van oracle
    Public Shared Function InsertTable(ByVal FieldNames As String(), ByVal FieldValues As String(), ByVal strTable As String) As Integer
        Dim strCommandText As String = String.Format("INSERT INTO {2} ({0}) VALUES ({1})", _
        String.Join(", ", FieldNames), String.Join(", ", FieldValues), strTable)
        Return Execute(strCommandText)
    End Function

    ' ham nay insert data vao mot bang dung proceduce va cac parameter
    Public Shared Function InsertTable(ByVal strProceduceOracle As String, ByVal FieldNames As String(), ByVal ListParameter As Object()) As Integer
        Dim oConnect As OracleConnection = New OracleConnection(strConnection)
        Dim oCommand As OracleCommand = New OracleCommand
        Try
            'setup cac thuoc tinh cua doi tuong oCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = strProceduceOracle
            oCommand.CommandType = CommandType.StoredProcedure
            'them vao cac parameter
            oCommand = AddParameter(oCommand, FieldNames, ListParameter)
            oConnect.Open()
            Return oCommand.ExecuteNonQuery()
        Catch ex As Exception
            'oCommand.Dispose()
            'oConnect.Close()
            'oConnect.Dispose()
        Finally
            oCommand.Dispose()
            oConnect.Close()
            oConnect.Dispose()
        End Try
    End Function

    Public Shared Function GetConnection() As OracleCommand
        Dim oConnect As OracleConnection = Nothing
        Dim oCommand As OracleCommand = Nothing
        Try
            oConnect = New OracleConnection(strConnection)
            oCommand.Connection = oConnect
        Catch ex As Exception
            oCommand.Dispose()
        Finally
            oCommand.Dispose()
        End Try
        Return oCommand
    End Function

    ' ham nay de them vao cac tham doi cho doi tuong OracleCommand
    Public Shared Function AddParameter(ByVal oCommand As OracleCommand, ByVal FieldParameter As String(), ByVal FieldValues As Object()) As OracleCommand
        Dim iCount As Integer = FieldParameter.Length
        Dim i As Integer = 0
        While i < iCount
            oCommand.Parameters.Add(FieldParameter(i).ToString(), FieldValues(i))
            i = i + 1
        End While
        Return oCommand
    End Function

    'ham cap nhat data dung cau lenh truy van truc tiep
    Public Shared Function Update(ByVal FieldNames As String(), ByVal strTable As String, ByVal strCriterias As String) As Integer
        Dim strCommandText As String = String.Format("UPDATE {1} SET {0} {2}", _
        String.Join(", ", FieldNames), _
        strTable, _
        IfNotNullOrEmpty(strCriterias, "WHERE " & strCriterias))
        Return Execute(strCommandText)
    End Function

    ' ham cap nhat tu cau lenh truy van truc tiep
    Public Shared Function Update(ByVal FieldNames As String(), ByVal FieldValues As String(), ByVal strTable As String, ByVal strCriterias As String) As Integer
        Dim Field(FieldNames.Length - 1) As String
        Dim i As Integer = 0
        While i < FieldNames.Length
            Field(i) += FieldNames(i) & "=" & FieldValues(i)
            i += 1
        End While
        Return Update(Field, strTable, strCriterias)
    End Function

    'ham cap nhat su dung procedure
    Public Shared Function Update(ByVal strProcedureOracle As String, ByVal FieldParameters As String(), _
    ByVal FieldValues As Object()) As Integer
        Dim oConnect As OracleConnection = New OracleConnection(strConnection)
        Dim oCommand As OracleCommand = New OracleCommand
        Try
            ' thiet lap cac thuoc tinh cua doi tuong oCommand
            oCommand.Connection = oConnect
            oCommand.CommandText = strProcedureOracle
            oCommand.CommandType = CommandType.StoredProcedure
            'them vao cac parameter
            oCommand = AddParameter(oCommand, FieldParameters, FieldValues)
            oConnect.Open()
            Return oCommand.ExecuteNonQuery()
        Catch ex As Exception
            'oCommand.Dispose()
            'oConnect.Close()
            'oConnect.Dispose()
        Finally
            oCommand.Dispose()
            oConnect.Close()
            oConnect.Dispose()
        End Try
    End Function

    'ham nay xoa data voi dieu kien di kem nao do
    Public Shared Function Delete(ByVal strTable As String, ByVal strCriterias As String) As Integer
        Dim strCommandText As String = String.Format("DELETE {0} {1}", _
                    strTable, IfNotNullOrEmpty(strCriterias, "WHERE " & strCriterias))
        Return Execute(strCommandText)
    End Function

    'ham nay xoa mot bang trong co so du lieu
    Public Shared Function DropTable(ByVal strTableName As String) As Integer
        Dim queryString As String = String.Format("DROP TABLE {0}", strTableName)
        Return Execute(queryString)
    End Function
End Class


