﻿Imports System.Text

Imports log4net
Public Class LogDebug
    Private Shared c_EventSource As String = Configuration.ConfigurationManager.AppSettings("Log_Name").ToString
    Private Const c_LogName As String = "Application"


    Private Shared ReadOnly log As ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Public Shared Sub WriteLog(ByVal ErrorMessage As String, ByVal ErrorType As EventLogEntryType)
        Try
            If ErrorType = EventLogEntryType.Error Then
                log.Error(ErrorMessage)
            Else
                log.Info(ErrorMessage)
            End If
        Catch ex As Exception
            log.Error(ex.Message + ex.StackTrace)
        End Try
    End Sub

    'Public Shared Sub WriteLog(ByVal ErrorMessage As String, ByVal ErrorType As EventLogEntryType)
    '    Try
    '        'Nếu event source đã tồn tại
    '        If EventLog.SourceExists(c_EventSource) Then
    '            'Ghi lỗi
    '            Dim msg As EventLog = New EventLog(c_LogName)
    '            msg.Source = c_EventSource
    '            msg.WriteEntry(ErrorMessage, ErrorType)
    '        Else 'Event source chưa tồn tại
    '            'Tạo event source cho lần ghi lỗi tiếp theo (cần quyền admin của hệ thống)
    '            EventLog.CreateEventSource(c_EventSource, c_LogName)
    '        End If
    '    Catch

    '    End Try
    'End Sub
    'Public Shared Sub WriteLog(ByVal ex As Exception, ByVal strDescription As String, Optional ByVal approverID As String = "")

    '    Dim sbErrMsg As StringBuilder
    '    sbErrMsg = New StringBuilder()
    '    sbErrMsg.Append(vbCrLf)
    '    If approverID <> "" Then
    '        sbErrMsg.Append("approverID: " & approverID)
    '        sbErrMsg.Append(vbCrLf)
    '    End If
    '    sbErrMsg.Append(strDescription & " : ")
    '    sbErrMsg.Append(ex.Source)
    '    sbErrMsg.Append(vbCrLf)
    '    sbErrMsg.Append(ex.Message)
    '    sbErrMsg.Append(vbCrLf)
    '    sbErrMsg.Append(ex.StackTrace)

    '    WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

    '    If Not ex.InnerException Is Nothing Then
    '        sbErrMsg = New StringBuilder()
    '        sbErrMsg.Append(strDescription & " : ")
    '        sbErrMsg.Append(ex.InnerException.Source)
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.InnerException.Message)
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.InnerException.StackTrace)

    '        WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
    '    End If

    '    WriteLog("Error source: " & ex.Source & vbNewLine _
    '          & "Error code: System error!" & vbNewLine _
    '          & "Error message: " & ex.Message, EventLogEntryType.Error)
    'End Sub
End Class
