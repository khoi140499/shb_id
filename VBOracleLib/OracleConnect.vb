﻿Imports System.Data.OracleClient

Public Class OracleConnect
    Dim strConnection As String
    Dim conn As OracleConnection
    Dim comm As OracleCommand
    Dim gstrKT38i As String
    Dim gstrPassKTKB As String

    Public Sub New()
        strConnection = "Data Source=" & gstrKT38i & ";User ID=TDTT;Password=" & gstrPassKTKB
    End Sub
    Public Sub New(ByVal strConn As String)
        strConnection = strConn
    End Sub
    Public Function ExecuteReturnDataset(ByVal strSql As String, ByVal strCommandType As CommandType) As DataSet
        Dim da As OracleDataAdapter
        Dim ds As New DataSet
        Try
            conn = New OracleConnection(strConnection)
            comm = New OracleCommand(strSql)
            conn.Open()
            comm.CommandType = strCommandType
            comm.Connection = conn
            da = New OracleDataAdapter
            da.SelectCommand = comm
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            If Not comm Is Nothing Then
                comm.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
            If Not da Is Nothing Then
                da.Dispose()
            End If
        End Try
    End Function

    Public Function ExecuteNonquery(ByVal strSql As String, ByVal arrParram As IDataParameter()) As ArrayList
        Dim arrOut As New ArrayList
        Dim p As IDataParameter
        Try
            conn = New OracleConnection(strConnection)
            comm = New OracleCommand(strSql)
            comm.CommandType = CommandType.StoredProcedure
            conn.Open()
            comm.Connection = conn
            For Each p In arrParram
                comm.Parameters.Add(p)
            Next
            comm.ExecuteNonQuery()
            For Each p In comm.Parameters
                If p.Direction <> ParameterDirection.Input Then
                    arrOut.Add(p.Value)
                End If
            Next
            Return arrOut
        Catch ex As Exception
            Throw ex
        Finally
            If Not comm Is Nothing Then comm.Dispose()
            If Not conn Is Nothing Then conn.Dispose()
        End Try
    End Function

    Public Function GetParameter( _
                                 ByVal paramName As String, _
                                 ByVal paramDirection As ParameterDirection, _
                                 ByVal paramValue As Object, _
                                 ByVal paramtype As DbType, _
                                 Optional ByVal paramSize As Integer = 0) As OracleParameter
        Dim param As OracleParameter
        param = New OracleParameter(paramName, paramtype)
        If paramDirection = ParameterDirection.Input Or paramDirection = ParameterDirection.InputOutput Then
            param.Value = paramValue
        Else
            param.Value = Nothing
        End If
        param.Direction = paramDirection
        If paramSize <> 0 Then
            param.Size = paramSize
        End If
        Return param
    End Function
End Class
