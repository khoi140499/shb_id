Public Class Globals
    'ham tao dau '...' cho mot xau
    Public Shared Function EscapeQuote(ByVal s As String) As String
        If s Is Nothing Then
            Return ""
        End If
        Return "'" & s.Trim().Replace("'", "''") & "'"
    End Function

    '/// Hàm kiểm tra xem một xâu có null hay empty không?
    Public Shared Function IsNullOrEmpty(ByVal strValue As String) As Boolean
        'neu la xau rong hoac null thi tra ve true
        If Not (strValue Is Nothing) Then
            Return (strValue.Trim.Length = 0)
        End If
        Return True
    End Function

    '/// Hàm kiểm tra xem một mảng có null hay empty không?
    Public Shared Function IsNullOrEmpty(ByVal strValues As String()) As Boolean
        If Not (strValues Is Nothing) Then
            Return (strValues.Length = 0)
        End If
        Return True
    End Function
    '/// Hàm kiểm tra xem một Dataset có null hay empty không?
    Public Shared Function IsNullOrEmpty(ByVal ds As DataSet) As Boolean
        If ds Is Nothing Then
            Return True
        End If
        If ds.Tables.Count <= 0 Then
            Return True
        End If
        If ds.Tables(0).Rows.Count <= 0 Then
            Return True
        End If
        Return False
    End Function
    '/// Hàm kiểm tra xem một Dataset có null hay empty không?
    Public Shared Function IsNullOrEmpty(ByVal dt As DataTable) As Boolean
        If dt Is Nothing Then
            Return True
        End If
        If dt.Rows.Count <= 0 Then
            Return True
        End If

        Return False
    End Function

    'ham nay xet xem mot xau co phai la rong hay ko
    'neu la rong thi tra ve string.empty nguoc lai gan no bang mot xau moi
    Public Shared Function IfNotNullOrEmpty(ByVal strOldValue As String, ByVal strNewValue As String) As String
        Return Microsoft.VisualBasic.IIf(IsNullOrEmpty(strOldValue), String.Empty, strNewValue)

        'mot dong tren thay cho tung nay dong duoi
        'If IsNullOrEmpty(strOldValue) = False Then
        '    Return strNewValue
        'Else
        '    Return String.Empty
        'End If

    End Function
    '// Định dạng lại chuỗi số (Ex: 1.000.000)
    Public Shared Function Format_Number(ByVal strNumber As String, ByVal split As String) As String
        strNumber = strNumber.Replace(split, "")
        If (strNumber.Length > 0) Then
            If strNumber.Substring(0, 1) = "-" Then
                strNumber = strNumber.Replace("-", "")
                If strNumber.Length > 3 Then
                    Dim i As Integer = strNumber.Length
                    While i > 3
                        i -= 3
                        strNumber = strNumber.Insert(i, split)
                    End While
                End If
                strNumber = "-" & strNumber
            Else
                If strNumber.Length > 3 Then
                    Dim i As Integer = strNumber.Length
                    While i > 3
                        i -= 3
                        strNumber = strNumber.Insert(i, split)
                    End While
                End If
            End If
        End If
        Return strNumber
    End Function

    Public Shared Function Format_Number2(ByVal strNumber As String, Optional ByVal split As String = ",", Optional ByVal split2 As String = ".") As String

        Dim aNumber() As String = strNumber.Replace(split, "").Split(split2)
        Dim vNguyen As String = aNumber(0)
        Dim vThapPhan As String = String.Empty
        If (aNumber.Length > 1) Then
            vThapPhan = aNumber(1)
        End If
        If (vNguyen.Length > 0) Then
            If vNguyen.Substring(0, 1) = "-" Then
                vNguyen = vNguyen.Replace("-", "")
                If vNguyen.Length > 3 Then
                    Dim i As Integer = vNguyen.Length
                    While i > 3
                        i -= 3
                        vNguyen = vNguyen.Insert(i, split)
                    End While
                End If
                If (aNumber.Length > 1) Then
                    vNguyen = "-" & vNguyen & split2 & vThapPhan
                Else
                    vNguyen = "-" & vNguyen
                End If

            Else
                If vNguyen.Length > 3 Then
                    Dim i As Integer = vNguyen.Length
                    While i > 3
                        i -= 3
                        vNguyen = vNguyen.Insert(i, split)
                    End While
                End If
                If (aNumber.Length > 1) Then
                    vNguyen = vNguyen & split2 & vThapPhan
                End If
            End If
        End If
        Return vNguyen
    End Function

    Public Shared Function Format_Bank_Account(ByVal strBankAccount As String, ByVal split As String) As String
        strBankAccount = strBankAccount.Replace(split, "")
        If strBankAccount.Length = 14 Then 'Format cho tai khoan bidv
            strBankAccount = strBankAccount.Insert(3, split)
            strBankAccount = strBankAccount.Insert(6, split)
            strBankAccount = strBankAccount.Insert(9, split)
            strBankAccount = strBankAccount.Insert(16, split)
        End If
        Return strBankAccount
    End Function

    Public Shared Function RemoveSign4VietnameseString(ByVal str As String) As String
        If str.Trim.Length = 0 Then
            Return ""
        End If
        Return BIDVUtil.Common.RemoveSign4VietnameseString(str)
    End Function

End Class


