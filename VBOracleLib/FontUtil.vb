﻿'********************************************************************************
' Muc dich: class nay chua cac ham dung trong viec xu ly de hien thi duoc font Unicode
'           Cac ham xu ly font : bien doi giua cac loai font voi nhau
' Nguoi viet:   Vu Van Them
' Ngay viet: 09/08/2003
' Cac ham:
'           Convert from TCVN string to Unicode string
'       Public Function convertFromTCVN(ByVal TCVNString As String) As String
'
'           Convert from Unicode string to TCVN string
'       Public Function convertToTCVN(ByVal UnicodeString As String) As String
'           
'            Convert from VPS string to Unicode string
'       Public Function convertFromVPS(ByVal VPSString As String) As String
'
'           Convert from Unicode string to VPS string
'       Public Function convertToVPS(ByVal UnicodeString As String) As String
'
'           Convert from VISCII string to Unicode string
'       Public Function convertFromVISCII(ByVal VISCIIString As String) As String

'           Convert from Unicode string to VISCII string
'       Public Function convertToVISCII(ByVal UnicodeString As String) As String

'           Convert from VNI string to Unicode string
'       Public Function convertFromVNI(ByVal VNIString As String) As String

'           Convert from Unicode string to VNI string
'       Public Function convertToVNI(ByVal UnicodeString As String) As String

'           Convert from VIQR string to Unicode string
'       Public Function convertFromVIQR(ByVal VIQRString As String) As String
'           Convert from Unicode string to VIQR string
'       Public Function convertToVIQR(ByVal UnicodeString As String) As String
'
'********************************************************************************
Imports System.IO

Public Class FontUtil

    ' Cac bien tong the dung de chua cac chuoi nguyen am mau
    Private Shared sUnicodeVowel As String = "áàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴĐ"
    Private Shared sNoneVowel As String = "aaaaaaaaaaaaaaaaaeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyydAAAAAAAAAAAAAAAAAEEEEEEEEEEEIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYD"
    Private Shared sTCVNVowel As String = "¸µ¶·¹¨¾»¼½Æ©ÊÇÈÉËÐÌÎÏÑªÕÒÓÔÖÝ×ØÜÞãßáâä«èåæçé¬íêëìîóïñòô­øõö÷ùýúûüþ®¸µ¶·¹¡¡¡¡¡¡¢¢¢¢¢¢ÐÌÎÏÑ££££££Ý×ØÜÞãßáâä¤¤¤¤¤¤¥¥¥¥¥¥óïñòô¦¦¦¦¦¦ýúûüþ§"
    Private Shared sVPSVowel As String = "áàäãåæ¡¢£¤¥âÃÀÄÅÆéèÈëËê‰Š‹ÍŒíìÌïÎóòÕõ†ôÓÒ°‡¶Ö§©ª«®úùûÛøÜÙØº»¿šÿ›ÏœÇÁÁÁÁÁˆˆˆˆðˆÂƒ„…ÂÂÉ×ÞþÉÊÊ""•Ê´µ·¸´¹¼½¾¹Ô–—˜™–÷÷÷Ÿ¦÷Ú¨Ñ¬ÚÐ­¯±ÐÐÝ²ý³Ýñ"
    Private Shared sVISCIIVowel As String   '="áàäãÕå¡¢ÆÇ£â¤¥¦ç§éèë¨©êª«¬­®íìïî¸óòöõ÷ô¯°±²µ½¾¶·ÞþúùüûøßÑ×ØæñýÏÖÛÜðÁÀÄÃÁÅ‚AAƒÂ„…†‡ÉÈËˆ‰ÊŠ‹ŒÊÍÌ›Î˜ÓÒ???Ô‘’“"+chr(180)+chr(149)+"–—³”ÚÙœÚ¿º»¼ÿ¹ÝŸ???Ð"

    Private Shared colVNIVowelMap As Collection
    Private Shared colVIQRVowelMap As Collection
    'Public bacup As String ="áàäãÕå¡¢ÆÇ£â¤¥¦ç§éèë¨©êª«¬­®íìïî¸óòöõ÷ô¯°±²µ½¾¶·ÞþúùüûøßÑ×ØæñýÏÖÛÜðÁÀÄÃÁÅ‚AAƒÂ„…†‡ÉÈËˆ‰ÊŠ‹ŒÊÍÌ›Î˜ÓÒ???Ô‘’“´•–—³”ÚÙœÚ¿º»¼ÿ¹ÝŸ???Ð"

    '==================================================
    ' Cac ham dung de convert font
    '==================================================
    'Public Sub New()
    '    Dim xmlTool As New XMLHelper
    '    xmlTool.LoadXMLDir("Parameter.xml", False)

    '    '--------------------------------
    '    ' Neu khong gan ban dau co the load du lieu len tu file XML
    '    '--------------------------------
    '    'sUnicodeVowel = xmlTool.Get_Key("UNICODE")
    '    'sTCVNVowel = xmlTool.Get_Key("TCVN")
    '    'sVPSVowel = xmlTool.Get_Key("VPS")
    '    'sVISCIIVowel = xmlTool.Get_Key("VISCII")

    '    ' Each VNI vowel might take 2 characters. Read VNI Vowels into a Listbox
    '    ' Each Listbox item contains just a vowel corresponding to the Unicode Vowel
    '    colVNIVowelMap = PopulateCollectionFromFileTxt("VNIVowelMap.txt")

    '    ' Each VIQR vowel might take 2 or 3 characters. Read VIQR Vowels into a Listbox
    '    ' Each Listbox item contains just a vowel corresponding to the Unicode Vowel
    '    colVIQRVowelMap = PopulateCollectionFromFileTxt("VIQRVowelMap.txt")
    'End Sub

    Public Function ReadTextFile(ByVal txtFileName As String) As String
        ' Ham nay doc mot file text tra ve noi dung
        Dim strText As String = ""

        If Not System.IO.File.Exists(Trim(txtFileName)) Then
            Dim strErrorMessage As String = "File " + txtFileName + " không tồn tại " 'GetAppMessage("MQ", mqe.Reason)
            ' write log file
            'Show Message to User
            'LogDebug.ShowMessage(strErrorMessage)
            Return strText
            Exit Function
        End If
        Try
            ' Create an instance of TextReader to read from a file.
            Dim srFileReader As StreamReader = New StreamReader(txtFileName)
            Dim line As String
            ' Read and display the lines from the file until the end 
            ' of the file is reached.
            strText = srFileReader.ReadToEnd
            srFileReader.Close()
        Catch E As Exception
            Dim strErrorMessage As String = "Lỗi khi đọc file " + txtFileName + Chr(13) + E.ToString
            ' write log file

        End Try

        Return strText
    End Function

    Public Function PopulateCollectionFromFileTxt(ByVal txtFileName As String) As Collection
        ' Display a text file in a Collection
        Dim clnList As New Collection

        Try
            ' Get out if the input file does not exist
            If Not System.IO.File.Exists(Trim(txtFileName)) Then
                Dim strErrorMessage As String = "File " + txtFileName + " không tồn tại " 'GetAppMessage("MQ", mqe.Reason)
                ' write log file
                Exit Function
            End If
            ' Create an instance of StreamReader to read from a file.
            Dim srFileReader As System.IO.StreamReader = New StreamReader(txtFileName)
            Dim line As String
            ' Read and display the lines from the file until the end 
            ' of the file is reached.
            Do
                line = srFileReader.ReadLine()
                clnList.Add(line)      ' Add the line to the listbox
            Loop Until line Is Nothing
            srFileReader.Close()
        Catch E As Exception
            Dim strErrorMessage As String = "Lỗi khi đọc file " + txtFileName + Chr(13) + E.ToString
            ' write log file
        End Try

        Return clnList
    End Function


    '**************************************************************
    '
    ' Muc dich: Bien doi tu xau Unicode sang Multichar (bien doi 1-n)
    ' Dau vao:  inString - Xau Unicode can bien doi
    '           colVowelMap - Bang map ma ky tu cua Multichar
    '           UVowels - Bang map ma ky tu cua Unicode
    ' Tra ve:
    '           Xau o dang Multichar
    ' Nguoi viet:   Vu Van Them
    ' Ngay viet:    1/08/2003
    '
    '**************************************************************
    Private Shared Function UnicodeToMultichar(ByVal inString As String, ByRef colVowelMap As Collection, ByVal UVowels As String) As String
        ' Convert Unicode vowels to an encoding like VNI or VIQR where it might take more than one
        '   characters to represent a vowel.
        Dim letter As String = ""
        Dim Text1 As String = ""
        Dim Text2 As String = ""
        Dim i, Pos
        ' Assign content of input Textbox to Text1
        Text1 = inString
        ' Iterate through every Unicode character, don't care if it take 1,2 or 3 characters internally
        For i = 1 To Len(Text1)
            ' get a Unicode character
            letter = Mid(Text1, i, 1)
            ' if it's a Carriage return or a LineFeed then just copy across
            If (letter = vbCr) Then
                Text2 = Text2 & vbCr
            ElseIf (letter = vbLf) Then
                Text2 = Text2 & vbLf
            Else
                ' can we find it in the Unicode vowel list?
                Pos = InStr(UVowels, letter)
                If Pos <= 0 Then
                    ' if not then just copy across as is
                    Text2 = Text2 & letter
                Else
                    ' convert to the corresponding multi-character vowel of VNI or VIQR
                    Text2 = Text2 & colVowelMap.Item(Pos)
                End If
            End If
        Next
        'return the result
        UnicodeToMultichar = Text2
    End Function

    '**************************************************************
    '
    ' Muc dich: Bien doi tu xau Multichar sang Unicode (bien doi n-1 )
    ' Dau vao:  inString - Xau Multichar can bien doi
    '           lstVNI - Bang map ma ky tu cua Multichar
    '           UVowels - Bang map ma ky tu cua Unicode
    ' Tra ve:
    '           Xau o dang Unicode
    ' Nguoi viet:   Vu Van Them
    ' Ngay viet:    1/08/2003
    '
    '**************************************************************
    Private Shared Function MulticharToUnicode(ByVal inString As String, ByVal lstVNI As Collection, ByVal UVowels As String) As String
        ' Convert a multi-character vowel in VNI or VIQR to Unicode
        Dim letter As String = ""
        Dim Text1 As String = ""
        Dim Text2 As String = ""
        Dim i, Pos, Item, MapNum, TLen

        ' Assign content of input Textbox to Text1
        Text1 = inString
        ' Replace every multi-character vowel in Text1 with a string like |067 that represents
        ' the 67th Unicode vowel
        ' Iterate through every multi-character vowel
        For i = 1 To lstVNI.Count
            ' Get an item from the Vowel listbox
            Item = lstVNI.Item(i)  ' like a^~016
            letter = RTrim(Left(Item, 3))  ' isolate the multi-character vowel eg: a^~
            MapNum = "|" & Right(Item, 3)  ' Prefix the | character to the digit string eg: &016
            Text1 = Replace(Text1, letter, MapNum) ' replace all occurences of the vowel
        Next
        ' Now map the position strings like &016, &114 to 16th and 114th Unicode vowels
        i = 1
        TLen = Len(Text1)
        Do While i <= TLen
            ' get a character
            letter = Mid(Text1, i, 1)
            ' if it's a Carriage return or a LineFeed then just copy across
            If (letter = vbCr) Then
                Text2 = Text2 & vbCr
                i = i + 1
            ElseIf (letter = vbLf) Then
                Text2 = Text2 & vbLf
                i = i + 1
            ElseIf letter <> "|" Then
                ' merely copy across everything else
                Text2 = Text2 & letter
                i = i + 1
            Else
                ' get here if encounter a "&", obtain the position of the Unicode vowel
                ' Note that there'll be a bug if the text string contains genuine "|" character
                Pos = Val(Mid(Text1, i + 1, 3))
                If Pos = 0 Then
                    Text2 = Text2 & letter
                    i = i + 1
                Else
                    ' get the Unicode vowel for output
                    Text2 = Text2 & Mid(UVowels, Pos, 1)
                    i = i + 4
                End If
            End If
        Loop
        ' Return the result
        MulticharToUnicode = Text2
    End Function

    '**************************************************************
    '
    ' Muc dich: Bien doi tu xau dang ma hoa 1 sang xau ma hoa 2 (bien doi 1-1)
    ' Dau vao:  inString - Xau dang ma hoa thu 1 can bien doi
    '           Vowel1 - Bang map ma ky tu thu 1
    '           Vowel2 - Bang map ma ky tu thu 2
    ' Tra ve:
    '           Xau o dang ma hoa thu 1
    ' Nguoi viet:   Vu Van Them
    ' Ngay viet:    1/08/2003
    '
    '**************************************************************
    Private Shared Function StringToString(ByVal inString As String, ByVal Vowel1 As String, ByVal Vowel2 As String) As String
        ' Direct one-to-one character mapping from one encoding to another
        ' using in convert Unicode to TCVN3.. and reverse
        Dim letter As String = ""
        Dim Text1 As String = ""
        Dim Text2 As String = ""
        Dim strXauNhay As String = ""
        Dim intNumNhay As Integer = 0

        Dim i, Pos
        ' Use Text1 to execute  a litle faster than TextBox1(0)
        Text1 = inString
        ' Iterate through each character of the from Text string
        For i = 1 To Len(Text1)
            letter = Mid(Text1, i, 1)
            ' remove Carriage Return and Line Feed characters as is
            If (letter = vbCr) Then
                'Text2 = Text2 & vbCr
            ElseIf (letter = vbLf) Then
                'Text2 = Text2 & vbLf
            Else
                ' Find position of character in the vowel list
                Pos = InStr(Vowel1, letter)
                If Pos <= 0 Then
                    ' Not found - so do  not map
                    If letter = "'" Then
                        strXauNhay = strXauNhay & letter
                        intNumNhay = intNumNhay + 1
                    ElseIf intNumNhay > 0 Then  ' Xu ly viec khong bo 2 ky tu ' gan nhau                        
                        If intNumNhay Mod 2 <> 0 Then
                            'Bo bot mot ky tu nhay di
                            strXauNhay = strXauNhay.Substring(1, strXauNhay.Length - 1)
                        End If
                        Text2 = Text2 & strXauNhay & letter
                        strXauNhay = ""
                        intNumNhay = 0
                    Else
                        Text2 = Text2 & letter
                        strXauNhay = ""
                        intNumNhay = 0
                    End If
                Else
                    ' Found - so pick the corresponding character in the other vowel list
                    Text2 = Text2 & Mid(Vowel2, Pos, 1)
                End If
            End If
        Next
        ' Xu ly viec khong bo 2 ky tu ' gan nhau
        If intNumNhay > 0 Then
            If intNumNhay Mod 2 <> 0 Then
                strXauNhay = strXauNhay.Substring(1, strXauNhay.Length - 1)
            End If
            Text2 = Text2 & strXauNhay
            strXauNhay = ""
            intNumNhay = 0
        End If

        StringToString = Text2
    End Function

    Private Function Xuly(ByVal strInString As String)


    End Function
    'Convert from TCVN string to Unicode string
    Public Shared Function convertFromTCVN(ByVal TCVNString As String) As String
        ' Convert the Text from TCVN to Unicode, using one-to-one look-up
        convertFromTCVN = StringToString(TCVNString, sTCVNVowel, sUnicodeVowel)
    End Function
    'Convert from Unicode string to TCVN string
    Public Shared Function convertToTCVN(ByVal UnicodeString As String) As String
        ' Convert the Text from Unicode to TCVN, using one-to-one look-up
        '  Note that this might not be correct for TCVN Uppercase (chu HOA)
        '     as each TCVN Uppercase takes 2 characters.
        ' If necessary, use the same conversion technique as for VNI and VIQR
        convertToTCVN = StringToString(UnicodeString, sUnicodeVowel, sTCVNVowel)
    End Function

    'Convert from VPS string to Unicode string
    Public Shared Function convertFromVPS(ByVal VPSString As String) As String
        ' Convert the Text from VPS to Unicode, using one-to-one look-up
        convertFromVPS = StringToString(VPSString, sVPSVowel, sUnicodeVowel)
    End Function
    'Convert from Unicode string to VPS string
    Public Shared Function convertToVPS(ByVal UnicodeString As String) As String
        ' Convert the Text from VPS to Unicode, using one-to-one look-up
        convertToVPS = StringToString(UnicodeString, sUnicodeVowel, sVPSVowel)
    End Function

    'Convert from VISCII string to Unicode string
    Public Shared Function convertFromVISCII(ByVal VISCIIString As String) As String
        ' Convert the Text from VISCII to Unicode, using one-to-one look-up
        convertFromVISCII = StringToString(VISCIIString, sVISCIIVowel, sUnicodeVowel)
    End Function
    'Convert from Unicode string to VISCII string
    Public Shared Function convertToVISCII(ByVal UnicodeString As String) As String
        ' Convert the Text from VISCII to Unicode, using one-to-one look-up
        convertToVISCII = StringToString(UnicodeString, sUnicodeVowel, sVISCIIVowel)
    End Function

    'Convert from VNI string to Unicode string
    Public Shared Function convertFromVNI(ByVal VNIString As String) As String
        ' Convert the Text from VNI  to Unicode, using look-up one Unicode vowel corresponds
        '     to a VNI Vowel in a collection
        convertFromVNI = MulticharToUnicode(VNIString, colVNIVowelMap, sUnicodeVowel)
    End Function

    'Convert from Unicode string to VNI string
    Public Shared Function convertToVNI(ByVal UnicodeString As String) As String
        ' Convert the Text from Unicode to VNI, using look-up one Unicode vowel corresponds
        '     to a VNI Vowel in a collection
        convertToVNI = UnicodeToMultichar(UnicodeString, colVNIVowelMap, sUnicodeVowel)
    End Function

    'Convert from VIQR string to Unicode string
    Public Shared Function convertFromVIQR(ByVal VIQRString As String) As String
        ' Convert the Text from VIQR  to Unicode, using look-up one Unicode vowel corresponds
        '     to a VNI Vowel in a collection
        convertFromVIQR = MulticharToUnicode(VIQRString, colVIQRVowelMap, sUnicodeVowel)
    End Function
    'Convert from Unicode string to VIQR string
    Public Shared Function convertToVIQR(ByVal UnicodeString As String) As String
        ' Convert the Text from Unicode to VIQR, using look-up one Unicode vowel corresponds
        '     to a VIQR Vowel in a collection
        convertToVIQR = UnicodeToMultichar(UnicodeString, colVIQRVowelMap, sUnicodeVowel)
    End Function

    'Convert from Unicode string to none string
    Public Shared Function convertToNone(ByVal UnicodeString As String) As String
        ' Convert the Text from Unicode to TCVN, using one-to-one look-up
        '  Note that this might not be correct for TCVN Uppercase (chu HOA)
        '     as each TCVN Uppercase takes 2 characters.
        ' If necessary, use the same conversion technique as for VNI and VIQR
        Return StringToString(UnicodeString, sUnicodeVowel, sNoneVowel)
    End Function
End Class
