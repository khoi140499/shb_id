﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Services;
using VBOracleLib;

namespace ETAX_API_SERVICE
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class eTAXAPI : System.Web.Services.WebService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [WebMethod]
        public string wsProcess(string xml)
        {
            string str = "";
            try
            {
                eTAXAPILib.ProcessMSG objPro = new eTAXAPILib.ProcessMSG();
                log.Info(" Message Request:"+xml);
                str = objPro.XuLyMSG(xml);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
            }       
            return str;
        }
    }
}