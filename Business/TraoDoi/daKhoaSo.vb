﻿Imports VBOracleLib
Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon

Public Class daKhoaSo
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    'ICB: Hàm kiểm tra xem còn tồn tại chứng từ đã kiểm soát mà chưa được in thành bảng kê hay ko?
    'Hoàng Văn Anh
    'Public Function Is_DaIn_BK() As Boolean
    '    Dim strSQL As String
    '    Dim KetNoi As DataAccess
    '    Dim bResult As Boolean = False
    '    Try
    '        KetNoi = New DataAccess
    '        If IsTinh_TP() Then
    '            strSQL = "Select trang_thai From TCS_CTU_HDR " & _
    '                " Where ngay_kb = " & gdtmNgayLV & " And trang_thai = '01' And TT_IN=0"
    '        Else
    '            strSQL = "Select trang_thai From TCS_CTU_HDR " & _
    '                " Where ngay_kb = " & gdtmNgayLV & " And trang_thai = '01' And TT_IN=0 And MA_XA <>" & Globals.EscapeQuote(gstrMaTTP)
    '        End If
    '        Dim ds As DataSet
    '        ds = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '        If IsEmptyDataSet(ds) Then
    '            bResult = True
    '        Else
    '            bResult = False
    '        End If
    '    Catch ex As Exception
    '        'LogDebug.Writelog("Lỗi trong quá trình kiểm tra chứng từ đã in Bk hay chưa?: " & ex.ToString)
    '        Throw ex
    '    Finally
    '        If Not KetNoi Is Nothing Then
    '            KetNoi.Dispose()
    '        End If
    '    End Try
    '    Return bResult
    'End Function
    ''ICB: Hàm kiểm tra xem còn tồn tại BL chưa lập GNT hay chưa?
    ''Hoàng Văn Anh
    'Public Function Get_BL(ByVal strTrangThai As String) As DataSet
    '    Dim strSQL As String
    '    Dim KetNoi As DataAccess
    '    Dim ds As DataSet = New DataSet
    '    Try
    '        KetNoi = New DataAccess
    '        If IsTinh_TP() Then
    '            strSQL = "Select distinct SO_BTHU From tcs_ctbl " & _
    '                "Where ngay_kb = " & gdtmNgayLV & " And trang_thai =" & Globals.EscapeQuote(strTrangThai)
    '        Else
    '            strSQL = "Select distinct SO_BTHU From tcs_ctbl " & _
    '               "Where ngay_kb = " & gdtmNgayLV & " And trang_thai =" & Globals.EscapeQuote(strTrangThai)
    '        End If
    '        ds = KetNoi.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '    Catch ex As Exception
    '        'LogDebug.Writelog("Lỗi trong quá trình kiểm tra xem còn BL chưa Lập GNT hay ko?: " & ex.ToString)
    '        Throw ex
    '    Finally
    '        If Not KetNoi Is Nothing Then
    '            KetNoi.Dispose()
    '        End If
    '    End Try

    '    Return ds
    'End Function
    Public Function getChungTu(ByVal strTThai As String, ByVal strMaNV As String) As DataSet
        Dim strSql As String
        Dim cnKS As DataAccess
        Dim ds As DataSet
        Dim gdtmNgayLV As Long
        gdtmNgayLV = GetNgayLV(strMaNV)
        Try
            cnKS = New DataAccess
            strSql = "Select distinct trang_thai,SO_BTHU From TCS_CTU_HDR" & _
                     "Where ngay_kb = " & gdtmNgayLV & " And trang_thai = '" & strTThai & "'"
            ds = cnKS.ExecuteReturnDataSet(strSql, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình kiểm tra sự tồn tại của chứng từ")
            'LogDebug.WriteLog("Lỗi trong quá trình kiểm tra sự tồn tại của chứng từ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnKS Is Nothing Then
                cnKS.Dispose()
            End If
        End Try
    End Function
    Public Sub KX_CQThu(ByVal lngNgayKX As Long)
        '---------------------------------------------------------------------------------
        ' Mục đích: gọi thủ tục ket xuat du lieu cho co quan thu
        ' Người viết: HoanNH
        ' Ngày viết: 18/06/2008
        '-------------------------------------------------------------------------------
        Dim cnTD As DataAccess
        Dim strSql As String
        Dim p As IDbDataParameter
        Dim arrIn As IDataParameter()
        Dim arrOut As ArrayList
        Try
            cnTD = New DataAccess
            ReDim arrIn(0)
            p = cnTD.GetParameter("p_ngayks", ParameterDirection.Input, lngNgayKX, DbType.Int64)
            p.Size = 50
            arrIn(0) = p
            strSql = "EXC_PKG_IMEXPORT.Prc_ketxuat_cqthu"
            arrOut = cnTD.ExecuteNonQuery(strSql, CommandType.StoredProcedure, arrIn)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình kết xuất chứng từ cho cơ quan thu")
            Throw ex
            'LogDebug.WriteLog("Lỗi trong quá trình kết xuất chứng từ cho cơ quan thu: " & ex.ToString, EventLogEntryType.Error)
        Finally
            If Not cnTD Is Nothing Then cnTD.Dispose()
        End Try
    End Sub
    Public Function KiemTraKS() As Boolean
        Dim cnKS As DataAccess
        Dim strSql As String
        Dim drKS As IDataReader
        Dim blnResult As Boolean
        Try
            cnKS = New DataAccess
            strSql = "Select 1 From TCS_KHOASO Where Ngay_LV = Ngay_KS"
            drKS = cnKS.ExecuteDataReader(strSql, CommandType.Text)
            If drKS.Read() Then
                blnResult = True
            Else
                blnResult = False
            End If
        Catch ex As Exception
            blnResult = True
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình kiểm tra ngày khóa sổ")
            Throw ex
            'LogDebug.WriteLog("Lỗi trong quá trình kiểm tra ngày khóa sổ: " & ex.ToString, EventLogEntryType.Error)
        Finally
            If Not drKS Is Nothing Then drKS.Close()
            If Not cnKS Is Nothing Then cnKS.Dispose()
        End Try
        Return blnResult
    End Function

    Public Function KhoaSo(ByVal strNgayLV As String, ByVal strNgayKS As String, ByVal strNgayDC As String, ByVal strMa_Dthu As String)
        Dim strSql As String
        Dim cnKS As DataAccess
        Dim iTransKS As IDbTransaction
        Try
            cnKS = New DataAccess
            iTransKS = cnKS.BeginTransaction()

            ' Bắt đầu khoá sổ
            strSql = "Update TCS_KHOASO " & _
                     " Set NGAY_LV = " & strNgayLV & "," & _
                     " Ngay_KS = " & strNgayKS & "," & _
                     " NGAY_DC = " & strNgayDC & _
                     " Where Ma_DThu in " & strMa_Dthu & "," & _
            cnKS.ExecuteNonQuery(strSql, CommandType.Text, iTransKS)

            ' Đưa vào bảng TCS_ThamSo
            strSql = "Update TCS_THAMSO " & _
                     " Set GIATRI_TS='" & ConvertNumbertoString(strNgayLV) & "' " & _
                     " Where TEN_TS='NGAY_LV' And Ma_Dthu in'" & strMa_Dthu & "'"
            cnKS.ExecuteNonQuery(strSql, CommandType.Text, iTransKS)

            iTransKS.Commit()
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khóa sổ")
            '  Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            iTransKS.Rollback()
            Throw ex
        Finally
            If Not iTransKS Is Nothing Then
                iTransKS.Dispose()
            End If
            If Not cnKS Is Nothing Then
                cnKS.Dispose()
            End If
        End Try
    End Function
    Public Sub KhoaSo_Dthu(ByVal pv_ma_Dthu As String)
        Dim cnTD As DataAccess
        Dim strSql As String
        Dim p As IDbDataParameter
        Dim arrIn As IDataParameter()
        Dim arrOut As ArrayList

        Try
            cnTD = New DataAccess
            ReDim arrIn(0)

            p = cnTD.GetParameter("p_MaDiemThu", ParameterDirection.Input, pv_ma_Dthu, DbType.String)
            p.Size = 200
            arrIn(0) = p
            strSql = "PRC_KHOASO_BYDIEMTHU"
            arrOut = cnTD.ExecuteNonQuery(strSql, CommandType.StoredProcedure, arrIn)
          
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khóa sổ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnTD Is Nothing Then cnTD.Dispose()
        End Try
    End Sub
End Class
