Imports VBOracleLib
Imports Business.TraoDoi
Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Imports TraoDoi.DMDC.Loi


Namespace DMDC
    Public Class daDMDC
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function GoiDL(ByVal strTSNCode As String, ByVal strWhere As String) As Object
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách các gói dữ liệu đang chờ nhận
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim strSQL As String = "SELECT   data_pkg.ID, data_pkg.crea_date, data_pkg.tran_num, " & _
                "tran_lst.tsn_name, data_pkg.tsn_code " & _
                "FROM tran_lst, data_pkg " & _
                "WHERE (tran_lst.tsn_code = data_pkg.tsn_code) " & _
                "AND (tran_lst.tsn_code = '" & strTSNCode & "') " & strWhere & _
                "ORDER BY crea_date DESC"
            Dim cnDataPkg As DataAccess
            Dim dsDataPkg As DataSet

            Try
                cnDataPkg = New DataAccess
                dsDataPkg = cnDataPkg.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin từ CSDL")
                'LogDebug.WriteLog("Có lỗi sảy ra khi lấy thông tin các gói dữ liệu từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnDataPkg Is Nothing Then cnDataPkg.Dispose()
            End Try

            Return dsDataPkg
        End Function

        Public Function LoadData(ByVal strTSNCode As String, ByVal strPkgID As String) As Object
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách các bản ghi của một gói dữ liệu
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim cnDMDC As DataAccess
            Dim drData As IDataReader
            Dim strResult As String = ""
            Dim dsDMDC As DataSet
            Dim strSQL As String = "SELECT fld_code, fld_name, val_type, xtg_name, mess_map, val_fmt, val_len, tab_name " & _
                "FROM tab_lst tab, fld_lst fld, xml_tag xml " & _
                "WHERE(fld.tbl_code = TAB.tab_code) " & _
                "AND tab.tsn_code = '" & strTSNCode & "' " & _
                "AND fld.xtg_code = xml.xtg_code " & _
                "ORDER BY fld.fld_order"

            Try
                cnDMDC = New DataAccess
                drData = cnDMDC.ExecuteDataReader(strSQL, CommandType.Text)
                Do While drData.Read
                    If strResult <> "" Then strResult = strResult & ", "
                    strResult = strResult & drData.GetValue(4)
                Loop

                drData.Close()

                If strResult <> "" Then
                    strResult = "Select " & strResult & " From TRAN_UP, MESS_UP " & _
                    "Where (TRAN_UP.PKG_ID = '" & strPkgID & "') And " & _
                    "(TRAN_UP.Tran_No = MESS_UP.Tran_No) And" & _
                    "((TRAN_UP.Sts_Code='" & gstrSSN & "') Or " & _
                    "((TRAN_UP.Sts_Code = '" & gstrDN & "') And " & _
                    "(TRAN_UP.Resu_Code = '01')))" ' And " & _
                    '"(Mid(TRAN_UP.Lcn_Recv,4,5) ='" & _
                    'Mid(gstrMaNoiLamViec, 4, 5) & " ') And " & _
                    '"Order By MESS_UP.CHAR01,MESS_UP.CHAR02,MESS_UP.CHAR10"

                    dsDMDC = cnDMDC.ExecuteReturnDataSet(strResult, CommandType.Text)
                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi sảy ra khi xây dựng câu lệnh SQL lấy danh mục dùng chung")
                Throw ex
                'LogDebug.WriteLog("Có lỗi sảy ra khi xây dựng câu lệnh SQL lấy danh mục dùng chung: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not drData Is Nothing Then drData.Close()
                If Not cnDMDC Is Nothing Then cnDMDC.Dispose()
            End Try

            Return dsDMDC
        End Function

        Public Function LoadDataDBHC(ByVal strPkgID As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách các bản ghi của một gói dữ liệu
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim cnDMDC As DataAccess
            Dim dsDMDC As DataSet
            Dim strSQL As String
            Try
                cnDMDC = New DataAccess
                strSQL = "Select CHAR02, CHAR03, CHAR04, CHAR05 " & _
                    "FROM MESS_UP, TRAN_UP " & _
                    "Where " & IIf(strPkgID.Trim = "", "", "(TRAN_UP.PKG_ID = '" & strPkgID & "') And ") & _
                    "(TRAN_UP.Tran_No = MESS_UP.Tran_No) And (TRAN_UP.Tsn_Code='05') And" & _
                    "((TRAN_UP.Sts_Code='" & gstrSSN & "') Or " & _
                    "((TRAN_UP.Sts_Code = '" & gstrDN & "') And " & _
                    "(TRAN_UP.Resu_Code = '01'))) And " & buTraoDoi.WhereLcn_Recv("-") & _
                    " Order By CHAR02, CHAR03, CHAR04, CHAR05 "

                dsDMDC = cnDMDC.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi sảy ra khi xây dựng câu lệnh SQL lấy danh mục dùng chung")
                Throw ex
                'LogDebug.WriteLog("Có lỗi sảy ra khi xây dựng câu lệnh SQL lấy danh mục dùng chung: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not cnDMDC Is Nothing Then cnDMDC.Dispose()
            End Try

            Return dsDMDC
        End Function

        Private Sub UpdateTransUp(ByVal strSTS_CODE As String, ByVal strRESU_CODE As String, ByVal strTRAN_NO As String)
            '-----------------------------------------------------------------
            ' Mục đích: Đưa lỗi vào CSDL trong quá trình thao tác truyền tin
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim cUpdate As DataAccess
            Try
                cUpdate = New DataAccess
                Dim strSQL = "Update TRAN_UP Set STS_CODE = '" & strSTS_CODE & "',RESU_CODE = '" & strRESU_CODE & "'" & _
                        " Where TRAN_NO = '" & strTRAN_NO & "'"
                cUpdate.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật lại trạng thái của bảng TransUp")
                Throw ex
                'LogDebug.WriteLog("Có lỗi sảy ra khi cập nhật lại trạng thái của bảng TransUp: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not cUpdate Is Nothing Then cUpdate.Dispose()
            End Try
        End Sub

        Public Sub Import(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
            ByVal fstrWhere As String, ByRef fintTongso As Integer, _
                ByRef fintSoloi As Integer, ByRef fstrImExportKey As String)
            '-----------------------------------------------------------------
            ' Mục đích: Thủ tục Import lấy dữ liệu trong CSDL trung gian đổ vào
            '           CSDL tác nghiệp
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 12/02/2004
            ' Người viết: Nguyễn Quốc Việt
            ' Hạn chế:  Thủ tục này sẽ bị hạn chế nếu CSDL tác nghiệp có quan hệ
            '           Master+Detail song nằm trong cùng một bảng và tham số 
            '           Is_Master = 'N' trong bảng Tab_Lst
            ' ----------------------------------------------------------------
            Dim cSelect As New DataAccess
            Dim ketnoiToInsert As New DataAccess
            Dim ketnoiToUpdate As New DataAccess
            Dim tranInsert As IDbTransaction
            Dim drParalist As IDataReader
            Dim drDatalist As IDataReader
            Dim arrFldIndex As New ArrayList
            Dim arrTblName As New ArrayList
            Dim arrIsMaster As New ArrayList
            Dim arrFldname As New ArrayList
            Dim arrFldtype As New ArrayList
            Dim strMessmapString As String = ""
            Dim strTblname As String
            Dim strIsMaster As String
            Dim strNewTblCode As String
            Dim strFldNameList As String
            Dim strOldTblCode As String = " " ' Một ký tự trống
            Dim dtmCreateTime As Date = Now()
            Dim strSQL As String
            Dim intNumFld As Integer = 0
            Dim strTmpID As String = getID(fstrTsn_Code)

            ' Sinh khoá cho bảng ImExport
            'fstrImExportKey = mdlCommon.getDataKey("TCS_IMEXPORT_SEQ.NEXTVAL")

            ' Hình thành câu lệnh select dữ liệu từ các bảng tham số để lấy ra:
            ' tên bảng, tên trường, danh sách mess_map,...
            strSQL = "SELECT  tab_code,tab_name,is_master,fld_name, map_form,fld_type " _
                    & "FROM tran_lst, tab_lst, fld_lst " _
                    & "WHERE  (TRAN_LST.tsn_code ='" & fstrTsn_Code _
                    & "') And (TRAN_LST.tsn_code = TAB_LST.tsn_code) And " _
                    & "(TRAN_LST.tsn_type='N') And (FLD_LST.fld_tn ='Y') " _
                    & "And (TAB_LST.tab_code = FLD_LST.tbl_code) " _
                    & "ORDER BY TAB_LST.tab_order,FLD_LST.fld_order"
            Try
                drParalist = cSelect.ExecuteDataReader(strSQL, CommandType.Text)
                While drParalist.Read
                    strNewTblCode = drParalist.GetValue(0)
                    If strNewTblCode <> strOldTblCode And intNumFld > 0 Then
                        arrTblName.Add(strTblname)
                        strFldNameList = strFldNameList.Substring(0, strFldNameList.Length - 1)
                        arrFldname.Add(strFldNameList)
                        arrFldIndex.Add(intNumFld)
                        arrIsMaster.Add(strIsMaster)
                        strFldNameList = ""
                    End If
                    strMessmapString &= drParalist.GetValue(4) & "," 'mess_map
                    strTblname = drParalist.GetValue(1)
                    strFldNameList &= drParalist.GetValue(3) & ","
                    arrFldtype.Add(drParalist.GetValue(5))
                    If Not drParalist.IsDBNull(2) Then
                        strIsMaster = drParalist.GetValue(2) 'Is_master
                    Else
                        strIsMaster = "" 'Is_Detail
                    End If
                    strOldTblCode = strNewTblCode
                    intNumFld += 1
                End While
                drParalist.Dispose()
                cSelect.Dispose()
                ' Insert vào phần tử cuối cùng lưu chỉ số của trường cuối cùng
                arrTblName.Add(strTblname)
                strFldNameList = strFldNameList.Substring(0, strFldNameList.Length - 1)
                arrFldname.Add(strFldNameList)
                arrFldIndex.Add(intNumFld)
                arrIsMaster.Add(strIsMaster)
                strMessmapString = strMessmapString.Substring(0, strMessmapString.Length - 1)
                ' Insert một bản ghi vào trong bảng ImExport trước khi đổ dữ liệu từ
                ' CSLD trung gian(Tran_up, Mess_up) vào CSDL tác nghiệp
                strSQL = "Insert into ImExport(ID,TSN_CODE,IEP_TYPE,IEP_DATE,IEP_USER,IEP_ROWNO,IEP_ERRNO)" _
                        & "  VALUES('" & fstrImExportKey & "','" & fstrTsn_Code & "' ,'N'," & ToValue(dtmCreateTime.Date, "DATE", "MM/dd/yyyy") & ",'" _
                        & gstrTenND & "',0,0)"
                ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                ' Bắt đầu đọc dữ liệu từ CSDL trung gian(Tran_up,Mess_up) để đổ vào bảng tác nghiệp (bao gồm dữ liệu không lỗi)
                Dim strOldTranNo As String = ""
                Dim strNewTranNo As String
                Dim blnUpdate As Boolean
                Dim strIdMess_Up As String
                Dim strMidFunction As String = "Mid"
                strMidFunction = "SUBSTR"
                '
                strSQL = "SELECT Mess_Up.Tran_no,Mess_Up.Id," & strMessmapString _
                        & "  FROM Tran_Up,Mess_up " _
                        & "  WHERE (Tran_Up.Tsn_code='" & fstrTsn_Code & "') And" _
                        & "((Tran_Up.Sts_Code='" & gstrSSN & "') Or " _
                        & "((Tran_Up.Sts_Code = '" & gstrDN & "') And (Tran_Up.Resu_Code = '01'))) And " _
                        & " (Tran_up.Tran_no = Mess_up.Tran_no) And" _
                        & " " & fstrWhere & " " _
                        & " ORDER BY Mess_up.Tran_no"
                '& " (" & strMidFunction & "(Tran_Up.Lcn_Recv,4,5) ='" & Mid(gstrMaNoiLamViec, 4, 5) & "') And" _
                drDatalist = cSelect.ExecuteDataReader(strSQL, CommandType.Text)
                ' Khởi tạo bắt đầu giao dịch
                tranInsert = ketnoiToInsert.BeginTransaction
                While drDatalist.Read
                    Dim intFldiIndex As Integer = 0
                    Dim intTblIndex As Integer = 0
                    Dim arrValues As New ArrayList
                    ' Lấy dữ liệu từ các trường của bản ghi vừa đọc và lần lựơt Insert vào
                    ' Các bảng nhận theo thứ tự danh sách các bảng lưu ở trên
                    Try
                        While intTblIndex <= arrTblName.Count - 1
                            Dim strSQLInsString As String
                            Dim strValueString As String = ""
                            While intFldiIndex <= arrFldIndex.Item(intTblIndex) - 1
                                Dim strFldType As String = arrFldtype.Item(intFldiIndex)
                                If Not drDatalist.IsDBNull(intFldiIndex + 2) Then
                                    Select Case UCase(strFldType)
                                        Case "VARCHAR2"
                                            strValueString &= "'" & drDatalist.GetValue(intFldiIndex + 2) & "',"
                                        Case "NUMBER"
                                            strValueString &= "" & drDatalist.GetValue(intFldiIndex + 2) & ","
                                        Case "DATE"
                                            strValueString &= drDatalist.GetDateTime(intFldiIndex + 2).Year & _
                                                drDatalist.GetDateTime(intFldiIndex + 2).Month.ToString.PadLeft(2, "0") & _
                                                    drDatalist.GetDateTime(intFldiIndex + 2).Day.ToString.PadLeft(2, "0") & ","
                                    End Select
                                Else
                                    Select Case UCase(strFldType)
                                        Case "VARCHAR2"
                                            strValueString &= "Null" & ","
                                        Case "NUMBER"
                                            strValueString &= "Null" & ","
                                        Case "DATE"
                                            strValueString &= "Null" & ","
                                    End Select
                                End If
                                ' Lấy giá trị cua cac truong để kiểm tra
                                arrValues.Add(drDatalist.GetValue(intFldiIndex + 2))
                                intFldiIndex += 1
                            End While
                            strValueString = strValueString.Substring(0, strValueString.Length - 1)
                            strFldNameList = arrFldname.Item(intTblIndex)
                            If strTmpID <> "" Then
                                strSQLInsString = "INSERT INTO  " & arrTblName.Item(intTblIndex) & "(" & strTmpID & "," _
                                                                & strFldNameList & ") VALUES(" & getDataKey("TCS_" & strTmpID & "_SEQ.NEXTVAL") & "," & strValueString & ")"
                            Else
                                strSQLInsString = "INSERT INTO  " & arrTblName.Item(intTblIndex) & "(" _
                                                                & strFldNameList & ") VALUES(" & strValueString & ")"
                            End If
                            strIsMaster = arrIsMaster.Item(intTblIndex)
                            If strIsMaster.ToUpper = "Y" Then
                                strNewTranNo = drDatalist.GetValue(0)
                                If strNewTranNo <> strOldTranNo Then
                                    gblnLoiDetail = False ' Bắt đầu một giao dịch khởi tạo trạng thái không bị lỗi
                                    blnUpdate = False ' Trạng thái cập nhât khi trùng khoá (hiện tại không cập nhật)
                                    ' Cập nhật tổng số bản ghi Insert và lỗi
                                    strSQL = "Update IMEXPORT Set IEP_ROWNO = " & fintTongso & "," & _
                                            "IEP_ERRNO = " & fintSoloi & " Where ID = '" & fstrImExportKey & "'"
                                    ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, tranInsert)
                                    ' Commit dữ liệu khi chuyển sang dòng Master+Detail mới
                                    tranInsert.Commit()
                                    ' Bắt đầu theo dõi quá trình cập nhật
                                    tranInsert = ketnoiToInsert.BeginTransaction
                                    strOldTranNo = strNewTranNo
                                End If
                                strIdMess_Up = drDatalist.GetValue(1)
                                If CheckValues(fstrTsn_Code, fblnGhide, "Y", arrValues, fstrImExportKey, strIdMess_Up, strNewTranNo, blnUpdate, ketnoiToInsert, tranInsert) Then
                                    ketnoiToInsert.ExecuteNonQuery(strSQLInsString, CommandType.Text, tranInsert)
                                    fintTongso += 1
                                Else
                                    If blnUpdate Then
                                        ' Khong duoc Insert va blnUpdate = True thi so loi khong duoc tang len va so ban ghi
                                        ' da nhan vao phai tang len.
                                        fintTongso += 1
                                    Else
                                        ' Truong hop bi loi va khong phai do cap nhat(blnUpdate=False). Tong so loi tang len
                                        fintSoloi += 1
                                        ' Neu Master bi loi khong duoc phep Insert du lieu vao bang Detail
                                        ' mac du kiem tra Bảng Detail không có lỗi
                                        gblnLoiDetail = True
                                    End If
                                End If
                            Else
                                ' Neu khong phai la bang master thi khong can so sanh Tran_No, moi ban ghi trong 
                                ' Mess_up se duoc tuong ung voi mot ban ghi trong bang tac nghiep
                                strIdMess_Up = drDatalist.GetValue(1)
                                If CheckValues(fstrTsn_Code, fblnGhide, "N", arrValues, fstrImExportKey, strIdMess_Up, strNewTranNo, blnUpdate, ketnoiToInsert, tranInsert) Then
                                    If Not gblnLoiDetail Then
                                        ' Co loi Detail tu truoc do do khi Detail tiep theo khong bi loi
                                        ' thi cung khong duoc Insert vao tiep nua
                                        ketnoiToInsert.ExecuteNonQuery(strSQLInsString, CommandType.Text, tranInsert)
                                    End If
                                Else
                                    If Not tranInsert Is Nothing Then
                                        ' Doan lenh nay chi thuc hien khi gap mot Detail loi dau tien
                                        tranInsert.Rollback()
                                        fintTongso -= 1
                                        fintSoloi += 1
                                    End If
                                End If
                            End If
                            intTblIndex += 1
                            strValueString = ""
                            strFldNameList = ""
                        End While
                    Catch ex As Exception
                        If Not tranInsert Is Nothing Then
                            tranInsert.Rollback()
                            tranInsert.Dispose()
                            fintTongso -= 1
                            fintSoloi += 1
                            ' Cập nhật vào bảng ImExport tổng số bản ghi đã kết xuất
                            strSQL = "Update IMEXPORT Set IEP_ROWNO = " & fintTongso & "," & _
                                    "IEP_ERRNO = " & fintSoloi & " Where ID = '" & fstrImExportKey & "'"
                            ketnoiToUpdate.ExecuteNonQuery(strSQL, CommandType.Text)
                            ketnoiToUpdate.Dispose()
                            ' TDTT-3333 - Lỗi không xác định
                            '''buLoi.InsertErr(fstrImExportKey, strIdMess_Up, "TDTT-3333")
                        End If
                    Finally
                    End Try
                End While
                drDatalist.Dispose()
                cSelect.Dispose()
                ' Truong hop cuoi cung phai commit cho ca Master+Detail cuoi cung
                If Not tranInsert Is Nothing Then
                    ' Cap nhat tong so ban ghi Insert va loi
                    strSQL = "Update IMEXPORT Set IEP_ROWNO = " & fintTongso & "," & _
                            "IEP_ERRNO = " & fintSoloi & " Where ID = '" & fstrImExportKey & "'"
                    ketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, tranInsert)
                    tranInsert.Commit()
                    tranInsert.Dispose()
                End If
                ketnoiToInsert.Dispose()
            Catch ex As Exception
                fintTongso = 0
                fintSoloi = 999999999 ' chin so 9
                ' Cập nhật vào bảng ImExport tổng số bản ghi đã kết xuất
                strSQL = "Update IMEXPORT Set IEP_ROWNO =" & fintTongso & ", IEP_ERRNO =" & fintSoloi & ",IEP_ERR = '" & _
                        ChuanHoaXau(ex.ToString, 255) & "' Where ID = '" & fstrImExportKey & "'"
                ketnoiToUpdate.ExecuteNonQuery(strSQL, CommandType.Text)
                ketnoiToUpdate.Dispose()
            End Try

        End Sub

        Private Function CheckValues(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
            ByVal fstrIsMaster As String, ByVal farrTmp_Values As ArrayList, _
                ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, _
                    ByVal fstrTran_No As String, ByRef fblnUpdate As Boolean, _
                        ByRef fketnoiToInsert As DataAccess, ByRef ftranInsert As IDbTransaction) As Boolean
            '-----------------------------------------------------------------
            ' Mục đích: Hàm kiểm tra dữ liệu theo strTsn_Code
            ' Tham số:
            ' Giá trị trả về:   + True: Cho phép Insert dữ liệu
            '                   + False:Không cho phép Insert dữ liệu
            ' Ngày viết: 12/02/2004
            ' Người viết: Nguyễn Quốc Việt
            ' ----------------------------------------------------------------
            Dim blnTmpCheckValues As Boolean = True
            '
            Select Case fstrTsn_Code
                Case "01"
                    Dim strSQL As String
                    '
                    If fstrIsMaster = "Y" Then ' Kiểm tra lỗi bảng Master
                        ' Kiểm tra mã cấp
                        If blnTmpCheckValues Then
                            If farrTmp_Values(0) Is System.DBNull.Value Then
                                ' TDTT-3012 - Trường mã cấp không được để trống(null)
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3012")
                                blnTmpCheckValues = False
                            Else
                                If Len(farrTmp_Values(0)) <> 1 Then
                                    ' TDTT-3000 - Mã cấp không đúng định dạng
                                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3000")
                                    blnTmpCheckValues = False
                                End If
                            End If
                        End If

                        ' Kiểm mã chương
                        If blnTmpCheckValues Then
                            If farrTmp_Values(1) Is System.DBNull.Value Then
                                ' TDTT-3013 - Trường mã chương không được để trống(null)
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3013")
                                blnTmpCheckValues = False
                            Else
                                If Len(farrTmp_Values(1)) <> 3 Then
                                    ' TDTT-3001 - Mã chương không đúng định dạng
                                    ' 'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3001")
                                    blnTmpCheckValues = False
                                End If
                            End If
                        End If

                        ' Kiểm tra tên gọi cấp chương
                        If blnTmpCheckValues Then
                            If farrTmp_Values(2) Is System.DBNull.Value Then
                                ' TDTT-3011 - Tên cấp chương không tồn tại
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3011")
                                blnTmpCheckValues = False
                            End If
                        End If
                    Else ' Kiểm tra lỗi bảng Detail
                        ' Giao dịch này không có bảng Detail
                    End If
                    'Kiểm tra khoá cho bảng Master
                    If blnTmpCheckValues Then  ' Không có lỗi chi tiết ở Master
                        If fstrIsMaster = "Y" Then ' Nếu không lỗi kiểm tra trùng khoá
                            Dim objCapChuong As New TDDMDC.infCapChuong
                            objCapChuong.MA_CAP = farrTmp_Values(0)
                            objCapChuong.MA_CHUONG = farrTmp_Values(1)
                            objCapChuong.NGAY_BD = farrTmp_Values(3).Year() & farrTmp_Values(3).Month() & farrTmp_Values(3).Day()
                            objCapChuong.NGAY_KT = farrTmp_Values(4).Year() & farrTmp_Values(4).Month() & farrTmp_Values(4).Day()

                            Dim tmpCapChuong As New TDDMDC.daCapChuong
                            objCapChuong = tmpCapChuong.Load(objCapChuong)

                            Dim cTonTai As New DataAccess
                            Dim drTonTai As IDataReader
                            strSQL = "Select * From TCS_DM_CAP_CHUONG Where (MA_CAP = '" & farrTmp_Values(0) & "') And (MA_CHUONG = '" & farrTmp_Values(1) & "')"
                            drTonTai = cTonTai.ExecuteDataReader(strSQL, CommandType.Text)
                            If objCapChuong.CCH_ID <> "" Then ' Tìm thấy khoá
                                If fblnGhide Then ' Cho phép ghi đè
                                    If tmpCapChuong.Update(objCapChuong) Then
                                        blnTmpCheckValues = False ' Đã cập nhật rồi không cần Insert dữ liệu
                                        fblnUpdate = True ' Dùng kết hợp với hàm CheckValues và không được tăng số lỗi lên
                                        ' Cập nhật lại trạng thái của bảng Tran_Up
                                        Dim cInsert As New DataAccess
                                        strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                                                " Where TRAN_NO = '" & fstrTran_No & "'"
                                        cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                                        cInsert.Dispose()
                                    End If
                                Else ' Trùng khoa và không cho phép ghi đè
                                    ' TDTT-3008 - Mã cấp chương đã tồn tại trong danh mục
                                    ' 'buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3008")
                                    blnTmpCheckValues = False ' Bỏ qua không được Insert dữ liệu
                                End If
                            Else ' Không bị trùng khoá, không lỗi và cho phép Insert dữ liệu
                                ' Cập nhật lại trạng thái của bảng Tran_Up
                                Dim cInsert As New DataAccess
                                strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '00'" & _
                                        "Where TRAN_NO = '" & fstrTran_No & "'"
                                cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                                cInsert.Dispose()
                            End If
                            drTonTai.Dispose()
                            cTonTai.Dispose()
                        Else ' Bảng Detail không có lỗi
                            ' Giao dịch này không có bảng Detail
                        End If
                    Else ' Kiểm tra có lỗi chi tiết trong bảng Master hoặc Detail
                        blnTmpCheckValues = False ' Không cho phép Insert dữ liệu
                        ' Cập nhật lại trạng thái của bảng Tran_Up
                        Dim cInsert As New DataAccess
                        strSQL = "Update TRAN_UP Set STS_CODE = '" & gstrDN & "',RESU_CODE = '01'" & _
                                "Where TRAN_NO = '" & fstrTran_No & "'"
                        cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
                        cInsert.Dispose()
                    End If
                Case "02"
                    Dim strSQL As String
                    '
                    If fstrIsMaster = "Y" Then ' Kiểm tra lỗi bảng Master
                        ' Kiểm tra mã loại
                        If blnTmpCheckValues Then
                            If farrTmp_Values(0) Is System.DBNull.Value Then
                                ' TDTT-3032 - Trường mã loại không được để trống (null)
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3032")
                                blnTmpCheckValues = False
                            Else
                                If Len(farrTmp_Values(0)) <> 2 Then
                                    ' TDTT-3020 - Mã loại không đúng định dạng
                                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3020")
                                    blnTmpCheckValues = False
                                End If
                            End If
                        End If

                        ' Kiểm mã khoản
                        If blnTmpCheckValues Then
                            If farrTmp_Values(1) Is System.DBNull.Value Then
                                ' TDTT-3033 - Trường mã khoản không được để trống (null)
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3033")
                                blnTmpCheckValues = False
                            Else
                                If Len(farrTmp_Values(1)) <> 2 Then
                                    ' TDTT-3021 - Mã khoản không đúng định dạng
                                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3021")
                                    blnTmpCheckValues = False
                                End If
                            End If
                        End If

                        ' Kiểm tra tên gọi loại khoản
                        If blnTmpCheckValues Then
                            If farrTmp_Values(2) Is System.DBNull.Value Then
                                ' TDTT-3031 - Tên loại khoản không tồn tại
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3031")
                                blnTmpCheckValues = False
                            End If
                        End If
                    Else ' Kiểm tra lỗi bảng Detail
                        ' Giao dịch này không có bảng Detail
                    End If

                    'Kiểm tra khoá cho bảng Master
                    If blnTmpCheckValues Then  ' Không có lỗi chi tiết ở Master
                        If fstrIsMaster = "Y" Then ' Nếu không lỗi kiểm tra trùng khoá
                            Dim objLoaiKhoan As New TDDMDC.infLoaiKhoan
                            objLoaiKhoan.MA_LOAI = farrTmp_Values(0)
                            objLoaiKhoan.MA_KHOAN = farrTmp_Values(1)
                            objLoaiKhoan.NGAY_BD = farrTmp_Values(3).Year() & farrTmp_Values(3).Month() & farrTmp_Values(3).Day()
                            objLoaiKhoan.NGAY_KT = farrTmp_Values(4).Year() & farrTmp_Values(4).Month() & farrTmp_Values(4).Day()

                            Dim tmpLoaiKhoan As New TDDMDC.daLoaiKhoan
                            objLoaiKhoan = tmpLoaiKhoan.Load(objLoaiKhoan)

                            If objLoaiKhoan.LKH_ID <> "" Then ' Tìm thấy khoá
                                If fblnGhide Then ' Cho phép ghi đè
                                    strSQL = "Update TCS_DM_LOAI_KHOAN Set TEN = " & oToString(farrTmp_Values(2), "C") & _
                                            ",NGAY_BD  = " & oToString(farrTmp_Values(3), "D") & _
                                            ",NGAY_KT  = " & oToString(farrTmp_Values(4), "D") & _
                                            " Where (MA_LOAI = '" & farrTmp_Values(0) & "') And (MA_KHOAN = '" & farrTmp_Values(1) & "')"
                                    fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)
                                    blnTmpCheckValues = False ' Đã cập nhật rồi không cần Insert dữ liệu
                                    fblnUpdate = True ' Dùng kết hợp với hàm CheckValues và không được tăng số lỗi lên
                                    ' Cập nhật lại trạng thái của bảng Tran_Up
                                    UpdateTransUp(gstrDN, "00", fstrTran_No)
                                Else ' Trùng khoa và không cho phép ghi đè
                                    blnTmpCheckValues = False ' Bỏ qua không được Insert dữ liệu
                                    ' TDTT-3028 - Mã loại khoản đã tồn tại trong danh mục
                                    Dim cInsert As New DataAccess
                                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3028")
                                    ' Cập nhật lại trạng thái của bảng Tran_Up
                                    UpdateTransUp(gstrDN, "01", fstrTran_No)
                                End If
                            Else ' Không bị trùng khoá, không lỗi và cho phép Insert dữ liệu
                                ' Cập nhật lại trạng thái của bảng Tran_Up
                                UpdateTransUp(gstrDN, "00", fstrTran_No)
                            End If
                        Else ' Không có lỗi ở bảng Detail
                            ' Giao dịch này không có bảng Detail
                        End If
                    Else ' Kiểm tra có lỗi chi tiết trong bảng Master hoặc Detail
                        blnTmpCheckValues = False ' Không cho phép Insert dữ liệu
                        ' Cập nhật lại trạng thái của bảng Tran_Up
                        UpdateTransUp(gstrDN, "01", fstrTran_No)
                    End If
                Case "03"
                    Dim strSQL As String
                    '
                    If fstrIsMaster = "Y" Then ' Kiểm tra lỗi bảng Master
                        ' Kiểm mã mục
                        If blnTmpCheckValues Then
                            If farrTmp_Values(0) Is System.DBNull.Value Then
                                ' TDTT-3072 - Trường mã mục không được để trống (null)
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3072")
                                blnTmpCheckValues = False
                            Else
                                If Len(farrTmp_Values(0)) <> 3 Then
                                    ' TDTT-3060 - Mã mục không đúng định dạng
                                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3060")
                                    blnTmpCheckValues = False
                                End If
                            End If
                        End If

                        ' Kiểm mã tiểu mục
                        If blnTmpCheckValues Then
                            If farrTmp_Values(1) Is System.DBNull.Value Then
                                ' TDTT-3073 - Trường mã tiểu mục không được để trống (null)
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3073")
                                blnTmpCheckValues = False
                            Else
                                If Len(farrTmp_Values(1)) <> 2 Then
                                    ' TDTT-3061 - Mã tiểu mục không đúng định dạng
                                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3061")
                                    blnTmpCheckValues = False
                                End If
                            End If
                        End If

                        ' Kiểm tra tên gọi mục tiểu mục
                        If blnTmpCheckValues Then
                            If farrTmp_Values(2) Is System.DBNull.Value Then
                                ' TDTT-3071 - Tên mục tiểu mục không tồn tại
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3071")
                                blnTmpCheckValues = False
                            End If
                        End If
                    Else ' Kiểm tra lỗi bảng Detail
                        ' Giao dịch này không có bảng Detail
                    End If
                    'Kiểm tra khoá cho bảng Master
                    If blnTmpCheckValues Then  ' Không có lỗi chi tiết ở Master
                        If fstrIsMaster = "Y" Then ' Nếu không lỗi kiểm tra trùng khoá
                            Dim cTonTai As New DataAccess
                            Dim drTonTai As IDataReader
                            strSQL = "Select * From TCS_DM_MUC_TMUC Where (MA_MUC = '" & farrTmp_Values(0) & "') And (MA_TMUC = '" & farrTmp_Values(1) & "')"
                            drTonTai = cTonTai.ExecuteDataReader(strSQL, CommandType.Text)
                            If drTonTai.Read Then ' Tìm thấy khoá
                                If fblnGhide Then ' Cho phép ghi đè
                                    strSQL = "Update TCS_DM_MUC_TMUC Set TEN = " & oToString(farrTmp_Values(2), "C") & _
                                            ",NGAY_BD  = " & oToString(farrTmp_Values(3), "D") & _
                                            ",NGAY_KT  = " & oToString(farrTmp_Values(4), "D") & _
                                            " Where (MA_MUC = '" & farrTmp_Values(0) & "') And (MA_TMUC = '" & farrTmp_Values(1) & "')"
                                    fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)
                                    blnTmpCheckValues = False ' Đã cập nhật rồi không cần Insert dữ liệu
                                    fblnUpdate = True ' Dùng kết hợp với hàm CheckValues và không được tăng số lỗi lên
                                    ' Cập nhật lại trạng thái của bảng Tran_Up
                                    UpdateTransUp("50", "00", fstrTran_No)
                                Else ' Trùng khoa và không cho phép ghi đè
                                    blnTmpCheckValues = False ' Bỏ qua không được Insert dữ liệu
                                    ' TDTT-3068 - Mã mục tiểu mục đã tồn tại trong danh mục
                                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3068")
                                    ' Cập nhật lại trạng thái của bảng Tran_Up
                                    UpdateTransUp(gstrDN, "01", fstrTran_No)
                                End If
                            Else ' Không bị trùng khoá, không lỗi và cho phép Insert dữ liệu
                                ' Cập nhật lại trạng thái của bảng Tran_Up
                                UpdateTransUp(gstrDN, "00", fstrTran_No)
                            End If
                            drTonTai.Dispose()
                            cTonTai.Dispose()
                        Else ' Bảng Detail không có lỗi
                            ' Giao dịch này không có bảng Detail
                        End If
                    Else ' Kiểm tra có lỗi chi tiết trong bảng Master hoặc Detail
                        blnTmpCheckValues = False ' Không cho phép Insert dữ liệu
                        ' Cập nhật lại trạng thái của bảng Tran_Up
                        UpdateTransUp(gstrDN, "01", fstrTran_No)
                    End If
                Case "04"
                    Dim strSQL As String

                    ' Kiểm tra mã nguyên tệ
                    If blnTmpCheckValues Then
                        If farrTmp_Values(0) Is System.DBNull.Value Then
                            ' TDTT-3012 - Trường mã nguyên tệ không đựơc để trống(null)
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3099")
                            blnTmpCheckValues = False
                        Else
                            If Len(farrTmp_Values(0)) <> 3 Then
                                ' TDTT-3000 - Mã nguyên tệ không đúng định dạng
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3098")
                                blnTmpCheckValues = False
                            End If
                        End If
                    End If

                    ' Kiểm tra tên nguyên tệ
                    If blnTmpCheckValues Then
                        If farrTmp_Values(1) Is System.DBNull.Value Then
                            ' TDTT-3013 - Trường tên nguyên tệ không đựơc để trống(null)
                            ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3097")
                            blnTmpCheckValues = False
                        Else
                            If Len(farrTmp_Values(1)) > 100 Then
                                ' TDTT-3001 - Tên nguyên tệ không đúng định dạng
                                ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3096")
                                blnTmpCheckValues = False
                            End If
                        End If
                    End If

                    If blnTmpCheckValues Then  ' Không có lỗi chi tiết ở Master
                        If fstrIsMaster = "Y" Then ' Nếu không lỗi kiểm tra trùng khoá
                            Dim objNguyenTe As TDDMDC.infNguyenTe
                            Dim tmpNguyenTe As New TDDMDC.daNguyenTe
                            objNguyenTe = tmpNguyenTe.Load(farrTmp_Values(0))

                            If objNguyenTe.Ma_NT <> "" Then ' Tìm thấy khoá
                                If fblnGhide Then ' Cho phép ghi đè
                                    strSQL = "Update TCS_DM_NGUYENTE Set TEN = " & oToString(farrTmp_Values(1), "C") & _
                                            " Where (MA_NT = '" & objNguyenTe.Ma_NT & "')"
                                    fketnoiToInsert.ExecuteNonQuery(strSQL, CommandType.Text, ftranInsert)
                                    blnTmpCheckValues = False ' Đã cập nhập rồi, không cần insert dữ liệu
                                    fblnUpdate = True ' Dùng kết hợp với hàm CheckValues và không được tăng số lỗi lên
                                    ' Cập nhật lại trạng thái của bảng Tran_Up
                                    UpdateTransUp(gstrDN, "01", fstrTran_No)
                                Else ' Trùng khoa và không cho phép ghi đè
                                    blnTmpCheckValues = False ' Bỏ qua không được Insert dữ liệu
                                    ' TDTT-3028 - Nguyên tệ đã tồn tại trong danh mục
                                    Dim cInsert As New DataAccess
                                    ''buLoi.InsertErr(fstrId_ImEx, fstrId_Mess, "TDTT-3095")
                                    ' Cập nhật lại trạng thái của bảng Tran_Up
                                    UpdateTransUp(gstrDN, "01", fstrTran_No)
                                End If
                            Else ' Không bị trùng khoá, không lỗi và cho phép Insert dữ liệu
                                ' Cập nhật lại trạng thái của bảng Tran_Up
                                UpdateTransUp(gstrDN, "00", fstrTran_No)
                            End If
                        Else ' Không có lỗi ở bảng Detail
                            ' Giao dịch này không có bảng Detail
                        End If
                    Else ' Kiểm tra có lỗi chi tiết trong bảng Master hoặc Detail
                        ' Cập nhật lại trạng thái của bảng Tran_Up
                        UpdateTransUp(gstrDN, "01", fstrTran_No)
                    End If
            End Select
            Return blnTmpCheckValues
        End Function

        Private Function getID(ByVal strTSNCode As String) As String
            '-----------------------------------------------------------------
            ' Mục đích: Xác định các trường khoá phụ thuộc vào strTSNCode
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Select Case strTSNCode
                Case "01"
                    Return "CCH_ID"
                Case "02"
                    Return "LKH_ID"
                Case "02"
                    Return "MTM_ID"
                Case Else
                    Return ""
            End Select

        End Function

    End Class
End Namespace
