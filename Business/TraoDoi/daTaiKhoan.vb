﻿Imports VBOracleLib
Namespace TDDMDC
    Public Class daTaiKhoan
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Dim cnTaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO TCS_DM_TAIKHOAN (TK, TEN_TK, " & IIf(objTaiKhoan.MA_CQTHU = "", "", "MA_CQThu, ") & "TINH_TRANG) " & _
                                "VALUES ('" & objTaiKhoan.TK & "','" & _
                                objTaiKhoan.TEN_TK & "'," & IIf(objTaiKhoan.MA_CQTHU = "", "", "'" & objTaiKhoan.MA_CQTHU & "',") & "'" & objTaiKhoan.TINH_TRANG & "')"

            Try
                cnTaiKhoan = New DataAccess
                cnTaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới tài khoản")
                'LogDebug.WriteLog("Có lỗi xảy ra khi thêm mới tài khoản: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
                Throw ex
            Finally
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Dim cnTaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_TAIKHOAN SET TEN_TK='" & objTaiKhoan.TEN_TK & "',MA_CQTHU=" & IIf(objTaiKhoan.MA_CQTHU = "", "NULL", "'" & objTaiKhoan.MA_CQTHU & "'") & ",TINH_TRANG='" & objTaiKhoan.TINH_TRANG & "' WHERE TK='" & objTaiKhoan.TK & "'"

            Try
                cnTaiKhoan = New DataAccess
                cnTaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật tài khoản")
                'LogDebug.WriteLog("Có lỗi xảy ra khi cập nhật tài khoản: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
                Throw ex
            Finally
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Dim cnTaiKhoan As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_TAIKHOAN " & _
                           "WHERE " & "TK='" & _
                           objTaiKhoan.TK & "'"
            Try
                cnTaiKhoan = New DataAccess
                cnTaiKhoan.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa tài khoản")
                Throw ex
                'LogDebug.WriteLog("Có lỗi xảy ra khi xoá tài khoản: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
            Finally
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strTK As String) As infTaiKhoan
            Dim objTaiKhoan As New infTaiKhoan
            Dim cnTaiKhoan As DataAccess
            Dim drTaiKhoan As IDataReader
            Dim strSQL As String = "Select TK, TEN_TK, MA_CQTHU, TINH_TRANG From TCS_DM_TAIKHOAN " & _
                        "Where TK='" & strTK & "'"
            Try
                cnTaiKhoan = New DataAccess
                drTaiKhoan = cnTaiKhoan.ExecuteDataReader(strSQL, CommandType.Text)
                If drTaiKhoan.Read Then
                    objTaiKhoan = New infTaiKhoan
                    objTaiKhoan.TK = strTK
                    objTaiKhoan.TEN_TK = drTaiKhoan("TEN_TK").ToString()
                    objTaiKhoan.MA_CQTHU = drTaiKhoan("MA_CQTHU").ToString()
                    objTaiKhoan.TINH_TRANG = drTaiKhoan("TINH_TRANG").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin tài khoản")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drTaiKhoan Is Nothing Then drTaiKhoan.Close()
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return objTaiKhoan
        End Function

        Public Function Load() As Object
            Dim strSQL As String
            Dim cnTaiKhoan As DataAccess
            Dim dsTaiKhoan As DataSet

            Try
                strSQL = "Select TK, " & _
                         "TCS_PCK_TRAODOI.Fnc_FormatTK(TO_CHAR(TK)) TK_VIEW, " & _
                         "TEN_TK, MA_CQThu, " & _
                         "DECODE(TINH_TRANG, Null, '', 1, 'Đã huỷ', 0, 'Đang hoạt động') TINH_TRANG " & _
                         "from TCS_DM_TAIKHOAN Order by TK ASC"

                cnTaiKhoan = New DataAccess
                dsTaiKhoan = cnTaiKhoan.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin tài khoản")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try
            Return dsTaiKhoan
        End Function

        Public Function CheckExist(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Return Load(objTaiKhoan.TK).TK <> ""
        End Function

        Public Shared Function Check2Del(ByVal objTaiKhoan As infTaiKhoan) As Byte
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra tài khoản có thể xoá không
            ' Tham số: objTaiKhoan: tài khoản cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 26/10/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------
            Dim cnTaiKhoan As DataAccess
            Dim drTaiKhoan As IDataReader
            Dim strSQL As String
            Dim bytResult As Byte = 0

            Try
                strSQL = "SELECT SHKB FROM TCS_CTU_HDR WHERE TK_CO = '" & objTaiKhoan.TK & "' OR TK_NO = '" & objTaiKhoan.TK & "'"
                cnTaiKhoan = New DataAccess
                drTaiKhoan = cnTaiKhoan.ExecuteDataReader(strSQL, CommandType.Text)
                If drTaiKhoan.Read Then bytResult = 1
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi kiểm tra quan hệ giữa tài khoản với chứng từ")
                'LogDebug.WriteLog("Có lỗi xảy ra khi kiểm tra quan hệ giữa tài khoản với chứng từ: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drTaiKhoan Is Nothing Then drTaiKhoan.Close()
                If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
            End Try

            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function

    End Class

End Namespace
