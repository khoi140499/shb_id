Imports VBOracleLib
Public Class daSoThue
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public Function DM_LoaiThue_Set() As DataSet
        Dim conn As DataAccess
        Try
            conn = New DataAccess
            Dim strSQL As String
            strSQL = "Select MA_LTHUE,TEN " & _
                "From TCS_DM_LTHUE " & _
                "Order By MA_LTHUE"
            Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Loại thuế")
            Throw ex
        Finally
            If (Not IsNothing(conn)) Then conn.Dispose()
        End Try
    End Function
End Class
