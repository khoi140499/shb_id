Imports VBOracleLib

Namespace DMDC.Loi
    Public Class daLoi
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Load(ByVal strImExportID As String, ByVal strTSNCode As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách các bản ghi của một gói dữ liệu
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim cnDMDC As DataAccess
            Dim drData As IDataReader
            Dim strResult As String = ""
            Dim dsDMDC As DataSet
            Dim intCountFld As Integer = 0
            Dim intTotalFld As Integer = 2
            Dim strSQL As String = "SELECT fld_code, fld_name, val_type, xtg_name, mess_map, val_fmt, val_len, tab_name " & _
                "FROM tab_lst tab, fld_lst fld, xml_tag xml " & _
                "WHERE(fld.tbl_code = TAB.tab_code) " & _
                "AND tab.tsn_code = '" & strTSNCode & "' " & _
                "AND fld.xtg_code = xml.xtg_code AND (FLD.fld_tn ='Y')" & _
                "ORDER BY fld.fld_order"

            Try
                cnDMDC = New DataAccess

                Select Case strTSNCode
                    Case "05"
                        strResult = "SUBSTR(CHAR01,1,3) AS Fld1,SUBSTR(CHAR01,4,2) AS Fld2,SUBSTR(CHAR01,6,2) AS Fld3, "
                    Case "09"
                        strResult = "CHAR01 AS Fld1, CHAR02 AS Fld2, CHAR05 AS Fld3, CHAR06 AS Fld4, CHAR07 AS Fld5, CHAR08 AS Fld6, "
                    Case "62"
                        strResult = "CHAR01 AS Fld1,CHAR04 Fld2, "
                    Case Else
                        drData = cnDMDC.ExecuteDataReader(strSQL, CommandType.Text)
                        Do While drData.Read And intCountFld < 2
                            strResult = strResult & drData.GetValue(4) & " AS Fld" & intCountFld & ", "
                            intCountFld += 1
                        Loop

                        drData.Close()
                End Select

                If strResult <> "" Then
                    'distinct
                    strResult = "SELECT " & strResult & "loi_dl.ma_loi, err_lst.tb_nsd " & _
                        "FROM tran_up, mess_up, loi_dl, err_lst " & _
                        "WHERE (loi_dl.id_imex = '" & strImExportID & "') " & _
                        "AND mess_up.ID = loi_dl.id_mess " & _
                        "AND loi_dl.ma_loi = err_lst.ma_loi(+) " & _
                        "AND tran_up.tran_no = mess_up.tran_no"

                    dsDMDC = cnDMDC.ExecuteReturnDataSet(strResult, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) 'CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xây dựng câu lệnh SQL lấy danh mục dùng chung")
                Throw ex
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Có lỗi sảy ra khi xây dựng câu lệnh SQL lấy danh mục dùng chung!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
            Finally
                If Not drData Is Nothing Then drData.Close()
                If Not cnDMDC Is Nothing Then cnDMDC.Dispose()
            End Try

            Return dsDMDC
        End Function

        Public Sub Insert(ByVal fstrId_ImEx As String, ByVal fstrId_Mess As String, ByVal fstrErrCode As String)
            '-----------------------------------------------------------------
            ' Mục đích: Đưa lỗi vào CSDL trong quá trình thao tác trao đổi
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim cInsert As DataAccess
            Try
                cInsert = New DataAccess
                Dim strSQL = "Insert Into LOI_DL(id_imex,id_mess,ma_loi) Values " & _
                    "('" & fstrId_ImEx & "','" & fstrId_Mess & "','" & fstrErrCode & "')"
                cInsert.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi ghi lỗi trao đổi")
                Throw ex
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: Có lỗi xảy ra khi ghi lỗi trao đổi!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
            Finally
                If Not cInsert Is Nothing Then cInsert.Dispose()
            End Try
        End Sub

    End Class

End Namespace
