Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace TraoDoi

    Public Class buTraoDoi       
        Public Shared Sub Import(ByVal fstrTsn_Code As String, ByVal fblnGhide As Boolean, _
                ByVal fstrWhere As String, ByRef fintTongso As Integer, _
                    ByRef fintSoloi As Integer, ByRef fstrImExportKey As String)
            '-----------------------------------------------------------------
            ' Mục đích: Thủ tục Import lấy dữ liệu trong CSDL trung gian đổ vào
            '           CSDL tác nghiệp
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 12/02/2004
            ' Người viết: Nguyễn Quốc Việt
            ' Hạn chế:  Thủ tục này sẽ bị hạn chế nếu CSDL tác nghiệp có quan hệ
            '           Master+Detail song nằm trong cùng một bảng và tham số 
            '           Is_Master = 'N' trong bảng Tab_Lst
            ' ----------------------------------------------------------------
            Try
                Dim objDanhMuc As New daTraoDoi
                objDanhMuc.Import(fstrTsn_Code, fblnGhide, fstrWhere, fintTongso, fintSoloi, fstrImExportKey)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub Export(ByVal fstrTsn_Code As String, ByVal fstrLcn_Recv As String, _
        ByVal fstrWhere As String, ByRef fintTongso As Integer, ByRef fintSoloi As Integer)
            '-----------------------------------------------------------------
            ' Mục đích: Thủ tục Import lấy dữ liệu trong CSDL trung gian đổ vào
            '           CSDL tác nghiệp
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 12/02/2004
            ' Người viết: Nguyễn Quốc Việt
            ' Hạn chế:  Thủ tục này sẽ bị hạn chế nếu CSDL tác nghiệp có quan hệ
            '           Master+Detail song nằm trong cùng một bảng và tham số 
            '           Is_Master = 'N' trong bảng Tab_Lst
            ' ----------------------------------------------------------------
            Dim objDanhMuc As New daTraoDoi
            objDanhMuc.Export(fstrTsn_Code, fstrLcn_Recv, fstrWhere, fintTongso, fintSoloi)
        End Sub



        'Public Shared Sub ExportData()
        '    '-----------------------------------------------------------------
        '    ' Mục đích: Thủ tục Import lấy dữ liệu trong CSDL trung gian đổ vào
        '    '           CSDL tác nghiệp gọi luôn thủ tục trong db
        '    ' Tham số:
        '    ' Giá trị trả về:
        '    ' Ngày viết: 
        '    ' Người viết: Vũ Trung Kiên
        '    ' ----------------------------------------------------------------
        '    Dim objDanhMuc As New daTraoDoi
        '    objDanhMuc.ExportData()
        'End Sub


        Public Shared Sub ExportData(ByVal pv_lngNgayKS As Long)
            Dim objDanhMuc As New daTraoDoi
            objDanhMuc.ExportData(pv_lngNgayKS)
        End Sub

        Public Shared Function GoiDL(ByVal strTSNCode As String, ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách các gói dữ liệu đang chờ nhận
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objDataPkg As New daTraoDoi
            Try
                Return objDataPkg.GoiDL(strTSNCode, strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Sub SetTN_FLAG(ByVal fblnLogOut As Boolean)
            Dim obTraoDoi As New daTraoDoi
            obTraoDoi.SetTN_FLAG(fblnLogOut)
        End Sub

        Public Shared Function GetTT_FLAG() As String
            Dim obTraoDoi As New daTraoDoi
            Return obTraoDoi.GetTT_FLAG()
        End Function

        Public Shared Function GetTN_FLAG() As String
            Dim obTraoDoi As New daTraoDoi
            Return obTraoDoi.GetTN_FLAG()
        End Function

        Public Shared Function Lcn_CodeValue(ByVal strTsn_Code As String) As String
            Dim obTraoDoi As New daTraoDoi
            Return obTraoDoi.Lcn_CodeValue(strTsn_Code)
        End Function
        Public Shared Sub GetTSTD()
            Dim objTD As New daTraoDoi
            objTD.GetTSTD()
        End Sub
        Public Shared Sub GetPreFix()
            Dim obTraoDoi As New daTraoDoi
            obTraoDoi.GetPreFix()
        End Sub
        Public Shared Function WhereLcn_Recv(ByVal strTSNCode As String) As String
            Dim obTraoDoi As New daTraoDoi
            Return obTraoDoi.WhereLcn_Recv(strTSNCode)
        End Function
        Public Shared Function DKTKhoan(ByVal strTKNO As String, ByVal strTKCo As String, _
                                 ByVal strUser As String, ByVal strChucnang As String) As Boolean
            Dim obj As New daTraoDoi
            Return obj.DKTKhoan(strTKNO, strTKCo, strUser, strChucnang)
        End Function
        Public Shared Function XoaDKTkhoan(ByVal strUser As String, ByVal strChucnang As String) As Boolean
            Dim obj As New daTraoDoi
            Return obj.XoaDKTkhoan(strUser, strChucnang)
        End Function
    End Class

End Namespace