Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace TDDMDC
    Public Class daCapChuong
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Update(ByVal objCapChuong As infCapChuong) As Boolean
            Dim cnCapChuong As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_CAP_CHUONG SET MA_CAP='" & objCapChuong.MA_CAP & "',MA_CHUONG='" & objCapChuong.MA_CHUONG & "',TEN='" & objCapChuong.Ten & "', " & _
                   "NGAY_BD=" & objCapChuong.NGAY_BD & ",NGAY_KT=" & IIf(objCapChuong.NGAY_KT = "", "NULL", objCapChuong.NGAY_KT) & ",GHI_CHU='" & _
                   objCapChuong.GHICHU & "' WHERE CCH_ID=" & objCapChuong.CCH_ID.Trim
            Try
                cnCapChuong = New DataAccess
                cnCapChuong.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi cập nhật cấp chương")
                'LogDebug.WriteLog("Có lỗi xảy ra khi cập nhật cấp - chương: " & ex.ToString, EventLogEntryType.Error)
                blnResult = False
                Throw ex
            Finally
                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objCapChuong As infCapChuong) As Boolean
            Dim cnCapChuong As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_CAP_CHUONG " & _
                    "WHERE CCH_ID=" & objCapChuong.CCH_ID
            Try
                cnCapChuong = New DataAccess
                cnCapChuong.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xoá cơ mục lục cấp - chương")
                blnResult = False
                Throw ex
                'LogDebug.WriteLog("Có lỗi xảy ra khi xoá cơ mục lục cấp - chương: " & ex.ToString, EventLogEntryType.Error)

            Finally
                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strCCH_ID As String) As infCapChuong
            Dim objCapChuong As New infCapChuong
            Dim cnCapChuong As DataAccess
            Dim drCapChuong As IDataReader
            Dim strSQL As String = "Select CCH_ID, MA_CAP, MA_CHUONG, TEN, NGAY_BD, NGAY_KT, GHI_CHU From TCS_DM_CAP_CHUONG " & _
                        "Where CCH_ID=" & strCCH_ID
            Try
                cnCapChuong = New DataAccess
                drCapChuong = cnCapChuong.ExecuteDataReader(strSQL, CommandType.Text)
                If drCapChuong.Read Then
                    objCapChuong = New infCapChuong
                    objCapChuong.CCH_ID = strCCH_ID
                    objCapChuong.MA_CAP = drCapChuong("MA_CAP").ToString()
                    objCapChuong.MA_CHUONG = drCapChuong("MA_CHUONG").ToString()
                    objCapChuong.Ten = drCapChuong("TEN").ToString()
                    objCapChuong.NGAY_BD = drCapChuong("NGAY_BD").ToString()
                    objCapChuong.NGAY_KT = drCapChuong("NGAY_KT").ToString()
                    objCapChuong.GHICHU = drCapChuong("GHI_CHU").ToString()
                End If
                Return objCapChuong
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin cấp chương từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drCapChuong Is Nothing Then drCapChuong.Close()
                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
            End Try
            Return objCapChuong
        End Function

        Public Function Load(ByVal tmpCapChuong As infCapChuong) As infCapChuong
            Dim objCapChuong As New infCapChuong
            Dim cnCapChuong As DataAccess
            Dim drCapChuong As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc
            Dim strSQL As String = "Select CCH_ID, MA_CAP, MA_CHUONG, TEN, NGAY_BD, NGAY_KT, GHI_CHU From TCS_DM_CAP_CHUONG " & _
                "Where " & IIf(tmpCapChuong.CCH_ID <> "", "CCH_ID <> " & tmpCapChuong.CCH_ID & " and ", "") & "MA_CAP='" & tmpCapChuong.MA_CAP & "' and MA_CHUONG='" & tmpCapChuong.MA_CHUONG & "' and (" & _
                "(NGAY_BD <= " & tmpCapChuong.NGAY_BD & " and (NGAY_KT IS NULL or NGAY_KT >= " & tmpCapChuong.NGAY_BD & ")) or " & _
                "(" & IIf(tmpCapChuong.NGAY_KT = "", "NGAY_KT IS NULL or NGAY_KT >= " & tmpCapChuong.NGAY_BD, "NGAY_BD <= " & tmpCapChuong.NGAY_KT & " and (NGAY_KT IS NULL or NGAY_KT >= " & tmpCapChuong.NGAY_KT & ")") & "))"

            Try
                cnCapChuong = New DataAccess
                drCapChuong = cnCapChuong.ExecuteDataReader(strSQL, CommandType.Text)
                If drCapChuong.Read Then
                    objCapChuong = New infCapChuong
                    objCapChuong.CCH_ID = drCapChuong("CCH_ID").ToString()
                    objCapChuong.MA_CAP = drCapChuong("MA_CAP").ToString()
                    objCapChuong.MA_CHUONG = drCapChuong("MA_CHUONG").ToString()
                    objCapChuong.Ten = drCapChuong("TEN").ToString()
                    objCapChuong.NGAY_BD = drCapChuong("NGAY_BD").ToString()
                    objCapChuong.NGAY_KT = drCapChuong("NGAY_KT").ToString()
                    objCapChuong.GHICHU = drCapChuong("GHI_CHU").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not drCapChuong Is Nothing Then drCapChuong.Close()
                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
            End Try
            Return objCapChuong
        End Function

        Public Function Load() As DataSet
            Dim strSQL As String
            Dim cnCapChuong As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "Select CCH_ID, MA_CAP, MA_CHUONG, TEN, " & _
                        "DECODE(NGAY_BD, Null,'',SUBSTR(NGAY_BD, 7, 2) || '/' || SUBSTR (NGAY_BD, 5, 2) || '/' || SUBSTR (NGAY_BD, 1, 4)) NGAY_BD, " & _
                        "DECODE(NGAY_KT, Null,'',SUBSTR(NGAY_KT, 7, 2) || '/' || SUBSTR (NGAY_KT, 5, 2) || '/' || SUBSTR (NGAY_KT, 1, 4)) NGAY_KT, " & _
                        "GHI_CHU from " & _
                        "TCS_DM_CAP_CHUONG Order by MA_CAP,MA_CHUONG"
                cnCapChuong = New DataAccess
                dsCapChuong = cnCapChuong.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL")
                'LogDebug.WriteLog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                cnCapChuong.Dispose()
            End Try

            Return dsCapChuong
        End Function

        Public Function CheckExist(ByVal objCapChuong As infCapChuong) As Boolean
            Return Load(objCapChuong).CCH_ID <> ""
        End Function

        Public Function CheckExist(ByVal strCCH_ID As String) As Boolean
            Return Load(strCCH_ID).CCH_ID <> ""
        End Function
    End Class

End Namespace
