Imports VBOracleLib

Namespace ChungTu
    Public Class daNhanChungTu
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function LoadDataHeader(ByVal strWhere As String) As Object
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            'Dim strSQL As String = "Select HDR.KYHIEU_CT,HDR.SO_CT,HDR.MA_DTNT," & _
            '        "HDR.TEN_DTNT,HDR.NGAY_NOP,HDR.TONGTIEN,HDR.GNT_HID " & _
            '        "From TCS_CTU_HDR HDR, TCS_CTU_DTL DTL " & _
            '        "Where (HDR.GNT_HID = DTL.GNT_HID) And " & strWhere & " " & _
            '        "Group By HDR.KYHIEU_CT,HDR.SO_CT,HDR.MA_DTNT," & _
            '            "HDR.TEN_DTNT,HDR.NGAY_NOP,HDR.TONGTIEN,HDR.GNT_HID " & _
            '        "Order By HDR.KYHIEU_CT,HDR.SO_CT"
            Dim strSQL As String = "SELECT   hdr.kyhieu_ct, hdr.so_ct, hdr.ma_nnthue, hdr.ten_nnthue, hdr.ngay_ct, " & _
                    "SUM (dtl.sotien) AS tongtien, hdr.shkb " & _
                "FROM tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " & _
                "WHERE(hdr.shkb = dtl.shkb) " & _
                    "AND (hdr.ngay_kb = dtl.ngay_kb) " & _
                    "AND (hdr.ma_nv = dtl.ma_nv) " & _
                    "AND (hdr.so_bt = dtl.so_bt) " & _
                    "AND (hdr.ma_dthu = dtl.ma_dthu) AND " & strWhere & _
                "GROUP BY hdr.kyhieu_ct, " & _
                    "hdr.so_ct, " & _
                    "hdr.ma_nnthue, " & _
                    "hdr.ten_nnthue, " & _
                    "hdr.ngay_ct, " & _
                    "hdr.shkb, " & _
                    "hdr.ngay_kb, " & _
                    "hdr.ma_nv, " & _
                    "hdr.so_bt, " & _
                    "hdr.ma_dthu"

            Try
                cnChungTu = New DataAccess
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy danh sách header của chứng từ")
                Throw ex
                'LogDebug.WriteLog("Có lỗi sảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try

            Return dsChungTu
        End Function

        Public Function LoadDataDetail(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ---------------------------------------------------- 
            Dim cnChungTu As DataAccess
            Dim dsChungTu As DataSet
            Try
                cnChungTu = New DataAccess
                Dim strSQL As String
                strSQL = "select b.* " & _
                    "from TCS_CTU_HDR a, TCS_CTU_DTL b " & _
                    "where (a.shkb = b.shkb) and (a.ngay_kb = b.ngay_kb) and (a.ma_nv = b.ma_nv) and (a.so_bt = b.so_bt) and (a.ma_dthu = b.ma_dthu) " & _
                    "and (a.KYHIEU_CT = '" & strKYHIEU_CT & "') " & _
                    "and (a.SO_CT = '" & strSO_CT & "') "
                dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin chi tiết của chứng từ")
                'LogDebug.WriteLog("Có lỗi sảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
            End Try

            Return dsChungTu
        End Function
    End Class
End Namespace
