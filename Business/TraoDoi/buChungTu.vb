Imports VBOracleLib
Imports Business.Common
Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Imports Business.NewChungTu


Public Class buChungTu
    Public Shared Function CapNhatPKT(ByVal strSoPKT As String, ByVal Update As Boolean) As Boolean
        Dim daPKT As New daChungTu
        Return daPKT.CapNhatPKT(strSoPKT, Update)
    End Function
    Public Shared Function InsertPKTTemp(ByVal strWhere As String, ByVal Update As Boolean) As Boolean
        Dim daPKT As New daChungTu
        Return daPKT.InsertPKTTemp(strWhere, Update)
    End Function
    Public Shared Function LoadDSCT(ByVal strSoPKT As String, ByVal lngNgayKB As Long, ByVal strMaDT As String) As DataSet
        Dim daCT As New daChungTu
        Return daCT.LoadDSCT(strSoPKT, lngNgayKB, strMaDT)
    End Function

    'Public Shared Function CTU_Get_LanIn(ByVal key As CTuKey) As String
    '    Try
    '        Dim daCT As New daChungTu
    '        Return daCT.CTU_Get_LanIn(key)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function


    Public Shared Function CTU_Get_LanIn(ByVal key As KeyCTu) As String
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_Get_LanIn(key)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function CTU_Header_Set(ByVal key As KeyCTu) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_Header_Set(key)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function CTU_BL_Header(ByVal soCT As String) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.CTu_BLanh_Header(soCT)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Shared Function CTU_ChiTiet_Set(ByVal key As CTuKey) As DataSet
    '    Try
    '        Dim daCT As New daChungTu
    '        Return daCT.CTU_ChiTiet_Set(key)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Public Shared Function CTU_Header(ByVal key As KeyCTu) As NewChungTu.infChungTuHDR
    '    Try
    '        Dim daCT As New daChungTu
    '        Return daCT.CTU_Header(key)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Public Shared Function CTU_Header(ByVal key As KeyCTu) As infChungTuHDR
    '    Try
    '        Dim daCT As New daChungTu
    '        Return daCT.CTU_Header(key)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function


    Public Shared Function CTU_Header(ByVal key As KeyCTu) As infChungTuHDR
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_Header(key)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Shared Function CTU_Header(ByVal strKHCT As String, ByVal strSoCT As String) As NewChungTu.infChungTuHDR
    '    Try
    '        Dim daCT As New daChungTu
    '        Return daCT.CTU_Header(strKHCT, strSoCT)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Shared Function CTU_ChiTiet(ByVal key As KeyCTu) As NewChungTu.infChungTuDTL()
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_ChiTiet(key)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Shared Function CTU_ChiTiet(ByVal key As KeyCTu) As infChungTuDTL()
    '    Try
    '        Dim daCT As New daChungTu
    '        Return daCT.CTU_ChiTiet(key)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Shared Function CTU_IN_LASER(ByVal key As KeyCTu) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_IN_LASER(key)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function CTU_IN_LASER_NDTQ(ByVal key As KeyCTu) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_IN_LASER_NDTQ(key)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function CTU_IN_TRUOCBA(ByVal key As KeyCTu) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_IN_TRUOCBA(key)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function CTU_IN_TRUOCBA_NHADAT(ByVal pvStrSoCT As String) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_IN_TRUOCBA_NHADAT(pvStrSoCT)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    'Public Shared Function Update(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), Optional ByVal strSoPKT As String = "") As Boolean
    '    '--------------------------------------------------------------------------------------
    '    ' Ghi vao bảng TCS_CTU_HDR & TCS_CTU_DTL
    '    ' 
    '    '--------------------------------------------------------------------------------------
    '    Dim daCT As New daChungTu
    '    'Cập nhật thông tin chứng từ
    '    daCT.Update(objCTuHDR, objCTuDTL, strSoPKT)
    '    Return True
    'End Function

    Public Shared Function Update(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), Optional ByVal strSoPKT As String = "") As Boolean
        '--------------------------------------------------------------------------------------
        ' Ghi vao bảng TCS_CTU_HDR & TCS_CTU_DTL
        ' 
        '--------------------------------------------------------------------------------------
        Dim daCT As New daChungTu
        'Cập nhật thông tin chứng từ
        daCT.Update(objCTuHDR, objCTuDTL, strSoPKT)
        Return True
    End Function


    Public Shared Function Update_New(ByVal objCTuHDR As NewChungTu.infChungTuHDR, ByVal objCTuDTL As NewChungTu.infChungTuDTL(), Optional ByVal strSoPKT As Boolean = False) As Boolean
        '--------------------------------------------------------------------------------------
        ' Ghi vao bảng TCS_CTU_HDR & TCS_CTU_DTL
        ' 
        '--------------------------------------------------------------------------------------
        Try
            Dim daCT As New daChungTu
            'Cập nhật thông tin chứng từ
            daCT.Update_New(objCTuHDR, objCTuDTL, strSoPKT)
            Return True
        Catch ex As Exception
            Throw ex
        End Try

    End Function

#Region "Format form kết xuất KTKB"
    Public Shared Function LoadCTKBDTL(ByVal strSHKB As String, ByVal intNgay_KB As Integer, _
                                      ByVal strMa_DThu As String, ByVal strTk_No As String, _
                                      ByVal strTk_Co As String, ByVal strDBHC As String, _
                                      ByVal strMa_CQthu As String, ByVal intTT As Integer) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ.
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Nguyễn Hữu Hoan
        ' ----------------------------------------------------------------
        Dim objChungTu As New daChungTu
        Try
            Return objChungTu.LoadCTKBDTL(strSHKB, intNgay_KB, strMa_DThu, strTk_No, strTk_Co, strDBHC, strMa_CQthu, intTT)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function LoadCTKBCTDTL(ByVal strSHKB As String, ByVal intNgay_KB As Integer, _
                                     ByVal strMa_DThu As String, ByVal strMa_NV As String, _
                                     ByVal strSo_BT As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách details
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Nguyễn Hữu Hoan
        ' ----------------------------------------------------------------
        Dim objChungTu As New daChungTu
        Try
            Return objChungTu.LoadCTKBCTDTL(strSHKB, intNgay_KB, strMa_DThu, strMa_NV, strSo_BT)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

    Public Shared Function LoadDataHeaderKXGNT(ByVal strWhere As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ.
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim objChungTu As New daChungTu
        Try
            Return objChungTu.LoadDataHeaderKXGNT(strWhere)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function LoadBKKXCTNVP(ByVal strWhere As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách bảng kê chứng từ.
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim objChungTu As New daChungTu
        Try
            Return objChungTu.LoadBKKXCTNVP(strWhere)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function LoadDataHeaderCQT(ByVal strWhere As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ.
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim objChungTu As New daChungTu
        Try
            Return objChungTu.LoadDataHeaderCQT(strWhere)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function LoadDataHeaderNhanCTNVP(ByVal strTSNCode As String, ByVal strPkgID As String, Optional ByVal strWhereNgay As String = "") As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ.
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim objChungTu As New daChungTu
        Try
            Return objChungTu.LoadDataHeaderNhanCTNVP(strTSNCode, strPkgID, strWhereNgay)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function LoadDataDetailNhanCTNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String, ByVal strTranNo As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ.
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim objChungTu As New daChungTu
        Try
            Return objChungTu.LoadDataDetailNhanCTNVP(strKYHIEU_CT, strSO_CT, strTranNo)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function LoadCTKBCTHDR(ByVal strWhere As String) As DataSet
        Dim objCTKB As New daChungTu
        Try
            Return objCTKB.LoadCTKBCTHDR(strWhere)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function LoadDataDetail(ByVal Key As infChungTuHDR) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ.
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim objChungTu As New daChungTu
        Try
            Return objChungTu.LoadDataDetail(Key)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function LoadDataDetailCQT(ByVal strSHKB As String, ByVal strNgayKB As String, _
                         ByVal strMaNV As String, ByVal strSoBT As String, ByVal strMaDThu As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ.
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim objChungTu As New daChungTu
        Try
            Return objChungTu.LoadDataDetailCQT(strSHKB, strNgayKB, strMaNV, strSoBT, strMaDThu)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function LoadTK() As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ.
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim objTaiKhoan As New daChungTu
        Dim dsTmp As DataSet
        Try
            dsTmp = objTaiKhoan.LoadTK()
        Catch ex As Exception
            'LogDebug.Writelog("Có lỗi xảy ra khi thông tin tài khoản để kễt xuất chứng từ: " & ex.ToString)
        End Try
        Return dsTmp
    End Function

    Public Shared Function CTU_HDR_SOBT_KTKB(ByVal lngSoPKT As Long, ByVal lngNgay As Long) As String
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin số bút toán KTKB của chứng từ 
        ' Ngày viết: 21/09/2008
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_HDR_SOBT_KTKB(lngSoPKT, lngNgay)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#Region "Sua chứng từ ngoài VP"
    Public Shared Function LoadDSDThu() As DataSet
        Dim daCT As New daChungTu
        Return daCT.LoadDSDThu()
    End Function

    Public Shared Function CTU_TraCuuNVP(ByVal strWhere As String, ByVal strWhereSoBK As String) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_TraCuuNVP(strWhere, strWhereSoBK)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function CTU_Header_SetNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_Header_SetNVP(strKYHIEU_CT, strSO_CT)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function CTU_ChiTiet_SetNVP(ByVal keyCT As KeyCTu) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.CTU_ChiTiet_SetNVP(keyCT)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function PhieuXN_HeaderNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.PhieuXN_HeaderNVP(strKYHIEU_CT, strSO_CT)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function PhieuXN_ChiTietNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.PhieuXN_ChiTietNVP(strKYHIEU_CT, strSO_CT)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function TCS_BK_CTU_TRACUUNVP(ByVal strSoCTList As String) As DataSet
        Try
            Dim daCT As New daChungTu
            Return daCT.TCS_BK_CTU_TRACUUNVP(strSoCTList)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Sub CTU_DTL_UpdateNVP(ByVal dtl As infChungTuDTL)
        Try
            Dim daCT As New daChungTu
            daCT.CTU_DTL_UpdateNVP(dtl)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class

