﻿'Imports VBOracleLib
'Imports Common
'Imports Business.Common.mdlSystemVariables
'Imports Business.Common.mdlCommon

'Public Module mldCT

'#Region "Biến toàn cục"
'    Public strLTSTTM As String = ""                                ' Chứng từ GNT tiền mặt
'    Public strLTSTCK As String = ",22,23,24,25,26,"                   ' Chứng từ GNT chuyển khoản
'    Public strTTTGTM As String = ",16,17,18,"                      ' Tạm thu tạm giữ tiền mặt
'    Public strTTTGCK As String = ",16,17,18,22,25,26,"                ' Tạm thu tạm giữ chuyển khoản
'    Public strCTTC As String = ",16,17,18,19,"                     ' Chứng từ ghi thu ghi chi
'    Public strCTVATTMCK As String = ",16,17,18,22,25,26,27,28,29," ' Thuế VAT chuyển khoản

'    Public mblnBatBuocMLNS As Boolean = True        'Ghi nhận việc có bắt buộc nhập MLNS hay không?
'    Public mblnNhapThucThu As Boolean = True        'Cho phép nhập số thực thu
'    Public mblnTLDT As Boolean = True               'Bắt buộc nhập Tỷ Lệ Điều Tiết
'    Private mstrCurrentNT As String                 'Lưu mã nguyên tệ hiện thời

'    Public mdblMaxTien As Double = 999999999999 '12

'    Public Enum NguonNNT As Byte
'        Khac = 0
'        NNThue_CTN = 1
'        NNThue_VL = 2
'        TruocBa = 3
'        HaiQuan = 4
'        SoThue_CTN = 5
'        VangLai = 6
'        Loi = 7
'    End Enum
'    'Public Enum RowHeader As Byte
'    '    NgayLV = 0
'    '    SoCT = 1
'    '    MaNNT = 2
'    '    TenNNT = 2
'    '    DChiNNT = 3
'    '    MaNNTien = 4
'    '    TenNNTien = 4
'    '    DChiNNTien = 5
'    '    MaDBHC = 6
'    '    TenDBHC = 6
'    '    TKNo = 7
'    '    TKCo = 8
'    '    MaCQThu = 9
'    '    TenCQThu = 9
'    '    LoaiThue = 10
'    '    MaNT = 11
'    '    TiGia = 12
'    '    ToKhaiSo = 13
'    '    NgayDK = 14
'    '    LHXNK = 15
'    '    SoQD = 16
'    '    NgayBHQD = 17
'    '    CQBH = 18
'    '    MaDVSDNS = 19
'    '    SoKhung = 20
'    '    SoMay = 21
'    '    SoCTNH = 22
'    '    TK_KH_NH = 23
'    '    NGAY_KH_NH = 24
'    '    MA_NH_A = 25
'    '    MA_NH_B = 26
'    '    TKKHNhan = 27
'    '    TenKHNhan = 28
'    '    DCKHNhan = 29
'    'End Enum
'#End Region

'#Region "Xử lý xâu"
'    Public Function GetTTTrangThai(ByVal strMaTT As String, ByVal strLanIn As String) As Integer
'        '------------------------------------------------------------------
'        ' Mục đích: Lấy thứ tự của trạng thái chứng từ khi đã biết mã (dùng hiển thị ảnh)
'        ' Tham số: 
'        ' Giá trị trả về: 
'        ' Ngày viết: 14/11/2007
'        ' Người viết: Lê Hồng Hà
'        ' -----------------------------------------------------------------
'        Select Case strMaTT
'            Case "00"   'Chưa kiểm soát
'                Return 0
'            Case "01"   'Đã kiểm soát
'                Return 1
'            Case "03"   'Kiểm soát lỗi
'                Return 3
'            Case "04"   'Đã hủy
'                Return 4
'            Case "05"   'Đã chuyển sang KTKB
'                Return 2
'        End Select
'    End Function
'    Public Function GetKyThue(ByVal strNgayLV As String) As String
'        '------------------------------------------------------------------
'        ' Mục đích: Đưa từ dạng ngày yyyyMMdd -> dạng 'MM/yyyy'
'        ' Tham số: 
'        ' Giá trị trả về: 
'        ' Ngày viết: 07/11/2007
'        ' Người viết: Lê Hồng Hà
'        ' -----------------------------------------------------------------
'        Try
'            If (strNgayLV.Length <> 8) Then Return "" 'Không đúng định dạng đầu vào

'            Dim strThang, strNam As String
'            strNam = strNgayLV.Substring(0, 4)
'            strThang = strNgayLV.Substring(4, 2)

'            Return (strThang & "/" & strNam)
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function
'    Public Function ChuanHoa_KyThue(ByVal strKyThue As String) As String
'        '-------------------------------------------------------------------------------------
'        ' Mục đích: Chuẩn hóa thông tin kỳ thuế trong trước bạ
'        ' Người viết: Lê Hồng Hà
'        ' Ngày viết: 07/11/2007
'        '--------------------------------------------------------------------------------------
'        Try
'            strKyThue = Trim(strKyThue)
'            If (Valid_KyThue(strKyThue)) Then Return strKyThue

'            Dim intYear As Integer = Convert.ToInt16(strKyThue)
'            Return ("01/" & strKyThue)
'        Catch ex As Exception
'            Return GetKyThue(gdtmNgayLV)
'        End Try
'    End Function
'#End Region

'#Region "Valid _DTL"
'    Public Function Valid_DTL(ByVal dtl As Common.infChungTuDTL, _
'                ByVal fblnBatBuocMLNS As Boolean, _
'                ByVal fblnVND As Boolean, _
'                ByVal fblnTLDT As Boolean) As Integer
'        Try
'            If (fblnBatBuocMLNS = True) Then
'                If (Not Valid_CCh(dtl.Ma_Cap, dtl.Ma_Chuong)) Then
'                    'MessageBox.Show("Thông tin Cấp-Chương không hợp lệ", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                    Return 0
'                End If
'                If (Not Valid_LK(dtl.Ma_Loai, dtl.Ma_Khoan)) Then
'                    'MessageBox.Show("Thông tin Loại-Khoản không hợp lệ", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                    Return 2
'                End If
'                If (Not Valid_MTM(dtl.Ma_Muc, dtl.Ma_TMuc)) Then
'                    'MessageBox.Show("Thông tin Mục-Tiểu Mục không hợp lệ", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                    Return 4
'                End If
'                If ((Not Valid_TLDT(dtl.Ma_TLDT)) And (fblnTLDT = True)) Then
'                    'MessageBox.Show("Thông tin Tỷ lệ điều tiết không hợp lệ", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                    Return 6
'                End If
'            End If

'            If (dtl.SoTien = 0) Then
'                'MessageBox.Show("Số tiền không được để 0", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                If (fblnVND) Then
'                    Return 9
'                Else
'                    Return 8
'                End If
'            End If
'            If (Not Valid_KyThue(dtl.Ky_Thue)) Then
'                'MessageBox.Show("Thông tin Kỳ thuế không hợp lệ (MM/yyyy)", "Chú ý", Windows.Forms.'MessageBoxButtons.OK, Windows.Forms.'MessageBoxIcon.Error)
'                Return 10
'            End If
'            Return -1
'        Catch ex As Exception
'            Return -1
'        End Try
'    End Function
'    Public Function Valid_DTL(ByVal dtl() As Common.infChungTuDTL, _
'            ByVal fblnBatBuocMLNS As Boolean, _
'            ByVal fblnVND As Boolean) As Integer()
'        Dim intReturn(1) As Integer
'        Try
'            Dim i, iCol As Integer
'            For i = 0 To dtl.Length - 1
'                iCol = Valid_DTL(dtl(i), fblnBatBuocMLNS, fblnVND, mblnTLDT)
'                If (iCol <> -1) Then
'                    intReturn(0) = i
'                    intReturn(1) = iCol
'                    Return intReturn
'                End If
'            Next
'            Return Nothing
'        Catch ex As Exception
'            Return Nothing
'        End Try
'    End Function
'#End Region

'#Region "Others"
'    Public Function GetTongTien(ByVal ds As DataSet, ByVal strFieldName As String) As Double
'        '-------------------------------------------------------------------------------------
'        ' Mục đích: Tính tổng tiền
'        ' Người viết: Lê Hồng Hà
'        ' Ngày viết: 07/11/2007
'        '--------------------------------------------------------------------------------------

'        Try
'            If (IsEmptyDataSet(ds)) Then Return 0

'            Dim dblTongTien, dblTien As Double
'            Dim intCount As Integer = ds.Tables(0).Rows.Count

'            Dim i As Integer
'            For i = 0 To intCount - 1
'                Try
'                    dblTien = Convert.ToDouble(ds.Tables(0).Rows(i).Item(strFieldName).ToString())
'                Catch ex As Exception
'                    dblTien = 0
'                End Try
'                dblTongTien += dblTien
'            Next
'            Return dblTongTien
'        Catch ex As Exception
'            Return 0
'        End Try
'    End Function
'    'Public Function GetGridIndex(ByVal grdDSCT As C1.Win.C1FlexGrid.C1FlexGrid, _
'    '    ByVal strKHCT As String, ByVal strSoCT As String) As Integer
'    '    '-------------------------------------------------------------------------------------
'    '    ' Mục đích: Lấy vị trí chứng từ trên grid
'    '    ' Người viết: Lê Hồng Hà
'    '    ' Ngày viết: 07/11/2007
'    '    '--------------------------------------------------------------------------------------
'    '    Dim i, intRowCount, intColCount As Integer
'    '    intRowCount = grdDSCT.Rows.Count
'    '    intColCount = grdDSCT.Cols.Count

'    '    If (intColCount < 5) Then Return 0
'    '    If (intRowCount < 2) Then Return 0
'    '    If ((Trim(strKHCT) = "") Or (Trim(strSoCT) = "")) Then Return 1

'    '    For i = 1 To intRowCount - 1
'    '        If ((Trim(grdDSCT(i, 3)) = strKHCT) And (Trim(grdDSCT(i, 1)) = strSoCT)) Then
'    '            Return i
'    '        End If
'    '    Next
'    '    Return 1
'    'End Function

'    Public Function ConvertKey(ByVal intKey As Integer) As Char
'        Try
'            Dim intKeyReturn As Integer = 0

'            Select Case intKey
'                Case 96 To 105
'                    intKeyReturn = intKey - 48
'                Case Else
'                    intKeyReturn = intKey
'            End Select

'            Return Chr(intKeyReturn)
'        Catch ex As Exception
'            Return ""
'        End Try
'    End Function
'#End Region

'End Module
