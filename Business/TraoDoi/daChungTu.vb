﻿Imports VBOracleLib
Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Imports Business.Common
Imports Business.TraoDoi
Imports Business.NewChungTu

Public Class daChungTu
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "Chi tiết chứng từ"
    'Public Function CTU_Header(ByVal strKHCT As String, ByVal strSoCT As String) As infChungTuHDR
    '    '-----------------------------------------------------
    '    ' Mục đích: Lấy thông tin chung của chứng từ
    '    ' Tham số: Ký hiệu chứng từ, số chứng từ
    '    ' Giá trị trả về:
    '    ' Ngày viết: 26/10/2007
    '    ' Người viết: Lê Hồng Hà
    '    ' ----------------------------------------------------  
    '    Dim cnCT As DataAccess
    '    Dim hdr As New infChungTuHDR
    '    Try
    '        cnCT = New DataAccess
    '        Dim strSQL As String
    '        strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV," & _
    '            "a.So_BT,a.Ma_DThu,a.So_BThu," & _
    '            "a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
    '            "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
    '            "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
    '            "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
    '            "a.Ngay_QD,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
    '            "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
    '            "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
    '            "a.So_TK,a.Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
    '            "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
    '            "a.So_Khung,a.So_May,a.TK_KH_NH,a.NGAY_KH_NH," & _
    '            "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
    '            "a.Lan_In,a.Trang_Thai, " & _
    '            "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, a.So_BT_KTKB" & _
    '            " from TCS_CTU_THOP_HDR a " & _
    '            "where (a.kyhieu_ct = '" & strKHCT & "') " & _
    '            "and (a.so_ct = '" & strSoCT & "') "

    '        Dim dsCT As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

    '        ' Kiểm tra không có dữ liệu -> Thoát
    '        If (IsEmptyDataSet(dsCT)) Then Return hdr

    '        'Có dữ liệu -> đưa vào đối tượng "hdr"
    '        hdr.CQ_QD = dsCT.Tables(0).Rows(0).Item("CQ_QD").ToString()
    '        hdr.DC_NNThue = dsCT.Tables(0).Rows(0).Item("DC_NNTHUE").ToString()
    '        hdr.DC_NNTien = dsCT.Tables(0).Rows(0).Item("DC_NNTIEN").ToString()
    '        hdr.Diachi_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Diachi_KH_Nhan").ToString()
    '        hdr.DVSDNS = dsCT.Tables(0).Rows(0).Item("DVSDNS").ToString()
    '        hdr.KyHieu_CT = dsCT.Tables(0).Rows(0).Item("KYHIEU_CT").ToString()
    '        hdr.Lan_In = dsCT.Tables(0).Rows(0).Item("LAN_IN").ToString()
    '        hdr.LH_XNK = dsCT.Tables(0).Rows(0).Item("LH_XNK").ToString()
    '        hdr.Ly_Do = dsCT.Tables(0).Rows(0).Item("LY_DO").ToString()
    '        hdr.Ma_CQThu = dsCT.Tables(0).Rows(0).Item("MA_CQTHU").ToString()
    '        hdr.Ma_DThu = dsCT.Tables(0).Rows(0).Item("MA_DTHU").ToString()
    '        hdr.Ma_Huyen = dsCT.Tables(0).Rows(0).Item("MA_HUYEN").ToString()
    '        hdr.Ma_KS = dsCT.Tables(0).Rows(0).Item("MA_KS").ToString()
    '        hdr.Ma_LThue = dsCT.Tables(0).Rows(0).Item("MA_LTHUE").ToString()
    '        hdr.MA_NH_A = dsCT.Tables(0).Rows(0).Item("MA_NH_A").ToString()
    '        hdr.MA_NH_B = dsCT.Tables(0).Rows(0).Item("MA_NH_B").ToString()
    '        hdr.Ma_NNThue = dsCT.Tables(0).Rows(0).Item("MA_NNTHUE").ToString()
    '        hdr.Ma_NNTien = dsCT.Tables(0).Rows(0).Item("MA_NNTIEN").ToString()
    '        hdr.Ma_NT = dsCT.Tables(0).Rows(0).Item("MA_NT").ToString()
    '        hdr.Ma_NV = dsCT.Tables(0).Rows(0).Item("MA_NV").ToString()
    '        hdr.Ma_Tinh = dsCT.Tables(0).Rows(0).Item("MA_TINH").ToString()
    '        hdr.Ma_TQ = dsCT.Tables(0).Rows(0).Item("MA_TQ").ToString()
    '        hdr.Ma_Xa = dsCT.Tables(0).Rows(0).Item("MA_XA").ToString()
    '        hdr.Ngay_CT = dsCT.Tables(0).Rows(0).Item("NGAY_CT").ToString()
    '        hdr.Ngay_HT = dsCT.Tables(0).Rows(0).Item("NGAY_HT").ToString()
    '        hdr.Ngay_KB = dsCT.Tables(0).Rows(0).Item("NGAY_KB").ToString()
    '        hdr.NGAY_KH_NH = dsCT.Tables(0).Rows(0).Item("NGAY_KH_NH").ToString()
    '        hdr.Ngay_QD = dsCT.Tables(0).Rows(0).Item("NGAY_QD").ToString()
    '        hdr.Ngay_TK = dsCT.Tables(0).Rows(0).Item("NGAY_TK").ToString()
    '        hdr.SHKB = dsCT.Tables(0).Rows(0).Item("SHKB").ToString()
    '        hdr.So_BT = dsCT.Tables(0).Rows(0).Item("SO_BT").ToString()
    '        hdr.So_BThu = dsCT.Tables(0).Rows(0).Item("SO_BTHU").ToString()
    '        hdr.So_CT = dsCT.Tables(0).Rows(0).Item("SO_CT").ToString()
    '        hdr.So_CT_NH = dsCT.Tables(0).Rows(0).Item("SO_CT_NH").ToString()
    '        hdr.So_Khung = dsCT.Tables(0).Rows(0).Item("SO_KHUNG").ToString()
    '        hdr.So_May = dsCT.Tables(0).Rows(0).Item("SO_MAY").ToString()
    '        hdr.So_QD = dsCT.Tables(0).Rows(0).Item("SO_QD").ToString()
    '        hdr.So_TK = dsCT.Tables(0).Rows(0).Item("SO_TK").ToString()
    '        hdr.Ten_DVSDNS = dsCT.Tables(0).Rows(0).Item("Ten_DVSDNS").ToString()
    '        hdr.Ten_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Ten_KH_Nhan").ToString()
    '        hdr.Ten_NNThue = dsCT.Tables(0).Rows(0).Item("TEN_NNTHUE").ToString()
    '        hdr.Ten_NNTien = dsCT.Tables(0).Rows(0).Item("TEN_NNTIEN").ToString()
    '        hdr.TG_ID = dsCT.Tables(0).Rows(0).Item("TG_ID").ToString()
    '        hdr.TK_Co = dsCT.Tables(0).Rows(0).Item("TK_Co").ToString()
    '        hdr.TK_KH_NH = dsCT.Tables(0).Rows(0).Item("TK_KH_NH").ToString()
    '        hdr.TK_KH_Nhan = dsCT.Tables(0).Rows(0).Item("TK_KH_Nhan").ToString()
    '        hdr.TK_No = dsCT.Tables(0).Rows(0).Item("TK_No").ToString()
    '        hdr.Trang_Thai = dsCT.Tables(0).Rows(0).Item("TRANG_THAI").ToString()
    '        hdr.TT_TThu = dsCT.Tables(0).Rows(0).Item("TT_TTHU").ToString()
    '        hdr.TTien = dsCT.Tables(0).Rows(0).Item("TTIEN").ToString()
    '        hdr.TTien_NT = dsCT.Tables(0).Rows(0).Item("TTIEN_NT").ToString()
    '        hdr.Ty_Gia = dsCT.Tables(0).Rows(0).Item("TY_GIA").ToString()
    '        hdr.XA_ID = dsCT.Tables(0).Rows(0).Item("XA_ID").ToString()
    '        hdr.So_BT_KTKB = dsCT.Tables(0).Rows(0).Item("So_BT_KTKB").ToString()
    '        Return hdr
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
    '    End Try
    'End Function


    Public Function CTU_Header(ByVal key As KeyCTu) As NewChungTu.infChungTuHDR
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chung của chứng từ
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Dim hdr As New NewChungTu.infChungTuHDR
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            'Kienvt : Lay bang TCS_CTU_THOP_HDR
            strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV," & _
                "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                "a.KyHieu_CT,a.So_CT,a.So_CT_NH,a.seq_no," & _
                "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
                "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
                "a.Ngay_QD,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
                "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
                "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
                "a.So_TK,a.Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                "a.So_Khung,a.So_May,a.TK_KH_NH,a.NGAY_KH_NH,a.ten_kh_nh," & _
                "ts.name MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu,a.TK_KB_NH, " & _
                "a.Lan_In,a.Trang_Thai,a.ISACTIVE, a.so_bk, a.ngay_bk, a.tkht,A.ten_nh_b ten_nh_b, a.MA_NTK," & _
                "a.TK_KH_Nhan,a.MA_HQ, c.TEN AS TEN_HQ, b.TEN_LH AS TEN_LHXNK, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, a.phi_gd, a.phi_vat, a.So_BT_KTKB,a.PT_TT,a.rm_ref_no,a.ref_no,a.QUAN_HUYENNNTIEN,a.TINH_TPNNTIEN, a.MAHUYEN_NNT, a.TENHUYEN_NNT, a.MATINH_NNT, a.TENTINH_NNT, a.TENHUYEN_NNTHAY, a.TENTINH_NNTHAY,a.ma_cn,a.PT_TT  " & _
                " from TCS_CTU_HDR a, TCS_DM_LHINH b, (select MA_HQ, TEN FROM TCS_DM_CQTHU WHERE MA_HQ IS NOT NULL) c," & _
                "(SELECT a.name, a.branch_id,b.ma_nv FROM tcs_dm_chinhanh a,tcs_dm_nhanvien b where b.ma_cn=a.id) TS " & _
                "where (a.ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                "and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _
                "and (a.so_bt = " & key.So_BT.ToString() & ") " & _
                "and (a.so_ct = '" & key.So_CT.ToString() & "') " & _
                "AND a.Ma_NV=ts.ma_nv AND (a.MA_HQ = c.MA_HQ(+)) AND (a.LH_XNK = b.MA_LH(+)) "
            Dim dsCT As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

            ' Kiểm tra không có dữ liệu -> Thoát
            If (IsEmptyDataSet(dsCT)) Then Return hdr

            'Có dữ liệu -> đưa vào đối tượng "hdr"
            hdr.CQ_QD = dsCT.Tables(0).Rows(0).Item("CQ_QD").ToString()
            hdr.DC_NNThue = dsCT.Tables(0).Rows(0).Item("DC_NNTHUE").ToString()
            hdr.DC_NNTien = dsCT.Tables(0).Rows(0).Item("DC_NNTIEN").ToString()
            hdr.Diachi_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Diachi_KH_Nhan").ToString()
            hdr.DVSDNS = dsCT.Tables(0).Rows(0).Item("DVSDNS").ToString()
            hdr.KyHieu_CT = dsCT.Tables(0).Rows(0).Item("KYHIEU_CT").ToString()
            hdr.Lan_In = dsCT.Tables(0).Rows(0).Item("LAN_IN").ToString()
            hdr.LH_XNK = dsCT.Tables(0).Rows(0).Item("LH_XNK").ToString()
            hdr.Ly_Do = dsCT.Tables(0).Rows(0).Item("LY_DO").ToString()
            hdr.Ma_CQThu = dsCT.Tables(0).Rows(0).Item("MA_CQTHU").ToString()
            hdr.Ma_DThu = dsCT.Tables(0).Rows(0).Item("MA_DTHU").ToString()
            hdr.Ma_Huyen = dsCT.Tables(0).Rows(0).Item("MA_HUYEN").ToString()
            hdr.Ma_KS = dsCT.Tables(0).Rows(0).Item("MA_KS").ToString()
            hdr.Ma_LThue = dsCT.Tables(0).Rows(0).Item("MA_LTHUE").ToString()
            hdr.MA_NH_A = dsCT.Tables(0).Rows(0).Item("MA_NH_A").ToString()
            hdr.MA_NH_B = dsCT.Tables(0).Rows(0).Item("MA_NH_B").ToString()

            hdr.Ma_NNThue = dsCT.Tables(0).Rows(0).Item("MA_NNTHUE").ToString()
            hdr.Ma_NNTien = dsCT.Tables(0).Rows(0).Item("MA_NNTIEN").ToString()
            hdr.Ma_NT = dsCT.Tables(0).Rows(0).Item("MA_NT").ToString()
            hdr.Ma_NV = dsCT.Tables(0).Rows(0).Item("MA_NV").ToString()
            hdr.Ma_Tinh = dsCT.Tables(0).Rows(0).Item("MA_TINH").ToString()
            hdr.Ma_TQ = dsCT.Tables(0).Rows(0).Item("MA_TQ").ToString()
            hdr.Ma_Xa = dsCT.Tables(0).Rows(0).Item("MA_XA").ToString()
            hdr.Ngay_CT = dsCT.Tables(0).Rows(0).Item("NGAY_CT").ToString()
            hdr.Ngay_HT = dsCT.Tables(0).Rows(0).Item("NGAY_HT").ToString()
            hdr.Ngay_KB = dsCT.Tables(0).Rows(0).Item("NGAY_KB").ToString()
            hdr.NGAY_KH_NH = dsCT.Tables(0).Rows(0).Item("NGAY_KH_NH").ToString()
            hdr.Ngay_QD = dsCT.Tables(0).Rows(0).Item("NGAY_QD").ToString()
            hdr.Ngay_TK = dsCT.Tables(0).Rows(0).Item("NGAY_TK").ToString()
            hdr.SHKB = dsCT.Tables(0).Rows(0).Item("SHKB").ToString()
            hdr.So_BT = dsCT.Tables(0).Rows(0).Item("SO_BT").ToString()
            hdr.So_BThu = dsCT.Tables(0).Rows(0).Item("SO_BTHU").ToString()
            hdr.So_CT = dsCT.Tables(0).Rows(0).Item("SO_CT").ToString()
            hdr.So_CT_NH = dsCT.Tables(0).Rows(0).Item("SO_CT_NH").ToString()
            hdr.So_Khung = dsCT.Tables(0).Rows(0).Item("SO_KHUNG").ToString()
            hdr.So_May = dsCT.Tables(0).Rows(0).Item("SO_MAY").ToString()
            hdr.So_QD = dsCT.Tables(0).Rows(0).Item("SO_QD").ToString()
            hdr.So_TK = dsCT.Tables(0).Rows(0).Item("SO_TK").ToString()
            hdr.Ten_DVSDNS = dsCT.Tables(0).Rows(0).Item("Ten_DVSDNS").ToString()
            hdr.Ten_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Ten_KH_Nhan").ToString()
            hdr.Ten_NNThue = dsCT.Tables(0).Rows(0).Item("TEN_NNTHUE").ToString()
            hdr.Ten_NNTien = dsCT.Tables(0).Rows(0).Item("TEN_NNTIEN").ToString()
            hdr.TG_ID = dsCT.Tables(0).Rows(0).Item("TG_ID").ToString()
            hdr.TK_Co = dsCT.Tables(0).Rows(0).Item("TK_Co").ToString()
            hdr.TK_KH_NH = dsCT.Tables(0).Rows(0).Item("TK_KH_NH").ToString()
            hdr.TEN_KH_NH = dsCT.Tables(0).Rows(0).Item("ten_kh_nh").ToString()
            hdr.TK_KH_Nhan = dsCT.Tables(0).Rows(0).Item("TK_KH_Nhan").ToString()
            hdr.TK_No = dsCT.Tables(0).Rows(0).Item("TK_No").ToString()
            hdr.Trang_Thai = dsCT.Tables(0).Rows(0).Item("TRANG_THAI").ToString()
            hdr.TT_TThu = dsCT.Tables(0).Rows(0).Item("TT_TTHU").ToString()
            hdr.TTien = dsCT.Tables(0).Rows(0).Item("TTIEN").ToString()
            hdr.TTien_NT = dsCT.Tables(0).Rows(0).Item("TTIEN_NT").ToString()
            hdr.Ty_Gia = dsCT.Tables(0).Rows(0).Item("TY_GIA").ToString()
            hdr.XA_ID = dsCT.Tables(0).Rows(0).Item("XA_ID").ToString()
            hdr.So_BT_KTKB = dsCT.Tables(0).Rows(0).Item("So_BT_KTKB").ToString()
            hdr.PT_TT = dsCT.Tables(0).Rows(0).Item("PT_TT").ToString()
            hdr.TK_KB_NH = dsCT.Tables(0).Rows(0).Item("TK_KB_NH").ToString()
            hdr.PHI_GD = dsCT.Tables(0).Rows(0).Item("phi_gd").ToString()
            hdr.PHI_VAT = dsCT.Tables(0).Rows(0).Item("phi_vat").ToString()
            hdr.So_RM = dsCT.Tables(0).Rows(0).Item("rm_ref_no").ToString()
            hdr.REF_NO = dsCT.Tables(0).Rows(0).Item("ref_no").ToString()
            hdr.QUAN_HUYEN_NNTIEN = dsCT.Tables(0).Rows(0).Item("QUAN_HUYENNNTIEN").ToString()
            hdr.TINH_TPHO_NNTIEN = dsCT.Tables(0).Rows(0).Item("TINH_TPNNTIEN").ToString()
            hdr.So_BK = dsCT.Tables(0).Rows(0).Item("so_bk").ToString()
            hdr.Ngay_BK = dsCT.Tables(0).Rows(0).Item("ngay_bk").ToString()
            hdr.MA_HQ = dsCT.Tables(0).Rows(0).Item("MA_HQ").ToString()
            hdr.TEN_MA_HQ = dsCT.Tables(0).Rows(0).Item("TEN_HQ").ToString()
            hdr.Ten_lhxnk = dsCT.Tables(0).Rows(0).Item("TEN_LHXNK").ToString()
            hdr.TKHT_NH = dsCT.Tables(0).Rows(0).Item("tkht").ToString()
            hdr.Ten_NH_B = dsCT.Tables(0).Rows(0).Item("TEN_NH_B").ToString()
            hdr.MA_NTK = dsCT.Tables(0).Rows(0).Item("MA_NTK").ToString()
            If IsDBNull(dsCT.Tables(0).Rows(0).Item("ISACTIVE")) Then
                hdr.TT_BDS = "0"
            Else
                hdr.TT_BDS = "1"
            End If

            'sonmt
            hdr.MA_HUYEN_NNT = dsCT.Tables(0).Rows(0).Item("MAHUYEN_NNT").ToString()
            hdr.TEN_HUYEN_NNT = dsCT.Tables(0).Rows(0).Item("TENHUYEN_NNT").ToString()
            hdr.MA_TINH_NNT = dsCT.Tables(0).Rows(0).Item("MATINH_NNT").ToString()
            hdr.TEN_TINH_NNT = dsCT.Tables(0).Rows(0).Item("TENTINH_NNT").ToString()
            hdr.HUYEN_NNTHAY = dsCT.Tables(0).Rows(0).Item("TENHUYEN_NNTHAY").ToString()
            hdr.TINH_NNTHAY = dsCT.Tables(0).Rows(0).Item("TENTINH_NNTHAY").ToString()
            hdr.MA_CN = dsCT.Tables(0).Rows(0).Item("ma_cn").ToString()

            hdr.PT_TT = dsCT.Tables(0).Rows(0).Item("PT_TT").ToString()
            hdr.SEQ_NO = dsCT.Tables(0).Rows(0).Item("seq_no").ToString()


            Return hdr
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chung chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Function CTu_BLanh_Header(ByVal strSo_CT As String) As DataSet
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim dsCT As DataSet = Nothing
            Dim strSQL As String
            'strSQL = "SELECT c.name Chi_Nhanh_NH,'' Tru_So_NH,'' DT_NH,'' FAX_NH,'' MST_NH," & _
            '        " a.so_bl SO_BL,a.trang_thai, a.ma_nnthue MST_NNT, a.ten_nnthue TEN_NNT, a.dc_nnthue DC_NNT," & _
            '        " '' DT_NNT,''FAX_NNT, '' SO_DKKD, '' CQ_CAP, '' NGAY_CAP, a.hd_nt SO_HD,  to_char(a.ngay_hd,'dd/MM/yyyy') NGAY_HD," & _
            '        " to_char(a.ngay_tk,'dd/MM/yyyy')ngay_tk, a.lhxnk LH_XNK," & _
            '        " a.so_tk,to_char(a.ngay_batdau,'dd/MM/yyyy') TU_NGAY, to_char(a.ngay_ketthuc,'dd/MM/yyyy') DEN_NGAY," & _
            '        " a.ttien SO_TIEN, a.ma_hq MaHQ, a.ten_hq Ten_HQ, a.ten_hq_ph HQ_PH, a.hoa_don HD, a.vt_don VTD," & _
            '        " a.so_ct,to_char(a.ngay_hdon,'dd/MM/yyyy') NGAY_HDON , to_char(a.NGAY_VTD,'dd/MM/yyyy') NGAY_VTD, a.SONGAY_BL, a. so_ct_tuchinh " & _
            '        " from tcs_baolanh_hdr a, tcs_dm_nhanvien b, tcs_dm_chinhanh c" & _
            '        " where  a.ma_nv= b.ma_nv and b.ma_cn= c.id and a.so_ct='" & strSo_CT & "'"

            strSQL = "SELECT c.name Chi_Nhanh_NH,'' Tru_So_NH,'' DT_NH,'' FAX_NH,'' MST_NH," & _
                    " a.so_bl SO_BL,a.type_bl trang_thai, a.ma_nnthue MST_NNT, a.ten_nnthue TEN_NNT, a.dc_nnthue DC_NNT," & _
                    " a.fone_num DT_NNT,fax_num FAX_NNT, a.SO_DKKD, a.CQ_CAP,to_char(a.ngay_cqcap,'dd/MM/yyyy') NGAY_CAP, decode(a.so_tk, '99999', a.hd_nt, null) SO_HD,  decode(a.so_tk, '99999', to_char(a.ngay_hd,'dd/MM/yyyy'), null) NGAY_HD," & _
                    " to_char(a.ngay_tk,'dd/MM/yyyy')ngay_tk, a.lhxnk LH_XNK," & _
                    " a.so_tk,to_char(a.ngay_batdau,'dd/MM/yyyy') TU_NGAY, to_char(a.ngay_ketthuc,'dd/MM/yyyy') DEN_NGAY," & _
                    " a.ttien SO_TIEN, a.ma_hq MaHQ,a.Ten_HQ, a.ten_hq_ph HQ_PH, a.hoa_don HD, a.vt_don VTD," & _
                    " a.so_ct,to_char(a.ngay_hdon,'dd/MM/yyyy') NGAY_HDON , to_char(a.NGAY_VTD,'dd/MM/yyyy') NGAY_VTD, a.SONGAY_BL, a. so_ct_tuchinh " & _
                    " from tcs_baolanh_hdr a, tcs_dm_nhanvien b, tcs_dm_chinhanh c" & _
                    " where  a.ma_nv= b.ma_nv and b.ma_cn= c.id and a.so_ct='" & strSo_CT & "'"

            dsCT = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Return dsCT
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chung bảo lãnh")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function

    'Public Function CTU_Header(ByVal key As CTuKey) As infChungTuHDR
    '    '-----------------------------------------------------
    '    ' Mục đích: Lấy thông tin chung của chứng từ
    '    ' Tham số: Ký hiệu chứng từ, số chứng từ
    '    ' Giá trị trả về:
    '    ' Ngày viết: 26/10/2007
    '    ' Người viết: Lê Hồng Hà
    '    ' ----------------------------------------------------  
    '    Dim cnCT As DataAccess
    '    Dim hdr As New infChungTuHDR
    '    Try
    '        cnCT = New DataAccess
    '        Dim strSQL As String
    '        strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV," & _
    '            "a.So_BT,a.Ma_DThu,a.So_BThu," & _
    '            "a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
    '            "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
    '            "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
    '            "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
    '            "a.Ngay_QD,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
    '            "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
    '            "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
    '            "a.So_TK,a.Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
    '            "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
    '            "a.So_Khung,a.So_May,a.TK_KH_NH,a.NGAY_KH_NH," & _
    '            "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
    '            "a.Lan_In,a.Trang_Thai, " & _
    '            "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, a.So_BT_KTKB" & _
    '            " from TCS_CTU_THOP_HDR a " & _
    '            "where (a.ngay_kb = " & key.NgayKB.ToString() & ") " & _
    '            "and (a.ma_nv = " & key.MaNV.ToString() & ") " & _
    '            "and (a.so_bt = " & key.SoBT.ToString() & ") " & _
    '            "and (a.shkb = '" & key.SHKB.ToString() & "') " & _
    '            "and (a.ma_dthu = '" & key.MaDT & "') "
    '        Dim dsCT As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

    '        ' Kiểm tra không có dữ liệu -> Thoát
    '        If (IsEmptyDataSet(dsCT)) Then Return hdr

    '        'Có dữ liệu -> đưa vào đối tượng "hdr"
    '        hdr.CQ_QD = dsCT.Tables(0).Rows(0).Item("CQ_QD").ToString()
    '        hdr.DC_NNThue = dsCT.Tables(0).Rows(0).Item("DC_NNTHUE").ToString()
    '        hdr.DC_NNTien = dsCT.Tables(0).Rows(0).Item("DC_NNTIEN").ToString()
    '        hdr.Diachi_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Diachi_KH_Nhan").ToString()
    '        hdr.DVSDNS = dsCT.Tables(0).Rows(0).Item("DVSDNS").ToString()
    '        hdr.KyHieu_CT = dsCT.Tables(0).Rows(0).Item("KYHIEU_CT").ToString()
    '        hdr.Lan_In = dsCT.Tables(0).Rows(0).Item("LAN_IN").ToString()
    '        hdr.LH_XNK = dsCT.Tables(0).Rows(0).Item("LH_XNK").ToString()
    '        hdr.Ly_Do = dsCT.Tables(0).Rows(0).Item("LY_DO").ToString()
    '        hdr.Ma_CQThu = dsCT.Tables(0).Rows(0).Item("MA_CQTHU").ToString()
    '        hdr.Ma_DThu = dsCT.Tables(0).Rows(0).Item("MA_DTHU").ToString()
    '        hdr.Ma_Huyen = dsCT.Tables(0).Rows(0).Item("MA_HUYEN").ToString()
    '        hdr.Ma_KS = dsCT.Tables(0).Rows(0).Item("MA_KS").ToString()
    '        hdr.Ma_LThue = dsCT.Tables(0).Rows(0).Item("MA_LTHUE").ToString()
    '        hdr.MA_NH_A = dsCT.Tables(0).Rows(0).Item("MA_NH_A").ToString()
    '        hdr.MA_NH_B = dsCT.Tables(0).Rows(0).Item("MA_NH_B").ToString()
    '        hdr.Ma_NNThue = dsCT.Tables(0).Rows(0).Item("MA_NNTHUE").ToString()
    '        hdr.Ma_NNTien = dsCT.Tables(0).Rows(0).Item("MA_NNTIEN").ToString()
    '        hdr.Ma_NT = dsCT.Tables(0).Rows(0).Item("MA_NT").ToString()
    '        hdr.Ma_NV = dsCT.Tables(0).Rows(0).Item("MA_NV").ToString()
    '        hdr.Ma_Tinh = dsCT.Tables(0).Rows(0).Item("MA_TINH").ToString()
    '        hdr.Ma_TQ = dsCT.Tables(0).Rows(0).Item("MA_TQ").ToString()
    '        hdr.Ma_Xa = dsCT.Tables(0).Rows(0).Item("MA_XA").ToString()
    '        hdr.Ngay_CT = dsCT.Tables(0).Rows(0).Item("NGAY_CT").ToString()
    '        hdr.Ngay_HT = dsCT.Tables(0).Rows(0).Item("NGAY_HT").ToString()
    '        hdr.Ngay_KB = dsCT.Tables(0).Rows(0).Item("NGAY_KB").ToString()
    '        hdr.NGAY_KH_NH = dsCT.Tables(0).Rows(0).Item("NGAY_KH_NH").ToString()
    '        hdr.Ngay_QD = dsCT.Tables(0).Rows(0).Item("NGAY_QD").ToString()
    '        hdr.Ngay_TK = dsCT.Tables(0).Rows(0).Item("NGAY_TK").ToString()
    '        hdr.SHKB = dsCT.Tables(0).Rows(0).Item("SHKB").ToString()
    '        hdr.So_BT = dsCT.Tables(0).Rows(0).Item("SO_BT").ToString()
    '        hdr.So_BThu = dsCT.Tables(0).Rows(0).Item("SO_BTHU").ToString()
    '        hdr.So_CT = dsCT.Tables(0).Rows(0).Item("SO_CT").ToString()
    '        hdr.So_CT_NH = dsCT.Tables(0).Rows(0).Item("SO_CT_NH").ToString()
    '        hdr.So_Khung = dsCT.Tables(0).Rows(0).Item("SO_KHUNG").ToString()
    '        hdr.So_May = dsCT.Tables(0).Rows(0).Item("SO_MAY").ToString()
    '        hdr.So_QD = dsCT.Tables(0).Rows(0).Item("SO_QD").ToString()
    '        hdr.So_TK = dsCT.Tables(0).Rows(0).Item("SO_TK").ToString()
    '        hdr.Ten_DVSDNS = dsCT.Tables(0).Rows(0).Item("Ten_DVSDNS").ToString()
    '        hdr.Ten_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Ten_KH_Nhan").ToString()
    '        hdr.Ten_NNThue = dsCT.Tables(0).Rows(0).Item("TEN_NNTHUE").ToString()
    '        hdr.Ten_NNTien = dsCT.Tables(0).Rows(0).Item("TEN_NNTIEN").ToString()
    '        hdr.TG_ID = dsCT.Tables(0).Rows(0).Item("TG_ID").ToString()
    '        hdr.TK_Co = dsCT.Tables(0).Rows(0).Item("TK_Co").ToString()
    '        hdr.TK_KH_NH = dsCT.Tables(0).Rows(0).Item("TK_KH_NH").ToString()
    '        hdr.TK_KH_Nhan = dsCT.Tables(0).Rows(0).Item("TK_KH_Nhan").ToString()
    '        hdr.TK_No = dsCT.Tables(0).Rows(0).Item("TK_No").ToString()
    '        hdr.Trang_Thai = dsCT.Tables(0).Rows(0).Item("TRANG_THAI").ToString()
    '        hdr.TT_TThu = dsCT.Tables(0).Rows(0).Item("TT_TTHU").ToString()
    '        hdr.TTien = dsCT.Tables(0).Rows(0).Item("TTIEN").ToString()
    '        hdr.TTien_NT = dsCT.Tables(0).Rows(0).Item("TTIEN_NT").ToString()
    '        hdr.Ty_Gia = dsCT.Tables(0).Rows(0).Item("TY_GIA").ToString()
    '        hdr.XA_ID = dsCT.Tables(0).Rows(0).Item("XA_ID").ToString()
    '        hdr.So_BT_KTKB = dsCT.Tables(0).Rows(0).Item("So_BT_KTKB").ToString()
    '        Return hdr
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
    '    End Try
    'End Function

    'Public Function CTU_Header(ByVal key As KeyCTu) As infChungTuHDR
    '    '-----------------------------------------------------
    '    ' Mục đích: Lấy thông tin chung của chứng từ
    '    ' Tham số: Ký hiệu chứng từ, số chứng từ
    '    ' Giá trị trả về:
    '    ' Ngày viết: 26/10/2007
    '    ' Người viết: Lê Hồng Hà
    '    ' ----------------------------------------------------  
    '    Dim cnCT As DataAccess
    '    Dim hdr As New infChungTuHDR
    '    Try
    '        cnCT = New DataAccess
    '        Dim strSQL As String
    '        strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV," & _
    '            "a.So_BT,a.Ma_DThu,a.So_BThu," & _
    '            "a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
    '            "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
    '            "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
    '            "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
    '            "a.Ngay_QD,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
    '            "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
    '            "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
    '            "a.So_TK,a.Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
    '            "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
    '            "a.So_Khung,a.So_May,a.TK_KH_NH,a.NGAY_KH_NH," & _
    '            "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
    '            "a.Lan_In,a.Trang_Thai, " & _
    '            "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, a.So_BT_KTKB" & _
    '            " from TCS_CTU_THOP_HDR a " & _
    '            "where (a.ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
    '            "and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _
    '            "and (a.so_bt = " & key.So_BT.ToString() & ") " & _
    '            "and (a.shkb = '" & key.SHKB.ToString() & "') " & _
    '            "and (a.ma_dthu = '" & key.Ma_Dthu & "') "
    '        Dim dsCT As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

    '        ' Kiểm tra không có dữ liệu -> Thoát
    '        If (IsEmptyDataSet(dsCT)) Then Return hdr

    '        'Có dữ liệu -> đưa vào đối tượng "hdr"
    '        hdr.CQ_QD = dsCT.Tables(0).Rows(0).Item("CQ_QD").ToString()
    '        hdr.DC_NNThue = dsCT.Tables(0).Rows(0).Item("DC_NNTHUE").ToString()
    '        hdr.DC_NNTien = dsCT.Tables(0).Rows(0).Item("DC_NNTIEN").ToString()
    '        hdr.Diachi_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Diachi_KH_Nhan").ToString()
    '        hdr.DVSDNS = dsCT.Tables(0).Rows(0).Item("DVSDNS").ToString()
    '        hdr.KyHieu_CT = dsCT.Tables(0).Rows(0).Item("KYHIEU_CT").ToString()
    '        hdr.Lan_In = dsCT.Tables(0).Rows(0).Item("LAN_IN").ToString()
    '        hdr.LH_XNK = dsCT.Tables(0).Rows(0).Item("LH_XNK").ToString()
    '        hdr.Ly_Do = dsCT.Tables(0).Rows(0).Item("LY_DO").ToString()
    '        hdr.Ma_CQThu = dsCT.Tables(0).Rows(0).Item("MA_CQTHU").ToString()
    '        hdr.Ma_DThu = dsCT.Tables(0).Rows(0).Item("MA_DTHU").ToString()
    '        hdr.Ma_Huyen = dsCT.Tables(0).Rows(0).Item("MA_HUYEN").ToString()
    '        hdr.Ma_KS = dsCT.Tables(0).Rows(0).Item("MA_KS").ToString()
    '        hdr.Ma_LThue = dsCT.Tables(0).Rows(0).Item("MA_LTHUE").ToString()
    '        hdr.MA_NH_A = dsCT.Tables(0).Rows(0).Item("MA_NH_A").ToString()
    '        hdr.MA_NH_B = dsCT.Tables(0).Rows(0).Item("MA_NH_B").ToString()
    '        hdr.Ma_NNThue = dsCT.Tables(0).Rows(0).Item("MA_NNTHUE").ToString()
    '        hdr.Ma_NNTien = dsCT.Tables(0).Rows(0).Item("MA_NNTIEN").ToString()
    '        hdr.Ma_NT = dsCT.Tables(0).Rows(0).Item("MA_NT").ToString()
    '        hdr.Ma_NV = dsCT.Tables(0).Rows(0).Item("MA_NV").ToString()
    '        hdr.Ma_Tinh = dsCT.Tables(0).Rows(0).Item("MA_TINH").ToString()
    '        hdr.Ma_TQ = dsCT.Tables(0).Rows(0).Item("MA_TQ").ToString()
    '        hdr.Ma_Xa = dsCT.Tables(0).Rows(0).Item("MA_XA").ToString()
    '        hdr.Ngay_CT = dsCT.Tables(0).Rows(0).Item("NGAY_CT").ToString()
    '        hdr.Ngay_HT = dsCT.Tables(0).Rows(0).Item("NGAY_HT").ToString()
    '        hdr.Ngay_KB = dsCT.Tables(0).Rows(0).Item("NGAY_KB").ToString()
    '        hdr.NGAY_KH_NH = dsCT.Tables(0).Rows(0).Item("NGAY_KH_NH").ToString()
    '        hdr.Ngay_QD = dsCT.Tables(0).Rows(0).Item("NGAY_QD").ToString()
    '        hdr.Ngay_TK = dsCT.Tables(0).Rows(0).Item("NGAY_TK").ToString()
    '        hdr.SHKB = dsCT.Tables(0).Rows(0).Item("SHKB").ToString()
    '        hdr.So_BT = dsCT.Tables(0).Rows(0).Item("SO_BT").ToString()
    '        hdr.So_BThu = dsCT.Tables(0).Rows(0).Item("SO_BTHU").ToString()
    '        hdr.So_CT = dsCT.Tables(0).Rows(0).Item("SO_CT").ToString()
    '        hdr.So_CT_NH = dsCT.Tables(0).Rows(0).Item("SO_CT_NH").ToString()
    '        hdr.So_Khung = dsCT.Tables(0).Rows(0).Item("SO_KHUNG").ToString()
    '        hdr.So_May = dsCT.Tables(0).Rows(0).Item("SO_MAY").ToString()
    '        hdr.So_QD = dsCT.Tables(0).Rows(0).Item("SO_QD").ToString()
    '        hdr.So_TK = dsCT.Tables(0).Rows(0).Item("SO_TK").ToString()
    '        hdr.Ten_DVSDNS = dsCT.Tables(0).Rows(0).Item("Ten_DVSDNS").ToString()
    '        hdr.Ten_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Ten_KH_Nhan").ToString()
    '        hdr.Ten_NNThue = dsCT.Tables(0).Rows(0).Item("TEN_NNTHUE").ToString()
    '        hdr.Ten_NNTien = dsCT.Tables(0).Rows(0).Item("TEN_NNTIEN").ToString()
    '        hdr.TG_ID = dsCT.Tables(0).Rows(0).Item("TG_ID").ToString()
    '        hdr.TK_Co = dsCT.Tables(0).Rows(0).Item("TK_Co").ToString()
    '        hdr.TK_KH_NH = dsCT.Tables(0).Rows(0).Item("TK_KH_NH").ToString()
    '        hdr.TK_KH_Nhan = dsCT.Tables(0).Rows(0).Item("TK_KH_Nhan").ToString()
    '        hdr.TK_No = dsCT.Tables(0).Rows(0).Item("TK_No").ToString()
    '        hdr.Trang_Thai = dsCT.Tables(0).Rows(0).Item("TRANG_THAI").ToString()
    '        hdr.TT_TThu = dsCT.Tables(0).Rows(0).Item("TT_TTHU").ToString()
    '        hdr.TTien = dsCT.Tables(0).Rows(0).Item("TTIEN").ToString()
    '        hdr.TTien_NT = dsCT.Tables(0).Rows(0).Item("TTIEN_NT").ToString()
    '        hdr.Ty_Gia = dsCT.Tables(0).Rows(0).Item("TY_GIA").ToString()
    '        hdr.PT_TT = "" 'Kienvt : Khong biet pttt la gi
    '        hdr.XA_ID = dsCT.Tables(0).Rows(0).Item("XA_ID").ToString()
    '        hdr.So_BT_KTKB = dsCT.Tables(0).Rows(0).Item("So_BT_KTKB").ToString()
    '        Return hdr
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
    '    End Try
    'End Function


    Public Function CTU_IN_LASER(ByVal key As KeyCTu) As DataSet
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select rownum as STT,dtl.SO_CT,dtl.noi_dung as NOI_DUNG, (dtl.ma_cap||dtl.ma_chuong) as CC, " & _
                    "dtl.so_tk as LK, (dtl.ma_muc||dtl.ma_tmuc) as MTM, " & _
                    "dtl.ky_thue as KyThue, dtl.sotien as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK,  " & _
                     " (select ma_db from tcs_dm_khobac where shkb=dtl.shkb and ROWNUM = 1) AS ma_dbhc " & _
                    "from tcs_ctu_dtl dtl " & _
                    "where   dtl.so_ct = '" & key.So_CT.ToString() & "'" & _
                    " order by rownum "

            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Function CTU_IN_LASER_NDTQ(ByVal key As KeyCTu) As DataSet
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select rownum as STT,dtl.SO_CT,dtl.noi_dung as NOI_DUNG, (dtl.ma_cap||dtl.ma_chuong) as CC, " & _
                    "dtl.so_tk as LK, (dtl.ma_muc||dtl.ma_tmuc) as MTM, " & _
                    "dtl.ky_thue as KyThue, dtl.sotien as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK,  " & _
                     " hdr.ma_xa || ' - ' || (select ten from tcs_dm_xa where ma_xa in (hdr.ma_xa) and rownum=1) AS ma_dbhc " & _
                    "from tcs_ctu_dtl dtl, tcs_ctu_hdr hdr " & _
                    "where hdr.so_ct = dtl.so_ct and  dtl.so_ct = '" & key.So_CT.ToString() & "'" & _
                    " order by rownum "

            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Function CTU_IN_TRUOCBA(ByVal key As KeyCTu) As DataSet
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select rownum as STT,dtl.SO_CT,(dtl.noi_dung || '\n. Số khung: ' || hdr.so_khung || '\n. Số máy: ' || hdr.so_may || '\n.' || hdr.dac_diem_ptien) as NOI_DUNG, (dtl.ma_cap||dtl.ma_chuong) as CC, " & _
                    "hdr.so_qd as LK, (dtl.ma_muc||dtl.ma_tmuc) as MTM, " & _
                    "dtl.ky_thue as KyThue, dtl.sotien as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK,  " & _
                    " hdr.ma_xa || ' - ' || (select ten from tcs_dm_xa where ma_xa in (hdr.ma_xa) and rownum=1) AS ma_dbhc " & _
                    "from tcs_ctu_dtl dtl, tcs_ctu_hdr hdr " & _
                    "where  hdr.so_ct = dtl.so_ct and  dtl.so_ct = '" & key.So_CT.ToString() & "'" & _
                    " order by rownum "

            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Function CTU_IN_TRUOCBA_NHADAT(ByVal pvStrSoCT As String) As DataSet
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select rownum as STT,dtl.SO_CT,(dtl.noi_dung || '\n. Mã hồ sơ: ' || hdr.ma_hs || '\n. Số thửa đất: ' || hdr.so_thuadat || '\n. Số tờ bản đồ: ' || hdr.so_to_bando || '\n. Mã ĐBHC tài sản: ' || hdr.ma_dbhc_ts) as NOI_DUNG, (dtl.ma_cap||dtl.ma_chuong) as CC, " & _
                    "hdr.so_qd as LK, (dtl.ma_muc||dtl.ma_tmuc) as MTM, " & _
                    "dtl.ky_thue as KyThue, dtl.sotien as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK,  " & _
                     " hdr.ma_xa || ' - ' || (select ten from tcs_dm_xa where ma_xa in (hdr.ma_xa) and rownum=1) AS ma_dbhc " & _
                    "from tcs_ctu_dtl dtl, tcs_ctu_hdr hdr " & _
                    "where  hdr.so_ct = dtl.so_ct and  dtl.so_ct = '" & pvStrSoCT & "'" & _
                    " order by rownum "
            'log.Info("CTU_IN_TRUOCBA - strSQL: " & strSQL)

            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            'log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function

    'Public Function CTU_IN_LASER(ByVal key As KeyCTu) As DataSet
    '    Dim cnCT As DataAccess
    '    Try
    '        cnCT = New DataAccess
    '        Dim strSQL As String
    '        strSQL = "select rownum as STT,dtl.SO_CT,dtl.noi_dung as NOI_DUNG, (dtl.ma_cap||dtl.ma_chuong) as CC, " & _
    '                "dtl.so_tk as LK, (dtl.ma_muc||dtl.ma_tmuc) as MTM, " & _
    '                "dtl.ky_thue as KyThue, dtl.sotien as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK, kb.MA_DB as MA_DBHC " & _
    '                "from tcs_ctu_dtl dtl, tcs_dm_khobac kb " & _
    '                "where  dtl.shkb = kb.shkb  and dtl.so_ct = '" & key.So_CT.ToString() & "'" & _
    '                " order by rownum "

    '        Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '    Catch ex As Exception
    '        CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
    '        'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '        '  & "Error code: System error!" & vbNewLine _
    '        '  & "Error message: " & ex.Message, EventLogEntryType.Error)
    '        Throw ex
    '    Finally
    '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
    '    End Try
    'End Function

    Public Function CTU_ChiTiet(ByVal key As KeyCTu) As NewChungTu.infChungTuDTL()
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chi tiết của chứng từ.
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            'Kienvt : lay tu bang TCS_CTU_THOP_DTL
            'strSQL = "select ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
            '        "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
            '        "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT " & _
            '        "from TCS_CTU_THOP_DTL " & _
            '        "where (ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
            '        "and (ma_nv = " & key.Ma_NV.ToString() & ") " & _
            '        "and (so_bt = " & key.So_BT.ToString() & ") " & _
            '        "and (shkb = '" & key.SHKB.ToString() & "') " & _
            '        "and (ma_dthu = '" & key.Ma_Dthu & "') "
            strSQL = "select ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT " & _
                "from TCS_CTU_DTL " & _
                "where (ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                "and (ma_nv = " & key.Ma_NV.ToString() & ") " & _
                "and (so_bt = " & key.So_BT.ToString() & ") " & _
                "and (shkb = '" & key.SHKB.ToString() & "') " & _
                "and (ma_dthu = '" & key.Ma_Dthu & "') "

            Dim ds As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

            If (IsEmptyDataSet(ds)) Then Return Nothing
            Dim i, intCount As Integer
            intCount = ds.Tables(0).Rows.Count - 1

            Dim dtl(intCount) As NewChungTu.infChungTuDTL

            For i = 0 To intCount
                dtl(i) = New NewChungTu.infChungTuDTL

                dtl(i).CCH_ID = ds.Tables(0).Rows(i).Item("CCH_ID").ToString()
                dtl(i).DT_ID = ds.Tables(0).Rows(i).Item("DT_ID").ToString()
                dtl(i).ID = ds.Tables(0).Rows(i).Item("ID").ToString()
                dtl(i).Ky_Thue = ds.Tables(0).Rows(i).Item("Ky_Thue").ToString()
                dtl(i).LKH_ID = ds.Tables(0).Rows(i).Item("LKH_ID").ToString()
                dtl(i).Ma_Cap = ds.Tables(0).Rows(i).Item("Ma_Cap").ToString()
                dtl(i).Ma_Chuong = ds.Tables(0).Rows(i).Item("Ma_Chuong").ToString()
                dtl(i).Ma_DThu = ds.Tables(0).Rows(i).Item("Ma_DThu").ToString()
                dtl(i).Ma_Khoan = ds.Tables(0).Rows(i).Item("Ma_Khoan").ToString()
                dtl(i).Ma_Loai = ds.Tables(0).Rows(i).Item("Ma_Loai").ToString()
                dtl(i).Ma_Muc = ds.Tables(0).Rows(i).Item("Ma_Muc").ToString()
                dtl(i).Ma_NV = ds.Tables(0).Rows(i).Item("Ma_NV").ToString()
                dtl(i).Ma_TLDT = ds.Tables(0).Rows(i).Item("Ma_TLDT").ToString()
                dtl(i).Ma_TMuc = ds.Tables(0).Rows(i).Item("Ma_TMuc").ToString()
                dtl(i).MTM_ID = ds.Tables(0).Rows(i).Item("MTM_ID").ToString()
                dtl(i).Ngay_KB = ds.Tables(0).Rows(i).Item("Ngay_KB").ToString()
                dtl(i).Noi_Dung = ds.Tables(0).Rows(i).Item("Noi_Dung").ToString()
                dtl(i).SHKB = ds.Tables(0).Rows(i).Item("SHKB").ToString()
                dtl(i).So_BT = ds.Tables(0).Rows(i).Item("So_BT").ToString()
                dtl(i).SoTien = ds.Tables(0).Rows(i).Item("SoTien").ToString()
                dtl(i).SoTien_NT = ds.Tables(0).Rows(i).Item("SoTien_NT").ToString()
            Next

            Return dtl
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function

    'Public Function CTU_ChiTiet(ByVal key As KeyCTu) As infChungTuDTL()
    '    '-----------------------------------------------------
    '    ' Mục đích: Lấy thông tin chi tiết của chứng từ.
    '    ' Tham số: Ký hiệu chứng từ, số chứng từ
    '    ' Giá trị trả về:
    '    ' Ngày viết: 26/10/2007
    '    ' Người viết: Lê Hồng Hà
    '    ' ---------------------------------------------------- 
    '    Dim cnCT As DataAccess
    '    Try
    '        cnCT = New DataAccess
    '        Dim strSQL As String
    '        strSQL = "select ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
    '                "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
    '                "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT " & _
    '                "from TCS_CTU_THOP_DTL " & _
    '                "where (ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
    '                "and (ma_nv = " & key.Ma_NV.ToString() & ") " & _
    '                "and (so_bt = " & key.So_BT.ToString() & ") " & _
    '                "and (shkb = '" & key.SHKB.ToString() & "') " & _
    '                "and (ma_dthu = '" & key.Ma_Dthu & "') "
    '        Dim ds As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

    '        If (IsEmptyDataSet(ds)) Then Return Nothing
    '        Dim i, intCount As Integer
    '        intCount = ds.Tables(0).Rows.Count - 1

    '        Dim dtl(intCount) As infChungTuDTL

    '        For i = 0 To intCount
    '            dtl(i) = New infChungTuDTL

    '            dtl(i).CCH_ID = ds.Tables(0).Rows(i).Item("CCH_ID").ToString()
    '            dtl(i).DT_ID = ds.Tables(0).Rows(i).Item("DT_ID").ToString()
    '            dtl(i).ID = ds.Tables(0).Rows(i).Item("ID").ToString()
    '            dtl(i).Ky_Thue = ds.Tables(0).Rows(i).Item("Ky_Thue").ToString()
    '            dtl(i).LKH_ID = ds.Tables(0).Rows(i).Item("LKH_ID").ToString()
    '            dtl(i).Ma_Cap = ds.Tables(0).Rows(i).Item("Ma_Cap").ToString()
    '            dtl(i).Ma_Chuong = ds.Tables(0).Rows(i).Item("Ma_Chuong").ToString()
    '            dtl(i).Ma_DThu = ds.Tables(0).Rows(i).Item("Ma_DThu").ToString()
    '            dtl(i).Ma_Khoan = ds.Tables(0).Rows(i).Item("Ma_Khoan").ToString()
    '            dtl(i).Ma_Loai = ds.Tables(0).Rows(i).Item("Ma_Loai").ToString()
    '            dtl(i).Ma_Muc = ds.Tables(0).Rows(i).Item("Ma_Muc").ToString()
    '            dtl(i).Ma_NV = ds.Tables(0).Rows(i).Item("Ma_NV").ToString()
    '            dtl(i).Ma_TLDT = ds.Tables(0).Rows(i).Item("Ma_TLDT").ToString()
    '            dtl(i).Ma_TMuc = ds.Tables(0).Rows(i).Item("Ma_TMuc").ToString()
    '            dtl(i).MTM_ID = ds.Tables(0).Rows(i).Item("MTM_ID").ToString()
    '            dtl(i).Ngay_KB = ds.Tables(0).Rows(i).Item("Ngay_KB").ToString()
    '            dtl(i).Noi_Dung = ds.Tables(0).Rows(i).Item("Noi_Dung").ToString()
    '            dtl(i).SHKB = ds.Tables(0).Rows(i).Item("SHKB").ToString()
    '            dtl(i).So_BT = ds.Tables(0).Rows(i).Item("So_BT").ToString()
    '            dtl(i).SoTien = ds.Tables(0).Rows(i).Item("SoTien").ToString()
    '            dtl(i).SoTien_NT = ds.Tables(0).Rows(i).Item("SoTien_NT").ToString()
    '        Next

    '        Return dtl
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
    '    End Try
    'End Function

    Public Function CTU_Header_Set(ByVal key As KeyCTu) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chung của chứng từ     
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV," & _
                           "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                           "a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
                           "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                           "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
                           "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
                           "to_char(a.Ngay_QD,'dd/MM/rrrr') as Ngay_QD ,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
                           "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
                           "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
                           "a.So_TK,to_char(a.Ngay_TK,'dd/MM/rrrr') as Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                           "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                           "a.So_Khung,a.So_May,a.TK_KH_NH,to_char(a.NGAY_KH_NH,'dd/MM/rrrr') as NGAY_KH_NH," & _
                           "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
                           "a.Lan_In,a.Trang_Thai,a.so_bt_ktkb, " & _
                           "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, " & _
                           "nha.ten as TEN_NH_A, nhb.ten as TEN_NH_B, " & _
                           "xa.ten as TenXa, tkno.ten_tk as Ten_TKNo, tkco.ten_tk as Ten_TKCo, " & _
                           "cqthu.ten as Ten_CQThu, nt.ten as Ten_NT, lt.ten as Ten_LThue " & _
                           "from TCS_CTU_HDR a, TCS_DM_XA xa, TCS_DM_TAIKHOAN tkno, TCS_DM_TAIKHOAN tkco, " & _
                           "TCS_DM_CQTHU cqthu, TCS_DM_NGUYENTE nt, TCS_DM_LTHUE lt, TCS_DM_NGANHANG nha, TCS_DM_NGANHANG nhb " & _
                           "where (a.xa_id = xa.xa_id(+))" & _
                           "and (a.tk_no = tkno.tk (+)) and (a.tk_co = tkco.tk(+)) and (a.ma_nh_a = nha.ma(+)) and (a.ma_nh_b = nhb.ma(+)) " & _
                           "and (a.ma_cqthu = cqthu.ma_cqthu(+)) and (a.ma_nt = nt.ma_nt(+)) and (a.ma_lthue = lt.ma_lthue(+)) " & _
                           "and (a.ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                           "and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _
                           "and (a.so_bt = " & key.So_BT.ToString() & ") " & _
                           "and (a.shkb = '" & key.SHKB.ToString() & "') "
            '& _
            ' "and (a.ma_dthu = '" & key.Ma_Dthu & "') "

            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chung chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    'Public Function CTU_ChiTiet_Set(ByVal key As CTuKey) As DataSet
    '    '-----------------------------------------------------
    '    ' Mục đích: Lấy thông tin chi tiết của chứng từ.
    '    ' Tham số: Ký hiệu chứng từ, số chứng từ
    '    ' Giá trị trả về:
    '    ' Ngày viết: 26/10/2007
    '    ' Người viết: Lê Hồng Hà
    '    ' ---------------------------------------------------- 
    '    Dim cnCT As DataAccess
    '    Try
    '        cnCT = New DataAccess
    '        Dim strSQL As String
    '        strSQL = "select ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
    '                "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
    '                "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT " & _
    '                "from TCS_CTU_THOP_DTL " & _
    '                "where (shkb = '" & key.SHKB & "') and (ngay_kb = " & key.NgayKB & ") " & _
    '                "and (ma_nv = " & key.MaNV & ") and (so_bt = " & key.SoBT & ") and (ma_dthu = '" & key.MaDT & "') "
    '        Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
    '    End Try
    'End Function
#End Region

#Region "Sửa chi tiết chứng từ"
    'Public Sub Update(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), Optional ByVal strSoPKT As String = "")
    '    ' ****************************************************************************
    '    ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
    '    ' Đầu vào: đối tượng HDR và mảng DTL
    '    ' Người viết: Lê Hồng Hà
    '    ' Ngày viết: 09/11/2007
    '    '*****************************************************************************
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Dim objDTL As infChungTuDTL
    '    Dim strSql As String
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction

    '        'Cập nhật thông tin HDR của chứng từ
    '        strSql = "Update TCS_CTU_THOP_HDR SET " & _
    '                 "Ma_CQThu='" & objCTuHDR.Ma_CQThu & "'," & _
    '                 "XA_ID=" & objCTuHDR.XA_ID & ",Ma_Tinh='" & objCTuHDR.Ma_Tinh & "',Ma_Huyen='" & objCTuHDR.Ma_Huyen & "'," & _
    '                 "Ma_Xa='" & objCTuHDR.Ma_Xa & "',TK_No='" & objCTuHDR.TK_No & "',TK_Co='" & objCTuHDR.TK_Co & "' " & _
    '                 " Where (SHKB = '" & objCTuHDR.SHKB & "') And (Ngay_KB = " & objCTuHDR.Ngay_KB & _
    '                 ") And (Ma_NV = " & objCTuHDR.Ma_NV & ") And (So_BT = " & objCTuHDR.So_BT & _
    '                 ") And (Ma_DThu = '" & objCTuHDR.Ma_DThu & "')"

    '        ' Insert vào bảng TCS_CT_HDR
    '        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
    '        For Each objDTL In objCTuDTL
    '            strSql = "Update TCS_CTU_THOP_DTL set " & _
    '                     "cch_id = " & objDTL.CCH_ID & "," & _
    '                     "MA_CAP = '" & objDTL.Ma_Cap & "', " & _
    '                     "MA_CHUONG = '" & objDTL.Ma_Chuong & "', " & _
    '                     "lkh_id = " & objDTL.LKH_ID & "," & _
    '                     "MA_LOAI = '" & objDTL.Ma_Loai & "', " & _
    '                     "MA_KHOAN = '" & objDTL.Ma_Khoan & "', " & _
    '                     "mtm_id = " & objDTL.MTM_ID & "," & _
    '                     "MA_MUC = '" & objDTL.Ma_Muc & "', " & _
    '                     "MA_TMUC = '" & objDTL.Ma_TMuc & "', " & _
    '                     "dt_id = " & objDTL.DT_ID & "," & _
    '                     "MA_TLDT = '" & objDTL.Ma_TLDT & "', " & _
    '                     "NOI_DUNG = '" & objDTL.Noi_Dung & "', " & _
    '                     "KY_THUE = '" & objDTL.Ky_Thue & "' " & _
    '                     "where ID = " & objDTL.ID
    '            ' Insert Vào bảng TCS_CTU_DTL    
    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        Next
    '        ' Cập nhật lại các chứng từ có cùng số phiếu kế toán
    '        If Not "".Equals(strSoPKT) And Not "0".Equals(strSoPKT) Then
    '            strSql = "Update tcs_ctu_thop_hdr set trang_thai='01' where so_phieu_kt =" & strSoPKT & _
    '                     " And ngay_kb =" & objCTuHDR.Ngay_KB
    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        End If
    '        ' Cập nhật dữ liệu vào CSDL
    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        'LogDebug.Writelog("Lỗi trong quá trình cập dữ liệu chứng từ: " & ex.ToString)
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Is Nothing Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Is Nothing Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    Public Sub Update(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), Optional ByVal strSoPKT As String = "")
        ' ****************************************************************************
        ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
        ' Đầu vào: đối tượng HDR và mảng DTL
        ' Người viết: Lê Hồng Hà
        ' Ngày viết: 09/11/2007
        '*****************************************************************************
        Dim conn As DataAccess
        Dim transCT As IDbTransaction
        Dim objDTL As infChungTuDTL
        Dim strSql As String
        Try
            conn = New DataAccess
            transCT = conn.BeginTransaction

            'Cập nhật thông tin HDR của chứng từ
            strSql = "Update TCS_CTU_THOP_HDR SET " & _
                     "Ma_CQThu='" & objCTuHDR.Ma_CQThu & "'," & _
                     "XA_ID=" & objCTuHDR.XA_ID & ",Ma_Tinh='" & objCTuHDR.Ma_Tinh & "',Ma_Huyen='" & objCTuHDR.Ma_Huyen & "'," & _
                     "Ma_Xa='" & objCTuHDR.Ma_Xa & "',TK_No='" & objCTuHDR.TK_No & "',TK_Co='" & objCTuHDR.TK_Co & "' " & _
                     " Where (SHKB = '" & objCTuHDR.SHKB & "') And (Ngay_KB = " & objCTuHDR.Ngay_KB & _
                     ") And (Ma_NV = " & objCTuHDR.Ma_NV & ") And (So_BT = " & objCTuHDR.So_BT & _
                     ") And (Ma_DThu = '" & objCTuHDR.Ma_DThu & "')"

            ' Insert vào bảng TCS_CT_HDR
            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
            ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
            For Each objDTL In objCTuDTL
                strSql = "Update TCS_CTU_THOP_DTL set " & _
                         "cch_id = " & objDTL.CCH_ID & "," & _
                         "MA_CAP = '" & objDTL.Ma_Cap & "', " & _
                         "MA_CHUONG = '" & objDTL.Ma_Chuong & "', " & _
                         "lkh_id = " & objDTL.LKH_ID & "," & _
                         "MA_LOAI = '" & objDTL.Ma_Loai & "', " & _
                         "MA_KHOAN = '" & objDTL.Ma_Khoan & "', " & _
                         "mtm_id = " & objDTL.MTM_ID & "," & _
                         "MA_MUC = '" & objDTL.Ma_Muc & "', " & _
                         "MA_TMUC = '" & objDTL.Ma_TMuc & "', " & _
                         "dt_id = " & objDTL.DT_ID & "," & _
                         "MA_TLDT = '" & objDTL.Ma_TLDT & "', " & _
                         "NOI_DUNG = '" & objDTL.Noi_Dung & "', " & _
                         "KY_THUE = '" & objDTL.Ky_Thue & "' " & _
                         "where ID = " & objDTL.ID
                ' Insert Vào bảng TCS_CTU_DTL    
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
            Next
            ' Cập nhật lại các chứng từ có cùng số phiếu kế toán
            If Not "".Equals(strSoPKT) And Not "0".Equals(strSoPKT) Then
                strSql = "Update tcs_ctu_thop_hdr set trang_thai='01' where so_phieu_kt =" & strSoPKT & _
                         " And ngay_kb =" & objCTuHDR.Ngay_KB
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
            End If
            ' Cập nhật dữ liệu vào CSDL
            transCT.Commit()
        Catch ex As Exception
            transCT.Rollback()
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập dữ liệu chứng từ")
            'LogDebug.WriteLog("Lỗi trong quá trình cập dữ liệu chứng từ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            ' Giải phóng Connection tới CSDL
            If Not transCT Is Nothing Then
                transCT.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Sub



    Public Sub Update_New(ByVal hdr As NewChungTu.infChungTuHDR, ByVal dtls As NewChungTu.infChungTuDTL(), Optional ByVal blnXoaThucThu As Boolean = True)
        '----------------------------------------------------------------------
        ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
        ' Đầu vào: đối tượng HDR và mảng DTL
        ' Người viết: Lê Hồng Hà
        ' Ngày viết: 09/11/2007
        '----------------------------------------------------------------------
        Dim conn As DataAccess
        Dim transCT As IDbTransaction
        Dim dtl As NewChungTu.infChungTuDTL
        Dim strSql As String
        Try
            conn = New DataAccess
            transCT = conn.BeginTransaction

            'And (Ma_DThu = '" & hdr.Ma_DThu & "')" & _
            'Dim v_strSQLChk As String = "Select ttien from TCS_CTU_HDR Where (SHKB = '" & hdr.SHKB & "') And (Ngay_KB = " & hdr.Ngay_KB & _
            '        ") And (Ma_NV = " & hdr.Ma_NV & ") And (So_BT = " & hdr.So_BT & _
            '        ") And (So_CT = '" & hdr.So_CT & "')"

            'Dim v_ds As DataSet = conn.ExecuteReturnDataSet(v_strSQLChk, CommandType.Text)
            'If v_ds.Tables.Count > 0 Then
            '    If CDbl(v_ds.Tables(0).Rows(0)(0)) <> CDbl(hdr.TTien) Then
            '        blnXoaThucThu = True
            '    Else
            '        blnXoaThucThu = False
            '    End If
            'Else
            '    blnXoaThucThu = False
            'End If

            'Cập nhật thông tin HDR của chứng từ
            strSql = "Update TCS_CTU_HDR SET " & _
                     "kyhieu_ct = '" & hdr.KyHieu_CT & "', " & _
                     "So_CT= '" & hdr.So_CT & "', So_BThu=" & hdr.So_BThu & ",So_CT_NH='" & hdr.So_CT_NH & "'," & _
                     "Ma_NNTien='" & hdr.Ma_NNTien & "',Ten_NNTien=:Ten_NNTien," & _
                     "DC_NNTien=:DC_NNTien,Ma_NNThue='" & hdr.Ma_NNThue & "'," & _
                     "Ten_NNThue=:Ten_NNThue,DC_NNThue=:DC_NNThue," & _
                     "Ma_KS=0,So_QD='" & hdr.So_QD & "'," & _
                     "PT_TT='" & hdr.PT_TT & "'," & "TK_KB_NH='" & hdr.TK_KB_NH & "'," & _
                     "Ngay_QD=" & hdr.Ngay_QD & ",CQ_QD='" & hdr.CQ_QD & "',Ngay_CT=" & hdr.Ngay_CT & "," & _
                     "Ngay_HT=" & hdr.Ngay_HT & ",Ma_CQThu='" & hdr.Ma_CQThu & "',DIEN_GIAI='" & hdr.Dien_giai & "'," & _
                     "XA_ID='" & hdr.XA_ID & "',Ma_Tinh='" & hdr.Ma_Tinh & "',Ma_Huyen='" & hdr.Ma_Huyen & "'," & _
                     "Ma_Xa='" & hdr.Ma_Xa & "',TK_No='" & hdr.TK_No & "',TK_Co='" & hdr.TK_Co & "'," & _
                     "So_TK='" & hdr.So_TK & "',Ngay_TK=" & hdr.Ngay_TK & ",LH_XNK='" & hdr.LH_XNK & "'," & _
                     "DVSDNS='" & hdr.DVSDNS & "',TEN_DVSDNS = '" & hdr.Ten_DVSDNS & "'," & _
                     "Ma_NT='" & hdr.Ma_NT & "',Ty_Gia=" & hdr.Ty_Gia & "," & _
                     "TG_ID = " & hdr.TG_ID & ",MA_LTHUE = '" & hdr.Ma_LThue & "',GHI_CHU = '" & hdr.Ghi_chu & "'," & _
                     "So_Khung='" & hdr.So_Khung & "',So_May='" & hdr.So_May & "'," & _
                     "TEN_KH_NH='" & hdr.TEN_KH_NH & "',TK_KH_NH='" & hdr.TK_KH_NH & "',NGAY_KH_NH=" & hdr.NGAY_KH_NH & "," & _
                     "MA_NH_A='" & hdr.MA_NH_A & "',MA_NH_B='" & hdr.MA_NH_B & "',MA_NH_TT='" & hdr.MA_NH_TT & "',TEN_NH_B=:TEN_NH_B,TEN_NH_TT=:TEN_NH_TT," & _
                     "TTien=" & hdr.TTien & ", Lan_In=0, Trang_Thai='" & hdr.Trang_Thai & "', " & _
                     "phi_gd =" & hdr.PHI_GD & ",pt_tinhphi='" & hdr.PT_TINHPHI & "', phi_vat=" & hdr.PHI_VAT & ", " & _
                     "TK_KH_Nhan='" & hdr.TK_KH_Nhan & "', Ten_KH_Nhan='" & hdr.Ten_KH_Nhan & "'," & _
                     "Diachi_KH_Nhan='" & hdr.Diachi_KH_Nhan & "', TTien_NT=" & hdr.TTien_NT & ", " & _
                     "So_BK = '" & hdr.So_BK & "',MA_SANPHAM = '" & hdr.SAN_PHAM & "',TT_CITAD = '" & hdr.TT_CITAD & "', Ngay_BK = " & hdr.Ngay_BK & ", " & _
                     "QUAN_HUYENNNTIEN='" & hdr.QUAN_HUYEN_NNTIEN & "',TINH_TPNNTIEN='" & hdr.TINH_TPHO_NNTIEN & "'," & _
                     "MA_HQ='" & hdr.MA_HQ & "',TEN_HQ='" & hdr.TEN_HQ & "',tt_tthu='" & hdr.TT_TThu & "'," & _
                     "MA_HQ_PH='" & hdr.Ma_hq_ph & "',TEN_HQ_PH='" & hdr.TEN_HQ_PH & "', MAHUYEN_NNT = '" & hdr.MA_HUYEN_NNT & "', TENHUYEN_NNT = '" & hdr.TEN_HUYEN_NNT & "', MATINH_NNT = '" & hdr.MA_TINH_NNT & "', TENTINH_NNT = '" & hdr.TEN_TINH_NNT & "', TENHUYEN_NNTHAY = '" & hdr.HUYEN_NNTHAY & "', TENTINH_NNTHAY = '" & hdr.TINH_NNTHAY & "', LOAI_NNT='" & hdr.LOAI_NNT & "'," & _
                     " shkb = '" & hdr.SHKB & "'," & _
                    "Ten_CQThu=:Ten_CQThu,dac_diem_ptien=:dac_diem_ptien, " & _
                      " ma_hs=:ma_hs,ma_gcn=:ma_gcn,so_thuadat = :so_thuadat, so_to_bando=:so_to_bando, dia_chi_ts=:dia_chi_ts,ma_dbhc_ts=:ma_dbhc_ts,ten_dbhc_ts=:ten_dbhc_ts " & _
                        " Where (Ngay_KB = " & hdr.Ngay_KB & _
                     ") And (so_ct = " & hdr.So_CT & ") And (So_BT = " & hdr.So_BT & ")"
            'And (Ma_DThu = '" & hdr.Ma_DThu & "')"
            ' Insert vào bảng TCS_CT_HDR
            ' conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

            Dim lsthdr As List(Of IDataParameter) = New List(Of IDataParameter)
            lsthdr.Add(DataAccess.NewDBParameter(":Ten_NNTien", ParameterDirection.Input, hdr.Ten_NNTien, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter(":DC_NNTien", ParameterDirection.Input, hdr.DC_NNTien, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter(":Ten_NNThue", ParameterDirection.Input, hdr.Ten_NNThue, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter(":DC_NNThue", ParameterDirection.Input, hdr.DC_NNThue, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter(":TEN_NH_B", ParameterDirection.Input, hdr.Ten_NH_B, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, hdr.TEN_NH_TT, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter(":Ten_CQThu", ParameterDirection.Input, hdr.Ten_cqthu, DbType.String))


            lsthdr.Add(DataAccess.NewDBParameter("dac_diem_ptien", ParameterDirection.Input, hdr.DAC_DIEM_PTIEN, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter("ma_hs", ParameterDirection.Input, hdr.MA_HS, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter("ma_gcn", ParameterDirection.Input, hdr.MA_GCN, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter("so_thuadat", ParameterDirection.Input, hdr.SO_THUADAT, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter("so_to_bando", ParameterDirection.Input, hdr.SO_TO_BANDO, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter("dia_chi_ts", ParameterDirection.Input, hdr.DIA_CHI_TS, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter("ma_dbhc_ts", ParameterDirection.Input, hdr.MA_DBHC_TS, DbType.String))
            lsthdr.Add(DataAccess.NewDBParameter("ten_dbhc_ts", ParameterDirection.Input, hdr.TEN_DBHC_TS, DbType.String))

            DataAccess.ExecuteNonQuery(strSql, CommandType.Text, lsthdr.ToArray(), transCT)

            'Xóa chứng từ chi tiết - để cập nhật lại
            strSql = "delete from TCS_CTU_DTL " & _
                    "where " & _
                    "so_ct=" & hdr.So_CT
            '"Ma_DThu='" & hdr.Ma_DThu & "'"
            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

            ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
            For Each dtl In dtls
                If Not dtl Is Nothing Then
                    strSql = "Insert into TCS_CTU_DTL(ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                        "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                        "DT_ID,MaDT,Ky_Thue,SoTien,SoTien_NT, MaQuy, Ma_DP, so_ct) " & _
                        "Values (" & dtl.ID & ",'" & dtl.SHKB & "'," & dtl.Ngay_KB & "," & dtl.Ma_NV & "," & _
                        dtl.So_BT & ",'" & dtl.Ma_DThu & "'," & dtl.CCH_ID & ",'" & _
                        dtl.Ma_Cap & "','" & dtl.Ma_Chuong & "'," & dtl.LKH_ID & ",'" & _
                        dtl.Ma_Loai & "','" & dtl.Ma_Khoan & "'," & _
                        dtl.MTM_ID & ",'" & dtl.Ma_Muc & "','" & dtl.Ma_TMuc & "'," & _
                        ":Noi_Dung," & _
                        dtl.DT_ID & ",NULL,'" & dtl.Ky_Thue & "'," & _
                        dtl.SoTien & "," & dtl.SoTien_NT & ",'" & dtl.MaQuy & "', NULL, '" & hdr.So_CT & "')"
                    ' Insert Vào bảng TCS_CTU_DTL    
                    'conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                    Dim lstdtl As List(Of IDataParameter) = New List(Of IDataParameter)
                    lstdtl.Add(DataAccess.NewDBParameter(":Noi_Dung", ParameterDirection.Input, dtl.Noi_Dung, DbType.String))

                    ' Insert Vào bảng TCS_CTU_DTL    
                    DataAccess.ExecuteNonQuery(strSql, CommandType.Text, lstdtl.ToArray(), transCT)
                End If
            Next
            ' Cập nhật dữ liệu vào CSDL
            transCT.Commit()
        Catch ex As Exception
            transCT.Rollback()
            'CTuCommon.WriteLog(ex, "Lỗi insert dư liệu chứng từ")
            LogApp.WriteLog("Update_New.GIP", ex, "Lỗi ghi và chuyển kiểm soát chứng từ")
           
            Throw ex
        Finally
            ' Giải phóng Connection tới CSDL
            If Not transCT Is Nothing Then
                transCT.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Sub


    'Public Sub Update_New(ByVal objCTuHDR As NewChungTu.infChungTuHDR, ByVal objCTuDTL As NewChungTu.infChungTuDTL(), Optional ByVal strSoPKT As String = "")
    '    ' ****************************************************************************
    '    ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
    '    ' Đầu vào: đối tượng HDR và mảng DTL
    '    ' Người viết: Lê Hồng Hà
    '    ' Ngày viết: 09/11/2007
    '    '*****************************************************************************
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Dim objDTL As NewChungTu.infChungTuDTL
    '    Dim strSql As String
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction

    '        'Cập nhật thông tin HDR của chứng từ
    '        strSql = "Update TCS_CTU_THOP_HDR SET " & _
    '                 "Ma_CQThu='" & objCTuHDR.Ma_CQThu & "'," & _
    '                 "XA_ID=" & objCTuHDR.XA_ID & ",Ma_Tinh='" & objCTuHDR.Ma_Tinh & "',Ma_Huyen='" & objCTuHDR.Ma_Huyen & "'," & _
    '                 "Ma_Xa='" & objCTuHDR.Ma_Xa & "',TK_No='" & objCTuHDR.TK_No & "',TK_Co='" & objCTuHDR.TK_Co & "' " & _
    '                 " Where (SHKB = '" & objCTuHDR.SHKB & "') And (Ngay_KB = " & objCTuHDR.Ngay_KB & _
    '                 ") And (Ma_NV = " & objCTuHDR.Ma_NV & ") And (So_BT = " & objCTuHDR.So_BT & _
    '                 ") And (Ma_DThu = '" & objCTuHDR.Ma_DThu & "')"

    '        ' Insert vào bảng TCS_CT_HDR
    '        conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        ' Hình thành câu lệnh insert vào bảng TCS_CTU_DTL
    '        For Each objDTL In objCTuDTL
    '            strSql = "Update TCS_CTU_THOP_DTL set " & _
    '                     "cch_id = " & objDTL.CCH_ID & "," & _
    '                     "MA_CAP = '" & objDTL.Ma_Cap & "', " & _
    '                     "MA_CHUONG = '" & objDTL.Ma_Chuong & "', " & _
    '                     "lkh_id = " & objDTL.LKH_ID & "," & _
    '                     "MA_LOAI = '" & objDTL.Ma_Loai & "', " & _
    '                     "MA_KHOAN = '" & objDTL.Ma_Khoan & "', " & _
    '                     "mtm_id = " & objDTL.MTM_ID & "," & _
    '                     "MA_MUC = '" & objDTL.Ma_Muc & "', " & _
    '                     "MA_TMUC = '" & objDTL.Ma_TMuc & "', " & _
    '                     "dt_id = " & objDTL.DT_ID & "," & _
    '                     "MA_TLDT = '" & objDTL.Ma_TLDT & "', " & _
    '                     "NOI_DUNG = '" & objDTL.Noi_Dung & "', " & _
    '                     "KY_THUE = '" & objDTL.Ky_Thue & "' " & _
    '                     "where ID = " & objDTL.ID
    '            ' Insert Vào bảng TCS_CTU_DTL    
    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        Next
    '        ' Cập nhật lại các chứng từ có cùng số phiếu kế toán
    '        If Not "".Equals(strSoPKT) And Not "0".Equals(strSoPKT) Then
    '            strSql = "Update tcs_ctu_thop_hdr set trang_thai='01' where so_phieu_kt =" & strSoPKT & _
    '                     " And ngay_kb =" & objCTuHDR.Ngay_KB
    '            conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
    '        End If
    '        ' Cập nhật dữ liệu vào CSDL
    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        'LogDebug.Writelog("Lỗi trong quá trình cập dữ liệu chứng từ: " & ex.ToString)
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Is Nothing Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Is Nothing Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub
#End Region

    'Public Function CTU_Get_LanIn(ByVal key As CTuKey) As String
    '    Dim cnCT As DataAccess
    '    Dim drCT As IDataReader
    '    Try
    '        cnCT = New DataAccess
    '        Dim strSQL As String
    '        strSQL = "select LAN_IN from tcs_ctu_hdr " & _
    '                "where shkb = '" & key.SHKB.ToString() & "' and " & _
    '                " ngay_kb = " & key.NgayKB.ToString() & " and " & _
    '                " ma_nv = " & key.MaNV.ToString() & " and " & _
    '                " so_bt = " & key.SoBT.ToString() & " and " & _
    '                " ma_dthu = '" & key.MaDT.ToString() & "' "

    '        drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
    '        Dim strLanIn As String = ""
    '        If (drCT.Read()) Then
    '            strLanIn = drCT(0).ToString()
    '        End If
    '        Return strLanIn
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
    '        If (Not IsNothing(drCT)) Then
    '            drCT.Close()
    '            drCT.Dispose()
    '        End If
    '    End Try
    'End Function

    Public Function CTU_Get_LanIn(ByVal key As KeyCTu) As String
        Dim cnCT As DataAccess
        Dim drCT As IDataReader
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select LAN_IN from tcs_ctu_hdr " & _
                    "where shkb = '" & key.SHKB.ToString() & "' and " & _
                    " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                    " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                    " so_bt = " & key.So_BT.ToString() ' &" and " & _
            '  " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

            drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
            Dim strLanIn As String = ""
            If (drCT.Read()) Then
                strLanIn = drCT(0).ToString()
            End If
            Return strLanIn
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin số lần in")
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            If (Not IsNothing(drCT)) Then
                drCT.Close()
                drCT.Dispose()
            End If
        End Try
    End Function

#Region "HoanNH"
    Public Function CapNhatPKT(ByVal strSoPKT As String, ByVal Update As Boolean) As Boolean
        Dim cnPKT As DataAccess
        Dim strSql As String
        Try
            cnPKT = New DataAccess
            If Update Then
                strSql = "Delete From tdt_tmp_checkchon Where session_id = " & gstrTenND & _
                         " And block_name='TCS_CTU_THOP_HDR' And id_hdr='" & strSoPKT & "'"
            Else
                strSql = "Insert Into tdt_tmp_checkchon(session_id, block_name,id_hdr) " & _
                         "Values(" & gstrTenND & ",'TCS_CTU_THOP_HDR','" & strSoPKT & "')"
            End If
            cnPKT.ExecuteNonQuery(strSql, CommandType.Text)
            Return True
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật PKT vào bảng temp")
            'LogDebug.WriteLog("Lỗi trong quá trình cập nhật PKT vào bảng temp: " & ex.ToString, EventLogEntryType.Error)
            Return False
        Finally
            If Not cnPKT Is Nothing Then cnPKT.Dispose()
        End Try
    End Function
    Public Function InsertPKTTemp(ByVal strWhere As String, ByVal Update As Boolean) As Boolean
        Dim cnPKT As DataAccess
        Dim strSql As String
        Try
            cnPKT = New DataAccess
            If Update Then
                strSql = "Delete From tdt_tmp_checkchon Where session_id = " & gstrTenND & _
                         " And block_name='TCS_CTU_THOP_HDR'"
            Else
                strSql = "Insert Into tdt_tmp_checkchon(id_hdr,session_id, block_name) " & _
                         "(Select distinct HDR.SO_PHIEU_KT || HDR.ngay_kb ," & gstrTenND & ",'TCS_CTU_THOP_HDR' " & _
                         "From TCS_CTU_THOP_HDR HDR WHERE (1=1) " & strWhere & ")"
            End If
            cnPKT.ExecuteNonQuery(strSql, CommandType.Text)
            Return True
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật PKT vào bảng temp")
            'LogDebug.WriteLog("Lỗi trong quá trình cập nhật PKT vào bảng temp: " & ex.ToString, EventLogEntryType.Error)
            Return False
        Finally
            If Not cnPKT Is Nothing Then cnPKT.Dispose()
        End Try
    End Function
    Public Function LoadDSDThu() As DataSet
        Dim cnDT As DataAccess
        Dim strSql As String
        Dim dsResult As DataSet
        Try
            cnDT = New DataAccess
            strSql = "SELECT ma_dthu, ten FROM tcs_dm_diemthu"
            dsResult = cnDT.ExecuteReturnDataSet(strSql, CommandType.Text)
            Return dsResult
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách điểm thu")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách điểm thu: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnDT Is Nothing Then cnDT.Dispose()
        End Try
    End Function
    Public Function LoadDSCT(ByVal strSoPKT As String, ByVal lngNgayKB As Long, ByVal strMaDT As String) As DataSet
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Dim strSQL As String
        Try
            cnChungTu = New DataAccess
            strSQL = "Select KYHIEU_CT, SO_CT, SO_BTHU, TRANG_THAI, SO_BT, MA_NV, Lan_In, TTien, SHKB, NGAY_KB, MA_DTHU " & _
                     " from TCS_CTU_THOP_HDR " & _
                     "where trang_thai ='00' And ma_dthu ='" & strMaDT & "' And ngay_kb =" & lngNgayKB & " And so_phieu_kt =" & strSoPKT
            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Return dsChungTu
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách chứng từ")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách chứng từ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
    End Function
#Region "Kết xuất sang KTKB"
    'Public Function LoadCTKBHDR(ByVal strWhere As String, ByVal intTT As Integer) As DataSet
    '    '-----------------------------------------------------------------
    '    ' Mục đích: Lấy danh sách header của chứng từ
    '    ' Tham số:
    '    ' Giá trị trả về:
    '    ' Ngày viết: 02/11/2007
    '    ' Người viết: Nguyễn Hữu Hoan
    '    ' ----------------------------------------------------------------
    '    Dim cnChungTu As DataAccess
    '    Dim dsChungTu As DataSet
    '    Dim tranCT As IDbTransaction
    '    Dim strSQL As String
    '    Dim p As IDbDataParameter
    '    Dim arrIn As IDataParameter()
    '    Try
    '        cnChungTu = New DataAccess
    '        If intTT = 0 Then
    '            tranCT = cnChungTu.BeginTransaction
    '            ' Thực hiện gom chứng từ
    '            ReDim arrIn(1)
    '            p = cnChungTu.GetParameter("p_manv", ParameterDirection.Input, CInt(gstrTenND), DbType.Int64)
    '            arrIn(0) = p
    '            p = cnChungTu.GetParameter("p_where", ParameterDirection.Input, strWhere, DbType.String)
    '            p.Size = 10000
    '            arrIn(1) = p
    '            strSQL = "TCS_PCK_KTKB.prc_gom_ctu"
    '            cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn, tranCT)
    '            ' Xóa trước khi đưa vào
    '            strSQL = "Delete From tdt_tmp_checkchon Where session_id=" & gstrTenND & _
    '                     " And block_name='TCS_CTU_THOP_HDR'"
    '            cnChungTu.ExecuteNonQuery(strSQL, CommandType.Text, tranCT)
    '            ' Insert vào bảng tdt_tmp_checkchon
    '            strSQL = "Insert Into tdt_tmp_checkchon(id_hdr,session_id, block_name) " & _
    '                     "(Select distinct HDR.SO_PHIEU_KT||HDR.Ngay_KB," & gstrTenND & ",'TCS_CTU_THOP_HDR' " & _
    '                     "From TCS_CTU_THOP_HDR HDR WHERE (1=1) " & strWhere & ")"
    '            cnChungTu.ExecuteNonQuery(strSQL, CommandType.Text, tranCT)
    '            ' Lấy dữ liệu sau khi gom chứng từ
    '            strSQL = "Select HDR.SO_PHIEU_KT," & _
    '                     "TCS_PCK_TRAODOI.Fnc_ToDate (HDR.NGAY_KB) as NGAY_KB," & _
    '                     "TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_NO) as TK_NO, TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_CO) as TK_CO," & _
    '                     "(HDR.MA_TINH || HDR.MA_HUYEN || HDR.MA_XA) as DBHC," & _
    '                     "HDR.MA_CQTHU,HDR.MA_DTHU, " & _
    '                     "SUM(HDR.TTIEN) AS TONGTIEN,1,HDR.SHKB,HDR.so_bt_ktkb,HDR.ma_nv " & _
    '                     "FROM TCS_CTU_THOP_HDR HDR " & _
    '                     "WHERE (hdr.trang_thai='00') " & strWhere & _
    '                     " Group By SO_PHIEU_KT,SHKB,NGAY_KB,MA_DTHU,TK_NO,TK_CO,MA_TINH,MA_HUYEN,MA_XA,MA_CQTHU,so_bt_ktkb,ma_nv " & _
    '                     " Order by so_phieu_kt asc"
    '            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '            tranCT.Commit()
    '        Else
    '            ' Tra cứu các chứng từ đã được chuyển sang ktkb
    '            ' Lấy dữ liệu sau khi gom chứng từ
    '            strSQL = "Select HDR.SO_PHIEU_KT," & _
    '                     "TCS_PCK_TRAODOI.Fnc_ToDate (HDR.NGAY_KB) as NGAY_KB," & _
    '                     "TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_NO) as TK_NO, TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_CO) as TK_CO," & _
    '                     "(HDR.MA_TINH || HDR.MA_HUYEN || HDR.MA_XA) as DBHC," & _
    '                     "HDR.MA_CQTHU,HDR.MA_DTHU, " & _
    '                     "SUM(HDR.TTIEN) AS TONGTIEN,0,HDR.SHKB,HDR.so_bt_ktkb,HDR.ma_nv " & _
    '                     "FROM TCS_CTU_THOP_HDR HDR " & _
    '                     "WHERE (hdr.trang_thai='01') " & strWhere & _
    '                     " AND hdr.ma_dthu<>'01' " _
    '                     & " Group By SO_PHIEU_KT,SHKB,NGAY_KB,MA_DTHU,TK_NO,TK_CO,MA_TINH,MA_HUYEN,MA_XA,MA_CQTHU,so_bt_ktkb,ma_nv " & _
    '                     " Order by so_phieu_kt asc"
    '            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '        End If
    '    Catch ex As Exception
    '        tranCT.Rollback()
    '        'LogDebug.Writelog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString)
    '        Throw ex
    '    Finally
    '        If Not tranCT Is Nothing Then tranCT.Dispose()
    '        If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
    '    End Try
    '    Return dsChungTu
    'End Function
    Public Function LoadCTKBCTHDR(ByVal strWhere As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ
        '         : không thực hiện gom chứng từ
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Nguyễn Hữu Hoan
        ' ----------------------------------------------------------------
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Dim strSQL As String
        Try
            cnChungTu = New DataAccess
            strSQL = "Select HDR.SO_CT," & _
                     "TCS_PCK_TRAODOI.Fnc_ToDate (HDR.NGAY_KB) as NGAY_KB," & _
                     "TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_NO) as TK_NO, TCS_PCK_TRAODOI.Fnc_FormatTK(HDR.TK_CO) as TK_CO," & _
                     "(HDR.MA_TINH || HDR.MA_HUYEN || HDR.MA_XA) as DBHC," & _
                     "HDR.MA_CQTHU,HDR.MA_DTHU, " & _
                     "HDR.TTIEN AS TONGTIEN,HDR.SHKB,hdr.ma_nv,hdr.so_bt " & _
                     "FROM TCS_CTU_THOP_HDR HDR " & _
                     "WHERE (hdr.trang_thai='01') " & strWhere
            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách header của chứng từ từ CSDL")
            'LogDebug.WriteLog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return dsChungTu
    End Function
    'Public Function HachToanKTKB(ByVal strWhere As String, _
    '                              ByVal strSHKB As String, ByVal maNV As Int64, _
    '                              ByVal NgayKTKB As Int64, ByRef ds As DataSet) As String
    '    Dim cnChungTu As DataAccess
    '    Dim cnKS As New OracleConnect
    '    Dim strConn As String
    '    Dim strSQL As String
    '    Dim arrOut As ArrayList
    '    Dim p As IDbDataParameter
    '    Dim arrIn As IDataParameter()
    '    Dim strResult As String
    '    Try
    '        cnChungTu = New DataAccess
    '        ' Thực hiện gom chứng từ
    '        ReDim arrIn(2)
    '        p = cnChungTu.GetParameter("p_where", ParameterDirection.Input, strWhere, DbType.String)
    '        p.Size = 10000
    '        arrIn(0) = p
    '        p = cnChungTu.GetParameter("p_ma_ks", ParameterDirection.Input, maNV, DbType.Int64)
    '        p.Size = 10
    '        arrIn(1) = p
    '        p = cnChungTu.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String)
    '        p.Size = 500
    '        arrIn(2) = p
    '        strSQL = "TCS_PCK_KTKB.Prc_Insert_TDT_Ktkb"
    '        arrOut = cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn)
    '        cnChungTu.Dispose()

    '        strResult = arrOut.Item(0).ToString
    '        If strResult.Trim().ToUpper.Equals("OK") Then
    '            ' Thuc hien chuyen sang KTKB
    '            ReDim arrIn(2)
    '            p = cnKS.GetParameter("p_ngaylv", ParameterDirection.Input, gdtmNgayLV, DbType.Int64)
    '            arrIn(0) = p
    '            p = cnKS.GetParameter("p_ma_ks", ParameterDirection.Input, maNV, DbType.Int64)
    '            arrIn(1) = p
    '            p = cnKS.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String, 500)
    '            arrIn(2) = p
    '            strSQL = "TDTT_PCK_KTKB.prc_hach_toan_ktkb_thop"
    '            arrOut = cnKS.ExecuteNonquery(strSQL, arrIn)
    '            strResult = arrOut.Item(0).ToString()
    '            strSQL = "Select so_phieu_kt,noidung,to_char(thoigian,'dd/MM/rrrr HH24:MI:ss') From TDT_Log order by so_phieu_kt asc"
    '            ds = cnKS.ExecuteReturnDataset(strSQL, CommandType.Text)
    '        End If
    '    Catch ex As Exception
    '        'LogDebug.Writelog("Lỗi trong quá trình hạch toánh KTKT: " & ex.ToString)
    '        Throw ex
    '    Finally
    '        cnChungTu = New DataAccess
    '        ' Thực hiện gom chứng từ
    '        ReDim arrIn(1)
    '        p = cnChungTu.GetParameter("p_ma_ks", ParameterDirection.Input, maNV, DbType.Int64)
    '        p.Size = 10
    '        arrIn(0) = p
    '        p = cnChungTu.GetParameter("p_error", ParameterDirection.Output, Nothing, DbType.String)
    '        p.Size = 500
    '        arrIn(1) = p
    '        strSQL = "TCS_PCK_KTKB.prc_xoa_tdtt"
    '        arrOut = cnChungTu.ExecuteNonQuery(strSQL, CommandType.StoredProcedure, arrIn)
    '        cnChungTu.Dispose()
    '        If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
    '    End Try
    '    Return strResult
    'End Function
    Public Function LoadCTKBDTL(ByVal strSHKB As String, ByVal intNgay_KB As Integer, _
                                ByVal strMa_DThu As String, ByVal strTk_No As String, _
                                ByVal strTk_Co As String, ByVal strDBHC As String, _
                                ByVal strMa_CQthu As String, ByVal intTT As Integer) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chi tiết của chứng từ.
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Nguyễn Hữu Hoan
        ' ---------------------------------------------------- 
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Try
            cnChungTu = New DataAccess
            Dim strSQL As String
            If intTT = 0 Then
                strSQL = "select dtl.ma_cap,dtl.ma_chuong,dtl.ma_loai,dtl.ma_khoan,dtl.ma_muc,dtl.ma_tmuc, " & _
                        "dtl.sotien as sotien " & _
                        "from TCS_CTU_THOP_HDR hdr, TCS_CTU_THOP_DTL dtl " & _
                        "Where (hdr.shkb = dtl.shkb) And (hdr.ngay_kb = dtl.ngay_kb) and (hdr.ma_nv = dtl.ma_nv) " & _
                        "And (hdr.so_bt = dtl.so_bt) And (hdr.ma_dthu = dtl.ma_dthu) " & _
                        "And (hdr.trang_thai='00') " & _
                        "And (hdr.SHKB = '" & strSHKB & "') " & _
                        "And (hdr.NGAY_KB = " & intNgay_KB & ") " & _
                        "And (hdr.MA_DTHU ='" & strMa_DThu & "') " & _
                        "And (hdr.TK_NO = '" & ChuanHoa_SoTK(strTk_No) & "') " & _
                        "And (hdr.TK_CO ='" & ChuanHoa_SoTK(strTk_Co) & "') " & _
                        "And ((HDR.MA_TINH || HDR.MA_HUYEN || HDR.MA_XA)='" & strDBHC & "') " & _
                        "And (hdr.Ma_cqthu = '" & strMa_CQthu & "') "
                '"Group by dtl.ma_cap,dtl.ma_chuong, dtl.ma_loai,dtl.ma_khoan,dtl.ma_muc,dtl.ma_tmuc "
            Else
                strSQL = "select dtl.ma_cap,dtl.ma_chuong,dtl.ma_loai,dtl.ma_khoan,dtl.ma_muc,dtl.ma_tmuc, " & _
                        "dtl.sotien as sotien " & _
                        "from TCS_CTU_THOP_HDR hdr, TCS_CTU_THOP_DTL dtl " & _
                        "Where (hdr.shkb = dtl.shkb) And (hdr.ngay_kb = dtl.ngay_kb) and (hdr.ma_nv = dtl.ma_nv) " & _
                        "And (hdr.so_bt = dtl.so_bt) And (hdr.ma_dthu = dtl.ma_dthu) " & _
                        "And (hdr.trang_thai='01') " & _
                        "And (hdr.SHKB = '" & strSHKB & "') " & _
                        "And (hdr.NGAY_KB = " & intNgay_KB & ") " & _
                        "And (hdr.MA_DTHU ='" & strMa_DThu & "') " & _
                        "And (hdr.TK_NO = '" & ChuanHoa_SoTK(strTk_No) & "') " & _
                        "And (hdr.TK_CO ='" & ChuanHoa_SoTK(strTk_Co) & "') " & _
                        "And ((HDR.MA_TINH || HDR.MA_HUYEN || HDR.MA_XA)='" & strDBHC & "') " & _
                        "And (hdr.Ma_cqthu = '" & strMa_CQthu & "') "
                '"Group by dtl.ma_cap,dtl.ma_chuong, dtl.ma_loai,dtl.ma_khoan,dtl.ma_muc,dtl.ma_tmuc "
            End If
            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ")
            'LogDebug.WriteLog("Có lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return dsChungTu
    End Function
    Public Function LoadCTKBCTDTL(ByVal strSHKB As String, ByVal intNgay_KB As Integer, _
                                ByVal strMa_DThu As String, ByVal strMa_NV As String, _
                                ByVal strSo_BT As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chi tiết của chứng từ.
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Nguyễn Hữu Hoan
        ' ---------------------------------------------------- 
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Try
            cnChungTu = New DataAccess
            Dim strSQL As String
            strSQL = "select dtl.ma_cap,dtl.ma_chuong,dtl.ma_loai,dtl.ma_khoan,dtl.ma_muc,dtl.ma_tmuc, " & _
                     "dtl.sotien " & _
                     "from TCS_CTU_THOP_DTL dtl " & _
                     "Where (dtl.SHKB = '" & strSHKB & "') " & _
                     "And (dtl.NGAY_KB = " & intNgay_KB & ") " & _
                     "And (dtl.MA_DTHU ='" & strMa_DThu & "') " & _
                     "And (dtl.ma_nv = " & strMa_NV & ") " & _
                     "And (dtl.so_bt =" & strSo_BT & ")"
            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ")
            'LogDebug.WriteLog("Có lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return dsChungTu
    End Function
#End Region
#End Region
#Region "TrongNM"

    Public Function WhereLcn_Recv(ByVal strTSNCode As String) As String
        Dim strtmp As String
        If "01,02,03,04,05,32,33".IndexOf(strTSNCode) >= 0 Then Return " (1=1) "
        strtmp = "(Substr(Lcn_Recv,4,5) ='" & _
                             Mid(gstrMaNoiLamViec, 4, 5) & "') "
        Return strtmp
    End Function
    Public Function LoadDataHeaderNhanCTNVP(ByVal strTSNCode As String, ByVal strPkgID As String, Optional ByVal strWhereNgay As String = "") As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh trọng
        ' Người sửa: Lê Hồng Hà
        ' Nội dung sửa: Cho phép tra cứu không theo gói dữ liệu
        ' ----------------------------------------------------------------
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Dim strSQL As String
        Try
            Dim strWhere As String = ""
            If (strPkgID <> "") Then
                strWhere = " (TRAN_UP.PKG_ID = '" & strPkgID & "') And "
            Else
                strWhere = " (tran_up.pkg_id IN ( SELECT p.ID FROM data_pkg p " & _
                            " WHERE (" & strWhereNgay & _
                            " (p.pkg_type = '1') AND (p.tsn_code = '62')))) and "
            End If
            cnChungTu = New DataAccess
            strSQL = "Select Char03, Char04, TCS_PCK_TRAODOI.Fnc_ToDate(num01) AS num01, Char08, " & _
                     "TCS_PCK_TRAODOI.Fnc_FormatTK(Char18) Char18, " & _
                     "TCS_PCK_TRAODOI.Fnc_FormatTK(Char19) Char19, " & _
                     "char15, char16, char17, num09, tran_up.tran_no , num02, num03, char02, char01 From TRAN_UP, MESS_UP " & _
                     "Where " & strWhere & _
                     "(TRAN_UP.Tran_No = MESS_UP.Tran_No) And" & _
                     "((TRAN_UP.Sts_Code='" & Business.Common.mdlSystemVariables.gstrSSN & "') Or " & _
                     "((TRAN_UP.Sts_Code = '" & Business.Common.mdlSystemVariables.gstrDN & "') And " & _
                     "(TRAN_UP.Resu_Code = '01'))) And " & buTraoDoi.WhereLcn_Recv(strTSNCode) & _
                     " Group by char01,char02,char03,char04,char08, char18, char19, char15, char16, char17, num01, num02, num03, num09, tran_up.tran_no "

            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chi tiết chứng từ")
            'LogDebug.Writelog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString)
            Throw ex
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return dsChungTu
    End Function

    Public Function LoadDataDetailNhanCTNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String, ByVal strTranNo As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách detail của chứng từ
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh trọng
        ' ----------------------------------------------------------------
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Dim strSQL As String
        Try
            cnChungTu = New DataAccess
            strSQL = "Select Char33, Char34, Char35, Char36, Char37, Char38, Char39, CHAR40, Num15, CHAR41 From MESS_UP " & _
            "Where (Tran_No='" & strTranNo & "' And Char03 = '" & strKYHIEU_CT & "') And " & _
            "(Char04 = '" & strSO_CT & "')"

            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chi tiết chứng từ")
            'LogDebug.Writelog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString)
            Throw ex
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return dsChungTu
    End Function
    Public Function LoadDataHeaderKXGNT(ByVal strWhere As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Dim strSQL As String
        Try
            cnChungTu = New DataAccess
            strSQL = "SELECT hdr.kyhieu_ct, hdr.so_ct, hdr.ma_nnthue, hdr.ten_nnthue, " & _
                     "TCS_PCK_TRAODOI.Fnc_ToDate (hdr.ngay_ct) ngay_ct, " & _
                     "hdr.ttien, hdr.shkb " & _
                 "FROM tcs_ctu_hdr hdr " & _
                 "WHERE " & strWhere _
                 & " Order by hdr.so_ct"

            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chi tiết chứng từ")
            'LogDebug.Writelog("Có lỗi sảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString)
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try

        Return dsChungTu
    End Function

    Public Function LoadBKKXCTNVP(ByVal strWhere As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report
        ' Tham số: 
        ' Giá trị trả về:
        ' Ngày viết: 13/12/2007
        ' Người viết: Ngô Minh Trọng

        ' Người Sửa: Hoàng Văn Anh
        ' Mục đích: sửa theo COA
        ' ----------------------------------------------------
        Dim conn As DataAccess
        Dim strSQL As String
        Try
            conn = New DataAccess
            strSQL = " SELECT DISTINCT dtl.maquy AS ma_quy, hdr.ngay_kb AS ngay_kb, hdr.shkb AS shkb," _
                        & " hdr.tk_no AS tk_no, hdr.tk_co AS tk_co, hdr.ma_xa AS dbhc, " _
                        & " (hdr.kyhieu_ct||hdr.so_ct) AS so_ct, hdr.ten_nnthue AS ten_nnt, " _
                        & " dtl.ma_chuong AS ma_chuong, dtl.ma_khoan AS ma_nganhkt, " _
                        & " dtl.ma_tmuc AS nd_kt, dtl.sotien AS so_tien " _
                        & " FROM tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " _
                        & " WHERE hdr.shkb = dtl.shkb " _
                        & "  AND hdr.ngay_kb = dtl.ngay_kb " _
                        & " AND hdr.ma_nv = dtl.ma_nv " _
                        & " AND hdr.so_bt = dtl.so_bt " _
                        & " AND hdr.ma_dthu = dtl.ma_dthu " _
                        & " AND " & strWhere _
                        & " ORDER BY (hdr.kyhieu_ct||hdr.so_ct), hdr.ngay_kb "
            Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi truy vấn BK CTU đưa ra DataSet để gắn vào report")
            Throw ex
        Finally
            If (Not IsNothing(conn)) Then conn.Dispose()
        End Try
    End Function

    Public Function LoadDataHeaderCQT(ByVal strWhere As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách header của chứng từ
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Dim strSQL As String
        Try
            cnChungTu = New DataAccess
            strSQL = "SELECT hdr.kyhieu_ct || '/' || hdr.so_ct, " & _
                     "TCS_PCK_TRAODOI.Fnc_ToDate(hdr.ngay_ct) ngay_ct,hdr.ma_nnthue, " & _
                     "TCS_PCK_TRAODOI.Fnc_FormatTK(hdr.tk_no) tk_no, " & _
                     "TCS_PCK_TRAODOI.Fnc_FormatTK(hdr.tk_co)tk_co, " & _
                     "lpad(to_char(hdr.ma_nv),3,'0'), hdr.ma_cqthu, " & _
                     "hdr.ttien,hdr.shkb,hdr.ngay_kb,hdr.ma_nv,hdr.so_bt,hdr.ma_dthu " & _
                     "FROM kx_cqt_hdr hdr Where (1=1) " & strWhere & _
                     " order by hdr.so_ct asc"
            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy danh sách header của chứng từ từ CSD")
            'LogDebug.Writelog("Có lỗi xảy ra khi lấy danh sách header của chứng từ từ CSDL: " & ex.ToString)
            Throw ex
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return dsChungTu
    End Function

    Public Function LoadDataDetail(ByVal key As infChungTuHDR) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chi tiết của chứng từ.
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng 
        ' ---------------------------------------------------- 
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Try
            cnChungTu = New DataAccess
            Dim strSQL As String
            strSQL = "select ma_cap,ma_chuong,ma_loai,ma_khoan,ma_muc,ma_tmuc, " & _
                     "ma_tldt,noi_dung,sotien,ky_thue " & _
                     "from TCS_CTU_DTL " & _
                     "Where (shkb = '" & key.SHKB & "') And (ngay_kb = " & key.Ngay_KB & ") and (ma_nv = " & key.Ma_NV & ") " & _
                     "And (so_bt = " & key.So_BT & ") And (ma_dthu = '" & key.Ma_DThu & "')"
            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ")
            'LogDebug.Writelog("Có lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString)
            Throw ex
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return dsChungTu
    End Function

    Public Function LoadDataDetailCQT(ByVal strSHKB As String, ByVal strNgayKB As String, _
                                 ByVal strMaNV As String, ByVal strSoBT As String, ByVal strMaDThu As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chi tiết của chứng từ.
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng 
        ' ---------------------------------------------------- 
        Dim cnChungTu As DataAccess
        Dim dsChungTu As DataSet
        Try
            cnChungTu = New DataAccess
            Dim strSQL As String
            strSQL = "select ma_cap,ma_chuong,ma_loai,ma_khoan,ma_muc,ma_tmuc, " & _
                     "sotien,ky_thue " & _
                     "from kx_cqt_dtl  " & _
                     "Where (shkb ='" & strSHKB & "') And (ngay_kb = " & strNgayKB & ") and (ma_nv = " & strMaNV & ") " & _
                     "And (so_bt = " & strSoBT & ") And (ma_dthu ='" & strMaDThu & "')"
            dsChungTu = cnChungTu.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ")
            'LogDebug.Writelog("Có lỗi xảy ra khi thông tin chi tiết chứng từ trong quá trình kết xuất chứng từ: " & ex.ToString)
            Throw ex
        Finally
            If Not cnChungTu Is Nothing Then cnChungTu.Dispose()
        End Try
        Return dsChungTu
    End Function

    Public Function LoadTK() As Object
        '-----------------------------------------------------------------
        ' Mục đích: Lấy danh sách tài khoản có, tài khoản nợ
        ' Tham số:
        ' Giá trị trả về:
        ' Ngày viết: 02/11/2007
        ' Người viết: Ngô Minh Trọng
        ' ----------------------------------------------------------------
        Dim strSQL As String = "Select TK_NO, TK_CO FROM TCS_MAP_TK_CTU"
        Dim cnTaiKhoan As DataAccess
        Dim dsTaiKhoan As DataSet

        Try
            cnTaiKhoan = New DataAccess
            dsTaiKhoan = cnTaiKhoan.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy danh sách tài khoản có, tài khoản nợ")
            Throw ex
            'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
        Finally
            If Not cnTaiKhoan Is Nothing Then cnTaiKhoan.Dispose()
        End Try

        Return dsTaiKhoan
    End Function
#End Region
#Region "Sửa chứng từ ngoài VP"
    Public Function CTU_TraCuuNVP(ByVal strWhere As String, ByVal strWhereSoBK As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Tra cứu chứng từ.
        ' Tham số: điều kiện tra cứu
        ' Giá trị trả về:
        ' Ngày viết: 26/10/2007
        ' Người viết: Nguyễn Hữu Hoan
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select distinct hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BTHU, hdr.TTIEN, hdr.TRANG_THAI, hdr.SO_BT, hdr.Ma_NNThue, hdr.Ten_NNThue, hdr.MA_NV, hdr.Lan_In " & _
                         " from TCS_CTU_THOP_HDR hdr, TCS_CTU_THOP_DTL dtl " & _
                         "where (hdr.shkb = dtl.shkb) and (hdr.ngay_kb = dtl.ngay_kb) and (hdr.ma_nv = dtl.ma_nv) and " & _
                         "(hdr.so_bt = dtl.so_bt) and (hdr.ma_dthu = dtl.ma_dthu) " & _
                         strWhere & strWhereSoBK

            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi tra cứu chứng từ")
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Function CTU_Header_SetNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chung của chứng từ
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV," & _
                           "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                           "a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
                           "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                           "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
                           "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
                           "to_char(a.Ngay_QD,'dd/MM/rrrr') as Ngay_QD,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
                           "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
                           "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
                           "a.So_TK,to_char(a.Ngay_TK,'dd/MM/rrrr') as Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                           "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                           "a.So_Khung,a.So_May,a.TK_KH_NH,to_char(a.NGAY_KH_NH,'dd/MM/rrrr') as NGAY_KH_NH," & _
                           "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
                           "a.Lan_In,a.Trang_Thai,a.so_phieu_kt, " & _
                           "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, " & _
                           "nha.ten as TEN_NH_A, nhb.ten as TEN_NH_B, " & _
                           "xa.ten as TenXa, tkno.ten_tk as Ten_TKNo, tkco.ten_tk as Ten_TKCo, " & _
                           "cqthu.ten as Ten_CQThu, nt.ten as Ten_NT, lt.ten as Ten_LThue " & _
                           "from TCS_CTU_THOP_HDR a, TCS_DM_XA xa, TCS_DM_TAIKHOAN tkno, TCS_DM_TAIKHOAN tkco, " & _
                           "TCS_DM_CQTHU cqthu, TCS_DM_NGUYENTE nt, TCS_DM_LTHUE lt, TCS_DM_NGANHANG nha, TCS_DM_NGANHANG nhb " & _
                           "where (a.xa_id = xa.xa_id(+))" & _
                           "and (a.tk_no = tkno.tk (+)) and (a.tk_co = tkco.tk(+)) and (a.ma_nh_a = nha.ma(+)) and (a.ma_nh_b = nhb.ma(+)) " & _
                           "and (a.ma_cqthu = cqthu.ma_cqthu(+)) and (a.ma_nt = nt.ma_nt(+)) and (a.ma_lthue = lt.ma_lthue(+)) " & _
                           "and (a.KYHIEU_CT = '" & strKYHIEU_CT & "') " & _
                           "and (a.SO_CT = '" & strSO_CT & "') "
            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy thông tin chung chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Function CTU_ChiTiet_SetNVP(ByVal keyCT As KeyCTu) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chi tiết của chứng từ.
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                    "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                    "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT " & _
                    "from TCS_CTU_THOP_DTL b " & _
                    "where (shkb = '" & keyCT.SHKB & "') and (ngay_kb =" & keyCT.Ngay_KB & ") " & _
                    "and (ma_nv = " & keyCT.Ma_NV & ") and (so_bt = " & keyCT.So_BT & ") " & _
                    "and (ma_dthu = '" & keyCT.Ma_Dthu & "')"

            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra khi lấy thông tin chi tiết chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Function TCS_BK_CTU_TRACUUNVP(ByVal strSoCTList As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Truy vấn BK CTU đưa ra DataSet để gắn vào report
        ' Tham số: 
        ' Giá trị trả về:
        ' Ngày viết: 16/11/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------
        Dim conn As DataAccess
        Dim strSQL As String
        Try
            conn = New DataAccess

            strSQL = "select hdr.tk_co as TK_Co , hdr.ma_nnthue as Ma_NNT, hdr.ten_nnthue as Ten_NNT, " & _
                            "hdr.kyhieu_ct as Kyhieu_CT, hdr.so_ct as So_CT, " & _
                            "(hdr.ma_tinh||'.'||hdr.ma_huyen||'.'||hdr.ma_xa) as Ma_DB, " & _
                            "hdr.ngay_kb as Ngay_KB, " & _
                            "hdr.so_bthu as Ma_BT, hdr.ttien as Tien , hdr.tt_tthu as So_TThu, " & _
                            "hdr.tk_no as TK_No " & _
                            "from TCS_CTU_THOP_HDR hdr " & _
                            "where (hdr.kyhieu_ct||hdr.so_ct) in (" & strSoCTList & ") " & _
                            "order by hdr.kyhieu_ct, hdr.so_ct "
            Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi truy vấn BK CTU đưa ra DataSet để gắn vào report")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(conn)) Then conn.Dispose()
        End Try
    End Function
    Public Function PhieuXN_HeaderNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chung của chứng từ để in phiếu xác nhận
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 01/12/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select Ma_DThu, Ngay_KB, KyHieu_CT, So_CT, " & _
                "Ma_NNThue, Ten_NNThue, DC_NNThue, TTien " & _
                "from TCS_CTU_THOP_HDR " & _
                "where (KYHIEU_CT = '" & strKYHIEU_CT & "') " & _
                "and (SO_CT = '" & strSO_CT & "') "
            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin của chứng từ để in phiếu xác nhận")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function

    Public Function PhieuXN_ChiTietNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chi tiết của chứng từ để in phiếu xác nhận
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 01/12/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select dtl.ma_cap as Cap, dtl.ma_chuong as Chuong, dtl.ma_loai as Loai, " & _
                    "dtl.ma_khoan as Khoan, dtl.ma_muc as Muc, dtl.ma_tmuc as TMuc, " & _
                    "dtl.noi_dung as NoiDung, dtl.sotien as SoTien, dtl.ky_thue as KyThue  " & _
                    "from tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " & _
                    "where (hdr.shkb = dtl.shkb ) and (hdr.ngay_kb = dtl.ngay_kb ) and (hdr.ma_nv =dtl.ma_nv ) " & _
                    "and (hdr.so_bt = dtl.so_bt ) and (hdr.ma_dthu = hdr.ma_dthu)  " & _
                    "and (hdr.kyhieu_ct = '" & strKYHIEU_CT & "') and (hdr.so_ct = '" & strSO_CT & "') "

            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin chi tiết của chứng từ để in phiếu xác nhận")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Sub CTU_DTL_UpdateNVP(ByVal dtl As infChungTuDTL)
        '-----------------------------------------------------
        ' Mục đích: Cập nhật thông tin chi tiết chứng từ.
        ' Tham số: 
        ' Giá trị trả về:
        ' Ngày viết: 31/10/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------   
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "update TCS_CTU_THOP_DTL set " & _
                "CCH_ID = " & dtl.CCH_ID & ", " & _
                "MA_CAP = '" & dtl.Ma_Cap & "', " & _
                "MA_CHUONG = '" & dtl.Ma_Chuong & "', " & _
                "LKH_ID = " & dtl.LKH_ID & ", " & _
                "MA_LOAI = '" & dtl.Ma_Loai & "', " & _
                "MA_KHOAN = '" & dtl.Ma_Khoan & "', " & _
                "MTM_ID = " & dtl.MTM_ID & ", " & _
                "MA_MUC = '" & dtl.Ma_Muc & "', " & _
                "MA_TMUC = '" & dtl.Ma_TMuc & "', " & _
                "DT_ID = " & dtl.DT_ID & ", " & _
                "MA_TLDT = '" & dtl.Ma_TLDT & "', " & _
                "NOI_DUNG = '" & dtl.Noi_Dung & "', " & _
                "SOTIEN_NT = '" & dtl.SoTien_NT & "', " & _
                "SOTIEN = " & dtl.SoTien & ", " & _
                "KY_THUE = '" & dtl.Ky_Thue & "' " & _
                "where ID = " & dtl.ID
            cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin chi tiết của chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Sub
#End Region


    Public Function CTU_HDR_SOBT_KTKB(ByVal lngSoPKT As Long, ByVal lngNgay As Long) As String
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin số bút toán KTKB của chứng từ 
        ' Ngày viết: 21/09/2008
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String = "select so_bt_ktkb from tcs_ctu_thop_hdr " & _
                                "where (so_phieu_kt = " & lngSoPKT & ") " & _
                                "and (ngay_kb = " & lngNgay & ") "
            Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
            Dim strReturn As String = "0"
            If (dr.Read()) Then
                strReturn = GetString(dr(0))
            End If

            dr.Close()
            Return strReturn
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin số bút toán KTKB của chứng từ ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function

End Class

