Namespace TDDMDC
    Public Class infMucTMuc
        Private strMTM_ID As String
        Private strMA_MUC As String
        Private strMA_TMUC As String
        Private strTen As String
        Private strNGAY_BD As String
        Private strNGAY_KT As String
        Private strGHICHU As String

        Sub New()
        End Sub

        Sub New(ByVal drwMucTMuc As DataRow)
            strMTM_ID = drwMucTMuc("MTM_ID").ToString().Trim()
            strMA_MUC = drwMucTMuc("MA_MUC").ToString().Trim()
            strMA_TMUC = drwMucTMuc("MA_TMUC").ToString().Trim()
            strTen = drwMucTMuc("Ten").ToString().Trim()
            strNGAY_BD = drwMucTMuc("NGAY_BD").ToString().Trim()
            strNGAY_KT = drwMucTMuc("NGAY_KT").ToString().Trim()
            strGHICHU = drwMucTMuc("GHI_CHU").ToString().Trim()
        End Sub

        Public Property MTM_ID() As String
            Get
                Return strMTM_ID
            End Get
            Set(ByVal Value As String)
                strMTM_ID = Value
            End Set
        End Property

        Public Property MA_MUC() As String
            Get
                Return strMA_MUC
            End Get
            Set(ByVal Value As String)
                strMA_MUC = Value
            End Set
        End Property

        Public Property MA_TMUC() As String
            Get
                Return strMA_TMUC
            End Get
            Set(ByVal Value As String)
                strMA_TMUC = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property

        Public Property NGAY_BD() As String
            Get
                Return strNGAY_BD
            End Get
            Set(ByVal Value As String)
                strNGAY_BD = Value
            End Set
        End Property

        Public Property NGAY_KT() As String
            Get
                Return strNGAY_KT
            End Get
            Set(ByVal Value As String)
                strNGAY_KT = Value
            End Set
        End Property

        Public Property GHICHU() As String
            Get
                Return strGHICHU
            End Get
            Set(ByVal Value As String)
                strGHICHU = Value
            End Set
        End Property
    End Class
End Namespace
