﻿Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Namespace HeThong

    Public Class daNhom
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Sub Insert(ByVal objNhom As infNhom)
            Dim conn As New DataAccess
            Dim strSql As String
            Try
                strSql = "Insert Into TCS_DM_NHOM " & _
                             "(MA_NHOM,TEN_NHOM, ID)" & _
                             "Values " & _
                             "('" & objNhom.Ma_Nhom & "','" & objNhom.Ten & "', tcs_nhm_id_seq.nextval)"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình thêm mới dữ liệu nhóm")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                conn.Dispose()
            End Try
        End Sub

        Public Sub Del(ByVal objNhom As infNhom)
            Dim conn As New DataAccess
            Dim strSql As String
            Try
                strSql = "Delete from TCS_DM_NHOM " & _
                         "Where Ma_Nhom = '" & objNhom.Ma_Nhom & "'"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình xóa dữ liệu nhóm")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                conn.Dispose()
            End Try
        End Sub

        Public Function SelectItem(ByVal Nhom As infNhom) As infNhom
            Dim objNhom As infNhom
            Dim conn As New DataAccess
            Dim ds As New DataSet
            Dim dt As DataRow
            Dim strSql As String
            Try
                strSql = "Select Ma_Nhom,Ten_Nhom From TCS_DM_NHOM " & _
                         " Where Ma_nhom = '" & Nhom.Ma_Nhom & "'"
                ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                objNhom = New infNhom
                If ds.Tables(0).Rows.Count > 0 Then
                    objNhom.Ma_Nhom = dt.Item(0).ToString()
                    objNhom.Ten = dt.Item(1).ToString()
                End If
                Return objNhom
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin Nhóm")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                conn.Dispose()
            End Try
        End Function

        Public Function SelectAll() As DataSet
            Dim conn As New DataAccess
            Dim strSql As String
            Try
                strSql = "Select Ma_Nhom,Ten_Nhom From TCS_DM_NHOM"
                Return conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin nhóm")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                conn.Dispose()
            End Try
        End Function
    End Class

End Namespace