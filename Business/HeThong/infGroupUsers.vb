Namespace HeThong
    Public Class infGroupUsers
        Private _MaNhom As String
        Private _TenNhom As String
        Private _GroupPermission As String

        Public Property GroupPermission() As String
            Get
                Return _GroupPermission
            End Get
            Set(ByVal value As String)
                _GroupPermission = value
            End Set
        End Property

        Sub New()
        End Sub

        Sub New(ByVal drGroupUsers As DataRow)
            _MaNhom = drGroupUsers("MaNhom").ToString().Trim()
            _TenNhom = drGroupUsers("TenNhom").ToString().Trim()
        End Sub


        Public Property MaNhom() As String
            Get
                Return _MaNhom
            End Get
            Set(ByVal value As String)
                _MaNhom = value
            End Set
        End Property

        Public Property TenNhom() As String
            Get
                Return _TenNhom
            End Get
            Set(ByVal value As String)
                _TenNhom = value
            End Set
        End Property

    End Class
End Namespace
