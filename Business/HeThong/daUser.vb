﻿Imports VBOracleLib
Imports GlobalProjects

Namespace HeThong
    Public Class daUser
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function check_MaCN(ByVal ma_cn As String) As Boolean
            Dim conn As DataAccess
            Dim strSql As String
            Dim dts As DataSet
            Dim flag As Boolean = False
            Try
                strSql = "select * from TCS_DM_CHINHANH where id='" & ma_cn & "'"
                dts = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                If dts.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Function Insert(ByVal objUser As infUser) As Boolean
            Dim blnResult As Boolean = True
            Dim cnUser As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction

            Dim ma_NV As String = Common.mdlCommon.getDataKey("TCS_NHANVIEN_ID_SEQ.NEXTVAL")
            'Dim his_ID As String = Common.mdlCommon.getDataKey("HIS_TCS_DM_NHANVIEN_ID.NEXTVAL")
            Try
                cnUser = New DataAccess

                transCT = cnUser.BeginTransaction
                strSql = "INSERT INTO TCS_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,UserDomain,BAN_LV,MA_NV_TAO) " & _
                                "VALUES (" & ma_NV & " ,'" & _
                                objUser.Ten & "','" & _
                                objUser.Mat_Khau & "','" & _
                                objUser.Ma_Nhom & "','" & _
                                objUser.Chuc_Danh & "','" & _
                                objUser.Trang_thai & "','" & _
                                objUser.TEN_ND & "','" & _
                                objUser.Teller_Id & "','" & _
                                objUser.MaCN & "'," & _
                                objUser.LDAP & ",'" & _
                                objUser.HanMuc_Tu & "','" & _
                                objUser.HanMuc_Den & "','" & _
                                objUser.User_Domain & "'," & _
                                Globals.EscapeQuote(objUser.Ban_Lv) & ", " & objUser.MA_NV_TAO & " )"

                cnUser.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                ' INSERT BẢNG LOG
                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,UserDomain,BAN_LV, NGAY_HT,MA_NV_TAO) " & _
                               "VALUES (" & ma_NV & "  ,'" & _
                                objUser.Ten & "','" & _
                                objUser.Mat_Khau & "','" & _
                                objUser.Ma_Nhom & "','" & _
                                objUser.Chuc_Danh & "','" & _
                                objUser.Trang_thai & "','" & _
                                objUser.TEN_ND & "','" & _
                                objUser.Teller_Id & "','" & _
                                objUser.MaCN & "'," & _
                                objUser.LDAP & ",'" & _
                                objUser.HanMuc_Tu & "','" & _
                                objUser.HanMuc_Den & "','" & _
                                objUser.User_Domain & "'," & _
                                Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                " SYSDATE, " & objUser.MA_NV_TAO & ")"

                cnUser.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                transCT.Commit()
            Catch ex As Exception
                blnResult = False
                transCT.Rollback()
                Throw ex
            Finally
                If Not cnUser Is Nothing Then
                    transCT.Dispose()
                    cnUser.Dispose()
                End If
            End Try
            Return blnResult
        End Function

        Public Function InsertUserTemp(ByVal objUser As infUser, ByVal strAction As String) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim action_ID As String = Common.mdlCommon.getDataKey("TCS_DM_NHANVIEN_TEMP_SEQ.NEXTVAL")
            Try
                conn = New DataAccess
                strSql = "INSERT INTO TCS_DM_NHANVIEN_TEMP (ACTION_ID,MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,UserDomain,BAN_LV,MA_NV_TAO,ACTION,ACTION_TIME) " & _
                            "VALUES ( " & action_ID & "," & objUser.Ma_NV & " ,'" & _
                            objUser.Ten & "','" & _
                            objUser.Mat_Khau & "','" & _
                            objUser.Ma_Nhom & "','" & _
                            objUser.Chuc_Danh & "','" & _
                            objUser.Trang_thai & "','" & _
                            objUser.TEN_ND & "','" & _
                            objUser.Teller_Id & "','" & _
                            objUser.MaCN & "'," & _
                            objUser.LDAP & ",'" & _
                            objUser.HanMuc_Tu & "','" & _
                            objUser.HanMuc_Den & "','" & _
                            objUser.User_Domain & "'," & _
                            Globals.EscapeQuote(objUser.Ban_Lv) & ",'" _
                            & objUser.MA_NV_TAO & "','" & strAction & "',SYSDATE)"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog("Loi khi thay doi thong tin User [" & objUser.TEN_ND & "]. Error: " & ex.ToString(), EventLogEntryType.Error)
                blnResult = False
            End Try
            Return blnResult
        End Function

        Public Function ApproveUserTemp(ByVal strAction_ID As String, ByVal strMa_NV_DUYET As String) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim objUser As New infUser
            Try
                conn = New DataAccess

                Dim dtUserTemp As DataTable = conn.ExecuteToTable("SELECT * FROM TCS_DM_NHANVIEN_TEMP WHERE ACTION_ID='" & strAction_ID & "'")

                If dtUserTemp.Rows.Count > 0 Then
                    objUser.Ma_NV = dtUserTemp.Rows(0)("ma_nv").ToString()
                    objUser.Ten = dtUserTemp.Rows(0)("TEN").ToString()
                    objUser.Mat_Khau = dtUserTemp.Rows(0)("MAT_KHAU").ToString()
                    objUser.Trang_thai = dtUserTemp.Rows(0)("TINH_TRANG").ToString()
                    objUser.Ban_Lv = dtUserTemp.Rows(0)("BAN_LV").ToString()
                    objUser.Chuc_Danh = dtUserTemp.Rows(0)("CHUC_DANH").ToString()
                    objUser.TEN_ND = dtUserTemp.Rows(0)("TEN_DN").ToString()
                    objUser.Teller_Id = dtUserTemp.Rows(0)("TELLER_ID").ToString()
                    objUser.MaCN = dtUserTemp.Rows(0)("MA_CN").ToString()
                    objUser.LDAP = dtUserTemp.Rows(0)("LDAP").ToString()
                    objUser.Ma_Nhom = dtUserTemp.Rows(0)("MA_NHOM").ToString()
                    objUser.HanMuc_Tu = dtUserTemp.Rows(0)("HANMUC_TU").ToString()
                    objUser.HanMuc_Den = dtUserTemp.Rows(0)("HANMUC_DEN").ToString()
                    objUser.User_Domain = dtUserTemp.Rows(0)("USERDOMAIN").ToString()
                    objUser.MA_NV_TAO = dtUserTemp.Rows(0)("MA_NV_TAO").ToString()
                    objUser.MA_NV_SUA = dtUserTemp.Rows(0)("MA_NV_TAO").ToString()
                    If dtUserTemp.Rows(0)("LAN_DN_SAI").ToString() <> "" Then
                        objUser.Lan_DN_SAI = Integer.Parse(dtUserTemp.Rows(0)("LAN_DN_SAI").ToString())
                    Else
                        objUser.Lan_DN_SAI = 0
                    End If

                    objUser.Da_Duyet = dtUserTemp.Rows(0)("DA_DUYET").ToString()
                    Dim strAction As String = dtUserTemp.Rows(0)("ACTION").ToString()

                    Select Case strAction
                        Case "INSERT"
                            Insert(objUser)
                        Case "UPDATE"
                            Dim strMa_CN_OLD As String = conn.ExecuteSQLScalar("SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & objUser.Ma_NV & "'")
                            Dim strMa_CN_NEW As String = dtUserTemp.Rows(0)("MA_CN").ToString()

                            If strMa_CN_OLD <> strMa_CN_NEW Then
                                If CheckExits(objUser.TEN_ND, "", objUser.MaCN) Then
                                    buAddUser.DoiCN_EXITS(objUser)
                                Else
                                    buAddUser.DoiTen(objUser, True)
                                End If
                            Else
                                buAddUser.DoiTen(objUser, False)
                            End If
                            Update_WrongLogInCount(objUser.Ma_NV, "0")
                        Case "DELETE"
                            Delete(objUser, "1")
                    End Select

                    strSql = "UPDATE TCS_DM_NHANVIEN_TEMP SET APPROVE_TIME=SYSDATE,DA_DUYET=1,MA_NV_DUYET='" & strMa_NV_DUYET & "' WHERE ACTION_ID='" & strAction_ID & "'"
                    conn.ExecuteNonQuery(strSql, CommandType.Text)

                    'strSql = "DELETE TCS_DM_NHANVIEN_TEMP WHERE ACTION_ID='" & strAction_ID & "'"
                    'conn.ExecuteNonQuery(strSql, CommandType.Text)
                End If
            Catch ex As Exception
                blnResult = False
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function

        Private Function CheckExits(ByVal strTen_DN As String, Optional ByVal strTrangThai As String = "", Optional ByVal strMaCN As String = "") As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim dr As IDataReader
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "SELECT * FROM TCS_DM_NHANVIEN " & _
                        "WHERE ten_DN ='" & strTen_DN & "' "
                If strTrangThai <> "" Then
                    strSql = strSql & "and TINH_TRANG = '" & strTrangThai & "'"
                End If
                If strMaCN <> "" Then
                    strSql = strSql & " and ma_cn ='" & strMaCN & "'"
                End If
                dr = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
                If dr.Read Then
                    blnResult = True
                Else
                    blnResult = False
                End If
            Catch ex As Exception
                blnResult = False
                Throw ex
            Finally
                If Not dr Is Nothing Then
                    dr.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objUser As infUser, ByVal strTrangThai As String) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction
                'transCT.Connection = conn.GetConnection()
                If CheckUSer_GD(objUser.Ma_NV) = "1" Then
                    strSql = "UPDATE TCS_DM_NHANVIEN SET TINH_TRANG='" & strTrangThai & "' " & _
                                             "WHERE Ma_NV=" & objUser.Ma_NV
                Else
                    strSql = "DELETE FROM  TCS_DM_NHANVIEN WHERE Ma_NV=" & objUser.Ma_NV
                End If
                '/// Không xóa user mà chỉ cập nhật trạng thái lock user

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                ' INSERT BẢNG LOG
                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,BAN_LV, NGAY_HT) " & _
                               " (SELECT MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,'" & strTrangThai & "',TEN_DN,TELLER_ID,MA_CN,BAN_LV, SYSDATE FROM TCS_DM_NHANVIEN " & _
                               "  WHERE MA_NV=" & objUser.Ma_NV & ")"

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                transCT.Commit()
            Catch ex As Exception
                blnResult = False
                transCT.Rollback()
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    transCT.Dispose()
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function

        Public Function Approve(ByVal strMa_NV As String, ByVal strMa_NV_Approve As String) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                transCT = conn.BeginTransaction
                conn = New DataAccess()
                strSql = "UPDATE TCS_DM_NHANVIEN SET DA_DUYET='1' WHERE Ma_NV='" & strMa_NV & "'"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                Dim his_ID = Common.mdlCommon.getDataKey("HIS_TCS_DM_NHANVIEN_ID.NEXTVAL")
                strSql = "INSERT INTO HIS_TCS_DM_NHANVIEN (HIS_ID,NGUOI_THUC_HIEN,MA_NV,TT_XU_LY,NGAY_THUC_HIEN,GHI_CHU,TYPE) " & _
                "VALUES (" & his_ID & ",'" & strMa_NV_Approve & "','" & strMa_NV & "'," & "'APPROVED',SYSDATE," & "'Duyet tai khoan dang nhap','APPROVE')"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                transCT.Commit()
            Catch ex As Exception
                blnResult = False
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        Public Function DoiTen(ByVal objUser As infUser, Optional ByVal tt_doiCN As Boolean = False) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                conn = New DataAccess
                Dim ma_NV As String = Common.mdlCommon.getDataKey("TCS_NHANVIEN_ID_SEQ.NEXTVAL")
                transCT = conn.BeginTransaction
                If tt_doiCN Then
                    If CheckUSer_GD(objUser.Ma_NV) = "1" Then
                        strSql = "UPDATE TCS_DM_NHANVIEN SET TINH_TRANG='1', MA_NV_SUA = " & objUser.MA_NV_SUA & _
                                                 " WHERE Ma_NV=" & objUser.Ma_NV
                    Else
                        strSql = "DELETE FROM  TCS_DM_NHANVIEN WHERE Ma_NV=" & objUser.Ma_NV
                    End If
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                    strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV, NGAY_HT, MA_NV_SUA) " & _
                              "VALUES (" & objUser.Ma_NV & "  ,'" & _
                              objUser.Ten & "','" & _
                              objUser.Mat_Khau & "','" & _
                              objUser.Ma_Nhom & "','" & _
                              objUser.Chuc_Danh & "','1','" & _
                              objUser.TEN_ND & "','" & _
                              objUser.Teller_Id & "','" & _
                              objUser.MaCN & "'," & _
                              objUser.LDAP & ",'" & _
                              objUser.HanMuc_Tu & "','" & _
                              objUser.HanMuc_Den & "','" & _
                              objUser.User_Domain & "'," & _
                              Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                              " SYSDATE, " & objUser.MA_NV_SUA & ")"
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                    objUser.Ma_NV = ma_NV
                    strSql = "INSERT INTO TCS_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV, MA_NV_SUA) " & _
                               "VALUES (" & objUser.Ma_NV & " ,'" & _
                               objUser.Ten & "','" & _
                               objUser.Mat_Khau & "','" & _
                               objUser.Ma_Nhom & "','" & _
                               objUser.Chuc_Danh & "','" & _
                               objUser.Trang_thai & "','" & _
                               objUser.TEN_ND & "','" & _
                               objUser.Teller_Id & "','" & _
                               objUser.MaCN & "'," & _
                               objUser.LDAP & ",'" & _
                                objUser.HanMuc_Tu & "','" & _
                                objUser.HanMuc_Den & "','" & _
                                objUser.User_Domain & "'," & _
                               Globals.EscapeQuote(objUser.Ban_Lv) & ", " & objUser.MA_NV_SUA & " )"

                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                    ' INSERT BẢNG LOG
                    strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV, NGAY_HT, MA_NV_SUA) " & _
                                   "VALUES (" & objUser.Ma_NV & "  ,'" & _
                                   objUser.Ten & "','" & _
                                   objUser.Mat_Khau & "','" & _
                                   objUser.Ma_Nhom & "','" & _
                                   objUser.Chuc_Danh & "','" & _
                                   objUser.Trang_thai & "','" & _
                                   objUser.TEN_ND & "','" & _
                                   objUser.Teller_Id & "','" & _
                                   objUser.MaCN & "'," & _
                                   objUser.LDAP & ",'" & _
                                    objUser.HanMuc_Tu & "','" & _
                                    objUser.HanMuc_Den & "','" & _
                                    objUser.User_Domain & "'," & _
                                   Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                   " SYSDATE, " & objUser.MA_NV_SUA & ")"
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                Else
                    strSql = "UPDATE TCS_DM_NHANVIEN " & _
                                           "SET TEN = '" & objUser.Ten & "' " & _
                                           ",CHUC_DANH='" & objUser.Chuc_Danh & "' " & _
                                           ",TELLER_ID='" & objUser.Teller_Id & "' " & _
                                           ",Ma_CN='" & objUser.MaCN & "' " & _
                                           ",Ma_Nhom='" & objUser.Ma_Nhom & "' " & _
                                           ",Mat_Khau = '" & objUser.Mat_Khau & "'" & _
                                           ",BAN_LV='" & objUser.Ban_Lv & "' " & _
                                           ",LDAP=" & objUser.LDAP & _
                                           ",hanmuc_tu='" & objUser.HanMuc_Tu & _
                                           "',hanmuc_den='" & objUser.HanMuc_Den & _
                                           "',USERDOMAIN='" & objUser.User_Domain & _
                                           "',TINH_TRANG='" & objUser.Trang_thai & _
                                           "',MA_NV_SUA='" & objUser.MA_NV_SUA & "' " & _
                                           "WHERE Ma_NV =" & objUser.Ma_NV
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                    strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV, NGAY_HT, MA_NV_SUA) " & _
                                                                     "VALUES (" & objUser.Ma_NV & "  ,'" & _
                                                                     objUser.Ten & "','" & _
                                                                     objUser.Mat_Khau & "','" & _
                                                                     objUser.Ma_Nhom & "','" & _
                                                                     objUser.Chuc_Danh & "','" & _
                                                                     objUser.Trang_thai & "','" & _
                                                                     objUser.TEN_ND & "','" & _
                                                                     objUser.Teller_Id & "','" & _
                                                                     objUser.MaCN & "'," & _
                                                                     objUser.LDAP & ",'" & _
                                                                    objUser.HanMuc_Tu & "','" & _
                                                                    objUser.HanMuc_Den & "','" & _
                                                                    objUser.User_Domain & "'," & _
                                                                     Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                                                     " SYSDATE, " & objUser.MA_NV_SUA & ")"
                    conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                End If

                transCT.Commit()
            Catch ex As Exception
                blnResult = False
                transCT.Rollback()
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                log.Error(ex.Message & "-" & ex.StackTrace) '
            Finally
                If Not conn Is Nothing Then
                    transCT.Dispose()
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function DoiCN_EXITS(ByVal objUser As infUser) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                conn = New DataAccess
                Dim ma_NV As String = Common.mdlCommon.getDataKey("TCS_NHANVIEN_ID_SEQ.NEXTVAL")
                transCT = conn.BeginTransaction
                ' If tt_doiCN Then
                If CheckUSer_GD(objUser.Ma_NV) = "1" Then
                    strSql = "UPDATE TCS_DM_NHANVIEN SET TINH_TRANG='1', MA_NV_SUA= " & objUser.MA_NV_SUA & _
                                             " WHERE Ma_NV=" & objUser.Ma_NV
                Else
                    strSql = "DELETE FROM  TCS_DM_NHANVIEN WHERE Ma_NV=" & objUser.Ma_NV
                End If
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN,BAN_LV, NGAY_HT, MA_NV_SUA) " & _
                                              "VALUES (" & objUser.Ma_NV & "  ,'" & _
                                              objUser.Ten & "','" & _
                                              objUser.Mat_Khau & "','" & _
                                              objUser.Ma_Nhom & "','" & _
                                              objUser.Chuc_Danh & "','1','" & _
                                              objUser.TEN_ND & "','" & _
                                              objUser.Teller_Id & "','" & _
                                              objUser.MaCN & "'," & _
                                              objUser.LDAP & ",'" & _
                                              objUser.HanMuc_Tu & "','" & _
                                              objUser.HanMuc_Den & "','" & _
                                              objUser.User_Domain & "'," & _
                                              Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                              " SYSDATE, " & objUser.MA_NV_SUA & ")"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)


                strSql = "UPDATE TCS_DM_NHANVIEN " & _
                                       "SET TEN = '" & objUser.Ten & "' " & _
                                       ",CHUC_DANH='" & objUser.Chuc_Danh & "' " & _
                                       ",TELLER_ID='" & objUser.Teller_Id & "' " & _
                                       ",Ma_CN='" & objUser.MaCN & "' " & _
                                       ",Ma_Nhom='" & objUser.Ma_Nhom & "' " & _
                                       ",Mat_Khau = '" & objUser.Mat_Khau & "'" & _
                                       ",BAN_LV='" & objUser.Ban_Lv & "' " & _
                                       ",LDAP=" & objUser.LDAP & _
                                       ",hanmuc_tu='" & objUser.HanMuc_Tu & _
                                       "',hanmuc_den='" & objUser.HanMuc_Den & _
                                       "',USERDOMAIN='" & objUser.User_Domain & _
                                       "',TINH_TRANG='0' " & _
                                       "',MA_NV_SUA=" & objUser.MA_NV_SUA & _
                                       " WHERE MA_NV IN (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE TEN_DN='" & objUser.TEN_ND & "' " & _
                                        " AND MA_CN='" & objUser.MaCN & "')"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,LDAP,BAN_LV,hanmuc_tu,hanmuc_den,USERDOMAIN, NGAY_HT, MA_NV_SUA) " & _
                                                                 "VALUES (" & objUser.Ma_NV & "  ,'" & _
                                                                 objUser.Ten & "','" & _
                                                                 objUser.Mat_Khau & "','" & _
                                                                 objUser.Ma_Nhom & "','" & _
                                                                 objUser.Chuc_Danh & "','" & _
                                                                 objUser.Trang_thai & "','" & _
                                                                 objUser.TEN_ND & "','" & _
                                                                 objUser.Teller_Id & "','" & _
                                                                 objUser.MaCN & "'," & _
                                                                 objUser.LDAP & ",'" & _
                                                                 objUser.HanMuc_Tu & "','" & _
                                                                 objUser.HanMuc_Den & "','" & _
                                                                 objUser.User_Domain & "'," & _
                                                                 Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                                                                 " SYSDATE, " & objUser.MA_NV_SUA & ")"
                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)

                transCT.Commit()
            Catch ex As Exception
                blnResult = False
                transCT.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) ' '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
            Finally
                If Not conn Is Nothing Then
                    transCT.Dispose()
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function DoiMK(ByVal objUser As infUser) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Dim transCT As IDbTransaction
            Try
                conn = New DataAccess
                transCT = conn.BeginTransaction()
                strSql = "UPDATE TCS_DM_NHANVIEN " & _
                    " SET Mat_Khau = '" & objUser.Mat_Khau & "'" & _
                    " WHERE Ma_NV =" & objUser.Ma_NV

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                ' INSERT BẢNG LOG
                strSql = "INSERT INTO TCS_LOG_DM_NHANVIEN (MA_NV,TEN,MAT_KHAU,MA_NHOM,CHUC_DANH,TINH_TRANG,TEN_DN,TELLER_ID,MA_CN,BAN_LV, NGAY_HT) " & _
                               "VALUES (" & objUser.Ma_NV & "  ,'" & _
                               objUser.Ten & "','" & _
                               objUser.Mat_Khau & "','" & _
                               objUser.Ma_Nhom & "','" & _
                               objUser.Chuc_Danh & "','" & _
                               objUser.Trang_thai & "','" & _
                               objUser.TEN_ND & "','" & _
                               objUser.Teller_Id & "','" & _
                               objUser.MaCN & "'," & _
                               Globals.EscapeQuote(objUser.Ban_Lv) & "," & _
                               " SYSDATE)"

                conn.ExecuteNonQuery(strSql, CommandType.Text, transCT)
                transCT.Commit()
            Catch ex As Exception
                transCT.Rollback()
                blnResult = False
                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
            Finally
                If Not conn Is Nothing Then
                    transCT.Dispose()
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function UpdateBanLV(ByVal objUser As infUser, ByVal strBanLv As String, ByVal strIP As String) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "UPDATE TCS_DM_NHANVIEN " & _
                    "SET LOG_ON= '1',local_ip = '" & strIP & "' " & _
                    "WHERE Ma_NV =" & objUser.Ma_NV
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                blnResult = False
                 log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function DoiNhom(ByRef objUser As infUser) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Update TCS_DM_NHANVIEN " & _
                    "Set NH_ID = '" & objUser.Ma_Nhom & "' " & _
                    "Where MA_NV =" & objUser.Ma_NV
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                blnResult = False
               log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function SelectNhom() As DataSet
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "Select MA_NHOM,TEN_NHOM From TCS_DM_NHOM"
                Return conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function TCS_LST_DM_NHANVIEN() As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Try
                cnQNhanvien = New DataAccess
                strSQL = "SELECT N.Ma_NV,N.Ten,N.Chuc_Danh,N.Ban_LV, N.local_ip,Ten_DN,CASE tinh_trang  WHEN '0' THEN 'Hoạt động' ELSE 'Đang bị khóa'" & _
                "END TINH_TRANG FROM TCS_DM_NHANVIEN N WHERE TINH_TRANG= 0  Order by Ma_NV "
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function

        Public Function TCS_LST_DM_NHANVIEN_TEMP(Optional ByRef objUser As infUser = Nothing) As DataTable
            Dim conn As DataAccess
            Dim strSQL As String
            Dim where As String = ""
            Try
                conn = New DataAccess
                If Not objUser Is Nothing Then
                    If objUser.Chuc_Danh <> "" Then
                        where = where + " and UPPER(N.Chuc_Danh) like UPPER ('%" & objUser.Chuc_Danh & "%')"
                    End If
                    If objUser.Ten <> "" Then
                        where = where + " and UPPER(N.ten) like UPPER('%" & objUser.Ten & "%') "
                    End If
                    If objUser.TEN_ND <> "" Then
                        where = where + " and UPPER(N.ten_DN) like UPPER('%" & objUser.TEN_ND & "%')"
                    End If
                    If objUser.MaCN <> "" Then
                        where = where + " and UPPER(N.ma_CN) = UPPER('" & objUser.MaCN & "')"
                    End If
                    If objUser.Trang_thai <> "" Then
                        where = where + " and N.tinh_trang ='" & objUser.Trang_thai & "'"
                    End If
                    If objUser.Ma_Nhom <> "" Then
                        where = where + " and N.ma_nhom ='" & objUser.Ma_Nhom & "'"
                    End If
                    If objUser.LDAP <> "" Then
                        where = where + " and N.LDAP ='" & objUser.LDAP & "'"
                    End If
                    If Not objUser.MA_NV_TAO.Equals("") Then
                        where = where + " and N.ma_nv_tao <> '" & objUser.MA_NV_TAO & "'"
                    End If
                    If objUser.Da_Duyet <> "" Then
                        where = where + " and UPPER(N.DA_DUYET)='" & objUser.Da_Duyet & "'"
                    End If

                    If objUser.User_Domain <> "" Then
                        where = where + " and N.userdomain ='" & objUser.User_Domain & "'"
                    End If
                End If

                strSQL = "SELECT (select ten from tcs_dm_nhanvien where ma_nv=N.MA_NV_TAO) as NGUOI_TAO,ACTION_ID,MA_NV,MA_NHOM,TEN,MAT_KHAU,TINH_TRANG,BAN_LV,CHUC_DANH,LOCAL_IP,LOG_ON,TEN_DN,TELLER_ID,MA_CN,LDAP,HANMUC_TU,HANMUC_DEN,USERDOMAIN,MA_NV_TAO,LAN_DN_SAI,DA_DUYET,ACTION,MA_NV_DUYET,ACTION_TIME,APPROVE_TIME FROM TCS_DM_NHANVIEN_TEMP N WHERE N.DA_DUYET=0 AND N.APPROVE_TIME is null " & where & " order by N.ACTION_TIME desc"
                Return conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách người sử dụng tạm: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        Public Function TCS_LST_DM_NHANVIEN_DIEMTHU(Optional ByRef objUser As infUser = Nothing) As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Dim where As String = ""
            Try
                cnQNhanvien = New DataAccess
                If Not objUser Is Nothing Then
                    If objUser.Chuc_Danh <> "" Then
                        where = where + " and UPPER(N.Chuc_Danh) like UPPER ('%" & objUser.Chuc_Danh & "%')"
                    End If
                    If objUser.Ten <> "" Then
                        where = where + " and lower(N.ten) like lower('%" & objUser.Ten & "%') "
                    End If
                    If objUser.TEN_ND <> "" Then
                        where = where + " and lower(N.ten_DN) like lower('%" & objUser.TEN_ND & "%')"
                    End If
                    If objUser.MaCN <> "" Then
                        where = where + " and UPPER(N.ma_CN) = UPPER('" & objUser.MaCN & "')"
                    End If
                    If objUser.Trang_thai <> "" Then
                        where = where + " and N.tinh_trang ='" & objUser.Trang_thai & "'"
                    End If
                    If objUser.Ma_Nhom <> "" Then
                        where = where + " and N.ma_nhom ='" & objUser.Ma_Nhom & "'"
                    End If
                    If objUser.LDAP <> "" Then
                        where = where + " and N.LDAP ='" & objUser.LDAP & "'"
                    End If

                    If objUser.Da_Duyet <> "" Then
                        where = where + " and UPPER(N.DA_DUYET)='" & objUser.Da_Duyet & "'"
                    End If

                    If objUser.User_Domain <> "" Then
                        where = where + " and N.userdomain ='" & objUser.User_Domain & "'"
                    End If
                End If
                strSQL = "SELECT N.Ma_NV,N.Ten,N.Chuc_Danh,N.Ban_LV,N.TELLER_ID,N.ten_DN, N.local_ip,CASE n.tinh_trang  WHEN '0' THEN 'Đang HĐ' ELSE 'Ngung HĐ' " & _
                "END TINH_TRANG, D.TEN TEN_DTHU, N.Ma_CN||'-'||T.NAME Ma_CN FROM TCS_DM_NHANVIEN N,TCS_DM_DIEMTHU D, tcs_dm_chinhanh t  WHERE  N.BAN_LV=D.MA_DTHU " & _
                " AND T.branch_id= N.MA_CN " & where & "  Order by Ma_NV "
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function
        'hoapt: lay thong tin mot nhan vien
        Public Function NHANVIEN_DETAIL(ByVal Ma_NV As String) As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Try
                cnQNhanvien = New DataAccess
                strSQL = "SELECT MA_NV,TEN,CHUC_DANH,BAN_LV,MAT_KHAU,TELLER_ID, LOCAL_IP,MA_NHOM,TEN_DN,MA_CN,LDAP,hanmuc_tu,hanmuc_den,USERDOMAIN, TINH_TRANG,LAN_DN_SAI,MA_NV_SUA FROM TCS_DM_NHANVIEN WHERE Ma_NV='" & Ma_NV & "'"
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function
        'hoapt: lay thong tin mot nhan vien
        Public Function TIMKIEM_NHANVIEN(Optional ByVal Ma_NV As String = "", Optional ByVal Ma_CN As String = "") As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Try
                cnQNhanvien = New DataAccess
                strSQL = "SELECT a.ma_nv, a.ma_nhom,a.ten_dn,d.name ten_nh_cn, a.ten, a.ban_lv,a.chuc_danh,a.mat_khau,a.teller_id,a.ma_cn" _
                & " FROM tcs_dm_nhanvien a, tcs_dm_chinhanh d " _
                & " WHERE d.id = a.ma_cn and a.ma_cn = '" & Ma_CN & "' and log_on='1' and a.tinh_trang='0'"
                If Ma_NV.Trim <> "" Then
                    strSQL = strSQL & "AND upper(a.ten_dn) like '%" & Ma_NV.ToUpper & "%'"
                End If
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function
        'hoapt: lay thong tin mot nhan vien
        Public Function TIMKIEM_NHOMNV(ByVal Ma_NV As String) As DataSet
            Dim cnQNhanvien As DataAccess
            Dim strSQL As String
            Try
                cnQNhanvien = New DataAccess
                strSQL = "SELECT MA_NHOM FROM TCS_DM_NHANVIEN WHERE Ma_NV='" & Ma_NV & "'"
                Return cnQNhanvien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách người sử dụng: " & ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not cnQNhanvien Is Nothing Then
                    cnQNhanvien.Dispose()
                End If
            End Try
        End Function
        Public Function UpdateTTLogin(ByVal strMaNV As String, ByVal strTrangThai As String, Optional ByVal strIP_Local As String = "") As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess

                strSql = "UPDATE TCS_DM_NHANVIEN " & _
                    " SET LOG_ON = '" & strTrangThai & "'," & _
                    " LOCAL_IP='" & strIP_Local & "' " & _
                    " WHERE Ma_NV =" & strMaNV
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                blnResult = False
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function CheckUSer_GD(ByVal strMaNV As String) As String
            Dim blnResult As String = String.Empty
            Dim conn As DataAccess
            Dim strSql As String
            Dim ds As DataSet
            Try
                conn = New DataAccess

                strSql = "Select 1 from tcs_ctu_hdr where (ma_nv=" & strMaNV & " or ma_ks=" & strMaNV & ")"
                ds = DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
                If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                    blnResult = "1"
                Else
                    blnResult = "0"
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function Insert_Log_DN(Optional ByVal strMA_NV As String = "", Optional ByVal strTEN_DN As String = "", Optional ByVal strLOCAL_IP As String = "", Optional ByVal strSTATUS As String = "", Optional ByVal strMA_CN As String = "", Optional ByVal strUSER_KICK As String = "") As Boolean
            Dim blnResult As Boolean = True
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess

                strSql = "INSERT INTO TCS_LOG_DANGNHAP (MA_NV,TEN_DN,LOCAL_IP,ACTION_TIME,STATUS,MA_CN,USER_KICK) " & _
                    " VALUES (" & strMA_NV & "," & _
                    " '" & strTEN_DN & "', " & _
                    " '" & strLOCAL_IP & "', " & _
                    " SYSDATE, " & _
                    " " & strSTATUS & " ," & _
                    " '" & strMA_CN & "', " & _
                    " '" & strUSER_KICK & "' )"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                blnResult = False
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Function Update_WrongLogInCount(ByVal MA_NV As String, ByVal wrongCount As String)
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "update tcs_dm_nhanvien set lan_dn_sai='" + wrongCount + "' where ma_nv='" + MA_NV + "'"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        ''' <summary>
        ''' Cập nhật trạng thái tài khoản của nhân viên
        ''' </summary>
        ''' <param name="strMa_NV">Mã nhân viên</param>
        ''' <param name="strTinh_Trang">Tình trạng tài khoản đăng nhập</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function UpdateTinhTrang(ByVal strMa_NV, ByVal strTinh_Trang)
            Dim conn As DataAccess
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "UPDATE TCS_DM_NHANVIEN SET tinh_trang='" + strTinh_Trang + "' WHERE Ma_NV=" + strMa_NV
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)'
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

    End Class
End Namespace