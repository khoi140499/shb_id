Imports VBOracleLib

Namespace HeThong
    Public Class daGroupUsers
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Private _dac As DataAccess = New DataAccess

        'Hien dung Boolean len chua mo ta ro ma loi (True - False)
        'Co the thay bang Integer voi cac dinh nghia ma loi khac nhau
        'Vi du :
        '  - 0:Thanh cong, -1: Loi he thong, 1:Trung MaNhom .....
        Private _flag As Boolean = False

        Public Function Insert(ByVal objGroup As infGroupUsers) As Boolean
            Try
                Dim strSql As String
                If (CheckBeforeInsert(objGroup)) Then
                    strSql = "INSERT INTO TCS_DM_NHOM (MA_NHOM,TEN_NHOM) " & _
                                        "VALUES ('" & objGroup.MaNhom & "','" & objGroup.TenNhom & "')"
                Else
                    Return False
                End If

                _flag = IIf(DataAccess.ExecuteNonQuery(strSql, CommandType.Text) > 0, True, False)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi thêm mới nhóm người sử dụng")
                _flag = False
                Throw ex
            Finally
            End Try
            Return _flag
        End Function

        Public Function Update(ByVal objGroup As infGroupUsers) As Boolean
            Dim strSql As String = "UPDATE TCS_DM_NHOM SET TEN_NHOM='" & objGroup.TenNhom & "' WHERE MA_NHOM='" & objGroup.MaNhom & "'"
            Try
                _flag = IIf(_dac.ExecuteNonQuery(strSql, CommandType.Text) > 0, True, False)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi cập nhật nhóm người sử dụng")
                _flag = False
                Throw ex
            Finally
            End Try
            Return _flag
        End Function

        Public Function Delete(ByVal objGroup As infGroupUsers) As Boolean

            Dim strSql As String = "DELETE FROM TCS_DM_NHOM WHERE MA_NHOM='" & objGroup.MaNhom & "'"
            Try
                If (CheckBeforeDeleted(objGroup)) Then
                    _flag = IIf(_dac.ExecuteNonQuery(strSql, CommandType.Text) > 0, True, False)
                    'Return False
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi xóa nhóm người sử dụng")
                Throw ex
                _flag = False
            Finally
            End Try
            Return _flag
        End Function

        Public Function Search(ByVal objGroup As infGroupUsers) As DataTable
            Dim strSQL As String = "SELECT MA_NHOM, TEN_NHOM FROM TCS_DM_NHOM"
            Try
                If objGroup.MaNhom IsNot Nothing Then
                    If objGroup.MaNhom.Length > 0 Then
                        strSQL += " WHERE MA_NHOM = '" & objGroup.MaNhom & "'"
                    End If
                End If
                If objGroup.TenNhom IsNot Nothing Then
                    If objGroup.TenNhom.Length > 0 Then
                        strSQL += " WHERE TEN_NHOM LIKE '%" & objGroup.TenNhom & "%'"
                    End If
                End If
                Return _dac.ExecuteToTable(strSQL)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin nhóm người sử dụng")
                Return New DataTable
                Throw ex
            End Try
        End Function

        Public Function CheckBeforeInsert(ByVal objGroup As infGroupUsers) As Boolean
            Dim dt As New DataTable
            dt = Search(objGroup)
            _flag = IIf(dt.Rows.Count > 0, False, True)
            Return _flag
        End Function

        Public Function CheckBeforeDeleted(ByVal objGroup As infGroupUsers) As Boolean
            Try
                Dim strSql As String

                strSql = "SELECT COUNT(1) FROM TCS_PHANQUYEN WHERE MA_NHOM = '" & objGroup.MaNhom & "'"
                _flag = IIf(Integer.Parse(_dac.ExecuteSQLScalar(strSql)) > 0, False, True)
                If _flag = True Then
                    strSql = "SELECT COUNT(1) FROM TCS_DM_NHANVIEN WHERE MA_NHOM = '" & objGroup.MaNhom & "'"
                    _flag = IIf(Integer.Parse(_dac.ExecuteSQLScalar(strSql)) > 0, False, True)
                End If
            Catch ex As Exception

            End Try
            Return _flag
        End Function

        Public Function InsertTempAction(ByVal objGroup As infGroupUsers, ByVal strMa_NV_TAO As String, ByVal strAction As String) As Boolean
            Dim result As Boolean = True
            Dim strSql As String
            Try
                Dim action_ID As String = Common.mdlCommon.getDataKey("TCS_DM_NHOM_TEMP_SEQ.NEXTVAL")
                strSql = "INSERT INTO TCS_DM_NHOM_TEMP (ACTION_ID,MA_NV_TAO,MA_NV_DUYET,ACTION_TIME,MA_NHOM,TEN_NHOM,PERMISSION,ACTION)" & _
                " VALUES(" & _
                action_ID & _
                ",'" & strMa_NV_TAO & _
                "','',SYSDATE,'" & _
                objGroup.MaNhom & _
                "','" & objGroup.TenNhom & _
                "','" & objGroup.GroupPermission & _
                "','" & strAction & "')"
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                result = False
            End Try
            Return result
        End Function

        Public Function SearchTempData(ByVal objGroup As infGroupUsers) As DataTable
            Dim strSQL As String = "SELECT ACTION_ID,MA_NV_TAO,MA_NV_DUYET,ACTION_TIME,APPROVE_TIME,MA_NHOM,TEN_NHOM,PERMISSION,ACTION FROM TCS_DM_NHOM_TEMP WHERE APPROVE_TIME is null "
            Try
                If objGroup.MaNhom IsNot Nothing Then
                    If objGroup.MaNhom.Length > 0 Then
                        strSQL += " and MA_NHOM = '" & objGroup.MaNhom & "'"
                    End If
                End If
                If objGroup.TenNhom IsNot Nothing Then
                    If objGroup.TenNhom.Length > 0 Then
                        strSQL += " and TEN_NHOM LIKE '%" & objGroup.TenNhom & "%'"
                    End If
                End If
                Return DataAccess.ExecuteToTable(strSQL)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin nhóm người sử dụng")
                Return New DataTable
                Throw ex
            End Try
        End Function

        Public Function ApproveTempData(ByVal strAction_ID As String, ByVal strMa_NV_DUYET As String) As Boolean
            Dim result As Boolean = True
            Try
                Dim dtActionApprove As DataTable = DataAccess.ExecuteToTable("SELECT ACTION_ID,MA_NV_TAO,MA_NV_DUYET,ACTION_TIME,APPROVE_TIME,MA_NHOM,TEN_NHOM,PERMISSION,ACTION FROM TCS_DM_NHOM_TEMP WHERE ACTION_ID=" & strAction_ID & "")

                If dtActionApprove.Rows.Count > 0 Then
                    Dim objGUser As New infGroupUsers
                    objGUser.TenNhom = dtActionApprove.Rows(0)("TEN_NHOM").ToString()
                    objGUser.MaNhom = dtActionApprove.Rows(0)("MA_NHOM").ToString()
                    objGUser.GroupPermission = dtActionApprove.Rows(0)("PERMISSION").ToString()
                    Dim strAction As String = dtActionApprove.Rows(0)("ACTION").ToString()

                    Select Case strAction
                        Case "INSERT"
                            Insert(objGUser)
                            UpdateGroupPermission(objGUser.MaNhom, objGUser.GroupPermission)
                        Case "UPDATE"
                            Update(objGUser)
                            UpdateGroupPermission(objGUser.MaNhom, objGUser.GroupPermission)
                        Case "DELETE"
                            Delete(objGUser)
                            AddOrRemovePermisson(objGUser.MaNhom, "NHOM_MAKER", "REMOVE")
                            AddOrRemovePermisson(objGUser.MaNhom, "NHOM_CHECKER", "REMOVE")
                            AddOrRemovePermisson(objGUser.MaNhom, "NHOM_VIEWER", "REMOVE")
                    End Select
                    Dim strSql As String = "UPDATE TCS_DM_NHOM_TEMP SET MA_NV_DUYET='" & strMa_NV_DUYET & "',APPROVE_TIME=SYSDATE WHERE ACTION_ID='" & strAction_ID & "'"
                    DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
                End If
            Catch ex As Exception
                result = False
            End Try
            Return result
        End Function

        Public Function UpdateGroupPermission(ByVal strMa_Nhom As String, ByVal strPermission As String) As Boolean
            Dim result As Boolean = True
            Dim strKey As String = ""

            If strPermission.Contains("Maker") Then
                strKey = "NHOM_MAKER"
            End If

            If strPermission.Contains("Checker") Then
                strKey = "NHOM_CHECKER"
            End If

            If strPermission.Contains("Checker") = False And strPermission.Contains("Maker") = False Then
                strKey = "NHOM_VIEWER"
            End If

            Try
                Dim dtKeyPermission As DataTable = DataAccess.ExecuteToTable("SELECT TEN_TS,GIATRI_TS,LOAI_TS,MO_TA FROM TCS_THAMSO_HT WHERE TEN_TS='" + strKey + "'")

                If dtKeyPermission.Rows.Count > 0 Then
                    Dim strPermissonValue As String = DataAccess.ExecuteSQLScalar("SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='" + strKey + "'")

                    Select Case (strKey)
                        Case "NHOM_MAKER"
                            AddOrRemovePermisson(strMa_Nhom, "NHOM_MAKER", "ADD")
                            AddOrRemovePermisson(strMa_Nhom, "NHOM_CHECKER", "REMOVE")
                            AddOrRemovePermisson(strMa_Nhom, "NHOM_VIEWER", "REMOVE")
                        Case "NHOM_CHECKER"
                            AddOrRemovePermisson(strMa_Nhom, "NHOM_MAKER", "REMOVE")
                            AddOrRemovePermisson(strMa_Nhom, "NHOM_CHECKER", "ADD")
                            AddOrRemovePermisson(strMa_Nhom, "NHOM_VIEWER", "REMOVE")
                        Case "NHOM_VIEWER"
                            AddOrRemovePermisson(strMa_Nhom, "NHOM_MAKER", "REMOVE")
                            AddOrRemovePermisson(strMa_Nhom, "NHOM_CHECKER", "REMOVE")
                            AddOrRemovePermisson(strMa_Nhom, "NHOM_VIEWER", "ADD")
                    End Select
                End If
            Catch ex As Exception

            End Try
        End Function

        Public Function AddOrRemovePermisson(ByVal strMa_Nhom As String, ByVal strTen_TS As String, ByVal strAction As String) As Boolean
            Dim result As Boolean = True
            If strAction = "ADD" Then
                Dim dtKeyPermission As DataTable = DataAccess.ExecuteToTable("SELECT TEN_TS,GIATRI_TS,LOAI_TS,MO_TA FROM TCS_THAMSO_HT WHERE TEN_TS='" + strTen_TS + "'")
                If dtKeyPermission.Rows.Count > 0 Then
                    Dim strPermissonValue As String = DataAccess.ExecuteSQLScalar("SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='" + strTen_TS + "'")
                    If strPermissonValue.Contains(strMa_Nhom) = False Then

                        If strPermissonValue.EndsWith(",") Then
                            strPermissonValue += strMa_Nhom + ","
                        Else
                            strPermissonValue += "," + strMa_Nhom + ","
                        End If
                    End If
                    buThamso.ThamSoHT_UpdateValue(strTen_TS, strPermissonValue)
                Else
                    buThamso.ThamSoHT_Insert(strTen_TS, strMa_Nhom + ",", "", "", "")
                End If
            ElseIf strAction = "REMOVE" Then
                Dim dtKeyPermission As DataTable = DataAccess.ExecuteToTable("SELECT TEN_TS,GIATRI_TS,LOAI_TS,MO_TA FROM TCS_THAMSO_HT WHERE TEN_TS='" + strTen_TS + "'")
                If dtKeyPermission.Rows.Count > 0 Then
                    Dim strPermissonValue As String = DataAccess.ExecuteSQLScalar("SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='" + strTen_TS + "'")
                    If strPermissonValue.Contains(strMa_Nhom) Then
                        strPermissonValue = strPermissonValue.Replace(strMa_Nhom + ",", "")
                    End If
                    buThamso.ThamSoHT_UpdateValue(strTen_TS, strPermissonValue)
                End If
            End If
        End Function
    End Class
End Namespace
