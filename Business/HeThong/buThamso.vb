Imports VBOracleLib
Namespace HeThong

    Public Class buThamso
        Public Shared Function ThamsoChung(ByVal sqlstr As String) As DataSet
            Dim ds As DataSet
            Dim da As New daThamso
            ds = da.Thamso(sqlstr)
            Return ds
        End Function

        Public Shared Function ThamsoRieng(ByVal sql1 As String, ByVal sql2 As String) As Boolean
            Try
                Dim daobj As New daThamso
                daobj.UpdateThamso(sql1, sql2)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function ThamSoHT_CheckExist(ByVal strTen_ThamSo As String) As Boolean
            Dim result As Boolean = False
            Try
                Dim objdaThamSo As New daThamso
                result = objdaThamSo.ThamSoHT_CheckExist(strTen_ThamSo)
            Catch ex As Exception

            End Try
            Return result
        End Function

        Public Shared Function ThamSoHT_Insert(ByVal strTen_TS As String, ByVal strGiaTri_TS As String, ByVal strLoai_TS As String, ByVal strMo_Ta As String, ByVal strXap_Xep As String) As Boolean
            Dim result As Boolean = False
            Try
                Dim objdaThamSo As New daThamso
                result = objdaThamSo.ThamSoHT_Insert(strTen_TS, strGiaTri_TS, strLoai_TS, strMo_Ta, strXap_Xep)
            Catch ex As Exception

            End Try
            Return result
        End Function

        Public Shared Function ThamSoHT_UpdateValue(ByVal strTen_TS As String, ByVal strGiaTri_TS As String) As Boolean
            Dim result As Boolean = False
            Try
                Dim objdaThamSo As New daThamso
                result = objdaThamSo.ThamSoHT_UpdateValue(strTen_TS, strGiaTri_TS)
            Catch ex As Exception

            End Try
            Return result
        End Function

        Public Shared Function ThamSoHT_GetValue(ByVal strTenTS As String) As String
            Dim result As String = ""
            Try
                Dim objdaThamSo As New daThamso
                result = objdaThamSo.ThamSoHT_GetValue(strTenTS)
            Catch ex As Exception

            End Try
            Return result
        End Function

        Public Shared Function ThamSoHT_Delete(ByVal strTen_TS As String) As Boolean
            Dim result As Boolean = True
            Try
                Dim objdaThamSo As New daThamso
                result = objdaThamSo.ThamSoHT_Delete(strTen_TS)
            Catch ex As Exception
                result = False
            End Try
            Return result
        End Function
    End Class

End Namespace