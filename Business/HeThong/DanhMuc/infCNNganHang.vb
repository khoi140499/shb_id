﻿Namespace DanhMuc.CNNganHang
    Public Class infCNNganHang
        Private strid As String
        Private strMA_PGD As String
        Private strTen_PGD As String
        Private strMA_CN As String
        Private strTinh_Trang As String
        Private strPhan_Cap As String
        Private strMa_Xa As String
        Private strHIS_ID As String
        Private strNGUOI_TAO As String
        Private strNGUOI_DUYET As String
        Private strTYPE As String
        Private strTT_XU_LY As String
        Private strbranch_code As String
        Private strTaikhoan_tienmat As String

        Sub New()
            strid = ""
            strMA_PGD = ""
            strTen_PGD = ""
            strMA_CN = ""
            strTinh_Trang = ""
            strPhan_Cap = ""
            strHIS_ID = ""
            strNGUOI_TAO = ""
            strNGUOI_DUYET = ""
            strTYPE = ""
            strTT_XU_LY = ""
            strbranch_code = ""
            strTaikhoan_tienmat = ""
        End Sub

        Public Property BRANCH_CODE() As String
            Get
                Return strbranch_code
            End Get
            Set(ByVal Value As String)
                strbranch_code = Value
            End Set
        End Property

        Public Property MA_PGD() As String
            Get
                Return strMA_PGD
            End Get
            Set(ByVal Value As String)
                strMA_PGD = Value
            End Set
        End Property
        Public Property MA_XA() As String
            Get
                Return strMa_Xa
            End Get
            Set(ByVal Value As String)
                strMa_Xa = Value
            End Set
        End Property
        Public Property ID() As String
            Get
                Return strid
            End Get
            Set(ByVal Value As String)
                strid = Value
            End Set
        End Property

        Public Property MA_CN() As String
            Get
                Return strMA_CN
            End Get
            Set(ByVal Value As String)
                strMA_CN = Value
            End Set
        End Property

        Public Property TEN_PGD() As String
            Get
                Return strTen_PGD
            End Get
            Set(ByVal Value As String)
                strTen_PGD = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTinh_Trang
            End Get
            Set(ByVal Value As String)
                strTinh_Trang = Value
            End Set
        End Property
        Public Property PHAN_CAP() As String
            Get
                Return strPhan_Cap
            End Get
            Set(ByVal Value As String)
                strPhan_Cap = Value
            End Set
        End Property
        Public Property HIS_ID() As String
            Get
                Return strHIS_ID
            End Get
            Set(ByVal Value As String)
                strHIS_ID = Value
            End Set
        End Property
        Public Property NGUOI_TAO() As String
            Get
                Return strNGUOI_TAO
            End Get
            Set(ByVal Value As String)
                strNGUOI_TAO = Value
            End Set
        End Property
        Public Property NGUOI_DUYET() As String
            Get
                Return strNGUOI_DUYET
            End Get
            Set(ByVal Value As String)
                strNGUOI_DUYET = Value
            End Set
        End Property
        Public Property TYPE() As String
            Get
                Return strTYPE
            End Get
            Set(ByVal Value As String)
                strTYPE = Value
            End Set
        End Property

        Public Property TT_XU_LY() As String
            Get
                Return strTT_XU_LY
            End Get
            Set(ByVal Value As String)
                strTT_XU_LY = Value
            End Set
        End Property
        Public Property TAIKHOAN_TIENMAT() As String
            Get
                Return strTaikhoan_tienmat
            End Get
            Set(ByVal Value As String)
                strTaikhoan_tienmat = Value
            End Set
        End Property
    End Class
End Namespace
