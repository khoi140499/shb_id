﻿Namespace DanhMuc.TieuMuc
    Public Class infTieuMuc
        Private strMTM_ID As String
        Private strMA_TMUC As String
        Private strTen As String
        Private strGHICHU As String
        Private strTINH_TRANG As String
        Private strID As String
        Private strNGUOI_TAO As String
        Private strNGUOI_DUYET As String
        Private strTYPE As String
        Private strTT_XU_LY As String
        Private strSac_thue As String

        Sub New()
            strMTM_ID = ""
            strTen = ""
            strGHICHU = ""
            strTINH_TRANG = "1"
            strID = ""
            strNGUOI_TAO = ""
            strNGUOI_DUYET = ""
            strTYPE = ""
            strTT_XU_LY = ""
            strSac_thue = ""

        End Sub

        Sub New(ByVal drwLoaiKhoan As DataRow)
            strMTM_ID = drwLoaiKhoan("MTM_ID").ToString().Trim()
            strMA_TMUC = drwLoaiKhoan("MA_TMUC").ToString().Trim()
            strSac_thue = drwLoaiKhoan("Sac_thue").ToString().Trim()
            strTen = drwLoaiKhoan("Ten").ToString().Trim()
            strTINH_TRANG = drwLoaiKhoan("TINH_TRANG").ToString().Trim()
            strGHICHU = drwLoaiKhoan("GHI_CHU").ToString().Trim()
        End Sub

        Public Property MTM_ID() As String
            Get
                Return strMTM_ID
            End Get
            Set(ByVal Value As String)
                strMTM_ID = Value
            End Set
        End Property

        Public Property MA_TMUC() As String
            Get
                Return strMA_TMUC
            End Get
            Set(ByVal Value As String)
                strMA_TMUC = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property

        Public Property GHICHU() As String
            Get
                Return strGHICHU
            End Get
            Set(ByVal Value As String)
                strGHICHU = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTINH_TRANG
            End Get
            Set(ByVal Value As String)
                strTINH_TRANG = Value
            End Set
        End Property
        Public Property ID() As String
            Get
                Return strID
            End Get
            Set(ByVal Value As String)
                strID = Value
            End Set
        End Property
        Public Property NGUOI_TAO() As String
            Get
                Return strNGUOI_TAO
            End Get
            Set(ByVal Value As String)
                strNGUOI_TAO = Value
            End Set
        End Property
        Public Property NGUOI_DUYET() As String
            Get
                Return strNGUOI_DUYET
            End Get
            Set(ByVal Value As String)
                strNGUOI_DUYET = Value
            End Set
        End Property
        Public Property TYPE() As String
            Get
                Return strTYPE
            End Get
            Set(ByVal Value As String)
                strTYPE = Value
            End Set
        End Property

        Public Property TT_XU_LY() As String
            Get
                Return strTT_XU_LY
            End Get
            Set(ByVal Value As String)
                strTT_XU_LY = Value
            End Set
        End Property
        Public Property SAC_THUE() As String
            Get
                Return strSac_thue
            End Get
            Set(ByVal Value As String)
                strSac_thue = Value
            End Set
        End Property
    End Class
End Namespace

