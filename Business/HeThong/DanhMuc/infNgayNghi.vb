﻿Namespace DanhMuc.NgayNghi
    Public Class infNgayNghi
        Private P_NgayNghi As String
        Private P_GhiChu As String
        Public Property NgayNghi() As String
            Get
                NgayNghi = P_NgayNghi
            End Get
            Set(ByVal Value As String)
                P_NgayNghi = Value
            End Set
        End Property
        Public Property GhiChu() As String
            Get
                GhiChu = P_GhiChu
            End Get
            Set(ByVal Value As String)
                P_GhiChu = Value
            End Set
        End Property
    End Class
End Namespace
