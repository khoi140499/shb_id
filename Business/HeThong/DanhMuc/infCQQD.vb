﻿
Namespace DanhMuc.CQQD

    Public Class infCQQD
        Private strMA_CQQD As String
        Private strTEN_CQQD As String
        Private strCAP As String
        Private strMA_LH As String
        Private strGHI_CHU As String
        Private strNGUOI_DUYET As String
        Private strMA_KB As String
        Private strMA_CQTHU As String
        Private strNGAY_DUYET As String
        Private strNGAY_TAO As String
        Private strNGUOI_TAO As String
        Private strTINH_TRANG As String
        Private strMA_DBHC As String
        Private strTRANGTHAI As String
        Private strTYPE As String
        Private strTT_XU_LY As String
        Private strID As String
        Private strID_CQQD As String

        Sub New()
            strMA_CQQD = ""
            strTEN_CQQD = ""
            strCAP = ""
            strMA_LH = ""
            strGHI_CHU = ""
            strNGUOI_DUYET = ""
            strMA_KB = ""
            strMA_CQTHU = ""
            strNGAY_DUYET = ""
            strNGAY_TAO = ""
            strNGUOI_TAO = ""
            strTINH_TRANG = "1"
            strMA_DBHC = ""
            strTRANGTHAI = ""
            strTYPE = ""
            strTT_XU_LY = ""
            strID = ""
            strID_CQQD = ""
        End Sub

        Sub New(ByVal drwCapChuong As DataRow)
            strMA_CQQD = drwCapChuong("MA_CQQD").ToString().Trim()
            strTEN_CQQD = drwCapChuong("TEN_CQQD").ToString().Trim()
            'strCAP = drwCapChuong("CAP").ToString().Trim()
            'strMA_LH = drwCapChuong("MA_LH").ToString().Trim()
            strMA_KB = drwCapChuong("MA_KB").ToString().Trim()
            strGHI_CHU = drwCapChuong("GHI_CHU").ToString().Trim()
            strTINH_TRANG = drwCapChuong("TINH_TRANG").ToString().Trim()
            strMA_CQTHU = drwCapChuong("MA_CQTHU").ToString().Trim()
            strMA_DBHC = drwCapChuong("MA_DBHC").ToString().Trim()
            strID = drwCapChuong("ID").ToString().Trim()
            strTRANGTHAI = drwCapChuong("TRANGTHAI").ToString().Trim()
        End Sub

        Public Property MA_CQQD() As String
            Get
                Return strMA_CQQD
            End Get
            Set(ByVal Value As String)
                strMA_CQQD = Value
            End Set
        End Property

        Public Property ID_CQQD() As String
            Get
                Return strID_CQQD
            End Get
            Set(ByVal Value As String)
                strID_CQQD = Value
            End Set
        End Property


        Public Property TEN_CQQD() As String
            Get
                Return strTEN_CQQD
            End Get
            Set(ByVal Value As String)
                strTEN_CQQD = Value
            End Set
        End Property

        Public Property CAP() As String
            Get
                Return strCAP
            End Get
            Set(ByVal Value As String)
                strCAP = Value
            End Set
        End Property

        Public Property MA_LH() As String
            Get
                Return strMA_LH
            End Get
            Set(ByVal Value As String)
                strMA_LH = Value
            End Set
        End Property

        Public Property GHI_CHU() As String
            Get
                Return strGHI_CHU
            End Get
            Set(ByVal Value As String)
                strGHI_CHU = Value
            End Set
        End Property

        Public Property NGUOI_DUYET() As String
            Get
                Return strNGUOI_DUYET
            End Get
            Set(ByVal Value As String)
                strNGUOI_DUYET = Value
            End Set
        End Property

        Public Property MA_KB() As String
            Get
                Return strMA_KB
            End Get
            Set(ByVal Value As String)
                strMA_KB = Value
            End Set
        End Property

        Public Property MA_CQTHU() As String
            Get
                Return strMA_CQTHU
            End Get
            Set(ByVal Value As String)
                strMA_CQTHU = Value
            End Set
        End Property

        Public Property NGAY_DUYET() As String
            Get
                Return strNGAY_DUYET
            End Get
            Set(ByVal Value As String)
                strNGAY_DUYET = Value
            End Set
        End Property

        Public Property NGAY_TAO() As String
            Get
                Return strNGAY_TAO
            End Get
            Set(ByVal Value As String)
                strNGAY_TAO = Value
            End Set
        End Property

        Public Property NGUOI_TAO() As String
            Get
                Return strNGUOI_TAO
            End Get
            Set(ByVal Value As String)
                strNGUOI_TAO = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTINH_TRANG
            End Get
            Set(ByVal Value As String)
                strTINH_TRANG = Value
            End Set
        End Property

        Public Property MA_DBHC() As String
            Get
                Return strMA_DBHC
            End Get
            Set(ByVal Value As String)
                strMA_DBHC = Value
            End Set
        End Property

        Public Property TRANGTHAI() As String
            Get
                Return strTRANGTHAI
            End Get
            Set(ByVal Value As String)
                strTRANGTHAI = Value
            End Set
        End Property

        Public Property TYPE() As String
            Get
                Return strTYPE
            End Get
            Set(ByVal Value As String)
                strTYPE = Value
            End Set
        End Property

        Public Property TT_XU_LY() As String
            Get
                Return strTT_XU_LY
            End Get
            Set(ByVal Value As String)
                strTT_XU_LY = Value
            End Set
        End Property

        Public Property ID() As String
            Get
                Return strID
            End Get
            Set(ByVal Value As String)
                strID = Value
            End Set
        End Property




    End Class


End Namespace