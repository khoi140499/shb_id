Namespace DanhMuc.TaiKhoan
    Public Class infTaiKhoan
        Private strTK As String
        Private strTEN_TK As String
        Private strMA_CQTHU As String
        Private strMaDBHC As String
        Private strTINH_TRANG As String
        Private strSHKB As String
        Private strTK_KB_NH As String


        Sub New()
            strTK = ""
            strTEN_TK = ""
            strMA_CQTHU = ""
            strMaDBHC = ""
            strSHKB = ""
            strTK_KB_NH = ""
        End Sub

        Sub New(ByVal drwTaiKhoan As DataRow)
            strTK = drwTaiKhoan("TK").ToString().Trim()
            strTEN_TK = drwTaiKhoan("TEN_TK").ToString().Trim()
            strMaDBHC = drwTaiKhoan("DBHC").ToString().Trim()
            strMA_CQTHU = drwTaiKhoan("MA_CQTHU").ToString().Trim()
            strTINH_TRANG = drwTaiKhoan("TINH_TRANG").ToString().Trim()
        End Sub

        Public Property TK() As String
            Get
                Return strTK
            End Get
            Set(ByVal Value As String)
                strTK = Value
            End Set
        End Property
        Public Property TK_KB_NH() As String
            Get
                Return strTK_KB_NH
            End Get
            Set(ByVal Value As String)
                strTK_KB_NH = Value
            End Set
        End Property
        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property

        Public Property TEN_TK() As String
            Get
                Return strTEN_TK
            End Get
            Set(ByVal Value As String)
                strTEN_TK = Value
            End Set
        End Property
        Public Property MA_DBHC() As String
            Get
                Return strMaDBHC
            End Get
            Set(ByVal Value As String)
                strMaDBHC = Value
            End Set
        End Property

        Public Property MA_CQTHU() As String
            Get
                Return strMA_CQTHU
            End Get
            Set(ByVal Value As String)
                strMA_CQTHU = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTINH_TRANG
            End Get
            Set(ByVal Value As String)
                strTINH_TRANG = Value
            End Set
        End Property

        Public ReadOnly Property TINHTRANG() As String
            Get
                If strTINH_TRANG = "0" Then
                    Return "Dang hoat dong"
                ElseIf strTINH_TRANG = "1" Then
                    Return "Ngung hoat dong"
                Else
                    Return ""
                End If
            End Get
        End Property

    End Class
End Namespace
