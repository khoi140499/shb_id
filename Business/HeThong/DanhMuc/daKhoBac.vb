Imports VBOracleLib

Namespace DanhMuc.KhoBac
    Public Class daKhoBac
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objKhoBac As infKhoBac, ByVal Nhom As String) As Boolean
            Dim cnKhoBac As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_KHO_BAC (ID,SHKB, TEN, MA_TINH,MA_DB, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_KHO_BAC.NEXTVAL") & "," & _
                                "'" & objKhoBac.SHKB & "','" & objKhoBac.TEN_KB & "'," & _
                                "'" & objKhoBac.MaTinh & "','" & objKhoBac.MaDBHC & "','" & objKhoBac.TINH_TRANG & "','" & objKhoBac.NGUOI_TAO & "', SYSDATE,'0','" & objKhoBac.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objKhoBac.TT_XU_LY = "1" Then
                    'nếu duyệt
                    strSql = "INSERT INTO TCS_DM_KHOBAC (SHKB, TEN, MA_TINH, MA_DB, TINH_TRANG) " & _
                                " SELECT HIS.SHKB ,HIS.TEN, HIS.MA_TINH, HIS.MA_DB, HIS.TINH_TRANG " & _
                                " FROM HIS_DM_KHO_BAC HIS WHERE HIS.ID=" & objKhoBac.ID
                ElseIf objKhoBac.TT_XU_LY = "2" Then
                    'nếu từ chối
                    strSql = "UPDATE HIS_DM_KHO_BAC SET NGUOI_DUYET='" & objKhoBac.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objKhoBac.ID
                End If
            End If
            Try
                cnKhoBac = New DataAccess
                cnKhoBac.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objKhoBac.TT_XU_LY = "1" Then
                    'Khi đã duyệt thêm mới 1 bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_KHO_BAC SET NGUOI_DUYET ='" & objKhoBac.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objKhoBac.ID
                    cnKhoBac.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi thêm mới kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objKhobac As infKhoBac, ByVal Nhom As String) As Boolean
            Dim cnKhoBac As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_KHO_BAC (ID,SHKB, TEN, MA_TINH,MA_DB, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_KHO_BAC.NEXTVAL") & _
                                ",'" & objKhobac.SHKB & "','" & objKhobac.TEN_KB & "'," & _
                                "'" & objKhobac.MaTinh & "','" & objKhobac.MaDBHC & "','" & objKhobac.TINH_TRANG & "','" & objKhobac.NGUOI_TAO & "', SYSDATE,'0','" & objKhobac.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objKhobac.TT_XU_LY = "1" Then
                    strSql = "UPDATE TCS_DM_KHOBAC SET TEN='" & objKhobac.TEN_KB.Trim & "',MA_TINH='" & objKhobac.MaTinh.Trim & "',MA_DB='" & objKhobac.MaDBHC & "', TINH_TRANG='" & objKhobac.TINH_TRANG.Trim & "'  WHERE SHKB='" & objKhobac.SHKB.Trim & "'"
                ElseIf objKhobac.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_KHO_BAC SET NGUOI_DUYET='" & objKhobac.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objKhobac.ID
                End If
            End If
            Try
                cnKhoBac = New DataAccess
                cnKhoBac.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objKhobac.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_KHO_BAC SET NGUOI_DUYET ='" & objKhobac.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objKhobac.ID
                    cnKhoBac.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi cập nhật kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objKhobac As infKhoBac, ByVal Nhom As String) As Boolean
            Dim cnKhoBac As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_KHO_BAC (ID,SHKB, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_KHO_BAC.NEXTVAL") & ",'" & _
                        objKhobac.SHKB.Trim & "','" & objKhobac.NGUOI_TAO & "', SYSDATE,'0','" & objKhobac.TYPE & "') "
            ElseIf Nhom = "11" Then
                If objKhoBac.TT_XU_LY = "1" Then
                    strSql = "DELETE FROM TCS_DM_KHOBAC " & _
                               "WHERE SHKB='" & objKhobac.SHKB & "'"
                ElseIf objKhoBac.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_KHO_BAC SET NGUOI_DUYET='" & objKhoBac.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objKhoBac.ID
                End If
            End If
            Try
                cnKhoBac = New DataAccess
                cnKhoBac.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objKhobac.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_KHO_BAC SET NGUOI_DUYET ='" & objKhobac.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objKhobac.ID
                    cnKhoBac.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi xóa kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá tài khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function LoadDetail(ByVal strSHKB As String) As infKhoBac
            Dim objKhoBac As New infKhoBac
            Dim cnKhoBac As DataAccess
            Dim drKhoBac As IDataReader
            Dim strSQL As String = "Select SHKB,Ten,MA_TINH,MA_DB, tinh_trang From TCS_DM_KhoBac " & _
                        "Where SHKB='" & strSHKB & "'"
            Try
                cnKhoBac = New DataAccess
                drKhoBac = cnKhoBac.ExecuteDataReader(strSQL, CommandType.Text)
                If drKhoBac.Read Then
                    objKhoBac = New infKhoBac
                    objKhoBac.SHKB = strSHKB
                    objKhoBac.TEN_KB = drKhoBac("TEN").ToString()
                    objKhoBac.TINH_TRANG = drKhoBac("TINH_TRANG").ToString()
                    objKhoBac.MaTinh = drKhoBac("MA_TINH").ToString()
                    objKhoBac.MaDBHC = drKhoBac("MA_DB").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drKhoBac Is Nothing Then drKhoBac.Close()
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return objKhoBac
        End Function

        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnKhoBac As DataAccess
            Dim dsKhoBac As DataSet

            Try
                strSQL = "SELECT SHKB,Ten,MA_TINH,MA_DB, tinh_trang,DECODE (tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 1, 'Đang hoạt động', " & _
                         "                 0, 'Ngừng hoạt động' " & _
                         "                ) tinhtrang  from tcs_dm_khobac" & _
                         "  WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY SHKB ASC "

                cnKhoBac = New DataAccess
                dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try

            Return dsKhoBac
        End Function
        Public Function Load_new(ByVal WhereClause As String) As DataSet
            Dim strSQL As String
            Dim cnCapChuong As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "SELECT   a.SHKB, a.ten, a.MA_TINH,a.MA_DB,b.SHKB as SHKB_NEW, " & _
                         "         DECODE (a.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngừng hoạt động', " & _
                         "                 1, 'Đang hoạt động' " & _
                         "                ) tinh_trang,DECODE(b.TYPE,NULL,'',1,'Thêm',2,'Sửa',3,'Xóa') ThayDoi ,DECODE(b.TT_XU_LY,NULL,'',0,'Chờ duyệt',1,'Đã duyệt',2,'Từ chối') xu_ly ," & _
                         " b.id,b.Type,b.TT_XU_LY, b.ma_tinh as ma_tinh_new, b.ten as ten_new, b.ma_db as ma_db_new, DECODE(b.tinh_trang,NULL,'',0,'Ngừng hoạt động',1,'Đang hoạt động') tinh_trang_new " & _
                         "    FROM his_dm_kho_bac b ,tcs_dm_khobac a " & _
                         "   WHERE  b.shkb=a.shkb(+) " & WhereClause & _
                         "ORDER BY b.ID DESC "
                cnCapChuong = New DataAccess
                dsCapChuong = cnCapChuong.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnCapChuong.Dispose()
            End Try

            Return dsCapChuong
        End Function

        Public Function CheckExist(ByVal objKhoBac As infKhoBac) As Boolean
            Return LoadDetail(objKhoBac.SHKB).SHKB <> ""
        End Function

        Public Function CheckDelete(ByVal strSHKB As String) As Object
            Dim cnKhoBac As DataAccess
            Dim dsKhoBac As DataSet

            Try
                Dim strSql As String = "select shkb from (select shkb from tcs_ctu_hdr union " & _
                           "select shkb from tcs_baolanh_hdr) where shkb='" & strSHKB & "'"

                cnKhoBac = New DataAccess
                dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try

            Return dsKhoBac
        End Function
    End Class

End Namespace
