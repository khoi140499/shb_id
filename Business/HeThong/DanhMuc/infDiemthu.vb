Namespace DanhMuc.DiemThu
    Public Class infDiemthu
        Private P_TitGroup As String
        Private P_MaDThu As String
        Private P_TenDThu As String
        Public Property TitGroup() As String
            Get
                TitGroup = P_TitGroup
            End Get
            Set(ByVal Value As String)
                P_TitGroup = Value
            End Set
        End Property
        Public Property MaDThu() As String
            Get
                MaDThu = P_MaDThu
            End Get
            Set(ByVal Value As String)
                P_MaDThu = Value
            End Set
        End Property
        Public Property TenDThu() As String
            Get
                TenDThu = P_TenDThu
            End Get
            Set(ByVal Value As String)
                P_TenDThu = Value
            End Set
        End Property
    End Class
End Namespace
