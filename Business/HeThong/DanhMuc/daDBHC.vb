Imports VBOracleLib

Namespace DanhMuc.DBHC
    Public Class daDBHC
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objDBHC As infDBHC, ByVal Nhom As String) As Boolean
            Dim cnDBHC As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_DBHC (ID,Xa_ID, ma_xa, TEN, Ma_Cha, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_DBHC.NEXTVAL") & "," & Common.mdlCommon.getDataKey("TCS_Xa_ID_SEQ.NEXTVAL") & ", " & _
                                "'" & objDBHC.Ma_DBHC & "','" & objDBHC.TEN_DBHC & "'," & _
                                "'" & objDBHC.Ma_Cha & "','" & objDBHC.TINH_TRANG & "','" & objDBHC.NGUOI_TAO & "', SYSDATE,'0','" & objDBHC.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objDBHC.TT_XU_LY = "1" Then
                    'nếu duyệt
                    strSql = "INSERT INTO TCS_DM_XA (Xa_ID, ma_xa, TEN, Ma_Cha, TINH_TRANG) " & _
                                " SELECT HIS.Xa_ID,HIS.ma_xa,HIS.TEN,HIS.Ma_Cha,HIS.TINH_TRANG " & _
                                " FROM HIS_DM_DBHC HIS WHERE HIS.ID=" & objDBHC.ID
                ElseIf objDBHC.TT_XU_LY = "2" Then
                    'nếu từ chối
                    strSql = "UPDATE HIS_DM_DBHC SET NGUOI_DUYET='" & objDBHC.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objDBHC.ID
                End If
            End If
            'Dim strSql As String = "INSERT INTO TCS_DM_Xa (Xa_id,MA_XA, TEN,MA_CHA, Tinh_Trang)" & _
            '                        "VALUES (tcs_xa_id_seq.nextval,'" & objDBHC.Ma_DBHC & "','" & objDBHC.TEN_DBHC_DBHC & "','" & objDBHC.Ma_Cha & "','" & objDBHC.TINH_TRANG & "')"

            Try
                cnDBHC = New DataAccess
                cnDBHC.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objDBHC.TT_XU_LY = "1" Then
                    'Khi đã duyệt thêm mới 1 bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_DBHC SET NGUOI_DUYET ='" & objDBHC.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objDBHC.ID
                    cnDBHC.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi thêm mới địa bàn hành chính")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objDBHC As infDBHC, ByVal Nhom As String) As Boolean
            Dim cnDBHC As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_DBHC (ID,Xa_ID, ma_xa, TEN, Ma_Cha, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_DBHC.NEXTVAL") & "," & objDBHC.Xa_ID.Trim & ", " & _
                                "'" & objDBHC.Ma_DBHC & "','" & objDBHC.TEN_DBHC & "'," & _
                                "'" & objDBHC.Ma_Cha & "','" & objDBHC.TINH_TRANG & "','" & objDBHC.NGUOI_TAO & "', SYSDATE,'0','" & objDBHC.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objDBHC.TT_XU_LY = "1" Then
                    strSql = "UPDATE TCS_DM_XA SET TEN='" & objDBHC.TEN_DBHC.Trim & "',Ma_Cha='" & objDBHC.Ma_Cha.Trim & "', TINH_TRANG='" & objDBHC.TINH_TRANG.Trim & "'  WHERE Xa_ID=" & objDBHC.Xa_ID.Trim
                ElseIf objDBHC.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_DBHC SET NGUOI_DUYET='" & objDBHC.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objDBHC.ID
                End If
            End If
            'Dim strSql As String = "UPDATE TCS_DM_Xa SET TEN='" & objDBHC.TEN_DBHC_DBHC & "',ma_xa='" & objDBHC.Ma_DBHC & "',ma_cha='" & objDBHC.Ma_Cha & "' " & _
            '                       ",TINH_TRANG='" & objDBHC.TINH_TRANG & "' WHERE xa_id='" & objDBHC.Xa_ID & "'"
            Try
                cnDBHC = New DataAccess
                cnDBHC.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objDBHC.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_DBHC SET NGUOI_DUYET ='" & objDBHC.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objDBHC.ID
                    cnDBHC.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi cập nhật địa bàn hành chính")
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objDBHC As infDBHC, ByVal Nhom As String) As Boolean
            Dim cnDBHC As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_DBHC (ID,Xa_ID,ma_xa, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_DBHC.NEXTVAL") & "," & _
                        objDBHC.Xa_ID.Trim & ",'" & objDBHC.Ma_DBHC & "','" & objDBHC.NGUOI_TAO & "', SYSDATE,'0','" & objDBHC.TYPE & "') "
            ElseIf Nhom = "11" Then
                If objDBHC.TT_XU_LY = "1" Then
                    strSql = "DELETE FROM TCS_DM_XA " & _
                               "WHERE Xa_ID=" & objDBHC.Xa_ID
                ElseIf objDBHC.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_DBHC SET NGUOI_DUYET='" & objDBHC.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objDBHC.ID
                End If
            End If
            'Dim strSql As String = "DELETE FROM TCS_DM_Xa " & _
            '               "WHERE xa_id='" & strXaID & "'"
            Try
                cnDBHC = New DataAccess
                cnDBHC.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objDBHC.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_DBHC SET NGUOI_DUYET ='" & objDBHC.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objDBHC.ID
                    cnDBHC.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi xóa địa bàn hành chính")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá tài khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function LoadDetail(ByVal strXaID As String) As infDBHC
            Dim objDBHC As New infDBHC
            Dim cnDBHC As DataAccess
            Dim drDBHC As IDataReader
            Dim strSQL As String = "Select xa_id,ma_xa,Ten,ma_cha, tinh_trang From TCS_DM_Xa " & _
                        "Where xa_id='" & strXaID & "'"
            Try
                cnDBHC = New DataAccess
                drDBHC = cnDBHC.ExecuteDataReader(strSQL, CommandType.Text)
                If drDBHC.Read Then
                    objDBHC = New infDBHC
                    objDBHC.Xa_ID = strXaID
                    objDBHC.Ma_DBHC = drDBHC("ma_xa").ToString()
                    objDBHC.TEN_DBHC = drDBHC("TEN").ToString()
                    objDBHC.Ma_Cha = drDBHC("ma_cha").ToString()
                    objDBHC.TINH_TRANG = drDBHC("TINH_TRANG").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin địa bàn hành chính")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drDBHC Is Nothing Then drDBHC.Close()
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try
            Return objDBHC
        End Function

        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnDBHC As DataAccess
            Dim dsDBHC As DataSet

            Try
                strSQL = "SELECT xa_id,ma_xa,Ten,ma_cha, tinh_trang,DECODE (tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 1, 'Đang hoạt động', " & _
                         "                 0, 'Ngừng hoạt động' " & _
                         "                ) tinhtrang  from TCS_DM_Xa" & _
                         "  WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY xa_id ASC "

                cnDBHC = New DataAccess
                dsDBHC = cnDBHC.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin địa bàn hành chính")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try

            Return dsDBHC
        End Function
        Public Function Load_new(ByVal WhereClause As String) As DataSet
            Dim strSQL As String
            Dim cnDBHC As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "SELECT   a.Xa_ID, a.ma_xa, a.ten, a.Ma_Cha, " & _
                         "         DECODE (a.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngừng hoạt động', " & _
                         "                 1, 'Đang hoạt động' " & _
                         "                ) tinh_trang,DECODE(b.TYPE,NULL,'',1,'Thêm',2,'Sửa',3,'Xóa') ThayDoi ,DECODE(b.TT_XU_LY,NULL,'',0,'Chờ duyệt',1,'Đã duyệt',2,'Từ chối') xu_ly ," & _
                         " b.id,b.Type,b.TT_XU_LY, b.ma_xa as ma_xa_new, b.ten as ten_new, b.Ma_Cha as Ma_Cha_new, DECODE(b.tinh_trang,NULL,'',0,'Ngừng hoạt động',1,'Đang hoạt động') tinh_trang_new " & _
                         "    FROM TCS_DM_XA a right outer join HIS_DM_DBHC b on a.Xa_ID=b.Xa_ID" & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY b.ID DESC "
                cnDBHC = New DataAccess
                dsCapChuong = cnDBHC.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin địa bàn hành chính")
                Throw ex
            Finally
                cnDBHC.Dispose()
            End Try

            Return dsCapChuong
        End Function
        Public Function check_maDBHC(ByVal ma_dbhc As String) As Boolean
            Dim connect As DataAccess
            Dim Dts As DataSet

            Try
                connect = New DataAccess
                Dim strSql As String = "select * from tcs_dm_xa where ma_xa = '" & ma_dbhc & "'"
                Dts = connect.ExecuteReturnDataSet(strSql, CommandType.Text)
                If Dts.Tables(0).Rows.Count = 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False

            Finally
                If Not connect Is Nothing Then connect.Dispose()
            End Try

        End Function
        Public Function CheckExist(ByVal objDBHC As infDBHC) As Boolean
            Return LoadDetail(objDBHC.Xa_ID).Xa_ID <> ""
        End Function
        Public Function CheckDelete(ByVal strMA_DBHC As String) As Object
            Dim cnDBHC As DataAccess
            Dim dsDBHC As DataSet

            Try
                Dim strSql As String = "select xa_id from (select xa_id from tcs_ctu_hdr union all " & _
                           "select xa_id from tcs_baolanh_hdr) where xa_id='" & strMA_DBHC & "'"

                cnDBHC = New DataAccess
                dsDBHC = cnDBHC.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnDBHC Is Nothing Then cnDBHC.Dispose()
            End Try

            Return dsDBHC
        End Function
    End Class

End Namespace
