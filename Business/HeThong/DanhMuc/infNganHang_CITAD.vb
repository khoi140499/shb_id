﻿Namespace DanhMuc.NganHang
    Public Class infNganHang_CITAD
        Private strSHKB As String
        Private strTenKB As String
        Private strMA_NH_TT As String
        Private strTEN_NH_TT As String
        Private strMA_NH_TH As String
        Private strTEN_NH_TH As String
        Private strTT_KB As String
        Private strHIS_ID As String
        Private strNGUOI_TAO As String
        Private strNGUOI_DUYET As String
        Private strTYPE As String
        Private strTT_XU_LY As String
        Private strIDSE As String
        Private strNH_SHB As String
        Public Property IDSE() As String
            Get
                Return strIDSE
            End Get
            Set(ByVal Value As String)
                strIDSE = Value
            End Set
        End Property
        Public Property MA_NH_TT() As String
            Get
                Return strMA_NH_TT
            End Get
            Set(ByVal Value As String)
                strMA_NH_TT = Value
            End Set
        End Property
        Public Property TT_KB() As String
            Get
                Return strTT_KB
            End Get
            Set(ByVal Value As String)
                strTT_KB = Value
            End Set
        End Property
        Public Property TEN_NH_TT() As String
            Get
                Return strTEN_NH_TT
            End Get
            Set(ByVal Value As String)
                strTEN_NH_TT = Value
            End Set
        End Property

        Public Property MA_NH_TH() As String
            Get
                Return strMA_NH_TH
            End Get
            Set(ByVal Value As String)
                strMA_NH_TH = Value
            End Set
        End Property

        Public Property TEN_NH_TH() As String
            Get
                Return strTEN_NH_TH
            End Get
            Set(ByVal Value As String)
                strTEN_NH_TH = Value
            End Set
        End Property

        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property
        Public Property Ten_KB() As String
            Get
                Return strTenKB
            End Get
            Set(ByVal Value As String)
                strTenKB = Value
            End Set
        End Property
        Public Property HIS_ID() As String
            Get
                Return strHIS_ID
            End Get
            Set(ByVal Value As String)
                strHIS_ID = Value
            End Set
        End Property
        Public Property NGUOI_TAO() As String
            Get
                Return strNGUOI_TAO
            End Get
            Set(ByVal Value As String)
                strNGUOI_TAO = Value
            End Set
        End Property
        Public Property NGUOI_DUYET() As String
            Get
                Return strNGUOI_DUYET
            End Get
            Set(ByVal Value As String)
                strNGUOI_DUYET = Value
            End Set
        End Property
        Public Property TYPE() As String
            Get
                Return strTYPE
            End Get
            Set(ByVal Value As String)
                strTYPE = Value
            End Set
        End Property

        Public Property TT_XU_LY() As String
            Get
                Return strTT_XU_LY
            End Get
            Set(ByVal Value As String)
                strTT_XU_LY = Value
            End Set
        End Property
        Public Property NH_SHB() As String
            Get
                Return strNH_SHB
            End Get
            Set(ByVal Value As String)
                strNH_SHB = Value
            End Set
        End Property
    End Class
End Namespace
