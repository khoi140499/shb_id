﻿Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace DanhMuc.NganHang
    Public Class daNganHang
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnNganHang As DataAccess
            Dim dsNganHang As DataSet

            Try
                strSQL = "SELECT  id, ma_xa, ma, ten, shkb " & _
                         "    FROM tcs_dm_nganhang " & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY ma_xa "

                cnNganHang = New DataAccess
                dsNganHang = cnNganHang.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try

            Return dsNganHang
        End Function

        Public Function Load(ByVal objNganHang As infNganHang) As Object
            Dim strSQL As String
            Dim cnNganHang As DataAccess
            Dim dsNganHang As DataSet

            Try
                strSQL = "SELECT  id, ma_xa, ma,  ten,  shkb " & _
                         "    FROM tcs_dm_nganhang " & _
                         "   WHERE 1 = 1 " & _
                         "ORDER BY ma_xa "

                cnNganHang = New DataAccess
                dsNganHang = cnNganHang.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try

            Return dsNganHang
        End Function

        Public Function Load(ByVal strMA_XA As String, ByVal strMA_NH As String) As infNganHang
            Dim objNganHang As New infNganHang
            Dim cnNganHang As DataAccess
            Dim drNganHang As IDataReader
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT id,ma_xa, ma, ten, shkb " & _
                         "  FROM tcs_dm_nganhang " & _
                         " WHERE ma_xa = '" + strMA_XA + "' " & _
                         "   AND ma = '" + strMA_NH + "' "

                cnNganHang = New DataAccess
                drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
                If drNganHang.Read Then
                    objNganHang = New infNganHang
                    objNganHang.MA_XA = strMA_XA
                    objNganHang.MA_NH = drNganHang("ma").ToString()
                    objNganHang.TEN_NH = drNganHang("ten").ToString()
                    objNganHang.SHKB = drNganHang("shkb").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not drNganHang Is Nothing Then drNganHang.Close()
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return objNganHang
        End Function
        Public Function LoadID(ByVal strID As String, ByVal strSHKB As String) As infNganHang
            Dim objNganHang As New infNganHang
            Dim cnNganHang As DataAccess
            Dim drNganHang As IDataReader
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT id,ma_xa, ma, ten, shkb " & _
                         "  FROM tcs_dm_nganhang " & _
                         " WHERE ma = '" + strID + "' and shkb='" + strSHKB + "' "

                cnNganHang = New DataAccess
                drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
                If drNganHang.Read Then
                    objNganHang = New infNganHang
                    objNganHang.MA_XA = drNganHang("ma_xa").ToString()
                    objNganHang.MA_NH = drNganHang("ma").ToString()
                    objNganHang.TEN_NH = drNganHang("ten").ToString()
                    objNganHang.SHKB = drNganHang("shkb").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not drNganHang Is Nothing Then drNganHang.Close()
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return objNganHang
        End Function

        Public Function Insert(ByVal objNganHang As infNganHang) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO tcs_dm_nganhang " & _
                                    "            (id,ma_xa, ma, " & _
                                    "             ten, " & _
                                    "             shkb " & _
                                    "            ) " & _
                                    "     VALUES (CTC_Owner.tcs_dmnh_seq.nextval,'" + objNganHang.MA_XA + "', '" + objNganHang.MA_NH + "', " & _
                                    "             '" + objNganHang.TEN_NH + "', " & _
                                    "             '" + objNganHang.SHKB + "' " & _
                                    "            ) "

            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi thêm mới Ngân hàng")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objNganHang As infNganHang) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE tcs_dm_nganhang " & _
                                   "   SET ma = '" + objNganHang.MA_NH + "', " & _
                                    "       ma_xa = '" + objNganHang.MA_XA + "', " & _
                                   "       ten = '" + objNganHang.TEN_NH + "', " & _
                                   "       shkb = '" + objNganHang.SHKB + "' " & _
                                   " WHERE id = '" + objNganHang.ID + "' "
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi cập nhật Ngân hàng")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objNganHang As infNganHang) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM tcs_dm_nganhang " & _
                                    "      WHERE id = '" + objNganHang.ID + "' "
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi xóa Ngân hàng")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá danh mục loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function CheckExist(ByVal objNganHang As infNganHang) As Boolean
            Return LoadID(objNganHang.MA_NH, objNganHang.SHKB).SHKB <> ""
        End Function

        Public Shared Function Check2Del(ByVal objNganHang As infNganHang) As Byte
            Dim cnNganHang As DataAccess
            Dim drNganHang As IDataReader
            Dim strSQL As String
            Dim bytResult As Byte = 0

            Try
                strSQL = "SELECT shkb " & _
                        "  FROM tcs_dm_nganhang " & _
                        " WHERE ma_xa = '" & objNganHang.MA_XA & "' "
                cnNganHang = New DataAccess
                drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
                If drNganHang.Read Then bytResult = 1
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi kiểm tra thông tin Ngân hàng")
                Throw ex
            Finally
                If Not drNganHang Is Nothing Then drNganHang.Close()
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try

            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function
    End Class
End Namespace
