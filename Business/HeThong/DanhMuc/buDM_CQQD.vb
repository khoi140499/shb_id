﻿Imports VBOracleLib
Imports Business.DanhMuc.CQQD

Namespace DanhMuc
    Public Class buDM_CQQD
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function ADD_DM_CQQD(ByVal pv_StrMA_CQQD As String, ByVal pv_StrTEN_CQQD As String, ByVal pv_StrMA_CQTHU As String, ByVal pv_StrGHI_CHU As String, _
                                    ByVal pv_StrMA_KB As String, ByVal pv_StrMAKER As String, ByVal pv_StrMA_DBHC As String) As String
            Try
                'kiem tra trung
                Dim strSelect As String = "SELECT * FROM TCS_DM_CQQD WHERE UPPER(MA_CQQD)='" & pv_StrMA_CQQD.ToUpper() & "' AND MA_KB='" & pv_StrMA_KB & "'  AND TRANGTHAI <> 0 "
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return "Trùng mã cơ quan thu vui lòng kiểm tra lại!"
                        End If
                    End If
                End If
                'insert
                strSelect = "INSERT INTO TCS_DM_CQQD( MA_CQQD, TEN_CQQD,   GHI_CHU, MA_CQTHU,"
                strSelect &= "  MA_KB, MDATE, MAKER,MA_DBHC, ID,trangthai,tinh_trang)VALUES("
                strSelect &= " :MA_CQQD, :TEN_CQQD,   :GHI_CHU, :MA_CQTHU,"
                strSelect &= " :MA_KB, SYSDATE, :MAKER,:MA_DBHC, tcs_cqqd_bienlai_seq.nextval,'0','1')"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("MA_CQQD", ParameterDirection.Input, pv_StrMA_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("TEN_CQQD", ParameterDirection.Input, pv_StrTEN_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("GHI_CHU", ParameterDirection.Input, pv_StrGHI_CHU, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_CQTHU", ParameterDirection.Input, pv_StrMA_CQTHU, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_KB", ParameterDirection.Input, pv_StrMA_KB, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MAKER", ParameterDirection.Input, pv_StrMAKER, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_DBHC", ParameterDirection.Input, pv_StrMA_DBHC, DbType.String))

                DataAccess.ExecuteNonQuery(strSelect, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function


        'Public Function Insert(ByVal objCQQD As infCQQD, ByVal Nhom As String) As Boolean
        '    Dim cnCQQD As DataAccess
        '    Dim blnResult As Boolean = True
        '    Dim strSql As String = ""
        '    If Nhom = "10" Then
        '        strSql = "INSERT INTO HIS_TCS_DM_CQQD (id_cqqd,id,ma_cqqd,ten_cqqd,ghi_chu,ma_kb,ma_cqthu,ngay_tao,nguoi_tao,ma_dbhc,tinh_trang,type,tt_xu_ly) " & _
        '                        "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_TCS_DM_CQQD.NEXTVAL") & "," & Common.mdlCommon.getDataKey("tcs_cqqd_bienlai_seq.NEXTVAL") & ", " & _
        '                        "'" & objCQQD.MA_CQQD & "','" & objCQQD.TEN_CQQD & "'," & _
        '                        "'" & objCQQD.GHI_CHU & ",'" & objCQQD.MA_KB & ",'" & objCQQD.MA_CQTHU & ", SYSDATE ,'" & objCQQD.NGUOI_TAO & "','" & objCQQD.MA_DBHC & "','" & objCQQD.TINH_TRANG & "','" & objCQQD.TYPE & "','0')"
        '    ElseIf Nhom = "11" Then
        '        If objCQQD.TT_XU_LY = "1" Then
        '            'nếu duyệt
        '            strSql = "INSERT INTO TCS_DM_CQQD (MA_CQQD,TEN_CQQD,GHI_CHU,MA_CQTHU,MA_KB, MDATE, MAKER,MA_DBHC, ID,TRANGTHAI,TINHTRANG) " & _
        '                        " SELECT HIS.MA_CQQD,HIS.TEN_CQQD,HIS.GHI_CHU,HIS.MA_CQTHU,HIS.MA_KB,HIS.MA_DBHC,HIS.ID,HIS.TRANGTHAI,HIS.TINH_TRANG " & _
        '                        " FROM HIS_TCS_DM_CQQD  HIS WHERE HIS.ID_CQQD=" & objCQQD.ID
        '        ElseIf objCQQD.TT_XU_LY = "2" Then
        '            'nếu từ chối
        '            strSql = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET='" & objCQQD.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID_CQQD=" & objCQQD.ID
        '        End If
        '    End If

        '    Try
        '        cnCQQD = New DataAccess
        '        cnCQQD.ExecuteNonQuery(strSql, CommandType.Text)
        '        If Nhom = "11" And objCQQD.TT_XU_LY = "1" Then
        '            'Khi đã duyệt thêm mới 1 bản ghi
        '            Dim sqlUpdate As String = "UPDATE HIS_DM_CAP_CHUONG SET NGUOI_DUYET ='" & objCQQD.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID_CQQD=" & objCQQD.ID
        '            cnCQQD.ExecuteNonQuery(sqlUpdate, CommandType.Text)
        '        End If
        '    Catch ex As Exception
        '        log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới cấp - chương")
        '        'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới cấp - chương: " & ex.ToString)
        '        blnResult = False
        '        Throw ex
        '    Finally
        '        If Not cnCQQD Is Nothing Then cnCQQD.Dispose()
        '    End Try
        '    Return blnResult
        'End Function


        'Public Function Update(ByVal objCQQD As infCQQD, ByVal Nhom As String) As Boolean
        '    Dim cnCQQD As DataAccess
        '    Dim blnResult As Boolean = True
        '    Dim strSql As String = ""
        '    If Nhom = "10" Then
        '        strSql = "INSERT INTO HIS_TCS_DM_CQQD (id_cqqd,id,ma_cqqd,ten_cqqd,ghi_chu,ma_kb,ma_cqthu,ngay_tao,nguoi_tao,ma_dbhc,tinh_trang,type,tt_xu_ly) " & _
        '                      "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_TCS_DM_CQQD.NEXTVAL") & "," & Common.mdlCommon.getDataKey("tcs_cqqd_bienlai_seq.NEXTVAL") & ", " & _
        '                      "'" & objCQQD.MA_CQQD & "','" & objCQQD.TEN_CQQD & "'," & _
        '                      "'" & objCQQD.GHI_CHU & ",'" & objCQQD.MA_KB & ",'" & objCQQD.MA_CQTHU & ", SYSDATE ,'" & objCQQD.NGUOI_TAO & "','" & objCQQD.MA_DBHC & "','" & objCQQD.TINH_TRANG & "','" & objCQQD.TYPE & "','0')"
        '    ElseIf Nhom = "11" Then
        '        If objCQQD.TT_XU_LY = "1" Then
        '            strSql = "UPDATE TCS_DM_CQQD SET TEN_CQQD='" & objCQQD.TEN_CQQD.Trim & "',GHI_CHU='" & objCQQD.GHI_CHU.Trim & "', TINH_TRANG='" & objCQQD.TINH_TRANG.Trim & "'  WHERE ID=" & objCQQD.ID.Trim
        '        ElseIf objCQQD.TT_XU_LY = "2" Then
        '            strSql = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET='" & objCQQD.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID_CQQD=" & objCQQD.ID
        '        End If
        '    End If
        '    Try
        '        cnCQQD = New DataAccess
        '        cnCQQD.ExecuteNonQuery(strSql, CommandType.Text)
        '        If Nhom = "11" And objCQQD.TT_XU_LY = "1" Then
        '            'Khi đã duyệt sửa bản ghi
        '            Dim sqlUpdate As String = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET ='" & objCQQD.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID_CQQD=" & objCQQD.ID
        '            cnCQQD.ExecuteNonQuery(sqlUpdate, CommandType.Text)
        '        End If
        '    Catch ex As Exception
        '        'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật cấp - chương: " & ex.ToString)
        '        log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật cấp - chương")
        '        blnResult = False
        '        Throw ex
        '    Finally
        '        If Not cnCQQD Is Nothing Then cnCQQD.Dispose()
        '    End Try
        '    Return blnResult
        'End Function


        'Public Function Delete(ByVal objCQQD As infCQQD, ByVal Nhom As String) As Boolean
        '    Dim cnCQQD As DataAccess
        '    Dim blnResult As Boolean = True
        '    Dim strSql As String = ""
        '    If Nhom = "10" Then
        '        strSql = "INSERT INTO HIS_TCS_DM_CQQD (id_cqqd,id,ma_cqqd,ten_cqqd,ghi_chu,ma_kb,ma_cqthu,ngay_tao,nguoi_tao,ma_dbhc,tinh_trang,type,tt_xu_ly) " & _
        '                    "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_TCS_DM_CQQD.NEXTVAL") & "," & Common.mdlCommon.getDataKey("tcs_cqqd_bienlai_seq.NEXTVAL") & ", " & _
        '                    "'" & objCQQD.MA_CQQD & "','" & objCQQD.TEN_CQQD & "'," & _
        '                    "'" & objCQQD.GHI_CHU & ",'" & objCQQD.MA_KB & ",'" & objCQQD.MA_CQTHU & ", SYSDATE ,'" & objCQQD.NGUOI_TAO & "','" & objCQQD.MA_DBHC & "','" & objCQQD.TINH_TRANG & "','" & objCQQD.TYPE & "','0')"
        '    ElseIf Nhom = "11" Then
        '        If objCQQD.TT_XU_LY = "1" Then
        '            strSql = "DELETE FROM TCS_DM_CQQD " & _
        '                       "WHERE ID=" & objCQQD.ID
        '        ElseIf objCQQD.TT_XU_LY = "2" Then
        '            strSql = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET='" & objCQQD.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID_CQQD=" & objCQQD.ID
        '        End If
        '    End If
        '    Try
        '        cnCQQD = New DataAccess
        '        cnCQQD.ExecuteNonQuery(strSql, CommandType.Text)
        '        If Nhom = "11" And objCQQD.TT_XU_LY = "1" Then
        '            'Khi đã duyệt sửa bản ghi
        '            Dim sqlUpdate As String = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET ='" & objCQQD.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID_CQQD=" & objCQQD.ID
        '            cnCQQD.ExecuteNonQuery(sqlUpdate, CommandType.Text)
        '        End If
        '    Catch ex As Exception
        '        log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa cấp - chương")
        '        Throw ex
        '        'LogDebug.Writelog("Có lỗi xảy ra khi xoá cơ mục lục cấp - chương: " & ex.ToString)
        '        blnResult = False
        '    Finally
        '        If Not cnCQQD Is Nothing Then cnCQQD.Dispose()
        '    End Try
        '    Return blnResult
        'End Function

        Public Function UPDATE_DM_CQQD(ByVal pv_StrMA_CQQD As String, ByVal pv_StrTEN_CQQD As String, ByVal pv_StrMA_CQTHU As String, ByVal pv_StrGHI_CHU As String, _
                                   ByVal pv_StrMA_KB As String, ByVal pv_StrMAKER As String, ByVal pv_StrMA_DBHC As String, ByVal ID As String, ByVal pv_TINH_TRANG As String) As String
            Try
                'kiem tra trung
                Dim strSelect As String = "SELECT * FROM TCS_DM_CQQD WHERE UPPER(MA_CQQD)='" & pv_StrMA_CQQD.ToUpper() & "' AND MA_KB='" & pv_StrMA_KB & "'  AND TRANGTHAI <> 1 AND ID <> '" & ID & "' "
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return "Trùng mã cơ quan thu vui lòng kiểm tra lại!"
                        End If
                    End If
                End If
                'insert
                strSelect = "UPDATE TCS_DM_CQQD SET "
                strSelect &= " MA_CQQD =:MA_CQQD, TEN_CQQD =:TEN_CQQD,   GHI_CHU =:GHI_CHU, MA_CQTHU =:MA_CQTHU,"
                strSelect &= " MA_KB =:MA_KB, MDATE=SYSDATE, MAKER =:MAKER,MA_DBHC =:MA_DBHC WHERE ID =:ID,TINH_TRANG = '" + pv_TINH_TRANG + "'"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("MA_CQQD", ParameterDirection.Input, pv_StrMA_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("TEN_CQQD", ParameterDirection.Input, pv_StrTEN_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("GHI_CHU", ParameterDirection.Input, pv_StrGHI_CHU, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_CQTHU", ParameterDirection.Input, pv_StrMA_CQTHU, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_KB", ParameterDirection.Input, pv_StrMA_KB, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MAKER", ParameterDirection.Input, pv_StrMAKER, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_DBHC", ParameterDirection.Input, pv_StrMA_DBHC, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, ID, DbType.String))
                'ls.Add(DataAccess.NewDBParameter("TINH_TRANG", ParameterDirection.Input, pv_TINH_TRANG, DbType.String))
                DataAccess.ExecuteNonQuery(strSelect, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
        Public Function Search_DM_CQTHU(ByVal pv_StrMA_CQQD As String, ByVal pv_StrTEN_CQQD As String, ByVal pv_StrMA_CQTHU As String, ByVal pv_StrGHI_CHU As String, ByVal pv_StrMA_KB As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.MA_CQQD, A.TEN_CQQD, A.GHI_CHU, A.MA_CQTHU,"
                strSelect &= " A.MA_KB,TO_CHAR( A.MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, A.MAKER"
                strSelect &= " FROM TCS_DM_CQQD A  WHERE TRANGTHAI= '0' "
                If pv_StrMA_CQQD.Length > 0 Then
                    strSelect &= " AND UPPER(MA_CQQD) LIKE '%" & pv_StrMA_CQQD.ToUpper() & "%' "
                End If
                If pv_StrTEN_CQQD.Length > 0 Then
                    strSelect &= " AND UPPER(TEN_CQQD) LIKE '%" & pv_StrTEN_CQQD.ToUpper() & "%' "
                End If
                If pv_StrMA_CQTHU.Length > 0 Then
                    strSelect &= " AND UPPER(MA_CQTHU) LIKE '%" & pv_StrMA_CQTHU.ToUpper() & "%' "
                End If
                If pv_StrGHI_CHU.Length > 0 Then
                    strSelect &= " AND UPPER(GHI_CHU) LIKE '%" & pv_StrGHI_CHU.ToUpper() & "%' "
                End If
                If pv_StrMA_KB.Length > 0 Then
                    strSelect &= " AND UPPER(MA_KB) LIKE '%" & pv_StrMA_KB.ToUpper() & "%' "
                End If
                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function
        Public Function Load_DM_CQQD(ByVal pv_StrWhere As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.MA_CQQD, A.TEN_CQQD, A.GHI_CHU, A.MA_CQTHU,A.ma_dbhc,"
                strSelect &= " A.MA_KB,TO_CHAR( A.MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, A.MAKER, A.ID , " & _
                         "         DECODE (a.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 '0', 'Ngừng hoạt động', " & _
                         "                 '1', 'Đang hoạt động' " & _
                         "                ) tinh_trang "
                strSelect &= " FROM TCS_DM_CQQD A  WHERE TRANGTHAI= '0' " & pv_StrWhere

                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function

        Public Function Load_DM_CQQD_NEW(ByVal pv_StrWhere As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.MA_CQQD, A.TEN_CQQD, A.GHI_CHU, A.MA_CQTHU,A.ma_dbhc,"
                strSelect &= " A.MA_KB,TO_CHAR( A.MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, A.MAKER, A.ID"
                strSelect &= " FROM TCS_DM_CQQD A  WHERE TRANGTHAI= '0' " & pv_StrWhere

                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function

        Public Function GetInfoDetail(ByVal pv_StrWhere As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.MA_CQQD, A.TEN_CQQD, A.GHI_CHU, A.MA_CQTHU,A.ma_dbhc,"
                strSelect &= " A.MA_KB,TO_CHAR( A.MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, A.MAKER, A.ID , " & _
                         "         DECODE (a.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngừng hoạt động', " & _
                         "                 1, 'Đang hoạt động' " & _
                         "                ) tinh_trang "
                strSelect &= " FROM TCS_DM_CQQD A  WHERE TRANGTHAI= '0' " & pv_StrWhere

                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function

        Public Function DELETE_DM_CQQD(ByVal pv_StrID As String) As String
            Dim strDelete As String = "DELETE FROM TCS_DM_CQQD WHERE ID =: ID"
            Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
            ls.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, pv_StrID, DbType.String))
            DataAccess.ExecuteNonQuery(strDelete, CommandType.Text, ls.ToArray())
            Return "0"
        End Function
    End Class
End Namespace
