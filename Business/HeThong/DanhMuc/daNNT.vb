﻿Imports VBOracleLib
Namespace DanhMuc.NNT
    Public Class daNNT
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Load(ByVal strTK As String, ByVal strLoaiThue As String) As Object
            'Dim objNNT As New infNNT
            Dim cnNNT As DataAccess
            Dim dsNNT As DataSet
            Dim strSQL As String = "SELECT   a.ten_nnt ten_nnt, a.ma_nnt ma_nnt, a.ma_chuong ma_chuong, a.ma_khoan ma_khoan, st.ma_tmuc ma_tmuc, NULL AS so_tk, NULL AS ma_hq " & _
                        "from tcs_dm_nnt a, tcs_sothue st where a.ma_nnt=st.ma_nnt(+) " & _
                        " " & strTK
            Dim strSQL1 As String = "select a.ten_nnt,a.ma_nnt,a.ma_chuong,a.ma_khoan,a.ma_tmuc,a.so_tk,a.ma_hq from tcs_ds_tokhai a " & _
                        "Where 1=1 " & strTK
            Dim strSQL2 As String = "SELECT   a.* " & _
                        "FROM   (SELECT   t.tennnt ten_nnt, " & _
                        "t.mannt ma_nnt, " & _
                        "t.machuong ma_chuong, " & _
                        "t.makhoan ma_khoan, " & _
                        "t.matmuc ma_tmuc, " & _
                        "t.sotk so_tk, " & _
                        "t.mahq ma_hq " & _
                        "FROM   (SELECT   a.ten_nnt tennnt, " & _
                        "a.ma_nnt mannt, " & _
                        "a.ma_chuong machuong, " & _
                        "a.ma_khoan makhoan, " & _
                        "tk.ma_tmuc matmuc, " & _
                        "NULL AS sotk, " & _
                        "NULL AS mahq " & _
                        "FROM   tcs_dm_nnt a, tcs_sothue tk " & _
                        "WHERE   a.ma_nnt = tk.ma_nnt(+)) t " & _
                        "UNION ALL " & _
                        "SELECT   t.tennnt ten_nnt, " & _
                        "t.mannt ma_nnt, " & _
                        "t.machuong ma_chuong, " & _
                        "t.makhoan ma_khoan, " & _
                        "t.matmuc ma_tmuc, " & _
                        "t.sotk so_tk, " & _
                        "t.mahq ma_hq " & _
                        "FROM   (SELECT   a.ten_nnt tennnt, " & _
                        "a.ma_nnt mannt, " & _
                        "a.ma_chuong machuong, " & _
                        "a.ma_khoan makhoan, " & _
                        "a.ma_tmuc matmuc, " & _
                        "a.so_tk sotk, " & _
                        "a.ma_hq mahq " & _
                        "FROM   tcs_ds_tokhai a " & _
                        "WHERE   1 = 1) t) a " & _
                        "Where 1=1 " & strTK
            Try
                cnNNT = New DataAccess
                If strLoaiThue = "1" Then
                    dsNNT = cnNNT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                ElseIf strLoaiThue = "0" Then
                    dsNNT = cnNNT.ExecuteReturnDataSet(strSQL1, CommandType.Text)
                Else
                    dsNNT = cnNNT.ExecuteReturnDataSet(strSQL2, CommandType.Text)
                End If

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin tài khoản")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnNNT Is Nothing Then cnNNT.Dispose()
            End Try
            Return dsNNT
        End Function
    End Class
End Namespace
