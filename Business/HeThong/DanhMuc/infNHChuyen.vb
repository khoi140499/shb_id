﻿Namespace DanhMuc.NHChuyen
    Public Class infNHChuyen
        Private strMa_NHC As String
        Private strTen_NHC As String
        Private strHIS_ID As String
        Private strNGUOI_TAO As String
        Private strNGUOI_DUYET As String
        Private strTYPE As String
        Private strTT_XU_LY As String
        

        Sub New()
            strMa_NHC = ""
            strTen_NHC = ""
        End Sub

        Sub New(ByVal drwNHC As DataRow)
            strMa_NHC = drwNHC("ma_nh_chuyen").ToString().Trim()
            strTen_NHC = drwNHC("ten_nh_chuyen").ToString().Trim()
        End Sub

        Public Property Ma_NHC() As String
            Get
                Return strMa_NHC
            End Get
            Set(ByVal Value As String)
                strMa_NHC = Value
            End Set
        End Property
        Public Property Ten_NHC() As String
            Get
                Return strTen_NHC
            End Get
            Set(ByVal Value As String)
                strTen_NHC = Value
            End Set
        End Property
        Public Property HIS_ID() As String
            Get
                Return strHIS_ID
            End Get
            Set(ByVal Value As String)
                strHIS_ID = Value
            End Set
        End Property
        Public Property NGUOI_TAO() As String
            Get
                Return strNGUOI_TAO
            End Get
            Set(ByVal Value As String)
                strNGUOI_TAO = Value
            End Set
        End Property
        Public Property NGUOI_DUYET() As String
            Get
                Return strNGUOI_DUYET
            End Get
            Set(ByVal Value As String)
                strNGUOI_DUYET = Value
            End Set
        End Property
        Public Property TYPE() As String
            Get
                Return strTYPE
            End Get
            Set(ByVal Value As String)
                strTYPE = Value
            End Set
        End Property

        Public Property TT_XU_LY() As String
            Get
                Return strTT_XU_LY
            End Get
            Set(ByVal Value As String)
                strTT_XU_LY = Value
            End Set
        End Property
    End Class
End Namespace

