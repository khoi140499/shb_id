Namespace DanhMuc.CQThu
    Public Class infCQThu
        Private strMa_CQThu As String
        Private strTen As String
        Private strMa_Dthu As String
        Private strMa_HQ As String
        Private strMa_QLT As String
        Private strMa_TINH As String
        Private strMa_HUYEN As String
        Private strID As String
        Private strNGUOI_TAO As String
        Private strNGUOI_DUYET As String
        Private strTYPE As String
        Private strTT_XU_LY As String

        Sub New()
        End Sub

        Sub New(ByVal drwCQThu As DataRow)
            strMa_CQThu = drwCQThu("Ma_CQThu").ToString().Trim()
            strTen = drwCQThu("Ten").ToString().Trim()
        End Sub

        Public Property Ma_CQThu() As String
            Get
                Return strMa_CQThu
            End Get
            Set(ByVal Value As String)
                strMa_CQThu = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property
        Public Property Ma_Dthu() As String
            Get
                Return strMa_Dthu
            End Get
            Set(ByVal Value As String)
                strMa_Dthu = Value
            End Set
        End Property
        Public Property Ma_HQ() As String
            Get
                Return strMa_HQ
            End Get
            Set(ByVal Value As String)
                strMa_HQ = Value
            End Set
        End Property
        Public Property Ma_QLT() As String
            Get
                Return strMa_QLT
            End Get
            Set(ByVal Value As String)
                strMa_QLT = Value
            End Set
        End Property
        Public Property MA_TINH() As String
            Get
                Return strMa_TINH
            End Get
            Set(ByVal Value As String)
                strMa_TINH = Value
            End Set
        End Property
        Public Property MA_HUYEN() As String
            Get
                Return strMa_HUYEN
            End Get
            Set(ByVal Value As String)
                strMa_HUYEN = Value
            End Set
        End Property
        Public Property ID() As String
            Get
                Return strID
            End Get
            Set(ByVal Value As String)
                strID = Value
            End Set
        End Property
        Public Property NGUOI_TAO() As String
            Get
                Return strNGUOI_TAO
            End Get
            Set(ByVal Value As String)
                strNGUOI_TAO = Value
            End Set
        End Property
        Public Property NGUOI_DUYET() As String
            Get
                Return strNGUOI_DUYET
            End Get
            Set(ByVal Value As String)
                strNGUOI_DUYET = Value
            End Set
        End Property
        Public Property TYPE() As String
            Get
                Return strTYPE
            End Get
            Set(ByVal Value As String)
                strTYPE = Value
            End Set
        End Property

        Public Property TT_XU_LY() As String
            Get
                Return strTT_XU_LY
            End Get
            Set(ByVal Value As String)
                strTT_XU_LY = Value
            End Set
        End Property
    End Class
End Namespace
