Imports VBOracleLib

Namespace DanhMuc.CQThu
    Public Class daCQThu
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objCQThu As infCQThu, ByVal Nhom As String) As Boolean
            Dim cnCQThu As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_CQTHU (ID, Ma_CQThu, TEN, Ma_Dthu, SHKB, MA_HQ,MA_QLT,DBHC_TINH,DBHC_HUYEN,MA_TINH, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_CQTHU.NEXTVAL") & ", " & _
                                "'" & objCQThu.Ma_CQThu & "','" & objCQThu.Ten & "'," & _
                                "'" & objCQThu.Ma_Dthu & "','" & objCQThu.Ma_Dthu & "','" & objCQThu.Ma_HQ & "','" & objCQThu.Ma_QLT & "','" & _
                                objCQThu.MA_TINH & "','" & objCQThu.MA_HUYEN & "','" & objCQThu.MA_TINH.Replace("TTT", "") & "','" & _
                                objCQThu.NGUOI_TAO & "', SYSDATE,'0','" & objCQThu.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objCQThu.TT_XU_LY = "1" Then
                    'nếu duyệt
                    strSql = "INSERT INTO TCS_DM_CQTHU (Ma_CQThu, TEN, Ma_Dthu, SHKB, MA_HQ, MA_QLT,DBHC_TINH, DBHC_HUYEN, MA_TINH) " & _
                                " SELECT HIS.Ma_CQThu,HIS.TEN,HIS.Ma_Dthu,HIS.SHKB,HIS.MA_HQ,HIS.MA_QLT,HIS.DBHC_TINH,HIS.DBHC_HUYEN,HIS.MA_TINH " & _
                                " FROM HIS_DM_CQTHU HIS WHERE HIS.ID=" & objCQThu.ID
                ElseIf objCQThu.TT_XU_LY = "2" Then
                    'nếu từ chối
                    strSql = "UPDATE HIS_DM_CQTHU SET NGUOI_DUYET='" & objCQThu.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objCQThu.ID
                End If
            End If
            Try
                cnCQThu = New DataAccess
                cnCQThu.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCQThu.TT_XU_LY = "1" Then
                    'Khi đã duyệt thêm mới 1 bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_CQTHU SET NGUOI_DUYET ='" & objCQThu.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objCQThu.ID
                    cnCQThu.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi thêm mới Cơ quan thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới cơ quan thu: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objCQThu As infCQThu, ByVal Nhom As String) As Boolean
            Dim cnCQThu As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_CQTHU (ID, Ma_CQThu, TEN, Ma_Dthu, SHKB, MA_HQ, MA_QLT,DBHC_TINH,DBHC_HUYEN,MA_TINH, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_CQTHU.NEXTVAL") & ", " & _
                                "'" & objCQThu.Ma_CQThu & "','" & objCQThu.Ten & "'," & _
                                "'" & objCQThu.Ma_Dthu & "','" & objCQThu.Ma_Dthu & "','" & objCQThu.Ma_HQ & "','" & objCQThu.Ma_QLT & "','" & _
                                objCQThu.MA_TINH & "','" & objCQThu.MA_HUYEN & "','" & objCQThu.MA_TINH.Replace("TTT", "") & "','" & _
                                objCQThu.NGUOI_TAO & "', SYSDATE,'0','" & objCQThu.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objCQThu.TT_XU_LY = "1" Then
                    strSql = "UPDATE TCS_DM_CQTHU SET TEN='" & objCQThu.Ten.Trim & "',Ma_Dthu='" & objCQThu.Ma_Dthu.Trim & "', MA_HQ='" & objCQThu.Ma_HQ.Trim & "', MA_QLT='" & objCQThu.Ma_QLT.Trim & "', SHKB='" & objCQThu.Ma_Dthu.Trim & _
                               "',MA_TINH= '" & objCQThu.MA_TINH.Replace("TTT", "") & "',DBHC_TINH= '" & objCQThu.MA_TINH & "',DBHC_HUYEN= '" & objCQThu.MA_HUYEN & "'  WHERE Ma_CQThu=" & objCQThu.Ma_CQThu.Trim
                ElseIf objCQThu.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_CQTHU SET NGUOI_DUYET='" & objCQThu.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objCQThu.ID
                End If
            End If
            Try
                cnCQThu = New DataAccess
                cnCQThu.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCQThu.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_CQTHU SET NGUOI_DUYET ='" & objCQThu.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objCQThu.ID
                    cnCQThu.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi cập nhật Cơ quan thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật cơ quan thu: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objCQThu As infCQThu, ByVal Nhom As String) As Boolean
            Dim cnCQThu As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_CQTHU (ID,Ma_CQThu, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_CQTHU.NEXTVAL") & _
                        ",'" & objCQThu.Ma_CQThu & "','" & objCQThu.NGUOI_TAO & "', SYSDATE,'0','" & objCQThu.TYPE & "') "
            ElseIf Nhom = "11" Then
                If objCQThu.TT_XU_LY = "1" Then
                    strSql = "DELETE FROM TCS_DM_CQTHU " & _
                               "WHERE Ma_CQThu=" & objCQThu.Ma_CQThu
                ElseIf objCQThu.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_CQTHU SET NGUOI_DUYET='" & objCQThu.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objCQThu.ID
                End If
            End If
            Try
                cnCQThu = New DataAccess
                cnCQThu.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCQThu.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_CQTHU SET NGUOI_DUYET ='" & objCQThu.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objCQThu.ID
                    cnCQThu.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi xóa Cơ quan thu")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá cơ quan thu: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strMa_CQThu As String) As infCQThu
            Dim objCQThu As New infCQThu
            Dim cnCQThu As DataAccess
            Dim drCQThu As IDataReader
            Dim strSQL As String = "Select MA_CQTHU, TEN,Ma_Dthu From TCS_DM_CQTHU " & _
                        "Where MA_CQTHU='" & strMa_CQThu.ToUpper() & "'"
            Try
                cnCQThu = New DataAccess
                drCQThu = cnCQThu.ExecuteDataReader(strSQL, CommandType.Text)
                If drCQThu.Read Then
                    objCQThu = New infCQThu
                    objCQThu.Ma_CQThu = strMa_CQThu
                    objCQThu.Ten = drCQThu("Ten").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Cơ quan thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cơ quan thu từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drCQThu Is Nothing Then drCQThu.Close()
                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
            End Try
            Return objCQThu
        End Function

        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As Object
            Dim cnCQThu As DataAccess
            Dim dsCQThu As DataSet
            Dim strSQL As String = "SELECT   c.ma_cqthu, c.ten, c.ma_dthu, d.ten AS ten_diemthu,c.ma_hq,c.ma_qlt,c.dbhc_tinh,c.dbhc_huyen,(select ten from tcs_dm_xa where ma_xa=c.dbhc_Tinh) ten_tinh,(select ten from tcs_dm_xa where ma_xa=c.dbhc_huyen) ten_Huyen " & _
                                    "    FROM tcs_dm_cqthu c, tcs_dm_khobac d " & _
                                    "   WHERE c.shkb = d.shkb " & WhereClause & _
                                    "ORDER BY ma_cqthu "

            Try
                cnCQThu = New DataAccess
                dsCQThu = cnCQThu.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Cơ quan thu")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cơ quan thu từ CSDL: " & ex.ToString)
            Finally
                If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
            End Try
            Return dsCQThu
        End Function
        Public Function Load_new(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String
            Dim cnCapChuong As DataAccess
            Dim dsCapChuong As DataSet
            Try
                strSQL = "SELECT b.DBHC_TINH, b.MA_TINH, b.DBHC_HUYEN," & _
                         " DECODE(b.TYPE,NULL,'',1,'Thêm',2,'Sửa',3,'Xóa') ThayDoi ,DECODE(b.TT_XU_LY,NULL,'',0,'Chờ duyệt',1,'Đã duyệt',2,'Từ chối') xu_ly ," & _
                         " b.id,b.Type,b.TT_XU_LY, b.ma_cqthu, b.ten , b.ma_dthu ,b.ma_hq ,b.ma_qlt,(select ten from tcs_dm_xa where ma_xa=b.dbhc_Tinh) ten_tinh,(select ten from tcs_dm_xa where ma_xa=b.dbhc_huyen) ten_Huyen" & _
                         " FROM  HIS_DM_CQTHU b " & _
                         " WHERE 1=1 " & WhereClause & _
                         "ORDER BY b.ID DESC,b.ma_cqthu "
                cnCapChuong = New DataAccess
                dsCapChuong = cnCapChuong.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cơ quan thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnCapChuong.Dispose()
            End Try
            Return dsCapChuong
        End Function

        Public Function CheckExist(ByVal objCQThu As infCQThu) As Boolean
            Return Load(objCQThu.Ma_CQThu).Ma_CQThu <> ""
        End Function

        Public Shared Function Check2Del(ByVal objCQThu As infCQThu) As Byte
            '---------------------------------------------------------------
            ' Mục đích: Kiểm tra CQT có thể xoá không
            ' Tham số: objCQThu: cơ quan cần kiểm tra
            ' Giá trị trả về: TRUE nếu hợp lệ, FALSE nếu không hợp lệ
            ' Ngày viết: 26/10/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------
            Dim cnCQThu As DataAccess
            Dim drQThu As IDataReader
            Dim strSQL As String
            Dim bytResult As Byte = 0

            If Not drQThu Is Nothing Then drQThu.Close()
            If Not cnCQThu Is Nothing Then cnCQThu.Dispose()
            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function
        Public Function CheckDelete(ByVal strMA_CQthu As String) As Object
            Dim cnCQT As DataAccess
            Dim dsCQT As DataSet

            Try
                Dim strSQL As String = "select ma_cqthu from (select ma_cqthu from tcs_ctu_hdr union all select ma_cqthu from tcs_baolanh_hdr) " & _
                        "Where ma_cqthu='" & strMA_CQthu & "'"

                cnCQT = New DataAccess
                dsCQT = cnCQT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnCQT Is Nothing Then cnCQT.Dispose()
            End Try

            Return dsCQT
        End Function
    End Class

End Namespace
