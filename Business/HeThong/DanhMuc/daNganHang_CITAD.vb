﻿Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace DanhMuc.NganHang
    Public Class daNganHang_CITAD
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim dsNganHang As New DataSet

            Try
                strSQL = "SELECT a.SHKB, b.ten TEN_KB, a.MA_TRUCTIEP, a.TEN_TRUCTIEP, a.MA_GIANTIEP, a.TEN_GIANTIEP,a.ID,DECODE(a.TT_SHB,NULL,'',0,'Không',1,'Có') as NH_SHB,a.TT_SHB  FROM TCS_DM_NH_GIANTIEP_TRUCTIEP a, tcs_dm_khobac b  "
                strSQL += " where a.shkb= b.shkb " & WhereClause
                strSQL += " order by shkb "
                dsNganHang = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
            Finally

            End Try

            Return dsNganHang
        End Function
        Public Function Load_new(ByVal WhereClause As String) As DataSet
            Dim strSQL As String
            Dim cnNganHang As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "SELECT   a.SHKB, a.TEN_KB, a.MA_TRUCTIEP, a.TEN_TRUCTIEP, a.MA_GIANTIEP, a.TEN_GIANTIEP,DECODE(a.TT_SHB,NULL,'',0,'Không',1,'Có') as NH_SHB,b.TEN_KB AS TEN_KB_NEW,a.ID,  " & _
                         " b.MA_TRUCTIEP MA_TRUCTIEP_NEW, b.TEN_TRUCTIEP TEN_TRUCTIEP_NEW,b.MA_GIANTIEP MA_GIANTIEP_NEW, b.TEN_GIANTIEP TEN_GIANTIEP_NEW, DECODE(b.TT_SHB,NULL,'',0,'Không',1,'Có') as NH_SHB_new,DECODE(b.TYPE,NULL,'',1,'Thêm',2,'Sửa',3,'Xóa') ThayDoi ,DECODE(b.TT_XU_LY,NULL,'',0,'Chờ duyệt',1,'Đã duyệt',2,'Từ chối') xu_ly , " & _
                         " b.his_id,b.SHKB as SHKB_NEW,b.Type,b.TT_XU_LY  " & _
                         " FROM HIS_DM_NGAN_HANG b, TCS_DM_NH_GIANTIEP_TRUCTIEP a " & _
                         " WHERE b.ID_GT_TT=a.ID(+) " & WhereClause & _
                         " ORDER BY b.HIS_ID DESC "
                cnNganHang = New DataAccess
                dsCapChuong = cnNganHang.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnNganHang.Dispose()
            End Try

            Return dsCapChuong
        End Function

        'Public Function Load(ByVal strMA_XA As String, ByVal strMA_NH As String) As infNganHang_CITAD
        '    Dim objNganHang As infNganHang_CITAD
        '    Dim cnNganHang As DataAccess
        '    Dim drNganHang As IDataReader
        '    Dim strSQL As String = ""
        '    Try
        '        strSQL = "SELECT id,ma_xa, ma, ten, shkb " & _
        '                 "  FROM tcs_dm_nh_giantiep_tructiep, ,TT_KB " & _
        '                 " WHERE ma_xa = '" + strMA_XA + "' " & _
        '                 "   AND ma = '" + strMA_NH + "' "

        '        cnNganHang = New DataAccess
        '        drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drNganHang.Read Then
        '            objNganHang = New infNganHang_CITAD
        '            objNganHang.MA_XA = strMA_XA
        '            objNganHang.MA_NH = drNganHang("ma").ToString()
        '            objNganHang.TEN_NH = drNganHang("ten").ToString()
        '            objNganHang.SHKB = drNganHang("shkb").ToString()
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If Not drNganHang Is Nothing Then drNganHang.Close()
        '        If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
        '    End Try
        '    Return objNganHang
        'End Function
        Public Function LoadID(ByVal strID As String) As infNganHang_CITAD
            Dim objNganHang As infNganHang_CITAD
            Dim cnNganHang As DataAccess
            Dim drNganHang As IDataReader
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT shkb,ten_kb,ma_tructiep,ten_tructiep,ma_giantiep,ten_giantiep,TT_KB    " & _
                         "  FROM tcs_dm_nh_giantiep_tructiep " & _
                         " WHERE shkb= '" + strID + "' "

                cnNganHang = New DataAccess
                drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
                If drNganHang.Read Then
                    objNganHang = New infNganHang_CITAD
                    objNganHang.SHKB = drNganHang("shkb").ToString()
                    objNganHang.Ten_KB = drNganHang("ten_kb").ToString()
                    objNganHang.MA_NH_TT = drNganHang("ma_tructiep").ToString()
                    objNganHang.TEN_NH_TT = drNganHang("ten_tructiep").ToString()
                    objNganHang.MA_NH_TH = drNganHang("ma_giantiep").ToString()
                    objNganHang.TEN_NH_TH = drNganHang("ten_giantiep").ToString()
                    objNganHang.TT_KB = drNganHang("TT_KB").ToString()
                End If
            Catch ex As Exception
                Throw ex
            Finally
                If Not drNganHang Is Nothing Then drNganHang.Close()
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return objNganHang
        End Function

        Public Function Insert(ByVal objNganHang As infNganHang_CITAD, ByVal Nhom As String) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_NGAN_HANG (HIS_ID, SHKB, ten_kb, ma_tructiep,ten_tructiep, ten_giantiep, ma_giantiep, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE,ID_GT_TT, TT_SHB) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_NGAN_HANG.NEXTVAL") & ",'" & objNganHang.SHKB & "', " & _
                                "'" & objNganHang.Ten_KB & "','" & objNganHang.MA_NH_TT & "','" & objNganHang.TEN_NH_TT & "', " & _
                                "'" & objNganHang.TEN_NH_TH & "','" & objNganHang.MA_NH_TH & "','" & objNganHang.NGUOI_TAO & "', SYSDATE,'0','" & objNganHang.TYPE & "',id_seq_nh_gt_tt.NEXTVAL, '" & objNganHang.NH_SHB & "')"
            ElseIf Nhom = "11" Then
                If objNganHang.TT_XU_LY = "1" Then
                    'nếu duyệt
                    strSql = "INSERT INTO TCS_DM_NH_GIANTIEP_TRUCTIEP (SHKB, TEN_KB, ma_tructiep, ten_tructiep, ma_giantiep, ten_giantiep, ID, TT_SHB) " & _
                                " SELECT HIS.SHKB, HIS.TEN_KB, HIS.ma_tructiep, HIS.ten_tructiep, HIS.ma_giantiep, HIS.ten_giantiep,HIS.ID_GT_TT, HIS.TT_SHB " & _
                                " FROM HIS_DM_NGAN_HANG HIS WHERE HIS.HIS_ID=" & objNganHang.HIS_ID
                ElseIf objNganHang.TT_XU_LY = "2" Then
                    'nếu từ chối
                    strSql = "UPDATE HIS_DM_NGAN_HANG SET NGUOI_DUYET='" & objNganHang.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE HIS_ID=" & objNganHang.HIS_ID
                End If
            End If
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objNganHang.TT_XU_LY = "1" Then
                    'Khi đã duyệt thêm mới 1 bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_NGAN_HANG SET NGUOI_DUYET ='" & objNganHang.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE HIS_ID=" & objNganHang.HIS_ID
                    cnNganHang.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objNganHang As infNganHang_CITAD, ByVal Nhom As String) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_NGAN_HANG (HIS_ID,SHKB,TEN_KB, ma_tructiep, ten_tructiep, ma_giantiep, ten_giantiep,NGUOI_TAO , NGAY_TAO,TT_XU_LY,TYPE,ID_GT_TT, TT_SHB) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_NGAN_HANG.NEXTVAL") & ",'" & objNganHang.SHKB.Trim & "', " & _
                                "'" & objNganHang.Ten_KB & "','" & objNganHang.MA_NH_TT & "','" & objNganHang.TEN_NH_TT & "'," & _
                                "'" & objNganHang.MA_NH_TH & "','" & objNganHang.TEN_NH_TH & "','" & objNganHang.NGUOI_TAO & "', SYSDATE,'0','" & objNganHang.TYPE & "','" & objNganHang.IDSE & "','" & objNganHang.NH_SHB & "')"
            ElseIf Nhom = "11" Then
                If objNganHang.TT_XU_LY = "1" Then
                    strSql = "UPDATE TCS_DM_NH_GIANTIEP_TRUCTIEP SET ma_tructiep='" & objNganHang.MA_NH_TT.Trim & "',ten_tructiep='" & objNganHang.TEN_NH_TT.Trim & "',ma_giantiep='" & objNganHang.MA_NH_TH & _
                             "', TEN_KB='" & objNganHang.Ten_KB & "', TEN_giantiep='" & objNganHang.TEN_NH_TH.Trim & "', TT_SHB='" & objNganHang.NH_SHB & "'  WHERE SHKB='" & objNganHang.SHKB.Trim & "' and id='" & objNganHang.IDSE & "' "
                ElseIf objNganHang.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_NGAN_HANG SET NGUOI_DUYET='" & objNganHang.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE HIS_ID=" & objNganHang.HIS_ID
                End If
            End If
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objNganHang.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_NGAN_HANG SET NGUOI_DUYET ='" & objNganHang.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE HIS_ID=" & objNganHang.HIS_ID
                    cnNganHang.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objNganHang As infNganHang_CITAD, ByVal Nhom As String) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_NGAN_HANG (HIS_ID,SHKB, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE,ID_GT_TT) VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_NGAN_HANG.NEXTVAL") & ",'" & _
                        objNganHang.SHKB.Trim & "','" & objNganHang.NGUOI_TAO & "', SYSDATE,'0','" & objNganHang.TYPE & "','" & objNganHang.IDSE & "') "
            ElseIf Nhom = "11" Then
                If objNganHang.TT_XU_LY = "1" Then
                    strSql = "DELETE FROM TCS_DM_NH_GIANTIEP_TRUCTIEP " & _
                               "WHERE ID = '" & objNganHang.IDSE & "'"
                ElseIf objNganHang.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_NGAN_HANG SET NGUOI_DUYET='" & objNganHang.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE HIS_ID=" & objNganHang.HIS_ID
                End If
            End If
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objNganHang.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_NGAN_HANG SET NGUOI_DUYET ='" & objNganHang.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE HIS_ID=" & objNganHang.HIS_ID
                    cnNganHang.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá danh mục loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function CheckExist(ByVal objNganHang As infNganHang_CITAD) As Boolean
            If Not LoadID(objNganHang.SHKB) Is Nothing Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function Check2Del(ByVal objNganHang As infNganHang) As Byte
            Dim cnNganHang As DataAccess
            Dim drNganHang As IDataReader
            Dim strSQL As String
            Dim bytResult As Byte = 0

            Try
                strSQL = "SELECT shkb " & _
                        "  FROM tcs_dm_nh_giantiep_tructiep " & _
                        " WHERE ma_xa = '" & objNganHang.MA_XA & "' "
                cnNganHang = New DataAccess
                drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
                If drNganHang.Read Then bytResult = 1
            Catch ex As Exception
                Throw ex
            Finally
                If Not drNganHang Is Nothing Then drNganHang.Close()
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try

            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function
        Public Function CheckExist_GT_TT(ByVal objNganHang As infNganHang_CITAD) As Boolean
            If CheckExis_TT_GT(objNganHang.SHKB, objNganHang.MA_NH_TT, objNganHang.MA_NH_TH) = True Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Function CheckExis_TT_GT(ByVal strID As String, ByVal strMa_tructiep As String, ByVal strMa_giantiep As String) As Boolean
            Dim blResult As Boolean = True
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT * FROM tcs_dm_nh_giantiep_tructiep " & _
                         " WHERE shkb= '" + strID + "' and  ma_tructiep='" & strMa_tructiep & "' and ma_giantiep ='" & strMa_giantiep & "'"

                Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    If dt.Rows.Count > 1 Then
                        blResult = False
                    End If

                End If
            Catch ex As Exception
                Throw ex
            Finally
            End Try
            Return blResult
        End Function
        Public Function CheckExist_GT_TT_Insert(ByVal objNganHang As infNganHang_CITAD) As Boolean
            If CheckExis_TT_GT_Insert(objNganHang.SHKB, objNganHang.MA_NH_TT, objNganHang.MA_NH_TH) = True Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Function CheckExis_TT_GT_Insert(ByVal strID As String, ByVal strMa_tructiep As String, ByVal strMa_giantiep As String) As Boolean
            Dim blResult As Boolean = True
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT * FROM tcs_dm_nh_giantiep_tructiep " & _
                         " WHERE shkb= '" + strID + "' and  ma_tructiep='" & strMa_tructiep & "' and ma_giantiep ='" & strMa_giantiep & "'"

                Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    blResult = False
                End If
            Catch ex As Exception
                Throw ex
            Finally
            End Try
            Return blResult
        End Function
    End Class
        
End Namespace
