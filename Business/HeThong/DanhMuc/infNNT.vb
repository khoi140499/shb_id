﻿Namespace DanhMuc.NNT
    Public Class infNNT
        Private strMaNNT As String
        Private strTEN_NNT As String

        Sub New()
            strMaNNT = ""
            strTEN_NNT = ""
        End Sub

        Public Property MaNNT() As String
            Get
                Return strMaNNT
            End Get
            Set(ByVal Value As String)
                strMaNNT = Value
            End Set
        End Property

        Public Property TenNNT() As String
            Get
                Return strTEN_NNT
            End Get
            Set(ByVal Value As String)
                strTEN_NNT = Value
            End Set
        End Property
    End Class
End Namespace

