﻿Imports VBOracleLib
Namespace DanhMuc.CQQD_LHThu
    Public Class daCQQD_LHThu
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objCQQD As infCQQD_LHThu, ByVal Nhom As String) As Boolean
            Dim cnKhoBac As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO tcs_map_cqqd_lhthu_tmp (ID,ID_HIS,ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("tcs_map_cqqd_lhthu_tmp_seq.NEXTVAL") & ",'" & objCQQD.ID & "'," & _
                                "'" & objCQQD.MA_QUY & "','" & objCQQD.TK_NS & "','" & objCQQD.MA_CHUONG & "','" & objCQQD.MA_NKT & "','" & objCQQD.MA_NDKT & "'," & _
                                "'" & objCQQD.MA_DBHC & "','" & objCQQD.KHTK & "','" & objCQQD.MA_LH & "','" & objCQQD.SHKB & "','" & objCQQD.MA_CQQD & "'," & _
                                "'" & objCQQD.MA_DVSDNS & "','" & objCQQD.TEN_CQ & "','" & objCQQD.TINH_TRANG & "','" & objCQQD.NGUOI_TAO & "', SYSDATE,'0','" & objCQQD.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objCQQD.TT_XU_LY = "1" Then
                    'nếu duyệt
                    strSql = "INSERT INTO tcs_map_cqqd_lhthu (ID,ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq, TINH_TRANG) " & _
                                " SELECT HIS.ID,HIS.ma_quy,HIS.tk_ns,HIS.ma_chuong,HIS.ma_nkt,HIS.ma_ndkt,HIS.ma_dbhc,HIS.khtk,HIS.ma_lh,HIS.shkb,HIS.ma_cqqd,HIS.ma_dvsdns,HIS.ten_cq, HIS.TINH_TRANG " & _
                                " FROM tcs_map_cqqd_lhthu_tmp HIS WHERE HIS.ID=" & objCQQD.ID
                ElseIf objCQQD.TT_XU_LY = "2" Then
                    'nếu từ chối
                    strSql = "UPDATE tcs_map_cqqd_lhthu_tmp SET NGUOI_DUYET='" & objCQQD.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objCQQD.ID
                End If
            End If
            Try
                cnKhoBac = New DataAccess
                cnKhoBac.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCQQD.TT_XU_LY = "1" Then
                    'Khi đã duyệt thêm mới 1 bản ghi
                    Dim sqlUpdate As String = "UPDATE tcs_map_cqqd_lhthu_tmp SET NGUOI_DUYET ='" & objCQQD.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objCQQD.ID
                    cnKhoBac.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi thêm mới danh mục cơ quan quyết định - loại hình thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objCQQD As infCQQD_LHThu, ByVal Nhom As String) As Boolean
            Dim cnKhoBac As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String
            If Nhom = "10" Then
                strSql = "INSERT INTO tcs_map_cqqd_lhthu_tmp (ID,ID_HIS,ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("tcs_map_cqqd_lhthu_tmp_seq.NEXTVAL") & ", '" & objCQQD.ID_HIS & "' " & _
                                ",'" & objCQQD.MA_QUY & "','" & objCQQD.TK_NS & "','" & objCQQD.MA_CHUONG & "','" & objCQQD.MA_NKT & "','" & objCQQD.MA_NDKT & "'," & _
                                "'" & objCQQD.MA_DBHC & "','" & objCQQD.KHTK & "','" & objCQQD.MA_LH & "','" & objCQQD.SHKB & "','" & objCQQD.MA_CQQD & "'," & _
                                "'" & objCQQD.MA_DVSDNS & "','" & objCQQD.TEN_CQ & "','" & objCQQD.TINH_TRANG & "','" & objCQQD.NGUOI_TAO & "', SYSDATE,'0','" & objCQQD.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objCQQD.TT_XU_LY = "1" Then
                    strSql = "UPDATE tcs_map_cqqd_lhthu SET ma_quy='" & objCQQD.MA_QUY.Trim & "',tk_ns='" & objCQQD.TK_NS.Trim & "',ma_chuong='" & objCQQD.MA_CHUONG & "', " & _
                            "ma_nkt='" & objCQQD.MA_NKT.Trim & "',ma_ndkt='" & objCQQD.MA_NDKT.Trim & "',ma_dbhc='" & objCQQD.MA_DBHC & "',khtk='" & objCQQD.KHTK & "'," & _
                            "ma_lh='" & objCQQD.MA_LH.Trim & "',shkb='" & objCQQD.SHKB.Trim & "',ma_cqqd='" & objCQQD.MA_CQQD & "',ma_dvsdns='" & objCQQD.MA_DVSDNS & "'," & _
                            "ten_cq='" & objCQQD.TEN_CQ.Trim & "',TINH_TRANG='" & objCQQD.TINH_TRANG.Trim & "'  WHERE ID='" & objCQQD.ID_HIS.Trim & "'"
                ElseIf objCQQD.TT_XU_LY = "2" Then
                    strSql = "UPDATE tcs_map_cqqd_lhthu_tmp SET NGUOI_DUYET='" & objCQQD.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objCQQD.ID
                End If
            End If
            Try
                cnKhoBac = New DataAccess
                cnKhoBac.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCQQD.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE tcs_map_cqqd_lhthu_tmp SET NGUOI_DUYET ='" & objCQQD.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objCQQD.ID
                    cnKhoBac.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi khi cập nhật cơ quan quyết định - loại hình thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật tài khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objCQQD As infCQQD_LHThu, ByVal Nhom As String) As Boolean
            Dim cnKhoBac As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO tcs_map_cqqd_lhthu_tmp (ID,ID_HIS,ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                              "VALUES (" & Common.mdlCommon.getDataKey("tcs_map_cqqd_lhthu_tmp_seq.NEXTVAL") & ",'" & objCQQD.ID_HIS & "'  " & _
                              ",'" & objCQQD.MA_QUY & "','" & objCQQD.TK_NS & "','" & objCQQD.MA_CHUONG & "','" & objCQQD.MA_NKT & "','" & objCQQD.MA_NDKT & "'," & _
                              "'" & objCQQD.MA_DBHC & "','" & objCQQD.KHTK & "','" & objCQQD.MA_LH & "','" & objCQQD.SHKB & "','" & objCQQD.MA_CQQD & "'," & _
                              "'" & objCQQD.MA_DVSDNS & "','" & objCQQD.TEN_CQ & "','" & objCQQD.NGUOI_TAO & "', SYSDATE,'0','" & objCQQD.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objCQQD.TT_XU_LY = "1" Then
                    strSql = "DELETE FROM tcs_map_cqqd_lhthu " & _
                               "WHERE ID='" & objCQQD.ID_HIS & "'"
                ElseIf objCQQD.TT_XU_LY = "2" Then
                    strSql = "UPDATE tcs_map_cqqd_lhthu_tmp SET NGUOI_DUYET='" & objCQQD.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objCQQD.ID
                End If
            End If
            Try
                cnKhoBac = New DataAccess
                cnKhoBac.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCQQD.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE tcs_map_cqqd_lhthu_tmp SET NGUOI_DUYET ='" & objCQQD.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objCQQD.ID
                    cnKhoBac.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi xóa dữ liệu map cơ quan quyết định - loại hình thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá tài khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function LoadDetail(ByVal strID As String) As infCQQD_LHThu
            Dim objCQQD As New infCQQD_LHThu
            Dim cnCQQD As DataAccess
            Dim drCQQD As IDataReader
            Dim strSQL As String = "Select ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq,tinh_trang From tcs_map_cqqd_lhthu " & _
                        "Where ID='" & strID & "'"
            Try
                cnCQQD = New DataAccess
                drCQQD = cnCQQD.ExecuteDataReader(strSQL, CommandType.Text)
                If drCQQD.Read Then
                    objCQQD = New infCQQD_LHThu
                    objCQQD.ID = strID
                    objCQQD.SHKB = drCQQD("ma_quy").ToString()
                    objCQQD.TK_NS = drCQQD("tk_ns").ToString()
                    objCQQD.MA_CHUONG = drCQQD("ma_chuong").ToString()
                    objCQQD.MA_NKT = drCQQD("ma_nkt").ToString()
                    objCQQD.MA_NDKT = drCQQD("ma_ndkt").ToString()
                    objCQQD.MA_DBHC = drCQQD("ma_dbhc").ToString()
                    objCQQD.KHTK = drCQQD("khtk").ToString()
                    objCQQD.MA_LH = drCQQD("ma_lh").ToString()
                    objCQQD.SHKB = drCQQD("shkb").ToString()
                    objCQQD.MA_CQQD = drCQQD("ma_cqqd").ToString()
                    objCQQD.MA_DVSDNS = drCQQD("ma_dvsdns").ToString()
                    objCQQD.TEN_CQ = drCQQD("ten_cq").ToString()
                    objCQQD.TINH_TRANG = drCQQD("TINH_TRANG").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin map cơ quan quyết định - loại hình thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drCQQD Is Nothing Then drCQQD.Close()
                If Not cnCQQD Is Nothing Then cnCQQD.Dispose()
            End Try
            Return objCQQD
        End Function

        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnKhoBac As DataAccess
            Dim dsKhoBac As DataSet

            Try
                strSQL = "SELECT ID,ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq, tinh_trang,DECODE (tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 1, 'Đang hoạt động', " & _
                         "                 0, 'Ngừng hoạt động' " & _
                         "                ) tinhtrang  from tcs_map_cqqd_lhthu" & _
                         "  WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY ID ASC "

                cnKhoBac = New DataAccess
                dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin cơ quan quyết định - loại hình thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try

            Return dsKhoBac
        End Function
        Public Function Load_new(ByVal WhereClause As String) As DataSet
            Dim strSQL As String
            Dim cnCapChuong As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "SELECT   b.ma_quy as MA_QUY_new,b.tk_ns as TK_NS_new,b.ma_chuong as MA_CHUONG_new,b.ma_nkt as MA_NKT_new,b.ma_ndkt as MA_NDKT_new,b.ma_dbhc as MA_DBHC_new, " & _
                         " b.khtk as KHTK_new,b.ma_lh as MA_LH_new, b.shkb  as SHKB_new,b.ma_cqqd as MA_CQQD_new, b.ma_dvsdns as MA_DVSDNS_new,b.ten_cq as TEN_CQ_new," & _
                         "         DECODE (a.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngừng hoạt động', " & _
                         "                 1, 'Đang hoạt động' " & _
                         "                ) tinh_trang,DECODE(b.TYPE,NULL,'',1,'Thêm',2,'Sửa',3,'Xóa') ThayDoi ,DECODE(b.TT_XU_LY,NULL,'',0,'Chờ duyệt',1,'Đã duyệt',2,'Từ chối') xu_ly ," & _
                         "  b.id,b.id_his,b.Type,b.TT_XU_LY, DECODE(b.tinh_trang,NULL,'',0,'Ngừng hoạt động',1,'Đang hoạt động') tinh_trang_new " & _
                         "    FROM tcs_map_cqqd_lhthu_tmp b ,tcs_map_cqqd_lhthu a " & _
                         "   WHERE  b.ID=a.ID(+) " & WhereClause & _
                         "ORDER BY b.ID DESC "
                cnCapChuong = New DataAccess
                dsCapChuong = cnCapChuong.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cơ quan quyết định - loại hình thu")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnCapChuong.Dispose()
            End Try

            Return dsCapChuong
        End Function

        Public Function CheckExist(ByVal objCQQD As infCQQD_LHThu) As Boolean
            Return LoadDetail(objCQQD.ID).ID <> ""
        End Function

        'Public Function CheckDelete(ByVal strSHKB As String) As Object
        '    Dim cnKhoBac As DataAccess
        '    Dim dsKhoBac As DataSet

        '    Try
        '        Dim strSql As String = "select shkb from (select shkb from tcs_ctu_hdr union " & _
        '                   "select shkb from tcs_baolanh_hdr) where shkb='" & strSHKB & "'"

        '        cnKhoBac = New DataAccess
        '        dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSql, CommandType.Text)
        '    Catch ex As Exception
        '        CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
        '        'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
        '        Throw ex
        '    Finally
        '        If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
        '    End Try

        '    Return dsKhoBac
        'End Function
    End Class
End Namespace
