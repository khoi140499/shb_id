Imports VBOracleLib

Namespace DanhMuc.LoaiHinh
    Public Class buLoaiHinh

        Private strStatus As String

        Sub New()
        End Sub

        Sub New(ByVal strTmpStatus As String)
            strStatus = strTmpStatus
        End Sub

        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property

        Public Function Insert(ByVal objLoaiHinh As infLoaiHinh) As Boolean
            Dim tmpLoaiHinh As New daLoaiHinh
            If tmpLoaiHinh.CheckExist(objLoaiHinh) = False Then
                Return tmpLoaiHinh.Insert(objLoaiHinh)
            Else
                MsgBox("Mã loại hình đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End If
        End Function

        Public Function Update(ByVal objLoaiHinh As infLoaiHinh) As Boolean
            Dim tmpLoaiHinh As New daLoaiHinh
            'Try
            '    Dim objExist As infLoaiHinh = tmpLoaiHinh.Load(objLoaiHinh)
            '    If objExist.MA_LH = "" Then
            Return tmpLoaiHinh.Update(objLoaiHinh)

            '    Else
            'Dim strMsg As String = "Bạn đã sửa mã loại hình trùng với mã loại hình khác đã tồn tại trong hệ thống:" & Chr(13) & _
            '"Tên loại hình: " & objExist.TEN_LH & Chr(13) & _
            '"Tên viết tắt: " & objExist.TEN_VT & Chr(13) & _
            '"Ngày an han: " & objExist.NGAY_ANHAN & Chr(13) & _
            'MsgBox(strMsg, MsgBoxStyle.Critical, "Chú ý")
            'Return False
            '    End If
            'Catch ex As Exception
            '    Return False
            'End Try
        End Function

        Public Function Delete(ByVal strTmpMaLoaiHinh As String) As Boolean
            Dim objLoaiHinh As New infLoaiHinh
            objLoaiHinh.MA_LH = strTmpMaLoaiHinh
            Dim tmpLoaiHinh As New daLoaiHinh
            Return tmpLoaiHinh.Delete(objLoaiHinh)
        End Function

        Public Function GetDataSet(ByVal WhereClause As String) As DataSet
            Dim objLoaiHinh As New daLoaiHinh
            Dim dsLoaiHinh As New DataSet
            Try
                dsLoaiHinh = objLoaiHinh.Load(WhereClause, "")
            Catch ex As Exception
            Finally
            End Try

            Return dsLoaiHinh
        End Function

        'Public Function GetReport() As String
        '    Return Common.mdlCommon.Report_Type.DM_CAPCHUONG
        'End Function

        'Public Function CheckDelete(ByVal strTmpMaCapChuong As String) As Boolean
        '    Dim ds As DataSet
        '    Dim tmpCapChuong As New daCapChuong
        '    ds = tmpCapChuong.CheckDelete(strTmpMaCapChuong)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        Return True
        '    Else
        '        Return False
        '    End If
        'End Function
    End Class
End Namespace
