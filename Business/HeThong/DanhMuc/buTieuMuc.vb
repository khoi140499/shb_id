﻿Imports VBOracleLib
Namespace DanhMuc.TieuMuc
    Public Class buTieuMuc
        Private strStatus As String

        Sub New()
        End Sub

        Sub New(ByVal strTmpStatus As String)
            strStatus = strTmpStatus
        End Sub

        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property

        Public Function Insert(ByVal objTieuMuc As infTieuMuc, ByVal Nhom As String) As Boolean
            Dim tmpTieuMuc As New daTieuMuc
            If tmpTieuMuc.CheckExist(objTieuMuc) = False Then
                Return tmpTieuMuc.Insert(objTieuMuc, Nhom)
            Else

                Return False
            End If
        End Function

        Public Function Update(ByVal objTieuMuc As infTieuMuc, ByVal Nhom As String) As Boolean
            Dim tmpTieuMuc As New daTieuMuc
            Try
                Dim objExist As infTieuMuc = tmpTieuMuc.Load(objTieuMuc)
                If objExist.MTM_ID <> "" Then
                    Return tmpTieuMuc.Update(objTieuMuc, Nhom)
                Else
                    Dim strMsg As String = "Bạn đã sửa ngành kinh tế trùng với ngành kinh tế khác đã tồn tại trong hệ thống:" & Chr(13) & _
                    "Mã ngành: " & objExist.MA_TMUC & Chr(13) & _
                    "Tên ngành: " & objExist.Ten & Chr(13) & _
                    "Tinh trạng: " & IIf(objExist.TINH_TRANG = "0", "Ngung hoat dong", "Dang hoat dong")
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function

        'Public Function Save(ByVal objTieuMuc As infTieuMuc) As Boolean
        '    If strStatus.Equals("EDIT") Then
        '        Return Me.Update(objTieuMuc)
        '    Else
        '        Return Me.Insert(objTieuMuc)
        '    End If
        'End Function

        Public Function Delete(ByVal objTieuMuc As infTieuMuc, ByVal Nhom As String) As Boolean
            Dim tmpTieuMuc As New daTieuMuc
            Return tmpTieuMuc.Delete(objTieuMuc, Nhom)
        End Function

        'Public Sub FormatFlexHeader(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    flxDanhMuc.Cols.Count = 5
        '    flxDanhMuc.Cols(1).Width = 60
        '    flxDanhMuc.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        '    flxDanhMuc.Cols(2).Width = 220
        '    flxDanhMuc.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
        '    flxDanhMuc.Cols(3).Width = 200
        '    flxDanhMuc.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
        '    flxDanhMuc.Cols(4).Width = 79
        '    flxDanhMuc.Cols(4).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter

        '    flxDanhMuc.Rows(0).Item(1) = "Mã ngành"
        '    flxDanhMuc.Rows(0).Item(2) = "Tên ngành"
        '    flxDanhMuc.Rows(0).Item(3) = "Ghi chú"
        '    flxDanhMuc.Rows(0).Item(4) = "Tình trạng"

        '    flxDanhMuc.Cols(0).Visible = False
        'End Sub

        'Public Sub FormatFrm(ByRef frm As Windows.Forms.Form)
        'End Sub

        Public Function Load(ByVal WhereClause As String) As DataSet
            Dim objTieuMuc As New daTieuMuc
            Try
                Dim dsLoaiKhoan As DataSet = objTieuMuc.Load(WhereClause, "")
                Return dsLoaiKhoan
            Catch ex As Exception
            Finally
            End Try

        End Function
        Public Function GetDataSet_new(ByVal WhereClause As String) As DataSet
            Dim objTieuMuc As New daTieuMuc
            Dim dsCapChuong As DataSet
            Try
                dsCapChuong = objTieuMuc.Load_new(WhereClause, "")
            Catch ex As Exception
            Finally
            End Try
            Return dsCapChuong
        End Function

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    Dim objTieuMuc As New daTieuMuc
        '    Try
        '        Dim tmpTieuMuc As infTieuMuc = objTieuMuc.Load(flxDanhMuc.Item(flxDanhMuc.Row, 0))
        '        flxDanhMuc(flxDanhMuc.Row, 0) = tmpTieuMuc.MTM_ID
        '        flxDanhMuc(flxDanhMuc.Row, 1) = tmpTieuMuc.MA_TMUC
        '        flxDanhMuc(flxDanhMuc.Row, 2) = tmpTieuMuc.Ten
        '        flxDanhMuc(flxDanhMuc.Row, 3) = tmpTieuMuc.GHICHU
        '        flxDanhMuc(flxDanhMuc.Row, 4) = IIf(tmpTieuMuc.TINH_TRANG = "0", "Ngung hoat dong", "Dang hoat dong")
        '        If flxDanhMuc.Cols(flxDanhMuc.Col).Sort <> C1.Win.C1FlexGrid.SortFlags.None Then flxDanhMuc.Sort(flxDanhMuc.Cols(flxDanhMuc.Col).Sort, flxDanhMuc.Col)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid, ByVal objTieuMuc As infTieuMuc)
        '    Try
        '        flxDanhMuc.Rows.Add()
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 0) = objTieuMuc.MTM_ID
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 1) = objTieuMuc.MA_TMUC
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 2) = objTieuMuc.Ten
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 3) = objTieuMuc.GHICHU
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 4) = IIf(objTieuMuc.TINH_TRANG = "0", "Ngung hoat dong", "Dang hoat dong")
        '        flxDanhMuc.Select(flxDanhMuc.Rows.Count - 1, flxDanhMuc.Col)
        '        If flxDanhMuc.Cols(flxDanhMuc.Col).Sort <> C1.Win.C1FlexGrid.SortFlags.None Then flxDanhMuc.Sort(flxDanhMuc.Cols(flxDanhMuc.Col).Sort, flxDanhMuc.Col)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Function Add() As Object
        '    Dim frmAdd As New MLNS.frmMLNS("LOAIKHOAN")
        '    frmAdd.ShowDialog()
        '    If frmAdd.DialogResult = DialogResult.OK Then
        '        Dim objTieuMuc As New infTieuMuc
        '        Dim tmpTieuMuc As New daTieuMuc
        '        objTieuMuc.MA_TMUC = frmAdd.txtChuong.Text.Trim
        '        objTieuMuc.TINH_TRANG = (1 - frmAdd.cboTinh_Trang.SelectedIndex).ToString
        '        objTieuMuc = tmpTieuMuc.Load(objTieuMuc)
        '        If objTieuMuc.MTM_ID <> "" Then
        '            Return objTieuMuc
        '        Else
        '            Return Nothing
        '        End If
        '    Else
        '        Return Nothing
        '    End If
        'End Function

        'Public Function Edit(ByVal strID As String) As Boolean
        '    Dim frmAdd As New MLNS.frmMLNS("LOAIKHOAN", strID)
        '    frmAdd.ShowDialog()
        '    Return frmAdd.DialogResult = DialogResult.OK
        'End Function

        Public Function GetReport() As String
            Return Common.mdlCommon.Report_Type.DM_LOAIKHOAN
        End Function

        Public Function CheckDelete(ByVal strTmpTieuMuc As String) As Boolean
            Dim ds As DataSet
            Dim tmpTieuMuc As New daTieuMuc
            ds = tmpTieuMuc.CheckDelete(strTmpTieuMuc)
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class
End Namespace
