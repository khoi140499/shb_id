﻿Imports VBOracleLib
Namespace DanhMuc
    Public Class buTCS_MAP_CQQD_LHTHU
        Public Function ADD_DM_CQQD(ByVal pv_StrMA_CQQD As String, ByVal pv_StrTEN_CQQD As String, ByVal pv_StrMA_CQTHU As String, ByVal pv_StrGHI_CHU As String, _
                                    ByVal pv_StrMA_KB As String, ByVal pv_StrMAKER As String, ByVal pv_StrMA_DBHC As String) As String
            Try
                'kiem tra trung
                Dim strSelect As String = "SELECT * FROM TCS_DM_CQQD WHERE UPPER(MA_CQQD)='" & pv_StrMA_CQQD.ToUpper() & "' AND MA_KB='" & pv_StrMA_KB & "'  AND TRANGTHAI <>0 "
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return "Trùng mã cơ quan thu vui lòng kiểm tra lại!"
                        End If
                    End If
                End If
                'insert
                strSelect = "INSERT INTO TCS_DM_CQQD( MA_CQQD, TEN_CQQD,   GHI_CHU, MA_CQTHU,"
                strSelect &= "  MA_KB, MDATE, MAKER,MA_DBHC)VALUES("
                strSelect &= " :MA_CQQD, :TEN_CQQD,   :GHI_CHU, :MA_CQTHU,"
                strSelect &= " :MA_KB, :SYSDATE, :MAKER,:MA_DBHC)"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("MA_CQQD", ParameterDirection.Input, pv_StrMA_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("TEN_CQQD", ParameterDirection.Input, pv_StrTEN_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("GHI_CHU", ParameterDirection.Input, pv_StrGHI_CHU, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_CQTHU", ParameterDirection.Input, pv_StrMA_CQTHU, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_KB", ParameterDirection.Input, pv_StrMA_KB, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MAKER", ParameterDirection.Input, pv_StrMAKER, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_DBHC", ParameterDirection.Input, pv_StrMA_DBHC, DbType.String))

                DataAccess.ExecuteNonQuery(strSelect, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
        Public Function Search_DM_TCS_MAP_CQQD_LHTHU(ByVal pv_StrMA_LH As String, ByVal pv_StrSHKB As String, ByVal pv_StrTKNS As String, ByVal pv_StrMA_TM As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.ID, A.MA_QUY, A.TK_NS, A.MA_CHUONG, A.MA_NKT, A.MA_NDKT,"
                strSelect &= "   A.MA_DBHC, A.KHTK, A.MA_LH, A.SHKB, A.MA_CQQD, A.MA_DVSDNS,"
                strSelect &= "   A.TEN_CQ, A.TINH_TRANG"
                strSelect &= " FROM TCS_MAP_CQQD_LHTHU A WHERE 1=1 "



                If pv_StrMA_LH.Length > 0 Then
                    strSelect &= " AND UPPER(MA_LH) LIKE '%" & pv_StrMA_LH.ToUpper() & "%' "
                End If

                If pv_StrSHKB.Length > 0 Then
                    strSelect &= " AND UPPER(SHKB) LIKE '%" & pv_StrSHKB.ToUpper() & "%' "
                End If
                If pv_StrTKNS.Length > 0 Then
                    strSelect &= " AND UPPER(TK_NS) LIKE '%" & pv_StrTKNS.ToUpper() & "%' "
                End If
                If pv_StrMA_TM.Length > 0 Then
                    strSelect &= " AND UPPER(MA_NDKT) LIKE '%" & pv_StrMA_TM.ToUpper() & "%' "
                End If
                strSelect &= "  ORDER BY A.SHKB,A.MA_LH,A.MA_DVSDNS"

                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function
        Public Function Search_DM_TCS_MAP_CQQD_LHTHU(ByVal pv_StrWhere As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.ID, A.MA_QUY, A.TK_NS, A.MA_CHUONG, A.MA_NKT, A.MA_NDKT,"
                strSelect &= "   A.MA_DBHC, A.KHTK, A.MA_LH, A.SHKB, A.MA_CQQD, A.MA_DVSDNS,"
                strSelect &= "   A.TEN_CQ, "
                strSelect &= "         DECODE (a.tinh_trang, "
                strSelect &= "                 NULL, '', "
                strSelect &= "                 0, 'Ngừng hoạt động', "
                strSelect &= "                 1, 'Đang hoạt động') tinh_trang  "
                strSelect &= " FROM TCS_MAP_CQQD_LHTHU A WHERE 1=1 " & pv_StrWhere
                strSelect &= "  ORDER BY A.SHKB,A.MA_LH,A.MA_DVSDNS"

                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function

        Public Function GetInfoDetail(ByVal pv_StrWhere As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.ID, A.MA_QUY, A.TK_NS, A.MA_CHUONG, A.MA_NKT, A.MA_NDKT,"
                strSelect &= "   A.MA_DBHC, A.KHTK, A.MA_LH, A.SHKB, A.MA_CQQD, A.MA_DVSDNS,"
                strSelect &= "   A.TEN_CQ, "
                strSelect &= "         DECODE (a.tinh_trang, "
                strSelect &= "                 NULL, '', "
                strSelect &= "                 0, 'Ngừng hoạt động', "
                strSelect &= "                 1, 'Đang hoạt động') tinh_trang "
                strSelect &= " FROM TCS_MAP_CQQD_LHTHU A WHERE 1=1 " & pv_StrWhere

                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function

        Public Function UPDATE_DM_CQQD_LHTHU(ByVal pv_StrMA_CQQD As String, ByVal pv_StrMA_KB As String, ByVal pv_StrMA_LH As String, ByVal pv_StrMaDBHC As String, _
                                   ByVal pv_StrMA_CQTHU As String, ByVal pv_StrTKNS As String, ByVal pv_StrChuong As String, ByVal strMa_NDKT As String, ByVal strMaQuy As String, ByVal ID As String) As String
            Try
                'kiem tra trung
                Dim strSelect As String = "SELECT * FROM TCS_MAP_CQQD_LHTHU WHERE UPPER(MA_CQQD)='" & pv_StrMA_CQQD.ToUpper() & "' AND shkb='" & pv_StrMA_KB & "' AND ma_lh='" & pv_StrMA_LH & "'  AND ID <> '" & ID & "' "
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return "Trùng cặp mã cơ quan quyết định, kho bạc, mã loại hình vui lòng kiểm tra lại!"
                        End If
                    End If
                End If
                'insert
                strSelect = "UPDATE TCS_MAP_CQQD_LHTHU SET "
                strSelect &= "  ma_quy =: ma_quy,"
                strSelect &= "  tk_ns =: tk_ns,"
                strSelect &= "  ma_chuong =: ma_chuong,"
                strSelect &= "  ma_ndkt =: ma_ndkt,"
                strSelect &= "  ma_lh =: ma_lh,"
                strSelect &= " shkb =: shkb,"
                strSelect &= " ma_dbhc =: ma_dbhc,"
                strSelect &= " ma_cqqd =: ma_cqqd,"
                strSelect &= " ma_dvsdns =: ma_dvsdns "
                strSelect &= " WHERE ID =: ID"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("ma_quy", ParameterDirection.Input, strMaQuy, DbType.String))
                ls.Add(DataAccess.NewDBParameter("tk_ns", ParameterDirection.Input, pv_StrTKNS, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_chuong", ParameterDirection.Input, pv_StrChuong, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_ndkt", ParameterDirection.Input, strMa_NDKT, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_lh", ParameterDirection.Input, pv_StrMA_LH, DbType.String))
                ls.Add(DataAccess.NewDBParameter("shkb", ParameterDirection.Input, pv_StrMA_KB, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_dbhc", ParameterDirection.Input, pv_StrMaDBHC, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_cqqd", ParameterDirection.Input, pv_StrMA_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_dvsdns", ParameterDirection.Input, pv_StrMA_CQTHU, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, ID, DbType.String))

                DataAccess.ExecuteNonQuery(strSelect, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Function INSERT_DM_CQQD_LHTHU(ByVal pv_StrMA_CQQD As String, ByVal pv_StrMA_KB As String, ByVal pv_StrMA_LH As String, ByVal pv_StrMaDBHC As String, _
                                   ByVal pv_StrMA_CQTHU As String, ByVal pv_StrTKNS As String, ByVal pv_StrChuong As String, ByVal strMa_NDKT As String, ByVal strMaQuy As String) As String
            Try
                'kiem tra trung
                Dim strSelect As String = "SELECT * FROM TCS_MAP_CQQD_LHTHU WHERE UPPER(MA_CQQD)='" & pv_StrMA_CQQD.ToUpper() & "' AND shkb='" & pv_StrMA_KB & "' AND ma_lh='" & pv_StrMA_LH & "' "
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return "Trùng cặp mã cơ quan quyết định, kho bạc, mã loại hình vui lòng kiểm tra lại!"
                        End If
                    End If
                End If
                'insert
                strSelect = "INSERT INTO TCS_MAP_CQQD_LHTHU (ID,ma_quy,tk_ns,ma_chuong,ma_ndkt,ma_lh,shkb,ma_dbhc,ma_cqqd,ma_dvsdns,tinh_trang) "
                strSelect &= "  VALUES (tcs_map_cqqd_lhthu_seq.nextval,:ma_quy,:tk_ns,:ma_chuong,:ma_ndkt,:ma_lh,:shkb,:ma_dbhc,:ma_cqqd,:ma_dvsdns,'1')"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("ma_quy", ParameterDirection.Input, strMaQuy, DbType.String))
                ls.Add(DataAccess.NewDBParameter("tk_ns", ParameterDirection.Input, pv_StrTKNS, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_chuong", ParameterDirection.Input, pv_StrChuong, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_ndkt", ParameterDirection.Input, strMa_NDKT, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_lh", ParameterDirection.Input, pv_StrMA_LH, DbType.String))
                ls.Add(DataAccess.NewDBParameter("shkb", ParameterDirection.Input, pv_StrMA_KB, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_dbhc", ParameterDirection.Input, pv_StrMaDBHC, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_cqqd", ParameterDirection.Input, pv_StrMA_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_dvsdns", ParameterDirection.Input, pv_StrMA_CQTHU, DbType.String))

                DataAccess.ExecuteNonQuery(strSelect, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
        Public Function DELETE_DM_CQQD_LHTHU(ByVal ID As String) As String
            Try
                Dim strDelete As String = "DELETE FROM TCS_MAP_CQQD_LHTHU WHERE ID =: ID"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, ID, DbType.String))
                DataAccess.ExecuteNonQuery(strDelete, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
    End Class
End Namespace
