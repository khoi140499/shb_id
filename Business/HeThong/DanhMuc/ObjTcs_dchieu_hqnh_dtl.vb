﻿Public Class ObjTcs_dchieu_hqnh_dtl
    Private _hdr_id As String
    Private _ma_quy As String
    Private _ma_cap As String
    Private _ma_chuong As String
    Private _ma_nkt As String
    Private _ma_nkt_cha As String
    Private _ma_ndkt As String
    Private _ma_ndkt_cha As String
    Private _ma_tlpc As String
    Private _ma_khtk As String
    Private _noi_dung As String
    Private _ma_dp As String
    Private _ky_thue As String
    Private _sotien As String

    Public Property hdr_id() As String
        Get
            Return _hdr_id
        End Get
        Set(ByVal Value As String)
            _hdr_id = Value
        End Set
    End Property
    Public Property ma_quy() As String
        Get
            Return _ma_quy
        End Get
        Set(ByVal Value As String)
            _ma_quy = Value
        End Set
    End Property
    Public Property ma_cap() As String
        Get
            Return _ma_cap
        End Get
        Set(ByVal Value As String)
            _ma_cap = Value
        End Set
    End Property
    Public Property ma_chuong() As String
        Get
            Return _ma_chuong
        End Get
        Set(ByVal Value As String)
            _ma_chuong = Value
        End Set
    End Property
    Public Property ma_nkt() As String
        Get
            Return _ma_nkt
        End Get
        Set(ByVal Value As String)
            _ma_nkt = Value
        End Set
    End Property
    Public Property ma_nkt_cha() As String
        Get
            Return _ma_nkt_cha
        End Get
        Set(ByVal Value As String)
            _ma_nkt_cha = Value
        End Set
    End Property
    Public Property ma_ndkt() As String
        Get
            Return _ma_ndkt
        End Get
        Set(ByVal Value As String)
            _ma_ndkt = Value
        End Set
    End Property
    Public Property ma_ndkt_cha() As String
        Get
            Return _ma_ndkt_cha
        End Get
        Set(ByVal Value As String)
            _ma_ndkt_cha = Value
        End Set
    End Property
    Public Property ma_tlpc() As String
        Get
            Return _ma_tlpc
        End Get
        Set(ByVal Value As String)
            _ma_tlpc = Value
        End Set
    End Property
    Public Property ma_khtk() As String
        Get
            Return _ma_khtk
        End Get
        Set(ByVal Value As String)
            _ma_khtk = Value
        End Set
    End Property
    Public Property noi_dung() As String
        Get
            Return _noi_dung
        End Get
        Set(ByVal Value As String)
            _noi_dung = Value
        End Set
    End Property
    Public Property ma_dp() As String
        Get
            Return _ma_dp
        End Get
        Set(ByVal Value As String)
            _ma_dp = Value
        End Set
    End Property
    Public Property ky_thue() As String
        Get
            Return _ky_thue
        End Get
        Set(ByVal Value As String)
            _ky_thue = Value
        End Set
    End Property
    Public Property sotien() As String
        Get
            Return _sotien
        End Get
        Set(ByVal Value As String)
            _sotien = Value
        End Set
    End Property
End Class
