Namespace DanhMuc.LoaiKhoan
    Public Class infLoaiKhoan
        Private strLKH_ID As String
        Private strMA_KHOAN As String
        Private strTen As String
        Private strGHICHU As String
        Private strTINH_TRANG As String

        Sub New()
            strLKH_ID = ""
            strTen = ""
            strGHICHU = ""
            strTINH_TRANG = "1"
        End Sub

        Sub New(ByVal drwLoaiKhoan As DataRow)
            strLKH_ID = drwLoaiKhoan("LKH_ID").ToString().Trim()
            strMA_KHOAN = drwLoaiKhoan("MA_KHOAN").ToString().Trim()
            strTen = drwLoaiKhoan("Ten").ToString().Trim()
            strTINH_TRANG = drwLoaiKhoan("TINH_TRANG").ToString().Trim()
            strGHICHU = drwLoaiKhoan("GHI_CHU").ToString().Trim()
        End Sub

        Public Property LKH_ID() As String
            Get
                Return strLKH_ID
            End Get
            Set(ByVal Value As String)
                strLKH_ID = Value
            End Set
        End Property

        Public Property MA_KHOAN() As String
            Get
                Return strMA_KHOAN
            End Get
            Set(ByVal Value As String)
                strMA_KHOAN = Value
            End Set
        End Property

        Public Property Ten() As String
            Get
                Return strTen
            End Get
            Set(ByVal Value As String)
                strTen = Value
            End Set
        End Property

        Public Property GHICHU() As String
            Get
                Return strGHICHU
            End Get
            Set(ByVal Value As String)
                strGHICHU = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTINH_TRANG
            End Get
            Set(ByVal Value As String)
                strTINH_TRANG = Value
            End Set
        End Property

    End Class
End Namespace
