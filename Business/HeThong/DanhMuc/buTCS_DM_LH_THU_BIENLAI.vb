﻿Imports VBOracleLib
Namespace DanhMuc
    Public Class buTCS_DM_LH_THU_BIENLAI
        Public Function ADD_DM_CQQD(ByVal pv_StrMA_CQQD As String, ByVal pv_StrTEN_CQQD As String, ByVal pv_StrMA_CQTHU As String, ByVal pv_StrGHI_CHU As String, _
                                    ByVal pv_StrMA_KB As String, ByVal pv_StrMAKER As String, ByVal pv_StrMA_DBHC As String) As String
            Try
                'kiem tra trung
                Dim strSelect As String = "SELECT * FROM TCS_DM_CQQD WHERE UPPER(MA_CQQD)='" & pv_StrMA_CQQD.ToUpper() & "' AND MA_KB='" & pv_StrMA_KB & "'  AND TRANGTHAI <>0 "
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Return "Trùng mã cơ quan thu vui lòng kiểm tra lại!"
                        End If
                    End If
                End If
                'insert
                strSelect = "INSERT INTO TCS_DM_CQQD( MA_CQQD, TEN_CQQD,   GHI_CHU, MA_CQTHU,"
                strSelect &= "  MA_KB, MDATE, MAKER,MA_DBHC)VALUES("
                strSelect &= " :MA_CQQD, :TEN_CQQD,   :GHI_CHU, :MA_CQTHU,"
                strSelect &= " :MA_KB, :SYSDATE, :MAKER,:MA_DBHC)"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("MA_CQQD", ParameterDirection.Input, pv_StrMA_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("TEN_CQQD", ParameterDirection.Input, pv_StrTEN_CQQD, DbType.String))
                ls.Add(DataAccess.NewDBParameter("GHI_CHU", ParameterDirection.Input, pv_StrGHI_CHU, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_CQTHU", ParameterDirection.Input, pv_StrMA_CQTHU, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_KB", ParameterDirection.Input, pv_StrMA_KB, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MAKER", ParameterDirection.Input, pv_StrMAKER, DbType.String))
                ls.Add(DataAccess.NewDBParameter("MA_DBHC", ParameterDirection.Input, pv_StrMA_DBHC, DbType.String))

                DataAccess.ExecuteNonQuery(strSelect, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
        Public Function Search_DM_LOAIHINH_THU_BIENLAI(ByVal pv_StrMA_LH As String, ByVal pv_StrTEN_LH As String, ByVal pv_StrSHKB As String, ByVal pv_StrTKNS As String, ByVal pv_StrMA_TM As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.MA_LH, A.TEN_LH, A.SHKB, A.MA_CHUONG, A.MA_TMUC, A.MA_TKNS,"
                strSelect &= "    A.GHI_CHU, TO_CHAR( A.MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, A.MAKER"
                strSelect &= " FROM TCS_DM_LH_THU_BIENLAI A  WHERE 1=1"

                If pv_StrMA_LH.Length > 0 Then
                    strSelect &= " AND UPPER(MA_LH) LIKE '%" & pv_StrMA_LH.ToUpper() & "%' "
                End If
                If pv_StrTEN_LH.Length > 0 Then
                    strSelect &= " AND UPPER(TEN_LH) LIKE '%" & pv_StrTEN_LH.ToUpper() & "%' "
                End If
                If pv_StrSHKB.Length > 0 Then
                    strSelect &= " AND UPPER(SHKB) LIKE '%" & pv_StrSHKB.ToUpper() & "%' "
                End If
                If pv_StrTKNS.Length > 0 Then
                    strSelect &= " AND UPPER(MA_TKNS) LIKE '%" & pv_StrTKNS.ToUpper() & "%' "
                End If
                If pv_StrMA_TM.Length > 0 Then
                    strSelect &= " AND UPPER(MA_TMUC) LIKE '%" & pv_StrMA_TM.ToUpper() & "%' "
                End If

                strSelect &= " ORDER BY A.SHKB, A.MA_LH "
                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function
        Public Function Load_DM_LOAIHINH_THU_BIENLAI(ByVal pv_StrWhere As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.MA_LH, A.TEN_LH, A.SHKB, A.MA_CHUONG, A.MA_TMUC, A.MA_TKNS,"
                strSelect &= "    A.GHI_CHU, TO_CHAR( A.MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, A.MAKER, A.ID "
                strSelect &= " FROM TCS_DM_LH_THU_BIENLAI A  WHERE 1=1 " & pv_StrWhere
                strSelect &= " ORDER BY A.SHKB, A.MA_LH "
                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function
        Public Function GetInfoDetail(ByVal pv_StrWhere As String) As DataSet
            Try
                Dim strSelect As String = "SELECT A.MA_LH, A.TEN_LH, A.SHKB, A.MA_CHUONG, A.MA_TMUC, A.MA_TKNS,"
                strSelect &= "    A.GHI_CHU, TO_CHAR( A.MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, A.MAKER, A.ID "
                strSelect &= " FROM TCS_DM_LH_THU_BIENLAI A  WHERE 1=1 " & pv_StrWhere
                Return DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function
        Public Function UPDATE_DM_LHTHU(ByVal pv_StrMA_LH As String, ByVal pv_StrTen_LH As String, ByVal pv_StrMA_KB As String, ByVal pv_StrTK_NSNN As String, _
                                   ByVal pv_StrMA_CHUONG As String, ByVal pv_StrMA_TMUC As String, ByVal pv_StrGhiChu As String, ByVal pv_StrMaker As String, ByVal ID As String) As String
            Try
                'ma_lh,ten_lh,shkb,ma_chuong,ma_tmuc,ma_tkns,ghi_chu,mdate,maker
                Dim strUpdate As String = ""
                strUpdate = "UPDATE TCS_DM_LH_THU_BIENLAI SET "
                strUpdate &= "  ma_lh =: ma_lh,"
                strUpdate &= "  ten_lh =: ten_lh,"
                strUpdate &= "  shkb =: shkb,"
                strUpdate &= "  ma_chuong =: ma_chuong,"
                strUpdate &= "  ma_tmuc =: ma_tmuc,"
                strUpdate &= " ma_tkns =: ma_tkns,"
                strUpdate &= " ghi_chu =: ghi_chu,"
                strUpdate &= " mdate = sysdate, "
                strUpdate &= " maker =: maker "
                strUpdate &= " WHERE ID =: ID"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("ma_lh", ParameterDirection.Input, pv_StrMA_LH, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ten_lh", ParameterDirection.Input, pv_StrTen_LH, DbType.String))
                ls.Add(DataAccess.NewDBParameter("shkb", ParameterDirection.Input, pv_StrMA_KB, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_chuong", ParameterDirection.Input, pv_StrMA_CHUONG, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_tmuc", ParameterDirection.Input, pv_StrMA_TMUC, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_tkns", ParameterDirection.Input, pv_StrTK_NSNN, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ghi_chu", ParameterDirection.Input, pv_StrGhiChu, DbType.String))
                ls.Add(DataAccess.NewDBParameter("maker", ParameterDirection.Input, pv_StrMaker, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, ID, DbType.String))

                DataAccess.ExecuteNonQuery(strUpdate, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Function INSERT_DM_LHTHU(ByVal pv_StrMA_LH As String, ByVal pv_StrTen_LH As String, ByVal pv_StrMA_KB As String, ByVal pv_StrTK_NSNN As String, _
                                   ByVal pv_StrMA_CHUONG As String, ByVal pv_StrMA_TMUC As String, ByVal pv_StrGhiChu As String, ByVal pv_StrMaker As String) As String
            Try
                'ma_lh,ten_lh,shkb,ma_chuong,ma_tmuc,ma_tkns,ghi_chu,mdate,maker
                Dim strInsert As String = ""
                'insert
                strInsert = "INSERT INTO TCS_DM_LH_THU_BIENLAI (ID,ma_lh,ten_lh,shkb,ma_chuong,ma_tmuc,ma_tkns,ghi_chu,mdate,maker) "
                strInsert &= "  VALUES (tcs_lhthu_bienlai_seq.nextval,:ma_lh,:ten_lh,:shkb,:ma_chuong,:ma_tmuc,:ma_tkns,:ghi_chu,sysdate,:maker)"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("ma_lh", ParameterDirection.Input, pv_StrMA_LH, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ten_lh", ParameterDirection.Input, pv_StrTen_LH, DbType.String))
                ls.Add(DataAccess.NewDBParameter("shkb", ParameterDirection.Input, pv_StrMA_KB, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_chuong", ParameterDirection.Input, pv_StrMA_CHUONG, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_tmuc", ParameterDirection.Input, pv_StrMA_TMUC, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ma_tkns", ParameterDirection.Input, pv_StrTK_NSNN, DbType.String))
                ls.Add(DataAccess.NewDBParameter("ghi_chu", ParameterDirection.Input, pv_StrGhiChu, DbType.String))
                ls.Add(DataAccess.NewDBParameter("maker", ParameterDirection.Input, pv_StrMaker, DbType.String))

                DataAccess.ExecuteNonQuery(strInsert, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
        Public Function DELETE_DM_LHTHU(ByVal pv_StrID As String) As String
            Try
                Dim strDel As String = "DELETE FROM TCS_DM_LH_THU_BIENLAI WHERE ID =: ID"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, pv_StrID, DbType.String))
                DataAccess.ExecuteNonQuery(strDel, CommandType.Text, ls.ToArray())
                Return "0"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
    End Class
End Namespace
