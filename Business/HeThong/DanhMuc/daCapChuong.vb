Imports VBOracleLib
Imports Common.mdlCommon

Namespace DanhMuc.CapChuong
    Public Class daCapChuong
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Function Insert(ByVal objCapChuong As infCapChuong, ByVal Nhom As String) As Boolean
            Dim cnCapChuong As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_CAP_CHUONG (ID,CCH_ID, MA_CHUONG, TEN, GHI_CHU, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_CAP_CHUONG.NEXTVAL") & "," & Common.mdlCommon.getDataKey("TCS_CCH_ID_SEQ.NEXTVAL") & ", " & _
                                "'" & objCapChuong.MA_CHUONG & "','" & objCapChuong.Ten & "'," & _
                                "'" & objCapChuong.GHICHU & "','" & objCapChuong.TINH_TRANG & "','" & objCapChuong.NGUOI_TAO & "', SYSDATE,'0','" & objCapChuong.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objCapChuong.TT_XU_LY = "1" Then
                    'nếu duyệt
                    strSql = "INSERT INTO TCS_DM_CAP_CHUONG (CCH_ID, MA_CHUONG, TEN, GHI_CHU, TINH_TRANG) " & _
                                " SELECT HIS.CCH_ID,HIS.MA_CHUONG,HIS.TEN,HIS.GHI_CHU,HIS.TINH_TRANG " & _
                                " FROM HIS_DM_CAP_CHUONG HIS WHERE HIS.ID=" & objCapChuong.ID
                ElseIf objCapChuong.TT_XU_LY = "2" Then
                    'nếu từ chối
                    strSql = "UPDATE HIS_DM_CAP_CHUONG SET NGUOI_DUYET='" & objCapChuong.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objCapChuong.ID
                End If
            End If

            Try
                cnCapChuong = New DataAccess
                cnCapChuong.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCapChuong.TT_XU_LY = "1" Then
                    'Khi đã duyệt thêm mới 1 bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_CAP_CHUONG SET NGUOI_DUYET ='" & objCapChuong.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objCapChuong.ID
                    cnCapChuong.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới cấp - chương: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objCapChuong As infCapChuong, ByVal Nhom As String) As Boolean
            Dim cnCapChuong As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_CAP_CHUONG (ID,CCH_ID, MA_CHUONG, TEN, GHI_CHU, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_CAP_CHUONG.NEXTVAL") & "," & objCapChuong.CCH_ID.Trim & ", " & _
                                "'" & objCapChuong.MA_CHUONG & "','" & objCapChuong.Ten & "'," & _
                                "'" & objCapChuong.GHICHU & "','" & objCapChuong.TINH_TRANG & "','" & objCapChuong.NGUOI_TAO & "', SYSDATE,'0','" & objCapChuong.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objCapChuong.TT_XU_LY = "1" Then
                    strSql = "UPDATE TCS_DM_CAP_CHUONG SET TEN='" & objCapChuong.Ten.Trim & "',GHI_CHU='" & objCapChuong.GHICHU.Trim & "', TINH_TRANG='" & objCapChuong.TINH_TRANG.Trim & "'  WHERE CCH_ID=" & objCapChuong.CCH_ID.Trim
                ElseIf objCapChuong.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_CAP_CHUONG SET NGUOI_DUYET='" & objCapChuong.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objCapChuong.ID
                End If
            End If
            Try
                cnCapChuong = New DataAccess
                cnCapChuong.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCapChuong.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_CAP_CHUONG SET NGUOI_DUYET ='" & objCapChuong.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objCapChuong.ID
                    cnCapChuong.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật cấp - chương: " & ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật cấp - chương")
                blnResult = False
                Throw ex
            Finally
                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
            End Try
                Return blnResult
        End Function

        Public Function Delete(ByVal objCapChuong As infCapChuong, ByVal Nhom As String) As Boolean
            Dim cnCapChuong As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_CAP_CHUONG (ID,CCH_ID,MA_CHUONG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_CAP_CHUONG.NEXTVAL") & "," & _
                        objCapChuong.CCH_ID.Trim & ",'" & objCapChuong.MA_CHUONG & "','" & objCapChuong.NGUOI_TAO & "', SYSDATE,'0','" & objCapChuong.TYPE & "') "
            ElseIf Nhom = "11" Then
                If objCapChuong.TT_XU_LY = "1" Then
                    strSql = "DELETE FROM TCS_DM_CAP_CHUONG " & _
                               "WHERE CCH_ID=" & objCapChuong.CCH_ID
                ElseIf objCapChuong.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_CAP_CHUONG SET NGUOI_DUYET='" & objCapChuong.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID=" & objCapChuong.ID
                End If
            End If
            Try
                cnCapChuong = New DataAccess
                cnCapChuong.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCapChuong.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_CAP_CHUONG SET NGUOI_DUYET ='" & objCapChuong.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID=" & objCapChuong.ID
                    cnCapChuong.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa cấp - chương")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá cơ mục lục cấp - chương: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
            End Try
                Return blnResult
        End Function

        Public Function Load(ByVal strCCH_ID As String) As infCapChuong
            Dim objCapChuong As New infCapChuong
            Dim cnCapChuong As DataAccess
            Dim drCapChuong As IDataReader
            Dim strSQL As String = "Select CCH_ID, MA_CHUONG, TEN, GHI_CHU, TINH_TRANG From TCS_DM_CAP_CHUONG " & _
                        "Where CCH_ID=" & strCCH_ID
            Try
                cnCapChuong = New DataAccess
                drCapChuong = cnCapChuong.ExecuteDataReader(strSQL, CommandType.Text)
                If drCapChuong.Read Then
                    objCapChuong = New infCapChuong
                    objCapChuong.CCH_ID = strCCH_ID
                    objCapChuong.MA_CHUONG = drCapChuong("MA_CHUONG").ToString()
                    objCapChuong.Ten = drCapChuong("TEN").ToString()
                    objCapChuong.TINH_TRANG = drCapChuong("TINH_TRANG").ToString()
                    objCapChuong.GHICHU = drCapChuong("GHI_CHU").ToString()
                End If
                Return objCapChuong
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drCapChuong Is Nothing Then drCapChuong.Close()
                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
            End Try
            Return objCapChuong
        End Function

        Public Function Load(ByVal tmpCapChuong As infCapChuong) As infCapChuong
            Dim objCapChuong As New infCapChuong
            Dim cnCapChuong As DataAccess
            Dim drCapChuong As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc
            Dim strSQL As String = "Select CCH_ID, MA_CHUONG, TEN, GHI_CHU, TINH_TRANG From TCS_DM_CAP_CHUONG " & _
                "Where " & IIf(tmpCapChuong.CCH_ID <> "", "CCH_ID <> " & tmpCapChuong.CCH_ID & " and ", "") & "MA_CHUONG='" & tmpCapChuong.MA_CHUONG & "'"

            Try
                cnCapChuong = New DataAccess
                drCapChuong = cnCapChuong.ExecuteDataReader(strSQL, CommandType.Text)
                If drCapChuong.Read Then
                    objCapChuong = New infCapChuong
                    objCapChuong.CCH_ID = drCapChuong("CCH_ID").ToString()
                    objCapChuong.MA_CHUONG = drCapChuong("MA_CHUONG").ToString()
                    objCapChuong.Ten = drCapChuong("TEN").ToString()
                    objCapChuong.TINH_TRANG = drCapChuong("TINH_TRANG").ToString()
                    objCapChuong.GHICHU = drCapChuong("GHI_CHU").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drCapChuong Is Nothing Then drCapChuong.Close()
                If Not cnCapChuong Is Nothing Then cnCapChuong.Dispose()
            End Try
            Return objCapChuong
        End Function

        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String
            Dim cnCapChuong As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "SELECT   cch_id, ma_chuong, ten, ghi_chu, " & _
                         "         DECODE (tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngừng hoạt động', " & _
                         "                 1, 'Đang hoạt động' " & _
                         "                ) tinh_trang " & _
                         "    FROM tcs_dm_cap_chuong " & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY ma_chuong "
                cnCapChuong = New DataAccess
                dsCapChuong = cnCapChuong.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnCapChuong.Dispose()
            End Try

            Return dsCapChuong
        End Function
        Public Function Load_new(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String
            Dim cnCapChuong As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "SELECT   a.cch_id, a.ma_chuong, a.ten, a.ghi_chu, " & _
                         "         DECODE (a.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngừng hoạt động', " & _
                         "                 1, 'Đang hoạt động' " & _
                         "                ) tinh_trang,DECODE(b.TYPE,NULL,'',1,'Thêm',2,'Sửa',3,'Xóa') ThayDoi ,DECODE(b.TT_XU_LY,NULL,'',0,'Chờ duyệt',1,'Đã duyệt',2,'Từ chối') xu_ly ," & _
                         " b.id,b.Type,b.TT_XU_LY, b.ma_chuong as ma_chuong_new, b.ten as ten_new, b.ghi_chu as ghi_chu_new, DECODE(b.tinh_trang,NULL,'',0,'Ngừng hoạt động',1,'Đang hoạt động') tinh_trang_new " & _
                         "    FROM tcs_dm_cap_chuong a right outer join his_dm_cap_chuong b on a.cch_id=b.cch_id" & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY b.ID DESC "
                cnCapChuong = New DataAccess
                dsCapChuong = cnCapChuong.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnCapChuong.Dispose()
            End Try

            Return dsCapChuong
        End Function
        Public Function CheckDelete(ByVal strCCH_ID As String) As Object
            Dim cnKhoBac As DataAccess
            Dim dsKhoBac As DataSet

            Try
                Dim strSQL As String = "select ma_chuong from (select ma_chuong from tcs_ctu_dtl union all select ma_chuong from tcs_baolanh_dtl) " & _
                        "Where ma_chuong=" & strCCH_ID

                cnKhoBac = New DataAccess
                dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
            End Try

            Return dsKhoBac
        End Function
        Public Function CheckExist(ByVal objCapChuong As infCapChuong) As Boolean
            Return Load(objCapChuong).CCH_ID <> ""
        End Function

        Public Function CheckExist(ByVal strCCH_ID As String) As Boolean
            Return Load(strCCH_ID).CCH_ID <> ""
        End Function
    End Class

End Namespace
