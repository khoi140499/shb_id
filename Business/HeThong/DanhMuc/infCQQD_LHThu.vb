﻿Namespace DanhMuc.CQQD_LHThu

    Public Class infCQQD_LHThu
        'ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq

        Private strMaQuy As String
        Private strTK_NS As String
        Private strMaChuong As String
        Private strMa_nkt As String

        Private strMa_ndkt As String
        Private strMaDBHC As String
        Private strKHTK As String
        Private strMa_LH As String

        Private strSHKB As String
        Private strMa_cqqd As String
        Private strMa_dvsdns As String
        Private strTen_cq As String

        Private strID As String
        Private strTinhTrang As String
        Private strNGUOI_TAO As String
        Private strNGUOI_DUYET As String
        Private strTYPE As String
        Private strTT_XU_LY As String
        Private strID_HIS As String

        Sub New()
            strMaQuy = ""
            strTK_NS = ""
            strMaChuong = ""
            strMa_nkt = ""
            strMa_ndkt = ""
            strMaDBHC = ""
            strKHTK = ""
            strMa_LH = ""
            strSHKB = ""
            strMa_cqqd = ""
            strMa_dvsdns = ""
            strTen_cq = ""

            strID = ""
            strTinhTrang = 0
            strNGUOI_TAO = ""
            strNGUOI_DUYET = ""
            strTYPE = ""
            strTT_XU_LY = ""
            strID_HIS = ""
        End Sub

        Sub New(ByVal drwTaiKhoan As DataRow)
            'ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq
            strMaQuy = drwTaiKhoan("ma_quy").ToString().Trim()
            strTK_NS = drwTaiKhoan("tk_ns").ToString().Trim()
            strMaChuong = drwTaiKhoan("ma_chuong").ToString().Trim()
            strMa_nkt = drwTaiKhoan("ma_nkt").ToString().Trim()
            strMa_ndkt = drwTaiKhoan("ma_ndkt").ToString().Trim()
            strMaDBHC = drwTaiKhoan("ma_dbhc").ToString().Trim()
            strKHTK = drwTaiKhoan("khtk").ToString().Trim()
            strMa_LH = drwTaiKhoan("ma_lh").ToString().Trim()
            strSHKB = drwTaiKhoan("shkb").ToString().Trim()
            strMa_cqqd = drwTaiKhoan("ma_cqqd").ToString().Trim()
            strMa_dvsdns = drwTaiKhoan("ma_dvsdns").ToString().Trim()
            strTen_cq = drwTaiKhoan("ten_cq").ToString().Trim()
            strTinhTrang = drwTaiKhoan("TINH_TRANG").ToString().Trim()
            strID_HIS = drwTaiKhoan("ID_HIS").ToString().Trim()
        End Sub

        Public Property MA_QUY() As String
            Get
                Return strMaQuy
            End Get
            Set(ByVal Value As String)
                strMaQuy = Value
            End Set
        End Property

        Public Property TK_NS() As String
            Get
                Return strTK_NS
            End Get
            Set(ByVal Value As String)
                strTK_NS = Value
            End Set
        End Property

       
        Public Property MA_CHUONG() As String
            Get
                Return strMaChuong
            End Get
            Set(ByVal Value As String)
                strMaChuong = Value
            End Set
        End Property
        Public Property MA_NKT() As String
            Get
                Return strMa_nkt
            End Get
            Set(ByVal Value As String)
                strMa_nkt = Value
            End Set
        End Property

        Public Property MA_NDKT() As String
            Get
                Return strMa_ndkt
            End Get
            Set(ByVal Value As String)
                strMa_ndkt = Value
            End Set
        End Property
        Public Property MA_DBHC() As String
            Get
                Return strMaDBHC
            End Get
            Set(ByVal Value As String)
                strMaDBHC = Value
            End Set
        End Property
        Public Property KHTK() As String
            Get
                Return strKHTK
            End Get
            Set(ByVal Value As String)
                strKHTK = Value
            End Set
        End Property
        Public Property MA_LH() As String
            Get
                Return strMa_LH
            End Get
            Set(ByVal Value As String)
                strMa_LH = Value
            End Set
        End Property
        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property
        Public Property MA_CQQD() As String
            Get
                Return strMa_cqqd
            End Get
            Set(ByVal Value As String)
                strMa_cqqd = Value
            End Set
        End Property
        Public Property MA_DVSDNS() As String
            Get
                Return strMa_dvsdns
            End Get
            Set(ByVal Value As String)
                strMa_dvsdns = Value
            End Set
        End Property
        Public Property TEN_CQ() As String
            Get
                Return strTen_cq
            End Get
            Set(ByVal Value As String)
                strTen_cq = Value
            End Set
        End Property


        Public Property ID() As String
            Get
                Return strID
            End Get
            Set(ByVal Value As String)
                strID = Value
            End Set
        End Property
        Public Property NGUOI_TAO() As String
            Get
                Return strNGUOI_TAO
            End Get
            Set(ByVal Value As String)
                strNGUOI_TAO = Value
            End Set
        End Property
        Public Property NGUOI_DUYET() As String
            Get
                Return strNGUOI_DUYET
            End Get
            Set(ByVal Value As String)
                strNGUOI_DUYET = Value
            End Set
        End Property
        Public Property TYPE() As String
            Get
                Return strTYPE
            End Get
            Set(ByVal Value As String)
                strTYPE = Value
            End Set
        End Property

        Public Property TT_XU_LY() As String
            Get
                Return strTT_XU_LY
            End Get
            Set(ByVal Value As String)
                strTT_XU_LY = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTinhTrang
            End Get
            Set(ByVal Value As String)
                strTinhTrang = Value
            End Set
        End Property
        Public Property ID_HIS() As String
            Get
                Return strID_HIS
            End Get
            Set(ByVal Value As String)
                strID_HIS = Value
            End Set
        End Property

        Public ReadOnly Property TINHTRANG() As String
            Get
                If strTinhTrang = "0" Then
                    Return "Dang hoat dong"
                ElseIf strTinhTrang = "1" Then
                    Return "Ngung hoat dong"
                Else
                    Return ""
                End If
            End Get
        End Property

    End Class
End Namespace
