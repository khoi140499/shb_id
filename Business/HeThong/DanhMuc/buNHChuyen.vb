﻿Imports VBOracleLib
Namespace DanhMuc.NHChuyen
    Public Class buNHChuyen
        Sub New()
        End Sub

        Public Function Load(ByVal WhereClause As String) As DataSet
            Dim objNganHangChuyen As New daNHChuyen
            Dim dsNganHangChuyen As New DataSet
            Try
                dsNganHangChuyen = objNganHangChuyen.Load(WhereClause)
                Return dsNganHangChuyen
            Catch ex As Exception
                Return dsNganHangChuyen
            Finally
            End Try
        End Function
        Public Function Load_new(ByVal WhereClause As String) As DataSet
            Dim objNganHangChuyen As New daNHChuyen
            Dim dsNganHangChuyen As New DataSet
            Try
                dsNganHangChuyen = objNganHangChuyen.Load_new(WhereClause)
                Return dsNganHangChuyen
            Catch ex As Exception
                Return dsNganHangChuyen
            Finally
            End Try
        End Function

        Public Function Delete(ByVal objNganHangChuyen As infNHChuyen, ByVal Nhom As String) As Boolean
            ' objNganHangChuyen.MA_DTHU = strMA_DTHU
            Dim tmpNganHang As New daNHChuyen
            'Dim kpKT As Byte = daNHChuyen.Check2Del(objNganHangChuyen)
            Dim blnResult As Boolean = True
            'If kpKT = 0 Then
            blnResult = tmpNganHang.Delete(objNganHangChuyen, Nhom)
            'Else
            '    blnResult = False
            'End If
            Return blnResult
        End Function

        Public Function Insert(ByVal objNganHangChuyen As infNHChuyen, ByVal Nhom As String) As Boolean
            Dim tmpNganHang As New daNHChuyen
            If tmpNganHang.CheckExist(objNganHangChuyen) = False Then
                Return tmpNganHang.Insert(objNganHangChuyen, Nhom)
            Else
                MsgBox("Danh mục ngân hàng đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End If
        End Function

        Public Function Update(ByVal objNganHangChuyen As infNHChuyen, ByVal Nhom As String) As Boolean
            Dim tmpNganHang As New daNHChuyen
            If tmpNganHang.CheckExist(objNganHangChuyen) = True Then
                Return tmpNganHang.Update(objNganHangChuyen, Nhom)
            Else
                Return False
            End If
        End Function
        Public Function CheckDelete(ByVal strTmpNHC As String) As Boolean
            Dim ds As DataSet
            Dim tmpNHC As New daNHChuyen
            ds = tmpNHC.CheckDelete(strTmpNHC)
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class
End Namespace
