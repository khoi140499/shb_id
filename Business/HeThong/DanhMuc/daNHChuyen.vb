﻿Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace DanhMuc.NHChuyen
    Public Class daNHChuyen
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnNHChuyen As DataAccess
            Dim dsNganHang As DataSet

            Try
                strSQL = "SELECT  * " & _
                         "    FROM tcs_nganhang_chuyen " & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY ma_nh_chuyen "

                cnNHChuyen = New DataAccess
                dsNganHang = cnNHChuyen.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try

            Return dsNganHang
        End Function
        Public Function Load_new(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnNHChuyen As DataAccess
            Dim dsNganHang As DataSet

            Try
                strSQL = "SELECT   a.id, a.ma_nh_chuyen, a.ten_nh_chuyen, " & _
                        " DECODE(b.TYPE,NULL,'',1,'Thêm',2,'Sửa',3,'Xóa') ThayDoi ,DECODE(b.TT_XU_LY,NULL,'',0,'Chờ duyệt',1,'Đã duyệt',2,'Từ chối') xu_ly ," & _
                        " b.his_id,b.Type,b.TT_XU_LY, b.ma_nh_chuyen as ma_nh_chuyen_new, b.ten_nh_chuyen as ten_nh_chuyen_new " & _
                        "   FROM tcs_nganhang_chuyen a, his_nh_chuyen b " & _
                        "   WHERE b.ma_nh_chuyen=a.ma_nh_chuyen(+) " & WhereClause & _
                        "ORDER BY b.HIS_ID DESC "
                cnNHChuyen = New DataAccess
                dsNganHang = cnNHChuyen.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '     CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try

            Return dsNganHang
        End Function

        Public Function Load(ByVal objNHCHuyen As infNHChuyen) As Object
            Dim strSQL As String
            Dim cnNHChuyen As DataAccess
            Dim dsNganHang As DataSet

            Try
                strSQL = "SELECT  * " & _
                         "    FROM tcs_nganhang_chuyen " & _
                         "   WHERE 1 = 1 " & _
                         "ORDER BY ma_nh_chuyen "

                cnNHChuyen = New DataAccess
                dsNganHang = cnNHChuyen.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try

            Return dsNganHang
        End Function
        Public Function LoadID(ByVal strID As String) As Boolean
            Dim objNHCHuyen As New infNHChuyen
            Dim cnNHChuyen As DataAccess
            Dim drNHChuyen As IDataReader
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT * " & _
                         "  FROM tcs_nganhang_chuyen " & _
                         " WHERE ma_nh_chuyen = '" + strID + "' "

                cnNHChuyen = New DataAccess
                drNHChuyen = cnNHChuyen.ExecuteDataReader(strSQL, CommandType.Text)
                If drNHChuyen.Read Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Ngân hàng")
                Throw ex
            Finally
                If Not drNHChuyen Is Nothing Then drNHChuyen.Close()
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try
            'Return objNHCHuyen
        End Function

        Public Function Insert(ByVal objNHCHuyen As infNHChuyen, ByVal Nhom As String) As Boolean
            Dim cnNHChuyen As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_NH_CHUYEN (HIS_ID,ID, ma_nh_chuyen, ten_nh_chuyen, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_NH_CHUYEN.NEXTVAL") & "," & Common.mdlCommon.getDataKey("tcs_nhchuyen_id_seq.NEXTVAL") & ", " & _
                                "'" & objNHCHuyen.Ma_NHC & "','" & objNHCHuyen.Ten_NHC & "'," & _
                                "'" & objNHCHuyen.NGUOI_TAO & "', SYSDATE,'0','" & objNHCHuyen.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objNHCHuyen.TT_XU_LY = "1" Then
                    'nếu duyệt
                    strSql = "INSERT INTO tcs_nganhang_chuyen (ID, ma_nh_chuyen, ten_nh_chuyen) " & _
                                " SELECT HIS.ID,HIS.ma_nh_chuyen,HIS.ten_nh_chuyen " & _
                                " FROM HIS_NH_CHUYEN HIS WHERE HIS.HIS_ID=" & objNHCHuyen.HIS_ID
                ElseIf objNHCHuyen.TT_XU_LY = "2" Then
                    'nếu từ chối
                    strSql = "UPDATE HIS_NH_CHUYEN SET NGUOI_DUYET='" & objNHCHuyen.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE HIS_ID=" & objNHCHuyen.HIS_ID
                End If
            End If
            Try
                cnNHChuyen = New DataAccess
                cnNHChuyen.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objNHCHuyen.TT_XU_LY = "1" Then
                    'Khi đã duyệt thêm mới 1 bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_NH_CHUYEN SET NGUOI_DUYET ='" & objNHCHuyen.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE HIS_ID=" & objNHCHuyen.HIS_ID
                    cnNHChuyen.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) 'CTuCommon.WriteLog(ex, "Lỗi khi thêm mới Ngân hàng")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objNHCHuyen As infNHChuyen, ByVal Nhom As String) As Boolean
            Dim cnNHChuyen As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_NH_CHUYEN (HIS_ID,ID, ma_nh_chuyen, ten_nh_chuyen, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_NH_CHUYEN.NEXTVAL") & "," & Common.mdlCommon.getDataKey("tcs_nhchuyen_id_seq.NEXTVAL") & "," & _
                                "'" & objNHCHuyen.Ma_NHC & "','" & objNHCHuyen.Ten_NHC & "'," & _
                                "'" & objNHCHuyen.NGUOI_TAO & "', SYSDATE,'0','" & objNHCHuyen.TYPE & "')"
            ElseIf Nhom = "11" Then
                If objNHCHuyen.TT_XU_LY = "1" Then
                    strSql = "UPDATE tcs_nganhang_chuyen SET ten_nh_chuyen='" & objNHCHuyen.Ten_NHC.Trim & "' WHERE ma_nh_chuyen='" & objNHCHuyen.Ma_NHC.Trim & "'"
                ElseIf objNHCHuyen.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_NH_CHUYEN SET NGUOI_DUYET='" & objNHCHuyen.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE HIS_ID=" & objNHCHuyen.HIS_ID
                End If
            End If
            Try
                cnNHChuyen = New DataAccess
                cnNHChuyen.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objNHCHuyen.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_NH_CHUYEN SET NGUOI_DUYET ='" & objNHCHuyen.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE HIS_ID=" & objNHCHuyen.HIS_ID
                    cnNHChuyen.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi cập nhật Ngân hàng")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objNHCHuyen As infNHChuyen, ByVal Nhom As String) As Boolean
            Dim cnNHChuyen As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_NH_CHUYEN (HIS_ID,ma_nh_chuyen, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_NH_CHUYEN.NEXTVAL") & _
                         ",'" & objNHCHuyen.Ma_NHC & "','" & objNHCHuyen.NGUOI_TAO & "', SYSDATE,'0','" & objNHCHuyen.TYPE & "') "
            ElseIf Nhom = "11" Then
                If objNHCHuyen.TT_XU_LY = "1" Then
                    strSql = "DELETE FROM tcs_nganhang_chuyen " & _
                               "WHERE ma_nh_chuyen='" & objNHCHuyen.Ma_NHC & "'"
                ElseIf objNHCHuyen.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_NH_CHUYEN SET NGUOI_DUYET='" & objNHCHuyen.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE HIS_ID=" & objNHCHuyen.HIS_ID
                End If
            End If
            Try
                cnNHChuyen = New DataAccess
                cnNHChuyen.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objNHCHuyen.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_NH_CHUYEN SET NGUOI_DUYET ='" & objNHCHuyen.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE HIS_ID=" & objNHCHuyen.HIS_ID
                    cnNHChuyen.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi xóa Ngân hàng")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá danh mục loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function CheckExist(ByVal objNHCHuyen As infNHChuyen) As Boolean
            If LoadID(objNHCHuyen.Ma_NHC) = True Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function Check2Del(ByVal objNHCHuyen As infNHChuyen) As Byte
            Dim cnNHChuyen As DataAccess
            Dim drNHChuyen As IDataReader
            Dim strSQL As String
            Dim bytResult As Byte = 0

            Try
                strSQL = "SELECT ma_nh_chuyen " & _
                        "  FROM tcs_nganhang_chuyen " & _
                        " WHERE ma_nh_chuyen = '" & objNHCHuyen.Ma_NHC & "' "
                cnNHChuyen = New DataAccess
                drNHChuyen = cnNHChuyen.ExecuteDataReader(strSQL, CommandType.Text)
                If drNHChuyen.Read Then bytResult = 1
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi kiểm tra thông tin Ngân hàng")
                Throw ex
            Finally
                If Not drNHChuyen Is Nothing Then drNHChuyen.Close()
                If Not cnNHChuyen Is Nothing Then cnNHChuyen.Dispose()
            End Try

            ' Trả lại kết quả cho hàm
            Return bytResult
        End Function
        Public Function CheckDelete(ByVal strMA_NHC As String) As Object
            Dim cnNHC As DataAccess
            Dim dsNHC As DataSet

            Try
                Dim strSQL As String = "select ma_nh_a from (select ma_nh_a from tcs_ctu_hdr union all select ma_nh_a from tcs_baolanh_hdr) " & _
                        "Where ma_nh_a='" & strMA_NHC & "'"

                cnNHC = New DataAccess
                dsNHC = cnNHC.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not cnNHC Is Nothing Then cnNHC.Dispose()
            End Try

            Return dsNHC
        End Function
    End Class
End Namespace

