﻿Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace DanhMuc.CQQD
    Public Class daCQQD

        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Function Insert(ByVal objCQQD As infCQQD, ByVal Nhom As String) As Boolean
            Dim cnCQQD As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
          
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_TCS_DM_CQQD (id_cqqd,id,ma_cqqd,ten_cqqd,ghi_chu,ma_kb,ma_cqthu,ngay_tao,nguoi_tao,ma_dbhc,tinh_trang,type,tt_xu_ly) " & _
                                "VALUES ('" & Common.mdlCommon.getDataKey("ID_HIS_TCS_DM_CQQD.NEXTVAL") & "','" & Common.mdlCommon.getDataKey("tcs_cqqd_bienlai_seq.NEXTVAL") & "', " & _
                                "'" & objCQQD.MA_CQQD & "','" & objCQQD.TEN_CQQD & "'," & _
                                "'" & objCQQD.GHI_CHU & "','" & objCQQD.MA_KB & "','" & objCQQD.MA_CQTHU & "', SYSDATE ,'" & objCQQD.NGUOI_TAO & "','" & objCQQD.MA_DBHC & "','" & objCQQD.TINH_TRANG & "','" & objCQQD.TYPE & "','0')"
            ElseIf Nhom = "11" Then
                If objCQQD.TT_XU_LY = "1" Then
                    'nếu duyệt
                    strSql = "INSERT INTO TCS_DM_CQQD (MA_CQQD,TEN_CQQD,GHI_CHU,MA_CQTHU,MA_KB, MDATE, MAKER,MA_DBHC, ID,TINH_TRANG) " & _
                                " SELECT HIS.MA_CQQD,HIS.TEN_CQQD,HIS.GHI_CHU,HIS.MA_CQTHU,HIS.MA_KB,HIS.NGAY_TAO,HIS.NGUOI_TAO,HIS.MA_DBHC,HIS.ID,HIS.TINH_TRANG " & _
                                " FROM HIS_TCS_DM_CQQD  HIS WHERE HIS.ID_CQQD='" & objCQQD.ID_CQQD & "'"
                ElseIf objCQQD.TT_XU_LY = "2" Then
                    'nếu từ chối
                    strSql = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET='" & objCQQD.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID_CQQD='" & objCQQD.ID_CQQD & "'"
                End If
            End If

            Try
                cnCQQD = New DataAccess
                cnCQQD.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCQQD.TT_XU_LY = "1" Then
                    'Khi đã duyệt thêm mới 1 bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET ='" & objCQQD.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID_CQQD='" & objCQQD.ID_CQQD & "'"
                    cnCQQD.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới cấp - chương: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnCQQD Is Nothing Then cnCQQD.Dispose()
            End Try
            Return blnResult
        End Function


        Public Function Update(ByVal objCQQD As infCQQD, ByVal Nhom As String) As Boolean
            Dim cnCQQD As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            Dim strSql1 As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_TCS_DM_CQQD (id_cqqd,id,ma_cqqd,ten_cqqd,ghi_chu,ma_kb,ma_cqthu,ngay_tao,nguoi_tao,ma_dbhc,tinh_trang,type,tt_xu_ly) " & _
                              "VALUES ('" & Common.mdlCommon.getDataKey("ID_HIS_TCS_DM_CQQD.NEXTVAL") & "','" & objCQQD.ID.Trim & "', " & _
                              "'" & objCQQD.MA_CQQD & "','" & objCQQD.TEN_CQQD & "'," & _
                              "'" & objCQQD.GHI_CHU & "','" & objCQQD.MA_KB & "','" & objCQQD.MA_CQTHU & "', SYSDATE ,'" & objCQQD.NGUOI_TAO & "','" & objCQQD.MA_DBHC & "','" & objCQQD.TINH_TRANG & "','" & objCQQD.TYPE & "','0')"
            ElseIf Nhom = "11" Then
                If objCQQD.TT_XU_LY = "1" Then

                    strSql = "UPDATE TCS_DM_CQQD SET TEN_CQQD='" & objCQQD.TEN_CQQD.Trim & "', MA_CQTHU='" & objCQQD.MA_CQTHU.Trim & "', MA_CQQD='" & objCQQD.MA_CQQD.Trim & "',MA_KB='" & objCQQD.MA_KB.Trim & "',MA_DBHC='" & objCQQD.MA_DBHC.Trim & "',GHI_CHU='" & objCQQD.GHI_CHU.Trim & "', TINH_TRANG='" & objCQQD.TINH_TRANG.Trim & "'  WHERE ID='" & objCQQD.ID.Trim & "'"

                    'strSql1 = "INSERT INTO TCS_DM_CQQDTCS_DM_CQQD a (a.MDATE,a.MAKER) SELECT HIS.NGAY_TAO,HIS.NGUOI_TAO FROM HIS_TCS_DM_CQQD  HIS WHERE HIS.ID='" & objCQQD.ID.Trim & "' and HIS.ID = a.ID "
                    'cnCQQD = New DataAccess
                    'cnCQQD.ExecuteNonQuery(strSql1, CommandType.Text)
                    '                If objCQQD.ID.Equals("'" & objCQQD.ID.Trim & "'") Then
                    '                    strSql = " Update (TCS_DM_CQQD) " & _
                    '  " SET TEN_CQQD='" & objCQQD.TEN_CQQD.Trim & "', MA_CQTHU='" & objCQQD.MA_CQTHU.Trim & "', MA_CQQD='" & objCQQD.MA_CQQD.Trim & "',MA_KB='" & objCQQD.MA_KB.Trim & "',MA_DBHC='" & objCQQD.MA_DBHC.Trim & "',GHI_CHU='" & objCQQD.GHI_CHU.Trim & "', TINH_TRANG='" & objCQQD.TINH_TRANG.Trim & "',MDATE = t2.NGAY_TAO,MAKER = t2.NGUOI_TAO  " & _
                    ' " FROM TCS_DM_CQQD t1, " & _
                    '"  HIS_TCS_DM_CQQD t2 " & _
                    ' " WHERE t1.id = t2.id "
                    '                End If
                ElseIf objCQQD.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET='" & objCQQD.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID_CQQD='" & objCQQD.ID_CQQD & "'"
                End If
                End If
            Try
                cnCQQD = New DataAccess
                cnCQQD.ExecuteNonQuery(strSql, CommandType.Text)

                If Nhom = "11" And objCQQD.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET ='" & objCQQD.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID_CQQD='" & objCQQD.ID_CQQD & "'"
                    cnCQQD.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật cấp - chương: " & ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật cấp - chương")
                blnResult = False
                Throw ex
            Finally
                If Not cnCQQD Is Nothing Then cnCQQD.Dispose()
            End Try
            Return blnResult
        End Function


        Public Function Delete(ByVal objCQQD As infCQQD, ByVal Nhom As String) As Boolean
            Dim cnCQQD As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_TCS_DM_CQQD (id_cqqd,id,ma_cqqd,ten_cqqd,ngay_tao,nguoi_tao,type,tt_xu_ly) " & _
                            "VALUES ('" & Common.mdlCommon.getDataKey("ID_HIS_TCS_DM_CQQD.NEXTVAL") & "','" & objCQQD.ID.Trim & "', " & _
                            "'" & objCQQD.MA_CQQD & "','" & objCQQD.TEN_CQQD & "', SYSDATE ,'" & objCQQD.NGUOI_TAO & "','" & objCQQD.TYPE & "','0')"
            ElseIf Nhom = "11" Then
                If objCQQD.TT_XU_LY = "1" Then
                    strSql = "DELETE FROM TCS_DM_CQQD " & _
                               "WHERE ID='" & objCQQD.ID & "'"
                ElseIf objCQQD.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET='" & objCQQD.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE ID_CQQD='" & objCQQD.ID_CQQD & "'"
                End If
            End If
            Try
                cnCQQD = New DataAccess
                cnCQQD.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCQQD.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_TCS_DM_CQQD SET NGUOI_DUYET ='" & objCQQD.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE ID_CQQD='" & objCQQD.ID_CQQD & "'"
                    cnCQQD.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa cấp - chương")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá cơ mục lục cấp - chương: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnCQQD Is Nothing Then cnCQQD.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strID As String) As infCQQD
            Dim objCQQD As New infCQQD
            Dim cnCQQD As DataAccess
            Dim drCQQD As IDataReader
            Dim strSQL As String = "Select ID, MA_CQQD,TEN_CQQD, GHI_CHU, MA_CQTHU,ma_dbhc, TINH_TRANG, MA_KB,TO_CHAR( MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, MAKER From TCS_DM_CQQD " & _
                        "Where ID='" & strID & "'"
            Try
                cnCQQD = New DataAccess
                drCQQD = cnCQQD.ExecuteDataReader(strSQL, CommandType.Text)
                If drCQQD.Read Then
                    objCQQD = New infCQQD
                    objCQQD.ID = strID
                    objCQQD.MA_CQQD = drCQQD("MA_CQQD").ToString()
                    objCQQD.TEN_CQQD = drCQQD("TEN_CQQD").ToString()
                    objCQQD.TINH_TRANG = drCQQD("TINH_TRANG").ToString()
                    objCQQD.GHI_CHU = drCQQD("GHI_CHU").ToString()
                    objCQQD.MA_CQTHU = drCQQD("MA_CQTHU").ToString()
                    objCQQD.MA_DBHC = drCQQD("MA_DBHC").ToString()
                    objCQQD.MA_KB = drCQQD("MA_KB").ToString()
                    objCQQD.NGAY_TAO = drCQQD("MDATE").ToString()
                    objCQQD.NGUOI_TAO = drCQQD("MAKER").ToString()

                End If
                Return objCQQD
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drCQQD Is Nothing Then drCQQD.Close()
                If Not cnCQQD Is Nothing Then cnCQQD.Dispose()
            End Try
            Return objCQQD
        End Function

        Public Function Load(ByVal tmpCQQD As infCQQD) As infCQQD
            Dim objCQQD As New infCQQD
            Dim cnCQQD As DataAccess
            Dim drCQQD As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc
            'Dim strSQL As String = "Select ID, MA_CQQD,TEN_CQQD, GHI_CHU, MA_CQTHU,ma_dbhc, TINH_TRANG, MA_KB,TO_CHAR( MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, MAKER From TCS_DM_CQQD " & _
            '    "Where " & IIf(tmpCQQD.ID <> "", "ID <> '" & tmpCQQD.ID & "' and ", "") & "MA_CQQD='" & tmpCQQD.MA_CQQD & "'"

            Dim strSQL As String = "Select ID, MA_CQQD,TEN_CQQD, GHI_CHU, MA_CQTHU,ma_dbhc, TINH_TRANG, MA_KB,TO_CHAR( MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, MAKER From TCS_DM_CQQD " & _
                "Where " & IIf(tmpCQQD.ID <> "", "ID = '" & tmpCQQD.ID & "'", "") & ""

            Try
                cnCQQD = New DataAccess
                drCQQD = cnCQQD.ExecuteDataReader(strSQL, CommandType.Text)
                If drCQQD.Read Then
                    objCQQD = New infCQQD
                    objCQQD.ID = drCQQD("ID").ToString()
                    objCQQD.MA_CQQD = drCQQD("MA_CQQD").ToString()
                    objCQQD.TEN_CQQD = drCQQD("TEN_CQQD").ToString()
                    objCQQD.TINH_TRANG = drCQQD("TINH_TRANG").ToString()
                    objCQQD.GHI_CHU = drCQQD("GHI_CHU").ToString()
                    objCQQD.MA_CQTHU = drCQQD("MA_CQTHU").ToString()
                    objCQQD.MA_DBHC = drCQQD("MA_DBHC").ToString()
                    objCQQD.MA_KB = drCQQD("MA_KB").ToString()
                    objCQQD.NGAY_TAO = drCQQD("MDATE").ToString()
                    objCQQD.NGUOI_TAO = drCQQD("MAKER").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                If Not drCQQD Is Nothing Then drCQQD.Close()
                If Not cnCQQD Is Nothing Then cnCQQD.Dispose()
            End Try
            Return objCQQD
        End Function

        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String
            Dim cnCQQD As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "SELECT  a.ID, a.MA_CQQD,a.TEN_CQQD, a.GHI_CHU, a.MA_CQTHU,a.ma_dbhc, a.MA_KB,TO_CHAR( a.MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, a.MAKER, " & _
                         "         DECODE (a.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngừng hoạt động', " & _
                         "                 1, 'Đang hoạt động' " & _
                         "                ) tinh_trang " & _
                         "    FROM TCS_DM_CQQD a " & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY a.ma_cqqd "
                cnCQQD = New DataAccess
                dsCapChuong = cnCQQD.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnCQQD.Dispose()
            End Try

            Return dsCapChuong
        End Function
        Public Function Load_new(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String
            Dim cnCQQD As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "SELECT   a.ID, a.MA_CQQD,a.TEN_CQQD, a.GHI_CHU, a.MA_CQTHU,a.ma_dbhc, a.MA_KB,TO_CHAR( a.MDATE,'DD/MM/YYYY HH24:MI:SS') MDATE, a.MAKER, " & _
                         "         DECODE (a.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngừng hoạt động', " & _
                         "                 1, 'Đang hoạt động' " & _
                         "                ) tinh_trang,DECODE(b.TYPE,NULL,'',1,'Thêm',2,'Sửa',3,'Xóa') ThayDoi ,DECODE(b.TT_XU_LY,NULL,'',0,'Chờ duyệt',1,'Đã duyệt',2,'Từ chối') xu_ly ," & _
                         " b.id_cqqd,b.Type,b.TT_XU_LY, b.MA_CQQD as ma_cqqd_new, b.TEN_CQQD as ten_cqqd_new, b.ghi_chu as ghi_chu_new,b.MA_CQTHU as ma_cqthu_new ,b.ma_dbhc as ma_dbhc_new, b.MA_KB as ma_kb_new ,TO_CHAR( b.NGAY_TAO,'DD/MM/YYYY HH24:MI:SS') NGAY_TAO, b.NGUOI_TAO , DECODE(b.tinh_trang,NULL,'',0,'Ngừng hoạt động',1,'Đang hoạt động') tinh_trang_new " & _
                         "    FROM TCS_DM_CQQD a right outer join HIS_TCS_DM_CQQD b on a.id=b.id" & _
                         "   WHERE 1 = 1 " & WhereClause & _
                         "ORDER BY b.ID_CQQD DESC "
                cnCQQD = New DataAccess
                dsCapChuong = cnCQQD.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnCQQD.Dispose()
            End Try

            Return dsCapChuong
        End Function
 
        'Public Function CheckExist(ByVal objCQQD As infCQQD) As Boolean
        '    Return Load(objCQQD).ID <> ""
        'End Function

        'Public Function CheckExist(ByVal ID As String) As Boolean
        '    Return Load(ID).ID <> ""
        'End Function
    End Class

End Namespace