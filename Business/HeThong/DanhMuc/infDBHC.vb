Namespace DanhMuc.DBHC
    Public Class infDBHC
        Private strXaID As String
        Private strDBHC As String
        Private strTenDBHC As String
        Private strTinhTrang As String
        Private strMaCha As String
        Private strID As String
        Private strNGUOI_TAO As String
        Private strNGUOI_DUYET As String
        Private strTYPE As String
        Private strTT_XU_LY As String

        Sub New()
            strDBHC = ""
            strTenDBHC = ""
            strTinhTrang = 0
            strMaCha = ""
            strID = ""
            strNGUOI_TAO = ""
            strNGUOI_DUYET = ""
            strTYPE = ""
            strTT_XU_LY = ""
        End Sub

        Sub New(ByVal drwTaiKhoan As DataRow)
            strDBHC = drwTaiKhoan("Ma_xa").ToString().Trim()
            strTenDBHC = drwTaiKhoan("TEN").ToString().Trim()
            strTinhTrang = drwTaiKhoan("TINH_TRANG").ToString().Trim()
            strMaCha = drwTaiKhoan("ma_cha").ToString().Trim()
        End Sub
        Public Property Xa_ID() As String
            Get
                Return strXaID
            End Get
            Set(ByVal Value As String)
                strXaID = Value
            End Set
        End Property
        Public Property Ma_DBHC() As String
            Get
                Return strDBHC
            End Get
            Set(ByVal Value As String)
                strDBHC = Value
            End Set
        End Property

        Public Property TEN_DBHC() As String
            Get
                Return strTenDBHC
            End Get
            Set(ByVal Value As String)
                strTenDBHC = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTinhTrang
            End Get
            Set(ByVal Value As String)
                strTinhTrang = Value
            End Set
        End Property
        Public Property Ma_Cha() As String
            Get
                Return strMaCha
            End Get
            Set(ByVal Value As String)
                strMaCha = Value
            End Set
        End Property

        Public ReadOnly Property TINHTRANG() As String
            Get
                If strTinhTrang = "0" Then
                    Return "Dang hoat dong"
                ElseIf strTinhTrang = "1" Then
                    Return "Ngung hoat dong"
                Else
                    Return ""
                End If
            End Get
        End Property

        Public Property ID() As String
            Get
                Return strID
            End Get
            Set(ByVal Value As String)
                strID = Value
            End Set
        End Property
        Public Property NGUOI_TAO() As String
            Get
                Return strNGUOI_TAO
            End Get
            Set(ByVal Value As String)
                strNGUOI_TAO = Value
            End Set
        End Property
        Public Property NGUOI_DUYET() As String
            Get
                Return strNGUOI_DUYET
            End Get
            Set(ByVal Value As String)
                strNGUOI_DUYET = Value
            End Set
        End Property
        Public Property TYPE() As String
            Get
                Return strTYPE
            End Get
            Set(ByVal Value As String)
                strTYPE = Value
            End Set
        End Property

        Public Property TT_XU_LY() As String
            Get
                Return strTT_XU_LY
            End Get
            Set(ByVal Value As String)
                strTT_XU_LY = Value
            End Set
        End Property

    End Class
End Namespace
