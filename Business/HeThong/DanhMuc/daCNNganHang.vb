﻿Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace DanhMuc.CNNganHang
    Public Class daCNNganHang
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Load(ByVal WhereClause As String) As Object
            Dim strSQL As String
            Dim cnNganHang As DataAccess
            Dim dsNganHang As DataSet

            Try
                strSQL = "SELECT a.id, a.name, a.branch_id, a.branch_code, a.tinh_trang, a.phan_cap,a.ma_xa, " & _
                "case when  a.tinh_trang= 1 then 'Hoạt động' else 'Ngừng hoạt động' end tinhtrang, " & _
                "decode (phan_cap,'1','HSC','2','CN','0','PGD') phancap, taikhoan_tienmat" & _
                "    FROM tcs_dm_chinhanh a " & _
                "   WHERE 1 = 1 " & WhereClause & _
                "ORDER BY id "

                cnNganHang = New DataAccess
                dsNganHang = cnNganHang.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Chi nhánh ngân hàng")
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try

            Return dsNganHang
        End Function
        Public Function Load_new(ByVal WhereClause As String) As DataSet
            Dim strSQL As String
            Dim cnNganHang As DataAccess
            Dim dsCapChuong As DataSet

            Try
                strSQL = "SELECT   a.ID, a.BRANCH_ID, a.NAME, a.MA_XA,b.BRANCH_ID AS BRANCH_ID_NEW,DECODE(a.PHAN_CAP,NULL,'',1,'HO',2,'CN',0,'PGD') PHAN_CAP,  " & _
                         "         DECODE (a.tinh_trang, " & _
                         "                 NULL, '', " & _
                         "                 0, 'Ngừng hoạt động', " & _
                         "                 1, 'Đang hoạt động' " & _
                         "                ) tinh_trang,DECODE(b.TYPE,NULL,'',1,'Thêm',2,'Sửa',3,'Xóa') ThayDoi ,DECODE(b.TT_XU_LY,NULL,'',0,'Chờ duyệt',1,'Đã duyệt',2,'Từ chối') xu_ly ," & _
                         " b.his_id,b.ID as ID_NEW,b.Type,b.TT_XU_LY, DECODE(a.PHAN_CAP,NULL,'',1,'HO',2,'CN',0,'PGD') PHAN_CAP_new, b.NAME as ten_new, b.MA_XA as MA_XA_new, DECODE(b.tinh_trang,NULL,'',0,'Ngừng hoạt động',1,'Đang hoạt động') tinh_trang_new, b.taikhoan_tienmat " & _
                         "    FROM his_dm_chi_nhanh b, tcs_dm_chinhanh a " & _
                         "   WHERE b.id=a.id(+) " & WhereClause & _
                         "ORDER BY b.HIS_ID DESC "
                cnNganHang = New DataAccess
                dsCapChuong = cnNganHang.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
                cnNganHang.Dispose()
            End Try

            Return dsCapChuong
        End Function

        'Public Function Load(ByVal objCNNganHang As infCNNganHang) As Object
        '    Dim strSQL As String
        '    Dim cnNganHang As DataAccess
        '    Dim dsNganHang As DataSet

        '    Try
        '        strSQL = "SELECT  id, ma_xa, ma,  ten,  shkb " & _
        '                 "    FROM tcs_dm_chinhanh " & _
        '                 "   WHERE 1 = 1 " & _
        '                 "ORDER BY ma_xa "

        '        cnNganHang = New DataAccess
        '        dsNganHang = cnNganHang.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception
        '    Finally
        '        If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
        '    End Try

        '    Return dsNganHang
        'End Function

        'Public Function Load(ByVal strMA_XA As String, ByVal strMA_NH As String) As infCNNganHang
        '    Dim objCNNganHang As New infCNNganHang
        '    Dim cnNganHang As DataAccess
        '    Dim drNganHang As IDataReader
        '    Dim strSQL As String = ""
        '    Try
        '        strSQL = "SELECT id,ma_xa, ma, ten, shkb " & _
        '                 "  FROM tcs_dm_chinhanh " & _
        '                 " WHERE ma_xa = '" + strMA_XA + "' " & _
        '                 "   AND ma = '" + strMA_NH + "' "

        '        cnNganHang = New DataAccess
        '        drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
        '        If drNganHang.Read Then
        '            objCNNganHang = New infCNNganHang
        '            objCNNganHang.MA_XA = strMA_XA
        '            objCNNganHang.MA_NH = drNganHang("ma").ToString()
        '            objCNNganHang.TEN_NH = drNganHang("ten").ToString()
        '            objCNNganHang.SHKB = drNganHang("shkb").ToString()
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If Not drNganHang Is Nothing Then drNganHang.Close()
        '        If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
        '    End Try
        '    Return objCNNganHang
        'End Function
        Public Function LoadID(ByVal strID As String) As infCNNganHang
            Dim objCNNganHang As New infCNNganHang
            Dim cnNganHang As DataAccess
            Dim drNganHang As IDataReader
            Dim strSQL As String = ""
            Try
                strSQL = "SELECT a.id, a.name, a.branch_id,a.branch_code, a.tinh_trang, a.phan_cap " & _
                         "  FROM tcs_dm_chinhanh a" & _
                         " WHERE a.id = '" + strID + "' "

                cnNganHang = New DataAccess
                drNganHang = cnNganHang.ExecuteDataReader(strSQL, CommandType.Text)
                If drNganHang.Read Then
                    objCNNganHang = New infCNNganHang
                    objCNNganHang.MA_PGD = drNganHang("id").ToString()
                    objCNNganHang.TEN_PGD = drNganHang("name").ToString()
                    objCNNganHang.MA_CN = drNganHang("branch_code").ToString()
                    objCNNganHang.TINH_TRANG = drNganHang("tinh_trang").ToString()
                    objCNNganHang.PHAN_CAP = drNganHang("phan_cap").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin Chi nhánh ngân hàng theo ID")
                Throw ex
            Finally
                If Not drNganHang Is Nothing Then drNganHang.Close()
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return objCNNganHang
        End Function

        Public Function Insert(ByVal objCNNganHang As infCNNganHang, ByVal Nhom As String) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_CHI_NHANH (HIS_ID, ID, PHAN_CAP, NAME,BRANCH_ID, MA_XA, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE, TAIKHOAN_TIENMAT) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_CHI_NHANH.NEXTVAL") & ",'" & objCNNganHang.MA_PGD & "', " & _
                                "'" & objCNNganHang.PHAN_CAP & "','" & objCNNganHang.TEN_PGD & "','" & objCNNganHang.MA_CN & "', " & _
                                "'" & objCNNganHang.MA_XA & "','" & objCNNganHang.TINH_TRANG & "','" & objCNNganHang.NGUOI_TAO & "', SYSDATE,'0','" & objCNNganHang.TYPE & "','" & objCNNganHang.TAIKHOAN_TIENMAT & "')"
            ElseIf Nhom = "11" Then
                If objCNNganHang.TT_XU_LY = "1" Then
                    'nếu duyệt
                    strSql = "INSERT INTO TCS_DM_CHINHANH (ID, PHAN_CAP, NAME, MA_XA, TINH_TRANG, BRANCH_ID,BRANCH_CODE,PROVICE_CODE,TAIKHOAN_TIENMAT ) " & _
                                " SELECT HIS.ID, HIS.PHAN_CAP, HIS.NAME, HIS.MA_XA, HIS.TINH_TRANG, HIS.ID, HIS.BRANCH_ID, HIS.MA_XA, HIS.TAIKHOAN_TIENMAT " & _
                                " FROM HIS_DM_CHI_NHANH HIS WHERE HIS.HIS_ID=" & objCNNganHang.HIS_ID
                ElseIf objCNNganHang.TT_XU_LY = "2" Then
                    'nếu từ chối
                    strSql = "UPDATE HIS_DM_CHI_NHANH SET NGUOI_DUYET='" & objCNNganHang.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE HIS_ID=" & objCNNganHang.HIS_ID
                End If
            End If
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCNNganHang.TT_XU_LY = "1" Then
                    'Khi đã duyệt thêm mới 1 bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_CHI_NHANH SET NGUOI_DUYET ='" & objCNNganHang.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE HIS_ID=" & objCNNganHang.HIS_ID
                    cnNganHang.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi thêm mới Chi nhánh ngân hàng")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới loại - khoản: " & ex.ToString)
                blnResult = False
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objCNNganHang As infCNNganHang, ByVal Nhom As String) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_CHI_NHANH (HIS_ID,ID,BRANCH_ID, PHAN_CAP, NAME, MA_XA, TINH_TRANG, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE, TAIKHOAN_TIENMAT) " & _
                                "VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_CHI_NHANH.NEXTVAL") & ",'" & objCNNganHang.MA_PGD.Trim & "', " & _
                                "'" & objCNNganHang.MA_CN & "','" & objCNNganHang.PHAN_CAP & "','" & objCNNganHang.TEN_PGD & "'," & _
                                "'" & objCNNganHang.MA_XA & "','" & objCNNganHang.TINH_TRANG & "','" & objCNNganHang.NGUOI_TAO & "', SYSDATE,'0','" & objCNNganHang.TYPE & "','" & objCNNganHang.TAIKHOAN_TIENMAT & "')"
            ElseIf Nhom = "11" Then
                If objCNNganHang.TT_XU_LY = "1" Then
                    strSql = "UPDATE TCS_DM_CHINHANH SET NAME='" & objCNNganHang.TEN_PGD.Trim & "',MA_XA='" & objCNNganHang.MA_XA.Trim & "',BRANCH_ID='" & objCNNganHang.MA_CN & _
                             "', PHAN_CAP='" & objCNNganHang.PHAN_CAP & "', TINH_TRANG='" & objCNNganHang.TINH_TRANG.Trim & "', TAIKHOAN_TIENMAT='" & objCNNganHang.TAIKHOAN_TIENMAT & "'   WHERE ID='" & objCNNganHang.MA_PGD.Trim & "'"
                ElseIf objCNNganHang.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_CHI_NHANH SET NGUOI_DUYET='" & objCNNganHang.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE HIS_ID=" & objCNNganHang.HIS_ID
                End If
            End If
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCNNganHang.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_CHI_NHANH SET NGUOI_DUYET ='" & objCNNganHang.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE HIS_ID=" & objCNNganHang.HIS_ID
                    cnNganHang.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi cập nhật Chi nhánh ngân hàng")
                Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật loại - khoản: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objCNNganHang As infCNNganHang, ByVal Nhom As String) As Boolean
            Dim cnNganHang As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            If Nhom = "10" Then
                strSql = "INSERT INTO HIS_DM_CHI_NHANH (HIS_ID,ID, NGUOI_TAO, NGAY_TAO,TT_XU_LY,TYPE) VALUES (" & Common.mdlCommon.getDataKey("ID_HIS_DM_CHI_NHANH.NEXTVAL") & ",'" & _
                        objCNNganHang.MA_PGD.Trim & "','" & objCNNganHang.NGUOI_TAO & "', SYSDATE,'0','" & objCNNganHang.TYPE & "') "
            ElseIf Nhom = "11" Then
                If objCNNganHang.TT_XU_LY = "1" Then
                    strSql = "DELETE FROM TCS_DM_CHINHANH " & _
                               "WHERE ID='" & objCNNganHang.MA_PGD & "'"
                ElseIf objCNNganHang.TT_XU_LY = "2" Then
                    strSql = "UPDATE HIS_DM_CHI_NHANH SET NGUOI_DUYET='" & objCNNganHang.NGUOI_DUYET & "', NGAY_DUYET = SYSDATE , TT_XU_LY='2' WHERE HIS_ID=" & objCNNganHang.HIS_ID
                End If
            End If
            Try
                cnNganHang = New DataAccess
                cnNganHang.ExecuteNonQuery(strSql, CommandType.Text)
                If Nhom = "11" And objCNNganHang.TT_XU_LY = "1" Then
                    'Khi đã duyệt sửa bản ghi
                    Dim sqlUpdate As String = "UPDATE HIS_DM_CHI_NHANH SET NGUOI_DUYET ='" & objCNNganHang.NGUOI_DUYET & "',NGAY_DUYET = SYSDATE, TT_XU_LY='1' WHERE HIS_ID=" & objCNNganHang.HIS_ID
                    cnNganHang.ExecuteNonQuery(sqlUpdate, CommandType.Text)
                End If
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá danh mục loại - khoản: " & ex.ToString)
                blnResult = False
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi xóa Chi nhánh ngân hàng")
                Throw ex
            Finally
                If Not cnNganHang Is Nothing Then cnNganHang.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function CheckExist(ByVal objCNNganHang As infCNNganHang) As Boolean
            Return LoadID(objCNNganHang.MA_PGD).MA_PGD <> ""
        End Function

    End Class
End Namespace
