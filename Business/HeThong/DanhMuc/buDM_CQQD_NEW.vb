﻿
Namespace DanhMuc.CQQD
    Public Class buDM_CQQD_NEW
        Private strStatus As String

        Sub New()
        End Sub

        Sub New(ByVal strTmpStatus As String)
            strStatus = strTmpStatus
        End Sub

        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property

        Public Function Insert(ByVal objCQQD As infCQQD, ByVal Nhom As String) As Boolean
            Dim tmpCQQD As New daCQQD
            Try
                'If tmpCQQD.CheckExist(objCQQD) = False Then
                Return tmpCQQD.Insert(objCQQD, Nhom)
                'Else
                'MsgBox("Mã CQQD đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")

                'End If
            Catch ex As Exception
                Return False
            End Try

        End Function

        Public Function Update(ByVal objCQQD As infCQQD, ByVal Nhom As String) As Boolean
            Dim tmpCQQD As New daCQQD
            Try
                Dim objExist As infCQQD = tmpCQQD.Load(objCQQD)
                'If objExist.ID = "" Then
                If objExist.ID.Equals(" ") = False Then
                    Return tmpCQQD.Update(objCQQD, Nhom)
                Else
                    'Dim strMsg As String = "Bạn đã sửa mã cqqd trùng với mã cqqd khác đã tồn tại trong hệ thống:" & Chr(13) & _
                    '"Mã CQQD: " & objExist.MA_CQQD & Chr(13) & _
                    '"Tên CQQD: " & objExist.TEN_CQQD & Chr(13) & _
                    '"Tinh trạng: " & IIf(objExist.TINH_TRANG = "0", "Ngung hoat dong", "Dang hoat dong")
                    'MsgBox(strMsg, MsgBoxStyle.Critical, "Chú ý")
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function



        Public Function Delete(ByVal objCQQD As infCQQD, ByVal Nhom As String) As Boolean
            'Dim objCQQD As New infCQQD
            'objCQQD.CCH_ID = strTmpMaCapChuong
            Dim tmpCQQD As New daCQQD
            Return tmpCQQD.Delete(objCQQD, Nhom)
        End Function


        Public Function GetDataSet(ByVal WhereClause As String) As DataSet
            Dim objCQQD As New daCQQD
            Dim dsCQQD As DataSet
            Try
                dsCQQD = objCQQD.Load(WhereClause, "")
            Catch ex As Exception
            Finally
            End Try

            Return dsCQQD
        End Function

        Public Function GetDataSet_new(ByVal WhereClause As String) As DataSet
            Dim objCQQD As New daCQQD
            Dim dsCQQD As DataSet
            Try
                dsCQQD = objCQQD.Load_new(WhereClause, "")
            Catch ex As Exception
            Finally
            End Try
            Return dsCQQD
        End Function



        Public Function GetReport() As String
            Return Common.mdlCommon.Report_Type.DM_CAPCHUONG
        End Function

        'Public Function CheckDelete(ByVal strTmpMaCapChuong As String) As Boolean
        '    Dim ds As DataSet
        '    Dim tmpCQQD As New daCQQD
        '    ds = tmpCQQD.CheckDelete(strTmpMaCapChuong)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        Return True
        '    Else
        '        Return False
        '    End If
        'End Function
    End Class

End Namespace