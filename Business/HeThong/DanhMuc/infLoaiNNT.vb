Namespace DanhMuc.LoaiNNT
    Public Class infLoaiNNT
        Private strMA_LOAI_NNT As String
        Private strTEN_LOAI_NNT As String
        
        Sub New()
            strMA_LOAI_NNT = ""
            strTEN_LOAI_NNT = ""
            
        End Sub

        Sub New(ByVal drwLoaiHinh As DataRow)
            strMA_LOAI_NNT = drwLoaiHinh("MA_LOAI_NNT").ToString().Trim()
            strTEN_LOAI_NNT = drwLoaiHinh("TEN_LOAI_NNT").ToString().Trim()
        End Sub

        Public Property MA_LOAI_NNT() As String
            Get
                Return strMA_LOAI_NNT
            End Get
            Set(ByVal Value As String)
                strMA_LOAI_NNT = Value
            End Set
        End Property

        Public Property TEN_LOAI_NNT() As String
            Get
                Return strTEN_LOAI_NNT
            End Get
            Set(ByVal Value As String)
                strTEN_LOAI_NNT = Value
            End Set
        End Property

    End Class
End Namespace
