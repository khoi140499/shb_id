﻿Imports VBOracleLib
Namespace DanhMuc.DiemThu

    Public Class buDiemthu
        Private strStatus As String
        Sub New()
        End Sub
        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property
        Sub New(ByVal strTmpStatus As String)
            strStatus = strTmpStatus
        End Sub

        Public Function Save(ByVal objdiemthu As infDiemthu) As Boolean
            If strStatus = "EDIT" Then
                Return Update(objdiemthu)
            Else
                Return Insert(objdiemthu)
            End If
        End Function
        Public Function Insert(ByVal objDiemthu As infDiemthu) As Boolean
            Dim tmpLoaiThue As New daDiemthu
            Dim objda As New daDiemthu
            Dim strRtu As Boolean = False

            If checkExist(objDiemthu) = 1 Then
                MsgBox("Mã điểm thu đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End If
            If checkExist(objDiemthu) = 2 Then
                MsgBox("Tên điểm thu đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End If

            Dim sql As String
            sql = "Insert into TCS_DM_DiemThu(Ma_DThu,Ten) values ('" & objDiemthu.MaDThu & "','" & objDiemthu.TenDThu & "')"
            objda.Save(sql)
            sql = " INSERT INTO TCS_ThamSo (Ten_TS, Giatri_TS, Loai_TS, Mo_Ta, Sap_xep, Ma_DThu)" & _
            " (SELECT Ten_TS, Giatri_TS, Loai_TS, Mo_Ta, Sap_xep,'" & objDiemthu.MaDThu & "' FROM TCS_ThamSo WHERE Ma_DThu = 0)"
            objda.Save(sql)
            sql = " update TCS_ThamSo set Giatri_TS='" & objDiemthu.MaDThu & "' where  upper(Ten_TS)=upper('Ma_DT') and Ma_DThu = " & objDiemthu.MaDThu
            objda.Save(sql)

            sql = " update TCS_ThamSo set Giatri_TS='" & objDiemthu.TenDThu & "' where  upper(Ten_TS)=upper('Ten_Dthu') and Ma_DThu = " & objDiemthu.MaDThu
            objda.Save(sql)
            strRtu = True
            Return strRtu
        End Function

        Public Function Update(ByVal objLoaiThue As infDiemthu) As Boolean
            Try
                Dim tmpdiemthu As New daDiemthu
                tmpdiemthu.UpdateDiemthu(objLoaiThue)
                Return True
            Catch ex As Exception
                MsgBox("Cập nhật không thành công")
                Return False
            End Try

        End Function

        Public Function GetDataSet(ByVal WhereClause As String) As DataSet
            Dim ds As DataSet
            Try
                Dim objdiemthu As New daDiemthu
                ds = objdiemthu.DsDiemthu(WhereClause, "")
            Catch ex As Exception
                Throw ex
            End Try
            Return ds
        End Function

        'Public Sub FormatFlexHeader(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    flxDanhMuc.Cols.Remove(2)
        '    flxDanhMuc.Cols(0).Width = 100
        '    flxDanhMuc.Cols(0).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        '    flxDanhMuc.Cols(1).Width = 488
        '    flxDanhMuc.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
        '    flxDanhMuc.Rows(0).Item(0) = "Mã điểm thu"
        '    flxDanhMuc.Rows(0).Item(1) = "Tên điểm thu"
        'End Sub
        'Public Function Add() As Object
        '    Dim frmAdd As New frmDiemthu
        '    frmAdd.ShowDialog()
        '    If frmAdd.DialogResult = DialogResult.OK Then
        '        Dim objdiemthu As New infDiemthu
        '        objdiemthu.MaDThu = frmAdd.txtMadiemthu.Text
        '        objdiemthu.TenDThu = frmAdd.txtTendiemthu.Text
        '        Return objdiemthu
        '    Else
        '        Return Nothing
        '    End If
        'End Function
        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid, ByVal objLoaiThue As infDiemthu)
        '    Try
        '        flxDanhMuc.Rows.Add()
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 0) = objLoaiThue.MaDThu
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 1) = objLoaiThue.TenDThu
        '        flxDanhMuc.Select(flxDanhMuc.Rows.Count - 1, flxDanhMuc.Col)
        '        If flxDanhMuc.Cols(flxDanhMuc.Col).Sort <> C1.Win.C1FlexGrid.SortFlags.None Then flxDanhMuc.Sort(flxDanhMuc.Cols(flxDanhMuc.Col).Sort, flxDanhMuc.Col)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    Dim objdiemthu As New daDiemthu
        '    Try
        '        Dim tmpdiemthu As infDiemthu = objdiemthu.Load(flxDanhMuc.Item(flxDanhMuc.Row, 0))
        '        flxDanhMuc(flxDanhMuc.Row, 0) = tmpdiemthu.MaDThu
        '        flxDanhMuc(flxDanhMuc.Row, 1) = tmpdiemthu.TenDThu
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Function Edit(ByVal strID As String) As Boolean
        '    Try
        '        Dim frmAdd As New frmDiemthu(strID)
        '        frmAdd.ShowDialog()
        '        Return frmAdd.DialogResult = DialogResult.OK
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Shared Function checkExist(ByVal objdiemthu As infDiemthu) As Byte
            Try
                Dim objda As New daDiemthu
                Dim sqlma, sqlten As String
                sqlma = "Select 1 from TCS_DM_DiemThu where Ma_Dthu='" & objdiemthu.MaDThu & "'"
                sqlten = "Select 1 from TCS_DM_DiemThu where Ten='" & objdiemthu.TenDThu & "'"
                Return objda.ExitsDiemthu(sqlma, sqlten)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'hoapt: loadDetail Diem thu
        Public Shared Function loadDetail(ByVal objdiemthu As infDiemthu) As DataSet
            Try
                Dim objda As New daDiemthu
                Dim sqlma
                sqlma = "Select * from TCS_DM_DiemThu where Ma_Dthu='" & objdiemthu.MaDThu & "'"
                Return objda.loadDetail(sqlma)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DsDiemthu() As DataSet
            Try
                Dim objda As New daDiemthu
                Return objda.DsDiemthu("", "")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DelDiemthu(ByVal MaDT As String) As Boolean
            Try
                Dim objda As New daDiemthu
                Return objda.DelDienthu(MaDT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function Delete(ByVal strTmpMaDT As String) As Boolean
            Dim tmpdiemthu As New daDiemthu
            Try
                tmpdiemthu.DelDienthu(strTmpMaDT)
                Return True
            Catch ex As Exception
                MsgBox("Điểm thu đã có quan hệ.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End Try
        End Function

        Public Function GetReport() As String
            Return Common.mdlCommon.Report_Type.DM_DIEMTHU
        End Function
    End Class
End Namespace