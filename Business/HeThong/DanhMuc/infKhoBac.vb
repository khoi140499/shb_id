Namespace DanhMuc.KhoBac
    Public Class infKhoBac
        Private strSHKB As String
        Private strTenKB As String
        Private strTinhTrang As String
        Private strMaTinh As String
        Private strMaDBHC As String
        Private strID As String
        Private strNGUOI_TAO As String
        Private strNGUOI_DUYET As String
        Private strTYPE As String
        Private strTT_XU_LY As String

        Sub New()
            strSHKB = ""
            strTenKB = ""
            strTinhTrang = 0
            strMaTinh = ""
            strMaDBHC = ""
            strID = ""
            strNGUOI_TAO = ""
            strNGUOI_DUYET = ""
            strTYPE = ""
            strTT_XU_LY = ""
        End Sub

        Sub New(ByVal drwTaiKhoan As DataRow)
            strSHKB = drwTaiKhoan("shkb").ToString().Trim()
            strTenKB = drwTaiKhoan("TEN").ToString().Trim()
            strTinhTrang = drwTaiKhoan("TINH_TRANG").ToString().Trim()
            strMaTinh = drwTaiKhoan("ma_tinh").ToString().Trim()
            strMaDBHC = drwTaiKhoan("ma_db").ToString().Trim()
        End Sub

        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property

        Public Property TEN_KB() As String
            Get
                Return strTenKB
            End Get
            Set(ByVal Value As String)
                strTenKB = Value
            End Set
        End Property

        Public Property TINH_TRANG() As String
            Get
                Return strTinhTrang
            End Get
            Set(ByVal Value As String)
                strTinhTrang = Value
            End Set
        End Property

        Public ReadOnly Property TINHTRANG() As String
            Get
                If strTinhTrang = "0" Then
                    Return "Dang hoat dong"
                ElseIf strTinhTrang = "1" Then
                    Return "Ngung hoat dong"
                Else
                    Return ""
                End If
            End Get
        End Property
        Public Property MaTinh() As String
            Get
                Return strMaTinh
            End Get
            Set(ByVal Value As String)
                strMaTinh = Value
            End Set
        End Property
        Public Property MaDBHC() As String
            Get
                Return strMaDBHC
            End Get
            Set(ByVal Value As String)
                strMaDBHC = Value
            End Set
        End Property
        Public Property ID() As String
            Get
                Return strID
            End Get
            Set(ByVal Value As String)
                strID = Value
            End Set
        End Property
        Public Property NGUOI_TAO() As String
            Get
                Return strNGUOI_TAO
            End Get
            Set(ByVal Value As String)
                strNGUOI_TAO = Value
            End Set
        End Property
        Public Property NGUOI_DUYET() As String
            Get
                Return strNGUOI_DUYET
            End Get
            Set(ByVal Value As String)
                strNGUOI_DUYET = Value
            End Set
        End Property
        Public Property TYPE() As String
            Get
                Return strTYPE
            End Get
            Set(ByVal Value As String)
                strTYPE = Value
            End Set
        End Property

        Public Property TT_XU_LY() As String
            Get
                Return strTT_XU_LY
            End Get
            Set(ByVal Value As String)
                strTT_XU_LY = Value
            End Set
        End Property
    End Class
End Namespace
