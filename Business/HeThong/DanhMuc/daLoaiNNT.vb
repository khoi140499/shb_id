Imports VBOracleLib
Imports System.Data.OracleClient
Imports Business.Common.mdlCommon
Imports System.Text
Imports System.Data




Namespace DanhMuc.LoaiNNT
    Public Class daLoaiNNT
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
       

        Public Function Insert(ByVal objLoaiNNT As infLoaiNNT) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As OracleConnection = Nothing
            Dim transCT As IDbTransaction = Nothing
            Try
                conn = DataAccess.GetConnection() ' GetConnection()
                transCT = conn.BeginTransaction()

                'insert TCS_DM_LHINH
                Dim sbsql As StringBuilder = New StringBuilder()

                sbsql.Append("INSERT INTO TCS_DM_LOAI_NNT (MA_LOAI_NNT, ")
                sbsql.Append("TEN_LOAI_NNT)")
                sbsql.Append("VALUES( :MA_LOAI_NNT, ")
                sbsql.Append(":TEN_LOAI_NNT)")

                Dim p(1) As IDbDataParameter
                p(0) = New OracleParameter()
                p(0).ParameterName = "MA_LOAI_NNT"
                p(0).DbType = DbType.String
                p(0).Direction = ParameterDirection.Input
                p(0).Value = DBNull.Value
                p(0).Value = objLoaiNNT.MA_LOAI_NNT

                p(1) = New OracleParameter()
                p(1).ParameterName = "TEN_LOAI_NNT"
                p(1).DbType = DbType.String
                p(1).Direction = ParameterDirection.Input
                p(1).Value = DBNull.Value
                p(1).Value = objLoaiNNT.TEN_LOAI_NNT

                DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT)
                transCT.Commit()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới DM - Loại NNT")
                blnResult = False
                Throw ex
            Finally
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Or conn.State <> ConnectionState.Closed Then
                    conn.Dispose()
                    conn.Close()
                End If
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objLoaiNNT As infLoaiNNT) As Boolean
            Dim blnResult As Boolean = True
            Dim conn As OracleConnection = Nothing
            Dim transCT As IDbTransaction = Nothing
            Try
                conn = DataAccess.GetConnection()
                transCT = conn.BeginTransaction()
                'update tcs_dm_lhinh
                Dim sbsql As StringBuilder = New StringBuilder()
                sbsql.Append("UPDATE TCS_DM_LOAI_NNT SET TEN_LOAI_NNT = :TEN_LOAI_NNT")
                sbsql.Append(" WHERE MA_LOAI_NNT = :MA_LOAI_NNT ")
                Dim p(1) As IDbDataParameter
                p(0) = New OracleParameter()
                p(0).ParameterName = "TEN_LOAI_NNT"
                p(0).DbType = DbType.String
                p(0).Direction = ParameterDirection.Input
                p(0).Value = objLoaiNNT.TEN_LOAI_NNT

                p(1) = New OracleParameter()
                p(1).ParameterName = "MA_LOAI_NNT"
                p(1).DbType = DbType.String
                p(1).Direction = ParameterDirection.Input
                p(1).Value = objLoaiNNT.MA_LOAI_NNT

                DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT)
                transCT.Commit()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật DM - Loại NNT")
                blnResult = False
                Throw ex
            Finally
                If Not transCT Is Nothing Then
                    transCT.Dispose()
                End If
                If Not conn Is Nothing Or conn.State <> ConnectionState.Closed Then
                    conn.Dispose()
                    conn.Close()
                End If
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objLoaiNNT As infLoaiNNT) As Boolean
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_LOAI_NNT " & _
                    "WHERE MA_LOAI_NNT='" & objLoaiNNT.MA_LOAI_NNT & "'"
            Try
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa DM - Loại NNT")
                Throw ex
                blnResult = False
            Finally
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strMA_LOAI_NNT As String) As infLoaiNNT
            Dim objLoaiNNT As New infLoaiNNT
            Dim dtLoaiNNT As DataTable
            Dim strSQL As String = "Select * From TCS_DM_LOAI_NNT " & _
                        "Where MA_LOAI_NNT='" & strMA_LOAI_NNT & "'"
            Try
                dtLoaiNNT = DataAccess.ExecuteToTable(strSQL)
                If dtLoaiNNT.Rows.Count > 0 Then
                    objLoaiNNT = New infLoaiNNT
                    objLoaiNNT.MA_LOAI_NNT = strMA_LOAI_NNT
                    objLoaiNNT.TEN_LOAI_NNT = dtLoaiNNT.Rows(0)("TEN_LOAI_NNT").ToString()
                End If
                Return objLoaiNNT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin DM - Loại NNT")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
            End Try
            Return objLoaiNNT
        End Function
        Public Function Load(ByVal tmpLoaiNNT As infLoaiNNT) As infLoaiNNT
            Dim objLoaiNNT As New infLoaiNNT
            Dim dtLoaiNNT As New DataTable

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc
            Dim strSQL As String = "Select * From TCS_DM_LOAI_NNT " & _
                "Where " & IIf(tmpLoaiNNT.MA_LOAI_NNT <> "", "MA_LOAI_NNT = '" & tmpLoaiNNT.MA_LOAI_NNT & "'", "")

            Try
                dtLoaiNNT = DataAccess.ExecuteToTable(strSQL)
                If dtLoaiNNT.Rows.Count > 0 Then
                    objLoaiNNT = New infLoaiNNT
                    objLoaiNNT.MA_LOAI_NNT = dtLoaiNNT.Rows(0)("MA_LOAI_NNT").ToString()
                    objLoaiNNT.TEN_LOAI_NNT = dtLoaiNNT.Rows(0)("TEN_LOAI_NNT").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin DM - Loai NNT")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                Throw ex
            Finally
            End Try
            Return objLoaiNNT
        End Function
        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String
            Dim dsLoaiNNT As DataSet

            Try
                strSQL = " SELECT * " & _
                         " FROM TCS_DM_LOAI_NNT " & _
                         " WHERE 1 = 1 " & WhereClause & _
                         " ORDER BY MA_LOAI_NNT "
                dsLoaiNNT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin DM - Loại NNT")
                Throw ex
            Finally
            End Try

            Return dsLoaiNNT
        End Function
        'Public Function CheckDelete(ByVal strCCH_ID As String) As Object
        '    Dim cnKhoBac As DataAccess
        '    Dim dsKhoBac As DataSet

        '    Try
        '        Dim strSQL As String = "select ma_chuong from (select ma_chuong from tcs_ctu_dtl union all select ma_chuong from tcs_baolanh_dtl) " & _
        '                "Where ma_chuong=" & strCCH_ID

        '        cnKhoBac = New DataAccess
        '        dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception
        '        CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
        '        'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
        '        Throw ex
        '    Finally
        '        If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
        '    End Try

        '    Return dsKhoBac
        'End Function

        Public Function CheckExist(ByVal objLoaiNNT As infLoaiNNT) As Boolean
            Return Load(objLoaiNNT).MA_LOAI_NNT <> ""
        End Function

        Public Function CheckExist(ByVal strMa_Loai_NNT As String) As Boolean
            Return Load(strMa_Loai_NNT).MA_LOAI_NNT <> ""
        End Function
    End Class

End Namespace
