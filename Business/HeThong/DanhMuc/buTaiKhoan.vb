Imports VBOracleLib
Namespace DanhMuc.TaiKhoan
    Public Class buTaiKhoan

        Private strStatus As String

        Sub New()
        End Sub

        Sub New(ByVal strTmpStatus As String)
            strStatus = strTmpStatus
        End Sub

        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property

        Public Function Insert(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Dim tmpTaiKhoan As New daTaiKhoan
            If tmpTaiKhoan.CheckExist(objTaiKhoan) = False Then
                Return tmpTaiKhoan.Insert(objTaiKhoan)
            Else
                MsgBox("Số tài khoản đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End If
        End Function

        Public Function Update(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            Dim tmpTaiKhoan As New daTaiKhoan
            If tmpTaiKhoan.CheckExist(objTaiKhoan) = True Then
                Return tmpTaiKhoan.Update(objTaiKhoan)
            Else
                Return False
            End If
        End Function

        Public Function Save(ByVal objTaiKhoan As infTaiKhoan) As Boolean
            If strStatus = "EDIT" Then
                Return Update(objTaiKhoan)
            Else
                Return Insert(objTaiKhoan)
            End If
        End Function

        Public Function Delete(ByVal strTmpMaTaiKhoan As String) As Boolean
            Dim objTaiKhoan As New infTaiKhoan
            objTaiKhoan.TK = strTmpMaTaiKhoan
            Dim tmpTaiKhoan As New daTaiKhoan
            Dim kpKT As Byte = daTaiKhoan.Check2Del(objTaiKhoan)
            Dim blnResult As Boolean = True
            If kpKT = 0 Then
                blnResult = tmpTaiKhoan.Delete(objTaiKhoan)
            Else
                'MsgBox("Tài khoản đã có quan hệ.", MsgBoxStyle.Critical, "Chú ý")
                blnResult = False
            End If
            Return blnResult
        End Function

        'Public Sub FormatFlexHeader(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    flxDanhMuc.Cols(0).Visible = False
        '    flxDanhMuc.Cols(1).Width = 95
        '    flxDanhMuc.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        '    flxDanhMuc.Cols(2).Width = 200
        '    flxDanhMuc.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
        '    flxDanhMuc.Cols(3).Width = 90
        '    flxDanhMuc.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        '    flxDanhMuc.Cols(4).Width = 90
        '    flxDanhMuc.Cols(4).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        '    flxDanhMuc.Cols(5).Width = 96
        '    flxDanhMuc.Cols(5).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        '    flxDanhMuc.Rows(0).Item(1) = "Tài khoản"
        '    flxDanhMuc.Rows(0).Item(2) = "Tên tài khoản"
        '    flxDanhMuc.Rows(0).Item(3) = "Mã ĐBHC"
        '    flxDanhMuc.Rows(0).Item(4) = "Mã CQ thu"
        '    flxDanhMuc.Rows(0).Item(5) = "Tình trạng"
        'End Sub

        Public Function Load(ByVal WhereClause As String) As DataSet
            Dim objTaiKhoan As New daTaiKhoan
            Try
                Dim dsTaiKhoan As DataSet = objTaiKhoan.Load(WhereClause, "")
                Return dsTaiKhoan
            Catch ex As Exception
            Finally
            End Try
        End Function

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    Dim objTaiKhoan As New daTaiKhoan
        '    Try
        '        Dim tmpTaiKhoan As infTaiKhoan = objTaiKhoan.Load(flxDanhMuc.Item(flxDanhMuc.Row, 0))
        '        flxDanhMuc(flxDanhMuc.Row, 0) = tmpTaiKhoan.TK
        '        flxDanhMuc(flxDanhMuc.Row, 1) = Common.mdlCommon.strFormatTK(tmpTaiKhoan.TK)
        '        flxDanhMuc(flxDanhMuc.Row, 2) = tmpTaiKhoan.TEN_TK
        '        flxDanhMuc(flxDanhMuc.Row, 3) = Common.mdlCommon.strFormatDBHC(tmpTaiKhoan.MA_DBHC)
        '        flxDanhMuc(flxDanhMuc.Row, 4) = tmpTaiKhoan.MA_CQTHU
        '        flxDanhMuc(flxDanhMuc.Row, 5) = tmpTaiKhoan.TINHTRANG
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid, ByVal objTaiKhoan As infTaiKhoan)
        '    Try
        '        flxDanhMuc.Rows.Add()
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 0) = objTaiKhoan.TK
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 1) = Common.mdlCommon.strFormatTK(objTaiKhoan.TK)
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 2) = objTaiKhoan.TEN_TK
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 3) = Common.mdlCommon.strFormatDBHC(objTaiKhoan.MA_DBHC)
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 4) = objTaiKhoan.MA_CQTHU
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 5) = objTaiKhoan.TINHTRANG
        '        flxDanhMuc.Select(flxDanhMuc.Rows.Count - 1, flxDanhMuc.Col)
        '        If flxDanhMuc.Cols(flxDanhMuc.Col).Sort <> C1.Win.C1FlexGrid.SortFlags.None Then flxDanhMuc.Sort(flxDanhMuc.Cols(flxDanhMuc.Col).Sort, flxDanhMuc.Col)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Sub FormatFrm(ByRef frm As Windows.Forms.Form)
        'End Sub

        'Public Function Add() As Object
        '    Dim frmAdd As New frmTaiKhoan
        '    frmAdd.ShowDialog()
        '    If frmAdd.DialogResult = DialogResult.OK Then
        '        Dim objTaiKhoan As New infTaiKhoan
        '        objTaiKhoan.TK = frmAdd.txtTK.Text
        '        objTaiKhoan.TEN_TK = frmAdd.txtTentaikhoan.Text
        '        objTaiKhoan.MA_DBHC = frmAdd.txtMaDBHC.Text
        '        objTaiKhoan.MA_CQTHU = frmAdd.txtMa_CQThu.Text
        '        objTaiKhoan.TINH_TRANG = frmAdd.cboTinh_Trang.SelectedIndex
        '        Return objTaiKhoan
        '    Else
        '        Return Nothing
        '    End If
        'End Function

        'Public Function Edit(ByVal strID As String) As Boolean
        '    Dim frmAdd As New frmTaiKhoan(strID)
        '    frmAdd.ShowDialog()
        '    Return frmAdd.DialogResult = DialogResult.OK
        'End Function

        Public Function GetReport() As String
            Return Common.mdlCommon.Report_Type.DM_TAIKHOAN
        End Function

    End Class
End Namespace
