Imports VBOracleLib
Imports Business.Common.mdlCommon

Namespace HeThong
    Public Class buGroupUsers

        Sub New()
        End Sub

        Private daGroup As New daGroupUsers

        Private _flag As Boolean = True
        Private _status As String

        Public Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal value As String)
                _status = value
            End Set
        End Property

        Public Function Insert(ByVal objGUsers As infGroupUsers) As Boolean
            Try
                Return daGroup.Insert(objGUsers)
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Function Update(ByVal objGUsers As infGroupUsers) As Boolean
            Try
                Return daGroup.Update(objGUsers)
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Function Save(ByVal objGUsers As infGroupUsers) As Boolean
            If _status.Equals("Update") Then
                Return Update(objGUsers)
            Else
                Return Insert(objGUsers)
            End If
        End Function

        Public Function Delete(ByVal objGUsers As infGroupUsers) As Boolean
            Try
                Return daGroup.Delete(objGUsers)
            Catch ex As Exception

            End Try
        End Function

        Public Function Search(ByVal objGUsers As infGroupUsers) As DataTable
            Return daGroup.Search(objGUsers)
        End Function

        Public Function InsertTempAction(ByVal objGroup As infGroupUsers, ByVal strMa_NV_TAO As String, ByVal strAction As String) As Boolean
            Try
                Return daGroup.InsertTempAction(objGroup, strMa_NV_TAO, strAction)
            Catch ex As Exception

            End Try
        End Function

        Public Function SearchTempData(ByVal objGroup As infGroupUsers) As DataTable
            Try
                Return daGroup.SearchTempData(objGroup)
            Catch ex As Exception

            End Try
        End Function

        Public Function ApproveTempData(ByVal strAction_ID As String, ByVal strMa_NV_DUYET As String) As Boolean
            Try
                Return daGroup.ApproveTempData(strAction_ID, strMa_NV_DUYET)
            Catch ex As Exception

            End Try
        End Function
    End Class
End Namespace
