Imports VBOracleLib
Namespace HeThong

    Public Class buUsers
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Shared Function check_MaCN(ByVal ma_cn As String) As Boolean
            Dim objUsers As New daUser
            Dim result As Boolean
            Try
                result = objUsers.check_MaCN(ma_cn)
                Return result
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_LST_DM_NHANVIEN() As DataSet
            Dim objUsers As New daUser
            Dim ds As New DataSet
            Try
                Return objUsers.TCS_LST_DM_NHANVIEN()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_LST_DM_NHANVIEN_DIEMTHU(Optional ByVal infUsers As infUser = Nothing) As DataSet
            Dim objUsers As New daUser
            Dim ds As New DataSet
            Try
                If Not infUsers Is Nothing Then
                    Return objUsers.TCS_LST_DM_NHANVIEN_DIEMTHU(infUsers)
                Else
                    Return objUsers.TCS_LST_DM_NHANVIEN_DIEMTHU()
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function NHANVIEN_DETAIL(ByVal ma_NV As String) As infUser
            Dim objUsers As New daUser
            Dim ds As New DataSet
            Dim objNV As New infUser
            Try
                ds = objUsers.NHANVIEN_DETAIL(ma_NV)
                If Not ds Is Nothing Then
                    objNV.Ma_NV = ds.Tables(0).Rows(0)("MA_NV").ToString
                    objNV.Ten = ds.Tables(0).Rows(0)("TEN").ToString
                    objNV.Chuc_Danh = ds.Tables(0).Rows(0)("CHUC_DANH").ToString
                    objNV.Ma_Nhom = ds.Tables(0).Rows(0)("MA_NHOM").ToString
                    objNV.Ban_Lv = ds.Tables(0).Rows(0)("BAN_LV").ToString
                    objNV.TEN_ND = ds.Tables(0).Rows(0)("TEN_DN").ToString
                    objNV.Mat_Khau = ds.Tables(0).Rows(0)("MAT_KHAU").ToString
                    objNV.Teller_Id = ds.Tables(0).Rows(0)("TELLER_ID").ToString
                    objNV.MaCN = ds.Tables(0).Rows(0)("Ma_CN").ToString
                    objNV.Trang_thai = ds.Tables(0).Rows(0)("TINH_TRANG").ToString
                    objNV.LDAP = ds.Tables(0).Rows(0)("LDAP").ToString
                    objNV.HanMuc_Tu = ds.Tables(0).Rows(0)("hanmuc_tu").ToString
                    objNV.HanMuc_Den = ds.Tables(0).Rows(0)("hanmuc_den").ToString
                    objNV.User_Domain = ds.Tables(0).Rows(0)("UserDomain").ToString
                    objNV.MA_NV_SUA = ds.Tables(0).Rows(0)("ma_nv_sua").ToString
                    If ds.Tables(0).Rows(0)("LAN_DN_SAI").ToString <> "" Then
                        objNV.Lan_DN_SAI = Integer.Parse(ds.Tables(0).Rows(0)("LAN_DN_SAI").ToString)
                    Else
                        objNV.Lan_DN_SAI = 0
                    End If

                    Return objNV
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TIMKIEM_NHANVIEN(ByVal ma_NV As String) As DataSet
            Dim objUsers As New daUser
            Dim ds As New DataSet
            Try
                ds = objUsers.TIMKIEM_NHOMNV(ma_NV)
                If Not ds Is Nothing Then
                    Return ds
                End If
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function
        Public Shared Function TKNHANVIEN_MADN(Optional ByVal ma_NV As String = "", Optional ByVal ma_CN As String = "") As DataSet
            Dim objUsers As New daUser
            Dim ds As New DataSet
            Try
                ds = objUsers.TIMKIEM_NHANVIEN(ma_NV, ma_CN)
                If Not ds Is Nothing Then
                    Return ds
                End If
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function
        Public Shared Function UpdateTTLogin(ByVal ma_NV As String, ByVal strTrangThai As String, Optional ByVal strIPLocal As String = "") As Boolean
            Dim objUsers As New daUser
            Dim blnResult As Boolean
            Try
                blnResult = objUsers.UpdateTTLogin(ma_NV, strTrangThai, strIPLocal)
                Return blnResult
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function TCS_LST_DM_NHANVIEN_TEMP(Optional ByRef objUser As infUser = Nothing) As DataTable
            Dim objUsers As New daUser
            Dim blnResult As DataTable
            Try
                blnResult = objUsers.TCS_LST_DM_NHANVIEN_TEMP(objUser)
            Catch ex As Exception

            End Try
            Return blnResult
        End Function

        Public Shared Function ApproveUserTemp(ByVal strAction_ID As String, ByVal strMa_NV_DUYET As String) As Boolean
            Dim objUsers As New daUser
            Dim result As Boolean
            Try
                result = objUsers.ApproveUserTemp(strAction_ID, strMa_NV_DUYET)
            Catch ex As Exception

            End Try
            Return result
        End Function

        Public Shared Function Insert_Log_DN(Optional ByVal strMA_NV As String = "", Optional ByVal strTEN_DN As String = "", Optional ByVal strLOCAL_IP As String = "", Optional ByVal strSTATUS As String = "", Optional ByVal strMA_CN As String = "", Optional ByVal strUSER_KICK As String = "") As Boolean
            Dim objUsers As New daUser
            Dim blnResult As Boolean
            Try
                blnResult = objUsers.Insert_Log_DN(strMA_NV, strTEN_DN, strLOCAL_IP, strSTATUS, strMA_CN, strUSER_KICK)
                Return blnResult
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function Update_WrongLoginAcount(ByVal MA_NV As String, ByVal wrongLoginCount As String)
            Dim objUsers As New daUser
            Try
                objUsers.Update_WrongLogInCount(MA_NV, wrongLoginCount)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
            End Try
        End Function
        Public Shared Function UpdateTinhTrang(ByVal strMa_NV As String, ByVal strTinh_Trang As String)
            Dim objUsers As New daUser
            Try
                objUsers.UpdateTinhTrang(strMa_NV, strTinh_Trang)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace