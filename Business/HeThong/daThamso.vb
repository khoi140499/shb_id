﻿Imports VBOracleLib
Namespace HeThong
    Public Class daThamso
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="sqlstr"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Thamso(ByVal sqlstr As String) As DataSet
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Return conn.ExecuteReturnDataSet(sqlstr, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi truy vấn tham số dùng chung")
                'LogDebug.WriteLog("Lỗi khi truy vấn tham số dùng chung " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="sql1"></param>
        ''' <param name="sql2"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function UpdateThamso(ByVal sql1 As String, ByVal sql2 As String) As Boolean
            Dim conn As DataAccess
            Dim trans As IDbTransaction
            Try
                conn = New DataAccess
                trans = conn.BeginTransaction
                conn.ExecuteNonQuery(sql1, CommandType.Text, trans)
                If Not "".Equals(sql2) And (Not sql2 Is Nothing) Then
                    conn.ExecuteNonQuery(sql2, CommandType.Text, trans)
                End If
                trans.Commit()
            Catch ex As Exception
                trans.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi khi cập nhật tham số dùng chung")
                'LogDebug.WriteLog("Lỗi khi cập nhật tham số dùng chung " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not trans Is Nothing Then
                    trans.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        ''' ***************BEGIN****************************************'
        ''' ---------------CREATE--------------------------
        '''  Tên:			Updates the thamso_ DC.
        '''  Người viết:	ANHLD
        '''  Ngày viết:		10/02/2012 00:00:00
        '''  Mục đích:	    Cap nhat tham so khi doi chieu tu dong
        ''' ---------------MODIFY--------------------------
        '''  Người sửa:	
        '''  Ngày sửa: 	
        '''  Mục đích:	
        ''' ---------------PARAM--------------------------
        ''' <param name="sql">The SQL.</param>
        ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
        Public Function UpdateThamso_DC(ByVal sql As String) As Boolean
            Dim conn As DataAccess
            Dim trans As IDbTransaction
            Try
                conn = New DataAccess
                trans = conn.BeginTransaction
                conn.ExecuteNonQuery(sql, CommandType.Text, trans)

                trans.Commit()
            Catch ex As Exception
                trans.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi cập nhật tham số dùng chung")
                'LogDebug.WriteLog("Lỗi khi cập nhật tham số dùng chung " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not trans Is Nothing Then
                    trans.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        ''' ***************BEGIN****************************************'
        ''' ---------------CREATE--------------------------
        '''  Tên:			Insert_s the log_ DC.
        '''  Người viết:	ANHLD
        '''  Ngày viết:		10/02/2012 00:00:00
        '''  Mục đích:	    Insert log khi thuc hien doi chieu tu dong
        ''' ---------------MODIFY--------------------------
        '''  Người sửa:	
        '''  Ngày sửa: 	
        '''  Mục đích:	
        ''' ---------------PARAM--------------------------
        ''' <param name="sql">The SQL.</param>
        ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
        Public Function Insert_Log_DC(ByVal sql As String) As Boolean
            Dim conn As DataAccess
            Dim trans As IDbTransaction
            Try
                conn = New DataAccess
                trans = conn.BeginTransaction
                conn.ExecuteNonQuery(sql, CommandType.Text, trans)

                trans.Commit()
            Catch ex As Exception
                trans.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khi cập nhật tham số dùng chung")
                'LogDebug.WriteLog("Lỗi khi cập nhật tham số dùng chung " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not trans Is Nothing Then
                    trans.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function

        ''' <summary>
        ''' Kiểm tra tham số hệ thống đã tồn tại hay chưa
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ThamSoHT_CheckExist(ByVal strTen_ThamSo As String) As Boolean
            Dim conn As DataAccess
            Dim result As Boolean = False
            Try
                conn = New DataAccess
                Dim strSql = "SELECT * FROM TCS_THAMSO_HT WHERE TEN_TS='" & strTen_ThamSo & "'"
                Dim dt As DataTable = conn.ExecuteToTable(strSql)

                If dt.Rows.Count > 0 Then
                    result = True
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return result
        End Function

        ''' <summary>
        ''' Thêm tham số hệ thống mới
        ''' </summary>
        ''' <param name="strTen_TS"></param>
        ''' <param name="strGiaTri_TS"></param>
        ''' <param name="strLoai_TS"></param>
        ''' <param name="strMo_Ta"></param>
        ''' <param name="strXap_Xep"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ThamSoHT_Insert(ByVal strTen_TS As String, ByVal strGiaTri_TS As String, ByVal strLoai_TS As String, ByVal strMo_Ta As String, ByVal strXap_Xep As String) As Boolean
            Dim conn As DataAccess
            Dim result As Boolean = True
            Try
                conn = New DataAccess
                Dim strSql = "INSERT INTO TCS_THAMSO_HT (TEN_TS,GIATRI_TS,SAP_XEP,MO_TA,LOAI_TS)" & _
                " VALUES('" & strTen_TS & "','" & strGiaTri_TS & "','" & strXap_Xep & "','" & strMo_Ta & "','" & _
                strLoai_TS & "')"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                result = False
            End Try
            Return result
        End Function

        ''' <summary>
        ''' Cập nhật giá trị tham số
        ''' </summary>
        ''' <param name="strTen_TS"></param>
        ''' <param name="strGia_Tri_TS"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ThamSoHT_UpdateValue(ByVal strTen_TS As String, ByVal strGia_Tri_TS As String) As Boolean
            Dim conn As DataAccess
            Dim result As Boolean = True
            Try
                conn = New DataAccess
                Dim strSql = "UPDATE TCS_THAMSO_HT SET GIATRI_TS='" & strGia_Tri_TS & "' WHERE TEN_TS='" & strTen_TS & "'"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                result = False
            End Try
            Return result
        End Function

        ''' <summary>
        ''' Lấy giá trị 
        ''' </summary>
        ''' <param name="strTenTS"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ThamSoHT_GetValue(ByVal strTenTS As String) As String
            Dim result As String = ""
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSql = "SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='" & strTenTS & "'"
                Dim dtTS As DataTable = DataAccess.ExecuteToTable(strSql)

                If dtTS.Rows.Count > 0 Then
                    result = dtTS.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
            End Try
            Return result
        End Function

        ''' <summary>
        ''' Xóa tham số hệ thống
        ''' </summary>
        ''' <param name="strTen_TS"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ThamSoHT_Delete(ByVal strTen_TS As String) As Boolean
            Dim result As Boolean = True
            Dim conn As DataAccess
            Try
                conn = New DataAccess
                Dim strSql = "DELETE TCS_THAMSO_HT WHERE TEN_TS='" & strTen_TS & "'"
                conn.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                result = False
            End Try
            Return result
        End Function
    End Class
End Namespace