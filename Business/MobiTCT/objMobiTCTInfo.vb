﻿Imports System.Text
Imports System.Collections
Namespace MobiTCT
    Public Class objMobiTCTInfo
        Public ID As String = ""
        Public REQ_TIME As String = ""
        Public RES_TIME As String = ""
        Public REQ_TYPE As String = ""
        Public ERRCODE As String = ""
        Public ERRMSG As String = ""
        Public REQ_MSG As String = ""
        Public RES_MSG As String = ""
        Public MST As String = ""
        Public Data As String = ""


    End Class
    Public Class TCTObjectIn
        Public Property duLieu As String
        Public signature As Signature
        Public Shared Function Base64Decode(ByVal base64EncodedData As String) As String
            Dim base64EncodedBytes = Convert.FromBase64String(base64EncodedData)
            Return Encoding.UTF8.GetString(base64EncodedBytes)
        End Function
    End Class
    Public Class Signature
        Public Property value As String
        Public Property certificates As String
    End Class
    Public Class objDSTAIKHOANLIENKETInfo
        Public ID As String = ""
        Public MADOITAC As String = ""
        Public LOAI As String = ""
        Public MST As String = ""
        Public TEN_NNT As String = ""
        Public LOAIGIAYTO As String = ""
        Public SOGIAYTO As String = ""
        Public DIENTHOAI As String = ""
        Public PHUONGTHUC As String = ""
        Public SOTAIKHOAN_THE As String = ""
        Public NGAYPHATHANH As String = ""
        Public NGAYHT As String = ""
        Public TRANGTHAI As String = ""
        Public LOAIOTP As String = ""
        Public MAGIAODICH As String = ""
        Public NGAYMO As String = ""
        Public TTHAI_TCT As String = ""
        Public IDREF As String = ""
        Public MOTA As String = ""
        Public GHICHU As String = ""
        Public EMAIL As String = ""

    End Class
    Public Class objMobiTCTCTUInfo
        Public MADOITAC As String = ""
        Public MATHAMCHIEU As String = ""
        Public SOTIEN As String = ""
        Public PHICHUYENPHAT As String = ""
        Public NGONNGU As String = ""
        Public MATIENTE As String = ""
        Public THONGTINGIAODICH As String = ""
        Public MADICHVU As String = ""
        Public MAHOSO As String = ""
        Public TKTHUHUONG As String = ""
        Public MALOAIHINHTHUPHAT As String = ""
        Public MALOAIHINHTHUE As String = ""
        Public TENLOAIHINHTHUE As String = ""
        Public MACQTHU As String = ""
        Public TENCQTHU As String = ""
        Public SHKB As String = ""
        Public TEN_KB As String = ""
        Public MACOQUANQD As String = ""
        Public TENCOQUANQD As String = ""
        Public SOQD As String = ""
        Public NGAYQD As String = ""
        Public MST As String = ""
        Public HOTENNGUOINOP As String = ""
        Public SOCMNDNGUOINOP As String = ""
        Public DIACHINGUOINOP As String = ""
        Public HUYENNGUOINOP As String = ""
        Public TINHNGUOINOP As String = ""
        Public DSKHOANNOP As String = ""
        Public MSGCONTENT As String = ""
        Public SO_CT As String = ""
        Public TRANGTHAI As String = ""
        Public NGAY_HT As String = ""
        Public NGAYMSG As String = ""
        Public SOKHUNG As String = ""
        Public SOMAY As String = ""
        Public MADBHC As String = ""
        Public THOIGIANGD As String = ""
        Public MANGANHANG As String = ""
        Public PHUONGTHUC As String = ""
        Public SOTAIKHOAN_THE As String = ""
        Public SOTAIKHOAN_THE_DESCRYPT As String = ""
        Public EWALLETTOKEN As String = ""
        Public SOTAIKHOAN As String = ""
        Public IDTAIKHOANLIENKET As String = ""
        Public LOAIOTP As String = ""
        Public TRANIDOTP As String = ""
        Public DIENTHOAI As String = ""
        Public TTHAI_TCT As String = ""
        Public MOTA As String = ""
        Public GHICHU As String = ""
        Public MA_NH_A As String = ""
        Public TEN_NH_A As String = ""
        Public MA_NH_B As String = ""
        Public TEN_NH_B As String = ""
        Public MA_NNTIEN As String = ""
        Public TEN_NNTIEN As String = ""
        Public DTL As List(Of objCTUDtl) = New List(Of objCTUDtl)
    End Class
    Public Class objCTUDtl
        Public LOAIPHILEPHI As String = ""
        Public MAPHILEPHI As String = ""
        Public TENPHILEPHI As String = ""
        Public SOTIEN As String = ""
        Public MACHUONG As String = ""
        Public MATMUC As String = ""

    End Class
    Public Class objResponse
        Public TRANGTHAI As String = ""
        Public MOTA As String = ""
    End Class
End Namespace


