﻿Imports VBOracleLib
Namespace MobiTCT
    Public Class MobiTCTLienKetTK
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function fnc_tracuuTaiKhoanLienKet(ByVal strWhere As String) As DataSet
            Try
                Dim strSQL As String = " SELECT A.ID, A.MADOITAC, A.LOAI, A.MST, A.TEN_NNT, A.LOAIGIAYTO,"
                strSQL &= " A.SOGIAYTO, A.DIENTHOAI, A.PHUONGTHUC, A.SOTAIKHOAN_THE,"
                strSQL &= " A.NGAYPHATHANH,to_char(A.NGAYHT,'DD/MM/YYYY HH24:MI:SS') NGAYHT, A.TRANGTHAI, "
                strSQL &= " A.LOAIOTP, A.MAGIAODICH, to_char(A.NGAYMO,'DD/MM/YYYY HH24:MI:SS') NGAYMO, A.TTHAI_TCT, A.IDREF,B.MOTA,A.GHICHU,A.EMAIL "
                strSQL &= " FROM SET_DSTK_LIENKET_LOG A,TCS_DM_TRANGTHAI B  WHERE A.TRANGTHAI = B.TRANGTHAI AND B.LOAI='DSTKLIENKET' "

                If strWhere.Length > 0 Then
                    strSQL &= strWhere
                End If
                strSQL &= " ORDER BY A.NGAYMO DESC "
                strSQL = "SELECT ROWNUM STT, A.* FROM (" & strSQL & ") A"
                log.Info("4.1. End frmTracuuLienKetTKMobiTCT - btnTimKiem_Click - load_dataGrid - fnc_tracuuTaiKhoanLienKet: " & strSQL)
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Info("Error. frmTracuuLienKetTKMobiTCT - btnTimKiem_Click - load_dataGrid - fnc_tracuuTaiKhoanLienKet loi: " & ex.Message & "-" & ex.StackTrace)
                log.Error(ex.Message & "-" & ex.StackTrace)
            End Try
            Return Nothing
        End Function
        Public Function MOLIENKETTK_06(ByVal strID As String, ByVal strGhiChu As String) As String
            Dim _conn As IDbConnection = Nothing ' DataAccess.GetConnectionNo();
            Dim _tran As IDbTransaction = Nothing
            Try
                _conn = DataAccess.GetConnectionNo()
                _tran = _conn.BeginTransaction()
                'cap nhat trang thai cho tct cap nhat trang thai
                Dim strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Trangthai='06',TTHAI_TCT='999',NGAYHT=SYSDATE,GHICHU=:GHICHU WHERE ID='" & strID & "'"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("GHICHU", ParameterDirection.Input, strGhiChu, DbType.String))
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, ls.ToArray(), _tran)
                'xoa neu da ton tai dang ky trong bang lien ket
                DataAccess.ExecuteNonQuery("DELETE FROM SET_DSTK_LIENKET WHERE ID='" & strID & "' ", CommandType.Text, _tran)
                ' DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, _tran)
                strSQL = "insert into SET_DSTK_LIENKET(id, madoitac, loai, mst, ten_nnt, loaigiayto, "
                strSQL += " sogiayto, dienthoai, phuongthuc, sotaikhoan_the, "
                strSQL += " ngayphathanh, ngayht, trangthai, loaiotp, magiaodich,       ngaymo) "
                strSQL += " SELECT id, madoitac, loai, mst, ten_nnt, loaigiayto, "
                strSQL += " sogiayto, dienthoai, phuongthuc, sotaikhoan_the, "
                strSQL += " ngayphathanh, ngayht, trangthai, loaiotp, magiaodich,       ngaymo "
                strSQL += " FROM set_dstk_lienket_log a where id='" & strID & "' "
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, _tran)
                _tran.Commit()
                Return "00"
            Catch ex As Exception
                log.Info(" MOLIENKETTK_06 Loi ID[" & strID & "]")
                log.Error(ex.Message & "-" & ex.StackTrace)
                If _tran IsNot Nothing Then _tran.Rollback()
            Finally
                If _tran IsNot Nothing Then
                    _tran.Dispose()
                End If
                If _conn IsNot Nothing Then
                    _conn.Close()
                    _conn.Dispose()
                End If
            End Try
            Return "06"
        End Function
        Public Function MOLIENKETTK_07(ByVal strID As String, ByVal strIDREF As String, ByVal strGhiChu As String) As String
            Dim _conn As IDbConnection = Nothing ' DataAccess.GetConnectionNo();
            Dim _tran As IDbTransaction = Nothing
            Try
                _conn = DataAccess.GetConnectionNo()
                _tran = _conn.BeginTransaction()
                'cap nhat trang thai cho tct cap nhat trang thai
                Dim strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Trangthai='07',TTHAI_TCT='999',NGAYHT=SYSDATE,GHICHU=:GHICHU WHERE ID='" & strID & "'"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("GHICHU", ParameterDirection.Input, strGhiChu, DbType.String))
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, ls.ToArray(), _tran)
                'xoa neu da ton tai dang ky trong bang lien ket
                DataAccess.ExecuteNonQuery("DELETE FROM SET_DSTK_LIENKET WHERE ID in ('" & strIDREF & "','" & strID & "') ", CommandType.Text, _tran)
                strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Trangthai='07',NGAYHT=SYSDATE WHERE ID='" & strIDREF & "'"
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, _tran)
                _tran.Commit()
                Return "00"
            Catch ex As Exception
                log.Info(" MOLIENKETTK_07 Loi ID[" & strID & "]")
                log.Error(ex.Message & "-" & ex.StackTrace)
                If _tran IsNot Nothing Then _tran.Rollback()
            Finally
                If _tran IsNot Nothing Then
                    _tran.Dispose()
                End If
                If _conn IsNot Nothing Then
                    _conn.Close()
                    _conn.Dispose()
                End If
            End Try
            Return "06"
        End Function
        Public Function MOLIENKETTK_04(ByVal strID As String, ByVal strGhiChu As String) As String
            Dim _conn As IDbConnection = Nothing ' DataAccess.GetConnectionNo();
            Dim _tran As IDbTransaction = Nothing
            Try
                _conn = DataAccess.GetConnectionNo()
                _tran = _conn.BeginTransaction()
                'cap nhat trang thai cho tct cap nhat trang thai
                Dim strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Trangthai='04',NGAYHT=SYSDATE,GHICHU=:GHICHU WHERE ID='" & strID & "'"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("GHICHU", ParameterDirection.Input, strGhiChu, DbType.String))
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, ls.ToArray(), _tran)
                'xoa neu da ton tai dang ky trong bang lien ket
                DataAccess.ExecuteNonQuery("DELETE FROM SET_DSTK_LIENKET WHERE ID in ('" & strID & "') ", CommandType.Text, _tran)
                _tran.Commit()
                Return "00"
            Catch ex As Exception
                log.Info(" MOLIENKETTK_04 Loi ID[" & strID & "]")
                log.Error(ex.Message & "-" & ex.StackTrace)
                If _tran IsNot Nothing Then _tran.Rollback()
            Finally
                If _tran IsNot Nothing Then
                    _tran.Dispose()
                End If
                If _conn IsNot Nothing Then
                    _conn.Close()
                    _conn.Dispose()
                End If
            End Try
            Return "06"
        End Function
        Public Function MOLIENKETTK_03(ByVal strID As String, ByVal strGhiChu As String) As String
            Dim _conn As IDbConnection = Nothing ' DataAccess.GetConnectionNo();
            Dim _tran As IDbTransaction = Nothing
            Try
                _conn = DataAccess.GetConnectionNo()
                _tran = _conn.BeginTransaction()
                'cap nhat trang thai cho tct cap nhat trang thai
                Dim strSQL = "UPDATE SET_DSTK_LIENKET_LOG SET Trangthai='03',NGAYHT=SYSDATE,GHICHU=:GHICHU WHERE ID='" & strID & "'"
                Dim ls As List(Of IDbDataParameter) = New List(Of IDbDataParameter)
                ls.Add(DataAccess.NewDBParameter("GHICHU", ParameterDirection.Input, strGhiChu, DbType.String))
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, ls.ToArray(), _tran)
                'xoa neu da ton tai dang ky trong bang lien ket
                DataAccess.ExecuteNonQuery("DELETE FROM SET_DSTK_LIENKET WHERE ID in ('" & strID & "') ", CommandType.Text, _tran)
                _tran.Commit()
                Return "00"
            Catch ex As Exception
                log.Info(" MOLIENKETTK_04 Loi ID[" & strID & "]")
                log.Error(ex.Message & "-" & ex.StackTrace)
                If _tran IsNot Nothing Then _tran.Rollback()
            Finally
                If _tran IsNot Nothing Then
                    _tran.Dispose()
                End If
                If _conn IsNot Nothing Then
                    _conn.Close()
                    _conn.Dispose()
                End If
            End Try
            Return "06"
        End Function
     

    End Class
End Namespace


