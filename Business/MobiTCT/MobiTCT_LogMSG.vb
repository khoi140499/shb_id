﻿Imports VBOracleLib
Namespace MobiTCT
    Public Class MobiTCT_LogMSG
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function fnc_tracuuLog(ByVal strWhere As String, Optional ByVal strOrder As String = "") As DataSet
            Try
                Dim strSQL As String = "SELECT A.MST,A.ID,TO_CHAR(A.REQ_TIME,'DD/MM/YYYY HH24:MI:SS') REQ_TIME,TO_CHAR(A.RES_TIME,'DD/MM/YYYY HH24:MI:SS') RES_TIME,A.REQ_TYPE,A.ERRCODE,A.ERRMSG FROM SET_LOG_API_TCT_MOBI A  WHERE 1=1 "

                If strWhere.Length > 0 Then
                    strSQL &= strWhere
                End If
                If strOrder.Length > 0 Then
                    strSQL &= "  " & strOrder
                End If
                strSQL = "SELECT ROWNUM STT, A.* FROM (" & strSQL & ") A"
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
            End Try
            Return Nothing
        End Function
        Public Function fnc_GetLogRequest(ByVal pv_strID As String) As DataSet
            Try
                Dim strSQL As String = "SELECT A.ID,TO_CHAR(A.REQ_TIME,'DD/MM/YYYY HH24:MI:SS') REQ_TIME,TO_CHAR(A.RES_TIME,'DD/MM/YYYY HH24:MI:SS') RES_TIME"
                strSQL &= ",A.REQ_TYPE,A.ERRCODE,A.ERRMSG,A.REQ_MSG,A.RES_MSG,MST FROM SET_LOG_API_TCT_MOBI A  WHERE ID='" & pv_strID & "' "

               
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
            End Try
            Return Nothing
            Return Nothing
        End Function
    End Class
End Namespace

