﻿Imports VBOracleLib
Imports Business.Common.mdlCommon
Namespace MobiTCT
    Public Class MobiTCTCTU
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function fnc_tracuuCTU(ByVal strWhere As String) As DataSet
            Try
                'Dim strSQL As String = " SELECT A.MADOITAC, A.MATHAMCHIEU, A.SOTIEN, A.PHICHUYENPHAT, A.NGONNGU,"
                'strSQL &= " A.MATIENTE, A.THONGTINGIAODICH, A.MADICHVU, A.MAHOSO,"
                'strSQL &= " A.TKTHUHUONG, A.MALOAIHINHTHUPHAT, A.MALOAIHINHTHUE,"
                'strSQL &= " A.TENLOAIHINHTHUE, A.MACQTHU, A.TENCQTHU, A.SHKB, A.MACOQUANQD,"
                'strSQL &= " A.TENCOQUANQD, A.SOQD,to_char( A.NGAYQD,'DD/MM/YYYY') NGAYQD, A.MST, A.HOTENNGUOINOP,"
                'strSQL &= " A.SOCMNDNGUOINOP, A.DIACHINGUOINOP, A.HUYENNGUOINOP,NVL(A.TEN_KB,(SELECT MAX(KB.TEN) FROM TCS_DM_KHOBAC KB WHERE KB.SHKB=A.SHKB)) TEN_KB,"
                'strSQL &= " A.TINHNGUOINOP, A.DSKHOANNOP, A.MSGCONTENT, A.SO_CT, A.TRANGTHAI,"
                'strSQL &= " to_char( A.NGAY_HT,'DD/MM/YYYY HH24:MI:SS') NGAY_HT,to_char( A.NGAYMSG,'DD/MM/YYYY HH24:MI:SS') NGAYMSG, A.SOKHUNG, A.SOMAY, A.MADBHC,"
                'strSQL &= " to_char( A.THOIGIANGD,'DD/MM/YYYY HH24:MI:SS') THOIGIANGD,"
                'strSQL &= " A.MANGANHANG, A.PHUONGTHUC, A.SOTAIKHOAN_THE,A.SOTAIKHOAN_THE_DECRYPT, A.EWALLETTOKEN,"
                'strSQL &= " A.SOTAIKHOAN, A.IDTAIKHOANLIENKET, A.LOAIOTP, A.TRANIDOTP,"
                'strSQL &= " A.DIENTHOAI, A.TTHAI_TCT, b.MOTA, A.GHICHU,CASE WHEN c.TT_SP in ('1','2') THEN C.ref_core_ttsp ELSE C.so_ct_nh END as ref_core_ttsp1 , (A.MA_NH_B ||' - '|| A.TEN_NH_B) NH_HUONG, C.dac_diem_ptien  "
                'strSQL &= " FROM SET_CTU_HDR_MOBITCT A,tcs_ctu_hdr_mobitct C, tcs_dm_trangthai b where A.SO_CT= C.SO_CT(+) AND a.trangthai = b.trangthai(+) and b.loai='CTUMOBITCT' "

                Dim strSQL As String = " SELECT A.MADOITAC, A.MATHAMCHIEU, A.SOTIEN, A.PHICHUYENPHAT, A.NGONNGU,"
                strSQL &= " A.MATIENTE, A.THONGTINGIAODICH, A.MADICHVU, A.MAHOSO,"
                strSQL &= " A.TKTHUHUONG, A.MALOAIHINHTHUPHAT, A.MALOAIHINHTHUE,"
                strSQL &= " A.TENLOAIHINHTHUE, A.MACQTHU, A.TENCQTHU, A.SHKB, A.MACOQUANQD,"
                strSQL &= " A.TENCOQUANQD, A.SOQD,to_char( A.NGAYQD,'DD/MM/YYYY') NGAYQD, A.MST, A.HOTENNGUOINOP,"
                strSQL &= " A.SOCMNDNGUOINOP, A.DIACHINGUOINOP, A.HUYENNGUOINOP,NVL(A.TEN_KB,(SELECT MAX(KB.TEN) FROM TCS_DM_KHOBAC KB WHERE KB.SHKB=A.SHKB)) TEN_KB,"
                strSQL &= " A.TINHNGUOINOP, A.DSKHOANNOP, A.MSGCONTENT, A.SO_CT, A.TRANGTHAI,"
                strSQL &= " to_char( A.NGAY_HT,'DD/MM/YYYY HH24:MI:SS') NGAY_HT,to_char( A.NGAYMSG,'DD/MM/YYYY HH24:MI:SS') NGAYMSG, A.SOKHUNG, A.SOMAY, A.MADBHC,"
                strSQL &= " to_char( A.THOIGIANGD,'DD/MM/YYYY HH24:MI:SS') THOIGIANGD,"
                strSQL &= " A.MANGANHANG, A.PHUONGTHUC, A.SOTAIKHOAN_THE,A.SOTAIKHOAN_THE_DECRYPT, A.EWALLETTOKEN,"
                strSQL &= " A.SOTAIKHOAN, A.IDTAIKHOANLIENKET, A.LOAIOTP, A.TRANIDOTP,"
                strSQL &= " A.DIENTHOAI, A.TTHAI_TCT, b.MOTA, A.GHICHU, C.so_ct_nh, (A.MA_NH_B ||' - '|| A.TEN_NH_B) NH_HUONG, C.dac_diem_ptien  "
                strSQL &= " FROM SET_CTU_HDR_MOBITCT A,tcs_ctu_hdr_mobitct C, tcs_dm_trangthai b where A.SO_CT= C.SO_CT(+) AND a.trangthai = b.trangthai(+) and b.loai='CTUMOBITCT' "

                If strWhere.Length > 0 Then
                    strSQL &= strWhere
                End If
                strSQL &= " ORDER BY A.NGAYMSG DESC "
                strSQL = "SELECT ROWNUM STT, A.* FROM (" & strSQL & ") A"
                'strSQL = "select * from (SELECT ROWNUM STT, A.* FROM (" & strSQL & ") A)"
                'If strWhere1.Length > 0 Then
                '    strSQL &= strWhere1
                'End If
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
            End Try
            Return Nothing
        End Function
        Public Function fnc_tracuuCTUDetail(ByVal strMathamchieu As String) As objMobiTCTCTUInfo
            Try
                'lay thong tin HDR
                Dim strSQL As String = " SELECT A.MADOITAC, A.MATHAMCHIEU, A.SOTIEN, A.PHICHUYENPHAT, A.NGONNGU,"
                strSQL &= " A.MATIENTE, A.THONGTINGIAODICH, A.MADICHVU, A.MAHOSO,"
                strSQL &= " A.TKTHUHUONG, A.MALOAIHINHTHUPHAT, A.MALOAIHINHTHUE,"
                strSQL &= " A.TENLOAIHINHTHUE, A.MACQTHU, A.TENCQTHU, A.SHKB, A.MACOQUANQD,"
                strSQL &= " A.TENCOQUANQD, A.SOQD,to_char( A.NGAYQD,'DD/MM/YYYY') NGAYQD, A.MST, A.HOTENNGUOINOP,"
                strSQL &= " A.SOCMNDNGUOINOP, A.DIACHINGUOINOP, A.HUYENNGUOINOP,NVL(A.TEN_KB,(SELECT MAX(KB.TEN) FROM TCS_DM_KHOBAC KB WHERE KB.SHKB=A.SHKB)) TEN_KB,"
                strSQL &= " A.TINHNGUOINOP, A.DSKHOANNOP, A.MSGCONTENT, A.SO_CT, A.TRANGTHAI,"
                strSQL &= " to_char( A.NGAY_HT,'DD/MM/YYYY HH24:MI:SS') NGAY_HT,to_char( A.NGAYMSG,'DD/MM/YYYY HH24:MI:SS') NGAYMSG, A.SOKHUNG, A.SOMAY, A.MADBHC,"
                strSQL &= " to_char( A.THOIGIANGD,'DD/MM/YYYY HH24:MI:SS') THOIGIANGD,"
                strSQL &= " A.MANGANHANG, A.PHUONGTHUC, A.SOTAIKHOAN_THE, A.EWALLETTOKEN, A.SOTAIKHOAN_THE_DECRYPT, "
                strSQL &= " A.SOTAIKHOAN, A.IDTAIKHOANLIENKET, A.LOAIOTP, A.TRANIDOTP, "
                strSQL &= " A.DIENTHOAI, A.TTHAI_TCT, b.MOTA, A.GHICHU, MA_NH_A,TEN_NH_A,MA_NH_B,TEN_NH_B,A.MA_NNTIEN,A.TEN_NNTIEN "
                strSQL &= " FROM SET_CTU_HDR_MOBITCT A, tcs_dm_trangthai b where a.trangthai = b.trangthai and b.loai='CTUMOBITCT' and a.MATHAMCHIEU='" + strMathamchieu + "' "
                Dim dsHDR As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If (Not dsHDR Is Nothing) Then
                    If (dsHDR.Tables.Count > 0) Then
                        If (dsHDR.Tables(0).Rows.Count > 0) Then
                            Dim objResult As objMobiTCTCTUInfo = New objMobiTCTCTUInfo()
                            ' objResult.MADOITAC = dsHDR.Tables(0).Rows(0)("").ToString()
                            objResult.MATHAMCHIEU = dsHDR.Tables(0).Rows(0)("MATHAMCHIEU").ToString()
                            objResult.SOTIEN = dsHDR.Tables(0).Rows(0)("SOTIEN").ToString()
                            objResult.PHICHUYENPHAT = dsHDR.Tables(0).Rows(0)("PHICHUYENPHAT").ToString()
                            ' objResult.NGONNGU = dsHDR.Tables(0).Rows(0)("").ToString()
                            objResult.MATIENTE = dsHDR.Tables(0).Rows(0)("MATIENTE").ToString()
                            objResult.THONGTINGIAODICH = dsHDR.Tables(0).Rows(0)("THONGTINGIAODICH").ToString()
                            objResult.MADICHVU = dsHDR.Tables(0).Rows(0)("MADICHVU").ToString()
                            objResult.MAHOSO = dsHDR.Tables(0).Rows(0)("MAHOSO").ToString()
                            objResult.TKTHUHUONG = dsHDR.Tables(0).Rows(0)("TKTHUHUONG").ToString()
                            objResult.MALOAIHINHTHUPHAT = dsHDR.Tables(0).Rows(0)("MALOAIHINHTHUPHAT").ToString()
                            objResult.MALOAIHINHTHUE = dsHDR.Tables(0).Rows(0)("MALOAIHINHTHUE").ToString()
                            objResult.TENLOAIHINHTHUE = dsHDR.Tables(0).Rows(0)("TENLOAIHINHTHUE").ToString()
                            objResult.MACQTHU = dsHDR.Tables(0).Rows(0)("MACQTHU").ToString()
                            objResult.TENCQTHU = dsHDR.Tables(0).Rows(0)("TENCQTHU").ToString()
                            objResult.SHKB = dsHDR.Tables(0).Rows(0)("SHKB").ToString()
                            objResult.TEN_KB = dsHDR.Tables(0).Rows(0)("TEN_KB").ToString()
                            objResult.MACOQUANQD = dsHDR.Tables(0).Rows(0)("MACOQUANQD").ToString()
                            objResult.TENCOQUANQD = dsHDR.Tables(0).Rows(0)("TENCOQUANQD").ToString()
                            objResult.SOQD = dsHDR.Tables(0).Rows(0)("SOQD").ToString()
                            objResult.NGAYQD = dsHDR.Tables(0).Rows(0)("NGAYQD").ToString()
                            objResult.MST = dsHDR.Tables(0).Rows(0)("MST").ToString()
                            objResult.HOTENNGUOINOP = dsHDR.Tables(0).Rows(0)("HOTENNGUOINOP").ToString()
                            objResult.SOCMNDNGUOINOP = dsHDR.Tables(0).Rows(0)("SOCMNDNGUOINOP").ToString()
                            objResult.DIACHINGUOINOP = dsHDR.Tables(0).Rows(0)("DIACHINGUOINOP").ToString()
                            objResult.HUYENNGUOINOP = dsHDR.Tables(0).Rows(0)("HUYENNGUOINOP").ToString()
                            objResult.TINHNGUOINOP = dsHDR.Tables(0).Rows(0)("TINHNGUOINOP").ToString()
                            objResult.DSKHOANNOP = dsHDR.Tables(0).Rows(0)("DSKHOANNOP").ToString()
                            '  objResult.MSGCONTENT = dsHDR.Tables(0).Rows(0)("").ToString()
                            objResult.SO_CT = dsHDR.Tables(0).Rows(0)("SO_CT").ToString()
                            objResult.TRANGTHAI = dsHDR.Tables(0).Rows(0)("TRANGTHAI").ToString()
                            objResult.NGAY_HT = dsHDR.Tables(0).Rows(0)("NGAY_HT").ToString()
                            objResult.NGAYMSG = dsHDR.Tables(0).Rows(0)("NGAYMSG").ToString()
                            objResult.SOKHUNG = dsHDR.Tables(0).Rows(0)("SOKHUNG").ToString()
                            objResult.SOMAY = dsHDR.Tables(0).Rows(0)("SOMAY").ToString()
                            objResult.MADBHC = dsHDR.Tables(0).Rows(0)("MADBHC").ToString()
                            objResult.THOIGIANGD = dsHDR.Tables(0).Rows(0)("THOIGIANGD").ToString()
                            objResult.MANGANHANG = dsHDR.Tables(0).Rows(0)("MANGANHANG").ToString()
                            objResult.PHUONGTHUC = dsHDR.Tables(0).Rows(0)("PHUONGTHUC").ToString()
                            objResult.SOTAIKHOAN_THE = dsHDR.Tables(0).Rows(0)("SOTAIKHOAN_THE").ToString()
                            objResult.SOTAIKHOAN_THE_DESCRYPT = dsHDR.Tables(0).Rows(0)("SOTAIKHOAN_THE_DECRYPT").ToString()
                            '  objResult.EWALLETTOKEN = dsHDR.Tables(0).Rows(0)("").ToString()
                            objResult.SOTAIKHOAN = dsHDR.Tables(0).Rows(0)("SOTAIKHOAN").ToString()
                            ' objResult.IDTAIKHOANLIENKET = dsHDR.Tables(0).Rows(0)("").ToString()
                            '  objResult.LOAIOTP = dsHDR.Tables(0).Rows(0)("").ToString()
                            ' objResult.TRANIDOTP = dsHDR.Tables(0).Rows(0)("").ToString()
                            ' objResult.DIENTHOAI = dsHDR.Tables(0).Rows(0)("").ToString()
                            'objResult.TTHAI_TCT = dsHDR.Tables(0).Rows(0)("").ToString()
                            objResult.MOTA = dsHDR.Tables(0).Rows(0)("MOTA").ToString()
                            objResult.GHICHU = dsHDR.Tables(0).Rows(0)("GHICHU").ToString()
                            objResult.MA_NH_A = dsHDR.Tables(0).Rows(0)("MA_NH_A").ToString()
                            objResult.TEN_NH_A = dsHDR.Tables(0).Rows(0)("TEN_NH_A").ToString()
                            objResult.MA_NH_B = dsHDR.Tables(0).Rows(0)("MA_NH_B").ToString()
                            objResult.TEN_NH_B = dsHDR.Tables(0).Rows(0)("TEN_NH_B").ToString()
                            objResult.MA_NNTIEN = dsHDR.Tables(0).Rows(0)("MA_NNTIEN").ToString()
                            objResult.TEN_NNTIEN = dsHDR.Tables(0).Rows(0)("TEN_NNTIEN").ToString()
                            'lay thong tin DTL
                            strSQL = "SELECT A.MATHAMCHIEU, A.LOAIPHILEPHI, A.MAPHILEPHI, A.TENPHILEPHI,"
                            strSQL &= " A.SOTIEN, A.MACHUONG, A.MATMUC, A.SOKHUNG, A.SOMAY, A.SOQD,"
                            strSQL &= " A.NGAYQD, A.MADBHC, A.THOIGIANGD"
                            strSQL &= " FROM SET_CTU_DTL_MOBITCT A  WHERE A.MATHAMCHIEU='" + strMathamchieu + "' AND THOIGIANGD=TO_DATE('" + objResult.THOIGIANGD + "','DD/MM/YYYY HH24:MI:SS')"
                            Dim dsDTL As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                            If Not dsDTL Is Nothing Then
                                If dsDTL.Tables.Count > 0 Then
                                    For Each vrow As DataRow In dsDTL.Tables(0).Rows
                                        Dim vobjDTL As objCTUDtl = New objCTUDtl()
                                        vobjDTL.LOAIPHILEPHI = vrow("LOAIPHILEPHI").ToString()
                                        vobjDTL.MAPHILEPHI = vrow("MAPHILEPHI").ToString()
                                        vobjDTL.TENPHILEPHI = vrow("TENPHILEPHI").ToString()
                                        vobjDTL.SOTIEN = vrow("SOTIEN").ToString()
                                        vobjDTL.MACHUONG = vrow("MACHUONG").ToString()
                                        vobjDTL.MATMUC = vrow("MATMUC").ToString()
                                        objResult.DTL.Add(vobjDTL)
                                    Next
                                End If
                            End If
                            Return objResult
                        End If
                    End If
                End If


            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
            End Try
            Return Nothing
        End Function
        Public Shared Sub UPDATE_CTU_THANHTOAN_TRANGTHAI_09(ByVal strMaThamCHieu As String, ByVal strSO_CT As String)
            Try
                Dim strSql = "UPDATE SET_CTU_HDR_MOBITCT A SET TRANGTHAI='09', ngay_ht=sysdate WHERE A.MATHAMCHIEU='" & strMaThamCHieu & "' AND A.TRANGTHAIin('05','06','09')"
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
                'cap nhat chung tu tai quay ve trang thai 07 gui thue loi de sau call gui thue
                strSql = "UPDATE TCS_CTU_HDR_MOBITCT A SET TRANG_THAI='07', ngay_ht=sysdate WHERE A.so_CT='" & strSO_CT & "' AND A.TRANG_THAI in ('07','01')"
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
            End Try
        End Sub
        Public Shared Sub UPDATE_CTU_THANHTOAN_TRANGTHAI_10(ByVal strMaThamCHieu As String, ByVal strSO_CT As String)
            Try
                Dim strSql = "UPDATE SET_CTU_HDR_MOBITCT A SET TRANGTHAI='10', ngay_ht=sysdate WHERE A.MATHAMCHIEU='" & strMaThamCHieu & "' AND A.TRANGTHAI in('05','06','09','10')"
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
                'cap nhat chung tu tai quay ve trang thai 07 gui thue loi de sau call gui thue
                strSql = "UPDATE TCS_CTU_HDR_MOBITCT A SET TRANG_THAI='01',tt_cthue=1, ngay_ht=sysdate WHERE A.so_CT='" & strSO_CT & "' AND A.TRANG_THAI in ('07','01')"
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
            End Try
        End Sub
        Public Shared Sub UPDATE_CTU_THANHTOAN_TRANGTHAI_07(ByVal strMaThamCHieu As String, ByVal strSO_CT As String, Optional ByVal StrGhiChu As String = "")
            Try
                Dim strSql = "UPDATE SET_CTU_HDR_MOBITCT A SET TRANGTHAI='07', ngay_ht=sysdate,ghichu=:ghichu WHERE A.MATHAMCHIEU='" & strMaThamCHieu & "' AND A.TRANGTHAI='08'"
                Dim lst As List(Of IDataParameter) = New List(Of IDataParameter)()
                If StrGhiChu.Length <= 500 Then
                    lst.Add(DataAccess.NewDBParameter("ghichu", ParameterDirection.Input, StrGhiChu, DbType.[String]))
                Else
                    lst.Add(DataAccess.NewDBParameter("ghichu", ParameterDirection.Input, StrGhiChu.Substring(0, 500), DbType.[String]))
                End If
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text, lst.ToArray())
                'cap nhat chung tu tai quay ve trang thai 07 gui thue loi de sau call gui thue
                strSql = "UPDATE TCS_CTU_HDR_MOBITCT A SET TRANG_THAI='04', ngay_ht=sysdate WHERE A.so_CT='" & strSO_CT & "' AND A.TRANG_THAI='05'"
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
                Throw ex
            End Try
        End Sub
        Public Shared Sub UPDATE_CTU_THANHTOAN_TRANGTHAI_04(ByVal strMaThamCHieu As String, Optional ByVal StrGhiChu As String = "")
            Try
                Dim strSql = "UPDATE SET_CTU_HDR_MOBITCT A SET TRANGTHAI='04', ngay_ht=sysdate,ghichu=:ghichu WHERE A.MATHAMCHIEU='" & strMaThamCHieu & "' AND A.TRANGTHAI='02'"
                Dim lst As List(Of IDataParameter) = New List(Of IDataParameter)()
                If StrGhiChu.Length <= 500 Then
                    lst.Add(DataAccess.NewDBParameter("ghichu", ParameterDirection.Input, StrGhiChu, DbType.[String]))
                Else
                    lst.Add(DataAccess.NewDBParameter("ghichu", ParameterDirection.Input, StrGhiChu.Substring(0, 500), DbType.[String]))
                End If
                ' DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
                'cap nhat chung tu tai quay ve trang thai 07 gui thue loi de sau call gui thue
                'GUI DI THUE
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text, lst.ToArray())
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
                Throw ex
            End Try
        End Sub
        Public Function CTU_HeaderIN(ByVal pvStrSoCT As String) As NewChungTu.infChungTuHDR
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chung của chứng từ
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Dim hdr As New NewChungTu.infChungTuHDR
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                'Kienvt : Lay bang TCS_CTU_THOP_HDR
                strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV," & _
                    "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                    "a.KyHieu_CT,a.So_CT,a.So_CT_NH,a.SO_XCARD," & _
                    "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                    "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
                    "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
                    "a.Ngay_QD,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
                    "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
                    "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co,a.seq_no," & _
                    "a.So_TK,a.Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                    "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                    "a.So_Khung,a.So_May,a.TK_KH_NH,a.NGAY_KH_NH,a.ten_kh_nh," & _
                    "ts.name MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu,a.TK_KB_NH, " & _
                    "a.Lan_In,a.Trang_Thai,a.ISACTIVE, a.so_bk, a.ngay_bk,a.ngay_ks, a.tkht,A.ten_nh_b ten_nh_b, a.MA_NTK, A.TEN_CQTHU," & _
                    "a.TK_KH_Nhan,a.MA_HQ, c.TEN AS TEN_HQ, b.TEN_LH AS TEN_LHXNK, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT,a.phi_gd2 as phi_gd,a.phi_vat2 as phi_vat, a.So_BT_KTKB,a.PT_TT,a.rm_ref_no,a.ref_no,a.QUAN_HUYENNNTIEN,a.TINH_TPNNTIEN" & _
                    " from TCS_CTU_HDR_MOBITCT a, TCS_DM_LHINH b, (select MA_HQ, TEN FROM TCS_DM_CQTHU WHERE MA_HQ IS NOT NULL) c," & _
                    "(SELECT a.name, a.branch_id,b.ma_nv FROM tcs_dm_chinhanh a,tcs_dm_nhanvien b where b.ma_cn=a.id) TS " & _
                    "where a.so_ct='" & pvStrSoCT & "' " & _
                    "AND a.Ma_NV=ts.ma_nv AND (a.MA_HQ = c.MA_HQ(+)) AND (a.LH_XNK = b.MA_LH(+)) "
                Dim dsCT As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

                ' Kiểm tra không có dữ liệu -> Thoát
                If (IsEmptyDataSet(dsCT)) Then Return hdr

                'Có dữ liệu -> đưa vào đối tượng "hdr"
                hdr.Ten_cqthu = dsCT.Tables(0).Rows(0).Item("TEN_CQTHU").ToString()
                hdr.CQ_QD = dsCT.Tables(0).Rows(0).Item("CQ_QD").ToString()
                hdr.DC_NNThue = dsCT.Tables(0).Rows(0).Item("DC_NNTHUE").ToString()
                hdr.DC_NNTien = dsCT.Tables(0).Rows(0).Item("DC_NNTIEN").ToString()
                hdr.Diachi_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Diachi_KH_Nhan").ToString()
                hdr.DVSDNS = dsCT.Tables(0).Rows(0).Item("DVSDNS").ToString()
                hdr.KyHieu_CT = dsCT.Tables(0).Rows(0).Item("KYHIEU_CT").ToString()
                hdr.Lan_In = dsCT.Tables(0).Rows(0).Item("LAN_IN").ToString()
                hdr.LH_XNK = dsCT.Tables(0).Rows(0).Item("LH_XNK").ToString()
                hdr.Ly_Do = dsCT.Tables(0).Rows(0).Item("LY_DO").ToString()
                hdr.Ma_CQThu = dsCT.Tables(0).Rows(0).Item("MA_CQTHU").ToString()
                hdr.Ma_DThu = dsCT.Tables(0).Rows(0).Item("MA_DTHU").ToString()
                hdr.Ma_Huyen = dsCT.Tables(0).Rows(0).Item("MA_HUYEN").ToString()
                hdr.Ma_KS = dsCT.Tables(0).Rows(0).Item("MA_KS").ToString()
                hdr.Ma_LThue = dsCT.Tables(0).Rows(0).Item("MA_LTHUE").ToString()
                hdr.MA_NH_A = dsCT.Tables(0).Rows(0).Item("MA_NH_A").ToString()
                hdr.MA_NH_B = dsCT.Tables(0).Rows(0).Item("MA_NH_B").ToString()
                hdr.Ma_NNThue = dsCT.Tables(0).Rows(0).Item("MA_NNTHUE").ToString()
                hdr.Ma_NNTien = dsCT.Tables(0).Rows(0).Item("MA_NNTIEN").ToString()
                hdr.Ma_NT = dsCT.Tables(0).Rows(0).Item("MA_NT").ToString()
                hdr.Ma_NV = dsCT.Tables(0).Rows(0).Item("MA_NV").ToString()
                hdr.Ma_Tinh = dsCT.Tables(0).Rows(0).Item("MA_TINH").ToString()
                hdr.Ma_TQ = dsCT.Tables(0).Rows(0).Item("MA_TQ").ToString()
                hdr.Ma_Xa = dsCT.Tables(0).Rows(0).Item("MA_XA").ToString()
                hdr.Ngay_CT = dsCT.Tables(0).Rows(0).Item("NGAY_CT").ToString()
                hdr.Ngay_HT = dsCT.Tables(0).Rows(0).Item("NGAY_HT").ToString()
                hdr.Ngay_KB = dsCT.Tables(0).Rows(0).Item("NGAY_KB").ToString()
                hdr.NGAY_KH_NH = dsCT.Tables(0).Rows(0).Item("NGAY_KH_NH").ToString()
                hdr.Ngay_QD = dsCT.Tables(0).Rows(0).Item("NGAY_QD").ToString()
                hdr.Ngay_TK = dsCT.Tables(0).Rows(0).Item("NGAY_TK").ToString()
                hdr.SHKB = dsCT.Tables(0).Rows(0).Item("SHKB").ToString()
                hdr.So_BT = dsCT.Tables(0).Rows(0).Item("SO_BT").ToString()
                hdr.So_BThu = dsCT.Tables(0).Rows(0).Item("SO_BTHU").ToString()
                hdr.So_CT = dsCT.Tables(0).Rows(0).Item("SO_CT").ToString()
                hdr.So_CT_NH = dsCT.Tables(0).Rows(0).Item("SO_CT_NH").ToString()
                hdr.So_Khung = dsCT.Tables(0).Rows(0).Item("SO_KHUNG").ToString()
                hdr.So_May = dsCT.Tables(0).Rows(0).Item("SO_MAY").ToString()
                hdr.So_QD = dsCT.Tables(0).Rows(0).Item("SO_QD").ToString()
                hdr.So_TK = dsCT.Tables(0).Rows(0).Item("SO_TK").ToString()
                hdr.Ten_DVSDNS = dsCT.Tables(0).Rows(0).Item("Ten_DVSDNS").ToString()
                hdr.Ten_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Ten_KH_Nhan").ToString()
                hdr.Ten_NNThue = dsCT.Tables(0).Rows(0).Item("TEN_NNTHUE").ToString()
                hdr.Ten_NNTien = dsCT.Tables(0).Rows(0).Item("TEN_NNTIEN").ToString()
                hdr.TG_ID = dsCT.Tables(0).Rows(0).Item("TG_ID").ToString()
                hdr.TK_Co = dsCT.Tables(0).Rows(0).Item("TK_Co").ToString()
                hdr.TK_KH_NH = dsCT.Tables(0).Rows(0).Item("TK_KH_NH").ToString()
                hdr.TEN_KH_NH = dsCT.Tables(0).Rows(0).Item("ten_kh_nh").ToString()
                hdr.TK_KH_Nhan = dsCT.Tables(0).Rows(0).Item("TK_KH_Nhan").ToString()
                hdr.TK_No = dsCT.Tables(0).Rows(0).Item("TK_No").ToString()
                hdr.Trang_Thai = dsCT.Tables(0).Rows(0).Item("TRANG_THAI").ToString()
                hdr.TT_TThu = dsCT.Tables(0).Rows(0).Item("TT_TTHU").ToString()
                hdr.TTien = dsCT.Tables(0).Rows(0).Item("TTIEN").ToString()
                hdr.TTien_NT = dsCT.Tables(0).Rows(0).Item("TTIEN_NT").ToString()
                hdr.Ty_Gia = dsCT.Tables(0).Rows(0).Item("TY_GIA").ToString()
                hdr.XA_ID = dsCT.Tables(0).Rows(0).Item("XA_ID").ToString()
                hdr.So_BT_KTKB = dsCT.Tables(0).Rows(0).Item("So_BT_KTKB").ToString()
                hdr.PT_TT = dsCT.Tables(0).Rows(0).Item("PT_TT").ToString()
                hdr.TK_KB_NH = dsCT.Tables(0).Rows(0).Item("TK_KB_NH").ToString()
                hdr.PHI_GD = dsCT.Tables(0).Rows(0).Item("phi_gd").ToString()
                hdr.PHI_VAT = dsCT.Tables(0).Rows(0).Item("phi_vat").ToString()
                hdr.So_RM = dsCT.Tables(0).Rows(0).Item("rm_ref_no").ToString()
                hdr.REF_NO = dsCT.Tables(0).Rows(0).Item("ref_no").ToString()
                hdr.QUAN_HUYEN_NNTIEN = dsCT.Tables(0).Rows(0).Item("QUAN_HUYENNNTIEN").ToString()
                hdr.TINH_TPHO_NNTIEN = dsCT.Tables(0).Rows(0).Item("TINH_TPNNTIEN").ToString()
                hdr.So_BK = dsCT.Tables(0).Rows(0).Item("so_bk").ToString()
                hdr.Ngay_BK = dsCT.Tables(0).Rows(0).Item("ngay_bk").ToString()
                hdr.MA_HQ = dsCT.Tables(0).Rows(0).Item("MA_HQ").ToString()
                hdr.TEN_MA_HQ = dsCT.Tables(0).Rows(0).Item("TEN_HQ").ToString()
                hdr.Ten_lhxnk = dsCT.Tables(0).Rows(0).Item("TEN_LHXNK").ToString()
                hdr.TKHT_NH = dsCT.Tables(0).Rows(0).Item("tkht").ToString()
                hdr.Ten_NH_B = dsCT.Tables(0).Rows(0).Item("TEN_NH_B").ToString()
                '   hdr.Ten_nh_a = dsCT.Tables(0).Rows(0).Item("Ten_NH_A").ToString()
                hdr.MA_NTK = dsCT.Tables(0).Rows(0).Item("MA_NTK").ToString()
                hdr.SEQ_NO = dsCT.Tables(0).Rows(0).Item("seq_no").ToString()
                If IsDBNull(dsCT.Tables(0).Rows(0).Item("ISACTIVE")) Then
                    hdr.TT_BDS = "0"
                Else
                    hdr.TT_BDS = "1"
                End If
                Return hdr
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_ChiTietIN(ByVal pvStrSoCT As String, ByVal pvStrNgay_BK As String) As NewChungTu.infChungTuDTL()
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ---------------------------------------------------- 
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                    "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                    "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT " & _
                    "from TCS_CTU_DTL " & _
                    "where (ngay_kb = " & pvStrNgay_BK & ") " & _
                    "and (so_ct = " & pvStrSoCT & ") "


                Dim ds As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

                If (IsEmptyDataSet(ds)) Then Return Nothing
                Dim i, intCount As Integer
                intCount = ds.Tables(0).Rows.Count - 1
                Dim dtl(intCount) As NewChungTu.infChungTuDTL

                For i = 0 To intCount
                    dtl(i) = New NewChungTu.infChungTuDTL

                    dtl(i).CCH_ID = ds.Tables(0).Rows(i).Item("CCH_ID").ToString()
                    dtl(i).DT_ID = ds.Tables(0).Rows(i).Item("DT_ID").ToString()
                    dtl(i).ID = ds.Tables(0).Rows(i).Item("ID").ToString()
                    dtl(i).Ky_Thue = ds.Tables(0).Rows(i).Item("Ky_Thue").ToString()
                    dtl(i).LKH_ID = ds.Tables(0).Rows(i).Item("LKH_ID").ToString()
                    dtl(i).Ma_Cap = ds.Tables(0).Rows(i).Item("Ma_Cap").ToString()
                    dtl(i).Ma_Chuong = ds.Tables(0).Rows(i).Item("Ma_Chuong").ToString()
                    dtl(i).Ma_DThu = ds.Tables(0).Rows(i).Item("Ma_DThu").ToString()
                    dtl(i).Ma_Khoan = ds.Tables(0).Rows(i).Item("Ma_Khoan").ToString()
                    dtl(i).Ma_Loai = ds.Tables(0).Rows(i).Item("Ma_Loai").ToString()
                    dtl(i).Ma_Muc = ds.Tables(0).Rows(i).Item("Ma_Muc").ToString()
                    dtl(i).Ma_NV = ds.Tables(0).Rows(i).Item("Ma_NV").ToString()
                    dtl(i).Ma_TLDT = ds.Tables(0).Rows(i).Item("Ma_TLDT").ToString()
                    dtl(i).Ma_TMuc = ds.Tables(0).Rows(i).Item("Ma_TMuc").ToString()
                    dtl(i).MTM_ID = ds.Tables(0).Rows(i).Item("MTM_ID").ToString()
                    dtl(i).Ngay_KB = ds.Tables(0).Rows(i).Item("Ngay_KB").ToString()
                    dtl(i).Noi_Dung = ds.Tables(0).Rows(i).Item("Noi_Dung").ToString()
                    dtl(i).SHKB = ds.Tables(0).Rows(i).Item("SHKB").ToString()
                    dtl(i).So_BT = ds.Tables(0).Rows(i).Item("So_BT").ToString()
                    dtl(i).SoTien = ds.Tables(0).Rows(i).Item("SoTien").ToString()
                    dtl(i).SoTien_NT = ds.Tables(0).Rows(i).Item("SoTien_NT").ToString()
                Next

                Return dtl
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_IN_LASER(ByVal pvStrSoCT As String, ByVal pvStrNgayKB As String) As DataSet
            ' Dim cnCT As DataAccess
            Try
                ' cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select rownum as STT,dtl.SO_CT,dtl.noi_dung as NOI_DUNG, (dtl.ma_cap||dtl.ma_chuong) as CC, " & _
                    "dtl.so_tk as LK, (dtl.ma_muc||dtl.ma_tmuc) as MTM, " & _
                    "dtl.ky_thue as KyThue, dtl.sotien as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK,  " & _
                     " (select ma_db from tcs_dm_khobac where shkb=dtl.shkb and ROWNUM = 1) AS ma_dbhc " & _
                    "from tcs_ctu_dtl_MOBITCT dtl " & _
                     "where so_ct = '" & pvStrSoCT & "' " & _
                    " order by rownum "
                log.Info("CTU_IN_LASER - strSQL: " & strSQL)

                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
                Throw ex
            Finally
                ' If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_IN_TRUOCBA(ByVal pvStrSoCT As String) As DataSet
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select rownum as STT,dtl.SO_CT,(dtl.noi_dung || '\n. Số khung: ' || hdr.so_khung || '\n. Số máy: ' || hdr.so_may || '\n.' || hdr.loaits ) as NOI_DUNG, (dtl.ma_cap||dtl.ma_chuong) as CC, " & _
                        "hdr.so_qd as LK, (dtl.ma_muc||dtl.ma_tmuc) as MTM, " & _
                        "dtl.ky_thue as KyThue, dtl.sotien as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK,  " & _
                         " (select ma_db from tcs_dm_khobac where shkb=dtl.shkb and ROWNUM = 1) AS ma_dbhc " & _
                        "from tcs_ctu_dtl_MOBITCT dtl, tcs_ctu_hdr_MOBITCT hdr " & _
                        "where  hdr.so_ct = dtl.so_ct and  dtl.so_ct = '" & pvStrSoCT & "'" & _
                        " order by rownum "
                log.Info("CTU_IN_TRUOCBA - strSQL: " & strSQL)

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

       
        Public Sub CTU_Update_LanIn(ByVal pvStrSoCT As String)
           
            Try

                Dim strSQL As String
                strSQL = "update TCS_CTU_HDR_MOBITCT set LAN_IN =nvl(LAN_IN,0)+1  " & _
                        "where  " & _
                       " so_ct = '" & pvStrSoCT & "'"
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)

            Catch ex As Exception
               log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
                Throw ex
            Finally
               
            End Try
        End Sub
        Public Shared Function BC_CHITIET_CTU_MOBITCT(ByVal pvStrTuNgay As String, ByVal pvStrDenNgay As String, Optional ByVal pvStrMaCN As String = "", Optional ByVal pvStrTrangThai As String = "") As DataSet
            Try
                Dim strSQL As String = "select rownum STT, a.* from (SELECT 'MOBITCT' KENHCT,A.MATHAMCHIEU SO_CT,A.SO_CT_NH,A.MST MA_NNTHUE,A.HOTENNGUOINOP TEN_NNTHUE"
                strSQL &= " ,A.SOTAIKHOAN TK_KH_NH,A.MATIENTE MA_NT,A.SOTIEN TTIEN_NT"
                strSQL &= " ,A.THONGTINGIAODICH  REMARKS,B.MOTA,A.MA_CN,TO_CHAR(A.NGAYMSG,'DD/MM/YYYY HH24:MI:SS') NGAYMSG"
                strSQL &= " FROM SET_CTU_HDR_MOBITCT A,TCS_DM_TRANGTHAI B"
                strSQL &= " WHERE A.TRANGTHAI = B.TRANGTHAI(+) AND B.LOAI='CTUMOBITCT'"
                If pvStrTuNgay.Length > 0 Then
                    Dim strNgayMSG As String = ""
                    If DateString_ChangeFormat("dd/MM/yyyy", pvStrTuNgay, "yyyyMMdd", strNgayMSG) Then
                        strSQL &= " AND to_char(a.NGAYMSG,'YYYYMMDD')>= '" & strNgayMSG & "'"
                    End If
                End If
                If pvStrDenNgay.Length > 0 Then
                    Dim strNgayMSG As String = ""
                    If DateString_ChangeFormat("dd/MM/yyyy", pvStrDenNgay, "yyyyMMdd", strNgayMSG) Then
                        strSQL &= " AND to_char(a.NGAYMSG,'YYYYMMDD')<= '" & strNgayMSG & "'"
                    End If
                End If
                If pvStrMaCN.Length > 0 Then
                    strSQL &= " AND a.MA_CN= '" & pvStrMaCN & "'"
                End If
                If pvStrTrangThai.Length > 0 Then
                    strSQL &= " AND a.TRANGTHAI= '" & pvStrTrangThai & "'"
                End If
                strSQL &= " ORDER BY NGAYMSG) a "
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace)
                Throw ex
            End Try
            Return Nothing
        End Function
    End Class
End Namespace

