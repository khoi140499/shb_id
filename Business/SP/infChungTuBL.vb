﻿Namespace ChungTuBL
    Public Class infChungTuBL
        Private _HDR As infChungTuBL_HDR
        Private _ListDTL As List(Of infChungTuBL_DTL)

        Public Sub New()
        End Sub
        Public Sub New(ByVal HDR As infChungTuBL_HDR, ByVal ListDTL As List(Of infChungTuBL_DTL))
            Me.HDR = HDR
            Me.ListDTL = ListDTL
        End Sub
        Public Property HDR() As infChungTuBL_HDR
            Get
                Return _HDR
            End Get
            Set(ByVal value As infChungTuBL_HDR)
                _HDR = value
            End Set
        End Property

        Public Property ListDTL() As List(Of infChungTuBL_DTL)
            Get
                Return _ListDTL
            End Get
            Set(ByVal value As List(Of infChungTuBL_DTL))
                _ListDTL = value
            End Set
        End Property

    End Class
End Namespace
