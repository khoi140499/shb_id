﻿Namespace ChungTuSP
    Public Class buChungTu
        Public Shared Function CTU_GetListKiemSoat(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, _
          Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "") As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetListKiemSoat(strNgayCT, strTrangThai, strMaDiemThu, strMaNV, strMaKS, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Header_Set(ByVal sSOCT As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Header_Set(sSOCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_ChiTiet_Set_CT_TK(ByVal so_ct As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_ChiTiet_Set_CT_TK(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Get_TrangThai(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_TrangThai(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub CTU_SetTrangThai(ByVal so_ct As String, ByVal strTT As String, _
                       Optional ByVal strMaKS As String = "", Optional ByVal strMaHuyKS As String = "", _
                       Optional ByVal strRMNo As String = "", Optional ByVal strTT_SP As String = "0", _
                       Optional ByVal strLyDoHuy As String = "", Optional ByVal strNgayKSVCT As Boolean = False)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_SetTrangThai(so_ct, strTT, strMaKS, strMaHuyKS, strRMNo, strTT_SP, strLyDoHuy, strNgayKSVCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        '// Lấy tên thể hiện trạng thái
        Public Shared Function Get_TrangThai(ByVal strIDTrangThai As String, ByVal strTT_SP As String) As String
            Dim strResult As String
            Select Case (strIDTrangThai)
                Case "01" '// mới nhận từ CITAD
                    strResult = "Mới nhận từ Citad"
                Case "02" '// chờ KS
                    strResult = "Chờ kiểm soát"
                Case "03" '// Hạch toán thành công
                    If strTT_SP = "2" Then
                        strResult = "Thành công"
                    ElseIf strTT_SP = "1" Then
                        strResult = "Thành công - chuyển TTSP lỗi"
                    ElseIf strTT_SP = "0" Then
                        strResult = "Thành công "
                    End If
                Case "04" '// Chuyển trả
                    strResult = "Chuyển trả"
                Case "05" '// Chờ duyệt hủy
                    strResult = "Chờ duyệt hủy"
                Case "06" '// đã Hủy
                    strResult = "Đã hủy"
                Case "07" '// đã Hủy
                    strResult = "Lệch thông tin"
                Case "08" '// đã Hủy
                    strResult = "Chuyển TTSP lỗi"
                Case "09" '// đã Hủy
                    strResult = "Thành công"
                Case "10" '// đã Hủy
                    strResult = "Chứng từ có KB ko mở tài khoản tại NHTM"
                Case Else
                    strResult = strIDTrangThai
            End Select
            Return strResult
        End Function


        '// Lấy tên thể hiện trạng thái
        Public Shared Function Get_TrangThai_Citad(ByVal strIDTrangThai As String, ByVal strTT_SP As String) As String
            Dim strResult As String
            Select Case (strIDTrangThai)
                Case "01" '// mới nhận từ CITAD
                    strResult = "Mới nhận từ Citad"
                Case "02" '// chờ KS
                    strResult = "Chờ kiểm soát"
                Case "03" '// Hạch toán thành công
                    If strTT_SP = "1" Then
                        strResult = "Thành công"
                    ElseIf strTT_SP = "0" Then
                        strResult = "Thành công - chuyển TTSP lỗi"
                    End If
                Case "04" '// Chuyển trả
                    strResult = "Chuyển trả"
                Case "05" '// Chờ duyệt hủy
                    strResult = "Chờ duyệt hủy"
                Case "06" '// đã Hủy
                    strResult = "Đã hủy"
                Case Else
                    strResult = strIDTrangThai
            End Select
            Return strResult
        End Function


        Public Shared Function CTU_GetListKiemSoatBL(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, _
          Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "") As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetListKiemSoatBL(strNgayCT, strTrangThai, strMaDiemThu, strMaNV, strMaKS, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Header_Set_BL(ByVal sSOCT As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Header_Set_BL(sSOCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_ChiTiet_Set_CT_BL(ByVal so_ct As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_ChiTiet_Set_CT_BL(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Get_TrangThai_BL(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_TrangThai_BL(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub CTU_SetTrangThai_BL(ByVal so_ct As String, ByVal strTT As String, _
                     Optional ByVal strMaKS As String = "", Optional ByVal strMaHuyKS As String = "", _
                     Optional ByVal strRMNo As String = "", Optional ByVal strTT_SP As String = "0", _
                     Optional ByVal strLyDoHuy As String = "", Optional ByVal strNgayKSVCT As Boolean = False, Optional ByVal strLyDoCT As String = "")
            Try
                Dim daCT As New daChungTu
                daCT.CTU_SetTrangThai_BL(so_ct, strTT, strMaKS, strMaHuyKS, strRMNo, strTT_SP, strLyDoHuy, strNgayKSVCT, strLyDoCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function CTU_IN_BL(ByVal so_ct As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_IN_BL(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_IN_BANGKE(ByVal so_ct As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_IN_BANGKE(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Get_LanIn_BK(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_LanIn_BK(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub CTU_Update_LanIn_BK(ByVal so_ct As String)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_Update_LanIn_BK(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function CTU_Get_LanIn_CTBL(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_LanIn_CTBL(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub CTU_Update_LanIn_CTBL(ByVal so_ct As String)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_Update_LanIn_CTBL(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function CTU_Get_LanIn_BL(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_LanIn_BL(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub CTU_Update_LanIn_BL(ByVal so_ct As String)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_Update_LanIn_BL(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function CTU_Get_Valid_LanKS(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_Valid_LanKS(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Get_LanKS(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_LanKS(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub CTU_SetLanKS(ByVal so_ct As String, ByVal strClickKS As String)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_SetLanKS(so_ct, strClickKS)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function CTU_Get_Valid_LanKS_Citad(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_Valid_LanKS_Citad(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Get_LanKS_Citad(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_LanKS_Citad(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub CTU_SetLanKS_Citad(ByVal so_ct As String, ByVal strClickKS As String)
            Try
                Dim daCT As New daChungTu
                daCT.CTU_SetLanKS_Citad(so_ct, strClickKS)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
       
        ''' <summary>
        ''' ham nay cho cacs phan bao lanh moi nam het tren HDR bor DTL
        ''' </summary>
        ''' <param name="so_ct"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CTU_IN_BL_NEW(ByVal so_ct As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_IN_BL_NEW(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_GetListKiemSoatBL_NEW(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, _
         Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "") As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_GetListKiemSoatBL_NEW(strNgayCT, strTrangThai, strMaDiemThu, strMaNV, strMaKS, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' DANH SACH CHUNG TU CHO PHAN DUYET CUA CAC KENH KHAC ETAX
        ''' </summary>
        ''' <param name="sSOCT"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CTU_Header_Set_BL_NEW(ByVal sSOCT As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Header_Set_BL_NEW(sSOCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' HAM LAY CHI TIET CHUNG TU THEO CAC KENH KHAC NHAU
        ''' </summary>
        ''' <param name="so_ct"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CTU_ChiTiet_Set_CT_BL_NEW(ByVal so_ct As String) As DataSet
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_ChiTiet_Set_CT_BL_NEW(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' lay trang thai thanh toan bien lai xem co can cat tien ko hay chi di TTSP
        ''' </summary>
        ''' <param name="so_ct"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CTU_Get_TrangThai_ThanhToan_BL(ByVal so_ct As String) As String
            Try
                Dim daCT As New daChungTu
                Return daCT.CTU_Get_TrangThai_ThanhToan_BL(so_ct)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
