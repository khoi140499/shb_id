﻿Namespace ChungTuSP
    Public Class infChungTuSP_DTL
        Private strDTL_ID As String
        Private strSo_ToKhai As String
        Private strNamDK As String
        Private strSacThue As String
        Private strMa_Chuong As String
        Private strMa_ndkt As String
        Private strMa_Muc As String
        Private strMa_TMuc As String
        Private strNoi_Dung As String
        Private strKy_Thue As String
        Private dblSoTien As Double
        Private dblSoTien_NT As Double
        Private strSo_CT As String
        Private strSO_CT_NH As String
        Private strSHKB As String
        Private iNgay_KB As Integer
        Private iMa_NV As Integer
        Private iSo_BT As Integer  ' số bút toán
        Private strMa_DThu As String
        Private iCCH_ID As String
        Private strMa_Cap As String

        Private iLKH_ID As String
        Private strMa_Loai As String
        Private strMa_Khoan As String
        Private iMTM_ID As String
       
        Private iDT_ID As String
        Private strMa_TLDT As String
      
        Private strMaQuy As String
        Private strMaDP As String
        Public _PT_TT As String
        Public Property SO_CT() As String
            Get
                Return strSo_CT
            End Get
            Set(ByVal value As String)
                strSo_CT = value
            End Set
        End Property
        Public Property SO_CT_NH() As String
            Get
                Return strSO_CT_NH
            End Get
            Set(ByVal value As String)
                strSO_CT_NH = value
            End Set
        End Property
        Public Property SO_TK() As String
            Get
                Return strSo_ToKhai
            End Get
            Set(ByVal value As String)
                strSo_ToKhai = value
            End Set
        End Property

        Public Property NGAY_TK() As String
            Get
                Return strNamDK
            End Get
            Set(ByVal value As String)
                strNamDK = value
            End Set
        End Property

        Public Property SAC_THUE() As String
            Get
                Return strSacThue
            End Get
            Set(ByVal value As String)
                strSacThue = value
            End Set
        End Property
        Public Property MA_NDKT() As String
            Get
                Return strMa_ndkt
            End Get
            Set(ByVal Value As String)
                strMa_ndkt = Value
            End Set
        End Property
        Public Sub New()

        End Sub

        Public Property PT_TT() As String
            Get
                Return _PT_TT
            End Get
            Set(ByVal Value As String)
                _PT_TT = Value
            End Set
        End Property

        Public Property ID() As String
            Get
                Return strDTL_ID
            End Get
            Set(ByVal Value As String)
                strDTL_ID = Value
            End Set
        End Property
        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property
        Public Property Ngay_KB() As Integer
            Get
                Return iNgay_KB
            End Get
            Set(ByVal Value As Integer)
                iNgay_KB = Value
            End Set
        End Property
        Public Property Ma_NV() As Integer
            Get
                Return iMa_NV
            End Get
            Set(ByVal Value As Integer)
                iMa_NV = Value
            End Set
        End Property
        Public Property So_BT() As Integer
            Get
                Return iSo_BT
            End Get
            Set(ByVal Value As Integer)
                iSo_BT = Value
            End Set
        End Property
        Public Property Ma_DThu() As String
            Get
                Return strMa_DThu
            End Get
            Set(ByVal Value As String)
                strMa_DThu = Value
            End Set
        End Property
        Public Property CCH_ID() As String
            Get
                Return iCCH_ID
            End Get
            Set(ByVal Value As String)
                iCCH_ID = Value
            End Set
        End Property
        Public Property Ma_Cap() As String
            Get
                Return strMa_Cap
            End Get
            Set(ByVal Value As String)
                strMa_Cap = Value
            End Set
        End Property
        Public Property Ma_Chuong() As String
            Get
                Return strMa_Chuong
            End Get
            Set(ByVal Value As String)
                strMa_Chuong = Value
            End Set
        End Property
        Public Property LKH_ID() As String
            Get
                Return iLKH_ID
            End Get
            Set(ByVal Value As String)
                iLKH_ID = Value
            End Set
        End Property
        Public Property Ma_Loai() As String
            Get
                Return strMa_Loai
            End Get
            Set(ByVal Value As String)
                strMa_Loai = Value
            End Set
        End Property
        Public Property Ma_Khoan() As String
            Get
                Return strMa_Khoan
            End Get
            Set(ByVal Value As String)
                strMa_Khoan = Value
            End Set
        End Property
        Public Property MTM_ID() As String
            Get
                Return iMTM_ID
            End Get
            Set(ByVal Value As String)
                iMTM_ID = Value
            End Set
        End Property

        Public Property Ma_Muc() As String
            Get
                Return strMa_Muc
            End Get
            Set(ByVal Value As String)
                strMa_Muc = Value
            End Set
        End Property
        Public Property Ma_TMuc() As String
            Get
                Return strMa_TMuc
            End Get
            Set(ByVal Value As String)
                strMa_TMuc = Value
            End Set
        End Property
        Public Property Noi_Dung() As String
            Get
                Return strNoi_Dung
            End Get
            Set(ByVal Value As String)
                strNoi_Dung = Value
            End Set
        End Property
        Public Property DT_ID() As String
            Get
                Return iDT_ID
            End Get
            Set(ByVal Value As String)
                iDT_ID = Value
            End Set
        End Property
        Public Property Ma_TLDT() As String
            Get
                Return strMa_TLDT
            End Get
            Set(ByVal Value As String)
                strMa_TLDT = Value
            End Set
        End Property

        Public Property Ky_Thue() As String
            Get
                Return strKy_Thue
            End Get
            Set(ByVal Value As String)
                strKy_Thue = Value
            End Set
        End Property
        Public Property SoTien() As Double
            Get
                Return dblSoTien
            End Get
            Set(ByVal Value As Double)
                dblSoTien = Value
            End Set
        End Property
        Public Property SoTien_NT() As Double
            Get
                Return dblSoTien_NT
            End Get
            Set(ByVal Value As Double)
                dblSoTien_NT = Value
            End Set
        End Property
        Public Property MaQuy() As String
            Get
                Return strMaQuy
            End Get
            Set(ByVal Value As String)
                strMaQuy = Value
            End Set
        End Property

        Public Property Ma_DP() As String
            Get
                Return strMaDP
            End Get
            Set(ByVal Value As String)
                strMaDP = Value
            End Set
        End Property
    End Class
End Namespace

