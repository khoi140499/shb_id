﻿Namespace BienLaiObj
    Public Class DtlBl

        Private _MA_CHUONG As String
        Private _MA_TMUC As String
        Private _NOI_DUNG As String
        Private _KY_THUE As String
        Private _SOTIEN As String
        Private _SOTIEN_NT As String

        Private _SHKB As String
        Private _NGAY_KB As String
        Private _MA_NV As String
        Private _SO_BT As String
        Private _MA_DTHU As String


        Private _SO_CT As String
        Private _TT_BTOAN As String

        Private _MA_LT As String



        Public Property MA_LT() As String
            Get
                Return _MA_LT
            End Get
            Set(ByVal value As String)
                _MA_LT = value
            End Set
        End Property



        Public Property KY_THUE() As String
            Get
                Return _KY_THUE
            End Get
            Set(ByVal value As String)
                _KY_THUE = value
            End Set
        End Property



        Public Property TT_BTOAN() As String
            Get
                Return _TT_BTOAN
            End Get
            Set(ByVal value As String)
                _TT_BTOAN = value
            End Set
        End Property

        Public Property SO_CT() As String
            Get
                Return _SO_CT
            End Get
            Set(ByVal value As String)
                _SO_CT = value
            End Set
        End Property

        Public Property SOTIEN_NT() As String
            Get
                Return _SOTIEN_NT
            End Get
            Set(ByVal value As String)
                _SOTIEN_NT = value
            End Set
        End Property

        Public Property SOTIEN() As String
            Get
                Return _SOTIEN
            End Get
            Set(ByVal value As String)
                _SOTIEN = value
            End Set
        End Property

        Public Property NOI_DUNG() As String
            Get
                Return _NOI_DUNG
            End Get
            Set(ByVal value As String)
                _NOI_DUNG = value
            End Set
        End Property

        Public Property MA_TMUC() As String
            Get
                Return _MA_TMUC
            End Get
            Set(ByVal value As String)
                _MA_TMUC = value
            End Set
        End Property

        Public Property MA_CHUONG() As String
            Get
                Return _MA_CHUONG
            End Get
            Set(ByVal value As String)
                _MA_CHUONG = value
            End Set
        End Property

        Public Property MA_DTHU() As String
            Get
                Return _MA_DTHU
            End Get
            Set(ByVal value As String)
                _MA_DTHU = value
            End Set
        End Property

        Public Property SO_BT() As String
            Get
                Return _SO_BT
            End Get
            Set(ByVal value As String)
                _SO_BT = value
            End Set
        End Property

        Public Property MA_NV() As String
            Get
                Return _MA_NV
            End Get
            Set(ByVal value As String)
                _MA_NV = value
            End Set
        End Property

        Public Property NGAY_KB() As String
            Get
                Return _NGAY_KB
            End Get
            Set(ByVal value As String)
                _NGAY_KB = value
            End Set
        End Property

        Public Property SHKB() As String
            Get
                Return _SHKB
            End Get
            Set(ByVal value As String)
                _SHKB = value
            End Set
        End Property
    End Class
End Namespace

