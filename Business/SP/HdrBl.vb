﻿Namespace BienLaiObj
    Public Class HdrBl

        Private _MA_NNTHUE As String    'Mã số thuế người nộp thuế
        Private _TEN_NNTHUE As String   'Tên người nộp thuế
        Private _DC_NNTHUE As String    'Địa chỉ người nộp thuế

        Private _MA_NNTIEN As String    'Mã số thuế người nộp tiền
        Private _TEN_NNTIEN As String   'Tên người nộp tiền
        Private _DC_NNTIEN As String    'Địa chỉ người nộp tiền

        Private _NGAY_NTIEN As String   'Ngay NNT nop tien

        Private _SHKB As String         'Số hiệu kho bạc
        Private _TEN_KB As String         'Ten kho bạc
        Private _NGAY_KB As String      'Ngày kho bạc
        Private _MA_DTHU As String      'Mã điểm thu => 0
        Private _TEN_DTHU As String      'Ten điểm thu => 0
        Private _NGAY_CT As String     'Ngày chứng từ
        Private _TK_CO As String        'Tài khoản có của kho bạc
        Private _TK_NO As String        'Tai khoan no
        Private _MA_LTHUE As String     'Mã loại thuế (Thuế nội địa, Thuế hq,....)
        Private _TEN_LTHUE As String     'Mã loại thuế (Thuế nội địa, Thuế hq,....)
        Private _CQ_QD As String        'Co quan quyet dinh
        Private _MA_CQTHU As String     'Mã cơ quan thu
        Private _TEN_CQTHU As String    'Ten cơ quan thu

        Private _MA_DBHC As String     'Mã dia ban hanh chinh
        Private _TEN_DBHC As String    'Ten dia ban hanh chinh
        Private _TK_KH_NH As String     'x  'Số tài khoản của khách hàng
        Private _TENTK_KH_NH As String     'ten tai khoan khach hang
        Private _NGAY_KH_NH As String   'x  'Ngày cắt tiền khách hàng

        Private _SO_BIENLAI As String

        Private _MA_NH_TT As String     'ma ngan hang truc tiep
        Private _TEN_NH_TT As String   'ten ngan hang truc tiep
        Private _MA_NH_B As String   'ma ngan hang gian tiep
        Private _TEN_NH_B As String   'ten ngan hang gian tiep


        Private _MA_NT As String        'Mã nguyên tệ (VND, USD)
        Private _TY_GIA As String       'Tỷ giá so với VND
        Private _TTIEN As String        'Tổng tiền VND
        Private _TTIEN_NT As String     'Tông tiền ngoại tệ (VND,USD,...)

        Private _REMARK As String      'x  'Nội dung citad

        Private _MA_NV As String        'x  'Mã nhân viên lập
        Private _MA_KS As String        'x  'Mã nhân viên kiểm soát
        Private _MA_CN As String        'x  'Mã chi nhánh nhân viên lập
        Private _NGAY_KS As String      'x  Ngày kiểm soát

        Private _SO_BT As String        'Số bút toán

        Private _KYHIEU_CT As String    'Ký hiệu chứng từ
        Private _SO_CT As String        'Số chứng từ

        Private _NGAY_HT As String      'Ngày hệ thống

        Private _MA_NH_A As String      'x  'Mã ngân hàng chuyển(MSB, DongA, ABB, EXIM,...)


        Private _LAN_IN As String       'x  'Số lần in chứng từ

        Private _TRANG_THAI As String   'Trạng thái chứng từ

        Private _PT_TINHPHI As String   'x  'Phương pháp tình phí
        Private _PHI_GD As String       'x  'Phí giao dịch
        Private _PHI_VAT As String      'x  'VAT của phí giao dịch
        Private _PHI_GD2 As String      'x  'Phí giao dịch(kiểm đếm,...)
        Private _PHI_VAT2 As String     'x  'VAT của phí kiểm đếm
        Private _SO_CT_NH As String
        Private _TT_SP As String     'x  'Trạng thái gửi song phuong

        Private _MA_TINH_NNTHUE As String     'x  'ma tinh nguoi nop thue
        Private _TEN_TINH_NNTHUE As String     'x  'ten tinh nguoi nop thue
        Private _MA_HUYEN_NNTHUE As String     'x  'ma  huyen nguwoi nop thue
        Private _TEN_HUYEN_NNTHUE As String  ' ten huyen nguoi nop thue

        Private _MA_TINH_NNTIEN As String     'x  'ma tinh nguoi nop tien
        Private _TEN_TINH_NNTIEN As String     'x  'ten tinh nguoi nop tien
        Private _MA_HUYEN_NNTIEN As String     'x  'ma  huyen nguwoi nop tien
        Private _TEN_HUYEN_NNTIEN As String  ' ten huyen nguoi nop tien
        Private _MA_CQQD As String
        Private _TEN_CQQD As String
        Private _SO_QD As String
        Private _NGAY_QD As String
        Private _MA_LHTHU As String
        Private _TEN_LHTHU As String

        Private _LY_DO_VPHC As String
        Private dbTTIEN_VPHC As String
        Private dbTTIEN_NOP_CHAM As String
        Private dbTTIEN_CTBL As String
        Private _PT_TT As String

        Private _MA_NTK As String
        Private _HT_THU As String
        Private _MA_DV As String = "" 'MA DON VI THU HUONG
        Private _PRODUCT As String = "" 'SAN PHAM
        Private _DIENGIAI As String = "" 'SAN PHAM
        Private _SO_THAM_CHIEU As String = "" 'so tham chieu
        Private _LY_DO_CHAM_NOP As String = ""
        Private _MA_CHUONG As String = ""
        Private _MA_NDKT As String = ""
        Private _KY_THUE As String = ""
        Private _NOI_DUNG As String = ""
        Private _MA_NH_KB As String = ""
        Private _SERI As String = ""
        Private _PAYMENT As String = "0"
        Public Property HT_THU() As String
            Get
                Return _HT_THU
            End Get
            Set(ByVal value As String)
                _HT_THU = value
            End Set
        End Property
        Public Property MA_NTK() As String
            Get
                Return _MA_NTK
            End Get
            Set(ByVal value As String)
                _MA_NTK = value
            End Set
        End Property
        Public Property LY_DO_VPHC() As String
            Get
                Return _LY_DO_VPHC
            End Get
            Set(ByVal value As String)
                _LY_DO_VPHC = value
            End Set
        End Property

        Public Property TTIEN_VPHC() As String
            Get
                Return dbTTIEN_VPHC
            End Get
            Set(ByVal value As String)
                dbTTIEN_VPHC = value
            End Set
        End Property
        Public Property TTIEN_NOP_CHAM() As String
            Get
                Return dbTTIEN_NOP_CHAM
            End Get
            Set(ByVal value As String)
                dbTTIEN_NOP_CHAM = value
            End Set
        End Property
        Public Property TTIEN_CTBL() As String
            Get
                Return dbTTIEN_CTBL
            End Get
            Set(ByVal value As String)
                dbTTIEN_CTBL = value
            End Set
        End Property
        Public Property MA_TINH_NNTHUE() As String
            Get
                Return _MA_TINH_NNTHUE
            End Get
            Set(ByVal value As String)
                _MA_TINH_NNTHUE = value
            End Set
        End Property
        Public Property TEN_TINH_NNTHUE() As String
            Get
                Return _TEN_TINH_NNTHUE
            End Get
            Set(ByVal value As String)
                _TEN_TINH_NNTHUE = value
            End Set
        End Property
        Public Property MA_HUYEN_NNTHUE() As String
            Get
                Return _MA_HUYEN_NNTHUE
            End Get
            Set(ByVal value As String)
                _MA_HUYEN_NNTHUE = value
            End Set
        End Property
        Public Property TEN_HUYEN_NNTHUE() As String
            Get
                Return _TEN_HUYEN_NNTHUE
            End Get
            Set(ByVal value As String)
                _TEN_HUYEN_NNTHUE = value
            End Set
        End Property
        Public Property MA_TINH_NNTIEN() As String
            Get
                Return _MA_TINH_NNTIEN
            End Get
            Set(ByVal value As String)
                _MA_TINH_NNTIEN = value
            End Set
        End Property
        Public Property TEN_TINH_NNTIEN() As String
            Get
                Return _TEN_TINH_NNTIEN
            End Get
            Set(ByVal value As String)
                _TEN_TINH_NNTIEN = value
            End Set
        End Property
        Public Property MA_HUYEN_NNTIEN() As String
            Get
                Return _MA_HUYEN_NNTIEN
            End Get
            Set(ByVal value As String)
                _MA_HUYEN_NNTIEN = value
            End Set
        End Property
        Public Property TEN_HUYEN_NNTIEN() As String
            Get
                Return _TEN_HUYEN_NNTIEN
            End Get
            Set(ByVal value As String)
                _TEN_HUYEN_NNTIEN = value
            End Set
        End Property
        Public Property TT_SP() As String
            Get
                Return _TT_SP
            End Get
            Set(ByVal value As String)
                _TT_SP = value
            End Set
        End Property

        Public Property SO_CT_NH() As String
            Get
                Return _SO_CT_NH
            End Get
            Set(ByVal value As String)
                _SO_CT_NH = value
            End Set
        End Property




        Public Property REMARK() As String
            Get
                Return _REMARK
            End Get
            Set(ByVal value As String)
                _REMARK = value
            End Set
        End Property

        Public Property MA_CQQD() As String
            Get
                Return _MA_CQQD
            End Get
            Set(ByVal value As String)
                _MA_CQQD = value
            End Set
        End Property

        Public Property TEN_CQQD() As String
            Get
                Return _TEN_CQQD
            End Get
            Set(ByVal value As String)
                _TEN_CQQD = value
            End Set
        End Property

        Public Property SO_QD() As String
            Get
                Return _SO_QD
            End Get
            Set(ByVal value As String)
                _SO_QD = value
            End Set
        End Property

        Public Property NGAY_QD() As String
            Get
                Return _NGAY_QD
            End Get
            Set(ByVal value As String)
                _NGAY_QD = value
            End Set
        End Property



        Public Property NGAY_KS() As String
            Get
                Return _NGAY_KS
            End Get
            Set(ByVal value As String)
                _NGAY_KS = value
            End Set
        End Property



        Public Property PHI_VAT2() As String
            Get
                Return _PHI_VAT2
            End Get
            Set(ByVal value As String)
                _PHI_VAT2 = value
            End Set
        End Property

        Public Property PHI_GD2() As String
            Get
                Return _PHI_GD2
            End Get
            Set(ByVal value As String)
                _PHI_GD2 = value
            End Set
        End Property

        Public Property PHI_VAT() As String
            Get
                Return _PHI_VAT
            End Get
            Set(ByVal value As String)
                _PHI_VAT = value
            End Set
        End Property

        Public Property PHI_GD() As String
            Get
                Return _PHI_GD
            End Get
            Set(ByVal value As String)
                _PHI_GD = value
            End Set
        End Property

        Public Property PT_TINHPHI() As String
            Get
                Return _PT_TINHPHI
            End Get
            Set(ByVal value As String)
                _PT_TINHPHI = value
            End Set
        End Property

        Public Property TRANG_THAI() As String
            Get
                Return _TRANG_THAI
            End Get
            Set(ByVal value As String)
                _TRANG_THAI = value
            End Set
        End Property

        Public Property TEN_NH_TT() As String
            Get
                Return _TEN_NH_TT
            End Get
            Set(ByVal value As String)
                _TEN_NH_TT = value
            End Set
        End Property

        Public Property MA_NH_TT() As String
            Get
                Return _MA_NH_TT
            End Get
            Set(ByVal value As String)
                _MA_NH_TT = value
            End Set
        End Property

        Public Property TEN_NH_B() As String
            Get
                Return _TEN_NH_B
            End Get
            Set(ByVal value As String)
                _TEN_NH_B = value
            End Set
        End Property

        Public Property MA_NH_B() As String
            Get
                Return _MA_NH_B
            End Get
            Set(ByVal value As String)
                _MA_NH_B = value
            End Set
        End Property

        Public Property MA_NH_A() As String
            Get
                Return _MA_NH_A
            End Get
            Set(ByVal value As String)
                _MA_NH_A = value
            End Set
        End Property

        Public Property NGAY_KH_NH() As String
            Get
                Return _NGAY_KH_NH
            End Get
            Set(ByVal value As String)
                _NGAY_KH_NH = value
            End Set
        End Property

        Public Property MA_LHTHU() As String
            Get
                Return _MA_LHTHU
            End Get
            Set(ByVal value As String)
                _MA_LHTHU = value
            End Set
        End Property
        Public Property TEN_LHTHU() As String
            Get
                Return _TEN_LHTHU
            End Get
            Set(ByVal value As String)
                _TEN_LHTHU = value
            End Set
        End Property

        Public Property TK_KH_NH() As String
            Get
                Return _TK_KH_NH
            End Get
            Set(ByVal value As String)
                _TK_KH_NH = value
            End Set
        End Property

        Public Property TEN_KH_NH() As String
            Get
                Return _TENTK_KH_NH
            End Get
            Set(ByVal value As String)
                _TENTK_KH_NH = value
            End Set
        End Property
        Public Property SO_BIENLAI() As String
            Get
                Return _SO_BIENLAI
            End Get
            Set(ByVal value As String)
                _SO_BIENLAI = value
            End Set
        End Property
        Public Property TTIEN_NT() As String
            Get
                Return _TTIEN_NT
            End Get
            Set(ByVal value As String)
                _TTIEN_NT = value
            End Set
        End Property

        Public Property TTIEN() As String
            Get
                Return _TTIEN
            End Get
            Set(ByVal value As String)
                _TTIEN = value
            End Set
        End Property

        Public Property TY_GIA() As String
            Get
                Return _TY_GIA
            End Get
            Set(ByVal value As String)
                _TY_GIA = value
            End Set
        End Property

        Public Property MA_NT() As String
            Get
                Return _MA_NT
            End Get
            Set(ByVal value As String)
                _MA_NT = value
            End Set
        End Property







        Public Property MA_CQTHU() As String
            Get
                Return _MA_CQTHU
            End Get
            Set(ByVal value As String)
                _MA_CQTHU = value
            End Set
        End Property

        Public Property NGAY_CT() As String
            Get
                Return _NGAY_CT
            End Get
            Set(ByVal value As String)
                _NGAY_CT = value
            End Set
        End Property

        Public Property CQ_QD() As String
            Get
                Return _CQ_QD
            End Get
            Set(ByVal value As String)
                _CQ_QD = value
            End Set
        End Property



        Public Property DC_NNTHUE() As String
            Get
                Return _DC_NNTHUE
            End Get
            Set(ByVal value As String)
                _DC_NNTHUE = value
            End Set
        End Property

        Public Property TEN_NNTHUE() As String
            Get
                Return _TEN_NNTHUE
            End Get
            Set(ByVal value As String)
                _TEN_NNTHUE = value
            End Set
        End Property

        Public Property MA_NNTHUE() As String
            Get
                Return _MA_NNTHUE
            End Get
            Set(ByVal value As String)
                _MA_NNTHUE = value
            End Set
        End Property

        Public Property MA_DBHC() As String
            Get
                Return _MA_DBHC
            End Get
            Set(ByVal value As String)
                _MA_DBHC = value
            End Set
        End Property

        Public Property TEN_DBHC() As String
            Get
                Return _TEN_DBHC
            End Get
            Set(ByVal value As String)
                _TEN_DBHC = value
            End Set
        End Property

        Public Property DC_NNTIEN() As String
            Get
                Return _DC_NNTIEN
            End Get
            Set(ByVal value As String)
                _DC_NNTIEN = value
            End Set
        End Property

        Public Property TEN_NNTIEN() As String
            Get
                Return _TEN_NNTIEN
            End Get
            Set(ByVal value As String)
                _TEN_NNTIEN = value
            End Set
        End Property

        Public Property MA_NNTIEN() As String
            Get
                Return _MA_NNTIEN
            End Get
            Set(ByVal value As String)
                _MA_NNTIEN = value
            End Set
        End Property

        Public Property MA_LTHUE() As String
            Get
                Return _MA_LTHUE
            End Get
            Set(ByVal value As String)
                _MA_LTHUE = value
            End Set
        End Property
        Public Property TEN_LTHUE() As String
            Get
                Return _TEN_LTHUE
            End Get
            Set(ByVal value As String)
                _TEN_LTHUE = value
            End Set
        End Property

        Public Property SO_CT() As String
            Get
                Return _SO_CT
            End Get
            Set(ByVal value As String)
                _SO_CT = value
            End Set
        End Property

        Public Property KYHIEU_CT() As String
            Get
                Return _KYHIEU_CT
            End Get
            Set(ByVal value As String)
                _KYHIEU_CT = value
            End Set
        End Property

        Public Property MA_DTHU() As String
            Get
                Return _MA_DTHU
            End Get
            Set(ByVal value As String)
                _MA_DTHU = value
            End Set
        End Property
        Public Property TEN_DTHU() As String
            Get
                Return _TEN_DTHU
            End Get
            Set(ByVal value As String)
                _TEN_DTHU = value
            End Set
        End Property

        Public Property SO_BT() As String
            Get
                Return _SO_BT
            End Get
            Set(ByVal value As String)
                _SO_BT = value
            End Set
        End Property

        Public Property MA_CN() As String
            Get
                Return _MA_CN
            End Get
            Set(ByVal value As String)
                _MA_CN = value
            End Set
        End Property



        Public Property MA_KS() As String
            Get
                Return _MA_KS
            End Get
            Set(ByVal value As String)
                _MA_KS = value
            End Set
        End Property

        Public Property MA_NV() As String
            Get
                Return _MA_NV
            End Get
            Set(ByVal value As String)
                _MA_NV = value
            End Set
        End Property


        Public Property TK_CO() As String
            Get
                Return _TK_CO
            End Get
            Set(ByVal value As String)
                _TK_CO = value
            End Set
        End Property

        Public Property TEN_CQTHU() As String
            Get
                Return _TEN_CQTHU
            End Get
            Set(ByVal value As String)
                _TEN_CQTHU = value
            End Set
        End Property

        Public Property TK_NO() As String
            Get
                Return _TK_NO
            End Get
            Set(ByVal value As String)
                _TK_NO = value
            End Set
        End Property



        Public Property NGAY_KB() As String
            Get
                Return _NGAY_KB
            End Get
            Set(ByVal value As String)
                _NGAY_KB = value
            End Set
        End Property

        Public Property SHKB() As String
            Get
                Return _SHKB
            End Get
            Set(ByVal value As String)
                _SHKB = value
            End Set
        End Property

        Public Property TEN_KB() As String
            Get
                Return _TEN_KB
            End Get
            Set(ByVal value As String)
                _TEN_KB = value
            End Set
        End Property

        Public Property NGAY_NTIEN() As String
            Get
                Return _NGAY_NTIEN
            End Get
            Set(ByVal value As String)
                _NGAY_NTIEN = value
            End Set
        End Property
        Public Property PT_TT() As String
            Get
                Return _PT_TT
            End Get
            Set(ByVal value As String)
                _PT_TT = value
            End Set
        End Property
        Public Property MA_DV() As String
            Get
                Return _MA_DV
            End Get
            Set(ByVal value As String)
                _MA_DV = value
            End Set
        End Property
        Public Property PRODUCT() As String
            Get
                Return _PRODUCT
            End Get
            Set(ByVal value As String)
                _PRODUCT = value
            End Set
        End Property
        Public Property DIENGIAI() As String
            Get
                Return _DIENGIAI
            End Get
            Set(ByVal value As String)
                _DIENGIAI = value
            End Set
        End Property
        Public Property SO_THAM_CHIEU() As String
            Get
                Return _SO_THAM_CHIEU

            End Get
            Set(ByVal value As String)
                _SO_THAM_CHIEU = value
            End Set
        End Property

        Public Property MA_CHUONG() As String
            Get
                Return _MA_CHUONG

            End Get
            Set(ByVal value As String)
                _MA_CHUONG = value
            End Set
        End Property
        Public Property MA_NDKT() As String
            Get
                Return _MA_NDKT

            End Get
            Set(ByVal value As String)
                _MA_NDKT = value
            End Set
        End Property
        Public Property KY_THUE() As String
            Get
                Return _KY_THUE

            End Get
            Set(ByVal value As String)
                _KY_THUE = value
            End Set
        End Property
        Public Property NOI_DUNG() As String
            Get
                Return _NOI_DUNG

            End Get
            Set(ByVal value As String)
                _NOI_DUNG = value
            End Set
        End Property

        Public Property MA_NH_KB() As String
            Get
                Return _MA_NH_KB

            End Get
            Set(ByVal value As String)
                _MA_NH_KB = value
            End Set
        End Property
        Public Property SERI() As String
            Get
                Return _SERI

            End Get
            Set(ByVal value As String)
                _SERI = value
            End Set
        End Property
        Public Property PAYMENT() As String
            Get
                Return _PAYMENT

            End Get
            Set(ByVal value As String)
                _PAYMENT = value
            End Set
        End Property
    End Class
End Namespace

