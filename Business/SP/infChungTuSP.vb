﻿Namespace ChungTuSP
    Public Class infChungTuSP
        Private _HDR As infChungTuSP_HDR
        Private _ListDTL As List(Of infChungTuSP_DTL)

        Public Sub New()
        End Sub
        Public Sub New(ByVal HDR As infChungTuSP_HDR, ByVal ListDTL As List(Of infChungTuSP_DTL))
            Me.HDR = HDR
            Me.ListDTL = ListDTL
        End Sub
        Public Property HDR() As infChungTuSP_HDR
            Get
                Return _HDR
            End Get
            Set(ByVal value As infChungTuSP_HDR)
                _HDR = value
            End Set
        End Property

        Public Property ListDTL() As List(Of infChungTuSP_DTL)
            Get
                Return _ListDTL
            End Get
            Set(ByVal value As List(Of infChungTuSP_DTL))
                _ListDTL = value
            End Set
        End Property

    End Class
End Namespace

