﻿Namespace ChungTuBL
    Public Class infChungTuBL_HDR
        Private strMa_NNThue As String
        Private strTen_NNThue As String
        Private strDC_NNThue As String
        Private strMa_NNTien As String
        Private strTen_NNTien As String
        Private strDC_NNTien As String
        Private strNgayNTien As String
        Private strSHKB As String
        Private strTen_kb As String
        Private iNgay_KB As Integer

        Private strMaDThu As String
        Private strSoBT As String
        Private strNgayCTu As String
        Private strTKCo As String
        Private strTKNo As String
        Private strMaLThue As String
        Private strCQQD As String

        Private strMa_CQThu As String
        Private strTen_CQThu As String
        Private strMaDBHC As String
        Private strTenDBHC As String
        Private strTK_KH_NH As String
        Private strTenTK_KH_NH As String
        Private strSoBienLai As String
        Private strNgay_KH_NH As String
        Private strMa_NH_TT As String
        Private strTen_NH_TT As String
        Private strMa_NH_B As String
        Private strTen_NH_B As String
        Private strMaNT As String
        Private strTen_NT As String
        Private strTyGia As String
        Private dblTTien As Double
        Private dblTTien_NT As Double

        Private strRemark As String

        Private strMa_NV As String
        Private strMa_NVKS As String
        Private strNgayHT As String
        Private strNgayHThien As String
        Private strNgayKS As String
        Private strMa_CN As String
        Private strMa_NH_A As String

        Private strSo_CT_NH As String
        Private strSO_CT As String
        Private strKH_ChungTu As String
        Private dTG_KY As String
        Private strTrang_thai As String
        Private strTT_SP As String
        Private strLy_do As String
        Private strLy_do_huy As String
        Private strSo_BThu As String

        Private strMa_Tinh_NNThue As String
        Private strTen_Tinh_NNThue As String
        Private strMa_Huyen_NNThue As String
        Private strTen_Huyen_NNThue As String

        Private strMa_Tinh_NNTien As String
        Private strTen_Tinh_NNTien As String
        Private strMa_Huyen_NNTien As String
        Private strTen_Huyen_NNTien As String
        Private strMa_CQQD As String
        Private strTen_CQQD As String
        Private strSoQD As String
        Private strNgay_QD As String
        Private strMA_LHTHU As String
        Private strTEN_LHTHU As String

        Private strLY_DO_VPHC As String
        Private dbTTIEN_VPHC As Double
        Private dbTTIEN_NOP_CHAM As Double
        Private dbTTIEN_CTBL As Double
        Private strPT_TT As String

        Private strMA_NTK As String
        Private strHT_THU As String
        Private strPRODUCT As String = "ETAX"
        Private strSERI As String = ""
        Private strSO_THAMCHIEU As String = ""
        Private strMA_NH_KB As String = ""
        Private strPAYMENT As String = "0"

        Public Property HT_THU() As String
            Get
                Return strHT_THU
            End Get
            Set(ByVal value As String)
                strHT_THU = value
            End Set
        End Property
        Public Property MA_NTK() As String
            Get
                Return strMA_NTK
            End Get
            Set(ByVal Value As String)
                strMA_NTK = Value
            End Set
        End Property
        Public Property MA_NNTHUE() As String
            Get
                Return strMa_NNThue
            End Get
            Set(ByVal Value As String)
                strMa_NNThue = Value
            End Set
        End Property

        Public Property TEN_NNTHUE() As String
            Get
                Return strTen_NNThue
            End Get
            Set(ByVal Value As String)
                strTen_NNThue = Value
            End Set
        End Property
        Public Property DC_NNTHUE() As String
            Get
                Return strDC_NNThue
            End Get
            Set(ByVal Value As String)
                strDC_NNThue = Value
            End Set
        End Property
        Public Property MA_NNTIEN() As String
            Get
                Return strMa_NNTien
            End Get
            Set(ByVal Value As String)
                strMa_NNTien = Value
            End Set
        End Property
        Public Property TEN_NNTIEN() As String
            Get
                Return strTen_NNTien
            End Get
            Set(ByVal Value As String)
                strTen_NNTien = Value
            End Set
        End Property
        Public Property DC_NNTIEN() As String
            Get
                Return strDC_NNTien
            End Get
            Set(ByVal Value As String)
                strDC_NNTien = Value
            End Set
        End Property
        Public Property NGAY_NTIEN() As String
            Get
                Return strNgayNTien
            End Get
            Set(ByVal Value As String)
                strNgayNTien = Value
            End Set
        End Property
        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property
        Public Property TEN_KB() As String
            Get
                Return strTen_kb
            End Get
            Set(ByVal Value As String)
                strTen_kb = Value
            End Set

        End Property
        Public Property NGAY_KB() As String
            Get
                Return iNgay_KB
            End Get
            Set(ByVal Value As String)
                iNgay_KB = Value
            End Set
        End Property
        Public Property MA_DTHU() As String
            Get
                Return strMaDThu
            End Get
            Set(ByVal Value As String)
                strMaDThu = Value
            End Set
        End Property
        Public Property SO_BT() As String
            Get
                Return strSoBT
            End Get
            Set(ByVal Value As String)
                strSoBT = Value
            End Set
        End Property
        Public Property NGAY_CT() As String
            Get
                Return strNgayCTu
            End Get
            Set(ByVal Value As String)
                strNgayCTu = Value
            End Set
        End Property
        Public Property TK_CO() As String
            Get
                Return strTKCo
            End Get
            Set(ByVal Value As String)
                strTKCo = Value
            End Set
        End Property
        Public Property TK_NO() As String
            Get
                Return strTKNo
            End Get
            Set(ByVal Value As String)
                strTKNo = Value
            End Set
        End Property
        Public Property MA_LTHUE() As String
            Get
                Return strMaLThue
            End Get
            Set(ByVal Value As String)
                strMaLThue = Value
            End Set
        End Property

        Public Property CQ_QD() As String
            Get
                Return strCQQD
            End Get
            Set(ByVal Value As String)
                strCQQD = Value
            End Set
        End Property
        Public Property MA_CQTHU() As String
            Get
                Return strMa_CQThu
            End Get
            Set(ByVal Value As String)
                strMa_CQThu = Value
            End Set
        End Property
        Public Property TEN_CQTHU() As String
            Get
                Return strTen_CQThu
            End Get
            Set(ByVal Value As String)
                strTen_CQThu = Value
            End Set
        End Property
        Public Property MA_DBHC() As String
            Get
                Return strMaDBHC
            End Get
            Set(ByVal Value As String)
                strMaDBHC = Value
            End Set
        End Property
        Public Property TEN_DBHC() As String
            Get
                Return strTenDBHC
            End Get
            Set(ByVal Value As String)
                strTenDBHC = Value
            End Set
        End Property
        Public Property TK_KH_NH() As String
            Get
                Return strTK_KH_NH
            End Get
            Set(ByVal Value As String)
                strTK_KH_NH = Value
            End Set
        End Property
        Public Property TENTK_KH_NH() As String
            Get
                Return strTenTK_KH_NH
            End Get
            Set(ByVal Value As String)
                strTenTK_KH_NH = Value
            End Set
        End Property
        Public Property SO_BIENLAI() As String
            Get
                Return strSoBienLai
            End Get
            Set(ByVal Value As String)
                strSoBienLai = Value
            End Set
        End Property
        Public Property NGAY_KH_NH() As String
            Get
                Return strNgay_KH_NH
            End Get
            Set(ByVal Value As String)
                strNgay_KH_NH = Value
            End Set
        End Property

        Public Property MA_NH_TT() As String
            Get
                Return strMa_NH_TT
            End Get
            Set(ByVal Value As String)
                strMa_NH_TT = Value
            End Set
        End Property
        Public Property TEN_NH_TT() As String
            Get
                Return strTen_NH_TT
            End Get
            Set(ByVal Value As String)
                strTen_NH_TT = Value
            End Set
        End Property
        Public Property MA_NH_B() As String
            Get
                Return strMa_NH_B
            End Get
            Set(ByVal Value As String)
                strMa_NH_B = Value
            End Set
        End Property
        Public Property TEN_NH_B() As String
            Get
                Return strTen_NH_B
            End Get
            Set(ByVal Value As String)
                strTen_NH_B = Value
            End Set
        End Property
        Public Property MA_NT() As String
            Get
                Return strMaNT
            End Get
            Set(ByVal Value As String)
                strMaNT = Value
            End Set
        End Property
        Public Property TY_GIA() As String
            Get
                Return strTyGia
            End Get
            Set(ByVal Value As String)
                strTyGia = Value
            End Set
        End Property
        Public Property TTIEN() As Double
            Get
                Return dblTTien
            End Get
            Set(ByVal Value As Double)
                dblTTien = Value
            End Set
        End Property
        Public Property TTIEN_NT() As Double
            Get
                Return dblTTien_NT
            End Get
            Set(ByVal Value As Double)
                dblTTien_NT = Value
            End Set
        End Property
       

        Public Property REMARK() As String
            Get
                Return strRemark
            End Get
            Set(ByVal Value As String)
                strRemark = Value
            End Set
        End Property
        Public Property MA_NV() As String
            Get
                Return strMa_NV
            End Get
            Set(ByVal Value As String)
                strMa_NV = Value
            End Set
        End Property
        Public Property MA_KS() As String
            Get
                Return strMa_NVKS
            End Get
            Set(ByVal Value As String)
                strMa_NVKS = Value
            End Set
        End Property
        Public Property MA_CN() As String
            Get
                Return strMa_CN
            End Get
            Set(ByVal Value As String)
                strMa_CN = Value
            End Set
        End Property
        Public Property MA_NH_A() As String
            Get
                Return strMa_NH_A
            End Get
            Set(ByVal Value As String)
                strMa_NH_A = Value
            End Set
        End Property
      
        Public Property SO_CT_NH() As String
            Get
                Return strSo_CT_NH
            End Get
            Set(ByVal Value As String)
                strSo_CT_NH = Value
            End Set
        End Property
        Public Property SO_CT() As String
            Get
                Return strSO_CT
            End Get
            Set(ByVal Value As String)
                strSO_CT = Value
            End Set
        End Property
        Public Property KYHIEU_CT() As String
            Get
                Return strKH_ChungTu
            End Get
            Set(ByVal Value As String)
                strKH_ChungTu = Value
            End Set
        End Property
        Public Property TG_KY() As String
            Get
                Return dTG_KY
            End Get
            Set(ByVal Value As String)
                dTG_KY = Value
            End Set
        End Property
        Public Property NGAY_HT() As String
            Get
                Return strNgayHT
            End Get
            Set(ByVal Value As String)
                strNgayHT = Value
            End Set
        End Property
        Public Property NGAY_HTHIEN() As String
            Get
                Return strNgayHThien
            End Get
            Set(ByVal Value As String)
                strSo_CT_NH = Value
            End Set
        End Property
        Public Property NGAYKS() As String
            Get
                Return strNgayKS
            End Get
            Set(ByVal Value As String)
                strNgayKS = Value
            End Set
        End Property
        Public Property TRANG_THAI() As String
            Get
                Return strTrang_thai
            End Get
            Set(ByVal Value As String)
                strTrang_thai = Value
            End Set
        End Property
        Public Property TT_SP() As String
            Get
                Return strTT_SP
            End Get
            Set(ByVal Value As String)
                strTT_SP = Value
            End Set
        End Property
        Public Property LY_DO() As String
            Get
                Return strLy_do
            End Get
            Set(ByVal Value As String)
                strLy_do = Value
            End Set
        End Property
        Public Property LY_DO_HUY() As String
            Get
                Return strLy_do_huy
            End Get
            Set(ByVal Value As String)
                strLy_do_huy = Value
            End Set
        End Property
        Public Property SO_BTHU() As String
            Get
                Return strSo_BThu
            End Get
            Set(ByVal Value As String)
                strSo_BThu = Value
            End Set
        End Property
        Public Property MA_TINH_NNTHUE() As String
            Get
                Return strMa_Tinh_NNThue
            End Get
            Set(ByVal Value As String)
                strMa_Tinh_NNThue = Value
            End Set
        End Property
        Public Property TEN_TINH_NNTHUE() As String
            Get
                Return strTen_Tinh_NNThue
            End Get
            Set(ByVal Value As String)
                strTen_Tinh_NNThue = Value
            End Set
        End Property
        Public Property MA_HUYEN_NNTHUE() As String
            Get
                Return strMa_Huyen_NNThue
            End Get
            Set(ByVal Value As String)
                strMa_Huyen_NNThue = Value
            End Set
        End Property
        Public Property TEN_HUYEN_NNTHUE() As String
            Get
                Return strTen_Huyen_NNThue
            End Get
            Set(ByVal Value As String)
                strTen_Huyen_NNThue = Value
            End Set
        End Property
        Public Property MA_TINH_NNTIEN() As String
            Get
                Return strMa_Tinh_NNTien
            End Get
            Set(ByVal Value As String)
                strMa_Tinh_NNTien = Value
            End Set
        End Property
        Public Property TEN_TINH_NNTIEN() As String
            Get
                Return strTen_Tinh_NNTien
            End Get
            Set(ByVal Value As String)
                strTen_Tinh_NNTien = Value
            End Set
        End Property
        Public Property MA_HUYEN_NNTIEN() As String
            Get
                Return strMa_Huyen_NNTien
            End Get
            Set(ByVal Value As String)
                strMa_Huyen_NNTien = Value
            End Set
        End Property
        Public Property TEN_HUYEN_NNTIEN() As String
            Get
                Return strTen_Huyen_NNTien
            End Get
            Set(ByVal Value As String)
                strTen_Huyen_NNTien = Value
            End Set
        End Property
        Public Property MA_CQQD() As String
            Get
                Return strMa_CQQD
            End Get
            Set(ByVal Value As String)
                strMa_CQQD = Value
            End Set
        End Property
        Public Property TEN_CQQD() As String
            Get
                Return strTen_CQQD
            End Get
            Set(ByVal Value As String)
                strTen_CQQD = Value
            End Set
        End Property
        Public Property SO_QD() As String
            Get
                Return strSoQD
            End Get
            Set(ByVal Value As String)
                strSoQD = Value
            End Set
        End Property
        Public Property NGAY_QD() As String
            Get
                Return strNgay_QD
            End Get
            Set(ByVal Value As String)
                strNgay_QD = Value
            End Set
        End Property
        Public Property MA_LHTHU() As String
            Get
                Return strMA_LHTHU
            End Get
            Set(ByVal Value As String)
                strMA_LHTHU = Value
            End Set
        End Property
        Public Property TEN_LHTHU() As String
            Get
                Return strTEN_LHTHU
            End Get
            Set(ByVal Value As String)
                strTEN_LHTHU = Value
            End Set
        End Property
        Public Property LY_DO_VPHC() As String
            Get
                Return strLY_DO_VPHC
            End Get
            Set(ByVal Value As String)
                strLY_DO_VPHC = Value
            End Set
        End Property
        Public Property TTIEN_VPHC() As Double
            Get
                Return dbTTIEN_VPHC
            End Get
            Set(ByVal Value As Double)
                dbTTIEN_VPHC = Value
            End Set
        End Property
        Public Property PT_TT() As String
            Get
                Return strPT_TT
            End Get
            Set(ByVal Value As String)
                strPT_TT = Value
            End Set
        End Property
        Public Property TTIEN_NOP_CHAM() As Double
            Get
                Return dbTTIEN_NOP_CHAM
            End Get
            Set(ByVal Value As Double)
                dbTTIEN_NOP_CHAM = Value
            End Set
        End Property
        Public Property TTIEN_CTBL() As Double
            Get
                Return dbTTIEN_CTBL
            End Get
            Set(ByVal Value As Double)
                dbTTIEN_CTBL = Value
            End Set
        End Property
        Public Property PRODUCT() As String
            Get
                Return strPRODUCT
            End Get
            Set(ByVal Value As String)
                strPRODUCT = Value
            End Set
        End Property
        Public Property SERI() As String
            Get
                Return strSERI
            End Get
            Set(ByVal Value As String)
                strSERI = Value
            End Set
        End Property
        Public Property SO_THAMCHIEU() As String
            Get
                Return strSO_THAMCHIEU
            End Get
            Set(ByVal Value As String)
                strSO_THAMCHIEU = Value
            End Set
        End Property
        Public Property MA_NH_KB() As String
            Get
                Return strMA_NH_KB
            End Get
            Set(ByVal Value As String)
                strMA_NH_KB = Value
            End Set
        End Property
        Public Property PAYMENT() As String
            Get
                Return strPAYMENT
            End Get
            Set(ByVal Value As String)
                strPAYMENT = Value
            End Set
        End Property


    End Class
End Namespace
