﻿Imports VBOracleLib

Namespace ChungTuSP
    Public Class daChungTu
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function CTU_GetListKiemSoat(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, _
          Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "", Optional ByVal strKenhCT As String = "") As DataSet
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strWhereLoaiThue As String = ""

                strSQL = " Select hdr.SHKB,hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BT,hdr.TRANG_THAI, hdr.SO_BTHU, hdr.TTIEN,hdr.TTien_NT, hdr.MA_NV, " & _
                " hdr.NGAY_KB, hdr.MA_DTHU, hdr.TT_SP, hdr.MA_NNTHUE,nv.TEN_DN, hdr.so_ct_nh,hdr.ma_ks, hdr.lan_ks " & _
                         " From tcs_ctu_songphuong_hdr hdr, tcs_dm_nhanvien nv " & _
                         " Where hdr.ma_nv=nv.ma_nv and nv.tinh_trang='0' and hdr.NGAY_KB = " & strNgayCT & " "
                '" Where hdr.ma_nv=nv.ma_nv and nv.tinh_trang='0' and hdr.NGAY_KB = " & strNgayCT & strWhereLoaiThue
                If (strTrangThai.Trim <> "") Then
                    If strTrangThai = "02" Then
                        strSQL = strSQL & " and hdr.Trang_thai='02' "
                    ElseIf strTrangThai = "030" Then
                        strSQL = strSQL & " and hdr.Trang_thai='03' and hdr.tt_sp='0' "
                    ElseIf strTrangThai = "031" Then
                        strSQL = strSQL & " and hdr.Trang_thai='03' and hdr.tt_sp='1' "
                    ElseIf strTrangThai = "04" Then
                        strSQL = strSQL & " and hdr.Trang_thai='04' "
                    ElseIf strTrangThai = "06" Then
                        strSQL = strSQL & " and hdr.Trang_thai='06' "
                    End If
                End If
                If Not "".Equals(strMaNV) Then
                    strSQL = strSQL & " and (hdr.Ma_NV='" & strMaNV & "') "
                End If
                If Not "".Equals(strSoBT) Then
                    strSQL = strSQL & " and (hdr.So_BT='" & strSoBT & "') "
                End If
                If Not "".Equals(strMaKS) Then
                    strSQL = strSQL & " and hdr.ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMaKS & "')) "
                End If
                'Kiênvt: Trong trường hợp thu hộ
                If strMaDiemThu.Length > 0 Then
                    strSQL &= " AND (hdr.MA_DTHU='" & strMaDiemThu & "') "
                End If

                strSQL = strSQL & " order by hdr.ma_nv, hdr.so_ct DESC"
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine & "Error StackTrace: " & ex.StackTrace & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_Header_Set(ByVal sSOCT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chung của chứng từ     
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess

                Dim strSQL As String
                strSQL = "select SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,SO_BTHU,KYHIEU_CT,SO_CT,SO_CT_NH,MA_NNTHUE,TEN_NNTHUE,DC_NNTHUE,MA_NNTIEN, " & _
                               "TEN_NNTIEN,DC_NNTIEN,NGAY_NTIEN,TEN_KB,CQ_QD,NGAY_CTU,TK_NO,TK_CO,MA_LTHUE,MA_CQTHU,TEN_CQTHU,MA_DBHC," & _
                               "TEN_DBHC,TK_KH_NH,TO_CHAR(NGAY_KH_NH, 'DD-MM-YYYY') as NGAY_KH_NH,TENKH_NHAN,TK_KH_NHAN,DIACHI_KH_NHAN,NH_GIU_TKNNT,NH_GIU_TKKB,MA_NT,TY_GIA," & _
                               "TTIEN,TTIEN_NT,MA_HQ,TEN_HQ,MA_HQ_PH,TEN_HQ_PH,REMARK,LY_DO,MA_KS,NGAY_HT,TG_KY,MA_NH_A,MA_NH_B,LAN_IN,TRANG_THAI," & _
                               "PT_TT,SO_BK,NGAY_BK,CTU_GNT_BL,PHI_GD,PHI_VAT,PT_TINHPHI,TT_SP,NGAY_KS,LY_DO_HUY,KENH_CT,NGAY_GDV_HUY," & _
                               "MA_HUYKS,NGAY_KSV_HUY,PHI_GD2,PHI_VAT2,TEN_NH_B,DIEN_GIAI,MA_CN,GHI_CHU,TEN_LTHUE,SO_LO,VALUE_DATE," & _
                               "TRAN_TYPE,SAVE_TYPE,NGAY_KSV_CT,NHANVIEN_VL,TTHAI_XL_LAI,NGAY_XL_LAI, ngay_citad, sobt_citad, TEN_NGUOI_CHUYEN, REF_NO, CONTENT_EX  " & _
                               " from tcs_ctu_songphuong_hdr " & _
                               " where so_ct = '" & sSOCT & "' "
                ' "and (a.ma_dthu = '" & key.Ma_Dthu & "') " & _

                '"and (a.so_ct = '" & key.So_CT & "') " & _
                '"and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _

                'Return
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chung chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_ChiTiet_Set_CT_TK(ByVal SO_CT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ---------------------------------------------------- 
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                strSQL = "select MA_NV,SO_BT,SO_TK,to_char(NGAY_TK,'dd/MM/yyyy') as NGAY_TK,SAC_THUE,MA_CHUONG,MA_MUC, " & _
                        " MA_TMUC,NOI_DUNG,KY_THUE,SOTIEN,SOTIEN_NT,SO_CT,ID " & _
                        " FROM TCS_CTU_SONGPHUONG_DTL " & _
                        " where (SO_CT='" & SO_CT & "')"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_Get_TrangThai(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select trang_thai from tcs_ctu_songphuong_hdr " & _
                        "where so_ct = '" & so_ct & "'"

                Dim strTT As String = cnCT.ExecuteSQLScalar(strSQL)

                Return strTT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function

        Public Sub CTU_SetTrangThai(ByVal so_ct As String, ByVal strTT As String, _
                                        Optional ByVal strMaKS As String = "", Optional ByVal strMaHuyKS As String = "", _
                                        Optional ByVal strRMNo As String = "", Optional ByVal strTT_SP As String = "0", _
                                        Optional ByVal strLyDoHuy As String = "", Optional ByVal strNgayKSVCT As Boolean = False)
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strNgayKS As String
                strSQL = "update TCS_CTU_SONGPHUONG_HDR " & _
                    "   set TRANG_THAI = '" & strTT & "'" & _
                    IIf(strMaKS.Length > 0, ",Ma_KS = '" & strMaKS & "'", "") & _
                    IIf(strMaKS.Length > 0, ",ngay_ks=SYSDATE" & "", "") & _
                    IIf(strMaHuyKS.Length > 0, ",Ma_HuyKS = '" & strMaHuyKS & "', NGAY_KSV_HUY=SYSDATE ", "") & _
                    IIf(strTT_SP.Length > 0, ",TT_SP = '" & strTT_SP & "'", "") & _
                    IIf(strLyDoHuy.Length > 0, ", ly_do = '" & strLyDoHuy & "'", "") & _
                    IIf(strNgayKSVCT = True, ", ngay_ksv_ct = SYSDATE", " ") & _
                    "   where so_ct = '" & so_ct & "' "

                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)

                'xem lại có cần cập nhật bảng này ko
                strSQL = "update tcs_chungtu_ttsp set TRANG_THAI = '" & strTT & "'  where so_ct = '" & so_ct & "' "
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi set trạng thái chứng từ")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub
        Public Function CTU_GetListKiemSoatBL(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, _
                  Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "", Optional ByVal strKenhCT As String = "") As DataSet
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strWhereLoaiThue As String = ""

                strSQL = " Select hdr.SHKB,hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BT,hdr.TRANG_THAI, hdr.TTIEN,hdr.TTien_NT, hdr.MA_NV, " & _
                " hdr.NGAY_KB, hdr.MA_DTHU, hdr.TT_SP, hdr.MA_NNTHUE,nv.TEN_DN, hdr.so_ct_nh,hdr.ma_ks, hdr.lan_ks " & _
                         " From tcs_bienlai_hdr hdr, tcs_dm_nhanvien nv " & _
                         " Where hdr.ma_nv=nv.ma_nv and nv.tinh_trang='0' and UPPER(hdr.PRODUCT) ='ETAX' and hdr.NGAY_KB = " & strNgayCT & " "

                If (strTrangThai.Trim <> "") Then
                    If strTrangThai = "02" Then
                        strSQL = strSQL & " and hdr.Trang_thai='02' "
                    ElseIf strTrangThai = "030" Then
                        strSQL = strSQL & " and hdr.Trang_thai='03' and hdr.tt_sp in ('0','2')"
                    ElseIf strTrangThai = "031" Then
                        strSQL = strSQL & " and hdr.Trang_thai='03' and hdr.tt_sp='1'"
                    ElseIf strTrangThai = "04" Then
                        strSQL = strSQL & " and hdr.Trang_thai='04' "
                    ElseIf strTrangThai = "06" Then
                        strSQL = strSQL & " and hdr.Trang_thai='06' "
                    End If

                End If
                If Not "".Equals(strMaNV) Then
                    strSQL = strSQL & " and (hdr.Ma_NV=" & strMaNV & ") "
                End If
                If Not "".Equals(strSoBT) Then
                    strSQL = strSQL & " and (hdr.So_BT=" & strSoBT & ") "
                End If
                If Not "".Equals(strMaKS) Then
                    strSQL = strSQL & " and hdr.ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMaKS & "')) "
                End If

                'Kiênvt: Trong trường hợp thu hộ
                If strMaDiemThu.Length > 0 Then
                    strSQL &= " AND (hdr.MA_DTHU=" & strMaDiemThu & ") "
                End If

                strSQL = strSQL & " order by hdr.ma_nv, hdr.so_bt DESC"
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine & "Error StackTrace: " & ex.StackTrace & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function CTU_Header_Set_BL(ByVal sSOCT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chung của chứng từ     
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess

                Dim strSQL As String
                strSQL = "select PRODUCT, SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,TEN_DTHU,SO_CT,NGAY_CT,KYHIEU_CT,SO_CT_NH,TK_CO,TEN_KB,MA_NNTHUE,TEN_NNTHUE,DC_NNTHUE,MA_NNTIEN,TEN_NNTIEN,DC_NNTIEN, " & _
                               "MA_LTHUE,TEN_LTHUE,MA_CQTHU,TEN_CQTHU,MA_DBHC,TEN_DBHC,MA_NH_A,TEN_NH_A,MA_NH_TT,TEN_NH_TT,MA_NH_GT,TEN_NH_GT,MA_NT," & _
                               "TTIEN,TTIEN_NT,TRANG_THAI,TK_KH_NH,TEN_KH_NH,NGAY_KH_NH,LY_DO,DIEN_GIAI,PT_TT,TT_SP,NGAY_KS,LY_DO_HUY,NGAY_GDV_HUY,MA_HUYKS," & _
                               "NGAY_KSV_HUY,NGAY_KSV_CT,NHANVIEN_VL,TTHAI_XL_LAI,NGAY_XL_LAI,MA_CN,MA_CQQD,TEN_CQQD,NGAY_HT," & _
                               "SO_QD,to_char(NGAY_QD,'dd/MM/yyyy') NGAY_QD,MA_LHTHU,TEN_LHTHU,LY_DO_VPHC,TTIEN_CTBL,TTIEN_VPHC,LY_DO_NOP_CHAM,TTIEN_NOP_CHAM, SO_BIENLAI, ten_huyen_nnthue,ten_tinh_nnthue, ten_huyen_nntien,ten_tinh_nntien, MA_NTK, HT_THU  " & _
                               " from tcs_bienlai_hdr " & _
                               " where so_ct = '" & sSOCT & "' "

                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chung chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_ChiTiet_Set_CT_BL(ByVal SO_CT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin chi tiết của chứng từ.
            ' Tham số: Ký hiệu chứng từ, số chứng từ
            ' Giá trị trả về:
            ' Ngày viết: 26/10/2007
            ' Người viết: Lê Hồng Hà
            ' ---------------------------------------------------- 
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                'strSQL = "select SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_QUY,MA_CHUONG, " & _
                '        " MA_NDKT as MA_TMUC,NOI_DUNG,KY_THUE,SOTIEN,SO_CT,ID " & _
                '        " FROM TCS_BIENLAI_DTL " & _
                '        " where (SO_CT='" & SO_CT & "')"

                strSQL = "select SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,'01' MA_QUY,MA_CHUONG, " & _
                       " MA_NDKT as MA_TMUC,NOI_DUNG,KY_THUE,TTIEN SOTIEN,SO_CT " & _
                       " FROM TCS_BIENLAI_HDR " & _
                       " where (SO_CT='" & SO_CT & "')"


                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_Get_TrangThai_BL(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select trang_thai from tcs_bienlai_hdr " & _
                        "where so_ct = '" & so_ct & "'"

                Dim strTT As String = cnCT.ExecuteSQLScalar(strSQL)

                Return strTT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Sub CTU_SetTrangThai_BL(ByVal so_ct As String, ByVal strTT As String, _
                                       Optional ByVal strMaKS As String = "", Optional ByVal strMaHuyKS As String = "", _
                                       Optional ByVal strRMNo As String = "", Optional ByVal strTT_SP As String = "0", _
                                       Optional ByVal strLyDoHuy As String = "", Optional ByVal strNgayKSVCT As Boolean = False, Optional ByVal strLyDoCT As String = "")
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strNgayKS As String
                strSQL = "update TCS_BIENLAI_HDR " & _
                    "   set TRANG_THAI = '" & strTT & "'" & _
                    IIf(strMaKS.Length > 0, ",Ma_KS = '" & strMaKS & "'", "") & _
                    IIf(strMaKS.Length > 0, ",ngay_ks=SYSDATE" & "", "") & _
                    IIf(strMaHuyKS.Length > 0, ",Ma_HuyKS = '" & strMaHuyKS & "', NGAY_KSV_HUY=SYSDATE ", "") & _
                    IIf(strRMNo.Length > 0, ", so_ct_nh = '" & strRMNo & "'", "") & _
                    IIf(strTT_SP.Length > 0, ",TT_SP = '" & strTT_SP & "'", "") & _
                    IIf(strLyDoHuy.Length > 0, ", LY_DO_HUY = '" & strLyDoHuy & "'", "") & _
                    IIf(strLyDoCT.Length > 0, ", LY_DO = '" & strLyDoCT & "'", "") & _
                    IIf(strNgayKSVCT = True, ", ngay_ksv_ct = SYSDATE", " ") & _
                    "   where so_ct = '" & so_ct & "' "

                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)


            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi set trạng thái chứng từ")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub

        Public Function CTU_IN_BL(ByVal so_ct As String) As DataSet
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                'strSQL = "select rownum as STT,dtl.SO_CT,dtl.noi_dung as NOI_DUNG,dtl.ma_chuong as CC, " & _
                '        "'' as LK, dtl.ma_ndkt as MTM, " & _
                '        " dtl.sotien as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK, " & _
                '         " (select ma_db from tcs_dm_khobac where shkb=dtl.shkb and ROWNUM = 1) AS ma_dbhc " & _
                '        "from tcs_bienlai_dtl dtl " & _
                '        "where   dtl.so_ct = '" & so_ct & "'" & _
                '        " order by rownum "

                strSQL = "select rownum as STT,dtl.SO_CT,dtl.noi_dung as NOI_DUNG,dtl.ma_chuong as CC, " & _
                       "'' as LK, dtl.ma_ndkt as MTM, " & _
                       " dtl.TTIEN as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK, " & _
                        " dtl.ma_dbhc " & _
                       "from tcs_bienlai_HDR dtl " & _
                       "where   dtl.so_ct = '" & so_ct & "'" & _
                       " order by rownum "

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
                'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '  & "Error code: System error!" & vbNewLine _
                '  & "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_IN_BANGKE(ByVal so_ct As String) As DataSet
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select rownum as STT,TEN_NNTHUE,DC_NNTHUE,SO_QD,to_char(NGAY_QD,'dd/MM/yyyy') NGAY_QD,KYHIEU_CT,SO_CT," & _
                         "  to_char(ngay_kh_nh,'dd/MM/yyyy') NGAY_NOP,ttien_vphc TIEN_PHAT,ttien_nop_cham TIENPHAT_NOPCHAM,(ttien_vphc + ttien_nop_cham) as TONG_TIEN   " & _
                        " from tcs_bienlai_hdr where  so_ct = '" & so_ct & "'" & _
                        " order by rownum "

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi in bảng kê biên lai")
                'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '  & "Error code: System error!" & vbNewLine _
                '  & "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_Get_LanIn_BK(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select LAN_IN_BK from tcs_bienlai_hdr " & _
                        "where so_ct = '" & so_ct & "'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = ""
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If
                Return strLanIn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy số lần in của bảng kê biên lai")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Sub CTU_Update_LanIn_BK(ByVal so_ct As String)
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select LAN_IN_BK from tcs_bienlai_hdr " & _
                                "where so_ct = '" & so_ct & "'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = "0"
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If

                If (Not IsNumeric(strLanIn)) Then strLanIn = "0"

                Dim intSoLanIn As Integer = Convert.ToInt16(strLanIn)
                intSoLanIn += 1

                cnCT = New DataAccess
                strSQL = "update tcs_bienlai_hdr set LAN_IN_BK = " & intSoLanIn.ToString() & " " & _
                        "where so_ct = '" & so_ct & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi cập nhật lần in bảng kê")

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Sub
        Public Function CTU_Get_LanIn_CTBL(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select LAN_IN from tcs_bienlai_hdr " & _
                        "where so_ct = '" & so_ct & "'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = ""
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If
                Return strLanIn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy số lần in của bảng kê biên lai")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Sub CTU_Update_LanIn_CTBL(ByVal so_ct As String)
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select LAN_IN from tcs_bienlai_hdr " & _
                                "where so_ct = '" & so_ct & "'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = "0"
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If

                If (Not IsNumeric(strLanIn)) Then strLanIn = "0"

                Dim intSoLanIn As Integer = Convert.ToInt16(strLanIn)
                intSoLanIn += 1

                cnCT = New DataAccess
                strSQL = "update tcs_bienlai_hdr set LAN_IN = " & intSoLanIn.ToString() & " " & _
                        "where so_ct = '" & so_ct & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi cập nhật lần in bảng kê")

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Sub
        Public Function CTU_Get_LanIn_BL(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select LAN_IN_BL from tcs_bienlai_hdr " & _
                        "where so_ct = '" & so_ct & "'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = ""
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If
                Return strLanIn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi lấy số lần in của bảng kê biên lai")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Sub CTU_Update_LanIn_BL(ByVal so_ct As String)
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select LAN_IN_BL from tcs_bienlai_hdr " & _
                                "where so_ct = '" & so_ct & "'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = "0"
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If

                If (Not IsNumeric(strLanIn)) Then strLanIn = "0"

                Dim intSoLanIn As Integer = Convert.ToInt16(strLanIn)
                intSoLanIn += 1

                cnCT = New DataAccess
                strSQL = "update tcs_bienlai_hdr set LAN_IN_BL = " & intSoLanIn.ToString() & " " & _
                        "where so_ct = '" & so_ct & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi cập nhật lần in bảng kê")

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Sub
        Public Function CTU_Get_Valid_LanKS(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select lan_ks from tcs_bienlai_hdr " & _
                        "where so_ct = '" & so_ct & "'"
                Dim strLanKS As String = cnCT.ExecuteSQLScalar(strSQL)

                Return strLanKS
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Function CTU_Get_LanKS(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select lan_ks from tcs_bienlai_hdr " & _
                        "where so_ct = '" & so_ct & "'"
                Dim strLanKS As String = cnCT.ExecuteSQLScalar(strSQL)

                Return strLanKS
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        'CTU_SetLanKS
        Public Sub CTU_SetLanKS(ByVal so_ct As String, ByVal strClickKS As String)
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "update tcs_bienlai_hdr " & _
                        "set lan_ks = '" & strClickKS & "'" & _
                        "where so_ct = '" & so_ct & "'"
                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)

                dr.Close()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi khởi tạo ghi chú chứng từ")

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub
        Public Function CTU_Get_Valid_LanKS_Citad(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select lan_ks from tcs_ctu_songphuong_hdr " & _
                        "where so_ct = '" & so_ct & "'"
                Dim strLanKS As String = cnCT.ExecuteSQLScalar(strSQL)

                Return strLanKS
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Function CTU_Get_LanKS_Citad(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select lan_ks from tcs_ctu_songphuong_hdr " & _
                        "where so_ct = '" & so_ct & "'"
                Dim strLanKS As String = cnCT.ExecuteSQLScalar(strSQL)

                Return strLanKS
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        'CTU_SetLanKS
        Public Sub CTU_SetLanKS_Citad(ByVal so_ct As String, ByVal strClickKS As String)
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "update tcs_ctu_songphuong_hdr " & _
                        "set lan_ks = '" & strClickKS & "'" & _
                        "where so_ct = '" & so_ct & "'"
                Dim dr As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)

                dr.Close()
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khởi tạo ghi chú chứng từ")

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' ham nay lay chi tiet theo bao lanh moi gop tren hdr bo dtl
        ''' </summary>
        ''' <param name="SO_CT"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CTU_ChiTiet_Set_CT_BL_New(ByVal SO_CT As String) As DataSet

            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim ds As New DataSet

                strSQL = "select SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,'01' MA_QUY,MA_CHUONG, " & _
                        " MA_NDKT as MA_TMUC,NOI_DUNG,KY_THUE,TTIEN SOTIEN,SO_CT " & _
                        " FROM TCS_BIENLAI_HDR " & _
                        " where (SO_CT='" & SO_CT & "')"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_IN_BL_NEW(ByVal so_ct As String) As DataSet
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select rownum as STT,dtl.SO_CT,dtl.noi_dung as NOI_DUNG,dtl.ma_chuong as CC, " & _
                        "'' as LK, dtl.ma_ndkt as MTM, " & _
                        " dtl.TTIEN as SOTIEN, 0 as SOTIEN_NT, dtl.ky_thue as NGAY_TK, " & _
                         " dtl.ma_dbhc " & _
                        "from tcs_bienlai_HDR dtl " & _
                        "where   dtl.so_ct = '" & so_ct & "'" & _
                        " order by rownum "

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
                'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '  & "Error code: System error!" & vbNewLine _
                '  & "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' ham nay danh cho cac bien lai lap  tu he thong khac
        ''' </summary>
        ''' <param name="strNgayCT"></param>
        ''' <param name="strTrangThai"></param>
        ''' <param name="strMaDiemThu"></param>
        ''' <param name="strMaNV"></param>
        ''' <param name="strMaKS"></param>
        ''' <param name="strSoBT"></param>
        ''' <param name="strKenhCT"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CTU_GetListKiemSoatBL_NEW(ByVal strNgayCT As String, ByVal strTrangThai As String, ByVal strMaDiemThu As String, _
                  Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "", Optional ByVal strKenhCT As String = "") As DataSet
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strWhereLoaiThue As String = ""

                strSQL = " Select hdr.SHKB,hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BT,hdr.TRANG_THAI, hdr.TTIEN,hdr.TTien_NT, hdr.MA_NV, " & _
                " hdr.NGAY_KB, hdr.MA_DTHU, hdr.TT_SP, hdr.MA_NNTHUE,nv.TEN_DN, hdr.so_ct_nh,hdr.ma_ks, hdr.lan_ks " & _
                         " From tcs_bienlai_hdr hdr, tcs_dm_nhanvien nv " & _
                   " Where hdr.ma_nv=nv.ma_nv and nv.tinh_trang='0' and hdr.PRODUCT<>'ETAX'  AND((hdr.TRANG_THAI IN ('10','09','06') AND hdr.NGAY_KB='" & strNgayCT & "') OR hdr.TRANG_THAI IN ('02','08','03','04')) "
                '   " Where hdr.ma_nv=nv.ma_nv and nv.tinh_trang='0' and hdr.NGAY_KB = " & strNgayCT & " "

                If (strTrangThai.Trim <> "") Then
                    If strTrangThai = "02" Then
                        strSQL = strSQL & " and hdr.Trang_thai='02' "
                    ElseIf strTrangThai = "030" Then
                        strSQL = strSQL & " and hdr.Trang_thai='08' "
                    ElseIf strTrangThai = "031" Then
                        strSQL = strSQL & " and hdr.Trang_thai='09'"
                    ElseIf strTrangThai = "04" Then
                        strSQL = strSQL & " and hdr.Trang_thai='04' "
                    ElseIf strTrangThai = "06" Then
                        strSQL = strSQL & " and hdr.Trang_thai='06' "
                    End If

                End If
                If Not "".Equals(strMaNV) Then
                    strSQL = strSQL & " and (hdr.Ma_NV=" & strMaNV & ") "
                End If
                If Not "".Equals(strSoBT) Then
                    strSQL = strSQL & " and (hdr.So_BT=" & strSoBT & ") "
                End If
                If Not "".Equals(strMaKS) Then
                    strSQL = strSQL & " and hdr.ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMaKS & "')) "
                End If

                'Kiênvt: Trong trường hợp thu hộ
                If strMaDiemThu.Length > 0 Then
                    strSQL &= " AND (hdr.MA_DTHU=" & strMaDiemThu & ") "
                End If

                strSQL = strSQL & " order by hdr.SO_CT DESC"
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine & "Error StackTrace: " & ex.StackTrace & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' HAM LAY DANH SACH CHUNG TU CHO PHAN DUYET MOI
        ''' </summary>
        ''' <param name="sSOCT"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CTU_Header_Set_BL_NEW(ByVal sSOCT As String) As DataSet

            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess

                Dim strSQL As String
                strSQL = "select PRODUCT, SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,TEN_DTHU,SO_CT,NGAY_CT,KYHIEU_CT,SO_CT_NH,TK_CO,TEN_KB,MA_NNTHUE,TEN_NNTHUE,DC_NNTHUE,MA_NNTIEN,TEN_NNTIEN,DC_NNTIEN, " & _
                               "MA_LTHUE,TEN_LTHUE,MA_CQTHU,TEN_CQTHU,MA_DBHC,TEN_DBHC,MA_NH_A,TEN_NH_A,MA_NH_TT,TEN_NH_TT,MA_NH_GT,TEN_NH_GT,MA_NT," & _
                               "TTIEN,TTIEN_NT,TRANG_THAI,TK_KH_NH,TEN_KH_NH,NGAY_KH_NH,LY_DO,DIEN_GIAI,PT_TT,TT_SP,NGAY_KS,LY_DO_HUY,NGAY_GDV_HUY,MA_HUYKS," & _
                               "NGAY_KSV_HUY,NGAY_KSV_CT,NHANVIEN_VL,TTHAI_XL_LAI,NGAY_XL_LAI,MA_CN,MA_CQQD,TEN_CQQD,NGAY_HT, PRODUCT, " & _
                               "SO_QD,to_char(NGAY_QD,'dd/MM/yyyy') NGAY_QD,MA_LHTHU,TEN_LHTHU,LY_DO_VPHC,TTIEN_CTBL,TTIEN_VPHC,LY_DO_NOP_CHAM,TTIEN_NOP_CHAM, SO_BIENLAI, ten_huyen_nnthue,ten_tinh_nnthue, ten_huyen_nntien,ten_tinh_nntien, MA_NTK, HT_THU  " & _
                               " from tcs_bienlai_hdr " & _
                               " where so_ct = '" & sSOCT & "' "

                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chung chứng từ")
                '  LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '& "Error code: System error!" & vbNewLine _
                '& "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function CTU_Get_TrangThai_ThanhToan_BL(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select payment from tcs_bienlai_hdr " & _
                        "where so_ct = '" & so_ct & "'"

                Dim strTT As String = cnCT.ExecuteSQLScalar(strSQL)

                Return strTT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
    End Class
End Namespace

