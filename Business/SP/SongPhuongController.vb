﻿Imports System
Imports System.Data
Imports VBOracleLib
Imports System.Text
Imports Common
Imports Business_HQ.NewChungTu
Imports System.Globalization
Imports System.Data.OracleClient


Namespace SP
    Public Class SongPhuongController
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Shared Function GetSoCtu(ByVal sMaNV As String, ByVal sSoCtu As String, ByVal sLoaiCTu As String) As DataTable
            Dim v_dt As DataTable
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                Dim strTThai As String = GetTrangThaiChungTu(sSoCtu)

                If strTThai.Equals("01") Then
                    sSQL = "select so_ref_core, SO_CT, TRANG_THAI, tt_sp " & _
                      " from tcs_chungtu_ttsp  where  TRANG_THAI = '01' AND SO_CT = :SO_CT  order by SO_CT desc "

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, sSoCtu.Trim, DbType.String))
                    v_dt = DataAccess.ExecuteReturnDataTable(sSQL, CommandType.Text, listParam.ToArray())
                Else

                    sSQL = "select A.SO_CT_NH, A.SO_CT, A.TRANG_THAI, A.tt_sp " & _
                    " from TCS_CTU_SONGPHUONG_HDR A , TCS_DM_NHANVIEN B where  A.MA_NV = B.MA_NV AND A.TRANG_THAI = '03' AND SO_CT = :SO_CT  order by A.SO_CT desc "

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, sSoCtu.Trim, DbType.String))
                    listParam.Add(DataAccess.NewDBParameter("MA_NV", ParameterDirection.Input, sMaNV.Trim, DbType.String))
                    v_dt = DataAccess.ExecuteReturnDataTable(sSQL, CommandType.Text, listParam.ToArray())

                End If




            Catch ex As Exception
                v_dt = Nothing
            End Try

            Return v_dt
        End Function
        Public Shared Function GetListChungTu() As DataSet
            Dim ds As DataSet = Nothing
            Dim sSQL As String = ""
            Try
                sSQL = "select so_ref_core, SO_CT, TRANG_THAI, tt_sp " & _
                  " from tcs_chungtu_ttsp  where  TRANG_THAI = '01' order by SO_CT desc "

                ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text)
            Catch ex As Exception
                ds = Nothing
            End Try

            Return ds
        End Function

        Public Shared Function GetListChungTu(ByVal sNgayKB As String, ByVal sMaNV As String, ByVal sMA_CN As String) As DataSet
            Dim ds As DataSet = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "select CI.so_ref_core, NVL(CI.SO_CT, CI.so_ref_core) SO_CT, CI.TRANG_THAI, CI.tt_sp,0 so_bt "
                sSQL &= " from tcs_chungtu_ttsp CI  where  CI.TRANG_THAI in  ('01','06') "
                sSQL &= " UNION ALL "
                sSQL &= "select A.SO_CT_NH as so_ref_core, A.SO_CT, A.TRANG_THAI,A.TT_SP,A.so_bt   "
                sSQL &= " from TCS_CTU_SONGPHUONG_HDR A, TCS_DM_NHANVIEN B where A.MA_NV = B.MA_NV AND B.TINH_TRANG = '0'  and  a.MA_CN = :MA_CN "
                sSQL &= " and a.NGAY_KB = :NGAY_KB and a.MA_NV = :MA_NV "
                sSQL &= "  order by SO_CT desc "

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_CN", ParameterDirection.Input, CStr(sMA_CN), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("NGAY_KB", ParameterDirection.Input, CStr(sNgayKB), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))

                ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray())
            Catch ex As Exception
                ds = Nothing
            End Try

            Return ds
        End Function
        Public Shared Function GetTrangThaiChungTu(ByVal strSo_ct As String) As String
            Dim sSQL As String = ""
            Try
                sSQL = "select  TRANG_THAI " & _
                  " from tcs_chungtu_ttsp  where  SO_REF_CORE='" & strSo_ct & "'"
                Return DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

            Catch ex As Exception
            End Try

            Return ""
        End Function
        Public Shared Function GetTrangThaiChungTuHDR(ByVal strSo_ct As String) As String
            Dim sSQL As String = ""
            Try
                sSQL = "select  TRANG_THAI " & _
                  " from tcs_ctu_songphuong_hdr  where  SO_CT='" & strSo_ct & "'"
                Return DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

            Catch ex As Exception
            End Try

            Return ""
        End Function

        Public Shared Function GetChungTu(ByVal sSoChungTu As String) As ChungTuSP.infChungTuSP
            Dim oRet As ChungTuSP.infChungTuSP = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Dim dDateTmp As Date
            Try
                'get HDR
                Dim HDR As New ChungTuSP.infChungTuSP_HDR

                Dim strTrang_thai As String = Business.SP.SongPhuongController.GetTrangThaiChungTu(sSoChungTu)

                If strTrang_thai.Equals("01") Then
                    sSQL = "select  TO_CHAR(SYSDATE,'dd/MM/yyyy') ngay_citad,a.BT_CITAD sobt_citad,a.ten_nnthue TEN_NGUOI_CHUYEN,'' ma_nnthue,'' ten_nnthue,a.dc_nnthue, '' ma_nntien,'' ten_nntien, '' dc_nntien, to_char(a.ngay_nntien,'DD-MM-YYYY') as ngay_ntien," & _
                    "'' SHKB,'' TEN_KB,'' NGAY_KB,to_char(SYSDATE,'dd/MM/yyyy') as NGAY_CTU, " & _
                    "0 So_BT,'' Ma_DThu,0 So_BThu," & _
                    "'' KyHieu_CT,a.So_CT,a.SO_REF_CORE as So_CT_NH ,'' TK_CO,'' TK_NO," & _
                    "'' ma_lthue,'' cq_qd,'' MA_CQTHU," & _
                    " '' TEN_CQTHU,'' MA_DBHC,'' TEN_DBHC," & _
                    "'' Ly_Do,'' ly_do_huy,'' Ma_KS,a.TK_KH_NH,'' ngay_kh_nh,'' tenkh_nhan," & _
                    "'' tk_kh_nhan ,'' diachi_kh_nhan,a.MA_NH_A nh_giu_tknnt," & _
                    "'' nh_giu_tkkb,a.ma_nt,a.ty_gia, a.TTien, a.TTien_NT," & _
                    "'' MA_HQ, '' TEN_HQ,a.PAYMENT_DETAIL as REMARK,a.content_ex,'' MA_HQ_PH, '' TEN_HQ_PH,a.ngay_ht,a.ma_cn,'' PT_TT, a.TRANG_THAI, a.tt_sp,a.MA_NV,'' REF_NO  " & _
                    " from tcs_chungtu_ttsp a  " & _
                      " where  a.SO_REF_CORE= :SO_CT"
                Else
                    sSQL = "select a.ma_nnthue,a.ten_nnthue,a.dc_nnthue, a.ma_nntien,a.ten_nntien, a.dc_nntien, a.ngay_ntien, " & _
                    "a.SHKB,a.TEN_KB,a.NGAY_KB,a.NGAY_CTU, " & _
                    "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                    "a.KyHieu_CT,a.So_CT,a.So_CT_NH,a.TK_CO,a.TK_NO," & _
                    "a.ma_lthue,a.cq_qd,a.MA_CQTHU," & _
                    "a.TEN_CQTHU,a.MA_DBHC,a.TEN_DBHC," & _
                    "a.Ly_Do,a.ly_do_huy,a.Ma_KS,a.TK_KH_NH,to_char(a.ngay_kh_nh,'DD-MM-YYYY') as ngay_kh_nh,a.tenkh_nhan," & _
                    "a.tk_kh_nhan ,a.diachi_kh_nhan,a.nh_giu_tknnt," & _
                    "a.nh_giu_tkkb,a.ma_nt,a.ty_gia, a.TTien, a.TTien_NT," & _
                    "a.MA_HQ, a.TEN_HQ,a.REMARK,a.content_ex,a.MA_HQ_PH, a.TEN_HQ_PH,a.ngay_ht,a.ma_cn,a.PT_TT as PT_TT, a.TRANG_THAI,a.tt_sp,a.MA_NV,a.ngay_citad, a.sobt_citad, a.TEN_NGUOI_CHUYEN,a.REF_NO " & _
                    " from TCS_CTU_SONGPHUONG_HDR a " & _
                    "where a.so_ct= :SO_CT"

                End If


                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    'ma va ten DBHC
                    HDR.MA_NNTHUE = dt.Rows(0)("MA_NNTHUE").ToString()
                    HDR.TEN_NNTHUE = dt.Rows(0)("TEN_NNTHUE").ToString()
                    HDR.DC_NNTHUE = dt.Rows(0)("DC_NNTHUE").ToString()

                    HDR.MA_NNTIEN = dt.Rows(0)("Ma_NNTien").ToString()
                    HDR.TEN_NNTIEN = dt.Rows(0)("TEN_NNTIEN").ToString()
                    HDR.DC_NNTIEN = dt.Rows(0)("DC_NNTIEN").ToString()

                    HDR.NGAY_NTIEN = dt.Rows(0)("ngay_ntien").ToString()
                    HDR.SHKB = dt.Rows(0)("SHKB").ToString()
                    HDR.TEN_KB = dt.Rows(0)("TEN_KB").ToString()
                    If dt.Rows(0)("NGAY_KB") Is Nothing OrElse IsDBNull(dt.Rows(0)("NGAY_KB")) Then
                        HDR.NGAY_KB = 0
                    Else
                        HDR.NGAY_KB = dt.Rows(0)("NGAY_KB")
                    End If
                    'Bo sung
                    If dt.Rows(0)("Ngay_CTu") Is Nothing OrElse IsDBNull(dt.Rows(0)("Ngay_CTu")) Then
                        HDR.NGAY_CTU = 0
                    Else
                        HDR.NGAY_CTU = dt.Rows(0)("Ngay_CTu")
                    End If
                    If dt.Rows(0)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(0)("So_BT")) Then
                        HDR.SO_BT = 0
                    Else
                        HDR.SO_BT = dt.Rows(0)("So_BT")
                    End If
                    If dt.Rows(0)("MA_DTHU") Is Nothing OrElse IsDBNull(dt.Rows(0)("MA_DTHU")) Then
                        HDR.MA_DTHU = ""
                    Else
                        HDR.MA_DTHU = dt.Rows(0)("MA_DTHU").ToString()
                    End If
                    If dt.Rows(0)("So_BThu") Is Nothing OrElse IsDBNull(dt.Rows(0)("So_BThu")) Then
                        HDR.SO_BTHU = ""
                    Else
                        HDR.SO_BTHU = dt.Rows(0)("So_BThu")
                    End If


                    HDR.KYHIEU_CT = dt.Rows(0)("KyHieu_CT").ToString()
                    HDR.SO_CT = dt.Rows(0)("So_CT").ToString()
                    HDR.SO_CT_NH = dt.Rows(0)("SO_CT_NH").ToString()
                    HDR.TK_CO = dt.Rows(0)("TK_Co").ToString()
                    HDR.TK_NO = dt.Rows(0)("TK_No").ToString()
                    HDR.MA_LTHUE = dt.Rows(0)("MA_LTHUE").ToString()
                    HDR.CQ_QD = dt.Rows(0)("CQ_QD").ToString()
                    HDR.MA_CQTHU = dt.Rows(0)("MA_CQTHU").ToString()
                    HDR.TEN_CQTHU = dt.Rows(0)("TEN_CQTHU").ToString()
                    HDR.MA_DBHC = dt.Rows(0)("MA_DBHC").ToString()
                    HDR.TEN_DBHC = dt.Rows(0)("TEN_DBHC").ToString()
                    HDR.LY_DO = dt.Rows(0)("ly_do").ToString()
                    HDR.LY_DO_HUY = dt.Rows(0)("ly_do_huy").ToString()
                    HDR.MA_KS = dt.Rows(0)("MA_KS").ToString()
                    HDR.TK_KH_NH = dt.Rows(0)("TK_KH_NH").ToString()

                    If dt.Rows(0)("NGAY_KH_NH") IsNot Nothing AndAlso dt.Rows(0)("NGAY_KH_NH") IsNot DBNull.Value Then
                        HDR.NGAY_KH_NH = dt.Rows(0)("NGAY_KH_NH")
                    Else
                        HDR.NGAY_KH_NH = String.Empty
                    End If
                    HDR.TENKH_NHAN = dt.Rows(0)("tenkh_nhan").ToString()
                    HDR.TK_KH_NHAN = dt.Rows(0)("tk_kh_nhan").ToString()
                    HDR.DIACHI_KH_NHAN = dt.Rows(0)("diachi_kh_nhan").ToString()
                    HDR.NH_GIU_TKNNT = dt.Rows(0)("nh_giu_tknnt").ToString()
                    HDR.NH_GIU_TKKB = dt.Rows(0)("nh_giu_tkkb").ToString()
                    HDR.MA_NT = "VND" 'dt.Rows(0)("Ma_NT").ToString()
                    If dt.Rows(0)("Ty_Gia") Is Nothing OrElse IsDBNull(dt.Rows(0)("Ty_Gia")) Then
                        HDR.TY_GIA = 1
                    Else
                        HDR.TY_GIA = 1 'dt.Rows(0)("Ty_Gia")
                    End If
                    If dt.Rows(0)("TTien") Is Nothing OrElse IsDBNull(dt.Rows(0)("TTien")) Then
                        HDR.TTIEN = 0
                    Else
                        HDR.TTIEN = dt.Rows(0)("TTien")
                    End If
                    If dt.Rows(0)("TTIEN_NT") Is Nothing OrElse IsDBNull(dt.Rows(0)("TTIEN_NT")) Then
                        HDR.TTIEN_NT = 0
                    Else
                        HDR.TTIEN_NT = dt.Rows(0)("TTIEN_NT")
                    End If

                    'ti gia luon la 1, nen set 
                    HDR.TTIEN_NT = HDR.TTIEN

                    HDR.MA_HQ = dt.Rows(0)("MA_HQ").ToString()
                    HDR.TEN_HQ = dt.Rows(0)("TEN_HQ").ToString()
                    HDR.MA_HQ_PH = dt.Rows(0)("MA_HQ_PH").ToString()
                    HDR.TEN_HQ_PH = dt.Rows(0)("TEN_HQ_PH").ToString()
                    HDR.REMARK = dt.Rows(0)("REMARK").ToString()
                    HDR.CONTENT_EX = dt.Rows(0)("content_ex").ToString()

                    If dt.Rows(0)("NGAY_HT") IsNot Nothing AndAlso dt.Rows(0)("NGAY_HT") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("NGAY_HT")
                        HDR.NGAY_HT = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.NGAY_HT = String.Empty
                    End If
                    HDR.MA_CN = dt.Rows(0)("ma_cn").ToString()

                    HDR.TRANG_THAI = dt.Rows(0)("Trang_Thai").ToString()
                    If dt.Rows(0)("TT_SP") Is Nothing OrElse IsDBNull(dt.Rows(0)("TT_SP")) Then
                        HDR.TT_SP = 0
                    Else
                        HDR.TT_SP = dt.Rows(0)("TT_SP")
                    End If
                    If dt.Rows(0)("MA_NV") Is Nothing OrElse IsDBNull(dt.Rows(0)("MA_NV")) Then
                        HDR.MA_NV = 0
                    Else
                        HDR.MA_NV = dt.Rows(0)("MA_NV")
                    End If

                    HDR.NGAY_CITAD = dt.Rows(0)("ngay_citad").ToString()
                    HDR.SOBT_CITAD = dt.Rows(0)("sobt_citad").ToString()
                    HDR.TEN_NGUOI_CHUYEN = dt.Rows(0)("TEN_NGUOI_CHUYEN").ToString()
                    HDR.REF_NO = dt.Rows(0)("REF_NO").ToString()

                End If
                'get DTL list
                Dim listDTL As New List(Of ChungTuSP.infChungTuSP_DTL)
                Dim DTL As ChungTuSP.infChungTuSP_DTL = Nothing

                If strTrang_thai.Equals("01") Then


                    Dim sContent_ex As String = HDR.CONTENT_EX
                    If sContent_ex.Trim.Length > 0 Then
                        GetCTFromRemark25(sContent_ex, HDR, listDTL)
                    Else
                        Dim sContent As String = HDR.REMARK
                        If sContent.Length > 0 Then
                            Dim lthue As String = sContent.Substring(0, 4)
                            If lthue.ToUpper().Equals("HQDT") Then
                                GetCTFromRemark2936(sContent, HDR, listDTL)
                            ElseIf lthue.ToUpper().Equals("NTDT") Then
                                GetCTFromRemarkNTDT(sContent, HDR, listDTL)
                            End If

                        End If
                    End If
                Else
                    dt = Nothing
                    listParam = New List(Of IDataParameter)
                    sSQL = "select id,ma_nv,so_bt,so_tk,to_char(ngay_tk,'dd/MM/yyyy') as ngay_tk,sac_thue,ma_chuong,ma_muc,ma_tmuc,noi_dung,ky_thue,sotien,sotien_nt,so_ct " & _
                            " FROM TCS_CTU_SONGPHUONG_DTL " & _
                            " where SO_CT = :SO_CT"
                    listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                    dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))

                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                        For i As Integer = 0 To dt.Rows.Count - 1
                            DTL = New ChungTuSP.infChungTuSP_DTL
                            DTL.ID = dt.Rows(i)("ID").ToString()

                            If dt.Rows(i)("Ma_NV") Is Nothing OrElse IsDBNull(dt.Rows(i)("Ma_NV")) Then
                                DTL.Ma_NV = 0
                            Else
                                DTL.Ma_NV = dt.Rows(i)("Ma_NV")
                            End If
                            If dt.Rows(i)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(i)("So_BT")) Then
                                DTL.So_BT = 0
                            Else
                                DTL.So_BT = dt.Rows(i)("So_BT")
                            End If
                            DTL.SO_TK = dt.Rows(i)("SO_TK").ToString()
                            DTL.NGAY_TK = dt.Rows(i)("Ngay_TK").ToString()
                            DTL.SAC_THUE = dt.Rows(i)("Sac_Thue").ToString()
                            DTL.Ma_Chuong = dt.Rows(i)("Ma_Chuong").ToString()
                            DTL.Ma_Muc = dt.Rows(i)("Ma_Muc").ToString()
                            DTL.Ma_TMuc = dt.Rows(i)("Ma_TMuc").ToString()
                            DTL.Noi_Dung = dt.Rows(i)("Noi_Dung").ToString()
                            DTL.Ky_Thue = dt.Rows(i)("Ky_Thue").ToString()
                            If dt.Rows(i)("sotien") Is Nothing OrElse IsDBNull(dt.Rows(i)("sotien")) Then
                                DTL.SoTien = 0
                            Else
                                DTL.SoTien = dt.Rows(i)("sotien")
                            End If
                            If dt.Rows(i)("SoTien_NT") Is Nothing OrElse IsDBNull(dt.Rows(i)("SoTien_NT")) Then
                                DTL.SoTien_NT = 0
                            Else
                                DTL.SoTien_NT = dt.Rows(i)("SoTien_NT")
                            End If

                            'ti gia luon la 1, nen set 
                            'DTL.SoTien_NT = DTL.SoTien

                            DTL.SO_CT = dt.Rows(i)("SO_CT").ToString()


                            listDTL.Add(DTL)
                        Next
                    End If

                End If

                oRet = New ChungTuSP.infChungTuSP(HDR, listDTL)
            Catch ex As Exception
                oRet = Nothing
                Throw ex
            End Try

            Return oRet
        End Function
        Public Shared Function GetCTFromRemark2936(ByVal sContent As String, ByVal HDR As ChungTuSP.infChungTuSP_HDR, ByVal listDTL As List(Of ChungTuSP.infChungTuSP_DTL))
            Dim sSQL As String = ""
            Dim dt As DataTable = Nothing
            Dim dDateTmp As Date
            Try
                Dim result As String = ""
                Dim lthue As String = sContent.Substring(0, 4)
                If lthue.ToUpper().Equals("HQDT") Then
                    Dim arr() As String
                    arr = sContent.Split("+")
                    Dim ct_LTHUE As String = arr(0).ToString() 'loại thuế : HQDT
                    Dim ct_ID As String = arr(1).ToString()    ' ID  : ID0
                    Dim ct_MST As String = arr(2).ToString()    ' mã số thuế : MST0123456789
                    Dim ct_C As String = arr(3).ToString()     ' chương : C152
                    Dim ct_NNT As String = arr(4).ToString()   ' ngày nộp thuế  : NNT07052019
                    Dim ct_HQ As String = arr(5).ToString()    ' hải quan : HQ18A1-18A1-2995100
                    Dim ct_TK As String = arr(6).ToString()    ' tờ khai  : TK10223490175
                    Dim ct_NDK As String = arr(7).ToString()   ' năm đăng ký, năm tờ khai : NDK21092018
                    Dim ct_LH As String = arr(8).ToString()   ' mã loại hình xuất nhập khẩu : LHA12
                    Dim ct_NTK As String = arr(9).ToString()   '  mã nhóm tài khoản
                    Dim ct_LT As String = arr(10).ToString()   ' Mã loại tiền (Thuế, phí, phạt...) : LT1
                    Dim ct_KB As String = arr(11).ToString()   ' kho bạc : KB1117
                    Dim ct_TKNS As String = arr(12).ToString()  ' tài khoản thu NS: TKNS7111
                    Dim ct_VND As String = arr(13).ToString()  ' nguyên tệ + chi tiết thuế : VND(TM1901+ST2+T9494179)(TM1901+ST2+T9494179)


                    Dim ID As String = ct_ID.Substring(2, ct_ID.Length - 2)
                    Dim MST As String = ct_MST.Substring(3, ct_MST.Length - 3)
                    Dim C As String = ct_C.Substring(1, ct_C.Length - 1)
                    Dim NNT As String = ct_NNT.Substring(3, ct_NNT.Length - 3)

                    Dim arrHQ() As String
                    arrHQ = ct_HQ.Split("-")
                    Dim ctHQ_HQ As String = ""
                    Dim HQ_PH As String = ""
                    Dim HQ As String = ""
                    Dim CQT As String = ""
                    If arrHQ.Length >= 3 Then
                        ctHQ_HQ = arrHQ(0).ToString()
                        HQ_PH = ctHQ_HQ.Substring(2, ctHQ_HQ.Length - 2)
                        HQ = arrHQ(1).ToString()
                        CQT = arrHQ(2).ToString()
                    End If


                    Dim TK As String = ct_TK.Substring(2, ct_TK.Length - 2)
                    Dim NDK As String = ct_NDK.Substring(3, ct_NDK.Length - 3)
                    Dim LH As String = ct_LH.Substring(2, ct_LH.Length - 2)
                    Dim NTK As String = ct_NTK.Substring(3, ct_NTK.Length - 3)
                    Dim LT As String = ct_LT.Substring(2, ct_LT.Length - 2)
                    Dim KB As String = ct_KB.Substring(2, ct_KB.Length - 2)
                    Dim TKNS As String = ct_TKNS.Substring(4, ct_TKNS.Length - 4)
                    Dim VND As String = ct_VND.Substring(0, 3)



                    HDR.MA_NNTHUE = MST
                    HDR.NGAY_NTIEN = Common.mdlCommon.FormatDate(NNT)
                    HDR.MA_HQ_PH = HQ_PH
                    HDR.MA_HQ = HQ
                    HDR.MA_CQTHU = CQT
                    HDR.SHKB = KB
                    HDR.TK_CO = TKNS
                    HDR.MA_NT = "VND"
                    HDR.MA_LTHUE = "04"
                    'Phân tách phần chi tiết thuế
                    Dim ctThue As String = ""
                    Dim dIndex = sContent.IndexOf("(")
                    If (dIndex > -1) Then
                        ctThue = sContent.Substring(dIndex, sContent.Length - dIndex) '(TM1901+ST2+T9494179)(TM1901+ST2+T9494179)
                    End If

                    Dim count As Integer = CountCharacter(ctThue, "(")

                    Dim DTL As ChungTuSP.infChungTuSP_DTL = Nothing

                    If count > 0 Then
                        Dim arrCThue() As String
                        arrCThue = ctThue.Split(")")
                        Dim sl As Integer = arrCThue.Length
                        If arrCThue(sl - 1).Length <= 0 Then
                            sl = sl - 1
                        End If
                        For i = 0 To sl - 1

                            DTL = New ChungTuSP.infChungTuSP_DTL
                            Dim ct_i As String = arrCThue(i).ToString().Replace("(", "").Replace(")", "")

                            Dim arrChiTiet() As String
                            arrChiTiet = ct_i.Split("+")
                            Dim ct_TM As String = arrChiTiet(0).ToString() 'tiểu mục
                            Dim ct_ST As String = arrChiTiet(1).ToString()  'sắc thuế
                            Dim ct_T As String = arrChiTiet(2).ToString()   ' số tiền

                            Dim TM As String = ct_TM.Substring(2, ct_TM.Length - 2)
                            Dim ST As String = ct_ST.Substring(2, ct_ST.Length - 2)
                            Dim T As String = ct_T.Substring(1, ct_T.Length - 1)

                            DTL.NGAY_TK = Common.mdlCommon.FormatDate(NDK)
                            DTL.SO_TK = TK
                            DTL.MA_NDKT = TM
                            DTL.Ma_TMuc = TM
                            DTL.SAC_THUE = GetSacThue(ST)
                            DTL.Noi_Dung = GetTenTieuMuc(TM)
                            DTL.Ky_Thue = ""
                            DTL.SoTien = T
                            DTL.Ma_Chuong = C
                            DTL.SoTien_NT = T
                            listDTL.Add(DTL)
                        Next
                    End If
                End If

            Catch ex As Exception
                'Throw ex
            End Try
        End Function

        Public Shared Function GetCTFromRemarkNTDT(ByVal sContent As String, ByVal HDR As ChungTuSP.infChungTuSP_HDR, ByVal listDTL As List(Of ChungTuSP.infChungTuSP_DTL))
            Dim sSQL As String = ""
            Dim dt As DataTable = Nothing
            Dim dDateTmp As Date
            Try
                Dim result As String = ""
                Dim lthue As String = sContent.Substring(0, 4)
                If lthue.ToUpper().Equals("NTDT") Then
                    Dim arr() As String
                    arr = sContent.Split("+")
                    'Dim LTHUE As String = arr(0).ToString() 'loại thuế : NTDT
                    Dim ct_KB As String = arr(1).ToString()    ' kho bạc - tên kho bạc : KB:0111-VP KBNN Ho Chi Minh
                    Dim ct_NgayNT As String = arr(2).ToString()    ' ngày nộp thuế  : NgayNT:22072020
                    Dim ct_MST As String = arr(3).ToString()     ' mã số thuế : MST:0100512273
                    Dim ct_DBHC As String = arr(4).ToString()   ' địa bàn hành chính  : DBHC:777HH
                    Dim ct_TKNS As String = arr(5).ToString()    ' tài khoản thu NS: TKNS:7111
                    Dim ct_CQT As String = arr(6).ToString()    ' cơ quan thu  : CQT:1054218
                    Dim ct_LThue As String = arr(7).ToString()   ' loai thue : LThue:01

                    Dim KB As String = ct_KB.Substring(3, 4)
                    Dim Ten_KB As String = ct_KB.Substring(8, ct_KB.Length - 8)
                    Dim NgayNT As String = ct_NgayNT.Split(":")(1).ToString()
                    Dim MST As String = ct_MST.Split(":")(1).ToString()
                    Dim DBHC As String = ct_DBHC.Split(":")(1).ToString()
                    Dim TKNS As String = ct_TKNS.Split(":")(1).ToString()
                    Dim CQT As String = ct_CQT.Split(":")(1).ToString()
                    Dim MaLThue As String = ct_LThue.Split(":")(1).ToString().Substring(0, 2)

                    HDR.SHKB = KB
                    HDR.TEN_KB = Ten_KB
                    HDR.NGAY_NTIEN = Common.mdlCommon.FormatDate(NgayNT)
                    HDR.MA_NNTHUE = MST
                    HDR.MA_DBHC = DBHC
                    HDR.TK_CO = TKNS
                    HDR.MA_CQTHU = CQT
                    HDR.MA_LTHUE = MaLThue

                    Dim DTL As ChungTuSP.infChungTuSP_DTL = Nothing
                    'Phân tách phần chi tiết thuế
                    Dim ctThue As String = ""
                    Dim dIndex = sContent.IndexOf("(")
                    If (dIndex > -1) Then
                        ctThue = sContent.Substring(dIndex, sContent.Length - dIndex) '(C:174-TM:1704-KT:22/07/2020-ST:24000000-GChu:)
                    End If

                    Dim arrCThue() As String
                    arrCThue = ctThue.Split(")")
                    Dim sl As Integer = arrCThue.Length
                    If arrCThue(sl - 1).Length <= 0 Then
                        sl = sl - 1
                    End If
                    For i = 0 To sl - 1
                        Dim ct_i As String = arrCThue(i).ToString().Replace("(", "").Replace(")", "")
                        DTL = New ChungTuSP.infChungTuSP_DTL
                        Dim arrChiTiet() As String
                        arrChiTiet = ct_i.Split("-")
                        If arrChiTiet.Length < 4 Then
                        Else
                            Dim ct_C As String = arrChiTiet(0).ToString() 'chương
                            Dim ct_TM As String = arrChiTiet(1).ToString() 'tiểu mục
                            Dim ct_KT As String = arrChiTiet(2).ToString()  'kỳ thuế
                            Dim ct_T As String = arrChiTiet(3).ToString()   ' số tiền
                            Dim ct_GChu As String = arrChiTiet(4).ToString() 'ghi chú GChu:day la dau (xxx- xxx) aa

                            Dim C As String = ct_C.Split(":")(1).ToString()
                            Dim TM As String = ct_TM.Split(":")(1).ToString()
                            Dim KT As String = ct_KT.Split(":")(1).ToString()
                            Dim T As String = ct_T.Split(":")(1).ToString()
                            Dim GChu As String = ct_GChu.Substring(5, ct_GChu.Length - 5)

                            DTL.SO_TK = ""
                            DTL.SAC_THUE = ""
                            DTL.NGAY_TK = ""
                            DTL.Ma_Chuong = C
                            DTL.MA_NDKT = TM
                            DTL.Ma_TMuc = TM
                            DTL.Noi_Dung = GChu
                            DTL.Ky_Thue = KT
                            DTL.SoTien = T
                            DTL.SoTien_NT = T
                        End If

                        listDTL.Add(DTL)
                    Next
                End If

            Catch ex As Exception
                'Throw ex
            End Try
        End Function
        Public Shared Function GetCTFromRemark25(ByVal sContent As String, ByVal HDR As ChungTuSP.infChungTuSP_HDR, ByVal listDTL As List(Of ChungTuSP.infChungTuSP_DTL))
            Dim sSQL As String = ""
            Dim dt As DataTable = Nothing
            Dim dDateTmp As Date
            Try
                Dim objEX As CitadContentEx_25.CitadContentEx_25 = New CitadContentEx_25.CitadContentEx_25()
                objEX = objEX.MSGToObject(sContent)

                Dim STC As String = objEX.STC    'Số tham chiếu
                Dim SCT As String = objEX.SCT     'Số chứng từ
                Dim KCT As String = objEX.KCT   'Ký hiệu chứng từ
                Dim TNT As String = objEX.TNT     'Tên Người nộp thuế
                Dim DNT As String = objEX.DNT   'Địa chỉ người nộp thuế
                Dim MST As String = objEX.MST   'Mã số thuế
                Dim MDB As String = objEX.MDB   'Mã địa bàn hành chính
                Dim CQT As String = objEX.CQT   'Mã cơ quan thu
                Dim TCQ As String = objEX.TCQ   'Tên cơ quan thu
                Dim NNT As String = objEX.NNT   'Ngày nộp thuế
                Dim LTH As String = objEX.LTH     'Loại thuế 04 hai quam, <> 04 noi dia
                Dim TKN As String = objEX.TKN   ' Tài khoản nộp vào: 01: Nộp vào NSNN 02: TK tạm thu 03: TK thu hồi quỹ hoàn thuếGTGT()
                Dim KLN As String = objEX.KLN   ' Kết luận nộp của cơquan có thẩm quyền option 01

                'HDR.SO_CT = SCT.Trim
                'HDR.KYHIEU_CT = KCT.Trim
                HDR.TEN_NNTHUE = TNT.Trim
                HDR.DC_NNTHUE = DNT.Trim
                HDR.MA_NNTHUE = MST.Trim
                HDR.MA_DBHC = MDB.Trim
                HDR.MA_CQTHU = CQT.Trim
                HDR.TEN_CQTHU = TCQ.Trim
                HDR.NGAY_NTIEN = NNT.Trim

                If TKN.Equals("01") Then
                    HDR.TK_CO = "7111"
                ElseIf TKN.Equals("03") Then
                    HDR.TK_CO = "8993"
                End If

                If NNT.Trim.Length >= 8 Then
                    NNT = convertDate(NNT)
                    HDR.NGAY_NTIEN = NNT
                End If

                HDR.MA_LTHUE = LTH.Trim
                HDR.CQ_QD = KLN.Trim

                Dim DTL As ChungTuSP.infChungTuSP_DTL = Nothing
                For Each objDTL In objEX.VSTD
                    DTL = New ChungTuSP.infChungTuSP_DTL

                    DTL.SO_TK = objDTL.STK.Trim
                    DTL.NGAY_TK = ""
                    DTL.SAC_THUE = ""
                    DTL.Ma_Chuong = objDTL.MCH.Trim
                    DTL.Ma_TMuc = objDTL.MND.Trim
                    DTL.Noi_Dung = objDTL.NDN.Trim
                    If LTH.Equals("04") Then
                        DTL.NGAY_TK = objDTL.NTK.Trim
                        DTL.Ky_Thue = ""
                    Else
                        DTL.Ky_Thue = objDTL.NTK.Trim
                        DTL.NGAY_TK = ""
                    End If

                    DTL.SoTien = objDTL.STN.Replace(",", ".")
                    DTL.SoTien_NT = objDTL.STN.Replace(",", ".")

                    listDTL.Add(DTL)
                Next

            Catch ex As Exception
                'Throw ex
            End Try
        End Function
        Public Shared Function convertDate(ByVal strText As String) As String
            Dim strDay As String, strMonth As String, strYear As String
            strDay = strText.Substring(6, 2)
            strMonth = strText.Substring(4, 2)
            strYear = strText.Substring(0, 4)
            Return Convert.ToString((Convert.ToString(strDay & Convert.ToString("/")) & strMonth) + "/") & strYear
        End Function
        Public Shared Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
            Dim cnt As Integer = 0
            For Each c As Char In value
                If c = ch Then
                    cnt += 1
                End If
            Next
            Return cnt
        End Function
        Public Shared Function GetMaDiemThu(ByVal sMaNV As String) As String
            Dim sRet As String = String.Empty
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='MA_DT' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV = :MA_NV)"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = String.Empty
            End Try

            Return sRet
        End Function
        Public Shared Function GhiChungTu2(ByVal hdr As SongPhuongObj.HdrSp, ByVal dtl() As SongPhuongObj.Dtl, ByVal sActionType As String, ByVal sSaveType As String) As String
            Dim sTenDN As String = ""
            Dim sDesCTU As String = ""
            Dim sRet As String = ""

            Try
                If hdr Is Nothing Then
                    Return "Err;Không có thông tin chứng từ để lưu. Vui lòng kiểm tra lại"
                End If
                Dim objUser = Nothing
                If hdr IsNot Nothing Then objUser = New CTuUser(hdr.MA_NV)
                If objUser IsNot Nothing Then sTenDN = objUser.Ten_DN
                If Business.SP.SongPhuongController.CheckTrungRefCore(hdr.SO_CT_NH, hdr.SO_CT) Then
                    Return "Err;Đã tồn tại số GD Corebank!"
                End If
                Dim blnVND As Boolean = True
                If (hdr.MA_NT <> "VND") Then blnVND = False
                Select Case sActionType
                    Case "GHI"
                        hdr.TRANG_THAI = "00"
                        If (sSaveType = "INSERT") Then
                            If SongPhuongController.Insert_Chungtu2(hdr, dtl, sRet) Then
                                sRet = "Success;Hoàn thiện chứng từ thành công"
                            End If
                        ElseIf sSaveType = "UPDATE" Then
                            If hdr.TRANG_THAI = "00" OrElse hdr.TRANG_THAI = "03" Then
                                hdr.TRANG_THAI = "00"
                                If SongPhuongController.Update_ChungTu22(hdr, dtl) Then
                                    sRet = "Success;Cập nhật chứng từ thành công"
                                Else
                                    sRet = ";" & hdr.SHKB & "/" & hdr.SO_CT & "/" & hdr.SO_BT
                                End If
                            Else
                                Return "Err;Trạng thái chứng từ không cho phép cập nhật"
                            End If
                        End If
                    Case "GHIKS"
                        If sSaveType = "INSERT_KS" Then
                            hdr.TRANG_THAI = "02"
                            If SongPhuongController.Insert_Chungtu2(hdr, dtl, sRet) Then
                                sRet = "Success;Hoàn thiện và gửi kiểm soát chứng từ thành công"
                            End If
                        ElseIf sSaveType = "UPDATE_KS" Then
                            If hdr.TRANG_THAI = "04" Then
                                hdr.TRANG_THAI = "02"
                                If SongPhuongController.Update_ChungTu22(hdr, dtl) Then
                                    sRet = "Success;Cập nhật và gửi kiểm soát thành công"
                                Else
                                    sRet = ";" & hdr.SHKB & "/" & hdr.SO_CT & "/" & hdr.SO_BT
                                End If
                            Else
                                Return "Err;Trạng thái chứng từ không cho phép cập nhật"
                            End If
                        End If
                    Case "GHIDC"
                        hdr.TRANG_THAI = "19" 'Trạng thái 19 - điều chỉnh gửi kiểm soát
                        If sSaveType = "UPDATE_DC" Then
                            hdr.TRANG_THAI = "19"
                            If SongPhuongController.Update_ChungTu22(hdr, dtl) Then
                                sRet = "Success;Điều chỉnh và gửi kiểm soát thành công"
                            Else
                                sRet = ";" & hdr.SHKB & "/" & hdr.SO_CT & "/" & hdr.SO_BT
                            End If

                        End If
                End Select
                'Nối thêm thông tin load lên form
                sRet = sRet & ";" & hdr.SHKB & "/" & hdr.SO_CT & "/" & hdr.SO_BT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi ghi chứng từ", sTenDN)
                If ex.Message.Contains("TCS_CTU_HDR_PK") Then
                    sRet = ""
                    If sSaveType.StartsWith("INSERT") Then
                        sRet = "Err;Không thể thêm mới! Đã tồn tại giấy nộp tiền này trong hệ thống"
                    ElseIf sSaveType.StartsWith("UPDATE") Then
                        sRet = "Err;Không thể cập nhật! Đã tồn tại giấy nộp tiền này trong hệ thống"
                    End If
                Else
                    sRet = ""
                    Throw ex
                End If
            End Try
            Return sRet
        End Function
        Private Shared Function Insert_Chungtu2(ByVal hdr As SongPhuongObj.HdrSp, ByVal dtl() As SongPhuongObj.Dtl, ByRef err As String) As Boolean
            Dim bRet As Boolean = False
            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then
                    Try
                        hdr.KYHIEU_CT = SongPhuongController.GetKyHieuChungTu(hdr.SHKB, hdr.MA_NV, "04")
                    Catch ex As Exception
                        err = "Err;Có lỗi trong quá trình sinh ký hiệu chứng từ"
                        Return False
                    End Try
                    Try
                        'nên chuyển vào trong câu query
                        If hdr.NGAY_KB Is Nothing Or String.IsNullOrEmpty(hdr.NGAY_KB) Or hdr.NGAY_KB.Length < 8 Then
                            hdr.NGAY_KB = SongPhuongController.GetNgayKhoBac(hdr.SHKB)
                        End If
                        Dim sSo_BT As String = "0"
                        Dim sNam_KB As String
                        Dim sThang_KB As String
                        sNam_KB = Mid(hdr.NGAY_KB, 3, 2)
                        sThang_KB = Mid(hdr.NGAY_KB, 5, 2)
                        'Sinh so CT ngay khi nhan ve tu corebank
                        If String.IsNullOrEmpty(hdr.SO_CT) Then
                            Dim sSoCT = CTuCommon.Get_SoCT_CITAD()
                            hdr.SO_CT = sNam_KB + sThang_KB + sSoCT
                        End If
                        hdr.KYHIEU_CT = hdr.KYHIEU_CT & hdr.SO_CT
                        sSo_BT = SongPhuongController.GetSoBT(hdr.NGAY_KB, hdr.MA_NV)
                        If Not String.IsNullOrEmpty(sSo_BT) Then
                            hdr.SO_BT = CInt(sSo_BT + 1)
                        Else
                            hdr.SO_BT = 1
                        End If
                    Catch ex As Exception
                        err = "Err;Có lỗi trong quá trình sinh số chứng từ"
                        Return False
                    End Try
                    If hdr.NGAY_NTIEN IsNot Nothing Then
                        hdr.NGAY_NTIEN = hdr.NGAY_NTIEN.Replace("/", "-")
                    End If
                    If hdr.NGAY_CTU IsNot Nothing Then
                        hdr.NGAY_CTU = hdr.NGAY_CTU.Replace("/", "-")
                    End If
                    If Not Business.SP.SongPhuongController.CheckTrangThai(hdr.SO_CT_NH) Then
                        err = "Err;Trạng thái của chứng từ đã thay đổi, vui lòng kiểm tra lại!"
                        Return False
                    End If
                    conn = DataAccess.GetConnection()
                    tran = conn.BeginTransaction()
                    sSQL = "INSERT INTO TCS_CTU_SONGPHUONG_HDR(SHKB,NGAY_KB,TK_CO,TK_NO,MA_NV,MA_CN,SO_BT,MA_DTHU,SO_BTHU, " & _
                                "KYHIEU_CT,SO_CT,MA_LTHUE,MA_NNTIEN,TEN_NNTIEN,DC_NNTIEN,MA_NNTHUE,TEN_NNTHUE," & _
                                "DC_NNTHUE,NGAY_CTU,NGAY_HT,MA_CQTHU,NGAY_NTIEN,TEN_KB,MA_DBHC, TEN_DBHC," & _
                                "MA_NT,TY_GIA,TTIEN,TTIEN_NT,TK_KH_NH,NGAY_KH_NH,NH_GIU_TKKB, " & _
                                "TRANG_THAI,CQ_QD,TEN_CQTHU,SO_CT_NH,TENKH_NHAN,TK_KH_NHAN,DIACHI_KH_NHAN,NH_GIU_TKNNT," & _
                                "TT_SP,MA_HQ,TEN_HQ,MA_HQ_PH,TEN_HQ_PH,REMARK, NGAY_CITAD, SOBT_CITAD, TEN_NGUOI_CHUYEN, CONTENT_EX,TG_KY)" & _
                            "VALUES(:SHKB, :NGAY_KB, :TK_CO,:TK_NO, :MA_NV, :MA_CN, :SO_BT, :MA_DTHU,'0'," & _
                                ":KYHIEU_CT, :SO_CT, :MA_LTHUE, :MA_NNTIEN, :TEN_NNTIEN, :DC_NNTIEN,:MA_NNTHUE, :TEN_NNTHUE, " & _
                                ":DC_NNTHUE, :NGAY_CTU, sysdate, :MA_CQTHU,:NGAY_NTIEN,:TEN_KB,:MA_DBHC, :TEN_DBHC, " & _
                                ":MA_NT, :TY_GIA, :TTIEN, :TTIEN_NT, :TK_KH_NH, TO_DATE(:NGAY_KH_NH,'DD-MM-YYYY'),:NH_GIU_TKKB,  " & _
                                ":TRANG_THAI,:CQ_QD,:TEN_CQTHU,:SO_CT_NH,:TENKH_NHAN,:TK_KH_NHAN,:DIACHI_KH_NHAN,:NH_GIU_TKNNT, " & _
                                ":TT_SP,:MA_HQ, :TEN_HQ, :MA_HQ_PH, :TEN_HQ_PH,:REMARK,:NGAY_CITAD, :SOBT_CITAD, :TEN_NGUOI_CHUYEN,:CONTENT_EX,sysdate)"

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(hdr.NGAY_KB), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_CO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_NO", ParameterDirection.Input, CStr(hdr.TK_NO), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(hdr.MA_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(hdr.MA_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(hdr.SO_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(hdr.MA_DTHU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":KYHIEU_CT", ParameterDirection.Input, CStr(hdr.KYHIEU_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, CStr(hdr.MA_LTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.MA_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, CStr(hdr.DC_NNTHUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":NGAY_CTU", ParameterDirection.Input, CStr(hdr.NGAY_CTU), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_HT", ParameterDirection.Input, CStr(hdr.NGAY_HT), DbType.String))       'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_NTIEN", ParameterDirection.Input, CStr(hdr.NGAY_NTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(hdr.TEN_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DBHC", ParameterDirection.Input, CStr(hdr.MA_DBHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_DBHC", ParameterDirection.Input, CStr(hdr.TEN_DBHC), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":CQ_QD", ParameterDirection.Input, CStr(hdr.CQ_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(hdr.TEN_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT_NH", ParameterDirection.Input, CStr(hdr.SO_CT_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TENKH_NHAN", ParameterDirection.Input, CStr(hdr.TENKH_NHAN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NHAN", ParameterDirection.Input, CStr(hdr.TK_KH_NHAN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DIACHI_KH_NHAN", ParameterDirection.Input, CStr(hdr.DIACHI_KH_NHAN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NH_GIU_TKNNT", ParameterDirection.Input, CStr(hdr.NH_GIU_TKNNT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NH_GIU_TKKB", ParameterDirection.Input, CStr(hdr.NH_GIU_TKKB), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TY_GIA", ParameterDirection.Input, CStr(hdr.TY_GIA), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NT", ParameterDirection.Input, CStr(hdr.TTIEN_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, hdr.NGAY_KH_NH, DbType.Date)) 'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TT_SP", ParameterDirection.Input, CStr(hdr.TT_SP), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(hdr.MA_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, CStr(hdr.TEN_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, CStr(hdr.MA_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, CStr(hdr.TEN_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":REMARK", ParameterDirection.Input, CStr(hdr.REMARK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_CITAD", ParameterDirection.Input, CStr(hdr.NGAY_CTITAD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SOBT_CITAD", ParameterDirection.Input, CStr(hdr.SOBT_CITAD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NGUOI_CHUYEN", ParameterDirection.Input, CStr(hdr.TEN_NGUOI_CHUYEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":CONTENT_EX", ParameterDirection.Input, CStr(hdr.CONTENT_EX), DbType.String))
                    Dim v_sqlT As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)
                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....
                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If dtl IsNot Nothing AndAlso dtl.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each objDtl In dtl
                                objDtl.SO_CT = hdr.SO_CT
                                objDtl.SHKB = hdr.SHKB
                                objDtl.NGAY_KB = hdr.NGAY_KB
                                objDtl.MA_NV = hdr.MA_NV
                                objDtl.SO_BT = hdr.SO_BT
                                objDtl.MA_DTHU = hdr.MA_DTHU
                                iRet = 0
                                sSQL = "INSERT INTO TCS_CTU_SONGPHUONG_DTL(" & _
                                            "ID,MA_NV,SO_BT,MA_CHUONG,MA_MUC,MA_TMUC," & _
                                            "NOI_DUNG,KY_THUE,SOTIEN,SOTIEN_NT,SO_CT, SO_TK, NGAY_TK, " & _
                                            "SAC_THUE) " & _
                                        "VALUES(" & _
                                            "tcs_ctu_songphuong_dtl_seq.NEXTVAL,:MA_NV, :SO_BT, :MA_CHUONG, '', :MA_TMUC," & _
                                            ":NOI_DUNG, :KY_THUE, :SOTIEN, :SOTIEN_NT, :SO_CT, :SO_TK, TO_DATE(:NGAY_TK,'dd/MM/yyyy'), " & _
                                            ":SAC_THUE)"
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(objDtl.MA_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(objDtl.SO_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(objDtl.MA_CHUONG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_TMUC", ParameterDirection.Input, CStr(objDtl.MA_TMUC), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(objDtl.NOI_DUNG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":KY_THUE", ParameterDirection.Input, CStr(objDtl.KY_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN", ParameterDirection.Input, CStr(objDtl.SOTIEN), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN_NT", ParameterDirection.Input, CStr(objDtl.SOTIEN_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(objDtl.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(objDtl.SO_TK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, objDtl.NGAY_TK, DbType.Date))
                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(objDtl.SAC_THUE), DbType.String))
                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next
                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If
                    If bRet = True Then
                        'cap nhat trang thai bang ttsp 02
                        sSQL = "UPDATE tcs_chungtu_ttsp " & _
                          "SET TRANG_THAI = '02',SO_CT= :SO_CT WHERE so_ref_core = :so_ref_core "
                        listParam = New List(Of IDataParameter)
                        listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                        listParam.Add(DataAccess.NewDBParameter(":so_ref_core", ParameterDirection.Input, CStr(hdr.SO_CT_NH), DbType.String))
                        iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    End If
                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                Throw
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try
            Return bRet
        End Function
        Public Shared Function Update_ChungTu22(ByVal hdr As SongPhuongObj.HdrSp, ByVal dtl() As SongPhuongObj.Dtl) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then
                    conn = VBOracleLib.DataAccess.GetConnection()
                    '  conn.Open()
                    tran = conn.BeginTransaction()
                    'delete DTL
                    sSQL = "DELETE FROM TCS_CTU_SONGPHUONG_DTL WHERE SO_CT = :SO_CT"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    If hdr.NGAY_NTIEN IsNot Nothing Then
                        hdr.NGAY_NTIEN = hdr.NGAY_NTIEN.Replace("/", "-")
                    End If
                    If hdr.NGAY_CTU IsNot Nothing Then
                        hdr.NGAY_CTU = hdr.NGAY_CTU.Replace("/", "-")
                    End If
                    'update HDR
                    sSQL = "UPDATE TCS_CTU_SONGPHUONG_HDR " & _
                           "SET SHKB = :SHKB, TK_CO = :TK_CO, " & _
                                 "TK_NO = :TK_NO,MA_DTHU = :MA_DTHU, MA_LTHUE = :MA_LTHUE," & _
                                 "MA_NNTIEN = :MA_NNTIEN,TEN_NNTIEN = :TEN_NNTIEN,DC_NNTIEN = :DC_NNTIEN, " & _
                                 "MA_NNTHUE = :MA_NNTHUE,TEN_NNTHUE = :TEN_NNTHUE,DC_NNTHUE = :DC_NNTHUE, " & _
                                 "NGAY_CTU = :NGAY_CT,NGAY_HT = SYSDATE,MA_CQTHU = :MA_CQTHU, " & _
                                 "NGAY_NTIEN = :NGAY_NTIEN,TEN_KB = :TEN_KB, " & _
                                 "MA_DBHC = :MA_DBHC,TEN_DBHC = :TEN_DBHC,MA_NT = :MA_NT,TY_GIA = :TY_GIA, " & _
                                 "TTIEN = :TTIEN,TTIEN_NT = :TTIEN_NT,TK_KH_NH = :TK_KH_NH, " & _
                                 "NGAY_KH_NH = :NGAY_KH_NH,NH_GIU_TKKB = :NH_GIU_TKKB, TRANG_THAI = :TRANG_THAI," & _
                                 "CQ_QD = :CQ_QD,TEN_CQTHU = :TEN_CQTHU, " & _
                                 "SO_CT_NH = :SO_CT_NH,TENKH_NHAN = :TENKH_NHAN,TK_KH_NHAN = :TK_KH_NHAN, " & _
                                 "DIACHI_KH_NHAN = :DIACHI_KH_NHAN,NH_GIU_TKNNT = :NH_GIU_TKNNT, " & _
                                 "MA_HQ = :MA_HQ,TEN_HQ = :TEN_HQ, " & _
                                 "MA_HQ_PH = :MA_HQ_PH,TEN_HQ_PH = :TEN_HQ_PH,REMARK = :REMARK,CONTENT_EX = :CONTENT_EX,NGAY_CITAD = :NGAY_CITAD, SOBT_CITAD = :SOBT_CITAD, TEN_NGUOI_CHUYEN = :TEN_NGUOI_CHUYEN " & _
                          " WHERE   SO_CT = :SO_CT "
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_CO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_NO", ParameterDirection.Input, CStr(hdr.TK_NO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(hdr.MA_DTHU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, CStr(hdr.MA_LTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.MA_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, CStr(hdr.DC_NNTHUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":NGAY_CT", ParameterDirection.Input, CStr(hdr.NGAY_CTU), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_HT", ParameterDirection.Input, CStr(hdr.NGAY_HT), DbType.String))       'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_NTIEN", ParameterDirection.Input, CStr(hdr.NGAY_NTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(hdr.TEN_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DBHC", ParameterDirection.Input, CStr(hdr.MA_DBHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_DBHC", ParameterDirection.Input, CStr(hdr.TEN_DBHC), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TY_GIA", ParameterDirection.Input, CStr(hdr.TY_GIA), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NT", ParameterDirection.Input, CStr(hdr.TTIEN_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, hdr.NGAY_KH_NH, DbType.Date)) 'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":NH_GIU_TKKB", ParameterDirection.Input, CStr(hdr.NH_GIU_TKKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":CQ_QD", ParameterDirection.Input, CStr(hdr.CQ_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(hdr.TEN_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT_NH", ParameterDirection.Input, CStr(hdr.SO_CT_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TENKH_NHAN", ParameterDirection.Input, CStr(hdr.TENKH_NHAN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NHAN", ParameterDirection.Input, CStr(hdr.TK_KH_NHAN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DIACHI_KH_NHAN", ParameterDirection.Input, CStr(hdr.DIACHI_KH_NHAN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NH_GIU_TKNNT", ParameterDirection.Input, CStr(hdr.NH_GIU_TKNNT), DbType.String))

                    'listParam.Add(DataAccess.NewDBParameter(":TT_SP", ParameterDirection.Input, CStr(hdr.TT_SP), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ", ParameterDirection.Input, CStr(hdr.MA_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ", ParameterDirection.Input, CStr(hdr.TEN_HQ), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_HQ_PH", ParameterDirection.Input, CStr(hdr.MA_HQ_PH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_HQ_PH", ParameterDirection.Input, CStr(hdr.TEN_HQ_PH), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":REMARK", ParameterDirection.Input, CStr(hdr.REMARK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":CONTENT_EX", ParameterDirection.Input, CStr(hdr.CONTENT_EX), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":NGAY_CITAD", ParameterDirection.Input, CStr(hdr.NGAY_CTITAD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SOBT_CITAD", ParameterDirection.Input, CStr(hdr.SOBT_CITAD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NGUOI_CHUYEN", ParameterDirection.Input, CStr(hdr.TEN_NGUOI_CHUYEN), DbType.String))

                    Dim vSql As String = DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)

                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If dtl IsNot Nothing AndAlso dtl.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each objDtl In dtl
                                objDtl.SO_CT = hdr.SO_CT
                                objDtl.SHKB = hdr.SHKB
                                objDtl.NGAY_KB = hdr.NGAY_KB
                                objDtl.MA_NV = hdr.MA_NV
                                objDtl.SO_BT = hdr.SO_BT
                                objDtl.MA_DTHU = hdr.MA_DTHU
                                iRet = 0
                                sSQL = "INSERT INTO TCS_CTU_SONGPHUONG_DTL(" & _
                                            "ID,MA_NV,SO_BT,MA_CHUONG,MA_MUC,MA_TMUC," & _
                                            "NOI_DUNG,KY_THUE,SOTIEN,SOTIEN_NT,SO_CT, SO_TK, NGAY_TK, " & _
                                            "SAC_THUE) " & _
                                        "VALUES(" & _
                                            "tcs_ctu_songphuong_dtl_seq.NEXTVAL,:MA_NV, :SO_BT, :MA_CHUONG, '', :MA_TMUC," & _
                                            ":NOI_DUNG, :KY_THUE, :SOTIEN, :SOTIEN_NT, :SO_CT, :SO_TK, TO_DATE(:NGAY_TK,'dd/MM/yyyy'), " & _
                                            ":SAC_THUE)"
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(objDtl.MA_NV), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(objDtl.SO_BT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(objDtl.MA_CHUONG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_TMUC", ParameterDirection.Input, CStr(objDtl.MA_TMUC), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(objDtl.NOI_DUNG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":KY_THUE", ParameterDirection.Input, CStr(objDtl.KY_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN", ParameterDirection.Input, CStr(objDtl.SOTIEN), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SOTIEN_NT", ParameterDirection.Input, CStr(objDtl.SOTIEN_NT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(objDtl.SO_CT), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_TK", ParameterDirection.Input, CStr(objDtl.SO_TK), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NGAY_TK", ParameterDirection.Input, objDtl.NGAY_TK, DbType.Date))
                                listParam.Add(DataAccess.NewDBParameter(":SAC_THUE", ParameterDirection.Input, CStr(objDtl.SAC_THUE), DbType.String))

                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next
                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If
                    If bRet = True Then
                        'cap nhat trang thai bang ttsp 02
                        sSQL = "UPDATE   tcs_chungtu_ttsp " & _
                          "SET  TRANG_THAI = '02'  WHERE SO_CT= :SO_CT "
                        listParam = New List(Of IDataParameter)
                        listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                        iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    End If
                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try
            Return bRet
        End Function
        Public Shared Function GhiChungTuBL(ByVal hdr As BienLaiObj.HdrBl, ByVal dtl() As BienLaiObj.DtlBl, ByVal sActionType As String, ByVal sSaveType As String) As String
            Dim sTenDN As String = ""
            Dim sDesCTU As String = ""
            Dim sRet As String = ""
            Try

                If hdr Is Nothing Then
                    sRet = ""
                    Throw New Exception("Không có thông tin chứng từ để lưu. Vui lòng kiểm tra lại")
                    Return sRet
                End If

                Dim objUser = Nothing
                If hdr IsNot Nothing Then objUser = New CTuUser(hdr.MA_NV)
                If objUser IsNot Nothing Then sTenDN = objUser.Ten_DN

                Dim blnVND As Boolean = True
                If (hdr.MA_NT <> "VND") Then blnVND = False

                Select Case sActionType
                    Case "GHI"
                        hdr.TRANG_THAI = "00"
                        If (sSaveType = "INSERT") Then

                            If SongPhuongController.Insert_ChungtuBL(hdr, dtl) Then
                                sRet = "Success;Hoàn thiện chứng từ thành công"
                            Else
                                sRet = String.Empty
                            End If
                        ElseIf sSaveType = "UPDATE" Then
                            If hdr.TRANG_THAI = "00" OrElse hdr.TRANG_THAI = "03" Then
                                hdr.TRANG_THAI = "00"
                                If SongPhuongController.Update_ChungTuBL(hdr, dtl) Then
                                    sRet = "Success;Cập nhật chứng từ thành công"
                                Else
                                    sRet = String.Empty
                                End If
                            Else
                                sRet = ""
                                Throw New Exception("Trạng thái chứng từ không cho phép cập nhật")
                                Return sRet
                            End If
                        End If
                    Case "GHIKS"
                        If sSaveType = "INSERT_KS" Then
                            hdr.TRANG_THAI = "02"

                            If SongPhuongController.Insert_ChungtuBL(hdr, dtl) Then
                                sRet = "Success;Hoàn thiện và gửi kiểm soát chứng từ thành công"
                            Else
                                sRet = String.Empty
                            End If
                        ElseIf sSaveType = "UPDATE_KS" Then
                            If hdr.TRANG_THAI = "04" Then
                                hdr.TRANG_THAI = "02"
                                If SongPhuongController.Update_ChungTuBL(hdr, dtl) Then
                                    sRet = "Success;Cập nhật và gửi kiểm soát thành công"
                                Else
                                    sRet = String.Empty
                                End If
                            ElseIf hdr.TRANG_THAI = "07" Then
                                hdr.TRANG_THAI = "02"
                                If SongPhuongController.Update_ChungTuBL_NEW(hdr, dtl) Then
                                    sRet = "Success;Cập nhật và gửi kiểm soát thành công"
                                Else
                                    sRet = String.Empty
                                End If
                            Else
                                sRet = ""
                                Throw New Exception("Trạng thái chứng từ không cho phép cập nhật")
                                Return sRet
                            End If
                        End If
                    Case "GHIDC"
                        hdr.TRANG_THAI = "19" 'Trạng thái 19 - điều chỉnh gửi kiểm soát
                        If sSaveType = "UPDATE_DC" Then
                            hdr.TRANG_THAI = "19"
                            If SongPhuongController.Update_ChungTuBL(hdr, dtl) Then
                                sRet = "Success;Điều chỉnh và gửi kiểm soát thành công"
                            Else
                                sRet = String.Empty
                            End If

                        End If
                End Select

                'Nối thêm thông tin load lên form
                sRet = sRet & ";" & hdr.SHKB & "/" & hdr.SO_CT & "/" & hdr.SO_BT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi ghi chứng từ", sTenDN)
                If ex.Message.Contains("TCS_BIENLAI_HDR_PK") Then
                    sRet = ""
                    If sSaveType.StartsWith("INSERT") Then
                        Throw New Exception("Không thể thêm mới! Đã tồn tại giấy nộp tiền này trong hệ thống")
                    ElseIf sSaveType.StartsWith("UPDATE") Then
                        Throw New Exception("Không thể cập nhật! Đã tồn tại giấy nộp tiền này trong hệ thống")
                    End If
                Else
                    sRet = ""
                    Throw ex
                End If
            End Try

            Return sRet
        End Function

        Public Shared Function Insert_ChungtuBL(ByVal hdr As BienLaiObj.HdrBl, ByVal dtl() As BienLaiObj.DtlBl) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then
                    Try
                        hdr.KYHIEU_CT = SongPhuongController.GetKyHieuChungTu(hdr.SHKB, hdr.MA_NV, "04")
                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh ký hiệu chứng từ")
                        Return bRet
                    End Try

                    Try
                        'nên chuyển vào trong câu query
                        If String.IsNullOrEmpty(hdr.NGAY_KB) Then
                            hdr.NGAY_KB = SongPhuongController.GetNgayKhoBac(hdr.SHKB)
                        End If

                        Dim sSo_BT As String = "0" 'String.Empty
                        Dim sNam_KB As String
                        Dim sThang_KB As String
                        sNam_KB = Mid(hdr.NGAY_KB, 3, 2)
                        sThang_KB = Mid(hdr.NGAY_KB, 5, 2)
                        'Sinh so CT ngay khi nhan ve tu corebank

                        If String.IsNullOrEmpty(hdr.SO_CT) Then
                            Dim sSoCT = CTuCommon.Get_SoCT_BL()
                            hdr.SO_CT = sNam_KB + sThang_KB + sSoCT
                        End If

                        If String.IsNullOrEmpty(hdr.SO_BIENLAI) Then
                            hdr.SO_BIENLAI = Business.SP.SongPhuongController.fnc_GetSoBienLai()
                        End If

                        hdr.KYHIEU_CT = hdr.KYHIEU_CT & hdr.SO_CT
                        sSo_BT = SongPhuongController.GetSoBT_BL(hdr.NGAY_KB, hdr.MA_NV)
                        If Not String.IsNullOrEmpty(sSo_BT) Then
                            hdr.SO_BT = CInt(sSo_BT + 1)
                        Else
                            hdr.SO_BT = 1
                        End If

                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh số chứng từ")
                        Return bRet
                    End Try

                    If hdr.NGAY_NTIEN IsNot Nothing Then
                        hdr.NGAY_NTIEN = hdr.NGAY_NTIEN.Replace("/", "-")
                    End If
                    If hdr.NGAY_CT IsNot Nothing Then
                        hdr.NGAY_CT = hdr.NGAY_CT.Replace("/", "-")
                    Else
                    End If
                    If hdr.NGAY_QD IsNot Nothing Then
                        hdr.NGAY_QD = hdr.NGAY_QD.Replace("-", "/")
                    Else
                    End If


                    conn = DataAccess.GetConnection() 'New OracleConnection(VBOracleLib.DataAccess.strConnection)
                    ' conn.Open()
                    tran = conn.BeginTransaction()

                    sSQL = "INSERT INTO TCS_BIENLAI_HDR(SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,SO_CT,NGAY_CT,KYHIEU_CT,TK_CO, " & _
                                "TEN_KB,MA_NNTHUE,TEN_NNTHUE,DC_NNTHUE,MA_NNTIEN,TEN_NNTIEN,DC_NNTIEN,MA_LTHUE,TEN_LTHUE," & _
                                "MA_CQTHU,TEN_CQTHU,MA_DBHC, TEN_DBHC,MA_NH_TT,TEN_NH_TT,MA_NH_GT,TEN_NH_GT,MA_NT,TTIEN," & _
                                "TRANG_THAI,TK_KH_NH,TEN_KH_NH,DIEN_GIAI,PT_TT,NGAY_HT,MA_CN, " & _
                                "MA_CQQD,TEN_CQQD,SO_QD,NGAY_QD," & _
                                "MA_LHTHU,TEN_LHTHU,LY_DO_VPHC,TTIEN_CTBL,TTIEN_VPHC, TTIEN_NOP_CHAM, NGAY_KH_NH, SO_BIENLAI,ten_huyen_nnthue, ten_tinh_nnthue,ten_huyen_nntien, ten_tinh_nntien,MA_NTK, HT_THU,product)" & _
                            "VALUES(:SHKB, :NGAY_KB, :MA_NV, :SO_BT, :MA_DTHU,:SO_CT,sysdate,:KYHIEU_CT,:TK_CO," & _
                                ":TEN_KB,:MA_NNTHUE, :TEN_NNTHUE,:DC_NNTHUE, :MA_NNTIEN, :TEN_NNTIEN, :DC_NNTIEN,:MA_LTHUE,:TEN_LTHUE, " & _
                                " :MA_CQTHU,:TEN_CQTHU,:MA_DBHC, :TEN_DBHC,:MA_NH_TT,:TEN_NH_TT,:MA_NH_GT,:TEN_NH_GT,:MA_NT,:TTIEN, " & _
                                " :TRANG_THAI,:TK_KH_NH,:TEN_KH_NH,:DIEN_GIAI,:PT_TT,sysdate, :MA_CN,  " & _
                                ":MA_CQQD,:TEN_CQQD,:SO_QD,to_date(:NGAY_QD,'dd/MM/yyyy'), " & _
                                ":MA_LHTHU,:TEN_LHTHU, :LY_DO_VPHC, :TTIEN_CTBL, :TTIEN_VPHC, :TTIEN_NOP_CHAM,sysdate,:SO_BIENLAI,:ten_huyen_nnthue, :ten_tinh_nnthue,:ten_huyen_nntien, :ten_tinh_nntien, :MA_NTK, :HT_THU,:product)"

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(hdr.NGAY_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(hdr.MA_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(hdr.SO_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(hdr.MA_DTHU), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":TEN_DTHU", ParameterDirection.Input, CStr(hdr.TEN_DTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_CT", ParameterDirection.Input, CStr(hdr.NGAY_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KYHIEU_CT", ParameterDirection.Input, CStr(hdr.KYHIEU_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_CO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(hdr.TEN_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, CStr(hdr.DC_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.MA_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":CQ_QD", ParameterDirection.Input, CStr(hdr.CQ_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, CStr(hdr.MA_LTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LTHUE", ParameterDirection.Input, CStr(hdr.TEN_LTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(hdr.TEN_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DBHC", ParameterDirection.Input, CStr(hdr.MA_DBHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_DBHC", ParameterDirection.Input, CStr(hdr.TEN_DBHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(hdr.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, CStr(hdr.TEN_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_GT", ParameterDirection.Input, CStr(hdr.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_GT", ParameterDirection.Input, CStr(hdr.TEN_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(hdr.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DIEN_GIAI", ParameterDirection.Input, CStr(hdr.REMARK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(hdr.PT_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(hdr.MA_CN), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_CQQD", ParameterDirection.Input, CStr(hdr.MA_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQQD", ParameterDirection.Input, CStr(hdr.TEN_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_QD", ParameterDirection.Input, CStr(hdr.SO_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_QD", ParameterDirection.Input, hdr.NGAY_QD, DbType.Date))

                    listParam.Add(DataAccess.NewDBParameter(":MA_LHTHU", ParameterDirection.Input, CStr(hdr.MA_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHTHU", ParameterDirection.Input, CStr(hdr.TEN_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LY_DO_VPHC", ParameterDirection.Input, CStr(hdr.LY_DO_VPHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_CTBL", ParameterDirection.Input, CStr(hdr.TTIEN_CTBL), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_VPHC", ParameterDirection.Input, CStr(hdr.TTIEN_VPHC), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":LY_DO_NOP_CHAM", ParameterDirection.Input, CStr(hdr.LY_DO_NOP_CHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NOP_CHAM", ParameterDirection.Input, CStr(hdr.TTIEN_NOP_CHAM), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, hdr.NGAY_KH_NH, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":SO_BIENLAI", ParameterDirection.Input, CStr(hdr.SO_BIENLAI), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":ten_huyen_nnthue", ParameterDirection.Input, CStr(hdr.TEN_HUYEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_tinh_nnthue", ParameterDirection.Input, CStr(hdr.TEN_TINH_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_huyen_nntien", ParameterDirection.Input, CStr(hdr.TEN_HUYEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_tinh_nntien", ParameterDirection.Input, CStr(hdr.TEN_TINH_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(hdr.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":HT_THU", ParameterDirection.Input, CStr(hdr.HT_THU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":product", ParameterDirection.Input, CStr(hdr.PRODUCT), DbType.String))
                    Dim v_sqlT As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....

                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If dtl IsNot Nothing AndAlso dtl.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each objDtl In dtl
                                sSQL = "UPDATE   TCS_BIENLAI_HDR  SET MA_CHUONG=:MA_CHUONG,MA_NDKT=:MA_NDKT,NOI_DUNG=:NOI_DUNG,KY_THUE=:KY_THUE WHERE SO_CT=:SO_CT"

                                iRet = 0

                                listParam = New List(Of IDataParameter)

                                listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(objDtl.MA_CHUONG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_NDKT", ParameterDirection.Input, CStr(objDtl.MA_TMUC), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(objDtl.NOI_DUNG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":KY_THUE", ParameterDirection.Input, CStr(objDtl.KY_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))


                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    'If iRet > 0 Then 'if insert HDR is success then insert DTL list
                    '    If dtl IsNot Nothing AndAlso dtl.Count > 0 Then
                    '        Dim bFlag As Boolean = False
                    '        For Each objDtl In dtl
                    '            objDtl.SO_CT = hdr.SO_CT
                    '            objDtl.SHKB = hdr.SHKB
                    '            objDtl.NGAY_KB = hdr.NGAY_KB
                    '            objDtl.MA_NV = hdr.MA_NV
                    '            objDtl.SO_BT = hdr.SO_BT
                    '            objDtl.MA_DTHU = hdr.MA_DTHU

                    '            iRet = 0
                    '            sSQL = "INSERT INTO TCS_BIENLAI_DTL(" & _
                    '                        "ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MA_NDKT," & _
                    '                        "NOI_DUNG,KY_THUE,SOTIEN,SO_CT) " & _
                    '                    "VALUES(" & _
                    '                        "tcs_bienlai_dtl_seq.NEXTVAL,:SHKB,:NGAY_KB,:MA_NV, :SO_BT,:MA_DTHU, :MA_CHUONG, :MA_NDKT," & _
                    '                        ":NOI_DUNG, :KY_THUE, :SOTIEN, :SO_CT)"

                    '            listParam = New List(Of IDataParameter)
                    '            listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(objDtl.SHKB), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(objDtl.NGAY_KB), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(objDtl.MA_NV), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(objDtl.SO_BT), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(objDtl.MA_DTHU), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(objDtl.MA_CHUONG), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":MA_NDKT", ParameterDirection.Input, CStr(objDtl.MA_TMUC), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(objDtl.NOI_DUNG), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":KY_THUE", ParameterDirection.Input, CStr(objDtl.KY_THUE), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":SOTIEN", ParameterDirection.Input, CStr(objDtl.SOTIEN), DbType.String))
                    '            listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(objDtl.SO_CT), DbType.String))

                    '            Dim v_sqlDTL As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)

                    '            iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    '            'if any DTL row insert is fail then rollback all transaction
                    '            If iRet > 0 Then
                    '                bFlag = True
                    '            Else
                    '                bFlag = False
                    '            End If
                    '        Next

                    '        If bFlag Then
                    '            bRet = True
                    '        End If
                    '    Else
                    '        bRet = True
                    '    End If
                    'End If


                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function
        Public Shared Function Update_ChungTuBL(ByVal hdr As BienLaiObj.HdrBl, ByVal dtl() As BienLaiObj.DtlBl) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then
                    conn = VBOracleLib.DataAccess.GetConnection()
                    '  conn.Open()
                    tran = conn.BeginTransaction()
                    'delete DTL
                    sSQL = "DELETE FROM TCS_BIENLAI_DTL WHERE SO_CT = :SO_CT"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    If hdr.NGAY_NTIEN IsNot Nothing Then
                        hdr.NGAY_NTIEN = hdr.NGAY_NTIEN.Replace("/", "-")
                    End If
                    If hdr.NGAY_CT IsNot Nothing Then
                        hdr.NGAY_CT = hdr.NGAY_CT.Replace("/", "-")
                    End If
                    If hdr.NGAY_QD IsNot Nothing Then
                        hdr.NGAY_QD = hdr.NGAY_QD.Replace("-", "/")
                    Else
                    End If
                    'update HDR
                    sSQL = "UPDATE   TCS_BIENLAI_HDR " & _
                           "SET   SHKB = :SHKB,TEN_KB = :TEN_KB,  " & _
                                 "MA_NNTIEN = :MA_NNTIEN,TEN_NNTIEN = :TEN_NNTIEN,DC_NNTIEN = :DC_NNTIEN, " & _
                                   "MA_NNTHUE = :MA_NNTHUE,TEN_NNTHUE = :TEN_NNTHUE,DC_NNTHUE = :DC_NNTHUE, " & _
                                 "PT_TT = :PT_TT,MA_NT = :MA_NT,TK_KH_NH =:TK_KH_NH,TEN_KH_NH = :TEN_KH_NH,MA_DTHU = :MA_DTHU," & _
                                 "MA_CQTHU = :MA_CQTHU,TEN_CQTHU = :TEN_CQTHU, MA_LTHUE = :MA_LTHUE,TK_CO = :TK_CO,NGAY_HT = SYSDATE, " & _
                                 "MA_NH_TT = :MA_NH_TT,TEN_NH_TT = :TEN_NH_TT, MA_NH_GT = :MA_NH_GT,TEN_NH_GT = :TEN_NH_GT,TTIEN = :TTIEN, " & _
                                 "MA_CQQD = :MA_CQQD, TEN_CQQD =:TEN_CQQD,MA_LHTHU = :MA_LHTHU,TEN_LHTHU = :TEN_LHTHU," & _
                                 "SO_QD = :SO_QD,NGAY_QD = to_date(:NGAY_QD,'dd/MM/yyyy'),TTIEN_VPHC = :TTIEN_VPHC, " & _
                                 "LY_DO_VPHC = :LY_DO_VPHC,TTIEN_NOP_CHAM = :TTIEN_NOP_CHAM, " & _
                                 "TTIEN_CTBL = :TTIEN_CTBL,DIEN_GIAI = :DIEN_GIAI, TRANG_THAI = :TRANG_THAI, SO_BIENLAI = :SO_BIENLAI, " & _
                                   "ten_huyen_nnthue = :ten_huyen_nnthue,ten_tinh_nnthue = :ten_tinh_nnthue, ten_huyen_nntien = :ten_huyen_nntien, ten_tinh_nntien = :ten_tinh_nntien, MA_NTK =:MA_NTK, HT_THU = :HT_THU " & _
                          " WHERE   SO_CT = :SO_CT "

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(hdr.TEN_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.MA_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, CStr(hdr.DC_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(hdr.PT_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(hdr.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(hdr.MA_DTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(hdr.TEN_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, CStr(hdr.MA_LTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_CO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(hdr.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, CStr(hdr.TEN_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_GT", ParameterDirection.Input, CStr(hdr.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_GT", ParameterDirection.Input, CStr(hdr.TEN_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQQD", ParameterDirection.Input, CStr(hdr.MA_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQQD", ParameterDirection.Input, CStr(hdr.TEN_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_QD", ParameterDirection.Input, CStr(hdr.SO_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_QD", ParameterDirection.Input, hdr.NGAY_QD, DbType.Date))


                    listParam.Add(DataAccess.NewDBParameter(":MA_LHTHU", ParameterDirection.Input, CStr(hdr.MA_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHTHU", ParameterDirection.Input, CStr(hdr.TEN_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LY_DO_VPHC", ParameterDirection.Input, CStr(hdr.LY_DO_VPHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_CTBL", ParameterDirection.Input, CStr(hdr.TTIEN_CTBL), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_VPHC", ParameterDirection.Input, CStr(hdr.TTIEN_VPHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NOP_CHAM", ParameterDirection.Input, CStr(hdr.TTIEN_NOP_CHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DIEN_GIAI", ParameterDirection.Input, CStr(hdr.REMARK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_CT", ParameterDirection.Input, CStr(hdr.NGAY_CT), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_HT", ParameterDirection.Input, CStr(hdr.NGAY_HT), DbType.String))       'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":SO_BIENLAI", ParameterDirection.Input, CStr(hdr.SO_BIENLAI), DbType.String))


                    listParam.Add(DataAccess.NewDBParameter(":ten_huyen_nnthue", ParameterDirection.Input, CStr(hdr.TEN_HUYEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_tinh_nnthue", ParameterDirection.Input, CStr(hdr.TEN_TINH_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_huyen_nntien", ParameterDirection.Input, CStr(hdr.TEN_HUYEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_tinh_nntien", ParameterDirection.Input, CStr(hdr.TEN_TINH_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(hdr.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":HT_THU", ParameterDirection.Input, CStr(hdr.HT_THU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))

                    Dim vSql As String = DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)

                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If dtl IsNot Nothing AndAlso dtl.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each objDtl In dtl
                                sSQL = "UPDATE   TCS_BIENLAI_HDR  SET MA_CHUONG=:MA_CHUONG,MA_NDKT=:MA_NDKT,NOI_DUNG=:NOI_DUNG,KY_THUE=:KY_THUE WHERE SO_CT=:SO_CT"

                                iRet = 0


                                listParam = New List(Of IDataParameter)

                                listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(objDtl.MA_CHUONG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_NDKT", ParameterDirection.Input, CStr(objDtl.MA_TMUC), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(objDtl.NOI_DUNG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":KY_THUE", ParameterDirection.Input, CStr(objDtl.KY_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))


                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()

                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function
        Public Shared Function GetKyHieuChungTu(ByVal strSHKB As String, Optional ByVal strMaNV As String = "", Optional ByVal strLoaiGD As String = "01") As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                sSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT'"
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString().Substring(0, 3)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Public Shared Function GetNgayKhoBac(ByVal strSHKB As String) As Long
            Dim lRet As Long = 0
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = " SELECT TO_NUMBER (TO_CHAR (TO_DATE (giatri_ts, 'DD/MM/YYYY'), 'YYYYMMDD')) FROM tcs_thamso " & _
                        " WHERE ten_ts = 'NGAY_LV' AND ma_dthu = (SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS = :GIATRI_TS)"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("GIATRI_TS", ParameterDirection.Input, CStr(strSHKB), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    If Not Long.TryParse(dt.Rows(0)(0).ToString(), lRet) Then lRet = Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
                Else
                    lRet = Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
                End If
            Catch ex As Exception
                lRet = Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
            End Try

            Return lRet
        End Function
        'Private Shared Function Check_ListDTL2(ByVal dtl() As SongPhuongObj.Dtl, ByVal fblnBatBuocMLNS As Boolean, _
        '                            ByVal fblnVND As Boolean, ByVal fblnBatBuocTLDT As Boolean, ByVal sTenDN As String) As Integer()
        '    Dim intReturn(1) As Integer
        '    Try
        '        Dim strMaQuy As String = ""
        '        Dim i, iCol As Integer
        '        For i = 0 To dtl.Count - 1
        '            iCol = Valid_DTL2(dtl(i), fblnBatBuocMLNS, fblnVND, fblnBatBuocTLDT, sTenDN)
        '            If (iCol <> -1) Then
        '                intReturn(0) = i
        '                intReturn(1) = iCol
        '                Return intReturn
        '            End If
        '        Next
        '        Return Nothing
        '    Catch ex As Exception
        '        CTuCommon.WriteLog(ex, "Lỗi valid dữ liệu dtl", sTenDN)
        '        Return Nothing
        '    End Try
        'End Function

        'Private Shared Function Valid_DTL2(ByVal dtl As SongPhuongObj.Dtl, ByVal fblnBatBuocMLNS As Boolean, _
        '                                    ByVal fblnVND As Boolean, ByVal fblnBatBuocTLDT As Boolean, ByVal sTenDN As String) As Integer
        '    Try
        '        If (fblnBatBuocMLNS = True) Then
        '            If (Not SongPhuongController.Valid_TMuc(dtl.MA_TMUC, True)) Then
        '                Return Common.mdlCommon.DTLColumn.MTieuMuc
        '            End If
        '        End If

        '        If (dtl.SOTIEN = 0) Then
        '            If (fblnVND) Then
        '                Return Common.mdlCommon.DTLColumn.Tien
        '            End If
        '        End If
        '        Return -1
        '    Catch ex As Exception
        '        CTuCommon.WriteLog(ex, "Lỗi valid chứng từ DTL", sTenDN)
        '        Return -1
        '    End Try
        'End Function
        Public Shared Function Valid_TMuc(ByVal sMaTMuc As String, Optional ByVal bDangHDong As Boolean = False) As Boolean
            Dim bRet As Boolean = False
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                If bDangHDong Then
                    sSQL = "Select 1 From TCS_DM_MUC_TMUC Where MA_TMUC = :MA_TMUC and tinh_trang = '1' "
                Else
                    sSQL = "Select 1 From TCS_DM_MUC_TMUC Where MA_TMUC = :MA_TMUC"
                End If

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_TMUC", ParameterDirection.Input, CStr(sMaTMuc), DbType.String))
                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString()
                End If

                If Not String.IsNullOrEmpty(sRet) Then
                    bRet = True
                End If
            Catch ex As Exception
                sRet = ""
                bRet = False
            End Try

            Return sRet
        End Function

        Public Shared Function GetSoBT(ByVal iNgayLV As Integer, ByVal iMaNV As Integer) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select MAX(So_BT) AS So_BT From TCS_CTU_SONGPHUONG_HDR Where ngay_kb = :ngay_kb AND Ma_NV = :Ma_NV"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("ngay_kb", ParameterDirection.Input, iNgayLV, DbType.Int32))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, iMaNV, DbType.Int32))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Public Shared Function GetSoBT_BL(ByVal iNgayLV As Integer, ByVal iMaNV As Integer) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select MAX(So_BT) AS So_BT From TCS_BIENLAI_HDR Where ngay_kb = :ngay_kb AND Ma_NV = :Ma_NV"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("ngay_kb", ParameterDirection.Input, iNgayLV, DbType.Int32))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, iMaNV, DbType.Int32))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function


        Public Shared Function CheckTrungRefCore(ByVal sSo_CT_NH As String, ByVal sSo_CT As String) As Boolean
            Dim bRet As Boolean = False
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                If sSo_CT IsNot Nothing Then
                    sSQL = "Select 1 From tcs_ctu_songphuong_hdr Where SO_CT_NH = :SO_CT_NH and SO_CT <> '" & sSo_CT & "' and trang_thai in ('01','02','03','04','05') "
                Else
                    sSQL = "Select 1 From tcs_ctu_songphuong_hdr Where SO_CT_NH = :SO_CT_NH and  trang_thai in ('01','02','03','04','05') "
                End If

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SO_CT_NH", ParameterDirection.Input, CStr(sSo_CT_NH), DbType.String))
                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString()
                End If

                If Not String.IsNullOrEmpty(sRet) Then
                    bRet = True
                End If

            Catch ex As Exception
                bRet = False
            End Try

            Return bRet
        End Function
        Public Shared Function HuyChungTu(ByVal sSo_CT As String, ByVal sLyDo As String, ByVal sMaNV As String) As String
            Dim sRet As String = String.Empty
            Dim sTenDN As String = ""
            Dim sSQL As String = String.Empty
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                Dim sTrangThaiCTu As String = String.Empty
                Dim sTT_SP As String = String.Empty
                Dim sSHKB As String = String.Empty
                Dim sSo_BT As String = String.Empty

                If String.IsNullOrEmpty(sSo_CT) Then
                    sRet = ""
                    Throw New Exception("Vui lòng chọn chứng từ để hủy")
                    Return sRet
                Else
                    sSQL = "select TRANG_THAI, TT_SP, SHKB from tcs_ctu_songphuong_hdr where SO_CT = :SO_CT and 1 = 1"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                    dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                        If dt.Rows(0)("TRANG_THAI") IsNot Nothing AndAlso dt.Rows(0)("TRANG_THAI") IsNot DBNull.Value Then
                            sTrangThaiCTu = dt.Rows(0)("TRANG_THAI").ToString()
                        End If
                        If dt.Rows(0)("TT_SP") IsNot Nothing AndAlso Not IsDBNull(dt.Rows(0)("TT_SP")) Then
                            sTT_SP = dt.Rows(0)("TT_SP")
                        End If

                        If Not String.IsNullOrEmpty(sTrangThaiCTu) Then
                            If sTrangThaiCTu = "04" Or sTrangThaiCTu = "02" Then
                                sSQL = "update tcs_ctu_songphuong_hdr set TRANG_THAI = '06',NGAY_GDV_HUY= sysdate, ly_do_huy = :LY_DO where SO_CT = :SO_CT and TRANG_THAI in ('01','02','04') "
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":LY_DO", ParameterDirection.Input, sLyDo, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
                            ElseIf sTrangThaiCTu = "01" Then
                                sSQL = "update tcs_ctu_songphuong_hdr set TRANG_THAI = '06',NGAY_GDV_HUY= sysdate, ly_do_huy = :LY_DO where SO_CT = :SO_CT and TRANG_THAI in ('01','02','04') "
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":LY_DO", ParameterDirection.Input, sLyDo, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())

                                sSQL = "select Id, SO_CT, so_ref_core, TRANG_THAI from tcs_chungtu_ttsp where SO_CT = :SO_CT and 1 = 1"
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                                    sSQL = "update tcs_chungtu_ttsp set TRANG_THAI = '06' where SO_CT = :SO_CT "
                                    listParam = New List(Of IDataParameter)
                                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
                                End If
                                
                            Else
                                Throw New Exception("Trạng thái chứng từ không cho phép hủy")
                            End If

                            If iRet > 0 Then
                                sRet = "Hủy chứng từ thành công"
                                'Nối thêm thông tin load lên form
                                sRet = sRet & ";" & sSHKB & "/" & sSo_CT & "/" & sSo_BT
                            End If
                        End If
                    Else
                        sRet = ""
                        Throw New Exception("Không tìm thấy chứng từ trong hệ thống")
                        Return sRet
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi hủy chứng từ", sTenDN)
                sRet = ""
                Throw ex
            End Try

            Return sRet
        End Function
        Public Shared Function HuyChungTuBL(ByVal sSo_CT As String, ByVal sLyDo As String, ByVal sMaNV As String) As String
            Dim sRet As String = String.Empty
            Dim sTenDN As String = ""
            Dim sSQL As String = String.Empty
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                Dim sTrangThaiCTu As String = String.Empty
                Dim sTT_SP As String = String.Empty
                Dim sSHKB As String = String.Empty
                Dim sSo_BT As String = String.Empty

                If String.IsNullOrEmpty(sSo_CT) Then
                    sRet = ""
                    Throw New Exception("Vui lòng chọn chứng từ để hủy")
                    Return sRet
                Else
                    sSQL = "select TRANG_THAI, TT_SP, SHKB from tcs_bienlai_hdr where SO_CT = :SO_CT and 1 = 1"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                    dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                        If dt.Rows(0)("TRANG_THAI") IsNot Nothing AndAlso dt.Rows(0)("TRANG_THAI") IsNot DBNull.Value Then
                            sTrangThaiCTu = dt.Rows(0)("TRANG_THAI").ToString()
                        End If
                        If dt.Rows(0)("TT_SP") IsNot Nothing AndAlso Not IsDBNull(dt.Rows(0)("TT_SP")) Then
                            sTT_SP = dt.Rows(0)("TT_SP")
                        End If

                        If Not String.IsNullOrEmpty(sTrangThaiCTu) Then
                            If sTrangThaiCTu = "04" Or sTrangThaiCTu = "01" Then
                                sSQL = "update tcs_bienlai_hdr set TRANG_THAI = '06',NGAY_GDV_HUY= sysdate, ly_do_huy = :LY_DO where SO_CT = :SO_CT and TRANG_THAI in ('01','04') "
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":LY_DO", ParameterDirection.Input, sLyDo, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
                            Else
                                Throw New Exception("Trạng thái chứng từ không cho phép hủy")
                            End If

                            If iRet > 0 Then
                                sRet = "Hủy chứng từ thành công"
                                'Nối thêm thông tin load lên form
                                sRet = sRet & ";" & sSHKB & "/" & sSo_CT & "/" & sSo_BT
                            End If
                        End If
                    Else
                        sRet = ""
                        Throw New Exception("Không tìm thấy chứng từ trong hệ thống")
                        Return sRet
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi hủy chứng từ", sTenDN)
                sRet = ""
                Throw ex
            End Try

            Return sRet
        End Function


        Public Shared Function getDien_NHKhac_TracuuXuly(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = " SELECT  A.SO_CT,A.SO_CT_NH,A.MA_NNTHUE,A.TEN_NNTHUE,A.TTIEN,A.TG_KY,B.mota TRANG_THAI, "
                strSQL &= "  (CASE WHEN A.TT_SP='0' AND A.TRANG_THAI ='03' THEN 'Gui song phuong loi' "
                strSQL &= "  WHEN A.TT_SP='1' AND A.TRANG_THAI ='03' THEN 'Gui song phuong thanh cong'  ELSE '' END) MA_LOI,"
                strSQL &= "  (NVL((SELECT MAX(X.NAVIGATEURL)||'?ID='||A.SO_CT FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '13.%' AND X.NODEID IN ('13.5')),'#')) NAVIGATEURL "
                strSQL &= " FROM  TCS_CTU_SONGPHUONG_HDR A, tcs_dm_trangthai_sp_bl B "
                strSQL &= "  WHERE A.TRANG_THAI = B.TRANGTHAI " & pvStrWhereClause
                strSQL &= "  ORDER BY SO_CT DESC "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception

            End Try
            Return ds
        End Function
        Public Shared Function GetSoCtuBL(ByVal sMaNV As String, ByVal sSoCtu As String, ByVal sLoaiCTu As String) As DataTable
            Dim v_dt As DataTable
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try

                sSQL = "select A.SO_CT_NH, A.SO_CT, A.TRANG_THAI, A.tt_sp " & _
                " from TCS_BIENLAI_HDR A , TCS_DM_NHANVIEN B where  A.MA_NV = B.MA_NV AND A.TRANG_THAI IN ('03','04')  AND SO_CT = :SO_CT  order by A.SO_CT desc "

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, sSoCtu.Trim, DbType.String))
                'listParam.Add(DataAccess.NewDBParameter("MA_NV", ParameterDirection.Input, sMaNV.Trim, DbType.String))
                v_dt = DataAccess.ExecuteReturnDataTable(sSQL, CommandType.Text, listParam.ToArray())

            Catch ex As Exception
                v_dt = Nothing
            End Try

            Return v_dt
        End Function
        Public Shared Function GetListChungTuBL(ByVal sNgayKB As String, ByVal sMaNV As String, ByVal sMA_CN As String, ByVal sProductType As String) As DataSet
            Dim ds As DataSet = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try

                sSQL &= "select A.SO_CT_NH as so_ref_core, A.SO_CT, A.TRANG_THAI,A.TT_SP,A.so_bt   "
                sSQL &= " from TCS_BIENLAI_HDR A, TCS_DM_NHANVIEN B where A.MA_NV = B.MA_NV AND B.TINH_TRANG = '0'  and  a.MA_CN = :MA_CN "
                sSQL &= " and a.NGAY_KB = :NGAY_KB and a.MA_NV = :MA_NV and UPPER(a.product) = UPPER(:product) "
                sSQL &= "  order by so_bt desc "

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_CN", ParameterDirection.Input, CStr(sMA_CN), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("NGAY_KB", ParameterDirection.Input, CStr(sNgayKB), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("product", ParameterDirection.Input, CStr(sProductType), DbType.String))
                ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray())
            Catch ex As Exception
                ds = Nothing
            End Try

            Return ds
        End Function

        Public Shared Function GetChungTuBL(ByVal sSoChungTu As String) As ChungTuBL.infChungTuBL
            Dim oRet As ChungTuBL.infChungTuBL = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Dim dDateTmp As Date
            Try
                'get HDR
                Dim HDR As New ChungTuBL.infChungTuBL_HDR
                Dim strProduct As String = "ETAX"
                sSQL = "select A.PRODUCT, a.ma_nnthue,a.ten_nnthue,a.dc_nnthue,a.PT_TT,a.ma_nt,a.TK_KH_NH,a.TEN_KH_NH, a.ma_nntien,a.ten_nntien, a.dc_nntien, " & _
                "a.SHKB,a.TEN_KB,a.NGAY_KB,a.NGAY_CT,a.MA_CQTHU,a.TEN_CQTHU,a.ma_lthue,a.TK_CO, " & _
                "a.So_BT,a.Ma_DThu,a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
                "a.MA_NH_TT, a.TEN_NH_TT, a.MA_NH_GT, a.TEN_NH_GT, a.TTien," & _
                "a.MA_DBHC,a.TEN_DBHC,a.MA_CQQD, a.TEN_CQQD, a.ma_lhthu, a.ten_lhthu, a.so_qd,to_char(a.ngay_qd,'dd/MM/yyyy') as ngay_qd, a.ttien_vphc, a.ly_do_vphc, a.ttien_nop_cham, a.ttien_ctbl," & _
                "a.Ly_Do,a.ly_do_huy,a.Ma_KS,to_char(a.ngay_kh_nh,'dd/MM/yyyy') as ngay_kh_nh," & _
                " a.DIEN_GIAI,a.ngay_ht,a.ma_cn, a.TRANG_THAI,a.tt_sp,a.MA_NV,a.SO_BIENLAI SO_BIENLAI, a.ten_huyen_nnthue, a.ten_tinh_nnthue, a.ten_huyen_nntien, a.ten_tinh_nntien, a.MA_NTK, a.HT_THU " & _
                ",a.Seri,a.so_tham_chieu,a.ma_nh_kb,  to_char(a.ngay_nntien,'DDMMYYYY') NGAY_NT " & _
                " from TCS_BIENLAI_HDR a " & _
                "where a.SO_CT= :SO_CT"

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    strProduct = dt.Rows(0)("PRODUCT").ToString()
                    'ma va ten DBHC
                    HDR.NGAY_NTIEN = dt.Rows(0)("NGAY_NT").ToString()
                    HDR.SERI = dt.Rows(0)("Seri").ToString()
                    HDR.SO_THAMCHIEU = dt.Rows(0)("so_tham_chieu").ToString()
                    HDR.MA_NH_KB = dt.Rows(0)("ma_nh_kb").ToString()

                    HDR.PRODUCT = dt.Rows(0)("PRODUCT").ToString()
                    HDR.MA_NNTHUE = dt.Rows(0)("MA_NNTHUE").ToString()
                    HDR.TEN_NNTHUE = dt.Rows(0)("TEN_NNTHUE").ToString()
                    HDR.DC_NNTHUE = dt.Rows(0)("DC_NNTHUE").ToString()

                    HDR.PT_TT = dt.Rows(0)("PT_TT").ToString()
                    HDR.MA_NT = dt.Rows(0)("Ma_NT").ToString()

                    HDR.TK_KH_NH = dt.Rows(0)("TK_KH_NH").ToString()
                    HDR.TENTK_KH_NH = dt.Rows(0)("TEN_KH_NH").ToString()

                    HDR.SO_BIENLAI = dt.Rows(0)("SO_BIENLAI").ToString()
                    HDR.TEN_HUYEN_NNTHUE = dt.Rows(0)("ten_huyen_nnthue").ToString()
                    HDR.TEN_TINH_NNTHUE = dt.Rows(0)("ten_tinh_nnthue").ToString()
                    HDR.TEN_HUYEN_NNTIEN = dt.Rows(0)("ten_huyen_nntien").ToString()
                    HDR.TEN_TINH_NNTIEN = dt.Rows(0)("ten_tinh_nntien").ToString()

                    HDR.MA_NNTIEN = dt.Rows(0)("Ma_NNTien").ToString()
                    HDR.TEN_NNTIEN = dt.Rows(0)("TEN_NNTIEN").ToString()
                    HDR.DC_NNTIEN = dt.Rows(0)("DC_NNTIEN").ToString()

                    HDR.SHKB = dt.Rows(0)("SHKB").ToString()
                    HDR.TEN_KB = dt.Rows(0)("TEN_KB").ToString()
                    If dt.Rows(0)("NGAY_KB") Is Nothing OrElse IsDBNull(dt.Rows(0)("NGAY_KB")) Then
                        HDR.NGAY_KB = 0
                    Else
                        HDR.NGAY_KB = dt.Rows(0)("NGAY_KB")
                    End If
                    'Bo sung
                    If dt.Rows(0)("Ngay_CT") Is Nothing OrElse IsDBNull(dt.Rows(0)("Ngay_CT")) Then
                        HDR.NGAY_CT = 0
                    Else
                        HDR.NGAY_CT = dt.Rows(0)("Ngay_CT")
                    End If
                    HDR.MA_CQTHU = dt.Rows(0)("MA_CQTHU").ToString()
                    HDR.TEN_CQTHU = dt.Rows(0)("TEN_CQTHU").ToString()
                    HDR.MA_LTHUE = dt.Rows(0)("MA_LTHUE").ToString()
                    HDR.TK_CO = dt.Rows(0)("TK_Co").ToString()

                    If dt.Rows(0)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(0)("So_BT")) Then
                        HDR.SO_BT = 0
                    Else
                        HDR.SO_BT = dt.Rows(0)("So_BT")
                    End If
                    If dt.Rows(0)("MA_DTHU") Is Nothing OrElse IsDBNull(dt.Rows(0)("MA_DTHU")) Then
                        HDR.MA_DTHU = ""
                    Else
                        HDR.MA_DTHU = dt.Rows(0)("MA_DTHU").ToString()
                    End If

                    HDR.KYHIEU_CT = dt.Rows(0)("KyHieu_CT").ToString()
                    HDR.SO_CT = dt.Rows(0)("So_CT").ToString()
                    HDR.SO_CT_NH = dt.Rows(0)("SO_CT_NH").ToString()

                    HDR.MA_NH_TT = dt.Rows(0)("MA_NH_TT").ToString()
                    HDR.TEN_NH_TT = dt.Rows(0)("TEN_NH_TT").ToString()
                    HDR.MA_NH_B = dt.Rows(0)("MA_NH_GT").ToString()
                    HDR.TEN_NH_B = dt.Rows(0)("TEN_NH_GT").ToString()

                    If dt.Rows(0)("TTien") Is Nothing OrElse IsDBNull(dt.Rows(0)("TTien")) Then
                        HDR.TTIEN = 0
                    Else
                        HDR.TTIEN = dt.Rows(0)("TTien")
                    End If
                    HDR.MA_DBHC = dt.Rows(0)("MA_DBHC").ToString()
                    HDR.TEN_DBHC = dt.Rows(0)("TEN_DBHC").ToString()

                    HDR.MA_CQQD = dt.Rows(0)("MA_CQQD").ToString()
                    HDR.TEN_CQQD = dt.Rows(0)("TEN_CQQD").ToString()
                    HDR.MA_LHTHU = dt.Rows(0)("ma_lhthu").ToString()
                    HDR.TEN_LHTHU = dt.Rows(0)("ten_lhthu").ToString()

                    HDR.SO_QD = dt.Rows(0)("so_qd").ToString()
                    HDR.NGAY_QD = dt.Rows(0)("ngay_qd").ToString()

                    HDR.TTIEN_VPHC = dt.Rows(0)("ttien_vphc").ToString()
                    HDR.LY_DO_VPHC = dt.Rows(0)("ly_do_vphc").ToString()

                    HDR.TTIEN_NOP_CHAM = dt.Rows(0)("ttien_nop_cham").ToString()
                    HDR.TTIEN_CTBL = dt.Rows(0)("ttien_ctbl").ToString()
                    HDR.LY_DO = dt.Rows(0)("ly_do").ToString()
                    HDR.LY_DO_HUY = dt.Rows(0)("ly_do_huy").ToString()
                    HDR.MA_KS = dt.Rows(0)("MA_KS").ToString()

                    If dt.Rows(0)("NGAY_KH_NH") IsNot Nothing AndAlso dt.Rows(0)("NGAY_KH_NH") IsNot DBNull.Value Then
                        HDR.NGAY_KH_NH = dt.Rows(0)("NGAY_KH_NH")
                    Else
                        HDR.NGAY_KH_NH = String.Empty
                    End If
                    HDR.REMARK = dt.Rows(0)("DIEN_GIAI").ToString()
                    If dt.Rows(0)("NGAY_HT") IsNot Nothing AndAlso dt.Rows(0)("NGAY_HT") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("NGAY_HT")
                        HDR.NGAY_HT = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.NGAY_HT = String.Empty
                    End If
                    HDR.MA_CN = dt.Rows(0)("ma_cn").ToString()

                    HDR.TRANG_THAI = dt.Rows(0)("Trang_Thai").ToString()
                    If dt.Rows(0)("TT_SP") Is Nothing OrElse IsDBNull(dt.Rows(0)("TT_SP")) Then
                        HDR.TT_SP = 0
                    Else
                        HDR.TT_SP = dt.Rows(0)("TT_SP")
                    End If
                    If dt.Rows(0)("MA_NV") Is Nothing OrElse IsDBNull(dt.Rows(0)("MA_NV")) Then
                        HDR.MA_NV = 0
                    Else
                        HDR.MA_NV = dt.Rows(0)("MA_NV")
                    End If

                    HDR.MA_NTK = dt.Rows(0)("MA_NTK")
                    HDR.HT_THU = dt.Rows(0)("HT_THU")
                    ''''''''''''''''''''''''''''''

                End If

                'get DTL list
                Dim listDTL As New List(Of ChungTuBL.infChungTuBL_DTL)
                Dim DTL As ChungTuBL.infChungTuBL_DTL = Nothing
                dt = Nothing
                listParam = New List(Of IDataParameter)
                'If strProduct.ToUpper().Equals("ETAX") Then
                '    sSQL = "select ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MA_NDKT,NOI_DUNG,KY_THUE,SOTIEN,SO_CT " & _
                '        " FROM TCS_BIENLAI_DTL " & _
                '        " where SO_CT = :SO_CT"
                'Else
                '    sSQL = "select 1 ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MA_NDKT,NOI_DUNG,KY_THUE,TTIEN SOTIEN,SO_CT " & _
                '        " FROM TCS_BIENLAI_HDR " & _
                '        " where SO_CT = :SO_CT"
                'End If

                sSQL = "select 1 ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MA_NDKT,NOI_DUNG,KY_THUE,TTIEN SOTIEN,SO_CT " & _
                        " FROM TCS_BIENLAI_HDR " & _
                        " where SO_CT = :SO_CT"

                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        DTL = New ChungTuBL.infChungTuBL_DTL
                        DTL.ID = dt.Rows(i)("ID").ToString()

                        If dt.Rows(i)("Ma_NV") Is Nothing OrElse IsDBNull(dt.Rows(i)("Ma_NV")) Then
                            DTL.Ma_NV = 0
                        Else
                            DTL.Ma_NV = dt.Rows(i)("Ma_NV")
                        End If
                        If dt.Rows(i)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(i)("So_BT")) Then
                            DTL.So_BT = 0
                        Else
                            DTL.So_BT = dt.Rows(i)("So_BT")
                        End If
                        DTL.SHKB = dt.Rows(i)("SHKB").ToString()
                        DTL.Ngay_KB = dt.Rows(i)("NGAY_KB").ToString()
                        DTL.Ma_Chuong = dt.Rows(i)("Ma_Chuong").ToString()
                        DTL.Ma_TMuc = dt.Rows(i)("MA_NDKT").ToString()
                        DTL.Noi_Dung = dt.Rows(i)("Noi_Dung").ToString()
                        DTL.Ky_Thue = dt.Rows(i)("Ky_Thue").ToString()
                        If dt.Rows(i)("sotien") Is Nothing OrElse IsDBNull(dt.Rows(i)("sotien")) Then
                            DTL.SoTien = 0
                        Else
                            DTL.SoTien = dt.Rows(i)("sotien")
                        End If

                        DTL.SO_CT = dt.Rows(i)("SO_CT").ToString()


                        listDTL.Add(DTL)
                    Next
                End If

                oRet = New ChungTuBL.infChungTuBL(HDR, listDTL)
            Catch ex As Exception
                oRet = Nothing
                Throw ex
            End Try

            Return oRet
        End Function
        Public Shared Function GetCT_IN_BL(ByVal sSoChungTu As String) As ChungTuBL.infChungTuBL
            Dim oRet As ChungTuBL.infChungTuBL = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Dim dDateTmp As Date
            Dim strProduct As String = "ETAX"
            Try
                'get HDR
                Dim HDR As New ChungTuBL.infChungTuBL_HDR

                sSQL = "select a.PRODUCT, a.ma_nnthue,a.ten_nnthue,a.dc_nnthue,a.PT_TT,a.ma_nt,a.TK_KH_NH,a.TEN_KH_NH, a.ma_nntien,a.ten_nntien, a.dc_nntien, " & _
                "a.SHKB,a.TEN_KB,a.NGAY_KB,a.NGAY_CT,a.MA_CQTHU,a.TEN_CQTHU,a.ma_lthue,a.TK_CO, " & _
                "a.So_BT,a.Ma_DThu,a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
                "a.MA_NH_TT, a.TEN_NH_TT, a.MA_NH_GT, a.TEN_NH_GT, a.TTien," & _
                "a.MA_DBHC,a.TEN_DBHC,a.MA_CQQD, a.TEN_CQQD, a.ma_lhthu, a.ten_lhthu, a.so_qd,to_char(a.ngay_qd,'dd/MM/yyyy') as ngay_qd, a.ttien_vphc, a.ly_do_vphc, a.ttien_nop_cham, a.ttien_ctbl," & _
                "a.Ly_Do,a.ly_do_huy,a.Ma_KS,to_char(a.ngay_kh_nh,'dd/MM/yyyy') as ngay_kh_nh, a.NGAY_QD," & _
                " a.DIEN_GIAI,a.ngay_ht,a.ma_cn, a.TRANG_THAI,a.tt_sp,a.MA_NV,a.SO_BIENLAI, a.ten_huyen_nnthue, a.ten_tinh_nnthue, a.ten_huyen_nntien, a.ten_tinh_nntien, HT_THU " & _
                " from TCS_BIENLAI_HDR a " & _
                "where a.SO_CT= :SO_CT"

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    'ma va ten DBHC
                    strProduct = dt.Rows(0)("PRODUCT").ToString()
                    HDR.PRODUCT = dt.Rows(0)("PRODUCT").ToString()
                    HDR.MA_NNTHUE = dt.Rows(0)("MA_NNTHUE").ToString()
                    HDR.TEN_NNTHUE = dt.Rows(0)("TEN_NNTHUE").ToString()
                    HDR.DC_NNTHUE = dt.Rows(0)("DC_NNTHUE").ToString()

                    HDR.PT_TT = dt.Rows(0)("PT_TT").ToString()
                    HDR.MA_NT = dt.Rows(0)("Ma_NT").ToString()

                    HDR.TK_KH_NH = dt.Rows(0)("TK_KH_NH").ToString()
                    HDR.TENTK_KH_NH = dt.Rows(0)("TEN_KH_NH").ToString()

                    HDR.SO_BIENLAI = dt.Rows(0)("SO_BIENLAI").ToString()
                    HDR.TEN_HUYEN_NNTHUE = dt.Rows(0)("ten_huyen_nnthue").ToString()
                    HDR.TEN_TINH_NNTHUE = dt.Rows(0)("ten_tinh_nnthue").ToString()
                    HDR.TEN_HUYEN_NNTIEN = dt.Rows(0)("ten_huyen_nntien").ToString()
                    HDR.TEN_TINH_NNTIEN = dt.Rows(0)("ten_tinh_nntien").ToString()

                    HDR.MA_NNTIEN = dt.Rows(0)("Ma_NNTien").ToString()
                    HDR.TEN_NNTIEN = dt.Rows(0)("TEN_NNTIEN").ToString()
                    HDR.DC_NNTIEN = dt.Rows(0)("DC_NNTIEN").ToString()

                    HDR.SHKB = dt.Rows(0)("SHKB").ToString()
                    HDR.TEN_KB = dt.Rows(0)("TEN_KB").ToString()
                    If dt.Rows(0)("NGAY_KB") Is Nothing OrElse IsDBNull(dt.Rows(0)("NGAY_KB")) Then
                        HDR.NGAY_KB = 0
                    Else
                        HDR.NGAY_KB = dt.Rows(0)("NGAY_KB")
                    End If
                    'Bo sung
                    If dt.Rows(0)("Ngay_CT") Is Nothing OrElse IsDBNull(dt.Rows(0)("Ngay_CT")) Then
                        HDR.NGAY_CT = 0
                    Else
                        HDR.NGAY_CT = dt.Rows(0)("Ngay_CT")
                    End If
                    HDR.MA_CQTHU = dt.Rows(0)("MA_CQTHU").ToString()
                    HDR.TEN_CQTHU = dt.Rows(0)("TEN_CQTHU").ToString()
                    HDR.MA_LTHUE = dt.Rows(0)("MA_LTHUE").ToString()
                    HDR.TK_CO = dt.Rows(0)("TK_Co").ToString()

                    If dt.Rows(0)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(0)("So_BT")) Then
                        HDR.SO_BT = 0
                    Else
                        HDR.SO_BT = dt.Rows(0)("So_BT")
                    End If
                    If dt.Rows(0)("MA_DTHU") Is Nothing OrElse IsDBNull(dt.Rows(0)("MA_DTHU")) Then
                        HDR.MA_DTHU = ""
                    Else
                        HDR.MA_DTHU = dt.Rows(0)("MA_DTHU").ToString()
                    End If

                    HDR.KYHIEU_CT = dt.Rows(0)("KyHieu_CT").ToString()
                    HDR.SO_CT = dt.Rows(0)("So_CT").ToString()
                    HDR.SO_CT_NH = dt.Rows(0)("SO_CT_NH").ToString()

                    HDR.MA_NH_TT = dt.Rows(0)("MA_NH_TT").ToString()
                    HDR.TEN_NH_TT = dt.Rows(0)("TEN_NH_TT").ToString()
                    HDR.MA_NH_B = dt.Rows(0)("MA_NH_GT").ToString()
                    HDR.TEN_NH_B = dt.Rows(0)("TEN_NH_GT").ToString()

                    If dt.Rows(0)("TTien") Is Nothing OrElse IsDBNull(dt.Rows(0)("TTien")) Then
                        HDR.TTIEN = 0
                    Else
                        HDR.TTIEN = dt.Rows(0)("TTien")
                    End If
                    HDR.MA_DBHC = dt.Rows(0)("MA_DBHC").ToString()
                    HDR.TEN_DBHC = dt.Rows(0)("TEN_DBHC").ToString()

                    HDR.MA_CQQD = dt.Rows(0)("MA_CQQD").ToString()
                    HDR.TEN_CQQD = dt.Rows(0)("TEN_CQQD").ToString()
                    HDR.MA_LHTHU = dt.Rows(0)("ma_lhthu").ToString()
                    HDR.TEN_LHTHU = dt.Rows(0)("ten_lhthu").ToString()

                    HDR.SO_QD = dt.Rows(0)("so_qd").ToString()
                    HDR.NGAY_QD = dt.Rows(0)("ngay_qd").ToString()

                    HDR.TTIEN_VPHC = dt.Rows(0)("ttien_vphc").ToString()
                    HDR.LY_DO_VPHC = dt.Rows(0)("ly_do_vphc").ToString()

                    HDR.TTIEN_NOP_CHAM = dt.Rows(0)("ttien_nop_cham").ToString()
                    HDR.TTIEN_CTBL = dt.Rows(0)("ttien_ctbl").ToString()
                    HDR.LY_DO = dt.Rows(0)("ly_do").ToString()
                    HDR.LY_DO_HUY = dt.Rows(0)("ly_do_huy").ToString()
                    HDR.MA_KS = dt.Rows(0)("MA_KS").ToString()

                    If dt.Rows(0)("NGAY_KH_NH") IsNot Nothing AndAlso dt.Rows(0)("NGAY_KH_NH") IsNot DBNull.Value Then
                        HDR.NGAY_KH_NH = dt.Rows(0)("NGAY_KH_NH")
                    Else
                        HDR.NGAY_KH_NH = String.Empty
                    End If
                    HDR.REMARK = dt.Rows(0)("DIEN_GIAI").ToString()
                    If dt.Rows(0)("NGAY_HT") IsNot Nothing AndAlso dt.Rows(0)("NGAY_HT") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("NGAY_HT")
                        HDR.NGAY_HT = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.NGAY_HT = String.Empty
                    End If
                    HDR.MA_CN = dt.Rows(0)("ma_cn").ToString()

                    HDR.TRANG_THAI = dt.Rows(0)("Trang_Thai").ToString()
                    If dt.Rows(0)("TT_SP") Is Nothing OrElse IsDBNull(dt.Rows(0)("TT_SP")) Then
                        HDR.TT_SP = 0
                    Else
                        HDR.TT_SP = dt.Rows(0)("TT_SP")
                    End If
                    If dt.Rows(0)("MA_NV") Is Nothing OrElse IsDBNull(dt.Rows(0)("MA_NV")) Then
                        HDR.MA_NV = 0
                    Else
                        HDR.MA_NV = dt.Rows(0)("MA_NV")
                    End If
                    HDR.HT_THU = dt.Rows(0)("HT_THU")
                    ''''''''''''''''''''''''''''''

                End If

                'get DTL list
                Dim listDTL As New List(Of ChungTuBL.infChungTuBL_DTL)
                Dim DTL As ChungTuBL.infChungTuBL_DTL = Nothing
                dt = Nothing
                listParam = New List(Of IDataParameter)
                'If strProduct.ToUpper().Equals("ETAX") Then
                '    sSQL = "select ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MA_NDKT,NOI_DUNG,KY_THUE,SOTIEN,SO_CT " & _
                '        " FROM TCS_BIENLAI_DTL " & _
                '        " where SO_CT = :SO_CT"
                'Else
                '    sSQL = "select 1 ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MA_NDKT,NOI_DUNG,KY_THUE,TTIEN SOTIEN,SO_CT " & _
                '        " FROM TCS_BIENLAI_HDR " & _
                '        " where SO_CT = :SO_CT"
                'End If


                sSQL = "select 1 ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MA_NDKT,NOI_DUNG,KY_THUE,TTIEN SOTIEN,SO_CT " & _
                        " FROM TCS_BIENLAI_HDR " & _
                        " where SO_CT = :SO_CT"


                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        DTL = New ChungTuBL.infChungTuBL_DTL
                        DTL.ID = dt.Rows(i)("ID").ToString()

                        If dt.Rows(i)("Ma_NV") Is Nothing OrElse IsDBNull(dt.Rows(i)("Ma_NV")) Then
                            DTL.Ma_NV = 0
                        Else
                            DTL.Ma_NV = dt.Rows(i)("Ma_NV")
                        End If
                        If dt.Rows(i)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(i)("So_BT")) Then
                            DTL.So_BT = 0
                        Else
                            DTL.So_BT = dt.Rows(i)("So_BT")
                        End If
                        DTL.SHKB = dt.Rows(i)("SHKB").ToString()
                        DTL.Ngay_KB = dt.Rows(i)("NGAY_KB").ToString()
                        DTL.Ma_Chuong = dt.Rows(i)("Ma_Chuong").ToString()
                        DTL.Ma_TMuc = dt.Rows(i)("MA_NDKT").ToString()
                        DTL.Noi_Dung = dt.Rows(i)("Noi_Dung").ToString()
                        DTL.Ky_Thue = dt.Rows(i)("Ky_Thue").ToString()
                        If dt.Rows(i)("sotien") Is Nothing OrElse IsDBNull(dt.Rows(i)("sotien")) Then
                            DTL.SoTien = 0
                        Else
                            DTL.SoTien = dt.Rows(i)("sotien")
                        End If

                        DTL.SO_CT = dt.Rows(i)("SO_CT").ToString()


                        listDTL.Add(DTL)
                    Next
                End If

                oRet = New ChungTuBL.infChungTuBL(HDR, listDTL)
            Catch ex As Exception
                oRet = Nothing
                Throw ex
            End Try

            Return oRet
        End Function

        Public Shared Function GetCT_IN_BANGKE(ByVal sSoChungTu As String) As ChungTuBL.infChungTuBL
            Dim oRet As ChungTuBL.infChungTuBL = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Dim dDateTmp As Date
            Try
                'get HDR
                Dim HDR As New ChungTuBL.infChungTuBL_HDR

                sSQL = "select a.ma_nnthue,a.ten_nnthue,a.dc_nnthue,a.PT_TT,a.ma_nt,a.TK_KH_NH,a.TEN_KH_NH, a.ma_nntien,a.ten_nntien, a.dc_nntien, " & _
                "a.SHKB,a.TEN_KB,a.NGAY_KB,a.NGAY_CT,a.MA_CQTHU,a.TEN_CQTHU,a.ma_lthue,a.TK_CO, " & _
                "a.So_BT,a.Ma_DThu,a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
                "a.MA_NH_TT, a.TEN_NH_TT, a.MA_NH_GT, a.TEN_NH_GT, a.TTien," & _
                "a.MA_DBHC,a.TEN_DBHC,a.MA_CQQD, a.TEN_CQQD, a.ma_lhthu, a.ten_lhthu, a.so_qd,to_char(a.ngay_qd,'dd/MM/yyyy') as ngay_qd, a.ttien_vphc, a.ly_do_vphc, a.ttien_nop_cham, a.ttien_ctbl," & _
                "a.Ly_Do,a.ly_do_huy,a.Ma_KS,to_char(a.ngay_kh_nh,'dd/MM/yyyy') as ngay_kh_nh, a.NGAY_QD," & _
                " a.DIEN_GIAI,a.ngay_ht,a.ma_cn, a.TRANG_THAI,a.tt_sp,a.MA_NV,a.SO_BIENLAI, a.ten_huyen_nnthue, a.ten_tinh_nnthue, a.ten_huyen_nntien, a.ten_tinh_nntien " & _
                " from TCS_BIENLAI_HDR a " & _
                "where a.SO_CT= :SO_CT"

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    'ma va ten DBHC
                    HDR.MA_NNTHUE = dt.Rows(0)("MA_NNTHUE").ToString()
                    HDR.TEN_NNTHUE = dt.Rows(0)("TEN_NNTHUE").ToString()
                    HDR.DC_NNTHUE = dt.Rows(0)("DC_NNTHUE").ToString()

                    HDR.PT_TT = dt.Rows(0)("PT_TT").ToString()
                    HDR.MA_NT = dt.Rows(0)("Ma_NT").ToString()

                    HDR.TK_KH_NH = dt.Rows(0)("TK_KH_NH").ToString()
                    HDR.TENTK_KH_NH = dt.Rows(0)("TEN_KH_NH").ToString()

                    HDR.SO_BIENLAI = dt.Rows(0)("SO_BIENLAI").ToString()
                    HDR.TEN_HUYEN_NNTHUE = dt.Rows(0)("ten_huyen_nnthue").ToString()
                    HDR.TEN_TINH_NNTHUE = dt.Rows(0)("ten_tinh_nnthue").ToString()
                    HDR.TEN_HUYEN_NNTIEN = dt.Rows(0)("ten_huyen_nntien").ToString()
                    HDR.TEN_TINH_NNTIEN = dt.Rows(0)("ten_tinh_nntien").ToString()

                    HDR.MA_NNTIEN = dt.Rows(0)("Ma_NNTien").ToString()
                    HDR.TEN_NNTIEN = dt.Rows(0)("TEN_NNTIEN").ToString()
                    HDR.DC_NNTIEN = dt.Rows(0)("DC_NNTIEN").ToString()

                    HDR.SHKB = dt.Rows(0)("SHKB").ToString()
                    HDR.TEN_KB = dt.Rows(0)("TEN_KB").ToString()
                    If dt.Rows(0)("NGAY_KB") Is Nothing OrElse IsDBNull(dt.Rows(0)("NGAY_KB")) Then
                        HDR.NGAY_KB = 0
                    Else
                        HDR.NGAY_KB = dt.Rows(0)("NGAY_KB")
                    End If
                    'Bo sung
                    If dt.Rows(0)("Ngay_CT") Is Nothing OrElse IsDBNull(dt.Rows(0)("Ngay_CT")) Then
                        HDR.NGAY_CT = 0
                    Else
                        HDR.NGAY_CT = dt.Rows(0)("Ngay_CT")
                    End If
                    HDR.MA_CQTHU = dt.Rows(0)("MA_CQTHU").ToString()
                    HDR.TEN_CQTHU = dt.Rows(0)("TEN_CQTHU").ToString()
                    HDR.MA_LTHUE = dt.Rows(0)("MA_LTHUE").ToString()
                    HDR.TK_CO = dt.Rows(0)("TK_Co").ToString()

                    If dt.Rows(0)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(0)("So_BT")) Then
                        HDR.SO_BT = 0
                    Else
                        HDR.SO_BT = dt.Rows(0)("So_BT")
                    End If
                    If dt.Rows(0)("MA_DTHU") Is Nothing OrElse IsDBNull(dt.Rows(0)("MA_DTHU")) Then
                        HDR.MA_DTHU = ""
                    Else
                        HDR.MA_DTHU = dt.Rows(0)("MA_DTHU").ToString()
                    End If

                    HDR.KYHIEU_CT = dt.Rows(0)("KyHieu_CT").ToString()
                    HDR.SO_CT = dt.Rows(0)("So_CT").ToString()
                    HDR.SO_CT_NH = dt.Rows(0)("SO_CT_NH").ToString()

                    HDR.MA_NH_TT = dt.Rows(0)("MA_NH_TT").ToString()
                    HDR.TEN_NH_TT = dt.Rows(0)("TEN_NH_TT").ToString()
                    HDR.MA_NH_B = dt.Rows(0)("MA_NH_GT").ToString()
                    HDR.TEN_NH_B = dt.Rows(0)("TEN_NH_GT").ToString()

                    If dt.Rows(0)("TTien") Is Nothing OrElse IsDBNull(dt.Rows(0)("TTien")) Then
                        HDR.TTIEN = 0
                    Else
                        HDR.TTIEN = dt.Rows(0)("TTien")
                    End If
                    HDR.MA_DBHC = dt.Rows(0)("MA_DBHC").ToString()
                    HDR.TEN_DBHC = dt.Rows(0)("TEN_DBHC").ToString()

                    HDR.MA_CQQD = dt.Rows(0)("MA_CQQD").ToString()
                    HDR.TEN_CQQD = dt.Rows(0)("TEN_CQQD").ToString()
                    HDR.MA_LHTHU = dt.Rows(0)("ma_lhthu").ToString()
                    HDR.TEN_LHTHU = dt.Rows(0)("ten_lhthu").ToString()

                    HDR.SO_QD = dt.Rows(0)("so_qd").ToString()
                    HDR.NGAY_QD = dt.Rows(0)("ngay_qd").ToString()

                    HDR.TTIEN_VPHC = dt.Rows(0)("ttien_vphc").ToString()
                    HDR.LY_DO_VPHC = dt.Rows(0)("ly_do_vphc").ToString()

                    HDR.TTIEN_NOP_CHAM = dt.Rows(0)("ttien_nop_cham").ToString()
                    HDR.TTIEN_CTBL = dt.Rows(0)("ttien_ctbl").ToString()
                    HDR.LY_DO = dt.Rows(0)("ly_do").ToString()
                    HDR.LY_DO_HUY = dt.Rows(0)("ly_do_huy").ToString()
                    HDR.MA_KS = dt.Rows(0)("MA_KS").ToString()

                    If dt.Rows(0)("NGAY_KH_NH") IsNot Nothing AndAlso dt.Rows(0)("NGAY_KH_NH") IsNot DBNull.Value Then
                        HDR.NGAY_KH_NH = dt.Rows(0)("NGAY_KH_NH")
                    Else
                        HDR.NGAY_KH_NH = String.Empty
                    End If
                    HDR.REMARK = dt.Rows(0)("DIEN_GIAI").ToString()
                    If dt.Rows(0)("NGAY_HT") IsNot Nothing AndAlso dt.Rows(0)("NGAY_HT") IsNot DBNull.Value Then
                        dDateTmp = dt.Rows(0)("NGAY_HT")
                        HDR.NGAY_HT = dDateTmp.ToString("dd/MM/yyyy")
                    Else
                        HDR.NGAY_HT = String.Empty
                    End If
                    HDR.MA_CN = dt.Rows(0)("ma_cn").ToString()

                    HDR.TRANG_THAI = dt.Rows(0)("Trang_Thai").ToString()
                    If dt.Rows(0)("TT_SP") Is Nothing OrElse IsDBNull(dt.Rows(0)("TT_SP")) Then
                        HDR.TT_SP = 0
                    Else
                        HDR.TT_SP = dt.Rows(0)("TT_SP")
                    End If
                    If dt.Rows(0)("MA_NV") Is Nothing OrElse IsDBNull(dt.Rows(0)("MA_NV")) Then
                        HDR.MA_NV = 0
                    Else
                        HDR.MA_NV = dt.Rows(0)("MA_NV")
                    End If

                    ''''''''''''''''''''''''''''''

                End If

                'get DTL list
                Dim listDTL As New List(Of ChungTuBL.infChungTuBL_DTL)
                Dim DTL As ChungTuBL.infChungTuBL_DTL = Nothing
                dt = Nothing
                listParam = New List(Of IDataParameter)
                sSQL = "select ID,SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,MA_CHUONG,MA_NDKT,NOI_DUNG,KY_THUE,SOTIEN,SO_CT " & _
                        " FROM TCS_BIENLAI_DTL " & _
                        " where SO_CT = :SO_CT"
                listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, CStr(sSoChungTu), DbType.String))
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        DTL = New ChungTuBL.infChungTuBL_DTL
                        DTL.ID = dt.Rows(i)("ID").ToString()

                        If dt.Rows(i)("Ma_NV") Is Nothing OrElse IsDBNull(dt.Rows(i)("Ma_NV")) Then
                            DTL.Ma_NV = 0
                        Else
                            DTL.Ma_NV = dt.Rows(i)("Ma_NV")
                        End If
                        If dt.Rows(i)("So_BT") Is Nothing OrElse IsDBNull(dt.Rows(i)("So_BT")) Then
                            DTL.So_BT = 0
                        Else
                            DTL.So_BT = dt.Rows(i)("So_BT")
                        End If
                        DTL.SHKB = dt.Rows(i)("SHKB").ToString()
                        DTL.Ngay_KB = dt.Rows(i)("NGAY_KB").ToString()
                        DTL.Ma_Chuong = dt.Rows(i)("Ma_Chuong").ToString()
                        DTL.Ma_TMuc = dt.Rows(i)("MA_NDKT").ToString()
                        DTL.Noi_Dung = dt.Rows(i)("Noi_Dung").ToString()
                        DTL.Ky_Thue = dt.Rows(i)("Ky_Thue").ToString()
                        If dt.Rows(i)("sotien") Is Nothing OrElse IsDBNull(dt.Rows(i)("sotien")) Then
                            DTL.SoTien = 0
                        Else
                            DTL.SoTien = dt.Rows(i)("sotien")
                        End If

                        DTL.SO_CT = dt.Rows(i)("SO_CT").ToString()


                        listDTL.Add(DTL)
                    Next
                End If

                oRet = New ChungTuBL.infChungTuBL(HDR, listDTL)
            Catch ex As Exception
                oRet = Nothing
                Throw ex
            End Try

            Return oRet
        End Function
        'Public Shared Function getBienLai_TracuuXuly(ByVal pvStrWhereClause As String) As DataSet
        '    Dim ds As DataSet = New DataSet()
        '    Try
        '        Dim strSQL As String = " SELECT  A.SO_CT,A.MA_NNTHUE,A.TEN_NNTHUE,A.NGAY_KH_NH,A.TTIEN,(CASE WHEN A.PT_TT='05' THEN 'Tiền mặt' ELSE 'Chuyển khoản' END) PT_TT,B.mota TRANG_THAI, "
        '        strSQL &= "  (CASE WHEN A.TT_SP='1' AND A.TRANG_THAI ='03' THEN 'Gui song phuong loi' "
        '        strSQL &= "  WHEN A.TT_SP='2' AND A.TRANG_THAI ='03' THEN 'Gui song phuong thanh cong'  ELSE '' END) MA_LOI,"
        '        strSQL &= "  (NVL((SELECT MAX(X.NAVIGATEURL)||'?ID='||A.SO_CT FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '13.%' AND X.NODEID IN ('13.9')),'#')) NAVIGATEURL "
        '        strSQL &= " FROM  TCS_BIENLAI_HDR A, tcs_dm_trangthai_sp_bl B "
        '        strSQL &= "  WHERE A.TRANG_THAI = B.TRANGTHAI " & pvStrWhereClause
        '        strSQL &= "  ORDER BY SO_CT DESC "
        '        ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception

        '    End Try
        '    Return ds
        'End Function

        Public Shared Function getBienLai_TracuuXuly(ByVal pvStrWhereClause As String) As DataSet
            Dim ds As DataSet = New DataSet()
            Try
                Dim strSQL As String = " SELECT  A.SO_CT,A.MA_NNTHUE,A.TEN_NNTHUE,A.NGAY_KH_NH,A.TTIEN,(CASE WHEN A.PT_TT='05' THEN 'Tiền mặt' ELSE 'Chuyển khoản' END) PT_TT,B.mota TRANG_THAI, "
                strSQL &= "  (CASE WHEN A.TT_SP='1' AND A.TRANG_THAI ='03' THEN 'Gui song phuong loi' "
                strSQL &= "  WHEN A.TT_SP='2' AND A.TRANG_THAI ='03' THEN 'Gui song phuong thanh cong'  ELSE '' END) MA_LOI,"
                strSQL &= "  (NVL((SELECT MAX(X.NAVIGATEURL)||'?ID='||A.SO_CT FROM TCS_SITE_MAP X WHERE X.NODEID LIKE '13.%' AND X.NODEID IN ('13.16')),'#')) NAVIGATEURL "
                strSQL &= " FROM  TCS_BIENLAI_HDR A, tcs_dm_trangthai_sp_bl B "
                strSQL &= "  WHERE A.TRANG_THAI = B.TRANGTHAI  AND B.phan_he='CTBL' " & pvStrWhereClause
                strSQL &= "  ORDER BY SO_CT DESC "
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception

            End Try
            Return ds
        End Function
        Public Shared Function GetSacThue(ByVal strMa_SacThue As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                sSQL = "select sac_thue from tcs_map_st where gia_tri='" & strMa_SacThue & "'"
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Public Shared Function GetMa_DBHC(ByVal strSHKB As String) As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                sSQL = "SELECT MA_DB FROM TCS_DM_KHOBAC WHERE SHKB='" & strSHKB & "'"
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function

        Public Shared Function CheckTrangThai(ByVal sSo_CT_NH As String) As Boolean
            Dim bRet As Boolean = False
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "Select trang_thai From tcs_chungtu_ttsp Where SO_REF_CORE = :SO_REF_CORE "
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("SO_REF_CORE", ParameterDirection.Input, CStr(sSo_CT_NH), DbType.String))
                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    sRet = dt.Rows(0)(0).ToString()
                End If

                If Not String.IsNullOrEmpty(sRet) Then
                    If sRet.Equals("01") Then
                        bRet = True
                    Else
                        bRet = False
                    End If
                Else
                    bRet = True
                End If

            Catch ex As Exception
                bRet = False
            End Try
            Return bRet
        End Function
        Public Shared Function GetTenTieuMuc(ByVal sMaNDKT As String) As String
            Dim v_sql As String = String.Empty
            Dim v_return As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing

            Dim dt As DataTable = Nothing
            Try

                v_sql = "SELECT B.TEN AS VALUE  FROM TCS_DM_MUC_TMUC B WHERE  MA_TMUC = :MA_NDKT AND TINH_TRANG = 1"

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("MA_NDKT", ParameterDirection.Input, CStr(sMaNDKT), DbType.String))
                dt = DataAccess.ExecuteReturnDataTable(v_sql, CommandType.Text, listParam.ToArray())

                If dt.Rows.Count > 0 Then
                    v_return = dt.Rows(0)(0).ToString()
                End If

            Catch ex As Exception

            End Try
            Return v_return
        End Function
        Public Shared Function fnc_GetSoBienLai() As String

            Dim strResult As String
            Try
                strResult = Business.CTuCommon.GetData_Seq("TCS_SO_BIEN_LAI_SEQ.NEXTVAL")
            Catch ex As Exception
                Throw ex
            End Try

            Return strResult.PadLeft(7, "0")

        End Function
        ''' <summary>
        ''' hiepvx them phan nay danh cho chung tu bien lai DVC maf eos can DTL  
        ''' do moi bien lai chi co 1 chi tiet
        ''' </summary>
        ''' <param name="hdr"></param>
        ''' <param name="pvStrSoCT">tra ra so chung tu</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Insert_ChungtuBL(ByVal hdr As BienLaiObj.HdrBl, ByRef pvStrSoCT As String) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then
                    Try
                        hdr.KYHIEU_CT = SongPhuongController.GetKyHieuChungTu(hdr.SHKB, hdr.MA_NV, "04")
                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh ký hiệu chứng từ")
                        Return bRet
                    End Try

                    Try
                        'nên chuyển vào trong câu query
                        If String.IsNullOrEmpty(hdr.NGAY_KB) Then
                            hdr.NGAY_KB = SongPhuongController.GetNgayKhoBac(hdr.SHKB)
                        End If

                        Dim sSo_BT As String = "0" 'String.Empty
                        Dim sNam_KB As String
                        Dim sThang_KB As String
                        sNam_KB = Mid(hdr.NGAY_KB, 3, 2)
                        sThang_KB = Mid(hdr.NGAY_KB, 5, 2)
                        'Sinh so CT ngay khi nhan ve tu corebank

                        If String.IsNullOrEmpty(hdr.SO_CT) Then
                            Dim sSoCT = CTuCommon.Get_SoCT_BL()
                            hdr.SO_CT = sNam_KB + sThang_KB + sSoCT
                        End If


                        hdr.KYHIEU_CT = hdr.KYHIEU_CT & hdr.SO_CT
                        sSo_BT = SongPhuongController.GetSoBT_BL(hdr.NGAY_KB, hdr.MA_NV)
                        If Not String.IsNullOrEmpty(sSo_BT) Then
                            hdr.SO_BT = CInt(sSo_BT + 1)
                        Else
                            hdr.SO_BT = 1
                        End If

                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh số chứng từ")
                        Return bRet
                    End Try



                    conn = DataAccess.GetConnection() 'New OracleConnection(VBOracleLib.DataAccess.strConnection)
                    ' conn.Open()
                    tran = conn.BeginTransaction()

                    sSQL = "INSERT INTO TCS_BIENLAI_HDR(SHKB, NGAY_KB, MA_NV, SO_BT, MA_DTHU, "
                    sSQL &= " SO_CT, NGAY_CT, KYHIEU_CT, SO_CT_NH,  TK_CO,"
                    sSQL &= " TEN_KB, MA_NNTHUE, TEN_NNTHUE, DC_NNTHUE, MA_NNTIEN,"
                    sSQL &= " TEN_NNTIEN, DC_NNTIEN, NGAY_NNTIEN, CQ_QD, MA_LTHUE,"
                    sSQL &= " TEN_LTHUE, MA_CQTHU, TEN_CQTHU, MA_DBHC, "
                    sSQL &= " MA_DVSDNS,   MA_NH_A, TEN_NH_A,"
                    sSQL &= "   MA_NH_GT, TEN_NH_GT, MA_NT,"
                    sSQL &= " TY_GIA, TTIEN, TTIEN_NT, TRANG_THAI, TK_KH_NH,"
                    sSQL &= "  NGAY_KH_NH,  DIEN_GIAI, MA_KHTK,"
                    sSQL &= " TG_KY, PT_TT, LAN_IN, TT_SP,  NGAY_HT,"
                    sSQL &= " MA_CN, "
                    sSQL &= "  MA_CQQD, TEN_CQQD, SO_QD, NGAY_QD,"
                    sSQL &= " MA_LHTHU, TEN_LHTHU, LY_DO_VPHC, TTIEN_CTBL,"
                    sSQL &= " TTIEN_VPHC, LY_DO_NOP_CHAM, TTIEN_NOP_CHAM, "
                    sSQL &= "  SO_BIENLAI, "
                    sSQL &= "  LAN_IN_BK, LAN_IN_BL,"
                    sSQL &= " MA_NTK, HT_THU, MA_DV,"
                    sSQL &= "  PRODUCT, SO_THAM_CHIEU, "
                    sSQL &= " MA_CHUONG, MA_NDKT, KY_THUE, NOI_DUNG,MA_NH_KB,SERI,PAYMENT "
                    If (hdr.TRANG_THAI.Equals("03")) Then
                        sSQL &= " ,MA_KS, NGAY_KS "
                    End If
                    sSQL &= " )VALUES("
                    sSQL &= " :SHKB, :NGAY_KB, :MA_NV, :SO_BT, :MA_DTHU, "
                    sSQL &= " :SO_CT,to_date( :NGAY_CT,'DD/MM/YYYY HH24:MI:SS'), :KYHIEU_CT, :SO_CT_NH,  :TK_CO,"
                    sSQL &= " :TEN_KB, :MA_NNTHUE, :TEN_NNTHUE, :DC_NNTHUE, :MA_NNTIEN,"
                    sSQL &= " :TEN_NNTIEN, :DC_NNTIEN,to_date( :NGAY_NNTIEN,'DD/MM/YYYY HH24:MI:SS'), :CQ_QD, :MA_LTHUE,"
                    sSQL &= " :TEN_LTHUE, :MA_CQTHU, :TEN_CQTHU, :MA_DBHC, "
                    sSQL &= " :MA_DVSDNS,  :MA_NH_A, :TEN_NH_A,"
                    sSQL &= "   :MA_NH_GT, :TEN_NH_GT, :MA_NT,"
                    sSQL &= " :TY_GIA, :TTIEN, :TTIEN_NT, :TRANG_THAI, :TK_KH_NH,"
                    sSQL &= "  to_date( :NGAY_KH_NH,'DD/MM/YYYY HH24:MI:SS'),  :DIEN_GIAI, :MA_KHTK,"
                    sSQL &= " to_date( :TG_KY,'DD/MM/YYYY HH24:MI:SS'), :PT_TT, 0, 0, sysdate,"
                    sSQL &= " :MA_CN, "
                    sSQL &= "  :MA_CQQD, :TEN_CQQD, :SO_QD,to_date(:NGAY_QD,'DD/MM/YYYY'),"
                    sSQL &= " :MA_LHTHU, :TEN_LHTHU, :LY_DO_VPHC, :TTIEN_CTBL,"
                    sSQL &= " :TTIEN_VPHC, :LY_DO_NOP_CHAM, :TTIEN_NOP_CHAM, "
                    sSQL &= "  :SO_BIENLAI, "
                    sSQL &= "  0, 0,"
                    sSQL &= " :MA_NTK, :HT_THU, :MA_DV,"
                    sSQL &= "  :PRODUCT, :SO_THAM_CHIEU, "
                    sSQL &= " :MA_CHUONG, :MA_NDKT, :KY_THUE, :NOI_DUNG,:MA_NH_KB,:SERI,:PAYMENT "
                    If (hdr.TRANG_THAI.Equals("03")) Then
                        sSQL &= " ,:MA_KS, to_date(:NGAY_KS,'DD/MM/YYYY HH24:MI:SS') "
                    End If
                    sSQL &= " )"

                    listParam = New List(Of IDataParameter)
                    'sSQL &= " :SHKB, :NGAY_KB, :MA_NV, :SO_BT, :MA_DTHU, "
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(hdr.NGAY_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(hdr.MA_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(hdr.SO_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    'sSQL &= " :SO_CT,to_date( :NGAY_CT,'DD/MM/YYYY HH24:MI:SS'), :KYHIEU_CT, :SO_CT_NH,  :TK_CO,"
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_CT", ParameterDirection.Input, CStr(hdr.NGAY_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KYHIEU_CT", ParameterDirection.Input, CStr(hdr.KYHIEU_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT_NH", ParameterDirection.Input, CStr(hdr.SO_CT_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_CO), DbType.String))
                    'sSQL &= " :TEN_KB, :MA_NNTHUE, :TEN_NNTHUE, :DC_NNTHUE, :MA_NNTIEN,"
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(hdr.TEN_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, CStr(hdr.DC_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.MA_NNTIEN), DbType.String))
                    'sSQL &= " :TEN_NNTIEN, :DC_NNTIEN, :NGAY_NNTIEN, :CQ_QD, :MA_LTHUE,"
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_NNTIEN", ParameterDirection.Input, CStr(hdr.NGAY_NTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":CQ_QD", ParameterDirection.Input, CStr(hdr.CQ_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, CStr(hdr.MA_LTHUE), DbType.String))
                    'sSQL &= " :TEN_LTHUE, :MA_CQTHU, :TEN_CQTHU, :MA_DBHC, "
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LTHUE", ParameterDirection.Input, CStr(hdr.TEN_LTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(hdr.TEN_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DBHC", ParameterDirection.Input, CStr(hdr.MA_DBHC), DbType.String))
                    'sSQL &= " :MA_DVSDNS, , :MA_NH_A, :TEN_NH_A,"
                    listParam.Add(DataAccess.NewDBParameter(":MA_DVSDNS", ParameterDirection.Input, CStr(hdr.MA_DV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, CStr(hdr.MA_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_A", ParameterDirection.Input, "", DbType.String))
                    'sSQL &= "   :MA_NH_GT, :TEN_NH_GT, :MA_NT,"
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_GT", ParameterDirection.Input, CStr(hdr.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_GT", ParameterDirection.Input, CStr(hdr.TEN_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    'sSQL &= " :TY_GIA, :TTIEN, :TTIEN_NT, :TRANG_THAI, :TK_KH_NH,"
                    listParam.Add(DataAccess.NewDBParameter(":TY_GIA", ParameterDirection.Input, CStr(hdr.TY_GIA), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NT", ParameterDirection.Input, CStr(hdr.TTIEN_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    'sSQL &= "  to_date( :NGAY_KH_NH,'DD/MM/YYYY HH24:MI:SS'),  :DIEN_GIAI, :MA_KHTK,"
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, CStr(hdr.NGAY_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DIEN_GIAI", ParameterDirection.Input, CStr(hdr.DIENGIAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_KHTK", ParameterDirection.Input, "0", DbType.String))
                    'sSQL &= " to_date( :TG_KY,'DD/MM/YYYY HH24:MI:SS'), :PT_TT, 0, 0, sysdate,"
                    listParam.Add(DataAccess.NewDBParameter(":TG_KY", ParameterDirection.Input, CStr(hdr.NGAY_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(hdr.PT_TT), DbType.String))
                    'sSQL &= " :MA_CN, "
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(hdr.MA_CN), DbType.String))
                    'sSQL &= "  :MA_CQQD, :TEN_CQQD, :SO_QD,to_date(:NGAY_QD,'DD/MM/YYYY'),"
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQQD", ParameterDirection.Input, CStr(hdr.MA_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQQD", ParameterDirection.Input, CStr(hdr.TEN_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_QD", ParameterDirection.Input, CStr(hdr.SO_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_QD", ParameterDirection.Input, CStr(hdr.NGAY_QD), DbType.String))
                    'sSQL &= " :MA_LHTHU, :TEN_LHTHU, :LY_DO_VPHC, :TTIEN_CTBL,"
                    listParam.Add(DataAccess.NewDBParameter(":MA_LHTHU", ParameterDirection.Input, CStr(hdr.MA_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHTHU", ParameterDirection.Input, CStr(hdr.TEN_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LY_DO_VPHC", ParameterDirection.Input, CStr(hdr.LY_DO_VPHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_CTBL", ParameterDirection.Input, CStr(hdr.TTIEN_CTBL), DbType.String))
                    'sSQL &= " :TTIEN_VPHC, :LY_DO_NOP_CHAM, :TTIEN_NOP_CHAM, "
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_VPHC", ParameterDirection.Input, CStr(hdr.TTIEN_VPHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LY_DO_NOP_CHAM", ParameterDirection.Input, "", DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NOP_CHAM", ParameterDirection.Input, CStr(hdr.TTIEN_NOP_CHAM), DbType.String))
                    'sSQL &= "  :SO_BIENLAI, "
                    listParam.Add(DataAccess.NewDBParameter(":SO_BIENLAI", ParameterDirection.Input, CStr(hdr.SO_BIENLAI), DbType.String))
                    'sSQL &= "  0, 0,"
                    'sSQL &= " :MA_NTK, :HT_THU, :MA_DV,"
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(hdr.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":HT_THU", ParameterDirection.Input, CStr(hdr.HT_THU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DV", ParameterDirection.Input, CStr(hdr.MA_DV), DbType.String))
                    'sSQL &= "  :PRODUCT, :SO_THAM_CHIEU, "
                    listParam.Add(DataAccess.NewDBParameter(":PRODUCT", ParameterDirection.Input, CStr(hdr.PRODUCT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_THAM_CHIEU", ParameterDirection.Input, CStr(hdr.SO_THAM_CHIEU), DbType.String))

                    'sSQL &= " :MA_CHUONG, :MA_NDKT, :KY_THUE)"
                    listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(hdr.MA_CHUONG), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NDKT", ParameterDirection.Input, CStr(hdr.MA_NDKT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KY_THUE", ParameterDirection.Input, CStr(hdr.KY_THUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(hdr.NOI_DUNG), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_KB", ParameterDirection.Input, CStr(hdr.MA_NH_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SERI", ParameterDirection.Input, CStr(hdr.SERI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PAYMENT", ParameterDirection.Input, CStr(hdr.PAYMENT), DbType.String))

                    If (hdr.TRANG_THAI.Equals("03")) Then
                        'sSQL &= " ,MA_KS, NGAY_KS "
                        listParam.Add(DataAccess.NewDBParameter(":MA_KS", ParameterDirection.Input, CStr(hdr.MA_KS), DbType.String))
                        listParam.Add(DataAccess.NewDBParameter(":NGAY_KS", ParameterDirection.Input, CStr(hdr.NGAY_KS), DbType.String))
                    End If
                    Dim v_sqlT As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....

                    If iRet > 0 Then 'if insert HDR is success then insert DTL list

                        bRet = True

                    Else
                        bRet = True
                    End If
                End If


                If bRet Then
                    tran.Commit()
                    pvStrSoCT = hdr.SO_CT
                Else
                    tran.Rollback()
                End If

            Catch ex As Exception
                bRet = False
                tran.Rollback()
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function
        Public Shared Function fnc_Update_TRANG_THAI_BIEN_LAI_CHUYEN_SP_LOI(ByVal pvStrSo_CT As String, Optional ByVal pvStrChecker As String = "") As Boolean
            Try
                Dim sSQL As String = "UPDATE TCS_BIENLAI_HDR SET TRANG_THAI='08'"
                If pvStrChecker.Length > 0 Then
                    sSQL &= ", MA_KS='" & pvStrChecker & "', NGAY_KS=sysdate "
                End If
                sSQL &= "  WHERE SO_CT='" & pvStrSo_CT & "'"
                Dim x As Integer = DataAccess.ExecuteNonQuery(sSQL, CommandType.Text)
                If x > 0 Then
                    Return True
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex.Message & "-" & ex.StackTrace, EventLogEntryType.Error)
                Return False
            End Try
            Return False
        End Function
        Public Shared Function fnc_Update_TRANG_THAI_BIEN_LAI_CHUYEN_THANH_CONG(ByVal pvStrSo_CT As String) As Boolean
            Try
                Dim sSQL As String = "UPDATE TCS_BIENLAI_HDR SET TRANG_THAI='09'  WHERE SO_CT='" & pvStrSo_CT & "'"
                Dim x As Integer = DataAccess.ExecuteNonQuery(sSQL, CommandType.Text)
                If x > 0 Then
                    Return True
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex.Message & "-" & ex.StackTrace, EventLogEntryType.Error)
                Return False
            End Try
            Return False
        End Function
        ''' <summary>
        ''' ham lay danh sach cac lenh bien lai theo API hoac CITAD
        ''' </summary>
        ''' <param name="sNgayKB"></param>
        ''' <param name="sMaNV"></param>
        ''' <param name="sMA_CN"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetListChungTuBL_Maker_New(ByVal sNgayKB As String, ByVal sMaNV As String) As DataSet
            Dim ds As DataSet = Nothing
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL &= "SELECT A.SO_CT_NH AS SO_REF_CORE, A.SO_CT, A.TRANG_THAI,A.TT_SP,A.SO_BT"
                sSQL &= "            FROM TCS_BIENLAI_HDR A WHERE  A.PRODUCT <>'ETAX' AND (A.TRANG_THAI IN ('01','07') OR (A.TRANG_THAI IN ('10','09','06') AND A.NGAY_KB=:NGAY_KB AND A.MA_NV=:MA_NV )"
                sSQL &= "                OR(A.TRANG_THAI IN ('08','03','02','04','05') AND A.MA_NV=:MA_NV ) )"
                sSQL &= "  order by SO_CT desc "
                'sSQL &= "select A.SO_CT_NH as so_ref_core, A.SO_CT, A.TRANG_THAI,A.TT_SP,A.so_bt   "
                'sSQL &= " from TCS_BIENLAI_HDR A, TCS_DM_NHANVIEN B where A.MA_NV = B.MA_NV AND B.TINH_TRANG = '0'  and  a.MA_CN = :MA_CN "
                'sSQL &= " and a.NGAY_KB = :NGAY_KB and a.MA_NV = :MA_NV "
                'sSQL &= "  order by so_bt desc "

                listParam = New List(Of IDataParameter)

                listParam.Add(DataAccess.NewDBParameter("NGAY_KB", ParameterDirection.Input, CStr(sNgayKB), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))

                ds = DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray())
            Catch ex As Exception
                ds = Nothing
            End Try

            Return ds
        End Function
        Public Shared Function Update_ChungTuBL_NEW(ByVal hdr As BienLaiObj.HdrBl, ByVal dtl() As BienLaiObj.DtlBl) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then
                    conn = VBOracleLib.DataAccess.GetConnection()
                    '  conn.Open()
                    tran = conn.BeginTransaction()
                    'delete DTL
                    sSQL = "DELETE FROM TCS_BIENLAI_DTL WHERE SO_CT = :SO_CT"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)

                    If hdr.NGAY_QD IsNot Nothing Then
                        hdr.NGAY_QD = hdr.NGAY_QD.Replace("-", "/")
                    Else
                    End If
                    'update HDR
                    sSQL = "UPDATE   TCS_BIENLAI_HDR " & _
                           "SET   SHKB = :SHKB,TEN_KB = :TEN_KB,  " & _
                                 "MA_NNTIEN = :MA_NNTIEN,TEN_NNTIEN = :TEN_NNTIEN,DC_NNTIEN = :DC_NNTIEN, " & _
                                   "MA_NNTHUE = :MA_NNTHUE,TEN_NNTHUE = :TEN_NNTHUE,DC_NNTHUE = :DC_NNTHUE, " & _
                                 "PT_TT = :PT_TT,MA_NT = :MA_NT,TK_KH_NH =:TK_KH_NH,TEN_KH_NH = :TEN_KH_NH,MA_DTHU = :MA_DTHU," & _
                                 "MA_CQTHU = :MA_CQTHU,TEN_CQTHU = :TEN_CQTHU, MA_LTHUE = :MA_LTHUE,TK_CO = :TK_CO,NGAY_HT = SYSDATE, " & _
                                 "MA_NH_TT = :MA_NH_TT,TEN_NH_TT = :TEN_NH_TT, MA_NH_GT = :MA_NH_GT,TEN_NH_GT = :TEN_NH_GT,TTIEN = :TTIEN, " & _
                                 "MA_CQQD = :MA_CQQD, TEN_CQQD =:TEN_CQQD,MA_LHTHU = :MA_LHTHU,TEN_LHTHU = :TEN_LHTHU," & _
                                 "SO_QD = :SO_QD,NGAY_QD = to_date(:NGAY_QD,'dd/MM/yyyy'),TTIEN_VPHC = :TTIEN_VPHC, " & _
                                 "LY_DO_VPHC = :LY_DO_VPHC,TTIEN_NOP_CHAM = :TTIEN_NOP_CHAM, " & _
                                 "TTIEN_CTBL = :TTIEN_CTBL,DIEN_GIAI = :DIEN_GIAI, TRANG_THAI = :TRANG_THAI, SO_BIENLAI = :SO_BIENLAI, " & _
                                   "ten_huyen_nnthue = :ten_huyen_nnthue,ten_tinh_nnthue = :ten_tinh_nnthue, ten_huyen_nntien = :ten_huyen_nntien, ten_tinh_nntien = :ten_tinh_nntien, MA_NTK =:MA_NTK, HT_THU = :HT_THU " & _
                          " WHERE   SO_CT = :SO_CT "

                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(hdr.TEN_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.MA_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTHUE", ParameterDirection.Input, CStr(hdr.DC_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(hdr.PT_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(hdr.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(hdr.MA_DTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQTHU", ParameterDirection.Input, CStr(hdr.TEN_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_LTHUE", ParameterDirection.Input, CStr(hdr.MA_LTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_CO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(hdr.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, CStr(hdr.TEN_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_GT", ParameterDirection.Input, CStr(hdr.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_GT", ParameterDirection.Input, CStr(hdr.TEN_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQQD", ParameterDirection.Input, CStr(hdr.MA_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQQD", ParameterDirection.Input, CStr(hdr.TEN_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_QD", ParameterDirection.Input, CStr(hdr.SO_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_QD", ParameterDirection.Input, hdr.NGAY_QD, DbType.Date))


                    listParam.Add(DataAccess.NewDBParameter(":MA_LHTHU", ParameterDirection.Input, CStr(hdr.MA_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHTHU", ParameterDirection.Input, CStr(hdr.TEN_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LY_DO_VPHC", ParameterDirection.Input, CStr(hdr.LY_DO_VPHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_CTBL", ParameterDirection.Input, CStr(hdr.TTIEN_CTBL), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_VPHC", ParameterDirection.Input, CStr(hdr.TTIEN_VPHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NOP_CHAM", ParameterDirection.Input, CStr(hdr.TTIEN_NOP_CHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DIEN_GIAI", ParameterDirection.Input, CStr(hdr.REMARK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_CT", ParameterDirection.Input, CStr(hdr.NGAY_CT), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_HT", ParameterDirection.Input, CStr(hdr.NGAY_HT), DbType.String))       'Fix sysdate
                    listParam.Add(DataAccess.NewDBParameter(":SO_BIENLAI", ParameterDirection.Input, CStr(hdr.SO_BIENLAI), DbType.String))


                    listParam.Add(DataAccess.NewDBParameter(":ten_huyen_nnthue", ParameterDirection.Input, CStr(hdr.TEN_HUYEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_tinh_nnthue", ParameterDirection.Input, CStr(hdr.TEN_TINH_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_huyen_nntien", ParameterDirection.Input, CStr(hdr.TEN_HUYEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_tinh_nntien", ParameterDirection.Input, CStr(hdr.TEN_TINH_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NTK", ParameterDirection.Input, CStr(hdr.MA_NTK), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":HT_THU", ParameterDirection.Input, CStr(hdr.HT_THU), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))

                    Dim vSql As String = DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)

                    If iRet > 0 Then 'if insert HDR is success then insert DTL list
                        If dtl IsNot Nothing AndAlso dtl.Count > 0 Then
                            Dim bFlag As Boolean = False
                            For Each objDtl In dtl
                                sSQL = "UPDATE   TCS_BIENLAI_HDR  SET MA_CHUONG=:MA_CHUONG,MA_NDKT=:MA_NDKT,NOI_DUNG=:NOI_DUNG,KY_THUE=:KY_THUE WHERE SO_CT=:SO_CT"

                                iRet = 0


                                listParam = New List(Of IDataParameter)

                                listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(objDtl.MA_CHUONG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":MA_NDKT", ParameterDirection.Input, CStr(objDtl.MA_TMUC), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(objDtl.NOI_DUNG), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":KY_THUE", ParameterDirection.Input, CStr(objDtl.KY_THUE), DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))


                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                                'if any DTL row insert is fail then rollback all transaction
                                If iRet > 0 Then
                                    bFlag = True
                                Else
                                    bFlag = False
                                End If
                            Next

                            If bFlag Then
                                bRet = True
                            End If
                        Else
                            bRet = True
                        End If
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()

                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function
        Public Shared Function checkPhanQuyen(ByVal sMaNV As String, ByVal sMaCN As String) As Boolean
            Dim bRet As Boolean = False
            Dim v_sql As String = String.Empty
            Dim total As Int32 = 0
            Dim listParam As List(Of IDataParameter) = Nothing

            Dim dt As DataTable = Nothing
            Try

                v_sql = "select count(*) as total from  (select pq.ma_cn, pq.ma_nhom, nv.ma_nv from tcs_dm_nhanvien nv, tcs_phanquyen pq "
                v_sql &= "where nv.ma_nhom = pq.ma_nhom and nv.ma_nv =:ma_nv ) A where A.ma_cn =:ma_cn"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("ma_nv", ParameterDirection.Input, CStr(sMaNV), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("ma_cn", ParameterDirection.Input, CStr(sMaCN), DbType.String))
                dt = DataAccess.ExecuteReturnDataTable(v_sql, CommandType.Text, listParam.ToArray())

                If dt.Rows.Count > 0 Then
                    total = Int32.Parse(dt.Rows(0)(0).ToString())
                End If
                If total > 0 Then
                    bRet = True
                Else
                    bRet = False
                End If

            Catch ex As Exception
                Return False
            End Try
            Return bRet
        End Function
    End Class
End Namespace
