﻿'Imports System.Data
'Imports Business.Common.mdlCommon
'Imports Business.Common.mdlSystemVariables
'Imports Business.XLCuoiNgay
'Imports VBOracleLib

'Namespace XLCuoiNgay

'    Public Module mdlCommonCN

'        Public Function GetTaiKhoan(ByVal strTKPre As String) As String
'            '------------------------------------------------------------------
'            ' Mục đích: Lấy tài khoản để nhập chứng từ hủy      
'            ' Ngày viết: 07/11/2007
'            ' Người viết: Lê Hồng Hà
'            ' -----------------------------------------------------------------
'            Dim strResult As String = ""
'            Dim conn As DataAccess
'            Dim drNT As IDataReader
'            Dim strSql As String
'            Try
'                conn = New DataAccess
'                strSql = "Select TK From TCS_DM_TAIKHOAN where TK like '" & strTKPre.Trim() & "%' order by TK"
'                drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
'                If drNT.Read Then
'                    strResult = drNT.GetString(0)
'                End If
'            Catch ex As Exception
'                'LogDebug.Writelog("Lỗi trong quá trình lấy Tài khoản: " & ex.ToString)
'                Return ""
'            Finally
'                If Not drNT Is Nothing Then
'                    drNT.Dispose()
'                End If
'                If Not conn Is Nothing Then
'                    conn.Dispose()
'                End If
'            End Try
'            Return strResult
'        End Function

'        Public Function GetMaCQThu() As String
'            '------------------------------------------------------------------
'            ' Mục đích: Lấy mã cơ quan thu để nhập chứng từ hủy      
'            ' Ngày viết: 07/11/2007
'            ' Người viết: Lê Hồng Hà
'            ' -----------------------------------------------------------------
'            Dim strResult As String = ""
'            Dim conn As DataAccess
'            Dim drNT As IDataReader
'            Dim strSql As String
'            Try
'                conn = New DataAccess
'                strSql = "Select ma_cqthu From TCS_DM_CQTHU"
'                drNT = conn.ExecuteDataReader(strSql, CommandType.Text)
'                If drNT.Read Then
'                    strResult = drNT.GetString(0)
'                End If
'            Catch ex As Exception
'                'LogDebug.Writelog("Lỗi trong quá trình lấy mã cơ quan thu: " & ex.ToString)
'                Return ""
'            Finally
'                If Not drNT Is Nothing Then
'                    drNT.Dispose()
'                End If
'                If Not conn Is Nothing Then
'                    conn.Dispose()
'                End If
'            End Try
'            Return strResult
'        End Function

'        Public Sub CTU_Insert(ByVal objCTuHDR As infCTU_HDR)
'            ' ****************************************************************************
'            ' Mục đích: Insert dữ liệu chứng từ vào bảng TCS_CTU_HDR và DTL
'            ' Đầu vào: đối tượng HDR và mảng DTL
'            ' Người viết: Lê Hồng Hà
'            ' Ngày viết: 07/12/2007
'            '*****************************************************************************
'            Dim conn As DataAccess
'            Dim strSql As String
'            Try
'                conn = New DataAccess
'                strSql = "Insert into TCS_CTU_HDR(SHKB,Ngay_KB,Ma_NV," & _
'                         "So_BT,Ma_DThu,So_BThu," & _
'                         "KyHieu_CT,So_CT,So_CT_NH," & _
'                         "Ma_NNTien,Ten_NNTien,DC_NNTien," & _
'                         "Ma_NNThue,Ten_NNThue,DC_NNThue," & _
'                         "Ly_Do,Ma_KS,Ma_TQ,So_QD," & _
'                         "Ngay_QD,CQ_QD,Ngay_CT,Ngay_HT," & _
'                         "Ma_CQThu,XA_ID,Ma_Tinh," & _
'                         "Ma_Huyen,Ma_Xa,TK_No,TK_Co," & _
'                         "So_TK,Ngay_TK,LH_XNK,DVSDNS,Ten_DVSDNS," & _
'                         "Ma_NT,Ty_Gia,TG_ID, Ma_LThue," & _
'                         "So_Khung,So_May,TK_KH_NH,NGAY_KH_NH," & _
'                         "MA_NH_A,MA_NH_B,TTien,TT_TThu," & _
'                         "Lan_In,Trang_Thai, " & _
'                         "TK_KH_Nhan, Ten_KH_Nhan, Diachi_KH_Nhan, TTien_NT) " & _
'                         "Values('" & objCTuHDR.SHKB & "'," & objCTuHDR.Ngay_KB & "," & objCTuHDR.Ma_NV & "," & _
'                         objCTuHDR.So_BT & ",'" & objCTuHDR.Ma_DThu & "'," & objCTuHDR.So_BThu & ",'" & _
'                         objCTuHDR.KyHieu_CT & "','" & objCTuHDR.So_CT & "','" & objCTuHDR.So_CT_NH & "','" & _
'                         objCTuHDR.Ma_NNTien & "','" & objCTuHDR.Ten_NNTien & "','" & objCTuHDR.DC_NNTien & "','" & _
'                         objCTuHDR.Ma_NNThue & "','" & objCTuHDR.Ten_NNThue & "','" & objCTuHDR.DC_NNThue & "','" & _
'                         objCTuHDR.Ly_Do & "'," & objCTuHDR.Ma_KS & "," & objCTuHDR.Ma_TQ & ",'" & objCTuHDR.So_QD & "'," & _
'                         objCTuHDR.Ngay_QD & ",'" & objCTuHDR.CQ_QD & "'," & objCTuHDR.Ngay_CT & "," & _
'                         objCTuHDR.Ngay_HT & ",'" & objCTuHDR.Ma_CQThu & "'," & _
'                         objCTuHDR.XA_ID & ",'" & objCTuHDR.Ma_Tinh & "','" & objCTuHDR.Ma_Huyen & "','" & _
'                         objCTuHDR.Ma_Xa & "','" & objCTuHDR.TK_No & "','" & objCTuHDR.TK_Co & "','" & _
'                         objCTuHDR.So_TK & "'," & objCTuHDR.Ngay_TK & ",'" & objCTuHDR.LH_XNK & "','" & _
'                         objCTuHDR.DVSDNS & "','" & objCTuHDR.Ten_DVSDNS & "','" & objCTuHDR.Ma_NT & "'," & _
'                         objCTuHDR.Ty_Gia & "," & objCTuHDR.TG_ID & ",'" & objCTuHDR.Ma_LThue & "','" & _
'                         objCTuHDR.So_Khung & "','" & objCTuHDR.So_May & "','" & objCTuHDR.TK_KH_NH & "'," & _
'                         objCTuHDR.NGAY_KH_NH & ",'" & objCTuHDR.MA_NH_A & "','" & objCTuHDR.MA_NH_B & "'," & _
'                         objCTuHDR.TTien & "," & objCTuHDR.TT_TThu & "," & objCTuHDR.Lan_In & ",'" & _
'                         objCTuHDR.Trang_Thai & "','" & objCTuHDR.TK_KH_Nhan & "','" & objCTuHDR.Ten_KH_Nhan & "','" & _
'                         objCTuHDR.Diachi_KH_Nhan & "'," & objCTuHDR.TTien_NT & ")"
'                ' Insert vào bảng TCS_CT_HDR
'                conn.ExecuteNonQuery(strSql, CommandType.Text)
'                gintSoBTCT += 1
'            Catch ex As Exception
'                'LogDebug.Writelog("Lỗi trong quá trình insert dữ liệu chứng từ: " & ex.ToString)
'                Throw ex
'            Finally
'                ' Giải phóng Connection tới CSDL       
'                If Not conn Is Nothing Then
'                    conn.Dispose()
'                End If
'            End Try
'        End Sub

'        Public Function bytTCS_GNT_Exist(ByVal strSoCT As String, ByVal strKHCT As String) As Byte
'            '----------------------------------------------------------------
'            ' Mục đích: Kiểm tra chứng từ đã tồn tại trong ngày làm việc hiện
'            '           thời hay chưa? 
'            ' Tham số: 
'            ' Giá trị trả về:   0: Nếu chưa tồn tại
'            '                   1: Đã tồn tại 
'            '                   2: Lỗi khi query dữ liệu
'            ' Ngày viết: 23/10/2007
'            ' Người viết: Nguyễn Hữu Hoan
'            ' ---------------------------------------------------------------
'            Dim cnCT As New DataAccess
'            Dim drCT As IDataReader
'            Dim strSQL As String
'            Dim bytResult As Byte = 0

'            Try
'                strSQL = "Select SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU " & _
'                         "From TCS_CTU_HDR " & _
'                         "Where KyHieu_CT='" & strKHCT & _
'                         "' And So_CT='" & strSoCT & "'"
'                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
'                If drCT.Read Then
'                    bytResult = 1
'                Else
'                    ' Số chứng từ này chưa được lập
'                    bytResult = 0
'                End If
'            Catch ex As Exception
'                'LogDebug.Writelog("Lỗi trong quá trình kiểm tra chứng từ CT đã tồn tại chưa ? - " & ex.ToString)
'                ' Lỗi do query dữ liệu
'                bytResult = 2
'            Finally
'                If Not drCT Is Nothing Then
'                    drCT.Dispose()
'                End If
'                If Not cnCT Is Nothing Then
'                    cnCT.Dispose()
'                End If
'            End Try
'            ' Trả lại kết quả cho hàm
'            Return bytResult
'        End Function

'    End Module

'End Namespace