﻿Imports Business.Common.mdlCommon
Imports Business.Common

Namespace BaoCao
    Public Class buBaoCao

#Region "DANH MUC DATASET"
        Public Shared Function TCS_DM_CAP_CHUONG() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_CAPCHUONG()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_LOAI_KHOAN() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_LOAIKHOAN()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_MUC_TMUC() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_MUCTMUC()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_DBHC() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_DBHC()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_CQQD() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_CQQD()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_CQThu() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_CQThu()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_LOAITIEN() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_LOAITIEN()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_NGUYENTE() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_NGUYENTE()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_TYGIA() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_TYGIA()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_TAIKHOAN() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_TAIKHOAN()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_DM_NNT() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_NNT()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_DM_LHTHU() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_LHTHU()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_DM_LOAITHUE() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_LOAITHUE()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_DM_TLDT() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_TLDT()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_NGANHANG() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_NGANHANG()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_KHOBAC() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_KHOBAC()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_DM_DIEMTHU() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_DIEMTHU()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_DM_MAQUY() As DataSet
            Try
                Dim db As New daBaoCao
                Return db.TCS_DM_MAQUY()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region

#Region "Báo cáo tiền giả"
        '/// tình hình tiền giả
        Public Shared Function TCS_DM_TINHHINH_TG(ByVal dtFrom As Date, ByVal dtTo As Date) As DataSet
            Try
                Dim db As New daBaoCao
                Return db.BC_TinhHinhTG(dtFrom, dtTo)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        '/// theo dõi tiền giả
        Public Shared Function TCS_DM_THEODOI_TG(ByVal dtFrom As Date, ByVal dtTo As Date) As DataSet
            Try
                Dim db As New daBaoCao
                Return db.BC_TheoDoiTG(dtFrom, dtTo)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        '/// xuất nhập tiền giả
        Public Shared Function TCS_DM_XUATNHAP_TG(ByVal dtFrom As Date, ByVal dtTo As Date) As DataSet
            Try
                Dim db As New daBaoCao
                Return db.BC_XuatNhapTG(dtFrom, dtTo)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "Bảng kê tiền mặt"
        '/// Sổ thu các loại tiền
        Public Shared Function TCS_SOTHU_CLTIEN(ByVal strWhere As String)
            Try
                Dim db As New daBaoCao
                Return db.SoThuCLTien(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function BKE_TIENMAT(ByVal strWhere As String) As DataSet
            Try
                Dim db As New daBaoCao
                Return db.BKE_TIENMAT(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetTongTienNop(ByVal strWhere As String) As Double
            Try
                Dim db As New daBaoCao
                Return db.GetTongTienNop(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function SOQUY_TM_CHITIET(ByVal strWhere As String) As DataSet
            Try
                Dim db As New daBaoCao
                Return db.SOQUY_TM_CHITIET(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function SOQUY_TM_TONGHOP(ByVal strWhere As String) As DataSet
            Try
                Dim db As New daBaoCao
                Return db.SOQUY_TM_TONGHOP(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "Kết xuất Cơ quan thu"
        Public Shared Function TCS_CTU_CQTHU(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_CTU_CQTHU(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_BKE_CTU_CHITIET(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BKE_CTU_CHITIET(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TCS_BKE_CTU_CQTHU(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BKE_CTU_CQTHU(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "Chứng từ phục hồi"
        Public Shared Function CTU_PhucHoi_Chitiet(ByVal strKHSoCT As String) As DataSet
            Try
                Dim daCT As New daBaoCao
                Return daCT.CTU_PhucHoi_Chitiet(strKHSoCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "Tra cứu chứng từ"

        Public Shared Function TCS_BK_CTU_TRACUU(ByVal strSoCTList As String) As DataSet
            Try
                Dim daCT As New daBaoCao
                Return daCT.TCS_BK_CTU_TRACUU(strSoCTList)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "Chứng từ từ biên lai thu"
        Public Shared Function CTU_BLT_TM(ByVal strKHCT As String, ByVal strSoCT As String) As DataSet
            Try
                Dim daCT As New daBaoCao
                Return daCT.CTU_BLT_TM(strKHCT, strSoCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "Biên lai thu"
        Public Shared Function BLT_TC(ByVal strWhere As String) As DataSet
            Try
                Dim daCT As New daBaoCao
                Return daCT.BLT_TC(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "Bảng kê chứng từ cuối ngày"
        Public Shared Function TCS_BKE_MAU10(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Dim ds As New DataSet
                ds = daTD.TCS_BKE_MAU10(strWhere)

                If (IsEmptyDataSet(ds)) Then Return ds

                Dim dt As New DataTable
                dt.Columns.Add("SoCT", GetType(String))
                dt.Columns.Add("NgayCT", GetType(String))
                dt.Columns.Add("TKNo", GetType(String))
                dt.Columns.Add("TKCo", GetType(String))

                dt.Columns.Add("CC", GetType(String))
                dt.Columns.Add("LK", GetType(String))
                dt.Columns.Add("MT1", GetType(String))
                dt.Columns.Add("MT2", GetType(String))
                dt.Columns.Add("MT3", GetType(String))
                dt.Columns.Add("MT4", GetType(String))

                dt.Columns.Add("DBHC", GetType(String))
                dt.Columns.Add("Tien", GetType(Long))
                dt.Columns.Add("MaNH", GetType(String))

                Dim lngTien, lngTienSave As Long
                Dim strKey, strKeySave, strMT As String
                Dim intMucTMucCount, intRow As Integer


                Dim obj As Object = New Object(12) {}
                Dim objSave As Object = New Object(12) {}

                Dim r As DataRow
                For Each r In ds.Tables(0).Rows
                    lngTien = Convert.ToInt32(r("Tien"))
                    strKey = GetString(r("Key"))
                    strMT = GetString(r("MT"))

                    If (strKey <> strKeySave) Then  'Thêm một dòng mới
                        obj(0) = GetString(r("SoCT"))
                        obj(1) = GetString(r("NgayCT"))
                        obj(2) = GetString(r("TKNo"))
                        obj(3) = GetString(r("TKCo"))
                        obj(4) = GetString(r("CC"))
                        obj(5) = GetString(r("LK"))
                        obj(6) = strMT
                        obj(7) = ""
                        obj(8) = ""
                        obj(9) = ""
                        obj(10) = GetString(r("DBHC"))
                        obj(11) = lngTien
                        obj(12) = GetString(r("MaNH"))

                        dt.Rows.Add(obj)

                        lngTienSave = lngTien
                        strKeySave = strKey
                        intMucTMucCount = 1
                    Else                            'Bổ sung thêm mục tiểu mục
                        lngTienSave += lngTien
                        intMucTMucCount += 1

                        intRow = dt.Rows.Count - 1
                        dt.Rows(intRow)("Tien") = lngTienSave
                        Select Case intMucTMucCount
                            Case 2
                                dt.Rows(intRow)("MT2") = strMT
                            Case 3
                                dt.Rows(intRow)("MT3") = strMT
                            Case 4
                                dt.Rows(intRow)("MT4") = strMT
                        End Select
                    End If
                Next

                Dim dsReturn As New DataSet
                dsReturn.Tables.Add(dt)
                Return dsReturn
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_BKE_MAU10_1068(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BKE_MAU10_1068(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
       
        Public Shared Function TCS_CTU_HUY(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_CTU_HUY(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    Public Shared Function TCS_BKE_MAU10_1068_TC(ByVal strKeyList As String, Optional ByVal blnTHOP As Boolean = False) As DataSet
        Try
            Dim daTD As New daBaoCao
            Return daTD.TCS_BKE_MAU10_1068_TC(strKeyList, blnTHOP)
        Catch ex As Exception
            Throw ex
        End Try
        End Function

        'HoaPT
        Public Shared Function TCS_BKE_CTTG(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BKE_CT_TrongGio(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'TYNK
        Public Shared Function TCS_BC_THUE_PGB(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BC_THUE_PGB(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_BKE_CTTG_NEW(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BKE_CT_TrongGio_New(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_BKE_DOICHIEU(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BKE_DOICHIEU(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'HoaPT
        Public Shared Function TCS_BKE_CTThuHo(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BKE_CT_ThuHo(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_BC_TONG_CN(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BC_TONG_CN(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_BC_DC_02_BKDC(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BC_DC_02_BKDC(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TCS_BC_TONG_NHCD(ByVal strWhere As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.TCS_BC_TONG_NHCD(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function checkHSC(ByVal MaNV As String) As DataSet
            Try
                Dim daTD As New daBaoCao
                Return daTD.checkHSC(MaNV)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function checkHSC_MaCN(ByVal strMaCN As String) As String
            Try
                Dim daTD As New daBaoCao
                Return daTD.checkHSC_MaCN(strMaCN)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetTenDN(ByVal MaNV As String) As String
            Try
                Dim daTD As New daBaoCao
                Return daTD.GetTenDN(MaNV)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

    End Class
End Namespace