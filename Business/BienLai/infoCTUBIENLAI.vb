﻿Namespace BienLai
    Public Class infoCTUBIENLAI
        Private _SO_CT As String = ""
        Private _NGAY_KB As String = ""
        Private _SHKB As String = ""
        Private _TEN_KB As String = ""
        Private _CQQD As String = ""
        Private _TEN_CQQD As String = ""
        Private _MA_LHTHU As String = ""
        Private _TEN_LHTHU As String = ""
        Private _TK_NS As String = ""
        Private _MA_CHUONG As String = ""
        Private _MA_NDKT As String = ""
        Private _NOI_DUNG As String = ""
        Private _TTIEN As String = "0"
        Private _TTIEN_VPHC As String = "0"
        Private _LY_DO As String = ""
        Private _TTIEN_NOP_CHAM As String = "0"
        Private _LY_DO_NOP_CHAM As String = ""
        Private _SO_QD As String = ""
        Private _NGAY_QD As String = ""
        Private _KYHIEU_CT As String = ""
        Private _NGAY_CT As String = ""
        Private _HINH_THUC As String = ""
        Private _MA_DBHC As String = ""
        Private _TRANG_THAI As String = ""
        Private _MA_NV As String = ""
        Private _MA_CN As String = ""
        Private _NGAY_HT As String = ""
        Private _MA_KS As String = ""
        Private _LY_DO_TUCHOI As String = ""
        Private _DIEN_GIAI As String = ""
        Private _REMAX As String = ""
        Private _MA_NNTHUE As String = ""
        Private _TEN_NNTHUE As String = ""
        Private _TEN_NNTIEN As String = ""
        Private _DC_NNTIEN As String = ""
        Private _GIAYTO_NNTIEN As String = ""
        Private _MA_CQTHU As String = ""
        Private _NGAY_KS As String = ""
        Private _MA_NV_TC As String = ""
        Private _NGAY_TC As String = ""
        Private _REF_NO As String = ""
        Private _RM_REF_NO As String = ""
        Private _SO_CT_NH As String = ""
        Private _TK_KH_NH As String = ""
        Private _MA_NH_A As String = ""
        Private _TEN_NH_A As String = ""
        Private _MA_NH_B As String = ""
        Private _TEN_NH_B As String = ""
        Private _MA_NH_TT As String = ""
        Private _TEN_NH_TT As String = ""
        Private _TTHAI_TTSP As String = ""
        Private _PT_TINHPHI As String = ""
        Private _PHI_GD As String = "0"
        Private _PHI_VAT As String = "0"
        Private _TEN_KH_NH As String = ""
        Private _ACTION As String = ""
        Private _MA_NT As String = ""
        Private _REF_CORE_TTSP As String = ""
        Private _KYHIEU_BL As String = ""
        Private _SOBL As String = ""
        Private _MOTA_TRANGTHAI As String = ""
        Private _LOAIBIENLAI As String = ""
        Private _TG_KY As String = ""
        Private _CODE_PHI As String = ""
        Private _MA_DTHU As String = ""
        Private _NGAY_KH_NH As String = ""
        Private _PRODUCT As String = ""
        Private _SO_BT As String = ""
        Private _KY_THUE As String = ""
        Public Sub New()

        End Sub
        Public Property SO_BT() As String
            Get
                Return _SO_BT
            End Get
            Set(ByVal Value As String)
                _SO_BT = Value
            End Set
        End Property
        Public Property MA_DTHU() As String
            Get
                Return _MA_DTHU
            End Get
            Set(ByVal Value As String)
                _MA_DTHU = Value
            End Set
        End Property
        Public Property NGAY_KH_NH() As String
            Get
                Return _NGAY_KH_NH
            End Get
            Set(ByVal Value As String)
                _NGAY_KH_NH = Value
            End Set
        End Property
        Public Property PRODUCT() As String
            Get
                Return _PRODUCT
            End Get
            Set(ByVal Value As String)
                _PRODUCT = Value
            End Set
        End Property
        Public Property SO_CT() As String
            Get
                Return _SO_CT
            End Get
            Set(ByVal Value As String)
                _SO_CT = Value
            End Set
        End Property
        Public Property NGAY_KB() As String
            Get
                Return _NGAY_KB
            End Get
            Set(ByVal Value As String)
                _NGAY_KB = Value
            End Set
        End Property
        Public Property SHKB() As String
            Get
                Return _SHKB
            End Get
            Set(ByVal Value As String)
                _SHKB = Value
            End Set
        End Property
        Public Property TEN_KB() As String
            Get
                Return _TEN_KB
            End Get
            Set(ByVal Value As String)
                _TEN_KB = Value
            End Set
        End Property
        Public Property CQQD() As String
            Get
                Return _CQQD
            End Get
            Set(ByVal Value As String)
                _CQQD = Value
            End Set
        End Property
        Public Property TEN_CQQD() As String
            Get
                Return _TEN_CQQD
            End Get
            Set(ByVal Value As String)
                _TEN_CQQD = Value
            End Set
        End Property
        Public Property MA_LHTHU() As String
            Get
                Return _MA_LHTHU
            End Get
            Set(ByVal Value As String)
                _MA_LHTHU = Value
            End Set
        End Property
        Public Property TEN_LHTHU() As String
            Get
                Return _TEN_LHTHU
            End Get
            Set(ByVal Value As String)
                _TEN_LHTHU = Value
            End Set
        End Property
        Public Property TK_NS() As String
            Get
                Return _TK_NS
            End Get
            Set(ByVal Value As String)
                _TK_NS = Value
            End Set
        End Property
        Public Property MA_CHUONG() As String
            Get
                Return _MA_CHUONG
            End Get
            Set(ByVal Value As String)
                _MA_CHUONG = Value
            End Set
        End Property
        Public Property MA_NDKT() As String
            Get
                Return _MA_NDKT
            End Get
            Set(ByVal Value As String)
                _MA_NDKT = Value
            End Set
        End Property
        Public Property NOI_DUNG() As String
            Get
                Return _NOI_DUNG
            End Get
            Set(ByVal Value As String)
                _NOI_DUNG = Value
            End Set
        End Property
        Public Property TTIEN() As String
            Get
                Return _TTIEN
            End Get
            Set(ByVal Value As String)
                _TTIEN = Value
            End Set
        End Property
        Public Property TTIEN_VPHC() As String
            Get
                Return _TTIEN_VPHC
            End Get
            Set(ByVal Value As String)
                _TTIEN_VPHC = Value
            End Set
        End Property
        Public Property LY_DO() As String
            Get
                Return _LY_DO
            End Get
            Set(ByVal Value As String)
                _LY_DO = Value
            End Set
        End Property
        Public Property TTIEN_NOP_CHAM() As String
            Get
                Return _TTIEN_NOP_CHAM
            End Get
            Set(ByVal Value As String)
                _TTIEN_NOP_CHAM = Value
            End Set
        End Property
        Public Property LY_DO_NOP_CHAM() As String
            Get
                Return _LY_DO_NOP_CHAM
            End Get
            Set(ByVal Value As String)
                _LY_DO_NOP_CHAM = Value
            End Set
        End Property
        Public Property SO_QD() As String
            Get
                Return _SO_QD
            End Get
            Set(ByVal Value As String)
                _SO_QD = Value
            End Set
        End Property
        Public Property NGAY_QD() As String
            Get
                Return _NGAY_QD
            End Get
            Set(ByVal Value As String)
                _NGAY_QD = Value
            End Set
        End Property
        Public Property KYHIEU_CT() As String
            Get
                Return _KYHIEU_CT
            End Get
            Set(ByVal Value As String)
                _KYHIEU_CT = Value
            End Set
        End Property
        Public Property NGAY_CT() As String
            Get
                Return _NGAY_CT
            End Get
            Set(ByVal Value As String)
                _NGAY_CT = Value
            End Set
        End Property
        Public Property HINH_THUC() As String
            Get
                Return _HINH_THUC
            End Get
            Set(ByVal Value As String)
                _HINH_THUC = Value
            End Set
        End Property
        Public Property MA_DBHC() As String
            Get
                Return _MA_DBHC
            End Get
            Set(ByVal Value As String)
                _MA_DBHC = Value
            End Set
        End Property
        Public Property TRANG_THAI() As String
            Get
                Return _TRANG_THAI
            End Get
            Set(ByVal Value As String)
                _TRANG_THAI = Value
            End Set
        End Property
        Public Property MA_NV() As String
            Get
                Return _MA_NV
            End Get
            Set(ByVal Value As String)
                _MA_NV = Value
            End Set
        End Property
        Public Property MA_CN() As String
            Get
                Return _MA_CN
            End Get
            Set(ByVal Value As String)
                _MA_CN = Value
            End Set
        End Property
        Public Property NGAY_HT() As String
            Get
                Return _NGAY_HT
            End Get
            Set(ByVal Value As String)
                _NGAY_HT = Value
            End Set
        End Property
        Public Property MA_KS() As String
            Get
                Return _MA_KS
            End Get
            Set(ByVal Value As String)
                _MA_KS = Value
            End Set
        End Property
        Public Property LY_DO_TUCHOI() As String
            Get
                Return _LY_DO_TUCHOI
            End Get
            Set(ByVal Value As String)
                _LY_DO_TUCHOI = Value
            End Set
        End Property
        Public Property DIEN_GIAI() As String
            Get
                Return _DIEN_GIAI
            End Get
            Set(ByVal Value As String)
                _DIEN_GIAI = Value
            End Set
        End Property
        Public Property REMAX() As String
            Get
                Return _REMAX
            End Get
            Set(ByVal Value As String)
                _REMAX = Value
            End Set
        End Property
        Public Property MA_NNTHUE() As String
            Get
                Return _MA_NNTHUE
            End Get
            Set(ByVal Value As String)
                _MA_NNTHUE = Value
            End Set
        End Property
        Public Property TEN_NNTHUE() As String
            Get
                Return _TEN_NNTHUE
            End Get
            Set(ByVal Value As String)
                _TEN_NNTHUE = Value
            End Set
        End Property
        Public Property TEN_NNTIEN() As String
            Get
                Return _TEN_NNTIEN
            End Get
            Set(ByVal Value As String)
                _TEN_NNTIEN = Value
            End Set
        End Property
        Public Property DC_NNTIEN() As String
            Get
                Return _DC_NNTIEN
            End Get
            Set(ByVal Value As String)
                _DC_NNTIEN = Value
            End Set
        End Property
        Public Property GIAYTO_NNTIEN() As String
            Get
                Return _GIAYTO_NNTIEN
            End Get
            Set(ByVal Value As String)
                _GIAYTO_NNTIEN = Value
            End Set
        End Property
        Public Property MA_CQTHU() As String
            Get
                Return _MA_CQTHU
            End Get
            Set(ByVal Value As String)
                _MA_CQTHU = Value
            End Set
        End Property
        Public Property NGAY_KS() As String
            Get
                Return _NGAY_KS
            End Get
            Set(ByVal Value As String)
                _NGAY_KS = Value
            End Set
        End Property
        Public Property MA_NV_TC() As String
            Get
                Return _MA_NV_TC
            End Get
            Set(ByVal Value As String)
                _MA_NV_TC = Value
            End Set
        End Property
        Public Property NGAY_TC() As String
            Get
                Return _NGAY_TC
            End Get
            Set(ByVal Value As String)
                _NGAY_TC = Value
            End Set
        End Property
        Public Property REF_NO() As String
            Get
                Return _REF_NO
            End Get
            Set(ByVal Value As String)
                _REF_NO = Value
            End Set
        End Property
        Public Property RM_REF_NO() As String
            Get
                Return _RM_REF_NO
            End Get
            Set(ByVal Value As String)
                _RM_REF_NO = Value
            End Set
        End Property
        Public Property SO_CT_NH() As String
            Get
                Return _SO_CT_NH
            End Get
            Set(ByVal Value As String)
                _SO_CT_NH = Value
            End Set
        End Property
        Public Property TK_KH_NH() As String
            Get
                Return _TK_KH_NH
            End Get
            Set(ByVal Value As String)
                _TK_KH_NH = Value
            End Set
        End Property
        Public Property MA_NH_A() As String
            Get
                Return _MA_NH_A
            End Get
            Set(ByVal Value As String)
                _MA_NH_A = Value
            End Set
        End Property
        Public Property TEN_NH_A() As String
            Get
                Return _TEN_NH_A
            End Get
            Set(ByVal Value As String)
                _TEN_NH_A = Value
            End Set
        End Property
        Public Property MA_NH_B() As String
            Get
                Return _MA_NH_B
            End Get
            Set(ByVal Value As String)
                _MA_NH_B = Value
            End Set
        End Property
        Public Property TEN_NH_B() As String
            Get
                Return _TEN_NH_B
            End Get
            Set(ByVal Value As String)
                _TEN_NH_B = Value
            End Set
        End Property
        Public Property MA_NH_TT() As String
            Get
                Return _MA_NH_TT
            End Get
            Set(ByVal Value As String)
                _MA_NH_TT = Value
            End Set
        End Property
        Public Property TEN_NH_TT() As String
            Get
                Return _TEN_NH_TT
            End Get
            Set(ByVal Value As String)
                _TEN_NH_TT = Value
            End Set
        End Property
        Public Property TTHAI_TTSP() As String
            Get
                Return _TTHAI_TTSP
            End Get
            Set(ByVal Value As String)
                _TTHAI_TTSP = Value
            End Set
        End Property
        Public Property PT_TINHPHI() As String
            Get
                Return _PT_TINHPHI
            End Get
            Set(ByVal Value As String)
                _PT_TINHPHI = Value
            End Set
        End Property
        Public Property PHI_GD() As String
            Get
                Return _PHI_GD
            End Get
            Set(ByVal Value As String)
                _PHI_GD = Value
            End Set
        End Property
        Public Property PHI_VAT() As String
            Get
                Return _PHI_VAT
            End Get
            Set(ByVal Value As String)
                _PHI_VAT = Value
            End Set
        End Property
        Public Property TEN_KH_NH() As String
            Get
                Return _TEN_KH_NH
            End Get
            Set(ByVal Value As String)
                _TEN_KH_NH = Value
            End Set
        End Property
        Public Property ACTION() As String
            Get
                Return _ACTION
            End Get
            Set(ByVal Value As String)
                _ACTION = Value
            End Set
        End Property
        Public Property MA_NT() As String
            Get
                Return _MA_NT
            End Get
            Set(ByVal Value As String)
                _MA_NT = Value
            End Set
        End Property
        Public Property REF_CORE_TTSP() As String
            Get
                Return _REF_CORE_TTSP
            End Get
            Set(ByVal Value As String)
                _REF_CORE_TTSP = Value
            End Set
        End Property
        Public Property KYHIEU_BL() As String
            Get
                Return _KYHIEU_BL
            End Get
            Set(ByVal Value As String)
                _KYHIEU_BL = Value
            End Set
        End Property
        Public Property SOBL() As String
            Get
                Return _SOBL
            End Get
            Set(ByVal Value As String)
                _SOBL = Value
            End Set
        End Property
        Public Property MOTA_TRANGTHAI() As String
            Get
                Return _MOTA_TRANGTHAI
            End Get
            Set(ByVal Value As String)
                _MOTA_TRANGTHAI = Value
            End Set
        End Property
        Public Property LOAIBIENLAI() As String
            Get
                Return _LOAIBIENLAI
            End Get
            Set(ByVal Value As String)
                _LOAIBIENLAI = Value
            End Set
        End Property
        Public Property TG_KY() As String
            Get
                Return _TG_KY
            End Get
            Set(ByVal Value As String)
                _TG_KY = Value
            End Set
        End Property

        Public Property CODE_PHI() As String
            Get
                Return _CODE_PHI
            End Get
            Set(ByVal Value As String)
                _CODE_PHI = Value
            End Set
        End Property
        Public Property KY_THUE() As String
            Get
                Return _KY_THUE
            End Get
            Set(ByVal Value As String)
                _KY_THUE = Value
            End Set
        End Property
    End Class
End Namespace

