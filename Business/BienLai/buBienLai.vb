﻿Imports VBOracleLib
Namespace BienLai

    Public Class buBienLai
        Public Function GhiChungTu2(ByVal hdr As infoCTUBIENLAI) As String
            Dim sTenDN As String = ""
            Dim sDesCTU As String = ""
            Dim sRet As String = ""
            Try
                'If GetBdsStatus() <> Common.mdlSystemVariables.gblnTTBDS Then
                '    sRet = ""
                '    Throw New Exception("Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại")
                '    Return sRet
                'End If
                If hdr Is Nothing Then
                    sRet = ""
                    Throw New Exception("Không có thông tin chứng từ để lưu. Vui lòng kiểm tra lại")
                    Return sRet
                End If

                Dim objUser = Nothing
                If hdr IsNot Nothing Then objUser = New CTuUser(hdr.MA_NV)
                If objUser IsNot Nothing Then sTenDN = objUser.Ten_DN

                Dim blnVND As Boolean = True
                If (hdr.MA_NT <> "VND") Then blnVND = False
                'lay trang thai chung tu hien tai


                'Kiem tra xem co so_ctu chua => bo xung 1 so truong cho so chung tu do


                Select Case hdr.ACTION

                    Case "GHIKS"

                        hdr.TRANG_THAI = "02"

                        If Insert_Chungtu2(hdr) Then
                            sRet = "Success;Hoàn thiện và gửi kiểm soát chứng từ thành công"
                        Else
                            sRet = String.Empty
                        End If

                    Case "UPDATE_KS"

                        hdr.TRANG_THAI = "02"

                        If UPDATE_Chungtu2(hdr) Then
                            sRet = "Success;Hoàn thiện và gửi kiểm soát chứng từ thành công"
                        Else
                            sRet = String.Empty
                        End If


                        ' End If
                End Select
                If sRet.Length <= 0 Then
                    sRet = ""
                    Throw New Exception("Trạng thái chứng từ không cho phép cập nhật")
                    Return sRet
                End If

                'Nối thêm thông tin load lên form
                sRet = sRet & ";" & hdr.SHKB & "/" & hdr.SO_CT
            Catch ex As Exception
                CTuCommon.WriteLog(ex, "Lỗi ghi chứng từ", sTenDN)
                'If ex.Message.Contains("TCS_CTU_HDR_PK") Then
                '    sRet = ""
                '    If sSaveType.StartsWith("INSERT") Then
                '        Throw New Exception("Không thể thêm mới! Đã tồn tại giấy nộp tiền này trong hệ thống")
                '    ElseIf sSaveType.StartsWith("UPDATE") Then
                '        Throw New Exception("Không thể cập nhật! Đã tồn tại giấy nộp tiền này trong hệ thống")
                '    End If
                'Else
                '    sRet = ""
                '    Throw ex
                'End If
            End Try

            Return sRet
        End Function
        Function getTrangThaiCTU() As String
            Return ""
        End Function
        Private Shared Function GetNgayKhoBac(ByVal strSHKB As String) As Long
            Dim lRet As Long = 0
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = " SELECT TO_NUMBER (TO_CHAR (TO_DATE (giatri_ts, 'DD/MM/YYYY'), 'YYYYMMDD')) FROM tcs_thamso " & _
                        " WHERE ten_ts = 'NGAY_LV' AND ma_dthu = (SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS = :GIATRI_TS)"
                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("GIATRI_TS", ParameterDirection.Input, CStr(strSHKB), DbType.String))

                Dim dt As DataTable = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    If Not Long.TryParse(dt.Rows(0)(0).ToString(), lRet) Then lRet = Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
                Else
                    lRet = Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
                End If
            Catch ex As Exception
                lRet = Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
            End Try

            Return lRet
        End Function
        Private Shared Function GetKyHieuChungTu(ByVal strSHKB As String, Optional ByVal strMaNV As String = "", Optional ByVal strLoaiGD As String = "01") As String
            Dim sRet As String = ""
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                sSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT'"
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    If strLoaiGD = "01" Then
                        sRet = dt.Rows(0)(0)
                    Else
                        sRet = dt.Rows(0)(0).ToString().Substring(0, 3) & "0000001"
                    End If
                End If
            Catch ex As Exception
                sRet = ""
            End Try

            Return sRet
        End Function
        Private Shared Function Insert_Chungtu2(ByVal hdr As infoCTUBIENLAI) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then
                    Try
                        hdr.KYHIEU_CT = GetKyHieuChungTu(hdr.SHKB, hdr.MA_NV, "01")
                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh ký hiệu chứng từ")
                        Return bRet
                    End Try

                    Try
                        'nên chuyển vào trong câu query
                        hdr.NGAY_KB = GetNgayKhoBac(hdr.SHKB)
                        Dim sSo_BT As String = "0" 'String.Empty
                        ' Dim sSo_BT As String = "0"
                        'sSo_BT = SongPhuongController.GetSoBT_BL(ctuHdr.NGAY_KB, ctuHdr.MA_NV)
                        'If Not String.IsNullOrEmpty(sSo_BT) Then
                        '    ctuHdr.SO_BT = CInt(sSo_BT + 1)
                        'Else
                        '    ctuHdr.SO_BT = 1
                        'End If
                        hdr.SO_BT = sSo_BT
                        Dim sNam_KB As String
                        Dim sThang_KB As String
                        sNam_KB = Mid(hdr.NGAY_KB, 3, 2)
                        sThang_KB = Mid(hdr.NGAY_KB, 5, 2)
                        hdr.SO_CT = sNam_KB + sThang_KB + CTuCommon.Get_SoCT() 'sNam_KB + sThang_KB + CTuCommon.Get_SoCT() MSB chi dung so chung tu 7 so. Khong dung dinh dang yyMMxxxxxxx


                        'hdr.REF_CORE_TTSP = "5" & CTuCommon.Get_SoRefCore_TTSP()


                    Catch ex As Exception
                        bRet = False
                        Throw New Exception("Có lỗi trong quá trình sinh số chứng từ")
                        Return bRet
                    End Try

                    conn = DataAccess.GetConnectionOpen() 'New OracleConnection(VBOracleLib.DataAccess.strConnection)
                    '  conn.Open()
                    tran = conn.BeginTransaction()

                    'sSQL = "INSERT INTO TCS_BIENLAI_HDR(SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,SO_CT,NGAY_CT,KYHIEU_CT,TK_CO, " & _
                    '                "TEN_KB,MA_NNTHUE,TEN_NNTHUE,MA_NNTIEN,TEN_NNTIEN,DC_NNTIEN," & _
                    '                "MA_CQTHU,MA_DBHC,ma_nh_a,ten_nh_a,MA_NH_TT,TEN_NH_TT,MA_NH_GT,TEN_NH_GT,MA_NT,TTIEN," & _
                    '                "TRANG_THAI,TK_KH_NH,TEN_KH_NH,DIEN_GIAI,PT_TT,NGAY_HT,MA_CN, " & _
                    '                "MA_CQQD,TEN_CQQD,SO_QD,NGAY_QD," & _
                    '                "MA_LHTHU,TEN_LHTHU,LY_DO_VPHC,TTIEN_CTBL,TTIEN_VPHC, TTIEN_NOP_CHAM, NGAY_KH_NH, SO_BIENLAI, HT_THU,product," & _
                    '                 " MA_CHUONG,MA_NDKT,NOI_DUNG,KY_THUE,LY_DO_NOP_CHAM,GIAYTO_NNTIEN,KYHIEU_BL,LOAIBIENLAI,TG_KY)" & _
                    '          "VALUES(:SHKB, :NGAY_KB, :MA_NV, :SO_BT, :MA_DTHU,:SO_CT,sysdate,:KYHIEU_CT,:TK_CO," & _
                    '              ":TEN_KB,:MA_NNTHUE, :TEN_NNTHUE, :MA_NNTIEN, :TEN_NNTIEN, :DC_NNTIEN, " & _
                    '              " :MA_CQTHU,:MA_DBHC,:ma_nh_a,:ten_nh_a,:MA_NH_TT,:TEN_NH_TT,:MA_NH_GT,:TEN_NH_GT,:MA_NT,:TTIEN, " & _
                    '              " :TRANG_THAI,:TK_KH_NH,:TEN_KH_NH,:DIEN_GIAI,:PT_TT,sysdate, :MA_CN,  " & _
                    '              ":MA_CQQD,:TEN_CQQD,:SO_QD,to_date(:NGAY_QD,'dd/MM/yyyy'), " & _
                    '              ":MA_LHTHU,:TEN_LHTHU, :LY_DO_VPHC, :TTIEN_CTBL, :TTIEN_VPHC, :TTIEN_NOP_CHAM,sysdate,:SO_BIENLAI, :HT_THU,:product," & _
                    '                ":MA_CHUONG,:MA_NDKT,:NOI_DUNG,:KY_THUE,:LY_DO_NOP_CHAM,:GIAYTO_NNTIEN,:KYHIEU_BL,:LOAIBIENLAI,SYSDATE)"

                    sSQL = "INSERT INTO TCS_BIENLAI_HDR(SHKB,NGAY_KB,MA_NV,SO_BT,MA_DTHU,SO_CT,NGAY_CT,KYHIEU_CT,TK_CO, " & _
                                    "TEN_KB,MA_NNTHUE,TEN_NNTHUE,MA_NNTIEN,TEN_NNTIEN,DC_NNTIEN," & _
                                    "MA_CQTHU,MA_DBHC,ma_nh_a,ten_nh_a,MA_NH_TT,TEN_NH_TT,MA_NH_GT,TEN_NH_GT,MA_NT,TTIEN," & _
                                    "TRANG_THAI,TK_KH_NH,TEN_KH_NH,DIEN_GIAI,PT_TT,NGAY_HT,MA_CN, " & _
                                    "MA_CQQD,TEN_CQQD,SO_QD,NGAY_QD," & _
                                    "MA_LHTHU,TEN_LHTHU,LY_DO_VPHC,TTIEN_CTBL,TTIEN_VPHC, TTIEN_NOP_CHAM, NGAY_KH_NH, SO_BIENLAI, HT_THU,product," & _
                                     " MA_CHUONG,MA_NDKT,NOI_DUNG,LY_DO_NOP_CHAM,GIAYTO_NNTIEN,KYHIEU_BL,LOAIBIENLAI,TG_KY)" & _
                              "VALUES(:SHKB, :NGAY_KB, :MA_NV, :SO_BT, :MA_DTHU,:SO_CT,sysdate,:KYHIEU_CT,:TK_CO," & _
                                  ":TEN_KB,:MA_NNTHUE, :TEN_NNTHUE, :MA_NNTIEN, :TEN_NNTIEN, :DC_NNTIEN, " & _
                                  " :MA_CQTHU,:MA_DBHC,:ma_nh_a,:ten_nh_a,:MA_NH_TT,:TEN_NH_TT,:MA_NH_GT,:TEN_NH_GT,:MA_NT,:TTIEN, " & _
                                  " :TRANG_THAI,:TK_KH_NH,:TEN_KH_NH,:DIEN_GIAI,:PT_TT,sysdate, :MA_CN,  " & _
                                  ":MA_CQQD,:TEN_CQQD,:SO_QD,to_date(:NGAY_QD,'dd/MM/yyyy'), " & _
                                  ":MA_LHTHU,:TEN_LHTHU, :LY_DO_VPHC, :TTIEN_CTBL, :TTIEN_VPHC, :TTIEN_NOP_CHAM,sysdate,:SO_BIENLAI, :HT_THU,:product," & _
                                    ":MA_CHUONG,:MA_NDKT,:NOI_DUNG,:LY_DO_NOP_CHAM,:GIAYTO_NNTIEN,:KYHIEU_BL,:LOAIBIENLAI,SYSDATE)"


                    listParam = New List(Of IDataParameter)

                    'map new bienlai
                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, CStr(hdr.NGAY_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(hdr.MA_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_BT", ParameterDirection.Input, CStr(hdr.SO_BT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DTHU", ParameterDirection.Input, CStr(hdr.MA_DTHU), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":TEN_DTHU", ParameterDirection.Input, CStr(hdr.TEN_DTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_CT", ParameterDirection.Input, CStr(hdr.NGAY_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KYHIEU_CT", ParameterDirection.Input, CStr(hdr.KYHIEU_CT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_NS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(hdr.TEN_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTIEN", ParameterDirection.Input, CStr(hdr.GIAYTO_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DBHC", ParameterDirection.Input, CStr(hdr.MA_DBHC), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":ma_nh_a", ParameterDirection.Input, CStr(hdr.MA_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":ten_nh_a", ParameterDirection.Input, CStr(hdr.TEN_NH_A), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(hdr.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, CStr(hdr.TEN_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_GT", ParameterDirection.Input, CStr(hdr.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_GT", ParameterDirection.Input, CStr(hdr.TEN_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(hdr.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DIEN_GIAI", ParameterDirection.Input, CStr(hdr.DIEN_GIAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(hdr.HINH_THUC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(hdr.MA_CN), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_CQQD", ParameterDirection.Input, CStr(hdr.CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQQD", ParameterDirection.Input, CStr(hdr.TEN_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_QD", ParameterDirection.Input, CStr(hdr.SO_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_QD", ParameterDirection.Input, hdr.NGAY_QD, DbType.Date))

                    listParam.Add(DataAccess.NewDBParameter(":MA_LHTHU", ParameterDirection.Input, CStr(hdr.MA_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHTHU", ParameterDirection.Input, CStr(hdr.TEN_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LY_DO_VPHC", ParameterDirection.Input, CStr(hdr.LY_DO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_CTBL", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_VPHC", ParameterDirection.Input, CStr(hdr.TTIEN_VPHC), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":LY_DO_NOP_CHAM", ParameterDirection.Input, CStr(hdr.LY_DO_NOP_CHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NOP_CHAM", ParameterDirection.Input, CStr(hdr.TTIEN_NOP_CHAM), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":NGAY_KH_NH", ParameterDirection.Input, hdr.NGAY_KH_NH, DbType.Date))
                    listParam.Add(DataAccess.NewDBParameter(":SO_BIENLAI", ParameterDirection.Input, CStr(hdr.SOBL), DbType.String))

                    'listParam.Add(DataAccess.NewDBParameter(":ten_huyen_nnthue", ParameterDirection.Input, CStr(hdr.TEN_HUYEN_NNTHUE), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":ten_tinh_nnthue", ParameterDirection.Input, CStr(hdr.TEN_TINH_NNTHUE), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":ten_huyen_nntien", ParameterDirection.Input, CStr(hdr.TEN_HUYEN_NNTIEN), DbType.String))
                    ''listParam.Add(DataAccess.NewDBParameter(":ten_tinh_nntien", ParameterDirection.Input, CStr(hdr.TEN_TINH_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":HT_THU", ParameterDirection.Input, CStr(hdr.HINH_THUC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":product", ParameterDirection.Input, CStr(hdr.PRODUCT), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(hdr.MA_CHUONG), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NDKT", ParameterDirection.Input, CStr(hdr.MA_NDKT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(hdr.NOI_DUNG), DbType.String))
                    'listParam.Add(DataAccess.NewDBParameter(":KY_THUE", ParameterDirection.Input, CStr(hdr.KY_THUE), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":LY_DO_NOP_CHAM", ParameterDirection.Input, CStr(hdr.LY_DO_NOP_CHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":GIAYTO_NNTIEN", ParameterDirection.Input, CStr(hdr.GIAYTO_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":KYHIEU_BL", ParameterDirection.Input, CStr(hdr.KYHIEU_BL), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAIBIENLAI", ParameterDirection.Input, CStr(hdr.LOAIBIENLAI), DbType.String))

                    Dim v_sqlT As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....

                    If iRet > 0 Then
                        bRet = True
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function

        Private Shared Function UPDATE_Chungtu2(ByVal hdr As infoCTUBIENLAI) As Boolean
            Dim bRet As Boolean = False

            Dim conn As IDbConnection = Nothing
            Dim tran As IDbTransaction = Nothing
            Dim sSQL As String = ""
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                'Insert by only one transaction
                If hdr IsNot Nothing Then




                    conn = DataAccess.GetConnectionOpen() 'New OracleConnection(VBOracleLib.DataAccess.strConnection)
                    '  conn.Open()
                    tran = conn.BeginTransaction()

                    sSQL = "UPDATE TCS_BIENLAI_HDR SET "
                    sSQL &= "    SHKB=:SHKB, TEN_KB=:TEN_KB"
                    sSQL &= "  , MA_CQQD=:MA_CQQD, TEN_CQQD=:TEN_CQQD "
                    sSQL &= "  , MA_LHTHU=:MA_LHTHU, TEN_LHTHU=:TEN_LHTHU"
                    sSQL &= "  , TK_CO=:TK_CO, MA_CHUONG=:MA_CHUONG, MA_NDKT=:MA_NDKT"
                    sSQL &= "  , NOI_DUNG=:NOI_DUNG, TTIEN=:TTIEN, TTIEN_VPHC=:TTIEN_VPHC, LY_DO_VPHC=:LY_DO_VPHC, TTIEN_NOP_CHAM=:TTIEN_NOP_CHAM  "
                    sSQL &= "  , LY_DO_NOP_CHAM=:LY_DO_NOP_CHAM, SO_QD=:SO_QD, NGAY_QD=TO_DATE(:NGAY_QD,'DD/MM/YYYY'), NGAY_CT=sysdate"
                    sSQL &= "  , PT_TT=:PT_TT, MA_DBHC=:MA_DBHC, TRANG_THAI=:TRANG_THAI, MA_NV=:MA_NV, MA_CN=:MA_CN"
                    sSQL &= "  , NGAY_HT=sysdate, DIEN_GIAI=:DIEN_GIAI"
                    sSQL &= "  , MA_NNTHUE=:MA_NNTHUE, TEN_NNTHUE=:TEN_NNTHUE, TEN_NNTIEN=:TEN_NNTIEN, DC_NNTIEN=:DC_NNTIEN "
                    sSQL &= "  , GIAYTO_NNTIEN=:GIAYTO_NNTIEN, MA_CQTHU=:MA_CQTHU "
                    sSQL &= "  , TK_KH_NH=:TK_KH_NH, MA_NH_A=:MA_NH_A,TEN_NH_A=:TEN_NH_A"
                    sSQL &= "  , MA_NH_GT=:MA_NH_GT, TEN_NH_GT=:TEN_NH_GT, MA_NH_TT=:MA_NH_TT, TEN_NH_TT=:TEN_NH_TT "
                    sSQL &= "  , HT_THU=:HT_THU, TEN_KH_NH=:TEN_KH_NH  "
                    sSQL &= "  , MA_NT=:MA_NT ,  LOAIBIENLAI=:LOAIBIENLAI  "


                    sSQL &= "  WHERE SO_CT=:SO_CT  AND TRANG_THAI='04' "


                    listParam = New List(Of IDataParameter)


                    listParam.Add(DataAccess.NewDBParameter(":SHKB", ParameterDirection.Input, CStr(hdr.SHKB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_KB", ParameterDirection.Input, CStr(hdr.TEN_KB), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQQD", ParameterDirection.Input, CStr(hdr.CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_CQQD", ParameterDirection.Input, CStr(hdr.TEN_CQQD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_LHTHU", ParameterDirection.Input, CStr(hdr.MA_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_LHTHU", ParameterDirection.Input, CStr(hdr.TEN_LHTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_CO", ParameterDirection.Input, CStr(hdr.TK_NS), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CHUONG", ParameterDirection.Input, CStr(hdr.MA_CHUONG), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NDKT", ParameterDirection.Input, CStr(hdr.MA_NDKT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NOI_DUNG", ParameterDirection.Input, CStr(hdr.NOI_DUNG), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN", ParameterDirection.Input, CStr(hdr.TTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_VPHC", ParameterDirection.Input, CStr(hdr.TTIEN_VPHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LY_DO_VPHC", ParameterDirection.Input, CStr(hdr.LY_DO), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TTIEN_NOP_CHAM", ParameterDirection.Input, CStr(hdr.TTIEN_NOP_CHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LY_DO_NOP_CHAM", ParameterDirection.Input, CStr(hdr.LY_DO_NOP_CHAM), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":SO_QD", ParameterDirection.Input, CStr(hdr.SO_QD), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":NGAY_QD", ParameterDirection.Input, CStr(hdr.NGAY_QD), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":PT_TT", ParameterDirection.Input, CStr(hdr.HINH_THUC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_DBHC", ParameterDirection.Input, CStr(hdr.MA_DBHC), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, CStr(hdr.TRANG_THAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NV", ParameterDirection.Input, CStr(hdr.MA_NV), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CN", ParameterDirection.Input, CStr(hdr.MA_CN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DIEN_GIAI", ParameterDirection.Input, CStr(hdr.DIEN_GIAI), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NNTHUE", ParameterDirection.Input, CStr(hdr.MA_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTHUE", ParameterDirection.Input, CStr(hdr.TEN_NNTHUE), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NNTIEN", ParameterDirection.Input, CStr(hdr.TEN_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":DC_NNTIEN", ParameterDirection.Input, CStr(hdr.DC_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":GIAYTO_NNTIEN", ParameterDirection.Input, CStr(hdr.GIAYTO_NNTIEN), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_CQTHU", ParameterDirection.Input, CStr(hdr.MA_CQTHU), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TK_KH_NH", ParameterDirection.Input, CStr(hdr.TK_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_A", ParameterDirection.Input, CStr(hdr.MA_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_A", ParameterDirection.Input, CStr(hdr.TEN_NH_A), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_GT", ParameterDirection.Input, CStr(hdr.MA_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_GT", ParameterDirection.Input, CStr(hdr.TEN_NH_B), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NH_TT", ParameterDirection.Input, CStr(hdr.MA_NH_TT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":TEN_NH_TT", ParameterDirection.Input, CStr(hdr.TEN_NH_TT), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":HT_THU", ParameterDirection.Input, CStr(hdr.HINH_THUC), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":TEN_KH_NH", ParameterDirection.Input, CStr(hdr.TEN_KH_NH), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":MA_NT", ParameterDirection.Input, CStr(hdr.MA_NT), DbType.String))
                    listParam.Add(DataAccess.NewDBParameter(":LOAIBIENLAI", ParameterDirection.Input, CStr(hdr.LOAIBIENLAI), DbType.String))

                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, CStr(hdr.SO_CT), DbType.String))
                    Dim v_sqlT As String = VBOracleLib.DataAccess.ConvertToSql(sSQL, listParam)

                    iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray(), tran)
                    'Mot so ngan hang cu van dung constraints HDR_PK gom cap cac column ....

                    If iRet > 0 Then
                        bRet = True
                    End If

                    If bRet Then
                        tran.Commit()
                    Else
                        tran.Rollback()
                    End If
                End If
            Catch ex As Exception
                bRet = False
                tran.Rollback()
                Throw ex
            Finally
                If tran IsNot Nothing Then tran.Dispose()
                If conn IsNot Nothing Then conn.Close()
            End Try

            Return bRet
        End Function

        Public Shared Function GetSoCtu(ByVal sMaNV As String, ByVal sSoCtu As String, ByVal sNgayKB As String) As DataTable
            Dim v_dt As DataTable
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT KYHIEU_CT, SO_CT,  SO_CT_NH, TRANG_THAI,  TTIEN, A.MA_NV,  SHKB, NGAY_KB,  B.TEN_DN,A.MA_KS " & _
                        " from TCS_BIENLAI_HDR A, TCS_DM_NHANVIEN B " & _
                        " where A.MA_NV = B.MA_NV " & _
                        " and A.Ma_NV = :Ma_NV AND NGAY_KB = :NGAYKB and ma_lhthu not in ('981','982','983')"


                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("NGAYKB", ParameterDirection.Input, sNgayKB, DbType.String))
                If sSoCtu.Length > 0 Then
                    sSQL &= "   A.SO_CT = :SO_CT "
                    listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, sSoCtu, DbType.String))
                End If
                sSQL &= " order by so_ct desc "
                v_dt = DataAccess.ExecuteReturnDataTable(sSQL, CommandType.Text, listParam.ToArray())
            Catch ex As Exception
                v_dt = Nothing
            End Try

            Return v_dt
        End Function

        Public Shared Function GetSoCtu1(ByVal sMaNV As String, ByVal sSoCtu As String, ByVal sNgayKB As String) As DataTable
            Dim v_dt As DataTable
            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "SELECT KYHIEU_CT, SO_CT,  SO_CT_NH, TRANG_THAI,  TTIEN, A.MA_NV,  SHKB, NGAY_KB,  B.TEN_DN,A.MA_KS " & _
                        " from TCS_BIENLAI_HDR A, TCS_DM_NHANVIEN B " & _
                        " where A.MA_NV = B.MA_NV " & _
                        " and A.Ma_NV = :Ma_NV AND NGAY_KB = :NGAYKB and ma_lhthu in ('981','982','983')"


                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter("Ma_NV", ParameterDirection.Input, CStr(sMaNV), DbType.String))
                listParam.Add(DataAccess.NewDBParameter("NGAYKB", ParameterDirection.Input, sNgayKB, DbType.String))
                If sSoCtu.Length > 0 Then
                    sSQL &= "   A.SO_CT = :SO_CT "
                    listParam.Add(DataAccess.NewDBParameter("SO_CT", ParameterDirection.Input, sSoCtu, DbType.String))
                End If
                sSQL &= " order by so_ct desc "
                v_dt = DataAccess.ExecuteReturnDataTable(sSQL, CommandType.Text, listParam.ToArray())
            Catch ex As Exception
                v_dt = Nothing
            End Try

            Return v_dt
        End Function

        Public Shared Function GetCTUDetail(ByVal sSoCtu As String) As infoCTUBIENLAI
            Try
                Dim strSQL As String = "SELECT A.SO_CT, A.NGAY_KB, A.SHKB, A.TEN_KB, A.ma_cqqd, A.TEN_CQQD,"
                strSQL &= "  A.MA_LHTHU, A.TEN_LHTHU, A.TK_CO, A.MA_CHUONG, A.MA_NDKT,"
                strSQL &= "    A.NOI_DUNG, A.TTIEN, A.TTIEN_VPHC, A.LY_DO, A.TTIEN_NOP_CHAM,"
                strSQL &= "  A.LY_DO_NOP_CHAM, A.SO_QD, to_char( A.NGAY_QD,'DD/MM/YYYY') NGAY_QD, A.KYHIEU_CT,to_char(A.NGAY_CT,'DD/MM/YYYY') NGAY_CT,"
                strSQL &= "  A.pt_tt HINH_THUC , A.MA_DBHC, A.TRANG_THAI, A.MA_NV, A.MA_CN,"
                strSQL &= "  to_char( A.NGAY_HT,'DD/MM/YYYY HH24:MI:SS') NGAY_HT, A.MA_KS, A.LY_DO_TUCHOI, A.DIEN_GIAI, "
                strSQL &= "  A.MA_NNTHUE, A.TEN_NNTHUE, A.TEN_NNTIEN, A.DC_NNTIEN,"
                strSQL &= "  A.GIAYTO_NNTIEN, A.MA_CQTHU,to_char( A.NGAY_KS,'DD/MM/YYYY HH24:MI:SS') NGAY_KS, "
                strSQL &= " A.SO_CT_NH, A.TK_KH_NH, A.MA_NH_A,"
                strSQL &= "  A.TEN_NH_A, A.MA_NH_GT, A.TEN_NH_GT, A.MA_NH_TT, A.TEN_NH_TT,"
                strSQL &= "   A.TK_KH_NH,A.TEN_KH_NH,A.ly_do_vphc,"
                strSQL &= "  A.MA_NT, A.KYHIEU_BL, A.SO_BIENLAI,B.mota MOTA_TRANGTHAI, LOAIBIENLAI "
                strSQL &= "  FROM TCS_BIENLAI_HDR A, TCS_DM_TRANGTHAI_SP_BL b  WHERE A.trang_thai =  B.trangthai AND B.phan_he='CTBL' AND A.so_ct='" & sSoCtu & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Dim vobj As infoCTUBIENLAI = New infoCTUBIENLAI()
                            vobj.SO_CT = ds.Tables(0).Rows(0)("SO_CT").ToString()
                            vobj.NGAY_KB = ds.Tables(0).Rows(0)("NGAY_KB").ToString()
                            vobj.SHKB = ds.Tables(0).Rows(0)("SHKB").ToString()
                            vobj.TEN_KB = ds.Tables(0).Rows(0)("TEN_KB").ToString()
                            vobj.CQQD = ds.Tables(0).Rows(0)("MA_CQQD").ToString()
                            vobj.TEN_CQQD = ds.Tables(0).Rows(0)("TEN_CQQD").ToString()
                            vobj.MA_LHTHU = ds.Tables(0).Rows(0)("MA_LHTHU").ToString()
                            vobj.TEN_LHTHU = ds.Tables(0).Rows(0)("TEN_LHTHU").ToString()
                            vobj.TK_NS = ds.Tables(0).Rows(0)("TK_CO").ToString()
                            vobj.MA_CHUONG = ds.Tables(0).Rows(0)("MA_CHUONG").ToString()
                            vobj.MA_NDKT = ds.Tables(0).Rows(0)("MA_NDKT").ToString()
                            vobj.NOI_DUNG = ds.Tables(0).Rows(0)("NOI_DUNG").ToString()
                            vobj.TTIEN = ds.Tables(0).Rows(0)("TTIEN").ToString()
                            vobj.TTIEN_VPHC = ds.Tables(0).Rows(0)("TTIEN_VPHC").ToString()
                            'vobj.LY_DO = ds.Tables(0).Rows(0)("LY_DO").ToString()
                            vobj.TTIEN_NOP_CHAM = ds.Tables(0).Rows(0)("TTIEN_NOP_CHAM").ToString()
                            vobj.LY_DO_NOP_CHAM = ds.Tables(0).Rows(0)("LY_DO_NOP_CHAM").ToString()
                            vobj.SO_QD = ds.Tables(0).Rows(0)("SO_QD").ToString()
                            vobj.NGAY_QD = ds.Tables(0).Rows(0)("NGAY_QD").ToString()
                            vobj.KYHIEU_CT = ds.Tables(0).Rows(0)("KYHIEU_CT").ToString()
                            vobj.NGAY_CT = ds.Tables(0).Rows(0)("NGAY_CT").ToString()
                            vobj.HINH_THUC = ds.Tables(0).Rows(0)("HINH_THUC").ToString()
                            vobj.MA_DBHC = ds.Tables(0).Rows(0)("MA_DBHC").ToString()
                            vobj.TRANG_THAI = ds.Tables(0).Rows(0)("TRANG_THAI").ToString()
                            vobj.MA_NV = ds.Tables(0).Rows(0)("MA_NV").ToString()
                            vobj.MA_CN = ds.Tables(0).Rows(0)("MA_CN").ToString()
                            vobj.NGAY_HT = ds.Tables(0).Rows(0)("NGAY_HT").ToString()
                            vobj.MA_KS = ds.Tables(0).Rows(0)("MA_KS").ToString()
                            vobj.LY_DO_TUCHOI = ds.Tables(0).Rows(0)("LY_DO_TUCHOI").ToString()
                            vobj.DIEN_GIAI = ds.Tables(0).Rows(0)("DIEN_GIAI").ToString()
                            vobj.MA_NNTHUE = ds.Tables(0).Rows(0)("MA_NNTHUE").ToString()
                            vobj.TEN_NNTHUE = ds.Tables(0).Rows(0)("TEN_NNTHUE").ToString()
                            vobj.TEN_NNTIEN = ds.Tables(0).Rows(0)("TEN_NNTIEN").ToString()
                            vobj.DC_NNTIEN = ds.Tables(0).Rows(0)("DC_NNTIEN").ToString()
                            vobj.GIAYTO_NNTIEN = ds.Tables(0).Rows(0)("GIAYTO_NNTIEN").ToString()
                            vobj.MA_CQTHU = ds.Tables(0).Rows(0)("MA_CQTHU").ToString()
                            vobj.NGAY_KS = ds.Tables(0).Rows(0)("NGAY_KS").ToString()
                            vobj.SO_CT_NH = ds.Tables(0).Rows(0)("SO_CT_NH").ToString()
                            vobj.TK_KH_NH = ds.Tables(0).Rows(0)("TK_KH_NH").ToString()
                            vobj.MA_NH_A = ds.Tables(0).Rows(0)("MA_NH_A").ToString()
                            vobj.TEN_NH_A = ds.Tables(0).Rows(0)("TEN_NH_A").ToString()
                            vobj.MA_NH_B = ds.Tables(0).Rows(0)("MA_NH_GT").ToString()
                            vobj.TEN_NH_B = ds.Tables(0).Rows(0)("TEN_NH_GT").ToString()
                            vobj.MA_NH_TT = ds.Tables(0).Rows(0)("MA_NH_TT").ToString()
                            vobj.TEN_NH_TT = ds.Tables(0).Rows(0)("TEN_NH_TT").ToString()
                            vobj.TK_KH_NH = ds.Tables(0).Rows(0)("TK_KH_NH").ToString()
                            vobj.TEN_KH_NH = ds.Tables(0).Rows(0)("TEN_KH_NH").ToString()
                            'vobj.REF_CORE_TTSP = ds.Tables(0).Rows(0)("REF_CORE_TTSP").ToString()
                            vobj.MA_NT = ds.Tables(0).Rows(0)("MA_NT").ToString()
                            vobj.KYHIEU_BL = ds.Tables(0).Rows(0)("KYHIEU_BL").ToString()
                            vobj.SOBL = ds.Tables(0).Rows(0)("SO_BIENLAI").ToString()
                            vobj.MOTA_TRANGTHAI = ds.Tables(0).Rows(0)("MOTA_TRANGTHAI").ToString()
                            vobj.LOAIBIENLAI = ds.Tables(0).Rows(0)("LOAIBIENLAI").ToString()
                            vobj.LY_DO = ds.Tables(0).Rows(0)("ly_do_vphc").ToString()
                            'vobj.CODE_PHI = ds.Tables(0).Rows(0)("CODE_PHI").ToString()
                            Return vobj
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
            Return Nothing
        End Function



        Public Shared Function HuyChungTu(ByVal sSo_CT As String, ByVal sLyDo As String, ByVal sMaNV As String) As String
            Dim sRet As String = String.Empty
            Dim sTenDN As String = ""
            Dim sSQL As String = String.Empty
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                Dim sTrangThaiCTu As String = String.Empty
                Dim sTT_CThue As String = String.Empty
                Dim sSHKB As String = String.Empty
                Dim sSo_BT As String = String.Empty

                If String.IsNullOrEmpty(sSo_CT) Then
                    sRet = ""
                    Throw New Exception("Vui lòng chọn chứng từ để hủy")
                    Return sRet
                Else
                    sSQL = "select TRANG_THAI, SHKB  from TCS_BIENLAI_HDR  WHERE SO_CT=:SO_CT"
                    listParam = New List(Of IDataParameter)
                    listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                    dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()))
                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                        If dt.Rows(0)("TRANG_THAI") IsNot Nothing AndAlso dt.Rows(0)("TRANG_THAI") IsNot DBNull.Value Then
                            sTrangThaiCTu = dt.Rows(0)("TRANG_THAI").ToString()
                            sSHKB = dt.Rows(0)("SHKB").ToString()
                        End If


                        If Not String.IsNullOrEmpty(sTrangThaiCTu) Then
                            If sTrangThaiCTu = "02" Or sTrangThaiCTu = "04" Then
                                ' GDV => 02 => KSV => 04
                                sSQL = "update TCS_BIENLAI_HDR set TRANG_THAI = '05',LY_DO_TUCHOI = :LY_DO, NGAY_HT=SYSDATE where SO_CT = :SO_CT and TRANG_THAI in ('02','04')"
                                listParam = New List(Of IDataParameter)
                                listParam.Add(DataAccess.NewDBParameter(":LY_DO", ParameterDirection.Input, sLyDo, DbType.String))
                                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
                            Else
                                Throw New Exception("Trạng thái chứng từ không cho phép hủy")
                            End If

                            If iRet > 0 Then
                                sRet = "Hủy chứng từ thành công"
                                'Nối thêm thông tin load lên form
                                sRet = sRet & ";" & sSo_CT & "/" & sSHKB
                            End If
                        End If
                    Else
                        sRet = ""
                        Throw New Exception("Không tìm thấy chứng từ trong hệ thống")
                        Return sRet
                    End If
                End If
            Catch ex As Exception
                CTuCommon.WriteLog(ex, "Lỗi hủy chứng từ", sTenDN)
                sRet = ""
                Throw ex
            End Try

            Return sRet
        End Function

        Public Shared Function GetCTUDetailCT(ByVal sSoCtu As String) As infoCTUBIENLAI
            Try
                Dim strSQL As String = "SELECT A.SO_CT, A.NGAY_KB, A.SHKB, A.TEN_KB, A.ma_cqqd, A.TEN_CQQD,"
                strSQL &= "  A.MA_LHTHU, A.TEN_LHTHU, A.TK_CO, A.MA_CHUONG, A.MA_NDKT,"
                strSQL &= "    A.NOI_DUNG, A.TTIEN, A.TTIEN_VPHC, A.LY_DO, A.TTIEN_NOP_CHAM,"
                strSQL &= "  A.LY_DO_NOP_CHAM, A.SO_QD, to_char( A.NGAY_QD,'DD/MM/YYYY') NGAY_QD, A.KYHIEU_CT,to_char(A.NGAY_CT,'DD/MM/YYYY') NGAY_CT,"
                strSQL &= "  A.pt_tt HINH_THUC , A.MA_DBHC, A.TRANG_THAI, A.MA_NV, A.MA_CN,"
                strSQL &= "  to_char( A.NGAY_HT,'DD/MM/YYYY HH24:MI:SS') NGAY_HT, A.MA_KS, A.LY_DO_TUCHOI, A.DIEN_GIAI, "
                strSQL &= "  A.MA_NNTHUE, A.TEN_NNTHUE, A.TEN_NNTIEN, A.DC_NNTIEN,"
                strSQL &= "  A.GIAYTO_NNTIEN, A.MA_CQTHU,to_char( A.NGAY_KS,'DD/MM/YYYY HH24:MI:SS') NGAY_KS, "
                strSQL &= " A.SO_CT_NH, A.TK_KH_NH, A.MA_NH_A,"
                strSQL &= "  A.TEN_NH_A, A.MA_NH_GT, A.TEN_NH_GT, A.MA_NH_TT, A.TEN_NH_TT,"
                strSQL &= "   A.TK_KH_NH,A.TEN_KH_NH,A.ly_do_vphc,"
                strSQL &= "  A.MA_NT, A.KYHIEU_BL, A.SO_BIENLAI,B.mota MOTA_TRANGTHAI, LOAIBIENLAI "
                strSQL &= "  FROM TCS_BIENLAI_HDR A, TCS_DM_TRANGTHAI_SP_BL b  WHERE A.trang_thai =  B.trangthai AND B.phan_he='CTBL' AND A.so_ct='" & sSoCtu & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Dim vobj As infoCTUBIENLAI = New infoCTUBIENLAI()
                            vobj.SO_CT = ds.Tables(0).Rows(0)("SO_CT").ToString()
                            vobj.NGAY_KB = ds.Tables(0).Rows(0)("NGAY_KB").ToString()
                            vobj.SHKB = ds.Tables(0).Rows(0)("SHKB").ToString()
                            vobj.TEN_KB = ds.Tables(0).Rows(0)("TEN_KB").ToString()
                            vobj.CQQD = ds.Tables(0).Rows(0)("MA_CQQD").ToString()
                            vobj.TEN_CQQD = ds.Tables(0).Rows(0)("TEN_CQQD").ToString()
                            vobj.MA_LHTHU = ds.Tables(0).Rows(0)("MA_LHTHU").ToString()
                            vobj.TEN_LHTHU = ds.Tables(0).Rows(0)("TEN_LHTHU").ToString()
                            vobj.TK_NS = ds.Tables(0).Rows(0)("TK_CO").ToString()
                            vobj.MA_CHUONG = ds.Tables(0).Rows(0)("MA_CHUONG").ToString()
                            vobj.MA_NDKT = ds.Tables(0).Rows(0)("MA_NDKT").ToString()
                            vobj.NOI_DUNG = ds.Tables(0).Rows(0)("NOI_DUNG").ToString()
                            vobj.TTIEN = ds.Tables(0).Rows(0)("TTIEN").ToString()
                            vobj.TTIEN_VPHC = ds.Tables(0).Rows(0)("TTIEN_VPHC").ToString()
                            'vobj.LY_DO = ds.Tables(0).Rows(0)("LY_DO").ToString()
                            vobj.TTIEN_NOP_CHAM = ds.Tables(0).Rows(0)("TTIEN_NOP_CHAM").ToString()
                            vobj.LY_DO_NOP_CHAM = ds.Tables(0).Rows(0)("LY_DO_NOP_CHAM").ToString()
                            vobj.SO_QD = ds.Tables(0).Rows(0)("SO_QD").ToString()
                            vobj.NGAY_QD = ds.Tables(0).Rows(0)("NGAY_QD").ToString()
                            vobj.KYHIEU_CT = ds.Tables(0).Rows(0)("KYHIEU_CT").ToString()
                            vobj.NGAY_CT = ds.Tables(0).Rows(0)("NGAY_CT").ToString()
                            vobj.HINH_THUC = ds.Tables(0).Rows(0)("HINH_THUC").ToString()
                            vobj.MA_DBHC = ds.Tables(0).Rows(0)("MA_DBHC").ToString()
                            vobj.TRANG_THAI = ds.Tables(0).Rows(0)("TRANG_THAI").ToString()
                            vobj.MA_NV = ds.Tables(0).Rows(0)("MA_NV").ToString()
                            vobj.MA_CN = ds.Tables(0).Rows(0)("MA_CN").ToString()
                            vobj.NGAY_HT = ds.Tables(0).Rows(0)("NGAY_HT").ToString()
                            vobj.MA_KS = ds.Tables(0).Rows(0)("MA_KS").ToString()
                            vobj.LY_DO_TUCHOI = ds.Tables(0).Rows(0)("LY_DO_TUCHOI").ToString()
                            vobj.DIEN_GIAI = ds.Tables(0).Rows(0)("DIEN_GIAI").ToString()
                            vobj.MA_NNTHUE = ds.Tables(0).Rows(0)("MA_NNTHUE").ToString()
                            vobj.TEN_NNTHUE = ds.Tables(0).Rows(0)("TEN_NNTHUE").ToString()
                            vobj.TEN_NNTIEN = ds.Tables(0).Rows(0)("TEN_NNTIEN").ToString()
                            vobj.DC_NNTIEN = ds.Tables(0).Rows(0)("DC_NNTIEN").ToString()
                            vobj.GIAYTO_NNTIEN = ds.Tables(0).Rows(0)("GIAYTO_NNTIEN").ToString()
                            vobj.MA_CQTHU = ds.Tables(0).Rows(0)("MA_CQTHU").ToString()
                            vobj.NGAY_KS = ds.Tables(0).Rows(0)("NGAY_KS").ToString()
                            vobj.SO_CT_NH = ds.Tables(0).Rows(0)("SO_CT_NH").ToString()
                            vobj.TK_KH_NH = ds.Tables(0).Rows(0)("TK_KH_NH").ToString()
                            vobj.MA_NH_A = ds.Tables(0).Rows(0)("MA_NH_A").ToString()
                            vobj.TEN_NH_A = ds.Tables(0).Rows(0)("TEN_NH_A").ToString()
                            vobj.MA_NH_B = ds.Tables(0).Rows(0)("MA_NH_GT").ToString()
                            vobj.TEN_NH_B = ds.Tables(0).Rows(0)("TEN_NH_GT").ToString()
                            vobj.MA_NH_TT = ds.Tables(0).Rows(0)("MA_NH_TT").ToString()
                            vobj.TEN_NH_TT = ds.Tables(0).Rows(0)("TEN_NH_TT").ToString()
                            vobj.TK_KH_NH = ds.Tables(0).Rows(0)("TK_KH_NH").ToString()
                            vobj.TEN_KH_NH = ds.Tables(0).Rows(0)("TEN_KH_NH").ToString()
                            'vobj.REF_CORE_TTSP = ds.Tables(0).Rows(0)("REF_CORE_TTSP").ToString()
                            vobj.MA_NT = ds.Tables(0).Rows(0)("MA_NT").ToString()
                            vobj.KYHIEU_BL = ds.Tables(0).Rows(0)("KYHIEU_BL").ToString()
                            vobj.SOBL = ds.Tables(0).Rows(0)("SO_BIENLAI").ToString()
                            vobj.MOTA_TRANGTHAI = ds.Tables(0).Rows(0)("MOTA_TRANGTHAI").ToString()
                            vobj.LOAIBIENLAI = ds.Tables(0).Rows(0)("LOAIBIENLAI").ToString()
                            vobj.LY_DO = ds.Tables(0).Rows(0)("ly_do_vphc").ToString()
                            'vobj.CODE_PHI = ds.Tables(0).Rows(0)("CODE_PHI").ToString()
                            Return vobj
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
            Return Nothing
        End Function


        Public Shared Function GetCTUDetailCapNhat(ByVal sSoCtu As String, ByVal ngay_kb As String) As infoCTUBIENLAI
            Try
                Dim strSQL As String = "SELECT A.SO_CT, A.NGAY_KB, A.SHKB, A.TEN_KB, A.ma_cqqd, A.TEN_CQQD,"
                strSQL &= "  A.MA_LHTHU, A.TEN_LHTHU, A.TK_CO, A.MA_CHUONG, A.MA_NDKT,"
                strSQL &= "    A.NOI_DUNG, A.TTIEN, A.TTIEN_VPHC, A.LY_DO, A.TTIEN_NOP_CHAM,"
                strSQL &= "  A.LY_DO_NOP_CHAM, A.SO_QD, to_char( A.NGAY_QD,'DD/MM/YYYY') NGAY_QD, A.KYHIEU_CT,to_char(A.NGAY_CT,'DD/MM/YYYY') NGAY_CT,"
                strSQL &= "  A.pt_tt HINH_THUC , A.MA_DBHC, A.TRANG_THAI, A.MA_NV, A.MA_CN,"
                strSQL &= "  to_char( A.NGAY_HT,'DD/MM/YYYY HH24:MI:SS') NGAY_HT, A.MA_KS, A.LY_DO_TUCHOI, A.DIEN_GIAI, "
                strSQL &= "  A.MA_NNTHUE, A.TEN_NNTHUE, A.TEN_NNTIEN, A.DC_NNTIEN,"
                strSQL &= "  A.GIAYTO_NNTIEN, A.MA_CQTHU,to_char( A.NGAY_KS,'DD/MM/YYYY HH24:MI:SS') NGAY_KS, "
                strSQL &= " A.SO_CT_NH, A.TK_KH_NH, A.MA_NH_A,"
                strSQL &= "  A.TEN_NH_A, A.MA_NH_GT, A.TEN_NH_GT, A.MA_NH_TT, A.TEN_NH_TT,"
                strSQL &= "   A.TK_KH_NH,A.TEN_KH_NH,A.ly_do_vphc,"
                strSQL &= "  A.MA_NT, A.KYHIEU_BL, A.SO_BIENLAI,B.mota MOTA_TRANGTHAI, LOAIBIENLAI "
                strSQL &= "  FROM TCS_BIENLAI_HDR A, TCS_DM_TRANGTHAI_SP_BL b  WHERE A.trang_thai =  B.trangthai AND B.phan_he='CTBL' AND A.so_ct='" & sSoCtu & "'"
                Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Dim vobj As infoCTUBIENLAI = New infoCTUBIENLAI()
                            vobj.SO_CT = ds.Tables(0).Rows(0)("SO_CT").ToString()
                            vobj.NGAY_KB = ds.Tables(0).Rows(0)("NGAY_KB").ToString()
                            vobj.SHKB = ds.Tables(0).Rows(0)("SHKB").ToString()
                            vobj.TEN_KB = ds.Tables(0).Rows(0)("TEN_KB").ToString()
                            vobj.CQQD = ds.Tables(0).Rows(0)("MA_CQQD").ToString()
                            vobj.TEN_CQQD = ds.Tables(0).Rows(0)("TEN_CQQD").ToString()
                            vobj.MA_LHTHU = ds.Tables(0).Rows(0)("MA_LHTHU").ToString()
                            vobj.TEN_LHTHU = ds.Tables(0).Rows(0)("TEN_LHTHU").ToString()
                            vobj.TK_NS = ds.Tables(0).Rows(0)("TK_CO").ToString()
                            vobj.MA_CHUONG = ds.Tables(0).Rows(0)("MA_CHUONG").ToString()
                            vobj.MA_NDKT = ds.Tables(0).Rows(0)("MA_NDKT").ToString()
                            vobj.NOI_DUNG = ds.Tables(0).Rows(0)("NOI_DUNG").ToString()
                            vobj.TTIEN = ds.Tables(0).Rows(0)("TTIEN").ToString()
                            vobj.TTIEN_VPHC = ds.Tables(0).Rows(0)("TTIEN_VPHC").ToString()
                            'vobj.LY_DO = ds.Tables(0).Rows(0)("LY_DO").ToString()
                            vobj.TTIEN_NOP_CHAM = ds.Tables(0).Rows(0)("TTIEN_NOP_CHAM").ToString()
                            vobj.LY_DO_NOP_CHAM = ds.Tables(0).Rows(0)("LY_DO_NOP_CHAM").ToString()
                            vobj.SO_QD = ds.Tables(0).Rows(0)("SO_QD").ToString()
                            vobj.NGAY_QD = ds.Tables(0).Rows(0)("NGAY_QD").ToString()
                            vobj.KYHIEU_CT = ds.Tables(0).Rows(0)("KYHIEU_CT").ToString()
                            vobj.NGAY_CT = ds.Tables(0).Rows(0)("NGAY_CT").ToString()
                            vobj.HINH_THUC = ds.Tables(0).Rows(0)("HINH_THUC").ToString()
                            vobj.MA_DBHC = ds.Tables(0).Rows(0)("MA_DBHC").ToString()
                            vobj.TRANG_THAI = ds.Tables(0).Rows(0)("TRANG_THAI").ToString()
                            vobj.MA_NV = ds.Tables(0).Rows(0)("MA_NV").ToString()
                            vobj.MA_CN = ds.Tables(0).Rows(0)("MA_CN").ToString()
                            vobj.NGAY_HT = ds.Tables(0).Rows(0)("NGAY_HT").ToString()
                            vobj.MA_KS = ds.Tables(0).Rows(0)("MA_KS").ToString()
                            vobj.LY_DO_TUCHOI = ds.Tables(0).Rows(0)("LY_DO_TUCHOI").ToString()
                            vobj.DIEN_GIAI = ds.Tables(0).Rows(0)("DIEN_GIAI").ToString()
                            vobj.MA_NNTHUE = ds.Tables(0).Rows(0)("MA_NNTHUE").ToString()
                            vobj.TEN_NNTHUE = ds.Tables(0).Rows(0)("TEN_NNTHUE").ToString()
                            vobj.TEN_NNTIEN = ds.Tables(0).Rows(0)("TEN_NNTIEN").ToString()
                            vobj.DC_NNTIEN = ds.Tables(0).Rows(0)("DC_NNTIEN").ToString()
                            vobj.GIAYTO_NNTIEN = ds.Tables(0).Rows(0)("GIAYTO_NNTIEN").ToString()
                            vobj.MA_CQTHU = ds.Tables(0).Rows(0)("MA_CQTHU").ToString()
                            vobj.NGAY_KS = ds.Tables(0).Rows(0)("NGAY_KS").ToString()
                            vobj.SO_CT_NH = ds.Tables(0).Rows(0)("SO_CT_NH").ToString()
                            vobj.TK_KH_NH = ds.Tables(0).Rows(0)("TK_KH_NH").ToString()
                            vobj.MA_NH_A = ds.Tables(0).Rows(0)("MA_NH_A").ToString()
                            vobj.TEN_NH_A = ds.Tables(0).Rows(0)("TEN_NH_A").ToString()
                            vobj.MA_NH_B = ds.Tables(0).Rows(0)("MA_NH_GT").ToString()
                            vobj.TEN_NH_B = ds.Tables(0).Rows(0)("TEN_NH_GT").ToString()
                            vobj.MA_NH_TT = ds.Tables(0).Rows(0)("MA_NH_TT").ToString()
                            vobj.TEN_NH_TT = ds.Tables(0).Rows(0)("TEN_NH_TT").ToString()
                            vobj.TK_KH_NH = ds.Tables(0).Rows(0)("TK_KH_NH").ToString()
                            vobj.TEN_KH_NH = ds.Tables(0).Rows(0)("TEN_KH_NH").ToString()
                            'vobj.REF_CORE_TTSP = ds.Tables(0).Rows(0)("REF_CORE_TTSP").ToString()
                            vobj.MA_NT = ds.Tables(0).Rows(0)("MA_NT").ToString()
                            vobj.KYHIEU_BL = ds.Tables(0).Rows(0)("KYHIEU_BL").ToString()
                            vobj.SOBL = ds.Tables(0).Rows(0)("SO_BIENLAI").ToString()
                            vobj.MOTA_TRANGTHAI = ds.Tables(0).Rows(0)("MOTA_TRANGTHAI").ToString()
                            vobj.LOAIBIENLAI = ds.Tables(0).Rows(0)("LOAIBIENLAI").ToString()
                            vobj.LY_DO = ds.Tables(0).Rows(0)("ly_do_vphc").ToString()
                            'vobj.CODE_PHI = ds.Tables(0).Rows(0)("CODE_PHI").ToString()
                            Return vobj
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
            Return Nothing
        End Function
        Public Shared Function updateTrangThaiBienLaiKS(ByVal sSo_CT As String, ByVal sNgayKB As String, ByVal sMaNV As String, ByVal sTrangThai As String, ByVal sLydoTC As String) As String
            Dim sSQL As String = String.Empty
            Dim iRet As Integer = 0
            Dim listParam As List(Of IDataParameter) = Nothing
            Try
                sSQL = "update TCS_BIENLAI_HDR set TRANG_THAI =:TRANG_THAI,MA_KS = :MA_KS, NGAY_HT=SYSDATE,ly_do_tuchoi =: ly_do_tuchoi where SO_CT = :SO_CT and NGAY_KB =:NGAY_KB"

                listParam = New List(Of IDataParameter)
                listParam.Add(DataAccess.NewDBParameter(":TRANG_THAI", ParameterDirection.Input, sTrangThai, DbType.String))
                listParam.Add(DataAccess.NewDBParameter(":MA_KS", ParameterDirection.Input, sMaNV, DbType.String))
                listParam.Add(DataAccess.NewDBParameter(":ly_do_tuchoi", ParameterDirection.Input, sLydoTC, DbType.String))
                listParam.Add(DataAccess.NewDBParameter(":SO_CT", ParameterDirection.Input, sSo_CT, DbType.String))
                listParam.Add(DataAccess.NewDBParameter(":NGAY_KB", ParameterDirection.Input, sNgayKB, DbType.String))
                iRet = DataAccess.ExecuteNonQuery(sSQL, listParam.ToArray())
            Catch ex As Exception
                CTuCommon.WriteLog(ex, "Lỗi khi cập nhật trạng thái chứng từ biên lai", sMaNV)
                Throw ex
            End Try

            Return ""
        End Function

        Public Shared Function CTU_IN_BL_NEW(ByVal so_ct As String) As DataSet
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                'strSQL = "select dtl.noi_dung as NOI_DUNG, " & _
                '        " dtl.TTIEN as SOTIEN " & _
                '        " from TCS_BIENLAI_HDR dtl " & _
                '        " where   dtl.so_ct = '" & so_ct & "' " & _
                '        " order by rownum "
                strSQL = "select ly_do_vphc as NOI_DUNG, ttien_vphc as SOTIEN "
                strSQL &= " from TCS_BIENLAI_HDR  "
                strSQL &= " where   so_ct = '" & so_ct & "'"
                strSQL &= " UNION ALL "
                strSQL &= " select 'Phat cham nop' NOI_DUNG, ttien_nop_cham as SOTIEN "
                strSQL &= " from TCS_BIENLAI_HDR  "
                strSQL &= " where   so_ct = '" & so_ct & "'"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
                'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
                '  & "Error code: System error!" & vbNewLine _
                '  & "Error message: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        'Public Shared Function CTU_IN_BL_NEW(ByVal so_ct As String, ByVal ref_core_ttsp As String) As DataSet
        '    Dim cnCT As DataAccess
        '    Try
        '        cnCT = New DataAccess
        '        Dim strSQL As String
        '        strSQL = "select rownum as STT,dtl.SO_CT,dtl.noi_dung as NOI_DUNG,dtl.ma_chuong as CC, " & _
        '                "'' as LK, dtl.ma_ndkt as MTM, " & _
        '                " dtl.TTIEN as SOTIEN, 0 as SOTIEN_NT, '' NGAY_TK, " & _
        '                 " dtl.ma_dbhc " & _
        '                "from tcs_ctu_bienlai_HDR dtl " & _
        '                "where   dtl.so_ct = '" & so_ct & "' and dtl.REF_CORE_TTSP='" & ref_core_ttsp & "'" & _
        '                " order by rownum "

        '        Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception
        '        CTuCommon.WriteLog(ex, "Lỗi in chứng từ")
        '        'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
        '        '  & "Error code: System error!" & vbNewLine _
        '        '  & "Error message: " & ex.Message, EventLogEntryType.Error)
        '        Throw ex
        '    Finally
        '        If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        '    End Try
        'End Function

        Public Shared Function CTU_Get_LanIn_BL(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select LAN_IN_BL from TCS_BIENLAI_HDR " & _
                        "where so_ct = '" & so_ct & "' "

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = ""
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If
                Return strLanIn
            Catch ex As Exception
                CTuCommon.WriteLog(ex, "Lỗi lấy số lần in của bảng kê biên lai")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Shared Sub CTU_Update_LanIn_BL(ByVal so_ct As String)
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select LAN_IN_BL from TCS_BIENLAI_HDR " & _
                                "where so_ct = '" & so_ct & "'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strLanIn As String = "0"
                If (drCT.Read()) Then
                    strLanIn = drCT(0).ToString()
                End If

                If (Not IsNumeric(strLanIn)) Then strLanIn = "0"

                Dim intSoLanIn As Integer = Convert.ToInt16(strLanIn)
                intSoLanIn += 1

                cnCT = New DataAccess
                strSQL = "update TCS_BIENLAI_HDR set LAN_IN_BL = " & intSoLanIn.ToString() & " " & _
                        "where so_ct = '" & so_ct & "'"
                cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                CTuCommon.WriteLog(ex, "Lỗi cập nhật lần in bảng kê")

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Sub

        Public Shared Function GET_DM_TRANGTHAI_BL(ByVal strPH As String) As DataTable
            Dim strSQL As String
            Dim dtResult As DataTable = New DataTable()
            Dim dk As String = ""
            Try
                strSQL = ""
                Dim arrParam(0) As IDataParameter

                strSQL = "SELECT trangthai,mota FROM TCS_DM_TRANGTHAI_SP_BL  Where phan_he=:phan_he "

                arrParam(0) = DataAccess.NewDBParameter("phan_he", ParameterDirection.Input, strPH, OracleClient.OracleType.NVarChar)
                dtResult = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, arrParam))


            Catch ex As Exception
            Finally
            End Try
            Return dtResult
        End Function

        Public Shared Function Get_TEN_TrangThai_BL(ByVal strIDTrangThai As String) As String
            Dim strResult As String

            Dim sSQL As String = ""
            Dim listParam As List(Of IDataParameter) = Nothing
            Dim dt As DataTable = Nothing
            Try
                sSQL = "SELECT mota FROM TCS_DM_TRANGTHAI_SP_BL WHERE phan_he='CTBL' and trangthai='" & strIDTrangThai & "'"
                dt = Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, Nothing))
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    strResult = dt.Rows(0)(0).ToString()
                End If
            Catch ex As Exception
                strResult = ""
            End Try

            Return strResult
        End Function

        Public Shared Function CTU_GetListKiemSoatBL(ByVal strNgayCT As String, ByVal strTrangThai As String, _
                 Optional ByVal strMaNV As String = "", Optional ByVal strMaKS As String = "", Optional ByVal strSoBT As String = "", Optional ByVal strKenhCT As String = "") As DataSet
            Dim cnCT As DataAccess
            Dim ds As DataSet
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim strWhereLoaiThue As String = ""

                strSQL = "SELECT A.KYHIEU_CT, A.SO_CT,  A.SO_CT_NH, A.TRANG_THAI,  A.TTIEN, A.MA_NV,  A.SHKB, A.NGAY_KB,  B.TEN_DN,A.MA_KS " & _
                         " from TCS_BIENLAI_HDR A, TCS_DM_NHANVIEN B " & _
                         " where A.MA_NV = B.MA_NV " & _
                         " AND NGAY_KB = " & strNgayCT & " "

                If (strTrangThai.Trim <> "") Then
                    strSQL = strSQL & " and A.Trang_thai='" & strTrangThai & "' "
                End If
                If Not "".Equals(strMaNV) Then
                    strSQL = strSQL & " and (A.Ma_NV=" & strMaNV & ") "
                End If
                If Not "".Equals(strMaKS) Then
                    strSQL = strSQL & " and A.ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMaKS & "')) "
                End If

                strSQL = strSQL & " order by A.ma_nv, A.so_ct DESC"
                ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                CTuCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ")

                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Shared Function CTU_Get_TrangThai(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select trang_thai from TCS_BIENLAI_HDR " & _
                        "where  so_ct = '" & so_ct & "'"

                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strTT As String = ""
                If (drCT.Read()) Then
                    strTT = drCT(0).ToString()
                End If
                Return strTT
            Catch ex As Exception
                CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy trạng thái chứng từ")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
        Public Shared Function CTU_Get_PTTT(ByVal so_ct As String) As String
            Dim cnCT As DataAccess
            Dim drCT As IDataReader
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select hinh_thuc from TCS_BIENLAI_HDR " & _
                         "where  so_ct = '" & so_ct & "'"


                drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                Dim strPT_TT As String = ""
                If (drCT.Read()) Then
                    strPT_TT = drCT(0).ToString()
                End If
                Return strPT_TT
            Catch ex As Exception
                CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chứng từ")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
                If (Not IsNothing(drCT)) Then
                    drCT.Close()
                    drCT.Dispose()
                End If
            End Try
        End Function
    End Class
End Namespace
