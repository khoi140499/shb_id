﻿Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports System.Data
Imports Business.Common.mdlSystemVariables
Imports Business

Public Class lovDanhMuc

    Private mv_strMaDiemThu As String
    'Private mv_strMaDBHC As String
    Private mv_strSHKB As String

    'Private mv_strMA_LOAIHINH As String

    Public Sub New(ByVal pv_strMaDiemThu As String, ByVal pv_strSKKB As String)
        mv_strMaDiemThu = pv_strMaDiemThu
        'mv_strMaDBHC = pv_strMaDBHC
        mv_strSHKB = pv_strSKKB

        'mv_strMA_LOAIHINH = pv_strMA_LOAIHINH
    End Sub

#Region "Hàm chính"
    Public Sub ShowTitle(ByVal type As String, ByRef strMaDanhMuc As String, ByRef strTenDanhMuc As String)
        Select Case type
            Case "CapChuong"
                strMaDanhMuc = "Mã cấp chương"
                strTenDanhMuc = "Tên cấp chương"
            Case "LoaiKhoan"
                strMaDanhMuc = "Mã ngành kinh tế"
                strTenDanhMuc = "Tên ngành kinh tế"
            Case "MucTMuc"
                strMaDanhMuc = "Mã mục tiểu mục"
                strTenDanhMuc = "Tên mục tiểu mục"
            Case "DBHC"
                strMaDanhMuc = "Mã địa bàn hành chính"
                strTenDanhMuc = "Tên địa bàn hành chính"
            Case "DBHCAll"
                strMaDanhMuc = "Mã địa bàn hành chính"
                strTenDanhMuc = "Tên địa bàn hành chính"
            Case "DBHCTINH"
                strMaDanhMuc = "Mã địa bàn hành chính"
                strTenDanhMuc = "Tên địa bàn hành chính"
            Case "DBHCHUYEN"
                strMaDanhMuc = "Mã địa bàn hành chính"
                strTenDanhMuc = "Tên địa bàn hành chính"
            Case "DBHCAtDThu"
                strMaDanhMuc = "Mã địa bàn hành chính"
                strTenDanhMuc = "Tên địa bàn hành chính"
            Case "NguyenTe"
                strMaDanhMuc = "Mã nguyên tệ"
                strTenDanhMuc = "Tên nguyên tệ"
            Case "NNThue"
                strMaDanhMuc = "Mã người nộp thuế"
                strTenDanhMuc = "Tên người nộp thuế"
            Case "NNThueNew"
                strMaDanhMuc = "Mã người nộp thuế"
                strTenDanhMuc = "Tên người nộp thuế"
            Case "NganHang"
                strMaDanhMuc = "Mã ngân hàng"
                strTenDanhMuc = "Tên ngân hàng"
            Case "CNNganHang"
                strMaDanhMuc = "Mã chi nhánh"
                strTenDanhMuc = "Tên chi nhánh"
            Case "CQThu"
            Case "CQThu_Thue"
            Case "CQThu_HaiQuan"
                strMaDanhMuc = "Mã cơ quan thu"
                strTenDanhMuc = "Tên cơ quan thu"
            Case "CQThuAtDThu"
                strMaDanhMuc = "Mã cơ quan thu"
                strTenDanhMuc = "Tên cơ quan thu"
            Case "TaiKhoan"
                strMaDanhMuc = "Mã tài khoản"
                strTenDanhMuc = "Tên tài khoản"
            Case "CQQuyetDinh"
                strMaDanhMuc = "Mã cơ quan quyết định"
                strTenDanhMuc = "Tên cơ quan quyết định"
            Case "LHThu"
                strMaDanhMuc = "Mã loại hình thu"
                strTenDanhMuc = "Tên loại hình thu"
            Case "LHXNK"
                strMaDanhMuc = "Mã loại hình xuất nhập khẩu"
                strTenDanhMuc = "Tên loại hình xuất nhập khẩu"
            Case "MaQuy"
                strMaDanhMuc = "Mã quỹ"
                strTenDanhMuc = "Tên quỹ"
            Case "KhoBac"
                strMaDanhMuc = "SHKB"
                strTenDanhMuc = "Tên kho bạc"
            Case "DM_NV"
                strMaDanhMuc = "Mã nhân viên"
                strTenDanhMuc = "Tên nhân viên"
            Case "DMNH_TT"
                strMaDanhMuc = "Mã ngân hàng"
                strTenDanhMuc = "Tên ngân hàng"
            Case "DMNH_GT"
                strMaDanhMuc = "Mã ngân hàng"
                strTenDanhMuc = "Tên ngân hàng"
                'sonmt
            Case "LOAI_NNTHUE"
                strMaDanhMuc = "Mã loại NNT"
                strTenDanhMuc = "Tên loại NNT"
            Case Else
                strMaDanhMuc = "Mã danh mục"
                strTenDanhMuc = "Tên tên danh mục"
        End Select
    End Sub

    Public Function ShowDanhMuc(ByVal type As String, ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String, ByVal strMA_LOAIHINH As String, Optional pvSHKB As String = "", Optional strMa_DBHC As String = "") As DataTable
        Select Case type.ToUpper
            Case "CapChuong".ToUpper
                Return ShowDM_CapChuong(strMaDanhMuc, strTenDanhMuc)
            Case "DMNH_TT".ToUpper
                Return ShowDM_DMNH_TT(strMaDanhMuc, strTenDanhMuc)
            Case "DMNH_GT".ToUpper
                Return ShowDM_DMNH_GT(strMaDanhMuc, strTenDanhMuc, pvSHKB)
            Case "LoaiKhoan".ToUpper
                Return ShowDM_LoaiKhoan(strMaDanhMuc, strTenDanhMuc)
            Case "MucTMuc".ToUpper
                Return ShowDM_MucTMuc(strMaDanhMuc, strTenDanhMuc)
            Case "DBHC".ToUpper
                Return ShowDM_DBHC(strMaDanhMuc, strTenDanhMuc, strMa_DBHC)
            Case "DBHCAll".ToUpper
                Return ShowDM_DBHCAll(strMaDanhMuc, strTenDanhMuc)
            Case "DBHCAtDThu".ToUpper
                Return ShowDM_DBHCAtDThu(strMaDanhMuc, strTenDanhMuc)
            Case "NguyenTe".ToUpper
                Return ShowDM_NguyenTe(strMaDanhMuc, strTenDanhMuc)
            Case "NNThue".ToUpper
                Return ShowDM_NNThue(strMaDanhMuc, strTenDanhMuc)
            Case "NganHang".ToUpper
                Return ShowDM_NganHang(strMaDanhMuc, strTenDanhMuc)
            Case "CNNganHang".ToUpper
                Return ShowDM_CNNganHang(strMaDanhMuc, strTenDanhMuc)
            Case "CQThu".ToUpper
                Return ShowDM_CQThu(strMaDanhMuc, strTenDanhMuc)
            Case "CQThu_Thue".ToUpper
                Return ShowDM_CQThu_Thue(strMaDanhMuc, strTenDanhMuc)
            Case "CQThu_HaiQuan".ToUpper
                Return ShowDM_CQThu_HaiQuan(strMaDanhMuc, strTenDanhMuc)
            Case "CQThuAtDThu".ToUpper
                Return ShowDM_CQThuAtDThu(strMaDanhMuc, strTenDanhMuc)
            Case "TaiKhoan".ToUpper
                Return ShowDM_TaiKhoan(strMaDanhMuc, strTenDanhMuc)
            Case "CQQuyetDinh".ToUpper
                Return ShowDM_CQQuyetDinh(strMaDanhMuc, strTenDanhMuc)
            Case "CQQD".ToUpper
                Return ShowDM_CQQD(strMaDanhMuc, strTenDanhMuc)
            Case "CQQD1".ToUpper
                Return ShowDM_CQQuyetDinh1(strMaDanhMuc, strTenDanhMuc, strMA_LOAIHINH)
            Case "LHThu".ToUpper
                Return ShowDM_LHThu(strMaDanhMuc, strTenDanhMuc)
            Case "LHTHUBIENLAI".ToUpper
                Return ShowLHTHUBIENLAI(strMaDanhMuc, strTenDanhMuc)
            Case "LHXNK".ToUpper
                Return ShowDM_LHXNK(strMaDanhMuc, strTenDanhMuc)
            Case "MaQuy".ToUpper
                Return ShowDM_MaQuy(strMaDanhMuc, strTenDanhMuc)
            Case "KhoBac".ToUpper
                Return ShowDM_KhoBac(strMaDanhMuc, strTenDanhMuc)
            Case "Ma_HaiQuan".ToUpper
                Return ShowDM_Ma_HaiQuan(strMaDanhMuc, strTenDanhMuc)
            Case "DM_NV".ToUpper
                Return ShowDM_NV(strMaDanhMuc, strTenDanhMuc)
            Case "DBHCTINH".ToUpper
                Return ShowDM_DBHC_TINH(strMaDanhMuc, strTenDanhMuc)
            Case "DBHCHUYEN".ToUpper
                Return ShowDM_DBHC_HUYEN(strMaDanhMuc, strTenDanhMuc)
                'sonmt
            Case "LOAI_NNTHUE".ToUpper
                Return ShowDM_LOAI_NNT(strMaDanhMuc, strTenDanhMuc)
            Case Else
                Return Nothing
        End Select
    End Function
#End Region

#Region "ShowDM"
    Private Function ShowDM_CapChuong(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable

        Try
            Dim strSQL As String = "SELECT MA_CHUONG as MA_DM, TEN as TEN_DM " & _
                                   " FROM TCS_DM_CAP_CHUONG  WHERE MA_CHUONG like '%" & strMaDanhMuc & "%' " & _
                                   " and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' and TINH_TRANG='1' ORDER BY MA_CHUONG "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Function ShowDM_DMNH_TT(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable

        Try
            Dim strSQL As String = "SELECT MA_TRUCTIEP as MA_DM, TEN_TRUCTIEP as TEN_DM " & _
                                   " FROM tcs_dm_nh_giantiep_tructiep  WHERE MA_TRUCTIEP like '%" & strMaDanhMuc & "%' " & _
                                   " and UPPER(TEN_TRUCTIEP) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA_TRUCTIEP "

            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Function ShowDM_DMNH_GT(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String, Optional ByVal pvSHKB As String = "") As DataTable

        Try
            'Dim strSQL As String = "SELECT DISTINCT( MA_GIANTIEP) as MA_DM, TEN_GIANTIEP as TEN_DM " & _
            '                       " FROM tcs_dm_nh_giantiep_tructiep  WHERE MA_GIANTIEP like '%" & strMaDanhMuc & "%' and SHKB like '%" & pvSHKB & "%'  " & _
            '                       " and UPPER(TEN_GIANTIEP) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY  TT_SHB desc,MA_GIANTIEP "
            Dim strSQL As String = "SELECT DISTINCT( MA_GIANTIEP) as MA_DM, TEN_GIANTIEP as TEN_DM,TT_SHB " & _
                                 " FROM tcs_dm_nh_giantiep_tructiep  WHERE MA_GIANTIEP like '%" & strMaDanhMuc & "%' and SHKB like '%" & pvSHKB & "%'  " & _
                                 " and UPPER(TEN_GIANTIEP) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY  TT_SHB desc,MA_GIANTIEP "

            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_LoaiKhoan(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable

        Try
            Dim strSQL As String = "SELECT MA_KHOAN as MA_DM, TEN as TEN_DM " & _
                                   "FROM TCS_DM_LOAI_KHOAN  WHERE MA_KHOAN like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' and TINH_TRANG='1' ORDER BY MA_KHOAN  "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_MucTMuc(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable

        Try
            Dim strSQL As String = "SELECT MA_TMUC as MA_DM, TEN as TEN_DM " & _
                                   "FROM TCS_DM_MUC_TMUC  WHERE MA_TMUC like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' and TINH_TRANG='1' ORDER BY MA_TMUC"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_DBHC(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String, Optional ByVal strMa_DBHC As String = "") As DataTable
        Try
            Dim strSQL As String = ""
            strSQL = GetString_DBThu(strMaDanhMuc, strTenDanhMuc, mv_strSHKB, strMa_DBHC)
            If strSQL <> "" Then
                Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    Return dt
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function ShowDM_DBHCAll(ByVal strMaXa As String, ByVal strTenXa As String) As DataTable
        Dim strSQL As String = ""
        Try

            strSQL = GetMaDBHCAll(strMaXa, strTenXa)
            If strSQL <> "" Then
                Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    Return dt
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function ShowDM_DBHCAtDThu(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strDBHC As String = CTuCommon.Get_ParmValues("DB_THU", mv_strMaDiemThu)
            Dim strSQL As String = CTuCommon.GetString_DBThu(strDBHC, strMaDanhMuc, mv_strMaDiemThu)
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_NguyenTe(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT MA_NT as MA_DM, TEN as TEN_DM " & _
                                   "FROM TCS_DM_NGUYENTE  WHERE MA_NT like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA_NT "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_NNThue(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT MA_NNT as MA_DM, TEN_NNT as TEN_DM " & _
                                   "FROM TCS_DM_NNT  WHERE MA_NNT like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(TEN_NNT) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA_NNT "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_NganHang(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT MA as MA_DM, TEN as TEN_DM " & _
                                   "FROM TCS_DM_NGANHANG  WHERE MA like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Function ShowDM_CNNganHang(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT ID as MA_DM, NAME as TEN_DM " & _
                                   "FROM TCS_DM_CHINHANH  WHERE ID like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(NAME) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY id "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_CQThu(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = ""
            'If mv_strSHKB.Trim.Length > 0 Then
            '    strSQL = "SELECT MA_CQTHU as MA_DM, TEN as TEN_DM " & _
            '                                      "FROM TCS_DM_CQTHU  WHERE MA_CQTHU like '%" & strMaDanhMuc & "%' " & _
            '                                       "and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' AND MA_DTHU in(SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & mv_strSHKB & "')"

            'Else
            '    strSQL = "SELECT MA_CQTHU as MA_DM, TEN as TEN_DM " & _
            '                                     "FROM TCS_DM_CQTHU  WHERE MA_CQTHU like '%" & strMaDanhMuc & "%' " & _
            '                                      "and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' AND MA_DTHU=" & mv_strMaDiemThu
            'End If
            strSQL = "SELECT MA_CQTHU as MA_DM, TEN as TEN_DM FROM TCS_DM_CQTHU WHERE MA_CQTHU like '%" & strMaDanhMuc & "%' " & _
                                                   "and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%'  ORDER BY MA_CQTHU"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_CQThu_Thue(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = ""
            strSQL = " SELECT MA_CQTHU as MA_DM, TEN as TEN_DM FROM TCS_DM_CQTHU " & _
                     " WHERE MA_CQTHU like '%" & strMaDanhMuc & "%' " & _
                           " AND UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' " & _
                           " AND MA_QLT IS NOT NULL ORDER BY MA_CQTHU "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_CQThu_HaiQuan(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = ""
            strSQL = " SELECT MA_CQTHU as MA_DM, TEN as TEN_DM FROM TCS_DM_CQTHU " & _
                     " WHERE MA_CQTHU like '%" & strMaDanhMuc & "%' " & _
                           " AND UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' " & _
                           " AND MA_HQ IS NOT NULL ORDER BY MA_CQTHU"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Function ShowDM_Ma_HaiQuan(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = ""
            strSQL = " SELECT MA_HQ as MA_DM, TEN as TEN_DM FROM TCS_DM_CQTHU " & _
                     " WHERE MA_HQ like '%" & strMaDanhMuc & "%' " & _
                           " AND UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' " & _
                           " AND MA_HQ IS NOT NULL ORDER BY MA_HQ"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_CQThuAtDThu(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT MA_CQTHU as MA_DM, TEN as TEN_DM " & _
                                   "FROM TCS_DM_CQTHU  WHERE MA_CQTHU like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' and MA_DTHU='" & mv_strMaDiemThu & "' ORDER BY MA_CQTHU "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_TaiKhoan(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT TK as MA_DM, TEN_TK as TEN_DM " & _
                                   "FROM TCS_DM_TAIKHOAN  WHERE TK like '%" & strMaDanhMuc.Replace(".", "") & "%' " & _
                                   "and UPPER(TEN_TK) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY TK "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_CQQuyetDinh(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            'Dim strSQL As String = "SELECT MA_CQQD as MA_DM, TEN_CQQD, CAP as TEN_DM " & _
            '                       "FROM TCS_DM_CQQD  WHERE MA_CQQD like '%" & strMaDanhMuc & "%' " & _
            '                       "and UPPER(TEN_CQQD) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA_CQQD"

            Dim strSQL As String = "SELECT DISTINCT TCS_DM_CQQD.MA_CQQD as MA_DM,  TCS_DM_CQQD.TEN_CQQD, TCS_DM_CQQD.CAP as TEN_DM" & _
          " FROM TCS_DM_CQQD, TCS_MAP_CQQD_LHTHU" & _
          " WHERE TCS_DM_CQQD.MA_CQQD=TCS_MAP_CQQD_LHTHU.MA_CQQD and TCS_MAP_CQQD_LHTHU.MA_LH not in ('981','982','983') and  TCS_DM_CQQD.MA_CQQD like '%" & strMaDanhMuc & "%' and UPPER(TCS_DM_CQQD.TEN_CQQD) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY TCS_DM_CQQD.MA_CQQD"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_CQQuyetDinh1(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String, ByVal strMA_LOAIHINH As String) As DataTable
        Try
            'Dim strSQL As String = "SELECT MA_CQQD as MA_DM, TEN_CQQD, CAP as TEN_DM " & _
            '                       "FROM TCS_DM_CQQD  WHERE MA_LH in ('981','982','983') and MA_CQQD like '%" & strMaDanhMuc & "%' " & _
            '                       "and UPPER(TEN_CQQD) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA_CQQD"


            ' Dim strSQL As String = "SELECT DISTINCT TCS_DM_CQQD.MA_CQQD as MA_DM,  TCS_DM_CQQD.TEN_CQQD, TCS_DM_CQQD.CAP as TEN_DM" & _
            '" FROM TCS_DM_CQQD, TCS_MAP_CQQD_LHTHU" & _
            '" WHERE TCS_DM_CQQD.MA_CQQD=TCS_MAP_CQQD_LHTHU.MA_CQQD and TCS_MAP_CQQD_LHTHU.MA_LH in ('981','982','983') and  TCS_DM_CQQD.MA_CQQD like '%" & strMaDanhMuc & "%' and UPPER(TCS_DM_CQQD.TEN_CQQD) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY TCS_DM_CQQD.MA_CQQD"


            Dim strSQL As String = "SELECT DISTINCT TCS_DM_CQQD.MA_CQQD as MA_DM,  TCS_DM_CQQD.TEN_CQQD, TCS_DM_CQQD.CAP as TEN_DM" & _
         " FROM TCS_DM_CQQD, TCS_MAP_CQQD_LHTHU" & _
         " WHERE TCS_DM_CQQD.MA_CQQD=TCS_MAP_CQQD_LHTHU.MA_CQQD and TCS_MAP_CQQD_LHTHU.MA_LH in ('" & strMA_LOAIHINH & "') and  TCS_DM_CQQD.MA_CQQD like '%" & strMaDanhMuc & "%' and UPPER(TCS_DM_CQQD.TEN_CQQD) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY TCS_DM_CQQD.MA_CQQD"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_CQQD(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            'Dim strSQL As String = "SELECT MA_CQQD as MA_DM, TEN_CQQD  TEN_DM " & _
            '                       "FROM TCS_DM_CQQD  WHERE MA_KB='" & mv_strSHKB & "' and MA_CQQD like '%" & strMaDanhMuc & "%'  " & _
            '                       "and UPPER(TEN_CQQD) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA_CQQD"
            Dim strSQL As String = "SELECT DISTINCT TCS_DM_CQQD.MA_CQQD as MA_DM,  TCS_DM_CQQD.TEN_CQQD, TCS_DM_CQQD.CAP as TEN_DM" & _
       " FROM TCS_DM_CQQD, TCS_MAP_CQQD_LHTHU" & _
       " WHERE TCS_DM_CQQD.MA_CQQD=TCS_MAP_CQQD_LHTHU.MA_CQQD and MA_KB='" & mv_strSHKB & "' and TCS_MAP_CQQD_LHTHU.MA_LH not in ('981','982','983') and  TCS_DM_CQQD.MA_CQQD like '%" & strMaDanhMuc & "%' and UPPER(TCS_DM_CQQD.TEN_CQQD) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY TCS_DM_CQQD.MA_CQQD"

            '     Dim strSQL As String = "SELECT DISTINCT TCS_DM_CQQD.MA_CQQD as MA_DM,  TCS_DM_CQQD.TEN_CQQD, TCS_DM_CQQD.CAP as TEN_DM" & _
            '" FROM TCS_DM_CQQD, TCS_MAP_CQQD_LHTHU" & _
            '" WHERE TCS_DM_CQQD.MA_CQQD=TCS_MAP_CQQD_LHTHU.MA_CQQD and MA_KB='" & mv_strSHKB & "' and TCS_DM_CQQD.MA_CQQD like '%" & strMaDanhMuc & "%' and UPPER(TCS_DM_CQQD.TEN_CQQD) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY TCS_DM_CQQD.MA_CQQD"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

   
    Private Function ShowLHTHUBIENLAI(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT MA_LH as MA_DM, TEN_LH as TEN_DM " & _
                                   "FROM TCS_DM_LH_THU_BIENLAI  WHERE SHKB='" & mv_strSHKB & "' AND MA_LH like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(TEN_LH) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA_LH"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Function ShowDM_LHThu(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT MA_LH as MA_DM, TEN_LH as TEN_DM " & _
                                   "FROM TCS_DM_LHTHU  WHERE MA_LH like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(TEN_LH) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA_LH"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_LHXNK(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT MA_LH as MA_DM, TEN_LH as TEN_DM, TEN_VT " & _
                                   "FROM TCS_DM_LHINH  WHERE MA_LH  like '%" & strMaDanhMuc.ToUpper() & "%' " & _
                                   "and UPPER(TEN_LH) like '%" & strTenDanhMuc.ToUpper() & "%' ORDER BY MA_LH"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_MaQuy(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT MA as MA_DM, TEN as TEN_DM " & _
                                   "FROM TCS_DM_MAQUY  WHERE MA like '%" & strMaDanhMuc & "%' " & _
                                   "and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' and TINH_TRANG='1' ORDER BY MA"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception
        End Try
    End Function

    Private Function ShowDM_KhoBac(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            'SELECT b.shkb,b.ten FROM TCS_THAMSO a,TCS_DM_KHOBAC b WHERE  instr(a.giatri_ts , b.shkb )>0  And a.ten_ts='SHKB_DThu'  and a.ma_dthu='" & pv_strMaDiemThu & ""
            Dim strSQL As String = "SELECT  b.shkb as MA_DM, b.ten as TEN_DM " & _
                                   "FROM TCS_DM_KHOBAC b  WHERE SHKB like '%" & strMaDanhMuc & "' " & _
             " and UPPER(TEN) like '%" & strTenDanhMuc.ToUpper() & "%' and TINH_TRANG='1' ORDER BY SHKB"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ShowDM_NV(ByVal strMaDanhMuc As String, ByVal strTenDanhMuc As String) As DataTable
        Try
            Dim strSQL As String = "SELECT  b.ten_dn as MA_DM, b.ten as TEN_DM " & _
                                   "FROM TCS_DM_NHANVIEN b  WHERE UPPER(b.ten_dn) like '%" & strMaDanhMuc.ToUpper & "%' " & _
             " and UPPER(b.TEN) like '%" & strTenDanhMuc.ToUpper() & "%' and b.TINH_TRANG='0' ORDER BY b.ten_dn"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Function ShowDM_DBHC_TINH(ByVal strMaXa As String, ByVal strTenXa As String) As DataTable
        Dim strSQL As String = ""
        Try

            strSQL = GetMaDBHC_TINH(strMaXa, strTenXa)
            If strSQL <> "" Then
                Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    Return dt
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function ShowDM_DBHC_HUYEN(ByVal strMaXa As String, ByVal strTenXa As String) As DataTable
        Dim strSQL As String = ""
        Try

            strSQL = GetMaDBHC_HUYEN(strMaXa, strTenXa)
            If strSQL <> "" Then
                Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
                If Not Globals.IsNullOrEmpty(dt) Then
                    Return dt
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'sonmt
    Private Function ShowDM_LOAI_NNT(ByVal strMa As String, ByVal strTen As String) As DataTable
        Try
            Dim strSQL As String = ""
            strSQL = " SELECT MA_LOAI_NNT as MA_DM, TEN_LOAI_NNT as TEN_DM FROM TCS_DM_LOAI_NNT " & _
                     " WHERE MA_LOAI_NNT like '%" & strMa & "%' " & _
                           " AND UPPER(TEN_LOAI_NNT) like '%" & strTen.ToUpper() & "%' " & _
                           " ORDER BY MA_LOAI_NNT "
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                Return dt
            End If
        Catch ex As Exception

        End Try
    End Function
#End Region

#Region "Load_DM_NNT"
    Private Function Load_DM_NNT(ByVal strInput As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy dữ danh mục NNThuế từ danh mục
        ' Tham số: 
        ' Giá trị trả về:
        ' Ngày viết: 12/11/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------
        Dim strWhere As String = ""
        strWhere = strWhere & " where (MA_NNT like '" & strInput & "%') "
        Dim strSQL As String
        Try
            strSQL = "select ma_nnt as ID, ten_nnt from " & _
                    "(select ma_nnt, ten_nnt from tcs_dm_nnt " & strWhere & _
                    "union " & _
                    "select ma_nnt, ten_nnt from tcs_sothue) dm_nnt " & strWhere & _
                    "order by ma_nnt "
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function Load_DM_NNT_VL(ByVal strInput As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy dữ danh mục NNThuế từ danh mục vãng lai
        ' Tham số: 
        ' Giá trị trả về:
        ' Ngày viết: 12/11/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------
        Dim strWhere As String = ""
        strWhere = strWhere & " where (MA_NNT like '" & strInput & "%') "
        Dim strSQL As String
        Try
            strSQL = "Select distinct MA_NNT as ID, TEN_NNT as Title " & _
                "From TCS_DM_NNT_VL " & _
                strWhere & _
                "Order By MA_NNT"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function Load_DM_NNT_ToKhaiHQ(ByVal strInput As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy dữ danh mục NNThuế từ tờ khai Hải Quan
        ' Tham số: 
        ' Giá trị trả về:
        ' Ngày viết: 12/11/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------
        Dim strWhere As String = ""

        strWhere = strWhere & " where (MA_NNT like '" & strInput & "%') "
        Dim strSQL As String
        Try
            strSQL = "Select distinct MA_NNT as ID, TEN_NNT as Title " & _
                "From TCS_DS_TOKHAI " & _
                strWhere & _
                "Order By MA_NNT"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function Load_DM_NNT_TruocBa(ByVal strInput As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy dữ danh mục NNThuế từ file trước bạ
        ' Tham số: 
        ' Giá trị trả về:
        ' Ngày viết: 12/11/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------
        Dim strWhere As String = ""
        strWhere = strWhere & " where (MADTNT like '" & strInput & "%') "
        Dim strSQL As String
        Try
            strSQL = "Select distinct MADTNT as ID, TENDTNT as Title " & _
                " From TCS_TRUOCBA " & _
                strWhere & _
                " Order By MADTNT"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

End Class



