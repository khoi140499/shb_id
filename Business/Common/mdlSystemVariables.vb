﻿Option Explicit On 
Option Strict Off

Namespace Common
    Public Class mdlSystemVariables
        Public Shared gc_DEFAULT_FORMAT_DATE As String = "dd/MM/yyyy"
        '********************************************************************************
        'Nhóm các biến hệ thống dùng cho tham số Database
        'Public Shared [Global] As New Global_class
        Public Shared gstrDB_Source As String = ""
        Public Shared gstrDB_UserName As String = ""
        Public Shared gstrDB_Password As String = ""
        '********************************************************************************
        'Nhóm các biến hệ thống dùng cho báo cáo
        Public Shared gstrDanhMuc As String = ""
        Public Shared strThongKe As String = ""
        Public Shared strChonNgay As String = ""
        'Public Shared gstrLoaiBaoCao As String
        'Public Shared gLoaiBaoCao As mdlCommon.Report_Type
        Public Shared mblnPrint As Boolean = False
        Public Shared mblnEditBL As Boolean = False
        Public Shared mblnUpdateBL As Boolean = False
        '********************************************************************************
        'Nhóm các biến hệ thống dùng để lưu các giá trị lấy từ bảng DTU_THAMSO
        'Public Shared gintKS_KTKB As Integer = 0  'Biến lưu cho phép gom hay không gom chứng từ
        'Public Shared gstrMaDBHC As String = ""         ' Mã địa bàn hành chính
        'Public Shared gstrMaTTP As String = ""      'Biến lưu mã tỉnh, TP của điểm thu.
        'Public Shared gstrMaQH As String = ""       'Biến lưu mã quận, huyện của ĐThu.
        'Public Shared gstrDBThu As String = ""      'Biến lưu danh sách địa bàn thu, ngăn cách bởi dấu ;
        'Public Shared gstrSHKB As String = ""       'Số hiệu kho bạc
        'Public Shared gstrTenKB As String = ""      'Biến lưu tên Kho bạc.
        'Public Shared gstrMaCQThue As String = ""   'Biến lưu mã Cơ quan Thuế tương ứng của KB.
        'Public Shared gstrTenTinhTP As String = ""  'Biến lưu tên tỉnh, TP.
        Public Shared gstrExportPath As String = "" 'Biến lưu đường dẫn kết xuất DL cuối ngày.
        Public Shared gstrFTPExpPath As String = "" 'Biến lưu đường dẫn FTP kết xuất DL cuối ngày.
        Public Shared gstrImportPath As String = "" 'Biến lưu đường dẫn nhận DL của cơ quan Thuế gửi.
        Public Shared gstrFTPImpPath As String = "" 'Biến lưu đường dẫn FTP nhận DL của cơ quan Thuế gửi.
        Public Shared gstrBackupPath As String = "" 'Biến lưu đường dẫn sao lưu và khôi phục DL.
        Public Shared gblnKT_MaTIN As Boolean = False 'Biến tuỳ chọn có kiểm tra hợp lệ mã ĐTNT khi lập GNT hay không.
        Public Shared gbytMauGNT As Byte = 0      'Biến lưu loại GNT (mẫu cũ hay mới)
        Public Shared gbytUutien As Byte = 0      'Biến lưu thứ tự thực hiện lấy dữ liệu: 1 Ưu tiên lấy tbạ, 2 lấy tờ khai, 3 lấy dm nnt
        Public Shared gstrTB_Path As String = ""    'Biến lưu đường dẫn của file của chương trình thu trước bạ.
        Public Shared gstrFTPTB_Path As String = "" 'Biến lưu đường dẫn FTP của file của chương trình thu trước bạ.
        Public Shared gstrKyThue As String = ""     'Biến lưu kỳ thuế hiện tại.
        ' Public Shared gdtmNgayLV As Long = 0      'Biến lưu ngày làm việc hiện tại của điểm thu.
        Public Shared gdtmNgayNH As Long = 0      'Biến lưu ngày làm việc hiện tại của điểm thu.
        Public Shared gbytSoBanLV As Byte = 0    'Biến lưu số bàn làm việc của điểm thu

        'Public Shared gstrMaDiemThu As String = ""  'Biến lưu mã số của điểm thu.
        'Public Shared gstrTenDiemThu As String = "" 'Biến lưu mã số của điểm thu.
        Public Shared gdblSaiThucThu As Double 'Biến lưu số chênh lệch thực thu tối đa cho phép.
        Public Shared gdblA5_Width As Double = 0  'Biến lưu Width của khổ giấy A5
        Public Shared gdblA5_Height As Double = 0 'Biến lưu Height của khổ giấy A5
        Public Shared gdblA5_Top As Double = 0    'Biến lưu Top của khổ giấy A5
        Public Shared gdblA5_Left As Double = 0   'Biến lưu Left của khổ giấy A5
        'Public Shared gstrMaNHKB As String = ""     'Biến lưu mã ngân hàng của kho bạc trong bảng ngân hàng
        Public Shared gstrPassKTKB As String = ""   'Biến lưu password truy cập user TDTT trong KTKB 
        Public Shared gintSoBTCT As Integer = 0     'Số bút toán chứng từ
        Public Shared gintSoBTDC As Integer = 0     'Số bút toán chứng từ dieu chinh
        Public Shared gdtmNgayDC As Integer = 0    ' Bien luu ngay dieu chinh
        Public Shared gblnNhapPLT As Boolean = True ' Bien cho phep nhap theo phan loai tien
        Public Shared gblnTTBDS As String = ""
        '********************************************************************************
        'Nhóm các biến hệ thống dùng để lưu các giá trị lấy từ file Initial.xml
        Public Shared gstrTenND As String = ""      'Biến lưu username của lần login trước.
        Public Shared gstrMaNhom As String = ""     'Biến lưu mã nhóm của user login lần trước.
        Public Shared gbytBanLV As Byte = 0       'Biến lưu Bàn làm việc được chọn ở lần login trước.

        Public Shared gstrSoCT As String = "0"       'Biến lưu số bắt đầu chứng từ hiện tại của bàn thu.
        Public Shared gstrKHBL As String = ""       'Biến lưu ký hiệu biên lai hiện tại của bàn thu.
        Public Shared gstrSoBL As String = "0"       'Biến lưu số bắt đầu biên lai hiện tại của bàn thu.    
        'Public Shared gbytKSCT As Byte = 0        'Biến cho phép kiểm soát chứng từ hay không: 0 - không kiểm soát, 1 - có kiểm soát
        Public Shared gstrKT38i As String = "KT38i" ' Biến lưu tên của database KTKB
        '********************************************************************************
        'Các biến khác
        Public Shared gstrHoTenND As String = ""    ' Họ tên người đăng nhập chương trình
        Public Shared gstrMatKhau As String = ""    ' Mật khẩu người đăng nhập chương trình
        Public Shared gstrChucDanhND As String = "" ' Chức danh người dùng cuongnb them
        Public Shared gstrTen_KTT As String = ""    ' Tên kế toán trưởng
        Public Shared gblnKTKB As Boolean = False 'Tham số xác định có kiểm soát, hủy chứng từ sang KTKB không?


        Public Shared gblnInPhoi As Boolean = True      ' In chứng từ ra phôi
        Public Shared gintMauInPhoi As Integer = 0      ' Biến lưu mẫu in phôi
        Public Shared gintMauInCTUA4 As Integer = 0     ' Biến lưu mẫu in chứng từ trên A4
        Public Shared gintMauInKimBLT As Integer = 1  ' Biến lưu mẫu in kim biên lai thu trên A5 (Letter) = Statement

        '********************************************************************************
        'Các biến lưu loại thuế
        Public Shared gstrLoaithue As String = ""  ' Biến lưu mã loại thuế
        Public Shared garrLoaithue() As String     ' Mảng lưu mã loại thuế
        Public Shared garrLT_Chitieu() As String   ' Mảng chứa tiêu chí phân biệt loại thuế
        '********************************************************************************
        ' Các biến lưu Cấp - Chương - Loại - Khoản của ĐTNT nằm trong danh mục hệ thống
        Public Shared gstrMaCap As String = ""      ' Biến lưu mã  cấp
        Public Shared gstrMaChuong As String = ""   ' Biến lưu mã chương
        Public Shared gstrMaLoai As String = ""     ' Biến lưu mã loại
        Public Shared gstrMaKhoan As String = ""    ' Biến lưu mã khoản
        '********************************************************************************
        ' Mảng phân quyền menu, button      '
        Public Shared garrTenMenu() As String      ' Màng chứa tên menu các chức năng được phân quyền mặc định
        Public Shared garrTenButton() As String    ' Tên button các chức năng được phân quyền mặc định
        Public Shared gintSoCN As Integer          ' Tổng số chức năng được phân quyền mặc định
        '********************************************************************************
        ' Các biến phục vu trao đổi CSDL trung gian (truyền tin)
        Public Shared gstrMaNoiLamViec As String = ""  ' Biến lưu mã nơi làm việc
        Public Shared gstrTenNoiLamViec As String = "" ' Biến lưu tên nơi làm việc
        Public Shared gblnLoiDetail As Boolean = False ' Biến cờ lưu trạng thái dữ liệu trong một giao dịch có bị lỗi hay không
        'Public Shared gDB_Type = [Global].DbType         ' Biến lưu kiểu Database dùng để convert dữ liệu
        Public Shared gstrSSG As String = "10"         ' Sẵn sàng gửi(10)
        Public Shared gstrSSN As String = "30"         ' Sẵn sàng nhận(30)
        Public Shared gstrTND As String = "30"         ' Tác nghiệp đổ(30)
        Public Shared gstrDN As String = "50"          ' Đã nhận(50)
        Public Shared gstrVerTDTT As String = ""       ' Biến mang giá trị phiên bản của Module Trao đổi thông tin
        Public Shared gstrDateTDTT As String = ""      ' Biến mang giá ngày cập nhật của Module Trao đổi thông tin
        '********************************************************************************
        ' Các biến phục vụ thông báo lỗi Exception
        Public Shared gstrLoiKetNoiCSDL As String = "Lỗi kết nối cơ sở dữ liệu !"
        Public Shared gstrLoiThaoTacCSDL As String = "Lỗi thao tác cơ sở dữ liệu !"
        Public Shared gstrLoiThaoTacNSD As String = "Lỗi thao tác người sử dụng !"
        Public Shared gstrLoiThaoTacThucHien As String = "Lỗi thao tác vừa thực hiện !"
        Public Shared gstrLoiXoaDuLieu As String = "Xoá dữ liệu không thành công !"
        Public Shared gstrLoiCapNhatDL As String = "Lỗi không thể cập nhật dữ liệu !"
        '********************************************************************************
        ' Các biến Lưu loại thuế
        Public Shared gstrLT_TB_NhaDat As String = "02"
        Public Shared gstrLT_TB_XeMay As String = "03"
        Public Shared gstrLT_HaiQuan As String = "04"
        '********************************************************************************
        ' Các biến bên ngân hàng ICB
        'Public Shared gstrPTTT As String = "" 'ICB- Phuong thuc thanh toan: tien mat hay chuyen khoan
        Public Shared gintSoLienInCT As Integer = 1     ' Biến lưu số liên in chứng từ
        Public Shared gintSoLienInBL As Integer = 1     ' Biến lưu số liên in biên lai thu
        ''Public Shared gstrKHCT As String = ""       'Biến lưu ký hiệu chứng từ hiện tại của bàn thu.
        Public Shared gstrTK_TM_NH As String = "" ' Tài khoản tiền mặt của Ngân Hàng
        Public Shared gstrTK_CK_NH As String = "" ' Tài khoản chuyển khoản của Ngân Hàng(thu chi hộ kho bạc)
        Public Shared gstrTenNH As String = "" ' Tên ngân Hàng

        ' Các biến mẫu chứng từ
        Public Shared gstrMAP_LTSTTM As String = ",30,31,"                                ' Chứng từ GNT tiền mặt
        Public Shared gstrMAP_LTSTCK As String = ",22,23,24,25,26,30,31,"                ' Chứng từ GNT chuyển khoản
        Public Shared gstrMAP_TTTGTM As String = ",16,17,18,30,31,"                      ' Tạm thu tạm giữ tiền mặt
        Public Shared gstrMAP_TTTGCK As String = ",16,17,18,22,23,24,25,26,30,31,"                ' Tạm thu tạm giữ chuyển khoản
        Public Shared gstrMAP_CTTC As String = ",16,17,18,19,30,31,"                     ' Chứng từ ghi thu ghi chi
        Public Shared gstrMAP_CTVATTMCK As String = ",16,17,18,22,24,25,26,27,28,29,30,31," ' Thuế VAT chuyển khoản
        Public Shared gstrMAP_DC As String = ",30,31,"

        'Ngan hang Bidv

        Public Shared gstrPT_TT_TM As String = "01"
        Public Shared gc_BLANK_CHARACTER As String = " "
        Public Shared gsMode As String = ""
    End Class
End Namespace