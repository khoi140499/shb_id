﻿Imports System.Configuration
Imports System.DirectoryServices
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports Business
Imports System.Data
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Net

Public Class KetNoi_LDAP
    Private Shared _path As String = ConfigurationManager.AppSettings.Get("LDAP_PATH").ToString()
    Private Shared _filterAttribute As String
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public Shared Function IsAuthenticated(ByVal username As String, ByVal userdomain As String, ByVal pwd As String) As Boolean
        Dim domainAndUsername As String = userdomain + "\" + username
        'LogDebug.WriteLog("Path: " & _path & "userdomain: " & domainAndUsername & "pass: " & pwd, EventLogEntryType.Information)
        Dim entry As New DirectoryEntry(_path, domainAndUsername, pwd, AuthenticationTypes.None)
        Dim result As Boolean = True
        Try
            Dim obj As Object = entry.NativeObject

            'Dim search As New DirectorySearcher(entry)
            ' LogDebug.WriteLog(
            'search.Filter = "(SAMAccountName=" + username + ")"
            'search.PropertiesToLoad.Add("cn")
            'Dim result As SearchResult = search.FindOne()

            'If result Is Nothing Then
            '    'Throw New Exception("Error authenticating user.")
            '    Return False
            'End If

        Catch ex As Exception
            '  LogDebug.WriteLog("AuthenticateUserLDAP : " & userdomain & username & " pass " & pwd & vbNewLine _
            '& "Error code: System error!" & vbNewLine _
            '& "Error message: " & ex.Message, EventLogEntryType.Error)
            log.Error(ex.Message & "-" & ex.StackTrace) '
            ' LogDebug.WriteLog("AuthenticateUserLDAP : " & ex.Source & vbNewLine _
            '& "Error code: System error!" & vbNewLine _
            '& "Error message: " & ex.Message, EventLogEntryType.Error)

            Return False
        End Try

        Return True
        'Return AuthenticateUser(_path, domainAndUsername, pwd)
    End Function
    Private Shared Function AuthenticateUser(ByVal path As String, ByVal user As String, ByVal pass As String) As Boolean
        'LogDebug.WriteLog("Path: " & path & "user: " & user & "pass: " & pass, EventLogEntryType.Information)
        '  log.Info("Path: " & path & "user: " & user & "pass: " & pass)
        Dim dirEntry As New DirectoryEntry(path, user, pass)

        Try

            Dim nat As Object = Nothing

            nat = dirEntry.NativeObject

            Return True

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            ' LogDebug.WriteLog("AuthenticateUserLDAP : " & ex.Source & vbNewLine _
            '& "Error code: System error!" & vbNewLine _
            '& "Error message: " & ex.Message, EventLogEntryType.Error)
            Return False

        End Try

    End Function
    Public Shared Function CheckPassWord(ByVal username As String, ByVal domain As String, ByVal pwd As String) As Boolean
        Dim cnUser As DataAccess
        Dim transCT As IDbTransaction
        Dim strLog_DN As String = ""
        cnUser = New DataAccess
        Dim b_result As Boolean = True
        Try
            Dim strSQL_LDAP As String
            Dim strUserDomain As String = ""

            'Kiem tra LDAP hay ko
            strSQL_LDAP = "select * from tcs_dm_nhanvien where UPPER (ten_dn) = UPPER ('" & username & "')"
            Dim dt_LDAP As New DataTable
            dt_LDAP = DataAccess.ExecuteToTable(strSQL_LDAP)
            'Ket thuc
            If Not dt_LDAP Is Nothing And dt_LDAP.Rows.Count > 0 Then
                If dt_LDAP.Rows(0)("LDAP").ToString() = "0" Then
                    strUserDomain = dt_LDAP.Rows(0)("USERDOMAIN").ToString()
                    'CHECK LDAP
                    If username.Equals("admin") Then
                        If Not pwd.Equals(DescryptStr(dt_LDAP.Rows(0)("MAT_KHAU"))) Then
                            b_result = False
                        End If
                    Else
                        Dim checkConnectLDAP As Boolean = IsAuthenticated(strUserDomain.ToUpper(), domain.ToUpper(), pwd)
                        If (checkConnectLDAP = False) Then
                            b_result = False
                        End If
                    End If
                    'END CHECK LDAP
                Else
                    If dt_LDAP.Rows(0)("MAT_KHAU").ToString() <> EncryptStr(pwd) Then
                        b_result = False
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
        Return b_result
    End Function
End Class
