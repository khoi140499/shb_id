Namespace KhoQuy
    Public Class infTienGiaHDR
        Private strSoBB As String
        Private intNgayBB As Integer
        Private strTenKH As String
        Private strDC_KH As String
        Private strSoCMND As String
        Private strTen_DDKB As String
        Private strChuc_Vu As String
        Private intMaTQ As Integer
        Public Sub New()

        End Sub
        Public Property So_BB() As String
            Get
                Return strSoBB
            End Get
            Set(ByVal Value As String)
                strSoBB = Value
            End Set
        End Property
        Public Property Ngay_BB() As Integer
            Get
                Return intNgayBB
            End Get
            Set(ByVal Value As Integer)
                intNgayBB = Value
            End Set
        End Property
        Public Property Ten_KH() As String
            Get
                Return strTenKH
            End Get
            Set(ByVal Value As String)
                strTenKH = Value
            End Set
        End Property
        Public Property DC_KH() As String
            Get
                Return strDC_KH
            End Get
            Set(ByVal Value As String)
                strDC_KH = Value
            End Set
        End Property
        Public Property Chuc_Vu() As String
            Get
                Return strChuc_Vu
            End Get
            Set(ByVal Value As String)
                strChuc_Vu = Value
            End Set
        End Property
        Public Property So_CMND() As String
            Get
                Return strSoCMND
            End Get
            Set(ByVal Value As String)
                strSoCMND = Value
            End Set
        End Property
        Public Property Ten_DDKB() As String
            Get
                Return strTen_DDKB
            End Get
            Set(ByVal Value As String)
                strTen_DDKB = Value
            End Set
        End Property
        Public Property Ma_TQ() As Integer
            Get
                Return intMaTQ
            End Get
            Set(ByVal Value As Integer)
                intMaTQ = Value
            End Set
        End Property
    End Class
    Public Class infTienGiaDTL
        Private strSoBB As String
        Private intNgayBB As Integer
        Private strMaLTien As String
        Private intSoTo As Integer
        Private intLoaiTien As Integer
        Private strSoSerial As String
        Private intRow As Integer
        Private strDacDiem As String
        Public Sub New()

        End Sub
        Public Property So_BB() As String
            Get
                Return strSoBB
            End Get
            Set(ByVal Value As String)
                strSoBB = Value
            End Set
        End Property
        Public Property Ngay_BB() As Integer
            Get
                Return intNgayBB
            End Get
            Set(ByVal Value As Integer)
                intNgayBB = Value
            End Set
        End Property
        Public Property Ma_LTien() As String
            Get
                Return strMaLTien
            End Get
            Set(ByVal Value As String)
                strMaLTien = Value
            End Set
        End Property
        Public Property So_To() As Integer
            Get
                Return intSoTo
            End Get
            Set(ByVal Value As Integer)
                intSoTo = Value
            End Set
        End Property
        Public Property So_Serial() As String
            Get
                Return strSoSerial
            End Get
            Set(ByVal Value As String)
                strSoSerial = Value
            End Set
        End Property
        Public Property LoaiTien() As Integer
            Get
                Return intLoaiTien
            End Get
            Set(ByVal Value As Integer)
                intLoaiTien = Value
            End Set
        End Property
        Public Property CurRow() As Integer
            Get
                Return intRow
            End Get
            Set(ByVal Value As Integer)
                intRow = Value
            End Set
        End Property
        Public Property DacDiem() As String
            Get
                Return strDacDiem
            End Get
            Set(ByVal Value As String)
                strDacDiem = Value
            End Set
        End Property
    End Class

End Namespace
