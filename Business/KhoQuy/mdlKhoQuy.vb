﻿Imports VBOracleLib
Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Imports Business.KhoQuy

Public Module mdlKhoQuy
    'Public  Function SuaThucThu(ByVal strKHCT As String, ByVal strSoCT As String, ByVal strLoaiDL As String) As Boolean
    '    '-----------------------------------------------------------------
    '    ' Mục đích      : Sửa thực thu
    '    ' Ngày viết     : 22/07/2008
    '    ' Người viết    : Lê Hồng Hà
    '    ' ----------------------------------------------------------------     
    '    Try
    '        Dim i As Integer
    '        Dim frmSuaTT As New frmSuaSoThucThu

    '        ' Kiểm tra CT gộp
    '        Dim bytGroup As Byte = 0
    '        'Lay Key cua nhom
    '        Dim keyGroup As KeyKhoQuy    ' Key của nhóm
    '        Dim keyCT As KeyKhoQuy  ' Key của chứng từ
    '        keyGroup = buTraCuuTThu.checkCT_KQ(strKHCT, strSoCT, bytGroup)
    '        keyCT = buTraCuuTThu.GetKeys(strKHCT, strSoCT, strLoaiDL)
    '        Dim dblTTien As Double
    '        dblTTien = buSuaThucThu.Get_TT(keyCT, strLoaiDL)
    '        If bytGroup = 2 Then
    '            dblTTien = buSuaThucThu.Get_TT(keyGroup, strLoaiDL)
    '            frmSuaTT.frmload(keyCT, strLoaiDL, strKHCT, strSoCT, keyGroup, bytGroup, dblTTien)

    '            If (frmSuaTT.ShowDialog() = DialogResult.OK) Then
    '                Return True
    '            Else
    '                Return False
    '            End If
    '        Else
    '            If keyGroup Is Nothing Then
    '                keyGroup = keyCT
    '            End If
    '            frmSuaTT.frmload(keyCT, strLoaiDL, strKHCT, strSoCT, keyGroup, bytGroup, dblTTien)

    '            If (frmSuaTT.ShowDialog() = DialogResult.OK) Then
    '                Return True
    '            Else
    '                Return False
    '            End If
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        'LogDebug.Writelog("Lỗi sửa số thực thu cmdSua_Click : " & ex.ToString())
    '        MsgBox("Lỗi trong quá trình sửa số thực thu.", MsgBoxStyle.Critical, "Chú ý")
    '    End Try
    'End Function

    Public Function XoaThucThu(ByVal strKHCT As String, ByVal strSoCT As String, ByVal strLoaiDL As String) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích      : Xóa thực thu
        ' Ngày viết     : 22/07/2008
        ' Người viết    : Lê Hồng Hà
        ' ----------------------------------------------------------------      
        Try
            ' Kiểm tra CT gộp
            Dim bytGroup As Byte = 0
            'Lay Key cua nhom
            Dim keyGroup As KeyKhoQuy ' Key của nhóm
            Dim keyCT As KeyKhoQuy    ' Key của chứng từ
            keyGroup = buTraCuuTThu.checkCT_KQ(strKHCT, strSoCT, bytGroup)
            keyCT = buTraCuuTThu.GetKeys(strKHCT, strSoCT, strLoaiDL)
            If bytGroup = 2 Then
                If MsgBox("Chứng từ này đã được gộp chung nhóm với các chứng từ khác." & Chr(13) & _
                          "       Bạn sẽ xóa các mệnh giá tiền của cả nhóm?", MsgBoxStyle.YesNo, "Thông báo!") = MsgBoxResult.Yes Then
                    ' Phải cập nhật lại toàn bộ chứng từ trong nhóm về trạng thái chưa nhập số thực thu.
                    buTraCuuTThu.Xoa_CT_Group(keyGroup, strKHCT, strSoCT, strLoaiDL)
                Else
                    Return False
                End If
            Else
                ' Không phải gộp theo nhóm khác
                buTraCuuTThu.Xoa_CT(keyCT, strLoaiDL)
            End If

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Module
