﻿Imports VBOracleLib
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.Common.mdlCommon
Imports GlobalProjects


Public Class daThucThu
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    '-----------------------------------------------------------------
    ' Mục đích      : Cung cấp lớp cho phép kết nối, thao tác trực tiếp lên database
    ' Tham số       : 
    ' Giá trị trả về: 
    ' Ngày viết     : 15/10/2007
    ' Người viết    : Lê anh Ngọc
    ' ----------------------------------------------------------------
    '*****************************************************************
    Public Function SelectDMloaitien() As DataSet
        '-----------------------------------------------------------------
        ' Mục đích      : Lấy danh mục các loại tiền
        ' Tham số       : 
        ' Giá trị trả về: 
        ' Ngày viết     : 15/10/2007
        ' Người viết    : Lê anh Ngọc
        '**********************************************
        Dim cnQLoaiTien As DataAccess
        Dim ds As DataSet
        Dim strSQL As String

        Try
            strSQL = "Select MA_LOAITIEN,MENH_GIA " & _
                 "From TCS_DM_LOAITIEN " & _
                 "Order by MENH_GIA Desc"

            cnQLoaiTien = New DataAccess
            ds = cnQLoaiTien.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy danh mục tiền")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách loại tiền: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
            If Not cnQLoaiTien Is Nothing Then
                cnQLoaiTien.Dispose()
            End If
        End Try
    End Function
    Public Function SelectDsBLThu(ByVal sqlstr As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích      : Lấy danh sách biên lai thu
        ' Tham số       : 
        ' Giá trị trả về: 
        ' Ngày viết     : 15/10/2007
        ' Người viết    : Lê anh Ngọc
        '*************************************
        Dim conn As DataAccess
        Dim ds As DataSet
        Try
            conn = New DataAccess
            ds = conn.ExecuteReturnDataSet(sqlstr, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách biên lai thu")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách biên lai thu: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function
    Public Function Chungtu(ByVal CT As KeyKhoQuy) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích      : Kiểm tra chứng từ này đã có người nhập số thực thu chưa
        ' Tham số       : 
        ' Giá trị trả về: 
        ' Ngày viết     : 15/10/2007
        ' Người viết    : Lê anh Ngọc
        '******************************************************************
        Dim conn As DataAccess
        Dim dr As IDataReader
        Dim sql As String
        sql = "Select 1 from TCS_CTU_HDR where SHKB='" & CT.SHKB & _
              "' And Ngay_KB=" & CT.Ngay_KB.ToString & " And Ma_NV=" & CT.Ma_NV.ToString & " and So_BT=" & _
              CT.So_BT.ToString & " and Ma_Dthu='" & CT.Ma_Dthu & "' and (not TT_Tthu>0)"
        Try
            conn = New DataAccess
            dr = conn.ExecuteDataReader(sql, CommandType.Text)
            Dim KQ As Byte = 0
            If dr.Read Then
                KQ = dr.GetValue(0)
            End If
            If KQ = 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy dữ liệu chứng từ")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu chứng từ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function
    Public Function Bienlai(ByVal CT As KeyKhoQuy) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích      : Kiểm tra biên lai này đã có người nhập số thực thu chưa
        ' Tham số       : 
        ' Giá trị trả về: 
        ' Ngày viết     : 15/10/2007
        ' Người viết    : Lê anh Ngọc
        '******************************************************************
        Dim conn As DataAccess
        Dim dr As IDataReader
        Dim sql As String
        sql = "Select 1 form TCS_CTBL where SHKB='" & CT.SHKB & "' and Ngay_KB=" & _
              CT.Ngay_KB.ToString & " and Ma_NV=" & CT.Ma_NV.ToString & " and So_BT=" & _
              CT.So_BT.ToString & " and Ma_Dthu='" & CT.Ma_Dthu & "'"
        Try
            conn = New DataAccess
            dr = conn.ExecuteDataReader(sql, CommandType.Text)
            Dim KQ As Byte = 0
            If dr.Read Then
                KQ = dr.GetValue(0)
            End If
            If KQ = 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy biên lai")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy biên lai: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function

    Public Function Update_TrangThai(ByVal CT As KeyKhoQuy, ByVal obj As TrangThai, ByVal Tongtien As Double) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích      : Cập nhật trạng thái, thu quy thuc hien, của chứng từ, biên lai
        ' Tham số       : 
        ' Giá trị trả về: 
        ' Ngày viết     : 15/10/2007
        ' Người viết    : Lê anh Ngọc
        '******************************************************************
        Dim conn As DataAccess
        Dim sql As String = ""

        Try
            If obj.CT_BL = "CT" Then
                sql = "Update TCS_CTU_HDR set Ma_TQ=" & obj.Ma_TQ & ", TT_Tthu=" & Tongtien & " where SHKB='" & CT.SHKB & _
                      "' and Ngay_KB=" & CT.Ngay_KB.ToString & " and Ma_NV=" & CT.Ma_NV.ToString & " and So_BT=" & _
                      CT.So_BT.ToString & " and Ma_Dthu='" & CT.Ma_Dthu & "'"
            End If
            If obj.CT_BL = "BL" Then
                sql = "Update TCS_CTBL set Ma_TQ=" & obj.Ma_TQ & ", TT_Tthu=" & Tongtien & " where SHKB='" & CT.SHKB & _
                      "' and Ngay_KB=" & CT.Ngay_KB.ToString & " and Ma_NV=" & CT.Ma_NV.ToString & " and So_BT=" & _
                      CT.So_BT.ToString & " and Ma_Dthu='" & CT.Ma_Dthu & "'"
            End If

            conn = New DataAccess
            If Not sql = "" Then
                conn.ExecuteNonQuery(sql, CommandType.Text)
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật trạng thái")
            'LogDebug.WriteLog("Lỗi trong quá trình cập nhật trạng thái: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function

    ' Ham ghi vao kho quy và ghi tổng số tiền thực thu được của mỗi chứng từ
    Public Function Ghi_DL_KhoQuy(ByVal objKQ As KeyKhoQuy, ByVal obj As TrangThai, ByVal Tien() As infLoaiTien, _
                                    ByVal TongTienTT As Double, ByVal CT As KeyKhoQuy, ByVal bln As Boolean) As Boolean
        '-----------------------------------------------------------------
        ' Mục đích      : Ghi vào bảng kho quỹ dlt các mệnh giá tiền
        ' Tham số       : 
        ' Giá trị trả về: 
        ' Ngày viết     : 24/10/2007
        ' Người viết    : Lê anh Ngọc
        '*****************************************************
        Dim conn As DataAccess
        Dim trans As IDbTransaction
        Dim SQL As String
        Dim blnResult As Boolean = False

        Try
            '---------- cap nhat trang thai và ghi số tiền thực thu--------------
            If obj.CT_BL = "CT" Then
                SQL = "Update TCS_CTU_HDR set Ma_TQ=" & obj.Ma_TQ & ", TT_Tthu=" & TongTienTT & _
                      " where SHKB='" & CT.SHKB & "' and Ngay_KB=" & CT.Ngay_KB.ToString & _
                      " and Ma_NV=" & CT.Ma_NV.ToString & " and So_BT=" & CT.So_BT.ToString & _
                      " and Ma_Dthu='" & CT.Ma_Dthu & "'"
            End If
            If obj.CT_BL = "BL" Then
                SQL = "Update TCS_CTBL set Ma_TQ=" & obj.Ma_TQ & ", TT_Tthu=" & TongTienTT & _
                      " where SHKB='" & CT.SHKB & "' and Ngay_KB=" & CT.Ngay_KB.ToString & _
                      " and Ma_NV=" & CT.Ma_NV.ToString & " and So_BT=" & CT.So_BT.ToString & _
                      " and Ma_Dthu='" & CT.Ma_Dthu & "'"
            End If

            conn = New DataAccess
            trans = conn.BeginTransaction
            ' Thực hiện cập nhật trạng thái
            conn.ExecuteNonQuery(SQL, CommandType.Text, trans)


            'ICB: chú ý trước khi ghi mới thì phải check xem có tồn tại row nào trong bảng TCS_KHOQUY_HDR chưa?
            ' nếu có thì xóa bỏ rồi mới ghi mới 
            'Bởi vì: trường hợp thanh toán viên cập nhật lại trạng thái PT_TT của chứng từ PT_TT=chuyển khoản --> PT_TT=Tiền Mặt
            'Như thế sẽ bắt nhập lại số thực thu mà trong bảng TCS_KHOQUY_HDR lúc này đã tồn tại

            '### sẽ sửa sau

            '---------------Lưu kho-----------------------
            If bln = True Then
                ' Thuc hien insert HDR
                SQL = "INSERT INTO TCS_KHOQUY_HDR(SHKB,Ngay_KB,Ma_NV,So_BT,Ma_Dthu) values ('" & objKQ.SHKB & "'," & objKQ.Ngay_KB & "," & objKQ.Ma_NV & _
                  "," & objKQ.So_BT & ",'" & objKQ.Ma_Dthu & "')"
                conn.ExecuteNonQuery(SQL, CommandType.Text, trans)
            End If


            ' Thuc hien insert Dtl
            For Each Tn As Business.infLoaiTien In Tien
                If bln Then ' Nếu nhập gộp - chỉ ghi số tờ lên chứng từ đầu tiên
                    SQL = "Insert Into TCS_KHOQUY_DTL(SHKB,Ngay_KB,Ma_NV,So_BT,Ma_Dthu,Kyhieu_CT,So_CT,Ma_LoaiTien,So_to) values('" & objKQ.SHKB & "'," & objKQ.Ngay_KB & _
                          "," & objKQ.Ma_NV & "," & objKQ.So_BT & ",'" & objKQ.Ma_Dthu & _
                          "','" & objKQ.Kyhieu_CT & "','" & objKQ.So_CT & "','" & getMaLoaiTien(Tn.MenhGia) & _
                          "'," & Tn.SoTo & ")"
                Else ' Sau đó ghi số tờ cho các chứng từ khác = 0
                    SQL = "Insert Into TCS_KHOQUY_DTL(SHKB,Ngay_KB,Ma_NV,So_BT,Ma_Dthu,Kyhieu_CT,So_CT,Ma_LoaiTien,So_to) values('" & objKQ.SHKB & "'," & objKQ.Ngay_KB & _
                          "," & objKQ.Ma_NV & "," & objKQ.So_BT & ",'" & objKQ.Ma_Dthu & _
                          "','" & objKQ.Kyhieu_CT & "','" & objKQ.So_CT & "','" & getMaLoaiTien(Tn.MenhGia) & _
                          "'," & 0 & ")"
                End If
                conn.ExecuteNonQuery(SQL, CommandType.Text, trans)
            Next
            trans.Commit()
            blnResult = True
            Return blnResult
        Catch ex As Exception
            trans.Rollback()
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình thực hiện ghi DL kho quỹ")
            'LogDebug.WriteLog("Lỗi trong quá trình thực hiện ghi DL kho quỹ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not trans Is Nothing Then
                trans.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function

    'Lấy mã các loại tiền
    Private Function getMaLoaiTien(ByVal menhgia As Int64) As String
        '-----------------------------------------------------------------
        ' Mục đích      : Lấy mã loại tiền 
        ' Tham số       : Mệnh giá
        ' Giá trị trả về: Mã tiền
        ' Ngày viết     : 24/10/2007
        ' Người viết    : Lê anh Ngọc
        '**************************************
        Dim conn As DataAccess
        Dim dr As IDataReader
        Dim strResult As String
        Try
            conn = New DataAccess
            Dim sql As String = ""
            sql = "select Ma_LoaiTien from  TCS_DM_LOAITIEN where Menh_Gia=" & menhgia
            dr = conn.ExecuteDataReader(sql, CommandType.Text)
            If dr.Read Then
                strResult = dr.GetValue(0)
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy mã các loại tiền")
            Throw ex
            'LogDebug.WriteLog("Lỗi trong quá trình lấy mã các loại tiền: " & ex.ToString, EventLogEntryType.Error)
            Return ""
        Finally
            If Not dr Is Nothing Then
                dr.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If

        End Try
        Return strResult
    End Function

    ''' <summary>
    ''' Lấy danh sách các thủ quỹ theo bàn làm việc của họ
    ''' 
    ''' </summary>
    ''' <param name="pv_strBanLV"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Get_DSTQ(ByVal pv_strBanLV As String) As Object
        Dim conn As DataAccess
        Dim ds As New DataSet
        Dim sql As String
        Try
            conn = New DataAccess
            sql = "Select Ma_NV, (lpad(Ma_NV, 3, '0') || '-' || Ten) as Ten  from TCS_DM_NHANVIEN  where Tinh_Trang='0' and (Ma_Nhom like '%03%' or Ma_Nhom like '03%') " _
                        & " AND ban_lv=" & Globals.EscapeQuote(pv_strBanLV) _
                        & " order by Ma_NV"

            ds = conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách các thủ quỹ theo bàn làm việc của họ")
            Throw ex
        Finally
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
        End Try
    End Function

    ' Lấy danh sách thủ quỹ
    Public Function Get_DSTQ() As Object
        '-----------------------------------------------------------------
        ' Mục đích      : Lấy danh sách các thủ quỹ
        ' Tham số       : 
        ' Giá trị trả về: dataset chua bang thu quy
        ' Ngày viết     : 24/10/2007
        ' Người viết    : Lê anh Ngọc
        '**************************************
        Dim conn As DataAccess
        Dim ds As New DataSet
        Dim sql As String
        Try
            conn = New DataAccess
            sql = "Select Ma_NV, (lpad(Ma_NV, 3, '0') || '-' || Ten) as Ten  from TCS_DM_NHANVIEN  where Tinh_Trang='0' and (Ma_Nhom like '%03%' or Ma_Nhom like '03%') " _
                        & " AND ban_lv=" & Globals.EscapeQuote(Get_BanLV_NV(CInt(gstrTenND)).ToString()) _
                        & " order by Ma_NV"

            ds = conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách các thủ quỹ")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách thủ quỹ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
        End Try
    End Function
    'Public Function SelectDSCTTT()
    '    Dim conn As DataAccess
    '    Dim strSql As String
    '    Dim ds As New DataSet
    '    Dim gdtmNgayLV As String
    '    gdtmNgayLV = GetNgayLV(strMaNV)
    '    Try
    '        'SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')
    '        conn = New DataAccess
    '        strSql = "Select distinct ct.kyhieu_ct,ct.so_ct,ct.ma_nv,ct.so_bt From tcs_ctu_hdr ct,tcs_khoquy_hdr kq " & _
    '                     "Where ((ct.shkb=kq.shkb and ct.ngay_kb=kq.ngay_kb and ct.ma_nv=kq.ma_nv and ct.so_bt=kq.so_bt " & _
    '                     "And ct.ma_dthu=kq.ma_dthu) " & _
    '                     "Or (ct.TT_Tthu <>0 And ct.Ma_TQ<>0  AND ct.kyhieu_ct || ct.so_ct NOT IN (" & _
    '                     "SELECT DISTINCT dtl.kyhieu_ct || dtl.so_ct " & _
    '                     "FROM tcs_khoquy_dtl dtl " & _
    '                     "WHERE dtl.ngay_kb =" & gdtmNgayLV & "))) " & _
    '                     "And ct.ngay_kb=" & gdtmNgayLV & " Order by so_ct asc"
    '        ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
    '        Return ds
    '    Catch ex As Exception
    '        'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách chứng từ đã nhập số thực thu: " & ex.ToString)
    '        Throw ex
    '    End Try
    'End Function

    Public Function SelectDSCTTT(ByVal strMaNV As String)
        Dim conn As DataAccess
        Dim strSql As String
        Dim ds As New DataSet
        Dim gdtmNgayLV As String
        gdtmNgayLV = GetNgayLV(strMaNV)
        Try
            conn = New DataAccess
            'ICB-17/10/2008: Thêm điều kiện PT_TT=00 (tài khoản tiền mặt)
            'Sửa: Hoàng Văn Anh
            strSql = "Select distinct ct.kyhieu_ct,ct.so_ct,ct.ma_nv,ct.so_bt " & _
                       " From tcs_ctu_hdr ct,tcs_khoquy_hdr kq " & _
                       "Where ((ct.shkb=kq.shkb and ct.ngay_kb=kq.ngay_kb and ct.ma_nv=kq.ma_nv and ct.so_bt=kq.so_bt " & _
                       "And ct.ma_dthu=kq.ma_dthu) " & _
                       "Or (ct.TT_Tthu <>0 And ct.Ma_TQ<>0  AND ct.kyhieu_ct || ct.so_ct NOT IN (" & _
                       "SELECT DISTINCT dtl.kyhieu_ct || dtl.so_ct " & _
                       "FROM tcs_khoquy_dtl dtl " & _
                       "WHERE dtl.ngay_kb =" & gdtmNgayLV & "))) " & _
                       "And (ct.ngay_kb=" & gdtmNgayLV & ") and (ct.ma_TQ = " & strMaNV & ")" _
                       & " AND ( ct.pt_tt = '00') AND (ct.Trang_Thai='00')" _
                       & " Order by so_ct asc"
            ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách chứng từ đã nhập số thực thu")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách chứng từ đã nhập số thực thu: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        End Try
    End Function

    Public Function SelectDSBLTT(ByVal strMaNV As String)
        Dim conn As DataAccess
        Dim strSql As String
        Dim ds As New DataSet
        Dim gdtmNgayLV As String
        gdtmNgayLV = GetNgayLV(strMaNV)
        Try
            conn = New DataAccess
            strSql = "Select distinct ct.kyhieu_bl as kyhieu_ct,ct.so_bl as so_ct,ct.ma_nv,ct.so_bt From tcs_ctbl ct,tcs_khoquy_hdr kq " & _
                        "Where ((ct.shkb=kq.shkb and ct.ngay_kb=kq.ngay_kb and ct.ma_nv=kq.ma_nv and ct.so_bt=kq.so_bt " & _
                        "And ct.ma_dthu=kq.ma_dthu) " & _
                        "Or (ct.TT_Tthu <>0 And ct.Ma_TQ<>0  AND ct.kyhieu_bl || ct.so_bl NOT IN (" & _
                        "SELECT DISTINCT dtl.kyhieu_ct || dtl.so_ct " & _
                        "FROM tcs_khoquy_dtl dtl " & _
                        "WHERE (dtl.ngay_kb =" & gdtmNgayLV & ") and (dtl.ma_nv = " & strMaNV & ")))) " & _
                        "And (ct.ngay_kb=" & gdtmNgayLV & ") and (ct.ma_nv = " & strMaNV & ") Order by so_ct asc"
            ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)

            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách chứng từ đã nhập số thực thu")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách chứng từ đã nhập số thực thu: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        End Try
    End Function

End Class
