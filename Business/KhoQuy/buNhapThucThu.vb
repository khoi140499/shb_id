﻿Imports VBOracleLib
Namespace KhoQuy

    Public Class buThucThu
        '-----------------------------------------------------------------
        ' Mục đích      : Lớp thao tác các chức năng xử lý
        ' Tham số       : 
        ' Giá trị trả về: 
        ' Ngày viết     : 15/10/2007
        ' Người viết    : Lê anh Ngọc
        ' ----------------------------------------------------------------
        '*****************************************************************
        Public Shared Function SelectDMloaitien() As DataSet
            Dim objThucthu As New daThucThu
            Try
                Return objThucthu.SelectDMloaitien()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function selectDsBLThu(ByVal strSql As String) As DataSet
            Dim objBLThu As New daThucThu
            Try
                Return objBLThu.SelectDsBLThu(strSql)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function selectTQ() As DataSet
            Dim objds As New daThucThu
            Try
                Return objds.Get_DSTQ()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''Kienvt : Lấy danh sách thủ quỹ dựa theo bàn làm việc
        Public Shared Function selectTQ(ByVal pv_strBanLamViec As String) As DataSet
            Dim objds As New daThucThu
            Try
                Return objds.Get_DSTQ(pv_strBanLamViec)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Shared Function SelectDSCTTT() As DataSet
        '    Dim da As New daThucThu
        '    Return da.SelectDSCTTT()
        'End Function

        Public Shared Function SelectDSCTTT(ByVal strMaNV As String) As DataSet
            Dim da As New daThucThu
            Return da.SelectDSCTTT(strMaNV)
        End Function

        Public Shared Function SelectDSBLTT(ByVal strMaNV As String) As DataSet
            Dim da As New daThucThu
            Return da.SelectDSBLTT(strMaNV)
        End Function
    End Class

End Namespace