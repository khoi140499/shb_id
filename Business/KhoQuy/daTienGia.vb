﻿Imports VBOracleLib
Imports Business.Common.mdlSystemVariables
Imports Business.Common
Imports Business.Common.mdlCommon

Namespace KhoQuy

    Public Class daTienGia
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Sub Insert(ByVal objHdr As infTienGiaHDR, ByVal objDtl As infTienGiaDTL())
            Dim cnTG As DataAccess
            Dim strSql As String
            Dim trans As IDbTransaction
            Dim obj As infTienGiaDTL
            Try
                cnTG = New DataAccess
                trans = cnTG.BeginTransaction
                strSql = "Insert InTo tcs_tiengia_hdr (so_bb, ngay_bb, ten_kh, dchi_kh, socmnd, ten_ddkb,chuc_vu, ma_tq) " & _
                         "Values ('" & objHdr.So_BB & "'," & objHdr.Ngay_BB & ",'" & objHdr.Ten_KH & "','" & _
                         objHdr.DC_KH & "','" & objHdr.So_CMND & "','" & objHdr.Ten_DDKB & "','" & objHdr.Chuc_Vu & "'," & _
                         objHdr.Ma_TQ & ")"
                cnTG.ExecuteNonQuery(strSql, CommandType.Text, trans)
                For Each obj In objDtl
                    strSql = "Insert Into tcs_tiengia_dtl (so_bb,ngay_bb,ma_ltien,so_to,loai_tien,so_serial,dac_diem) " & _
                             "Values('" & obj.So_BB & "'," & obj.Ngay_BB & ",'" & obj.Ma_LTien & "'," & _
                             obj.So_To & "," & obj.LoaiTien & ",'" & obj.So_Serial & "','" & obj.DacDiem & "')"
                    cnTG.ExecuteNonQuery(strSql, CommandType.Text, trans)
                Next
                trans.Commit()
            Catch ex As Exception
                trans.Rollback()
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình insert tiền giả")
                'LogDebug.WriteLog("Lỗi trong quá trình insert tiền giả: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not trans Is Nothing Then trans.Dispose()
                If Not cnTG Is Nothing Then cnTG.Dispose()
            End Try
        End Sub
        Public Sub XoaTGia(ByVal strSoBB As String, ByVal lngNgayBB As Long)
            Dim cnTG As DataAccess
            Dim strSql As String
            Try
                cnTG = New DataAccess
                strSql = "Delete From tcs_tiengia_hdr where so_bb='" & strSoBB & "' And ngay_bb =" & lngNgayBB
                cnTG.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình xóa tiền giả")
                'LogDebug.WriteLog("Lỗi trong quá trình xóa tiền giả: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnTG Is Nothing Then cnTG.Dispose()
            End Try
        End Sub
        Public Function Get_SoBB(ByVal lngNgayLV As Long) As Integer
            Dim cnTG As New DataAccess
            Dim strSql As String
            Dim obj As infTienGiaDTL
            Dim ds As DataSet
            Dim intValue As Integer = 0
            Try
                cnTG = New DataAccess
                strSql = "Select max(so_bb) as so_bb From tcs_tiengia_hdr where ngay_bb= " & lngNgayLV '& " order by so_bb desc"
                ds = cnTG.ExecuteReturnDataSet(strSql, CommandType.Text)
                If IsEmptyDataSet(ds) Then
                    intValue = 1
                Else
                    If IsNumeric(GetString(ds.Tables(0).Rows(0).Item("so_bb"))) Then
                        intValue = CInt(GetString(ds.Tables(0).Rows(0).Item("so_bb"))) + 1
                    Else
                        intValue = 1
                    End If
                End If
                Return intValue
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy max số biên bản")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy max số biên bản: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnTG Is Nothing Then cnTG.Dispose()
            End Try
        End Function
        Public Function TraCuuTG(ByVal strWhere As String) As DataSet
            Dim cnTG As DataAccess
            Dim strSql As String
            Dim ds As New DataSet
            Try
                cnTG = New DataAccess
                strSql = "Select a.so_bb,a.ngay_bb,c.menh_gia,b.so_to,b.so_serial,b.loai_tien,b.dac_diem " & _
                         "From tcs_tiengia_hdr a,tcs_tiengia_dtl b,tcs_dm_loaitien c " & _
                         "Where a.so_bb = b.so_bb and a.ngay_bb = b.ngay_bb " & _
                         "and b.ma_ltien = c.ma_loaitien " & strWhere & " order by a.so_bb,a.ngay_bb asc"
                ds = cnTG.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình tra cứu tiền giả")
                'LogDebug.WriteLog("Lỗi trong quá trình tra cứu tiền giả: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnTG Is Nothing Then cnTG.Dispose()
            End Try
        End Function
        Public Function In_BienBan(ByVal strSoBB As String, ByVal intNgayBB As Integer) As DataSet
            Dim ds As New DataSet
            Dim strSql As String
            Dim cnTG As DataAccess
            Try
                cnTG = New DataAccess
                strSql = "Select a.so_bb,a.ngay_bb,a.ten_ddkb,a.chuc_vu as chuc_vu_dd,d.ten as ten_nl,d.chuc_danh as chuc_vu_nl," & _
                                "a.ten_kh,a.dchi_kh,a.socmnd,decode(b.loai_tien,0,'Tiền giấy','Tiền polymer') as loai_tien,c.menh_gia,b.so_to,b.so_serial,b.dac_diem as dacdiem,a.ma_tq " & _
                                "From tcs_tiengia_hdr a,tcs_tiengia_dtl b,tcs_dm_loaitien c,tcs_dm_nhanvien d " & _
                                "Where a.so_bb = b.so_bb and a.ngay_bb = b.ngay_bb " & _
                                "and a.ma_tq = d.ma_nv " & _
                                "and b.ma_ltien = c.ma_loaitien and a.ngay_bb=" & intNgayBB & _
                                "And a.so_bb ='" & strSoBB & "' order by a.so_bb,a.ngay_bb asc"

                ds = cnTG.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình tra cứu in tiền giả")
                'LogDebug.WriteLog("Lỗi trong quá trình tra cứu in tiền giả: " & ex.Message, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not cnTG Is Nothing Then cnTG.Dispose()
            End Try
        End Function
    End Class

End Namespace