﻿Public Class TrangThai
    Private p_TT As String
    Private p_Ma_TQ As Integer
    Private p_CT_BL As String
    Public Property TT() As String
        Get
            Return P_TT
        End Get
        Set(ByVal Value As String)
            P_TT = Value
        End Set
    End Property
    Public Property Ma_TQ() As Integer
        Get
            Return P_Ma_TQ
        End Get
        Set(ByVal Value As Integer)
            P_Ma_TQ = Value
        End Set
    End Property
    Public Property CT_BL() As String
        Get
            Return P_CT_BL
        End Get
        Set(ByVal Value As String)
            P_CT_BL = Value
        End Set
    End Property
End Class

#Region "HoanNH"
Public Class infLoaiTien
    Private strMaLoaiTien As String
    Private intMenhGia As Integer
    Private intSoTo As Integer
    Public Property MaLoaiTien() As String
        Get
            Return strMaLoaiTien
        End Get
        Set(ByVal Value As String)
            strMaLoaiTien = Value
        End Set
    End Property
    Public Property MenhGia() As Integer
        Get
            Return intMenhGia
        End Get
        Set(ByVal Value As Integer)
            intMenhGia = Value
        End Set
    End Property
    Public Property SoTo() As Integer
        Get
            Return intSoTo
        End Get
        Set(ByVal Value As Integer)
            intSoTo = Value
        End Set
    End Property
End Class
Public Class KeyKhoQuy
    Private p_SHKB As String
    Private p_Ngay_KB As Integer
    Private p_Ma_NV As Integer
    Private p_So_BT As Integer
    Private p_Ma_Dthu As String
    Private p_Kyhieu_CT As String
    Private p_So_CT As String
    Public Property SHKB() As String
        Get
            Return p_SHKB
        End Get
        Set(ByVal Value As String)
            p_SHKB = Value
        End Set
    End Property

    Public Property Ngay_KB() As Integer
        Get
            Return p_Ngay_KB
        End Get
        Set(ByVal Value As Integer)
            p_Ngay_KB = Value
        End Set
    End Property
    Public Property Ma_NV() As Integer
        Get
            Return p_Ma_NV
        End Get
        Set(ByVal Value As Integer)
            p_Ma_NV = Value
        End Set
    End Property
    Public Property So_BT() As Integer
        Get
            Return p_So_BT
        End Get
        Set(ByVal Value As Integer)
            p_So_BT = Value
        End Set
    End Property
    Public Property Ma_Dthu() As String
        Get
            Return p_Ma_Dthu
        End Get
        Set(ByVal Value As String)
            p_Ma_Dthu = Value
        End Set
    End Property
    Public Property Kyhieu_CT() As String
        Get
            Return p_Kyhieu_CT
        End Get
        Set(ByVal Value As String)
            p_Kyhieu_CT = Value
        End Set
    End Property
    Public Property So_CT() As String
        Get
            Return p_So_CT
        End Get
        Set(ByVal Value As String)
            p_So_CT = Value
        End Set
    End Property
End Class
#End Region