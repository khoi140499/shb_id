﻿Imports Business.Common.mdlSystemVariables
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.NewChungTu
Namespace KhoQuy

    Public Class buCTU

#Region "Lê Hồng Hà"
        Public Shared Function CTU_GetList(ByVal strNgayCT As String, ByVal strSoCT As String) As DataSet
            Try
                Dim daCT As New daCTU
                Return daCT.CTU_GetList(strNgayCT, strSoCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_Header_Set(ByVal key As KeyCTu) As DataSet
            Try
                Dim daCT As New daCTU
                Return daCT.CTU_Header_Set(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_ChiTiet_Set(ByVal key As KeyCTu) As DataSet
            Try
                Dim daCT As New daCTU
                Return daCT.CTU_ChiTiet_Set(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_Header(ByVal key As KeyCTu) As infChungTuHDR
            Try
                Dim daCT As New daCTU
                Return daCT.CTU_Header(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CTU_ChiTiet(ByVal key As KeyCTu) As infChungTuDTL()
            Try
                Dim daCT As New daCTU
                Return daCT.CTU_ChiTiet(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub CTU_Update_LanIn(ByVal key As KeyCTu)
            Try
                Dim daCT As New daCTU
                daCT.CTU_Update_LanIn(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function CTU_Get_LanIn(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daCTU
                Return daCT.CTU_Get_LanIn(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_Get_TrangThai(ByVal key As KeyCTu) As String
            Try
                Dim daCT As New daCTU
                Return daCT.CTU_Get_TrangThai(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CTU_IN_LASER(ByVal key As KeyCTu) As DataSet
            Try
                Dim daCT As New daCTU
                Return daCT.CTU_IN_LASER(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

    End Class


End Namespace
