﻿Imports System.Data
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports Business.Common.mdlSystemVariables

Namespace KhoQuy

    Public Class buSuaThucThu

#Region "Nguyễn Hữu Hoan"
        Public Shared Function Update_CTKQ_Group(ByVal KeyGroup As KeyKhoQuy, ByVal arrKHCT() As String, _
                                     ByVal arrSoCT() As String, ByVal strLoaiDL As String, _
                                     ByVal dblTTien As Double, _
                                     Optional ByVal arrLTien() As infLoaiTien = Nothing) As Boolean
            Try
                Dim daSuaTT As New daSuaThucThu
                Return daSuaTT.Update_CTKQ_Group(KeyGroup, arrKHCT, arrSoCT, strLoaiDL, dblTTien, arrLTien)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Update_CTKQ(ByVal Key As KeyKhoQuy, ByVal dblTTien As Double, _
                              ByVal strLoaiDL As String, ByVal bytGroup As Byte, _
                              Optional ByVal arrLTien() As infLoaiTien = Nothing) As Boolean
            Try
                Dim daSuaTT As New daSuaThucThu
                Return daSuaTT.Update_CTKQ(Key, dblTTien, strLoaiDL, bytGroup, arrLTien)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Get_TTienGroup(ByVal KeyGroup As KeyKhoQuy) As Double
            Try
                Dim daTC As New daSuaThucThu
                Return daTC.Get_TTienGroup(KeyGroup)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Get_SaiSo(ByVal Key As KeyKhoQuy, ByVal strLoaiDL As String) As Double
            Try
                Dim daCT As New daSuaThucThu
                Return daCT.Get_SaiSo(Key, strLoaiDL)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Get_LoaiTienKQ(ByVal keyCT As KeyKhoQuy) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy dữ liệu về loại tiền trong bảng kho quỹ
            ' Tham số       : 
            ' Giá trị trả về: ds
            ' Ngày viết     : 19/12/2007
            ' Người viết    : Nguyễn Hữu Hoan
            ' ----------------------------------------------------------------
            Try
                Dim daSuaTT As New daSuaThucThu
                Return daSuaTT.Get_LoaiTienKQ(keyCT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Get_TT(ByVal ct As KeyKhoQuy, ByVal dl As String) As Double
            Try
                Dim daTT As New daSuaThucThu
                Return daTT.Get_TT(ct, dl)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'ICB: xóa số thực thu
        'Hoàng Văn Anh
        Public Shared Function Delete_STThu(ByVal Key As KeyKhoQuy)
            Try
                Dim daCT As New daSuaThucThu
                Return daCT.Delete_STThu(Key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

    End Class


End Namespace