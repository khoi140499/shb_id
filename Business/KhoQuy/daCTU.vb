﻿Imports VBOracleLib
Imports System.Data.OleDb
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.NewChungTu

Public Class daCTU
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "Xử lý chứng từ"
    Public Function CTU_Get_LanIn(ByVal key As KeyCTu) As String
        Dim cnCT As DataAccess
        Dim drCT As IDataReader
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select LAN_IN from tcs_ctu_hdr " & _
                    "where shkb = '" & key.SHKB.ToString() & "' and " & _
                    " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                    " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                    " so_bt = " & key.So_BT.ToString() & " and " & _
                    " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

            drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
            Dim strLanIn As String = ""
            If (drCT.Read()) Then
                strLanIn = drCT(0).ToString()
            End If
            Return strLanIn
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin số lần in chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            If (Not IsNothing(drCT)) Then
                drCT.Close()
                drCT.Dispose()
            End If
        End Try
    End Function
    Public Function CTU_Get_TrangThai(ByVal key As KeyCTu) As String
        Dim cnCT As DataAccess
        Dim drCT As IDataReader
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select trang_thai from tcs_ctu_hdr " & _
                    "where shkb = '" & key.SHKB.ToString() & "' and " & _
                    " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                    " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                    " so_bt = " & key.So_BT.ToString() & " and " & _
                    " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

            drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
            Dim strLanIn As String = ""
            If (drCT.Read()) Then
                strLanIn = drCT(0).ToString()
            End If
            Return strLanIn
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin trạng thái chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            If (Not IsNothing(drCT)) Then
                drCT.Close()
                drCT.Dispose()
            End If
        End Try
    End Function

    Public Sub CTU_Update_LanIn(ByVal key As KeyCTu)
        Dim cnCT As DataAccess
        Dim drCT As IDataReader
        Try
            cnCT = New DataAccess
            Dim strSQL As String

            strSQL = "select LAN_IN from tcs_ctu_hdr " & _
                            "where shkb = '" & key.SHKB.ToString() & "' and " & _
                            " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                            " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                            " so_bt = " & key.So_BT.ToString() & " and " & _
                            " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

            drCT = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
            Dim strLanIn As String = "0"
            If (drCT.Read()) Then
                strLanIn = drCT(0).ToString()
            End If

            If (Not IsNumeric(strLanIn)) Then strLanIn = "0"

            Dim intSoLanIn As Integer = Convert.ToInt16(strLanIn)
            intSoLanIn += 1

            cnCT = New DataAccess
            strSQL = "update tcs_ctu_hdr set LAN_IN = " & intSoLanIn.ToString() & " " & _
                    "where shkb = '" & key.SHKB.ToString() & "' and " & _
                    " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                    " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                    " so_bt = " & key.So_BT.ToString() & " and " & _
                    " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "
            cnCT.ExecuteNonQuery(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật lần in")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            If (Not IsNothing(drCT)) Then
                drCT.Close()
                drCT.Dispose()
            End If
        End Try
    End Sub
    Public Function CTU_GetList(ByVal strNgayCT As String, ByVal strSoCT As String) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy danh sách chứng từ trong ngày để kiểm soát
        ' Tham số: Ngày chứng từ, trạng thái chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Dim ds As DataSet
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select KYHIEU_CT, SO_CT, SO_BT,TRANG_THAI, SO_BTHU, TTIEN,Lan_In, MA_NV, SHKB, NGAY_KB, MA_DTHU " & _
                "from TCS_CTU_HDR " & _
                "where (NGAY_KB = " & strNgayCT & ") and (ma_tq=0) and (TT_Tthu=0) and (Trang_Thai <>'04') AND ma_nv=" & CInt(gstrTenND) & _
                " AND SO_CT=" & Globals.EscapeQuote(strSoCT) & _
                " order by ma_nv, so_bt "
            ds = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách chứng từ trong ngày để kiểm soát")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
#End Region

#Region "Chi tiết chứng từ"
    Public Function CTU_Header(ByVal key As KeyCTu) As infChungTuHDR
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chung của chứng từ
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Dim hdr As New infChungTuHDR
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV," & _
                "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                "a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
                "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
                "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
                "a.Ngay_QD,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
                "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
                "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
                "a.So_TK,a.Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                "a.So_Khung,a.So_May,a.TK_KH_NH,a.NGAY_KH_NH," & _
                "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
                "a.Lan_In,a.Trang_Thai, " & _
                "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, a.So_BT_KTKB" & _
                " from TCS_CTU_HDR a " & _
                "where (a.ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                "and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _
                "and (a.so_bt = " & key.So_BT.ToString() & ") " & _
                "and (a.shkb = '" & key.SHKB.ToString() & "') " & _
                "and (a.ma_dthu = '" & key.Ma_Dthu & "') "
            Dim dsCT As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

            ' Kiểm tra không có dữ liệu -> Thoát
            If (IsEmptyDataSet(dsCT)) Then Return hdr

            'Có dữ liệu -> đưa vào đối tượng "hdr"
            hdr.CQ_QD = dsCT.Tables(0).Rows(0).Item("CQ_QD").ToString()
            hdr.DC_NNThue = dsCT.Tables(0).Rows(0).Item("DC_NNTHUE").ToString()
            hdr.DC_NNTien = dsCT.Tables(0).Rows(0).Item("DC_NNTIEN").ToString()
            hdr.Diachi_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Diachi_KH_Nhan").ToString()
            hdr.DVSDNS = dsCT.Tables(0).Rows(0).Item("DVSDNS").ToString()
            hdr.KyHieu_CT = dsCT.Tables(0).Rows(0).Item("KYHIEU_CT").ToString()
            hdr.Lan_In = dsCT.Tables(0).Rows(0).Item("LAN_IN").ToString()
            hdr.LH_XNK = dsCT.Tables(0).Rows(0).Item("LH_XNK").ToString()
            hdr.Ly_Do = dsCT.Tables(0).Rows(0).Item("LY_DO").ToString()
            hdr.Ma_CQThu = dsCT.Tables(0).Rows(0).Item("MA_CQTHU").ToString()
            hdr.Ma_DThu = dsCT.Tables(0).Rows(0).Item("MA_DTHU").ToString()
            hdr.Ma_Huyen = dsCT.Tables(0).Rows(0).Item("MA_HUYEN").ToString()
            hdr.Ma_KS = dsCT.Tables(0).Rows(0).Item("MA_KS").ToString()
            hdr.Ma_LThue = dsCT.Tables(0).Rows(0).Item("MA_LTHUE").ToString()
            hdr.MA_NH_A = dsCT.Tables(0).Rows(0).Item("MA_NH_A").ToString()
            hdr.MA_NH_B = dsCT.Tables(0).Rows(0).Item("MA_NH_B").ToString()
            hdr.Ma_NNThue = dsCT.Tables(0).Rows(0).Item("MA_NNTHUE").ToString()
            hdr.Ma_NNTien = dsCT.Tables(0).Rows(0).Item("MA_NNTIEN").ToString()
            hdr.Ma_NT = dsCT.Tables(0).Rows(0).Item("MA_NT").ToString()
            hdr.Ma_NV = dsCT.Tables(0).Rows(0).Item("MA_NV").ToString()
            hdr.Ma_Tinh = dsCT.Tables(0).Rows(0).Item("MA_TINH").ToString()
            hdr.Ma_TQ = dsCT.Tables(0).Rows(0).Item("MA_TQ").ToString()
            hdr.Ma_Xa = dsCT.Tables(0).Rows(0).Item("MA_XA").ToString()
            hdr.Ngay_CT = dsCT.Tables(0).Rows(0).Item("NGAY_CT").ToString()
            hdr.Ngay_HT = dsCT.Tables(0).Rows(0).Item("NGAY_HT").ToString()
            hdr.Ngay_KB = dsCT.Tables(0).Rows(0).Item("NGAY_KB").ToString()
            hdr.NGAY_KH_NH = dsCT.Tables(0).Rows(0).Item("NGAY_KH_NH").ToString()
            hdr.Ngay_QD = dsCT.Tables(0).Rows(0).Item("NGAY_QD").ToString()
            hdr.Ngay_TK = dsCT.Tables(0).Rows(0).Item("NGAY_TK").ToString()
            hdr.SHKB = dsCT.Tables(0).Rows(0).Item("SHKB").ToString()
            hdr.So_BT = dsCT.Tables(0).Rows(0).Item("SO_BT").ToString()
            hdr.So_BThu = dsCT.Tables(0).Rows(0).Item("SO_BTHU").ToString()
            hdr.So_CT = dsCT.Tables(0).Rows(0).Item("SO_CT").ToString()
            hdr.So_CT_NH = dsCT.Tables(0).Rows(0).Item("SO_CT_NH").ToString()
            hdr.So_Khung = dsCT.Tables(0).Rows(0).Item("SO_KHUNG").ToString()
            hdr.So_May = dsCT.Tables(0).Rows(0).Item("SO_MAY").ToString()
            hdr.So_QD = dsCT.Tables(0).Rows(0).Item("SO_QD").ToString()
            hdr.So_TK = dsCT.Tables(0).Rows(0).Item("SO_TK").ToString()
            hdr.Ten_DVSDNS = dsCT.Tables(0).Rows(0).Item("Ten_DVSDNS").ToString()
            hdr.Ten_KH_Nhan = dsCT.Tables(0).Rows(0).Item("Ten_KH_Nhan").ToString()
            hdr.Ten_NNThue = dsCT.Tables(0).Rows(0).Item("TEN_NNTHUE").ToString()
            hdr.Ten_NNTien = dsCT.Tables(0).Rows(0).Item("TEN_NNTIEN").ToString()
            hdr.TG_ID = dsCT.Tables(0).Rows(0).Item("TG_ID").ToString()
            hdr.TK_Co = dsCT.Tables(0).Rows(0).Item("TK_Co").ToString()
            hdr.TK_KH_NH = dsCT.Tables(0).Rows(0).Item("TK_KH_NH").ToString()
            hdr.TK_KH_Nhan = dsCT.Tables(0).Rows(0).Item("TK_KH_Nhan").ToString()
            hdr.TK_No = dsCT.Tables(0).Rows(0).Item("TK_No").ToString()
            hdr.Trang_Thai = dsCT.Tables(0).Rows(0).Item("TRANG_THAI").ToString()
            hdr.TT_TThu = dsCT.Tables(0).Rows(0).Item("TT_TTHU").ToString()
            hdr.TTien = dsCT.Tables(0).Rows(0).Item("TTIEN").ToString()
            hdr.TTien_NT = dsCT.Tables(0).Rows(0).Item("TTIEN_NT").ToString()
            hdr.Ty_Gia = dsCT.Tables(0).Rows(0).Item("TY_GIA").ToString()
            hdr.XA_ID = dsCT.Tables(0).Rows(0).Item("XA_ID").ToString()
            hdr.So_BT_KTKB = dsCT.Tables(0).Rows(0).Item("So_BT_KTKB").ToString()
            Return hdr
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chung của chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Function CTU_ChiTiet(ByVal key As KeyCTu) As infChungTuDTL()
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chi tiết của chứng từ.
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                    "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                    "DT_ID,Ma_TLDT,Ky_Thue,SoTien,SoTien_NT " & _
                    "from TCS_CTU_DTL " & _
                    "where (ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                    "and (ma_nv = " & key.Ma_NV.ToString() & ") " & _
                    "and (so_bt = " & key.So_BT.ToString() & ") " & _
                    "and (shkb = '" & key.SHKB.ToString() & "') " & _
                    "and (ma_dthu = '" & key.Ma_Dthu & "') "
            Dim ds As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

            If (IsEmptyDataSet(ds)) Then Return Nothing
            Dim i, intCount As Integer
            intCount = ds.Tables(0).Rows.Count - 1

            Dim dtl(intCount) As infChungTuDTL

            For i = 0 To intCount
                dtl(i) = New infChungTuDTL

                dtl(i).CCH_ID = ds.Tables(0).Rows(i).Item("CCH_ID").ToString()
                dtl(i).DT_ID = ds.Tables(0).Rows(i).Item("DT_ID").ToString()
                dtl(i).ID = ds.Tables(0).Rows(i).Item("ID").ToString()
                dtl(i).Ky_Thue = ds.Tables(0).Rows(i).Item("Ky_Thue").ToString()
                dtl(i).LKH_ID = ds.Tables(0).Rows(i).Item("LKH_ID").ToString()
                dtl(i).Ma_Cap = ds.Tables(0).Rows(i).Item("Ma_Cap").ToString()
                dtl(i).Ma_Chuong = ds.Tables(0).Rows(i).Item("Ma_Chuong").ToString()
                dtl(i).Ma_DThu = ds.Tables(0).Rows(i).Item("Ma_DThu").ToString()
                dtl(i).Ma_Khoan = ds.Tables(0).Rows(i).Item("Ma_Khoan").ToString()
                dtl(i).Ma_Loai = ds.Tables(0).Rows(i).Item("Ma_Loai").ToString()
                dtl(i).Ma_Muc = ds.Tables(0).Rows(i).Item("Ma_Muc").ToString()
                dtl(i).Ma_NV = ds.Tables(0).Rows(i).Item("Ma_NV").ToString()
                dtl(i).Ma_TLDT = ds.Tables(0).Rows(i).Item("Ma_TLDT").ToString()
                dtl(i).Ma_TMuc = ds.Tables(0).Rows(i).Item("Ma_TMuc").ToString()
                dtl(i).MTM_ID = ds.Tables(0).Rows(i).Item("MTM_ID").ToString()
                dtl(i).Ngay_KB = ds.Tables(0).Rows(i).Item("Ngay_KB").ToString()
                dtl(i).Noi_Dung = ds.Tables(0).Rows(i).Item("Noi_Dung").ToString()
                dtl(i).SHKB = ds.Tables(0).Rows(i).Item("SHKB").ToString()
                dtl(i).So_BT = ds.Tables(0).Rows(i).Item("So_BT").ToString()
                dtl(i).SoTien = ds.Tables(0).Rows(i).Item("SoTien").ToString()
                dtl(i).SoTien_NT = ds.Tables(0).Rows(i).Item("SoTien_NT").ToString()
            Next

            Return dtl
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chi tiết của chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function

    Public Function CTU_Header_Set(ByVal key As KeyCTu) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chung của chứng từ     
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------  
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select a.SHKB,a.Ngay_KB,a.Ma_NV," & _
                            "a.So_BT,a.Ma_DThu,a.So_BThu," & _
                            "a.KyHieu_CT,a.So_CT,a.So_CT_NH," & _
                            "a.Ma_NNTien,a.Ten_NNTien,a.DC_NNTien," & _
                            "a.Ma_NNThue,a.Ten_NNThue,a.DC_NNThue," & _
                            "a.Ly_Do,a.Ma_KS,a.Ma_TQ,a.So_QD," & _
                            "to_char(a.Ngay_QD,'dd/MM/rrrr') as Ngay_QD ,a.CQ_QD,a.Ngay_CT,a.Ngay_HT," & _
                            "a.Ma_CQThu,a.XA_ID,a.Ma_Tinh," & _
                            "a.Ma_Huyen,a.Ma_Xa,a.TK_No,a.TK_Co," & _
                            "a.So_TK,to_char(a.Ngay_TK,'dd/MM/rrrr') as Ngay_TK,a.LH_XNK,a.DVSDNS,a.Ten_DVSDNS," & _
                            "a.Ma_NT,a.Ty_Gia,a.TG_ID, a.Ma_LThue," & _
                            "a.So_Khung,a.So_May,a.TK_KH_NH,to_char(a.NGAY_KH_NH,'dd/MM/rrrr') as NGAY_KH_NH," & _
                            "a.MA_NH_A,a.MA_NH_B,a.TTien,a.TT_TThu," & _
                            "a.Lan_In,a.Trang_Thai,a.so_bt_ktkb, " & _
                            "a.TK_KH_Nhan, a.Ten_KH_Nhan, a.Diachi_KH_Nhan, a.TTien_NT, " & _
                            "nha.ten as TEN_NH_A, nhb.ten as TEN_NH_B, " & _
                            "xa.ten as TenXa, tkno.ten_tk as Ten_TKNo, tkco.ten_tk as Ten_TKCo, " & _
                            "cqthu.ten as Ten_CQThu, nt.ten as Ten_NT, lt.ten as Ten_LThue,a.PT_TT as PT_TT, " & _
                            "a.So_BK, to_char(a.Ngay_BK,'dd/MM/yyyy') as Ngay_BK " & _
                            "from TCS_CTU_HDR a, TCS_DM_XA xa, TCS_DM_TAIKHOAN tkno, TCS_DM_TAIKHOAN tkco, " & _
                            "TCS_DM_CQTHU cqthu, TCS_DM_NGUYENTE nt, TCS_DM_LTHUE lt, TCS_DM_NGANHANG nha, TCS_DM_NGANHANG nhb " & _
                            "where (a.xa_id = xa.xa_id(+))" & _
                            "and (a.tk_no = tkno.tk (+)) and (a.tk_co = tkco.tk(+)) and (a.ma_nh_a = nha.ma(+)) and (a.ma_nh_b = nhb.ma(+)) " & _
                            "and (a.ma_cqthu = cqthu.ma_cqthu(+)) and (a.ma_nt = nt.ma_nt(+)) and (a.ma_lthue = lt.ma_lthue(+)) " & _
                            "and (a.ngay_kb = " & key.Ngay_KB.ToString() & ") " & _
                            "and (a.ma_nv = " & key.Ma_NV.ToString() & ") " & _
                            "and (a.so_bt = " & key.So_BT.ToString() & ") " & _
                            "and (a.shkb = '" & key.SHKB.ToString() & "') " & _
                            "and (a.ma_dthu = '" & key.Ma_Dthu & "') "
            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chung chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
    Public Function CTU_ChiTiet_Set(ByVal key As KeyCTu) As DataSet
        '-----------------------------------------------------
        ' Mục đích: Lấy thông tin chi tiết của chứng từ.
        ' Tham số: Ký hiệu chứng từ, số chứng từ
        ' Giá trị trả về:
        ' Ngày viết: 26/10/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String
            strSQL = "select ID,SHKB,Ngay_KB,Ma_NV,So_BT,Ma_DThu,CCH_ID,Ma_Cap," & _
                     "Ma_Chuong,LKH_ID,Ma_Loai,Ma_Khoan,MTM_ID,Ma_Muc,Ma_TMuc,Noi_Dung," & _
                     "DT_ID,MaDT,Ky_Thue,SoTien,SoTien_NT, MaQuy, Ma_DP, ID " & _
                     "from TCS_CTU_DTL " & _
                     "where (shkb = '" & key.SHKB & "') and (ngay_kb = " & key.Ngay_KB & ") " & _
                     "and (ma_nv = " & key.Ma_NV & ") and (so_bt = " & key.So_BT & ") and (ma_dthu = '" & key.Ma_Dthu & "') "
            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chi tiết chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
#End Region

    Public Function CTU_IN_LASER(ByVal key As KeyCTu) As DataSet
        Dim cnCT As DataAccess
        Try
            cnCT = New DataAccess
            Dim strSQL As String

            strSQL = "select noi_dung as NoiDung, (ma_cap||ma_chuong) as CC, " & _
                               "(ma_loai||ma_khoan) as LK, (ma_muc||ma_tmuc) as MTM, " & _
                               "ky_thue as KyThue, sotien as SoTien " & _
                               "from tcs_ctu_dtl " & _
                               "where shkb = '" & key.SHKB.ToString() & "' and " & _
                               " ngay_kb = " & key.Ngay_KB.ToString() & " and " & _
                               " ma_nv = " & key.Ma_NV.ToString() & " and " & _
                               " so_bt = " & key.So_BT.ToString() & " and " & _
                               " ma_dthu = '" & key.Ma_Dthu.ToString() & "' "

            Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình in chứng từ")
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        Finally
            If (Not IsNothing(cnCT)) Then cnCT.Dispose()
        End Try
    End Function
End Class
