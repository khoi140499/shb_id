﻿Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Imports VBOracleLib

Namespace KhoQuy

    Public Class buTraCuuTThu
        Public Shared Function Xoa_CT_Group(ByVal keyGroup As KeyKhoQuy, ByVal strKHCT As String, ByVal strSoCT As String, ByVal strLoaiDL As String) As Boolean
            Try
                Dim arrKHCT() As String
                Dim arrSoCT() As String
                Dim daTC As New daTraCuuTThu
                Select Case strLoaiDL
                    Case "BL"
                        daTC.Get_BL_Group(keyGroup, arrKHCT, arrSoCT)
                    Case "CT"
                        daTC.Get_CT_Group(keyGroup, arrKHCT, arrSoCT)
                End Select

                Return daTC.Xoa_CT_Group(keyGroup, arrKHCT, arrSoCT, strLoaiDL)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Xoa_CT(ByVal Key As KeyKhoQuy, ByVal strLoaiDL As String) As Boolean
            Try
                Dim daTC As New daTraCuuTThu
                Return daTC.Xoa_CT(Key, strLoaiDL)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Get_CT_Group(ByVal keyGroup As KeyKhoQuy, ByRef arrKHCT() As String, ByRef arrSoCT() As String, ByRef arrNVBT() As String) As Boolean
            Try
                Dim daTC As New daTraCuuTThu
                daTC.Get_CT_Group(keyGroup, arrKHCT, arrSoCT, arrNVBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Get_BL_Group(ByVal keyGroup As KeyKhoQuy, ByRef arrKHCT() As String, ByRef arrSoCT() As String, ByRef arrNVBT() As String) As Boolean
            Try
                Dim daTC As New daTraCuuTThu
                daTC.Get_BL_Group(keyGroup, arrKHCT, arrSoCT, arrNVBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DS_TraCuu_ThucThu(ByVal strCT As String, ByVal strBL As String, ByVal intPhamvi As Integer) As DataSet
            '-----------------------------------------------------
            ' Mục đích      : Thực hiện xử lý các yêu cầu do NSD nhập vào từ form
            ' Dữ liệu vào   : Các giá trị người dùng nhập từ form
            ' Giá trị trả về: dataset chua du lieu chứng từ, biên lai đã nhập số thực thu
            ' Ngày viết     : 18/12/2007
            ' Người viết    : Nguyen Huu Hoan
            ' ----------------------------------------------------
            Try
                Dim strWhereCT As String
                Dim strWhereBL As String
                Dim daTraCuu As New daTraCuuTThu
                Dim ds As DataSet
                ' Khai báo các câu lệnh 
                strWhereCT = "Select lpad(ma_nv,3,'0') ||'/' || lpad(so_bt,4,'0') as NVBT," & _
                              "so_ct as SOCT," & _
                              "nvl (Ten_nntien,Ten_nnthue) as TenNNT," & _
                              "ttien,tt_tthu,kyhieu_ct as KHCT From tcs_ctu_hdr " & _
                              "Where(1=1)" & strCT
                'strWhereCT = "Select lpad(ma_nv,3,'0') ||'/' || lpad(so_bt,4,'0') as NVBT," & _
                '             "so_ct as SOCT," & _
                '             "nvl (Ten_nntien,Ten_nnthue) as TenNNT," & _
                '             "ttien,tt_tthu,kyhieu_ct as KHCT From tcs_ctu_hdr " & _
                '             "Where TT_Tthu <>0 And Ma_TQ<>0 " & strCT 

                ' Câu lệnh truy vấn Biên lai thu
                strWhereBL = "Select lpad(ma_nv,3,'0')||'/'|| lpad(so_bt,4,'0') as NVBT," & _
                          "So_BL as SOCT,Ten_NNTien as TenNNT," & _
                          "ttien,tt_tthu,Kyhieu_BL as KHCT From TCS_CTBL " & _
                          "Where (1=1) " & strBL
                'strWhereBL = "Select lpad(ma_nv,3,'0')||'/'|| lpad(so_bt,4,'0') as NVBT," & _
                '            "So_BL as SOCT,Ten_NNTien as TenNNT," & _
                '            "ttien,tt_tthu,Kyhieu_BL as KHCT From TCS_CTBL " & _
                '            "Where (TT_Tthu <>0) And (Trang_thai<>'03') " & strBL
                ' ----------Truy vấn dữ liệu và trả về dataset --------------
                Select Case intPhamvi
                    Case 0 ' Lấy tất cả
                        ds = daTraCuu.DSSoThucThu(strWhereCT & " Union " & strWhereBL)
                    Case 1 ' Lấy chứng từ
                        ds = daTraCuu.DSSoThucThu(strWhereCT)
                    Case 2 ' Lấy biên lai
                        ds = daTraCuu.DSSoThucThu(strWhereBL)
                End Select
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function In_DSCT(ByVal strCT As String, ByVal strBL As String, ByVal intPhamvi As Integer) As DataSet
            '-----------------------------------------------------
            ' Mục đích      : Thực hiện xử lý các yêu cầu do NSD nhập vào từ form
            ' Dữ liệu vào   : Các giá trị người dùng nhập từ form
            ' Giá trị trả về: dataset chua du lieu chứng từ, biên lai đã nhập số thực thu
            ' Ngày viết     : 18/12/2007
            ' Người viết    : Nguyen Huu Hoan
            ' ----------------------------------------------------
            Try
                Dim strWhereCT As String
                Dim strWhereBL As String
                Dim daTraCuu As New daTraCuuTThu
                Dim ds As DataSet

                strWhereCT = "Select Ma_nnthue as MaNNT,lpad(ma_nv,3,'0') ||'/' || lpad(so_bt,4,'0') as NVBT," & _
                                "so_ct as SOCT," & _
                                "nvl (Ten_nntien, Ten_nnthue) as TenNNT," & _
                                "ttien,tt_tthu From tcs_ctu_hdr " & _
                                "Where (1=1) " & strCT & _
                                "  Order by so_ct asc"
                'strWhereCT = "Select Ma_nnthue as MaNNT,lpad(ma_nv,3,'0') ||'/' || lpad(so_bt,4,'0') as NVBT," & _
                '             "so_ct as SOCT," & _
                '             "nvl (Ten_nntien, Ten_nnthue) as TenNNT," & _
                '             "ttien,tt_tthu From tcs_ctu_hdr " & _
                '             "Where TT_Tthu <>0 And Ma_TQ<>0 and trang_thai<>'04' " & strCT & _
                '             "  Order by so_ct asc"

                ' Câu lệnh truy vấn Biên lai thu
                strWhereBL = "Select lpad(ma_nv,3,'0')||'/'|| lpad(so_bt,4,'0') as NVBT," & _
                                            "So_BL as SOCT,Ten_NNTien as TenNNT," & _
                                            "ttien,tt_tthu,Kyhieu_BL as KHCT From TCS_CTBL " & _
                                            "Where (1=1) " & strBL
                'strWhereBL = "Select lpad(ma_nv,3,'0')||'/'|| lpad(so_bt,4,'0') as NVBT," & _
                '            "So_BL as SOCT,Ten_NNTien as TenNNT," & _
                '            "ttien,tt_tthu,Kyhieu_BL as KHCT From TCS_CTBL " & _
                '            "Where (TT_Tthu <>0) And (Trang_thai<>'03') " & strBL
                ' ----------Truy vấn dữ liệu và trả về dataset --------------
                Select Case intPhamvi
                    Case 0 ' Lấy tất cả
                        ds = daTraCuu.DSSoThucThu(strWhereCT & " Union " & strWhereBL)
                    Case 1 ' Lấy chứng từ
                        ds = daTraCuu.DSSoThucThu(strWhereCT)
                    Case 2 ' Lấy biên lai
                        ds = daTraCuu.DSSoThucThu(strWhereBL)
                End Select
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetKeys(ByVal strKHCT As String, ByVal strSoCT As String, ByVal strDL As String) As KeyKhoQuy
            Try
                Dim daTC As New daTraCuuTThu
                Return daTC.GetKeys(strKHCT, strSoCT, strDL)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function checkCT_KQ(ByVal strKyhieuCT As String, ByVal strSoCT As String, ByRef GroupType As Byte) As KeyKhoQuy
            '-----------------------------------------------------
            ' Mục đích      : Kiểm tra CT có tồn tại trong bảng kho quỹ không
            ' Tham số       : 
            ' Giá trị trả về: - GroupType = 0 CT khong co trong bang kho quy
            '                 - GroupType = 1 CT co trong bang kho quy, khong duoc gop
            '                 - GroupType = 2 CT duoc gop trong bang kho quy
            '                 - Object Dataset chứa Key của CT
            ' Ngày viết     : 24/10/2007
            ' Người viết    : Lê Anh Ngọc
            ' ----------------------------------------------------
            Dim daTC As New daTraCuuTThu
            Dim ds As New DataSet
            Dim keyGroup As KeyKhoQuy
            Try
                keyGroup = daTC.GetKey_Group(strKyhieuCT, strSoCT)
                If keyGroup Is Nothing Then ' Nhập theo tông tiền
                    GroupType = 0
                    Return Nothing
                Else ' Nhập theo mệnh giá
                    ds = daTC.CheckGopCT(keyGroup)
                    If ds.Tables(0).Rows.Count > 1 Then ' Có trong kho quỹ, nhập gộp
                        GroupType = 2
                        Return keyGroup
                    ElseIf ds.Tables(0).Rows.Count = 1 Then ' Có trong kho quỹ, không nhập gộp
                        GroupType = 1
                        Return keyGroup
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function KBPLTM(ByVal strw As String) As DataSet
            Try
                Dim obj As New daTraCuuTThu
                Dim ds As DataSet
                ds = obj.BKPLTM(strw)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class

End Namespace