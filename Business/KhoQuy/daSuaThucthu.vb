﻿Imports VBOracleLib
Imports System.Data
Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon

Namespace KhoQuy

    Public Class daSuaThucThu
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "Nguyễn Hữu Hoan"
        Public Function Update_CTKQ_Group(ByVal KeyGroup As KeyKhoQuy, ByVal arrKHCT() As String, _
                                     ByVal arrSoCT() As String, ByVal strLoaiDL As String, _
                                     ByVal dblTTien As Double, _
                                     Optional ByVal arrLTien() As infLoaiTien = Nothing) As Boolean
            '----------------------------------------------------------------------------
            ' Mục đích : Ghi dữ liệu kho quỹ
            ' Đầu vào  : KeyGroup - Bộ khóa của nhóm chứng từ
            ' dblTtien : Tổng tiền thực thu cho chứng từ thứ nhất
            ' strLoaiDL: Loại dữ liệu CT - chứng từ, BL - Biên lai
            ' bytGroup : Xác định tồn tại của dữ liệu CT: 0 - Chưa có trong bảng kho quỹ
            '                                             2 - Tồn tại trong bảng kho quỹ
            ' arrLTien : Mảng chứa dữ liệu các loại tiền
            '            Nếu là nothing: --> Nhập theo tổng tiền
            '            else            --> Nhập theo mệnh giá
            '-----------------------------------------------------------------------------
            Dim cnTT As DataAccess
            Dim strSql As String
            Dim tranTT As IDbTransaction
            Dim objLT As infLoaiTien
            Dim i As Integer
            Try
                cnTT = New DataAccess
                tranTT = cnTT.BeginTransaction
                ' Xóa trong bảng kho quỹ theo key group
                ' Xóa detail
                strSql = "Delete From TCS_KhoQuy_Dtl " & _
                         "Where SHKB='" & KeyGroup.SHKB & "' And Ngay_KB= " & KeyGroup.Ngay_KB & _
                         " And Ma_NV=" & KeyGroup.Ma_NV & " And So_BT= " & KeyGroup.So_BT & _
                         " And Ma_Dthu='" & KeyGroup.Ma_Dthu & "'"
                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                ' Xóa trong bảng header
                strSql = "Delete From TCS_KhoQuy_Hdr " & _
                         "Where SHKB='" & KeyGroup.SHKB & "' And Ngay_KB= " & KeyGroup.Ngay_KB & _
                         " And Ma_NV=" & KeyGroup.Ma_NV & " And So_BT= " & KeyGroup.So_BT & _
                         " And Ma_Dthu='" & KeyGroup.Ma_Dthu & "'"
                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                ' Nếu nhập theo phân loại tiền thì insert vào bảng kho quỹ
                If Not arrLTien Is Nothing Then
                    ' Đưa vào bảng Hdr
                    strSql = "Insert Into Tcs_khoquy_hdr(shkb, ngay_kb,ma_nv, so_bt, ma_dthu) " & _
                             "Values('" & KeyGroup.SHKB & "'," & KeyGroup.Ngay_KB & "," & KeyGroup.Ma_NV & "," & _
                             KeyGroup.So_BT & ",'" & KeyGroup.Ma_Dthu & "')"
                    cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                    For i = 0 To arrKHCT.Length - 1
                        If arrKHCT(i) = KeyGroup.Kyhieu_CT And arrSoCT(i) = KeyGroup.So_CT Then
                            For Each objLT In arrLTien
                                ' Đưa vào bảng Hdr
                                strSql = "Insert Into Tcs_khoquy_dtl(shkb, ngay_kb,ma_nv, so_bt, ma_dthu,kyhieu_ct,so_ct,ma_loaitien,so_to) " & _
                                         "Values('" & KeyGroup.SHKB & "'," & KeyGroup.Ngay_KB & "," & KeyGroup.Ma_NV & "," & _
                                         KeyGroup.So_BT & ",'" & KeyGroup.Ma_Dthu & "','" & KeyGroup.Kyhieu_CT & "','" & KeyGroup.So_CT & "','" & _
                                         objLT.MaLoaiTien & "'," & objLT.SoTo & ")"
                                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                            Next
                        Else
                            For Each objLT In arrLTien
                                ' Đưa vào bảng Hdr
                                strSql = "Insert Into Tcs_khoquy_dtl(shkb, ngay_kb,ma_nv, so_bt, ma_dthu,kyhieu_ct,so_ct,ma_loaitien,so_to) " & _
                                         "Values('" & KeyGroup.SHKB & "'," & KeyGroup.Ngay_KB & "," & KeyGroup.Ma_NV & "," & _
                                         KeyGroup.So_BT & ",'" & KeyGroup.Ma_Dthu & "','" & arrKHCT(i) & "','" & arrSoCT(i) & "','" & _
                                         objLT.MaLoaiTien & "'," & 0 & ")"
                                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                            Next
                        End If
                    Next
                End If
                ' Cập nhật trong bảng TCS_CTu_HDR,Tcs_CTBL cho chứng từ đầu tiên
                If "CT".Equals(strLoaiDL) Then
                    ' Cập nhật chứng từ
                    strSql = "Update TCS_CTU_HDR Set Ma_TQ =" & gstrTenND & ", " & _
                             " tt_tthu= " & dblTTien & _
                             " Where SHKB='" & KeyGroup.SHKB & "' And Ngay_KB= " & KeyGroup.Ngay_KB & _
                             " And Ma_NV=" & KeyGroup.Ma_NV & " And So_BT= " & KeyGroup.So_BT & _
                             " And Ma_Dthu='" & KeyGroup.Ma_Dthu & "'"
                    cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                Else
                    ' Cập nhật biên lai
                    strSql = "Update TCS_CTBL Set Ma_TQ =" & gstrTenND & ", " & _
                             " tt_tthu= " & dblTTien & _
                             " Where SHKB='" & KeyGroup.SHKB & "' And Ngay_KB= " & KeyGroup.Ngay_KB & _
                             " And Ma_NV=" & KeyGroup.Ma_NV & " And So_BT= " & KeyGroup.So_BT & _
                             " And Ma_Dthu='" & KeyGroup.Ma_Dthu & "'"
                    cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                End If
                tranTT.Commit()
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình xóa số thực thu của nhóm: " & ex.ToString)
                If Not tranTT Is Nothing Then
                    tranTT.Rollback()
                End If
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình xóa số thực thu của nhóm")
                Throw ex
            Finally
                If Not tranTT Is Nothing Then tranTT.Dispose()
                If Not cnTT Is Nothing Then cnTT.Dispose()
            End Try
            Return True
        End Function
        'ICB: xóa Số thực thu
        'Hoàng Văn Anh
        Public Function Delete_STThu(ByVal Key As KeyKhoQuy)
            Dim cnTT As DataAccess
            Dim strSql As String
            Dim tranTT As IDbTransaction
            Try
                cnTT = New DataAccess
                tranTT = cnTT.BeginTransaction
                strSql = "Delete From TCS_KhoQuy_Dtl " & _
                    "Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                    " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                    " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                ' Xóa trong bảng header
                strSql = "Delete From TCS_KhoQuy_Hdr " & _
                         "Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                         " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                         " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                'Cập nhật lại trong bảng tcs_ctu_hdr
                strSql = "Update TCS_CTU_HDR set Ma_TQ=0,TT_TTHU=0 " & _
                                "Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                                " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                                " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                tranTT.Commit()
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình xóa thực thu cho chứng từ: " & ex.ToString)
                If Not tranTT Is Nothing Then
                    tranTT.Rollback()
                End If
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình xóa thực thu cho chứng từ")
                Throw ex
            Finally
                If Not tranTT Is Nothing Then tranTT.Dispose()
                If Not cnTT Is Nothing Then cnTT.Dispose()
            End Try

        End Function
        Public Function Update_CTKQ(ByVal Key As KeyKhoQuy, ByVal dblTTien As Double, _
                               ByVal strLoaiDL As String, ByVal bytGroup As Byte, _
                               Optional ByVal arrLTien() As infLoaiTien = Nothing) As Boolean
            '----------------------------------------------------------------------------
            ' Mục đích : Ghi dữ liệu kho quỹ
            ' Đầu vào  : Key - Bộ khóa của chứng từ
            ' dblTtien : Tổng tiền thực thu
            ' strLoaiDL: Loại dữ liệu CT - chứng từ, BL - Biên lai
            ' bytGroup : Xác định tồn tại của dữ liệu CT: 0 - Chưa có trong bảng kho quỹ
            '                                             1 - Tồn tại trong bảng kho quỹ
            ' arrLTien : Mảng chứa dữ liệu các loại tiền
            '            Nếu là nothing: --> Nhập theo tổng tiền
            '            else            --> Nhập theo mệnh giá
            '-----------------------------------------------------------------------------
            Dim cnTT As DataAccess
            Dim strSql As String
            Dim tranTT As IDbTransaction
            Dim i As Integer
            Dim objLT As infLoaiTien
            Try
                cnTT = New DataAccess
                tranTT = cnTT.BeginTransaction

                ' Nếu có trong bảng khoquy thì xóa trước khi cập nhật
                ' If bytGroup = 1 Then
                strSql = "Delete From TCS_KhoQuy_Dtl " & _
                         "Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                         " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                         " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                ' Xóa trong bảng header
                strSql = "Delete From TCS_KhoQuy_Hdr " & _
                         "Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                         " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                         " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)

                'End If
                ' Nếu nhập theo phân loại tiền thì insert vào bảng kho quỹ
                If Not arrLTien Is Nothing Then
                    ' Đưa vào bảng Hdr
                    strSql = "Insert Into Tcs_khoquy_hdr(shkb, ngay_kb,ma_nv, so_bt, ma_dthu) " & _
                             "Values('" & Key.SHKB & "'," & Key.Ngay_KB & "," & Key.Ma_NV & "," & _
                             Key.So_BT & ",'" & Key.Ma_Dthu & "')"
                    cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                    For Each objLT In arrLTien
                        ' Đưa vào bảng Hdr
                        strSql = "Insert Into Tcs_khoquy_dtl(shkb, ngay_kb,ma_nv, so_bt, ma_dthu,kyhieu_ct,so_ct,ma_loaitien,so_to) " & _
                                 "Values('" & Key.SHKB & "'," & Key.Ngay_KB & "," & Key.Ma_NV & "," & _
                                 Key.So_BT & ",'" & Key.Ma_Dthu & "','" & Key.Kyhieu_CT & "','" & Key.So_CT & "','" & _
                                 objLT.MaLoaiTien & "'," & objLT.SoTo & ")"
                        cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                    Next
                End If

                ' Cập nhật trong bảng TCS_CTu_HDR,Tcs_CTBL
                If "CT".Equals(strLoaiDL) Then
                    ' Cập nhật chứng từ
                    strSql = "Update TCS_CTU_HDR Set Ma_TQ =" & gstrTenND & ", " & _
                             " tt_tthu= " & dblTTien & _
                             " Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                             " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                             " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                    cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                Else
                    ' Cập nhật biên lai
                    strSql = "Update TCS_CTBL Set Ma_TQ =" & gstrTenND & ", " & _
                             " tt_tthu= " & dblTTien & _
                             " Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                             " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                             " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                    cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                End If
                tranTT.Commit()
            Catch ex As Exception
                'LogDebug.Writelog("Lỗi trong quá trình ghi thực thu cho chứng từ: " & ex.ToString)
                If Not tranTT Is Nothing Then
                    tranTT.Rollback()
                End If
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình ghi thực thu cho chứng từ")
                Throw ex
            Finally
                If Not tranTT Is Nothing Then tranTT.Dispose()
                If Not cnTT Is Nothing Then cnTT.Dispose()
            End Try
            Return True
        End Function
        Public Function Get_LoaiTienKQ(ByVal keyCT As KeyKhoQuy) As DataSet
            Dim cnTC As DataAccess
            Dim strSql As String
            Dim ds As DataSet
            Try
                cnTC = New DataAccess
                strSql = "Select ma_loaitien,so_to From tcs_khoquy_dtl " & _
                         "Where  SHKB='" & keyCT.SHKB & "' And Ngay_KB= " & keyCT.Ngay_KB & _
                         " And Ma_NV=" & keyCT.Ma_NV & " And So_BT= " & keyCT.So_BT & _
                         " And Ma_Dthu='" & keyCT.Ma_Dthu & "' And So_to <>0 "
                ds = cnTC.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách tiền trong bảng kho quỹ")
                'LogDebug.Writelog("Lỗi trong quá trình lấy danh sách tiền trong bảng kho quỹ: " & ex.ToString)
                Throw ex
            Finally
                If Not cnTC Is Nothing Then cnTC.Dispose()
            End Try
        End Function
        Public Function Get_TTienGroup(ByVal KeyGroup As KeyKhoQuy) As Double
            '---------------------------------------------------------------
            ' Mục đích: Lấy tổng tiền của cả nhóm
            ' Đầu vào : Key của nhóm
            ' Đầu ra  : Tổng tiền tính được
            ' Người viết: Nguyễn Hữu Hoan
            '---------------------------------------------------------------
            Dim cnTC As DataAccess
            Dim strSql As String
            Dim ds As DataSet
            Dim dblResult As Double = 0
            Try
                cnTC = New DataAccess
                strSql = "Select sum(kq.so_to*lt.menh_gia) as TTien " & _
                         "From tcs_khoquy_dtl kq,tcs_dm_loaitien lt " & _
                         "Where kq.ma_loaitien = lt.ma_loaitien " & _
                         "And kq.SHKB='" & KeyGroup.SHKB & "' And kq.Ngay_KB= " & KeyGroup.Ngay_KB & _
                         " And kq.Ma_NV=" & KeyGroup.Ma_NV & " And kq.So_BT= " & KeyGroup.So_BT & _
                         " And kq.Ma_Dthu='" & KeyGroup.Ma_Dthu & "'"
                ds = cnTC.ExecuteReturnDataSet(strSql, CommandType.Text)
                If IsEmptyDataSet(ds) Then
                    dblResult = 0
                Else
                    dblResult = CDbl(ds.Tables(0).Rows(0).Item(0))
                End If
                Return dblResult
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình tính tổng tiền của nhóm chứng từ")
                'LogDebug.Writelog("Lỗi trong quá trình tính tổng tiền của nhóm chứng từ: " & ex.ToString)
                Throw ex
            Finally
                If Not cnTC Is Nothing Then cnTC.Dispose()
            End Try
        End Function
#End Region

        Public Function Get_SaiSo(ByVal Key As KeyKhoQuy, ByVal strLoaiDL As String) As Double
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy số thực thu của chứng từ
            ' Tham số       : 
            ' Giá trị trả về: 
            ' Ngày viết     : 23/10/2007
            ' Người viết    : Lê anh Ngọc
            ' ----------------------------------------------------------------
            '*****************************************************************
            Dim conn As DataAccess
            Dim strSql As String = ""
            Dim dr As IDataReader
            Dim dblTT As Double = 0
            Try
                conn = New DataAccess
                If "CT".Equals(strLoaiDL) Then
                    strSql = "Select tt_tthu-Ttien from TCS_CTU_HDR " & _
                             "Where SHKB='" & Key.SHKB & "' And Ngay_KB=" & Key.Ngay_KB & _
                             " And Ma_NV=" & Key.Ma_NV & " And So_BT=" & Key.So_BT & " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                Else
                    strSql = "Select tt_tthu-Ttien from TCS_CTBL " & _
                             "Where SHKB='" & Key.SHKB & "' And Ngay_KB=" & Key.Ngay_KB & _
                             " And Ma_NV=" & Key.Ma_NV & " And So_BT=" & Key.So_BT & " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                End If
                dr = conn.ExecuteDataReader(strSql, CommandType.Text)
                If dr.Read() Then
                    dblTT = CDbl(dr(0))
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy số tiền của chứng từ")
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình lấy số tiền của chứng từ: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not dr Is Nothing Then
                    dr.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return dblTT
        End Function
        Public Function Get_TT(ByVal Key As KeyKhoQuy, ByVal strLoaiDL As String) As Double
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy số thực thu của chứng từ
            ' Tham số       : 
            ' Giá trị trả về: 
            ' Ngày viết     : 23/10/2007
            ' Người viết    : Lê anh Ngọc
            ' ----------------------------------------------------------------
            '*****************************************************************
            Dim conn As DataAccess
            Dim strSql As String = ""
            Dim dr As IDataReader
            Dim dblTT As Double = 0
            Try
                conn = New DataAccess
                If "CT".Equals(strLoaiDL) Then
                    strSql = "Select Ttien from TCS_CTU_HDR Where SHKB='" & Key.SHKB & "' And Ngay_KB=" & Key.Ngay_KB & _
                             " And Ma_NV=" & Key.Ma_NV & " And So_BT=" & Key.So_BT & " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                Else
                    strSql = "Select Ttien from TCS_CTBL Where SHKB='" & Key.SHKB & "' And Ngay_KB=" & Key.Ngay_KB & _
                             " And Ma_NV=" & Key.Ma_NV & " And So_BT=" & Key.So_BT & " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                End If
                dr = conn.ExecuteDataReader(strSql, CommandType.Text)
                If dr.Read() Then
                    dblTT = CDbl(dr(0))
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy số tiền của chứng từ")
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình lấy số tiền của chứng từ: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not dr Is Nothing Then
                    dr.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return dblTT
        End Function
    End Class

End Namespace