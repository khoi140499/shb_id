﻿Imports VBOracleLib
Imports System.Data
Imports Business.Common.mdlSystemVariables
Imports Business.Common
Imports Business.Common.mdlCommon
Public Class daTraCuuTThu
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public Function Get_CT_Group(ByVal keyGroup As KeyKhoQuy, ByRef arrKHCT() As String, ByRef arrSoCT() As String, Optional ByRef arrNVBT() As String = Nothing) As Boolean
        ' -----------------------------------------------------------------------
        ' Mục đích: Lấy danh sách các chứng từ đã nhập gộp
        ' Đầu vào: Khóa chính của các chứng từ trong bảng kho quỹ dtl
        ' Đầu ra : Mảng các khct và số ct
        ' Người viết: Nguyễn Hữu Hoan
        ' Ngày viết: 19/12/2007
        '-------------------------------------------------------------------------
        Dim cnTT As DataAccess
        Dim strSql As String
        Dim ds As DataSet
        Dim i As Integer = 0
        Try
            cnTT = New DataAccess
            strSql = "Select distinct kq.kyhieu_ct,kq.So_CT,ct.ma_nv,ct.so_bt From tcs_khoquy_dtl kq,tcs_ctu_hdr ct " & _
                     "Where kq.SHKB='" & keyGroup.SHKB & "' And kq.Ngay_KB= " & keyGroup.Ngay_KB & _
                     " And kq.Ma_NV=" & keyGroup.Ma_NV & " And kq.So_BT= " & keyGroup.So_BT & _
                     " And kq.Ma_Dthu='" & keyGroup.Ma_Dthu & "'" & _
                     " And kq.kyhieu_ct=ct.kyhieu_ct And kq.so_ct=ct.so_ct "
            ds = cnTT.ExecuteReturnDataSet(strSql, CommandType.Text)
            If Not IsEmptyDataSet(ds) Then
                i = ds.Tables(0).Rows.Count - 1
                ReDim arrKHCT(i)
                ReDim arrSoCT(i)
                ReDim arrNVBT(i)
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    arrKHCT(i) = ds.Tables(0).Rows(i).Item(0)
                    arrSoCT(i) = ds.Tables(0).Rows(i).Item(1)
                    arrNVBT(i) = ds.Tables(0).Rows(i).Item("MA_NV").ToString().PadLeft(3, "0") & "/" & ds.Tables(0).Rows(i).Item("SO_BT").ToString().PadLeft(4, "0")
                Next
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách chứng từ trong nhóm")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách chứng từ trong nhóm: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnTT Is Nothing Then cnTT.Dispose()
        End Try
        Return True
    End Function

    Public Function Get_BL_Group(ByVal keyGroup As KeyKhoQuy, ByRef arrKHCT() As String, ByRef arrSoCT() As String, Optional ByRef arrNVBT() As String = Nothing) As Boolean
        ' -----------------------------------------------------------------------
        ' Mục đích: Lấy danh sách các biên lại đã nhập gộp
        ' Đầu vào: Khóa chính của các biên lai trong bảng kho quỹ dtl
        ' Đầu ra : Mảng các khct và số ct
        ' Người viết: Nguyễn Hữu Hoan
        ' Ngày viết: 19/12/2007
        '-------------------------------------------------------------------------
        Dim cnTT As DataAccess
        Dim strSql As String
        Dim ds As DataSet
        Dim i As Integer = 0
        Try
            cnTT = New DataAccess
            strSql = "Select distinct kq.kyhieu_ct,kq.So_CT,ct.ma_nv,ct.so_bt From tcs_khoquy_dtl kq,tcs_ctbl ct " & _
                     "Where kq.SHKB='" & keyGroup.SHKB & "' And kq.Ngay_KB= " & keyGroup.Ngay_KB & _
                     " And kq.Ma_NV=" & keyGroup.Ma_NV & " And kq.So_BT= " & keyGroup.So_BT & _
                     " And kq.Ma_Dthu='" & keyGroup.Ma_Dthu & "'" & _
                     " And kq.kyhieu_ct=ct.kyhieu_bl And kq.so_ct=ct.so_bl "
            ds = cnTT.ExecuteReturnDataSet(strSql, CommandType.Text)
            If Not IsEmptyDataSet(ds) Then
                i = ds.Tables(0).Rows.Count - 1
                ReDim arrKHCT(i)
                ReDim arrSoCT(i)
                ReDim arrNVBT(i)
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    arrKHCT(i) = ds.Tables(0).Rows(i).Item(0)
                    arrSoCT(i) = ds.Tables(0).Rows(i).Item(1)
                    arrNVBT(i) = ds.Tables(0).Rows(i).Item("MA_NV").ToString().PadLeft(3, "0") & "/" & ds.Tables(0).Rows(i).Item("SO_BT").ToString().PadLeft(4, "0")
                Next
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy danh sách chứng từ trong nhóm")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy danh sách chứng từ trong nhóm: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnTT Is Nothing Then cnTT.Dispose()
        End Try
        Return True
    End Function

    Public Function Xoa_CT_Group(ByVal KeyGroup As KeyKhoQuy, ByVal arrKHCT() As String, ByVal arrSoCT() As String, ByVal strLoaiDL As String) As Boolean
        ' -----------------------------------------------------------------------
        ' Mục đích: Xóa, cập nhật các chứng từ khi xóa thực thu của nhóm
        ' Đầu vào: keygroup,arrkhct,arrsoct,strloaidl
        ' Đầu ra : 
        ' Người viết: Nguyễn Hữu Hoan
        ' Ngày viết: 19/12/2007
        '-------------------------------------------------------------------------
        Dim cnTT As DataAccess
        Dim strSql As String
        Dim tranTT As IDbTransaction
        Dim i As Integer
        Try
            cnTT = New DataAccess
            tranTT = cnTT.BeginTransaction
            ' Xóa trong bảng kho quỹ theo key group
            ' Xóa detail
            strSql = "Delete From TCS_KhoQuy_Dtl " & _
                     "Where SHKB='" & KeyGroup.SHKB & "' And Ngay_KB= " & KeyGroup.Ngay_KB & _
                     " And Ma_NV=" & KeyGroup.Ma_NV & " And So_BT= " & KeyGroup.So_BT & _
                     " And Ma_Dthu='" & KeyGroup.Ma_Dthu & "'"
            cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
            ' Xóa trong bảng header
            strSql = "Delete From TCS_KhoQuy_Hdr " & _
                     "Where SHKB='" & KeyGroup.SHKB & "' And Ngay_KB= " & KeyGroup.Ngay_KB & _
                     " And Ma_NV=" & KeyGroup.Ma_NV & " And So_BT= " & KeyGroup.So_BT & _
                     " And Ma_Dthu='" & KeyGroup.Ma_Dthu & "'"
            cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
            ' Cập nhật trong bảng TCS_CTu_HDR,Tcs_CTBL
            If "CT".Equals(strLoaiDL) Then
                ' Cập nhật chứng từ
                For i = 0 To arrKHCT.Length - 1
                    strSql = "Update TCS_CTU_HDR Set Ma_TQ = 0,tt_tthu=0 " & _
                             "Where kyhieu_ct='" & arrKHCT(i) & "' " & _
                             "And So_CT = '" & arrSoCT(i) & "' And Trang_thai <>'04' "
                    cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                Next
            Else
                ' Cập nhật biên lai
                For i = 0 To arrKHCT.Length - 1
                    strSql = "Update TCS_CTBL Set Ma_TQ = 0,tt_tthu=0 " & _
                                                 "Where kyhieu_bl='" & arrKHCT(i) & "' " & _
                                                 "And So_Bl = '" & arrSoCT(i) & "' And Trang_thai <>'03' "
                    cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
                Next
            End If
            tranTT.Commit()
        Catch ex As Exception
            'LogDebug.Writelog("Lỗi trong quá trình xóa số thực thu của nhóm: " & ex.ToString)
            If Not tranTT Is Nothing Then
                tranTT.Rollback()
            End If
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình xóa số thực thu của nhóm")
            Throw ex
        Finally
            If Not tranTT Is Nothing Then tranTT.Dispose()
            If Not cnTT Is Nothing Then cnTT.Dispose()
        End Try
        Return True
    End Function
    Public Function Xoa_CT(ByVal Key As KeyKhoQuy, ByVal strLoaiDL As String) As Boolean
        Dim cnTT As DataAccess
        Dim strSql As String
        Dim tranTT As IDbTransaction
        Dim i As Integer
        Try
            cnTT = New DataAccess
            tranTT = cnTT.BeginTransaction
            ' Xóa trong bảng kho quỹ theo key group
            ' Xóa detail
            strSql = "Delete From TCS_KhoQuy_Dtl " & _
                     "Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                     " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                     " And Ma_Dthu='" & Key.Ma_Dthu & "'"
            cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
            ' Xóa trong bảng header
            strSql = "Delete From TCS_KhoQuy_Hdr " & _
                     "Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                     " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                     " And Ma_Dthu='" & Key.Ma_Dthu & "'"
            cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
            ' Cập nhật trong bảng TCS_CTu_HDR,Tcs_CTBL
            If "CT".Equals(strLoaiDL) Then
                ' Cập nhật chứng từ
                strSql = "Update TCS_CTU_HDR Set Ma_TQ = 0,tt_tthu=0 " & _
                         "Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                         " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                         " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
            Else
                ' Cập nhật biên lai
                strSql = "Update TCS_CTBL Set Ma_TQ = 0,tt_tthu=0 " & _
                         "Where SHKB='" & Key.SHKB & "' And Ngay_KB= " & Key.Ngay_KB & _
                         " And Ma_NV=" & Key.Ma_NV & " And So_BT= " & Key.So_BT & _
                         " And Ma_Dthu='" & Key.Ma_Dthu & "'"
                cnTT.ExecuteNonQuery(strSql, CommandType.Text, tranTT)
            End If
            tranTT.Commit()
        Catch ex As Exception
            'LogDebug.Writelog("Lỗi trong quá trình xóa số thực thu: " & ex.ToString)
            If Not tranTT Is Nothing Then
                tranTT.Rollback()
            End If
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình xóa số thực thu")
            Throw ex
        Finally
            If Not tranTT Is Nothing Then tranTT.Dispose()
            If Not cnTT Is Nothing Then cnTT.Dispose()
        End Try
        Return True
    End Function
    Public Function GetKeys(ByVal strKHCT As String, ByVal strSoCT As String, ByVal strDL As String) As KeyKhoQuy
        Dim obj As New KeyKhoQuy
        Dim cnTT As DataAccess
        Dim strSql As String
        Dim ds As New DataSet
        Try
            cnTT = New DataAccess
            If "CT".Equals(strDL) Then
                strSql = "Select ct.SHKB,ct.Ngay_KB,ct.Ma_NV,ct.So_BT,ct.Ma_Dthu From tcs_ctu_hdr ct,(SELECT GIATRI_TS,Ma_Dthu FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV') T " & _
                         "Where ct.kyhieu_ct = '" & strKHCT & "' And ct.So_CT = '" & strSoCT & "' And ct.trang_thai<>'04' AND ct.NGAY_KB= to_char(t.GIATRI_TS,'yyyyMMdd') and t.Ma_Dthu=ct.ma_Dthu" ' & gdtmNgayLV
            Else
                strSql = "Select ct.SHKB,ct.Ngay_KB,ct.Ma_NV,ct.So_BT,ct.Ma_Dthu From tcs_ctbl ct,(SELECT GIATRI_TS,Ma_Dthu FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV') T  " & _
                         "Where ct.kyhieu_bl = '" & strKHCT & "' And ct.So_BL = '" & strSoCT & "' And ct.trang_thai<>'03' AND  ct.NGAY_KB= to_char(t.GIATRI_TS,'yyyyMMdd') and t.Ma_Dthu=ct.ma_Dthu"
            End If
            ds = cnTT.ExecuteReturnDataSet(strSql, CommandType.Text)
            If Not IsEmptyDataSet(ds) Then
                obj.SHKB = ds.Tables(0).Rows(0).Item(0)
                obj.Ngay_KB = ds.Tables(0).Rows(0).Item(1)
                obj.Ma_NV = ds.Tables(0).Rows(0).Item(2)
                obj.So_BT = ds.Tables(0).Rows(0).Item(3)
                obj.Ma_Dthu = ds.Tables(0).Rows(0).Item(4)
            End If
            Return obj
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy key cho kho quỹ")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy key cho kho quỹ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not cnTT Is Nothing Then
                cnTT.Dispose()
            End If
        End Try
    End Function
    Public Function GetKey_Group(ByVal strKyhieuCT As String, ByVal strSoCT As String) As KeyKhoQuy
        '-----------------------------------------------------
        ' Mục đích      : Lấy Key của nhóm CT trong bảng kho quỹ
        ' Tham số       : 
        ' Giá trị trả về:
        ' Ngày viết     : 23/10/2007
        ' Người viết    : Lê Anh Ngọc
        ' ----------------------------------------------------
        Dim conn As DataAccess
        Dim strSql As String
        Dim KeyGroup As New KeyKhoQuy
        Dim ds As DataSet
        Try
            strSql = "Select distinct SHKB,Ngay_KB,Ma_NV,So_BT,Ma_Dthu, Kyhieu_CT,So_CT " & _
                     "From TCS_KHOQUY_DTL Where Kyhieu_CT='" & strKyhieuCT & "' and So_CT='" & strSoCT & "'"
            conn = New DataAccess
            ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            If IsEmptyDataSet(ds) Then
                Return Nothing
            Else
                KeyGroup.SHKB = ds.Tables(0).Rows(0).Item(0)
                KeyGroup.Ngay_KB = ds.Tables(0).Rows(0).Item(1)
                KeyGroup.Ma_NV = ds.Tables(0).Rows(0).Item(2)
                KeyGroup.So_BT = ds.Tables(0).Rows(0).Item(3)
                KeyGroup.Ma_Dthu = ds.Tables(0).Rows(0).Item(4)
                KeyGroup.Kyhieu_CT = strKyhieuCT
                KeyGroup.So_CT = strSoCT
            End If
            Return KeyGroup
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra trong khi truy vấn dữ liệu kho quỹ")
            Throw ex
            'LogDebug.WriteLog("Có lỗi xảy ra trong khi truy vấn dữ liệu kho quỹ: " & ex.ToString, EventLogEntryType.Error)
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function
    Public Function DSSoThucThu(ByVal strSql As String) As DataSet
        '-----------------------------------------------------------------
        ' Mục đích      : Lấy danh sách số thực thu
        ' Tham số       : 
        ' Giá trị trả về: dataset
        ' Ngày viết     : 19/10/2007
        ' Người viết    : Lê anh Ngọc
        '---------------------------------------------------------
        Dim conn As DataAccess
        Dim ds As DataSet
        Try
            conn = New DataAccess
            ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra trong quá trình lấy DS số thực thu")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy DS số thực thu: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function
    Public Function Get_ThuQuy(ByVal sql As String) As DataSet
        Dim conn As DataAccess
        Dim ds As DataSet
        Try
            conn = New DataAccess
            ds = New DataSet
            ds = conn.ExecuteReturnDataSet(sql, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra trong quá trình lấy DS thủ quỹ")
            'LogDebug.WriteLog("Lỗi trong quá trình lấy DS thủ quỹ: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function
    Public Function CheckGopCT(ByVal KeyG As KeyKhoQuy) As DataSet
        '-----------------------------------------------------
        ' Mục đích      : Kiểm tra và lấy dữ liệu về các CT được gộp với CT KeyG 
        ' Tham số       : KeyG Key của nhóm CT gộp 
        ' Giá trị trả về:
        '       - Dataset chua số CT va  ký hiệu CT được gộp chung
        ' Ngày viết     : 23/10/2007
        ' Người viết    : Lê Anh Ngọc
        ' ----------------------------------------------------
        Dim conn As DataAccess
        Dim strSql As String
        Dim ds As DataSet
        Try
            conn = New DataAccess
            strSql = "Select distinct Kyhieu_CT,So_CT from TCS_KHOQUY_DTL where SHKB='" & KeyG.SHKB & _
                     "' And Ngay_KB=" & KeyG.Ngay_KB & " And Ma_NV=" & KeyG.Ma_NV & " and So_BT=" & KeyG.So_BT & " and Ma_Dthu='" & KeyG.Ma_Dthu & "'"
            ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            Return ds
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi xảy ra trong quá trình kiểm tra gộp chứng từ")
            'LogDebug.WriteLog("Có lỗi xảy ra trong quá trình kiểm tra gộp chứng từ: -" & ex.ToString, EventLogEntryType.Error)
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function
#Region "BK menh giá tien"
    Public Function BKPLTM(ByVal strWhere As String) As DataSet
        ' Mục đích: Lấy dữ liệu bảng kê số tiền thực thu trongngày theo mệnh giá
        ' Tham số:
        ' người viết: Ngocla
        ' Ngày viết: 10/12/07
        '---------------------------
        Dim conn As DataAccess
        Try
            conn = New DataAccess
            ' truy van tren bang ct
            Dim sql As String
            sql = "SELECT lt.menh_gia as MenhGia, SUM (kq.so_to) AS so_to,(lt.menh_gia * SUM (kq.so_to)) AS tien " & _
                  "FROM tcs_dm_loaitien lt, tcs_khoquy_dtl kq,tcs_ctu_hdr ct " & _
                  "WHERE (kq.ma_loaitien = lt.ma_loaitien) " & _
                  "And (kq.shkb=ct.shkb) And (kq.ngay_kb=ct.ngay_kb) And (kq.ma_nv=ct.ma_nv) " & _
                  "And (kq.ma_dthu=ct.ma_dthu) And (kq.so_bt=ct.so_bt) " & strWhere & _
                  " Group by menh_gia"
            Return conn.ExecuteReturnDataSet(sql, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi xảy ra trong quá trình lấy dữ liệu bản kê phân loại tiền mặt")
            'LogDebug.WriteLog("Lỗi khi lấy dữ liệu bản kê phân loại tiền mặt: " & ex.ToString, EventLogEntryType.Error)
            Throw ex
        Finally
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
    End Function
#End Region
End Class
