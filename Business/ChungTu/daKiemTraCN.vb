﻿Imports VBOracleLib
Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Namespace ChungTu
    Public Class daKiemTraCN
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function lstHeaderCT(ByVal strWhere As String, ByVal strDK As String) As DataSet

            Dim strSelectDBHC As String
            Dim strSelectMLNS As String
            Dim strSelectALL As String



            'Dim strSQL As String
            'strSQL = "select distinct hdr.SHKB,hdr.Ngay_KB,hdr.Ma_NV,hdr.So_BT,hdr.Ma_DThu," & _
            '         "hdr.KyHieu_CT,hdr.So_CT,hdr.Ma_CQThu,hdr.Ma_Tinh," & _
            '         "hdr.Ma_Huyen,hdr.Ma_Xa,hdr.Ma_LThue,hdr.TTien," & _
            '         "hdr.Lan_In,hdr.Trang_Thai " & _
            '         "From TCS_CTU_HDR Hdr,TCS_DM_TaiKhoan TK  Where (Trang_Thai <> '04') " & strWhere
            'Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

            strSelectDBHC = "SELECT DISTINCT to_char(hdr.ngay_kb) AS ngay_kb, hdr.kyhieu_ct AS kyhieu_ct, " & _
                        "hdr.so_ct AS so_ct, (lpad(hdr.ma_nv, 3, '0') ||'/'|| lpad(hdr.so_bt, 5, '0')) AS manv_sobt, " & _
                        "hdr.ma_xa ||'-'||hdr.tk_co||'-'|| hdr.ma_cqthu AS thongtinsai, " & _
                        "tk.dbhc ||'-'||tk.tk ||'-'|| tk.ma_cqthu AS thongtindung " & _
                        "From TCS_CTU_HDR Hdr,TCS_DM_TaiKhoan TK, tcs_ctu_dtl dtl  " & _
                        "Where (hdr.Trang_Thai <> '04') " & _
                        "AND (hdr.shkb = dtl.shkb) " & _
                        "AND (hdr.ngay_kb = dtl.ngay_kb) " & _
                        "AND (hdr.ma_nv = dtl.ma_nv) " & _
                        "AND (hdr.so_bt = dtl.so_bt) " & _
                        "AND (hdr.ma_dthu = dtl.ma_dthu)" & _
                        "And (Hdr.Tk_co=TK.TK) AND (TRIM (tk.dbhc) <> '' OR NOT tk.dbhc IS NULL) AND (TRIM (tk.ma_cqthu) <> '' OR NOT tk.ma_cqthu IS NULL) AND ((hdr.ma_xa||hdr.ma_cqthu) <> tk.dbhc||tk.ma_cqthu) "
            strSelectDBHC = strSelectDBHC & strWhere
            'Xử lý cho MLNS
            strSelectMLNS = " SELECT   to_char(hdr.ngay_kb) AS Ngay_KB, hdr.kyhieu_ct AS kyhieu_ct, hdr.so_ct as so_ct , (lpad(hdr.ma_nv, 3, '0') ||'/'|| lpad(hdr.so_bt, 5, '0')) as MaNV_SoBT, " & _
                            "       ( dtl.ma_chuong||'.'||dtl.ma_khoan||'.'||dtl.ma_tmuc||'-'||dtl.madt) AS thongtinsai, " & _
                            "        (   (dtl.ma_chuong || '.' || dtl.ma_khoan || '.' || dtl.ma_tmuc|| '-') " & _
                            "       || (SELECT tldt.madt AS madt " & _
                            "             FROM tcs_dm_tldt tldt, tcs_dm_dieutiet dmdt " & _
                            "             WHERE (tldt.madt = dmdt.madt) " & _
                            "               AND (tldt.tinh_trang = '1') " & _
                            "               AND (dmdt.tu_chuong <= dtl.ma_chuong) " & _
                            "                AND (dmdt.den_chuong >= dtl.ma_chuong) " & _
                            "                 AND (dmdt.tu_lk <= dtl.ma_khoan) " & _
                            "                 AND (dmdt.den_lk >= dtl.ma_khoan) " & _
                            "                AND (dmdt.tu_muc <= dtl.ma_tmuc) " & _
                            "                 AND (dmdt.den_muc >= dtl.ma_tmuc) " & _
                            "                 AND (ROWNUM <= 1)) " & _
                            "         ) AS thongtindung " & _
                            "    FROM tcs_ctu_hdr hdr, tcs_ctu_dtl dtl " & _
                            "   WHERE (hdr.shkb = dtl.shkb) " & _
                            "     AND (hdr.ngay_kb = dtl.ngay_kb) " & _
                            "     AND (hdr.ma_nv = dtl.ma_nv) " & _
                            "     AND (hdr.so_bt = dtl.so_bt) " & _
                            "     AND (hdr.ma_dthu = dtl.ma_dthu) " & _
                            "     AND (TRIM (dtl.ma_chuong || dtl.ma_khoan || dtl.ma_tmuc) IS NOT NULL) " & _
                            "     AND (dtl.madt NOT IN ( " & _
                            "             SELECT tldt.madt AS madt " & _
                            "               FROM tcs_dm_tldt tldt, tcs_dm_dieutiet dmdt " & _
                            "              WHERE (tldt.madt = dmdt.madt) " & _
                            "                AND (tldt.tinh_trang = '1') " & _
                            "                AND (dmdt.tu_chuong <= dtl.ma_chuong) " & _
                            "                AND (dmdt.den_chuong >= dtl.ma_chuong) " & _
                            "                AND (dmdt.tu_lk <= dtl.ma_khoan) " & _
                            "                AND (dmdt.den_lk >= dtl.ma_khoan) " & _
                            "                AND (dmdt.tu_muc <= dtl.ma_tmuc) " & _
                            "                AND (dmdt.den_muc >= dtl.ma_tmuc))) "
            strSelectMLNS = strSelectMLNS & strWhere
            'Xử lý cho DBHC-TK_Co-CQThu và MLNS
            strSelectALL = strSelectDBHC & " UNION " & strSelectMLNS
            strSelectDBHC = strSelectDBHC & "ORDER BY hdr.ngay_kb, MaNV_SoBT"
            strSelectMLNS = strSelectMLNS & "ORDER BY hdr.ngay_kb, MaNV_SoBT"
            strSelectALL = strSelectALL & "order by ngay_kb, MaNV_SoBT"

            Try

                Dim strSQL As String
                If strDK = "DBHC" Then
                    strSQL = strSelectDBHC
                ElseIf strDK = "MLNS" Then
                    strSQL = strSelectMLNS
                Else
                    strSQL = strSelectALL
                End If
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)

            Catch ex As Exception
                ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy thông tin chứng từ")
                log.Error(ex.Message & "-" & ex.StackTrace)

                'LogDebug.WriteLog("Lỗi trong quá trình lấy thông tin chứng từ sai: " & ex.ToString(), EventLogEntryType.Error)
                Throw ex
            Finally
                'If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
    End Class

End Namespace