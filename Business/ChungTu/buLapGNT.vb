﻿Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables

Namespace ChungTu
    Public Class buLapGNT
        Public Shared Function TCS_GetLyDo(ByVal strWhere As String) As String
            Dim objda As New daLapGNT
            Return objda.TCS_GetLyDo(strWhere)
        End Function
        Public Shared Function TCS_Noidung(ByVal strWhere As String) As String
            Dim objda As New daLapGNT
            Return objda.TCS_GetNoidung(strWhere)
        End Function
        Public Shared Function GNTtuBLT(ByVal strWhere) As DataSet
            Dim objda As New daLapGNT
            Return objda.TCS_dsGNT(strWhere)
        End Function
        Public Shared Function SelectBL(ByVal strWhere As String) As DataSet
            Dim objda As New daLapGNT
            Return objda.SelectBL(strWhere)
        End Function
        'Public Shared Function LayDsCTLap(ByVal maNV As Integer) As DataSet
        '    Dim objda As New daLapGNT
        '    Return objda.SelectCTLap(maNV)
        'End Function
        Public Shared Function TTChiTietGNT(ByVal strWhere As String) As DataSet
            Dim objda As New daLapGNT
            Dim ds As DataSet = objda.TCS_dsGNT(strWhere)
            Return ds
        End Function
        Public Shared Function DsBK_ThuTP(ByVal strwhere As String)
            Dim objda As New daLapGNT
            Dim ds As DataSet = objda.dsBKThuTP(strwhere)
            If Not ds Is Nothing Then
                Return ds
            End If
        End Function
        Public Shared Function MaTLDT(ByVal sqlstr As String) As String
            Dim ds As New DataSet
            Dim objda As New daLapGNT
            ds = objda.LayTLDT(sqlstr)
            Dim strReturn As String = ""
            If ds.Tables(0).Rows.Count > 0 Then
                strReturn = ds.Tables(0).Rows(0).Item("Ma_TLDT")
            End If
            Return strReturn
        End Function
    End Class
End Namespace