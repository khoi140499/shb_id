﻿Imports VBOracleLib
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data.OleDb
Imports Business.NewChungTu

Namespace ChungTu
    Public Class daNNThue
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function SoThue_CTN(ByVal strMaNNT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong sổ thuế
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select s.ma_nnt, s.ten_nnt, s.dia_chi, s.so_qd, s.ngay_qd, " & _
                    "s.cq_qd, s.ma_tinh, s.ma_huyen, s.ma_xa, c.ma_cqthu, " & _
                    "s.kythue, s.tk_thu_ns, s.ma_cap, s.ma_chuong, s.ma_loai, " & _
                    "s.ma_khoan, s.ma_muc, s.ma_tmuc, s.so_phainop, s.tt_nop, c.shkb, s.loai_nnt, nt.ten_loai_nnt " & _
                    "from tcs_sothue s, tcs_dm_cqthu  c, tcs_dm_loai_nnt nt " & _
                    "where (s.ma_qlt = c.ma_cqthu) and s.loai_nnt = nt.ma_loai_nnt(+) and s.ma_nnt = '" & strMaNNT & "' " & _
                    "order by s.kythue desc"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy lý thông tin NN thuế trong sổ thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function SoThue_CTN(ByVal strMaNNT As String, ByVal strSoQD As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong sổ thuế
            ' Tham số: mã người nộp thuế, số quyết định
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select ma_nnt, ten_nnt, dia_chi, so_qd, ngay_qd, " & _
                    "cq_qd, ma_tinh, ma_huyen, ma_xa, ma_cqthu, " & _
                    "kythue, tk_thu_ns, ma_cap, ma_chuong, ma_loai, " & _
                    "ma_khoan, ma_muc, ma_tmuc, so_phainop, tt_nop " & _
                    "from tcs_sothue " & _
                    "where ma_nnt = '" & strMaNNT & "' " & _
                    "and so_qd = '" & strSoQD & "'" & _
                    "order by kythue desc"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) 'CTuCommon.WriteLog(ex, "Lỗi lấy thông tin NN thuế trong sổ thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function ToKhai(ByVal strMaNNT As String, ByVal strMaCQThu As String, ByVal strSoTK As String, ByVal strLHXNK As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong tờ khai Hải Quan
            ' Tham số: mã người nộp thuế, mã CQT, số tờ khai, LHXNK
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select ma_nnt, ten_nnt, dia_chi, lh_xnk, so_tk, " & _
                           "to_char(ngay_tk, 'dd/MM/yyyy') as ngay_tk, ma_tinh, ma_huyen, ma_xa, ma_cqthu, " & _
                           "tk_thu_ns, ma_kb, ma_cap, ma_chuong, ma_loai, " & _
                           "ma_khoan, ma_muc, ma_tmuc, so_phainop, tt_nop " & _
                           "from tcs_ds_tokhai " & _
                           "where (ma_nnt = '" & strMaNNT & "') " & _
                           "and (so_tk = '" & strSoTK & "') " & _
                           "and (lh_xnk = '" & strLHXNK & "') " & _
                           "and (trim(ma_hq) = '" & strMaCQThu & "') " & _
                           " order by ngay_tk desc"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin NN thuế trong tờ khai Hải Quan")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        ''' ******************************************************'
        ''' Name: To the khai.
        ''' Author: ANHLD
        ''' Goals: Loc to khai theo loai tien thue
        ''' Date: 21.09.2012
        ''' Event: Add
        ''' ************************BEGIN******************************'
        ''' <param name="strMaNNT">The STR ma NNT.</param>
        ''' <param name="strMaCQThu">The STR ma CQ thu.</param>
        ''' <param name="strSoTK">The STR so TK.</param>
        ''' <param name="strLHXNK">The STR LHXNK.</param>
        ''' <param name="strLtt">The STR LTT.</param>
        ''' <returns>DataSet.</returns>
        Public Function ToKhai(ByVal strMaNNT As String, ByVal strMaCQThu As String, ByVal strSoTK As String, ByVal strLHXNK As String, ByVal strLtt As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong tờ khai Hải Quan
            ' Tham số: mã người nộp thuế, mã CQT, số tờ khai, LHXNK
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select ma_nnt, ten_nnt, dia_chi, lh_xnk, so_tk, " & _
                           "to_char(ngay_tk, 'dd/MM/yyyy') as ngay_tk, ma_tinh, ma_huyen, ma_xa, ma_cqthu, " & _
                           "tk_thu_ns, ma_kb, ma_cap, ma_chuong, ma_loai,ma_hq, ten_hq, " & _
                           "ma_khoan, ma_muc, ma_tmuc, so_phainop, tt_nop, ma_hq_ph, ten_hq_ph,ma_lt " & _
                           "from tcs_ds_tokhai " & _
                           "where (ma_nnt = '" & strMaNNT & "') " & _
                           "and (so_tk = '" & strSoTK & "') " & _
                           "and (ma_lt = '" & strLtt & "') " & _
                           "and (lh_xnk = '" & strLHXNK & "') " & _
                           "and (trim(ma_hq) = '" & strMaCQThu & "') " & _
                           " order by ngay_tk desc"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lọc tờ khai theo loại tiền thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        ''' ************************END********************************'
        ''' 
        Public Function ToKhaiHQ(ByVal strMaNNT As String, ByVal strMaCQThu As String, ByVal strSoTK As String, ByVal strLHXNK As String, ByVal strLtt As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong tờ khai Hải Quan
            ' Tham số: mã người nộp thuế, mã CQT, số tờ khai, LHXNK
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select ma_nnt, ten_nnt, dia_chi, lh_xnk, so_tk, " & _
                           "to_char(ngay_tk, 'dd/MM/yyyy') as ngay_tk, ma_tinh, ma_huyen, ma_xa, ma_cqthu, " & _
                           "tk_thu_ns, ma_kb, ma_cap, ma_chuong, ma_loai,ma_hq,ten_hq, " & _
                           "ma_khoan, ma_muc, ma_tmuc, so_phainop, tt_nop, ma_hq_ph, ten_hq_ph, ma_lt " & _
                           "from tcs_ds_tokhai " & _
                           "where (ma_nnt = '" & strMaNNT & "') " & _
                           "and (so_tk = '" & strSoTK & "') " & _
                           "and (ma_lt = (select  ma_lt from tcs_ds_tokhai where ma_nnt='" & strMaNNT & "' and so_tk='" & strSoTK & "' and lh_xnk='" & strLHXNK & "' and TRIM(ma_hq)='" & strMaCQThu & "' and rownum=1)) " & _
                           "and (lh_xnk = '" & strLHXNK & "') " & _
                           "and (trim(ma_hq) = '" & strMaCQThu & "') " & _
                           " order by ngay_tk desc"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lọc tờ khai theo loại tiền thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function
        Public Function ToKhai(ByVal strMaNNT As String, ByVal strSoTK As String, ByVal strLoaiTT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong tờ khai Hải Quan
            ' Tham số: mã người nộp thuế, số tờ khai
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim strWhere As String = ""
            If strLoaiTT = "0" Then
                strWhere = "and (ma_lt = (select  ma_lt from tcs_ds_tokhai where ma_nnt='" & strMaNNT & "' and so_tk='" & strSoTK & "' and rownum=1)) "
            Else
                strWhere = "and (ma_lt ='" & strLoaiTT & "')"
            End If
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String

                strSQL = "select ma_nnt, ten_nnt, dia_chi, lh_xnk, so_tk, " & _
                               "to_char(ngay_tk, 'dd/MM/yyyy') as ngay_tk, ma_tinh, ma_huyen, ma_xa, ma_cqthu, " & _
                               "tk_thu_ns, ma_kb, ma_cap, ma_chuong, ma_loai,ma_hq,ten_hq, " & _
                               "ma_khoan, ma_muc, ma_tmuc, so_phainop, tt_nop, ma_hq_ph, ten_hq_ph, ma_lt, ma_ntk , '' as dien_giai " & _
                               "from tcs_ds_tokhai " & _
                               "where (ma_nnt = '" & strMaNNT & "') " & _
                               "and (so_tk = '" & strSoTK & "') " & strWhere & _
                               " order by ngay_tk desc"

                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lọc tờ khai theo loại tiền thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function ToKhai(ByVal strMaNNT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong tờ khai Hải Quan
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select ma_nnt, ten_nnt, dia_chi, lh_xnk, so_tk, " & _
                    "to_char(ngay_tk,'dd/mm/yyyy') as ngay_tk, ma_tinh, ma_huyen, ma_xa, ma_cqthu, " & _
                    "tk_thu_ns, ma_kb, ma_cap, ma_chuong, ma_loai, " & _
                    "ma_khoan, ma_muc, ma_tmuc, so_phainop, tt_nop, c.shkb " & _
                    "from tcs_ds_tokhai, tcs_dm_cqthu  c   " & _
                    "where (s.ma_cqthu = c.ma_qlt(+)) and ma_nnt = '" & strMaNNT & "'"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lọc tờ khai theo loại tiền thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function ToKhai(ByVal strMaNNT As String, ByVal strSoTK As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong tờ khai Hải Quan
            ' Tham số: mã người nộp thuế, số tờ khai
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                Dim where As String = ""
                If strSoTK <> "" Then
                    where = " and (so_tk = '" & strSoTK & "')"
                End If
                strSQL = "select ma_nnt, ten_nnt, dia_chi, lh_xnk, so_tk, " & _
                    "to_char(ngay_tk, 'dd/MM/yyyy') as NGAY_TK, ma_tinh, ma_huyen, ma_xa, ma_cqthu, " & _
                    "tk_thu_ns, ma_kb, ma_cap, ma_chuong, ma_loai,ma_hq, ten_hq, " & _
                    "ma_khoan, ma_muc, ma_tmuc, so_phainop, tt_nop, ma_hq_ph, ten_hq_ph,ma_lt " & _
                    "from tcs_ds_tokhai " & _
                    "where ma_nnt = '" & strMaNNT & "'" & where
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi lọc tờ khai theo loại tiền thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function NNThue(ByVal strMaNNT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong danh mục NNThuế
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select ma_nnt, ten_nnt, dia_chi, " & _
                    "tcs_dm_nnt.ma_tinh, tcs_dm_nnt.ma_huyen, tcs_dm_nnt.ma_xa, t.ma_cqthu, " & _
                    "ma_cap, ma_chuong, ma_loai, ma_khoan,t.shkb,A.MA_TRUCTIEP,A.TEN_TRUCTIEP,A.MA_GIANTIEP,A.TEN_GIANTIEP, tcs_dm_nnt.ma_loai_nnt loai_nnt, nt.ten_loai_nnt " & _
                    "from tcs_dm_nnt, tcs_dm_cqthu t, tcs_dm_nh_giantiep_tructiep A, tcs_dm_loai_nnt nt" & _
                    "where (tcs_dm_nnt.ma_cqthu = t.ma_qlt(+)) and A.SHKB=T.SHKB AND tcs_dm_nnt.ma_loai_nnt = nt.ma_loai_nnt(+) and ma_nnt = '" & strMaNNT & "'"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin NNThuế trong danh mục NNThuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function NNT_VL(ByVal strMaNNT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin NNThuế trong danh mục NNThuế
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select ma_nnt, ten_nnt, dia_chi, " & _
                    "ma_tinh, ma_huyen, ma_xa, ma_cqthu, " & _
                    "ma_cap, ma_chuong, ma_loai, ma_khoan,'' shkb, '' loai_nnt, '' ten_loai_nnt " & _
                    "from tcs_dm_nnt_vl " & _
                    "where ma_nnt = '" & strMaNNT & "'"
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin NNThuế trong danh mục NNThuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function TruocBa(ByVal fstrMaNNT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Tìm kiếm thông tin NNT trong bảng danh mục file trước bạ
            ' Tham số: 
            ' Giá trị trả về:        
            ' ----------------------------------------------------
            If (Not System.IO.File.Exists(gstrTB_Path & "DANHBA_TH.DBF")) Then Return Nothing

            Dim cnNNT_TB As New OleDbConnection
            Dim cmdNNT_TB As New OleDbCommand
            Dim daNNT_TB As New OleDbDataAdapter
            Dim strMaNNT As String = UCase(fstrMaNNT)
            Dim strSql As String
            Dim bytReturn As Byte
            ' Trường hợp không tìm thấy ĐTNT trong danh bạ hệ thống
            ' Tiếp tục tìm kiếm trong sổ thuế trước bạ nếu Điểm thu
            ' có thu thuế trước bạ.
            Try
                Dim dsNNT As New DataSet

                cnNNT_TB.ConnectionString = _
                               "Provider=vfpoledb;Data Source=" & gstrTB_Path & "DANHBA_TH.DBF" & _
                               ";Mode=ReadWrite;Collating Sequence=MACHINE;Password=''"
                cnNNT_TB.Open()

                strSql = "Select MACAP,MACHUONG,MALOAI,MAKHOAN,TENDTNT," & _
                         "DIACHI,TENNGHE,MADBTHU,SOKHUNG,SOMAY,MADTNT " & _
                         "From DANHBA_TH " & _
                         "Where MADTNT = '" & strMaNNT & "'"
                cmdNNT_TB.Connection = cnNNT_TB
                cmdNNT_TB.CommandText = strSql
                daNNT_TB.SelectCommand = cmdNNT_TB
                daNNT_TB.Fill(dsNNT)

                Return dsNNT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi lấy thông tin NNT trong bảng danh mục file trước bạ")
                Throw ex
            Finally
                cmdNNT_TB.Dispose()
                cnNNT_TB.Dispose()
                daNNT_TB.Dispose()
            End Try
        End Function

        Public Function SoThue_TruocBa(ByVal fstrMaNNT As String) As DataSet
            '-----------------------------------------------------
            ' Mục đích: Tìm kiếm thông tin NNT trong bảng danh mục file trước bạ
            ' Tham số: 
            ' Giá trị trả về:        
            ' ----------------------------------------------------
            If (Not System.IO.File.Exists(gstrTB_Path & "thue_tb.dbf")) Then Return Nothing

            Dim cnNNT_TB As New OleDbConnection
            Dim cmdNNT_TB As New OleDbCommand
            Dim daNNT_TB As New OleDbDataAdapter
            Dim strMaNNT As String = UCase(fstrMaNNT)
            Dim strSql As String
            Dim bytReturn As Byte
            ' Trường hợp không tìm thấy ĐTNT trong danh bạ hệ thống
            ' Tiếp tục tìm kiếm trong sổ thuế trước bạ nếu Điểm thu
            ' có thu thuế trước bạ.
            Try
                Dim dsNNT As New DataSet

                cnNNT_TB.ConnectionString = _
                               "Provider=vfpoledb;Data Source=" & gstrTB_Path & "thue_tb.dbf" & _
                               ";Mode=ReadWrite;Collating Sequence=MACHINE;Password=''"
                cnNNT_TB.Open()

                strSql = "Select MADTNT,KYTHUE,MAMUC,MATM,THUE " & _
                         "From thue_tb " & _
                         "Where MADTNT = '" & strMaNNT & "'"
                cmdNNT_TB.Connection = cnNNT_TB
                cmdNNT_TB.CommandText = strSql
                daNNT_TB.SelectCommand = cmdNNT_TB
                daNNT_TB.Fill(dsNNT)

                Return dsNNT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin NNT trong bảng danh mục file trước bạ")
                Throw ex
            Finally
                cmdNNT_TB.Dispose()
                cnNNT_TB.Dispose()
                daNNT_TB.Dispose()
            End Try
        End Function

        Public Function VangLai() As DataSet
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin Vãng lai đặc biệt
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' Ngày viết: 28/11/2007
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "SELECT ma_nnt, ten_nnt, dia_chi, ma_tinh, ma_huyen, " & _
                        "ma_xa, ma_cqthu, ma_cap, ma_chuong, ma_loai, ma_khoan " & _
                        "FROM tcs_dm_nnt_kb "
                Return cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin vãng lai đặc biệt")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function Get_VangLai() As String
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin Vãng lai đặc biệt
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "SELECT ma_nnt " & _
                        "FROM tcs_dm_nnt_kb "
                Dim ds As DataSet = cnCT.ExecuteReturnDataSet(strSQL, CommandType.Text)

                If (IsEmptyDataSet(ds)) Then
                    Return "0000000017"
                Else
                    Return GetString(ds.Tables(0).Rows(0)("ma_nnt"))
                End If

            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi lấy thông tin vãng lai đặc biệt")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function Exists_SoThue_CTN(ByVal strMaNNT As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của NNThuế trong sổ thuế
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                ' cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select  count(1) " & _
                    "from tcs_sothue " & _
                    "where ma_nnt = '" & strMaNNT & "'"
                Dim blnReturn As Boolean = False
                Dim drNNT As String = cnCT.ExecuteSQLScalar(strSQL)
                If drNNT <> "" And Integer.Parse(drNNT) > 0 Then
                    blnReturn = True
                Else
                    blnReturn = False
                End If
                Return blnReturn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi Kiểm tra sự tồn tại của NNThuế trong sổ thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function Exists_ToKhai(ByVal strMaNNT As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của NNThuế trong tờ khai Hải Quan
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:        
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select 1 " & _
                    "from tcs_ds_tokhai " & _
                    "where ma_nnt = '" & strMaNNT & "'"
                Dim blnReturn As Boolean = False
                Dim drNNT As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                If (drNNT.Read()) Then
                    blnReturn = True
                End If
                drNNT.Close()
                Return blnReturn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi Kiểm tra sự tồn tại của NNThuế trong tờ khai Hải Quan")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function Exists_NNT(ByVal strMaNNT As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của NNThuế trong danh mục NNThuế
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:        
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select 1 " & _
                    "from tcs_dm_nnt " & _
                    "where ma_nnt = '" & strMaNNT & "'"
                Dim blnReturn As Boolean = False
                Dim drNNT As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                If (drNNT.Read()) Then
                    blnReturn = True
                End If
                drNNT.Close()
                Return blnReturn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi Kiểm tra sự tồn tại của NNThuế trong danh mục NNThuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function Exists_NNT_VL(ByVal strMaNNT As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của NNThuế trong sổ thuế
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select 1 " & _
                    "from tcs_dm_nnt_vl " & _
                    "where ma_nnt = '" & strMaNNT & "'"
                Dim blnReturn As Boolean = False
                Dim drNNT As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                If (drNNT.Read()) Then
                    blnReturn = True
                End If
                drNNT.Close()
                Return blnReturn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi Kiểm tra sự tồn tại của NNThuế trong sổ thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Function Exists_TruocBa(ByVal fstrMaNNT As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của NNThuế trong bảng danh mục file trước bạ
            ' Tham số: 
            ' Giá trị trả về:        
            ' ----------------------------------------------------
            If (Not System.IO.File.Exists(gstrTB_Path & "DANHBA_TH.DBF")) Then Return Nothing

            Dim cnNNT_TB As New OleDbConnection
            Dim cmdNNT_TB As New OleDbCommand
            Dim strMaNNT As String = UCase(fstrMaNNT)
            Dim strSql As String
            Try
                Dim dsNNT As New DataSet

                cnNNT_TB.ConnectionString = _
                               "Provider=vfpoledb;Data Source=" & gstrTB_Path & "DANHBA_TH.DBF" & _
                               ";Mode=ReadWrite;Collating Sequence=MACHINE;Password=''"
                cnNNT_TB.Open()

                strSql = "Select MADTNT " & _
                         "From DANHBA_TH " & _
                         "Where MADTNT = '" & strMaNNT & "'"
                cmdNNT_TB.Connection = cnNNT_TB
                cmdNNT_TB.CommandText = strSql

                Dim drNNT As IDataReader = cmdNNT_TB.ExecuteReader(CommandBehavior.CloseConnection)
                Dim blnReturn As Boolean = False
                If (drNNT.Read()) Then
                    blnReturn = True
                End If
                drNNT.Close()

                Return blnReturn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi Kiểm tra sự tồn tại của NNThuế trong bảng danh mục file trước bạ")
                Throw ex
            Finally
                cmdNNT_TB.Dispose()
                cnNNT_TB.Dispose()
            End Try
        End Function

        Public Function Exists_VangLai(ByVal strMaNNT As String) As Boolean
            '-----------------------------------------------------
            ' Mục đích: Kiểm tra sự tồn tại của NNThuế trong sổ thuế
            ' Tham số: mã người nộp thuế
            ' Giá trị trả về:
            ' ----------------------------------------------------  
            Dim cnCT As DataAccess
            Try
                cnCT = New DataAccess
                Dim strSQL As String
                strSQL = "select 1 " & _
                    "from tcs_dm_nnt_kb " & _
                    "where ma_nnt = '" & strMaNNT & "'"
                Dim blnReturn As Boolean = False
                Dim drNNT As IDataReader = cnCT.ExecuteDataReader(strSQL, CommandType.Text)
                If (drNNT.Read()) Then
                    blnReturn = True
                End If
                drNNT.Close()
                Return blnReturn
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi Kiểm tra sự tồn tại của NNThuế trong sổ thuế")
                Throw ex
            Finally
                If (Not IsNothing(cnCT)) Then cnCT.Dispose()
            End Try
        End Function

        Public Sub NNT_VL_Insert(ByVal objHDR As infChungTuHDR, ByVal objDTL As infChungTuDTL)
            '-----------------------------------------------------
            ' Mục đích: Insert dữ liệu vào bảng TCS_DM_NNT_VL
            ' Đầu vào: 
            '-----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess
                strSQL = "Insert Into TCS_DM_NNT_VL(Ma_NNT,Ten_NNT,Dia_Chi,Ngay_CN,Ma_Tinh," & _
                         "Ma_Huyen,Ma_Xa,Ma_CQThu,Ma_Cap,Ma_Chuong,Ma_Loai,Ma_Khoan) " & _
                         "Values('" & objHDR.Ma_NNThue & "','" & objHDR.Ten_NNThue & "','" & _
                         objHDR.DC_NNThue & "'," & objHDR.Ngay_HT & ",'" & objHDR.Ma_Tinh & "','" & _
                         objHDR.Ma_Huyen & "','" & objHDR.Ma_Xa & "','" & _
                         objHDR.Ma_CQThu & "','" & objDTL.Ma_Cap & "','" & objDTL.Ma_Chuong & "','" & _
                         objDTL.Ma_Loai & "','" & objDTL.Ma_Khoan & "')"
                conn.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi Insert dữ liệu vào bảng TCS_DM_NNT_VL")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu vào bảng: TCS_DM_NNT_VL - " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                ' Giải phóng Connection tới CSDL
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub

        Public Sub NNT_VL_Update(ByVal objHDR As infChungTuHDR, ByVal objDTL As infChungTuDTL)
            '-----------------------------------------------------
            ' Mục đích: Update thông tin NNT Vãng Lai
            ' Đầu vào: 
            '-----------------------------------------------------
            Dim conn As DataAccess
            Dim strSQL As String
            Try
                conn = New DataAccess
                strSQL = "update TCS_DM_NNT_VL set " & _
                        "Ten_NNT = '" & objHDR.Ten_NNThue & "', Dia_Chi = '" & objHDR.DC_NNThue & "', " & _
                        "Ngay_CN = " & objHDR.Ngay_HT & ", Ma_Tinh = '" & objHDR.Ma_Tinh & "', " & _
                        "Ma_Huyen = '" & objHDR.Ma_Huyen & "', Ma_Xa = '" & objHDR.Ma_Xa & "', " & _
                        "Ma_CQThu = '" & objHDR.Ma_CQThu & "', Ma_Cap = '" & objDTL.Ma_Cap & "', " & _
                        "Ma_Chuong = '" & objDTL.Ma_Chuong & "', Ma_Loai = '" & objDTL.Ma_Loai & "', " & _
                        "Ma_Khoan = '" & objDTL.Ma_Khoan & "' " & _
                        "where Ma_NNT = '" & objHDR.Ma_NNThue & "'"
                conn.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi cập nhật thông tin NNT vãng lai")
                'LogDebug.WriteLog("Lỗi trong quá trình insert dữ liệu vào bảng: TCS_DM_NNT_VL - " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Sub
    End Class

End Namespace