﻿Public Class infTK_KH_TH


    Private m_RetAccountName As String
    Private m_CertCode As String
    Private m_CertType As String
    Private m_Balance As String
    Private m_BranchNo As String
    Private m_Cif_No As String
    Private m_AvaiBalance As String
    Private m_DescAccount As String
    Private m_ErrorCode As String
    Private m_ErrorMessage As String
    Private m_CurrencyCode As String
    Public Property CurrencyCode() As String
        Get
            Return m_CurrencyCode
        End Get
        Set(ByVal value As String)
            m_CurrencyCode = value
        End Set
    End Property

    Public Property RetAccountName() As String
        Get
            Return m_RetAccountName
        End Get
        Set(ByVal value As String)
            m_RetAccountName = value
        End Set
    End Property

    Public Property CertCode() As String
        Get
            Return m_CertCode
        End Get
        Set(ByVal value As String)
            m_CertCode = value
        End Set
    End Property

    Public Property CertType() As String
        Get
            Return m_CertType
        End Get
        Set(ByVal value As String)
            m_CertType = value
        End Set
    End Property

    Public Property Balance() As String
        Get
            Return m_Balance
        End Get
        Set(ByVal value As String)
            m_Balance = value
        End Set
    End Property

    Public Property BranchNo() As String
        Get
            Return m_BranchNo
        End Get
        Set(ByVal value As String)
            m_BranchNo = value
        End Set
    End Property

    Public Property Cif_No() As String
        Get
            Return m_Cif_No
        End Get
        Set(ByVal value As String)
            m_Cif_No = value
        End Set
    End Property

    Public Property AvaiBalance() As String
        Get
            Return m_AvaiBalance
        End Get
        Set(ByVal value As String)
            m_AvaiBalance = value
        End Set
    End Property

    Public Property DescAccount() As String
        Get
            Return m_DescAccount
        End Get
        Set(ByVal value As String)
            m_DescAccount = value
        End Set
    End Property

    Public Property ErrorCode() As String
        Get
            Return m_ErrorCode
        End Get
        Set(ByVal value As String)
            m_ErrorCode = value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return m_ErrorMessage
        End Get
        Set(ByVal value As String)
            m_ErrorMessage = value
        End Set
    End Property


End Class

