Imports VBOracleLib

Namespace NTDT.NNTDT
    Public Class daNNTDT
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Insert(ByVal objNNTDT As infNNTDT) As Boolean
            Dim cnNNTDT As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "INSERT INTO TCS_DM_NNTDT (NNTDT_ID, PBAN_TLIEU_XML, MST, TEN_NNT, DIACHI_NNT, MA_CQT, EMAIL_NNT, SDT_NNT, TEN_LHE_NTHUE, SERIAL_CERT_NTHUE, SUBJECT_CERT_NTHUE, ISSUER_CERT_NTHUE, MA_NHANG, TEN_NHANG, VAN_ID, TEN_TVAN, NGAY_GUI, STK_NHANG, TENTK_NHANG, SIGNATURE_NNT, SIGNATURE_TCT, TRANG_THAI, NGAY_TAO, LDO_TCHOI) " & _
                                "VALUES (tcs_dm_nntdt_seq.nextval, " & objNNTDT.PBAN_TLIEU_XML & ", " & objNNTDT.MST & "', '" & objNNTDT.TEN_NNT & "', " & objNNTDT.DIACHI_NNT & "', '" & objNNTDT.MA_CQT & "', '" & objNNTDT.EMAIL_NNT & "', '" & objNNTDT.SERIAL_CERT_NTHUE & "', '" & objNNTDT.SUBJECT_CERT_NTHUE & "', '" & objNNTDT.ISSUER_CERT_NTHUE & "', '" & objNNTDT.MA_NHANG & "', '" & objNNTDT.TEN_NHANG & "', '" & objNNTDT.VAN_ID & "', '" & objNNTDT.TEN_TVAN & "', '" & objNNTDT.NGAY_GUI & "', '" & objNNTDT.STK_NHANG & "', '" & objNNTDT.TENTK_NHANG & "', '" & objNNTDT.SIGNATURE_NNT & "', '" & objNNTDT.SIGNATURE_TCT & "', '" & objNNTDT.TRANG_THAI & "', '" & objNNTDT.NGAY_TAO & "', '" & objNNTDT.LDO_TCHOI & "')"
            Try
                cnNNTDT = New DataAccess
                cnNNTDT.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi thêm mới NNTDT")
                'LogDebug.Writelog("Có lỗi xảy ra khi thêm mới cấp - chương: " & ex.ToString)
                blnResult = False
                'Throw ex
            Finally
                If Not cnNNTDT Is Nothing Then cnNNTDT.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Update(ByVal objNNTDT As infNNTDT) As Boolean
            Dim cnNNTDT As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "UPDATE TCS_DM_NNTDT SET SDT_NNT='" & objNNTDT.SDT_NNT & "',EMAIL_NNT='" & objNNTDT.EMAIL_NNT & "', " & _
                   "SERIAL_CERT_NTHUE='" & objNNTDT.SERIAL_CERT_NTHUE & "', SUBJECT_CERT_NTHUE='" & objNNTDT.SUBJECT_CERT_NTHUE & _
                   "', ISSUER_CERT_NTHUE='" & objNNTDT.ISSUER_CERT_NTHUE & "', MA_NHANG='" & objNNTDT.MA_NHANG & "', STK_NHANG='" & objNNTDT.STK_NHANG & _
                   "', TENTK_NHANG='" & objNNTDT.TENTK_NHANG & _
                   "' WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
            Try
                cnNNTDT = New DataAccess
                cnNNTDT.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật cấp - chương: " & ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật NNTDT")
                blnResult = False
                'Throw ex
            Finally
                If Not cnNNTDT Is Nothing Then cnNNTDT.Dispose()
            End Try
            Return blnResult
        End Function
        Public Function Update_HoanThien(ByVal objNNTDT As infNNTDT) As Boolean
            Dim cnNNTDT As DataAccess
            Dim blnResult As Boolean = True

            Dim strSql As String = "UPDATE TCS_DM_NNTDT SET TRANG_THAI='" & objNNTDT.TRANG_THAI & "', NV_HOAN_THIEN='" & objNNTDT.NV_TT & "', NV_TT='" & objNNTDT.NV_TT & "', MA_CN='" & objNNTDT.MA_CN & _
                   "', NGAY_HOAN_THIEN=SYSDATE, TENTK_NHANG='" & objNNTDT.TENTK_NHANG & "' " & _
                   " WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
            Try
                cnNNTDT = New DataAccess
                cnNNTDT.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật cấp - chương: " & ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật NNTDT")
                blnResult = False
                ' Throw ex
            Finally
                If Not cnNNTDT Is Nothing Then cnNNTDT.Dispose()
            End Try
            Return blnResult
        End Function
        Public Function Update_TrangThai(ByVal objNNTDT As infNNTDT) As Boolean
            Dim cnNNTDT As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = ""
            Select Case objNNTDT.TRANG_THAI
                Case "02" 'Hủy - Chờ duyệt
                    strSql = "UPDATE TCS_DM_NNTDT SET TRANG_THAI='" & objNNTDT.TRANG_THAI & "', MA_CN='" & objNNTDT.MA_CN & _
                            "', NV_TT='" & objNNTDT.NV_TT.Trim & "', LDO_TCHOI='" & objNNTDT.LDO_TCHOI & "' " & _
                            " WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
                Case "03" 'KS thành công
                    strSql = "UPDATE TCS_DM_NNTDT SET TRANG_THAI='" & objNNTDT.TRANG_THAI & _
                            "', NV_TT='" & objNNTDT.NV_TT.Trim & _
                            "', NV_KS='" & objNNTDT.NV_TT.Trim & "', NGAY_KS=SYSDATE, UPDATE_TT='00' " & _
                            " WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
                Case "04" 'Hủy : duyệt hủy
                    strSql = "UPDATE TCS_DM_NNTDT SET TRANG_THAI='" & objNNTDT.TRANG_THAI & _
                            "', NV_TT='" & objNNTDT.NV_TT.Trim & _
                            "', NV_KS='" & objNNTDT.NV_TT.Trim & "'" & _
                            " WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
                Case "05" 'Điều chỉnh
                    strSql = "UPDATE TCS_DM_NNTDT SET TRANG_THAI='" & objNNTDT.TRANG_THAI & _
                            "', NV_TT='" & objNNTDT.NV_TT.Trim & "'" & _
                            " WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
                    'Case "06" 'Ngừng
                    '    strSql = "UPDATE TCS_DM_NNTDT SET TRANG_THAI='" & objNNTDT.TRANG_THAI & _
                    '            "', NV_TT='" & objNNTDT.NV_TT.Trim & "'" & _
                    '            "', NV_KS='" & objNNTDT.NV_KS.Trim & "'" & _
                    '            " WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
                Case "07" 'Chuyển trả
                    strSql = "UPDATE TCS_DM_NNTDT SET TRANG_THAI='" & objNNTDT.TRANG_THAI & _
                            "', NV_TT='" & objNNTDT.NV_TT.Trim & "'," & _
                            " NV_KS='" & objNNTDT.NV_TT.Trim & "'," & _
                            " LDO_TCHOI='" & objNNTDT.LDO_TCHOI.Trim & "'" & _
                            " WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
                Case "08" 'Thành công - chờ xác nhận từ tct
                    strSql = "UPDATE TCS_DM_NNTDT SET TRANG_THAI='" & objNNTDT.TRANG_THAI & _
                            "', NV_TT='" & objNNTDT.NV_TT.Trim & _
                            "', NV_KS='" & objNNTDT.NV_TT.Trim & "', NGAY_KS=SYSDATE " & _
                            " WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
                Case "09" 'Hủy - chờ xác nhận từ tct
                    strSql = "UPDATE TCS_DM_NNTDT SET TRANG_THAI='" & objNNTDT.TRANG_THAI & _
                            "', NV_TT='" & objNNTDT.NV_TT.Trim & _
                            "', NV_KS='" & objNNTDT.NV_TT.Trim & "'" & _
                            " WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID.Trim
            End Select

            Try
                cnNNTDT = New DataAccess
                cnNNTDT.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi cập nhật cấp - chương: " & ex.ToString)
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi cập nhật trạng thái NNTDT")
                blnResult = False
                'Throw ex
            Finally
                If Not cnNNTDT Is Nothing Then cnNNTDT.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Delete(ByVal objNNTDT As infNNTDT) As Boolean
            Dim cnNNTDT As DataAccess
            Dim blnResult As Boolean = True
            Dim strSql As String = "DELETE FROM TCS_DM_NNTDT " & _
                    "WHERE NNTDT_ID=" & objNNTDT.NNTDT_ID
            Try
                cnNNTDT = New DataAccess
                cnNNTDT.ExecuteNonQuery(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi xóa NNTDT")
                'Throw ex
                'LogDebug.Writelog("Có lỗi xảy ra khi xoá cơ mục lục cấp - chương: " & ex.ToString)
                blnResult = False
            Finally
                If Not cnNNTDT Is Nothing Then cnNNTDT.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Load(ByVal strNNTDT_ID As String) As infNNTDT
            Dim objNNTDT As New infNNTDT
            Dim cnNNTDT As DataAccess
            Dim drNNTDT As IDataReader
            Dim strSQL As String = "SELECT * FROM tcs_dm_nntdt Where NNTDT_ID=" & strNNTDT_ID
            Try
                cnNNTDT = New DataAccess
                drNNTDT = cnNNTDT.ExecuteDataReader(strSQL, CommandType.Text)
                If drNNTDT.Read Then
                    objNNTDT = New infNNTDT
                    objNNTDT.NNTDT_ID = strNNTDT_ID
                    objNNTDT.PBAN_TLIEU_XML = drNNTDT("PBAN_TLIEU_XML").ToString()
                    objNNTDT.MST = drNNTDT("MST").ToString()
                    objNNTDT.TEN_NNT = drNNTDT("TEN_NNT").ToString()
                    objNNTDT.DIACHI_NNT = drNNTDT("DIACHI_NNT").ToString()
                    objNNTDT.MA_CQT = drNNTDT("MA_CQT").ToString()
                    objNNTDT.EMAIL_NNT = drNNTDT("EMAIL_NNT").ToString()
                    objNNTDT.SDT_NNT = drNNTDT("SDT_NNT").ToString()
                    objNNTDT.TEN_LHE_NTHUE = drNNTDT("TEN_LHE_NTHUE").ToString()
                    objNNTDT.SERIAL_CERT_NTHUE = drNNTDT("SERIAL_CERT_NTHUE").ToString()
                    objNNTDT.SUBJECT_CERT_NTHUE = drNNTDT("SUBJECT_CERT_NTHUE").ToString()
                    objNNTDT.ISSUER_CERT_NTHUE = drNNTDT("ISSUER_CERT_NTHUE").ToString()
                    objNNTDT.MA_NHANG = drNNTDT("MA_NHANG").ToString()
                    objNNTDT.TEN_NHANG = drNNTDT("TEN_NHANG").ToString()
                    objNNTDT.VAN_ID = drNNTDT("VAN_ID").ToString()
                    objNNTDT.TEN_TVAN = drNNTDT("TEN_TVAN").ToString()
                    objNNTDT.NGAY_GUI = drNNTDT("NGAY_GUI").ToString()
                    objNNTDT.STK_NHANG = drNNTDT("STK_NHANG").ToString()
                    objNNTDT.TENTK_NHANG = drNNTDT("TENTK_NHANG").ToString()
                    objNNTDT.SIGNATURE_NNT = drNNTDT("SIGNATURE_NNT").ToString()
                    objNNTDT.SIGNATURE_TCT = drNNTDT("SIGNATURE_TCT").ToString()
                    objNNTDT.TRANG_THAI = drNNTDT("TRANG_THAI").ToString()
                    objNNTDT.NGAY_TAO = drNNTDT("NGAY_TAO").ToString()
                    objNNTDT.LDO_TCHOI = drNNTDT("LDO_TCHOI").ToString()
                    objNNTDT.MA_GDICH = drNNTDT("MA_GDICH").ToString()
                    objNNTDT.NGAY_GUI_GDICH = drNNTDT("NGAY_GUI_GDICH").ToString()
                    objNNTDT.MA_GDICH_TCHIEU = drNNTDT("MA_GDICH_TCHIEU").ToString()
                    objNNTDT.TGIAN_NGUNG = drNNTDT("TGIAN_NGUNG").ToString()
                    objNNTDT.LDO_NGUNG = drNNTDT("LDO_NGUNG").ToString()
                    objNNTDT.MA_CN = drNNTDT("MA_CN").ToString()
                    objNNTDT.NV_KS = drNNTDT("NV_KS").ToString()
                    objNNTDT.NV_HOAN_THIEN = drNNTDT("NV_HOAN_THIEN").ToString()
                    objNNTDT.NV_TT = drNNTDT("NV_TT").ToString()
                    objNNTDT.UPDATE_TT = drNNTDT("UPDATE_TT").ToString()
                End If
                Return objNNTDT
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin NNTDT")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                'Throw ex
            Finally
                If Not drNNTDT Is Nothing Then drNNTDT.Close()
                If Not cnNNTDT Is Nothing Then cnNNTDT.Dispose()
            End Try
            Return objNNTDT
        End Function

        Public Function Load(ByVal tmpNNTDT As infNNTDT) As infNNTDT
            Dim objNNTDT As New infNNTDT
            Dim cnNNTDT As DataAccess
            Dim drNNTDT As IDataReader

            ' Kiểm tra tồn tại nếu
            ' Mã cấp chương đã tồn tại và
            ' Ngày bắt đầu nằm trong khoảng ngày bắt đầu và kết thúc hoặc
            ' Ngày kết thúc nằm trong khoảng ngày bắt đầu và kết thúc
            Dim strSQL As String = "Select * From TCS_DM_NNTDT " & _
                "Where NNTDT_ID=" & tmpNNTDT.NNTDT_ID & ""

            Try
                cnNNTDT = New DataAccess
                drNNTDT = cnNNTDT.ExecuteDataReader(strSQL, CommandType.Text)
                If drNNTDT.Read Then
                    objNNTDT = New infNNTDT
                    objNNTDT.NNTDT_ID = drNNTDT("NNTDT_ID").ToString()
                    objNNTDT.PBAN_TLIEU_XML = drNNTDT("PBAN_TLIEU_XML").ToString()
                    objNNTDT.MST = drNNTDT("MST").ToString()
                    objNNTDT.TEN_NNT = drNNTDT("TEN_NNT").ToString()
                    objNNTDT.DIACHI_NNT = drNNTDT("DIACHI_NNT").ToString()
                    objNNTDT.MA_CQT = drNNTDT("MA_CQT").ToString()
                    objNNTDT.EMAIL_NNT = drNNTDT("EMAIL_NNT").ToString()
                    objNNTDT.SDT_NNT = drNNTDT("SDT_NNT").ToString()
                    objNNTDT.TEN_LHE_NTHUE = drNNTDT("TEN_LHE_NTHUE").ToString()
                    objNNTDT.SERIAL_CERT_NTHUE = drNNTDT("SERIAL_CERT_NTHUE").ToString()
                    objNNTDT.SUBJECT_CERT_NTHUE = drNNTDT("SUBJECT_CERT_NTHUE").ToString()
                    objNNTDT.ISSUER_CERT_NTHUE = drNNTDT("ISSUER_CERT_NTHUE").ToString()
                    objNNTDT.MA_NHANG = drNNTDT("MA_NHANG").ToString()
                    objNNTDT.TEN_NHANG = drNNTDT("TEN_NHANG").ToString()
                    objNNTDT.VAN_ID = drNNTDT("VAN_ID").ToString()
                    objNNTDT.TEN_TVAN = drNNTDT("TEN_TVAN").ToString()
                    objNNTDT.NGAY_GUI = drNNTDT("NGAY_GUI").ToString()
                    objNNTDT.STK_NHANG = drNNTDT("STK_NHANG").ToString()
                    objNNTDT.TENTK_NHANG = drNNTDT("TENTK_NHANG").ToString()
                    objNNTDT.SIGNATURE_NNT = drNNTDT("SIGNATURE_NNT").ToString()
                    objNNTDT.SIGNATURE_TCT = drNNTDT("SIGNATURE_TCT").ToString()
                    objNNTDT.TRANG_THAI = drNNTDT("TRANG_THAI").ToString()
                    objNNTDT.NGAY_TAO = drNNTDT("NGAY_TAO").ToString()
                    objNNTDT.LDO_TCHOI = drNNTDT("LDO_TCHOI").ToString()
                    objNNTDT.MA_GDICH = drNNTDT("MA_GDICH").ToString()
                    objNNTDT.NGAY_GUI_GDICH = drNNTDT("NGAY_GUI_GDICH").ToString()
                    objNNTDT.MA_GDICH_TCHIEU = drNNTDT("MA_GDICH_TCHIEU").ToString()
                    objNNTDT.TGIAN_NGUNG = drNNTDT("TGIAN_NGUNG").ToString()
                    objNNTDT.LDO_NGUNG = drNNTDT("LDO_NGUNG").ToString()
                    objNNTDT.UPDATE_TT = drNNTDT("UPDATE_TT").ToString()
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                'Throw ex
            Finally
                If Not drNNTDT Is Nothing Then drNNTDT.Close()
                If Not cnNNTDT Is Nothing Then cnNNTDT.Dispose()
            End Try
            Return objNNTDT
        End Function

        Public Function Load(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String
            Dim cnNNTDT As DataAccess
            Dim dsNNTDT As DataSet

            Try
                strSQL = "SELECT a.NNTDT_ID,a.PBAN_TLIEU_XML,a.MST,a.TEN_NNT,a.DIACHI_NNT,a.MA_CQT,a.EMAIL_NNT,a.SDT_NNT,a.TEN_LHE_NTHUE," & _
                         " a.SERIAL_CERT_NTHUE,a.SUBJECT_CERT_NTHUE,a.ISSUER_CERT_NTHUE,a.MA_NHANG,a.TEN_NHANG,a.VAN_ID," & _
                         " a.TEN_TVAN,a.NGAY_GUI,b.SO_TK_NH AS STK_NHANG," & _
                         " a.TENTK_NHANG,a.SIGNATURE_NNT,a.SIGNATURE_TCT,a.TRANG_THAI,a.NGAY_TAO,a.LDO_TCHOI,a.MA_GDICH,a.NGAY_GUI_GDICH,a.MA_GDICH_TCHIEU," & _
                         " a.TGIAN_NGUNG, a.LDO_NGUNG,a.MA_CN,a.NV_HOAN_THIEN,a.NGAY_HOAN_THIEN,a.NV_KS,a.NGAY_KS,a.NV_TT " & _
                         " FROM tcs_dm_nntdt a,NTDT_MAP_MST_TKNH b" & _
                         " WHERE (a.NNTDT_ID = b.NNTDT_ID(+) AND a.mst= b.MST (+)) " & WhereClause & _
                         " ORDER BY NNTDT_ID"
                cnNNTDT = New DataAccess
                dsNNTDT = cnNNTDT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                'Throw ex
            Finally
                cnNNTDT.Dispose()
            End Try

            Return dsNNTDT
        End Function
 Public Function Load_Header(ByVal WhereClause As String, ByVal temp As String) As DataSet
            Dim strSQL As String
            Dim cnNNTDT As DataAccess
            Dim dsNNTDT As DataSet

            Try
                strSQL = "SELECT a.NNTDT_ID,a.PBAN_TLIEU_XML,a.MST,a.TEN_NNT,a.DIACHI_NNT,a.MA_CQT,a.EMAIL_NNT,a.SDT_NNT,a.TEN_LHE_NTHUE," & _
                         " a.SERIAL_CERT_NTHUE,a.SUBJECT_CERT_NTHUE,a.ISSUER_CERT_NTHUE,a.MA_NHANG,a.TEN_NHANG,a.VAN_ID," & _
                         " a.TEN_TVAN,a.NGAY_GUI," & _
                         " a.TENTK_NHANG,a.SIGNATURE_NNT,a.SIGNATURE_TCT,D.mota TRANG_THAI,a.NGAY_TAO,a.LDO_TCHOI,a.MA_GDICH,a.NGAY_GUI_GDICH,a.MA_GDICH_TCHIEU," & _
                         " a.TGIAN_NGUNG, a.LDO_NGUNG,a.MA_CN,a.NV_HOAN_THIEN,a.NGAY_HOAN_THIEN,a.NV_KS,to_char(a.NGAY_KS,'dd/MM/yyyy hh24:mm:ss') ngay_ks,a.NV_TT,c.NAME, n1.ten_dn maker, n2.ten_dn checker, n3.ten_dn nv_thaotac " & _
                         " FROM tcs_dm_nntdt a, tcs_dm_chinhanh c,TCS_DM_TRANGTHAI_NTDT d, tcs_dm_nhanvien n1,tcs_dm_nhanvien n2, tcs_dm_nhanvien n3  " & _
                         " WHERE (a.MA_CN = c.ID(+) and d.trang_thai=a.TRANG_THAI and d.GHICHU='NNT' and a.nv_hoan_thien= n1.ma_nv(+) and a.nv_ks= n2.ma_nv(+) and a.nv_tt= n3.ma_nv(+))  " & WhereClause & _
                         " ORDER BY NNTDT_ID"
                cnNNTDT = New DataAccess
                dsNNTDT = cnNNTDT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin cấp - chương")
                'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin cấp - chương từ CSDL: " & ex.ToString)
                'Throw ex
            Finally
                cnNNTDT.Dispose()
            End Try

            Return dsNNTDT
        End Function
        'Public Function CheckDelete(ByVal strCCH_ID As String) As Object
        '    Dim cnKhoBac As DataAccess
        '    Dim dsKhoBac As DataSet

        '    Try
        '        Dim strSQL As String = "select ma_chuong from (select ma_chuong from tcs_ctu_dtl union all select ma_chuong from tcs_baolanh_dtl) " & _
        '                "Where ma_chuong=" & strCCH_ID

        '        cnKhoBac = New DataAccess
        '        dsKhoBac = cnKhoBac.ExecuteReturnDataSet(strSQL, CommandType.Text)
        '    Catch ex As Exception
        '        CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin kho bạc")
        '        'LogDebug.Writelog("Có lỗi xảy ra khi lấy thông tin tài khoản từ CSDL: " & ex.ToString)
        '        Throw ex
        '    Finally
        '        If Not cnKhoBac Is Nothing Then cnKhoBac.Dispose()
        '    End Try

        '    Return dsKhoBac
        'End Function

        Public Function CheckExist(ByVal objNNTDT As infNNTDT) As Boolean
            Return Load(objNNTDT).MST <> ""
        End Function

        Public Function CheckExist(ByVal strMST As String) As Boolean
            Return Load(strMST).MST <> ""
        End Function

        Public Function GetAccountHistory(ByVal WhereClause As String) As DataSet
            Dim strSQL As String
            Dim cnNNTDT As DataAccess
            Dim dsNNTDT As DataSet
            Try
                strSQL = " SELECT NNTDT_ID,MST,SO_TK_NH,ID,NGAY_TH FROM NTDT_MAP_MST_TKNH_LOG A " & _
                         " WHERE 1=1 " & WhereClause & _
                         " ORDER BY ID ,NGAY_TH desc"
                cnNNTDT = New DataAccess
                dsNNTDT = cnNNTDT.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Có lỗi xảy ra khi lấy thông tin lịch sử thay đổi thông tin tài khoản")
                'Throw ex
            Finally
                cnNNTDT.Dispose()
            End Try

            Return dsNNTDT
        End Function
    End Class

End Namespace
