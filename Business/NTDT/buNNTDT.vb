Imports VBOracleLib

Namespace NTDT.NNTDT
    Public Class buNNTDT

        Private strStatus As String

        Sub New()
        End Sub

        Sub New(ByVal strTmpStatus As String)
            strStatus = strTmpStatus
        End Sub

        Public Property Status() As String
            Get
                Return strStatus
            End Get
            Set(ByVal Value As String)
                strStatus = Value
            End Set
        End Property

        Public Function Insert(ByVal objNNTDT As infNNTDT) As Boolean
            Dim tmpNNTDT As New daNNTDT
            If tmpNNTDT.CheckExist(objNNTDT) = False Then
                Return tmpNNTDT.Insert(objNNTDT)
            Else
                MsgBox("MST đã tồn tại trong hệ thống.", MsgBoxStyle.Critical, "Chú ý")
                Return False
            End If
        End Function

        Public Function Update(ByVal objNNTDT As infNNTDT) As Boolean
            Dim tmpNNTDT As New daNNTDT
            Try
                Dim objExist As infNNTDT = tmpNNTDT.Load(objNNTDT)
                If objExist.MST = "" Then
                    Return tmpNNTDT.Update(objNNTDT)
                Else
                    Dim strMsg As String = "Bạn đã sửa MST trùng với MST khác đã tồn tại trong hệ thống:" & Chr(13) & _
                    "MST: " & objExist.MST & Chr(13) & _
                    "Trạng thái: " & objExist.TRANG_THAI
                    MsgBox(strMsg, MsgBoxStyle.Critical, "Chú ý")
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Function Update_HoanThien(ByVal objNNTDT As infNNTDT) As Boolean
            Dim tmpNNTDT As New daNNTDT
            Try
                Dim objExist As infNNTDT = tmpNNTDT.Load(objNNTDT)
                Return tmpNNTDT.Update_HoanThien(objNNTDT)
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Function Update_TrangThai(ByVal objNNTDT As infNNTDT) As Boolean
            Dim tmpNNTDT As New daNNTDT
            Try
                Dim objExist As infNNTDT = tmpNNTDT.Load(objNNTDT)
                Return tmpNNTDT.Update_TrangThai(objNNTDT)
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function Save(ByVal objNNTDT As infNNTDT) As Boolean
            If strStatus.Equals("EDIT") Then
                Return Me.Update(objNNTDT)
            Else
                Return Me.Insert(objNNTDT)
            End If
        End Function

        Public Function Delete(ByVal strTmpMST As String) As Boolean
            Dim objNNTDT As New infNNTDT
            objNNTDT.MST = strTmpMST
            Dim tmpNNTDT As New daNNTDT
            Return tmpNNTDT.Delete(objNNTDT)
        End Function

        'Public Sub FormatFlexHeader(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    flxDanhMuc.Cols.Count = 5
        '    flxDanhMuc.Cols(1).Width = 60
        '    flxDanhMuc.Cols(1).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
        '    flxDanhMuc.Cols(2).Width = 220
        '    flxDanhMuc.Cols(2).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
        '    flxDanhMuc.Cols(3).Width = 200
        '    flxDanhMuc.Cols(3).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
        '    flxDanhMuc.Cols(4).Width = 79
        '    flxDanhMuc.Cols(4).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter

        '    flxDanhMuc.Rows(0).Item(1) = "Mã chương"
        '    flxDanhMuc.Rows(0).Item(2) = "Tên chương"
        '    flxDanhMuc.Rows(0).Item(3) = "Ghi chú"
        '    flxDanhMuc.Rows(0).Item(4) = "Tình trạng"

        '    flxDanhMuc.Cols(0).Visible = False
        'End Sub

        'Public Sub FormatFrm(ByRef frm As Windows.Forms.Form)
        'End Sub

        Public Function GetDataSet(ByVal WhereClause As String) As DataSet
            Dim objNNTDT As New daNNTDT
            Dim dsNNTDT As DataSet
            Try
                dsNNTDT = objNNTDT.Load(WhereClause, "")
            Catch ex As Exception
            Finally
            End Try

            Return dsNNTDT
        End Function

        Public Function GetData_Header(ByVal WhereClause As String) As DataSet
            Dim objNNTDT As New daNNTDT
            Dim dsNNTDT As DataSet
            Try
                dsNNTDT = objNNTDT.Load_Header(WhereClause, "")
            Catch ex As Exception
            Finally
            End Try

            Return dsNNTDT
        End Function

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid)
        '    Dim objNNTDT As New daNNTDT
        '    Try
        '        Dim tmpNNTDT As infNNTDT = objNNTDT.Load(flxDanhMuc.Item(flxDanhMuc.Row, 0))
        '        flxDanhMuc(flxDanhMuc.Row, 0) = tmpNNTDT.CCH_ID
        '        flxDanhMuc(flxDanhMuc.Row, 1) = tmpNNTDT.MA_CHUONG
        '        flxDanhMuc(flxDanhMuc.Row, 2) = tmpNNTDT.Ten
        '        flxDanhMuc(flxDanhMuc.Row, 3) = tmpNNTDT.GHICHU.Trim
        '        flxDanhMuc(flxDanhMuc.Row, 4) = IIf(tmpNNTDT.TINH_TRANG = "0", "Ngung hoat dong", "Dang hoat dong")
        '        If flxDanhMuc.Cols(flxDanhMuc.Col).Sort <> C1.Win.C1FlexGrid.SortFlags.None Then flxDanhMuc.Sort(flxDanhMuc.Cols(flxDanhMuc.Col).Sort, flxDanhMuc.Col)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Sub UpdateFlex(ByRef flxDanhMuc As C1.Win.C1FlexGrid.C1FlexGrid, ByVal objNNTDT As infNNTDT)
        '    Try
        '        flxDanhMuc.Rows.Add()
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 0) = objNNTDT.CCH_ID
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 1) = objNNTDT.MA_CHUONG
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 2) = objNNTDT.Ten
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 3) = objNNTDT.GHICHU
        '        flxDanhMuc(flxDanhMuc.Rows.Count - 1, 4) = IIf(objNNTDT.TINH_TRANG = "0", "Ngung hoat dong", "Dang hoat dong")
        '        flxDanhMuc.Select(flxDanhMuc.Rows.Count - 1, flxDanhMuc.Col)
        '        If flxDanhMuc.Cols(flxDanhMuc.Col).Sort <> C1.Win.C1FlexGrid.SortFlags.None Then flxDanhMuc.Sort(flxDanhMuc.Cols(flxDanhMuc.Col).Sort, flxDanhMuc.Col)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Public Function Add() As Object
        '    Dim frmAdd As New MLNS.frmMLNS("NNTDT")
        '    frmAdd.ShowDialog()
        '    If frmAdd.DialogResult = DialogResult.OK Then
        '        Dim objNNTDT As New infNNTDT
        '        Dim tmpNNTDT As New daNNTDT
        '        objNNTDT.MA_CHUONG = frmAdd.txtChuong.Text.Trim
        '        objNNTDT.TINH_TRANG = (1 - frmAdd.cboTinh_Trang.SelectedIndex).ToString
        '        objNNTDT = tmpNNTDT.Load(objNNTDT)
        '        If objNNTDT.CCH_ID <> "" Then
        '            Return objNNTDT
        '        Else
        '            Return Nothing
        '        End If
        '    Else
        '        Return Nothing
        '    End If
        'End Function

        'Public Function Edit(ByVal strID As String) As Boolean
        '    Dim frmAdd As New MLNS.frmMLNS("NNTDT", strID)
        '    frmAdd.ShowDialog()
        '    Return frmAdd.DialogResult = DialogResult.OK
        'End Function

        'Public Function GetReport() As String
        '    Return Common.mdlCommon.Report_Type.DM_NNTDT
        'End Function

        'Public Function CheckDelete(ByVal strTmpMaNNTDT As String) As Boolean
        '    Dim ds As DataSet
        '    Dim tmpNNTDT As New daNNTDT
        '    ds = tmpNNTDT.CheckDelete(strTmpMaNNTDT)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        Return True
        '    Else
        '        Return False
        '    End If
        'End Function
        Public Function GetAccountHistory(ByVal WhereClause As String) As DataSet
            Dim objNNTDT As New daNNTDT
            Dim dsNNTDT As DataSet
            Try
                dsNNTDT = objNNTDT.GetAccountHistory(WhereClause)
            Catch ex As Exception
            Finally
            End Try

            Return dsNNTDT
        End Function
    End Class
End Namespace
