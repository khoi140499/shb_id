Namespace NTDT.NNTDT
    Public Class infNNTDT

        Private strNNTDT_ID As String
        Private strPBAN_TLIEU_XML As String
        Private strMST As String
        Private strTEN_NNT As String
        Private strDIACHI_NNT As String
        Private strMA_CQT As String
        Private strEMAIL_NNT As String
        Private strSDT_NNT As String
        Private strTEN_LHE_NTHUE As String
        Private strSERIAL_CERT_NTHUE As String
        Private strSUBJECT_CERT_NTHUE As String
        Private strISSUER_CERT_NTHUE As String
        Private strMA_NHANG As String
        Private strTEN_NHANG As String
        Private strVAN_ID As String
        Private strTEN_TVAN As String
        Private strNGAY_GUI As String
        Private strSTK_NHANG As String
        Private strTENTK_NHANG As String
        Private strSIGNATURE_NNT As String
        Private strSIGNATURE_TCT As String
        Private strTRANG_THAI As String
        Private strNGAY_TAO As String
        Private strLDO_TCHOI As String
        Private strMA_GDICH As String
        Private strNGAY_GUI_GDICH As String
        Private strMA_GDICH_TCHIEU As String
        Private strTGIAN_NGUNG As String
        Private strLDO_NGUNG As String
        Private strMA_CN As String
        Private strNV_HOAN_THIEN As String
        Private strNV_KS As String
        Private strNV_TT As String
        Private strUpdate_TT As String


        Sub New()
            strNNTDT_ID = ""
            strPBAN_TLIEU_XML = ""
            strMST = ""
            strTEN_NNT = ""
            strDIACHI_NNT = ""
            strMA_CQT = ""
            strEMAIL_NNT = ""
            strSDT_NNT = ""
            strTEN_LHE_NTHUE = ""
            strSERIAL_CERT_NTHUE = ""
            strSUBJECT_CERT_NTHUE = ""
            strISSUER_CERT_NTHUE = ""
            strMA_NHANG = ""
            strTEN_NHANG = ""
            strVAN_ID = ""
            strTEN_TVAN = ""
            strNGAY_GUI = ""
            strSTK_NHANG = ""
            strTENTK_NHANG = ""
            strSIGNATURE_NNT = ""
            strSIGNATURE_TCT = ""
            strTRANG_THAI = ""
            strNGAY_TAO = ""
            strLDO_TCHOI = ""
            strMA_GDICH = ""
            strNGAY_GUI_GDICH = ""
            strMA_GDICH_TCHIEU = ""
            strTGIAN_NGUNG = ""
            strLDO_NGUNG = ""
            strMA_CN = ""
            strNV_HOAN_THIEN = ""
            strNV_KS = ""
            strNV_TT = ""
            strUpdate_TT = ""
        End Sub

        Sub New(ByVal drwNNTDT As DataRow)
            strNNTDT_ID = drwNNTDT("NNTDT_ID").ToString().Trim()
            strPBAN_TLIEU_XML = drwNNTDT("PBAN_TLIEU_XML").ToString().Trim()
            strMST = drwNNTDT("MST").ToString().Trim()
            strTEN_NNT = drwNNTDT("TEN_NNT").ToString().Trim()
            strDIACHI_NNT = drwNNTDT("DIACHI_NNT").ToString().Trim()
            strMA_CQT = drwNNTDT("MA_CQT").ToString().Trim()
            strEMAIL_NNT = drwNNTDT("EMAIL_NNT").ToString().Trim()
            strSDT_NNT = drwNNTDT("SDT_NNT").ToString().Trim()
            strTEN_LHE_NTHUE = drwNNTDT("TEN_LHE_NTHUE").ToString().Trim()
            strSERIAL_CERT_NTHUE = drwNNTDT("SERIAL_CERT_NTHUE").ToString().Trim()
            strSUBJECT_CERT_NTHUE = drwNNTDT("SUBJECT_CERT_NTHUE").ToString().Trim()
            strISSUER_CERT_NTHUE = drwNNTDT("ISSUER_CERT_NTHUE").ToString().Trim()
            strMA_NHANG = drwNNTDT("MA_NHANG").ToString().Trim()
            strTEN_NHANG = drwNNTDT("TEN_NHANG").ToString().Trim()
            strVAN_ID = drwNNTDT("VAN_ID").ToString().Trim()
            strTEN_TVAN = drwNNTDT("TEN_TVAN").ToString().Trim()
            strNGAY_GUI = drwNNTDT("NGAY_GUI").ToString().Trim()
            strSTK_NHANG = drwNNTDT("STK_NHANG1").ToString().Trim()
            strTENTK_NHANG = drwNNTDT("STK_NHANG2").ToString().Trim()
            strSIGNATURE_NNT = drwNNTDT("SIGNATURE_NNT").ToString().Trim()
            strSIGNATURE_TCT = drwNNTDT("SIGNATURE_TCT").ToString().Trim()
            strTRANG_THAI = drwNNTDT("TRANG_THAI").ToString().Trim()
            strNGAY_TAO = drwNNTDT("NGAY_TAO").ToString().Trim()
            strLDO_TCHOI = drwNNTDT("LDO_TCHOI").ToString().Trim()
            strMA_GDICH = drwNNTDT("MA_GDICH").ToString().Trim()
            strNGAY_GUI_GDICH = drwNNTDT("NGAY_GUI_GDICH").ToString().Trim()
            strMA_GDICH_TCHIEU = drwNNTDT("MA_GDICH_TCHIEU").ToString().Trim()
            strTGIAN_NGUNG = drwNNTDT("TGIAN_NGUNG").ToString().Trim()
            strLDO_NGUNG = drwNNTDT("LDO_NGUNG").ToString().Trim()
            strMA_CN = drwNNTDT("MA_CN").ToString().Trim()
            strNV_HOAN_THIEN = drwNNTDT("NV_HOAN_THIEN").ToString().Trim()
            strNV_KS = drwNNTDT("NV_KS").ToString().Trim()
            strNV_TT = drwNNTDT("NV_TT").ToString().Trim()
            strUpdate_TT = drwNNTDT("UPDATE_TT").ToString().Trim()
        End Sub
        Public Property NNTDT_ID() As String
            Get
                Return strNNTDT_ID
            End Get
            Set(ByVal Value As String)
                strNNTDT_ID = Value
            End Set
        End Property

        Public Property PBAN_TLIEU_XML() As String
            Get
                Return strPBAN_TLIEU_XML
            End Get
            Set(ByVal Value As String)
                strPBAN_TLIEU_XML = Value
            End Set
        End Property

        Public Property MST() As String
            Get
                Return strMST
            End Get
            Set(ByVal Value As String)
                strMST = Value
            End Set
        End Property

        Public Property TEN_NNT() As String
            Get
                Return strTEN_NNT
            End Get
            Set(ByVal Value As String)
                strTEN_NNT = Value
            End Set
        End Property

        Public Property DIACHI_NNT() As String
            Get
                Return strDIACHI_NNT
            End Get
            Set(ByVal Value As String)
                strDIACHI_NNT = Value
            End Set
        End Property

        Public Property MA_CQT() As String
            Get
                Return strMA_CQT
            End Get
            Set(ByVal Value As String)
                strMA_CQT = Value
            End Set
        End Property

        Public Property EMAIL_NNT() As String
            Get
                Return strEMAIL_NNT
            End Get
            Set(ByVal Value As String)
                strEMAIL_NNT = Value
            End Set
        End Property

        Public Property SDT_NNT() As String
            Get
                Return strSDT_NNT
            End Get
            Set(ByVal Value As String)
                strSDT_NNT = Value
            End Set
        End Property

        Public Property TEN_LHE_NTHUE() As String
            Get
                Return strTEN_LHE_NTHUE
            End Get
            Set(ByVal Value As String)
                strTEN_LHE_NTHUE = Value
            End Set
        End Property

        Public Property SERIAL_CERT_NTHUE() As String
            Get
                Return strSERIAL_CERT_NTHUE
            End Get
            Set(ByVal Value As String)
                strSERIAL_CERT_NTHUE = Value
            End Set
        End Property

        Public Property SUBJECT_CERT_NTHUE() As String
            Get
                Return strSUBJECT_CERT_NTHUE
            End Get
            Set(ByVal Value As String)
                strSUBJECT_CERT_NTHUE = Value
            End Set
        End Property

        Public Property ISSUER_CERT_NTHUE() As String
            Get
                Return strISSUER_CERT_NTHUE
            End Get
            Set(ByVal Value As String)
                strISSUER_CERT_NTHUE = Value
            End Set
        End Property

        Public Property MA_NHANG() As String
            Get
                Return strMA_NHANG
            End Get
            Set(ByVal Value As String)
                strMA_NHANG = Value
            End Set
        End Property

        Public Property TEN_NHANG() As String
            Get
                Return strTEN_NHANG
            End Get
            Set(ByVal Value As String)
                strTEN_NHANG = Value
            End Set
        End Property

        Public Property VAN_ID() As String
            Get
                Return strVAN_ID
            End Get
            Set(ByVal Value As String)
                strVAN_ID = Value
            End Set
        End Property

        Public Property TEN_TVAN() As String
            Get
                Return strTEN_TVAN
            End Get
            Set(ByVal Value As String)
                strTEN_TVAN = Value
            End Set
        End Property

        Public Property NGAY_GUI() As String
            Get
                Return strNGAY_GUI
            End Get
            Set(ByVal Value As String)
                strNGAY_GUI = Value
            End Set
        End Property

        Public Property STK_NHANG() As String
            Get
                Return strSTK_NHANG
            End Get
            Set(ByVal Value As String)
                strSTK_NHANG = Value
            End Set
        End Property

        Public Property TENTK_NHANG() As String
            Get
                Return strTENTK_NHANG
            End Get
            Set(ByVal Value As String)
                strTENTK_NHANG = Value
            End Set
        End Property

        Public Property SIGNATURE_NNT() As String
            Get
                Return strSIGNATURE_NNT
            End Get
            Set(ByVal Value As String)
                strSIGNATURE_NNT = Value
            End Set
        End Property

        Public Property SIGNATURE_TCT() As String
            Get
                Return strSIGNATURE_TCT
            End Get
            Set(ByVal Value As String)
                strSIGNATURE_TCT = Value
            End Set
        End Property

        Public Property TRANG_THAI() As String
            Get
                Return strTRANG_THAI
            End Get
            Set(ByVal Value As String)
                strTRANG_THAI = Value
            End Set
        End Property

        Public Property NGAY_TAO() As String
            Get
                Return strNGAY_TAO
            End Get
            Set(ByVal Value As String)
                strNGAY_TAO = Value
            End Set
        End Property

        Public Property LDO_TCHOI() As String
            Get
                Return strLDO_TCHOI
            End Get
            Set(ByVal Value As String)
                strLDO_TCHOI = Value
            End Set
        End Property

        Public Property MA_GDICH() As String
            Get
                Return strMA_GDICH
            End Get
            Set(ByVal Value As String)
                strMA_GDICH = Value
            End Set
        End Property

        Public Property NGAY_GUI_GDICH() As String
            Get
                Return strNGAY_GUI_GDICH
            End Get
            Set(ByVal Value As String)
                strNGAY_GUI_GDICH = Value
            End Set
        End Property

        Public Property MA_GDICH_TCHIEU() As String
            Get
                Return strMA_GDICH_TCHIEU
            End Get
            Set(ByVal Value As String)
                strMA_GDICH_TCHIEU = Value
            End Set
        End Property

        Public Property TGIAN_NGUNG() As String
            Get
                Return strTGIAN_NGUNG
            End Get
            Set(ByVal Value As String)
                strTGIAN_NGUNG = Value
            End Set
        End Property

        Public Property LDO_NGUNG() As String
            Get
                Return strLDO_NGUNG
            End Get
            Set(ByVal Value As String)
                strLDO_NGUNG = Value
            End Set
        End Property

        Public Property MA_CN() As String
            Get
                Return strMA_CN
            End Get
            Set(ByVal Value As String)
                strMA_CN = Value
            End Set
        End Property

        Public Property NV_HOAN_THIEN() As String
            Get
                Return strNV_HOAN_THIEN
            End Get
            Set(ByVal Value As String)
                strNV_HOAN_THIEN = Value
            End Set
        End Property

        Public Property NV_KS() As String
            Get
                Return strNV_KS
            End Get
            Set(ByVal Value As String)
                strNV_KS = Value
            End Set
        End Property
        Public Property NV_TT() As String
            Get
                Return strNV_TT
            End Get
            Set(ByVal Value As String)
                strNV_TT = Value
            End Set
        End Property

        Public Property UPDATE_TT() As String
            Get
                Return strUpdate_TT
            End Get
            Set(ByVal Value As String)
                strUpdate_TT = Value
            End Set
        End Property
    End Class

End Namespace
