﻿Namespace NewChungTu

    Public Class infChungTuHDR
        Private strSHKB As String
        Private iNgay_KB As Integer
        Private iMa_NV As Integer
        Private iSo_BT As Integer  ' số bút toán
        Private strMa_DThu As String
        Private iSo_BThu As Integer
        Private strKyHieu_CT As String
        Private strSo_CT As String
        Private strSo_CT_TuChinh As String
        Private strSo_CT_NH As String
        Private strMa_NNTien As String
        Private strTen_NNTien As String
        Private strDC_NNTien As String
        Private strMa_NNThue As String
        Private strTen_NNThue As String
        Private strDC_NNThue As String
        Private strLy_Do As String
        Private iMa_KS As String
        Private iMa_TQ As String
        Private strSo_QD As String
        Private strNgay_QD As String
        Private strCQ_QD As String
        Private iNgay_CT As Integer
        Private strNgay_HT As String
        Private strMa_CQThu As String
        Private strXA_ID As String
        Private strMa_Tinh As String
        Private strMa_Huyen As String
        Private strMa_Xa As String
        Private strTK_No As String
        Private strTen_TKNo As String
        Private strTK_Co As String
        Private strTen_TKCo As String
        Private strSo_TK As String
        Private strNgay_TK As String
        Private strLH_XNK As String
        Private strDVSDNS As String
        Private strTen_DVSDNS As String
        Private strMa_NT As String
        Private dblTy_Gia As Double
        Private iTG_ID As String
        Private strMa_LThue As String
        Private strSo_Khung As String
        Private strSo_May As String

        Private strTK_KH_NH As String
        Private strNGAY_KH_NH As String

        Private strSO_CMND As String
        Private strSO_FONE As String

        Private strMA_NH_A As String
        Private strMA_NH_B As String

        Private strPHI_GD As String
        Private strPHI_VAT As String
        Private strPT_TINHPHI As String

        Private dblTTien As Double
        Private dblTT_TThu As Double
        Private strLan_In As String
        Private strTrang_Thai As String

        Private strTK_KH_Nhan As String
        Private strTen_KH_Nhan As String
        Private strDiachi_KH_Nhan As String
        Private dblTTien_NT As String
        Private intSo_BT_KTKT As Integer

        Private strSo_BK As String
        Private strNgay_BK As String
        Private strPT_TT As String
        Private strTK_GL_NH As String
        Private strTEN_KH_NH As String
        Private strTK_KB_NH As String
        Private intMode As Integer
        Private strTT_NSTT As String
        Private strTT_BDS As String
        Private strtellerGDV As String
        Private strtellerKSV As String
        Private strSoRM As String
        Private strRM_REF_NO As String
        Private strREF_NO As String

        Private _REF_CORE_TTSP As String

        Private strQuan_Huyen_NNTien As String
        Private strTinh_TPho_NNTien As String
        Private strTKHTNH As String

        Private strTIME_BEGIN As String

        'Thuoc tinh dung cho Bao lanh HQ
        Private strKhct As String
        Private strTen_kb As String
        Private strTen_nv As String
        Private strHuyen_nnthue As String
        Private strTinh_nnthue As String
        Private strTen_ks As String
        Private strTen_cqthu As String
        Private strLhxnk As String
        Private strVt_lhxnk As String
        Private strTen_lhxnk As String
        Private strTen_nt As String
        Private strTen_nh_a As String
        Private strTtien_tthu As Integer
        Private strGhi_chu As String
        Private strTk_ns As String
        Private strTen_tk_ns As String
        Private strPhuong_thuc As String
        Private strHinh_thuc As String
        Private strKhoi_phuc As String
        Private strReponse_code As String
        Private strHq_tran_id As String
        Private strHq_loai_thue As String
        Private strRes_hq As String
        Private strSo_bl As String
        Private strSongay_bl As Integer
        Private strKieu_bl As String
        Private strMa_hq_ph As String
        Private strDien_giai As String
        Private strTrangthai_lapct As String
        Private strNgay_batdau As String
        Private strNgay_ketthuc As String
        Private strError_code_hq As String
        Private strSo_ct_ctu As String
        Private strDchieu_hq As String
        Private strIs_ma_hq As String
        Private strNgay_dc_hq As String
        Private strKb_thu_ho As String
        Private strMa_loaitien As String
        Private strMa_hq_cqt As String
        Private strSo_bt_hq As Integer
        Private strTrangThai_BL As String

        Private strTen_hq As String
        Private strTen_hq_ph As String
        'thêm thông tin về số hợp đồng
        Private strHD_NT As String
        Private strNgay_HD_NT As String
        Private strHDon As String
        Private strVTD As String
        Private strCIF As String
        Private strNgay_HDon As String
        Private strNgay_VTD As String
        Private strKenh_CT As String = "01"
        Private strVTD2 As String
        Private strNgay_VTD2 As String
        Private strVTD3 As String
        Private strNgay_VTD3 As String
        Private strVTD4 As String
        Private strNgay_VTD4 As String
        Private strVTD5 As String
        Private strNgay_VTD5 As String
        Private strMa_hq_kb As String
        ' ****************************************************************************
        ' Người sửa: Anhld
        ' Ngày 14/09/2012
        ' Mục đích: Them ma hai quan va loai tien thue
        '*****************************************************************************
        Private strMa_hq As String
        Private strLoai_tt As String
        Private strTen_MA_HQ As String
        Private strLy_do_ctra As String
        Private strMA_SANPHAM As String
        Private strTT_CITAD As String
        Private strSEQ_NO As String
        Private strLoaiBL As String
        Private strMaCapChuong As String
        Private strMa_nh_tt As String
        Private strTen_nh_tt As String
        Private strMa_DV_DD As String
        Private strTen_DV_DD As String
        Private strMA_NTK As String
        Private str_Ten_NH_B As String
        Private str_Fone_Num As String
        Private str_Fax_Num As String
        Private str_So_DKKD As String
        Private strCQ_CAP As String
        Private strNgay_CQCAP As String

        'sonmt
        Private strMaHuyenNNT As String
        Private strTenHuyenNNT As String
        Private strMaTinhNNT As String
        Private strTenTinhNNT As String
        Private strHuyenNNTHAY As String
        Private strTinhNNTHAY As String

        'sonmt
        Private strLoaiNNT As String
        Private strTenLoaiNNT As String

        Private strMA_CN As String

        Private strDAC_DIEM_PTIEN As String
        Private _MA_HS As String
        Private _SO_THUADAT As String
        Private _SO_TO_BANDO As String
        Private _DIA_CHI_TS As String
        Private _MA_DBHC_TS As String
        Private _TEN_DBHC_TS As String
        Private _MA_GCN As String

        Private _Ma_Huyen_NNThue As String
        Private _Ma_Tinh_NNThue As String
        Private _Ten_Huyen_NNThue As String
        Private _Ten_Tinh_NNThue As String

        Private _MA_QUAN_HUYENNNTIEN As String
        Private _MA_TINH_TPNNTIEN As String
        Private _TEN_QUAN_HUYENNNTIEN As String
        Private _TEN_TINH_TPNNTIEN As String
        Private _Ten_Xa As String
        Private _REMARKS As String
        Private _Ten_tinh As String
        Private strPHI_GD2 As String
        Private strPHI_VAT2 As String
        Private _TT_CTHUE As Integer
        Private _LOAI_CTU As String


        Private _MA_CUC As String
        Private _TEN_CUC As String
        Public Property MA_CUC() As String
            Get
                Return _MA_CUC
            End Get
            Set(ByVal value As String)
                _MA_CUC = value
            End Set
        End Property
        Public Property TEN_CUC() As String
            Get
                Return _TEN_CUC
            End Get
            Set(ByVal value As String)
                _TEN_CUC = value
            End Set
        End Property
        Public Property LOAI_CTU() As String
            Get
                Return _LOAI_CTU
            End Get
            Set(ByVal value As String)
                _LOAI_CTU = value
            End Set
        End Property
        Public Property TT_CTHUE() As Integer
            Get
                Return _TT_CTHUE
            End Get
            Set(ByVal value As Integer)
                _TT_CTHUE = value
            End Set
        End Property
        Public Property PHI_GD2() As String
            Get
                Return strPHI_GD2
            End Get
            Set(ByVal Value As String)
                strPHI_GD2 = Value
            End Set
        End Property


        Public Property PHI_VAT2() As String
            Get
                Return strPHI_VAT2
            End Get
            Set(ByVal Value As String)
                strPHI_VAT2 = Value
            End Set
        End Property
        Public Property Ten_tinh() As String
            Get
                Return _Ten_tinh
            End Get
            Set(ByVal value As String)
                _Ten_tinh = value
            End Set
        End Property
        Public Property REMARKS() As String
            Get
                Return _REMARKS
            End Get
            Set(ByVal Value As String)
                _REMARKS = Value
            End Set
        End Property
        Public Property Ten_Xa() As String
            Get
                Return _Ten_Xa
            End Get
            Set(ByVal value As String)
                _Ten_Xa = value
            End Set
        End Property
        Public Property Ma_Huyen_NNThue() As String
            Get
                Return _Ma_Huyen_NNThue
            End Get
            Set(ByVal value As String)
                _Ma_Huyen_NNThue = value
            End Set
        End Property

        Public Property Ma_Tinh_NNThue() As String
            Get
                Return _Ma_Tinh_NNThue
            End Get
            Set(ByVal value As String)
                _Ma_Tinh_NNThue = value
            End Set
        End Property
        Public Property Ten_Huyen_NNThue() As String
            Get
                Return _Ten_Huyen_NNThue
            End Get
            Set(ByVal value As String)
                _Ten_Huyen_NNThue = value
            End Set
        End Property

        Public Property Ten_Tinh_NNThue() As String
            Get
                Return _Ten_Tinh_NNThue
            End Get
            Set(ByVal value As String)
                _Ten_Tinh_NNThue = value
            End Set
        End Property
        Public Property MA_QUAN_HUYENNNTIEN() As String
            Get
                Return _MA_QUAN_HUYENNNTIEN
            End Get
            Set(ByVal value As String)
                _MA_QUAN_HUYENNNTIEN = value
            End Set
        End Property

        Public Property MA_TINH_TPNNTIEN() As String
            Get
                Return _MA_TINH_TPNNTIEN
            End Get
            Set(ByVal value As String)
                _MA_TINH_TPNNTIEN = value
            End Set
        End Property
        Public Property TEN_QUAN_HUYENNNTIEN() As String
            Get
                Return _TEN_QUAN_HUYENNNTIEN
            End Get
            Set(ByVal value As String)
                _TEN_QUAN_HUYENNNTIEN = value
            End Set
        End Property

        Public Property TEN_TINH_TPNNTIEN() As String
            Get
                Return _TEN_TINH_TPNNTIEN
            End Get
            Set(ByVal value As String)
                _TEN_TINH_TPNNTIEN = value
            End Set
        End Property
        Public Property DAC_DIEM_PTIEN() As String
            Get
                Return strDAC_DIEM_PTIEN
            End Get
            Set(ByVal Value As String)
                strDAC_DIEM_PTIEN = Value
            End Set
        End Property

        Public Property MA_HS() As String
            Get
                Return _MA_HS
            End Get
            Set(ByVal value As String)
                _MA_HS = value
            End Set
        End Property
        Public Property SO_THUADAT() As String
            Get
                Return _SO_THUADAT
            End Get
            Set(ByVal value As String)
                _SO_THUADAT = value
            End Set
        End Property
        Public Property SO_TO_BANDO() As String
            Get
                Return _SO_TO_BANDO
            End Get
            Set(ByVal value As String)
                _SO_TO_BANDO = value
            End Set
        End Property
        Public Property DIA_CHI_TS() As String
            Get
                Return _DIA_CHI_TS
            End Get
            Set(ByVal value As String)
                _DIA_CHI_TS = value
            End Set
        End Property
        Public Property MA_DBHC_TS() As String
            Get
                Return _MA_DBHC_TS
            End Get
            Set(ByVal value As String)
                _MA_DBHC_TS = value
            End Set
        End Property
        Public Property TEN_DBHC_TS() As String
            Get
                Return _TEN_DBHC_TS
            End Get
            Set(ByVal value As String)
                _TEN_DBHC_TS = value
            End Set
        End Property
        Public Property MA_GCN() As String
            Get
                Return _MA_GCN
            End Get
            Set(ByVal value As String)
                _MA_GCN = value
            End Set
        End Property

        Public Property TIME_BEGIN() As String
            Get
                Return strTIME_BEGIN
            End Get
            Set(ByVal Value As String)
                strTIME_BEGIN = Value
            End Set
        End Property
        Public Property REF_CORE_TTSP() As String
            Get
                Return _REF_CORE_TTSP
            End Get
            Set(ByVal value As String)
                _REF_CORE_TTSP = value
            End Set
        End Property
        Public Property TEN_LOAI_NNT() As String
            Get
                Return strTenLoaiNNT
            End Get
            Set(ByVal Value As String)
                strTenLoaiNNT = Value
            End Set
        End Property
        Public Property LOAI_NNT() As String
            Get
                Return strLoaiNNT
            End Get
            Set(ByVal Value As String)
                strLoaiNNT = Value
            End Set
        End Property

        Public Property Ten_NH_B() As String
            Get
                Return str_Ten_NH_B
            End Get
            Set(ByVal value As String)
                str_Ten_NH_B = value
            End Set
        End Property
        Public Property Fone_Num() As String
            Get
                Return str_Fone_Num
            End Get
            Set(ByVal value As String)
                str_Fone_Num = value
            End Set
        End Property
        Public Property Fax_Num() As String
            Get
                Return str_Fax_Num
            End Get
            Set(ByVal value As String)
                str_Fax_Num = value
            End Set
        End Property
        Public Property So_DKKD() As String
            Get
                Return str_So_DKKD
            End Get
            Set(ByVal value As String)
                str_So_DKKD = value
            End Set
        End Property
        Public Property CQ_CAP() As String
            Get
                Return strCQ_CAP
            End Get
            Set(ByVal value As String)
                strCQ_CAP = value
            End Set
        End Property
        Public Property Ngay_CQCap() As String
            Get
                Return strNgay_CQCAP
            End Get
            Set(ByVal value As String)
                strNgay_CQCAP = value
            End Set
        End Property
        Public Property TrangThai_BL() As String
            Get
                Return strTrangThai_BL
            End Get
            Set(ByVal value As String)
                strTrangThai_BL = value
            End Set
        End Property
        Public Property Kenh_CT() As String
            Get
                Return strKenh_CT
            End Get
            Set(ByVal value As String)
                strKenh_CT = value
            End Set
        End Property
        Public Property Ngay_HD_NT() As String
            Get
                Return strNgay_HD_NT
            End Get
            Set(ByVal value As String)
                strNgay_HD_NT = value
            End Set
        End Property
        Public Property Ngay_HDon() As String
            Get
                Return strNgay_HDon
            End Get
            Set(ByVal value As String)
                strNgay_HDon = value
            End Set
        End Property
        Public Property Ngay_VTD() As String
            Get
                Return strNgay_VTD
            End Get
            Set(ByVal value As String)
                strNgay_VTD = value
            End Set
        End Property
        Public Property HDon() As String
            Get
                Return strHDon
            End Get
            Set(ByVal value As String)
                strHDon = value
            End Set
        End Property
        Public Property VTD() As String
            Get
                Return strVTD
            End Get
            Set(ByVal value As String)
                strVTD = value
            End Set
        End Property
        Public Property HD_NT() As String
            Get
                Return strHD_NT
            End Get
            Set(ByVal value As String)
                strHD_NT = value
            End Set
        End Property
        Public Property MA_HQ() As String
            Get
                Return strMa_hq
            End Get
            Set(ByVal value As String)
                strMa_hq = value
            End Set
        End Property
	Public Property TEN_HQ() As String
            Get
                Return strTen_hq
            End Get
            Set(ByVal value As String)
                strTen_hq = value
            End Set
        End Property
	Public Property TEN_HQ_PH() As String
            Get
                Return strTen_hq_ph
            End Get
            Set(ByVal value As String)
                strTen_hq_ph = value
            End Set
        End Property
        Public Property LOAI_TT() As String
            Get
                Return strLoai_tt
            End Get
            Set(ByVal value As String)
                strLoai_tt = value
            End Set
        End Property
        Public Property TEN_MA_HQ() As String
            Get
                Return strTen_MA_HQ
            End Get
            Set(ByVal value As String)
                strTen_MA_HQ = value
            End Set
        End Property

        Public Property LY_DO_CTRA() As String
            Get
                Return strLy_do_ctra
            End Get
            Set(ByVal value As String)
                strLy_do_ctra = value
            End Set
        End Property

        ' ****************************************************************************
        ' End
        '*****************************************************************************

        Public Property KHCT() As String
            Get
                Return strKhct
            End Get
            Set(ByVal value As String)
                strKhct = value
            End Set
        End Property


        Public Property Ten_kb() As String
            Get
                Return strTen_kb
            End Get
            Set(ByVal value As String)
                strTen_kb = value
            End Set
        End Property

        Public Property Ten_nv() As String
            Get
                Return strTen_nv
            End Get
            Set(ByVal value As String)
                strTen_nv = value
            End Set
        End Property

        Public Property Huyen_nnthue() As String
            Get
                Return strHuyen_nnthue
            End Get
            Set(ByVal value As String)
                strHuyen_nnthue = value
            End Set
        End Property

        Public Property Tinh_nnthue() As String
            Get
                Return strTinh_nnthue
            End Get
            Set(ByVal value As String)
                strTinh_nnthue = value
            End Set
        End Property

        Public Property Ten_ks() As String
            Get
                Return strTen_ks
            End Get
            Set(ByVal value As String)
                strTen_ks = value
            End Set
        End Property

        Public Property Ten_cqthu() As String
            Get
                Return strTen_cqthu
            End Get
            Set(ByVal value As String)
                strTen_cqthu = value
            End Set
        End Property

        Public Property Lhxnk() As String
            Get
                Return strLhxnk
            End Get
            Set(ByVal value As String)
                strLhxnk = value
            End Set
        End Property

        Public Property Vt_lhxnk() As String
            Get
                Return strVt_lhxnk
            End Get
            Set(ByVal value As String)
                strVt_lhxnk = value
            End Set
        End Property

        Public Property Ten_lhxnk() As String
            Get
                Return strTen_lhxnk
            End Get
            Set(ByVal value As String)
                strTen_lhxnk = value
            End Set
        End Property

        Public Property Ten_nt() As String
            Get
                Return strTen_nt
            End Get
            Set(ByVal value As String)
                strTen_nt = value
            End Set
        End Property

        Public Property Ten_nh_a() As String
            Get
                Return strTen_nh_a
            End Get
            Set(ByVal value As String)
                strTen_nh_a = value
            End Set
        End Property

        Public Property SO_CMND() As String
            Get
                Return strSO_CMND
            End Get
            Set(ByVal Value As String)
                strSO_CMND = Value
            End Set
        End Property
        Public Property SO_FONE() As String
            Get
                Return strSO_FONE
            End Get
            Set(ByVal Value As String)
                strSO_FONE = Value
            End Set
        End Property

        Public Property Ttien_tthu() As Integer
            Get
                Return strTtien_tthu
            End Get
            Set(ByVal value As Integer)
                strTtien_tthu = value
            End Set
        End Property

        Public Property Ghi_chu() As String
            Get
                Return strGhi_chu
            End Get
            Set(ByVal value As String)
                strGhi_chu = value
            End Set
        End Property

        Public Property Tk_ns() As String
            Get
                Return strTk_ns
            End Get
            Set(ByVal value As String)
                strTk_ns = value
            End Set
        End Property

        Public Property Ten_tk_ns() As String
            Get
                Return strTen_tk_ns
            End Get
            Set(ByVal value As String)
                strTen_tk_ns = value
            End Set
        End Property

        Public Property Phuong_thuc() As String
            Get
                Return strPhuong_thuc
            End Get
            Set(ByVal value As String)
                strPhuong_thuc = value
            End Set
        End Property

        Public Property Hinh_thuc() As String
            Get
                Return strHinh_thuc
            End Get
            Set(ByVal value As String)
                strHinh_thuc = value
            End Set
        End Property

        Public Property Khoi_phuc() As String
            Get
                Return strKhoi_phuc
            End Get
            Set(ByVal value As String)
                strKhoi_phuc = value
            End Set
        End Property

        Public Property Reponse_code() As String
            Get
                Return strReponse_code
            End Get
            Set(ByVal value As String)
                strReponse_code = value
            End Set
        End Property

        Public Property Hq_tran_id() As String
            Get
                Return strHq_tran_id
            End Get
            Set(ByVal value As String)
                strHq_tran_id = value
            End Set
        End Property

        Public Property Hq_loai_thue() As String
            Get
                Return strHq_loai_thue
            End Get
            Set(ByVal value As String)
                strHq_loai_thue = value
            End Set
        End Property

        Public Property Res_hq() As String
            Get
                Return strRes_hq
            End Get
            Set(ByVal value As String)
                strRes_hq = value
            End Set
        End Property

        Public Property So_bl() As String
            Get
                Return strSo_bl
            End Get
            Set(ByVal value As String)
                strSo_bl = value
            End Set
        End Property

        Public Property Songay_bl() As Integer
            Get
                Return strSongay_bl
            End Get
            Set(ByVal value As Integer)
                strSongay_bl = value
            End Set
        End Property

        Public Property Kieu_bl() As String
            Get
                Return strKieu_bl
            End Get
            Set(ByVal value As String)
                strKieu_bl = value
            End Set
        End Property

        Public Property Ma_hq_ph() As String
            Get
                Return strMa_hq_ph
            End Get
            Set(ByVal value As String)
                strMa_hq_ph = value
            End Set
        End Property

        Public Property Dien_giai() As String
            Get
                Return strDien_giai
            End Get
            Set(ByVal value As String)
                strDien_giai = value
            End Set
        End Property

        Public Property Trangthai_lapct() As String
            Get
                Return strTrangthai_lapct
            End Get
            Set(ByVal value As String)
                strTrangthai_lapct = value
            End Set
        End Property

        Public Property Ngay_batdau() As String
            Get
                Return strNgay_batdau
            End Get
            Set(ByVal value As String)
                strNgay_batdau = value
            End Set
        End Property

        Public Property Ngay_ketthuc() As String
            Get
                Return strNgay_ketthuc
            End Get
            Set(ByVal value As String)
                strNgay_ketthuc = value
            End Set
        End Property

        Public Property Error_code_hq() As String
            Get
                Return strError_code_hq
            End Get
            Set(ByVal value As String)
                strError_code_hq = value
            End Set
        End Property

        Public Property So_ct_ctu() As String
            Get
                Return strSo_ct_ctu
            End Get
            Set(ByVal value As String)
                strSo_ct_ctu = value
            End Set
        End Property

        Public Property Dchieu_hq() As String
            Get
                Return strDchieu_hq
            End Get
            Set(ByVal value As String)
                strDchieu_hq = value
            End Set
        End Property

        Public Property Is_ma_hq() As String
            Get
                Return strIs_ma_hq
            End Get
            Set(ByVal value As String)
                strIs_ma_hq = value
            End Set
        End Property

        Public Property Ngay_dc_hq() As String
            Get
                Return strNgay_dc_hq
            End Get
            Set(ByVal value As String)
                strNgay_dc_hq = value
            End Set
        End Property

        Public Property Kb_thu_ho() As String
            Get
                Return strKb_thu_ho
            End Get
            Set(ByVal value As String)
                strKb_thu_ho = value
            End Set
        End Property

        Public Property Ma_loaitien() As String
            Get
                Return strMa_loaitien
            End Get
            Set(ByVal value As String)
                strMa_loaitien = value
            End Set
        End Property

        Public Property Ma_hq_cqt() As String
            Get
                Return strMa_hq_cqt
            End Get
            Set(ByVal value As String)
                strMa_hq_cqt = value
            End Set
        End Property

        Public Property So_bt_hq() As Integer
            Get
                Return strSo_bt_hq
            End Get
            Set(ByVal value As Integer)
                strSo_bt_hq = value
            End Set
        End Property

        '--------------------------

        Public Property QUAN_HUYEN_NNTIEN() As String
            Get
                Return strQuan_Huyen_NNTien
            End Get
            Set(ByVal Value As String)
                strQuan_Huyen_NNTien = Value
            End Set
        End Property
        Public Property TKHT_NH() As String
            Get
                Return strTKHTNH
            End Get
            Set(ByVal Value As String)
                strTKHTNH = Value
            End Set
        End Property
        Public Property TEN_TK_NO() As String
            Get
                Return strTen_TKNo
            End Get
            Set(ByVal Value As String)
                strTen_TKNo = Value
            End Set
        End Property
        Public Property TEN_TK_CO() As String
            Get
                Return strTen_TKCo
            End Get
            Set(ByVal Value As String)
                strTen_TKCo = Value
            End Set
        End Property
        Public Property TINH_TPHO_NNTIEN() As String
            Get
                Return strTinh_TPho_NNTien
            End Get
            Set(ByVal Value As String)
                strTinh_TPho_NNTien = Value
            End Set
        End Property

        Public Property So_RM() As String
            Get
                Return strSoRM
            End Get
            Set(ByVal Value As String)
                strSoRM = Value
            End Set
        End Property
        Public Property MATELLER_GDV() As String
            Get
                Return strtellerGDV
            End Get
            Set(ByVal Value As String)
                strtellerGDV = Value
            End Set
        End Property
        Public Property MATELLER_KSV() As String
            Get
                Return strtellerKSV
            End Get
            Set(ByVal Value As String)
                strtellerKSV = Value
            End Set
        End Property
        Public Property RM_REF_NO() As String
            Get
                Return strRM_REF_NO
            End Get
            Set(ByVal Value As String)
                strRM_REF_NO = Value
            End Set
        End Property

        Public Property REF_NO() As String
            Get
                Return strREF_NO
            End Get
            Set(ByVal Value As String)
                strREF_NO = Value
            End Set
        End Property

        Public Property TK_KB_NH() As String
            Get
                Return strTK_KB_NH
            End Get
            Set(ByVal Value As String)
                strTK_KB_NH = Value
            End Set
        End Property
        Public Property Mode() As String
            Get
                Return intMode
            End Get
            Set(ByVal Value As String)
                intMode = Value
            End Set
        End Property
        Public Property PT_TT() As String
            Get
                Return strPT_TT
            End Get
            Set(ByVal Value As String)
                strPT_TT = Value
            End Set
        End Property
        Public Property TEN_KH_NH() As String
            Get
                Return strTEN_KH_NH
            End Get
            Set(ByVal Value As String)
                strTEN_KH_NH = Value
            End Set
        End Property
        Public Property TK_GL_NH() As String
            Get
                Return strTK_GL_NH
            End Get
            Set(ByVal Value As String)
                strTK_GL_NH = Value
            End Set
        End Property

        Public Property SHKB() As String
            Get
                Return strSHKB
            End Get
            Set(ByVal Value As String)
                strSHKB = Value
            End Set
        End Property
        Public Property Ngay_KB() As Integer
            Get
                Return iNgay_KB
            End Get
            Set(ByVal Value As Integer)
                iNgay_KB = Value
            End Set
        End Property
        Public Property Ma_NV() As Integer
            Get
                Return iMa_NV
            End Get
            Set(ByVal Value As Integer)
                iMa_NV = Value
            End Set
        End Property
        Public Property So_BT() As Integer
            Get
                Return iSo_BT
            End Get
            Set(ByVal Value As Integer)
                iSo_BT = Value
            End Set
        End Property
        Public Property Ma_DThu() As String
            Get
                Return strMa_DThu
            End Get
            Set(ByVal Value As String)
                strMa_DThu = Value
            End Set
        End Property
        Public Property So_BThu() As Integer
            Get
                Return iSo_BThu
            End Get
            Set(ByVal Value As Integer)
                iSo_BThu = Value
            End Set
        End Property
        Public Property KyHieu_CT() As String
            Get
                Return strKyHieu_CT
            End Get
            Set(ByVal Value As String)
                strKyHieu_CT = Value
            End Set
        End Property
        Public Property So_CT() As String
            Get
                Return strSo_CT
            End Get
            Set(ByVal Value As String)
                strSo_CT = Value
            End Set
        End Property
        Public Property So_CT_TuChinh() As String
            Get
                Return strSo_CT_TuChinh
            End Get
            Set(ByVal Value As String)
                strSo_CT_TuChinh = Value
            End Set
        End Property
        Public Property So_CT_NH() As String
            Get
                Return strSo_CT_NH
            End Get
            Set(ByVal Value As String)
                strSo_CT_NH = Value
            End Set
        End Property
        Public Property Ma_NNTien() As String
            Get
                Return strMa_NNTien
            End Get
            Set(ByVal Value As String)
                strMa_NNTien = Value
            End Set
        End Property
        Public Property Ten_NNTien() As String
            Get
                Return strTen_NNTien
            End Get
            Set(ByVal Value As String)
                strTen_NNTien = Value
            End Set
        End Property
        Public Property DC_NNTien() As String
            Get
                Return strDC_NNTien
            End Get
            Set(ByVal Value As String)
                strDC_NNTien = Value
            End Set
        End Property
        Public Property Ma_NNThue() As String
            Get
                Return strMa_NNThue
            End Get
            Set(ByVal Value As String)
                strMa_NNThue = Value
            End Set
        End Property
        Public Property Ten_NNThue() As String
            Get
                Return strTen_NNThue
            End Get
            Set(ByVal Value As String)
                strTen_NNThue = Value
            End Set
        End Property
        Public Property DC_NNThue() As String
            Get
                Return strDC_NNThue
            End Get
            Set(ByVal Value As String)
                strDC_NNThue = Value
            End Set
        End Property
        Public Property Ly_Do() As String
            Get
                Return strLy_Do
            End Get
            Set(ByVal Value As String)
                strLy_Do = Value
            End Set
        End Property
        Public Property Ma_KS() As String
            Get
                Return iMa_KS
            End Get
            Set(ByVal Value As String)
                iMa_KS = Value
            End Set
        End Property
        Public Property Ma_TQ() As String
            Get
                Return iMa_TQ
            End Get
            Set(ByVal Value As String)
                iMa_TQ = Value
            End Set
        End Property
        Public Property So_QD() As String
            Get
                Return strSo_QD
            End Get
            Set(ByVal Value As String)
                strSo_QD = Value
            End Set
        End Property
        Public Property Ngay_QD() As String
            Get
                Return strNgay_QD
            End Get
            Set(ByVal Value As String)
                strNgay_QD = Value
            End Set
        End Property
        Public Property CQ_QD() As String
            Get
                Return strCQ_QD
            End Get
            Set(ByVal Value As String)
                strCQ_QD = Value
            End Set
        End Property
        Public Property Ngay_CT() As Integer
            Get
                Return iNgay_CT
            End Get
            Set(ByVal Value As Integer)
                iNgay_CT = Value
            End Set
        End Property
        Public Property Ngay_HT() As String
            Get
                Return strNgay_HT
            End Get
            Set(ByVal Value As String)
                strNgay_HT = Value
            End Set
        End Property
        Public Property Ma_CQThu() As String
            Get
                Return strMa_CQThu
            End Get
            Set(ByVal Value As String)
                strMa_CQThu = Value
            End Set
        End Property
        Public Property XA_ID() As String
            Get
                Return strXA_ID
            End Get
            Set(ByVal Value As String)
                strXA_ID = Value
            End Set
        End Property
        Public Property Ma_Tinh() As String
            Get
                Return strMa_Tinh
            End Get
            Set(ByVal Value As String)
                strMa_Tinh = Value
            End Set
        End Property
        Public Property Ma_Huyen() As String
            Get
                Return strMa_Huyen
            End Get
            Set(ByVal Value As String)
                strMa_Huyen = Value
            End Set
        End Property
        Public Property Ma_Xa() As String
            Get
                Return strMa_Xa
            End Get
            Set(ByVal Value As String)
                strMa_Xa = Value
            End Set
        End Property
        Public Property TK_No() As String
            Get
                Return strTK_No
            End Get
            Set(ByVal Value As String)
                strTK_No = Value
            End Set
        End Property
        Public Property TK_Co() As String
            Get
                Return strTK_Co
            End Get
            Set(ByVal Value As String)
                strTK_Co = Value
            End Set
        End Property
        Public Property So_TK() As String
            Get
                Return strSo_TK
            End Get
            Set(ByVal Value As String)
                strSo_TK = Value
            End Set
        End Property
        Public Property Ngay_TK() As String
            Get
                Return strNgay_TK
            End Get
            Set(ByVal Value As String)
                strNgay_TK = Value
            End Set
        End Property
        Public Property LH_XNK() As String
            Get
                Return strLH_XNK
            End Get
            Set(ByVal Value As String)
                strLH_XNK = Value
            End Set
        End Property
        Public Property DVSDNS() As String
            Get
                Return strDVSDNS
            End Get
            Set(ByVal Value As String)
                strDVSDNS = Value
            End Set
        End Property
        Public Property Ten_DVSDNS() As String
            Get
                Return strTen_DVSDNS
            End Get
            Set(ByVal Value As String)
                strTen_DVSDNS = Value
            End Set
        End Property
        Public Property Ma_LThue() As String
            Get
                Return strMa_LThue
            End Get
            Set(ByVal Value As String)
                strMa_LThue = Value
            End Set
        End Property
        Public Property TG_ID() As String
            Get
                Return iTG_ID
            End Get
            Set(ByVal Value As String)
                iTG_ID = Value
            End Set
        End Property
        Public Property Ma_NT() As String
            Get
                Return strMa_NT
            End Get
            Set(ByVal Value As String)
                strMa_NT = Value
            End Set
        End Property
        Public Property Ty_Gia() As Double
            Get
                Return dblTy_Gia
            End Get
            Set(ByVal Value As Double)
                dblTy_Gia = Value
            End Set
        End Property
        Public Property So_Khung() As String
            Get
                Return strSo_Khung
            End Get
            Set(ByVal Value As String)
                strSo_Khung = Value
            End Set
        End Property
        Public Property So_May() As String
            Get
                Return strSo_May
            End Get
            Set(ByVal Value As String)
                strSo_May = Value
            End Set
        End Property

        Public Property TK_KH_NH() As String
            Get
                Return strTK_KH_NH
            End Get
            Set(ByVal Value As String)
                strTK_KH_NH = Value
            End Set
        End Property
        Public Property NGAY_KH_NH() As String
            Get
                Return strNGAY_KH_NH
            End Get
            Set(ByVal Value As String)
                strNGAY_KH_NH = Value
            End Set
        End Property
        Public Property MA_NH_A() As String
            Get
                Return strMA_NH_A
            End Get
            Set(ByVal Value As String)
                strMA_NH_A = Value
            End Set
        End Property
        Public Property MA_NH_B() As String
            Get
                Return strMA_NH_B
            End Get
            Set(ByVal Value As String)
                strMA_NH_B = Value
            End Set
        End Property

        Public Property TTien() As Double
            Get
                Return dblTTien
            End Get
            Set(ByVal Value As Double)
                dblTTien = Value
            End Set
        End Property
        Public Property TT_TThu() As Double
            Get
                Return dblTT_TThu
            End Get
            Set(ByVal Value As Double)
                dblTT_TThu = Value
            End Set
        End Property
        Public Property Lan_In() As String
            Get
                Return strLan_In
            End Get
            Set(ByVal Value As String)
                strLan_In = Value
            End Set
        End Property
        Public Property Trang_Thai() As String
            Get
                Return strTrang_Thai
            End Get
            Set(ByVal Value As String)
                strTrang_Thai = Value
            End Set
        End Property

        Public Property TK_KH_Nhan() As String
            Get
                Return strTK_KH_Nhan
            End Get
            Set(ByVal Value As String)
                strTK_KH_Nhan = Value
            End Set
        End Property
        Public Property Ten_KH_Nhan() As String
            Get
                Return strTen_KH_Nhan
            End Get
            Set(ByVal Value As String)
                strTen_KH_Nhan = Value
            End Set
        End Property
        Public Property Diachi_KH_Nhan() As String
            Get
                Return strDiachi_KH_Nhan
            End Get
            Set(ByVal Value As String)
                strDiachi_KH_Nhan = Value
            End Set
        End Property
        Public Property TTien_NT() As Double
            Get
                Return dblTTien_NT
            End Get
            Set(ByVal Value As Double)
                dblTTien_NT = Value
            End Set
        End Property

        Public Property So_BT_KTKB() As Integer
            Get
                Return intSo_BT_KTKT
            End Get
            Set(ByVal Value As Integer)
                intSo_BT_KTKT = Value
            End Set
        End Property

        Public Property So_BK() As String
            Get
                Return strSo_BK
            End Get
            Set(ByVal Value As String)
                strSo_BK = Value
            End Set
        End Property

        Public Property Ngay_BK() As String
            Get
                Return strNgay_BK
            End Get
            Set(ByVal Value As String)
                strNgay_BK = Value
            End Set
        End Property

        Public Property TT_BDS() As String
            Get
                Return strTT_BDS
            End Get
            Set(ByVal Value As String)
                strTT_BDS = Value
            End Set
        End Property


        Public Property TT_NSTT() As String
            Get
                Return strTT_NSTT
            End Get
            Set(ByVal Value As String)
                strTT_NSTT = Value
            End Set
        End Property



        Public Property PHI_GD() As String
            Get
                Return strPHI_GD
            End Get
            Set(ByVal Value As String)
                strPHI_GD = Value
            End Set
        End Property


        Public Property PHI_VAT() As String
            Get
                Return strPHI_VAT
            End Get
            Set(ByVal Value As String)
                strPHI_VAT = Value
            End Set
        End Property
       

        Public Property PT_TINHPHI() As String
            Get
                Return strPT_TINHPHI
            End Get
            Set(ByVal Value As String)
                strPT_TINHPHI = Value
            End Set
        End Property

        Public Property MA_NH_TT() As String
            Get
                Return strMa_nh_tt
            End Get
            Set(ByVal Value As String)
                strMa_nh_tt = Value
            End Set
        End Property

        Public Property TEN_NH_TT() As String
            Get
                Return strTen_nh_tt
            End Get
            Set(ByVal Value As String)
                strTen_nh_tt = Value
            End Set
        End Property
	
	        Public Property CIF() As String
            Get
                Return strCif
            End Get
            Set(ByVal Value As String)
                strCif = Value
            End Set
        End Property
        Public Property SAN_PHAM() As String
            Get
                Return strMA_SANPHAM
            End Get
            Set(ByVal Value As String)
                strMA_SANPHAM = Value
            End Set
        End Property
        Public Property TT_CITAD() As String
            Get
                Return strTT_CITAD
            End Get
            Set(ByVal Value As String)
                strTT_CITAD = Value
            End Set
        End Property
        Public Property SEQ_NO() As String
            Get
                Return strSEQ_NO
            End Get
            Set(ByVal Value As String)
                strSEQ_NO = Value
            End Set
        End Property
        Public Property LOAI_BL() As String
            Get
                Return strLoaiBL
            End Get
            Set(ByVal Value As String)
                strLoaiBL = Value
            End Set
        End Property
        Public Property MA_CAP_CHUONG() As String
            Get
                Return strMaCapChuong
            End Get
            Set(ByVal Value As String)
                strMaCapChuong = Value
            End Set
        End Property
        
        Public Property MA_DV_DD() As String
            Get
                Return strMa_DV_DD
            End Get
            Set(ByVal Value As String)
                strMa_DV_DD = Value
            End Set
        End Property
        Public Property TEN_DV_DD() As String
            Get
                Return strTen_DV_DD
            End Get
            Set(ByVal Value As String)
                strTen_DV_DD = Value
            End Set
        End Property
        Public Property MA_NTK() As String
            Get
                Return strMA_NTK
            End Get
            Set(ByVal Value As String)
                strMA_NTK = Value
            End Set
        End Property
        Public Property VTD2() As String
            Get
                Return strVTD2
            End Get
            Set(ByVal value As String)
                strVTD2 = value
            End Set
        End Property
        Public Property Ngay_VTD2() As String
            Get
                Return strNgay_VTD2
            End Get
            Set(ByVal value As String)
                strNgay_VTD2 = value
            End Set
        End Property
        Public Property VTD3() As String
            Get
                Return strVTD3
            End Get
            Set(ByVal value As String)
                strVTD3 = value
            End Set
        End Property
        Public Property Ngay_VTD3() As String
            Get
                Return strNgay_VTD3
            End Get
            Set(ByVal value As String)
                strNgay_VTD3 = value
            End Set
        End Property
        Public Property VTD4() As String
            Get
                Return strVTD4
            End Get
            Set(ByVal value As String)
                strVTD4 = value
            End Set
        End Property
        Public Property Ngay_VTD4() As String
            Get
                Return strNgay_VTD4
            End Get
            Set(ByVal value As String)
                strNgay_VTD4 = value
            End Set
        End Property
        Public Property VTD5() As String
            Get
                Return strVTD5
            End Get
            Set(ByVal value As String)
                strVTD5 = value
            End Set
        End Property
        Public Property Ngay_VTD5() As String
            Get
                Return strNgay_VTD5
            End Get
            Set(ByVal value As String)
                strNgay_VTD5 = value
            End Set
        End Property
        Public Property MA_HQ_KB() As String
            Get
                Return strMa_hq_kb
            End Get
            Set(ByVal value As String)
                strMa_hq_kb = value
            End Set
        End Property

        'sonmt
        Public Property MA_HUYEN_NNT() As String
            Get
                Return strMaHuyenNNT
            End Get
            Set(ByVal value As String)
                strMaHuyenNNT = value
            End Set
        End Property
        Public Property TEN_HUYEN_NNT() As String
            Get
                Return strTenHuyenNNT
            End Get
            Set(ByVal value As String)
                strTenHuyenNNT = value
            End Set
        End Property
        Public Property MA_TINH_NNT() As String
            Get
                Return strMaTinhNNT
            End Get
            Set(ByVal value As String)
                strMaTinhNNT = value
            End Set
        End Property
        Public Property TEN_TINH_NNT() As String
            Get
                Return strTenTinhNNT
            End Get
            Set(ByVal value As String)
                strTenTinhNNT = value
            End Set
        End Property
        Public Property HUYEN_NNTHAY() As String
            Get
                Return strHuyenNNTHAY
            End Get
            Set(ByVal value As String)
                strHuyenNNTHAY = value
            End Set
        End Property
        Public Property TINH_NNTHAY() As String
            Get
                Return strTinhNNTHAY
            End Get
            Set(ByVal value As String)
                strTinhNNTHAY = value
            End Set
        End Property
        Public Property MA_CN() As String
            Get
                Return strMA_CN
            End Get
            Set(ByVal Value As String)
                strMA_CN = Value
            End Set
        End Property

    End Class
    
End Namespace
