﻿
Public Class infKQPhi

    Private v_Errcode As String
    Private v_ErrMessage As String
    Private v_FreeAMT As String
    Private v_FeeVAT As String
    Private v_FeeName As String

    Public Property Errcode As String
        Set(ByVal value As String)
            Me.v_Errcode = value
        End Set
        Get
            Return Me.v_Errcode
        End Get
    End Property
    Public Property ErrMessage As String
        Set(ByVal value As String)
            Me.v_ErrMessage = value
        End Set
        Get
            Return Me.v_ErrMessage
        End Get
    End Property
    Public Property FreeAMT As String
        Set(ByVal value As String)
            Me.v_FreeAMT = value
        End Set
        Get
            Return Me.v_FreeAMT
        End Get
    End Property
    Public Property FeeVAT As String
        Set(ByVal value As String)
            Me.v_FeeVAT = value
        End Set
        Get
            Return Me.v_FeeVAT
        End Get
    End Property
    Public Property FeeName As String
        Set(ByVal value As String)
            Me.v_FeeName = value
        End Set
        Get
            Return Me.v_FeeName
        End Get
    End Property

End Class
