﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Xml.Serialization
Imports System.Xml
Imports System.IO

Namespace CitadContentEx_25
    <Serializable()> _
    <XmlRootAttribute("VST")> _
    Public Class CitadContentEx_25
        Public Sub New()
        End Sub

        <XmlElement("STC")> _
        Public STC As String
        <XmlElement("SCT")> _
        Public SCT As String
        <XmlElement("KCT")> _
        Public KCT As String
        <XmlElement("TNT")> _
        Public TNT As String
        <XmlElement("DNT")> _
        Public DNT As String
        <XmlElement("MST")> _
        Public MST As String
        <XmlElement("MDB")> _
        Public MDB As String = "00000"
        <XmlElement("CQT")> _
        Public CQT As String = "0000000"
        <XmlElement("TCQ")> _
        Public TCQ As String
        <XmlElement("NNT")> _
        Public NNT As String
        <XmlElement("LTH")> _
        Public LTH As String
        <XmlElement("TKN")> _
        Public TKN As String
        <XmlElement("KLN")> _
        Public KLN As String
        <XmlElement("MKB")> _
        Public MKB As String
        <XmlElement("VSTD")> _
        Public VSTD As List(Of VSTD_DATA)

        Public Function MSGToObject(ByVal p_XML As String) As CitadContentEx_25
            Dim serializer As New XmlSerializer(GetType(CitadContentEx_25))

            Dim reader As XmlReader = XmlReader.Create(New StringReader(p_XML))

            Dim LoadedObjTmp As CitadContentEx_25 = DirectCast(serializer.Deserialize(reader), CitadContentEx_25)

            Return LoadedObjTmp

        End Function

        Public Function MSGtoXML(ByVal p_MsgIn As CitadContentEx_25) As String
            Dim serializer As New XmlSerializer(GetType(CitadContentEx_25))
            Dim sw As New StringWriter()
            serializer.Serialize(sw, p_MsgIn)
            Dim strTemp As String = sw.ToString()
            Return strTemp

        End Function
        Private Sub serializer_UnknownNode(ByVal sender As Object, ByVal e As XmlNodeEventArgs)
            Console.WriteLine("Unknown Node:" + e.Name + vbTab + e.Text)
        End Sub

        Private Sub serializer_UnknownAttribute(ByVal sender As Object, ByVal e As XmlAttributeEventArgs)
            Dim attr As System.Xml.XmlAttribute = e.Attr
            Console.WriteLine("Unknown attribute " + attr.Name + "='" + attr.Value + "'")
        End Sub
        Public Function toStringCITAD25() As String



            Dim XMLCITAD As String = "<?xml version=" & ControlChars.Quote & "1.0" & ControlChars.Quote & "?>"
            XMLCITAD &= "<VST>"
            XMLCITAD &= "<STC>" & STC & "</STC>"
            XMLCITAD &= "<SCT>" & SCT & "</SCT>"
            XMLCITAD &= "<KCT>" & KCT & "</KCT>"
            XMLCITAD &= "<TNT>" & TNT & "</TNT>"
            XMLCITAD &= "<DNT>" & DNT & "</DNT>"
            XMLCITAD &= "<MST>" & MST & "</MST>"
            XMLCITAD &= "<MDB>" & MDB & "</MDB>"
            XMLCITAD &= "<CQT>" & CQT & "</CQT>"
            XMLCITAD &= "<TCQ>" & TCQ & "</TCQ>"
            XMLCITAD &= "<NNT>" & NNT & "</NNT>"
            XMLCITAD &= "<LTH>" & LTH & "</LTH>"
            XMLCITAD &= "<TKN>" & TKN & "</TKN>"
            XMLCITAD &= "<KLN>" & KLN & "</KLN>"
            XMLCITAD &= "<MKB>" & MKB & "</MKB>"
            For i = 0 To VSTD.Count - 1
                XMLCITAD &= "<VSTD>"
                XMLCITAD &= "<STK>" & VSTD(i).STK & "</STK>"
                XMLCITAD &= "<NTK>" & VSTD(i).NTK & "</NTK>"
                XMLCITAD &= "<NDN>" & VSTD(i).NDN & "</NDN>"
                XMLCITAD &= "<STN>" & VSTD(i).STN & "</STN>"
                XMLCITAD &= "<MND>" & VSTD(i).MND & "</MND>"
                XMLCITAD &= "<MCH>" & VSTD(i).MCH & "</MCH>"
                XMLCITAD &= "</VSTD>"
            Next

            XMLCITAD &= "</VST>"
            Return XMLCITAD
        End Function
    End Class


    Public Class VSTD_DATA

        Public Sub New()
        End Sub
        <XmlElement("STK")> _
        Public STK As String
        <XmlElement("NTK")> _
        Public NTK As String
        <XmlElement("NDN")> _
        Public NDN As String
        <XmlElement("STN")> _
        Public STN As String
        <XmlElement("MND")> _
        Public MND As String
        <XmlElement("MCH")> _
        Public MCH As String
    End Class

End Namespace
