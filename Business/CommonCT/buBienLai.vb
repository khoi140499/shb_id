﻿Imports VBOracleLib
Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Imports Business.Common
Imports Business.NewChungTu

Namespace ChungTu
    Public Class buBienLai
        Public Shared Function Update(ByVal strKHBL As String, ByVal strSoBL As String) As Boolean
            Dim daBL As New daBienLai
            Return daBL.Update(strKHBL, strSoBL)
        End Function

        Public Shared Function blnUpdateBL(ByVal objBL As infBienLai) As Boolean
            Dim daBL As New daBienLai
            Dim blnResult As Boolean
            Try
                daBL.UpdateBLT(objBL)
                blnResult = True
            Catch ex As Exception
                blnResult = False
            End Try
            Return blnResult
        End Function
      

        Public Shared Function Insert(ByVal objBL As infBienLai, ByVal intLanIn As Integer) As Boolean
            Dim daBL As New daBienLai
            Dim blnResult As Boolean
            Try
                objBL.Lan_In = intLanIn
                gstrSoBL = Valid_SoBL_Existed(gstrSoBL, gstrKHBL)
                objBL.So_BL = gstrSoBL
                daBL.Insert(objBL)
                ' Tăng số bút toán lên
                gintSoBTCT += 1
                'Tăng số biên lai lên
                gstrSoBL = GetSoBL_Next()
                blnResult = True
            Catch ex As Exception
                blnResult = False
            End Try
            Return blnResult
        End Function

        Public Shared Function SelectLHT() As DataSet
            Dim daBL As New daBienLai
            Return daBL.SelectLHT()
        End Function

        Public Shared Function SelectLHT(ByVal strMaLH As String) As DataSet
            Dim daBL As New daBienLai
            Return daBL.SelectLHT(strMaLH)
        End Function

        Public Shared Function SelectBL(ByVal lngNgayLV As Long, ByVal iMaNV As Integer, ByVal iAll As Boolean) As DataSet
            Dim daBL As New daBienLai
            Return daBL.SelectBL(lngNgayLV, iMaNV, iAll)
        End Function

        Public Shared Sub KhoiPhucBL(ByVal strSoBL As String, ByVal strSoBT As String)
            Try
                Dim daBL As New daBienLai
                daBL.KhoiPhucBL(strSoBL, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub KiemSoatBL(ByVal strSoBL As String, ByVal strSoBT As String)
            Try
                Dim daBL As New daBienLai
                daBL.KiemSoat(strSoBL, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub ChuyenKiemSoat(ByVal strSoBL As String, ByVal strSoBT As String)
            Try
                Dim daBL As New daBienLai
                daBL.ChuyenKS(strSoBL, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub ChuyenTraBL(ByVal strSoBL As String, ByVal strSoBT As String)
            Try
                Dim daBL As New daBienLai
                daBL.ChuyenTraBL(strSoBL, strSoBT)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function HuyBL(ByVal mstrSoBL As String, ByVal mstrsoBT As String) As Boolean
            Dim daBL As New daBienLai
            Return daBL.HuyBL(mstrSoBL, mstrsoBT)
        End Function

        Public Shared Function SelectBL(ByVal mstrSoBL As String, ByVal mstrsoBT As String) As DataSet
            Dim daBL As New daBienLai
            Return daBL.SelectBL(mstrSoBL, mstrsoBT)
        End Function

        Public Shared Function GetBL(ByVal strNgayKB As String, ByVal strMaNV As String, ByVal strSoBT As String) As DataSet
            Dim daBL As New daBienLai
            Return daBL.GetBL(strNgayKB, strMaNV, strSoBT)
        End Function

        Public Shared Function ValidSoBL(ByVal strKHBL As String, ByVal strSoBL As String) As Boolean
            Dim daBL As New daBienLai
            Try
                Return daBL.SelectExistBL(strKHBL, strSoBL)
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function BLT_MLNS(ByVal fstrBL_ID As String) As DataSet
            Dim objda As New daBienLai
            Return objda.TCS_dsMLNS(fstrBL_ID)
        End Function

        Public Shared Function GetMLNS(ByVal key As KeyCTu) As DataSet
            Try
                Dim daBL As New daBienLai
                Return daBL.GetMLNS(key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace