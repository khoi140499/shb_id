﻿Imports VBOracleLib
Imports Business.Common
Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Imports Business.NewChungTu

Namespace ChungTu
    Public Class daBienLai
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
        Public Function Update(ByVal strKHBL As String, ByVal strSoBL As String) As Boolean
            Dim connBL As DataAccess
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                connBL = New DataAccess
                strSql = "Update TCS_CTBL Set Lan_in = 1 " & _
                         "Where KyHieu_BL='" & strKHBL & "' And So_BL='" & strSoBL & "' And Trang_Thai<>'03'"
                connBL.ExecuteNonQuery(strSql, CommandType.Text)
                blnResult = True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật lần in cho biên lai")
                blnResult = False
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình cập nhật lần in cho biên lai: " & ex.ToString, EventLogEntryType.Error)

            Finally
                If Not connBL Is Nothing Then connBL.Dispose()
            End Try
            Return blnResult
        End Function

        Public Function Insert(ByVal objBL As infBienLai)
            Dim connBL As DataAccess
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                connBL = New DataAccess
                ' Insert vào bảng HDR
                'Bỏ insert so_BL
                strSql = "Insert Into TCS_CTBL(So_BL,SHKB,Ngay_KB,Ma_NV," & _
                       " So_BT,Ma_DThu,So_BThu," & _
                       " KyHieu_BL,Ma_NNTien," & _
                       " Ten_NNTien,DC_NNTien,Ly_Do," & _
                       " Ma_KS,Ma_TQ,So_QD,Ngay_QD," & _
                       " Ma_CQQD,Ngay_CT,Ngay_HT," & _
                       " TK_Co,LH_ID,TTien,TT_TThu,Lan_In,Trang_Thai,Ma_LH, Ma_Huyen, Ma_Tinh) Values(" & _
                       Globals.EscapeQuote(objBL.So_BL) + ",'" & _
                       objBL.SHKB & "'," & objBL.Ngay_KB & "," & objBL.Ma_NV & "," & _
                       objBL.So_BT & ",'" & objBL.Ma_DThu & "'," & objBL.So_BThu & ",'" & _
                       objBL.KyHieu_BL & "','" & objBL.Ma_NNTien & "','" & _
                       objBL.Ten_NNTien & "','" & objBL.DC_NNTien & "','" & objBL.Ly_Do & "'," & _
                       objBL.Ma_KS & "," & objBL.Ma_TQ & ",'" & objBL.So_QD & "'," & objBL.Ngay_QD & ",'" & _
                       objBL.CQ_QD & "'," & objBL.Ngay_CT & "," & objBL.Ngay_HT & ",'" & _
                       objBL.TK_Co & "'," & objBL.ID_LH & "," & objBL.TTien & "," & objBL.TT_TThu & ",'" & _
                       objBL.Lan_In & "','" & objBL.Trang_Thai & "','" & objBL.Ma_LH & "','" & objBL.Ma_Huyen & "', '" & objBL.Ma_Tinh & "')"
                connBL.ExecuteNonQuery(strSql, CommandType.Text)
                blnResult = True
            Catch ex As Exception
                blnResult = False
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình thêm mới dữ liệu biên lai thu")
                'LogDebug.WriteLog("Lỗi trong quá trình thêm mới dữ liệu biên lai thu: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try
            Return blnResult
        End Function

        Public Function UpdateBLT(ByVal objBL As infBienLai)
            Dim connBL As DataAccess
            Dim strSql As String
            Dim blnResult As Boolean
            Try
                connBL = New DataAccess
                ' Insert vào bảng HDR
                strSql = "Update TCS_CTBL Set " & _
                        "Ma_NNTien ='" & objBL.Ma_NNTien & "'," & _
                        "Ten_NNTien='" & objBL.Ten_NNTien & "'," & _
                        "DC_NNTien='" & objBL.DC_NNTien & "'," & _
                        "Ly_Do='" & objBL.Ly_Do & "'," & _
                        "So_QD='" & objBL.So_QD & "'," & _
                        "Ngay_QD= " & objBL.Ngay_QD & "," & _
                        "Ma_CQQD ='" & objBL.CQ_QD & "'," & _
                        "TK_Co ='" & objBL.TK_Co & "'," & _
                        "LH_ID =" & objBL.ID_LH & "," & _
                        "Ma_LH = '" & objBL.Ma_LH & "', " & _
                        "Ma_Huyen ='" & objBL.Ma_Huyen & "'," & _
                        "Ma_Tinh = '" & objBL.Ma_Tinh & "', " & _
                        "Trang_Thai = '" & objBL.Trang_Thai & "' " & _
                        "Where (so_bl = '" & objBL.So_BL & "') And " & _
                        " (so_bt = " & objBL.So_BT & ")"
                connBL.ExecuteNonQuery(strSql, CommandType.Text)
                blnResult = True
            Catch ex As Exception
                blnResult = False
                log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi trong quá trình cập nhật dữ liệu biên lai thu")
                'LogDebug.WriteLog("Lỗi trong quá trình cập nhật dữ liệu biên lai thu: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try
            Return blnResult
        End Function
        Public Sub KhoiPhucBL(ByVal strSoBL As String, ByVal strSoBT As String)
            Dim connBL As DataAccess
            Dim strSQL As String
            Try
                connBL = New DataAccess
                strSQL = "Update TCS_CTBL " & _
                        "set trang_thai='00' " & _
                        "Where (so_bl = '" & strSoBL & "') " & _
                        " And (so_bt = " & strSoBT & ") "

                connBL.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try

        End Sub
        Public Sub ChuyenTraBL(ByVal strSoBL As String, ByVal strSoBT As String)
            Dim connBL As DataAccess
            Dim strSQL As String
            Try
                connBL = New DataAccess
                strSQL = "Update TCS_CTBL " & _
                        "set trang_thai='04' " & _
                        "Where (so_bl = '" & strSoBL & "') " & _
                        " And (so_bt = " & strSoBT & ") "

                connBL.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '   CTuCommon.WriteLog(ex, "Lỗi trong quá trình chuyển trả dữ liệu biên lai thu")
                'LogDebug.WriteLog("Lỗi trong quá trình chuyển trả dữ liệu biên lai thu: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try

        End Sub
        Public Sub ChuyenKS(ByVal strSoBL As String, ByVal strSoBT As String)
            Dim connBL As DataAccess
            Dim strSQL As String
            Try
                connBL = New DataAccess
                strSQL = "Update TCS_CTBL " & _
                        "set trang_thai='05' " & _
                        "Where (so_bl = '" & strSoBL & "') " & _
                        " And (so_bt = " & strSoBT & ") "

                connBL.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try

        End Sub
        Public Sub KiemSoat(ByVal strSoBL As String, ByVal strSoBT As String)
            Dim connBL As DataAccess
            Dim strSQL As String
            Try
                connBL = New DataAccess
                strSQL = "Update TCS_CTBL " & _
                        "set trang_thai='01' " & _
                        "Where (so_bl = '" & strSoBL & "') " & _
                        " And (so_bt = " & strSoBT & ") "

                connBL.ExecuteNonQuery(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình kiểm soát dữ liệu biên lai thu")
                'LogDebug.WriteLog("Lỗi trong quá trình kiểm soát dữ liệu biên lai thu: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try

        End Sub
        Public Function HuyBL(ByVal mstrSoBL As String, ByVal mstrsoBT As String) As Boolean
            '---------------------------------------------------------------------------
            ' Mục đích: Hủy biên lai người sử dụng chọn
            ' Đầu vào: KHBL và số biên lai
            ' Người viết: Nguyễn Hữu Hoan
            ' Ngày viết: 06/11/2007

            ' Người Sửa: Hoàng Văn Anh
            ' Mục đích: Sửa theo COA của ngân hàng
            '----------------------------------------------------------------------------
            'Dim connBL As DataAccess
            Dim strSql As String
            Dim blnResult As Boolean

            'Dim transBL As IDbTransaction
            Dim intLanIn As Integer = 0
            Try

                'Dim strTrangThaiBL As String = Get_TrangThai_BL(gdtmNgayLV, CInt(strMaNV), CInt(strSoBT))
                'Select Case strTrangThaiBL.Trim()
                '    Case "02"
                '        MsgBox("Biên Lai này đã được lập thành GNT vì vậy không thể xóa được!", MsgBoxStyle.Critical, "Thông Báo")
                '        Return True
                '    Case "03"
                '        MsgBox("Biên Lai này đã được hủy trước đó rồi!", MsgBoxStyle.Critical, "Thông Báo")
                '        Return True
                '    Case ""
                '        MsgBox("Bạn không có quyền hủy Biên Lai này, vì ngày lập Biên Lai này khác ngày làm việc của hệ thống!", MsgBoxStyle.Critical, "Thông Báo")
                '        Return True
                'End Select

                ' connBL = New DataAccess
                'transBL = DataAccess.BeginTransaction
                '' Xóa trong bảng kho quỹ DTL
                'strSql = "Delete From TCS_KHOQUY_DTL " & _
                '         "Where (so_ct = " & mstrSoBL & ") " & _
                '         "And (so_bt = " & mstrsoBT & ") "
                'DataAccess.ExecuteNonQuery(strSql, CommandType.Text, transBL)
                '' Xóa trong bảng HDR
                'strSql = "Delete From TCS_KHOQUY_HDR " & _
                '        "Where (so_ct = " & mstrSoBL & ") " & _
                '         "And (so_bt = " & mstrsoBT & ") "
                'DataAccess.ExecuteNonQuery(strSql, CommandType.Text, transBL)

                ' hủy biên lai
                strSql = "Update TCS_CTBL Set Trang_Thai = '03', Lan_In = 0 " & _
                            "Where (so_bl = " & mstrSoBL & ") " & _
                         "And (so_bt = " & mstrsoBT & ") "
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
                ' transBL.Commit()
                blnResult = True
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình hủy lai thu")
                blnResult = False
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình hủy biên lai thu: " & ex.ToString, EventLogEntryType.Error)
                ' transBL.Rollback()

            Finally
                'If Not transBL Is Nothing Then
                '    transBL.Dispose()
                'End If
                'If Not connBL Is Nothing Then
                '    connBL.Dispose()
                'End If
            End Try
            Return blnResult
        End Function
        Public Function SelectBL(ByVal lngNgayLV As Long, ByVal iMaNV As Integer, ByVal iAll As Boolean) As DataSet
            ''------------------------------------------------
            '' Mục đích: Select toàn bộ các biên lai lập
            ''           trong ngày của nhân viên đó
            '' Người viết: Nguyễn Hữu Hoan
            '' Ngày viết: 05/11/2007
            '' 
            '' Người sửa: Hoàng Văn Anh
            '' Ngày 26/12/2008
            '' Mục đích: sửa theo COA
            ''------------------------------------------------
            'Dim connBL As DataAccess
            'Dim strSql As String
            'Dim dsResult As DataSet
            'Try
            '    connBL = New DataAccess

            '    strSql = "Select a.shkb,b.Ten TenKB,a.KyHieu_BL,a.So_BL,a.So_BT,a.Trang_thai," & _
            '             "a.Ma_NV,a.Lan_in,a.So_CT,a.ttien, a.ngay_kb, a.Ma_Tinh, a.Ma_Huyen " & _
            '             "From TCS_CTBL a,TCS_DM_KHOBAC b Where a.SHKB=b.SHKB and a.ma_dthu='" & gstrMaDiemThu & "' And a.Ngay_kb = " & lngNgayLV & " AND a.MA_NV=" & iMaNV & _
            '             " Order by a.so_bt desc"
            '    dsResult = connBL.ExecuteReturnDataSet(strSql, CommandType.Text)
            'Catch ex As Exception
            '    'LogDebug.Writelog("Lỗi trong quá trình lấy dữ liệu biên lai: " & ex.ToString)
            '    Throw ex
            'Finally
            '    If Not connBL Is Nothing Then
            '        connBL.Dispose()
            '    End If
            'End Try
            'Return dsResult
        End Function
        Public Function SelectKeyBL(ByVal strKHBL As String, ByVal strSoBL As String) As DataSet
            Dim connBL As DataAccess
            Dim strSql As String
            Dim dsResult As DataSet
            Try
                connBL = New DataAccess
                strSql = "SELECT shkb,ngay_kb,ma_nv,so_bt,ma_dthu " & _
                         "FROM tcs_ctbl " & _
                         "WHERE (kyhieu_bl = '" & strKHBL & "') And (so_bl = '" & strSoBL & "') " & _
                         "AND Trang_Thai <> '03'"
                dsResult = connBL.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy key dữ liệu biên lai")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy key dữ liệu biên lai: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try
            Return dsResult
        End Function
        Public Function SelectExistBL(ByVal strKHBL As String, ByVal strSoBL As String) As Boolean
            Dim connBL As DataAccess
            Dim strSql As String
            Dim dsResult As DataSet
            Try
                connBL = New DataAccess
                strSql = "SELECT 1 FROM tcs_ctbl " & _
                         "WHERE (kyhieu_bl = '" & strKHBL & "') And (so_bl = '" & strSoBL & "') "
                dsResult = connBL.ExecuteReturnDataSet(strSql, CommandType.Text)
                If IsEmptyDataSet(dsResult) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy dữ liệu biên lai thu")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu biên lai: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try
        End Function
        Public Function GetBL(ByVal strNgayKB As String, ByVal strMaNV As String, ByVal strSoBT As String) As DataSet
            Dim connBL As DataAccess
            Dim strSQL As String
            Dim dsResult As DataSet
            Try
                connBL = New DataAccess
                strSQL = "SELECT BL.kyhieu_bl, BL.so_bl, BL.ma_nntien, BL.ten_nntien, BL.dc_nntien," & _
                           "BL.ly_do,BL.so_qd, to_char(BL.ngay_qd, 'dd/MM/yyyy') as ngay_qd, BL.ma_cqqd,CQ.ten_cqqd," & _
                           "BL.ngay_ct, BL.tk_co, BL.ttien,LH.ma_lh,LH.ten_lh,BL.Lan_In,BL.kyhieu_ct,BL.so_ct, " & _
                           "BL.Ma_Tinh, BL.Ma_Huyen " & _
                           "FROM tcs_ctbl BL,tcs_dm_cqqd CQ,tcs_dm_taikhoan T,tcs_dm_lhthu LH " & _
                           "WHERE (BL.ngay_kb = " & strNgayKB & ") And (BL.ma_nv = " & strMaNV & ") And (BL.so_bt = " & strSoBT & ") " & _
                           "AND (BL.ma_cqqd=CQ.ma_cqqd) " & _
                           "AND (BL.ma_lh = LH.ma_lh)"

                dsResult = connBL.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy dữ liệu biên lai thu")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu biên lai: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try
            Return dsResult
        End Function

        Public Function SelectBL(ByVal strSoBL As String, ByVal strSoBT As String) As DataSet
            Dim connBL As DataAccess
            Dim strSQL As String
            Dim dsResult As DataSet
            Try
                connBL = New DataAccess
                strSQL = "SELECT bl.shkb,kb.ten tenkb,BL.ma_nv,BL.kyhieu_bl, BL.so_bl, BL.ma_nntien, BL.ten_nntien, BL.dc_nntien," & _
                         "BL.ly_do,BL.so_qd, BL.ngay_qd, BL.ma_cqqd,CQ.ten_cqqd," & _
                         "BL.ngay_ct, BL.tk_co, BL.ttien,LH.ma_lh,LH.ten_lh,BL.Lan_In,BL.kyhieu_ct,BL.so_ct, " & _
                         "BL.Ma_Tinh, BL.Ma_Huyen " & _
                         "FROM tcs_ctbl BL,tcs_dm_cqqd CQ,tcs_dm_lhthu LH,tcs_dm_khobac kb " & _
                         "WHERE (bl.shkb=kb.shkb) And (BL.so_bl = '" & strSoBL & "') And (BL.so_bt = " & strSoBT & ") " & _
                         "AND (BL.ma_cqqd=CQ.ma_cqqd)" & _
                         "AND (BL.ma_lh = LH.ma_lh)"

                dsResult = connBL.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy dữ liệu biên lai thu")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy dữ liệu biên lai: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not connBL Is Nothing Then
                    connBL.Dispose()
                End If
            End Try
            Return dsResult
        End Function
        Public Function SelectLHT() As DataSet
            Dim conn As DataAccess
            Dim strSql As String
            Dim ds As DataSet
            Try
                conn = New DataAccess
                strSql = "Select MA_LH,TEN_LH From TCS_DM_LHTHU"
                ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy loại hình thu")
                'LogDebug.WriteLog("Lỗi trong quá trình lấy loại hình thu: " & ex.ToString, EventLogEntryType.Error)
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return ds
        End Function
        Public Function SelectLHT(ByVal strMaLH) As DataSet
            Dim conn As DataAccess
            Dim strSql As String
            Dim ds As DataSet
            Try
                conn = New DataAccess
                strSql = "Select lh.TEN_LH, mlns.TK From TCS_DM_LHTHU lh," & _
                        "TCS_DM_LHTHU_MLNS mlns WHERE (lh.MA_LH ='" & strMaLH & "') and (lh.ma_lh = mlns.ma_lh(+))"

                ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi trong quá trình lấy loại hình thu")
                Throw ex
                'LogDebug.WriteLog("Lỗi trong quá trình lấy loại hình thu: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
            Return ds
        End Function
        'Public Function BLT_Get_LanIn(ByVal strKYHIEU_BL As String, ByVal strSO_BL As String) As String
        '    Dim cnBL As DataAccess
        '    Dim drBL As IDataReader
        '    Try
        '        cnBL = New DataAccess
        '        Dim strSQL As String
        '        strSQL = "select LAN_IN from tcs_ctbl " & _
        '            "where (kyhieu_bl = '" & strKYHIEU_BL & "') and (so_bl = '" & strSO_BL & "')"

        '        drBL = cnBL.ExecuteDataReader(strSQL, CommandType.Text)
        '        Dim strLanIn As String = ""
        '        If (drBL.Read()) Then
        '            strLanIn = drBL(0).ToString()
        '        End If
        '        Return strLanIn
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        If (Not IsNothing(cnBL)) Then cnBL.Dispose()
        '        If (Not IsNothing(drBL)) Then
        '            drBL.Close()
        '            drBL.Dispose()
        '        End If
        '    End Try
        'End Function
        Public Function TCS_dsMLNS(ByVal strLH_ID As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy DS MLNS
            ' Tham số       : 
            ' Giá trị trả về: 
            ' Ngày viết     : 02/04/2008
            ' Người viết    : Ngô Minh Trong
            ' ----------------------------------------------------------------
            Dim conn As DataAccess
            Dim ds As DataSet
            Dim strSql As String
            Try
                conn = New DataAccess
                strSql = "SELECT a.ma_cap,a.ma_chuong, a.ma_loai, a.ma_khoan, " & _
                    "a.ma_muc, a.ma_tmuc " & _
                    "FROM tcs_dm_lhthu_mlns a where a.ma_lh='" & strLH_ID & "'"
                ds = conn.ExecuteReturnDataSet(strSql, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy danh sách mục lục ngân sách loại hình thu")
                Throw ex
                'LogDebug.WriteLog("Lỗi khi lấy danh sách mục lục ngân sách loại hình thu: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not ds Is Nothing Then
                    ds.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
        Public Function GetMLNS(ByVal key As KeyCTu) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích      : Lấy DS MLNS        
            ' Ngày viết     : 14/11/2008
            ' Người viết    : Lê Hồng Hà
            ' ----------------------------------------------------------------
            Dim conn As DataAccess
            Dim ds As DataSet
            Dim strSQL As String
            Try
                conn = New DataAccess

                strSQL = "select tm.ten as NoiDung, " & _
                                "(mlns.ma_cap || mlns.ma_chuong) as CC, " & _
                                "(mlns.ma_loai || mlns.ma_khoan) as LK, " & _
                                "(mlns.ma_muc || mlns.ma_tmuc) as MTM, " & _
                                "to_char(sysdate, 'MM/yyyy') as KyThue, " & _
                                "bl.ttien as Tien " & _
                                "from tcs_ctbl bl, tcs_dm_lhthu_mlns mlns, tcs_dm_muc_tmuc tm " & _
                                "where (bl.ma_lh  = mlns.ma_lh(+)) " & _
                                "And (bl.lh_id=mlns.id or bl.lh_id is null) " & _
                                "AND ((mlns.ma_muc is null) or ((tm.ma_muc || tm.ma_tmuc) = (mlns.ma_muc || mlns.ma_tmuc))) " & _
                                "and (bl.ngay_kb = " & key.Ngay_KB & " )  " & _
                                "and (bl.ma_nv = " & key.Ma_NV & " ) " & _
                                "and (bl.so_bt = " & key.So_BT & " ) "


                ds = conn.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Return ds
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) ' CTuCommon.WriteLog(ex, "Lỗi khi lấy danh sách mục lục ngân sách loại hình thu")
                Throw ex
                'LogDebug.WriteLog("Lỗi khi lấy danh sách mục lục ngân sách loại hình thu: " & ex.ToString, EventLogEntryType.Error)
            Finally
                If Not ds Is Nothing Then
                    ds.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If
            End Try
        End Function
    End Class
End Namespace