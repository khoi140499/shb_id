﻿Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Imports Business.NewChungTu

Namespace Common


    Public Class buChungTuTD
        Public Shared Function CapNhatPKT(ByVal strSoPKT As String, ByVal Update As Boolean) As Boolean
            Dim daPKT As New daChungTuTD
            Return daPKT.CapNhatPKT(strSoPKT, Update)
        End Function
        Public Shared Function InsertPKTTemp(ByVal strWhere As String, ByVal Update As Boolean) As Boolean
            Dim daPKT As New daChungTuTD
            Return daPKT.InsertPKTTemp(strWhere, Update)
        End Function
        Public Shared Function LoadDSCT(ByVal strSoPKT As String, ByVal lngNgayKB As Long, ByVal strMaDT As String) As DataSet
            Dim daCT As New daChungTuTD
            Return daCT.LoadDSCT(strSoPKT, lngNgayKB, strMaDT)
        End Function
        Public Shared Function LoadDSCT(ByVal key As KeyCTu, _
                                Optional ByVal blnTHOP As Boolean = False, _
                                Optional ByVal blnTrangThaiAll As Boolean = False, _
                                Optional ByVal strTrangThaiChiTiet As String = "00") As DataSet
            Dim daCT As New daChungTuTD
            Return daCT.LoadDSCT(key, blnTHOP, blnTrangThaiAll, strTrangThaiChiTiet)
        End Function

        Public Shared Sub initBanThu(ByRef chkBanThu As Windows.Forms.CheckedListBox)
            '-----------------------------------------------------------------
            ' Mục đích: Khởi tạo danh sách bàn thu.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 12/11/2007
            ' Người viết: Ngô Minh Trọng
            '-----------------------------------------------------------------
            Dim i As Integer

            chkBanThu.Items.Clear()
            For i = 1 To gbytSoBanLV
                chkBanThu.Items.Add("Bàn " & i.ToString, True)
            Next
        End Sub

        Public Shared Function LoadDataHeaderKXGNT(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ.
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objChungTu As New daChungTuTD
            Try
                Return objChungTu.LoadDataHeaderKXGNT(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function LoadBKKXCTNVP(ByVal strWhere As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách bảng kê chứng từ.
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objChungTu As New daChungTuTD
            Try
                Return objChungTu.LoadBKKXCTNVP(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function LoadDataHeaderCQT(ByVal strWhere As String, ByVal strWhereNgay As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ.
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objChungTu As New daChungTuTD
            Try
                Return objChungTu.LoadDataHeaderCQT(strWhere, strWhereNgay)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function LoadDataHeaderNhanCTNVP(ByVal strTSNCode As String, ByVal strPkgID As String, Optional ByVal strWhereNgay As String = "") As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ.
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objChungTu As New daChungTuTD
            Try
                Return objChungTu.LoadDataHeaderNhanCTNVP(strTSNCode, strPkgID, strWhereNgay)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function LoadDataDetailNhanCTNVP(ByVal strKYHIEU_CT As String, ByVal strSO_CT As String, ByVal strTranNo As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ.
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objChungTu As New daChungTuTD
            Try
                Return objChungTu.LoadDataDetailNhanCTNVP(strKYHIEU_CT, strSO_CT, strTranNo)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function LoadCTKBHDR(ByVal strWhere As String, ByVal intTT As Integer) As DataSet
            Dim objCTKB As New daChungTuTD
            Try
                Return objCTKB.LoadCTKBHDR(strWhere, intTT)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function LoadCTKBCTHDR(ByVal strWhere As String) As DataSet
            Dim objCTKB As New daChungTuTD
            Try
                Return objCTKB.LoadCTKBCTHDR(strWhere)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function LoadDataDetail(ByVal Key As infChungTuHDR) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ.
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objChungTu As New daChungTuTD
            Try
                Return objChungTu.LoadDataDetail(Key)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Public Shared Function HachToanKTKB(ByVal strWhere As String, _
        '                                     ByVal strSHKB As String, ByVal MaNV As Int64, _
        '                                     ByVal NgayKTKB As Int64, ByRef ds As DataSet) As String
        '    Try
        '        Dim objCT As New daChungTuTD
        '        Return objCT.HachToanKTKB(strWhere, strSHKB, MaNV, NgayKTKB, ds)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        Public Shared Function LoadDataDetailCQT(ByVal strSHKB As String, ByVal strNgayKB As String, _
                             ByVal strMaNV As String, ByVal strSoBT As String, ByVal strMaDThu As String, _
                                    ByVal strMaNNT As String, ByVal strTKNo As String, ByVal strTKCo As String, _
                                     ByVal strMaXa As String) As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ.
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objChungTu As New daChungTuTD
            Try
                Return objChungTu.LoadDataDetailCQT(strSHKB, strNgayKB, strMaNV, strSoBT, strMaDThu, strMaNNT, strTKNo, strTKCo, strMaXa)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function LoadTK(Optional ByVal strType As String = "") As DataSet
            '-----------------------------------------------------------------
            ' Mục đích: Lấy danh sách header của chứng từ.
            ' Tham số:
            ' Giá trị trả về:
            ' Ngày viết: 02/11/2007
            ' Người viết: Ngô Minh Trọng
            ' ----------------------------------------------------------------
            Dim objTaiKhoan As New daChungTuTD
            Dim dsTmp As DataSet
            Try
                dsTmp = objTaiKhoan.LoadTK(strType)
            Catch ex As Exception
                'LogDebug.Writelog("Có lỗi xảy ra khi thông tin tài khoản để kễt xuất chứng từ: " & ex.ToString)
            End Try
            Return dsTmp
        End Function
        'Public Shared Function HuyCTNVP(ByVal lngNgay As Long, ByVal intMaNV As Integer, _
        '                        ByVal intSo_Phieu_KT As Integer, ByVal intSBT_KTKB As Integer, _
        '                        ByVal intMaKS As Integer, ByRef strError As String) As Boolean
        '    Try
        '        If intSBT_KTKB > 0 Then
        '            Dim daCT As New daChungTuTD
        '            daCT.HuyKTKB_NgoaiVP(lngNgay, intMaNV, intSo_Phieu_KT, intSBT_KTKB, intMaKS, strError)
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        Public Shared Function CTU_HDR_SOBT_KTKB(ByVal lngSoPKT As Long, ByVal lngNgay As Long) As String
            '-----------------------------------------------------
            ' Mục đích: Lấy thông tin số bút toán KTKB của chứng từ 
            ' Ngày viết: 21/09/2008
            ' Người viết: Lê Hồng Hà
            ' ----------------------------------------------------  
            Try
                Dim daCT As New daChungTuTD
                Return daCT.CTU_HDR_SOBT_KTKB(lngSoPKT, lngNgay)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


#Region "Sua chứng từ ngoài VP"
        Public Shared Function LoadDSDThu() As DataSet
            Dim daCT As New daChungTuTD
            Return daCT.LoadDSDThu()
        End Function
        Public Shared Sub initDiemThu(ByRef chkBanThu As Windows.Forms.CheckedListBox)
            '-----------------------------------------------------------------
            ' Mục đích: Khởi tạo danh sách bàn thu.
            ' Tham số: 
            ' Giá trị trả về:
            ' Ngày viết: 10/12/2007
            ' Người viết: Nguyễn Hữu Hoan
            '-----------------------------------------------------------------
            Dim i As Integer
            Dim ds As DataSet
            Dim strValue As String
            Try
                ds = LoadDSDThu()
                If IsEmptyDataSet(ds) Then
                    Exit Sub
                End If
                chkBanThu.Items.Clear()
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    strValue = ds.Tables(0).Rows(i).Item(1)
                    chkBanThu.Items.Add(strValue.PadRight(100, " ") & ds.Tables(0).Rows(i).Item(0), True)
                Next
            Catch ex As Exception
                MsgBox("Lỗi trong quá trình lấy danh sách điểm thu.")
            End Try
        End Sub

        Public Shared Sub CTU_DTL_UpdateNVP(ByVal dtl As infChungTuDTL)
            Try
                Dim daCT As New daChungTuTD
                daCT.CTU_DTL_UpdateNVP(dtl)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function Update(ByVal objCTuHDR As infChungTuHDR, ByVal objCTuDTL As infChungTuDTL(), Optional ByVal strSoPKT As String = "") As Boolean
            '--------------------------------------------------------------------------------------
            ' Ghi vao bảng TCS_CTU_HDR & TCS_CTU_DTL
            ' 
            '--------------------------------------------------------------------------------------
            Dim daCT As New daChungTuTD
            'Cập nhật thông tin chứng từ
            daCT.Update(objCTuHDR, objCTuDTL, strSoPKT)
            Return True
        End Function
#End Region

    End Class
End Namespace