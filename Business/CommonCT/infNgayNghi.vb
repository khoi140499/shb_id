﻿Namespace NgayNghi
    Public Class infNgayNghi
        Private v_errorCode As String
        Private v_errorDesc As String
        Private r_listHolidayDt As listHolidayDt

        Public Property errorCode As String
            Set(ByVal value As String)
                Me.v_errorCode = value
            End Set
            Get
                Return Me.v_errorCode
            End Get
        End Property

        Public Property errorDesc As String
            Set(ByVal value As String)
                Me.v_errorDesc = value
            End Set
            Get
                Return Me.v_errorDesc
            End Get
        End Property

        Public Property listHolidayDt As listHolidayDt
            Set(ByVal value As listHolidayDt)
                Me.r_listHolidayDt = value
            End Set
            Get
                Return Me.r_listHolidayDt
            End Get
        End Property
    End Class

    Public Class listHolidayDt
        Private r_element As List(Of element)
        Public Property element As List(Of element)
            Set(ByVal value As List(Of element))
                Me.r_element = value
            End Set
            Get
                Return Me.r_element
            End Get
        End Property
    End Class
    Public Class element
        Private v_holiDesc As String
        Private v_holiDt As String

        Public Property holiDesc As String
            Set(ByVal value As String)
                Me.v_holiDesc = value
            End Set
            Get
                Return Me.v_holiDesc
            End Get
        End Property

        Public Property holiDt As String
            Set(ByVal value As String)
                Me.v_holiDesc = value
            End Set
            Get
                Return Me.v_holiDesc
            End Get
        End Property

    End Class


End Namespace

