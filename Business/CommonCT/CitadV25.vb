﻿Imports System.Text
Imports System.Security.Cryptography
Imports VBOracleLib
Imports System.Diagnostics
Imports System.Data
Imports System.IO
Imports System.Collections.Generic

Public Class CitadV25
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public FileName As String = String.Empty
    Public YYYMMDD As String = String.Empty
    Public ma_nh_chuyen As String = ""
    Public trandate As String = DateTime.Now.ToString("yyyyMMdd")
    Public REC_TYPE As String = "DD"
    Public TRX_TYPE As String = "201101"
    Public SD_TIME As String = padRightSpace(14)
    Public SERIAL_NO As String = padRightSpace(8)
    Public RELATION_NO As String = ""
    Public RESPONSE_CODE As String = padRightSpace(4)
    Public O_CI_CODE As String = ""
    Public R_CI_CODE As String = ""
    Public O_INDIRECT_CODE As String = ""
    Public R_INDIRECT_CODE As String = ""
    Public FEE_CI_CODE As String = ""

    Public TRX_DATE As String = DateTime.Now.ToString("yyyyMMdd")
    Public CURRENCY As String = ""
    Public EXCHANGE_RATE = padLeftSpace("1,00", 12)
    Public AMOUNT As String = ""
    Public SD_CODE As String = padRightSpace(20)
    Public SD_NAME As String = ""  'ten nguoi nop thue
    Public SD_ADDR As String = "" 'ten kho bac
    Public SD_ACCNT As String = "" 'TK_KH_NH
    Public SD_ID_NO As String = padRightSpace(20)
    Public SD_ISSUE_DATE As String = padRightSpace(8)
    Public SD_ISSUER As String = padRightSpace(25)

    Public RV_CODE As String = "" 'ma co quan thu
    Public RV_NAME As String = "" 'ten CQTHU
    Public RV_ADDR As String = "" 'ten kho bac
    Public RV_ACCNT As String = ""
    Public RV_ID_NO As String = "" 'tai khoan co kho bac nha nuoc
    Public RV_ISSUE_DATE As String = padRightSpace(8)
    Public RV_ISSUER As String = padRightSpace(25)
    Public CONTENT As String = "" 'remark
    Public AUTHORIZED As String = padRightSpace(1)
    Public FEE_FLAG As String = padRightSpace(1)
    Public REFERENCE As String = padRightSpace(100)
    Public EX_E_SIGN As String = padRightSpace(400)
    Public CREATE_TIME As String = padRightSpace(14)
    Public APPR_ID As String = padRightSpace(16)
    Public E_SIGN As String = padRightSpace(400)
    Public OPTION1 As String = padRightSpace(20)
    Public OPTION2 As String = padRightSpace(30)
    Public OPTION3 As String = padRightSpace(40)
    Public SPACE As String = padRightSpace(50)
    Public MAC As String = padRightSpace(44)
    Public CONTENT_EX As String = ""
    Public TAX_CODE As String = ""
    Public MACN As String = ""
    Public sSoCtuNH As String = ""
    Public sSO_CT As String = ""
    Public sRefSoCtuNH As String = ""
    Sub CitadV25()
        'FileName = "GTWOCS" + GetDateBy("yyyyMMdd") & "." & AutoGenHHmmss()
        FileName = "GTWOCS" + trandate & "." & AutoGenHHmmss()

        YYYMMDD = GetDateBy()
    End Sub

    Public Function BuildCitad() As String
        Dim v_return As String = String.Empty
        Try

        Catch ex As Exception

        End Try
        Return v_return
    End Function

    Public Function BuildCitadFileName() As String
        Dim v_return As String = String.Empty
        Try
            'FileName = "GTWOCS" + GetDateBy("yyyyMMdd") & "." & AutoGenHHmmss()
            FileName = "GTWOCS" + trandate & "." & AutoGenHHmmss()
            Return FileName
        Catch ex As Exception

        End Try
        Return v_return
    End Function

    Public Function BuildCitadHeader() As String
        Dim v_return As String = String.Empty
        Try
            Dim v_regType As String = "HH"
            Dim v_ciCode As String = padRightSpace(12, ma_nh_chuyen)
            Dim v_fileName As String = padRightSpace(25, FileName)
            'Dim v_tranDate As String = YYYMMDD
            Dim v_tranDate As String = trandate 'GetDateBy()
            Dim v_dataCnt As String = padLeftString(8, "1")

            v_return = v_regType & v_ciCode & v_fileName & v_tranDate & v_dataCnt
        Catch ex As Exception

        End Try
        Return v_return
    End Function

    Public Function BuildCitadBody() As String
        Dim v_return As String = String.Empty
        Try
            'Hang so mac dinh
            REC_TYPE = padRightSpace(REC_TYPE, 2)
            TRX_TYPE = padRightSpace(TRX_TYPE, 6)
            SD_TIME = padRightSpace(SD_TIME, 14)
            SERIAL_NO = padRightSpace(RELATION_NO, 8)
            RELATION_NO = padRightSpace(RELATION_NO, 40)
            RESPONSE_CODE = padRightSpace(RESPONSE_CODE, 4)
            O_CI_CODE = padRightSpace(O_CI_CODE, 12)
            R_CI_CODE = padRightSpace(R_CI_CODE, 12)
            O_INDIRECT_CODE = padRightSpace(O_INDIRECT_CODE, 12)
            R_INDIRECT_CODE = padRightSpace(R_INDIRECT_CODE, 12)
            FEE_CI_CODE = padRightSpace(FEE_CI_CODE, 12)
            TRX_DATE = padRightSpace(TRX_DATE, 8)
            CURRENCY = padRightSpace(CURRENCY.ToUpper(), 3)
            EXCHANGE_RATE = padLeftString(EXCHANGE_RATE, 12)
            AMOUNT = padLeftString(AMOUNT, 22)
            SD_CODE = padRightSpace(SD_CODE, 20)
            SD_NAME = padRightSpace(SD_NAME, 150)
            SD_ADDR = padRightSpace(SD_ADDR, 100)
            SD_ACCNT = padRightSpace(SD_ACCNT, 25)
            SD_ID_NO = padRightSpace(SD_ID_NO, 20)
            SD_ISSUE_DATE = padRightSpace(SD_ISSUE_DATE, 8)
            SD_ISSUER = padRightSpace(SD_ISSUER, 25)
            RV_CODE = padRightSpace(RV_CODE, 20)
            RV_NAME = padRightSpace(RV_NAME, 150)
            RV_ADDR = padRightSpace(RV_ADDR, 100)
            RV_ACCNT = padRightSpace(RV_ACCNT, 25)
            RV_ID_NO = padRightSpace(RV_ID_NO, 20)
            RV_ISSUE_DATE = padRightSpace(RV_ISSUE_DATE, 8)
            RV_ISSUER = padRightSpace(RV_ISSUER, 25)
            If CONTENT.Length <= 197 Then
                CONTENT = CONTENT & "[" & sSO_CT & "]"
            End If
            CONTENT = padRightSpace(CONTENT, 210)
            AUTHORIZED = padRightSpace(AUTHORIZED, 1)
            FEE_FLAG = padRightSpace(FEE_FLAG, 1)
            REFERENCE = padRightSpace(REFERENCE, 100)
            EX_E_SIGN = padRightSpace(EX_E_SIGN, 400)
            CREATE_TIME = padRightSpace(CREATE_TIME, 14)
            APPR_ID = padRightSpace(APPR_ID, 16)
            E_SIGN = padRightSpace(E_SIGN, 400)
            OPTION1 = padRightSpace(OPTION1, 20)
            OPTION2 = padRightSpace(OPTION2, 30)
            OPTION3 = padRightSpace(OPTION3, 40)
            SPACE = padRightSpace(SPACE, 50)
            MAC = padRightSpace(MAC, 44)
            CONTENT_EX = padRightSpace(3000, CONTENT_EX)

            v_return = REC_TYPE & TRX_TYPE & SD_TIME & SERIAL_NO & RELATION_NO & RESPONSE_CODE & O_CI_CODE & R_CI_CODE & O_INDIRECT_CODE & R_INDIRECT_CODE _
                        & FEE_CI_CODE & TRX_DATE & CURRENCY & EXCHANGE_RATE & AMOUNT & SD_CODE & SD_NAME & SD_ADDR & SD_ACCNT & SD_ID_NO & SD_ISSUE_DATE _
                         & SD_ISSUER & RV_CODE & RV_NAME & RV_ADDR & RV_ACCNT & RV_ID_NO & RV_ISSUE_DATE & RV_ISSUER & CONTENT & AUTHORIZED _
                         & FEE_FLAG & REFERENCE & EX_E_SIGN & CREATE_TIME & APPR_ID & E_SIGN & OPTION1 & OPTION2 & OPTION3 & SPACE & MAC & CONTENT_EX
        Catch ex As Exception

        End Try
        Return v_return
    End Function

    Public Function BuildCitadFooter() As String
        Dim v_return As String = String.Empty
        Try
            Dim REC_TYPE As String = "TT"
            Dim CI_CODE As String = padRightSpace(12, ma_nh_chuyen)
            Dim FILE_NAME As String = padRightSpace(25, FileName)
            Dim TR_DATE As String = trandate 'GetDateBy()
            Dim DATA_CNT As String = padLeftString(8, "1")

            v_return = REC_TYPE & CI_CODE & FILE_NAME & TR_DATE & DATA_CNT
        Catch ex As Exception

        End Try
        Return v_return
    End Function

    Public Function GetMac(ByVal pv_header As String, ByVal pv_body As String, ByVal pv_footer As String) As String
        Dim v_return As String = String.Empty
        Try
            v_return = signMAC(pv_header & pv_body & pv_footer)
        Catch ex As Exception

        End Try
        Return v_return
    End Function

    Public Function padRightSpace(ByVal pv_length As Integer, Optional ByVal pv_inString As String = "") As String
        Dim v_return As String = String.Empty
        Try
            If pv_inString.Length > 0 Then
                Dim v_lengthNew As Integer = pv_length - pv_inString.Length

                'hiepvx sua doan nay de khi v_lengthNew >0 thi ghep them chuoi con neu ko thi cat lay cac ky tu dau
                If v_lengthNew > 0 Then
                    v_return = pv_inString & v_return.PadRight(v_lengthNew)
                Else
                    v_return = pv_inString.Substring(0, pv_length)
                End If
            Else
                v_return = v_return.PadRight(pv_length)
            End If
        Catch ex As Exception
            v_return = v_return.PadRight(pv_length)
        End Try
        Return v_return
    End Function

    Public Function padLeftSpace(ByVal pv_length As Integer, Optional ByVal pv_inString As String = "") As String
        Dim v_return As String = String.Empty
        Try
            If pv_inString.Length > 0 Then
                Dim v_lengthNew As Integer = pv_length - pv_inString.Length

                'hiepvx sua doan nay de khi v_lengthNew >0 thi ghep them chuoi con neu ko thi cat lay cac ky tu dau
                If v_lengthNew > 0 Then
                    v_return = v_return.PadLeft(v_lengthNew) & pv_inString
                Else
                    v_return = pv_inString.Substring(0, pv_length)
                End If
            Else
                v_return = v_return.PadLeft(pv_length)
            End If
        Catch ex As Exception
            v_return = v_return.PadLeft(pv_length)
        End Try
        Return v_return
    End Function

    Public Function padLeftString(ByVal pv_length As Integer, Optional ByVal pv_inString As String = "", Optional ByVal pv_leftString As String = "0") As String
        Dim v_return As String = String.Empty
        Try
            If pv_inString.Length > 0 Then
                Dim v_lengthNew As Integer = pv_length - pv_inString.Length

                'hiepvx sua doan nay de khi v_lengthNew >0 thi ghep them chuoi con neu ko thi cat lay cac ky tu dau
                If v_lengthNew > 0 Then
                    v_return = v_return.PadLeft(v_lengthNew, pv_leftString) & pv_inString
                Else
                    v_return = pv_inString.Substring(0, pv_length)
                End If
            Else
                v_return = v_return.PadLeft(pv_length, pv_leftString)
            End If
        Catch ex As Exception
            v_return = v_return.PadLeft(pv_length, pv_leftString)
        End Try
        Return v_return
    End Function

    Public Function GetDateBy(Optional ByVal pv_format As String = "yyyyMMdd") As String
        Dim v_return As String = String.Empty
        Try
            v_return = DateTime.Now.ToString(pv_format)
        Catch ex As Exception
            v_return = DateTime.Now.ToString("yyyyMMdd")
        End Try
        Return v_return
    End Function

    Public Function signMAC(ByVal sContentMsg As String) As String
        Dim unicode As New UnicodeEncoding
        Dim hash() As Byte
        Dim byProduct() As Byte
        byProduct = unicode.GetBytes(sContentMsg)
        Dim SH1 As New SHA1CryptoServiceProvider
        hash = SH1.ComputeHash(byProduct)
        Return Convert.ToBase64String(hash)
    End Function

    Public Function AutoGenHHmmss() As String
        Dim pv_int As Integer = Convert.ToInt32(DataAccess.ExecuteSQLScalar("SELECT SEQ_CREATED_HHMMSS_CITAD.NEXTVAL FROM DUAL"))
        Dim v_return As String = String.Empty
        Try
            Dim v_hour As Integer = 0
            Dim v_minus As Integer = 0
            Dim v_second As Integer = 0

            Dim v_hourM As Integer = pv_int Mod 3600
            If (pv_int < 3600) Then
                v_hour = 0
            Else
                v_hour = (pv_int - v_hourM) / 3600
            End If

            Dim v_v_minusM As Integer = (pv_int - (v_hour * 3600)) Mod 60

            If pv_int < 60 Then
                v_minus = 0
            Else
                v_minus = (pv_int - (v_hour * 3600) - v_v_minusM) / 60
            End If

            v_second = pv_int - (v_hour * 3600) - (v_minus * 60)

            'Fix citi => 22 - 23h
            If v_hour = 0 Then
                v_hour = 22
            Else
                v_hour = 23
            End If

            v_return = v_hour.ToString.PadLeft(2, "0") & v_minus.ToString.PadLeft(2, "0") & v_second.ToString.PadLeft(2, "0")

        Catch ex As Exception
        End Try
        Return v_return
    End Function

    Public Function SaveInfoCitad() As String
       
    End Function

    Public Shared Function SaveFileServer(ByVal sMaCn As String, ByVal pv_fileName As String, ByVal pv_mac As String, _
                                    ByVal pv_header As String, ByVal pv_body As String, ByVal pv_footer As String, ByVal pvtrandate As String) As String

     
    End Function

    Public Shared Function CheckCutOff(ByVal v_cut As String, ByVal v_on As String, ByVal v_now As String) As Boolean
        Dim v_flag As String = False
        Dim v_arrCut As String() = v_cut.Split(":")
        Dim v_arrOn As String() = v_on.Split(":")
        Dim v_arrNow As String() = v_now.Split(":")

        If Convert.ToInt32(v_arrNow(0)) < Convert.ToInt32(v_arrOn(0)) Or Convert.ToInt32(v_arrNow(0)) > Convert.ToInt32(v_arrCut(0)) Then
            v_flag = True
        End If

        If Convert.ToInt32(v_arrNow(0)) = Convert.ToInt32(v_arrOn(0)) Then
            If Convert.ToInt32(v_arrNow(1)) < Convert.ToInt32(v_arrOn(1)) Then
                v_flag = True
            End If
        End If

        If Convert.ToInt32(v_arrNow(0)) = Convert.ToInt32(v_arrCut(0)) Then
            If Convert.ToInt32(v_arrNow(1)) > Convert.ToInt32(v_arrCut(1)) Then
                v_flag = True
            End If
        End If

        Return v_flag
    End Function

    Public Shared Sub UpdateTT_Citad_NTDT(ByVal so_ct As String, ByVal tthai As String)

        Try

            Dim sql As String = "update tcs_ctu_ntdt_hdr set tt_citad= '" + tthai + "' where so_ctu = '" + so_ct + "'"
            DataAccess.ExecuteNonQuery(sql, CommandType.Text)


            sql = "update tcs_ctu_ntdt_hdr_core set tt_citad= '" + tthai + "' where so_ctu = '" + so_ct + "' and TRANGTHAI_IMPORT = 15"
            DataAccess.ExecuteNonQuery(sql, CommandType.Text)

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Sub UpdateTT_Citad_ETAX(ByVal so_ct As String, ByVal tthai As String)

        Try

            Dim sql As String = "update tcs_ctu_hdr set tt_citad= '" + tthai + "' where so_ct = '" + so_ct + "'"
            DataAccess.ExecuteNonQuery(sql, CommandType.Text)


            sql = "update tcs_ctu_hdr_core set tt_citad= '" + tthai + "' where so_ctu = '" + so_ct + "'"
            DataAccess.ExecuteNonQuery(sql, CommandType.Text)

        Catch ex As Exception

        End Try

    End Sub

    'Public Shared Sub WriteLog(ByVal ex As Exception, ByVal strDescription As String, Optional ByVal approverID As String = "")

    '    Dim sbErrMsg As StringBuilder
    '    sbErrMsg = New StringBuilder()
    '    sbErrMsg.Append(vbCrLf)
    '    If approverID <> "" Then
    '        sbErrMsg.Append("approverID: " & approverID)
    '        sbErrMsg.Append(vbCrLf)
    '    End If
    '    sbErrMsg.Append(strDescription & " : ")
    '    sbErrMsg.Append(ex.Source)
    '    sbErrMsg.Append(vbCrLf)
    '    sbErrMsg.Append(ex.Message)
    '    sbErrMsg.Append(vbCrLf)
    '    sbErrMsg.Append(ex.StackTrace)

    '    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

    '    If Not ex.InnerException Is Nothing Then
    '        sbErrMsg = New StringBuilder()
    '        sbErrMsg.Append(strDescription & " : ")
    '        sbErrMsg.Append(ex.InnerException.Source)
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.InnerException.Message)
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.InnerException.StackTrace)

    '        LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
    '    End If

    '    LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '          & "Error code: System error!" & vbNewLine _
    '          & "Error message: " & ex.Message, EventLogEntryType.Error)
    'End Sub

    Public Function CreateCitadV2(ByVal sSoCtu As String, ByVal sType As String) As String
        Dim sReturn As String = String.Empty
        Try
            Dim vSql As String = "SELECT A.MA_CN, A.MA_CQTHU, A.TEN_CQTHU, A.TK_KH_NH, A.TTIEN, A.TK_CO, KB.TEN TEN_KB, " & _
                                        "A.MA_NNTHUE, A.TEN_NNTHUE, A.MA_NH_A, A.MA_NH_B, A.MA_NH_TT, A.TEN_NH_B, A.TEN_NH_TT, A.REMARKS, A.SO_CT_NH,a.ma_nt MA_NGUYENTE,'' ma_thamchieu,a.kyhieu_ct,to_char(a.ngay_kh_nh,'RRRRMMDD') ngay_kh_nh,a.ma_lthue " & _
                                    "FROM TCS_CTU_HDR A, TCS_DM_KHOBAC KB WHERE A.SHKB = KB.SHKB AND A.SO_CT ='" & sSoCtu & "'"

            Select Case sType
                Case "NTDT"
                    vSql = "SELECT A.MA_CN,A.MA_CQTHU, A.TEN_CQTHU, A.STK_NHANG_NOP TK_KH_NH, A.TONG_TIEN TTIEN, A.STK_THU TK_CO,A.TEN_KBAC TEN_KB, " & _
                                "A.MST_NNOP MA_NNTHUE, A.TEN_NNOP TEN_NNTHUE, A.MA_CN_TK MA_NH_A, NH.MA_GIANTIEP MA_NH_B, NH.MA_TRUCTIEP MA_NH_TT, NH.TEN_GIANTIEP TEN_NH_B, " & _
                                "NH.TEN_TRUCTIEP TEN_NH_TT, A.REMARK REMARKS, A.SO_CT_NH,A.MA_NGUYENTE,a.ma_thamchieu ,mahieu_ctu kyhieu_ct,to_char(a.ngay_nhang,'RRRRMMDD') ngay_kh_nh,'01' ma_lthue " & _
                              "FROM TCS_CTU_NTDT_HDR A, TCS_DM_NH_GIANTIEP_TRUCTIEP NH " & _
                              "WHERE A.MA_HIEU_KBAC = NH.SHKB AND A.SO_CTU ='" & sSoCtu & "' AND A.ma_nhang_unt = nh.MA_GIANTIEP"
                Case Else
                    Exit Select
            End Select
            sSO_CT = sSoCtu
            Dim vDt As DataTable = DataAccess.ExecuteToTable(vSql)
            If vDt IsNot Nothing Then
                If vDt.Rows.Count > 0 Then
                    Dim sCoreRef As String = sType & "." & sSoCtu

                    Dim sMaCn As String = vDt.Rows(0)("MA_CN")
                    CURRENCY = vDt.Rows(0)("MA_NGUYENTE").ToString().Trim().ToUpper()
                    If CURRENCY.Equals("USD") Then
                        TRX_TYPE = "201111"
                    ElseIf CURRENCY.Equals("EUR") Then
                        TRX_TYPE = "201121"
                    Else
                        Dim strGT_CAOTHAP As String = Business.Common.mdlCommon.TCS_GETTS("TT_CITAD")
                        If Convert.ToDecimal(DateTime.Now.ToString("HHmm")) < Convert.ToDecimal(strGT_CAOTHAP.Replace(":", "")) Then
                            TRX_TYPE = "101101"
                        Else
                            TRX_TYPE = "201101"
                        End If

                    End If
                    SERIAL_NO = padRightSpace(sSoCtu, 8)
                    RELATION_NO = sType & "." & sSoCtu
                    O_CI_CODE = vDt.Rows(0)("MA_NH_A").ToString()
                    R_CI_CODE = vDt.Rows(0)("MA_NH_TT").ToString()
                    O_INDIRECT_CODE = vDt.Rows(0)("MA_NH_A").ToString()
                    R_INDIRECT_CODE = vDt.Rows(0)("MA_NH_B").ToString()
                    FEE_CI_CODE = vDt.Rows(0)("MA_NH_A").ToString()
                    AMOUNT = Decimal.Parse(vDt.Rows(0)("TTIEN").ToString()).ToString("#.##")
                    AMOUNT = AMOUNT.Replace(".", ",")
                    EXCHANGE_RATE = "1.00" 'LUON FIX TY GIA =1.00
                    SD_CODE = ""
                    SD_NAME = Globals.RemoveSign4VietnameseString(vDt.Rows(0)("TEN_NNTHUE").ToString())
                    SD_ADDR = ""
                    SD_ACCNT = vDt.Rows(0)("TK_KH_NH").ToString()
                    SD_ID_NO = ""
                    SD_ISSUE_DATE = ""
                    SD_ISSUER = ""

                    RV_CODE = vDt.Rows(0)("MA_CQTHU").ToString()
                    RV_NAME = Globals.RemoveSign4VietnameseString(vDt.Rows(0)("TEN_CQTHU").ToString())
                    RV_ADDR = Globals.RemoveSign4VietnameseString(vDt.Rows(0)("TEN_KB").ToString())
                    RV_ACCNT = vDt.Rows(0)("TK_CO").ToString()
                    RV_ID_NO = ""

                    RV_ISSUE_DATE = ""
                    RV_ISSUER = ""
                    CONTENT = Globals.RemoveSign4VietnameseString(vDt.Rows(0)("REMARKS").ToString())
                    TAX_CODE = vDt.Rows(0)("MA_NNTHUE").ToString()
                    sSoCtuNH = vDt.Rows(0)("SO_CT_NH").ToString
                    ma_nh_chuyen = O_CI_CODE
                    sRefSoCtuNH = sType & "." & sSoCtu
                    'doan nay em lam cho phan chuoi content ex
                    Dim objEX As CitadContentEx_25.CitadContentEx_25 = New CitadContentEx_25.CitadContentEx_25()
                    objEX.STC = padRightSpace(vDt.Rows(0)("ma_thamchieu").ToString(), 50)
                    objEX.SCT = padRightSpace(sSoCtu, 40)
                    objEX.KCT = padRightSpace(vDt.Rows(0)("kyhieu_ct").ToString(), 10)
                    objEX.TNT = padRightSpace(Globals.RemoveSign4VietnameseString(vDt.Rows(0)("TEN_NNTHUE").ToString()), 200)
                    objEX.MST = padRightSpace(vDt.Rows(0)("MA_NNTHUE").ToString(), 14)
                    objEX.MDB = "00000"
                    objEX.CQT = padRightSpace(vDt.Rows(0)("MA_CQTHU").ToString(), 7)
                    objEX.TCQ = padRightSpace(Globals.RemoveSign4VietnameseString(vDt.Rows(0)("TEN_CQTHU").ToString()), 200)

                    'Dim cls_corebank As New CorebankServiceESB.clsCoreBank()

                    objEX.NNT = vDt.Rows(0)("ngay_kh_nh").ToString()
                    If vDt.Rows(0)("ma_lthue").ToString().Equals("04") Then
                        objEX.LTH = "04"
                    Else
                        objEX.LTH = "01"
                    End If
                    objEX.TKN = "01"
                    If vDt.Rows(0)("TK_CO").ToString().Length >= 4 Then
                        If vDt.Rows(0)("TK_CO").ToString().Substring(0, 2).Equals("35") Then
                            objEX.TKN = "02"
                        End If
                    End If
                    objEX.KLN = padRightSpace(2)
                    'h den doan em lam chi tiet
                    objEX.VSTD = New List(Of CitadContentEx_25.VSTD_DATA)
                    Dim strSQL As String = ""
                    If sType.Equals("NTDT") Then
                        strSQL = " SELECT ma_chuong,"
                        strSQL &= " ma_tmuc,"
                        strSQL &= " A.kythue ky_thue,"
                        strSQL &= " A.so_tk_tb_qd so_tk,"
                        strSQL &= " A.noi_dung,"
                        strSQL &= "  A.sotien"
                        strSQL &= " FROM tcs_ctu_ntdt_dtl a WHERE A.so_ct='" & sSoCtu & "'"
                    Else
                        strSQL = "SELECT ma_chuong,"
                        strSQL &= "   ma_tmuc,"
                        strSQL &= "NVL (ky_thue, TO_CHAR (a.ngay_tk, 'DD/MM/RRRR')) ky_thue,"
                        strSQL &= "NVL (a.so_tk,"
                        strSQL &= "(SELECT MAX (B.SO_QD)"
                        strSQL &= "FROM TCS_CTU_HDR B"
                        strSQL &= "              WHERE A.so_ct = B.SO_CT)"
                        strSQL &= " )"
                        strSQL &= "    so_tk,"
                        strSQL &= " A.noi_dung, "
                        strSQL &= " A.sotien "
                        strSQL &= " FROM tcs_ctu_dtl a WHERE SO_CT='" & sSoCtu & "'"

                    End If
                    Dim dsdtl As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                    If dsdtl Is Nothing Then
                        Throw New Exception("Khong ton tai chi tiet chu tu " & sSoCtu)
                    End If
                    If dsdtl.Tables.Count <= 0 Then
                        Throw New Exception("Khong ton tai chi tiet chu tu " & sSoCtu)
                    End If
                    If dsdtl.Tables(0).Rows.Count <= 0 Then
                        Throw New Exception("Khong ton tai chi tiet chu tu " & sSoCtu)
                    End If
                    For i As Integer = 0 To dsdtl.Tables(0).Rows.Count - 1
                        Dim objdtl As CitadContentEx_25.VSTD_DATA = New CitadContentEx_25.VSTD_DATA()
                        objdtl.MCH = dsdtl.Tables(0).Rows(i)("ma_chuong").ToString()
                        objdtl.MND = dsdtl.Tables(0).Rows(i)("ma_tmuc").ToString()
                        objdtl.STN = dsdtl.Tables(0).Rows(i)("sotien").ToString()
                        objdtl.NDN = padRightSpace(Globals.RemoveSign4VietnameseString(dsdtl.Tables(0).Rows(i)("noi_dung").ToString()), 100)
                        objdtl.NTK = dsdtl.Tables(0).Rows(i)("ky_thue").ToString()
                        If objdtl.NTK.Trim.Length = 0 Then
                            objdtl.NTK = Date.Now.ToString("dd/MM/yyyy")
                        End If
                        If objdtl.NTK.Trim.Length = 6 Then
                            objdtl.NTK = "01/0" & objdtl.NTK
                        End If
                        If objdtl.NTK.Trim.Length = 7 Then
                            objdtl.NTK = "01/" & objdtl.NTK
                        End If
                        objdtl.STK = padRightSpace(dsdtl.Tables(0).Rows(i)("so_tk").ToString(), 30)
                        objEX.VSTD.Add(objdtl)
                    Next
                    CONTENT_EX = padRightSpace(objEX.toStringCITAD25(), 3000)
                    sReturn = SaveInfoCitad()
                End If
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog(ex.Message & " " & ex.StackTrace, EventLogEntryType.Error)
        End Try
        Return sReturn
    End Function
    Public Function CreateContentEx(ByVal sSoCtu As String, ByVal sType As String, ByVal sCoreDate As String) As String
        Dim sReturn As String = String.Empty
        Try
            Dim vSql As String = "SELECT A.MA_CN, A.MA_CQTHU,CQ.TEN as TEN_CQTHU,A.ma_hq_ph, A.TK_KH_NH, A.TTIEN, A.TK_CO, KB.TEN TEN_KB,NVL(A.MA_XA, '00000') ma_dbhc, " & _
                                        "A.MA_NNTHUE, A.TEN_NNTHUE,A.DC_NNTHUE as DC_NNTHUE, A.MA_NH_A, A.MA_NH_B, A.MA_NH_TT, A.TEN_NH_B, A.TEN_NH_TT, A.SO_CT_NH,a.ma_nt MA_NGUYENTE,'' ma_thamchieu,a.kyhieu_ct,to_char(a.ngay_kh_nh,'RRRRMMDD') ngay_kh_nh,a.ma_lthue,a.cq_qd ID_TK_KNGHI,A.SHKB,  " & _
                                        "A.so_khung, A.so_may, A.so_to_bando, A.ma_dbhc_ts, A.so_thuadat, A.dia_chi_ts, A.dac_diem_ptien  " & _
                                    " FROM TCS_CTU_HDR A, TCS_DM_KHOBAC KB, TCS_DM_CQTHU CQ WHERE A.SHKB = KB.SHKB AND A.MA_CQTHU = CQ.MA_CQTHU AND A.SO_CT ='" & sSoCtu & "'"

            Select Case sType
                Case "NTDT"
                    ' tuanda - 27/02/24
                    ' thêm ma_thamchieu_knop
                    vSql = "SELECT a.ma_thamchieu_knop, A.MA_CQTHU, A.TEN_CQTHU, A.STK_NHANG_NOP TK_KH_NH, A.TONG_TIEN TTIEN, A.STK_THU TK_CO,A.TEN_KBAC TEN_KB,A.ma_dbhc_thu as ma_dbhc, " & _
                              "A.MST_NNOP MA_NNTHUE, A.TEN_NNOP TEN_NNTHUE,A.DIACHI_NNOP as DC_NNTHUE, A.ma_nhang_unt MA_NH_B, '' MA_NH_TT, A.ten_nhang_unt TEN_NH_B, " & _
                              "'' TEN_NH_TT, A.SO_CT_NH,A.MA_NGUYENTE,a.ma_thamchieu ,mahieu_ctu kyhieu_ct,to_char(a.ngay_nhang,'RRRRMMDD') ngay_kh_nh,A.ma_lthue, A.ID_TK_KNGHI,A.MA_HIEU_KBAC as SHKB " & _
                            "FROM TCS_CTU_NTDT_HDR A " & _
                            "WHERE  A.SO_CTU ='" & sSoCtu & "'"
                Case "HQDT"
                    vSql = "SELECT A.MA_CQTHU, A.TEN_CQTHU, C.TAIKHOAN_TH TK_KH_NH, A.TTIEN TTIEN, A.TK_CO TK_CO,A.SHKB,(SELECT MAX(KB.TEN) FROM TCS_DM_KHOBAC KB WHERE KB.SHKB=A.SHKB) TEN_KB,'00000' as ma_dbhc,'' ID_TK_KNGHI, " & _
                               "A.MA_NNTHUE MA_NNTHUE, A.TEN_NNTHUE TEN_NNTHUE,A.dc_nnthue as DC_NNTHUE, '' MA_NH_B, '' MA_NH_TT, '' TEN_NH_B, " & _
                               "'' TEN_NH_TT, A.SO_CT_NH ,a.ma_nt MA_NGUYENTE,'' ma_thamchieu ,a.kyhieu_ct ,to_char(a.ngay_kh_nh,'RRRRMMDD') ngay_kh_nh,'04' ma_lthue " & _
                             "FROM TCS_CTU_NTDT_HQ247_HDR A, TCS_HQ247_304 C " & _
                             " WHERE  A.SO_CT=C.SO_CTU AND A.SO_CT_NH=C.SO_CT_NH AND A.SO_CT ='" & sSoCtu & "'"
                Case "HQNT"
                    vSql = "SELECT A.MA_CQTHU, A.TEN_CQTHU, C.TAIKHOAN_TH TK_KH_NH, A.TTIEN TTIEN, A.TK_CO TK_CO,A.SHKB,(SELECT MAX(KB.TEN) FROM TCS_DM_KHOBAC KB WHERE KB.SHKB=A.SHKB) TEN_KB,'00000' as ma_dbhc,'' ID_TK_KNGHI, " & _
                              "A.MA_NNTHUE MA_NNTHUE, A.TEN_NNTHUE TEN_NNTHUE,A.dc_nnthue as DC_NNTHUE, '' MA_NH_B, '' MA_NH_TT, '' TEN_NH_B, " & _
                              "'' TEN_NH_TT, A.SO_CT_NH ,a.ma_nt MA_NGUYENTE,'' ma_thamchieu ,a.kyhieu_ct ,to_char(a.ngay_kh_nh,'RRRRMMDD') ngay_kh_nh,'04' ma_lthue " & _
                            "FROM tcs_ctu_hq_nhothu_hdr A, TCS_DM_NH_GIANTIEP_TRUCTIEP NH ,TCS_HQ247_201 C " & _
                            " WHERE  A.SO_CT=C.SO_CTU AND A.SO_CT_NH=C.SO_CT_NH AND A.SO_CT ='" & sSoCtu & "' "
                Case "BIENLAI"
                    vSql = "select  a.MA_CN, a.ma_cqthu,a.ten_cqthu,'' ma_hq_ph,a.tk_kh_nh,a.ttien as TTIEN, a.tk_co,a.ten_kb,'00000' as ma_dbhc, " & _
                             " a.ma_nnthue,a.ten_nnthue,a.dc_nnthue,a.ma_nh_gt MA_NH_B,a.ma_nh_tt,a.ten_nh_gt TEN_NH_B, a.ten_nh_tt, " & _
                             " a.so_ct_nh,a.ma_nt MA_NGUYENTE,'' ma_thamchieu,a.kyhieu_ct,to_char(a.ngay_kh_nh,'RRRRMMDD') ngay_kh_nh,a.ma_lthue,a.cq_qd ID_TK_KNGHI, a.shkb " & _
                           " from tcs_bienlai_hdr a where A.SO_CT ='" & sSoCtu & "' "
                Case "MOBILE"
                    vSql = "select A.ma_cn,A.MA_CQTHU,A.ten_cqthu,A.TK_KH_NH,A.TTIEN, A.TK_CO, KB.TEN TEN_KB,A.ma_xa as ma_dbhc, '' MA_NTK, " &
                            "  A.ma_nnthue, A.TEN_NNTHUE, A.DC_NNTHUE, A.ma_nh_a,A.ma_nh_b,A.MA_NH_TT, A.ten_nh_b,   " &
                            "  A.TEN_NH_TT, '' REMARKS, A.so_ct_nh, A.ma_nt as  MA_NGUYENTE, A.mathamchieu ma_thamchieu,A.kyhieu_ct," &
                            "  to_char(a.ngay_kh_nh,'RRRRMMDD') ngay_kh_nh, A.ma_lthue, A.so_khung, A.so_may,'' ma_hq_ph  " &
                            "  from TCS_CTU_HDR_MOBITCT A, TCS_DM_KHOBAC KB where A.SHKB = KB.SHKB AND  A.SO_CT ='" & sSoCtu & "'"
                Case Else
                    Exit Select
            End Select
            sSO_CT = sSoCtu
            Dim vDt As DataTable = DataAccess.ExecuteToTable(vSql)
            If vDt IsNot Nothing Then
                If vDt.Rows.Count > 0 Then
                    'doan nay em lam cho phan chuoi content ex
                    Dim strTEN_CQTHU As String = ""
                    Dim strMa_hq As String = ""
                    Dim sKH_CT As String = ""

                    Dim strSoKhung As String = ""
                    Dim strSoMay As String = ""
                    Dim strThueDat As String = ""
                    Dim strSoToBanDo As String = ""
                    Dim strDBHCTS As String = ""
                    Dim strSoThuaDat As String = ""
                    Dim strDiaChiTS As String = ""
                    Dim dac_diem_pt As String = ""
                    If sType.Equals("NTTQ") Then
                        Dim dtKH_CT As DataTable = DataAccess.ExecuteToTable("select giatri_ts from tcs_thamso where ten_ts='KHCT' and loai_ts = 'LTV'")
                        If dtKH_CT.Rows.Count > 0 Then
                            sKH_CT = dtKH_CT.Rows(0)(0).ToString()
                        End If
                        strTEN_CQTHU = vDt.Rows(0)("TEN_CQTHU").ToString()
                        If strTEN_CQTHU.Length <= 0 Then
                            strMa_hq = vDt.Rows(0)("ma_hq_ph").ToString()
                            strTEN_CQTHU = Business.Common.mdlCommon.TCS_GETTENCQTHU(vDt.Rows(0)("MA_CQTHU").ToString(), strMa_hq)
                        End If


                        strSoKhung = vDt.Rows(0)("SO_KHUNG").ToString()
                        strSoMay = vDt.Rows(0)("SO_MAY").ToString()
                        dac_diem_pt = vDt.Rows(0)("dac_diem_ptien").ToString()

                        strSoToBanDo = vDt.Rows(0)("so_to_bando").ToString()
                        strDBHCTS = vDt.Rows(0)("ma_dbhc_ts").ToString()
                        strSoThuaDat = vDt.Rows(0)("so_thuadat").ToString()
                        strDiaChiTS = vDt.Rows(0)("dia_chi_ts").ToString()
                        'SOTOBANDOABC DBHCTS789+SOTHUADAT12345459 DC657 

                        If (vDt.Rows(0)("ma_lthue").ToString().Equals("08")) Then
                            strThueDat = "SOTOBANDO" & strSoToBanDo & " DBHCTS" & strDBHCTS & "+SOTHUADAT" & strSoThuaDat & " DC" & strDiaChiTS
                        End If

                    Else
                        strTEN_CQTHU = vDt.Rows(0)("TEN_CQTHU").ToString()
                    End If


                    If sType.Equals("MOBILE") Then
                        strSoKhung = vDt.Rows(0)("SO_KHUNG").ToString()
                        strSoMay = vDt.Rows(0)("SO_MAY").ToString()
                    End If

                    Dim ten_nnthue As String = vDt.Rows(0)("TEN_NNTHUE").ToString()
                    Dim dc_nnthue As String = vDt.Rows(0)("DC_NNTHUE").ToString()

                    If dc_nnthue.Contains("&amp;") Then
                        dc_nnthue = dc_nnthue.Replace("&amp;", " ")
                    End If
                    dc_nnthue = dc_nnthue.Replace("&", " ")

                    'Dim strChar As String = Business.Common.mdlCommon.TCS_GETTS("SPECIAL_CHAR_PAYMENTDETAIL")
                    'Dim arrChar() As String = strChar.Split(",")

                    'For Each special In arrChar
                    '    ten_nnthue = ten_nnthue.Replace(special, "")
                    '    dc_nnthue = dc_nnthue.Replace(special, "")
                    '    strTEN_CQTHU = strTEN_CQTHU.Replace(special, "")
                    'Next

                    Dim objEX As CitadContentEx_25.CitadContentEx_25 = New CitadContentEx_25.CitadContentEx_25()
                    objEX.STC = vDt.Rows(0)("so_ct_nh").ToString()
                    objEX.SCT = vDt.Rows(0)("so_ct_nh").ToString()
                    'Dong A yeu cau truyen so chung tu vao KHCT
                    If sType.Equals("NTTQ") Then
                        objEX.KCT = sSoCtu
                    Else
                        objEX.KCT = sSoCtu
                    End If


                    objEX.TNT = Globals.RemoveSign4VietnameseString(ten_nnthue)

                    'TODO: vinhvv
                    'Dim dc_nnthue = vDt.Rows(0)("DC_NNTHUE").ToString()
                    Dim ma_nnthue = vDt.Rows(0)("MA_NNTHUE").ToString()

                    If String.IsNullOrEmpty(dc_nnthue) AndAlso Not String.IsNullOrEmpty(ma_nnthue) Then
                        'Lấy từ dm tk_kh
                        ' Dim query = "select dia_chi from tcs_dm_taikhoan_kh where rownum = 1 and length(dia_chi)>0 and masothue = '" & ma_nnthue & "'"
                        'dc_nnthue = DataAccess.ExecuteSQLScalar(query)
                    End If

                    If String.IsNullOrEmpty(dc_nnthue) Then
                        dc_nnthue = "KHACH HANG NOP THUE TAI NGAN HANG SHB"
                    End If

                    If dc_nnthue.Length > 200 Then
                        dc_nnthue = dc_nnthue.Substring(0, 200)
                    End If

                    Dim ma_dbhc As String = vDt.Rows(0)("ma_dbhc").ToString()
                    'If sType.Equals("NTDT") Then
                    'Else
                    '    ma_dbhc = Business.Common.mdlCommon.TCS_GETDBHC(vDt.Rows(0)("SHKB").ToString())
                    '    If String.IsNullOrEmpty(ma_dbhc) Then
                    '        ma_dbhc = "00000"
                    '    Else
                    '        If ma_dbhc.Equals("NULL") Or ma_dbhc.Trim.Length < 5 Then
                    '            ma_dbhc = "00000"
                    '        End If
                    '    End If

                    'End If
                    If String.IsNullOrEmpty(ma_dbhc) Then
                        ma_dbhc = "00000"
                    Else
                        If ma_dbhc.Equals("NULL") Or ma_dbhc.Trim.Length < 5 Then
                            ma_dbhc = "00000"
                        End If
                    End If
                    If vDt.Rows(0)("ma_lthue").ToString().Equals("04") Then
                        ma_dbhc = "00000"
                    End If

                    objEX.DNT = Globals.RemoveSign4VietnameseString(dc_nnthue)
                    If vDt.Rows(0)("ma_lthue").ToString().Equals("04") Then
                        If ma_nnthue.Length = 13 Then
                            Dim ma_chinh As String = ma_nnthue.Substring(0, 10)
                            Dim ma_cn_mst As String = ma_nnthue.Substring(10, 3)

                            ma_nnthue = ma_chinh & "-" & ma_cn_mst

                        End If
                    End If
                    objEX.MST = ma_nnthue
                    objEX.MDB = ma_dbhc
                    objEX.CQT = vDt.Rows(0)("MA_CQTHU").ToString()
                    objEX.TCQ = Globals.RemoveSign4VietnameseString(strTEN_CQTHU)

                    'Dim cls_corebank As New CorebankServiceESB.clsCoreBank()

                    objEX.NNT = sCoreDate
                    Dim sMa_Lthue As String = vDt.Rows(0)("ma_lthue").ToString()
                    If sMa_Lthue.Equals("04") Then
                        objEX.LTH = "04"
                    ElseIf sMa_Lthue.Equals("01") Or sMa_Lthue.Equals("02") Or sMa_Lthue.Equals("03") Or sMa_Lthue.Equals("05") Or sMa_Lthue.Equals("06") Or sMa_Lthue.Equals("08") Then
                        objEX.LTH = "01"
                    ElseIf sMa_Lthue.Equals("07") Then
                        objEX.LTH = "03"
                    End If
                    objEX.TKN = "01"
                    If vDt.Rows(0)("TK_CO").ToString().Length >= 4 Then
                        If vDt.Rows(0)("TK_CO").ToString().Substring(0, 2).Equals("35") Then
                            objEX.TKN = "02"
                        ElseIf vDt.Rows(0)("TK_CO").ToString().Substring(0, 4).Equals("8993") Then
                            objEX.TKN = "03"
                        End If
                    End If

                    Dim tk_knghi As String = vDt.Rows(0)("ID_TK_KNGHI").ToString()
                    If tk_knghi.Length <= 0 Or tk_knghi.Length > 2 Then
                        tk_knghi = "04"
                    ElseIf tk_knghi.Length = 1 Then
                        tk_knghi = "0" & tk_knghi
                    End If

                    objEX.KLN = tk_knghi
                    objEX.MKB = vDt.Rows(0)("SHKB").ToString()
                    'h den doan em lam chi tiet
                    objEX.VSTD = New List(Of CitadContentEx_25.VSTD_DATA)
                    Dim strSQL As String = ""
                    ' tuanda - 27/02/24
                    ' thêm id_knop
                    If sType.Equals("NTDT") Then
                        strSQL = " SELECT id_knop,"
                        strSQL &= " ma_chuong,"
                        strSQL &= " ma_tmuc,"
                        strSQL &= " A.kythue ky_thue,"
                        strSQL &= " A.so_tk_tb_qd so_tk,"
                        strSQL &= " A.noi_dung,"
                        strSQL &= "  A.sotien"
                        strSQL &= " FROM tcs_ctu_ntdt_dtl a WHERE A.so_ct='" & sSoCtu & "'"
                    ElseIf sType.Equals("HQDT") Then
                        strSQL = "  SELECT ma_chuong,"
                        strSQL &= "    ma_tmuc,"
                        strSQL &= "      NVL (ky_thue, TO_CHAR (a.ngay_tk, 'DD/MM/RRRR')) ky_thue,"
                        strSQL &= "        a.so_tk,"
                        strSQL &= "    A.noi_dung,"
                        strSQL &= "    A.sotien"
                        strSQL &= "    FROM tcs_ctu_ntdt_hq247_dtl a WHERE A.so_ct='" & sSoCtu & "'"
                    ElseIf sType.Equals("HQNT") Then
                        strSQL = "  SELECT ma_chuong,"
                        strSQL &= "    ma_tmuc,"
                        strSQL &= "      NVL (ky_thue, TO_CHAR (a.ngay_tk, 'DD/MM/RRRR')) ky_thue,"
                        strSQL &= "        a.so_tk,"
                        strSQL &= "    A.noi_dung,"
                        strSQL &= "    A.sotien"
                        strSQL &= "    FROM tcs_ctu_hq_nhothu_dtl a WHERE A.so_ct='" & sSoCtu & "'"
                    ElseIf sType.Equals("BIENLAI") Then
                        strSQL = "  SELECT ma_chuong, ma_ndkt ma_tmuc,"
                        strSQL &= "     TO_CHAR (a.ngay_qd, 'DD/MM/RRRR') ky_thue, "
                        strSQL &= "      a.SO_QD so_tk, A.noi_dung, A.ttien sotien"
                        strSQL &= "       FROM TCS_bienlai_HDR a  WHERE A.so_ct='" & sSoCtu & "'"
                    ElseIf sType.Equals("MOBILE") Then
                        strSQL = "  select A.ma_chuong,A.ma_tmuc,A.ky_thue,"
                        strSQL &= "    A.so_tk,"
                        strSQL &= " A.noi_dung, "
                        strSQL &= " A.sotien, '' ma_lhxnk, '' ma_lt from TCS_CTU_dtl_MOBITCT A where A.so_ct='" & sSoCtu & "'"
                    Else
                        If vDt.Rows(0)("ma_lthue").ToString().Equals("04") Then
                            strSQL = "SELECT ma_chuong,"
                            strSQL &= "   ma_tmuc,"
                            strSQL &= "TO_CHAR (a.ngay_tk, 'DD/MM/RRRR') ky_thue,"
                            strSQL &= "    A.so_tk,"
                            strSQL &= " A.noi_dung, "
                            strSQL &= " A.sotien "
                            strSQL &= " FROM tcs_ctu_dtl a WHERE SO_CT='" & sSoCtu & "'"
                        Else
                            strSQL = "SELECT ma_chuong,"
                            strSQL &= "   ma_tmuc,"
                            strSQL &= "A.ky_thue ky_thue,"
                            strSQL &= "(SELECT MAX (B.SO_QD)"
                            strSQL &= "FROM TCS_CTU_HDR B"
                            strSQL &= "              WHERE A.so_ct = B.SO_CT)"
                            strSQL &= "    so_tk,"
                            strSQL &= " A.noi_dung, "
                            strSQL &= " A.sotien "
                            strSQL &= " FROM tcs_ctu_dtl a WHERE SO_CT='" & sSoCtu & "'"
                        End If

                    End If
                    Dim dsdtl As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                    If dsdtl Is Nothing Then
                        Throw New Exception("Khong ton tai chi tiet chu tu " & sSoCtu)
                    End If
                    If dsdtl.Tables.Count <= 0 Then
                        Throw New Exception("Khong ton tai chi tiet chu tu " & sSoCtu)
                    End If
                    If dsdtl.Tables(0).Rows.Count <= 0 Then
                        Throw New Exception("Khong ton tai chi tiet chu tu " & sSoCtu)
                    End If
                    For i As Integer = 0 To dsdtl.Tables(0).Rows.Count - 1
                        Dim objdtl As CitadContentEx_25.VSTD_DATA = New CitadContentEx_25.VSTD_DATA()
                        'Dim stk_tb_qd As String = dsdtl.Tables(0).Rows(i)("so_tk").ToString()
                        Dim stk_tb_qd As String = ""
                        If sType.Equals("NTDT") Then
                            Dim strIDKhoanNop As String = dsdtl.Tables(0).Rows(i)("id_knop")
                            Dim strSoTK As String = dsdtl.Tables(0).Rows(i)("so_tk")
                            If strIDKhoanNop.Length > 0 And strSoTK.Length > 0 Then
                                stk_tb_qd = strIDKhoanNop
                            ElseIf strIDKhoanNop.Length > 0 And strSoTK.Length = 0 Then
                                stk_tb_qd = strIDKhoanNop
                            ElseIf strIDKhoanNop.Length = 0 And strSoTK.Length > 0 Then
                                stk_tb_qd = strSoTK
                            End If
                        Else
                            stk_tb_qd = Globals.RemoveSign4VietnameseString(dsdtl.Tables(0).Rows(i)("so_tk").ToString())
                        End If


                        Dim noidung As String = dsdtl.Tables(0).Rows(i)("noi_dung").ToString()
                        'For Each special In arrChar
                        '    stk_tb_qd = stk_tb_qd.Replace(special, "")
                        '    noidung = noidung.Replace(special, "")
                        'Next
                        'vinhvv 
                        '' Thêm cho thuế trước bạ
                        If sType.Equals("NTTQ") Then
                            If (strSoKhung.Length > 0 And strSoMay.Length > 0) Then
                                'noidung &= " so khung " & strSoKhung & " so may " & strSoMay
                                noidung = "SK " & strSoKhung & " SM " & strSoMay & " DDPT " & dac_diem_pt & " " & noidung
                            End If

                            If strThueDat.Length > 0 Then
                                noidung = strThueDat & " " & noidung
                            End If
                        ElseIf sType.Equals("MOBILE") Then
                            If (strSoKhung.Length > 0 And strSoMay.Length > 0) Then
                                noidung = "so khung " & strSoKhung & " so may " & strSoMay & " " & noidung
                            End If
                        End If

                        If noidung.Length > 100 Then
                            noidung = noidung.Substring(0, 100)
                        End If
                        objdtl.MCH = dsdtl.Tables(0).Rows(i)("ma_chuong").ToString()
                        objdtl.MND = dsdtl.Tables(0).Rows(i)("ma_tmuc").ToString()
                        'objdtl.STN = dsdtl.Tables(0).Rows(i)("sotien").ToString()
                        Dim so_tien As Decimal = 0
                        Decimal.TryParse(dsdtl.Tables(0).Rows(i)("sotien").ToString(), so_tien)

                        objdtl.STN = so_tien.ToString("F").Replace(".", ",")

                        objdtl.NDN = Globals.RemoveSign4VietnameseString(noidung)
                        objdtl.NTK = dsdtl.Tables(0).Rows(i)("ky_thue").ToString()
                        If objdtl.NTK.Trim.Length = 0 Then
                            objdtl.NTK = Date.Now.ToString("dd/MM/yyyy")
                        End If
                        If objdtl.NTK.Trim.Length = 6 Then
                            objdtl.NTK = "01/0" & objdtl.NTK
                        End If
                        If objdtl.NTK.Trim.Length = 7 Then
                            objdtl.NTK = "01/" & objdtl.NTK
                        End If
                        objdtl.STK = stk_tb_qd
                        objEX.VSTD.Add(objdtl)
                    Next
                    CONTENT_EX = objEX.toStringCITAD25().Trim()
                End If
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  LogDebug.WriteLog(ex.Message & " " & ex.StackTrace, EventLogEntryType.Error)
        End Try
        Return CONTENT_EX
    End Function

    Public Shared Sub UpdateTTCITAD(ByVal so_ct As String, ByVal tthai As String)
        Try
            Dim sql As String = "update tcs_ctu_hdr set tt_citad= '" + tthai + "' where so_ct = '" + so_ct + "'"
            DataAccess.ExecuteNonQuery(sql, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog("Update TT_Citad_Ebank. Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
    Public Shared Function get_core_date() As String
        Dim connect As DataAccess
        Dim value_date As String = ""
        Try
            connect = New DataAccess
            Dim strSql As String = "select tcs_pck_util_shb.fnc_get_val_date('SHB') value_date from dual"

            Dim ds As DataSet = connect.ExecuteReturnDataSet(strSql, CommandType.Text)

            If Not IsEmptyDataSet(ds) Then
                Return value_date = ds.Tables(0).Rows(0)("value_date").ToString()
            Else
                Return DateTime.Now.ToString("yyyyMMdd")
            End If

        Catch ex As Exception
            Return DateTime.Now.ToString("yyyyMMdd")
        Finally
            connect.Dispose()
        End Try

    End Function
    Public Shared Function IsEmptyDataSet(ByVal ds As DataSet) As Boolean
        Try
            If (IsNothing(ds)) Then Return True
            If (ds.Tables.Count = 0) Then Return True
            If (ds.Tables(0).Rows.Count = 0) Then Return True

            Return False
        Catch ex As Exception
            Return True
        End Try
    End Function
    'Nhamltl: 20170901_Sửa cho phép save file trên local hoặc host.
    Public Shared Function SaveFileServerCitad(ByVal sMaCn As String, ByVal pv_fileName As String, ByVal pv_mac As String, _
                                    ByVal pv_header As String, ByVal pv_body As String, ByVal pv_footer As String) As String
        'Dim host As String = String.Empty
        'Dim host_id As String = String.Empty
        'Dim host_pw As String = String.Empty
        'Dim v_fCore As String = String.Empty
        'Dim v_dirRoot As String = String.Empty

        'host = CTuCommon.getDirBy(sMaCn, "HOST2")
        'host_id = CTuCommon.getDirBy(sMaCn, "USERNAME2")
        'host_pw = CTuCommon.getDirBy(sMaCn, "PASSWORD2")
        'host_pw = VBOracleLib.Security.Decrypt3Des(host_pw)
        'v_fCore = CTuCommon.getDirBy(sMaCn, "CITAD")
        'Try
        '    If v_fCore.StartsWith("\\") Then

        '        Dim connectNetwork As String
        '        connectNetwork = BIDVUtil.ConnectNetworkShare.connectToRemote("\\" & host, host_id, host_pw)
        '        v_dirRoot = v_fCore & "\"
        '        If Not Directory.Exists(v_dirRoot) Then
        '            Directory.CreateDirectory(v_dirRoot)
        '        End If
        '        v_dirRoot &= pv_fileName
        '        Using sw As New StreamWriter(v_dirRoot, True)
        '            sw.WriteLine(pv_mac)
        '            sw.WriteLine(pv_header)
        '            sw.WriteLine(pv_body)
        '            sw.WriteLine(pv_footer)
        '            sw.Close()
        '            Return v_dirRoot
        '        End Using
        '        connectNetwork = BIDVUtil.ConnectNetworkShare.disconnectRemote("\\" & host)
        '    Else

        '        v_dirRoot = v_fCore & "\"
        '        If Not Directory.Exists(v_dirRoot) Then
        '            Directory.CreateDirectory(v_dirRoot)
        '        End If
        '        v_dirRoot &= pv_fileName
        '        Using sw As New StreamWriter(v_dirRoot, True)
        '            sw.WriteLine(pv_mac)
        '            sw.WriteLine(pv_header)
        '            sw.WriteLine(pv_body)
        '            sw.WriteLine(pv_footer)
        '            sw.Close()
        '            Return v_dirRoot
        '        End Using
        '    End If

        'Catch ex As Exception
        '    If v_fCore.StartsWith("\\") Then
        '        WriteLog(ex, "Co loi khi CoreFile.SaveFileServerCitad host:" & host & " pv_fileName:" & pv_fileName & " pv_body:" & pv_body)
        '    Else
        '        WriteLog(ex, "Co loi khi CoreFile.SaveFileServerCitad pv_fileName:" & pv_fileName & " pv_body:" & pv_body)
        '    End If

        '    Throw ex
        'End Try
        'Return v_dirRoot
    End Function


End Class
