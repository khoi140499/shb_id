Imports VBOracleLib
Imports System.Data
Imports VBOracleLib.Globals
Imports VBOracleLib.Security

Public Class CTuUser

#Region "  Private Variable"

    Private _MA_NV As String ' Mã NV
    Private _MA_NHOM As String ' Mã nhóm
    Private _HO_TEN As String ' Họ tên
    Private _BAN_LV As String ' Bàn làm việc
    Private _CHUC_DANH As String ' Chức danh
    Private _MAT_KHAU As String 'Mat khau
    Private _TEN_DN As String 'Ten dang nhap
    Private _NGAY_LV As String 'Ngay lam viec
    Private _TELLER_ID As String
    Private _MA_CN As String
    Private _CN_NGANHANG As String
    Private _MA_NH_CN As String
    Private _TEN_NH_CN As String
    Private _USER_DOMAIN As String
#End Region

#Region "  Property of class USER"
    Property CN_NH() As String
        Get
            Return _CN_NGANHANG
        End Get
        Set(ByVal value As String)
            _CN_NGANHANG = value
        End Set
    End Property
    Property Ma_NV() As String
        Get
            Return _MA_NV
        End Get
        Set(ByVal value As String)
            _MA_NV = value
        End Set
    End Property
    Property Ma_Nhom() As String
        Get
            Return _MA_NHOM
        End Get
        Set(ByVal value As String)
            _MA_NHOM = value
        End Set
    End Property
    Property Ho_Ten() As String
        Get
            Return _HO_TEN
        End Get
        Set(ByVal value As String)
            _HO_TEN = value
        End Set
    End Property
    Property Ten_DN() As String
        Get
            Return _TEN_DN
        End Get
        Set(ByVal value As String)
            _TEN_DN = value
        End Set
    End Property
    Property Ban_LV() As String
        Get
            Return _BAN_LV
        End Get
        Set(ByVal value As String)
            _BAN_LV = value
        End Set
    End Property
    Property Ngay_LV() As String
        Get
            Return _NGAY_LV
        End Get
        Set(ByVal value As String)
            _NGAY_LV = value
        End Set
    End Property
    Property Chuc_Danh() As String
        Get
            Return _CHUC_DANH
        End Get
        Set(ByVal value As String)
            _CHUC_DANH = value
        End Set
    End Property

    ReadOnly Property Mat_Khau() As String
        Get
            Return _MAT_KHAU
        End Get
    End Property

    Property TELLER_ID() As String
        Get
            Return _TELLER_ID
        End Get
        Set(ByVal value As String)
            _TELLER_ID = value
        End Set
    End Property

    Property MA_CN() As String
        Get
            Return _MA_CN
        End Get
        Set(ByVal value As String)
            _MA_CN = value
        End Set
    End Property

    Property MA_NH_CN() As String
        Get
            Return _MA_NH_CN
        End Get
        Set(ByVal value As String)
            _MA_NH_CN = value
        End Set
    End Property

    Property TEN_NH_CN() As String
        Get
            Return _TEN_NH_CN
        End Get
        Set(ByVal value As String)
            _TEN_NH_CN = value
        End Set
    End Property
    Property USER_DOMAIN() As String
        Get
            Return _USER_DOMAIN
        End Get
        Set(ByVal value As String)
            _USER_DOMAIN = value
        End Set
    End Property
#End Region

#Region "  Performance of Class USER"
    Sub New()
        _MA_NV = ""
        _MA_NHOM = ""
        _HO_TEN = ""
        _BAN_LV = ""
        _CHUC_DANH = ""
        _TEN_DN = ""
        _NGAY_LV = ""
        _TELLER_ID = ""
        _MA_CN = ""
        _MA_NH_CN = ""
        _TEN_NH_CN = ""
        _USER_DOMAIN = ""
    End Sub

    'Lay thong tin chi tiet cua NSD dua vao ten dang nhap
    Sub New(ByVal strUserID As String)
        Try
            Dim strSQL As String = "SELECT a.ma_nhom,a.ten_dn,d.name ten_CN_NH, a.ten, a.ban_lv,a.chuc_danh,a.mat_khau,a.teller_id,a.ma_cn,a.userdomain,t.giatri_ts ngay_lv,n.ten_nhom" _
                & " FROM tcs_dm_nhanvien a, tcs_thamso t,tcs_dm_chinhanh d,tcs_dm_nhom n " _
                & " WHERE d.id = a.ma_cn and a.ban_lv=t.ma_dthu and a.ma_nhom=n.ma_nhom and t.ten_ts='NGAY_LV' and a.tinh_trang='0' AND a.ma_nv=" & CInt(strUserID)
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                _MA_NV = CInt(strUserID)
                _MA_NHOM = dt.Rows(0)("ma_nhom").ToString()
                _HO_TEN = dt.Rows(0)("ten").ToString()
                _BAN_LV = dt.Rows(0)("ban_lv").ToString()
                _CHUC_DANH = dt.Rows(0)("ten_nhom").ToString()
                _MAT_KHAU = dt.Rows(0)("mat_khau").ToString()
                _TEN_DN = dt.Rows(0)("ten_dn").ToString()
                _NGAY_LV = CTuCommon.ConvertDateToNumber(dt.Rows(0)("ngay_lv").ToString())
                _MA_CN = dt.Rows(0)("ma_cn").ToString()
                _TELLER_ID = dt.Rows(0)("teller_id").ToString()
                _CN_NGANHANG = dt.Rows(0)("ten_CN_NH").ToString()
                _USER_DOMAIN = dt.Rows(0)("userdomain").ToString()
            Else
                _MA_NV = ""
                _MA_NHOM = ""
                _HO_TEN = ""
                _BAN_LV = ""
                _CHUC_DANH = ""
                _MAT_KHAU = ""
                _TEN_DN = ""
                _NGAY_LV = ""
                _TELLER_ID = ""
                _MA_CN = ""
                _USER_DOMAIN = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Public Function CheckPassKS(ByVal strMatKhau As String, ByVal strMa_KS As String) As Boolean
    '    Try
    '        Dim user As CTuUser
    '        user = New CTuUser(strMa_KS)
    '        If strMatKhau.Trim.Length > 0 Then
    '            Dim strUser As String = user.Ten_DN.Trim
    '            Dim strSQL As String = "SELECT MA_NV, TEN, BAN_LV, MAT_KHAU,CHUC_DANH,LOCAL_IP,MA_NHOM,TEN_DN " _
    '                        & " FROM TCS_DM_NHANVIEN  WHERE TEN_DN='" & strUser & "'" _
    '                        & " AND tinh_trang='0' AND mat_khau=" & EscapeQuote(EncryptStr(strMatKhau))
    '            Dim dt As New DataTable
    '            dt = DataAccess.ExecuteToTable(strSQL)
    '            If Not dt Is Nothing And dt.Rows.Count > 0 Then
    '                Return True
    '            Else
    '                Return False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '        Return False
    '    End Try
    'End Function

#End Region

End Class
