﻿Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports System.Text
Imports Business.NewChungTu
Imports System.Xml

Public Class builMsgCTu
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private Shared mNguonNNT As NguonNNT = NguonNNT.Khac
    Public Shared gc_MaQuyMacDinh As String = "01"
    Private Const gc_XML_HEADER_TAG As String = "<?xml version='1.0' encoding='UTF-8'?>"
    Private Const gc_XML_START_CTU_HDR_TAG As String = "<CTU_HDR>"
    Private Const gc_XML_END_CTU_HDR_TAG As String = "</CTU_HDR>"

    Private Const gc_XML_START_CTU_DTL_TAG As String = "<CTU_DTL>"
    Private Const gc_XML_END_CTU_DTL_TAG As String = "</CTU_DTL>"

    Private Const gc_XML_START_NGAYLVV_TAG As String = "<NGAYLV>"
    Private Const gc_XML_END_NGAYLV_TAG As String = "</NGAYLV>"

    Private Const gc_XML_START_SOCT_TAG As String = "<SOCT>"
    Private Const gc_XML_END_SOCT_TAG As String = "</SOCT>"

    Private Const gc_XML_START_SHKB_TAG As String = "<SHKB>"
    Private Const gc_XML_END_SHKB_TAG As String = "</SHKB>"

    Private Const gc_XML_START_TENKB_TAG As String = "<TENKB>"
    Private Const gc_XML_END_TENKB_TAG As String = "</TENKB>"

    Private Const gc_XML_START_CTU_ROOT_TAG As String = "<CTC_CTU>"
    Private Const gc_XML_END_CTU_ROOT_TAG As String = "</CTC_CTU>"
    ' hoapt chuyen từ file clsCTU sang de gọi trong project taxgw
    Public Shared Function TCS_Check_NNThue(ByVal strMaNNT As String) As String
        Try
            Dim strNgayLV As String
            strNgayLV = TCS_GetNgayLV()
            mNguonNNT = ChungTu.buNNThue.Exists_NNThue(strMaNNT)
            Select Case mNguonNNT
                Case NguonNNT.Khac  'Không tìm thấy                            
                    Return String.Empty
                Case NguonNNT.NNThue_CTN
                    Dim v_strRet As String = TCS_CheckDM_NNThue(strMaNNT, False, strNgayLV)
                    If v_strRet.Length > 0 Then
                        Return v_strRet
                    Else
                        Return TCS_CheckSoThue_CTN(strMaNNT)
                    End If
                Case NguonNNT.NNThue_VL
                    Return TCS_CheckDM_NNThue(strMaNNT, True, strNgayLV)
                Case NguonNNT.SoThue_CTN
                    Return TCS_CheckSoThue_CTN(strMaNNT)

                Case NguonNNT.Loi  ' Lỗi
                    Return String.Empty
            End Select

            Return String.Empty
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            Return String.Empty
        End Try
    End Function
    Private Shared Function TCS_CheckSoThue_CTN(ByVal strMaNNT As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable

            Dim dsNNT As DataSet = ChungTu.buNNThue.SoThue_CTN(strMaNNT)
            If (IsEmptyDataSet(dsNNT)) Then Return ""

            mNguonNNT = NguonNNT.SoThue_CTN
            Dim strTemp As String
            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            v_dtHDR.Rows(0)(ColHDR.TKCo) = strFormatTK(GetString(r("tk_thu_ns")))
            ' v_dtHDR.Rows(0)(ColHDR.TenTKCo) = Get_TenTKhoan(GetString(r("tk_thu_ns")))
            If GetString(r("ma_cqthu")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaCQThu) = GetString(r("ma_cqthu"))
                v_dtHDR.Rows(0)(ColHDR.TenCQThu) = Get_TenCQThu(GetString(r("ma_cqthu")))
            End If
            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(GetString(r("ma_xa")))
            End If


            'Nếu chưa có trong danh mục NNThue thì mới lấy thông tin này ra
            'If (Not mblnExistsNNT) Then
            If GetString(r("SHKB")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.SHKB) = GetString(r("SHKB"))
                v_dtHDR.Rows(0)(ColHDR.TenKB) = Get_TenKhoBac(GetString(r("SHKB")))
            End If
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            If GetString(r("dia_chi")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            Else
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = TCS_GetDCNNT(strMaNNT)
            End If

            'End If

            Dim i As Integer
            Dim intMaxRow As Integer

            'Cho phép hiển thị tối đa 4 dòng chi tiết
            If (dsNNT.Tables(0).Rows.Count > 4) Then
                intMaxRow = 4
            Else
                intMaxRow = dsNNT.Tables(0).Rows.Count
            End If

            v_dtDTL = TCS_BuildCTU_DTL_TBL(intMaxRow)

            For i = 0 To intMaxRow - 1
                r = dsNNT.Tables(0).Rows(i)
                v_dtDTL.Rows(i)(ColDTL.MaQuy) = builMsgCTu.gc_MaQuyMacDinh

                strTemp = GetString(r("ma_chuong")).Trim()
                If (strTemp <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.CChuong) = strTemp
                End If


                strTemp = GetString(r("ma_khoan")).Trim()
                If (strTemp <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.LKhoan) = strTemp
                End If

                strTemp = GetString(r("ma_tmuc")).Trim()
                If (strTemp <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = strTemp
                    v_dtDTL.Rows(i)(ColDTL.NoiDung) = Get_TenTieuMuc(strTemp)
                End If

                If (strTemp <> "") Then
                    'grdChiTiet(i + 1, ColDTL.TLDT) = ""
                End If

                Dim strTien As String = GetString(r("so_phainop"))
                If (IsNumeric(strTien)) Then
                    v_dtDTL.Rows(i)(ColDTL.Tien) = Globals.Format_Number(strTien, ".")
                End If

                Dim strKyThue As String = ""
                If GetString(r("kythue")).ToString <> "" Then
                    strKyThue = GetString(r("kythue")).ToString
                Else

                    strKyThue = GetKyThue(builMsgCTu.TCS_GetNgayLV())
                End If
                v_dtDTL.Rows(i)(ColDTL.KyThue) = strKyThue 'ChuanHoa_KyThue(GetString(r("kythue")))

                v_dtDTL.Rows(i)(ColDTL.SoDT) = TCS_CalSoDaThu(strMaNNT, builMsgCTu.gc_MaQuyMacDinh, _
                    GetString(r("ma_chuong")).Trim(), GetString(r("ma_khoan")).Trim(), GetString(r("ma_tmuc")).Trim(), strKyThue)
            Next
            v_dtHDR.Rows(0)(ColHDR.LoaiThue) = "01"
            v_dtHDR.Rows(0)(ColHDR.DescLoaiThue) = Get_TenLoaithue("01")

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
             log.Error(ex.Message & "-" & ex.StackTrace)'
            Return String.Empty
        End Try
    End Function
    Public Shared Function TCS_CheckDM_NNThue(ByVal strMaNNT As String, ByVal fblnVangLai As Boolean, ByVal strNgayLV As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable

            Dim v_strRetXML As String = String.Empty

            Dim dsNNT As DataSet

            If (Not fblnVangLai) Then
                dsNNT = ChungTu.buNNThue.NNThue(strMaNNT)
            Else
                dsNNT = ChungTu.buNNThue.NNT_VL(strMaNNT)
            End If

            If (IsEmptyDataSet(dsNNT)) Then

            End If

            mNguonNNT = NguonNNT.NNThue_CTN

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            If (Not fblnVangLai) Then
                mNguonNNT = NguonNNT.NNThue_CTN
            Else
                mNguonNNT = NguonNNT.NNThue_VL
            End If

            Dim r As DataRow = dsNNT.Tables(0).Rows(0)
            If GetString(r("shkb")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.SHKB) = GetString(r("shkb"))
                v_dtHDR.Rows(0)(ColHDR.TenKB) = Get_TenKhoBac(r("shkb"))
            End If
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))

            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(r("ma_xa"))
            End If
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            If GetString(r("ma_cqthu")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaCQThu) = GetString(r("ma_cqthu"))
                v_dtHDR.Rows(0)(ColHDR.TenCQThu) = Get_TenCQThu(GetString(r("ma_cqthu")))
            End If


            v_dtDTL = TCS_BuildCTU_DTL_TBL(1)

            v_dtDTL.Rows(0)(ColDTL.MaQuy) = builMsgCTu.gc_MaQuyMacDinh
            v_dtDTL.Rows(0)(ColDTL.CChuong) = GetString(r("ma_chuong"))
            v_dtDTL.Rows(0)(ColDTL.LKhoan) = GetString(r("ma_khoan"))
            v_dtDTL.Rows(0)(ColDTL.KyThue) = CTuCommon.GetKyThue(strNgayLV)



            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
             log.Error(ex.Message & "-" & ex.StackTrace)'
            Return String.Empty
        End Try
    End Function
    Private Shared Function TCS_GetDCNNT(ByVal strMa_NNT As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "select dia_chi from tcs_dm_nnt where ma_nnt='" & strMa_NNT & "'"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Shared Function TCS_CalSoDaThu(ByVal strMa_NNT As String, ByVal strMaQuy As String, ByVal strMaChuong As String, _
                                    ByVal strMaKhoan As String, ByVal strMaMuc As String, ByVal strKyThue As String) As String

        'Neu la tai khoan dac biet khong show so da thu
        If strMa_NNT.Trim = "0000000017" Then
            Return "0"
        Else
            Return ChungTu.buChungTu.CTU_CalSoDaThu(strMa_NNT, strMaQuy, strMaChuong, _
                                        strMaKhoan, strMaMuc, strKyThue)
        End If
    End Function
    Public Shared Function Get_TenTieuMuc(ByVal MaTMuc As String) As String
        Dim cnTenMTM As New DataAccess
        Dim drTenMTM As IDataReader
        Dim strSql As String
        Dim strResult As String = ""
        Try
            strSql = "Select TEN From TCS_DM_MUC_TMUC Where MA_TMUC='" & MaTMuc & "' and tinh_trang = '1'"

            drTenMTM = cnTenMTM.ExecuteDataReader(strSql, CommandType.Text)
            If drTenMTM.Read Then
                strResult = Trim(drTenMTM.GetString(0))
            End If
        Catch ex As Exception

        Finally
            If Not drTenMTM Is Nothing Then drTenMTM.Close()
            If Not cnTenMTM Is Nothing Then cnTenMTM.Dispose()
        End Try
        Return strResult

    End Function
    Public Enum ColHDR As Byte
        NgayLV = 0
        SoCT = 1
        So_BT = 2
        SHKB = 3
        TenKB = 4
        MaNNT = 5
        TenNNT = 6
        DChiNNT = 7
        MaNNTien = 8
        TenNNTien = 9
        DChiNNTien = 10
        MaCQThu = 11
        TenCQThu = 12
        MaDBHC = 13
        TenDBHC = 14
        TKNo = 15
        TenTKNo = 16
        TKCo = 17
        TenTKCo = 18
        HTTT = 19
        ' SoCTNH = 20
        TK_KH_NH = 20
        TenTK_KH_NH = 21
        SoDu_KH_NH = 22
        NGAY_KH_NH = 23

        MA_NH_A = 24
        Ten_NH_A = 25
        MA_NH_B = 26
        Ten_NH_B = 27

        LoaiThue = 28
        DescLoaiThue = 29
        MaNT = 30
        TenNT = 31
        'ToKhaiSo = 32
        'NgayDK = 33
        'LHXNK = 34
        ' DescLHXNK = 35
        SoKhung = 32
        SoMay = 33
        So_BK = 34
        Ngay_BK = 35
        TRANG_THAI = 36
        'TT_BDS = 41
        ' TT_NSTT = 42
        PHI_GD = 37
        PHI_VAT = 38
        PT_TINHPHI = 39
        Huyen_NNTIEN = 40
        Tinh_NNTIEN = 41
        TTIEN = 42
        'TT_TTHU = 47
        MA_NV = 43
        'DSToKhai = 52
        'MA_HQ = 48
        'LOAI_TT = 49
        'TEN_HQ = 50
        'MA_HQ_PH = 51
        'TEN_HQ_PH = 52
        MA_NH_TT = 44
        TEN_NH_TT = 45

        MA_KS = 46
    End Enum
    Public Enum ColDTL As Byte
        'ChkRemove = 0
        MaQuy = 0
        CChuong = 1
        LKhoan = 2
        MTieuMuc = 3
        NoiDung = 4
        Tien = 5
        SoDT = 6
        KyThue = 7
    End Enum
#Region "BuildXML Message"
    Private Shared Function TCS_BuildCTU_HDR_TBL() As DataTable
        Try
            Dim v_dtHDR As New DataTable("CTU_HDR")

            v_dtHDR.Columns.Add("NgayLV", GetType(System.String)) '0
            v_dtHDR.Columns.Add("SoCT", GetType(System.String)) '1
            v_dtHDR.Columns.Add("So_BT", GetType(System.String)) '1
            v_dtHDR.Columns.Add("SHKB", GetType(System.String)) '2
            v_dtHDR.Columns.Add("TenKB", GetType(System.String)) '3
            v_dtHDR.Columns.Add("MaNNT", GetType(System.String)) '4
            v_dtHDR.Columns.Add("TenNNT", GetType(System.String)) '5
            v_dtHDR.Columns.Add("DChiNNT", GetType(System.String)) '6
            v_dtHDR.Columns.Add("MaNNTien", GetType(System.String)) '7

            v_dtHDR.Columns.Add("TenNNTien", GetType(System.String)) '8
            v_dtHDR.Columns.Add("DChiNNTien", GetType(System.String)) '9
            v_dtHDR.Columns.Add("MaCQThu", GetType(System.String)) '10
            v_dtHDR.Columns.Add("TenCQThu", GetType(System.String)) '11

            v_dtHDR.Columns.Add("MaDBHC", GetType(System.String)) '12
            v_dtHDR.Columns.Add("TenDBHC", GetType(System.String)) '13
            v_dtHDR.Columns.Add("TKNo", GetType(System.String)) '14
            v_dtHDR.Columns.Add("TenTKNo", GetType(System.String)) '15
            v_dtHDR.Columns.Add("TKCo", GetType(System.String)) '16
            v_dtHDR.Columns.Add("TenTKCo", GetType(System.String)) '17

            v_dtHDR.Columns.Add("HTTT", GetType(System.String)) '18
            'v_dtHDR.Columns.Add("SoCTNH", GetType(System.String)) '19
            v_dtHDR.Columns.Add("TK_KH_NH", GetType(System.String)) '20
            v_dtHDR.Columns.Add("TenTK_KH_NH", GetType(System.String)) '20
            v_dtHDR.Columns.Add("SoDu_KH_NH", GetType(System.String)) '21
            v_dtHDR.Columns.Add("NGAY_KH_NH", GetType(System.String)) '22

            'PHAN THONG TIN NGAN HANG
            v_dtHDR.Columns.Add("MA_NH_A", GetType(System.String)) '20
            v_dtHDR.Columns.Add("Ten_NH_A", GetType(System.String)) '20
            v_dtHDR.Columns.Add("MA_NH_B", GetType(System.String)) '21
            v_dtHDR.Columns.Add("Ten_NH_B", GetType(System.String)) '22

            v_dtHDR.Columns.Add("LoaiThue", GetType(System.String)) '23
            v_dtHDR.Columns.Add("DescLoaiThue", GetType(System.String)) '24
            v_dtHDR.Columns.Add("MaNT", GetType(System.String)) '25
            v_dtHDR.Columns.Add("TenNT", GetType(System.String)) '26
            v_dtHDR.Columns.Add("ToKhaiSo", GetType(System.String)) '27
            v_dtHDR.Columns.Add("NgayDK", GetType(System.String)) '28
            v_dtHDR.Columns.Add("LHXNK", GetType(System.String)) '29
            v_dtHDR.Columns.Add("DescLHXNK", GetType(System.String)) '30
            v_dtHDR.Columns.Add("SoKhung", GetType(System.String)) '31
            v_dtHDR.Columns.Add("SoMay", GetType(System.String)) '32
            v_dtHDR.Columns.Add("So_BK", GetType(System.String)) '33
            v_dtHDR.Columns.Add("Ngay_BK", GetType(System.String)) '34
            v_dtHDR.Columns.Add("TRANG_THAI", GetType(System.String)) '0
            ' v_dtHDR.Columns.Add("TT_BDS", GetType(System.String)) '0
            ' v_dtHDR.Columns.Add("TT_NSTT", GetType(System.String)) '0

            v_dtHDR.Columns.Add("PHI_GD", GetType(System.String)) '0
            v_dtHDR.Columns.Add("PHI_VAT", GetType(System.String)) '0
            v_dtHDR.Columns.Add("PT_TINHPHI", GetType(System.String)) '0
            v_dtHDR.Columns.Add("Huyen_NNTIEN", GetType(System.String)) '0
            v_dtHDR.Columns.Add("Tinh_NNTIEN", GetType(System.String)) '0

            v_dtHDR.Columns.Add("TTIEN", GetType(System.String)) '0
            ' v_dtHDR.Columns.Add("TT_TTHU", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_NV", GetType(System.String)) '0
            'v_dtHDR.Columns.Add("DSToKhai", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_HQ", GetType(System.String)) '0
            v_dtHDR.Columns.Add("LOAI_TT", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TEN_HQ", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_HQ_PH", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TEN_HQ_PH", GetType(System.String)) '0
            'manhnv-10/10/2012
            v_dtHDR.Columns.Add("MA_NH_TT", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TEN_NH_TT", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_KS", GetType(System.String)) '0

            Dim v_drRow1 As DataRow = v_dtHDR.NewRow()
            v_dtHDR.Rows.Add(v_drRow1)
            Return v_dtHDR
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Shared Function TCS_BuildCTU_DTL_TBL(ByVal rownum As Integer) As DataTable
        Try
            Dim v_dtDTL As New DataTable("CTU_DTL")
            'v_dtDTL.Columns.Add("CHK", GetType(System.String)) '0
            v_dtDTL.Columns.Add("MaQuy", GetType(System.String)) '1
            v_dtDTL.Columns.Add("CChuong", GetType(System.String)) '2
            v_dtDTL.Columns.Add("LKhoan", GetType(System.String)) '3
            v_dtDTL.Columns.Add("MTieuMuc", GetType(System.String)) '4
            v_dtDTL.Columns.Add("NoiDung", GetType(System.String)) '5
            v_dtDTL.Columns.Add("Tien", GetType(System.String)) '6
            v_dtDTL.Columns.Add("SoDT", GetType(System.String)) '7
            v_dtDTL.Columns.Add("KyThue", GetType(System.String)) '8
            Dim v_drRow As DataRow
            For i As Integer = 1 To rownum
                v_drRow = v_dtDTL.NewRow()
                v_dtDTL.Rows.Add(v_drRow)
            Next
            Return v_dtDTL
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Shared Function TCS_BuildXML_FROM_TBL(ByVal pv_dtHDR As DataTable, ByVal pv_dtDTL As DataTable) As String
        Try
            Dim v_strRtXML As New StringBuilder
            v_strRtXML.Append(gc_XML_HEADER_TAG)
            v_strRtXML.Append(gc_XML_START_CTU_ROOT_TAG)

            '1)Build hdr chi co 1 dong
            v_strRtXML.Append(gc_XML_START_CTU_HDR_TAG)
            For Each v_dc As DataColumn In pv_dtHDR.Columns
                v_strRtXML.Append("<" & v_dc.ColumnName & ">")
                v_strRtXML.Append(pv_dtHDR.Rows(0)(v_dc.ColumnName).ToString.Replace("&", "&amp;"))
                v_strRtXML.Append("</" & v_dc.ColumnName & ">")
            Next
            v_strRtXML.Append(gc_XML_END_CTU_HDR_TAG)

            v_strRtXML.Append(gc_XML_START_CTU_DTL_TAG)

            ''2)Build dtl co nhieu dong
            For Each v_dr As DataRow In pv_dtDTL.Rows
                v_strRtXML.Append("<ITEMS>")
                For Each v_dc As DataColumn In pv_dtDTL.Columns
                    v_strRtXML.Append("<" & v_dc.ColumnName & ">")
                    v_strRtXML.Append(v_dr(v_dc.ColumnName))
                    v_strRtXML.Append("</" & v_dc.ColumnName & ">")
                Next
                v_strRtXML.Append("</ITEMS>")
            Next
            v_strRtXML.Append(gc_XML_END_CTU_DTL_TAG)

            ''3)Build the error cho biet trang thai error
            v_strRtXML.Append(gc_XML_END_CTU_ROOT_TAG)

            Return v_strRtXML.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Shared Function getDSToKhai(ByVal dt As DataTable) As String
        Try
            Dim strOutput As String = ""
            For i As Integer = 0 To dt.Rows.Count - 1
                strOutput = strOutput & dt.Rows(i)("so_tk").ToString() & ";"
            Next
            Return strOutput
        Catch ex As Exception
            Throw ex
        End Try
    End Function


#End Region
    Public Shared Function TCS_GetDetailsCTU(ByVal pv_strSoCT As String, ByVal pv_strKenhCT As String) As String
        Dim key As New KeyCTu
        'key.Ma_Dthu = TCS_GetMaDT(pv_strMaNV)
        'key.Kyhieu_CT = TCS_GetKHCT(pv_strSHKB)
        'key.Ma_NV = pv_strMaNV
        'If strNgayKB.Length <> 0 Then
        '    key.Ngay_KB = strNgayKB
        'Else
        '    key.Ngay_KB = TCS_GetNgayLV(pv_strMaNV)
        'End If
        'key.SHKB = pv_strSHKB
        'key.So_BT = pv_strSoBT
        'key.So_CT = pv_strSoCT
        Return LoadCTChiTiet(pv_strSoCT, pv_strKenhCT)
    End Function

    Public Shared Function TCS_GetMaDT(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='MA_DT' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function TCS_GetNgayLV(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function LoadCTChiTiet(ByVal pv_strSoCT As String, ByVal pv_strKenhCT As String) As String
        Try
            Dim v_dtHDR As New DataTable
            Dim v_dtDTL As New DataTable
            Dim v_strTrangThaiCT As String = "01"

            Dim s, strTKCo, strTKNo As String
            Dim dsCT As New DataSet
            dsCT = ChungTu.buChungTu.Get_DetailsCTU(pv_strSoCT, pv_strKenhCT)
            Dim iCount As Integer

            If (IsEmptyDataSet(dsCT)) Then Return "Không có dữ liệu"

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            Dim dr As DataRow = dsCT.Tables(0).Rows(0)

            iCount = dsCT.Tables(0).Rows.Count

            v_dtHDR.Rows(0)(ColHDR.TRANG_THAI) = dr("TRANG_THAI").ToString()
            v_strTrangThaiCT = dr("TRANG_THAI").ToString()
            'v_dtHDR.Rows(0)(ColHDR.TT_BDS) = IIf(dr("ISACTIVE").ToString() = "1", "O", "F")
            'v_dtHDR.Rows(0)(ColHDR.TT_NSTT) = IIf(dr("TT_TTHU") <> 0, "Y", "N")
            'v_dtHDR.Rows(0)(ColHDR.TT_TTHU) = dr("TT_TTHU").ToString
            v_dtHDR.Rows(0)(ColHDR.MA_NV) = dr("MA_NV").ToString
            v_dtHDR.Rows(0)(ColHDR.So_BT) = dr("So_BT").ToString()
            v_dtHDR.Rows(0)(ColHDR.SHKB) = dr("SHKB").ToString()
            'v_dtHDR.Rows(0)(ColHDR.TenKB) = dr("TEN_KB").ToString()

            v_dtHDR.Rows(0).Item("MA_HQ") = dr("MA_HQ").ToString()
            v_dtHDR.Rows(0).Item("LOAI_TT") = dr("LOAI_TT").ToString()
            v_dtHDR.Rows(0).Item("TEN_HQ") = dr("TEN_HQ").ToString()
            v_dtHDR.Rows(0).Item("MA_HQ_PH") = dr("MA_HQ_PH").ToString()
            v_dtHDR.Rows(0).Item("TEN_HQ_PH") = dr("TEN_HQ_PH").ToString()

            v_dtHDR.Rows(0)(ColHDR.NgayLV) = ConvertNumbertoString(dr("NGAY_KB").ToString())

            v_dtHDR.Rows(0)(ColHDR.TKCo) = dr("TK_Co").ToString()
            v_dtHDR.Rows(0)(ColHDR.TKNo) = dr("TK_No").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenTKNo) = ""

            strTKCo = dr("TK_Co").ToString()
            strTKNo = dr("TK_No").ToString()

            'Dim intKieuMH, intKieuCT As Integer

            'Get_Map_TK_Form(strTKNo, strTKCo, intKieuMH, intKieuCT)

            'Kiểm tra chuyển khoản tại kho bạc hay ngân hàng?
            'Select Case (Get_Map_KieuCT(intKieuCT))
            '    Case CTEnum.CK_NganHang
            '        mblnNhapThucThu = False
            '    Case CTEnum.CK_KhoBac
            '        mblnNhapThucThu = False
            '        intKieuMH = 1
            '    Case CTEnum.TienMat
            '        mblnNhapThucThu = True
            'End Select

            'mblnBatBuocMLNS = Get_Map_BatBuocMLNS(intKieuCT)

            'Kiểm tra bỏ nhập Tỉ lệ phân chia
            'mblnBatBuocTLDT = Get_Map_BatBuocTLDT(intKieuCT)
            'grdChiTiet.Cols(ColDTL.TLDT).Visible = mblnBatBuocTLDT

            'clsGridCTU.DynamicGrid(grdHeader, intKieuMH)

            'mkeyCT.SHKB = GetString(dr("SHKB"))
            'mkeyCT.Ngay_KB = GetString(dr("ngay_kb"))
            'mkeyCT.Ma_NV = GetString(dr("ma_nv"))
            'mkeyCT.So_BT = GetString(dr("so_bt"))
            'mkeyCT.Ma_Dthu = GetString(dr("ma_dthu"))
            'mkeyCT.So_CT = GetString(dr("so_ct"))
            'mkeyCT.Kyhieu_CT = GetString(dr("kyhieu_ct"))


            'DBHC
            's = dr("MA_XA").ToString()
            'If dr("MA_XA").ToString().Length > 0 Then
            '    v_dtHDR.Rows(0)(ColHDR.MaDBHC) = s
            '    v_dtHDR.Rows(0)(ColHDR.TenDBHC) = dr("TENXA").ToString()
            'End If


            v_dtHDR.Rows(0)(ColHDR.SoCT) = dr("SO_CT").ToString()
            'CType(grdHeader.Rows(RowHeader.SoCT).Cells.Item(1).Controls(0), TextBox).Text = dr("so_ct").ToString()
            'CType(grdHeader.Rows(RowHeader.SoCT).Cells.Item(2).Controls(0), TextBox).Text = dr("kyhieu_ct").ToString()


            Dim v_strMa_NNT As String = dr("MA_NNTHUE").ToString()
            v_dtHDR.Rows(0)(ColHDR.MaNNT) = dr("MA_NNTHUE").ToString()
            'v_dtHDR.Rows(0)(ColHDR.TenNNT) = gc_BLANK_CHARACTER & dr("TEN_NNTHUE").ToString()
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = dr("DC_NNTHUE").ToString()

            v_dtHDR.Rows(0)(ColHDR.MaNNTien) = dr("Ma_NNTien").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenNNTien) = dr("TEN_NNTIEN").ToString()
            v_dtHDR.Rows(0)(ColHDR.DChiNNTien) = dr("DC_NNTIEN").ToString()
            v_dtHDR.Rows(0)(ColHDR.MaNT) = "VND"
            v_dtHDR.Rows(0)(ColHDR.TenNT) = "Việt Nam Đồng"
            v_dtHDR.Rows(0)(ColHDR.TenTKNo) = ""

            Dim strMaLH As String = GetString(dr("LH_XNK").ToString())
            Dim strTenVT, strTenLH As String
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)
            'v_dtHDR.Rows(0)(ColHDR.LHXNK) = strMaLH ' strTenVT
            'v_dtHDR.Rows(0)(ColHDR.DescLHXNK) = strTenLH 'strMaLH & "-" &

            v_dtHDR.Rows(0)(ColHDR.SoKhung) = dr("SO_KHUNG").ToString()
            v_dtHDR.Rows(0)(ColHDR.SoMay) = dr("SO_MAY").ToString()
            'v_dtHDR.Rows(0)(ColHDR.ToKhaiSo) = dr("SO_TK").ToString()
            'v_dtHDR.Rows(0)(ColHDR.NgayDK) = dr("NGAY_TK").ToString()
            v_dtHDR.Rows(0)(ColHDR.So_BK) = dr("So_BK").ToString()
            v_dtHDR.Rows(0)(ColHDR.Ngay_BK) = dr("Ngay_BK").ToString()
            v_dtHDR.Rows(0)(ColHDR.TK_KH_NH) = dr("TK_KH_NH").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenTK_KH_NH) = dr("TEN_KH_NH").ToString()
            v_dtHDR.Rows(0)(ColHDR.NGAY_KH_NH) = dr("NGAY_KH_NH").ToString()

            v_dtHDR.Rows(0)(ColHDR.HTTT) = GetString(dr("PT_TT")).ToString

            v_dtHDR.Rows(0)(ColHDR.MA_NH_A) = GetString(dr("MA_NH_A").ToString())
            v_dtHDR.Rows(0)(ColHDR.MA_NH_B) = GetString(dr("MA_NH_B").ToString())
            v_dtHDR.Rows(0)(ColHDR.MA_NH_TT) = GetString(dr("MA_NH_TT").ToString())
            v_dtHDR.Rows(0)(ColHDR.TEN_NH_TT) = GetString(dr("TEN_NH_TT").ToString())


            v_dtHDR.Rows(0)(ColHDR.PHI_GD) = GetString(dr("PHI_GD").ToString())
            v_dtHDR.Rows(0)(ColHDR.PHI_VAT) = GetString(dr("PHI_VAT").ToString())
            v_dtHDR.Rows(0)(ColHDR.PT_TINHPHI) = GetString(dr("PT_TINHPHI").ToString())
            v_dtHDR.Rows(0)(ColHDR.Huyen_NNTIEN) = dr("QUAN_HUYENNNTIEN").ToString()
            v_dtHDR.Rows(0)(ColHDR.Tinh_NNTIEN) = dr("TINH_TPNNTIEN").ToString()

            v_dtHDR.Rows(0)(ColHDR.MA_KS) = dr("MA_KS").ToString()
            ' Lấy thông tin chi tiết
            Dim dblTien As Double = 0

            Dim dblTongTien As Double = 0

            'dsCT = ChungTu.buChungTu.CTU_ChiTiet_Set(key, blnTHOP)
            dsCT = ChungTu.buChungTu.CTU_Details_Set(dr("SHKB").ToString(), dr("So_BT").ToString(), dr("MA_NV").ToString())

            If (IsEmptyDataSet(dsCT)) Then Return "Không có dữ liệu cbji tiết"


            iCount = dsCT.Tables(0).Rows.Count      'Lấy số bản ghi
            Dim strIDList(iCount - 1) As String

            v_dtDTL = TCS_BuildCTU_DTL_TBL(iCount)
            Dim i As Integer

            'grdChiTiet.Rows.Count = 1
            For i = 0 To iCount - 1
                dr = dsCT.Tables(0).Rows(i)
                strIDList(i) = dr("ID").ToString()

                v_dtDTL.Rows(i)(ColDTL.MaQuy) = GetString(dr("MaQuy").ToString())
                v_dtDTL.Rows(i)(ColDTL.CChuong) = GetString(dr("Ma_Chuong").ToString())
                v_dtDTL.Rows(i)(ColDTL.LKhoan) = GetString(dr("Ma_Khoan").ToString())
                v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = GetString(dr("Ma_TMuc").ToString())
                v_dtDTL.Rows(i)(ColDTL.NoiDung) = GetString(dr("Noi_Dung").ToString())

                s = dr("SoTien_NT").ToString()
                If (IsNumeric(s)) Then
                    dblTien = Convert.ToDouble(s)
                Else
                    dblTien = 0
                End If

                s = dr("TTien").ToString()

                If (IsNumeric(s)) Then
                    dblTien = Convert.ToDouble(s)
                Else
                    dblTien = 0
                End If
                dblTongTien += dblTien

                v_dtDTL.Rows(i)(ColDTL.Tien) = VBOracleLib.Globals.Format_Number(dblTien.ToString, ".")
                v_dtDTL.Rows(i)(ColDTL.KyThue) = dr("Ky_Thue").ToString()
                v_dtDTL.Rows(i)(ColDTL.SoDT) = TCS_CalSoDaThu(v_strMa_NNT, dr("MaQuy").ToString(), dr("Ma_Chuong").ToString(), dr("Ma_Khoan").ToString(), dr("Ma_TMuc").ToString(), dr("Ky_Thue").ToString())
            Next

            'Cap nhat truong tong tien            
            v_dtHDR.Rows(0)(ColHDR.TTIEN) = VBOracleLib.Globals.Format_Number(dblTongTien, ".")

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
              log.Error(ex.Message & "-" & ex.StackTrace)'
            Return String.Empty
        End Try
    End Function
    Public Shared Function XMLToBean(ByVal pv_strXML As String, ByRef hdr As NewChungTu.infChungTuHDR, _
                              ByRef dtls As NewChungTu.infChungTuDTL(), ByRef vitriloi As String, Optional ByVal pv_intMaNV As Integer = 0) As Boolean

        Try
            Dim strValue As String
            Dim dtl As NewChungTu.infChungTuDTL
            Dim dblTTien As Double = 0.0

            Dim v_xmlDocument As New XmlDocument
            Dim v_nodeCTU_HDR As XmlNodeList
            Dim v_nodeCTU_DTL As XmlNodeList
            Dim strXmlRoot As String = "/CTC_CTU/CTU_HDR/"

            v_xmlDocument.LoadXml(pv_strXML.Replace("&", "&amp;"))

            v_nodeCTU_HDR = v_xmlDocument.SelectNodes("/CTC_CTU/CTU_HDR")

            'hứng từ, tạo bộ key mới
            ' Ngay KB
            hdr.Ngay_KB = builMsgCTu.TCS_GetNgayLV()
            ' Mã NV
            If pv_intMaNV <> 0 Then
                hdr.Ma_NV = pv_intMaNV
            Else
                hdr.Ma_NV = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MA_NV).InnerText
            End If
            hdr.Ma_KS = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MA_KS).InnerText
            ' Số bút toán
            hdr.So_BT = CTuCommon.Get_SoBT(builMsgCTu.TCS_GetNgayLV(), hdr.Ma_NV)
            ' Số CT
            ' hdr.So_CT = CTuCommon.Get_SoCT()
            Dim str_nam_kb As String = Mid(hdr.Ngay_KB, 1, 4)
            Dim str_thang_kb As String = Mid(hdr.Ngay_KB, 5, 2)
            '  hdr.So_CT = str_nam_kb + Ma_CN + str_thang_kb + CTuCommon.Get_SoCT()
            ' Mã NV
            ' hdr.Ma_NV = pv_intMaNV
            ' Số bút toán
            '  hdr.So_BT = CTuCommon.Get_SoBT(clsCTU.TCS_GetNgayLV(pv_intMaNV), hdr.Ma_NV)
            ' Mã điểm thu
            'hdr.Ma_DThu = clsCTU.TCS_GetMaDT(pv_intMaNV)
            ' Số CT
            Dim Ma_CN As String
            Dim ten_TK_KH_NK As String
            ten_TK_KH_NK = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TenTK_KH_NH).InnerText
            Ma_CN = TCS_GetMaChiNhanh(hdr.Ma_NV)
            hdr.So_CT = str_nam_kb + Ma_CN + str_thang_kb + CTuCommon.Get_SoCT()
            'Trang thai bds hien tai
            hdr.TT_BDS = "O"

            hdr.TT_NSTT = "N"


            'Khoi tao phan HDR
            If Not v_nodeCTU_HDR Is Nothing Then

                'SHKB
                hdr.SHKB = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.SHKB).InnerText
                If String.IsNullOrEmpty(hdr.SHKB) Then
                    vitriloi = "shkb"
                    Return False
                End If
                ' Mã điểm thu
                hdr.Ma_DThu = builMsgCTu.TCS_GetMaDT_KB(hdr.SHKB)
                ' Mã NNTien
                hdr.KyHieu_CT = builMsgCTu.TCS_GetKHCT(hdr.SHKB)
                hdr.Ma_NNTien = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MaNNTien).InnerText

                ' Tên NNTien
                hdr.Ten_NNTien = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TenNNTien).InnerText

                ' Địa chỉ NNTien
                hdr.DC_NNTien = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.DChiNNTien).InnerText

                ' Mã NNT
                hdr.Ma_NNThue = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MaNNT).InnerText

                ' Tên NNT
                hdr.Ten_NNThue = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TenNNT).InnerText
                ' Địa chỉ NNT
                hdr.DC_NNThue = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.DChiNNT).InnerText

                If (hdr.DC_NNThue = "") Then hdr.DC_NNThue = ""

                ' Lý do
                hdr.Ly_Do = ""
                ' Mã TQ
                hdr.Ma_TQ = 0

                hdr.So_QD = ""
                hdr.Ngay_QD = "null"
                hdr.CQ_QD = ""

                ' Ngay CT
                hdr.Ngay_CT = builMsgCTu.TCS_GetNgayLV()
                ' Ngay HT
                hdr.Ngay_HT = ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") 'GetDBSysDate() '

                hdr.Ma_CQThu = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MaCQThu).InnerText

                ' DBHC
                hdr.Ma_Xa = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MaDBHC).InnerText
                hdr.XA_ID = Get_XaID(hdr.Ma_Xa)
                hdr.Ma_Huyen = ""
                hdr.Ma_Tinh = ""

                ' TK No
                hdr.TK_No = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TKNo).InnerText.Replace(".", "")
                hdr.TEN_TK_NO = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TenTKNo).InnerText
                Dim v_strSHKB As String = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.SHKB).InnerText
                Dim v_strMaDBHC As String = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MaDBHC).InnerText

                ' Dim arrTMP As ArrayList = CTuCommon.MapTK_Load(v_strMaDBHC, v_strSHKB, builMsgCTu.TCS_GetDBThu(pv_intMaNV), builMsgCTu.TCS_GetMaNHKB(pv_intMaNV))
                Dim v_strMaHTTT As String = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.HTTT).InnerText

                ' If v_strMaHTTT = "01" Then 'Chuyen khoan
                hdr.TK_GL_NH = "NULL"
                'Else
                '

                ' TK Co
                hdr.TK_Co = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TKCo).InnerText.Replace(".", "")
                hdr.TEN_TK_CO = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TenTKCo).InnerText
                hdr.Ngay_TK = "null"

                hdr.LH_XNK = ""

                ' DVSDNS
                hdr.DVSDNS = ""
                hdr.Ten_DVSDNS = ""

                ' Ma_NT
                'hdr.Ma_NT = "VND"
                'hdr.Ma_NT = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MaNT).InnerText 
                ' Ti_gia
                'strValue = 'Trim(CType(grdHeader.Rows(RowHeader.TiGia).Cells.Item(1).Controls(0), TextBox).Text)
                ' TG_ID
                ' hdr.TG_ID = "1062"
                'hdr.Ty_Gia = get_TyGia(hdr.Ma_NT,hdr.TG_ID)
                'hdr.Ty_Gia = "1"
                hdr.Ma_NT = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MaNT).InnerText
                hdr.Ty_Gia = ChungTu.buChungTu.get_tygia(hdr.Ma_NT, hdr.TG_ID)
                ' Mã Loại Thuế
                hdr.Ma_LThue = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.LoaiThue).InnerText

                ' So Khung - So May
                hdr.So_Khung = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.SoKhung).InnerText
                hdr.So_May = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.SoMay).InnerText

                'Phuong thuc thanh toan
                hdr.PT_TT = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.HTTT).InnerText
                hdr.Mode = "1"
                hdr.MA_NH_A = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.MA_NH_A).InnerText

                ' hoapt sua ma ngan hang truc tiep gian tiep
                Dim strMaNH As String = GetTT_NH_NHAN(v_strSHKB)
                hdr.MA_NH_B = strMaNH.Split(";")(0).ToString
                hdr.MA_NH_TT = strMaNH.Split(";")(2).ToString

                ' TK_KH_NH 
                hdr.TK_KH_NH = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TK_KH_NH).InnerText.Replace("-", "")
                If String.IsNullOrEmpty(v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TK_KH_NH).InnerText.ToString()) Then
                    vitriloi = "tkkh"
                    Return False
                End If
                hdr.TEN_KH_NH = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TenTK_KH_NH).InnerText

                'Ngay_KH_NH
                strValue = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.NGAY_KH_NH).InnerText
                hdr.NGAY_KH_NH = ToValue(strValue, "DATE")

                'THEM PHI GIAO DỊCH,VAT
                hdr.PHI_GD = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.PHI_GD).InnerText.Replace(".", "")
                hdr.PHI_VAT = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.PHI_VAT).InnerText.Replace(".", "")
                hdr.PT_TINHPHI = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.PT_TINHPHI).InnerText.Replace(".", "")

                ' quận_huyên NNTien
                hdr.QUAN_HUYEN_NNTIEN = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.Huyen_NNTIEN).InnerText
                ' Tinh _NNTien
                hdr.TINH_TPHO_NNTIEN = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.Tinh_NNTIEN).InnerText
                Dim ref_no As String = String.Empty

                hdr.RM_REF_NO = builMsgCTu.GenerateRMRef("", ref_no)
                hdr.REF_NO = ref_no
                ' them kênh chung tu

                ' So BK
                hdr.So_BK = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.So_BK).InnerText

                ' Ngay BK
                If (v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.Ngay_BK).InnerText.Length > 0) Then
                    strValue = v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.Ngay_BK).InnerText
                    If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
                        hdr.Ngay_BK = ToValue(strValue, "DATE")
                    Else
                        hdr.Ngay_BK = "null"
                    End If
                Else
                    hdr.Ngay_BK = "null"
                End If

                hdr.Lan_In = "0" '?????

                'hdr.Ma_KS = 0
                If (hdr.Ma_KS.Length > 0 And v_nodeCTU_HDR.Item(0).ChildNodes(builMsgCTu.ColHDR.TRANG_THAI).InnerText = "01") Then
                    hdr.Trang_Thai = "01" '????
                Else
                    hdr.Trang_Thai = "00" '
                End If

                v_nodeCTU_DTL = v_xmlDocument.SelectNodes("/CTC_CTU/CTU_DTL/ITEMS")
                Dim k As Integer = 0

                'Khoi tao phan DTL
                If Not v_nodeCTU_DTL Is Nothing Then
                    ReDim Preserve dtls(v_nodeCTU_DTL.Count - 1)
                    For i As Integer = 0 To v_nodeCTU_DTL.Count - 1 'v_nodeCTU_DTL.Item(0).ChildNodes.Count - 1
                        dtl = New NewChungTu.infChungTuDTL
                        'For j As Integer = 0 To v_nodeCTU_DTL.Item(i).ChildNodes.Count

                        'So tu tang
                        dtl.ID = getDataKey("TCS_CTU_DTL_SEQ.NEXTVAL")

                        ' SHKB
                        dtl.SHKB = hdr.SHKB

                        ' Ngày KB
                        dtl.Ngay_KB = hdr.Ngay_KB

                        ' Mã NV
                        dtl.Ma_NV = hdr.Ma_NV

                        ' Số Bút toán
                        dtl.So_BT = hdr.So_BT

                        ' Mã điểm thu
                        dtl.Ma_DThu = hdr.Ma_DThu

                        'Mã quỹ
                        dtl.MaQuy = v_nodeCTU_DTL.Item(i).ChildNodes(builMsgCTu.ColDTL.MaQuy).InnerText

                        ' Mã Cấp
                        dtl.Ma_Cap = ""

                        ' Mã Chương
                        dtl.Ma_Chuong = v_nodeCTU_DTL.Item(i).ChildNodes(builMsgCTu.ColDTL.CChuong).InnerText

                        ' CCH_ID
                        dtl.CCH_ID = Get_CCHID(dtl.Ma_Chuong)

                        ' Mã Loại
                        dtl.Ma_Loai = ""

                        ' Mã Khoản
                        dtl.Ma_Khoan = v_nodeCTU_DTL.Item(i).ChildNodes(builMsgCTu.ColDTL.LKhoan).InnerText

                        ' CCH_ID
                        dtl.LKH_ID = Get_LKHID(dtl.Ma_Khoan)

                        ' Mã Mục
                        dtl.Ma_Muc = ""

                        ' Mã TMục
                        dtl.Ma_TMuc = v_nodeCTU_DTL.Item(i).ChildNodes(builMsgCTu.ColDTL.MTieuMuc).InnerText

                        ' MTM_ID
                        dtl.MTM_ID = Get_MTMucID(dtl.Ma_TMuc)

                        ' Mã TLDT
                        dtl.Ma_TLDT = "null"
                        ' DT_ID
                        dtl.DT_ID = "null"

                        '  Nội dung
                        dtl.Noi_Dung = v_nodeCTU_DTL.Item(i).ChildNodes(builMsgCTu.ColDTL.NoiDung).InnerText

                        ' Tiền nguyên tệ
                        strValue = ""
                        If "".Equals(strValue) Then
                            strValue = "0"
                        End If

                        If String.IsNullOrEmpty(v_nodeCTU_DTL.Item(i).ChildNodes(builMsgCTu.ColDTL.Tien).InnerText.ToString()) Then
                            vitriloi = "sotien"
                            Return False
                        End If
                        dtl.SoTien_NT = CDbl(strValue.Replace(" ", ""))
                        ' Tiền 
                        dtl.SoTien = StoN(v_nodeCTU_DTL.Item(i).ChildNodes(builMsgCTu.ColDTL.Tien).InnerText.Replace(".", ""))

                        dblTTien += dtl.SoTien

                        ' Kỳ thuế
                        dtl.Ky_Thue = RemoveApostrophe(v_nodeCTU_DTL.Item(i).ChildNodes(builMsgCTu.ColDTL.KyThue).InnerText)

                        ' Mã dự phòng
                        dtl.Ma_DP = "null"

                        'Next
                        If dtl.SoTien <> 0 Then
                            dtls(k) = dtl 'Kienvt : Chi insert nhung ban ghi co so tien <> 0
                        End If
                        k += 1
                    Next
                End If

                ' Tiền thực thu
                hdr.TTien = dblTTien

                If (hdr.Ma_NT.ToUpper() <> "VND") Then
                    hdr.TTien_NT = hdr.TTien / hdr.Ty_Gia
                Else
                    hdr.TTien_NT = hdr.TTien
                End If

                hdr.Ma_TQ = hdr.Ma_NV
                hdr.TT_TThu = 0
            End If
            Return True
        Catch ex As Exception
           log.Error(ex.Message & "-" & ex.StackTrace)'
        End Try
    End Function
    Public Shared Function TCS_GetNgayLV() As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU='00'"
            Return ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            Return Nothing
        End Try
    End Function
    Public Shared Function GetKyThue(ByVal strNgayLV As String) As String
        Try
            If (strNgayLV.Length <> 8) Then Return ""

            Dim strThang, strNam As String
            strNam = strNgayLV.Substring(0, 4)
            strThang = strNgayLV.Substring(4, 2)

            Return (strThang & "/" & strNam)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        End Try
    End Function
    Public Shared Function TCS_GetMaDT_KB(ByVal strSHKB As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "'"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            Return Nothing
        End Try
    End Function
    Public Shared Function TCS_GetKHCT(ByVal strSHKB As String) As String
        Try
            Dim strSQL As String
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' AND MA_DTHU=(SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "')"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            Return Nothing
        End Try

    End Function
    Public Shared Function GetTT_NH_NHAN(ByVal strSHKB As String) As String
        Dim strResult As String = "null"
        Dim dt As New DataTable
        Dim strSql As String
        Try
            strSql = "Select MA_GIANTIEP, TEN_GIANTIEP, MA_TRUCTIEP, TEN_TRUCTIEP  From TCS_DM_NH_GIANTIEP_TRUCTIEP" & _
                " Where SHKB='" & strSHKB.Trim & "'"
            dt = DataAccess.ExecuteToTable(strSql)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString & ";" & dt.Rows(0)(2).ToString() & ";" & dt.Rows(0)(3).ToString
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
        Finally
            If Not dt Is Nothing Then
                dt.Dispose()
            End If
        End Try
        Return ""
    End Function
    Public Shared Function GenerateRMRef(ByVal pv_strBranchNo As String, ByRef seq_no As String) As String
        Try
            If pv_strBranchNo.Length > 3 Then
                pv_strBranchNo = pv_strBranchNo.Substring(0, 3)
            End If

            Dim v_strTempREF As String = String.Empty
            Dim v_strWorkingDate As String = String.Empty
            Dim v_strAutoID As String = String.Empty
            'Ngay22/09/2012 - ManhNV, KienVT
            'Dim v_strSQL As String = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') NGAY_LV FROM DUAL"
            Dim v_strSQL As String = "SELECT GIATRI_TS NGAY_LV FROM TCS_THAMSO_HT WHERE TEN_TS = 'NGAY_BDS'"
            '****************

            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(v_strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                v_strWorkingDate = dt.Rows(0)(0).ToString
            End If

            v_strSQL = "SELECT TCS_REF_NO_SEQ.NEXTVAL FROM DUAL"
            dt = DatabaseHelp.ExecuteToTable(v_strSQL)
            If Not Globals.IsNullOrEmpty(dt) Then
                v_strAutoID = dt.Rows(0)(0).ToString
            End If
            seq_no = v_strAutoID
            Dim v_lngTempDate As Long = ConvertDateToNumber2(v_strWorkingDate)
            v_strTempREF = pv_strBranchNo & "2" & v_lngTempDate & "9" & Strings.Right("0000" & CStr(v_strAutoID), Len("0000"))
            Return v_strTempREF
        Catch ex As Exception
             log.Error(ex.Message & "-" & ex.StackTrace)'
            Return String.Empty
        End Try
    End Function

    Public Shared Function TCS_GetMaChiNhanh(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "'"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        End Try
    End Function
#Region "Valid _DTL"

    Public Shared Function Valid_DTL(ByVal dtl As NewChungTu.infChungTuDTL, _
                ByVal fblnBatBuocMLNS As Boolean, _
                ByVal fblnVND As Boolean, _
                ByVal fblnBatBuocTLDT As Boolean) As Integer
        Try
            Dim v_intRet As Integer = -1

            If (fblnBatBuocMLNS = True) Then
                If (Not Valid_MaQuy(dtl.MaQuy, True)) Then
                    'lblStatus.Text = "Thông tin 'Mã quỹ' không hợp lệ"
                    Return builMsgCTu.ColDTL.MaQuy
                End If
                If (Not Valid_Chuong(dtl.Ma_Chuong, True)) Then
                    'lblStatus.Text = "Thông tin 'Chương' không hợp lệ"

                    Return builMsgCTu.ColDTL.CChuong
                End If
                If (Not Valid_Khoan(dtl.Ma_Khoan, True)) Then
                    'lblStatus.Text = "Thông tin 'Ngành kinh tế' không hợp lệ"
                    Return builMsgCTu.ColDTL.LKhoan
                End If
                If (Not Valid_TMuc(dtl.Ma_TMuc, True)) Then
                    'lblStatus.Text = "Thông tin 'Nội dung kinh tế' không hợp lệ"

                    Return builMsgCTu.ColDTL.MTieuMuc
                End If
                'If ((Not Valid_TLDT(dtl.Ma_TLDT, True)) And (fblnBatBuocTLDT = True)) Then
                '    lblStatus.Text = "Thông tin Tỷ lệ phân chia không hợp lệ"
                '    Return ColDTL.TLDT
                'End If
            End If

            If (dtl.SoTien = 0) Then
                'lblStatus.Text = "Số tiền không được để 0"
                If (fblnVND) Then
                    Return builMsgCTu.ColDTL.Tien
                    'Else
                    'Return ColDTL.TienNT
                End If
            End If
            'If (Not Valid_KyThue(dtl.Ky_Thue)) Then
            '    'lblStatus.Text = "Thông tin Kỳ thuế không hợp lệ (MM/yyyy)"
            '    Return clsCTU.ColDTL.KyThue
            'End If
            Return -1
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Public Shared Function Valid_DTL(ByVal dtl() As NewChungTu.infChungTuDTL, _
            ByVal fblnBatBuocMLNS As Boolean, _
            ByVal fblnVND As Boolean, _
            ByVal fblnBatBuocTLDT As Boolean) As Integer()
        Dim intReturn(1) As Integer
        Try
            Dim strMaQuy As String = ""
            Dim i, iCol As Integer
            For i = 0 To dtl.Length - 1
                iCol = Valid_DTL(dtl(i), fblnBatBuocMLNS, fblnVND, fblnBatBuocTLDT)
                If (iCol <> -1) Then
                    intReturn(0) = i
                    intReturn(1) = iCol
                    Return intReturn
                End If

                If (strMaQuy <> "") And (strMaQuy <> dtl(i).MaQuy) Then
                    'lblStatus.Text = "Chỉ cho phép 1 mã quỹ trên 1 chứng từ"
                    intReturn(0) = i
                    intReturn(1) = builMsgCTu.ColDTL.MaQuy
                    Return intReturn
                End If

                strMaQuy = dtl(i).MaQuy
            Next
            Return Nothing
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region
End Class
