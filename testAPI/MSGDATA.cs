﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APITEST.Models
{
    public class MSGDATA
    {
        public Header Header;
        public MSGDATA()
        {
            Header = new Header();
        }
    }
    public class Header
    {
        public string MSGTYPE="";
        public string TRANID= Guid.NewGuid().ToString();
        public string TRANDATE="";
        public string ERRCODE="";
        public string ERRMSG="";
        public string REQID = "";
        public Header() { TRANID = Guid.NewGuid().ToString();
        TRANDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
    public class MSG128
    {
        public Header Header;
        public BODYMSG128 Body;
        public MSG128()
        {
            Header = new Header();
            Body = new BODYMSG128();
        }

    }
    public class BODYMSG128
    {
        public string mst = "";
        public string macqthu = "";
        public BODYMSG128() { }
    }
    public class MSG129
    {
        public Header Header;
        public BODYMSG129 Body;
        public MSG129()
        {
            Header = new Header();
            Body = new BODYMSG129();
        }
    }
    public class BODYMSG129
    {
        public string mst = "";
        public string ten_nnt = "";
        public string loai_nnt = "";
        public string so_cmnd = "";
        public string so_cccd = "";
        public string dc_nnthue = "";
        public string huyen_nnthue = "";
        public string tinh_nnthue = "";
        public string tong_ctThue = "";
        public string cnt = "";
        public List<DTLMSG129> LISTDTL;
        public BODYMSG129() { }

    }
    public class DTLMSG129
    {
        public string ma_cqthu = "";
        public string ten_cqthu = "";
        public string shkb = "";
        public string ten_kb = "";
        public string tk_co = "";
        public string ma_nh_th = "";
        public string ten_nh_th = "";
        public string ma_nh_tt = "";
        public string ten_nh_tt = "";
        public string ma_chuong = "";
        public string ma_tmuc = "";
        public string noi_dung = "";
        public string soTien = "";
        public string MA_DBHC = "";
        public string SO_QDINH = "";
        public string NGAY_QDINH = "";
        public DTLMSG129() { }
    }
}
