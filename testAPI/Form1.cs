﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Security;
using System.Security.Cryptography;
using System.Configuration;
namespace testAPI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                API.eTAXAPI svr = new API.eTAXAPI();
                richTextBox2.Text = svr.wsProcess(richTextBox1.Text);
            }
            catch (Exception ex)
            {
                richTextBox2.AppendText(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            WebClient client = new WebClient();
            client.Headers["Content-type"] = "application/json";
            client.Encoding = Encoding.UTF8;

            string json = client.UploadString(textBox1.Text, richTextBox1.Text);
            richTextBox2.Text = json;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(textBox1.Text);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = richTextBox1.Text.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(richTextBox1.Text);
                requestWriter.Close();

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    richTextBox2.Text = response;
                    responseReader.Close();
                }
                catch (Exception ex)
                {

                    richTextBox2.Text = ex.Message;
                }
            }
            catch (Exception ex)
            { }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            TCTObjectIn obj = new TCTObjectIn();
            obj.duLieu = Base64Endcode(richTextBox1.Text);
            obj.signature = new Signature();
            obj.signature.value = "vY5oO66bUTbTXdSrcpvwofHYQfwT2Ofv0uNdpeYtf8OaYN8g8d+dcTAwhGT1/cpFQHz2uDef3SOyEjc++swB6ZoeJDZN5H0IUkQmyifGWEshCa/f6Bi4McBEubld2pJ4ki8ShTPuNvS1Mje0/A5rgqpJ4JDRCqBwLntj7TYByT7e2KzfZY73BUJrBiH4QRvDNRHbqhJRgbqvPmpZu5G8PQ3yrNbCaSYNXrEZYp4Se1YEnELW4XDtvr3fOOxAvFBupF1hm020ka5sWyn89F86WaUm0XJdq74bDK+4KGCm6aCKtA9odCk864AsvRyE7VO6+G62MSO76CejX3vdI1pJUw==";
            obj.signature.publickey = "MIINszCCA9cwggK/oAMCAQICEBvkc4ofPsCPR5+mzzXFmCIwDQYJKoZIhvcNAQEFBQAwfjELMAkGA1UEBhMCVk4xMzAxBgNVBAoTKk1pbmlzdHJ5IG9mIEluZm9ybWF0aW9uIGFuZCBDb21tdW5pY2F0aW9uczEbMBkGA1UECxMSTmF0aW9uYWwgQ0EgQ2VudGVyMR0wGwYDVQQDExRNSUMgTmF0aW9uYWwgUm9vdCBDQTAeFw0wODA1MTYwMTEyNDlaFw00MDA1MTYwMTIwMzJaMH4xCzAJBgNVBAYTAlZOMTMwMQYDVQQKEypNaW5pc3RyeSBvZiBJbmZvcm1hdGlvbiBhbmQgQ29tbXVuaWNhdGlvbnMxGzAZBgNVBAsTEk5hdGlvbmFsIENBIENlbnRlcjEdMBsGA1UEAxMUTUlDIE5hdGlvbmFsIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQChP1lRDv4w/2HblngUi9tDPDa+Ya/ctT9gHtMNAUGt+MmKQo7I7TKGR8X+tffq1kQ3EBLDSAhRULrlV+nONcmOHHMJhTNM4bs4rI6ttxMELmaBVy3M+xr9LpyL/QlA8GAXm3FFbX3j2yrSu5rd68xe5ADf1WwwhZu9V38t1CTTgPvgKFG+sNZhB8zloEfnkW0vh1jotK3BsUaz3FodOAnZ+7UnmJ5e/PH0SP/ooTv/UDPZJquymWKdzHvIUk2eXEKLb3R+/JQT3grS8LVq+JZMUJzmIY7B5ZcBTs2fsEAZfBtZb1w4GZo32tGBORYy74GtsCFrEZfCwQi9byNcBA/TAgMBAAGjUTBPMAsGA1UdDwQEAwIBhjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBTNYnHkYb3+PeyyQGDTgXXdOqxrxjAQBgkrBgEEAYI3FQEEAwIBADANBgkqhkiG9w0BAQUFAAOCAQEATJ3NfiMggBwoz/Dxyd8RyrJpU6PyXwyzZZwvVOEcUCrgVRfEF76ok1FGqobtJGQvTxOOsLQUnHxDvi1OY1Js29ZwF1lHW6Jas8eWerYN9S0N2WQ6r9BUOtETaLb6OA4F6+D8MPdMm1/i7md7+GLqdK9yVEzU+l6L7Ou4J4EsmI73yqBRlnPGF4n59FOXP1fD21UoSXNPSjcqZ4TFE77Gye4JR2K/qLLqfOVfJNs7uHQ1sOGFj4qXSI7neGeJy+WkAuxbV6G5x5a3wenlXY6Zpqclmh2wztY7azPyvknhLdaHgbl2rqf8zbPGdjKvTVBSbC0Nsr3XUOXTdWOfXc0KmTCCBFwwggNEoAMCAQICCmHEtJIAAAAAABMwDQYJKoZIhvcNAQEFBQAwfjELMAkGA1UEBhMCVk4xMzAxBgNVBAoTKk1pbmlzdHJ5IG9mIEluZm9ybWF0aW9uIGFuZCBDb21tdW5pY2F0aW9uczEbMBkGA1UECxMSTmF0aW9uYWwgQ0EgQ2VudGVyMR0wGwYDVQQDExRNSUMgTmF0aW9uYWwgUm9vdCBDQTAeFw0xOTA2MDExMTE2MTVaFw0yNDA2MDExMTI2MTVaMG4xCzAJBgNVBAYTAlZOMRgwFgYDVQQKEw9GUFQgQ29ycG9yYXRpb24xHzAdBgNVBAsTFkZQVCBJbmZvcm1hdGlvbiBTeXN0ZW0xJDAiBgNVBAMTG0ZQVCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAIsNhMe2pcqoFN/QPAd5BKk7g36q2CJ8+KZXRnp/zVFEvEngzj+ffqz8K7D8RVD5ue3SDvsfQj85EqRj9QEzHz4Bheyiz8fgu2RS0josDR1ci2SS/yy4J5I3lC8rhlv/cQeXG8robqun90Hbklq8mr4MukGGWaXKHoeajvvQzpzSZ7uR1GxVdGSeQqaZrt2f8jrqRlRM7ncznXWCY92OjawVvgmc3kzT3UhKua/+EUKpK7Owf4BwnpUJ7saCxVXfWWy1Tl3IF8P7aZXb9+Tn7fp++qt38N5hkqB3Mlz7T6gPsPA8pKUbebaHX+JYrAdjJOZpQV56xmqLCw4TcVOAVmMCAwEAAaOB6zCB6DASBgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQWBBSaphGlCw6MEi0SLfWW1uUXayA8LzAfBgNVHSMEGDAWgBTNYnHkYb3+PeyyQGDTgXXdOqxrxjA8BgNVHR8ENTAzMDGgL6AthitodHRwOi8vcHVibGljLnJvb3RjYS5nb3Yudm4vY3JsL21pY25yY2EuY3JsMEcGCCsGAQUFBwEBBDswOTA3BggrBgEFBQcwAoYraHR0cDovL3B1YmxpYy5yb290Y2EuZ292LnZuL2NydC9taWNucmNhLmNydDALBgNVHQ8EBAMCAYYwDQYJKoZIhvcNAQEFBQADggEBAIr8jHiJry+i+ipvPFpDM5f3lSBPDeHqstYfzC4Wc6+bf1DEL+cX0vx3uHPrDfBoR7J+kxktgKKr7RAu1TzQLWZJQyE7dEe1CkOzMY51XRX/wgg3wNdFgJ2QFUYKwEBwuMYCiSUZx5rnokaDaAmYVvieRIqP6Oa+jcwFG8ra2VT/eyMrZ5XaYG1uEbqMR8dNnH7bF5kWJY25yNz5lMXDT8nQ1JUhWHdExRy0efEOylwgQ1xN95/FWWcMkhpgH0LRoH6FkyefLBad3EF5EJ+anlHkCEJ/vuRrc1sUpCB/v/9VeKtgWTYcaSOsb3TsTD5H+3o43BkbPpD1IODPVASkwRowggV0MIIEXKADAgECAhBUAQEEAY+9h8391qQb+FZJMA0GCSqGSIb3DQEBCwUAMG4xCzAJBgNVBAYTAlZOMRgwFgYDVQQKEw9GUFQgQ29ycG9yYXRpb24xHzAdBgNVBAsTFkZQVCBJbmZvcm1hdGlvbiBTeXN0ZW0xJDAiBgNVBAMTG0ZQVCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0yMDA2MTUwMzU3MzVaFw0yMzA2MTUwMzI3MDBaMIG8MQswCQYDVQQGEwJWTjESMBAGA1UECAwJSMOgIE7hu5lpMRIwEAYDVQQHDAlIw6AgTuG7mWkxHDAaBgNVBAoME1Thu5VuZyBD4bulYyBUaHXhur8xIzAhBgNVBAMMGlThu5VuZyBD4bulYyBUaHXhur8gVGVzdDAxMSIwIAYKCZImiZPyLGQBAQwSTVNUOjAxMDAyMzEyMjYtOTk4MR4wHAYJKoZIhvcNAQkBFg90ZXN0QGZwdC5jb20udm4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDktvsOHAB81szUB8zKLPeg4CbvWLSEsrJeqddZTTekYy2Q75YhLMGywYBKJrmQ3aOfHRCC4vam4pqbYufK38v+kvKKciky7ic5eb2GSKNpGGZciRWlZkmQOfQaQnbHwzJTRDEbkTrs5vLyJR6lJWIuJysJ6Yc9J86Qw54ydexB2fIQCWaA1T/EhEzNbReb3FTST2NI4YBOvYDYD8iBPTP50FybrssCuqxJ+oF3+rxMWyH/uL0oBsfm9dKcHqHzaSY1ke/HAYu3mZ4teK3a3HIN0TQ9DBrINS0Syugp3Xi9nZmYLYZF0XBHjOpRZ4nuCq5cY6tlaQs9XGGzW8WxIZKDAgMBAAGjggG9MIIBuTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFJqmEaULDowSLRIt9ZbW5RdrIDwvMIGoBggrBgEFBQcBAQSBmzCBmDA3BggrBgEFBQcwAoYraHR0cDovL3B1YmxpYy5yb290Y2EuZ292LnZuL2NydC9taWNucmNhLmNydDA4BggrBgEFBQcwAoYsaHR0cDovL2RpY2h2dWRpZW50dS5mcHQuY29tLnZuL2NydC9mcHRjYS5jcnQwIwYIKwYBBQUHMAGGF2h0dHA6Ly9vY3NwMi5maXMuY29tLnZuME8GA1UdIARIMEYwRAYLKwYBBAGB7QMBBAEwNTAzBggrBgEFBQcCARYnaHR0cDovL2RpY2h2dWRpZW50dS5mcHQuY29tLnZuL2Nwcy5odG1sMDQGA1UdJQQtMCsGCCsGAQUFBwMCBggrBgEFBQcDBAYKKwYBBAGCNwoDDAYJKoZIhvcvAQEFMCcGA1UdHwQgMB4wHKAaoBiGFmh0dHA6Ly9jcmwyLmZpcy5jb20udm4wHQYDVR0OBBYEFIfsaUQMk8OsBbuQMI4lsaQTh4q5MA4GA1UdDwEB/wQEAwIE8DANBgkqhkiG9w0BAQsFAAOCAQEAbrL7Bubrw8ut8wSdDiX9xFigMJwQK8TTKVgAllETSiLKTVSJ0FYldwkGN3jcEOlNRE1msGc14hHRiABzmCpO8EJAwIbvuwNQ5c2ZoyaRuW0uyUw9IraTWbweWHhdBaJtLrYLcQR0g9DUO6LPGXGCx9F5zaWJp9UuKY1mUCsiq6sZvHrC1ReO2NCRu3m5nZqS3qQcSNZTH10uNyOWieLJ5MtlHqcxIignYqW1JgWrqSns1XgKVnuDsehrbaEoXBJNSxCxwdphuzGPlX2vgExHQ3vYXqkAn8zhvMBgvCExUlYTJ3bLAKHD6BTHmpzsSC09yqiNGoKbhjXc7PuL23TgFQ==";
            richTextBox2.Text = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }

        public static string Base64Endcode(string strData)
        {
            var base64EncodedBytes = System.Text.Encoding.UTF8.GetBytes(strData);
            //System.Convert.FromBase64String(base64EncodedData);
            return System.Convert.ToBase64String(base64EncodedBytes);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //string jsonStr = "{\r\n"
            //            + "    \"maDoiTac\": \"0100112437\",\r\n"
            //            + "    \"maThamChieu\": \"11020220003630371\",\r\n"
            //            + "    \"soTien\": 2676,\r\n"
            //            + "    \"phiChuyenPhat\": 0,\r\n"
            //            + "    \"ngonNgu\": \"vi-VN\",\r\n"
            //            + "    \"maTienTe\": \"VND\",\r\n"
            //            + "    \"thongTinGiaoDich\": \"Thanh toan thue khac_ma so thue 0316364839\",\r\n"
            //            + "    \"thoiGianGD\": \"20220507092519\",\r\n"
            //            + "    \"thongTinBienLai\": {\r\n"
            //            + "        \"tkthuHuong\": \"7111\",\r\n"
            //            + "        \"phiLePhi\": [\r\n"
            //            + "            {\r\n"
            //            + "                \"soTien\": 892,\r\n"
            //            + "                \"loaiPhiLePhi\": \"+++++\",\r\n"
            //            + "                \"maPhiLePhi\": \"+757+4917++\",\r\n"
            //            + "                \"tenPhiLePhi\": \"Thuế thu nhập từ hoạt động sản xuất, kinh doanh của cá nhân\"\r\n"
            //            + "            },\r\n"
            //            + "            {\r\n"
            //            + "                \"soTien\": 1784,\r\n"
            //            + "                \"loaiPhiLePhi\": \"+++++\",\r\n"
            //            + "                \"maPhiLePhi\": \"+757+4931++\",\r\n"
            //            + "                \"tenPhiLePhi\": \"Thuế giá trị gia tăng hàng sản xuất, kinh doanh trong nước (gồm cả dịch vụ trong lĩnh vực dầu khí)\"\r\n"
            //            + "            }\r\n"
            //            + "        ],\r\n"
            //            + "        \"maHoSo\": null,\r\n"
            //            + "        \"maLoaiHinhThuPhat\": \"ETAX_TCT\",\r\n"
            //            + "        \"maLoaiHinhThue\": \"03\",\r\n"
            //            + "        \"tenLoaiHinhThue\": \"Thue ca nhan\",\r\n"
            //            + "        \"mst\": \"0316364839\",\r\n"
            //            + "        \"hoTenNguoiNop\": \"Trần Thị Huyền\",\r\n"
            //            + "        \"soCMNDNguoiNop\": \"\",\r\n"
            //            + "        \"diaChiNguoiNop\": \"57 Huỳnh Thúc Kháng, Láng Hạ, Đống Đa, Hà Nội\",\r\n"
            //            + "        \"huyenNguoiNop\": \"\",\r\n"
            //            + "        \"tinhNguoiNop\": \"\",\r\n"
            //            + "        \"maCoQuanQD\": \"1056283\",\r\n"
            //            + "        \"tenCoQuanQD\": \"\",\r\n"
            //            + "        \"khoBac\": \"0112\",\r\n"
            //            + "        \"ngayQD\": \"\",\r\n"
            //            + "        \"soQD\": \"\",\r\n"
            //            + "        \"maDichVu\": \"90\",\r\n"
            //            + "        \"dskhoanNop\": [\r\n"
            //            + "            {\r\n"
            //            + "                \"soTien\": 2676,\r\n"
            //            + "                \"noiDung\": \"Thuế thu nhập từ hoạt động sản xuất, kinh doanh của cá nhân+Thuế giá trị gia tăng hàng sản xuất, kinh doanh trong nước (gồm cả dịch vụ trong lĩnh vực dầu khí)\"\r\n"
            //            + "            }\r\n"
            //            + "        ]\r\n"
            //            + "    },\r\n"
            //            + "    \"maNganHang\": \"BIDV\",\r\n"
            //            + "	\"phuongThuc\": \"Account\",\r\n"
            //            + "	\"soTaiKhoan_The\": \"120xxx2747\",\r\n"
            //            + "    \"ewalletToken\":\"abc12344556788\"\r\n"
            //            + "}";
            THANHTOANMOBI_IN vIn = JsonConvert.DeserializeObject<THANHTOANMOBI_IN>(richTextBox1.Text);
            richTextBox2.Text = JsonConvert.SerializeObject(vIn);

        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(textBox1.Text + "/XACTHUCNNT");
                request.Method = "POST";
                request.ContentType = "application/json";
                string strDATA = GetMSG(richTextBox1.Text);
                request.ContentLength = strDATA.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(strDATA);
                requestWriter.Close();

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    richTextBox2.Text = response;
                    responseReader.Close();
                }
                catch (Exception ex)
                {

                    richTextBox2.Text = ex.Message;
                }
            }
            catch (Exception ex)
            { }
        }
        private static string GetMSG(string str)
        {
            TCTObjectIn obj = new TCTObjectIn();
            obj.duLieu = Base64Endcode(str);
            obj.signature = new Signature();
            obj.signature.value = "vY5oO66bUTbTXdSrcpvwofHYQfwT2Ofv0uNdpeYtf8OaYN8g8d+dcTAwhGT1/cpFQHz2uDef3SOyEjc++swB6ZoeJDZN5H0IUkQmyifGWEshCa/f6Bi4McBEubld2pJ4ki8ShTPuNvS1Mje0/A5rgqpJ4JDRCqBwLntj7TYByT7e2KzfZY73BUJrBiH4QRvDNRHbqhJRgbqvPmpZu5G8PQ3yrNbCaSYNXrEZYp4Se1YEnELW4XDtvr3fOOxAvFBupF1hm020ka5sWyn89F86WaUm0XJdq74bDK+4KGCm6aCKtA9odCk864AsvRyE7VO6+G62MSO76CejX3vdI1pJUw==";
            obj.signature.publickey = "MIINszCCA9cwggK/oAMCAQICEBvkc4ofPsCPR5+mzzXFmCIwDQYJKoZIhvcNAQEFBQAwfjELMAkGA1UEBhMCVk4xMzAxBgNVBAoTKk1pbmlzdHJ5IG9mIEluZm9ybWF0aW9uIGFuZCBDb21tdW5pY2F0aW9uczEbMBkGA1UECxMSTmF0aW9uYWwgQ0EgQ2VudGVyMR0wGwYDVQQDExRNSUMgTmF0aW9uYWwgUm9vdCBDQTAeFw0wODA1MTYwMTEyNDlaFw00MDA1MTYwMTIwMzJaMH4xCzAJBgNVBAYTAlZOMTMwMQYDVQQKEypNaW5pc3RyeSBvZiBJbmZvcm1hdGlvbiBhbmQgQ29tbXVuaWNhdGlvbnMxGzAZBgNVBAsTEk5hdGlvbmFsIENBIENlbnRlcjEdMBsGA1UEAxMUTUlDIE5hdGlvbmFsIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQChP1lRDv4w/2HblngUi9tDPDa+Ya/ctT9gHtMNAUGt+MmKQo7I7TKGR8X+tffq1kQ3EBLDSAhRULrlV+nONcmOHHMJhTNM4bs4rI6ttxMELmaBVy3M+xr9LpyL/QlA8GAXm3FFbX3j2yrSu5rd68xe5ADf1WwwhZu9V38t1CTTgPvgKFG+sNZhB8zloEfnkW0vh1jotK3BsUaz3FodOAnZ+7UnmJ5e/PH0SP/ooTv/UDPZJquymWKdzHvIUk2eXEKLb3R+/JQT3grS8LVq+JZMUJzmIY7B5ZcBTs2fsEAZfBtZb1w4GZo32tGBORYy74GtsCFrEZfCwQi9byNcBA/TAgMBAAGjUTBPMAsGA1UdDwQEAwIBhjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBTNYnHkYb3+PeyyQGDTgXXdOqxrxjAQBgkrBgEEAYI3FQEEAwIBADANBgkqhkiG9w0BAQUFAAOCAQEATJ3NfiMggBwoz/Dxyd8RyrJpU6PyXwyzZZwvVOEcUCrgVRfEF76ok1FGqobtJGQvTxOOsLQUnHxDvi1OY1Js29ZwF1lHW6Jas8eWerYN9S0N2WQ6r9BUOtETaLb6OA4F6+D8MPdMm1/i7md7+GLqdK9yVEzU+l6L7Ou4J4EsmI73yqBRlnPGF4n59FOXP1fD21UoSXNPSjcqZ4TFE77Gye4JR2K/qLLqfOVfJNs7uHQ1sOGFj4qXSI7neGeJy+WkAuxbV6G5x5a3wenlXY6Zpqclmh2wztY7azPyvknhLdaHgbl2rqf8zbPGdjKvTVBSbC0Nsr3XUOXTdWOfXc0KmTCCBFwwggNEoAMCAQICCmHEtJIAAAAAABMwDQYJKoZIhvcNAQEFBQAwfjELMAkGA1UEBhMCVk4xMzAxBgNVBAoTKk1pbmlzdHJ5IG9mIEluZm9ybWF0aW9uIGFuZCBDb21tdW5pY2F0aW9uczEbMBkGA1UECxMSTmF0aW9uYWwgQ0EgQ2VudGVyMR0wGwYDVQQDExRNSUMgTmF0aW9uYWwgUm9vdCBDQTAeFw0xOTA2MDExMTE2MTVaFw0yNDA2MDExMTI2MTVaMG4xCzAJBgNVBAYTAlZOMRgwFgYDVQQKEw9GUFQgQ29ycG9yYXRpb24xHzAdBgNVBAsTFkZQVCBJbmZvcm1hdGlvbiBTeXN0ZW0xJDAiBgNVBAMTG0ZQVCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAIsNhMe2pcqoFN/QPAd5BKk7g36q2CJ8+KZXRnp/zVFEvEngzj+ffqz8K7D8RVD5ue3SDvsfQj85EqRj9QEzHz4Bheyiz8fgu2RS0josDR1ci2SS/yy4J5I3lC8rhlv/cQeXG8robqun90Hbklq8mr4MukGGWaXKHoeajvvQzpzSZ7uR1GxVdGSeQqaZrt2f8jrqRlRM7ncznXWCY92OjawVvgmc3kzT3UhKua/+EUKpK7Owf4BwnpUJ7saCxVXfWWy1Tl3IF8P7aZXb9+Tn7fp++qt38N5hkqB3Mlz7T6gPsPA8pKUbebaHX+JYrAdjJOZpQV56xmqLCw4TcVOAVmMCAwEAAaOB6zCB6DASBgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQWBBSaphGlCw6MEi0SLfWW1uUXayA8LzAfBgNVHSMEGDAWgBTNYnHkYb3+PeyyQGDTgXXdOqxrxjA8BgNVHR8ENTAzMDGgL6AthitodHRwOi8vcHVibGljLnJvb3RjYS5nb3Yudm4vY3JsL21pY25yY2EuY3JsMEcGCCsGAQUFBwEBBDswOTA3BggrBgEFBQcwAoYraHR0cDovL3B1YmxpYy5yb290Y2EuZ292LnZuL2NydC9taWNucmNhLmNydDALBgNVHQ8EBAMCAYYwDQYJKoZIhvcNAQEFBQADggEBAIr8jHiJry+i+ipvPFpDM5f3lSBPDeHqstYfzC4Wc6+bf1DEL+cX0vx3uHPrDfBoR7J+kxktgKKr7RAu1TzQLWZJQyE7dEe1CkOzMY51XRX/wgg3wNdFgJ2QFUYKwEBwuMYCiSUZx5rnokaDaAmYVvieRIqP6Oa+jcwFG8ra2VT/eyMrZ5XaYG1uEbqMR8dNnH7bF5kWJY25yNz5lMXDT8nQ1JUhWHdExRy0efEOylwgQ1xN95/FWWcMkhpgH0LRoH6FkyefLBad3EF5EJ+anlHkCEJ/vuRrc1sUpCB/v/9VeKtgWTYcaSOsb3TsTD5H+3o43BkbPpD1IODPVASkwRowggV0MIIEXKADAgECAhBUAQEEAY+9h8391qQb+FZJMA0GCSqGSIb3DQEBCwUAMG4xCzAJBgNVBAYTAlZOMRgwFgYDVQQKEw9GUFQgQ29ycG9yYXRpb24xHzAdBgNVBAsTFkZQVCBJbmZvcm1hdGlvbiBTeXN0ZW0xJDAiBgNVBAMTG0ZQVCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0yMDA2MTUwMzU3MzVaFw0yMzA2MTUwMzI3MDBaMIG8MQswCQYDVQQGEwJWTjESMBAGA1UECAwJSMOgIE7hu5lpMRIwEAYDVQQHDAlIw6AgTuG7mWkxHDAaBgNVBAoME1Thu5VuZyBD4bulYyBUaHXhur8xIzAhBgNVBAMMGlThu5VuZyBD4bulYyBUaHXhur8gVGVzdDAxMSIwIAYKCZImiZPyLGQBAQwSTVNUOjAxMDAyMzEyMjYtOTk4MR4wHAYJKoZIhvcNAQkBFg90ZXN0QGZwdC5jb20udm4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDktvsOHAB81szUB8zKLPeg4CbvWLSEsrJeqddZTTekYy2Q75YhLMGywYBKJrmQ3aOfHRCC4vam4pqbYufK38v+kvKKciky7ic5eb2GSKNpGGZciRWlZkmQOfQaQnbHwzJTRDEbkTrs5vLyJR6lJWIuJysJ6Yc9J86Qw54ydexB2fIQCWaA1T/EhEzNbReb3FTST2NI4YBOvYDYD8iBPTP50FybrssCuqxJ+oF3+rxMWyH/uL0oBsfm9dKcHqHzaSY1ke/HAYu3mZ4teK3a3HIN0TQ9DBrINS0Syugp3Xi9nZmYLYZF0XBHjOpRZ4nuCq5cY6tlaQs9XGGzW8WxIZKDAgMBAAGjggG9MIIBuTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFJqmEaULDowSLRIt9ZbW5RdrIDwvMIGoBggrBgEFBQcBAQSBmzCBmDA3BggrBgEFBQcwAoYraHR0cDovL3B1YmxpYy5yb290Y2EuZ292LnZuL2NydC9taWNucmNhLmNydDA4BggrBgEFBQcwAoYsaHR0cDovL2RpY2h2dWRpZW50dS5mcHQuY29tLnZuL2NydC9mcHRjYS5jcnQwIwYIKwYBBQUHMAGGF2h0dHA6Ly9vY3NwMi5maXMuY29tLnZuME8GA1UdIARIMEYwRAYLKwYBBAGB7QMBBAEwNTAzBggrBgEFBQcCARYnaHR0cDovL2RpY2h2dWRpZW50dS5mcHQuY29tLnZuL2Nwcy5odG1sMDQGA1UdJQQtMCsGCCsGAQUFBwMCBggrBgEFBQcDBAYKKwYBBAGCNwoDDAYJKoZIhvcvAQEFMCcGA1UdHwQgMB4wHKAaoBiGFmh0dHA6Ly9jcmwyLmZpcy5jb20udm4wHQYDVR0OBBYEFIfsaUQMk8OsBbuQMI4lsaQTh4q5MA4GA1UdDwEB/wQEAwIE8DANBgkqhkiG9w0BAQsFAAOCAQEAbrL7Bubrw8ut8wSdDiX9xFigMJwQK8TTKVgAllETSiLKTVSJ0FYldwkGN3jcEOlNRE1msGc14hHRiABzmCpO8EJAwIbvuwNQ5c2ZoyaRuW0uyUw9IraTWbweWHhdBaJtLrYLcQR0g9DUO6LPGXGCx9F5zaWJp9UuKY1mUCsiq6sZvHrC1ReO2NCRu3m5nZqS3qQcSNZTH10uNyOWieLJ5MtlHqcxIignYqW1JgWrqSns1XgKVnuDsehrbaEoXBJNSxCxwdphuzGPlX2vgExHQ3vYXqkAn8zhvMBgvCExUlYTJ3bLAKHD6BTHmpzsSC09yqiNGoKbhjXc7PuL23TgFQ==";
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(textBox1.Text + "/LKTAIKHOAN");
                request.Method = "POST";
                request.ContentType = "application/json";
                string strDATA = GetMSG(richTextBox1.Text);
                request.ContentLength = strDATA.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(strDATA);
                requestWriter.Close();

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    richTextBox2.Text = response;
                    responseReader.Close();
                }
                catch (Exception ex)
                {

                    richTextBox2.Text = ex.Message;
                }
            }
            catch (Exception ex)
            { }
        }
        //thanh toán
        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(textBox1.Text + "/THANHTOANMOBITCT");
                request.Method = "POST";
                request.ContentType = "application/json";
                string strDATA = GetMSG(richTextBox1.Text);
                request.ContentLength = strDATA.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(strDATA);
                requestWriter.Close();

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    richTextBox2.Text = response;
                    responseReader.Close();
                }
                catch (Exception ex)
                {

                    richTextBox2.Text = ex.Message;
                }
            }
            catch (Exception ex)
            { }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(textBox1.Text + "/APIXACTHUCOTP");
                request.Method = "POST";
                request.ContentType = "application/json";
                string strDATA = GetMSG(richTextBox1.Text);
                request.ContentLength = strDATA.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(strDATA);
                requestWriter.Close();

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    richTextBox2.Text = response;
                    responseReader.Close();
                }
                catch (Exception ex)
                {

                    richTextBox2.Text = ex.Message;
                }
            }
            catch (Exception ex)
            { }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(textBox1.Text + "/APICAPNHATTRANGTHAI");
                request.Method = "POST";
                request.ContentType = "application/json";
                string strDATA = GetMSG(richTextBox1.Text);
                request.ContentLength = strDATA.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(strDATA);
                requestWriter.Close();

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    richTextBox2.Text = response;
                    responseReader.Close();
                }
                catch (Exception ex)
                {

                    richTextBox2.Text = ex.Message;
                }
            }
            catch (Exception ex)
            { }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = System.Configuration.ConfigurationSettings.AppSettings.Get("URL_API");
        }

        //in chứng từ
        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(textBox1.Text + "/APIINCTU");
                request.Method = "POST";
                request.ContentType = "application/json";
                INCTUMOBITCT_IN obj = Newtonsoft.Json.JsonConvert.DeserializeObject<INCTUMOBITCT_IN>(richTextBox1.Text);
                //obj.MaXacThuc = Base64EncodeSHA256(obj.MaDoiTac + obj.MaThamChieu + obj.ThoiGianGD + obj.MaXacThuc);
                //string strDATA = GetMSG(Newtonsoft.Json.JsonConvert.SerializeObject(obj));

                string strMaBiMat1 = ConfigurationManager.AppSettings["MOBITCT.PASSWORD"];
                string strMaBiMatKey = ConfigurationManager.AppSettings["MOBITCT.KEY"];
                string strMaBiMat = VBOracleLib.Security.DescryptStr(strMaBiMat1, strMaBiMatKey, 1);

                //ghep chuoi bi mat de ma hoa
                string strTem = obj.MaDoiTac + "|" + obj.MaThamChieu + "|" + obj.ThoiGianGD + "|" + strMaBiMat;
                string strTemBase = VBOracleLib.Security.Base64EncodeSHA256(strTem);
                obj.MaXacThuc = strTemBase;

                string strDATA = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                request.ContentLength = strDATA.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(strDATA);
                requestWriter.Close();
                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    richTextBox2.Text = response;
                    responseReader.Close();
                }
                catch (Exception ex)
                {

                    richTextBox2.Text = ex.Message;
                }
            }
            catch (Exception ex)
            { richTextBox2.Text = ex.Message; }
        }
        public static string Base64EncodeSHA256(string plainText)
        {
            try
            {
                if (plainText.Length > 0)
                {
                    SHA256 mySHA256 = SHA256Managed.Create();

                    byte[] plainTextBytes = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(plainText));
                    return Convert.ToBase64String(plainTextBytes);
                }
            }
            catch (Exception ex)
            {
            }
            return "";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(textBox1.Text);
                request.Method = "POST";
                request.ContentType = "application/json";
                APITEST.Models.MSG128 obj128 = new APITEST.Models.MSG128();

                obj128.Header.MSGTYPE = "128";
                obj128.Header.TRANDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                obj128.Body.mst = richTextBox1.Text;
                string strDATA = richTextBox1.Text;// Newtonsoft.Json.JsonConvert.SerializeObject(obj128);// richTextBox1.Text;
                // richTextBox1.Text = strDATA;
                request.ContentLength = strDATA.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(strDATA);
                requestWriter.Close();
                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    richTextBox2.Text = response;
                    responseReader.Close();
                }
                catch (Exception ex)
                {

                    richTextBox2.Text = ex.Message;
                }
            }
            catch (Exception ex)
            { richTextBox2.Text = ex.Message; }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        private void bEncodeBase64_Click(object sender, EventArgs e)
        {
            string a = richTextBox1.Text.Trim();
            richTextBox2.Text = Base64Encode(a);
        }

        private void bDecodeBase64_Click(object sender, EventArgs e)
        {
            string a = richTextBox1.Text.Trim();
            richTextBox2.Text = Base64Decode(a);
        }

    }
    public class TCTObjectIn
    {
        public string duLieu { get; set; }
        public Signature signature;
    }
    public class Signature
    {
        public string value { get; set; }
        public string publickey { get; set; }
    }
    public class INCTUMOBITCT_IN
    {
        public string MaDoiTac { get; set; }
        public string MaThamChieu { get; set; }
        public string ThoiGianGD { get; set; }
        public string MaXacThuc { get; set; }
    }
}
