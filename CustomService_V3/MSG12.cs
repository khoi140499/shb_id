﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG12
{
    [Serializable]
    [XmlRootAttribute("CUSTOMS", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG12
    {
        public MSG12()
        {
        }
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Security")]
        public Security Security; 

        public MSG12 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG12.MSG12));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG12.MSG12 LoadedObjTmp = (CustomsV3.MSG.MSG12.MSG12)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSG12toXML(MSG12 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG12));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

    public class Data
    {
        public Data()
        {
        }
        [XmlElement("Item")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Ma_Cuc { get; set; }
        public string Ten_Cuc { get; set; }
        public string Ma_HQ_PH { get; set; }
        public string Ma_HQ_CQT { get; set; }
        public string Ten_HQ_PH { get; set; }
        public string Ma_DV { get; set; }
        public string Ma_Chuong { get; set; }
        public string Ten_DV { get; set; }
        public string Ma_HQ { get; set; }
        public string Ten_HQ { get; set; }
        public string Ma_LH { get; set; }
        public string Ten_LH { get; set; }
        public string Nam_DK { get; set; }
        public string So_TK { get; set; }
        public string Ma_NTK { get; set; }
        public string Ma_LT { get; set; }
        public string Ten_NTK { get; set; }
        public string Ma_HTVCHH { get; set; }
        public string Ten_HTVCHH { get; set; }
        public string Ngay_DK { get; set; }
        public string Ma_KB { get; set; }
        public string Ten_KB { get; set; }
        public string TKKB { get; set; }
        public string TTNo { get; set; }
        public string Ten_TTN_VT { get; set; }
        public string Ten_TTN { get; set; }
        public string TTNo_CT { get; set; }
        public string Khoan_XK { get; set; }
        public string TieuMuc_XK { get; set; }
        public string DuNo_XK { get; set; }
        public string Khoan_NK { get; set; }
        public string TieuMuc_NK { get; set; }
        public string DuNo_NK { get; set; }
        public string Khoan_VA { get; set; }
        public string TieuMuc_VA { get; set; }
        public string DuNo_VA { get; set; }
        public string Khoan_TD { get; set; }
        public string TieuMuc_TD { get; set; }
        public string DuNo_TD { get; set; }
        public string Khoan_TV { get; set; }
        public string TieuMuc_TV { get; set; }
        public string DuNo_TV { get; set; }
        public string Khoan_MT { get; set; }
        public string TieuMuc_MT { get; set; }
        public string DuNo_MT { get; set; }
        public string Khoan_KH { get; set; }
        public string TieuMuc_KH { get; set; }
        public string DuNo_KH { get; set; }
        public string DuNo_TO { get; set; }
    }

    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Request_ID { get; set; }
    }
    public class Security
    {
        public Security()
        {
        }
        public string Signature { get; set; }
    }
    public class Error
    {
        public string Error_Number { get; set; }
        public string Error_Message { get; set; }
    }
}
