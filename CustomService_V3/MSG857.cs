﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.MSGError;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGHeader;

namespace CustomsV3.MSG.MSG857
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG857
    {
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("DigitalSignatures")]
        public DigitalSignatures DigitalSignatures;
        public MSG857()
        {
           
        }
        public string MSG857toXML(MSG857 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG857));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }

        public MSG857 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG857.MSG857));


            XmlReader reader = XmlReader.Create(new StringReader(p_XML));
            CustomsV3.MSG.MSG857.MSG857 LoadedObjTmp = (CustomsV3.MSG.MSG857.MSG857)serializer.Deserialize(reader);
            return LoadedObjTmp;


        }

     
    }
     public class Data
    {
        public Data()
        {
           
        }
        [XmlElement("Ma_NH_DC")]
        public string Ma_NH_DC { get; set; }
        [XmlElement("Ngay_DC")]
        public string Ngay_DC { get; set; }
        [XmlElement("Transactions")]
        public List<Transactions> Transactions { get; set; }
        [XmlElement("Error")]
        public Error Error { get; set; }
    }
    public class Transactions
    {
        public Transactions()
        {
        }
        [XmlElement("Transaction_ID")]
        public string Transaction_ID { get; set; }
        [XmlElement("ThongTinChungTu")]
        public ThongTinChungTu ThongTinChungTu { get; set; }
        [XmlElement("ThongTinGiaoDich")]
        public ThongTinGiaoDich ThongTinGiaoDich { get; set; }
        [XmlElement("KQ_DC")]
        public string KQ_DC { get; set; }
    }
    public class Error
    {
        public Error()
        {
           
        }
        [XmlElement("ErrorMessage")]
        public string ErrorMessage { get; set; }
        [XmlElement("ErrorNumber")]
        public string ErrorNumber { get; set; }
    }
    public class ThongTinChungTu
    {
        public ThongTinChungTu()
        {
            GNT_CT = new List<GNT_CT>();
        }
        [XmlElement("NgayLap_CT")]
        public string NgayLap_CT { get; set; }
        [XmlElement("NgayTruyen_CT")]
        public string NgayTruyen_CT { get; set; }
        [XmlElement("Ma_DV")]
        public string Ma_DV { get; set; }
        [XmlElement("Ma_Chuong")]
        public string Ma_Chuong { get; set; }
        [XmlElement("Ten_DV")]
        public string Ten_DV { get; set; }
        [XmlElement("Ma_KB")]
        public string Ma_KB { get; set; }
        [XmlElement("Ten_KB")]
        public string Ten_KB { get; set; }
        [XmlElement("TKKB")]
        public string TKKB { get; set; }
        [XmlElement("Ma_NTK")]
        public string Ma_NTK { get; set; }
        [XmlElement("Ma_HQ_PH")]
        public string Ma_HQ_PH { get; set; }
        [XmlElement("Ma_HQ_CQT")]
        public string Ma_HQ_CQT { get; set; }

        [XmlElement("KyHieu_CT")]
        public string KyHieu_CT { get; set; }
        [XmlElement("So_CT")]
        public string So_CT { get; set; }
        [XmlElement("Loai_CT")]
        public string Loai_CT { get; set; }
        [XmlElement("Ngay_BN")]
        public string Ngay_BN { get; set; }
        [XmlElement("Ngay_CT")]
        public string Ngay_CT { get; set; }
        [XmlElement("Ma_NT")]
        public string Ma_NT { get; set; }
        [XmlElement("Ty_Gia")]
        public string Ty_Gia { get; set; }
        [XmlElement("SoTien_TO")]
        public string SoTien_TO { get; set; }
        [XmlElement("DienGiai")]
        public string DienGiai { get; set; }
        [XmlElement("GNT_CT")]
        public List<GNT_CT> GNT_CT;

    }

    public class GNT_CT
    {
        public GNT_CT()
        {
            ToKhai_CT = new List<ToKhai_CT>();
        }
        [XmlElement("ID_HS")]
        public string ID_HS { get; set; }
        [XmlElement("TTButToan")]
        public string TTButToan { get; set; }
        [XmlElement("Ma_HQ")]
        public string Ma_HQ { get; set; }

        [XmlElement("Ma_LH")]
        public string Ma_LH { get; set; }
        [XmlElement("Nam_DK")]
        public string Nam_DK { get; set; }

        [XmlElement("Ngay_DK")]
        public string Ngay_DK { get; set; }
        [XmlElement("So_TK")]
        public string So_TK { get; set; }
        [XmlElement("Ma_LT")]
        public string Ma_LT { get; set; }
        [XmlElement("ToKhai_CT")]
        public List<ToKhai_CT> ToKhai_CT;
    }
    public class ToKhai_CT
    {
        public ToKhai_CT()
        { }
        [XmlElement("Ma_ST")]
        public string Ma_ST { get; set; }
        [XmlElement("NDKT")]
        public string NDKT { get; set; }
        [XmlElement("SoTien_NT")]
        public string SoTien_NT { get; set; }
        [XmlElement("SoTien_VND")]
        public string SoTien_VND { get; set; }

    }
    public class ThongTinGiaoDich
    {
        public ThongTinGiaoDich()
        {
            NguoiNopTien = new NguoiNopTien();
            TaiKhoan_NopTien = new TaiKhoan_NopTien();

        }
        [XmlElement("NguoiNopTien")]
        public NguoiNopTien NguoiNopTien;
        [XmlElement("TaiKhoan_NopTien")]
        public TaiKhoan_NopTien TaiKhoan_NopTien;

    }
    public class NguoiNopTien
    {
        public NguoiNopTien()
        {

        }
        [XmlElement("Ma_ST")]
        public string Ma_ST { get; set; }
        [XmlElement("So_CMT")]
        public string So_CMT { get; set; }
        [XmlElement("Ten_NNT")]
        public string Ten_NNT { get; set; }
        [XmlElement("DiaChi")]
        public string DiaChi { get; set; }
        [XmlElement("TT_Khac")]
        public string TT_Khac { get; set; }

    }
    public class TaiKhoan_NopTien
    {
        public TaiKhoan_NopTien()
        {

        }
        [XmlElement("Ma_NH_TH")]
        public string Ma_NH_TH { get; set; }
        [XmlElement("Ten_NH_TH")]
        public string Ten_NH_TH { get; set; }
        [XmlElement("TaiKhoan_TH")]
        public string TaiKhoan_TH { get; set; }
        [XmlElement("Ten_TaiKhoan_TH")]
        public string Ten_TaiKhoan_TH { get; set; }

    }
    
    public class DigitalSignatures
    {

    }
    public class Signature
    {
        public Signature() { }
    }

}
