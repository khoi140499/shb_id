﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
namespace CustomsV3.MSG.MSG207
{
    // Thông điệp trả lời danh mục hải quan
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG207
    {
        public MSG207()
        {
        }
        [XmlElement("Header", Order = 1)]
        public Header Header { get; set; }
        [XmlElement("Data", Order = 2)]
        public Data Data;
        [XmlElement("Error", Order = 3)]
        public Error Error;
        [XmlElement("Signature", Order = 4)]
        public Signature Signature;

        public MSG207 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG207.MSG207));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG207.MSG207 LoadedObjTmp = (CustomsV3.MSG.MSG207.MSG207)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSG207toXML(MSG207 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG207));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_DV { get; set; }
        public string Ma_Chuong { get; set; }
        public string Ten_DV { get; set; }
        public string DiaChi { get; set; }
    }

    public class Error
    {
        public Error() { }
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
}
