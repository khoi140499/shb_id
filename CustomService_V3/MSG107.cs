﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;

namespace CustomsV3.MSG.MSG107
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG107
    {
        public MSG107()
        {
        }
        [XmlElement("Header")]
        public Header Header { get; set; }
        [XmlElement("Data")]
        public Data Data { get; set; }
        [XmlElement("Signature")]
        public Signature Signature { get; set; }        

        public string MSG107toXML(MSG107 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG107));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG107 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG107.MSG107));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG107.MSG107 LoadedObjTmp = (CustomsV3.MSG.MSG107.MSG107)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Transaction_Req { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
}
