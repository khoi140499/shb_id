﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.MSGError;

namespace CustomsV3.MSG.MSGYCKQDC
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSGYCKQDC
    {
        public MSGYCKQDC()
        {
            Error = new Error();
        }
        [XmlElement("Header", Order = 1)]
        public HeaderCustoms Header;
        [XmlElement("Data", Order = 2)]
        public Data Data;
        [XmlElement("Error", Order = 3)]
        public Error Error;
        [XmlElement("Signature", Order = 4)]
        public Signature Signature;

        public string MSGYCKQDCtoXML(MSGYCKQDC p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSGYCKQDC));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }
        public string Loai_TD_DC { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }

}
