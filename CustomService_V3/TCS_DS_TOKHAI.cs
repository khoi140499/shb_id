﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace CustomsServiceV3
{

    public class TCS_DS_TOKHAI
    {
        public TCS_DS_TOKHAI()
        {
        }
        public string ma_nnt { get; set; }
        public string ten_nnt { get; set; }
        public string dia_chi { get; set; }
        public string lh_xnk { get; set; }
        public string ten_lhxnk { get; set; }
        public string so_tk { get; set; }
        public string ngay_tk { get; set; }
        public string nam_tk { get; set; }
        public string ma_tinh { get; set; }
        public string ma_huyen { get; set; }
        public string ma_xa { get; set; }
        public string ma_cqthu { get; set; }
        public string tk_thu_ns { get; set; }
        public string ma_kb { get; set; }
        public string ma_cap { get; set; }
        public string ma_chuong { get; set; }
        public string ma_loai { get; set; }
        public string ma_khoan { get; set; }
        public string ma_muc { get; set; }
        public string ma_tmuc { get; set; }
        public long so_phainop { get; set; }
        public string tt_nop { get; set; }
        public string lcn_owner { get; set; }

        public string ma_hq_ph { get; set; }
        public string ten_hq_ph { get; set; }
        public string ma_hq { get; set; }
        public string ten_hq { get; set; }
        public string ma_lt { get; set; }
        public string ma_ntk { get; set; }
        public string ten_ntk { get; set; }
        public string ma_nv { get; set; }
    }

}
