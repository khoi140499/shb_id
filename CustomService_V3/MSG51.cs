﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
namespace CustomsV3.MSG.MSG51
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG51
    {
        public MSG51()
        {
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Security")]
        public Security Security; 

        public string MSG51toXML(MSG51 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG51));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            strTemp = strTemp.Replace("<TEMP>", "");
            strTemp = strTemp.Replace("</TEMP>", "");
            strTemp = strTemp.Replace("<TEMP />", "");
            return strTemp;

        }
    }
    public class Data
    {
        public Data()
        {
            tem = new Tem();
        }

        [XmlElement("TEMP")]
        public Tem tem;
        [XmlElement("Accept_Transactions")]
        public Accept_Transactions Accept_Transactions;
        [XmlElement("Reject_Transactions")]
        public Reject_Transactions Reject_Transactions;
    }
    public class Tem
    {
        public Tem() { }
        public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }
        public string Loai_TD_DC { get; set; }
    }
    public class Accept_Transactions
    {
        public Accept_Transactions()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class Reject_Transactions
    {
        public Reject_Transactions()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Transaction_ID { get; set; }
        public string So_TN_CT { get; set; }
        public string Ngay_TN_CT { get; set; }
             
    }

  
}
