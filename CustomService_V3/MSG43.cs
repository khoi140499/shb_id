﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG43
{
    [Serializable]
    [XmlRootAttribute("CUSTOMS", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG43
    {
        public MSG43()
        {
        }
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Security")]
        public Security Security; 

        public string MSG43toXML(MSG43 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG43));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }
        public string Loai_TT_DC { get; set; }
    }

    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Request_ID { get; set; }
        
    }
    public class Security
    {
        public Security()
        {
        }
        public string Signature { get; set; }
    }
}
