﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG54
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG54
    {
        public MSG54()
        {
        }
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Security")]
        public Security Security; 


        public MSG54 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG54.MSG54));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG54.MSG54 LoadedObjTmp = (CustomsV3.MSG.MSG54.MSG54)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSGtoXML(MSG54 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG54));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }
    public class Data
    {
        public Data()
        {
        }
        [XmlElement("Accept_Transactions")]
        public Accept_Transactions Accept_Transactions;
        [XmlElement("Reject_Transactions")]
        public Reject_Transactions Reject_Transactions;
    }
    public class Accept_Transactions
    {
        public Accept_Transactions()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class Reject_Transactions
    {
        public Reject_Transactions()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Transaction_ID { get; set; }
        public string So_TN_CT { get; set; }
        public string Ngay_TN_CT { get; set; }
        public string KQ_DC { get; set; }
    }

    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
    }
    public class Error
    {
        public string Error_Number { get; set; }
        public string Error_Message { get; set; }
    }
    public class Security
    {
        public Security()
        {
        }
        public string Signature { get; set; }
    }
}
