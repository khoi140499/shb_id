﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Configuration;

namespace CustomsV3.MSG.MSGHeaderCustoms
{
    [Serializable]
    [XmlRootAttribute("Data", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class HeaderCustoms
    {
        private static String strApplication_Name = ConfigurationManager.AppSettings.Get("CUSTOMS.Application_Name").ToString();
        private static String strApplication_Version = ConfigurationManager.AppSettings.Get("CUSTOMS.Application_Version").ToString();
        private static String strMessage_Version = ConfigurationManager.AppSettings.Get("CUSTOMS.Message_Version").ToString();
        public static String strSender_Code = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
        public static String strSender_Name = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
        public HeaderCustoms()
        {
        }
        public HeaderCustoms(string strMessage_Type, string strMessage_Name, string req_id)
        {
            this.Application_Name = strApplication_Name;
            this.Application_Version = strApplication_Version;
            this.Sender_Code = strSender_Code;
            this.Sender_Name = strSender_Name;
            this.Message_Version = strMessage_Version;
            this.Message_Type = strMessage_Type;
            this.Message_Name = strMessage_Name;
            this.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            this.Transaction_ID = Guid.NewGuid().ToString();
            this.Request_ID = req_id;
        }
        public string Application_Name { get; set; }
        public string Application_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Message_Version { get; set; }
        public string Message_Type { get; set; }
        public string Message_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Request_ID { get; set; }
        public HeaderCustoms MSGToObject(string p_XML)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSGHeaderCustoms.HeaderCustoms));

                XmlReader reader = XmlReader.Create(new StringReader(p_XML));

                CustomsV3.MSG.MSGHeaderCustoms.HeaderCustoms LoadedObjTmp = (CustomsV3.MSG.MSGHeaderCustoms.HeaderCustoms)serializer.Deserialize(reader);
                return LoadedObjTmp;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }

}
