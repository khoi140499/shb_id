﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG16
{
    // Thông điệp trả lời danh mục hải quan
    [Serializable]
    [XmlRootAttribute("CUSTOMS", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG16
    {
        public MSG16()
        {
        }
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Security")]
        public Security Security; 

        public MSG16 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG16.MSG16));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG16.MSG16 LoadedObjTmp = (CustomsV3.MSG.MSG16.MSG16)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSG16toXML(MSG16 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG16));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

    public class Data
    {
        public Data()
        {
        }
        [XmlElement("Item")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Ma_HQ { get; set; }
        public string Ten_HQ { get; set; }
        public string Ma_Cu { get; set; }
        public string Ma_QHNS { get; set; }
        
    }

    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Request_ID { get; set; }
    }
    public class Security
    {
        public Security()
        {
        }
        public string Signature { get; set; }
    }
    public class Error
    {
        public string Error_Number { get; set; }
        public string Error_Message { get; set; }
    }
}
