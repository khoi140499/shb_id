﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CustomsV3.MSG.MSGError
{

    public class Error
    {
        public Error()
        {
            ErrorMessage = "";
            ErrorNumber = "0";
        }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
        
    }
}

