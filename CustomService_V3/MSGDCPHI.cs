﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
using CustomsV3.MSG.MSGHeaderCustoms;
namespace CustomsV3.MSG.MSGDCPHI
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSGDCPHI
    {
        public MSGDCPHI()
        {
        }
        [XmlElement("Header")]
        public CustomsV3.MSG.MSGHeaderCustoms.HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;

        [XmlElement("Error")]
        public Error Error;

        [XmlElement("Security")]
        public Security Security;

        public string MSGDCPHItoXML(MSGDCPHI p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSGDCPHI));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
    public class Data
    {
        public Data()
        {
        }

        [XmlElement("Ma_NH_DC")]
        public string Ma_NH_DC { get; set; }
        [XmlElement("Ngay_DC")]
        public string Ngay_DC { get; set; }
        [XmlElement("Transactions")]
        public List<Transaction> Transactions { get; set; }     
    }
    public class Transaction
    {

        public Transaction()
        {
        }
        [XmlElement(Order = 1)]
        public string Transaction_ID { get; set; }
        [XmlElement(Order = 2)]
        public string So_TN_CT { get; set; }
        [XmlElement(Order = 3)]
        public string Ngay_TN_CT { get; set; }
        [XmlElement(Order = 4)]
        public string Ma_NH_PH { get; set; }            
        [XmlElement(Order = 5)]
        public string Ten_NH_PH { get; set; }
        [XmlElement(Order = 6)]
        public string KyHieu_CT { get; set; }
        [XmlElement(Order = 7)]
        public string So_CT { get; set; }
        [XmlElement(Order = 8)]
        public string Ngay_CT { get; set; }
        [XmlElement(Order = 9)]
        public string Ngay_BN { get; set; }
        [XmlElement(Order = 10)]
        public string Ngay_BC { get; set; }
        [XmlElement(Order = 11)]
        public string So_HS { get; set; }
        [XmlElement(Order = 12)]
        public string Ma_DVQL { get; set; }
        [XmlElement(Order = 13)]
        public string Ten_DVQL { get; set; }
        [XmlElement(Order = 14)]
        public string KyHieu_CT_PT { get; set; }
        [XmlElement(Order = 15)]
        public string So_CT_PT { get; set; }
        [XmlElement(Order = 16)]
        public string Nam_CT_PT { get; set; }
        [XmlElement(Order = 17)]
        public NguoiNopTien NguoiNopTien { get; set; }
        [XmlElement(Order = 18)]
        public ThongTin_NopTien ThongTin_NopTien { get; set; }
        [XmlElement(Order = 19)]
        public List<ChungTu_CT> ChungTu_CT;
        [XmlElement(Order = 20)]
        public TaiKhoan_NopTien TaiKhoan_NopTien{ get; set; } 
    }
    public class NguoiNopTien
    {
        public NguoiNopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_ST { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_DV { get; set; }
        [XmlElement(Order = 3)]
        public string DiaChi { get; set; }
        [XmlElement(Order = 4)]
        public string TT_Khac { get; set; }       
    }
    public class ThongTin_NopTien
    {
        public ThongTin_NopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_NT { get; set; }
        [XmlElement(Order = 2)]
        public string TyGia { get; set; }
        [XmlElement(Order = 3)]
        public string TongTien_NT { get; set; }
        [XmlElement(Order = 4)]
        public string TongTien_VND { get; set; }   
    }
    public class ChungTu_CT
    {
        public ChungTu_CT()
        {
        }
        [XmlElement(Order = 1)]
        public string STT { get; set; }
        [XmlElement(Order = 2)]
        public string NDKT { get; set; }
        [XmlElement(Order = 3)]
        public string Ten_NDKT { get; set; }
        [XmlElement(Order = 4)]
        public string SoTien_NT { get; set; }
        [XmlElement(Order = 5)]
        public string SoTien_VND { get; set; }
        [XmlElement(Order = 6)]
        public string GhiChu { get; set; }
    }
    public class TaiKhoan_NopTien
    {
        public TaiKhoan_NopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_NH_TH { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_NH_TH { get; set; }
        [XmlElement(Order = 3)]
        public string TaiKhoan_TH { get; set; }
        [XmlElement(Order = 4)]
        public string Ten_TaiKhoan_TH { get; set; }
    }

    }
