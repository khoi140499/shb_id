﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGError;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
namespace CustomsV3.MSG.MSG202
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG202
    {
        public MSG202()
        {
        }
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Security")]
        public Security Security;

        public MSG202 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG202.MSG202));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG202.MSG202 LoadedObjTmp = (CustomsV3.MSG.MSG202.MSG202)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSG202toXML(MSG202 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG202));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

    public class Data
    {
        public Data()
        {
        }
        [XmlElement("Item")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        [XmlElement("Ma_Cuc")]
        public string Ma_Cuc { get; set; }
        [XmlElement("Ten_Cuc")]
        public string Ten_Cuc { get; set; }
        [XmlElement("Ma_HQ_PH")]
        public string Ma_HQ_PH { get; set; }
        [XmlElement("Ten_HQ_PH")]
        public string Ten_HQ_PH { get; set; }
        [XmlElement("Ma_HQ_CQT")]
        public string Ma_HQ_CQT { get; set; }
        [XmlElement("Ma_DV")]
        public string Ma_DV { get; set; }
        [XmlElement("Ten_DV")]
        public string Ten_DV { get; set; }
        [XmlElement("Ma_Chuong")]
        public string Ma_Chuong { get; set; }
        [XmlElement("Ma_NTK")]
        public string Ma_NTK { get; set; }
        [XmlElement("Ten_NTK")]
        public string Ten_NTK { get; set; }
        [XmlElement("Ma_KB")]
        public string Ma_KB { get; set; }
        [XmlElement("Ten_KB")]
        public string Ten_KB { get; set; }
        [XmlElement("TKKB")]
        public string TKKB { get; set; }
        [XmlElement("DuNo_TO")]
        public string DuNo_TO { get; set; }
        
        [XmlElement("CT_No")]
        public List<CT_No> CT_No{ get; set; }
    }
    public class CT_No
    {
        public CT_No()
        {
        }
        [XmlElement("Ma_HQ")]
        public string Ma_HQ { get; set; }
        [XmlElement("Ten_HQ")]
        public string Ten_HQ { get; set; }
        [XmlElement("Ma_LH")]
        public string Ma_LH { get; set; }
        [XmlElement("Ten_LH")]
        public string Ten_LH { get; set; }
        [XmlElement("Nam_DK")]
        public string Nam_DK { get; set; }
        [XmlElement("So_TK")]
        public string So_TK { get; set; }
        [XmlElement("Ngay_DK")]
        public string Ngay_DK { get; set; }
        [XmlElement("LoaiThue")]
        public string LoaiThue { get; set; }
        [XmlElement("Khoan")]
        public string Khoan { get; set; }
        [XmlElement("TieuMuc")]
        public string TieuMuc { get; set; }
        [XmlElement("DuNo")]
        public string DuNo { get; set; }
    }

    public class Header
    {
        public Header()
        {
        }
        public string Application_Name { get; set; }
        public string Application_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Message_Version { get; set; }
        public string Message_Type { get; set; }
        public string Message_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Request_ID { get; set; }
    }
   
    public class Error
    {
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
    }
}
