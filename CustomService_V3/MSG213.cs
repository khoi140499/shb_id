﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
namespace CustomsV3.MSG.MSG213
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG213
    {
        public MSG213()
        {
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Signature")]
        public Signature Signature;

        public MSG213 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG213.MSG213));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG213.MSG213 LoadedObjTmp = (CustomsV3.MSG.MSG213.MSG213)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSGtoXML(MSG213 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG213));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

    public class Data
    {

        public Data()
        {
        }
        [XmlElement("Loai_TD_TraLoi")]
        public string Loai_TD_TraLoi{ get; set; }
        [XmlElement("Ma_KQ_XL")]
        public string Ma_KQ_XL { get; set; }
        [XmlElement("NoiDung_XL")]
        public string NoiDung_XL { get; set; }
    }

    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
}
