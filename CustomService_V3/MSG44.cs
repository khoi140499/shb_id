﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG44
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG44
    {
        public MSG44()
        {
        }
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Security")]
        public Security Security;
        [XmlElement("Error")]
        public Error Error;

        public MSG44 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG44.MSG44));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG44.MSG44 LoadedObjTmp = (CustomsV3.MSG.MSG44.MSG44)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSGtoXML(MSG44 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG44));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

    public class Data
    {
        public Data()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Transaction_ID { get; set; }
        public string So_TN_CT { get; set; }
        public string Ngay_TN_CT { get; set; }
        public string Ma_NH_PH { get; set; }
        public string Ten_NH_PH { get; set; }
        public string Ma_NH_TH { get; set; }
        public string Ten_NH_TH { get; set; }
        public string Ma_Chuong { get; set; }
        public string Ma_DV { get; set; }
        public string Ten_DV { get; set; }
        public string Ma_HQ_PH { get; set; }
        public string Ma_HQ_CQT { get; set; }
        public string Ma_HQ { get; set; }
        public string Ma_LH { get; set; }
        public string SoTK { get; set; }
        public string Ngay_DK { get; set; }
        public string Ma_LT { get; set; }
        public string Ma_NTK { get; set; }
        public string KyHieu_CT { get; set; }
        public string So_CT { get; set; }
        public string TTButToan { get; set; }
        public string Ma_KB { get; set; }
        public string Ten_KB { get; set; }
        public string TKKB { get; set; }
        public string TKKB_CT { get; set; }
        public string Ngay_BN { get; set; }
        public string Ngay_BC { get; set; }
        public string Ngay_CT { get; set; }

        public string DienGiai { get; set; }
        public string Khoan_XK { get; set; }
        public string TieuMuc_XK { get; set; }
        public string DuNo_XK { get; set; }
        public string Khoan_NK { get; set; }
        public string TieuMuc_NK { get; set; }
        public string DuNo_NK { get; set; }
        public string Khoan_VA { get; set; }
        public string TieuMuc_VA { get; set; }
        public string DuNo_VA { get; set; }
        public string Khoan_TD { get; set; }
        public string TieuMuc_TD { get; set; }
        public string DuNo_TD { get; set; }
        public string Khoan_TV { get; set; }
        public string TieuMuc_TV { get; set; }
        public string DuNo_TV { get; set; }
        public string Khoan_MT { get; set; }
        public string TieuMuc_MT { get; set; }
        public string DuNo_MT { get; set; }
        public string Khoan_KH { get; set; }
        public string TieuMuc_KH { get; set; }
        public string DuNo_KH { get; set; }
        public string DuNo_TO { get; set; }
        public string KQ_DC { get; set; }
    }

    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Request_ID { get; set; }
    }
    public class Security
    {
        public Security()
        {
        }
        public string Signature { get; set; }
    }
    public class Error
    {
        public Error()
        {
        }
        public string Error_Number { get; set; }
        public string Error_Message { get; set; }

    }
}
