﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
//using System.Data.OracleClient;
using System.Data;
namespace CustomsV3.MSG.MSG313
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG313
    {
        public MSG313()
        {
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Signature")]
        public Signature Signature;
        public MSG313 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG313));
            XmlReader reader = XmlReader.Create(new StringReader(p_XML));
            MSG313 LoadedObjTmp = (MSG313)serializer.Deserialize(reader);
            return LoadedObjTmp;
        }
        public string MSG313toXML(MSG313 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG313));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
    }
    public class Data
    {
        public Data()
        {  }
        [XmlElement("So_HS")]
        public string So_HS { get; set; }
    }
    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }
    public class Signature
    {
        public Signature() { }
    }
}
