﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.MSGError;

namespace CustomsV3.MSG.MSG299
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG299
    {
        public MSG299()
        {
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Signature")]
        public Signature Signature;        

        public string MSG299toXML(MSG299 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG299));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG299 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG299.MSG299));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG299.MSG299 LoadedObjTmp = (CustomsV3.MSG.MSG299.MSG299)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        //public string Transaction_Req { get; set; }
    }

    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
   
}
