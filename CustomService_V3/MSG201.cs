﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Data;
namespace CustomsV3.MSG.MSG201
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG201
    {
        public MSG201()
        {
        }
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Signature")]
        public Signature Signature; 

        public MSG201 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG201.MSG201));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG201.MSG201 LoadedObjTmp = (CustomsV3.MSG.MSG201.MSG201)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSG201toXML(MSG201 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG201));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
        //hoanglv edit 20200115
        public DataSet convertGNT_CTToDS()
        {
            DataSet ds = new DataSet();

            try
            {
                if (Data.Item.Count <= 0)
                { return null; }
                if (Data.Item[0].CT_No.Count <= 0)
                {
                    return null;
                }
                DataTable tbl = new DataTable();
                tbl.Columns.Add("ID_HS");
                tbl.Columns.Add("TTButToan");
                tbl.Columns.Add("Ma_HQ");
                tbl.Columns.Add("Ma_LH");
                tbl.Columns.Add("Nam_DK");
                tbl.Columns.Add("So_TK");
                tbl.Columns.Add("Ma_LT");
                tbl.Columns.Add("Ma_ST");
                tbl.Columns.Add("MTMUC");
                tbl.Columns.Add("NDKT");
                tbl.Columns.Add("SoTien_NT");
                tbl.Columns.Add("SoTien_VND");
                ds.Tables.Add(tbl);
                int vIntTTButToan = 0;
                for (int i = 0; i < Data.Item.Count; i++)
                {
                    for (int x = 0; x < Data.Item[i].CT_No.Count; x++)
                    {
                        DataRow vrow = ds.Tables[0].NewRow();
                        vrow["ID_HS"] = Header.Request_ID;
                        vIntTTButToan++;
                        vrow["TTButToan"] = vIntTTButToan;
                        vrow["Ma_HQ"] = Data.Item[i].Ma_HQ;
                        vrow["Ma_LH"] = Data.Item[i].Ma_LH;
                        vrow["So_TK"] = Data.Item[i].So_TK;
                        vrow["Nam_DK"] = Data.Item[i].Nam_DK;
                        vrow["Ma_LT"] = Data.Item[i].Ma_LT;
                        vrow["Ma_ST"] = Data.Item[i].CT_No[x].LoaiThue;
                        vrow["MTMUC"] = Data.Item[i].CT_No[x].TieuMuc;
                        vrow["NDKT"] = Business_HQ.Common.mdlCommon.Get_TenMTM(Data.Item[i].CT_No[x].TieuMuc);
                        vrow["SoTien_NT"] = Data.Item[i].CT_No[x].DuNo;
                        vrow["SoTien_VND"] = Data.Item[i].CT_No[x].DuNo;
                        ds.Tables[0].Rows.Add(vrow);
                    }
                }

            }
            catch (Exception ex)
            { }
            return ds;
        }
    }

    public class Header
    {
        public Header()
        {
        }
        public string Application_Name { get; set; }
        public string Application_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Message_Version { get; set; }
        public string Message_Type { get; set; }
        public string Message_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Request_ID { get; set; }
    }

    public class Data
    {
        public Data()
        {
        }
        [XmlElement("Item")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        [XmlElement("Ma_Cuc")]
        public string Ma_Cuc { get; set; }
        [XmlElement("Ten_Cuc")]
        public string Ten_Cuc { get; set; }
        [XmlElement("Ma_HQ_PH")]
        public string Ma_HQ_PH { get; set; }
        [XmlElement("Ten_HQ_PH")]
        public string Ten_HQ_PH { get; set; }
        [XmlElement("Ma_HQ_CQT")]
        public string Ma_HQ_CQT { get; set; }
        [XmlElement("Ten_HQ_CQT")]
        public string Ten_HQ_CQT { get; set; }
        [XmlElement("Ma_DV")]
        public string Ma_DV { get; set; }
        [XmlElement("Ten_DV")]
        public string Ten_DV { get; set; }
        [XmlElement("Ma_Chuong")]
        public string Ma_Chuong { get; set; }
        [XmlElement("Ma_HQ")]
        public string Ma_HQ { get; set; }
        [XmlElement("Ten_HQ")]
        public string Ten_HQ { get; set; }
        [XmlElement("Ma_LH")]
        public string Ma_LH { get; set; }
        [XmlElement("Ten_LH")]
        public string Ten_LH { get; set; }
        [XmlElement("Nam_DK")]
        public string Nam_DK { get; set; }
        [XmlElement("So_TK")]
        public string So_TK { get; set; }
        [XmlElement("Ma_NTK")]
        public string Ma_NTK { get; set; }
        [XmlElement("Ten_NTK")]
        public string Ten_NTK { get; set; }
        [XmlElement("Ma_LT")]
        public string Ma_LT { get; set; }
        [XmlElement("Ma_HTVCHH")]
        public string Ma_HTVCHH { get; set; }
        [XmlElement("Ten_HTVCHH")]
        public string Ten_HTVCHH { get; set; }
        [XmlElement("Ngay_DK")]
        public string Ngay_DK { get; set; }
        [XmlElement("Ma_KB")]
        public string Ma_KB { get; set; }
        [XmlElement("Ten_KB")]
        public string Ten_KB { get; set; }
        [XmlElement("TKKB")]
        public string TKKB { get; set; }
        [XmlElement("TTNo")]
        public string TTNo { get; set; }
        [XmlElement("Ten_TTN")]
        public string Ten_TTN { get; set; }
        [XmlElement("TTNo_CT")]
        public string TTNo_CT { get; set; }
        [XmlElement("Ten_TTN_VT")]
        public string Ten_TTN_VT { get; set; }
        [XmlElement("DuNo_TO")]
        public string DuNo_TO { get; set; }
        [XmlElement("CT_No")]
        public List<CT_No> CT_No{ get; set; }
    }
    public class CT_No
    {
        public CT_No()
        {
        }
        [XmlElement("LoaiThue")]
        public string LoaiThue { get; set; }
        [XmlElement("Khoan")]
        public string Khoan{ get; set; }
        [XmlElement("TieuMuc")]
        public string TieuMuc { get; set; }
        [XmlElement("DuNo")]
        public string DuNo { get; set; }
    }

    public class Error
    {
        public Error() { }
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Signature {
        public Signature() { }

    }

    
}
