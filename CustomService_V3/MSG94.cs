﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG94
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG94
    {
        public MSG94()
        {
        }
        [XmlElement("Header", Order = 1)]
        public Header Header;
        [XmlElement("Data", Order = 2)]
        public Data Data;
        [XmlElement("Error", Order = 3)]
        public Error Error;
        [XmlElement("Signature", Order = 4)]
        public Signature Signature;


        public MSG94 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG94.MSG94));
                      
            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG94.MSG94 LoadedObjTmp = (CustomsV3.MSG.MSG94.MSG94)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSGtoXML(MSG94 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG94));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }
    public class Data
    {
        public Data()
        {
            temp = new Temp();
        }
        [XmlElement("TEMP")]
        public Temp temp;
        [XmlElement("Accept_Transactions")]
        public Accept_Transactions Accept_Transactions;
        [XmlElement("Reject_Transactions")]
        public Reject_Transactions Reject_Transactions;
    }
    public class Temp
    {
        public Temp()
        {
        }
        public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }
        //public string Loai_BL { get; set; }
        public string Loai_TD_DC { get; set; }

    }
    public class Accept_Transactions
    {
        public Accept_Transactions()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class Reject_Transactions
    {
        public Reject_Transactions()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Transaction_ID { get; set; }
        public string So_TN_CT { get; set; }
        public string Ngay_TN_CT { get; set; }
        public string KQ_DC { get; set; }
    }

    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
    }
    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
}
