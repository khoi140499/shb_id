﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
//using System.Data.OracleClient;
using System.Data;
namespace CustomsV3.MSG.MSG312
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG312
    {
        [XmlElement("Document")]
        public Document Document;
        [XmlElement("DigitalSignatures")]
        public DigitalSignatures DigitalSignatures;
        public MSG312()
        {
            DigitalSignatures = new DigitalSignatures();
        }
      
        
        public MSG312 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG312));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG312 LoadedObjTmp = (MSG312)serializer.Deserialize(reader);
            //if (LoadedObjTmp.Data.ThongTin_NNT == null)
            //{
            //    LoadedObjTmp = LoadManual(p_XML,LoadedObjTmp);
            //}
            return LoadedObjTmp;

        }
        private MSG312 LoadManual(string p_XML, MSG312 LoadedObjTmp)
        {
            MSG312 obj= LoadedObjTmp ;
            //XmlDocument vdoc = new XmlDocument();
            //string strXMLTemp = p_XML.Replace(CustomsServiceV3.ProcessMSG.strXMLdefin2, "<Customs>");
            //vdoc.LoadXml(strXMLTemp);
            //obj.Data.ThongTin_NNT = new ThongTin_NNT();
            ////XmlNode vNode = vdoc.SelectSingleNode("/Customs/Data/ThongTin_NTT/So_CMT");
            //string str = vdoc.SelectSingleNode("/Customs/Data/ThongTin_NTT").InnerXml;
            //StringReader strReader = new StringReader("<ThongTin_NTT>" + str + "</ThongTin_NTT>");
            //DataSet ds = new DataSet();
            //ds.ReadXml(strReader);
            //obj.Data.ThongTin_NNT.So_CMT = vdoc.SelectSingleNode("/Customs/Data/ThongTin_NTT/So_CMT").InnerText;
            //obj.Data.ThongTin_NNT.Ho_Ten = vdoc.SelectSingleNode("/Customs/Data/ThongTin_NTT/Ho_Ten").InnerText;
            //obj.Data.ThongTin_NNT.NgaySinh = vdoc.SelectSingleNode("/Customs/Data/ThongTin_NTT/NgaySinh").InnerText;
            //obj.Data.ThongTin_NNT.NguyenQuan = vdoc.SelectSingleNode("/Customs/Data/ThongTin_NTT/NguyenQuan").InnerText;
            //if (ds.Tables.Count > 0)
            //{
            //    if (ds.Tables["ThongTinLienHe"] != null)
            //    {
            //        obj.Data.ThongTin_NNT.ThongTinLienHe = new List<ThongTinLienHe>();
            //        for (int i = 0; i < ds.Tables["ThongTinLienHe"].Rows.Count; i++)
            //        {
            //            ThongTinLienHe objtmp = new ThongTinLienHe();
            //            objtmp.So_DT = ds.Tables["ThongTinLienHe"].Rows[i]["So_DT"].ToString();
            //            objtmp.Email = ds.Tables["ThongTinLienHe"].Rows[i]["Email"].ToString();
            //            obj.Data.ThongTin_NNT.ThongTinLienHe.Add(objtmp);
            //        }
            //    }
            //    if (ds.Tables["ChungThuSo"] != null && ds.Tables["ChungThuSo"].Rows.Count>0)
            //    {
            //        ChungThuSo objtmp = new ChungThuSo();
            //        objtmp.SerialNumber = ds.Tables["ChungThuSo"].Rows[0]["SerialNumber"].ToString();
            //        objtmp.Noi_Cap = ds.Tables["ChungThuSo"].Rows[0]["Noi_Cap"].ToString();
            //        objtmp.Ngay_HL = ds.Tables["ChungThuSo"].Rows[0]["Ngay_HL"].ToString();
            //        objtmp.Ngay_HHL = ds.Tables["ChungThuSo"].Rows[0]["Ngay_HHL"].ToString();
            //        objtmp.PublicKey = ds.Tables["ChungThuSo"].Rows[0]["PublicKey"].ToString();
            //        obj.Data.ThongTin_NNT.ChungThuSo = objtmp;

            //    }
            //    if (ds.Tables["ThongTinTaiKhoan"] != null && ds.Tables["ThongTinTaiKhoan"].Rows.Count > 0)
            //    {
            //        ThongTinTaiKhoan objtmp = new ThongTinTaiKhoan();
            //        objtmp.Ma_NH_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["Ma_NH_TH"].ToString();
            //        objtmp.Ten_NH_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["Ten_NH_TH"].ToString();
            //        objtmp.TaiKhoan_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["TaiKhoan_TH"].ToString();
            //        objtmp.Ten_TaiKhoan_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["Ten_TaiKhoan_TH"].ToString();
            //        obj.Data.ThongTin_NNT.ThongTinTaiKhoan = objtmp;

            //    }

            //}
            return obj;
        }
        public string MSG312toXML(MSG312 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG312));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }

    }
    public class Document
    {
        public Document()
        {
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        
    }
    public class Data
    {
        public Data()
        {  }
        [XmlElement(Order = 1)]
        public string So_HS { get; set; }
        [XmlElement(Order = 2)]
        public string Loai_HS { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_DV { get; set; }
        [XmlElement(Order = 4)]
        public string Ten_DV { get; set; }
        [XmlElement(Order = 5)]
        public string DiaChi { get; set; }
        [XmlElement(Order = 6)]
        public string Ngay_HL { get; set; }
        [XmlElement(Order = 7)]
        public ThongTin_NNT ThongTin_NNT;
      
    }
    public class ThongTin_NNT
    {
        public ThongTin_NNT()
        { }
        [XmlElement(Order = 1)]
        public string So_CMT { get; set; }
        [XmlElement(Order = 2)]
        public string Ho_Ten { get; set; }
        [XmlElement(Order = 3)]
        public string NgaySinh { get; set; }
        [XmlElement(Order = 4)]
        public string NguyenQuan { get; set; }
        [XmlElement(Order = 5)]
        public List<ThongTinLienHe> ThongTinLienHe;
        [XmlElement(Order = 6)]
        public ChungThuSo ChungThuSo ;
        [XmlElement(Order = 7)]
        public ThongTinTaiKhoan ThongTinTaiKhoan ;
    }
    public class ThongTinLienHe
    {
        public ThongTinLienHe()
        {
        }
        [XmlElement("So_DT")]
        public string So_DT { get; set; }
        [XmlElement("Email")]
        public string Email { get; set; }
    }
    public class ChungThuSo
    {
        public ChungThuSo()
        {
        }
        [XmlElement("SerialNumber")]
        public string SerialNumber { get; set; }
        [XmlElement("Noi_Cap")]
        public string Noi_Cap { get; set; }
        [XmlElement("Ngay_HL")]
        public string Ngay_HL { get; set; }
        [XmlElement("Ngay_HHL")]
        public string Ngay_HHL { get; set; }
        [XmlElement("PublicKey")]
        public string PublicKey { get; set; }
    }
    public class ThongTinTaiKhoan
    {
        public ThongTinTaiKhoan()
        {
        }
        [XmlElement("Ma_NH_TH")]
        public string Ma_NH_TH { get; set; }
        [XmlElement("Ten_NH_TH")]
        public string Ten_NH_TH { get; set; }
        [XmlElement("TaiKhoan_TH")]
        public string TaiKhoan_TH { get; set; }
        [XmlElement("Ten_TaiKhoan_TH")]
        public string Ten_TaiKhoan_TH { get; set; }
    }
    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }
     public class DigitalSignatures
    {
        
    }
    public class Signature
    {
        public Signature() { }
    }
     
       
}
