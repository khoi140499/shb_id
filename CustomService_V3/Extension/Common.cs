﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Reflection;
using VBOracleLib;

namespace CustomsServiceV3.Extension
{
    public class Common
    {
        public static string GetName<T>(T item, bool first, string prefix) where T : class
        {
            string strReturnObject = string.Empty;

            var fieldTypes = typeof(T).GetProperties();
            foreach (var fieldItem in fieldTypes)
            {
                if (first)
                {
                    strReturnObject += "?,";
                }
                else
                {
                    strReturnObject += fieldItem.Name + ",";
                }


            }

            return strReturnObject.Remove(strReturnObject.Length - 1);
        }

        public static int getLengthOfType<T>(T item) where T : class
        {
            return typeof(T).GetProperties().Length;
        }

        public static DbType TypeToDbType(string t)
        {
            DbType dbt;
            try
            {
                dbt = (DbType)Enum.Parse(typeof(DbType), t);
            }
            catch
            {
                dbt = DbType.Object;
            }
            return dbt;
        }

        public static string getStrValueInsert<T>(T item) where T : class
        {
            var fieldTypes = typeof(T).GetProperties();


            Type myType = typeof(T);
            FieldInfo[] myField = myType.GetFields();
            string listInserted = string.Empty;

            for (int i = 0; i < fieldTypes.Length; i++)
            {
                var value = item.GetType().GetProperty(fieldTypes[i].Name).GetValue(item, null);

                var console = fieldTypes[i].PropertyType;
                //string hehe = console.AssemblyQualifiedName;                

                if (TypeToDbType(console.Name) == DbType.DateTime)
                {

                    listInserted += " to_date(to_char(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY'), ";
                }
                else
                {
                    listInserted += "'" + item.GetType().GetProperty(fieldTypes[i].Name).GetValue(item, null) + "',"; ;// item.GetType().GetProperty(fieldTypes[i].Name).GetValue(item, null);
                }

                //listInserted += // .GetValue(null);//fieldTypes[i]. .GetType().GetField(fieldTypes[i].Name).GetValue(item);
            }
            return listInserted.Remove(listInserted.Length - 1);
        }


        public static IDbDataParameter[] getIDbParameter<T>(T item) where T : class
        {
            var fieldTypes = typeof(T).GetProperties();

            IDbDataParameter[] p = new IDbDataParameter[fieldTypes.Length];
            Type myType = typeof(T);
            FieldInfo[] myField = myType.GetFields();

            string temp = string.Empty;
            for (int i = 0; i < fieldTypes.Length; i++)
            {
                p[i] = new OracleParameter();
                p[i].ParameterName = fieldTypes[i].Name;


                var console = fieldTypes[i].PropertyType;
                try
                {

                    //string hehe = console.AssemblyQualifiedName;
                    p[i].DbType = DbType.String;
                }
                catch
                {
                }

                p[i].Direction = ParameterDirection.Input;
                var value = item.GetType().GetProperty(fieldTypes[i].Name).GetValue(item, null);
                if (TypeToDbType(console.Name) == DbType.DateTime)
                {
                    p[i].Value = " to_date(to_char(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY') ";

                    temp += p[i].Value + ",";
                }
                //else if (TypeToDbType(console.Name) == DbType.Int32 || TypeToDbType(console.Name) == DbType.Int64 || TypeToDbType(console.Name) == DbType.Decimal || TypeToDbType(console.Name) == DbType.Double)
                //{
                //    p[i].Value = value;
                //}
                else
                {

                    p[i].Value = "'" + value + "'";// item.GetType().GetProperty(fieldTypes[i].Name).GetValue(item, null);
                    temp += p[i].Value + ",";
                }
                //p[i].Value = item.GetType().GetProperty(fieldTypes[i].Name).GetValue(item, null);// .GetValue(null);//fieldTypes[i]. .GetType().GetField(fieldTypes[i].Name).GetValue(item);
            }

            //string haha = temp;
            return p;
        }

        public static string getString<T>(T item) where T : class
        {
            var fieldTypes = typeof(T).GetProperties();


            Type myType = typeof(T);
            FieldInfo[] myField = myType.GetFields();

            string temp = string.Empty;
            string haha = temp;
            for (int i = 0; i < fieldTypes.Length; i++)
            {

                haha += "string " + fieldTypes[i].Name + ",";
            }


            return haha;
        }


        static Type ConvertType(DbType dbType)
        {
            Type toReturn = typeof(DBNull);

            switch (dbType)
            {
                case DbType.String:
                    toReturn = typeof(string);
                    break;

                case DbType.UInt64:
                    toReturn = typeof(UInt64);
                    break;

                case DbType.Int64:
                    toReturn = typeof(Int64);
                    break;

                case DbType.Int32:
                    toReturn = typeof(Int32);
                    break;

                case DbType.UInt32:
                    toReturn = typeof(UInt32);
                    break;

                case DbType.Single:
                    toReturn = typeof(float);
                    break;

                case DbType.Date:
                    toReturn = typeof(DateTime);
                    break;

                case DbType.DateTime:
                    toReturn = typeof(DateTime);
                    break;

                case DbType.Time:
                    toReturn = typeof(DateTime);
                    break;

                case DbType.StringFixedLength:
                    toReturn = typeof(string);
                    break;

                case DbType.UInt16:
                    toReturn = typeof(UInt16);
                    break;

                case DbType.Int16:
                    toReturn = typeof(Int16);
                    break;

                case DbType.SByte:
                    toReturn = typeof(byte);
                    break;

                case DbType.Object:
                    toReturn = typeof(object);
                    break;

                case DbType.AnsiString:
                    toReturn = typeof(string);
                    break;

                case DbType.AnsiStringFixedLength:
                    toReturn = typeof(string);
                    break;

                case DbType.VarNumeric:
                    toReturn = typeof(decimal);
                    break;

                case DbType.Currency:
                    toReturn = typeof(double);
                    break;

                case DbType.Binary:
                    toReturn = typeof(byte[]);
                    break;

                case DbType.Decimal:
                    toReturn = typeof(decimal);
                    break;

                case DbType.Double:
                    toReturn = typeof(Double);
                    break;

                case DbType.Guid:
                    toReturn = typeof(Guid);
                    break;

                case DbType.Boolean:
                    toReturn = typeof(bool);
                    break;
            }

            return toReturn;
        }

        public static string FormatDateTimeDDMMYYYY(string date)
        {
            string[] dateSplit = date.Split('/');
            string month = dateSplit[0];
            if (month.Length == 1)
            {
                month = "0" + month;
            }
            return dateSplit[1] + "/" + month +"/" + dateSplit[2];
        }

        public static string FormatDateTime(string date)
        {
            string[] dateSplit = date.Split('-');
            return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
        }

        /// <summary>
        /// Hàm lấy thông tin ngày làm việc của hệ thống theo mã nhân viên
        /// </summary>
        /// <param name="strMa_NV">Mã nhân viên</param>
        /// <returns>Ngày làm việc của hệ thống</returns>
        /// <remarks>
        ///</remarks>
        public static Int64 TCS_GetSequenceValue(string p_Seq)
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT ");
                sbSQL.Append(p_Seq);
                sbSQL.Append(".nextval FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                return Int64.Parse(strRet);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
    }
}
