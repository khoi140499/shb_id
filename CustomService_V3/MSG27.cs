﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG27
{
    [Serializable]
    [XmlRootAttribute("CUSTOMS", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG27
    {
        public MSG27()
        {
        }
        [XmlElement(Order = 1)]
        public Header Header;
        [XmlElement(Order = 2)]
        public Data Data;
        [XmlElement(Order = 3)]
        public Security Security; 

        public string MSG27toXML(MSG27 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG27));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
    public class Header
    {
        public Header()
        {
        }
        [XmlElement(Order = 1)]
        public string Message_Version { get; set; }
        [XmlElement(Order = 2)]
        public string Sender_Code { get; set; }
        [XmlElement(Order = 3)]
        public string Sender_Name { get; set; }
        [XmlElement(Order = 4)]
        public string Transaction_Type { get; set; }
        [XmlElement(Order = 5)]
        public string Transaction_Name { get; set; }
        [XmlElement(Order = 6)]
        public string Transaction_Date { get; set; }
        [XmlElement(Order = 7)]
        public string Transaction_ID { get; set; }
    }
    public class Data
    {
        
        public Data()
        {
                  }
        [XmlElement(Order = 1)]
        public string Ma_NH_PH { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_NH_PH { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_NH_TH { get; set; }
        [XmlElement(Order = 4)]
        public string Ten_NH_TH { get; set; }
        [XmlElement(Order = 5)]
        public string Ma_DV { get; set; }
        [XmlElement(Order = 6)]
        public string Ma_Chuong { get; set; }
        [XmlElement(Order = 7)]
        public string Ten_DV { get; set; }
        [XmlElement(Order = 8)]
        public string Ma_KB { get; set; }
        [XmlElement(Order = 9)]
        public string Ten_KB { get; set; }
        [XmlElement(Order = 10)]
        public string TKKB { get; set; }
        [XmlElement(Order = 11)]
        public string Ma_NTK { get; set; }
        [XmlElement(Order = 12)]
        public string Ma_HQ_PH { get; set; }
        [XmlElement(Order = 13)]
        public string Ma_HQ_CQT { get; set; }
        [XmlElement(Order = 14)]
        public string KyHieu_CT { get; set; }
        [XmlElement(Order = 15)]
        public string So_CT { get; set; }
        [XmlElement(Order = 16)]
        public string Loai_CT { get; set; }
        [XmlElement(Order = 17)]
        public string So_TN_CTS { get; set; }
        [XmlElement(Order = 18)]
        public string Ngay_TN_CTS { get; set; }
        [XmlElement(Order = 19)]
        public string Ngay_BN { get; set; }
        [XmlElement(Order = 20)]
        public string Ngay_BC { get; set; }
        [XmlElement(Order = 21)]
        public string Ngay_CT { get; set; }
        [XmlElement(Order = 22)]
        public string SoTien_TO { get; set; }
        [XmlElement(Order = 23)]
        public string DienGiai { get; set; }
        [XmlElement(Order = 24)]
        public List<GNT_CT> GNT_CT; 
        
    }
    public class GNT_CT
    {
        public GNT_CT()
        {
        }
        [XmlElement(Order = 1)]
        public string TTButToan { get; set; }
        [XmlElement(Order = 2)]
        public string Ma_HQ { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_LH { get; set; }
        [XmlElement(Order = 4)]
        public string Nam_DK { get; set; }
        [XmlElement(Order = 5)]
        public string So_TK { get; set; }
        [XmlElement(Order = 6)]
        public string Ma_LT { get; set; }
        [XmlElement(Order = 7)]
        public List<ToKhai_CT> ToKhai_CT; 
    }
    public class ToKhai_CT
    {
        public ToKhai_CT()
        {
        }
        public string Ma_ST { get; set; }
        public string NDKT { get; set; }
        public string Ma_NT { get; set; }
        public string Ty_Gia { get; set; }
        public string SoTien_NT { get; set; }
        public string SoTien { get; set; }
    }
    public class Security
    {
        public Security()
        {
        }
        public string Signature { get; set; }
    }
}
