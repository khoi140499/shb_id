﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG11
{
    [Serializable]
    [XmlRootAttribute("CUSTOMS", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG11
    {
        public MSG11()
        {
        }
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Security")]
        public Security Security;        

        public string MSG11toXML(MSG11 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG11));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG11 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG11.MSG11));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG11.MSG11 LoadedObjTmp = (CustomsV3.MSG.MSG11.MSG11)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_DV { get; set; }
        //public string Ten_DV { get; set; }
        public string Nam_DK { get; set; }
        public string So_TK { get; set; }
    }


    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
    }
    public class Security
    {
        public Security() 
        { 
        }
        public string Signature { get; set; }
    }
}
