﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
using Business_HQ.HQ247;
using CustomsV3.MSG.MSGHeader;
using System.Data;

namespace CustomsV3.MSG.MSG304
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG304
    {
        public MSG304()
        {
            Error = new Error();
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Signature")]
        public Signature Signature;


        public MSG304 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG304));


            XmlReader reader = XmlReader.Create(new StringReader(p_XML));
            MSG304 LoadedObjTmp = (MSG304)serializer.Deserialize(reader);
            return LoadedObjTmp;


        }
        
        public void SETDATA(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG304));

         
            XmlReader reader = XmlReader.Create(new StringReader(p_XML));
            MSG304 LoadedObjTmp = (MSG304)serializer.Deserialize(reader);
            Header = LoadedObjTmp.Header;
            Data = LoadedObjTmp.Data;
           
            strXML = p_XML;
            

        }
        
        public string strXML { get; set; }
        public MSG304DC_HDR ObjectMSG304DC(string strID)
        {
            MSG304DC_HDR objhdr = new MSG304DC_HDR();
            try
            {
                objhdr.NGAYLAP_CT = Data.ThongTinChungTu.NgayLap_CT;
                objhdr.NGAYTRUYEN_CT = Data.ThongTinChungTu.NgayTruyen_CT;
                objhdr.MA_DV = Data.ThongTinChungTu.Ma_DV;
                objhdr.MA_CHUONG = Data.ThongTinChungTu.Ma_Chuong;
                objhdr.TEN_DV = Data.ThongTinChungTu.Ten_DV;
                objhdr.MA_KB = Data.ThongTinChungTu.Ma_KB;
                objhdr.TEN_KB = Data.ThongTinChungTu.Ten_KB;
                objhdr.TKKB = Data.ThongTinChungTu.TKKB;
                objhdr.MA_NTK = Data.ThongTinChungTu.Ma_NTK;
                objhdr.MA_HQ_PH = Data.ThongTinChungTu.Ma_HQ_PH;
                objhdr.MA_HQ_CQT = Data.ThongTinChungTu.Ma_HQ_CQT;
                objhdr.KYHIEU_CT = Data.ThongTinChungTu.KyHieu_CT;
                objhdr.SO_CT = Data.ThongTinChungTu.So_CT;
                objhdr.LOAI_CT = Data.ThongTinChungTu.Loai_CT;
                objhdr.NGAY_BN = Data.ThongTinChungTu.Ngay_BN;
                objhdr.NGAY_CT = Data.ThongTinChungTu.Ngay_CT;
                objhdr.MA_NT = Data.ThongTinChungTu.Ma_NT;
                objhdr.TY_GIA = Data.ThongTinChungTu.Ty_Gia;
                objhdr.SOTIEN_TO = Data.ThongTinChungTu.SoTien_TO;
                objhdr.DIENGIAI = Data.ThongTinChungTu.DienGiai;
                objhdr.MA_ST = Data.ThongTinGiaoDich.NguoiNopTien.Ma_ST;
                objhdr.SO_CMT = Data.ThongTinGiaoDich.NguoiNopTien.So_CMT;
                objhdr.TEN_NNT = Data.ThongTinGiaoDich.NguoiNopTien.Ten_NNT;
                objhdr.DIACHI = Data.ThongTinGiaoDich.NguoiNopTien.DiaChi;
                objhdr.TT_KHAC = Data.ThongTinGiaoDich.NguoiNopTien.TT_Khac;
                objhdr.MA_NH_TH = Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ma_NH_TH;
                objhdr.TEN_NH_TH = Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_NH_TH;
                objhdr.TAIKHOAN_TH = Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH;
                objhdr.TEN_TAIKHOAN_TH = Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH;
                foreach (GNT_CT objGNT_CT in Data.ThongTinChungTu.GNT_CT)
                {
                    foreach (ToKhai_CT objToKhai_CT in objGNT_CT.ToKhai_CT)
                    {
                        MSG304DC_DTL objdtl = new MSG304DC_DTL();
                        objdtl.ID = strID;
                        objdtl.ID_HS = objGNT_CT.IS_HS;
                        objdtl.MA_HQ = objGNT_CT.Ma_HQ;
                        objdtl.MA_LH = objGNT_CT.Ma_LH;
                        objdtl.MA_LT = objGNT_CT.Ma_LT;
                        objdtl.SO_TK = objGNT_CT.So_TK;
                        objdtl.NAM_DK = objGNT_CT.Nam_DK;
                        objdtl.TTBUTTOAN = objGNT_CT.TTButToan;
                        objdtl.MA_ST = objToKhai_CT.Ma_ST;
                        objdtl.NDKT = objToKhai_CT.NDKT;
                        objdtl.SOTIEN_NT = objToKhai_CT.SoTien_NT;
                        objdtl.SOTIEN_VND = objToKhai_CT.SoTien_VND;
                        objhdr.MSG304DC_DTL.Add(objdtl);
                    }

                }
                return objhdr;
            }
            catch (Exception ex)
            { }
            return null;
        }
        
    }
   
    public class ThongTinChungTu
    {
        public ThongTinChungTu()
        {
            GNT_CT = new List<GNT_CT>();
        }
        [XmlElement("NgayLap_CT")]
        public string NgayLap_CT { get; set; }
        [XmlElement("NgayTruyen_CT")]
        public string NgayTruyen_CT { get; set; }
        [XmlElement("Ma_DV")]
        public string Ma_DV { get; set; }
        [XmlElement("Ma_Chuong")]
        public string Ma_Chuong { get; set; }
        [XmlElement("Ten_DV")]
        public string Ten_DV { get; set; }
        [XmlElement("Ma_KB")]
        public string Ma_KB { get; set; }
        [XmlElement("Ten_KB")]
        public string Ten_KB { get; set; }
        [XmlElement("TKKB")]
        public string TKKB { get; set; }
        [XmlElement("Ma_NTK")]
        public string Ma_NTK { get; set; }
        [XmlElement("Ma_HQ_PH")]
        public string Ma_HQ_PH { get; set; }
        [XmlElement("Ma_HQ_CQT")]
        public string Ma_HQ_CQT { get; set; }

        [XmlElement("KyHieu_CT")]
        public string KyHieu_CT { get; set; }
        [XmlElement("So_CT")]
        public string So_CT { get; set; }
        [XmlElement("Loai_CT")]
        public string Loai_CT { get; set; }
        [XmlElement("Ngay_BN")]
        public string Ngay_BN { get; set; }
        [XmlElement("Ngay_CT")]
        public string Ngay_CT { get; set; }
        [XmlElement("Ma_NT")]
        public string Ma_NT { get; set; }
        [XmlElement("Ty_Gia")]
        public string Ty_Gia { get; set; }
        [XmlElement("SoTien_TO")]
        public string SoTien_TO { get; set; }
        [XmlElement("DienGiai")]
        public string DienGiai { get; set; }
        [XmlElement("GNT_CT")]
        public List<GNT_CT> GNT_CT;

        public DataSet convertGNT_CTToDS()
        {
            DataSet ds = new DataSet();

            try
            {
                DataTable tbl = new DataTable();
                tbl.Columns.Add("ID_HS");
                tbl.Columns.Add("TTButToan");
                tbl.Columns.Add("Ma_HQ");
                tbl.Columns.Add("Ma_LH");
                tbl.Columns.Add("Nam_DK");
                tbl.Columns.Add("So_TK");
                tbl.Columns.Add("Ma_LT");
                tbl.Columns.Add("Ma_ST");
                tbl.Columns.Add("MTMUC");
                tbl.Columns.Add("NDKT");
                tbl.Columns.Add("SoTien_NT");
                tbl.Columns.Add("SoTien_VND");
                ds.Tables.Add(tbl);

                for (int i = 0; i < GNT_CT.Count; i++)
                {
                    for (int x = 0; x < GNT_CT[i].ToKhai_CT.Count; x++)
                    {
                        DataRow vrow = ds.Tables[0].NewRow();
                        vrow["ID_HS"] = GNT_CT[i].IS_HS;
                        vrow["TTButToan"]= GNT_CT[i].TTButToan;
                        vrow["Ma_HQ"]= GNT_CT[i].Ma_HQ;
                        vrow["Ma_LH"]= GNT_CT[i].Ma_LH;
                        vrow["So_TK"] = GNT_CT[i].So_TK;
                        vrow["Nam_DK"]= GNT_CT[i].Nam_DK;
                        vrow["Ma_LT"]= GNT_CT[i].Ma_LT;
                        vrow["Ma_ST"]= GNT_CT[i].ToKhai_CT[x].Ma_ST;
                        vrow["MTMUC"] = GNT_CT[i].ToKhai_CT[x].NDKT;
                        vrow["NDKT"] = Business_HQ.Common.mdlCommon.Get_TenMTM(GNT_CT[i].ToKhai_CT[x].NDKT);
                        vrow["SoTien_NT"] = GNT_CT[i].ToKhai_CT[x].SoTien_NT;
                        vrow["SoTien_VND"] = GNT_CT[i].ToKhai_CT[x].SoTien_VND; 
                        ds.Tables[0].Rows.Add(vrow);
                    }
                }

            }catch(Exception ex)
            {}
            return ds;
        }

    }
    
    public class GNT_CT
    {
        public GNT_CT()
        {
            ToKhai_CT = new List<ToKhai_CT>();     
        }
        [XmlElement("ID_HS")]
        public string IS_HS { get; set; }
        [XmlElement("TTButToan")]
        public string TTButToan { get; set; }
        [XmlElement("Ma_HQ")]
        public string Ma_HQ { get; set; }

        [XmlElement("Ma_LH")]
        public string Ma_LH { get; set; }
        [XmlElement("Nam_DK")]
        public string Nam_DK { get; set; }

        [XmlElement("Ngay_DK")]
        public string Ngay_DK { get; set; }
        [XmlElement("So_TK")]
        public string So_TK { get; set; }
        [XmlElement("Ma_LT")]
        public string Ma_LT { get; set; }
        [XmlElement("ToKhai_CT")]
        public List<ToKhai_CT> ToKhai_CT;
    }
    public class ToKhai_CT
    {
        public ToKhai_CT()
        {}
        [XmlElement("Ma_ST")]
        public string Ma_ST { get; set; }
        [XmlElement("NDKT")]
        public string NDKT { get; set; }
        [XmlElement("SoTien_NT")]
        public string SoTien_NT { get; set; }
        [XmlElement("SoTien_VND")]
        public string SoTien_VND { get; set; }

    }
    public class ThongTinGiaoDich
    {
        public ThongTinGiaoDich()
        { 
            NguoiNopTien=new NguoiNopTien();
            TaiKhoan_NopTien = new TaiKhoan_NopTien();

        }
        [XmlElement("NguoiNopTien")]
        public NguoiNopTien NguoiNopTien;
        [XmlElement("TaiKhoan_NopTien")]
        public TaiKhoan_NopTien TaiKhoan_NopTien;
    
    }
    public class NguoiNopTien
    {
        public NguoiNopTien()
        {

        }
        [XmlElement("Ma_ST")]
        public string Ma_ST { get; set; }
        [XmlElement("So_CMT")]
        public string So_CMT { get; set; }
        [XmlElement("Ten_NNT")]
        public string Ten_NNT { get; set; }
        [XmlElement("DiaChi")]
        public string DiaChi { get; set; }
        [XmlElement("TT_Khac")]
        public string TT_Khac { get; set; }

    }
    public class TaiKhoan_NopTien
    {
        public TaiKhoan_NopTien()
        {

        }
        [XmlElement("Ma_NH_TH")]
        public string Ma_NH_TH { get; set; }
        [XmlElement("Ten_NH_TH")]
        public string Ten_NH_TH { get; set; }
        [XmlElement("TaiKhoan_TH")]
        public string TaiKhoan_TH { get; set; }
        [XmlElement("Ten_TaiKhoan_TH")]
        public string Ten_TaiKhoan_TH { get; set; }

    }
    public class Data
    {
        public Data()
        {
            ThongTinChungTu = new ThongTinChungTu();
            ThongTinGiaoDich = new ThongTinGiaoDich();
        }
        [XmlElement("ThongTinChungTu")]
        public ThongTinChungTu ThongTinChungTu;
        [XmlElement("ThongTinGiaoDich")]
        public ThongTinGiaoDich ThongTinGiaoDich;
    }
    public class Error
    {
        public Error() { }
        public string ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }

    
}
