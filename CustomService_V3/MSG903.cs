﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using CustomsV3.MSG.MSGHeaderCustoms;
using System.IO;
using CustomsV3.MSG.MSG303;

namespace CustomsService
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG903
    {
        public MSG903()
        {
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Signature")]
        public Signature Signature;

        public string MSG903toXML(MSG903 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG303));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
    public class Data
    {
        [XmlElement(Order = 1)]
        public string Ma_NH_DC { get; set; }
        [XmlElement(Order = 2)]
        public string Ngay_DC
        { get; set; }
        [XmlElement(Order = 3)]
        public string Loai_TD_DC
        { get; set; }
        public Accept_Transactions Accept_Transactions { get; set; }
        public Reject_Transactions Reject_Transactions { get; set; }
    }

    public class Accept_Transactions
    {
        public Accept_Transactions()
        {
        }
        [XmlElement(Order = 1)]
        public List<Transaction> Transaction { get; set; }
    }

    public class Transaction
    {
        public Transaction()
        { }
        [XmlElement(Order = 1)]
        public string Transaction_ID { get; set; }
        [XmlElement(Order = 2)]
        public string So_TN_CT { get; set; }
        [XmlElement(Order = 3)]
        public string Ngay_TN_CT { get; set; }
    }



    public class Reject_Transactions
    {
        public Reject_Transactions()
        {
        }
        [XmlElement(Order = 1)]
        public List<Transaction> Transaction { get; set; }
    }

    public class Error
    {
        public Error()
        { }
        [XmlElement(Order = 1)]
        public string ErrorMessage { get; set; }

        [XmlElement(Order = 2)]
        public int ErrorNumber { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }

}
