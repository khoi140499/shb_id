﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.MSGError;

namespace CustomsV3.MSG.MSGDCVTDON
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSGDCVTDON
    {
        public MSGDCVTDON()
        {
        }
        [XmlElement("Header", Order = 1)]
        public HeaderCustoms Header;
        [XmlElement("Data", Order = 2)]
        public Data Data;
        [XmlElement("Error", Order = 3)]
        public Error Error;
        [XmlElement("Signature", Order = 4)]
        public Signature Signature;

        public string MSGDCVTDONtoXML(MSGDCVTDON p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSGDCVTDON));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }
        [XmlElement("Transactions")]
        public List<ItemData> Transactions { get; set; }
    }
   
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Transaction_ID { get; set; }
        public string So_TN_CT { get; set; }
        public string Ngay_TN_CT { get; set; }
        public string Ma_NH_PH { get; set; }
        public string MST_NH_PH { get; set; }
        public string Ten_NH_PH { get; set; }
        public string Ma_DV { get; set; }
        public string Ten_DV { get; set; }
        public string Ma_DV_DD { get; set; }
        public string Ten_DV_DD { get; set; }
        public string Ma_HQ_KB { get; set; }
        public string So_HD { get; set; }
        public string Ngay_HD { get; set; }
        public string So_VD_01 { get; set; }
        public string Ngay_VD_01 { get; set; }
        public string So_VD_02 { get; set; }
        public string Ngay_VD_02 { get; set; }
        public string So_VD_03 { get; set; }
        public string Ngay_VD_03 { get; set; }
        public string So_VD_04 { get; set; }
        public string Ngay_VD_04 { get; set; }
        public string So_VD_05 { get; set; }
        public string Ngay_VD_05 { get; set; }
        public string Loai_CT { get; set; }
        public string KyHieu_CT { get; set; }
        public string So_CT { get; set; }
        public string Ngay_CT { get; set; }
        public string TTButToan { get; set; }
        public string SNBL { get; set; }
        public string Ngay_HL { get; set; }
        public string Ngay_HHL { get; set; }
        public string SoTien { get; set; }
        public string DienGiai { get; set; }
    }


    public class Signature
    {
        public Signature() { }

    }

   }
