﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
namespace CustomsV3.MSG.MSG53
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG53
    {
        public MSG53()
        {
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Security")]
        public Security Security; 

        public string MSG53toXML(MSG53 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG53));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }
        public string Loai_TD_DC { get; set; }
        
    }

    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Request_ID { get; set; }

    }
    public class Security
    {
        public Security()
        {
        }
        public string Signature { get; set; }
    }
}
