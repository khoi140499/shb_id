﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG25
{
    // Thông điệp điều chỉnh tiền thuế bằng ngoại tệ
    [Serializable]
    [XmlRootAttribute("CUSTOMS", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG25
    {
        public MSG25()
        {
        }        
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Security")]
        public Security Security; 

        public string MSG25toXML(MSG25 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG25));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
    }
    public class Data
    {
        public Data()
        {
            Khoan_XK = "";
            TieuMuc_XK = "";
            DuNo_NT_XK = "0";
            DuNo_XK = "0";

            Khoan_NK = "";
            TieuMuc_NK = "";
            DuNo_NT_NK = "0";
            DuNo_NK = "0";

            Khoan_VA = "";
            TieuMuc_VA = "";
            DuNo_NT_VA = "0";
            DuNo_VA = "0";

            Khoan_TD = "";
            TieuMuc_TD = "";
            DuNo_NT_TD = "0";
            DuNo_TD = "0";

            Khoan_TV = "";
            TieuMuc_TV = "";
            DuNo_NT_TV = "0";
            DuNo_TV = "0";

            Khoan_MT = "";
            TieuMuc_MT = "";
            DuNo_NT_MT = "0";
            DuNo_MT = "0";

            Khoan_KH = "";
            TieuMuc_KH = "";
            DuNo_NT_KH = "0";
            DuNo_KH = "0";
        }
        public string Ma_NH_PH { get; set; }
        public string Ten_NH_PH { get; set; }
        public string Ma_NH_TH { get; set; }
        public string Ten_NH_TH { get; set; }
        public string Ma_DV { get; set; }
        public string Ma_Chuong { get; set; }
        public string Ten_DV { get; set; }
        public string Ma_HQ_PH { get; set; }
        public string Ma_HQ_CQT { get; set; }
        public string Ma_HQ { get; set; }
        public string Ma_LH { get; set; }
        public string Ten_LH { get; set; }
        public string So_TK { get; set; }
        public string Ngay_DK { get; set; }
        public string Ma_LT { get; set; }
        public string Ma_NTK { get; set; }
        public string Loai_CT { get; set; }
        public string So_TN_CTS { get; set; }
        public string Ngay_TN_CTS { get; set; }
        public string KyHieu_CT { get; set; }
        public string So_CT { get; set; }
        public string TTButToan { get; set; }
        public string Ma_KB { get; set; }
        public string Ten_KB { get; set; }
        public string TKKB { get; set; }
        public string TKKB_CT { get; set; }
        public string Ngay_BN { get; set; }
        public string Ngay_BC { get; set; }
        public string Ngay_CT { get; set; }
        public string Ma_NT { get; set; }
        public string Ty_Gia { get; set; }
        public string DienGiai { get; set; }
        public string Khoan_XK { get; set; }
        public string TieuMuc_XK { get; set; }
        public string DuNo_NT_XK { get; set; }
        public string DuNo_XK { get; set; }
        public string Khoan_NK { get; set; }
        public string TieuMuc_NK { get; set; }
        public string DuNo_NT_NK { get; set; }
        public string DuNo_NK { get; set; }
        public string Khoan_VA { get; set; }
        public string TieuMuc_VA { get; set; }
        public string DuNo_NT_VA { get; set; }
        public string DuNo_VA { get; set; }
        public string Khoan_TD { get; set; }
        public string TieuMuc_TD { get; set; }
        public string DuNo_NT_TD { get; set; }
        public string DuNo_TD { get; set; }
        public string Khoan_TV { get; set; }
        public string TieuMuc_TV { get; set; }
        public string DuNo_NT_TV { get; set; }
        public string DuNo_TV { get; set; }
        public string Khoan_MT { get; set; }
        public string TieuMuc_MT { get; set; }
        public string DuNo_NT_MT { get; set; }
        public string DuNo_MT { get; set; }
        public string Khoan_KH { get; set; }
        public string TieuMuc_KH { get; set; }
        public string DuNo_NT_KH { get; set; }
        public string DuNo_KH { get; set; }
        public string DuNo_NT_TO { get; set; }
        public string DuNo_TO { get; set; }
    }    
    public class Security
    {
        public Security()
        {
        }
        public string Signature { get; set; }
    }
}
