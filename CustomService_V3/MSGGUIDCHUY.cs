﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
namespace CustomsV3.MSG.MSGGUIDCHUY
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSGGUIDCHUY
    {
        public MSGGUIDCHUY()
        {
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Security")]
        public Security Security;

        public string MSGGUIDCHUYtoXML(MSGGUIDCHUY p_MsgIn)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(MSGGUIDCHUY));
                StringWriter sw = new StringWriter();
                serializer.Serialize(sw, p_MsgIn);
                string strTemp = sw.ToString();
                return strTemp;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
    public class Data
    {
        public Data()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_NH_DC { get; set; }
        [XmlElement(Order = 2)]
        public string Ngay_DC { get; set; }
        [XmlElement(Order = 3)]
        public string Loai_TD_DC { get; set; }
        [XmlElement(Order = 4)]
        public Accept_Transactions Accept_Transactions { get; set; }
        [XmlElement(Order = 5)]
        public Reject_Transactions Reject_Transactions { get; set; }
    }

    public class Accept_Transactions
    {
        public Accept_Transactions()
        {
        }
        [XmlElement(Order = 1)]
        public List<Transaction> Transaction {get;set;}
    }

    public class Reject_Transactions
    {
        public Reject_Transactions()
        {
        }
        [XmlElement(Order = 1)]
        public List<Transaction> Transaction { get; set; }
    }
    public class Transaction
    {
        public Transaction()
        {
        }
        [XmlElement(Order = 1)]
        public string Transaction_ID { get; set; }
        [XmlElement(Order = 2)]
        public string So_TN_CT { get; set; }
        [XmlElement(Order = 3)]
        public string Ngay_TN_CT { get; set; }

    }


}
