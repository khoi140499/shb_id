﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
using CustomsV3.MSG.SECURITY;

namespace CustomsV3.MSG.MSG104
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG104
    {
        public MSG104()
        {
        }
        [XmlElement("Header", Order = 1)]
        public Header Header { get; set; }
        [XmlElement("Data", Order = 2)]
        public Data Data;
        [XmlElement("Error", Order = 3)]
        public Error Error;
        [XmlElement("Signature", Order = 4)]
        public Signature Signature;

        public string MSG104toXML(MSG104 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG104));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG104 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG104.MSG104));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG104.MSG104 LoadedObjTmp = (CustomsV3.MSG.MSG104.MSG104)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_DV { get; set; }
        public string KyHieu_CT { get; set; }
        public string So_CT { get; set; }
    }

    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
}
