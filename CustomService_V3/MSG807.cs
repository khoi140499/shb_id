﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
//using CustomsV3.MSG.MSGHeader;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.MSGError;
using CustomsV3.MSG.SECURITY;


namespace CustomsV3.MSG.MSG807
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG807
    {

        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("DigitalSignatures")]
        public DigitalSignatures DigitalSignatures;
        public MSG807()
        {
            DigitalSignatures = new DigitalSignatures();
            Data = new Data();
        }
        public string MSG807toXML(MSG807 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG807));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }

    }
    public class Data
    {
        public Data()
        {
           Error = new Error();
        }
        [XmlElement("Ma_NH_DC")]
        public string Ma_NH_DC { get; set; }
        [XmlElement("Ngay_DC")]
        public string Ngay_DC { get; set; }
        [XmlElement("Transactions")]
        public List<Transactions> Transactions { get; set; }
        [XmlElement("Error")]
        public Error Error { get; set; }
    }
    public class Transactions
    {
        public Transactions()
        {
        }
        [XmlElement(Order = 1)]
        public string Transaction_ID { get; set; }
        [XmlElement(Order = 2)]
        public ThongTinChungTu ThongTinChungTu { get; set; }
        [XmlElement(Order = 3)]
        public ThongTinGiaoDich ThongTinGiaoDich { get; set; }
    }
    public class Error
    {
        public Error()
        {
            ErrorNumber = "0";
            ErrorMessage = "Thanh cong";
        }
        [XmlElement(Order = 1)]
        public string ErrorMessage { get; set; }
        [XmlElement(Order = 2)]
        public string ErrorNumber { get; set; }
    }
    public class ThongTinChungTu
    {
        public ThongTinChungTu()
        {
        }
        [XmlElement(Order = 1)]
        public string NgayLap_CT { get; set; }
        [XmlElement(Order = 2)]
        public string NgayTruyen_CT { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_DV { get; set; }
        [XmlElement(Order = 4)]
        public string Ma_Chuong { get; set; }
        [XmlElement(Order = 5)]
        public string Ten_DV { get; set; }
        [XmlElement(Order = 6)]
        public string Ma_KB { get; set; }
        [XmlElement(Order = 7)]
        public string Ten_KB { get; set; }
        [XmlElement(Order = 8)]
        public string TKKB { get; set; }
        [XmlElement(Order = 9)]
        public string Ma_NTK { get; set; }
        [XmlElement(Order = 10)]
        public string Ma_HQ_PH { get; set; }
        [XmlElement(Order = 11)]
        public string Ma_HQ_CQT { get; set; }
        [XmlElement(Order = 12)]
        public string KyHieu_CT { get; set; }
        [XmlElement(Order = 13)]
        public string So_CT { get; set; }
        [XmlElement(Order = 14)]
        public string Loai_CT { get; set; }
        [XmlElement(Order = 15)]
        public string Ngay_BN { get; set; }
        [XmlElement(Order = 16)]
        public string Ngay_CT { get; set; }
        [XmlElement(Order = 17)]
        public string Ma_NT { get; set; }
        [XmlElement(Order = 18)]
        public string Ty_Gia { get; set; }
        [XmlElement(Order = 19)]
        public string SoTien_TO { get; set; }
        [XmlElement(Order = 20)]
        public string DienGiai { get; set; }
        [XmlElement(Order = 21)]
        public List<GNT_CT> GNT_CT { get; set; }
    }
    public class GNT_CT
    {
        public GNT_CT()
        {
        }
        [XmlElement(Order = 1)]
        public string ID_HS { get; set; }
        [XmlElement(Order = 2)]
        public string TTButToan { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_HQ { get; set; }
        [XmlElement(Order = 4)]
        public string Ma_LH { get; set; }
        [XmlElement(Order = 5)]
        public string Nam_DK { get; set; }
        [XmlElement(Order = 6)]
        public string So_TK { get; set; }
        [XmlElement(Order = 7)]
        public string Ma_LT { get; set; }
        [XmlElement(Order = 8)]
        public List<ToKhai_CT> ToKhai_CT { get; set; }
    }
    public class ToKhai_CT
    {
        public ToKhai_CT()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_ST { get; set; }
        [XmlElement(Order = 2)]
        public string NDKT { get; set; }
        [XmlElement(Order = 3)]
        public string SoTien_NT { get; set; }
        [XmlElement(Order = 4)]
        public string SoTien_VND { get; set; }
    }
    public class ThongTinGiaoDich
    {
        public ThongTinGiaoDich()
        {
        }
        [XmlElement(Order = 1)]
        public NguoiNopTien NguoiNopTien { get; set; }
        [XmlElement(Order = 2)]
        public TaiKhoan_NopTien TaiKhoan_NopTien { get; set; }
    }
    public class NguoiNopTien
    {
        public NguoiNopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_ST { get; set; }
        [XmlElement(Order = 2)]
        public string So_CMT { get; set; }
        [XmlElement(Order = 3)]
        public string Ten_NNT { get; set; }
        [XmlElement(Order = 4)]
        public string DiaChi { get; set; }
        [XmlElement(Order = 5)]
        public string TT_Khac { get; set; }
    }
    public class TaiKhoan_NopTien
    {
        public TaiKhoan_NopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_NH_TH { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_NH_TH { get; set; }
        [XmlElement(Order = 3)]
        public string TaiKhoan_TH { get; set; }
        [XmlElement(Order = 4)]
        public string Ten_TaiKhoan_TH { get; set; }
    }
    public class DigitalSignatures
    {

    }
    public class Signature
    {
        public Signature() { }
    }
}
