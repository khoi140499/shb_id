﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;

namespace CustomsV3.MSG.MSG106
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG106
    {
        public MSG106()
        {
        }
        [XmlElement("Header", Order=1)]
        public Header Header;
        [XmlElement("Data", Order = 2)]
        public Data Data;
        [XmlElement("Signature", Order=3)]
        public Signature Signature;        

        public string MSG106toXML(MSG106 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG106));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG106 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG106.MSG106));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG106.MSG106 LoadedObjTmp = (CustomsV3.MSG.MSG106.MSG106)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Loai_DM { get; set; }
        public string Ma_DV { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
   
}
