﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
using CustomsV3.MSG.SECURITY;
namespace CustomsV3.MSG.MSG49
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG49
    {
        public MSG49()
        {
        }
        [XmlElement("Header")]
        public Header Header { get; set; }
        [XmlElement("Data")]
        public Data Data { get; set; }       
        [XmlElement("Security")]
        public Security Security { get; set; } 

        public string MSG49toXML(MSG49 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG49));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }
        [XmlElement("Transaction")]
        public List<Transaction> Transaction { get; set; }        
    }
    public class Transaction
    {

        public Transaction()
        {
        }
        [XmlElement(Order = 1)]
        public string Transaction_ID { get; set; }
        [XmlElement(Order = 2)]
        public string So_TN_CT { get; set; }
        [XmlElement(Order = 3)]
        public string Ngay_TN_CT { get; set; }
        [XmlElement(Order = 4)]
        public string Ma_NH_PH { get; set; }
        [XmlElement(Order =5)]
        public string Ten_NH_PH { get; set; }
        [XmlElement(Order = 6)]
        public string Ma_NH_TH { get; set; }
        [XmlElement(Order = 7)]
        public string Ten_NH_TH { get; set; }
        [XmlElement(Order = 8)]
        public string Ma_DV { get; set; }
        [XmlElement(Order = 9)]
        public string Ma_Chuong { get; set; }
        [XmlElement(Order = 10)]
        public string Ten_DV { get; set; }
        [XmlElement(Order = 11)]
        public string Ma_KB { get; set; }
        [XmlElement(Order = 12)]
        public string Ten_KB { get; set; }
        [XmlElement(Order = 13)]
        public string TKKB { get; set; }
        [XmlElement(Order = 14)]
        public string Ma_NTK { get; set; }
        [XmlElement(Order = 15)]
        public string Ma_HQ_PH { get; set; }
        [XmlElement(Order = 16)]
        public string Ma_HQ_CQT { get; set; }
        [XmlElement(Order = 17)]
        public string KyHieu_CT { get; set; }
        [XmlElement(Order = 18)]
        public string So_CT { get; set; }
        [XmlElement(Order = 19)]
        public string Loai_CT { get; set; }
        [XmlElement(Order = 20)]
        public string Ngay_BN { get; set; }
        [XmlElement(Order = 21)]
        public string Ngay_BC { get; set; }
        [XmlElement(Order = 22)]
        public string Ngay_CT { get; set; }
        [XmlElement(Order = 23)]
        public string Ma_NT { get; set; }
        [XmlElement(Order = 24)]
        public string Ty_Gia { get; set; }
        [XmlElement(Order = 25)]
        public string SoTien_TO { get; set; }
        [XmlElement(Order = 26)]
        public string DienGiai { get; set; }
        [XmlElement(Order = 27)]
        public List<GNT_CT> GNT_CT { get; set; } 

    }
    public class GNT_CT
    {
        public GNT_CT()
        {
        }
        [XmlElement(Order = 1)]
        public string TTButToan { get; set; }
        [XmlElement(Order = 2)]
        public string Ma_HQ { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_LH { get; set; }
        [XmlElement(Order = 4)]
        public string Nam_DK { get; set; }
        [XmlElement(Order = 5)]
        public string So_TK { get; set; }
        [XmlElement(Order = 6)]
        public string Ma_LT { get; set; }
        [XmlElement(Order = 7)]
        public List<ToKhai_CT> ToKhai_CT { get; set; } 
    }
    public class ToKhai_CT
    {
        public ToKhai_CT()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_ST { get; set; }
        [XmlElement(Order = 2)]
        public string NDKT { get; set; }
        
        [XmlElement(Order = 5)]
        public string SoTien_NT { get; set; }
        [XmlElement(Order = 6)]
        public string SoTien { get; set; }
    }
}
