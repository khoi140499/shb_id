﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
using CustomsV3.MSG.SECURITY;
namespace CustomsV3.MSG.MSGKQDCPHI
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSGKQDCPHI
    {
        public MSGKQDCPHI()
        {
        }
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Security")]
        public Security Security;

        public MSGKQDCPHI MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSGKQDCPHI.MSGKQDCPHI));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSGKQDCPHI.MSGKQDCPHI LoadedObjTmp = (CustomsV3.MSG.MSGKQDCPHI.MSGKQDCPHI)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Data
    {
        public Data()
        {
        }
        public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }
        [XmlElement("Transactions")]
        public List<Transaction> Transactions { get; set; }     
    }
    public class Transaction
    {

        public Transaction()
        {
        }
        [XmlElement(Order = 1)]
        public string Transaction_ID { get; set; }
        [XmlElement(Order = 2)]
        public string So_TN_CT { get; set; }
        [XmlElement(Order = 3)]
        public string Ngay_TN_CT { get; set; }
        [XmlElement(Order = 4)]
        public string Ma_NH_PH { get; set; }
        [XmlElement(Order = 5)]
        public string Ten_NH_PH { get; set; }
        [XmlElement(Order = 6)]
        public string KyHieu_CT { get; set; }
        [XmlElement(Order = 7)]
        public string So_CT { get; set; }
        [XmlElement(Order = 8)]
        public string Ngay_CT { get; set; }
        [XmlElement(Order = 9)]
        public string Ngay_BN { get; set; }
        [XmlElement(Order = 10)]
        public string Ngay_BC { get; set; }
        [XmlElement(Order = 11)]
        public string So_HS { get; set; }
        [XmlElement(Order = 12)]
        public string Ma_DVQL { get; set; }
        [XmlElement(Order = 13)]
        public string Ten_DVQL { get; set; }
        [XmlElement(Order = 14)]
        public string KyHieu_CT_PT { get; set; }
        [XmlElement(Order = 15)]
        public string So_CT_PT { get; set; }
        [XmlElement(Order = 16)]
        public string Nam_CT_PT { get; set; }
        [XmlElement(Order = 17)]
        public NguoiNopTien NguoiNopTien { get; set; }
        [XmlElement(Order = 18)]
        public ThongTin_NopTien ThongTin_NopTien { get; set; }
        [XmlElement(Order = 19)]
        public List<ChungTu_CT> ChungTu_CT;
        [XmlElement(Order = 20)]
        public TaiKhoan_NopTien TaiKhoan_NopTien{ get; set; } 
    }
    public class NguoiNopTien
    {
        public NguoiNopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_ST { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_NNT { get; set; }
        [XmlElement(Order = 3)]
        public string DiaChi { get; set; }
        [XmlElement(Order = 4)]
        public string TT_Khac { get; set; }       
    }
    public class ThongTin_NopTien
    {
        public ThongTin_NopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_NT { get; set; }
        [XmlElement(Order = 2)]
        public string TyGia { get; set; }
        [XmlElement(Order = 3)]
        public string TongTien_NT { get; set; }
        [XmlElement(Order = 4)]
        public string TongTien_VND { get; set; }   
    }
    public class ChungTu_CT
    {
        public ChungTu_CT()
        {
        }
        [XmlElement(Order = 1)]
        public string STT { get; set; }
        [XmlElement(Order = 2)]
        public string NDKT { get; set; }
        [XmlElement(Order = 3)]
        public string Ten_NDKT { get; set; }
        [XmlElement(Order = 4)]
        public string SoTien_NT { get; set; }
        [XmlElement(Order = 5)]
        public string SoTien_VND { get; set; }
        [XmlElement(Order = 6)]
        public string GhiChu { get; set; }
    }
    public class TaiKhoan_NopTien
    {
        public TaiKhoan_NopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_NH_TH { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_NH_TH { get; set; }
        [XmlElement(Order = 3)]
        public string TaiKhoan_TH { get; set; }
        [XmlElement(Order = 4)]
        public string Ten_TaiKhoan_TH { get; set; }
    }

     public class TCS_KQDC_PHI_BO_NGANH
    {
         public TCS_KQDC_PHI_BO_NGANH()
         {
         }
        public string Ma_NH_DC { get; set; }
        public string  Ngay_DC { get; set; }//to_date()
        public string Transaction_ID { get; set; }
        public string So_TN_CT { get; set; }
        public string Ngay_TN_CT { get; set; }
        //        MA_NH_PH
        public string MA_NH_PH { get; set; }

        //TEN_NH_PH
        public string TEN_NH_PH { get; set; }
        //KYHIEU_CT
        public string KYHIEU_CT { get; set; }
        //SO_CT
        public string SO_CT { get; set; }

        //NGAY_CT
        public DateTime NGAY_CT { get; set; }

        //NGAY_BN
        public DateTime NGAY_BN { get; set; }
        //NGAY_BC
        public DateTime NGAY_BC { get; set; }

        //SO_HS
        public int SO_HS { get; set; }
        //MA_DVQL
        public string MA_DVQL { get; set; }
        //TEN_DVQL
        public string TEN_DVQL { get; set; }
        //KYHIEU_CT_PT
        public string KYHIEU_CT_PT { get; set; }
        //SO_CT_PT
        public string SO_CT_PT { get; set; }
        //NAM_CT_PT
        public int NAM_CT_PT { get; set; }
        //NNT_MST
        public string NNT_MST { get; set; }
        //NNT_TEN_DV
        public string NNT_TEN_DV { get; set; }

        //NNT_DIACHI
        public string NNT_DIACHI { get; set; }
        //NNT_TT_KHAC
        public string NNT_TT_KHAC { get; set; }
        //TT_NT_MA_NT
        public string TT_NT_MA_NT { get; set; }
        //TT_NT_TYGIA
        public double TT_NT_TYGIA { get; set; }
        //TT_NT_TONGTIEN_NT
        public double TT_NT_TONGTIEN_NT { get; set; }
        //TT_NT_TONGTIEN_VND
        public double TT_NT_TONGTIEN_VND { get; set; }
        //CHUNGTU_CT_STT
        public string CHUNGTU_CT_STT { get; set; }
        //CHUNGTU_CT_NDKT
        public string CHUNGTU_CT_NDKT { get; set; }

        //CHUNGTU_CT_NDKT
        public string CHUNGTU_CT_TEN_NDKT { get; set; }

        //CHUNGTU_CT_SOTIEN_NT
        public double CHUNGTU_CT_SOTIEN_NT { get; set; }
        //CHUNGTU_CT_SOTIEN_VND
        public double CHUNGTU_CT_SOTIEN_VND { get; set; }
        //CHUNGTU_CT_GHICHU
        public string CHUNGTU_CT_GHICHU { get; set; }
        //TAIKHOAN_NT_MA_NH_TH
        public string TAIKHOAN_NT_MA_NH_TH { get; set; }
        //TAIKHOAN_NT_TEN_NH_TH
        public string TAIKHOAN_NT_TEN_NH_TH { get; set; }
        //TAIKHOAN_NT_TK_TH
        public string TAIKHOAN_NT_TK_TH { get; set; }
        //TAIKHOAN_NT_TEN_TK_TH
        public string TAIKHOAN_NT_TEN_TK_TH { get; set; }

    }
}
