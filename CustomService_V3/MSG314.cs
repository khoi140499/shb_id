﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
//using System.Data.OracleClient;
using Business_HQ.nhothu;
using System.Data;
namespace CustomsV3.MSG.MSG314
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG314
    {
        
        public MSG314()
        {
            DigitalSignatures = new DigitalSignatures();
        }
        public MSG314(string pvStrSO_HS)
        {
            try
            {
                TRANGTHAI = "-1";
                DataSet ds = daTCS_HQ247_314.gf_get314InfoToObj(pvStrSO_HS);
                if(ds!=null)
                    if(ds.Tables.Count>0)
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            Document = new Document();
                            Document.Data = new Data();
                            Document.Data.So_HS = ds.Tables[0].Rows[0]["So_HS"].ToString();
                            Document.Data.Loai_HS = ds.Tables[0].Rows[0]["Loai_HS"].ToString();
                            Document.Data.Ma_DV = ds.Tables[0].Rows[0]["Ma_DV"].ToString();
                            Document.Data.Ten_DV = ds.Tables[0].Rows[0]["Ten_DV"].ToString();
                            Document.Data.Ngay_HL_UQ = ds.Tables[0].Rows[0]["Ngay_HL_UQ"].ToString();
                            Document.Data.Ngay_HHL_UQ = ds.Tables[0].Rows[0]["Ngay_HHL_UQ"].ToString();
                            Document.Data.DiaChi = ds.Tables[0].Rows[0]["DiaChi"].ToString();
                            Document.Data.ThongTin_NNT = new ThongTin_NNT();
                            Document.Data.ThongTin_NNT.Ho_Ten = ds.Tables[0].Rows[0]["Ho_Ten"].ToString();
                            Document.Data.ThongTin_NNT.NgaySinh = ds.Tables[0].Rows[0]["NgaySinh"].ToString();
                            Document.Data.ThongTin_NNT.NguyenQuan = ds.Tables[0].Rows[0]["NguyenQuan"].ToString();
                            Document.Data.ThongTin_NNT.So_CMT = ds.Tables[0].Rows[0]["So_CMT"].ToString();
                            Document.Data.ThongTin_NNT.ThongTinLienHe = new List<ThongTinLienHe>();
                            StringReader strReader = new StringReader("<data>" + ds.Tables[0].Rows[0]["THONGTINLIENHE"].ToString() + "</data>");
                            DataSet dsTMP = new DataSet();
                            dsTMP.ReadXml(strReader);
                            if (dsTMP.Tables.Count > 0)
                                if (dsTMP.Tables[0].Rows.Count > 0)
                                {
                                    for (int i = 0; i < dsTMP.Tables[0].Rows.Count; i++)
                                    {
                                        CustomsV3.MSG.MSG314.ThongTinLienHe item = new CustomsV3.MSG.MSG314.ThongTinLienHe();
                                        item.Email = dsTMP.Tables[0].Rows[i]["Email"].ToString();
                                        item.So_DT = dsTMP.Tables[0].Rows[i]["So_DT"].ToString();
                                        Document.Data.ThongTin_NNT.ThongTinLienHe.Add(item);
                                    }
                                }
                            Document.Data.ThongTin_NNT.ChungThuSo = new CustomsV3.MSG.MSG314.ChungThuSo();
                            Document.Data.ThongTin_NNT.ChungThuSo.SerialNumber = ds.Tables[0].Rows[0]["SERIALNUMBER"].ToString();
                            Document.Data.ThongTin_NNT.ChungThuSo.Noi_Cap = ds.Tables[0].Rows[0]["NOICAP"].ToString();
                            Document.Data.ThongTin_NNT.ChungThuSo.Ngay_HL = ds.Tables[0].Rows[0]["NGAY_HL"].ToString();
                            Document.Data.ThongTin_NNT.ChungThuSo.Ngay_HHL = ds.Tables[0].Rows[0]["NGAY_HHL"].ToString();
                            Document.Data.ThongTin_NNT.ChungThuSo.PublicKey = ds.Tables[0].Rows[0]["PUBLICKEY"].ToString();
                            Document.Data.ThongTin_NNT.ThongTinTaiKhoan = new CustomsV3.MSG.MSG314.ThongTinTaiKhoan();
                            Document.Data.ThongTin_NNT.ThongTinTaiKhoan.Ma_NH_TH = ds.Tables[0].Rows[0]["MA_NH_TH"].ToString();
                            Document.Data.ThongTin_NNT.ThongTinTaiKhoan.Ten_NH_TH = ds.Tables[0].Rows[0]["TEN_NH_TH"].ToString();
                            Document.Data.ThongTin_NNT.ThongTinTaiKhoan.TaiKhoan_TH = ds.Tables[0].Rows[0]["TAIKHOAN_TH"].ToString();
                            Document.Data.ThongTin_NNT.ThongTinTaiKhoan.Ten_TaiKhoan_TH = ds.Tables[0].Rows[0]["TEN_TAIKHOAN_TH"].ToString();
                        }
            }
            catch (Exception ex)
            { }
            
        }
        [XmlElement("Document")]
        public Document Document;
        [XmlElement("DigitalSignatures")]
        public DigitalSignatures DigitalSignatures;
        public MSG314 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG314));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG314 LoadedObjTmp = (MSG314)serializer.Deserialize(reader);
            if (LoadedObjTmp.Document.Data.ThongTin_NNT == null)
            {
                LoadedObjTmp = LoadManual(p_XML, LoadedObjTmp);
            }
            return LoadedObjTmp;

        }
        private MSG314 LoadManual(string p_XML, MSG314 LoadedObjTmp)
        {
            MSG314 obj = LoadedObjTmp;
            XmlDocument vdoc = new XmlDocument();
            string strXMLTemp = p_XML.Replace(CustomsServiceV3.ProcessMSG.strXMLdefin2, "<Customs>");
            vdoc.LoadXml(strXMLTemp);
            obj.Document.Data.ThongTin_NNT = new ThongTin_NNT();

            //XmlNode vNode = vdoc.SelectSingleNode("/Customs/Data/ThongTin_NTT/So_CMT");
            string str = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT").InnerXml;
            StringReader strReader = new StringReader("<ThongTin_NTT>" + str + "</ThongTin_NTT>");
            DataSet ds = new DataSet();
            ds.ReadXml(strReader);
            obj.Document.Data.ThongTin_NNT.So_CMT = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT/So_CMT").InnerText;
            obj.Document.Data.ThongTin_NNT.Ho_Ten = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT/Ho_Ten").InnerText;
            obj.Document.Data.ThongTin_NNT.NgaySinh = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT/NgaySinh").InnerText;
            obj.Document.Data.ThongTin_NNT.NguyenQuan = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT/NguyenQuan").InnerText;
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables["ThongTinLienHe"] != null)
                {
                    obj.Document.Data.ThongTin_NNT.ThongTinLienHe = new List<ThongTinLienHe>();
                    for (int i = 0; i < ds.Tables["ThongTinLienHe"].Rows.Count; i++)
                    {
                        ThongTinLienHe objtmp = new ThongTinLienHe();
                        objtmp.So_DT = ds.Tables["ThongTinLienHe"].Rows[i]["So_DT"].ToString();
                        objtmp.Email = ds.Tables["ThongTinLienHe"].Rows[i]["Email"].ToString();
                        obj.Document.Data.ThongTin_NNT.ThongTinLienHe.Add(objtmp);
                    }
                }
                if (ds.Tables["ChungThuSo"] != null && ds.Tables["ChungThuSo"].Rows.Count > 0)
                {
                    ChungThuSo objtmp = new ChungThuSo();
                    objtmp.SerialNumber = ds.Tables["ChungThuSo"].Rows[0]["SerialNumber"].ToString();
                    objtmp.Noi_Cap = ds.Tables["ChungThuSo"].Rows[0]["Noi_Cap"].ToString();
                    objtmp.Ngay_HL = ds.Tables["ChungThuSo"].Rows[0]["Ngay_HL"].ToString();
                    objtmp.Ngay_HHL = ds.Tables["ChungThuSo"].Rows[0]["Ngay_HHL"].ToString();
                    objtmp.PublicKey = ds.Tables["ChungThuSo"].Rows[0]["PublicKey"].ToString();
                    obj.Document.Data.ThongTin_NNT.ChungThuSo = objtmp;

                }
                if (ds.Tables["ThongTinTaiKhoan"] != null && ds.Tables["ThongTinTaiKhoan"].Rows.Count > 0)
                {
                    ThongTinTaiKhoan objtmp = new ThongTinTaiKhoan();
                    objtmp.Ma_NH_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["Ma_NH_TH"].ToString();
                    objtmp.Ten_NH_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["Ten_NH_TH"].ToString();
                    objtmp.TaiKhoan_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["TaiKhoan_TH"].ToString();
                    objtmp.Ten_TaiKhoan_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["Ten_TaiKhoan_TH"].ToString();
                    obj.Document.Data.ThongTin_NNT.ThongTinTaiKhoan = objtmp;

                }

            }

            return obj;

        }

        public string MSG314toXML(MSG314 p_MsgIn)
        {

            XmlSerializer serializer = new XmlSerializer(typeof(MSG314));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public string getSoDT()
        {
            try
            {
                if (Document != null)
                {
                    if (Document.Data != null)
                    {
                        if (Document.Data.ThongTin_NNT != null)
                        {
                            if (Document.Data.ThongTin_NNT.ThongTinLienHe != null)
                            {
                                foreach (ThongTinLienHe objx in Document.Data.ThongTin_NNT.ThongTinLienHe)
                                {
                                    if (objx.So_DT.Trim().Length > 0)
                                    {
                                        return objx.So_DT.Trim();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return "";
        }
        public string getEmail()
        {
            try
            {
                if (Document != null)
                {
                    if (Document.Data != null)
                    {
                        if (Document.Data.ThongTin_NNT != null)
                        {
                            if (Document.Data.ThongTin_NNT.ThongTinLienHe != null)
                            {
                                foreach (ThongTinLienHe objx in Document.Data.ThongTin_NNT.ThongTinLienHe)
                                {
                                    if (objx.So_DT.Trim().Length > 0)
                                    {
                                        return objx.Email.Trim();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return "";
        }
        public string GetThongTinLienHe()
        {
            string strTT = "";
            try
            {
                if (Document != null)
                {
                    if (Document.Data != null)
                    {
                        if (Document.Data.ThongTin_NNT != null)
                        {
                            if (Document.Data.ThongTin_NNT.ThongTinLienHe != null)
                            {
                                foreach (ThongTinLienHe objx in Document.Data.ThongTin_NNT.ThongTinLienHe)
                                {
                                    strTT += "<ThongTinLienHe><So_DT>"+ objx.So_DT +"</So_DT><Email>"+ objx.Email +"</Email></ThongTinLienHe>";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return strTT;
        }
        public string TRANGTHAI { get; set; }
       
    }
    public class Document
    {
        public Document()
        {}
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
     }
    public class Data
    {
        public Data()
        {  }
        [XmlElement(ElementName = "So_HS", Order = 1)] 
        public string So_HS { get; set; }
        [XmlElement(ElementName = "Loai_HS", Order = 2)] 
        public string Loai_HS { get; set; }
        [XmlElement(ElementName = "Ma_DV", Order = 3)] 
        public string Ma_DV { get; set; }
        [XmlElement(ElementName = "Ten_DV", Order = 4)] 
        public string Ten_DV { get; set; }
        [XmlElement(ElementName = "DiaChi", Order = 5)] 
        public string DiaChi { get; set; }
        [XmlElement(ElementName = "Ngay_HL_UQ", Order = 6)] 
        public string Ngay_HL_UQ { get; set; }
        [XmlElement(ElementName = "Ngay_HHL_UQ", Order = 7)] 
        public string Ngay_HHL_UQ { get; set; }
        [XmlElement(ElementName = "ThongTin_NNT", Order = 8)] 
        public ThongTin_NNT ThongTin_NNT;
      
    }
    public class ThongTin_NNT
    {
        public ThongTin_NNT()
        { }
        [XmlElement(ElementName = "So_CMT", Order = 1)] 
        public string So_CMT { get; set; }
        [XmlElement(ElementName = "Ho_Ten", Order = 2)] 
        public string Ho_Ten { get; set; }
        [XmlElement(ElementName = "NgaySinh", Order = 3)] 
        public string NgaySinh { get; set; }
        [XmlElement(ElementName = "NguyenQuan", Order = 4)] 
        public string NguyenQuan { get; set; }
        [XmlElement(ElementName = "ThongTinLienHe", Order = 5)] 
        public List<ThongTinLienHe> ThongTinLienHe;
        [XmlElement(ElementName = "ChungThuSo", Order = 6)] 
        public ChungThuSo ChungThuSo ;
        [XmlElement(ElementName = "ThongTinTaiKhoan", Order = 7)] 
        public ThongTinTaiKhoan ThongTinTaiKhoan ;
    }
    public class ThongTinLienHe
    {
        public ThongTinLienHe()
        {
        }
        [XmlElement(ElementName = "So_DT", Order = 1)] 
        public string So_DT { get; set; }
        [XmlElement(ElementName = "Email", Order = 2)] 
        public string Email { get; set; }
    }
    public class ChungThuSo
    {
        public ChungThuSo()
        {
        }
        [XmlElement(ElementName = "SerialNumber", Order = 1)] 
        public string SerialNumber { get; set; }
        [XmlElement(ElementName = "Noi_Cap", Order = 2)] 
        public string Noi_Cap { get; set; }
        [XmlElement(ElementName = "Ngay_HL", Order = 3)] 
        public string Ngay_HL { get; set; }
        [XmlElement(ElementName = "Ngay_HHL", Order = 4)] 
        public string Ngay_HHL { get; set; }
        [XmlElement(ElementName = "PublicKey", Order = 5)] 
        public string PublicKey { get; set; }
    }
    public class ThongTinTaiKhoan
    {
        public ThongTinTaiKhoan()
        {
        }
        [XmlElement(ElementName = "Ma_NH_TH", Order = 1)] 
        public string Ma_NH_TH { get; set; }
        [XmlElement(ElementName = "Ten_NH_TH", Order = 2)] 
        public string Ten_NH_TH { get; set; }
        [XmlElement(ElementName = "TaiKhoan_TH", Order = 3)] 
        public string TaiKhoan_TH { get; set; }
        [XmlElement(ElementName = "Ten_TaiKhoan_TH", Order = 4)] 
        public string Ten_TaiKhoan_TH { get; set; }
       
    }

    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }
    public class DigitalSignatures
    {
        public DigitalSignatures() { }
    }
  
}
