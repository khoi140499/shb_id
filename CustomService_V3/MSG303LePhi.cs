﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
using System.Reflection;
using System.Data.OracleClient;
using System.Data;
using CustomsServiceV3;
using CustomsServiceV3.Extension;
using VBOracleLib;
using System.Diagnostics;
using CustomsV3.MSG.MSGError;
using CustomsV3.MSG.MSGHeaderCustoms;



namespace CustomsV3.MSG.MSG303LePhi
{
    [Serializable]
    [XmlRootAttribute("Customs")]
    public class MSG303LePhi
    {
        public MSG303LePhi()
        {
        }
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Security")]
        public Security Security;

        public string MSG303LePhitoXML(MSG303LePhi p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG303LePhi.MSG303LePhi));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }

        public MSG303LePhi MSGLePhiToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG303LePhi.MSG303LePhi));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);
            CustomsV3.MSG.MSG303LePhi.MSG303LePhi LoadedObjTmp = null;
            try
            {
                XmlReader reader = XmlReader.Create(new StringReader(p_XML));
                LoadedObjTmp = (CustomsV3.MSG.MSG303LePhi.MSG303LePhi)serializer.Deserialize(reader);

            }
            catch (Exception)
            {

                throw;
            }
            return LoadedObjTmp;

        }
    }


    public class Data
    {
        [XmlElement(Order = 1)]
        public string So_HS { get; set; }
        [XmlElement(Order = 2)]
        public string Ma_DVQL { get; set; }
        [XmlElement(Order = 3)]
        public string Ten_DVQL { get; set; }
        [XmlElement(Order = 4)]
        public string KyHieu_CT { get; set; }
        [XmlElement(Order = 5)]
        public string So_CT { get; set; }
        [XmlElement(Order = 6)]
        public string Nam_CT { get; set; }

        [XmlElement(Order = 7)]
        public NguoiNopTien NguoiNopTien { get; set; }
        [XmlElement(Order = 8)]
        public ThongTin_NopTien ThongTin_NopTien { get; set; }
        [XmlElement(Order = 9)]
        public List<ChiTiet_CT> ChiTiet_CT { get; set; }
        [XmlElement(Order = 10)]
        public TaiKhoan_NopTien TaiKhoan_NopTien { get; set; }


    }

    public class NguoiNopTien
    {
        public NguoiNopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_ST { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_NNT { get; set; }
        [XmlElement(Order = 3)]
        public string DiaChi { get; set; }
        [XmlElement(Order = 4)]
        public string TT_Khac { get; set; }
    }

    public class ThongTin_NopTien
    {
        public ThongTin_NopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_NT { get; set; }
        [XmlElement(Order = 2)]
        public string TyGia { get; set; }
        [XmlElement(Order = 3)]
        public string TongTien_NT { get; set; }
        [XmlElement(Order = 4)]
        public string TongTien_VND { get; set; }
    }

    public class ChiTiet_CT
    {
        public ChiTiet_CT()
        { }

        [XmlElement(Order = 1)]
        public string STT { get; set; }
        [XmlElement(Order = 2)]
        public string NDKT { get; set; }

        [XmlElement(Order = 3)]
        public string Ten_NDKT { get; set; }
        [XmlElement(Order = 4)]
        public string SoTien_NT { get; set; }

        [XmlElement(Order = 5)]
        public string SoTien_VND { get; set; }
        [XmlElement(Order = 6)]
        public string GhiChu { get; set; }

    }


    public class TaiKhoan_NopTien
    {
        public TaiKhoan_NopTien()
        {
        }
        [XmlElement(Order = 1)]
        public string Ma_NH_TH { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_NH_TH { get; set; }
        [XmlElement(Order = 3)]
        public string TaiKhoan_TH { get; set; }
        [XmlElement(Order = 4)]
        public string Ten_TaiKhoan_TH { get; set; }
    }

    public class VMTaiKhoan_NopTien : TaiKhoan_NopTien
    {
        public int STT { get; set; }
    }


    #region Xu ly các lệ phí hải quan


    public class TCS_TRA_CUU_PHI_BAN_NGANH
    {
        public string SO_CT { get; set; }
        public string NGAY_BC { get; set; }
        public string NGUOI_LAP_CT { get; set; }
        public string NGUOI_DUYET_CT { get; set; }
        public string TRANG_THAI_CT { get; set; }
        public string MA_NV { get; set; }
    }


    public class TCS_IN_BC_BO_BAN_NGANH
    {
        public string TUNGAY { get; set; }
        public string DENNGAY { get; set; }
        public string MST { get; set; }
        public string SOHOSO { get; set; }
        public string NAM_CT { get; set; }
        public string TRANGTHAI_CT { get; set; }
        public string TT_CT { get; set; }
        public string MA_NV { get; set; }
        public string MA_CN { get; set; }
        public string RootID { get; set; }
    }

    public class TCS_LEPHI_HAIQUAN_EX : TCS_LEPHI_HAIQUAN
    {

    }

    public class TCS_LEPHI_HAIQUAN
    {
        //        MA_NH_PH
        public string MA_NH_PH { get; set; }

        //TEN_NH_PH
        public string TEN_NH_PH { get; set; }
        //KYHIEU_CT
        public string KYHIEU_CT { get; set; }
        //SO_CT
        public string SO_CT { get; set; }

        //NGAY_CT
        public String NGAY_CT { get; set; }

        //NGAY_BN
        public String NGAY_BN { get; set; }
        //NGAY_BC
        public String NGAY_BC { get; set; }

        //SO_HS
        public string SO_HS { get; set; }
        //MA_DVQL
        public string MA_DVQL { get; set; }
        //TEN_DVQL
        public string TEN_DVQL { get; set; }
        //KYHIEU_CT_PT
        public string KYHIEU_CT_PT { get; set; }
        //SO_CT_PT
        public string SO_CT_PT { get; set; }
        //NAM_CT_PT
        public int NAM_CT_PT { get; set; }
        //NNT_MST
        public string NNT_MST { get; set; }
        //NNT_TEN_DV
        public string NNT_TEN_DV { get; set; }

        //NNT_DIACHI
        public string NNT_DIACHI { get; set; }
        //NNT_TT_KHAC
        public string NNT_TT_KHAC { get; set; }
        //TT_NT_MA_NT
        public string TT_NT_MA_NT { get; set; }
        //TT_NT_TYGIA
        public double TT_NT_TYGIA { get; set; }
        //TT_NT_TONGTIEN_NT
        public double TT_NT_TONGTIEN_NT { get; set; }
        //TT_NT_TONGTIEN_VND
        public double TT_NT_TONGTIEN_VND { get; set; }

        //TAIKHOAN_NT_MA_NH_TH
        public string TAIKHOAN_NT_MA_NH_TH { get; set; }
        //TAIKHOAN_NT_TEN_NH_TH
        public string TAIKHOAN_NT_TEN_NH_TH { get; set; }

        //TAIKHOAN_NT_MA_NH_TT
        public string TAIKHOAN_NT_MA_NH_TT { get; set; }
        //TAIKHOAN_NT_TEN_NH_TT
        public string TAIKHOAN_NT_TEN_NH_TT { get; set; }

        //TAIKHOAN_NT_TK_TH
        public string TAIKHOAN_NT_TK_TH { get; set; }
        //TAIKHOAN_NT_TEN_TK_TH
        public string TAIKHOAN_NT_TEN_TK_TH { get; set; }
        //TRANGTHAI_XULY
        public int TRANGTHAI_XULY { get; set; }
        public string TRANGTHAI_CT { get; set; }

        public int MA_NV { get; set; }

        public int PTTT { get; set; }
        public string TK_THANHTOAN { get; set; }

        //bo sung ngay 30/03/2016 các trường thông tin chi tiết của chứng từ lệ phí hải quan
        public string NNT_QUAN_HUYEN { get; set; }
        public string NNT_TINH_TP { get; set; }
        public string TT_NT_So_CMND { get; set; }
        public string TT_NT_So_DT { get; set; }

        public string TK_THANH_TOAN_QUAN_HUYEN { get; set; }
        public string TK_THANH_TOAN_TINH_TP { get; set; }
        public string TEN_TK_CHUYEN { get; set; }
        public string SO_DU { get; set; }
        public string TK_THANH_TOAN_DIACHI { get; set; }
        public string MA_CN { get; set; }

        public string FEE { get; set; }
        public string VAT { get; set; }
        public string TONGTIEN_TRUOC_VAT { get; set; }

        public string REF_NO { get; set; }
        public string RM_REF_NO { get; set; }
        public string PT_TINHPHI { get; set; }
    }

    public class ChungTuCode
    {
        public string SoCT { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
        public string TrangThaiCT { get; set; }
    }

    #endregion

    #region DataAccess
    public class daLePhiHaiQuan
    {

        public static DataSet MSG303BaoCaoPhiBoBanNganh(TCS_IN_BC_BO_BAN_NGANH tcs_in_bc_bo_ban_nganh)
        {
            //initialize connection
            var connection = DataAccess.GetConnection();

            //create sql condition
            string SQLCommand = "SELECT distinct(a.so_ct),a.ma_nh_ph, a.ten_nh_ph, a.kyhieu_ct,  a.ngay_ct, ";
            SQLCommand += " a.ngay_bn, a.ngay_bc, a.so_hs, a.ma_dvql, a.ten_dvql, ";
            SQLCommand += " a.kyhieu_ct_pt, a.so_ct_pt, a.nam_ct_pt, a.nnt_mst, a.nnt_ten_dv,";
            SQLCommand += " a.nnt_diachi, a.nnt_tt_khac, tt_nt_ma_nt, a.tt_nt_tygia,";
            SQLCommand += " a.tt_nt_tongtien_vnd, a.tt_nt_tongtien_nt, ";
            SQLCommand += " a.chungtu_ct_stt,";
            SQLCommand += " a.chungtu_ct_ndkt, a.chungtu_ct_ten_ndkt, a.chungtu_ct_sotien_nt,";
            SQLCommand += " a.chungtu_ct_sotien_vnd, a.chungtu_ct_ghichu,";
            SQLCommand += " a.taikhoan_nt_ma_nh_th, a.taikhoan_nt_ten_nh_th,";
            SQLCommand += " a.taikhoan_nt_tk_th, a.taikhoan_nt_ten_tk_th, a.trangthai_xuly,";
            SQLCommand += " a.trangthai_ct, a.ma_nv, a.ma_ks, a.ngay_xuly, b.ten_dn NGUOILAP, c.ten_dn NGUOIDUYET ";
            SQLCommand += " FROM tcs_thue_phi_bo_nganh a inner join TCS_DM_NHANVIEN b on a.Ma_NV= b.ma_nv inner join tcs_dm_nhanvien c on a.ma_nv = c.ma_nv ";

            string predicate = " Where 1=1 ";
            if (!string.IsNullOrEmpty(tcs_in_bc_bo_ban_nganh.SOHOSO))
            {
                predicate += " and a.so_ct like '%" + tcs_in_bc_bo_ban_nganh.SOHOSO + "%' ";
            }
            if (!string.IsNullOrEmpty(tcs_in_bc_bo_ban_nganh.TUNGAY) && !string.IsNullOrEmpty(tcs_in_bc_bo_ban_nganh.DENNGAY))
            {
                predicate += " and a.ngay_ct >= to_date('" + tcs_in_bc_bo_ban_nganh.TUNGAY + " 00:00:00','DD/MM/YYYY HH24:MI:SS') and a.ngay_ct <= to_date('" + tcs_in_bc_bo_ban_nganh.DENNGAY + " 23:59:59','DD/MM/YYYY HH24:MI:SS') ";
            }

            if (!string.IsNullOrEmpty(tcs_in_bc_bo_ban_nganh.MST))
            {
                predicate += " and lower(a.nnt_mst) like lower('%" + tcs_in_bc_bo_ban_nganh.MST + "%') ";
            }
            if (!string.IsNullOrEmpty(tcs_in_bc_bo_ban_nganh.NAM_CT))
            {
                predicate += " and a.nam_ct_pt like '%" + tcs_in_bc_bo_ban_nganh.NAM_CT + "%' ";
            }
            if (!string.IsNullOrEmpty(tcs_in_bc_bo_ban_nganh.TT_CT) && tcs_in_bc_bo_ban_nganh.TT_CT != "-1")
            {
                predicate += " and a.trangthai_xuly =" + tcs_in_bc_bo_ban_nganh.TT_CT + "";
            }
            if (!string.IsNullOrEmpty(tcs_in_bc_bo_ban_nganh.TRANGTHAI_CT) && tcs_in_bc_bo_ban_nganh.TRANGTHAI_CT != "-1")
            {
                predicate += " and a.TRANGTHAI_CT = '" + tcs_in_bc_bo_ban_nganh.TRANGTHAI_CT + "' ";
            }
            //if (tcs_in_bc_bo_ban_nganh.RootID == "1")
            //{
            //    predicate += " and  a.MA_CN in (SELECT ID FROM TCS_DM_chinhanh start with ID='" + tcs_in_bc_bo_ban_nganh.MA_CN + "' connect by NOCYCLE PRIOR SUBSTR(id, 2, LENGTH(id) - 2) = branch_ID) ";
            //}
            if(tcs_in_bc_bo_ban_nganh.RootID == "1")
            {
                predicate += " and a.MA_CN ='" + tcs_in_bc_bo_ban_nganh.MA_CN + "' ";
                //predicate += " and  a.MA_CN in (SELECT ID FROM TCS_DM_chinhanh start with ID='" + tcs_in_bc_bo_ban_nganh.MA_CN + "' connect by nocycle PRIOR ID = branch_code) ";
            }
            SQLCommand = SQLCommand + predicate + " order by a.so_ct desc ";



            var dsPhiBanNganh = DataAccess.ExecuteReturnDataSet(SQLCommand, CommandType.Text);

            //string json = JsonConvert.SerializeObject(ds2, Formatting.Indented);

            return dsPhiBanNganh;
        }

        public static DataSet MSG303GetPhiBoBanNganh(TCS_TRA_CUU_PHI_BAN_NGANH tcs_tra_cuu_phi_ban_nganh)
        {
            //initialize connection
            var connection = DataAccess.GetConnection();

            //create sql condition
            string SQLCommand = "SELECT a.ma_nh_ph, a.ten_nh_ph, a.kyhieu_ct, a.so_ct, a.ngay_ct, ";
            SQLCommand += " a.ngay_bn, a.ngay_bc, a.so_hs, a.ma_dvql, a.ten_dvql, ";
            SQLCommand += " a.kyhieu_ct_pt, a.so_ct_pt, a.nam_ct_pt, a.nnt_mst, a.nnt_ten_dv,";
            SQLCommand += " a.nnt_diachi, a.nnt_tt_khac, a.tt_nt_ma_nt, a.tt_nt_tygia,";
            SQLCommand += " (select sum(sotien_vnd) from  tcs_thue_phi_bo_nganh_dtl dtl where a.so_ct = dtl.so_ct) tt_nt_tongtien_vnd, (select sum(sotien_nt) from  tcs_thue_phi_bo_nganh_dtl dtl where a.so_ct = dtl.so_ct) tt_nt_tongtien_nt, ";
            SQLCommand += "  a.chungtu_ct_stt,";
            SQLCommand += " a.chungtu_ct_ndkt, a.chungtu_ct_ten_ndkt, a.chungtu_ct_sotien_nt,";
            SQLCommand += " a.chungtu_ct_sotien_vnd, a.chungtu_ct_ghichu,";
            SQLCommand += " a.taikhoan_nt_ma_nh_th, a.taikhoan_nt_ten_nh_th,";
            SQLCommand += " a.taikhoan_nt_tk_th, a.taikhoan_nt_ten_tk_th, a.trangthai_xuly,";
            SQLCommand += " a.trangthai_ct, a.ma_nv, a.ma_ks, a.ngay_xuly, b.ten_dn NGUOILAP, c.ten_dn NGUOIDUYET ";
            SQLCommand += " FROM tcs_thue_phi_bo_nganh a inner join TCS_DM_NHANVIEN b on a.Ma_NV= b.ma_nv inner join tcs_dm_nhanvien c on a.ma_ks = c.ma_nv ";

            string predicate = " Where 1=1 ";
            if (!string.IsNullOrEmpty(tcs_tra_cuu_phi_ban_nganh.SO_CT))
            {
                predicate += " and so_ct like '%" + tcs_tra_cuu_phi_ban_nganh.SO_CT + "%' ";
            }
            if (!string.IsNullOrEmpty(tcs_tra_cuu_phi_ban_nganh.NGAY_BC))
            {
                predicate += " and ngay_bc = '%" + tcs_tra_cuu_phi_ban_nganh.NGAY_BC + "%' ";
            }

            if (!string.IsNullOrEmpty(tcs_tra_cuu_phi_ban_nganh.NGUOI_LAP_CT))
            {
                predicate += " and ma_nv like '%" + tcs_tra_cuu_phi_ban_nganh.NGUOI_LAP_CT + "%' ";
            }
            if (!string.IsNullOrEmpty(tcs_tra_cuu_phi_ban_nganh.NGUOI_DUYET_CT))
            {
                predicate += " and ma_ks like '%" + tcs_tra_cuu_phi_ban_nganh.NGUOI_DUYET_CT + "%' ";
            }
            if (!string.IsNullOrEmpty(tcs_tra_cuu_phi_ban_nganh.TRANG_THAI_CT))
            {
                predicate += " and trangthai_ct = '" + tcs_tra_cuu_phi_ban_nganh.TRANG_THAI_CT + "' ";
            }
            predicate += " and ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV=" + tcs_tra_cuu_phi_ban_nganh.MA_NV + ") AND   ma_nv = " + tcs_tra_cuu_phi_ban_nganh.MA_NV + ") ";

            SQLCommand = SQLCommand + predicate + " order by a.so_ct desc";



            var dsPhiBanNganh = DataAccess.ExecuteReturnDataSet(SQLCommand, CommandType.Text);

            //string json = JsonConvert.SerializeObject(ds2, Formatting.Indented);

            return dsPhiBanNganh;
        }


        public static DataSet MSG303GetDSChungTu(string trangthai, string ma_nv)
        {
            //initialize connection
            var connection = DataAccess.GetConnection();

            //create sql condition
            string SQLCommand = "SELECT nv.ten_dn, a.ma_nh_ph, a.ten_nh_ph, a.kyhieu_ct, a.so_ct, a.ngay_ct, ";
            SQLCommand += " a.ngay_bn, a.ngay_bc, a.so_hs, a.ma_dvql, a.ten_dvql, ";
            SQLCommand += " a.kyhieu_ct_pt, a.so_ct_pt, a.nam_ct_pt, a.nnt_mst, a.nnt_ten_dv,";
            SQLCommand += " a.nnt_diachi, a.nnt_tt_khac, a.tt_nt_ma_nt, a.tt_nt_tygia,";
            SQLCommand += " (select sum(sotien_vnd) from  tcs_thue_phi_bo_nganh_dtl dtl where a.so_ct = dtl.so_ct) tt_nt_tongtien_vnd, (select sum(sotien_nt) from  tcs_thue_phi_bo_nganh_dtl dtl where a.so_ct = dtl.so_ct) tt_nt_tongtien_nt, ";
            SQLCommand += " a.chungtu_ct_stt,";
            SQLCommand += " a.chungtu_ct_ndkt, a.chungtu_ct_ten_ndkt, a.chungtu_ct_sotien_nt,";
            SQLCommand += " a.chungtu_ct_sotien_vnd, a.chungtu_ct_ghichu,";
            SQLCommand += " a.taikhoan_nt_ma_nh_th, a.taikhoan_nt_ten_nh_th,";
            SQLCommand += " a.taikhoan_nt_tk_th, a.taikhoan_nt_ten_tk_th, a.trangthai_xuly,";
            SQLCommand += " a.trangthai_ct, a.ma_nv, a.ma_ks, a.fee, a.vat, a.TONGTIEN_TRUOC_VAT";
            SQLCommand += " FROM tcs_thue_phi_bo_nganh a, tcs_dm_nhanvien nv , tcs_thue_phi_bo_nganh b,  tcs_dm_nhanvien ks ";//outer join tcs_dm_nhanvien b on a.ma_nv = b.ma_nv ";

            string predicate = " Where a.ma_nv=nv.ma_nv and b.ma_nv = ks.ma_nv and a.so_ct = b.so_ct ";


            if (trangthai == "05")
            {
                predicate += " and a.trangthai_ct='" + trangthai + "'";
            }

            if (trangthai != "-1" && trangthai != "05")
            {
                predicate += " and a.trangthai_ct='" + trangthai + "' and a.ngay_ct between TRUNC(sysdate) and trunc(sysdate) +1 - interval  '1' second)";

            }

            else
            {
                predicate += "  and a.trangthai_ct<>'01' or (a.trangthai_ct='01' and a.ngay_ct=to_date(sysdate, 'dd/MM/yyyy'))";
            }
            predicate += " and ma_nv in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV=" + ma_nv + ") AND   ma_nv = " + ma_nv + ") ";

            SQLCommand = SQLCommand + predicate + " order by a.so_ct desc";


            var dsPhiBanNganh = DataAccess.ExecuteReturnDataSet(SQLCommand, CommandType.Text);

            //string json = JsonConvert.SerializeObject(ds2, Formatting.Indented);

            return dsPhiBanNganh;
        }

        public static DataSet GetListBankToTransfer()
        {
            var connection = DataAccess.GetConnection();

            //create sql condition
            string SQLCommand = "select  ma_nh_chuyen, ten_nh_chuyen";
            SQLCommand += " FROM tcs_nganhang_chuyen ";




            SQLCommand = SQLCommand;



            var dsPhiBanNganh = DataAccess.ExecuteReturnDataSet(SQLCommand, CommandType.Text);

            //string json = JsonConvert.SerializeObject(ds2, Formatting.Indented);

            return dsPhiBanNganh;
        }

        private static bool InsertChiTiet_CT(string id, string so_ct, string stt, string ndkt, string ten_ndkt, string SoTien_NT, string SoTien_VND, string GhiChu)
        {

            bool blnResult = true;
            string strSql = "INSERT INTO tcs_thue_phi_bo_nganh_dtl " + " (id,so_ct,stt,ndkt,ten_ndkt,sotien_nt, sotien_vnd, ghichu) " + "     VALUES ('" + id + "', '" + so_ct + "', " + "  '" + stt + "', '" + ndkt + "', '" + ten_ndkt + "', '" + SoTien_NT + "', '" + SoTien_VND + "', '" + GhiChu + "') ";
            StringBuilder sbErrMsg;
            sbErrMsg = new StringBuilder();
            //DataAccess dataAccess = new DataAccess();
            //var conn = DataAccess.GetConnection();
            //IDbTransaction transCT = null;

            try
            {
                //LogDebug.WriteLog("Thời gian vào cầu: " + DateTime.Now.ToString("dd/MM/yyyy H:mm:ss zzz"), EventLogEntryType.Error);
                //transCT = DataAccess.BeginTransaction();
                DataAccess.ExecuteNonQuery(strSql, CommandType.Text);
                //transCT.Commit();
            }
            catch (Exception ex)
            {
                //transCT.Rollback();
                //LogDebug.WriteLog("Thời gian xảy ra lỗi: " + DateTime.Now.ToString("dd/MM/yyyy H:mm:ss zzz") + " : " + ex.ToString(), EventLogEntryType.Error);
                blnResult = false;
                throw ex;
            }
            finally
            {
                //if ((transCT != null))
                //{
                //    transCT.Dispose();
                //}
                //if ((conn != null))
                //{
                //    DataAccess.Dispose();
                //}
            }
            return blnResult;
        }


        public static ChungTuCode MSG303ToDB(TCS_LEPHI_HAIQUAN TCS_LEPHI_HAIQUAN, List<ChiTiet_CT> ListChiTiet_CT)
        {
            StringBuilder sbErrMsg;
            sbErrMsg = new StringBuilder();
         //   DataAccess dataAccess = new DataAccess();
            var conn = DataAccess.GetConnection();
            IDbTransaction transCT = null;

            ChungTuCode chungTuCode = new ChungTuCode();

            Int64 iId = Common.TCS_GetSequenceValue("TCS_CTU_ID_SEQ");
            string idCT = iId.ToString();
            idCT = idCT.PadLeft(5, '0');
            string currentYear = DateTime.Now.Year.ToString();
            string currentMonth = DateTime.Now.Month.ToString();
            if (currentMonth.Length < 2)
                currentMonth = "0" + currentMonth;

            TCS_LEPHI_HAIQUAN.TRANGTHAI_XULY = (int)EnumLePhiHaiQuan.Temp;

            currentYear = currentYear.Substring(currentYear.Length - 2);
            string soChungTu = (currentYear.ToString() + currentMonth.ToString() + idCT.ToString());
            TCS_LEPHI_HAIQUAN.SO_CT = soChungTu;
            string preColumn = CustomsServiceV3.Extension.Common.GetName<TCS_LEPHI_HAIQUAN>(TCS_LEPHI_HAIQUAN, false, "");
            //string preValue = CustomsService.Extension.Common.GetName<TCS_LEPHI_HAIQUAN>(TCS_LEPHI_HAIQUAN, true, ":");
            string preValue = CustomsServiceV3.Extension.Common.getStrValueInsert<TCS_LEPHI_HAIQUAN>(TCS_LEPHI_HAIQUAN);//

            //string strExecuteLePhi = "INSERT INTO TCS_THUE_PHI_BO_NGANH(MA_NH_PH,TEN_NH_PH, KYHIEU_CT, SO_CT, NGAY_CT,NGAY_BN, NGAY_BC, SO_HS, MA_DVQL, TEN_DVQL, KYHIEU_CT_PT, SO_CT_PT, NAM_CT_PT, NNT_MST, NNT_TEN_DV, NNT_DIACHI, NNT_TT_KHAC, TT_NT_MA_NT, TT_NT_TYGIA, TT_NT_TONGTIEN_NT, TT_NT_TONGTIEN_VND, TAIKHOAN_NT_MA_NH_TH, TAIKHOAN_NT_TEN_NH_TH, TAIKHOAN_NT_TK_TH, TAIKHOAN_NT_TEN_TK_TH, TRANGTHAI_XULY, TRANGTHAI_CT, MA_NV, PTTT, TK_THANHTOAN, NNT_QUAN_HUYEN, NNT_TINH_TP, TT_NT_So_CMND, TT_NT_So_DT, TK_THANH_TOAN_QUAN_HUYEN, TK_THANH_TOAN_TINH_TP, TEN_TK_CHUYEN, SO_DU, TK_THANH_TOAN_DIACHI, MA_CN) " +
            //            "VALUES(" + TCS_LEPHI_HAIQUAN.MA_NH_PH + ",'" + TCS_LEPHI_HAIQUAN.TEN_NH_PH + "',  '" + TCS_LEPHI_HAIQUAN.KYHIEU_CT + "',  '" + TCS_LEPHI_HAIQUAN.SO_CT + 
            //            "', to_date('" + TCS_LEPHI_HAIQUAN.NGAY_CT + "','DD/MM/YYYY'),to_date('" + TCS_LEPHI_HAIQUAN.NGAY_BN + "','DD/MM/YYYY'),to_date('" + TCS_LEPHI_HAIQUAN.NGAY_BC + "','DD/MM/YYYY'), '" + TCS_LEPHI_HAIQUAN.SO_HS + "', '" + TCS_LEPHI_HAIQUAN.MA_DVQL + "', '" + TCS_LEPHI_HAIQUAN.TEN_DVQL + "', '" + TCS_LEPHI_HAIQUAN.KYHIEU_CT_PT + "', '" + TCS_LEPHI_HAIQUAN.SO_CT_PT + "', " + TCS_LEPHI_HAIQUAN.NAM_CT_PT + ", '" + TCS_LEPHI_HAIQUAN.NNT_MST + "', '" + TCS_LEPHI_HAIQUAN.NNT_TEN_DV + "', '" + TCS_LEPHI_HAIQUAN.NNT_DIACHI + "', '" + TCS_LEPHI_HAIQUAN.NNT_TT_KHAC + "', '" + TCS_LEPHI_HAIQUAN.TT_NT_MA_NT + "', '" + TCS_LEPHI_HAIQUAN.TT_NT_TYGIA + "', " + TCS_LEPHI_HAIQUAN.TT_NT_TONGTIEN_NT + ", " + TCS_LEPHI_HAIQUAN.TT_NT_TONGTIEN_VND + ", '" + TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_MA_NH_TH + "',  '" + TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_TEN_NH_TH + "', '" + TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_TK_TH + "', '" + TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_TEN_TK_TH + "', " + TCS_LEPHI_HAIQUAN.TRANGTHAI_XULY + ", '" + TCS_LEPHI_HAIQUAN.TRANGTHAI_CT + "', " + TCS_LEPHI_HAIQUAN.MA_NV + ", " + TCS_LEPHI_HAIQUAN.PTTT + ", '" + TCS_LEPHI_HAIQUAN.TK_THANHTOAN + "', '" + TCS_LEPHI_HAIQUAN.NNT_QUAN_HUYEN + "', '" + TCS_LEPHI_HAIQUAN.NNT_TINH_TP + "', '" + TCS_LEPHI_HAIQUAN.TT_NT_So_CMND + "', '" + TCS_LEPHI_HAIQUAN.TT_NT_So_DT + "', '" + TCS_LEPHI_HAIQUAN.TK_THANH_TOAN_QUAN_HUYEN + "', '" + TCS_LEPHI_HAIQUAN.TK_THANH_TOAN_TINH_TP + "', '" + TCS_LEPHI_HAIQUAN.TEN_TK_CHUYEN + "', '" + TCS_LEPHI_HAIQUAN.SO_DU + "', '" + TCS_LEPHI_HAIQUAN.TK_THANH_TOAN_DIACHI + "', '" + TCS_LEPHI_HAIQUAN.MA_CN + "')";

            string strExecuteLePhi = "INSERT INTO TCS_THUE_PHI_BO_NGANH(MA_NH_PH,TEN_NH_PH, KYHIEU_CT, SO_CT, NGAY_CT,NGAY_BN,NGAY_BC, SO_HS, MA_DVQL, TEN_DVQL, " +
                        " KYHIEU_CT_PT, SO_CT_PT, NAM_CT_PT, NNT_MST, NNT_TEN_DV, NNT_DIACHI, NNT_TT_KHAC, TT_NT_MA_NT, TT_NT_TYGIA, TT_NT_TONGTIEN_NT, " +
                        " TT_NT_TONGTIEN_VND, TAIKHOAN_NT_MA_NH_TH, TAIKHOAN_NT_TEN_NH_TH, TAIKHOAN_NT_MA_NH_TT, TAIKHOAN_NT_TEN_NH_TT, TAIKHOAN_NT_TK_TH, TAIKHOAN_NT_TEN_TK_TH, TRANGTHAI_XULY, TRANGTHAI_CT, " +
                        " MA_NV, PTTT, TK_THANHTOAN, NNT_QUAN_HUYEN, NNT_TINH_TP, TT_NT_So_CMND, TT_NT_So_DT, TK_THANH_TOAN_QUAN_HUYEN, TK_THANH_TOAN_TINH_TP, " +
                        " TEN_TK_CHUYEN, SO_DU, TK_THANH_TOAN_DIACHI, MA_CN, FEE, VAT, TONGTIEN_TRUOC_VAT,REF_NO,RM_REF_NO,PT_TINHPHI, NGAY_HT) VALUES('" + TCS_LEPHI_HAIQUAN.MA_NH_PH + "','" +
                        TCS_LEPHI_HAIQUAN.TEN_NH_PH + "',  '" + TCS_LEPHI_HAIQUAN.KYHIEU_CT + "',  '" + TCS_LEPHI_HAIQUAN.SO_CT + "', to_date('" +
                        TCS_LEPHI_HAIQUAN.NGAY_CT + "','DD/MM/YYYY'),to_date('" + TCS_LEPHI_HAIQUAN.NGAY_CT + "','DD/MM/YYYY'),to_date('" + TCS_LEPHI_HAIQUAN.NGAY_CT + "','DD/MM/YYYY'), '" + TCS_LEPHI_HAIQUAN.SO_HS + "', '" + TCS_LEPHI_HAIQUAN.MA_DVQL + "', '" +
                        TCS_LEPHI_HAIQUAN.TEN_DVQL + "', '" + TCS_LEPHI_HAIQUAN.KYHIEU_CT_PT + "', '" + TCS_LEPHI_HAIQUAN.SO_CT_PT + "', " +
                        TCS_LEPHI_HAIQUAN.NAM_CT_PT + ", '" + TCS_LEPHI_HAIQUAN.NNT_MST + "', '" + TCS_LEPHI_HAIQUAN.NNT_TEN_DV + "', '" +
                        TCS_LEPHI_HAIQUAN.NNT_DIACHI + "', '" + TCS_LEPHI_HAIQUAN.NNT_TT_KHAC + "', '" + TCS_LEPHI_HAIQUAN.TT_NT_MA_NT + "', '" +
                        TCS_LEPHI_HAIQUAN.TT_NT_TYGIA + "', " + TCS_LEPHI_HAIQUAN.TT_NT_TONGTIEN_NT + ", " + TCS_LEPHI_HAIQUAN.TT_NT_TONGTIEN_VND + ", '" +
                        TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_MA_NH_TH + "',  '" + TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_TEN_NH_TH + "', '"+TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_MA_NH_TT+"', '"+TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_TEN_NH_TT+"', '" +
                        TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_TK_TH + "', '" + TCS_LEPHI_HAIQUAN.TAIKHOAN_NT_TEN_TK_TH + "', " + TCS_LEPHI_HAIQUAN.TRANGTHAI_XULY + ", '" +
                        TCS_LEPHI_HAIQUAN.TRANGTHAI_CT + "', " + TCS_LEPHI_HAIQUAN.MA_NV + ", " + TCS_LEPHI_HAIQUAN.PTTT + ", '" +
                        TCS_LEPHI_HAIQUAN.TK_THANHTOAN + "', '" + TCS_LEPHI_HAIQUAN.NNT_QUAN_HUYEN + "', '" + TCS_LEPHI_HAIQUAN.NNT_TINH_TP + "', '" +
                        TCS_LEPHI_HAIQUAN.TT_NT_So_CMND + "', '" + TCS_LEPHI_HAIQUAN.TT_NT_So_DT + "', '" + TCS_LEPHI_HAIQUAN.TK_THANH_TOAN_QUAN_HUYEN + "', '" +
                        TCS_LEPHI_HAIQUAN.TK_THANH_TOAN_TINH_TP + "', '" + TCS_LEPHI_HAIQUAN.TEN_TK_CHUYEN + "', '" + TCS_LEPHI_HAIQUAN.SO_DU + "', '" +
                        TCS_LEPHI_HAIQUAN.TK_THANH_TOAN_DIACHI + "', '" + TCS_LEPHI_HAIQUAN.MA_CN + "', '" + TCS_LEPHI_HAIQUAN.FEE + "', '" +
                        TCS_LEPHI_HAIQUAN.VAT + "','" + TCS_LEPHI_HAIQUAN.TONGTIEN_TRUOC_VAT + "','" + TCS_LEPHI_HAIQUAN.REF_NO + "','" + TCS_LEPHI_HAIQUAN.RM_REF_NO + "','" + TCS_LEPHI_HAIQUAN.PT_TINHPHI + "', to_date(to_char(sysdate,'DD/MM/YYYY'),'dd/MM/yyyy'))";

            try
            {
                transCT = conn.BeginTransaction();
                //IDbDataParameter[] p = Common.getIDbParameter<TCS_LEPHI_HAIQUAN>(TCS_LEPHI_HAIQUAN);



                DataAccess.ExecuteNonQuery(strExecuteLePhi, CommandType.Text);

                if (ListChiTiet_CT != null)
                {
                    foreach (var item in ListChiTiet_CT)
                    {
                        iId = Common.TCS_GetSequenceValue("TCS_CTU_DTL_SEQ");
                        InsertChiTiet_CT(iId.ToString(), soChungTu, item.STT, item.NDKT, item.Ten_NDKT, item.SoTien_NT, item.SoTien_VND, item.GhiChu);
                    }

                }
                transCT.Commit();

                sbErrMsg.Append("\nTạo thành công chứng từ số:" + soChungTu);
                chungTuCode.Message = sbErrMsg.ToString();
                chungTuCode.SoCT = soChungTu;
                chungTuCode.Code = 1;

            }
            catch(Exception ex)
            {
                transCT.Rollback();


                sbErrMsg.Append("Lỗi trong quá trình đưa dữ liệu theo MSG 103: ĐTNT - ");

                sbErrMsg.Append("\n" + ex.ToString());

                sbErrMsg.Append("\n");

                chungTuCode.Message = sbErrMsg.ToString();
                chungTuCode.SoCT = "";
                chungTuCode.Code = -1;

                //LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);


            }
            finally
            {
                if ((transCT != null))
                {
                    transCT.Dispose();
                }
                if ((conn != null))
                {
                    conn.Close();
                    conn.Dispose();

                }
            }

            return chungTuCode;


        }


    }



    #endregion


}
