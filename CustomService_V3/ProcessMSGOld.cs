﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using CustomsV3.MSG.MSG11;
//using CustomsV3.MSG.MSG12;
//using CustomsV3.MSG.MSG21;
//using CustomsV3.MSG.MSG22;
//using CustomsV3.MSG.MSG31;
//using CustomsV3.MSG.MSG32;
//using CustomsV3.MSG.MSG41;
//using CustomsV3.MSG.MSG42;
//using CustomsV3.MSG.MSG43;
//using CustomsV3.MSG.MSG44;
//using CustomsV3.MSG.MSG51;
//using CustomsV3.MSG.MSG52;
//using CustomsV3.MSG.MSG53;
//using CustomsV3.MSG.MSG54;
//using CustomsV3.MSG.MSG61;
//using CustomsV3.MSG.MSG62;
//using CustomsV3.MSG.MSG71;
//using CustomsV3.MSG.MSG72;
//using CustomsV3.MSG.MSG81;
//using CustomsV3.MSG.MSG82;
//using CustomsV3.MSG.MSG83;
//using CustomsV3.MSG.MSG84;
//using CustomsV3.MSG.MSG91;
//using CustomsV3.MSG.MSG92;
//using CustomsV3.MSG.MSG93;
//using CustomsV3.MSG.MSG94;
//using VBOracleLib;
//using System.Diagnostics;
//using System.Configuration;
//using System.Data.OracleClient;
//using System.Data;
//using System.Collections;
//using Signature;
//using System.Net;
//using System.Security.Cryptography.X509Certificates;

//using System.Xml;
//using System.Xml.Linq;
//using System.IO;
////using CustomsV3.MSG.MSG62;
//using CustomsV3.MSG.MSG63;
//using CustomsV3.MSG.MSG86;
//using CustomsV3.MSG.MSG85;
//using System.Runtime.InteropServices;
//using CustomsV3.MSG.MSG88;
//using CustomsV3.MSG.MSG87;
//using CustomsV3.MSG.MSG65;
//using CustomsV3.MSG.MSG23;
//using CustomsV3.MSG.MSG45;
//using CustomsV3.MSG.MSG46;
//using CustomsV3.MSG.MSG15;
//using CustomsV3.MSG.MSG16;
//using CustomsV3.MSG.MSG18;
//using CustomsV3.MSG.MSG19;
//using CustomsV3.MSG.MSG17;
//using CustomsV3.MSG.MSG24;
//using CustomsV3.MSG.MSG25;
//using CustomsV3.MSG.MSG26;
//using CustomsV3.MSG.MSG27;
//using CustomsV3.MSG.MSG47;
//using CustomsV3.MSG.MSG49;
//using CustomsV3.MSG.MSG48;
//using CustomsV3.MSG.MSG50;
////using Oracle.DataAccess.Client;
////using Oracle

//namespace CustomsService
//{
//    public class ProcessMSGOld
//    {
//        #region "Khai bao bien"
//        private static String strCustomOn = ConfigurationManager.AppSettings.Get("CUSTOMS.ON").ToString();
//        private static String strTen_NH_PH = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
//        private static String strXMLdefout1 = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
//        private static String strXMLdefout3 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
//        private static String strXMLdefout2 = " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\"";
//        private static String strXMLdefin1 = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
//        private static String strXMLdefin2 = "<CUSTOMS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
//        private static String strConnection1 = ConfigurationManager.AppSettings.Get("CTC_DB_CONN").ToString();
//        private static String strConnection = VBOracleLib.Security.DescryptStr(strConnection1);
//        private static String strMessage_Version = ConfigurationManager.AppSettings.Get("CUSTOMS.Message_Version").ToString();
//        private static String strSender_Code = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
//        private static String strSender_Name = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
//        private static String strSender_TaxNo = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Taxno").ToString();
//        private static String strCustomsOpenTag = "<CUSTOMS>";
//        private static String strCustomsCloseTag = "</CUSTOMS>";
//        private static String strLoai_CT = "22";
//        private static String strLoai_BLTK = "31";
//        private static String strLoai_BLVTD = "32";
//        private static String strLoai_BLC = "33";
//        private static String strDM_HQ = "HQ";
//        private static String strDM_LH = "LH";
//        private static String strDM_KB = "KB";
//        private static String strDM_DV = "DV";
//        private static String strGio_DC = " " + TCS_GetGIO_DC();
//        private static String strDKNGAYDC = " AND to_char(to_timestamp(ngay_tn_ct,'YYYY-MM-DD\"T\"HH24:MI:SS\"Z\"'),'yyyy-MM-dd HH24:MI:SS') >=:ngaydc1" +
//                                            " and to_char(to_timestamp(ngay_tn_ct,'YYYY-MM-DD\"T\"HH24:MI:SS\"Z\"'),'yyyy-MM-dd HH24:MI:SS')<=:ngaydc2";
//        private static String strPLK = "MIIHFDCCBfygAwIBAgIQVAK8XKzOZpwAAgAAAAGY+TANBgkqhkiG9w0BAQUFADAz" +
//"MQswCQYDVQQGEwJWTjEWMBQGA1UEChMNTkFDRU5DT01NIFNDVDEMMAoGA1UEAxMD" +
//"Q0EyMB4XDTE0MDQxNjA0MjMxMFoXDTE1MTAyMTA4MzUxMFowggGrMQswCQYDVQQG" +
//"EwJWTjEXMBUGA1UECBMOTVNUOjE0MDAxMTYyMzMxgYEwfwYDVQQHDHhUw6LMgG5n" +
//"IDE2LCAyMywgMjQgVG/MgGEgbmhhzIAgTUlQRUMsIHPDtMyBIDIyOSBUw6J5IFPG" +
//"oW4sIFBoxrDGocyAbmcgTmdhzIMgVMawIFPGocyJLCBRdcOizKNuIMSQw7TMgW5n" +
//"IMSQYSwgSGHMgCBOw7TMo2kxRzBFBgNVBAoMPk5Hw4JOIEjDgE5HIFRIxq/GoE5H" +
//"IE3huqBJIEPhu5QgUEjhuqZOIFjEgk5HIETDgsyAVSBQRVRST0xJTUVYMT0wOwYD" +
//"VQQLDDTDlG5nIE5ndXnDqsyDbiBRdWFuZyDEkGnMo25oIOKAkyBU4buVbmcgR2nD" +
//"oW0gxJHhu5FjMUcwRQYDVQQDDD5OR8OCTiBIw4BORyBUSMavxqBORyBN4bqgSSBD" +
//"4buUIFBI4bqmTiBYxIJORyBEw4LMgFUgUEVUUk9MSU1FWDEuMCwGCSqGSIb3DQEJ" +
//"ARYfdGFtcHQxLnBnYmFua0BwZXRyb2xpbWV4LmNvbS52bjCBnzANBgkqhkiG9w0B" +
//"AQEFAAOBjQAwgYkCgYEA67hLxON/oxGC97svVolWU25xlZUwQQOXRhET5JkZDql4" +
//"cLvvF+PnuRo4Dog6Y6KgExpB+eeQD6/NegMm+7dpgWoHb83nvYZQXNxpm8dVEI0y" +
//"NJQ4HfVCd8hmbS7Kz8QEQojUXAUr4VsiZJkwxKxtTCNVDg7QXLVGsNn51yCxHxkC" +
//"AwEAAaOCAywwggMoMAsGA1UdDwQEAwIFoDBEBgkqhkiG9w0BCQ8ENzA1MA4GCCqG" +
//"SIb3DQMCAgIAgDAOBggqhkiG9w0DBAICAIAwBwYFKw4DAgcwCgYIKoZIhvcNAwcw" +
//"HQYDVR0OBBYEFAjrJDnHVk251agYJLD2MCYtjpcrMD0GCSsGAQQBgjcVBwQwMC4G" +
//"JisGAQQBgjcVCILF1heDvIEHhYmRN4eA62iDrtZGHofZhCaH3ddcAgFkAgECMB8G" +
//"A1UdIwQYMBaAFFjPY+HeQkBsMe8BB+3C/cCgFvfCMIH7BgNVHR8EgfMwgfAwge2g" +
//"geqggeeGgaJsZGFwOi8vL0NOPUNBMixDTj13d3csQ049Q0RQLENOPVB1YmxpYyUy" +
//"MEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9" +
//"Y2F2bixEQz12bj9jZXJ0aWZpY2F0ZVJldm9jYXRpb25MaXN0P2Jhc2U/b2JqZWN0" +
//"Q2xhc3M9Y1JMRGlzdHJpYnV0aW9uUG9pbnSGJWh0dHA6Ly93d3cuY2F2bi52bi9D" +
//"ZXJ0RW5yb2xsL0NBMi5jcmyGGWh0dHA6Ly9jYXZuLnZuL2NhMmNybC5jcmwwggEi" +
//"BggrBgEFBQcBAQSCARQwggEQMIGeBggrBgEFBQcwAoaBkWxkYXA6Ly8vQ049Q0Ey" +
//"LENOPUFJQSxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxD" +
//"Tj1Db25maWd1cmF0aW9uLERDPWNhdm4sREM9dm4/Y0FDZXJ0aWZpY2F0ZT9iYXNl" +
//"P29iamVjdENsYXNzPWNlcnRpZmljYXRpb25BdXRob3JpdHkwPQYIKwYBBQUHMAKG" +
//"MWh0dHA6Ly93d3cuY2F2bi52bi9DZXJ0RW5yb2xsL3d3dy5jYXZuLnZuX0NBMi5j" +
//"cnQwLgYIKwYBBQUHMAGGImh0dHA6Ly8xODMuOTEuNy4xOTg6ODA4MC9vY3NwL29j" +
//"c3AwEwYDVR0lBAwwCgYIKwYBBQUHAwQwGwYJKwYBBAGCNxUKBA4wDDAKBggrBgEF" +
//"BQcDBDANBgkqhkiG9w0BAQUFAAOCAQEAGy/xi4ZCmiwN2Xgo38YFrMojl8jXcoOT" +
//"rmVcnxavxP3lYAA3og7Z/lzKa7C7g+BJVCH+s0pvTVAdHZO0dynYCa6a6ZZ6N/eF" +
//"iPVNxsnEfex1NHxBfCGPfRyZ4uYgL9v+XTBjlKadg7986W5n3kNWosex1CBVV4uE" +
//"oj6GuUGBkIj10kuxFsPY7O0R/3GJ+KEA5MpPodK/4Sd+v0dAgI9Y9bU0qnMNCyvk" +
//"zaU9rgJxus+CaGvU+2/MqTBaAW7ocpOIyv609Bmww1Akc4ZTnGmR1tiwUZceS7Zx" +
//"RxdGHXUntWYtT58vcpbE/TEvkEZFyArjIJ6jXou5f9ZUYdy1ZVuY+Q==";
//        #endregion "Khai bao bien"

//        #region "Utility"
//        /// <summary>
//        /// Hàm lấy connection đến CSDL và thực hiện mở connection
//        /// </summary>
//        /// <returns>Oracle Connection</returns>
//        /// <remarks>
//        ///</remarks>
//        private static OracleConnection GetConnection()
//        {
//            OracleConnection _connection = default(OracleConnection);
//            try
//            {
//                _connection = new OracleConnection(strConnection);
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//            _connection.Open();
//            return _connection;
//        }

//        /// <summary>
//        /// Hàm thực thi câu lệnh không trả về dữ liệu record (dùng để thực hiện lệnh insert, update, delete hoặc các store procedure)
//        /// </summary>
//        /// <param name="Statement">Câu lệnh thực thi</param>
//        /// <param name="cmdType">Loại lệnh thực thi</param>
//        /// <param name="arrParamIn">Các tham số truyền vào câu lệnh</param>
//        /// <param name="trans">Transaction thực thi câu lệnh</param>
//        /// <returns>Kết quả thực hiện</returns>
//        /// <remarks>
//        ///</remarks>
//        private static int ExecuteNonQuery(string Statement, CommandType cmdType, IDataParameter[] arrParamIn, IDbTransaction trans)
//        {
//            OracleCommand oCommand = null;
//            int rows = 0;
//            ArrayList outList = new ArrayList();
//            bool leaveOpen = false;
//            try
//            {
//                IDataParameter p = null;
//                oCommand = new OracleCommand();
//                oCommand.CommandText = Statement;
//                oCommand.CommandType = cmdType;
//                oCommand.Connection = (OracleConnection)trans.Connection;
//                oCommand.Transaction = (OracleTransaction)trans;

//                foreach (IDataParameter p_loopVariable in arrParamIn)
//                {
//                    p = p_loopVariable;
//                    oCommand.Parameters.Add(p);
//                }
//                rows = oCommand.ExecuteNonQuery();
//                oCommand.Parameters.Clear();
//                return rows;
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//            finally
//            {
//                oCommand.Dispose();
//            }

//        }

//        /// <summary>
//        /// Hàm thực thi câu lệnh không trả về dữ liệu record (dùng để thực hiện lệnh insert, update, delete hoặc các store procedure)
//        /// </summary>
//        /// <param name="Statement">Câu lệnh thực thi</param>
//        /// <param name="cmdType">Loại lệnh thực thi</param>
//        /// <param name="arrParamIn">Các tham số truyền vào câu lệnh</param>
//        /// <param name="trans">Transaction thực thi câu lệnh</param>
//        /// <returns>Kết quả thực hiện</returns>
//        /// <remarks>
//        ///</remarks>
//        private static int ExecuteNonQuery(string Statement, CommandType cmdType, OracleParameter[] arrParamIn, IDbTransaction trans)
//        {
//            OracleCommand oCommand = null;
//            int rows = 0;
//            ArrayList outList = new ArrayList();
//            bool leaveOpen = false;
//            try
//            {
//                IDataParameter p = null;
//                oCommand = new OracleCommand();
//                oCommand.CommandText = Statement;
//                oCommand.CommandType = cmdType;
//                oCommand.Connection = (OracleConnection)trans.Connection;
//                oCommand.Transaction = (OracleTransaction)trans;

//                foreach (OracleParameter p_loopVariable in arrParamIn)
//                {
//                    p = p_loopVariable;
//                    oCommand.Parameters.Add(p);
//                }
//                rows = oCommand.ExecuteNonQuery();
//                oCommand.Parameters.Clear();
//                return rows;
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//            finally
//            {
//                oCommand.Dispose();
//            }

//        }

//        /// <summary>
//        /// Hàm thực thi câu lệnh trả về dữ liệu là một Dataset
//        /// </summary>
//        /// <param name="Statement">Câu lệnh thực thi</param>
//        /// <param name="cmdType">Loại lệnh thực thi</param>
//        /// <param name="arrParamIn">Các tham số truyền vào câu lệnh</param>
//        /// <returns>Dataset chứa dữ liệu</returns>
//        /// <remarks>
//        ///</remarks>
//        private static DataSet ExecuteReturnDataSet(string Statement, CommandType cmdType, IDataParameter[] arrParamIn)
//        {
//            OracleCommand oCommand = null;
//            ArrayList outList = new ArrayList();
//            bool leaveOpen = false;
//            DataSet ds = new DataSet();
//            OracleDataAdapter da = default(OracleDataAdapter);

//            try
//            {
//                IDataParameter p = null;
//                oCommand = new OracleCommand();
//                oCommand.CommandText = Statement;
//                oCommand.CommandType = cmdType;
//                oCommand.Connection = GetConnection();

//                da = new OracleDataAdapter();

//                foreach (IDataParameter p_loopVariable in arrParamIn)
//                {
//                    p = p_loopVariable;
//                    oCommand.Parameters.Add(p);
//                }

//                da.SelectCommand = oCommand;
//                da.Fill(ds);
//                return ds;


//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//            finally
//            {
//                if (oCommand.Connection != null)
//                {
//                    oCommand.Connection.Close();
//                }
//                oCommand.Parameters.Clear();
//                oCommand.Dispose();
//            }

//        }


//        private static DataSet ExecuteReturnDataSet(string Statement, CommandType cmdType, IDataParameter[] arrParamIn, OracleConnection conn)
//        {
//            OracleCommand oCommand = null;
//            ArrayList outList = new ArrayList();
//            bool leaveOpen = false;
//            DataSet ds = new DataSet();
//            OracleDataAdapter da = default(OracleDataAdapter);

//            try
//            {
//                IDataParameter p = null;
//                oCommand = new OracleCommand();
//                oCommand.CommandText = Statement;
//                oCommand.CommandType = cmdType;
//                oCommand.Connection = conn;

//                da = new OracleDataAdapter();

//                foreach (IDataParameter p_loopVariable in arrParamIn)
//                {
//                    p = p_loopVariable;
//                    oCommand.Parameters.Add(p);
//                }

//                da.SelectCommand = oCommand;
//                da.Fill(ds);
//                return ds;


//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//            finally
//            {
//                oCommand.Parameters.Clear();
//                oCommand.Dispose();
//            }

//        }
//        /// <summary>
//        /// Hàm chuyển từ dữ liệu kiểu ngày về dạng số (dd/MM/yyyy thành yyyyMMdd)
//        /// </summary>
//        /// <param name="strDate">Ngày cần chuyển với định dạng dd/MM/yyyy</param>
//        /// <returns>Kết quả dạng yyyyMMdd</returns>
//        /// <remarks>
//        ///</remarks>
//        private static long ConvertDateToNumber(string strDate)
//        {
//            //-----------------------------------------------------
//            // Mục đích: Chuyển một string ngày tháng 'dd/MM/yyyy' --> dạng số 'yyyyMMdd'.
//            // Tham số: string đầu vào
//            // Giá trị trả về: 
//            // Ngày viết: 18/10/2007
//            // Người viết: Lê Hồng Hà
//            // ----------------------------------------------------
//            string[] arr = null;

//            try
//            {
//                string sep = "/";
//                strDate = strDate.Trim();
//                if (strDate.Length < 10)
//                    return 0;
//                arr = strDate.Split(sep.ToCharArray());
//                sep = "/";
//                arr[0] = arr[0].PadLeft(2, '0');
//                arr[1] = arr[1].PadLeft(2, '0');
//                if (arr[2].Length == 2)
//                {
//                    arr[2] = "20" + arr[2];
//                }

//            }
//            catch (Exception ex)
//            {
//                return Convert.ToInt64(DateTime.Now.ToString("yyyyMMdd"));
//            }

//            return long.Parse(arr[2] + arr[1] + arr[0]);
//        }


//        /// <summary>
//        /// Hàm lấy thông tin ngày làm việc của hệ thống theo mã nhân viên
//        /// </summary>
//        /// <param name="strMa_NV">Mã nhân viên</param>
//        /// <returns>Ngày làm việc của hệ thống</returns>
//        /// <remarks>
//        ///</remarks>
//        private static string TCS_GetNgayLV(string strMa_NV)
//        {
//            try
//            {
//                string strSQL = string.Empty;
//                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV'";
//                return (ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString())).ToString();
//            }
//            catch (Exception ex)
//            {
//                return null;
//            }
//        }

//        private static string TCS_GetGIO_DC()
//        {
//            try
//            {
//                string strSQL = string.Empty;
//                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='GIO_DC_CT'";
//                return (DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString()).ToString();
//            }
//            catch (Exception ex)
//            {
//                return "Lỗi trong quá trình lấy giờ đối chiếu";
//            }
//        }


//        /// <summary>
//        /// Hàm lấy thông tin ngày làm việc của hệ thống theo mã nhân viên
//        /// </summary>
//        /// <param name="strMa_NV">Mã nhân viên</param>
//        /// <returns>Ngày làm việc của hệ thống</returns>
//        /// <remarks>
//        ///</remarks>
//        private static Int64 TCS_GetSequenceValue(string p_Seq)
//        {
//            try
//            {
//                StringBuilder sbSQL = new StringBuilder();
//                sbSQL.Append("SELECT ");
//                sbSQL.Append(p_Seq);
//                sbSQL.Append(".nextval FROM DUAL");
//                String strSQL = sbSQL.ToString();
//                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
//                return Int64.Parse(strRet);
//            }
//            catch (Exception ex)
//            {
//                return -1;
//            }
//        }
//        #endregion "Utility"

//        #region "Truy vấn số thuế phải thu M11 - M12"

//        public static void ExecInsertSothue(TCS_DS_TOKHAI p_dsTkhai, ref IDbTransaction p_transCT, String p_SqlInsert)
//        {
//            IDbDataParameter[] p = new IDbDataParameter[28];

//            p[0] = new OracleParameter();
//            p[0].ParameterName = "MA_NNT";
//            p[0].DbType = DbType.String;
//            p[0].Direction = ParameterDirection.Input;
//            p[0].Value = p_dsTkhai.ma_nnt.Trim();

//            p[1] = new OracleParameter();
//            p[1].ParameterName = "TEN_NNT";
//            p[1].DbType = DbType.String;
//            p[1].Direction = ParameterDirection.Input;
//            p[1].Value = p_dsTkhai.ten_nnt.Trim();

//            p[2] = new OracleParameter();
//            p[2].ParameterName = "DIA_CHI";
//            p[2].DbType = DbType.String;
//            p[2].Direction = ParameterDirection.Input;
//            p[2].Value = p_dsTkhai.dia_chi.Trim();

//            p[3] = new OracleParameter();
//            p[3].ParameterName = "LH_XNK";
//            p[3].DbType = DbType.String;
//            p[3].Direction = ParameterDirection.Input;
//            p[3].Value = p_dsTkhai.lh_xnk.Trim();

//            p[4] = new OracleParameter();
//            p[4].ParameterName = "SO_TK";
//            p[4].DbType = DbType.String;
//            p[4].Direction = ParameterDirection.Input;
//            p[4].Value = p_dsTkhai.so_tk.Trim();

//            p[5] = new OracleParameter();
//            p[5].ParameterName = "NGAY_TK";
//            p[5].DbType = DbType.DateTime;
//            p[5].Direction = ParameterDirection.Input;
//            if (p_dsTkhai.ngay_tk != null)
//                p[5].Value = DateTime.ParseExact(p_dsTkhai.ngay_tk, "yyyy-MM-dd", null);
//            else
//                p[5].Value = DBNull.Value;

//            p[6] = new OracleParameter();
//            p[6].ParameterName = "MA_TINH";
//            p[6].DbType = DbType.String;
//            p[6].Direction = ParameterDirection.Input;
//            p[6].Value = p_dsTkhai.ma_tinh.Trim();

//            p[7] = new OracleParameter();
//            p[7].ParameterName = "MA_HUYEN";
//            p[7].DbType = DbType.String;
//            p[7].Direction = ParameterDirection.Input;
//            p[7].Value = p_dsTkhai.ma_huyen.Trim();

//            p[8] = new OracleParameter();
//            p[8].ParameterName = "MA_XA";
//            p[8].DbType = DbType.String;
//            p[8].Direction = ParameterDirection.Input;
//            p[8].Value = p_dsTkhai.ma_xa.Trim();

//            p[9] = new OracleParameter();
//            p[9].ParameterName = "MA_CQTHU";
//            p[9].DbType = DbType.String;
//            p[9].Direction = ParameterDirection.Input;
//            p[9].Value = p_dsTkhai.ma_cqthu.Trim();

//            p[10] = new OracleParameter();
//            p[10].ParameterName = "TK_THU_NS";
//            p[10].DbType = DbType.String;
//            p[10].Direction = ParameterDirection.Input;
//            p[10].Value = p_dsTkhai.tk_thu_ns.Trim();

//            p[11] = new OracleParameter();
//            p[11].ParameterName = "MA_KB";
//            p[11].DbType = DbType.String;
//            p[11].Direction = ParameterDirection.Input;
//            p[11].Value = p_dsTkhai.ma_kb.Trim();

//            p[12] = new OracleParameter();
//            p[12].ParameterName = "MA_CAP";
//            p[12].DbType = DbType.String;
//            p[12].Direction = ParameterDirection.Input;
//            p[12].Value = p_dsTkhai.ma_cap.Trim();

//            p[13] = new OracleParameter();
//            p[13].ParameterName = "MA_CHUONG";
//            p[13].DbType = DbType.String;
//            p[13].Direction = ParameterDirection.Input;
//            p[13].Value = p_dsTkhai.ma_chuong.Trim();

//            p[14] = new OracleParameter();
//            p[14].ParameterName = "MA_LOAI";
//            p[14].DbType = DbType.String;
//            p[14].Direction = ParameterDirection.Input;
//            p[14].Value = p_dsTkhai.ma_loai.Trim();

//            p[15] = new OracleParameter();
//            p[15].ParameterName = "MA_KHOAN";
//            p[15].DbType = DbType.String;
//            p[15].Direction = ParameterDirection.Input;
//            p[15].Value = p_dsTkhai.ma_khoan.Trim();

//            p[16] = new OracleParameter();
//            p[16].ParameterName = "MA_MUC";
//            p[16].DbType = DbType.String;
//            p[16].Direction = ParameterDirection.Input;
//            p[16].Value = p_dsTkhai.ma_muc.Trim();

//            p[17] = new OracleParameter();
//            p[17].ParameterName = "MA_TMUC";
//            p[17].DbType = DbType.String;
//            p[17].Direction = ParameterDirection.Input;
//            p[17].Value = p_dsTkhai.ma_tmuc.Trim();

//            p[18] = new OracleParameter();
//            p[18].ParameterName = "SO_PHAINOP";
//            p[18].DbType = DbType.String;
//            p[18].Direction = ParameterDirection.Input;
//            p[18].Value = p_dsTkhai.so_phainop;

//            p[19] = new OracleParameter();
//            p[19].ParameterName = "TT_NOP";
//            p[19].DbType = DbType.String;
//            p[19].Direction = ParameterDirection.Input;
//            p[19].Value = p_dsTkhai.tt_nop.Trim();

//            p[20] = new OracleParameter();
//            p[20].ParameterName = "MA_HQ_PH";
//            p[20].DbType = DbType.String;
//            p[20].Direction = ParameterDirection.Input;
//            p[20].Value = p_dsTkhai.ma_hq_ph.Trim();

//            p[21] = new OracleParameter();
//            p[21].ParameterName = "TEN_HQ_PH";
//            p[21].DbType = DbType.String;
//            p[21].Direction = ParameterDirection.Input;
//            p[21].Value = p_dsTkhai.ten_hq_ph.Trim();

//            p[22] = new OracleParameter();
//            p[22].ParameterName = "MA_HQ";
//            p[22].DbType = DbType.String;
//            p[22].Direction = ParameterDirection.Input;
//            p[22].Value = p_dsTkhai.ma_hq;

//            p[23] = new OracleParameter();
//            p[23].ParameterName = "TEN_HQ";
//            p[23].DbType = DbType.String;
//            p[23].Direction = ParameterDirection.Input;
//            p[23].Value = p_dsTkhai.ten_hq.Trim();

//            p[24] = new OracleParameter();
//            p[24].ParameterName = "MA_LT";
//            p[24].DbType = DbType.String;
//            p[24].Direction = ParameterDirection.Input;
//            p[24].Value = p_dsTkhai.ma_lt;

//            p[25] = new OracleParameter();
//            p[25].ParameterName = "MA_NTK";
//            p[25].DbType = DbType.String;
//            p[25].Direction = ParameterDirection.Input;
//            p[25].Value = p_dsTkhai.ma_ntk.Trim();

//            p[26] = new OracleParameter();
//            p[26].ParameterName = "TEN_NTK";
//            p[26].DbType = DbType.String;
//            p[26].Direction = ParameterDirection.Input;
//            p[26].Value = p_dsTkhai.ten_ntk.Trim();

//            p[27] = new OracleParameter();
//            p[27].ParameterName = "LCN_OWNER";
//            p[27].DbType = DbType.String;
//            p[27].Direction = ParameterDirection.Input;
//            p[27].Value = p_dsTkhai.lcn_owner.Trim();

//            ExecuteNonQuery(p_SqlInsert, CommandType.Text, p, p_transCT);
//        }


//        /// <summary>
//        /// Ham du du lieu MSG12 vao DB
//        /// </summary>
//        /// <returns>Oracle Connection</returns>
//        /// <param name="p_MaDTNT"></param>
//        /// <param name="p_TenDTNT"></param>
//        /// <param name="p_Ma_HQ"></param>
//        /// <param name="p_Ten_HQ"></param>
//        /// <param name="p_Ma_LH"></param>
//        /// <param name="p_Nam_DK"></param>
//        /// <param name="p_SoTK"></param>
//        /// <remarks>
//        ///</remarks>
//        public static void getMSG12(string p_MaDTNT, string p_Nam_DK, string p_SoTK, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG11 msg11 = new MSG11();
//                msg11.Data = new CustomsV3.MSG.MSG11.Data();
//                msg11.Header = new CustomsV3.MSG.MSG11.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg11.Header.Message_Version = strMessage_Version;
//                msg11.Header.Sender_Code = strSender_Code;
//                msg11.Header.Sender_Name = strSender_Name;
//                msg11.Header.Transaction_Type = "11";
//                msg11.Header.Transaction_Name = "Thông điệp truy vấn số thuế phải thu";
//                msg11.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg11.Header.Transaction_ID = Guid.NewGuid().ToString();

//                //Gan vao Data
//                msg11.Data.Ma_DV = p_MaDTNT;
//                msg11.Data.Nam_DK = p_Nam_DK;
//                msg11.Data.So_TK = p_SoTK;

//                //Chuyen thanh XML
//                string strXMLOut = msg11.MSG11toXML(msg11);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));

//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG11", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG12 msg12 = new MSG12();
//                MSG12 objTemp = msg12.MSGToObject(strXMLIn);
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;
//                //Verify sign                
//                if (verifySignMsg(objTemp, "MSG12"))
//                {
//                    //Cap nhan CSDL
//                    capnhatSothue(msg11.Data.Ma_DV, msg11.Data.So_TK, msg11.Data.Nam_DK, objTemp);
//                }
//                else
//                {
//                    throw new Exception("Sai chu ky");
//                }

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu MSG 12: ĐTNT - ");
//                sbErrMsg.Append(p_MaDTNT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                pv_ErrorNum = "99999";
//                pv_ErrorMes = ex.Message.ToString();
//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu MSG 12: ĐTNT - ");
//                    sbErrMsg.Append(p_MaDTNT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }
//            }

//        }

//        /// <summary>
//        /// Hàm cập nhật thông tin đăng ký thuế và sổ thuế từ cổng GIP vào CSDL
//        /// </summary>
//        /// <param name="pTIN">Mã ĐTNT</param>
//        /// <param name="thongTinDangKyThue">Thông tin đăng ký thuế</param>
//        /// <param name="thongTinThuePhaiNop">Thông tin sổ thuế</param>
//        /// <remarks>
//        ///</remarks>
//        private static void capnhatSothue(string p_MaDTNT, string p_SoTK, string p_namTK, MSG12 thongTinThuePhaiNop)
//        {
//            OracleConnection conn = GetConnection();
//            IDbTransaction transCT = null;
//            StringBuilder sbSqlInsert = new StringBuilder("");

//            sbSqlInsert.Append("INSERT INTO tcs_ds_tokhai (ma_nnt,");
//            sbSqlInsert.Append("                           so_tk,");
//            sbSqlInsert.Append("                           ma_cqthu,");
//            sbSqlInsert.Append("                           lh_xnk,");

//            sbSqlInsert.Append("                           ten_nnt,");
//            sbSqlInsert.Append("                           dia_chi,");
//            sbSqlInsert.Append("                           ngay_tk,");
//            sbSqlInsert.Append("                           ma_tinh,");
//            sbSqlInsert.Append("                           ma_huyen,");
//            sbSqlInsert.Append("                           ma_xa,");
//            sbSqlInsert.Append("                           tk_thu_ns,");
//            sbSqlInsert.Append("                           ma_kb,");
//            sbSqlInsert.Append("                           ma_cap,");
//            sbSqlInsert.Append("                           ma_chuong,");
//            sbSqlInsert.Append("                           ma_loai,");
//            sbSqlInsert.Append("                           ma_khoan,");
//            sbSqlInsert.Append("                           ma_muc,");
//            sbSqlInsert.Append("                           ma_tmuc,");
//            sbSqlInsert.Append("                           so_phainop,");
//            sbSqlInsert.Append("                           tt_nop,");
//            sbSqlInsert.Append("                           ma_hq_ph,");
//            sbSqlInsert.Append("                           ten_hq_ph,");
//            sbSqlInsert.Append("                           ma_hq,");
//            sbSqlInsert.Append("                           ten_hq,");
//            sbSqlInsert.Append("                           ma_lt,");
//            sbSqlInsert.Append("                           ma_ntk,");
//            sbSqlInsert.Append("                           ten_ntk,");
//            sbSqlInsert.Append("                           lcn_owner)");
//            sbSqlInsert.Append("  VALUES   (:ma_nnt,");
//            sbSqlInsert.Append("            :so_tk,");
//            sbSqlInsert.Append("            :ma_cqthu,");
//            sbSqlInsert.Append("            :lh_xnk,");
//            sbSqlInsert.Append("            :ten_nnt,");
//            sbSqlInsert.Append("            :dia_chi,");
//            sbSqlInsert.Append("            :ngay_tk,");
//            sbSqlInsert.Append("            :ma_tinh,");
//            sbSqlInsert.Append("            :ma_huyen,");
//            sbSqlInsert.Append("            :ma_xa,");
//            sbSqlInsert.Append("            :tk_thu_ns,");
//            sbSqlInsert.Append("            :ma_kb,");
//            sbSqlInsert.Append("            :ma_cap,");
//            sbSqlInsert.Append("            :ma_chuong,");
//            sbSqlInsert.Append("            :ma_loai,");
//            sbSqlInsert.Append("            :ma_khoan,");
//            sbSqlInsert.Append("            :ma_muc,");
//            sbSqlInsert.Append("            :ma_tmuc,");
//            sbSqlInsert.Append("            :so_phainop,");
//            sbSqlInsert.Append("            :tt_nop,");
//            sbSqlInsert.Append("                           :ma_hq_ph,");
//            sbSqlInsert.Append("                           :ten_hq_ph,");
//            sbSqlInsert.Append("                           :ma_hq,");
//            sbSqlInsert.Append("                           :ten_hq,");
//            sbSqlInsert.Append("                           :ma_lt,");
//            sbSqlInsert.Append("                           :ma_ntk,");
//            sbSqlInsert.Append("                           :ten_ntk,");
//            sbSqlInsert.Append("            :lcn_owner)");

//            try
//            {
//                //conn = GetConnection();
//                transCT = conn.BeginTransaction();

//                //Xoa du lieu truoc
//                //Khai bao cac cau lenh
//                StringBuilder sbSqlDelete = new StringBuilder("");
//                sbSqlDelete.Append("DELETE FROM TCS_DS_TOKHAI WHERE (MA_NNT = :MA_NNT) AND SO_TK=:SO_TK");

//                IDbDataParameter[] p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "MA_NNT";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = p_MaDTNT.Trim();

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "SO_TK";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = p_SoTK.Trim();

//                //p[2] = new OracleParameter();
//                //p[2].ParameterName = "NAM_TK";
//                //p[2].DbType = DbType.String;
//                //p[2].Direction = ParameterDirection.Input;
//                //p[2].Value = p_namTK.Trim();

//                //p[3] = new OracleParameter();
//                //p[3].ParameterName = "LH_XNK";
//                //p[3].DbType = DbType.String;
//                //p[3].Direction = ParameterDirection.Input;
//                //p[3].Value = p_Ma_LH.Trim();

//                ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                //Lay thong tin so thue phai nop dua vao CSDL
//                foreach (CustomsV3.MSG.MSG12.ItemData itdata in thongTinThuePhaiNop.Data.Item)
//                {
//                    //String strMaLT = itdata.Ma_LT.Trim();
//                    //if (strMaLT.Equals("1"))
//                    //{
//                    TCS_DS_TOKHAI dsTkhai = new TCS_DS_TOKHAI();
//                    //Lay cac thong tin giong nhau
//                    dsTkhai.ma_nnt = DBNull.Value.ToString();
//                    if (itdata.Ma_DV != null)
//                    {
//                        if (!itdata.Ma_DV.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_nnt = itdata.Ma_DV;
//                        }
//                    }

//                    dsTkhai.so_tk = DBNull.Value.ToString();
//                    if (itdata.So_TK != null)
//                    {
//                        if (!itdata.So_TK.Equals(string.Empty))
//                        {
//                            dsTkhai.so_tk = itdata.So_TK;
//                        }
//                    }

//                    dsTkhai.ma_cqthu = DBNull.Value.ToString();
//                    if (itdata.Ma_HQ_CQT != null)
//                    {
//                        if (!itdata.Ma_HQ_CQT.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_cqthu = itdata.Ma_HQ_CQT;
//                        }
//                    }

//                    dsTkhai.lh_xnk = DBNull.Value.ToString();
//                    if (itdata.Ma_LH != null)
//                    {
//                        if (!itdata.Ma_LH.Equals(string.Empty))
//                        {
//                            dsTkhai.lh_xnk = itdata.Ma_LH;
//                        }
//                    }
//                    dsTkhai.ten_lhxnk = DBNull.Value.ToString();
//                    if (itdata.Ten_LH != null)
//                    {
//                        if (!itdata.Ten_LH.Equals(string.Empty))
//                        {
//                            dsTkhai.ten_lhxnk = itdata.Ten_LH;
//                        }
//                    }
//                    dsTkhai.ten_nnt = DBNull.Value.ToString();
//                    if (itdata.Ten_DV != null)
//                    {
//                        if (!itdata.Ten_DV.Equals(string.Empty))
//                        {
//                            dsTkhai.ten_nnt = itdata.Ten_DV;
//                        }
//                    }

//                    dsTkhai.dia_chi = DBNull.Value.ToString();

//                    //DateTime dtngay_tk = DBNull.Value.ToString();
//                    if (itdata.Ngay_DK != null)
//                    {
//                        if (!itdata.Ngay_DK.Equals(string.Empty))
//                        {
//                            dsTkhai.ngay_tk = itdata.Ngay_DK;
//                        }
//                    }
//                    if (itdata.Nam_DK != null)
//                    {
//                        if (!itdata.Ngay_DK.Equals(string.Empty))
//                        {
//                            dsTkhai.nam_tk = itdata.Nam_DK;
//                        }
//                    }
//                    dsTkhai.ma_tinh = DBNull.Value.ToString();

//                    dsTkhai.ma_huyen = DBNull.Value.ToString();

//                    dsTkhai.ma_xa = DBNull.Value.ToString();

//                    dsTkhai.tk_thu_ns = DBNull.Value.ToString();
//                    if (itdata.TKKB != null)
//                    {
//                        if (!itdata.TKKB.Equals(string.Empty))
//                        {
//                            dsTkhai.tk_thu_ns = itdata.TKKB;
//                        }
//                    }

//                    dsTkhai.ma_kb = DBNull.Value.ToString();
//                    if (itdata.Ma_KB != null)
//                    {
//                        if (!itdata.Ma_KB.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_kb = itdata.Ma_KB;
//                        }
//                    }

//                    dsTkhai.tt_nop = DBNull.Value.ToString();
//                    if (itdata.TTNo_CT != null)
//                    {
//                        if (!itdata.TTNo_CT.Equals(string.Empty))
//                        {
//                            dsTkhai.tt_nop = itdata.TTNo_CT;
//                        }
//                    }
//                    if (itdata.Ma_HQ_PH != null)
//                    {
//                        if (!itdata.Ma_HQ_PH.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_hq_ph = itdata.Ma_HQ_PH;
//                        }
//                    }
//                    if (itdata.Ten_HQ_PH != null)
//                    {
//                        if (!itdata.Ten_HQ_PH.Equals(string.Empty))
//                        {
//                            dsTkhai.ten_hq_ph = itdata.Ten_HQ_PH;
//                        }
//                    }
//                    if (itdata.Ma_HQ != null)
//                    {
//                        if (!itdata.Ma_HQ.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_hq = itdata.Ma_HQ;
//                        }
//                    }
//                    if (itdata.Ten_HQ != null)
//                    {
//                        if (!itdata.Ten_HQ.Equals(string.Empty))
//                        {
//                            dsTkhai.ten_hq = itdata.Ten_HQ;
//                        }
//                    }
//                    if (itdata.Ma_LT != null)
//                    {
//                        if (!itdata.Ma_LT.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_lt = itdata.Ma_LT;
//                        }
//                    }
//                    if (itdata.Ma_NTK != null)
//                    {
//                        if (!itdata.Ma_NTK.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_ntk = itdata.Ma_NTK;
//                        }
//                    }
//                    if (itdata.Ten_NTK != null)
//                    {
//                        if (!itdata.Ten_NTK.Equals(string.Empty))
//                        {
//                            dsTkhai.ten_ntk = itdata.Ten_NTK;
//                        }
//                    }
//                    dsTkhai.ma_cap = DBNull.Value.ToString();

//                    dsTkhai.ma_loai = DBNull.Value.ToString();

//                    dsTkhai.ma_muc = DBNull.Value.ToString();
//                    dsTkhai.ma_chuong = itdata.Ma_Chuong;

//                    //lcn_owner
//                    dsTkhai.lcn_owner = "HQA0000000";
//                    //Bat dau gan cap, chuong, loai, khoan, muc, tieu muc de insert
//                    #region "Thue XK"
//                    //Bat dua thue xuat khau
//                    //chuong xuat khau

//                    //if (itdata.Chuong_XK != null)
//                    //{
//                    //    if (!itdata.Chuong_XK.Equals(string.Empty))
//                    //    {
//                    //        dsTkhai.ma_chuong = itdata.Chuong_XK;
//                    //    }
//                    //}

//                    //khoan xuat khau
//                    dsTkhai.ma_khoan = DBNull.Value.ToString();
//                    if (itdata.Khoan_XK != null)
//                    {
//                        if (!itdata.Khoan_XK.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_khoan = itdata.Khoan_XK;
//                        }
//                    }

//                    //tieu muc xuat khau
//                    dsTkhai.ma_tmuc = DBNull.Value.ToString();
//                    if (itdata.TieuMuc_XK != null)
//                    {
//                        if (!itdata.TieuMuc_XK.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_tmuc = itdata.TieuMuc_XK;
//                        }
//                    }

//                    //so thue xuat khau
//                    dsTkhai.so_phainop = 0;
//                    if (itdata.DuNo_XK != null)
//                    {
//                        if (!itdata.DuNo_XK.Equals(string.Empty))
//                        {
//                            try
//                            {
//                                dsTkhai.so_phainop = long.Parse(itdata.DuNo_XK);
//                            }
//                            catch
//                            {
//                                dsTkhai.so_phainop = 0;
//                            }
//                        }

//                    }

//                    if (dsTkhai.so_phainop > 0)
//                    {
//                        ExecInsertSothue(dsTkhai, ref transCT, sbSqlInsert.ToString());
//                    }

//                    //Ket thuc thue xuat khau
//                    #endregion "Thue XK"

//                    #region "Thue NK"
//                    //Bat dua thue nhap khau
//                    //chuong nhap khau
//                    //dsTkhai.ma_chuong = DBNull.Value.ToString();
//                    //if (itdata.Chuong_NK != null)
//                    //{
//                    //    if (!itdata.Chuong_NK.Equals(string.Empty))
//                    //    {
//                    //        dsTkhai.ma_chuong = itdata.Chuong_NK;
//                    //    }
//                    //}

//                    //khoan nhap khau
//                    dsTkhai.ma_khoan = DBNull.Value.ToString();
//                    if (itdata.Khoan_NK != null)
//                    {
//                        if (!itdata.Khoan_NK.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_khoan = itdata.Khoan_NK;
//                        }
//                    }

//                    //tieu muc nhap khau
//                    dsTkhai.ma_tmuc = DBNull.Value.ToString();
//                    if (itdata.TieuMuc_NK != null)
//                    {
//                        if (!itdata.TieuMuc_NK.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_tmuc = itdata.TieuMuc_NK;
//                        }
//                    }

//                    //so thue nhap khau
//                    dsTkhai.so_phainop = 0;
//                    if (itdata.DuNo_NK != null)
//                    {
//                        if (!itdata.DuNo_NK.Equals(string.Empty))
//                        {
//                            try
//                            {
//                                dsTkhai.so_phainop = long.Parse(itdata.DuNo_NK);
//                            }
//                            catch
//                            {
//                                dsTkhai.so_phainop = 0;
//                            }
//                        }
//                    }

//                    if (dsTkhai.so_phainop > 0)
//                    {
//                        ExecInsertSothue(dsTkhai, ref transCT, sbSqlInsert.ToString());
//                    }

//                    //Ket thuc thue nhap khau
//                    #endregion "Thue NK"

//                    #region "Thue VAT"
//                    //Bat dua thue VAT
//                    //chuong nhap khau
//                    //dsTkhai.ma_chuong = DBNull.Value.ToString();
//                    //if (itdata.Chuong_VA != null)
//                    //{
//                    //    if (!itdata.Chuong_VA.Equals(string.Empty))
//                    //    {
//                    //        dsTkhai.ma_chuong = itdata.Chuong_VA;
//                    //    }
//                    //}

//                    //khoan VAT
//                    dsTkhai.ma_khoan = DBNull.Value.ToString();
//                    if (itdata.Khoan_VA != null)
//                    {
//                        if (!itdata.Khoan_VA.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_khoan = itdata.Khoan_VA;
//                        }
//                    }

//                    //tieu muc VAT
//                    dsTkhai.ma_tmuc = DBNull.Value.ToString();
//                    if (itdata.TieuMuc_VA != null)
//                    {
//                        if (!itdata.TieuMuc_VA.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_tmuc = itdata.TieuMuc_VA;
//                        }
//                    }

//                    //so thue VAT
//                    dsTkhai.so_phainop = 0;
//                    if (itdata.DuNo_VA != null)
//                    {
//                        if (!itdata.DuNo_VA.Equals(string.Empty))
//                        {
//                            try
//                            {
//                                dsTkhai.so_phainop = long.Parse(itdata.DuNo_VA);
//                            }
//                            catch
//                            {
//                                dsTkhai.so_phainop = 0;
//                            }
//                        }
//                    }

//                    if (dsTkhai.so_phainop > 0)
//                    {
//                        ExecInsertSothue(dsTkhai, ref transCT, sbSqlInsert.ToString());
//                    }

//                    //Ket thuc thue VAT
//                    #endregion "Thue VAT"

//                    #region "Thue TD"
//                    //Bat dua thue TD
//                    //chuong TD
//                    //dsTkhai.ma_chuong = DBNull.Value.ToString();
//                    //if (itdata.Chuong_TD != null)
//                    //{
//                    //    if (!itdata.Chuong_TD.Equals(string.Empty))
//                    //    {
//                    //        dsTkhai.ma_chuong = itdata.Chuong_TD;
//                    //    }
//                    //}

//                    //khoan TD
//                    dsTkhai.ma_khoan = DBNull.Value.ToString();
//                    if (itdata.Khoan_TD != null)
//                    {
//                        if (!itdata.Khoan_TD.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_khoan = itdata.Khoan_TD;
//                        }
//                    }

//                    //tieu muc TD
//                    dsTkhai.ma_tmuc = DBNull.Value.ToString();
//                    if (itdata.TieuMuc_TD != null)
//                    {
//                        if (!itdata.TieuMuc_TD.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_tmuc = itdata.TieuMuc_TD;
//                        }
//                    }

//                    //so thue TD
//                    dsTkhai.so_phainop = 0;
//                    if (itdata.DuNo_TD != null)
//                    {
//                        if (!itdata.DuNo_TD.Equals(string.Empty))
//                        {
//                            try
//                            {
//                                dsTkhai.so_phainop = long.Parse(itdata.DuNo_TD);
//                            }
//                            catch
//                            {
//                                dsTkhai.so_phainop = 0;
//                            }
//                        }
//                    }

//                    if (dsTkhai.so_phainop > 0)
//                    {
//                        ExecInsertSothue(dsTkhai, ref transCT, sbSqlInsert.ToString());
//                    }

//                    //Ket thuc thue TD
//                    #endregion "Thue TD"

//                    #region "Thue TV"
//                    //Bat dua thue TV
//                    //chuong TV
//                    //dsTkhai.ma_chuong = DBNull.Value.ToString();
//                    //if (itdata.Chuong_TV != null)
//                    //{
//                    //    if (!itdata.Chuong_TV.Equals(string.Empty))
//                    //    {
//                    //        dsTkhai.ma_chuong = itdata.Chuong_TV;
//                    //    }
//                    //}

//                    //khoan TV
//                    dsTkhai.ma_khoan = DBNull.Value.ToString();
//                    if (itdata.Khoan_TV != null)
//                    {
//                        if (!itdata.Khoan_TV.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_khoan = itdata.Khoan_TV;
//                        }
//                    }

//                    //tieu muc TV
//                    dsTkhai.ma_tmuc = DBNull.Value.ToString();
//                    if (itdata.TieuMuc_TV != null)
//                    {
//                        if (!itdata.TieuMuc_TV.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_tmuc = itdata.TieuMuc_TV;
//                        }
//                    }

//                    //so thue TV
//                    dsTkhai.so_phainop = 0;
//                    if (itdata.DuNo_TV != null)
//                    {
//                        if (!itdata.DuNo_TV.Equals(string.Empty))
//                        {
//                            try
//                            {
//                                dsTkhai.so_phainop = long.Parse(itdata.DuNo_TV);
//                            }
//                            catch
//                            {
//                                dsTkhai.so_phainop = 0;
//                            }
//                        }
//                    }

//                    if (dsTkhai.so_phainop > 0)
//                    {
//                        ExecInsertSothue(dsTkhai, ref transCT, sbSqlInsert.ToString());
//                    }

//                    //Ket thuc thue TV
//                    #endregion "Thue TV"

//                    #region "Thue MT"
//                    //Bat dua thue MT
//                    //chuong MT
//                    //dsTkhai.ma_chuong = DBNull.Value.ToString();
//                    //if (itdata.Chuong_MT != null)
//                    //{
//                    //    if (!itdata.Chuong_MT.Equals(string.Empty))
//                    //    {
//                    //        dsTkhai.ma_chuong = itdata.Chuong_MT;
//                    //    }
//                    //}

//                    //khoan MT
//                    dsTkhai.ma_khoan = DBNull.Value.ToString();
//                    if (itdata.Khoan_MT != null)
//                    {
//                        if (!itdata.Khoan_MT.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_khoan = itdata.Khoan_MT;
//                        }
//                    }

//                    //tieu muc MT
//                    dsTkhai.ma_tmuc = DBNull.Value.ToString();
//                    if (itdata.TieuMuc_MT != null)
//                    {
//                        if (!itdata.TieuMuc_MT.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_tmuc = itdata.TieuMuc_MT;
//                        }
//                    }

//                    //so thue MT
//                    dsTkhai.so_phainop = 0;
//                    if (itdata.DuNo_MT != null)
//                    {
//                        if (!itdata.DuNo_MT.Equals(string.Empty))
//                        {
//                            try
//                            {
//                                dsTkhai.so_phainop = long.Parse(itdata.DuNo_MT);
//                            }
//                            catch
//                            {
//                                dsTkhai.so_phainop = 0;
//                            }
//                        }
//                    }

//                    if (dsTkhai.so_phainop > 0)
//                    {
//                        ExecInsertSothue(dsTkhai, ref transCT, sbSqlInsert.ToString());
//                    }

//                    //Ket thuc thue MT
//                    #endregion "Thue MT"

//                    #region "Thue KH"
//                    //Bat dua thue KH
//                    //chuong KH
//                    //dsTkhai.ma_chuong = DBNull.Value.ToString();
//                    //if (itdata.Chuong_KH != null)
//                    //{
//                    //    if (!itdata.Chuong_KH.Equals(string.Empty))
//                    //    {
//                    //        dsTkhai.ma_chuong = itdata.Chuong_KH;
//                    //    }
//                    //}

//                    //khoan KH
//                    dsTkhai.ma_khoan = DBNull.Value.ToString();
//                    if (itdata.Khoan_KH != null)
//                    {
//                        if (!itdata.Khoan_KH.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_khoan = itdata.Khoan_KH;
//                        }
//                    }

//                    //tieu muc KH
//                    dsTkhai.ma_tmuc = DBNull.Value.ToString();
//                    if (itdata.TieuMuc_KH != null)
//                    {
//                        if (!itdata.TieuMuc_KH.Equals(string.Empty))
//                        {
//                            dsTkhai.ma_tmuc = itdata.TieuMuc_KH;
//                        }
//                    }

//                    //so thue KH
//                    dsTkhai.so_phainop = 0;
//                    if (itdata.DuNo_KH != null)
//                    {
//                        if (!itdata.DuNo_KH.Equals(string.Empty))
//                        {
//                            try
//                            {
//                                dsTkhai.so_phainop = long.Parse(itdata.DuNo_KH);
//                            }
//                            catch
//                            {
//                                dsTkhai.so_phainop = 0;
//                            }
//                        }
//                    }

//                    if (dsTkhai.so_phainop > 0)
//                    {
//                        ExecInsertSothue(dsTkhai, ref transCT, sbSqlInsert.ToString());
//                    }

//                    //Ket thuc thue KH
//                    #endregion "Thue KH"
//                    //}
//                }
//                transCT.Commit();
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 12: ĐTNT - ");
//                sbErrMsg.Append(p_MaDTNT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 12: ĐTNT - ");
//                    sbErrMsg.Append(p_MaDTNT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;
//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }

//        }
//        private static void insertLogtest(String strMsgOut, String strMsgIn, String msg_type, String desc)
//        {
//            OracleConnection conn = GetConnection();
//            IDbTransaction transCT = null;
//            try
//            {

//                StringBuilder sbSqlInsert = new StringBuilder("");

//                String strS = "INSERT INTO tcs_log_msg_hq (id, msg_in, msg_out, date_send, description, msg_type) values (:id, :msg_in, :msg_out, :date_send, :description, :msg_type)";
//                transCT = conn.BeginTransaction();

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[6];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "ID";
//                p[0].DbType = DbType.Int64;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = 1;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "MSG_IN";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Size = 4000;
//                p[1].Value = strMsgIn;

//                //long ab = strMsgIn.Length;
//                //long ac = strMsgOut.Length;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "MSG_OUT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Size = 4000;
//                p[2].Value = strMsgOut;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "DATE_SEND";
//                p[3].DbType = DbType.Date;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = DateTime.Now;

//                p[4] = new OracleParameter();
//                p[4].ParameterName = "description";
//                p[4].DbType = DbType.String;
//                p[4].Direction = ParameterDirection.Input;
//                p[4].Value = desc;

//                p[5] = new OracleParameter();
//                p[5].ParameterName = "MSG_TYPE";
//                p[5].DbType = DbType.String;
//                p[5].Direction = ParameterDirection.Input;
//                p[5].Value = msg_type;


//                ExecuteNonQuery(strS, CommandType.Text, p, transCT);

//                transCT.Commit();
//            }
//            catch (Exception ex)
//            {

//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }
//        }
//        private static void insertLog(string strMsgOut, String strMsgIn, String msg_type, String desc)
//        {
//            OracleConnection conn = GetConnection();
//            OracleCommand myCMD = new OracleCommand();
//            try
//            {

//                StringBuilder sbSqlInsert = new StringBuilder("");

//                String strS = "INSERT INTO tcs_log_msg_hq (id, msg_in, msg_out, date_send, description, msg_type) values (:id, :msg_in, :msg_out, :date_send, :description, :msg_type)";

//                myCMD.Connection = conn;
//                myCMD.CommandText = strS;
//                myCMD.CommandType = CommandType.Text;
//                myCMD.Parameters.Add("id", OracleType.Int16, 1).Value = 1;
//                myCMD.Parameters.Add("msg_in", OracleType.Clob, 32767).Value = strMsgIn;
//                myCMD.Parameters.Add("msg_out", OracleType.Clob, 32767).Value = strMsgOut;
//                myCMD.Parameters.Add("date_send", OracleType.DateTime).Value = DateTime.Now;
//                myCMD.Parameters.Add("description", OracleType.VarChar).Value = desc;
//                myCMD.Parameters.Add("msg_type", OracleType.NVarChar).Value = msg_type;
//                myCMD.ExecuteNonQuery();

//            }
//            catch (Exception ex)
//            {

//            }
//            finally
//            {

//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    myCMD.Dispose();
//                    conn.Dispose();

//                    conn.Close();
//                }
//            }
//        }
//        #endregion "Truy vấn số thuế phải thu M11 - M12"
//        #region "Thông điệp truy vấn thông tin bảo lãnh chung M13- M14"

//        public static void getMSG14(string p_MaDTNT, string p_SoCT, string p_KHCT, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG13 msg13 = new MSG13();
//                msg13.Data = new CustomsV3.MSG.MSG13.Data();
//                msg13.Header = new CustomsV3.MSG.MSG13.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg13.Header.Message_Version = strMessage_Version;
//                msg13.Header.Sender_Code = strSender_Code;
//                msg13.Header.Sender_Name = strSender_Name;
//                msg13.Header.Transaction_Type = "13";
//                msg13.Header.Transaction_Name = "Thông điệp truy vấn thông tin bảo lãnh chung M13";
//                msg13.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg13.Header.Transaction_ID = Guid.NewGuid().ToString();

//                //Gan vao Data
//                msg13.Data.Ma_DV = p_MaDTNT;
//                msg13.Data.So_CT = p_SoCT;
//                msg13.Data.KyHieu_CT = p_KHCT;

//                //Chuyen thanh XML
//                string strXMLOut = msg13.MSG13toXML(msg13);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));

//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG13", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG14 msg14 = new MSG14();
//                MSG14 objTemp = msg14.MSGToObject(strXMLIn);
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;
//                //Verify sign                
//                if (verifySignMsg(objTemp, "MSG14"))
//                {
//                    //Cap nhan CSDL
//                    capnhatBLC(p_MaDTNT, p_SoCT, objTemp);
//                }
//                else
//                {
//                    throw new Exception("Sai chu ky");
//                }

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu MSG 12: ĐTNT - ");
//                sbErrMsg.Append(p_MaDTNT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                pv_ErrorNum = "99999";
//                pv_ErrorMes = ex.Message.ToString();
//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu MSG 12: ĐTNT - ");
//                    sbErrMsg.Append(p_MaDTNT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }
//            }

//        }

//        /// <summary>
//        /// Hàm cập nhật thông tin đăng ký thuế và sổ thuế từ cổng GIP vào CSDL
//        /// </summary>
//        /// <param name="pTIN">Mã ĐTNT</param>
//        /// <param name="thongTinDangKyThue">Thông tin đăng ký thuế</param>
//        /// <param name="thongTinThuePhaiNop">Thông tin sổ thuế</param>
//        /// <remarks>
//        ///</remarks>
//        private static void capnhatBLC(string p_MaDTNT, string p_SoCT, MSG14 thongtinblc)
//        {
//            OracleConnection conn = GetConnection();
//            IDbTransaction transCT = null;
//            StringBuilder sbSqlInsert = new StringBuilder("");

//            sbSqlInsert.Append("INSERT INTO tcs_BLC_HQ (so_ct,");
//            sbSqlInsert.Append("                           ma_nnthue,");
//            sbSqlInsert.Append("                           kh_ct,");
//            sbSqlInsert.Append("                           Ngay_CT,");
//            sbSqlInsert.Append("                           Ngay_HL,");
//            sbSqlInsert.Append("                           Ngay_HHL,");
//            sbSqlInsert.Append("                           HanMuc,");
//            sbSqlInsert.Append("                           SoDu,");
//            sbSqlInsert.Append("                           So_TK,");
//            sbSqlInsert.Append("                           Ngay_DK,");
//            sbSqlInsert.Append("                           So_SD,");
//            sbSqlInsert.Append("                           So_PH)");
//            sbSqlInsert.Append("  VALUES                   (:so_ct,");
//            sbSqlInsert.Append("                           :ma_nnthue,");
//            sbSqlInsert.Append("                           :kh_ct,");
//            sbSqlInsert.Append("                           :Ngay_CT,");
//            sbSqlInsert.Append("                           :Ngay_HL,");
//            sbSqlInsert.Append("                           :Ngay_HHL,");
//            sbSqlInsert.Append("                           :HanMuc,");
//            sbSqlInsert.Append("                           :SoDu,");
//            sbSqlInsert.Append("                           :So_TK,");
//            sbSqlInsert.Append("                           :Ngay_DK,");
//            sbSqlInsert.Append("                           :So_SD,");
//            sbSqlInsert.Append("                           :So_PH)");

//            try
//            {
//                //conn = GetConnection();
//                transCT = conn.BeginTransaction();

//                //Xoa du lieu truoc
//                //Khai bao cac cau lenh
//                StringBuilder sbSqlDelete = new StringBuilder("");
//                sbSqlDelete.Append("DELETE FROM tcs_BLC_HQ WHERE (ma_nnthue=: MA_NNT and so_ct = :SoCT )");

//                IDbDataParameter[] p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "MA_NNT";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = p_MaDTNT.Trim();

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "SoCT";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = p_SoCT.Trim();

//                ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                //Lay thong tin so thue phai nop dua vao CSDL
//                string strso_ct = DBNull.Value.ToString();
//                string strma_nnthue = DBNull.Value.ToString();
//                string strkh_ct = DBNull.Value.ToString();
//                string strNgay_CT = DBNull.Value.ToString();
//                string strNgay_HL = DBNull.Value.ToString();
//                string strNgay_HHL = DBNull.Value.ToString();
//                string strHanMuc = DBNull.Value.ToString();
//                string strSoDu = DBNull.Value.ToString();
//                string strSo_TK = DBNull.Value.ToString();
//                string strNgay_DK = DBNull.Value.ToString();
//                string strSo_SD = DBNull.Value.ToString();
//                string strSo_PH = DBNull.Value.ToString();

//                if (thongtinblc.Data.So_CT != null)
//                {
//                    if (!thongtinblc.Data.So_CT.Equals(string.Empty))
//                    {
//                        strso_ct = thongtinblc.Data.So_CT;
//                    }
//                }

//                if (thongtinblc.Data.Ma_DV != null)
//                {
//                    if (!thongtinblc.Data.Ma_DV.Equals(string.Empty))
//                    {
//                        strma_nnthue = thongtinblc.Data.Ma_DV;
//                    }
//                }

//                if (thongtinblc.Data.KyHieu_CT != null)
//                {
//                    if (!thongtinblc.Data.KyHieu_CT.Equals(string.Empty))
//                    {
//                        strkh_ct = thongtinblc.Data.KyHieu_CT;
//                    }
//                }
//                if (thongtinblc.Data.Ngay_CT != null)
//                {
//                    if (!thongtinblc.Data.Ngay_CT.Equals(string.Empty))
//                    {
//                        strNgay_CT = thongtinblc.Data.Ngay_CT;
//                    }
//                }
//                if (thongtinblc.Data.Ngay_HL != null)
//                {
//                    if (!thongtinblc.Data.Ngay_HL.Equals(string.Empty))
//                    {
//                        strNgay_HL = thongtinblc.Data.Ngay_HL;
//                    }
//                }
//                if (thongtinblc.Data.Ngay_HHL != null)
//                {
//                    if (!thongtinblc.Data.Ngay_HHL.Equals(string.Empty))
//                    {
//                        strNgay_HHL = thongtinblc.Data.Ngay_HHL;
//                    }
//                }
//                if (thongtinblc.Data.HanMuc != null)
//                {
//                    if (!thongtinblc.Data.HanMuc.Equals(string.Empty))
//                    {
//                        strHanMuc = thongtinblc.Data.HanMuc;
//                    }
//                }
//                if (thongtinblc.Data.Item.Count == 0)
//                {
//                    p = new IDbDataParameter[12];
//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "so_ct";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = strso_ct;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "ma_nnthue";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = strma_nnthue;

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "kh_ct";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strkh_ct;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "Ngay_CT";
//                    p[3].DbType = DbType.DateTime;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;
//                    if (strNgay_CT != null && strNgay_CT != "")
//                        p[3].Value = DateTime.ParseExact(strNgay_CT, "yyyy-MM-dd", null);

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "Ngay_HL";
//                    p[4].DbType = DbType.DateTime;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;
//                    if (strNgay_HL != null && strNgay_HL != "")
//                        p[4].Value = DateTime.ParseExact(strNgay_HL, "yyyy-MM-dd", null);

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "Ngay_HHL";
//                    p[5].DbType = DbType.DateTime;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = DBNull.Value;
//                    if (strNgay_HHL != null && strNgay_HHL != "")
//                        p[5].Value = DateTime.ParseExact(strNgay_HHL, "yyyy-MM-dd", null);

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "HanMuc";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = strHanMuc;

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "SoDu";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = strSoDu;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "So_TK";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = strSo_TK;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "Ngay_DK";
//                    p[9].DbType = DbType.DateTime;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;
//                    if (strNgay_DK != null && strNgay_DK != "")
//                        p[9].Value = DateTime.ParseExact(strNgay_DK, "yyyy-MM-dd", null);

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "So_SD";
//                    p[10].DbType = DbType.String;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = strSo_SD;

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "So_PH";
//                    p[11].DbType = DbType.String;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = strSo_PH;

//                    ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
//                    transCT.Commit();
//                    return;
//                }
//                foreach (CustomsV3.MSG.MSG14.ItemData itdata in thongtinblc.Data.Item)
//                {
//                    if (itdata.So_TK != null)
//                    {
//                        if (!itdata.So_TK.Equals(string.Empty))
//                        {
//                            strSo_TK = itdata.So_TK;
//                        }
//                    }
//                    if (itdata.Ngay_DK != null)
//                    {
//                        if (!itdata.Ngay_DK.Equals(string.Empty))
//                        {
//                            strNgay_DK = itdata.Ngay_DK;
//                        }
//                    }
//                    if (itdata.So_SD != null)
//                    {
//                        if (!itdata.So_SD.Equals(string.Empty))
//                        {
//                            strSo_SD = itdata.So_SD;
//                        }
//                    }
//                    if (itdata.So_SD != null)
//                    {
//                        if (!itdata.So_SD.Equals(string.Empty))
//                        {
//                            strSo_PH = itdata.So_PH;
//                        }
//                    }

//                    p = new IDbDataParameter[12];
//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "so_ct";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = strso_ct;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "ma_nnthue";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = strma_nnthue;

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "kh_ct";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strkh_ct;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "Ngay_CT";
//                    p[3].DbType = DbType.DateTime;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;
//                    if (strNgay_CT != null && strNgay_CT != "")
//                        p[3].Value = DateTime.ParseExact(strNgay_CT, "yyyy-MM-dd", null);

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "Ngay_HL";
//                    p[4].DbType = DbType.DateTime;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;
//                    if (strNgay_HL != null && strNgay_HL != "")
//                        p[4].Value = DateTime.ParseExact(strNgay_HL, "yyyy-MM-dd", null);

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "Ngay_HHL";
//                    p[5].DbType = DbType.DateTime;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = DBNull.Value;
//                    if (strNgay_HHL != null && strNgay_HHL != "")
//                        p[5].Value = DateTime.ParseExact(strNgay_HHL, "yyyy-MM-dd", null);

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "HanMuc";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = strHanMuc;

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "SoDu";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = strSoDu;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "So_TK";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = strSo_TK;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "Ngay_DK";
//                    p[9].DbType = DbType.DateTime;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;
//                    if (strNgay_DK != null && strNgay_DK != "")
//                        p[9].Value = DateTime.ParseExact(strNgay_DK, "yyyy-MM-dd", null);

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "So_SD";
//                    p[10].DbType = DbType.String;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = strSo_SD;

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "So_PH";
//                    p[11].DbType = DbType.String;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = strSo_PH;

//                    ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
//                }
//                transCT.Commit();
//            }
//            catch (Exception ex)
//            {
//                transCT.Rollback();
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 13: ĐTNT - ");
//                sbErrMsg.Append(p_MaDTNT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 12: ĐTNT - ");
//                    sbErrMsg.Append(p_MaDTNT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;
//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }

//        }

//        #endregion "Thông điệp truy vấn thông tin bảo lãnh chung (M13- M14)"
//        #region "M15-16-17-18"
//        public static void getMSG15(string p_LoaiDM, string p_MaDV, String pv_ErrorNum, String pv_ErrorMes)
//        {
//            try
//            {

//                MSG15 msg15 = new MSG15();
//                msg15.Data = new CustomsV3.MSG.MSG15.Data();
//                msg15.Header = new CustomsV3.MSG.MSG15.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg15.Header.Message_Version = strMessage_Version;
//                msg15.Header.Sender_Code = strSender_Code;
//                msg15.Header.Sender_Name = strSender_Name;
//                msg15.Header.Transaction_Type = "15";
//                msg15.Header.Transaction_Name = "Thông điệp truy vấn thông tin danh mục";
//                msg15.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg15.Header.Transaction_ID = Guid.NewGuid().ToString();

//                //Gan vao Data
//                msg15.Data.Loai_DM = p_LoaiDM;
//                msg15.Data.Ma_DV = p_MaDV;

//                //Chuyen thanh XML
//                string strXMLOut = msg15.MSG15toXML(msg15);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));

//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG15", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                // Verify sign
//                StoreDocSigned(strXMLIn,p_LoaiDM + ".xml");
//                if (p_LoaiDM == strDM_HQ)
//                {
//                    MSG16 msg16 = new MSG16();
//                    MSG16 objTemp = msg16.MSGToObject(strXMLIn);
//                    pv_ErrorNum = objTemp.Error.Error_Number;
//                    pv_ErrorMes = objTemp.Error.Error_Message;
//                    if (verifySignMsg(objTemp, "MSG16"))
//                    {
//                        //Cap nhan CSDL
//                        if (pv_ErrorNum == "0")
//                        {
//                            capnhatdmHQ(objTemp);
//                        }

//                    }
//                    else
//                    {
//                        throw new Exception("Sai chu ky");
//                    }
//                }
//                else if (p_LoaiDM == strDM_LH)
//                {
//                    MSG18 msg18 = new MSG18();
//                    MSG18 objTemp = msg18.MSGToObject(strXMLIn);
//                    pv_ErrorNum = objTemp.Error.Error_Number;
//                    pv_ErrorMes = objTemp.Error.Error_Message;
//                    if (verifySignMsg(objTemp, "MSG18"))
//                    {
//                        //Cap nhan CSDL
//                        if (pv_ErrorNum == "0")
//                        {
//                            capnhatdmLH(objTemp);
//                        }

//                    }
//                    else
//                    {
//                        throw new Exception("Sai chu ky");
//                    }
//                }
//                else if (p_LoaiDM == strDM_DV)
//                {
//                    MSG19 msg19 = new MSG19();
//                    MSG19 objTemp = msg19.MSGToObject(strXMLIn);
//                    pv_ErrorNum = objTemp.Error.Error_Number;
//                    pv_ErrorMes = objTemp.Error.Error_Message;
//                    if (verifySignMsg(objTemp, "MSG19"))
//                    {
//                        //Cap nhan CSDL
//                        if (pv_ErrorNum == "0")
//                        {
//                            capnhatDangkythue(p_MaDV, objTemp);
//                        }

//                    }
//                    else
//                    {
//                        throw new Exception("Sai chu ky");
//                    }
//                }
//                else if (p_LoaiDM == strDM_KB)
//                {
//                    MSG17 msg17 = new MSG17();
//                    MSG17 objTemp = msg17.MSGToObject(strXMLIn);
//                    pv_ErrorNum = objTemp.Error.Error_Number;
//                    pv_ErrorMes = objTemp.Error.Error_Message;
//                    if (verifySignMsg(objTemp, "MSG17"))
//                    {
//                        //Cap nhan CSDL
//                        if (pv_ErrorNum == "0")
//                        {
//                            capnhatdmSHKB(objTemp);
//                        }

//                    }
//                    else
//                    {
//                        throw new Exception("Sai chu ky");
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//            }
//        }
//        private static void capnhatdmHQ(MSG16 msg16)
//        {
//            OracleConnection conn = new OracleConnection();
//            IDbTransaction transCT = null;
//            //Tao cau lenh Insert
//            StringBuilder sbSqlInsert = new StringBuilder("");
//            sbSqlInsert.Append("INSERT INTO tcs_dm_hq (ma_hq,");
//            sbSqlInsert.Append("ma_cu,");
//            sbSqlInsert.Append("ma_qhns,");
//            sbSqlInsert.Append("ten)");
//            sbSqlInsert.Append(" VALUES(:ma_hq,");
//            sbSqlInsert.Append(":ma_cu,");
//            sbSqlInsert.Append(":ma_qhns,");
//            sbSqlInsert.Append(":ten)");
//            try
//            {
//                conn = GetConnection();
//                transCT = conn.BeginTransaction();
//                if (msg16.Data.Item == null)
//                {
//                    throw new Exception("Lỗi đồng bộ danh mục từ hải quan");
//                }

//                if (msg16.Data.Item.Count > 0)
//                {

//                    //Xoa du lieu truoc
//                    //Khai bao cac cau lenh
//                    StringBuilder sbSqlDelete = new StringBuilder("");
//                    sbSqlDelete.Append("DELETE FROM tcs_dm_hq_bk");
//                    IDbDataParameter[] p = new IDbDataParameter[0];
//                    ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                    sbSqlDelete = new StringBuilder("");
//                    sbSqlDelete.Append("INSERT INTO tcs_dm_hq_bk ( ma_hq, ma_cu, ma_qhns, ten,ngay_bk)");
//                    sbSqlDelete.Append(" SELECT ma_hq, ma_cu, ma_qhns, ten, sysdate FROM TCS_DM_HQ  ");
//                    p = new IDbDataParameter[0];
//                    ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                    sbSqlDelete = new StringBuilder("");
//                    sbSqlDelete.Append("DELETE FROM TCS_DM_HQ ");


//                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
//                    p = new IDbDataParameter[0];
//                    ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                    //Gan cac thong tin
//                    string vMa_HQ = DBNull.Value.ToString();
//                    string vMa_Cu = DBNull.Value.ToString();
//                    string vQHNS = DBNull.Value.ToString();
//                    string vTen = DBNull.Value.ToString();

//                    foreach (CustomsV3.MSG.MSG16.ItemData itdata in msg16.Data.Item)
//                    {

//                        if (itdata.Ma_HQ != null)
//                        {
//                            if (!itdata.Ma_HQ.Equals(string.Empty))
//                            {
//                                vMa_HQ = itdata.Ma_HQ;
//                            }
//                        }
//                        if (itdata.Ma_Cu != null)
//                        {
//                            if (!itdata.Ma_Cu.Equals(string.Empty))
//                            {
//                                vMa_Cu = itdata.Ma_Cu;
//                            }
//                        }
//                        if (itdata.Ma_QHNS != null)
//                        {
//                            if (!itdata.Ma_QHNS.Equals(string.Empty))
//                            {
//                                vQHNS = itdata.Ma_QHNS;
//                            }
//                        }
//                        if (itdata.Ten_HQ != null)
//                        {
//                            if (!itdata.Ten_HQ.Equals(string.Empty))
//                            {
//                                vTen = itdata.Ten_HQ;
//                            }
//                        }
//                        p = new IDbDataParameter[4];
//                        p[0] = new OracleParameter();
//                        p[0].ParameterName = "MA_HQ";
//                        p[0].DbType = DbType.String;
//                        p[0].Direction = ParameterDirection.Input;
//                        p[0].Value = vMa_HQ;

//                        p[1] = new OracleParameter();
//                        p[1].ParameterName = "ma_cu";
//                        p[1].DbType = DbType.String;
//                        p[1].Direction = ParameterDirection.Input;
//                        p[1].Value = vMa_Cu;


//                        p[2] = new OracleParameter();
//                        p[2].ParameterName = "ma_qhns";
//                        p[2].DbType = DbType.String;
//                        p[2].Direction = ParameterDirection.Input;
//                        p[2].Value = vQHNS;

//                        p[3] = new OracleParameter();
//                        p[3].ParameterName = "ten";
//                        p[3].DbType = DbType.String;
//                        p[3].Direction = ParameterDirection.Input;
//                        p[3].Value = vTen;
//                        ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
//                    }
//                }
//                transCT.Commit();
//            }
//            catch (Exception ex)
//            {
//                transCT.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                conn.Dispose();
//                conn.Close();
//            }

//        }
//        private static void capnhatdmLH(MSG18 msg18)
//        {
//            OracleConnection conn = new OracleConnection();
//            IDbTransaction transCT = null;
//            //Tao cau lenh Insert
//            StringBuilder sbSqlInsert = new StringBuilder("");
//            sbSqlInsert.Append("INSERT INTO tcs_dm_lhinh (ma_lh,");
//            sbSqlInsert.Append("ten_lh,");
//            sbSqlInsert.Append("ngay_anhan)");
//            sbSqlInsert.Append(" VALUES(:ma_lh,");
//            sbSqlInsert.Append(":ten_lh,");
//            sbSqlInsert.Append(":ngay_anhan)");
//            try
//            {
//                conn = GetConnection();
//                transCT = conn.BeginTransaction();
//                if (msg18.Data.Item == null)
//                {
//                    throw new Exception("Lỗi đồng bộ danh mục từ hải quan");
//                }

//                if (msg18.Data.Item.Count > 0)
//                {

//                    //Xoa du lieu truoc
//                    //Khai bao cac cau lenh
//                    StringBuilder sbSqlDelete = new StringBuilder("");
//                    sbSqlDelete.Append("DELETE FROM tcs_dm_lhinh_bk");
//                    IDbDataParameter[] p = new IDbDataParameter[0];
//                    ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                    sbSqlDelete = new StringBuilder("");
//                    sbSqlDelete.Append("INSERT INTO tcs_dm_lhinh_bk ( ma_lh, ten_lh, ngay_anhan,ngay_bk)");
//                    sbSqlDelete.Append(" SELECT ma_lh, ten_lh, ngay_anhan, sysdate FROM tcs_dm_lhinh  ");
//                    p = new IDbDataParameter[0];
//                    ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                    sbSqlDelete = new StringBuilder("");
//                    sbSqlDelete.Append("DELETE FROM tcs_dm_lhinh ");


//                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
//                    p = new IDbDataParameter[0];
//                    ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                    //Gan cac thong tin
//                    string vMa_LH = DBNull.Value.ToString();
//                    Int64 vNgay_AH = 0;
//                    string vTen = DBNull.Value.ToString();

//                    foreach (CustomsV3.MSG.MSG18.ItemData itdata in msg18.Data.Item)
//                    {

//                        if (itdata.Ma_LH != null)
//                        {
//                            if (!itdata.Ma_LH.Equals(string.Empty))
//                            {
//                                vMa_LH = itdata.Ma_LH;
//                            }
//                        }
//                        if (itdata.Ten_LH != null)
//                        {
//                            if (!itdata.Ten_LH.Equals(string.Empty))
//                            {
//                                vTen = itdata.Ten_LH;
//                            }
//                        }
//                        if (itdata.SN_AH != null)
//                        {
//                            if (!itdata.SN_AH.Equals(string.Empty))
//                            {
//                                vNgay_AH = Int64.Parse(itdata.SN_AH);
//                            }
//                        }
//                        p = new IDbDataParameter[3];
//                        p[0] = new OracleParameter();
//                        p[0].ParameterName = "ma_lh";
//                        p[0].DbType = DbType.String;
//                        p[0].Direction = ParameterDirection.Input;
//                        p[0].Value = vMa_LH;

//                        p[1] = new OracleParameter();
//                        p[1].ParameterName = "ten_lh";
//                        p[1].DbType = DbType.String;
//                        p[1].Direction = ParameterDirection.Input;
//                        p[1].Value = vTen;


//                        p[2] = new OracleParameter();
//                        p[2].ParameterName = "ngay_anhan";
//                        p[2].DbType = DbType.String;
//                        p[2].Direction = ParameterDirection.Input;
//                        p[2].Value = vNgay_AH;
//                        ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
//                    }
//                }
//                transCT.Commit();
//            }
//            catch (Exception ex)
//            {
//                transCT.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                conn.Dispose();
//                conn.Close();
//            }

//        }
//        private static void capnhatDangkythue(string pTIN, MSG19 msg19)
//        {
//            OracleConnection conn = default(OracleConnection);
//            IDbTransaction transCT = null;
//            //Tao cau lenh Insert
//            //Insert vao bang dang ky thue
//            StringBuilder sbSqlInsert = new StringBuilder("");
//            sbSqlInsert.Append("INSERT INTO tcs_dm_nnt (ma_nnt,");
//            sbSqlInsert.Append("ten_nnt,");
//            sbSqlInsert.Append("dia_chi,");
//            sbSqlInsert.Append("ma_tinh,");
//            sbSqlInsert.Append("ma_huyen,");
//            sbSqlInsert.Append("ma_xa,");
//            sbSqlInsert.Append("ma_cqthu,");
//            sbSqlInsert.Append("ma_chuong,");
//            sbSqlInsert.Append("trang_thai,");
//            sbSqlInsert.Append("ma_loai_nnt,");
//            sbSqlInsert.Append("so_cmt,");
//            sbSqlInsert.Append("ngay_cap_mst)");
//            sbSqlInsert.Append("VALUES   (:ma_nnt,");
//            sbSqlInsert.Append(":ten_nnt,");
//            sbSqlInsert.Append(":dia_chi,");
//            sbSqlInsert.Append(":ma_tinh,");
//            sbSqlInsert.Append(":ma_huyen,");
//            sbSqlInsert.Append(":ma_xa,");
//            sbSqlInsert.Append(":ma_cqthu,");
//            sbSqlInsert.Append(":ma_chuong,");
//            sbSqlInsert.Append(":trang_thai,");
//            sbSqlInsert.Append(":ma_loai_nnt,");
//            sbSqlInsert.Append(":so_cmt,");
//            sbSqlInsert.Append(":ngay_cap_mst)");
//            try
//            {
//                conn = GetConnection();
//                transCT = conn.BeginTransaction();
//                if (msg19.Data != null)
//                {
//                    //Xoa du lieu truoc
//                    StringBuilder sbSqlDelete = new StringBuilder("");
//                    sbSqlDelete.Append("DELETE FROM TCS_DM_NNT WHERE MA_NNT = :TIN");
//                    //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
//                    IDbDataParameter[] p = new IDbDataParameter[1];
//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "TIN";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = pTIN;

//                    ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);
//                    //Gan cac thong tin
//                    string vMaNnt = DBNull.Value.ToString();
//                    string vTenNnt = DBNull.Value.ToString();
//                    string vSoCMTNnt = DBNull.Value.ToString();
//                    string vMaCQQLT = DBNull.Value.ToString();
//                    string vMaChuong = DBNull.Value.ToString();
//                    string vMaloai_nnt = DBNull.Value.ToString();
//                    string vtrangthai = DBNull.Value.ToString();
//                    string vngaycap = DBNull.Value.ToString();
//                    string vDiaChi = DBNull.Value.ToString();
//                    string vMaTinh = DBNull.Value.ToString();
//                    string vMaHuyen = DBNull.Value.ToString();
//                    string vMaXa = DBNull.Value.ToString();
//                    CustomsV3.MSG.MSG19.Data itdata = msg19.Data;
//                    if (itdata.Ma_DV != null)
//                    {
//                        if (!itdata.Ma_DV.Equals(string.Empty))
//                        {
//                            vMaNnt = itdata.Ma_DV;
//                        }
//                    }
//                    if (itdata.Ten_DV != null)
//                    {
//                        if (!itdata.Ten_DV.Equals(string.Empty))
//                        {
//                            vTenNnt = itdata.Ten_DV;
//                        }
//                    }

//                    if (itdata.Ma_Chuong != null)
//                    {
//                        if (!itdata.Ma_Chuong.Equals(string.Empty))
//                        {
//                            vMaChuong = itdata.Ma_Chuong;
//                        }
//                    }


//                    if (itdata.DiaChi != null)
//                    {
//                        if (!itdata.DiaChi.Equals(string.Empty))
//                        {
//                            vDiaChi = itdata.DiaChi;
//                        }
//                    }

//                    p = new IDbDataParameter[12];
//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "MA_NNT";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = vMaNnt;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TEN_NNT";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = vTenNnt;

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "DIA_CHI";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = vDiaChi;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "MA_TINH";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = vMaTinh;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "MA_HUYEN";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = vMaHuyen;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "MA_XA";
//                    p[5].DbType = DbType.String;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = vMaXa;

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "MA_CQTHU";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = vMaCQQLT;

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "MA_CHUONG";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = vMaChuong;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "TRANG_THAI";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = vtrangthai;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "MA_LOAI_NNT";
//                    p[9].DbType = DbType.String;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = vMaloai_nnt;

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "SO_CMT";
//                    p[10].DbType = DbType.String;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = vSoCMTNnt;

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_CAP_MST";
//                    p[11].DbType = DbType.String;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = vngaycap;
//                    ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);

//                }
//                transCT.Commit();
//            }
//            catch (Exception ex)
//            {
//                transCT.Rollback();
//                // throw ex;
//            }
//            finally
//            {
//                conn.Dispose();
//                conn.Close();
//            }

//        }
//        private static void capnhatdmSHKB(MSG17 msg17)
//        {
//            OracleConnection conn = new OracleConnection();
//            IDbTransaction transCT = null;
//            //Tao cau lenh Insert
//            StringBuilder sbSqlInsert = new StringBuilder("");
//            /* Formatted on 20-Dec-2014 15:25:59 (QP5 v5.126) */
//            sbSqlInsert.Append("INSERT INTO tcs_dm_khobac (shkb,");
//            sbSqlInsert.Append("ten,");
//            sbSqlInsert.Append("ma_tinh,");
//            sbSqlInsert.Append("ma_huyen,");
//            sbSqlInsert.Append("ma_xa,");
//            sbSqlInsert.Append("tinh_trang,");
//            sbSqlInsert.Append("auto_khoaso,");
//            sbSqlInsert.Append("ma_db)");
//            sbSqlInsert.Append(" VALUES(:shkb,");
//            sbSqlInsert.Append(":ten,");
//            sbSqlInsert.Append(":ma_tinh,");
//            sbSqlInsert.Append(":ma_huyen,");
//            sbSqlInsert.Append(":ma_xa,");
//            sbSqlInsert.Append(":tinh_trang,");
//            sbSqlInsert.Append(":auto_khoaso,");
//            sbSqlInsert.Append(":ma_db)");
//            try
//            {
//                conn = GetConnection();
//                transCT = conn.BeginTransaction();
//                if (msg17.Error.Error_Number != "0")
//                {
//                    throw new Exception(msg17.Error.Error_Number + msg17.Error.Error_Message);
//                }

//                //Xoa du lieu truoc
//                //Khai bao cac cau lenh
//                StringBuilder sbSqlDelete = new StringBuilder("");

//                sbSqlDelete.Append("DELETE FROM tcs_dm_khobac_bk");
//                IDbDataParameter[] p = new IDbDataParameter[0];
//                ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                sbSqlDelete = new StringBuilder("");
//                sbSqlDelete.Append("INSERT INTO tcs_dm_khobac_bk ( shkb, ten, ma_tinh, ma_huyen, ma_xa, tinh_trang,auto_khoaso,ma_db)");
//                sbSqlDelete.Append(" SELECT shkb, ten, ma_tinh, ma_huyen, ma_xa, tinh_trang,auto_khoaso,ma_db FROM tcs_dm_khobac ");
//                p = new IDbDataParameter[0];
//                ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                sbSqlDelete = new StringBuilder("");
//                sbSqlDelete.Append("DELETE FROM tcs_dm_khobac");


//                //Dim p[) As IDbDataParameter = New IDbDataParameter(1) {}
//                p = new IDbDataParameter[0];
//                ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

//                //Gan cac thong tin
//                string vshkb = DBNull.Value.ToString();
//                string vten = DBNull.Value.ToString();
//                string vma_tinh = DBNull.Value.ToString();
//                string vma_huyen = DBNull.Value.ToString();
//                string vma_xa = DBNull.Value.ToString();
//                string vtinh_trang = "1";
//                string vauto_khoaso = "1";
//                string vma_db = DBNull.Value.ToString();
//                Int64 vNgayBD = 0;
//                Int64 vNgayKT = 0;
//                //Int64 vcch_id = 0;
//                foreach (CustomsV3.MSG.MSG17.ItemData itdata in msg17.Data.Item)
//                {
//                    if (itdata.Ma_KB != null)
//                    {
//                        if (!itdata.Ma_KB.Equals(string.Empty))
//                        {
//                            vshkb = itdata.Ma_KB;
//                        }
//                    }
//                    if (itdata.Ten_KB != null)
//                    {
//                        if (!itdata.Ten_KB.Equals(string.Empty))
//                        {
//                            vten = itdata.Ten_KB;
//                        }
//                    }
//                    //if (danhmuc_ct.MA_TINH != null)
//                    //{
//                    //    if (!danhmuc_ct.MA_TINH.Equals(string.Empty))
//                    //    {
//                    //        vma_tinh = danhmuc_ct.MA_TINH;
//                    //    }
//                    //}
//                    //if (danhmuc_ct.MA_HUYEN != null)
//                    //{
//                    //    if (!danhmuc_ct.MA_HUYEN.Equals(string.Empty))
//                    //    {
//                    //        vma_huyen = danhmuc_ct.MA_HUYEN;
//                    //        vma_db = danhmuc_ct.MA_HUYEN;
//                    //    }
//                    //}
//                    //if (danhmuc_ct.MA_XA != null)
//                    //{
//                    //    if (!danhmuc_ct.MA_XA.Equals(string.Empty))
//                    //    {
//                    //        vma_xa = danhmuc_ct.MA_XA;
//                    //    }
//                    //}

//                    //if (danhmuc_ct.HIEU_LUC_TU != null)
//                    //{
//                    //    if (!danhmuc_ct.HIEU_LUC_TU.Equals(string.Empty))
//                    //    {
//                    //        vNgayBD = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_TU);
//                    //    }
//                    //}
//                    //if (danhmuc_ct.HIEU_LUC_DEN != null)
//                    //{
//                    //    if (!danhmuc_ct.HIEU_LUC_DEN.Equals(string.Empty))
//                    //    {
//                    //        vNgayKT = ConvertDateToNumber(danhmuc_ct.HIEU_LUC_DEN);
//                    //    }
//                    //}
//                    ////   vcch_id = TCS_GetSequence("TCS_CCH_ID_SEQ");


//                    p = new IDbDataParameter[8];
//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "shkb";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = vshkb;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "ten";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = vten;

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "ma_tinh";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = vma_tinh;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "ma_huyen";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = vma_huyen;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "ma_xa";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = vma_xa;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "tinh_trang";
//                    p[5].DbType = DbType.String;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = vtinh_trang;

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "auto_khoaso";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = vauto_khoaso;

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "ma_db";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = vma_db;
//                    ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
//                }
//                transCT.Commit();
//            }
//            catch (Exception ex)
//            {
//                transCT.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                conn.Dispose();
//                conn.Close();
//            }

//        }
//        #endregion "M15_M16-17-18-19"
//        #region "Xác nhận nộp thuế của ngân hàng M21 - M22"

//        public static void CTUToMSG21(String pv_strMaNTT, String pv_strSHKB,
//                                          String pv_strSoCT, String pv_strSoBT,
//                                          String pv_strMaNV, ref MSG21 msg21)
//        {
//            msg21.Data = new CustomsV3.MSG.MSG21.Data();
//            msg21.Data.Ma_DV = pv_strMaNTT;

//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ten_nh_b ten_nh_th,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         h.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         h.ma_hq ma_hq,");
//                sbsql.Append("         h.lh_xnk ma_lh,");
//                sbsql.Append("         h.so_tk sotk,");
//                sbsql.Append("         to_char(h.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         h.loai_tt ma_lt,");
//                //sbsql.Append("         NVL(h.ma_ntk,DECODE(SUBSTR(h.tk_co,0,3),'741','1','711','1','2')) ma_ntk,");
//                sbsql.Append("         h.ma_ntk ma_ntk,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         ma_nh_a ma_nh_ph,");
//                sbsql.Append("         ma_nh_b ma_nh_th,");
//                sbsql.Append("         h.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (h.shkb) ten_kb,");
//                sbsql.Append("         h.tk_co tkkb,");
//                sbsql.Append("         h.tk_co tkkb_ct,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'YYYY-MM-DD') ngay_ct,");
//                sbsql.Append("         h.ly_do diengiai,");
//                sbsql.Append("         h.ttien ttien");
//                sbsql.Append("  FROM   tcs_ctu_hdr h");
//                sbsql.Append(" WHERE   ((h.shkb = :shkb)");
//                sbsql.Append("          AND (h.ma_nv = :ma_nv)");
//                sbsql.Append("          AND (h.so_ct = '" + pv_strSoCT + "')");
//                sbsql.Append("          AND (h.so_bt = :so_bt)");
//                sbsql.Append("          AND (h.NGAY_KB = :ngay_kb))");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[4];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "SHKB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_strSHKB;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "MA_NV";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_strMaNV;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "SO_BT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strSoBT;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "ngay_kb";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = v_ngaylv;


//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg21.Data.Ma_NH_PH = strSender_Code;
//                msg21.Data.Ten_NH_PH = strTen_NH_PH;
//                msg21.Data.Ma_NH_TH = ds.Tables[0].Rows[0]["MA_NH_TH"].ToString();
//                msg21.Data.Ten_NH_TH = ds.Tables[0].Rows[0]["TEN_NH_TH"].ToString();
//                msg21.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg21.Data.Ma_HQ_CQT = ds.Tables[0].Rows[0]["MA_HQ_CQT"].ToString();
//                msg21.Data.Ma_HQ_PH = ds.Tables[0].Rows[0]["MA_HQ_PH"].ToString();
//                msg21.Data.Ma_HQ = ds.Tables[0].Rows[0]["MA_HQ"].ToString();
//                msg21.Data.Ma_LH = ds.Tables[0].Rows[0]["MA_LH"].ToString();
//                msg21.Data.So_TK = ds.Tables[0].Rows[0]["SOTK"].ToString();
//                msg21.Data.Ngay_DK = ds.Tables[0].Rows[0]["NGAY_DK"].ToString();
//                msg21.Data.Ma_LT = ds.Tables[0].Rows[0]["MA_LT"].ToString();
//                msg21.Data.Ma_NTK = ds.Tables[0].Rows[0]["MA_NTK"].ToString();
//                //if ((ds.Tables[0].Rows[0]["MA_NTK"].ToString().Substring(0, 3) == "741") || (ds.Tables[0].Rows[0]["MA_NTK"].ToString().Substring(0, 4) == "7111"))
//                //{
//                //    msg21.Data.Ma_NTK = "1";
//                //}
//                //else {
//                //    msg21.Data.Ma_NTK = "2";
//                //}
//                msg21.Data.Loai_CT = strLoai_CT;// GNT từ ngân hàng không có TKKB
//                msg21.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg21.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//                msg21.Data.TTButToan = ds.Tables[0].Rows[0]["TTBUTTOAN"].ToString();//Hoi lai
//                msg21.Data.Ma_KB = ds.Tables[0].Rows[0]["MA_KB"].ToString();//Hoi lai
//                msg21.Data.Ten_KB = Globals.RemoveSign4VietnameseString(ds.Tables[0].Rows[0]["TEN_KB"].ToString());//Hoi lai
//                msg21.Data.TKKB = ds.Tables[0].Rows[0]["TKKB"].ToString();
//                msg21.Data.TKKB_CT = ds.Tables[0].Rows[0]["TKKB_CT"].ToString();
//                msg21.Data.Ngay_BN = ds.Tables[0].Rows[0]["NGAY_BN"].ToString();
//                msg21.Data.Ngay_BC = ds.Tables[0].Rows[0]["NGAY_BC"].ToString();
//                msg21.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();
//                msg21.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();
//                msg21.Data.DuNo_TO = ds.Tables[0].Rows[0]["TTIEN"].ToString();

//                //Lay thong tin detail
//                sbsql = new StringBuilder();
//                sbsql.Append("SELECT   d.ky_thue,");
//                sbsql.Append("         d.ma_chuong,");
//                sbsql.Append("         d.ma_khoan,");
//                sbsql.Append("         d.ma_tmuc,");
//                sbsql.Append("         d.sotien");
//                sbsql.Append("  FROM   tcs_ctu_dtl d");
//                sbsql.Append(" WHERE   (    (d.shkb = :shkb)");
//                sbsql.Append("          AND (d.ma_nv = :ma_nv)");
//                // sbsql.Append("          AND (d.so_ct = :so_ct)");
//                sbsql.Append("          AND (d.so_bt = :so_bt)");
//                sbsql.Append("          AND (d.ngay_kb = :ngay_kb))");


//                //Lay thong tin chi tiet chung tu

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg21.Data.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();

//                foreach (DataRow row in ds.Tables[0].Rows)
//                {
//                    bool bFlag_Gan_TMT = false;
//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("185"))
//                    {
//                        msg21.Data.Khoan_XK = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_XK = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_XK = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("190") && (!row["ma_tmuc"].ToString().Equals("1903")))
//                    {
//                        msg21.Data.Khoan_NK = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_NK = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_NK = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue VA
//                    if (row["ma_tmuc"].ToString().StartsWith("170"))
//                    {
//                        msg21.Data.Khoan_VA = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_VA = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_VA = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TD
//                    if (row["ma_tmuc"].ToString().StartsWith("175") )
//                    {
//                        msg21.Data.Khoan_TD = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_TD = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_TD = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TV
//                    if (row["ma_tmuc"].ToString().StartsWith("195") || row["ma_tmuc"].ToString().Equals("1903"))
//                    {
//                        msg21.Data.Khoan_TV = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_TV = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_TV = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                    //Gan so tien thue MT
//                    if (row["ma_tmuc"].ToString().StartsWith("200"))
//                    {
//                        msg21.Data.Khoan_MT = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_MT = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_MT = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }//4253
//                    if (row["ma_tmuc"].ToString().StartsWith("425"))
//                    {
//                        msg21.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_KH = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    if (bFlag_Gan_TMT == false)
//                    {
//                        msg21.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_KH = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                }


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUToMSG21): ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append(", SHKB - ");
//                sbErrMsg.Append(pv_strSHKB);
//                sbErrMsg.Append(", So CT - ");
//                sbErrMsg.Append(pv_strSoCT);
//                sbErrMsg.Append(", So BT - ");
//                sbErrMsg.Append(pv_strSoBT);
//                sbErrMsg.Append(", Ma NV - ");
//                sbErrMsg.Append(pv_strMaNV);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUToMSG21): ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append(", SHKB - ");
//                    sbErrMsg.Append(pv_strSHKB);
//                    sbErrMsg.Append(", So CT - ");
//                    sbErrMsg.Append(pv_strSoCT);
//                    sbErrMsg.Append(", So BT - ");
//                    sbErrMsg.Append(pv_strSoBT);
//                    sbErrMsg.Append(", Ma NV - ");
//                    sbErrMsg.Append(pv_strMaNV);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }
//        /*public static void updateStatusCT(String pv_strMaNTT, String pv_strSHKB,
//                                          String pv_strSoCT, String pv_strSoBT,
//                                          String pv_strMaNV, String pv_strMaDThu, String pv_strStatus)
//        {
//            try
//            {
//                //Lay thong tin header
//                OracleConnection conn = GetConnection();
//                IDbTransaction transCT = null;
//                transCT = conn.BeginTransaction();
//                StringBuilder sbsql = new StringBuilder();
//                sbsql.Append("UPDATE   tcs_ctu_hdr h set h.trang_thai = '" + pv_strStatus + "'");
//                sbsql.Append(" WHERE   (    (h.shkb = :shkb)");
//                sbsql.Append("          AND (h.ngay_kb = :ngay_kb)");
//                sbsql.Append("          AND (h.ma_nv = :ma_nv)");
//                sbsql.Append("          AND (h.ma_dthu = :ma_dthu)");
//                sbsql.Append("          AND (h.so_bt = :so_bt))");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[5];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "SHKB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_strSHKB;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAY_KB";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = v_ngaylv;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "MA_NV";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strMaNV;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "MA_DTHU";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = pv_strMaDThu;

//                p[4] = new OracleParameter();
//                p[4].ParameterName = "SO_BT";
//                p[4].DbType = DbType.String;
//                p[4].Direction = ParameterDirection.Input;
//                p[4].Value = pv_strSoBT;

//                ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);
//                transCT.Commit;

//            }
//            catch (Exception ex)
//            {
                
//                throw ex;

//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }


//        }*/
//        /// <summary>
//        /// Ham gui lay du lieu MSG 22
//        /// </summary>
//        /// <returns>Oracle Connection</returns>
//        /// <param name="p_MaDTNT"></param>
//        /// <param name="p_TenDTNT"></param>
//        /// <param name="p_Ma_HQ"></param>
//        /// <param name="p_Ten_HQ"></param>
//        /// <param name="p_Ma_LH"></param>
//        /// <param name="p_Nam_DK"></param>
//        /// <param name="p_SoTK"></param>
//        /// <remarks>
//        ///</remarks>
//        public static void getMSG22(String pv_strMaNTT, String pv_strSHKB,
//                                          String pv_strSoCT, String pv_strSoBT,
//                                          String pv_strMaNV, String pv_strMaDThu, ref String pv_ErrorNum, ref String pv_ErrorMes,
//                                            ref String pv_transaction_id, ref String pv_so_tn_ct, ref String pv_ngay_tn_ct)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG21 msg21 = new MSG21();
//                msg21.Data = new CustomsV3.MSG.MSG21.Data();
//                msg21.Header = new CustomsV3.MSG.MSG21.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg21.Header.Message_Version = strMessage_Version;
//                msg21.Header.Sender_Code = strSender_Code;
//                msg21.Header.Sender_Name = strSender_Name;
//                msg21.Header.Transaction_Type = "21";
//                msg21.Header.Transaction_Name = "Thông điệp xác nhận nộp thuế của ngân hàng";
//                msg21.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                pv_transaction_id = Guid.NewGuid().ToString();
//                msg21.Header.Transaction_ID = pv_transaction_id;

//                //Gan du lieu Data
//                CTUToMSG21(pv_strMaNTT, pv_strSHKB,
//                                           pv_strSoCT, pv_strSoBT,
//                                           pv_strMaNV, ref msg21);

//                //Chuyen thanh XML
//                string strXMLOut = msg21.MSG21toXML(msg21);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG21", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG22 msg22 = new MSG22();
//                MSG22 objTemp = msg22.MSGToObject(strXMLIn);

//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                if (pv_ErrorNum.Equals("0"))
//                {
//                    pv_so_tn_ct = objTemp.Data.So_TN_CT;
//                    pv_ngay_tn_ct = objTemp.Data.Ngay_TN_CT;
//                }

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = sbErrMsg.ToString();

//                //throw ex;
//            }

//        }

//        #endregion "Xác nhận nộp thuế của ngân hàng M21 - M22"
//        #region "Xác nhận nộp thuế bằng ngoại tệ M22-M23"
//        public static void CTUToMSG23(String pv_strMaNTT, String pv_strSHKB,
//                                         String pv_strSoCT, String pv_strSoBT,
//                                         String pv_strMaNV, ref MSG23 msg23)
//        {
//            msg23.Data = new CustomsV3.MSG.MSG23.Data();
//            msg23.Data.Ma_DV = pv_strMaNTT;

//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ten_nh_b ten_nh_th,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         h.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         h.ma_hq ma_hq,");
//                sbsql.Append("         h.lh_xnk ma_lh,");
//                sbsql.Append("         h.so_tk sotk,");
//                sbsql.Append("         to_char(h.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         h.loai_tt ma_lt,");
//                //sbsql.Append("         NVL(h.ma_ntk,DECODE(SUBSTR(h.tk_co,0,3),'741','1','711','1','2')) ma_ntk,");
//                sbsql.Append("         h.ma_ntk ma_ntk,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         ma_nh_a ma_nh_ph,");
//                sbsql.Append("         ma_nh_b ma_nh_th,");
//                sbsql.Append("         h.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (h.shkb) ten_kb,");
//                sbsql.Append("         h.tk_co tkkb,");
//                sbsql.Append("         h.tk_co tkkb_ct,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'YYYY-MM-DD') ngay_ct,");
//                sbsql.Append("         h.ly_do diengiai,");
//                sbsql.Append("         h.ttien ttien,");
//                sbsql.Append("         h.ttien_nt ttien_nt,");
//                sbsql.Append("         h.ma_nt ma_nt,");
//                sbsql.Append("         h.ty_gia ty_gia");
//                sbsql.Append("  FROM   tcs_ctu_hdr h");
//                sbsql.Append(" WHERE   ((h.shkb = :shkb)");
//                sbsql.Append("          AND (h.ma_nv = :ma_nv)");
//                sbsql.Append("          AND (h.so_ct = '" + pv_strSoCT + "')");
//                sbsql.Append("          AND (h.so_bt = :so_bt)");
//                sbsql.Append("          AND (h.NGAY_KB = :ngay_kb))");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[4];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "SHKB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_strSHKB;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "MA_NV";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_strMaNV;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "SO_BT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strSoBT;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "ngay_kb";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = v_ngaylv;


//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg23.Data.Ma_NH_PH = strSender_Code;
//                msg23.Data.Ten_NH_PH = strTen_NH_PH;
//                msg23.Data.Ma_NH_TH = ds.Tables[0].Rows[0]["MA_NH_TH"].ToString();
//                msg23.Data.Ten_NH_TH = ds.Tables[0].Rows[0]["TEN_NH_TH"].ToString();
//                msg23.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg23.Data.Ma_HQ_CQT = ds.Tables[0].Rows[0]["MA_HQ_CQT"].ToString();
//                msg23.Data.Ma_HQ_PH = ds.Tables[0].Rows[0]["MA_HQ_PH"].ToString();
//                msg23.Data.Ma_HQ = ds.Tables[0].Rows[0]["MA_HQ"].ToString();
//                msg23.Data.Ma_LH = ds.Tables[0].Rows[0]["MA_LH"].ToString();
//                msg23.Data.So_TK = ds.Tables[0].Rows[0]["SOTK"].ToString();
//                msg23.Data.Ngay_DK = ds.Tables[0].Rows[0]["NGAY_DK"].ToString();
//                msg23.Data.Ma_LT = ds.Tables[0].Rows[0]["MA_LT"].ToString();
//                msg23.Data.Ma_NTK = ds.Tables[0].Rows[0]["MA_NTK"].ToString();
//                //if ((ds.Tables[0].Rows[0]["MA_NTK"].ToString().Substring(0, 3) == "741") || (ds.Tables[0].Rows[0]["MA_NTK"].ToString().Substring(0, 4) == "7111"))
//                //{
//                //    msg23.Data.Ma_NTK = "1";
//                //}
//                //else {
//                //    msg23.Data.Ma_NTK = "2";
//                //}
//                msg23.Data.Loai_CT = strLoai_CT;// GNT từ ngân hàng không có TKKB
//                msg23.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg23.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//                msg23.Data.TTButToan = ds.Tables[0].Rows[0]["TTBUTTOAN"].ToString();//Hoi lai
//                msg23.Data.Ma_KB = ds.Tables[0].Rows[0]["MA_KB"].ToString();//Hoi lai
//                msg23.Data.Ten_KB = Globals.RemoveSign4VietnameseString(ds.Tables[0].Rows[0]["TEN_KB"].ToString());//Hoi lai
//                msg23.Data.TKKB = ds.Tables[0].Rows[0]["TKKB"].ToString();
//                msg23.Data.TKKB_CT = ds.Tables[0].Rows[0]["TKKB_CT"].ToString();
//                msg23.Data.Ngay_BN = ds.Tables[0].Rows[0]["NGAY_BN"].ToString();
//                msg23.Data.Ngay_BC = ds.Tables[0].Rows[0]["NGAY_BC"].ToString();
//                msg23.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();
//                msg23.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();
//                msg23.Data.DuNo_TO = ds.Tables[0].Rows[0]["TTIEN"].ToString();
//                msg23.Data.DuNo_NT_TO = ds.Tables[0].Rows[0]["TTIEN_NT"].ToString();
//                msg23.Data.Ma_NT = ds.Tables[0].Rows[0]["Ma_NT"].ToString();
//                msg23.Data.Ty_Gia = ds.Tables[0].Rows[0]["Ty_Gia"].ToString();
//                //Lay thong tin detail
//                sbsql = new StringBuilder();
//                sbsql.Append("SELECT   d.ky_thue,");
//                sbsql.Append("         d.ma_chuong,");
//                sbsql.Append("         d.ma_khoan,");
//                sbsql.Append("         d.ma_tmuc,");
//                sbsql.Append("         d.sotien_nt,");
//                sbsql.Append("         d.sotien");
//                sbsql.Append("  FROM   tcs_ctu_dtl d");
//                sbsql.Append(" WHERE   (    (d.shkb = :shkb)");
//                sbsql.Append("          AND (d.ma_nv = :ma_nv)");
//                sbsql.Append("          AND (d.so_ct = '" + pv_strSoCT + "')");
//                sbsql.Append("          AND (d.so_bt = :so_bt)");
//                sbsql.Append("          AND (d.ngay_kb = :ngay_kb))");


//                //Lay thong tin chi tiet chung tu

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg23.Data.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();

//                foreach (DataRow row in ds.Tables[0].Rows)
//                {
//                    bool bFlag_Gan_TMT = false;
//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("185"))
//                    {
//                        msg23.Data.Khoan_XK = row["ma_khoan"].ToString();
//                        msg23.Data.TieuMuc_XK = row["ma_tmuc"].ToString();
//                        msg23.Data.DuNo_XK = row["sotien"].ToString();
//                        msg23.Data.DuNo_NT_XK = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("190") && (!row["ma_tmuc"].ToString().Equals("1903")))
//                    {
//                        msg23.Data.Khoan_NK = row["ma_khoan"].ToString();
//                        msg23.Data.TieuMuc_NK = row["ma_tmuc"].ToString();
//                        msg23.Data.DuNo_NK = row["sotien"].ToString();
//                        msg23.Data.DuNo_NT_NK = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue VA
//                    if (row["ma_tmuc"].ToString().StartsWith("170"))
//                    {
//                        msg23.Data.Khoan_VA = row["ma_khoan"].ToString();
//                        msg23.Data.TieuMuc_VA = row["ma_tmuc"].ToString();
//                        msg23.Data.DuNo_VA = row["sotien"].ToString();
//                        msg23.Data.DuNo_NT_VA = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TD
//                    if (row["ma_tmuc"].ToString().StartsWith("175") )
//                    {
//                        msg23.Data.Khoan_TD = row["ma_khoan"].ToString();
//                        msg23.Data.TieuMuc_TD = row["ma_tmuc"].ToString();
//                        msg23.Data.DuNo_TD = row["sotien"].ToString();
//                        msg23.Data.DuNo_NT_TD = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TV
//                    if (row["ma_tmuc"].ToString().StartsWith("195") || row["ma_tmuc"].ToString().Equals("1903"))
//                    {
//                        msg23.Data.Khoan_TV = row["ma_khoan"].ToString();
//                        msg23.Data.TieuMuc_TV = row["ma_tmuc"].ToString();
//                        msg23.Data.DuNo_TV = row["sotien"].ToString();
//                        msg23.Data.DuNo_NT_TV = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                    //Gan so tien thue MT
//                    if (row["ma_tmuc"].ToString().StartsWith("20"))
//                    {
//                        msg23.Data.Khoan_MT = row["ma_khoan"].ToString();
//                        msg23.Data.TieuMuc_MT = row["ma_tmuc"].ToString();
//                        msg23.Data.DuNo_MT = row["sotien"].ToString();
//                        msg23.Data.DuNo_NT_MT = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }//4253
//                    if (row["ma_tmuc"].ToString().StartsWith("425"))
//                    {
//                        msg23.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg23.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg23.Data.DuNo_KH = row["sotien"].ToString();
//                        msg23.Data.DuNo_NT_KH = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    if (bFlag_Gan_TMT == false)
//                    {
//                        msg23.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg23.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg23.Data.DuNo_KH = row["sotien"].ToString();
//                        msg23.Data.DuNo_NT_XK = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                }


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg23): ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append(", SHKB - ");
//                sbErrMsg.Append(pv_strSHKB);
//                sbErrMsg.Append(", So CT - ");
//                sbErrMsg.Append(pv_strSoCT);
//                sbErrMsg.Append(", So BT - ");
//                sbErrMsg.Append(pv_strSoBT);
//                sbErrMsg.Append(", Ma NV - ");
//                sbErrMsg.Append(pv_strMaNV);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg23): ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append(", SHKB - ");
//                    sbErrMsg.Append(pv_strSHKB);
//                    sbErrMsg.Append(", So CT - ");
//                    sbErrMsg.Append(pv_strSoCT);
//                    sbErrMsg.Append(", So BT - ");
//                    sbErrMsg.Append(pv_strSoBT);
//                    sbErrMsg.Append(", Ma NV - ");
//                    sbErrMsg.Append(pv_strMaNV);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }
//        public static void getMSG23(String pv_strMaNTT, String pv_strSHKB,
//                                        String pv_strSoCT, String pv_strSoBT,
//                                        String pv_strMaNV, String pv_strMaDThu, ref String pv_ErrorNum, ref String pv_ErrorMes,
//                                          ref String pv_transaction_id, ref String pv_so_tn_ct, ref String pv_ngay_tn_ct)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG23 msg23 = new MSG23();
//                msg23.Data = new CustomsV3.MSG.MSG23.Data();
//                msg23.Header = new CustomsV3.MSG.MSG23.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg23.Header.Message_Version = strMessage_Version;
//                msg23.Header.Sender_Code = strSender_Code;
//                msg23.Header.Sender_Name = strSender_Name;
//                msg23.Header.Transaction_Type = "23";
//                msg23.Header.Transaction_Name = "Thông điệp xác nhận nộp thuế của tổ chức tín dụng bằng ngoại tệ";
//                msg23.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                pv_transaction_id = Guid.NewGuid().ToString();
//                msg23.Header.Transaction_ID = pv_transaction_id;

//                //Gan du lieu Data
//                CTUToMSG23(pv_strMaNTT, pv_strSHKB,
//                                           pv_strSoCT, pv_strSoBT,
//                                           pv_strMaNV, ref msg23);

//                //Chuyen thanh XML
//                string strXMLOut = msg23.MSG23toXML(msg23);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG23", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG22 msg22 = new MSG22();
//                MSG22 objTemp = msg22.MSGToObject(strXMLIn);

//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                if (pv_ErrorNum.Equals("0"))
//                {
//                    pv_so_tn_ct = objTemp.Data.So_TN_CT;
//                    pv_ngay_tn_ct = objTemp.Data.Ngay_TN_CT;
//                }

//                //Verify sign                
//                //if (!verifySignMsg(objTemp, "MSG23"))
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //}
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }
//                throw ex;
//            }

//        }

//        #endregion "kết thúc xác nhận nộp thuế bằng ngoại tệ"
//        #region "Thông điệp điều chỉnh thông tin nộp thuế bằng VNĐ M24"

//        public static void CTUToMSG24(String pv_strMaNTT, String pv_strSHKB,
//                                          String pv_strSoCT, String pv_strSoBT,
//                                          String pv_strMaNV, String pv_so_TN_CT, String pv_ngay_TN_CT, ref MSG24 msg24)
//        {
//            msg24.Data = new CustomsV3.MSG.MSG24.Data();
//            msg24.Data.Ma_DV = pv_strMaNTT;

//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ten_nh_b ten_nh_th,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         h.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         h.ma_hq ma_hq,");
//                sbsql.Append("         h.lh_xnk ma_lh,");
//                sbsql.Append("         h.so_tk sotk,");
//                sbsql.Append("         to_char(h.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         h.loai_tt ma_lt,");
//                sbsql.Append("         h.ma_ntk ma_ntk,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         ma_nh_a ma_nh_ph,");
//                sbsql.Append("         ma_nh_b ma_nh_th,");
//                sbsql.Append("         h.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (h.shkb) ten_kb,");
//                sbsql.Append("         h.tk_co tkkb,");
//                sbsql.Append("         h.tk_co tkkb_ct,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'YYYY-MM-DD') ngay_ct,");
//                sbsql.Append("         h.ly_do diengiai,");
//                sbsql.Append("         h.ttien ttien");
//                sbsql.Append("  FROM   tcs_ctu_hdr h");
//                sbsql.Append(" WHERE   ((h.shkb = :shkb)");
//                sbsql.Append("          AND (h.ma_nv = :ma_nv)");
//                sbsql.Append("          AND (h.so_ct = '" + pv_strSoCT + "')");
//                sbsql.Append("          AND (h.so_bt = :so_bt)");
//                sbsql.Append("          AND (h.NGAY_KB = :ngay_kb))");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[4];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "SHKB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_strSHKB;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "MA_NV";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_strMaNV;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "SO_BT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strSoBT;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "ngay_kb";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = v_ngaylv;


//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg24.Data.Ma_NH_PH = strSender_Code;
//                msg24.Data.Ten_NH_PH = strTen_NH_PH;
//                msg24.Data.Ma_NH_TH = ds.Tables[0].Rows[0]["MA_NH_TH"].ToString();
//                msg24.Data.Ten_NH_TH = ds.Tables[0].Rows[0]["TEN_NH_TH"].ToString();
//                msg24.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg24.Data.Ma_HQ_CQT = ds.Tables[0].Rows[0]["MA_HQ_CQT"].ToString();
//                msg24.Data.Ma_HQ_PH = ds.Tables[0].Rows[0]["MA_HQ_PH"].ToString();
//                msg24.Data.Ma_HQ = ds.Tables[0].Rows[0]["MA_HQ"].ToString();
//                msg24.Data.Ma_LH = ds.Tables[0].Rows[0]["MA_LH"].ToString();
//                msg24.Data.So_TK = ds.Tables[0].Rows[0]["SOTK"].ToString();
//                msg24.Data.Ngay_DK = ds.Tables[0].Rows[0]["NGAY_DK"].ToString();
//                msg24.Data.Ma_LT = ds.Tables[0].Rows[0]["MA_LT"].ToString();
//                msg24.Data.Ma_NTK = ds.Tables[0].Rows[0]["MA_NTK"].ToString();
//                msg24.Data.Loai_CT = strLoai_CT;// GNT từ ngân hàng không có TKKB
//                msg24.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg24.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//                msg24.Data.TTButToan = ds.Tables[0].Rows[0]["TTBUTTOAN"].ToString();//Hoi lai
//                msg24.Data.Ma_KB = ds.Tables[0].Rows[0]["MA_KB"].ToString();//Hoi lai
//                msg24.Data.Ten_KB = Globals.RemoveSign4VietnameseString(ds.Tables[0].Rows[0]["TEN_KB"].ToString());//Hoi lai
//                msg24.Data.TKKB = ds.Tables[0].Rows[0]["TKKB"].ToString();
//                msg24.Data.TKKB_CT = ds.Tables[0].Rows[0]["TKKB_CT"].ToString();
//                msg24.Data.Ngay_BN = ds.Tables[0].Rows[0]["NGAY_BN"].ToString();
//                msg24.Data.Ngay_BC = ds.Tables[0].Rows[0]["NGAY_BC"].ToString();
//                msg24.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();
//                msg24.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();
//                msg24.Data.DuNo_TO = ds.Tables[0].Rows[0]["TTIEN"].ToString();
//                msg24.Data.So_TN_CTS = pv_so_TN_CT;
//                msg24.Data.Ngay_TN_CTS = pv_ngay_TN_CT;
//                //Lay thong tin detail
//                sbsql = new StringBuilder();
//                sbsql.Append("SELECT   d.ky_thue,");
//                sbsql.Append("         d.ma_chuong,");
//                sbsql.Append("         d.ma_khoan,");
//                sbsql.Append("         d.ma_tmuc,");
//                sbsql.Append("         d.sotien");
//                sbsql.Append("  FROM   tcs_ctu_dtl d");
//                sbsql.Append(" WHERE   (    (d.shkb = :shkb)");
//                sbsql.Append("          AND (d.ma_nv = :ma_nv)");
//                // sbsql.Append("          AND (d.so_ct = :so_ct)");
//                sbsql.Append("          AND (d.so_bt = :so_bt)");
//                sbsql.Append("          AND (d.ngay_kb = :ngay_kb))");


//                //Lay thong tin chi tiet chung tu

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg24.Data.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();

//                foreach (DataRow row in ds.Tables[0].Rows)
//                {
//                    bool bFlag_Gan_TMT = false;
//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("185"))
//                    {
//                        msg24.Data.Khoan_XK = row["ma_khoan"].ToString();
//                        msg24.Data.TieuMuc_XK = row["ma_tmuc"].ToString();
//                        msg24.Data.DuNo_XK = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("190") && (!row["ma_tmuc"].ToString().Equals("1903")))
//                    {
//                        msg24.Data.Khoan_NK = row["ma_khoan"].ToString();
//                        msg24.Data.TieuMuc_NK = row["ma_tmuc"].ToString();
//                        msg24.Data.DuNo_NK = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue VA
//                    if (row["ma_tmuc"].ToString().StartsWith("170"))
//                    {
//                        msg24.Data.Khoan_VA = row["ma_khoan"].ToString();
//                        msg24.Data.TieuMuc_VA = row["ma_tmuc"].ToString();
//                        msg24.Data.DuNo_VA = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TD
//                    if (row["ma_tmuc"].ToString().StartsWith("175") || row["ma_tmuc"].ToString().Equals("1903"))
//                    {
//                        msg24.Data.Khoan_TD = row["ma_khoan"].ToString();
//                        msg24.Data.TieuMuc_TD = row["ma_tmuc"].ToString();
//                        msg24.Data.DuNo_TD = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TV
//                    if (row["ma_tmuc"].ToString().StartsWith("195"))
//                    {
//                        msg24.Data.Khoan_TV = row["ma_khoan"].ToString();
//                        msg24.Data.TieuMuc_TV = row["ma_tmuc"].ToString();
//                        msg24.Data.DuNo_TV = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                    //Gan so tien thue MT
//                    if (row["ma_tmuc"].ToString().StartsWith("200"))
//                    {
//                        msg24.Data.Khoan_MT = row["ma_khoan"].ToString();
//                        msg24.Data.TieuMuc_MT = row["ma_tmuc"].ToString();
//                        msg24.Data.DuNo_MT = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }//4253
//                    if (row["ma_tmuc"].ToString().StartsWith("425"))
//                    {
//                        msg24.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg24.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg24.Data.DuNo_KH = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    if (bFlag_Gan_TMT == false)
//                    {
//                        msg24.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg24.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg24.Data.DuNo_KH = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                }


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg24): ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append(", SHKB - ");
//                sbErrMsg.Append(pv_strSHKB);
//                sbErrMsg.Append(", So CT - ");
//                sbErrMsg.Append(pv_strSoCT);
//                sbErrMsg.Append(", So BT - ");
//                sbErrMsg.Append(pv_strSoBT);
//                sbErrMsg.Append(", Ma NV - ");
//                sbErrMsg.Append(pv_strMaNV);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg24): ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append(", SHKB - ");
//                    sbErrMsg.Append(pv_strSHKB);
//                    sbErrMsg.Append(", So CT - ");
//                    sbErrMsg.Append(pv_strSoCT);
//                    sbErrMsg.Append(", So BT - ");
//                    sbErrMsg.Append(pv_strSoBT);
//                    sbErrMsg.Append(", Ma NV - ");
//                    sbErrMsg.Append(pv_strMaNV);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }

//        public static void getMSG24(String pv_strMaNTT, String pv_strSHKB,
//                                          String pv_strSoCT, String pv_strSoBT,
//                                          String pv_strMaNV, String pv_strMaDThu, ref String pv_ErrorNum, ref String pv_ErrorMes,
//                                            ref String pv_transaction_id, ref String pv_so_tn_ct, ref String pv_ngay_tn_ct)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG24 msg24 = new MSG24();
//                msg24.Data = new CustomsV3.MSG.MSG24.Data();
//                msg24.Header = new CustomsV3.MSG.MSG24.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg24.Header.Message_Version = strMessage_Version;
//                msg24.Header.Sender_Code = strSender_Code;
//                msg24.Header.Sender_Name = strSender_Name;
//                msg24.Header.Transaction_Type = "24";
//                msg24.Header.Transaction_Name = "Thông điệp điều chỉnh thông tin nộp thuế bằng VNĐ";
//                msg24.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                pv_transaction_id = Guid.NewGuid().ToString();
//                msg24.Header.Transaction_ID = pv_transaction_id;

//                //Gan du lieu Data
//                CTUToMSG24(pv_strMaNTT, pv_strSHKB,
//                                           pv_strSoCT, pv_strSoBT,
//                                           pv_strMaNV, pv_so_tn_ct, pv_ngay_tn_ct, ref msg24);

//                //Chuyen thanh XML
//                string strXMLOut = msg24.MSG24toXML(msg24);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG24", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);

//                MSG22 msg22 = new MSG22();
//                MSG22 objTemp = msg22.MSGToObject(strXMLIn);

//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                if (pv_ErrorNum.Equals("0"))
//                {
//                    pv_so_tn_ct = objTemp.Data.So_TN_CT;
//                    pv_ngay_tn_ct = objTemp.Data.Ngay_TN_CT;
//                }

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = sbErrMsg.ToString();

//                //throw ex;
//            }

//        }

//        #endregion "Xác nhận nộp thuế của ngân hàng M24"
//        #region "Thông điệp điều chỉnh thông tin nộp thuế bằng ngoại tệ M22-M25"
//        public static void CTUToMSG25(String pv_strMaNTT, String pv_strSHKB,
//                                         String pv_strSoCT, String pv_strSoBT,
//                                         String pv_strMaNV, String pv_So_TN_CT, String pv_Ngay_TN_CT, ref MSG25 msg25)
//        {
//            msg25.Data = new CustomsV3.MSG.MSG25.Data();
//            msg25.Data.Ma_DV = pv_strMaNTT;

//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ten_nh_b ten_nh_th,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         h.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         h.ma_hq ma_hq,");
//                sbsql.Append("         h.lh_xnk ma_lh,");
//                sbsql.Append("         h.so_tk sotk,");
//                sbsql.Append("         to_char(h.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         h.loai_tt ma_lt,");
//                sbsql.Append("         h.ma_ntk ma_ntk,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         ma_nh_a ma_nh_ph,");
//                sbsql.Append("         ma_nh_b ma_nh_th,");
//                sbsql.Append("         h.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (h.shkb) ten_kb,");
//                sbsql.Append("         h.tk_co tkkb,");
//                sbsql.Append("         h.tk_co tkkb_ct,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'YYYY-MM-DD') ngay_ct,");
//                sbsql.Append("         h.ly_do diengiai,");
//                sbsql.Append("         h.ttien ttien,");
//                sbsql.Append("         h.ttien_nt ttien_nt,");
//                sbsql.Append("         h.ma_nt ma_nt,");
//                sbsql.Append("         h.ty_gia ty_gia");
//                sbsql.Append("  FROM   tcs_ctu_hdr h");
//                sbsql.Append(" WHERE   ((h.shkb = :shkb)");
//                sbsql.Append("          AND (h.ma_nv = :ma_nv)");
//                sbsql.Append("          AND (h.so_ct = '" + pv_strSoCT + "')");
//                sbsql.Append("          AND (h.so_bt = :so_bt)");
//                sbsql.Append("          AND (h.NGAY_KB = :ngay_kb))");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[4];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "SHKB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_strSHKB;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "MA_NV";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_strMaNV;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "SO_BT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strSoBT;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "ngay_kb";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = v_ngaylv;


//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg25.Data.Ma_NH_PH = strSender_Code;
//                msg25.Data.Ten_NH_PH = strTen_NH_PH;
//                msg25.Data.Ma_NH_TH = ds.Tables[0].Rows[0]["MA_NH_TH"].ToString();
//                msg25.Data.Ten_NH_TH = ds.Tables[0].Rows[0]["TEN_NH_TH"].ToString();
//                msg25.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg25.Data.Ma_HQ_CQT = ds.Tables[0].Rows[0]["MA_HQ_CQT"].ToString();
//                msg25.Data.Ma_HQ_PH = ds.Tables[0].Rows[0]["MA_HQ_PH"].ToString();
//                msg25.Data.Ma_HQ = ds.Tables[0].Rows[0]["MA_HQ"].ToString();
//                msg25.Data.Ma_LH = ds.Tables[0].Rows[0]["MA_LH"].ToString();
//                msg25.Data.So_TK = ds.Tables[0].Rows[0]["SOTK"].ToString();
//                msg25.Data.Ngay_DK = ds.Tables[0].Rows[0]["NGAY_DK"].ToString();
//                msg25.Data.Ma_LT = ds.Tables[0].Rows[0]["MA_LT"].ToString();
//                msg25.Data.Ma_NTK = ds.Tables[0].Rows[0]["MA_NTK"].ToString();
//                msg25.Data.Loai_CT = strLoai_CT;// GNT từ ngân hàng không có TKKB
//                msg25.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg25.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//                msg25.Data.TTButToan = ds.Tables[0].Rows[0]["TTBUTTOAN"].ToString();//Hoi lai
//                msg25.Data.Ma_KB = ds.Tables[0].Rows[0]["MA_KB"].ToString();//Hoi lai
//                msg25.Data.Ten_KB = Globals.RemoveSign4VietnameseString(ds.Tables[0].Rows[0]["TEN_KB"].ToString());//Hoi lai
//                msg25.Data.TKKB = ds.Tables[0].Rows[0]["TKKB"].ToString();
//                msg25.Data.TKKB_CT = ds.Tables[0].Rows[0]["TKKB_CT"].ToString();
//                msg25.Data.Ngay_BN = ds.Tables[0].Rows[0]["NGAY_BN"].ToString();
//                msg25.Data.Ngay_BC = ds.Tables[0].Rows[0]["NGAY_BC"].ToString();
//                msg25.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();
//                msg25.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();
//                msg25.Data.DuNo_TO = ds.Tables[0].Rows[0]["TTIEN"].ToString();
//                msg25.Data.DuNo_NT_TO = ds.Tables[0].Rows[0]["TTIEN_NT"].ToString();
//                msg25.Data.Ma_NT = ds.Tables[0].Rows[0]["Ma_NT"].ToString();
//                msg25.Data.Ty_Gia = ds.Tables[0].Rows[0]["Ty_Gia"].ToString();
//                msg25.Data.So_TN_CTS = pv_So_TN_CT;
//                msg25.Data.Ngay_TN_CTS = pv_Ngay_TN_CT;
//                //Lay thong tin detail
//                sbsql = new StringBuilder();
//                sbsql.Append("SELECT   d.ky_thue,");
//                sbsql.Append("         d.ma_chuong,");
//                sbsql.Append("         d.ma_khoan,");
//                sbsql.Append("         d.ma_tmuc,");
//                sbsql.Append("         d.sotien_nt,");
//                sbsql.Append("         d.sotien");
//                sbsql.Append("  FROM   tcs_ctu_dtl d");
//                sbsql.Append(" WHERE   (    (d.shkb = :shkb)");
//                sbsql.Append("          AND (d.ma_nv = :ma_nv)");
//                sbsql.Append("          AND (d.so_ct = '" + pv_strSoCT + "')");
//                sbsql.Append("          AND (d.so_bt = :so_bt)");
//                sbsql.Append("          AND (d.ngay_kb = :ngay_kb))");


//                //Lay thong tin chi tiet chung tu

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg25.Data.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();

//                foreach (DataRow row in ds.Tables[0].Rows)
//                {
//                    bool bFlag_Gan_TMT = false;
//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("185"))
//                    {
//                        msg25.Data.Khoan_XK = row["ma_khoan"].ToString();
//                        msg25.Data.TieuMuc_XK = row["ma_tmuc"].ToString();
//                        msg25.Data.DuNo_XK = row["sotien"].ToString();
//                        msg25.Data.DuNo_NT_XK = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("190") && (!row["ma_tmuc"].ToString().Equals("1903")))
//                    {
//                        msg25.Data.Khoan_NK = row["ma_khoan"].ToString();
//                        msg25.Data.TieuMuc_NK = row["ma_tmuc"].ToString();
//                        msg25.Data.DuNo_NK = row["sotien"].ToString();
//                        msg25.Data.DuNo_NT_NK = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue VA
//                    if (row["ma_tmuc"].ToString().StartsWith("170"))
//                    {
//                        msg25.Data.Khoan_VA = row["ma_khoan"].ToString();
//                        msg25.Data.TieuMuc_VA = row["ma_tmuc"].ToString();
//                        msg25.Data.DuNo_VA = row["sotien"].ToString();
//                        msg25.Data.DuNo_NT_VA = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TD
//                    if (row["ma_tmuc"].ToString().StartsWith("175") || row["ma_tmuc"].ToString().Equals("1903"))
//                    {
//                        msg25.Data.Khoan_TD = row["ma_khoan"].ToString();
//                        msg25.Data.TieuMuc_TD = row["ma_tmuc"].ToString();
//                        msg25.Data.DuNo_TD = row["sotien"].ToString();
//                        msg25.Data.DuNo_NT_TD = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TV
//                    if (row["ma_tmuc"].ToString().StartsWith("195"))
//                    {
//                        msg25.Data.Khoan_TV = row["ma_khoan"].ToString();
//                        msg25.Data.TieuMuc_TV = row["ma_tmuc"].ToString();
//                        msg25.Data.DuNo_TV = row["sotien"].ToString();
//                        msg25.Data.DuNo_NT_TV = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                    //Gan so tien thue MT
//                    if (row["ma_tmuc"].ToString().StartsWith("20"))
//                    {
//                        msg25.Data.Khoan_MT = row["ma_khoan"].ToString();
//                        msg25.Data.TieuMuc_MT = row["ma_tmuc"].ToString();
//                        msg25.Data.DuNo_MT = row["sotien"].ToString();
//                        msg25.Data.DuNo_NT_MT = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }//4253
//                    if (row["ma_tmuc"].ToString().StartsWith("425"))
//                    {
//                        msg25.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg25.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg25.Data.DuNo_KH = row["sotien"].ToString();
//                        msg25.Data.DuNo_NT_KH = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    if (bFlag_Gan_TMT == false)
//                    {
//                        msg25.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg25.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg25.Data.DuNo_KH = row["sotien"].ToString();
//                        msg25.Data.DuNo_NT_XK = row["sotien_nt"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                }


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg25): ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append(", SHKB - ");
//                sbErrMsg.Append(pv_strSHKB);
//                sbErrMsg.Append(", So CT - ");
//                sbErrMsg.Append(pv_strSoCT);
//                sbErrMsg.Append(", So BT - ");
//                sbErrMsg.Append(pv_strSoBT);
//                sbErrMsg.Append(", Ma NV - ");
//                sbErrMsg.Append(pv_strMaNV);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg25): ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append(", SHKB - ");
//                    sbErrMsg.Append(pv_strSHKB);
//                    sbErrMsg.Append(", So CT - ");
//                    sbErrMsg.Append(pv_strSoCT);
//                    sbErrMsg.Append(", So BT - ");
//                    sbErrMsg.Append(pv_strSoBT);
//                    sbErrMsg.Append(", Ma NV - ");
//                    sbErrMsg.Append(pv_strMaNV);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }
//        public static void getMSG25(String pv_strMaNTT, String pv_strSHKB,
//                                        String pv_strSoCT, String pv_strSoBT,
//                                        String pv_strMaNV, String pv_strMaDThu, ref String pv_ErrorNum, ref String pv_ErrorMes,
//                                          ref String pv_transaction_id, ref String pv_so_tn_ct, ref String pv_ngay_tn_ct)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG25 msg25 = new MSG25();
//                msg25.Data = new CustomsV3.MSG.MSG25.Data();
//                msg25.Header = new CustomsV3.MSG.MSG25.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg25.Header.Message_Version = strMessage_Version;
//                msg25.Header.Sender_Code = strSender_Code;
//                msg25.Header.Sender_Name = strSender_Name;
//                msg25.Header.Transaction_Type = "25";
//                msg25.Header.Transaction_Name = "Thông điệp điều chỉnh thông tin nộp thuế bằng ngoại tệ";
//                msg25.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                pv_transaction_id = Guid.NewGuid().ToString();
//                msg25.Header.Transaction_ID = pv_transaction_id;

//                //Gan du lieu Data
//                CTUToMSG25(pv_strMaNTT, pv_strSHKB,
//                                           pv_strSoCT, pv_strSoBT,
//                                           pv_strMaNV, pv_so_tn_ct, pv_ngay_tn_ct, ref msg25);

//                //Chuyen thanh XML
//                string strXMLOut = msg25.MSG25toXML(msg25);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG25", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG22 msg22 = new MSG22();
//                MSG22 objTemp = msg22.MSGToObject(strXMLIn);

//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                if (pv_ErrorNum.Equals("0"))
//                {
//                    pv_so_tn_ct = objTemp.Data.So_TN_CT;
//                    pv_ngay_tn_ct = objTemp.Data.Ngay_TN_CT;
//                }

//                //Verify sign                
//                //if (!verifySignMsg(objTemp, "MSG23"))
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //}
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }
//                throw ex;
//            }

//        }
//        #endregion "kết thúc điều chỉnh nộp thuế bằng ngoại tệ"
//        #region "Xác nhận nộp thuế bằng cho nhiều tờ khai bằng VNĐ M26- M22"
//        public static void getMSG26(String pv_strMaNTT, String pv_strSHKB,
//                                        String pv_strSoCT, String pv_strSoBT,
//                                        String pv_strMaNV, String pv_strMaDThu, ref String pv_ErrorNum, ref String pv_ErrorMes,
//                                          ref String pv_transaction_id, ref String pv_so_tn_ct, ref String pv_ngay_tn_ct)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG26 msg26 = new MSG26();
//                msg26.Data = new CustomsV3.MSG.MSG26.Data();
//                msg26.Header = new CustomsV3.MSG.MSG26.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg26.Header.Message_Version = strMessage_Version;
//                msg26.Header.Sender_Code = strSender_Code;
//                msg26.Header.Sender_Name = strSender_Name;
//                msg26.Header.Transaction_Type = "26";
//                msg26.Header.Transaction_Name = "Thông điệp xác nhận nộp tiền cho nhiều tờ khai bằng VNĐ";
//                msg26.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                pv_transaction_id = Guid.NewGuid().ToString();
//                msg26.Header.Transaction_ID = pv_transaction_id;

//                //Gan du lieu Data
//                CTUToMSG26(pv_strMaNTT, pv_strSHKB,
//                                           pv_strSoCT, pv_strSoBT,
//                                           pv_strMaNV, pv_so_tn_ct, pv_ngay_tn_ct, ref msg26);

//                //Chuyen thanh XML
//                string strXMLOut = msg26.MSG26toXML(msg26);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG26", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);

//                MSG22 msg22 = new MSG22();
//                MSG22 objTemp = msg22.MSGToObject(strXMLIn);

//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                if (pv_ErrorNum.Equals("0"))
//                {
//                    pv_so_tn_ct = objTemp.Data.So_TN_CT;
//                    pv_ngay_tn_ct = objTemp.Data.Ngay_TN_CT;
//                }

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 26: ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = sbErrMsg.ToString();
//                throw ex;
//            }
//        }
//        public static void CTUToMSG26(String pv_strMaNTT, String pv_strSHKB,
//                                        String pv_strSoCT, String pv_strSoBT,
//                                        String pv_strMaNV, String pv_so_TN_CT, String pv_ngay_TN_CT, ref MSG26 msg26)
//        {
//            msg26.Data = new CustomsV3.MSG.MSG26.Data();
//            msg26.Data.Ma_DV = pv_strMaNTT;

//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            // string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ten_nh_b ten_nh_th,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         h.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         h.ma_hq ma_hq,");
//                sbsql.Append("         h.lh_xnk ma_lh,");
//                sbsql.Append("         h.so_tk sotk,");
//                sbsql.Append("         to_char(h.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         h.loai_tt ma_lt,");
//                //sbsql.Append("         NVL(h.ma_ntk,DECODE(SUBSTR(h.tk_co,0,3),'741','1','711','1','2')) ma_ntk,");
//                sbsql.Append("         h.ma_ntk ma_ntk,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         ma_nh_a ma_nh_ph,");
//                sbsql.Append("         ma_nh_b ma_nh_th,");
//                sbsql.Append("         h.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (h.shkb) ten_kb,");
//                sbsql.Append("         h.tk_co tkkb,");
//                sbsql.Append("         h.tk_co tkkb_ct,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'YYYY-MM-DD') ngay_ct,");
//                sbsql.Append("         h.ly_do diengiai,");
//                sbsql.Append("         h.ttien ttien");
//                sbsql.Append("  FROM   tcs_ctu_hdr h");
//                sbsql.Append(" WHERE   ((h.shkb = :shkb)");
//                sbsql.Append("          AND (h.ma_nv = :ma_nv)");
//                sbsql.Append("          AND (h.so_ct = :so_CT)");
//                sbsql.Append("          AND (h.so_bt = :so_bt))");
//                // sbsql.Append("          AND (h.NGAY_KB = :ngay_kb))");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[4];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "SHKB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_strSHKB;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "MA_NV";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_strMaNV;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "so_CT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strSoCT;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "SO_BT";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = pv_strSoBT;


//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg26.Data.Ma_NH_PH = strSender_Code;
//                msg26.Data.Ten_NH_PH = strTen_NH_PH;
//                msg26.Data.Ma_NH_TH = ds.Tables[0].Rows[0]["MA_NH_TH"].ToString();
//                msg26.Data.Ten_NH_TH = ds.Tables[0].Rows[0]["TEN_NH_TH"].ToString();
//                msg26.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg26.Data.Ma_HQ_CQT = ds.Tables[0].Rows[0]["MA_HQ_CQT"].ToString();
//                msg26.Data.Ma_HQ_PH = ds.Tables[0].Rows[0]["MA_HQ_PH"].ToString();
//                msg26.Data.Ma_NTK = ds.Tables[0].Rows[0]["MA_NTK"].ToString();
//                msg26.Data.Loai_CT = strLoai_CT;// GNT từ ngân hàng không có TKKB
//                msg26.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg26.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//                msg26.Data.Ma_KB = ds.Tables[0].Rows[0]["MA_KB"].ToString();//Hoi lai
//                msg26.Data.Ten_KB = Globals.RemoveSign4VietnameseString(ds.Tables[0].Rows[0]["TEN_KB"].ToString());//Hoi lai
//                msg26.Data.TKKB = ds.Tables[0].Rows[0]["TKKB"].ToString();
//                msg26.Data.Ngay_BN = ds.Tables[0].Rows[0]["NGAY_BN"].ToString();
//                msg26.Data.Ngay_BC = ds.Tables[0].Rows[0]["NGAY_BC"].ToString();
//                msg26.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();
//                msg26.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();
//                msg26.Data.SoTien_TO = ds.Tables[0].Rows[0]["TTIEN"].ToString();
//                msg26.Data.So_TN_CTS = pv_so_TN_CT;
//                msg26.Data.Ngay_TN_CTS = pv_ngay_TN_CT;
//                // lấy ra mảng tờ khai va loại tiền, sau đó lấy chi tiết từng tờ khai đó
//                string so_tk = ds.Tables[0].Rows[0]["sotk"].ToString();
//                string ma_loaitienthue = ds.Tables[0].Rows[0]["ma_lt"].ToString();
//                string[] tokhaiso_arr = so_tk.Split(';');
//                string[] loaitienthue_arr = ma_loaitienthue.Split(';');
//                List<CustomsV3.MSG.MSG26.GNT_CT> listGNT = new List<CustomsV3.MSG.MSG26.GNT_CT>();

//                //Lay thong tin detail
//                for (int i = 0; i <= tokhaiso_arr.Length - 1; i++)
//                {
//                    sbsql = new StringBuilder();
//                    sbsql.Append("SELECT   d.ky_thue,");
//                    sbsql.Append("         d.ma_chuong,");
//                    sbsql.Append("         d.ma_tmuc,");
//                    sbsql.Append("         d.tt_btoan,");
//                    sbsql.Append("         d.ma_hq,");
//                    sbsql.Append("         d.ma_lhxnk,");
//                    sbsql.Append("         to_char(d.ngay_tk,'yyyy') ngay_tk,");
//                    sbsql.Append("         d.so_tk,");
//                    sbsql.Append("         st.sac_thue,");
//                    sbsql.Append("         d.ma_lt,");
//                    sbsql.Append("         d.sotien_nt,");
//                    sbsql.Append("         d.sotien");
//                    sbsql.Append("  FROM   tcs_ctu_dtl d,tcs_map_tm_st st");
//                    sbsql.Append(" WHERE   (    (d.shkb = :shkb)");
//                    sbsql.Append("          AND (d.ma_nv = :ma_nv)");
//                    sbsql.Append("          AND (d.so_ct = :so_ct)");
//                    sbsql.Append("          AND (d.so_bt = :so_bt)");
//                    sbsql.Append("          AND (d.so_tk = '" + tokhaiso_arr[i] + "')");
//                    sbsql.Append("          AND (d.ma_lt = '" + loaitienthue_arr[i] + "')");
//                    sbsql.Append("          AND (d.ma_tmuc = st.ma_ndkt))");

//                    //Lay thong tin chi tiet chung tu

//                    ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                    if (ds.Tables[0].Rows.Count <= 0)
//                    {
//                        throw new Exception("Không tìm thấy chứng từ!");
//                    }
//                    msg26.Data.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();
//                    List<CustomsV3.MSG.MSG26.ToKhai_CT> listToKhai = new List<CustomsV3.MSG.MSG26.ToKhai_CT>();
//                    CustomsV3.MSG.MSG26.GNT_CT GNT = new CustomsV3.MSG.MSG26.GNT_CT();
//                    GNT.TTButToan = ds.Tables[0].Rows[0]["tt_btoan"].ToString();
//                    GNT.So_TK = ds.Tables[0].Rows[0]["so_tk"].ToString();
//                    GNT.Nam_DK = ds.Tables[0].Rows[0]["ngay_tk"].ToString();
//                    GNT.Ma_HQ = ds.Tables[0].Rows[0]["ma_hq"].ToString();
//                    GNT.Ma_LH = ds.Tables[0].Rows[0]["ma_lhxnk"].ToString();
//                    GNT.Ma_LT = ds.Tables[0].Rows[0]["ma_lt"].ToString();
//                    foreach (DataRow row in ds.Tables[0].Rows)
//                    {
//                        CustomsV3.MSG.MSG26.ToKhai_CT ToKhai_CT = new CustomsV3.MSG.MSG26.ToKhai_CT();
//                        ToKhai_CT.Ma_ST = row["sac_thue"].ToString();
//                        ToKhai_CT.NDKT = row["ma_tmuc"].ToString();
//                        ToKhai_CT.SoTien = row["sotien"].ToString();
//                        listToKhai.Add(ToKhai_CT);
//                    }
//                    GNT.ToKhai_CT = listToKhai;
//                    listGNT.Add(GNT);
//                }
//                msg26.Data.GNT_CT = listGNT;
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg26): ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append(", SHKB - ");
//                sbErrMsg.Append(pv_strSHKB);
//                sbErrMsg.Append(", So CT - ");
//                sbErrMsg.Append(pv_strSoCT);
//                sbErrMsg.Append(", So BT - ");
//                sbErrMsg.Append(pv_strSoBT);
//                sbErrMsg.Append(", Ma NV - ");
//                sbErrMsg.Append(pv_strMaNV);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg26): ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append(", SHKB - ");
//                    sbErrMsg.Append(pv_strSHKB);
//                    sbErrMsg.Append(", So CT - ");
//                    sbErrMsg.Append(pv_strSoCT);
//                    sbErrMsg.Append(", So BT - ");
//                    sbErrMsg.Append(pv_strSoBT);
//                    sbErrMsg.Append(", Ma NV - ");
//                    sbErrMsg.Append(pv_strMaNV);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }

//        }
//        #endregion"Kết thúc xác nhận nộp thuế nhiều tờ khai M26- M22"
//        #region "Xác nhận nộp thuế bằng cho nhiều tờ khai bằng ngoại tệ M27- M22"
//        public static void getMSG27(String pv_strMaNTT, String pv_strSHKB,
//                                        String pv_strSoCT, String pv_strSoBT,
//                                        String pv_strMaNV, String pv_strMaDThu, ref String pv_ErrorNum, ref String pv_ErrorMes,
//                                          ref String pv_transaction_id, ref String pv_so_tn_ct, ref String pv_ngay_tn_ct)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG27 msg27 = new MSG27();
//                msg27.Data = new CustomsV3.MSG.MSG27.Data();
//                msg27.Header = new CustomsV3.MSG.MSG27.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg27.Header.Message_Version = strMessage_Version;
//                msg27.Header.Sender_Code = strSender_Code;
//                msg27.Header.Sender_Name = strSender_Name;
//                msg27.Header.Transaction_Type = "27";
//                msg27.Header.Transaction_Name = "Thông điệp xác nhận nộp tiền cho nhiều tờ khai bằng ngoại tệ";
//                msg27.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                pv_transaction_id = Guid.NewGuid().ToString();
//                msg27.Header.Transaction_ID = pv_transaction_id;

//                //Gan du lieu Data
//                CTUToMSG27(pv_strMaNTT, pv_strSHKB,
//                                           pv_strSoCT, pv_strSoBT,
//                                           pv_strMaNV, pv_so_tn_ct, pv_ngay_tn_ct, ref msg27);

//                //Chuyen thanh XML
//                string strXMLOut = msg27.MSG27toXML(msg27);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG27", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);

//                MSG22 msg22 = new MSG22();
//                MSG22 objTemp = msg22.MSGToObject(strXMLIn);

//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                if (pv_ErrorNum.Equals("0"))
//                {
//                    pv_so_tn_ct = objTemp.Data.So_TN_CT;
//                    pv_ngay_tn_ct = objTemp.Data.Ngay_TN_CT;
//                }

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 22: ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 26: ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = sbErrMsg.ToString();
//                throw ex;
//            }
//        }
//        public static void CTUToMSG27(String pv_strMaNTT, String pv_strSHKB,
//                                        String pv_strSoCT, String pv_strSoBT,
//                                        String pv_strMaNV, String pv_so_TN_CT, String pv_ngay_TN_CT, ref MSG27 msg27)
//        {
//            msg27.Data = new CustomsV3.MSG.MSG27.Data();
//            msg27.Data.Ma_DV = pv_strMaNTT;

//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            // string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ten_nh_b ten_nh_th,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         h.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         h.ma_hq ma_hq,");
//                sbsql.Append("         h.lh_xnk ma_lh,");
//                sbsql.Append("         h.so_tk sotk,");
//                sbsql.Append("         to_char(h.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         h.loai_tt ma_lt,");
//                sbsql.Append("         h.ma_ntk ma_ntk,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         ma_nh_a ma_nh_ph,");
//                sbsql.Append("         ma_nh_b ma_nh_th,");
//                sbsql.Append("         h.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (h.shkb) ten_kb,");
//                sbsql.Append("         h.tk_co tkkb,");
//                sbsql.Append("         h.tk_co tkkb_ct,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'YYYY-MM-DD') ngay_ct,");
//                sbsql.Append("         h.ly_do diengiai,");
//                sbsql.Append("         h.ma_nt ma_nt,");
//                sbsql.Append("         h.ty_gia ty_gia,");
//                sbsql.Append("         h.ttien ttien");
//                sbsql.Append("  FROM   tcs_ctu_hdr h");
//                sbsql.Append(" WHERE   ((h.shkb = :shkb)");
//                sbsql.Append("          AND (h.ma_nv = :ma_nv)");
//                sbsql.Append("          AND (h.so_ct = :so_CT)");
//                sbsql.Append("          AND (h.so_bt = :so_bt))");
//                // sbsql.Append("          AND (h.NGAY_KB = :ngay_kb))");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[4];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "SHKB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_strSHKB;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "MA_NV";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_strMaNV;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "so_CT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strSoCT;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "SO_BT";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = pv_strSoBT;


//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg27.Data.Ma_NH_PH = strSender_Code;
//                msg27.Data.Ten_NH_PH = strTen_NH_PH;
//                msg27.Data.Ma_NH_TH = ds.Tables[0].Rows[0]["MA_NH_TH"].ToString();
//                msg27.Data.Ten_NH_TH = ds.Tables[0].Rows[0]["TEN_NH_TH"].ToString();
//                msg27.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg27.Data.Ma_HQ_CQT = ds.Tables[0].Rows[0]["MA_HQ_CQT"].ToString();
//                msg27.Data.Ma_HQ_PH = ds.Tables[0].Rows[0]["MA_HQ_PH"].ToString();
//                msg27.Data.Ma_NTK = ds.Tables[0].Rows[0]["MA_NTK"].ToString();
//                msg27.Data.Loai_CT = strLoai_CT;// GNT từ ngân hàng không có TKKB
//                msg27.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg27.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//                msg27.Data.Ma_KB = ds.Tables[0].Rows[0]["MA_KB"].ToString();//Hoi lai
//                msg27.Data.Ten_KB = Globals.RemoveSign4VietnameseString(ds.Tables[0].Rows[0]["TEN_KB"].ToString());//Hoi lai
//                msg27.Data.TKKB = ds.Tables[0].Rows[0]["TKKB"].ToString();
//                msg27.Data.Ngay_BN = ds.Tables[0].Rows[0]["NGAY_BN"].ToString();
//                msg27.Data.Ngay_BC = ds.Tables[0].Rows[0]["NGAY_BC"].ToString();
//                msg27.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();
//                msg27.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();
//                msg27.Data.SoTien_TO = ds.Tables[0].Rows[0]["TTIEN"].ToString();
//                msg27.Data.So_TN_CTS = pv_so_TN_CT;
//                msg27.Data.Ngay_TN_CTS = pv_ngay_TN_CT;
//                // lấy ra mảng tờ khai va loại tiền, sau đó lấy chi tiết từng tờ khai đó
//                string so_tk = ds.Tables[0].Rows[0]["sotk"].ToString();
//                string ma_loaitienthue = ds.Tables[0].Rows[0]["ma_lt"].ToString();
//                string ty_gia = ds.Tables[0].Rows[0]["ty_gia"].ToString();
//                string ma_nt = ds.Tables[0].Rows[0]["ma_nt"].ToString();
//                string[] tokhaiso_arr = so_tk.Split(';');
//                string[] loaitienthue_arr = ma_loaitienthue.Split(';');
//                List<CustomsV3.MSG.MSG27.GNT_CT> listGNT = new List<CustomsV3.MSG.MSG27.GNT_CT>();

//                //Lay thong tin detail
//                for (int i = 0; i <= tokhaiso_arr.Length - 1; i++)
//                {
//                    sbsql = new StringBuilder();
//                    sbsql.Append("SELECT   d.ky_thue,");
//                    sbsql.Append("         d.ma_chuong,");
//                    sbsql.Append("         d.ma_tmuc,");
//                    sbsql.Append("         d.tt_btoan,");
//                    sbsql.Append("         d.ma_hq,");
//                    sbsql.Append("         d.ma_lhxnk,");
//                    sbsql.Append("         to_char(d.ngay_tk,'yyyy') ngay_tk,");
//                    sbsql.Append("         d.so_tk,");
//                    sbsql.Append("         st.sac_thue,");
//                    sbsql.Append("         d.ma_lt,");
//                    sbsql.Append("         d.sotien_nt,");
//                    sbsql.Append("         d.sotien");
//                    sbsql.Append("  FROM   tcs_ctu_dtl d,tcs_map_tm_st st");
//                    sbsql.Append(" WHERE   (    (d.shkb = :shkb)");
//                    sbsql.Append("          AND (d.ma_nv = :ma_nv)");
//                    sbsql.Append("          AND (d.so_ct = :so_ct)");
//                    sbsql.Append("          AND (d.so_bt = :so_bt)");
//                    sbsql.Append("          AND (d.so_tk = '" + tokhaiso_arr[i] + "')");
//                    sbsql.Append("          AND (d.ma_lt = '" + loaitienthue_arr[i] + "')");
//                    sbsql.Append("          AND (d.ma_tmuc = st.ma_ndkt))");

//                    //Lay thong tin chi tiet chung tu

//                    ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                    if (ds.Tables[0].Rows.Count <= 0)
//                    {
//                        throw new Exception("Không tìm thấy chứng từ!");
//                    }
//                    msg27.Data.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();
//                    List<CustomsV3.MSG.MSG27.ToKhai_CT> listToKhai = new List<CustomsV3.MSG.MSG27.ToKhai_CT>();
//                    CustomsV3.MSG.MSG27.GNT_CT GNT = new CustomsV3.MSG.MSG27.GNT_CT();
//                    GNT.TTButToan = ds.Tables[0].Rows[0]["tt_btoan"].ToString();
//                    GNT.So_TK = ds.Tables[0].Rows[0]["so_tk"].ToString();
//                    GNT.Nam_DK = ds.Tables[0].Rows[0]["ngay_tk"].ToString();
//                    GNT.Ma_HQ = ds.Tables[0].Rows[0]["ma_hq"].ToString();
//                    GNT.Ma_LH = ds.Tables[0].Rows[0]["ma_lhxnk"].ToString();
//                    GNT.Ma_LT = ds.Tables[0].Rows[0]["ma_lt"].ToString();
//                    //GNT.Ty_Gia = ty_gia;
//                    foreach (DataRow row in ds.Tables[0].Rows)
//                    {
//                        CustomsV3.MSG.MSG27.ToKhai_CT ToKhai_CT = new CustomsV3.MSG.MSG27.ToKhai_CT();
//                        ToKhai_CT.Ma_ST = row["sac_thue"].ToString();
//                        ToKhai_CT.NDKT = row["ma_tmuc"].ToString();
//                        ToKhai_CT.SoTien = row["sotien"].ToString();
//                        ToKhai_CT.SoTien_NT = row["sotien_nt"].ToString();
//                        ToKhai_CT.Ma_NT = ma_nt;
//                        ToKhai_CT.Ty_Gia = ty_gia;
//                        listToKhai.Add(ToKhai_CT);
//                    }
//                    GNT.ToKhai_CT = listToKhai;
//                    listGNT.Add(GNT);
//                }
//                msg27.Data.GNT_CT = listGNT;
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg27): ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append(", SHKB - ");
//                sbErrMsg.Append(pv_strSHKB);
//                sbErrMsg.Append(", So CT - ");
//                sbErrMsg.Append(pv_strSoCT);
//                sbErrMsg.Append(", So BT - ");
//                sbErrMsg.Append(pv_strSoBT);
//                sbErrMsg.Append(", Ma NV - ");
//                sbErrMsg.Append(pv_strMaNV);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUTomsg27): ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append(", SHKB - ");
//                    sbErrMsg.Append(pv_strSHKB);
//                    sbErrMsg.Append(", So CT - ");
//                    sbErrMsg.Append(pv_strSoCT);
//                    sbErrMsg.Append(", So BT - ");
//                    sbErrMsg.Append(pv_strSoBT);
//                    sbErrMsg.Append(", Ma NV - ");
//                    sbErrMsg.Append(pv_strMaNV);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }

//        }
//        #endregion"Kết thúc xác nhận nộp thuế nhiều tờ khai bằng ngoại tệ M27- M22"
//        #region "Đề nghị hủy xác nhận nộp thuế của ngân hàng M31 - M32"

//        //public static void CTUToMSG31(String pv_strMaNTT, String pv_strSHKB,
//        //                                  String pv_strSoCT, String pv_strSoBT,
//        //                                  String pv_strMaNV, String pv_strMaDThu, String strNgayKB, ref MSG31 msg31)
//        //{
//        //    msg31.Data = new CustomsV3.MSG.MSG31.Data();
//        //    msg31.Data.Ma_NH_PH = strSender_Code;
//        //    msg31.Data.Ten_NH_PH = strSender_Name;

//        //    //Lay thong tin Header CTU
//        //    StringBuilder sbsql = new StringBuilder();
//        //    string v_ngaylv = "";
//        //    if (strNgayKB.Trim().Length < 8)
//        //    {
//        //        v_ngaylv = TCS_GetNgayLV(pv_strMaNV);
//        //    }
//        //    else
//        //    {
//        //        v_ngaylv = strNgayKB;
//        //    }


//        //    DataSet ds = null;

//        //    try
//        //    {
//        //        //Lay thong tin header
//        //        sbsql.Append("SELECT   h.kyhieu_ct kyhieu_ct,");
//        //        sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//        //        sbsql.Append("         1 ttbuttoan,");
//        //        sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bn");
//        //        sbsql.Append("  FROM   tcs_ctu_hdr h");
//        //        sbsql.Append(" WHERE   (    (h.shkb = :shkb)");
//        //        sbsql.Append("          AND (h.ngay_kb = :ngay_kb)");
//        //        sbsql.Append("          AND (h.ma_nv = :ma_nv)");
//        //        sbsql.Append("          AND (h.ma_dthu = :ma_dthu)");
//        //        sbsql.Append("          AND (h.so_bt = :so_bt))");

//        //        IDbDataParameter[] p = null;
//        //        p = new IDbDataParameter[5];
//        //        p[0] = new OracleParameter();
//        //        p[0].ParameterName = "SHKB";
//        //        p[0].DbType = DbType.String;
//        //        p[0].Direction = ParameterDirection.Input;
//        //        p[0].Value = pv_strSHKB;

//        //        p[1] = new OracleParameter();
//        //        p[1].ParameterName = "NGAY_KB";
//        //        p[1].DbType = DbType.String;
//        //        p[1].Direction = ParameterDirection.Input;
//        //        p[1].Value = v_ngaylv;

//        //        p[2] = new OracleParameter();
//        //        p[2].ParameterName = "MA_NV";
//        //        p[2].DbType = DbType.String;
//        //        p[2].Direction = ParameterDirection.Input;
//        //        p[2].Value = pv_strMaNV;

//        //        p[3] = new OracleParameter();
//        //        p[3].ParameterName = "MA_DTHU";
//        //        p[3].DbType = DbType.String;
//        //        p[3].Direction = ParameterDirection.Input;
//        //        p[3].Value = pv_strMaDThu;

//        //        p[4] = new OracleParameter();
//        //        p[4].ParameterName = "SO_BT";
//        //        p[4].DbType = DbType.String;
//        //        p[4].Direction = ParameterDirection.Input;
//        //        p[4].Value = pv_strSoBT;

//        //        ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//        //        if (ds.Tables[0].Rows.Count <= 0)
//        //        {
//        //            throw new Exception("Không tìm thấy chứng từ!");
//        //        }

//        //        msg31.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//        //        msg31.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//        //        msg31.Data.TTButToan = ds.Tables[0].Rows[0]["TTBUTTOAN"].ToString();//Hoi lai
//        //        msg31.Data.Ngay_BN = ds.Tables[0].Rows[0]["NGAY_BN"].ToString();//Hoi lai

//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        StringBuilder sbErrMsg = default(StringBuilder);
//        //        sbErrMsg = new StringBuilder();
//        //        sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUToMSG31): ĐTNT - ");
//        //        sbErrMsg.Append(pv_strMaNTT);
//        //        sbErrMsg.Append(", SHKB - ");
//        //        sbErrMsg.Append(pv_strSHKB);
//        //        sbErrMsg.Append(", So CT - ");
//        //        sbErrMsg.Append(pv_strSoCT);
//        //        sbErrMsg.Append(", So BT - ");
//        //        sbErrMsg.Append(pv_strSoBT);
//        //        sbErrMsg.Append(", Ma NV - ");
//        //        sbErrMsg.Append(pv_strMaNV);
//        //        sbErrMsg.Append("\n");
//        //        sbErrMsg.Append(ex.Message);
//        //        sbErrMsg.Append("\n");
//        //        sbErrMsg.Append(ex.StackTrace);

//        //        LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//        //        if ((ex.InnerException != null))
//        //        {
//        //            sbErrMsg = new StringBuilder();
//        //            sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ (CTUToMSG31): ĐTNT - ");
//        //            sbErrMsg.Append(pv_strMaNTT);
//        //            sbErrMsg.Append(", SHKB - ");
//        //            sbErrMsg.Append(pv_strSHKB);
//        //            sbErrMsg.Append(", So CT - ");
//        //            sbErrMsg.Append(pv_strSoCT);
//        //            sbErrMsg.Append(", So BT - ");
//        //            sbErrMsg.Append(pv_strSoBT);
//        //            sbErrMsg.Append(", Ma NV - ");
//        //            sbErrMsg.Append(pv_strMaNV);
//        //            sbErrMsg.Append("\n");
//        //            sbErrMsg.Append(ex.InnerException.Message);
//        //            sbErrMsg.Append("\n");
//        //            sbErrMsg.Append(ex.InnerException.StackTrace);

//        //            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//        //        }

//        //        throw ex;

//        //    }
//        //    finally
//        //    {
//        //    }


//        //}

//        /// <summary>
//        /// Ham gui lay du lieu MSG 22
//        /// </summary>
//        /// <returns>Oracle Connection</returns>
//        /// <param name="p_MaDTNT"></param>
//        /// <param name="p_TenDTNT"></param>
//        /// <param name="p_Ma_HQ"></param>
//        /// <param name="p_Ten_HQ"></param>
//        /// <param name="p_Ma_LH"></param>
//        /// <param name="p_Nam_DK"></param>
//        /// <param name="p_SoTK"></param>
//        /// <remarks>
//        ///</remarks>
//        public static void getMSG32(String pv_strSo_TN_CT_YCH, ref String pv_ErrorNum, ref String pv_ErrorMes, ref String pv_strSo_TN_CT,
//                                    ref String pv_ngay_TN_CT, ref String pv_transaction_id)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG31 msg31 = new MSG31();
//                msg31.Data = new CustomsV3.MSG.MSG31.Data();
//                msg31.Header = new CustomsV3.MSG.MSG31.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg31.Header.Message_Version = strMessage_Version;
//                msg31.Header.Sender_Code = strSender_Code;
//                msg31.Header.Sender_Name = strSender_Name;
//                msg31.Header.Transaction_Type = "31";
//                msg31.Header.Transaction_Name = "Thông điệp đề nghị hủy xác nhận nộp thuế của ngân hàng";
//                msg31.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                pv_transaction_id = Guid.NewGuid().ToString();
//                msg31.Header.Transaction_ID = pv_transaction_id;
//                msg31.Data.So_TN_CT_YCH = pv_strSo_TN_CT_YCH;
//                ////Gan du lieu Data
//                //CTUToMSG31(pv_strMaNTT, pv_strSHKB,
//                //                           pv_strSoCT, pv_strSoBT,
//                //                           pv_strMaNV, pv_strMaDThu, pv_strNgayKB, ref msg31);

//                //Chuyen thanh XML
//                string strXMLOut = msg31.MSG31toXML(msg31);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG31", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG32 msg32 = new MSG32();
//                MSG32 objTemp = msg32.MSGToObject(strXMLIn);
//                //Verify sign                
//                //if (!verifySignMsg(objTemp, "MSG32"))
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //    return;
//                //}
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;
//              if (objTemp.Data.So_TN_CT != null && objTemp.Data.Ngay_TN_CT != null)
//                {
//                    pv_strSo_TN_CT = objTemp.Data.So_TN_CT;
//                    pv_ngay_TN_CT = objTemp.Data.Ngay_TN_CT;
//                }


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 32: ĐTNT - ");
//                sbErrMsg.Append(pv_strSo_TN_CT_YCH);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 32: ĐTNT - ");
//                    sbErrMsg.Append(pv_strSo_TN_CT_YCH);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = ex.Message.ToString();

//                //throw ex;
//            }

//        }

//        #endregion "Đề nghị hủy xác nhận nộp thuế của ngân hàng M31 - M32"

//        #region "Đối chiếu dữ liệu giao dịch nộp thuế thành công M41 - M42"

//        public static void DOICHIEUToMSG41(DateTime pv_NgayDC, ref MSG41 msg41)
//        {
//            msg41.Data = new CustomsV3.MSG.MSG41.Data();

//            msg41.Data.tem.Ma_NH_DC = strSender_Code;
//            msg41.Data.tem.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");


//            StringBuilder sbsql = new StringBuilder();

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   hdr.id,");
//                sbsql.Append("         hdr.transaction_id transaction_id,");
//                sbsql.Append("         hdr.SO_TN_CT SO_TN_CT,");
//                sbsql.Append("         hdr.NGAY_TN_CT NGAY_TN_CT,");
//                sbsql.Append("         hdr.ma_nnthue ma_dv,");
//                sbsql.Append("         hdr.ten_nnthue ten_dv,");
//                sbsql.Append("         hdr.ma_nh_ph ma_nh_ph,");
//                sbsql.Append("         hdr.ten_nh_ph ten_nh_ph,");
//                sbsql.Append("         hdr.ma_nh_th ma_nh_th,");
//                sbsql.Append("         hdr.ten_nh_th ten_nh_th,");
//                sbsql.Append("         hdr.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         hdr.ma_hq_cqt ma_hq_cqt,");
//                sbsql.Append("         hdr.ma_hq ma_hq,");
//                sbsql.Append("         hdr.lhxnk ma_lh,");
//                sbsql.Append("         hdr.so_tk sotk,");
//                sbsql.Append("         to_char(hdr.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         nvl(hdr.ma_loaitien,'1') ma_lt,");
//                //sbsql.Append("         NVL(hdr.ma_nkt,DECODE(SUBSTR(hdr.tkkb_ct,0,3),'741','1','2')) ma_ntk,");
//                sbsql.Append("         hdr.ma_ntk ma_ntk,");
//                sbsql.Append("         hdr.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(hdr.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         hdr.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (hdr.shkb) ten_kb,");
//                sbsql.Append("         hdr.tkkb_ct tkkb,");
//                sbsql.Append("         hdr.ttien ttien,");
//                sbsql.Append("         hdr.tkkb_ct tkkb_ct,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bn, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bc, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (hdr.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD')");
//                sbsql.Append("             ngay_ct,");
//                sbsql.Append("         hdr.diengiai diengiai");
//                sbsql.Append("  FROM   tcs_dchieu_nhhq_hdr hdr");
//                sbsql.Append(" WHERE   hdr.transaction_type = 'M41'and hdr.accept_yn='Y' " + strDKNGAYDC);
//                DateTime ngayDC1 = pv_NgayDC.AddDays(-1);

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAYDC1";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = ngayDC1.ToString("yyyy-MM-dd") + strGio_DC;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAYDC2";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_NgayDC.ToString("yyyy-MM-dd") + strGio_DC;

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                List<CustomsV3.MSG.MSG41.ItemData> listItems = new List<CustomsV3.MSG.MSG41.ItemData>();
//                int i = 0;
//                foreach (DataRow drhdr in ds.Tables[0].Rows)
//                {
//                    CustomsV3.MSG.MSG41.ItemData item = new CustomsV3.MSG.MSG41.ItemData();
//                    item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                    item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                    item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();
//                    //Muc dich: Them cac truong msg hq can
//                    //Tac gia : Anhld
//                    item.Ma_NH_PH = strSender_Code;
//                    item.Ten_NH_PH = strTen_NH_PH;
//                    item.Ma_NH_TH = drhdr["Ma_NH_TH"].ToString();
//                    item.Ten_NH_TH = drhdr["ten_nh_th"].ToString();
//                    item.Ma_DV = drhdr["MA_DV"].ToString();
//                    item.Ten_DV = drhdr["TEN_DV"].ToString();
//                    item.Ma_HQ_CQT = drhdr["MA_HQ_CQT"].ToString();
//                    item.Ma_HQ_PH = drhdr["MA_HQ_PH"].ToString();
//                    item.Ma_HQ = drhdr["MA_HQ"].ToString();
//                    item.Ma_LH = drhdr["MA_LH"].ToString();
//                    item.So_TK = drhdr["SOTK"].ToString();
//                    item.Ngay_DK = drhdr["NGAY_DK"].ToString();
//                    item.Ma_LT = drhdr["MA_LT"].ToString();
//                    item.Ma_NTK = drhdr["MA_NTK"].ToString();

//                    //if (drhdr["MA_NTK"].ToString().Substring(0, 3) == "741" || drhdr["MA_NTK"].ToString().Substring(0, 4) == "7111")
//                    //{
//                    //    item.Ma_NTK = "1";
//                    //}
//                    //else {
//                    //    item.Ma_NTK = "2";
//                    //}
//                    item.Loai_CT = strLoai_CT;
//                    item.KyHieu_CT = drhdr["KYHIEU_CT"].ToString();
//                    item.So_CT = drhdr["SO_CT"].ToString();
//                    item.TTButToan = drhdr["TTBUTTOAN"].ToString();//Hoi lai
//                    item.Ma_KB = drhdr["MA_KB"].ToString();//Hoi lai
//                    item.Ten_KB = Globals.RemoveSign4VietnameseString(drhdr["TEN_KB"].ToString());//Hoi lai
//                    item.TKKB = drhdr["TKKB"].ToString();
//                    item.TKKB_CT = drhdr["TKKB_CT"].ToString();
//                    item.Ngay_BN = drhdr["NGAY_BN"].ToString();
//                    item.Ngay_BC = drhdr["NGAY_BC"].ToString();
//                    item.Ngay_CT = drhdr["NGAY_CT"].ToString();
//                    item.DienGiai = drhdr["DIENGIAI"].ToString();
//                    item.DuNo_TO = drhdr["TTIEN"].ToString();

//                    //Lay thong tin detail
//                    StringBuilder sbsqldetail = new StringBuilder();
//                    sbsqldetail.Append("SELECT   dtl.ma_chuong ma_chuong,");
//                    sbsqldetail.Append("         dtl.ma_nkt ma_khoan,");
//                    sbsqldetail.Append("         dtl.ma_ndkt ma_tmuc,");
//                    sbsqldetail.Append("         dtl.sotien sotien");
//                    sbsqldetail.Append("  FROM   tcs_dchieu_nhhq_dtl dtl");
//                    sbsqldetail.Append(" WHERE   ( (dtl.hdr_id = :hdr_id))");

//                    p = new IDbDataParameter[1];
//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "HDR_ID";
//                    p[0].DbType = DbType.Int64;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = Int64.Parse(drhdr["ID"].ToString());

//                    ds = ExecuteReturnDataSet(sbsqldetail.ToString(), CommandType.Text, p);

//                    item.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();
//                    foreach (DataRow row in ds.Tables[0].Rows)
//                    {
//                        bool bFlag_Gan_TMT = false;

//                        //Gan so tien thue XK
//                        if (row["ma_tmuc"].ToString().StartsWith("185"))
//                        {
//                            // item.Chuong_XK = row["ma_chuong"].ToString();
//                            item.Khoan_XK = row["ma_khoan"].ToString();
//                            item.TieuMuc_XK = row["ma_tmuc"].ToString();
//                            item.DuNo_XK = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        //Gan so tien thue XK
//                        if (row["ma_tmuc"].ToString().StartsWith("190") && (!row["ma_tmuc"].ToString().Equals("1903")))
//                        {
//                            // item.Chuong_NK = row["ma_chuong"].ToString();
//                            item.Khoan_NK = row["ma_khoan"].ToString();
//                            item.TieuMuc_NK = row["ma_tmuc"].ToString();
//                            item.DuNo_NK = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        //Gan so tien thue VA
//                        if (row["ma_tmuc"].ToString().StartsWith("170"))
//                        {
//                            //  item.Chuong_VA = row["ma_chuong"].ToString();
//                            item.Khoan_VA = row["ma_khoan"].ToString();
//                            item.TieuMuc_VA = row["ma_tmuc"].ToString();
//                            item.DuNo_VA = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        //Gan so tien thue TD
//                        if (row["ma_tmuc"].ToString().StartsWith("175") || row["ma_tmuc"].ToString().Equals("1903"))
//                        {
//                            //  item.Chuong_TD = row["ma_chuong"].ToString();
//                            item.Khoan_TD = row["ma_khoan"].ToString();
//                            item.TieuMuc_TD = row["ma_tmuc"].ToString();
//                            item.DuNo_TD = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        //Gan so tien thue TV
//                        if (row["ma_tmuc"].ToString().StartsWith("195"))
//                        {
//                            //  item.Chuong_TV = row["ma_chuong"].ToString();
//                            item.Khoan_TV = row["ma_khoan"].ToString();
//                            item.TieuMuc_TV = row["ma_tmuc"].ToString();
//                            item.DuNo_TV = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }
//                        //Gan so tien thue MT
//                        if (row["ma_tmuc"].ToString().StartsWith("200"))
//                        {
//                            //  item.Chuong_MT = row["ma_chuong"].ToString();
//                            item.Khoan_MT = row["ma_khoan"].ToString();
//                            item.TieuMuc_MT = row["ma_tmuc"].ToString();
//                            item.DuNo_MT = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }
//                        if (row["ma_tmuc"].ToString().StartsWith("425"))
//                        {
//                            // item.Chuong_KH = row["ma_chuong"].ToString();
//                            item.Khoan_KH = row["ma_khoan"].ToString();
//                            item.TieuMuc_KH = row["ma_tmuc"].ToString();
//                            item.DuNo_KH = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        if (bFlag_Gan_TMT == false)
//                        {
//                            //  item.Chuong_KH = row["ma_chuong"].ToString();
//                            item.Khoan_KH = row["ma_khoan"].ToString();
//                            item.TieuMuc_KH = row["ma_tmuc"].ToString();
//                            item.DuNo_KH = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }
//                    }
//                    listItems.Add(item);
//                }
//                msg41.Data.Item = listItems;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUToMSG41): Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUToMSG41): Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }
//        #region "Đối chiếu dữ liệu giao dịch nộp thuế thành công M42 - M45"

//        public static void DOICHIEUToMSG45(DateTime pv_NgayDC, ref MSG45 msg45)
//        {
//            msg45.Data = new CustomsV3.MSG.MSG45.Data();

//            msg45.Data.tem.Ma_NH_DC = strSender_Code;
//            msg45.Data.tem.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");


//            StringBuilder sbsql = new StringBuilder();

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   hdr.id,");
//                sbsql.Append("         hdr.transaction_id transaction_id,");
//                sbsql.Append("         hdr.SO_TN_CT SO_TN_CT,");
//                sbsql.Append("         hdr.NGAY_TN_CT NGAY_TN_CT,");
//                sbsql.Append("         hdr.ma_nnthue ma_dv,");
//                sbsql.Append("         hdr.ten_nnthue ten_dv,");
//                sbsql.Append("         hdr.ma_nh_ph ma_nh_ph,");
//                sbsql.Append("         '' ten_nh_ph,");
//                sbsql.Append("         hdr.ma_nh_th ma_nh_th,");
//                sbsql.Append("         hdr.ten_nh_th ten_nh_th,");
//                sbsql.Append("         hdr.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         hdr.ma_hq_cqt ma_hq_cqt,");
//                sbsql.Append("         hdr.ma_hq ma_hq,");
//                sbsql.Append("         hdr.lhxnk ma_lh,");
//                sbsql.Append("         hdr.so_tk sotk,");
//                sbsql.Append("         to_char(hdr.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         nvl(hdr.ma_loaitien,'1') ma_lt,");
//                //sbsql.Append("         NVL(hdr.ma_nkt,DECODE(SUBSTR(hdr.tkkb_ct,0,3),'741','1','2')) ma_ntk,");
//                sbsql.Append("         hdr.ma_ntk ma_ntk,");
//                sbsql.Append("         hdr.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(hdr.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         hdr.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (hdr.shkb) ten_kb,");
//                sbsql.Append("         hdr.tkkb_ct tkkb,");
//                sbsql.Append("         hdr.ttien ttien,");
//                sbsql.Append("         hdr.tkkb_ct tkkb_ct,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bn, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bc, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (hdr.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD') ngay_ct,");
//                sbsql.Append("         hdr.ma_nt   ma_nt,");
//                sbsql.Append("         hdr.ty_gia  ty_gia,");
//                sbsql.Append("         hdr.ttien_nt  ttien_nt,");
//                sbsql.Append("         hdr.diengiai diengiai");
//                sbsql.Append("  FROM   tcs_dchieu_nhhq_hdr hdr");
//                //sbsql.Append(" WHERE   hdr.transaction_type = 'M45'and hdr.accept_yn='Y' ") ;
//                sbsql.Append(" WHERE   hdr.transaction_type = 'M45'and hdr.accept_yn='Y' " + strDKNGAYDC);
//                DateTime ngayDC1 = pv_NgayDC.AddDays(-1);

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAYDC1";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = ngayDC1.ToString("yyyy-MM-dd") + strGio_DC;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAYDC2";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_NgayDC.ToString("yyyy-MM-dd") + strGio_DC;

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                List<CustomsV3.MSG.MSG45.ItemData> listItems = new List<CustomsV3.MSG.MSG45.ItemData>();
//                int i = 0;
//                foreach (DataRow drhdr in ds.Tables[0].Rows)
//                {
//                    CustomsV3.MSG.MSG45.ItemData item = new CustomsV3.MSG.MSG45.ItemData();
//                    item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                    item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                    item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();
//                    //Muc dich: Them cac truong msg hq can
//                    //Tac gia : Anhld
//                    item.Ma_NH_PH = drhdr["Ma_NH_PH"].ToString();
//                    item.Ten_NH_PH = strTen_NH_PH;
//                    item.Ma_NH_TH = drhdr["Ma_NH_TH"].ToString();
//                    item.Ten_NH_TH = drhdr["ten_nh_th"].ToString();
//                    item.Ma_DV = drhdr["MA_DV"].ToString();
//                    item.Ten_DV = drhdr["TEN_DV"].ToString();
//                    item.Ma_HQ_CQT = drhdr["MA_HQ_CQT"].ToString();
//                    item.Ma_HQ_PH = drhdr["MA_HQ_PH"].ToString();
//                    item.Ma_HQ = drhdr["MA_HQ"].ToString();
//                    item.Ma_LH = drhdr["MA_LH"].ToString();
//                    item.So_TK = drhdr["SOTK"].ToString();
//                    item.Ngay_DK = drhdr["NGAY_DK"].ToString();
//                    item.Ma_LT = drhdr["MA_LT"].ToString();
//                    item.Ma_NTK = drhdr["MA_NTK"].ToString();
//                    item.Loai_CT = strLoai_CT;
//                    item.KyHieu_CT = drhdr["KYHIEU_CT"].ToString();
//                    item.So_CT = drhdr["SO_CT"].ToString();
//                    item.TTButToan = drhdr["TTBUTTOAN"].ToString();//Hoi lai
//                    item.Ma_KB = drhdr["MA_KB"].ToString();//Hoi lai
//                    item.Ten_KB = Globals.RemoveSign4VietnameseString(drhdr["TEN_KB"].ToString());//Hoi lai
//                    item.TKKB = drhdr["TKKB"].ToString();
//                    item.TKKB_CT = drhdr["TKKB_CT"].ToString();
//                    item.Ngay_BN = drhdr["NGAY_BN"].ToString();
//                    item.Ngay_BC = drhdr["NGAY_BC"].ToString();
//                    item.Ngay_CT = drhdr["NGAY_CT"].ToString();
//                    item.DienGiai = drhdr["DIENGIAI"].ToString();
//                    item.DuNo_NT_TO = drhdr["TTIEN_NT"].ToString();
//                    item.DuNo_TO = drhdr["TTIEN"].ToString();
//                    item.Ma_NT = drhdr["MA_NT"].ToString();
//                    item.Ty_Gia = drhdr["Ty_Gia"].ToString();
//                    //Lay thong tin detail
//                    StringBuilder sbsqldetail = new StringBuilder();
//                    sbsqldetail.Append("SELECT   dtl.ma_chuong ma_chuong,");
//                    sbsqldetail.Append("         dtl.ma_nkt ma_khoan,");
//                    sbsqldetail.Append("         dtl.ma_ndkt ma_tmuc,");
//                    sbsqldetail.Append("         dtl.sotien_nt sotien_nt,");
//                    sbsqldetail.Append("         dtl.sotien sotien");
//                    sbsqldetail.Append("  FROM   tcs_dchieu_nhhq_dtl dtl");
//                    sbsqldetail.Append(" WHERE   ( (dtl.hdr_id = :hdr_id))");

//                    p = new IDbDataParameter[1];
//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "HDR_ID";
//                    p[0].DbType = DbType.Int64;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = Int64.Parse(drhdr["ID"].ToString());

//                    ds = ExecuteReturnDataSet(sbsqldetail.ToString(), CommandType.Text, p);

//                    item.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();
//                    foreach (DataRow row in ds.Tables[0].Rows)
//                    {
//                        bool bFlag_Gan_TMT = false;

//                        //Gan so tien thue XK
//                        if (row["ma_tmuc"].ToString().StartsWith("185"))
//                        {
//                            // item.Chuong_XK = row["ma_chuong"].ToString();
//                            item.Khoan_XK = row["ma_khoan"].ToString();
//                            item.TieuMuc_XK = row["ma_tmuc"].ToString();
//                            item.DuNo_NT_XK = row["sotien_nt"].ToString();
//                            item.DuNo_XK = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        //Gan so tien thue XK
//                        if (row["ma_tmuc"].ToString().StartsWith("190") && (!row["ma_tmuc"].ToString().Equals("1903")))
//                        {
//                            // item.Chuong_NK = row["ma_chuong"].ToString();
//                            item.Khoan_NK = row["ma_khoan"].ToString();
//                            item.TieuMuc_NK = row["ma_tmuc"].ToString();
//                            item.DuNo_NT_NK = row["sotien_nt"].ToString();
//                            item.DuNo_NK = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        //Gan so tien thue VA
//                        if (row["ma_tmuc"].ToString().StartsWith("170"))
//                        {
//                            //  item.Chuong_VA = row["ma_chuong"].ToString();
//                            item.Khoan_VA = row["ma_khoan"].ToString();
//                            item.TieuMuc_VA = row["ma_tmuc"].ToString();
//                            item.DuNo_NT_VA = row["sotien_nt"].ToString();
//                            item.DuNo_VA = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        //Gan so tien thue TD
//                        if (row["ma_tmuc"].ToString().StartsWith("175") || row["ma_tmuc"].ToString().Equals("1903"))
//                        {
//                            //  item.Chuong_TD = row["ma_chuong"].ToString();
//                            item.Khoan_TD = row["ma_khoan"].ToString();
//                            item.TieuMuc_TD = row["ma_tmuc"].ToString();
//                            item.DuNo_NT_TD = row["sotien_nt"].ToString();
//                            item.DuNo_TD = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        //Gan so tien thue TV
//                        if (row["ma_tmuc"].ToString().StartsWith("195"))
//                        {
//                            //  item.Chuong_TV = row["ma_chuong"].ToString();
//                            item.Khoan_TV = row["ma_khoan"].ToString();
//                            item.TieuMuc_TV = row["ma_tmuc"].ToString();
//                            item.DuNo_NT_TV = row["sotien_nt"].ToString();
//                            item.DuNo_TV = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }
//                        //Gan so tien thue MT
//                        if (row["ma_tmuc"].ToString().StartsWith("200"))
//                        {
//                            //  item.Chuong_MT = row["ma_chuong"].ToString();
//                            item.Khoan_MT = row["ma_khoan"].ToString();
//                            item.TieuMuc_MT = row["ma_tmuc"].ToString();
//                            item.DuNo_NT_MT = row["sotien_nt"].ToString();
//                            item.DuNo_MT = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }
//                        if (row["ma_tmuc"].ToString().StartsWith("425"))
//                        {
//                            // item.Chuong_KH = row["ma_chuong"].ToString();
//                            item.Khoan_KH = row["ma_khoan"].ToString();
//                            item.TieuMuc_KH = row["ma_tmuc"].ToString();
//                            item.DuNo_NT_KH = row["sotien_nt"].ToString();
//                            item.DuNo_KH = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }

//                        if (bFlag_Gan_TMT == false)
//                        {
//                            //  item.Chuong_KH = row["ma_chuong"].ToString();
//                            item.Khoan_KH = row["ma_khoan"].ToString();
//                            item.TieuMuc_KH = row["ma_tmuc"].ToString();
//                            item.DuNo_NT_KH = row["sotien_nt"].ToString();
//                            item.DuNo_KH = row["sotien"].ToString();
//                            bFlag_Gan_TMT = true;
//                        }
//                    }
//                    listItems.Add(item);
//                }
//                msg45.Data.Item = listItems;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUToMSG41): Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUToMSG41): Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }

//        public static void getMSG45(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG45 msg45 = new MSG45();
//                msg45.Data = new CustomsV3.MSG.MSG45.Data();
//                msg45.Header = new CustomsV3.MSG.MSG45.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg45.Header.Message_Version = strMessage_Version;
//                msg45.Header.Sender_Code = strSender_Code;
//                msg45.Header.Sender_Name = strSender_Name;
//                msg45.Header.Transaction_Type = "45";
//                msg45.Header.Transaction_Name = "Thông điệp đối chiếu dữ liệu giao dịch thành công đối với ngoại tệ";
//                msg45.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg45.Header.Transaction_ID = Guid.NewGuid().ToString();
//                InsertDCInfo(msg45.Header.Transaction_ID, "MSG45", pv_NgayDC, strLoai_CT);
//                //Gan du lieu Data
//                DOICHIEUToMSG45(pv_NgayDC, ref msg45);
//                //Chuyen thanh XML
//                string strXMLOut = msg45.MSG45toXML(msg45);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG45", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //                StringBuilder sb = new StringBuilder();
//                //                sb.Append(strXMLdefin1);
//                //                sb.Append("\r\n");
//                //                sb.Append(strXMLIn);
//                //                strXMLIn = sb.ToString();

//                MSG42 msg42 = new MSG42();
//                MSG42 objTemp = msg42.MSGToObject(strXMLIn);
//                //Verify sign                
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                //Verify sign                
//                //if (!verifySignMsg(objTemp, "MSG42"))
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //}
//                UpdateDCInfo(msg45.Header.Transaction_ID, "MSG45", pv_ErrorNum, pv_ErrorMes);

//            }
//            catch (Exception ex)
//            {
//                MSG41 msg41 = new MSG41();
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 42: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 42: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = sbErrMsg.ToString();
//                UpdateDCInfo(msg41.Header.Transaction_ID, "MSG41", pv_ErrorNum, pv_ErrorMes);
//            }

//        }

//        #endregion "Đối chiếu dữ liệu giao dịch nộp thuế thành công M42 - M45"
//        public static void getMSG42(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG41 msg41 = new MSG41();
//                msg41.Data = new CustomsV3.MSG.MSG41.Data();
//                msg41.Header = new CustomsV3.MSG.MSG41.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg41.Header.Message_Version = strMessage_Version;
//                msg41.Header.Sender_Code = strSender_Code;
//                msg41.Header.Sender_Name = strSender_Name;
//                msg41.Header.Transaction_Type = "41";
//                msg41.Header.Transaction_Name = "Thông điệp đối chiếu dữ liệu giao dịch thành công";
//                msg41.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg41.Header.Transaction_ID = Guid.NewGuid().ToString();
//                InsertDCInfo(msg41.Header.Transaction_ID, "MSG41", pv_NgayDC, strLoai_CT);
//                //Gan du lieu Data
//                DOICHIEUToMSG41(pv_NgayDC, ref msg41);
//                //Chuyen thanh XML
//                string strXMLOut = msg41.MSG41toXML(msg41);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG41", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //                StringBuilder sb = new StringBuilder();
//                //                sb.Append(strXMLdefin1);
//                //                sb.Append("\r\n");
//                //                sb.Append(strXMLIn);
//                //                strXMLIn = sb.ToString();

//                MSG42 msg42 = new MSG42();
//                MSG42 objTemp = msg42.MSGToObject(strXMLIn);
//                //Verify sign                
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                //Verify sign                
//                //if (!verifySignMsg(objTemp, "MSG42"))
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //}
//                UpdateDCInfo(msg41.Header.Transaction_ID, "MSG41", pv_ErrorNum, pv_ErrorMes);

//            }
//            catch (Exception ex)
//            {
//                MSG41 msg41 = new MSG41();
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 42: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 42: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = ex.Message.ToString();
//                //UpdateDCInfo(msg41.Header.Transaction_ID, "MSG41", pv_ErrorNum, pv_ErrorMes);
//            }

//        }

//        #endregion "Đối chiếu dữ liệu giao dịch nộp thuế thành công M41 - M42"
//        #region "Đối chiếu dữ liệu nộp thuế thành công cho nhiều tờ khai M47 -M42"
//        public static void getMSG47(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG47 msg47 = new MSG47();
//                msg47.Data = new CustomsV3.MSG.MSG47.Data();
//                msg47.Header = new CustomsV3.MSG.MSG47.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg47.Header.Message_Version = strMessage_Version;
//                msg47.Header.Sender_Code = strSender_Code;
//                msg47.Header.Sender_Name = strSender_Name;
//                msg47.Header.Transaction_Type = "47";
//                msg47.Header.Transaction_Name = "Thông điệp yêu cầu đối chiếu dữ liệu nộp thuế bằng VNĐ cho nhiều tờ khai thành công";
//                msg47.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg47.Header.Transaction_ID = Guid.NewGuid().ToString();
//                InsertDCInfo(msg47.Header.Transaction_ID, "MSG47", pv_NgayDC, strLoai_CT);
//                //Gan du lieu Data
//                DOICHIEUToMSG47(pv_NgayDC, ref msg47);
//                //Chuyen thanh XML
//                string strXMLOut = msg47.MSG47toXML(msg47);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG47", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                MSG42 msg42 = new MSG42();
//                MSG42 objTemp = msg42.MSGToObject(strXMLIn);
//                //Verify sign                
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                UpdateDCInfo(msg47.Header.Transaction_ID, "MSG47", pv_ErrorNum, pv_ErrorMes);

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 42: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 42: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = ex.Message.ToString();
//                //UpdateDCInfo(msg47.Header.Transaction_ID, "msg47", pv_ErrorNum, pv_ErrorMes);
//            }

//        }
//        public static void DOICHIEUToMSG47(DateTime pv_NgayDC, ref MSG47 msg47)
//        {
//            msg47.Data = new CustomsV3.MSG.MSG47.Data();

//            msg47.Data.tem.Ma_NH_DC = strSender_Code;
//            msg47.Data.tem.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");


//            StringBuilder sbsql = new StringBuilder();

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   hdr.id,");
//                sbsql.Append("         hdr.transaction_id transaction_id,");
//                sbsql.Append("         hdr.SO_TN_CT SO_TN_CT,");
//                sbsql.Append("         hdr.NGAY_TN_CT NGAY_TN_CT,");
//                sbsql.Append("         hdr.ma_nnthue ma_dv,");
//                sbsql.Append("         hdr.ten_nnthue ten_dv,");
//                sbsql.Append("         hdr.ma_nh_ph ma_nh_ph,");
//                sbsql.Append("         hdr.ten_nh_ph ten_nh_ph,");
//                sbsql.Append("         hdr.ma_nh_th ma_nh_th,");
//                sbsql.Append("         hdr.ten_nh_th ten_nh_th,");
//                sbsql.Append("         hdr.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         hdr.ma_hq_cqt ma_hq_cqt,");
//                sbsql.Append("         hdr.ma_hq ma_hq,");
//                sbsql.Append("         hdr.lhxnk ma_lh,");
//                sbsql.Append("         hdr.so_tk sotk,");
//                sbsql.Append("         to_char(hdr.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         nvl(hdr.ma_loaitien,'1') ma_lt,");
//                //sbsql.Append("         NVL(hdr.ma_nkt,DECODE(SUBSTR(hdr.tkkb_ct,0,3),'741','1','2')) ma_ntk,");
//                sbsql.Append("         hdr.ma_ntk ma_ntk,");
//                sbsql.Append("         hdr.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(hdr.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         hdr.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (hdr.shkb) ten_kb,");
//                sbsql.Append("         hdr.tkkb_ct tkkb,");
//                sbsql.Append("         hdr.ttien ttien,");
//                sbsql.Append("         hdr.tkkb_ct tkkb_ct,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bn, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bc, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (hdr.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD')");
//                sbsql.Append("             ngay_ct,");
//                sbsql.Append("         hdr.diengiai diengiai");
//                sbsql.Append("  FROM   tcs_dchieu_nhhq_hdr hdr");
//                sbsql.Append(" WHERE   hdr.transaction_type = 'M47'and hdr.accept_yn='Y' " + strDKNGAYDC);
//                DateTime ngayDC1 = pv_NgayDC.AddDays(-1);

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAYDC1";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = ngayDC1.ToString("yyyy-MM-dd") + strGio_DC;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAYDC2";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_NgayDC.ToString("yyyy-MM-dd") + strGio_DC;

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                List<CustomsV3.MSG.MSG47.Transaction> listItems = new List<CustomsV3.MSG.MSG47.Transaction>();
//               // int i = 0;

//                foreach (DataRow drhdr in ds.Tables[0].Rows)
//                {
//                    CustomsV3.MSG.MSG47.Transaction item = new CustomsV3.MSG.MSG47.Transaction();
//                    item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                    item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                    item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();
//                    //Muc dich: Them cac truong msg hq can
//                    //Tac gia : Anhld
//                    item.Ma_NH_PH = strSender_Code;
//                    item.Ten_NH_PH = strTen_NH_PH;
//                    item.Ma_NH_TH = drhdr["Ma_NH_TH"].ToString();
//                    item.Ten_NH_TH = drhdr["ten_nh_th"].ToString();
//                    item.Ma_DV = drhdr["MA_DV"].ToString();
//                    item.Ten_DV = drhdr["TEN_DV"].ToString();
//                    item.Ma_HQ_CQT = drhdr["MA_HQ_CQT"].ToString();
//                    item.Ma_HQ_PH = drhdr["MA_HQ_PH"].ToString();
//                    item.Ma_NTK = drhdr["MA_NTK"].ToString();
//                    item.Loai_CT = strLoai_CT;
//                    item.KyHieu_CT = drhdr["KYHIEU_CT"].ToString();
//                    item.So_CT = drhdr["SO_CT"].ToString();
//                    item.Ma_KB = drhdr["MA_KB"].ToString();//Hoi lai
//                    item.Ten_KB = Globals.RemoveSign4VietnameseString(drhdr["TEN_KB"].ToString());//Hoi lai
//                    item.TKKB = drhdr["TKKB"].ToString();
//                    item.Ngay_BN = drhdr["NGAY_BN"].ToString();
//                    item.Ngay_BC = drhdr["NGAY_BC"].ToString();
//                    item.Ngay_CT = drhdr["NGAY_CT"].ToString();
//                    item.SoTien_TO=drhdr["TTien"].ToString();
//                    item.DienGiai = drhdr["DIENGIAI"].ToString();
//                    // lấy ra mảng tờ khai va loại tiền, sau đó lấy chi tiết từng tờ khai đó
//                    string so_tk = drhdr["sotk"].ToString();
//                    string ma_loaitienthue = drhdr["ma_lt"].ToString();
//                    //string ty_gia = drhdr["ty_gia"].ToString();
//                    string[] tokhaiso_arr = so_tk.Split(';');
//                    string[] loaitienthue_arr = ma_loaitienthue.Split(';');
//                    List<CustomsV3.MSG.MSG47.GNT_CT> listGNT = new List<CustomsV3.MSG.MSG47.GNT_CT>();
//                    //Lay thong tin detail
//                    for (int i = 0; i <= tokhaiso_arr.Length - 1; i++)
//                    {
//                        //Lay thong tin detail mục đich chỉ để test dữ liệu với hải quan 
//                        //chứ code để dùng ko lấy bảng tcs_ctu_dtl mà phải lấy ở tcs_dchieu_nhhq_dtl(dư lieuj phải dược inset đủ đẻ đối chiéuu)
//                        StringBuilder sbsqldetail = new StringBuilder();
//                        sbsqldetail.Append("SELECT   dtl.ma_chuong ma_chuong,");
//                        sbsqldetail.Append("         dtl.Ma_TMuc ma_tmuc,");
//                        sbsqldetail.Append("         dtl.TT_BTOAN TT_BTOAN,");
//                        sbsqldetail.Append("         dtl.MA_HQ MA_HQ,");
//                        sbsqldetail.Append("         dtl.MA_LHXNK MA_LHXNK,");
//                        sbsqldetail.Append("         to_char(dtl.ngay_tk,'yyyy') ngay_tk,");
//                        sbsqldetail.Append("         dtl.SO_TK SO_TK,");
//                        sbsqldetail.Append("         dtl.MA_LT MA_LT,");
//                        sbsqldetail.Append("         dtl.SAC_THUE SAC_THUE,");
//                        sbsqldetail.Append("         dtl.sotien sotien");
//                        sbsqldetail.Append("  FROM   tcs_ctu_dtl dtl");
//                        sbsqldetail.Append(" WHERE   dtl.so_ct = :hdr_id");
//                        sbsqldetail.Append(" and dtl.so_tk=:so_tk ");
//                        sbsqldetail.Append(" and dtl.ma_lt=:ma_lt ");

//                        p = new IDbDataParameter[3];
//                        p[0] = new OracleParameter();
//                        p[0].ParameterName = "HDR_ID";
//                        p[0].DbType = DbType.Int64;
//                        p[0].Direction = ParameterDirection.Input;
//                        p[0].Value = Int64.Parse(drhdr["So_CT"].ToString());

//                        p[1] = new OracleParameter();
//                        p[1].ParameterName = "so_tk";
//                        p[1].DbType = DbType.String;
//                        p[1].Direction = ParameterDirection.Input;
//                        p[1].Value = tokhaiso_arr[i].ToString();

//                        p[2] = new OracleParameter();
//                        p[2].ParameterName = "ma_lt";
//                        p[2].DbType = DbType.String;
//                        p[2].Direction = ParameterDirection.Input;
//                        p[2].Value = loaitienthue_arr[i].ToString();

//                        ds = ExecuteReturnDataSet(sbsqldetail.ToString(), CommandType.Text, p);

//                        item.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();

//                        CustomsV3.MSG.MSG47.GNT_CT GNT = new CustomsV3.MSG.MSG47.GNT_CT();
//                        GNT.TTButToan = ds.Tables[0].Rows[0]["tt_btoan"].ToString();
//                        GNT.So_TK = ds.Tables[0].Rows[0]["so_tk"].ToString();
//                        GNT.Nam_DK = ds.Tables[0].Rows[0]["ngay_tk"].ToString();
//                        GNT.Ma_HQ = ds.Tables[0].Rows[0]["ma_hq"].ToString();
//                        GNT.Ma_LH = ds.Tables[0].Rows[0]["ma_lhxnk"].ToString();
//                        GNT.Ma_LT = ds.Tables[0].Rows[0]["ma_lt"].ToString();
//                        List<CustomsV3.MSG.MSG47.ToKhai_CT> listToKhai = new List<CustomsV3.MSG.MSG47.ToKhai_CT>();
//                        foreach (DataRow row in ds.Tables[0].Rows)
//                        {

//                            CustomsV3.MSG.MSG47.ToKhai_CT ToKhai_CT = new CustomsV3.MSG.MSG47.ToKhai_CT();
//                            ToKhai_CT.Ma_ST = row["sac_thue"].ToString();
//                            ToKhai_CT.NDKT = row["ma_tmuc"].ToString();
//                            ToKhai_CT.SoTien = row["sotien"].ToString();
//                            //ToKhai_CT.SoTien_NT = row["sotien_nt"].ToString();
//                            //ToKhai_CT.Ty_Gia = ty_gia;
//                            listToKhai.Add(ToKhai_CT);
//                        }
//                        GNT.ToKhai_CT = listToKhai;
//                        listGNT.Add(GNT);
//                    }
//                    item.GNT_CT = listGNT;
//                    listItems.Add(item);
//                }
//                msg47.Data.Transaction = listItems;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUTomsg47): Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUTomsg47): Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }
//        #endregion"Kết thúc đối chiếu nộp thuế cho nhiều thành công M47"
//        #region "Đối chiếu dữ liệu nộp thuế thành công cho nhiều tờ khai M49 -M42"
//        public static void getMSG49(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG49 msg49 = new MSG49();
//                msg49.Data = new CustomsV3.MSG.MSG49.Data();
//                msg49.Header = new CustomsV3.MSG.MSG49.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg49.Header.Message_Version = strMessage_Version;
//                msg49.Header.Sender_Code = strSender_Code;
//                msg49.Header.Sender_Name = strSender_Name;
//                msg49.Header.Transaction_Type = "49";
//                msg49.Header.Transaction_Name = "Thông điệp yêu cầu đối chiếu dữ liệu nộp thuế bằng ngoại tệ cho nhiều tờ khai thành công";
//                msg49.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg49.Header.Transaction_ID = Guid.NewGuid().ToString();
//                InsertDCInfo(msg49.Header.Transaction_ID, "MSG49", pv_NgayDC, strLoai_CT);
//                //Gan du lieu Data
//                DOICHIEUToMSG49(pv_NgayDC, ref msg49);
//                //Chuyen thanh XML
//                string strXMLOut = msg49.MSG49toXML(msg49);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG49", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                MSG42 msg42 = new MSG42();
//                MSG42 objTemp = msg42.MSGToObject(strXMLIn);
//                //Verify sign                
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                UpdateDCInfo(msg49.Header.Transaction_ID, "MSG49", pv_ErrorNum, pv_ErrorMes);

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 42: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 42: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = ex.Message.ToString();
//                //UpdateDCInfo(msg49.Header.Transaction_ID, "msg49", pv_ErrorNum, pv_ErrorMes);
//            }

//        }
//        public static void DOICHIEUToMSG49(DateTime pv_NgayDC, ref MSG49 msg49)
//        {
//            msg49.Data = new CustomsV3.MSG.MSG49.Data();

//            msg49.Data.tem.Ma_NH_DC = strSender_Code;
//            msg49.Data.tem.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");


//            StringBuilder sbsql = new StringBuilder();

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   hdr.id,");
//                sbsql.Append("         hdr.transaction_id transaction_id,");
//                sbsql.Append("         hdr.SO_TN_CT SO_TN_CT,");
//                sbsql.Append("         hdr.NGAY_TN_CT NGAY_TN_CT,");
//                sbsql.Append("         hdr.ma_nnthue ma_dv,");
//                sbsql.Append("         hdr.ten_nnthue ten_dv,");
//                sbsql.Append("         hdr.ma_nh_ph ma_nh_ph,");
//                sbsql.Append("         hdr.ten_nh_ph ten_nh_ph,");
//                sbsql.Append("         hdr.ma_nh_th ma_nh_th,");
//                sbsql.Append("         hdr.ten_nh_th ten_nh_th,");
//                sbsql.Append("         hdr.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         hdr.ma_hq_cqt ma_hq_cqt,");
//                sbsql.Append("         hdr.ma_hq ma_hq,");
//                sbsql.Append("         hdr.lhxnk ma_lh,");
//                sbsql.Append("         hdr.so_tk sotk,");
//                sbsql.Append("         to_char(hdr.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         nvl(hdr.ma_loaitien,'1') ma_lt,");
//                //sbsql.Append("         NVL(hdr.ma_nkt,DECODE(SUBSTR(hdr.tkkb_ct,0,3),'741','1','2')) ma_ntk,");
//                sbsql.Append("         hdr.ma_ntk ma_ntk,");
//                sbsql.Append("         hdr.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(hdr.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         hdr.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (hdr.shkb) ten_kb,");
//                sbsql.Append("         hdr.tkkb_ct tkkb,");
//                sbsql.Append("         hdr.ttien ttien,");
//                sbsql.Append("         hdr.tkkb_ct tkkb_ct,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bn, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bc, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (hdr.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD')");
//                sbsql.Append("             ngay_ct,");
//                sbsql.Append("         hdr.diengiai diengiai,");
//                sbsql.Append("         hdr.ty_gia ty_gia,");
//                sbsql.Append("         hdr.ma_nt ma_nt");
//                sbsql.Append("  FROM   tcs_dchieu_nhhq_hdr hdr");
//                sbsql.Append(" WHERE   hdr.transaction_type = 'M49'and hdr.accept_yn='Y' " + strDKNGAYDC);
//                DateTime ngayDC1 = pv_NgayDC.AddDays(-1);

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAYDC1";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = ngayDC1.ToString("yyyy-MM-dd") + strGio_DC;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAYDC2";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_NgayDC.ToString("yyyy-MM-dd") + strGio_DC;

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                List<CustomsV3.MSG.MSG49.Transaction> listItems = new List<CustomsV3.MSG.MSG49.Transaction>();
//                //int i = 0;

//                foreach (DataRow drhdr in ds.Tables[0].Rows)
//                {
//                    CustomsV3.MSG.MSG49.Transaction item = new CustomsV3.MSG.MSG49.Transaction();
//                    item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                    item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                    item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();
//                    //Muc dich: Them cac truong msg hq can
//                    //Tac gia : Anhld
//                    item.Ma_NH_PH = strSender_Code;
//                    item.Ten_NH_PH = strTen_NH_PH;
//                    item.Ma_NH_TH = drhdr["Ma_NH_TH"].ToString();
//                    item.Ten_NH_TH = drhdr["ten_nh_th"].ToString();
//                    item.Ma_DV = drhdr["MA_DV"].ToString();
                    
//                    item.Ten_DV = drhdr["TEN_DV"].ToString();
//                    item.Ma_HQ_CQT = drhdr["MA_HQ_CQT"].ToString();
//                    item.Ma_HQ_PH = drhdr["MA_HQ_PH"].ToString();
//                    item.Ma_NTK = drhdr["MA_NTK"].ToString();
//                    item.Loai_CT = strLoai_CT;
//                    item.KyHieu_CT = drhdr["KYHIEU_CT"].ToString();
//                    item.So_CT = drhdr["SO_CT"].ToString();
//                    item.Ma_KB = drhdr["MA_KB"].ToString();//Hoi lai
//                    item.Ten_KB = Globals.RemoveSign4VietnameseString(drhdr["TEN_KB"].ToString());//Hoi lai
//                    item.TKKB = drhdr["TKKB"].ToString();
//                    item.Ngay_BN = drhdr["NGAY_BN"].ToString();
//                    item.Ngay_BC = drhdr["NGAY_BC"].ToString();
//                    item.Ngay_CT = drhdr["NGAY_CT"].ToString();
//                    item.SoTien_TO = drhdr["ttien"].ToString(); ;
//                    item.DienGiai = drhdr["DIENGIAI"].ToString();
//                    // lấy ra mảng tờ khai va loại tiền, sau đó lấy chi tiết từng tờ khai đó
//                    string so_tk = drhdr["sotk"].ToString();
//                    string ma_loaitienthue = drhdr["ma_lt"].ToString();
//                    string ty_gia = drhdr["ty_gia"].ToString();
//                    string ma_nt = drhdr["ma_nt"].ToString();
//                    string[] tokhaiso_arr = so_tk.Split(';');
//                    string[] loaitienthue_arr = ma_loaitienthue.Split(';');
//                    List<CustomsV3.MSG.MSG49.GNT_CT> listGNT = new List<CustomsV3.MSG.MSG49.GNT_CT>();
//                    //Lay thong tin detail
//                    for (int i = 0; i <= tokhaiso_arr.Length - 1; i++)
//                    {
//                        //Lay thong tin detail
//                        StringBuilder sbsqldetail = new StringBuilder();
//                        sbsqldetail.Append("SELECT   dtl.ma_chuong ma_chuong,");
//                        sbsqldetail.Append("         dtl.Ma_TMuc ma_tmuc,");
//                        sbsqldetail.Append("         dtl.TT_BTOAN TT_BTOAN,");
//                        sbsqldetail.Append("         dtl.MA_HQ MA_HQ,");
//                        sbsqldetail.Append("         dtl.MA_LHXNK MA_LHXNK,");
//                        sbsqldetail.Append("         to_char(dtl.ngay_tk,'yyyy') ngay_tk,");
//                        sbsqldetail.Append("         dtl.SO_TK SO_TK,");
//                        sbsqldetail.Append("         dtl.MA_LT MA_LT,");
//                        sbsqldetail.Append("         dtl.SAC_THUE SAC_THUE,");
//                        sbsqldetail.Append("         dtl.sotien sotien,");
//                        sbsqldetail.Append("         dtl.sotien_nt sotien_nt");
//                        sbsqldetail.Append("  FROM   tcs_ctu_dtl dtl");
//                        sbsqldetail.Append(" WHERE   dtl.so_ct = :hdr_id");
//                        sbsqldetail.Append(" and dtl.so_tk=:so_tk ");
//                        sbsqldetail.Append(" and dtl.ma_lt=:ma_lt ");

//                        p = new IDbDataParameter[3];
//                        p[0] = new OracleParameter();
//                        p[0].ParameterName = "HDR_ID";
//                        p[0].DbType = DbType.Int64;
//                        p[0].Direction = ParameterDirection.Input;
//                        p[0].Value = Int64.Parse(drhdr["SO_CT"].ToString());

//                        p[1] = new OracleParameter();
//                        p[1].ParameterName = "so_tk";
//                        p[1].DbType = DbType.String;
//                        p[1].Direction = ParameterDirection.Input;
//                        p[1].Value = tokhaiso_arr[i].ToString();

//                        p[2] = new OracleParameter();
//                        p[2].ParameterName = "ma_lt";
//                        p[2].DbType = DbType.String;
//                        p[2].Direction = ParameterDirection.Input;
//                        p[2].Value = loaitienthue_arr[i].ToString();

//                        ds = ExecuteReturnDataSet(sbsqldetail.ToString(), CommandType.Text, p);
//                        item.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();
//                        CustomsV3.MSG.MSG49.GNT_CT GNT = new CustomsV3.MSG.MSG49.GNT_CT();
//                        GNT.TTButToan = ds.Tables[0].Rows[0]["tt_btoan"].ToString();
//                        GNT.So_TK = ds.Tables[0].Rows[0]["so_tk"].ToString();
//                        GNT.Nam_DK = ds.Tables[0].Rows[0]["ngay_tk"].ToString();
//                        GNT.Ma_HQ = ds.Tables[0].Rows[0]["ma_hq"].ToString();
//                        GNT.Ma_LH = ds.Tables[0].Rows[0]["ma_lhxnk"].ToString();
//                        GNT.Ma_LT = ds.Tables[0].Rows[0]["ma_lt"].ToString();
//                        List<CustomsV3.MSG.MSG49.ToKhai_CT> listToKhai = new List<CustomsV3.MSG.MSG49.ToKhai_CT>();
//                        foreach (DataRow row in ds.Tables[0].Rows)
//                        {

//                            CustomsV3.MSG.MSG49.ToKhai_CT ToKhai_CT = new CustomsV3.MSG.MSG49.ToKhai_CT();
//                            ToKhai_CT.Ma_ST = row["sac_thue"].ToString();
//                            ToKhai_CT.NDKT = row["ma_tmuc"].ToString();
//                            ToKhai_CT.SoTien = row["sotien"].ToString();
//                            ToKhai_CT.SoTien_NT = row["sotien_nt"].ToString();
//                            ToKhai_CT.Ma_NT = ma_nt;
//                            ToKhai_CT.Ty_Gia = ty_gia;
//                            listToKhai.Add(ToKhai_CT);
//                        }
//                        GNT.ToKhai_CT = listToKhai;
//                        listGNT.Add(GNT);
//                    }
//                    item.GNT_CT = listGNT;
//                    listItems.Add(item);
//                }
//                msg49.Data.Transaction = listItems;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUTomsg49): Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUTomsg49): Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }
//        #endregion"Kết thúc đối chiếu nộp thuế cho nhiều thành công M49"
//        #region "Xoa du lieu doi chieu"
//        public static void xoaDuLieuDC(String strTransactionType, DateTime dtNgayDC, IDbTransaction tranDel)
//        {
//            StringBuilder sbsqlDelete = new StringBuilder();
//            sbsqlDelete.Append("DELETE FROM tcs_dchieu_hqnh_dtl ");
//            sbsqlDelete.Append("WHERE hdr_id IN ( ");
//            sbsqlDelete.Append("SELECT hdr.id FROM tcs_dchieu_hqnh_hdr hdr  ");
//            sbsqlDelete.Append("WHERE hdr.transaction_type = :transaction_type  ");
//            sbsqlDelete.Append("AND to_char(hdr.ngay_bn,'yyyyMMdd') = :ngay_bn )");

//            IDbDataParameter[] pDel = new IDbDataParameter[2];
//            pDel[0] = new OracleParameter();
//            pDel[0].ParameterName = "TRANSACTION_TYPE";
//            pDel[0].DbType = DbType.String;
//            pDel[0].Direction = ParameterDirection.Input;
//            pDel[0].Value = strTransactionType;

//            pDel[1] = new OracleParameter();
//            pDel[1].ParameterName = "NGAY_BN";
//            pDel[1].DbType = DbType.String;
//            pDel[1].Direction = ParameterDirection.Input;
//            pDel[1].Value = dtNgayDC.ToString("yyyyMMdd");

//            ExecuteNonQuery(sbsqlDelete.ToString(), CommandType.Text, pDel, tranDel);
//            //Xoa hdr
//            sbsqlDelete = new StringBuilder();
//            sbsqlDelete.Append("DELETE FROM tcs_dchieu_hqnh_hdr hdr  ");
//            sbsqlDelete.Append("WHERE hdr.transaction_type = :transaction_type  ");
//            sbsqlDelete.Append("AND to_char(hdr.ngay_bn,'yyyyMMdd') = :ngay_bn ");

//            ExecuteNonQuery(sbsqlDelete.ToString(), CommandType.Text, pDel, tranDel);
//        }
//        #endregion "Xoa du lieu doi chieu"

//        #region "Truy vấn kết quả đối chiếu dữ liệu giao dịch thành công M43 - M44"

//        public static void MSG44ToDB(MSG44 msg44, DateTime pv_NgayDC)
//        {
//            OracleConnection conn = GetConnection();
//            IDbTransaction transCT = null;
//            string strTransactionType = "44";
//            try
//            {
//                transCT = conn.BeginTransaction();
//                //Xoa du lieu dc cu
//                xoaDuLieuDC(strTransactionType, pv_NgayDC, transCT);

//                foreach (CustomsV3.MSG.MSG44.ItemData itdata in msg44.Data.Item)
//                {
//                    Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//                    StringBuilder sbsql = null;
//                    IDbDataParameter[] p = null;

//                    DataSet dsCtu = null;
//                    //Lay thong tin trong tcs_ctu_hdr                    
//                    sbsql = new StringBuilder();
//                    sbsql.Append("SELECT ");
//                    sbsql.Append("to_char(ctu.ngay_tk,'RRRR-MM-DD') ngay_tk ");
//                    sbsql.Append(", ctu.ttien ");
//                    sbsql.Append(", ctu.ttien_nt ");
//                    sbsql.Append(", ctu.dvsdns ");
//                    sbsql.Append(", ctu.ma_nv ");
//                    sbsql.Append(", ctu.ten_dvsdns ");
//                    sbsql.Append("FROM   tcs_ctu_hdr ctu ");
//                    sbsql.Append("WHERE TO_NUMBER(ctu.so_ct) = :so_ct ");

//                    p = new IDbDataParameter[1];
//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "SO_CT";
//                    p[0].DbType = DbType.Int64;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = Int64.Parse(itdata.So_CT);

//                    dsCtu = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                    //Insert thong tin header     ds.Tables[0].Rows[0][""];
//                    sbsql = new StringBuilder();
//                    sbsql.Append("INSERT INTO tcs_dchieu_hqnh_hdr (id,");
//                    sbsql.Append("                                 transaction_id,");
//                    sbsql.Append("                                 transaction_type,");
//                    sbsql.Append("                                 shkb,");
//                    sbsql.Append("                                 ten_kb,");
//                    sbsql.Append("                                 ngay_kb,");
//                    sbsql.Append("                                 so_bt,");
//                    sbsql.Append("                                 kyhieu_ct,");
//                    sbsql.Append("                                 so_ct,");
//                    sbsql.Append("                                 ngay_ct,");
//                    sbsql.Append("                                 ngay_bn,");
//                    sbsql.Append("                                 ngay_bc,");
//                    sbsql.Append("                                 ma_nnthue,");
//                    sbsql.Append("                                 ten_nnthue,");
//                    sbsql.Append("                                 ma_cqthu,");
//                    sbsql.Append("                                 ten_cqthu,");
//                    sbsql.Append("                                 so_tk,");
//                    sbsql.Append("                                 ngay_tk,");
//                    sbsql.Append("                                 lhxnk,");
//                    sbsql.Append("                                 vt_lhxnk,");
//                    sbsql.Append("                                 ten_lhxnk,");
//                    sbsql.Append("                                 ttien,");
//                    sbsql.Append("                                 ttien_nt,");
//                    sbsql.Append("                                 trang_thai,");
//                    sbsql.Append("                                 tk_ns,");
//                    sbsql.Append("                                 ten_tk_ns,");
//                    sbsql.Append("                                 ma_nkt,");
//                    sbsql.Append("                                 so_bl,");
//                    sbsql.Append("                                 ngay_bl_batdau,");
//                    sbsql.Append("                                 ngay_bl_ketthuc,");
//                    sbsql.Append("                                 songay_bl,");
//                    sbsql.Append("                                 kieu_bl,");
//                    sbsql.Append("                                 dien_giai,");
//                    sbsql.Append("                                 request_id,");
//                    sbsql.Append("                                 accept_yn,");
//                    sbsql.Append("                                 parent_transaction_id,");
//                    sbsql.Append("                                 ma_hq_ph,");
//                    sbsql.Append("                                 kq_dc, ");
//                    sbsql.Append("                                 ma_nv, ");
//                    sbsql.Append("                                 SO_TN_CT, ");
//                    sbsql.Append("                                NGAY_TN_CT)");
//                    sbsql.Append("  VALUES   (:id,");
//                    sbsql.Append("            :transaction_id,");
//                    sbsql.Append("            :transaction_type,");
//                    sbsql.Append("            :shkb,");
//                    sbsql.Append("            :ten_kb,");
//                    sbsql.Append("            :ngay_kb,");
//                    sbsql.Append("            :so_bt,");
//                    sbsql.Append("            :kyhieu_ct,");
//                    sbsql.Append("            :so_ct,");
//                    sbsql.Append("            :ngay_ct,");
//                    sbsql.Append("            :ngay_bn,");
//                    sbsql.Append("            :ngay_bc,");
//                    sbsql.Append("            :ma_nnthue,");
//                    sbsql.Append("            :ten_nnthue,");
//                    sbsql.Append("            :ma_cqthu,");
//                    sbsql.Append("            :ten_cqthu,");
//                    sbsql.Append("            :so_tk,");
//                    sbsql.Append("            :ngay_tk,");
//                    sbsql.Append("            :lhxnk,");
//                    sbsql.Append("            :vt_lhxnk,");
//                    sbsql.Append("            :ten_lhxnk,");
//                    sbsql.Append("            :ttien,");
//                    sbsql.Append("            :ttien_nt,");
//                    sbsql.Append("            :trang_thai,");
//                    sbsql.Append("            :tk_ns,");
//                    sbsql.Append("            :ten_tk_ns,");
//                    sbsql.Append("            :ma_nkt,");
//                    sbsql.Append("            :so_bl,");
//                    sbsql.Append("            :ngay_bl_batdau,");
//                    sbsql.Append("            :ngay_bl_ketthuc,");
//                    sbsql.Append("            :songay_bl,");
//                    sbsql.Append("            :kieu_bl,");
//                    sbsql.Append("            :dien_giai,");
//                    sbsql.Append("            :request_id,");
//                    sbsql.Append("            :accept_yn,");
//                    sbsql.Append("            :parent_transaction_id,");
//                    sbsql.Append("            :ma_hq_ph,");
//                    sbsql.Append("            :kq_dc,");
//                    sbsql.Append("            :ma_nv,");
//                    sbsql.Append("            :SO_TN_CT,");
//                    sbsql.Append("            :NGAY_TN_CT)");

//                    p = new IDbDataParameter[41];

//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "ID";
//                    p[0].DbType = DbType.Int64;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = DBNull.Value;
//                    p[0].Value = iId;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TRANSACTION_ID";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = DBNull.Value;
//                    if (itdata.Transaction_ID != null)
//                        p[1].Value = itdata.Transaction_ID.ToString().Trim();

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "TRANSACTION_TYPE";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strTransactionType;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "SHKB";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;
//                    if (itdata.Ma_KB != null)
//                        p[3].Value = itdata.Ma_KB;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "TEN_KB";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;
//                    if (itdata.Ten_KB != null)
//                        p[4].Value = itdata.Ten_KB;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "NGAY_KB";
//                    p[5].DbType = DbType.Int64;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = DBNull.Value;
//                    if (itdata.Ngay_BC != null)
//                        p[5].Value = Int64.Parse((DateTime.ParseExact(itdata.Ngay_BC, "yyyy-MM-dd", null)).ToString("yyyyMMdd"));

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "SO_BT";
//                    p[6].DbType = DbType.Int64;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = DBNull.Value;
//                    if (itdata.TTButToan != null)
//                        p[6].Value = Int64.Parse(itdata.TTButToan);//Hoi lai

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "KYHIEU_CT";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = DBNull.Value;
//                    if (itdata.KyHieu_CT != null)
//                        p[7].Value = itdata.KyHieu_CT;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "SO_CT";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = DBNull.Value;
//                    if (itdata.So_CT != null)
//                        p[8].Value = itdata.So_CT;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "NGAY_CT";
//                    p[9].DbType = DbType.Int64;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;
//                    if (itdata.Ngay_CT != null)
//                        p[9].Value = Int64.Parse((DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null)).ToString("yyyyMMdd"));

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "NGAY_BN";
//                    p[10].DbType = DbType.DateTime;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = pv_NgayDC;
//                    if (itdata.Ngay_BN != null)
//                        p[10].Value = DateTime.ParseExact(itdata.Ngay_BN, "yyyy-MM-dd", null);

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_BC";
//                    p[11].DbType = DbType.DateTime;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = DBNull.Value;
//                    if (itdata.Ngay_BC != null)
//                        p[11].Value = DateTime.ParseExact(itdata.Ngay_BC, "yyyy-MM-dd", null);

//                    p[12] = new OracleParameter();
//                    p[12].ParameterName = "MA_NNTHUE";
//                    p[12].DbType = DbType.String;
//                    p[12].Direction = ParameterDirection.Input;
//                    p[12].Value = DBNull.Value;
//                    if (itdata.Ma_DV != null)
//                        p[12].Value = itdata.Ma_DV;

//                    p[13] = new OracleParameter();
//                    p[13].ParameterName = "TEN_NNTHUE";
//                    p[13].DbType = DbType.String;
//                    p[13].Direction = ParameterDirection.Input;
//                    p[13].Value = DBNull.Value;
//                    if (itdata.Ten_DV != null)
//                        p[13].Value = itdata.Ten_DV;

//                    p[14] = new OracleParameter();
//                    p[14].ParameterName = "MA_CQTHU";
//                    p[14].DbType = DbType.String;
//                    p[14].Direction = ParameterDirection.Input;
//                    p[14].Value = DBNull.Value;
//                    if (itdata.Ma_HQ_CQT != null)
//                        p[14].Value = itdata.Ma_HQ_CQT.Trim();

//                    p[15] = new OracleParameter();
//                    p[15].ParameterName = "TEN_CQTHU";
//                    p[15].DbType = DbType.String;
//                    p[15].Direction = ParameterDirection.Input;
//                    p[15].Value = DBNull.Value;

//                    p[16] = new OracleParameter();
//                    p[16].ParameterName = "SO_TK";
//                    p[16].DbType = DbType.String;
//                    p[16].Direction = ParameterDirection.Input;
//                    p[16].Value = DBNull.Value;
//                    if (itdata.SoTK != null)
//                        p[16].Value = itdata.SoTK;

//                    p[17] = new OracleParameter();
//                    p[17].ParameterName = "NGAY_TK";
//                    p[17].DbType = DbType.DateTime;
//                    p[17].Direction = ParameterDirection.Input;
//                    p[17].Value = DBNull.Value;
//                    if (dsCtu.Tables[0].Rows.Count > 0)
//                        if (!String.Empty.Equals(dsCtu.Tables[0].Rows[0]["NGAY_TK"].ToString()))
//                            p[17].Value = DateTime.ParseExact(dsCtu.Tables[0].Rows[0]["NGAY_TK"].ToString(), "yyyy-MM-dd", null);


//                    p[18] = new OracleParameter();
//                    p[18].ParameterName = "LHXNK";
//                    p[18].DbType = DbType.String;
//                    p[18].Direction = ParameterDirection.Input;
//                    p[18].Value = DBNull.Value;
//                    if (itdata.Ma_LH != null)
//                        p[18].Value = itdata.Ma_LH;

//                    p[19] = new OracleParameter();
//                    p[19].ParameterName = "VT_LHXNK";//???
//                    p[19].DbType = DbType.String;
//                    p[19].Direction = ParameterDirection.Input;
//                    p[19].Value = DBNull.Value;


//                    p[20] = new OracleParameter();
//                    p[20].ParameterName = "TEN_LHXNK";
//                    p[20].DbType = DbType.String;
//                    p[20].Direction = ParameterDirection.Input;
//                    p[20].Value = DBNull.Value;

//                    p[21] = new OracleParameter();
//                    p[21].ParameterName = "TTIEN";
//                    p[21].DbType = DbType.Int64;
//                    p[21].Direction = ParameterDirection.Input;
//                    p[21].Value = DBNull.Value;
//                    Int64 iTongDuNo = 0;
//                    if (itdata.DuNo_TO != null)
//                    {
//                        if (!String.Empty.Equals(itdata.DuNo_TO.Trim()))
//                        {
//                            iTongDuNo += Int64.Parse(itdata.DuNo_TO.Trim());
//                        }
//                    }
//                    p[21].Value = iTongDuNo;

//                    p[22] = new OracleParameter();
//                    p[22].ParameterName = "TTIEN_NT";
//                    p[22].DbType = DbType.Int64;
//                    p[22].Direction = ParameterDirection.Input;
//                    p[22].Value = DBNull.Value;


//                    p[23] = new OracleParameter();
//                    p[23].ParameterName = "TRANG_THAI";//???
//                    p[23].DbType = DbType.String;
//                    p[23].Direction = ParameterDirection.Input;
//                    p[23].Value = DBNull.Value;

//                    p[24] = new OracleParameter();
//                    p[24].ParameterName = "TK_NS";//???
//                    p[24].DbType = DbType.String;
//                    p[24].Direction = ParameterDirection.Input;
//                    p[24].Value = DBNull.Value;
//                    if (dsCtu.Tables[0].Rows.Count > 0)
//                        if (dsCtu.Tables[0].Rows[0]["DVSDNS"] != null)
//                            p[24].Value = dsCtu.Tables[0].Rows[0]["DVSDNS"];

//                    p[25] = new OracleParameter();
//                    p[25].ParameterName = "TEN_TK_NS";
//                    p[25].DbType = DbType.String;
//                    p[25].Direction = ParameterDirection.Input;
//                    p[25].Value = DBNull.Value;
//                    if (dsCtu.Tables[0].Rows.Count > 0)
//                        if (dsCtu.Tables[0].Rows[0]["TEN_DVSDNS"] != null)
//                            p[25].Value = dsCtu.Tables[0].Rows[0]["TEN_DVSDNS"];

//                    p[26] = new OracleParameter();
//                    p[26].ParameterName = "MA_NKT";
//                    p[26].DbType = DbType.String;
//                    p[26].Direction = ParameterDirection.Input;
//                    p[26].Value = DBNull.Value;
//                    if (itdata.Ma_NTK != null)
//                        p[26].Value = itdata.Ma_NTK;

//                    p[27] = new OracleParameter();
//                    p[27].ParameterName = "SO_BL";//???
//                    p[27].DbType = DbType.String;
//                    p[27].Direction = ParameterDirection.Input;
//                    p[27].Value = DBNull.Value;


//                    p[28] = new OracleParameter();
//                    p[28].ParameterName = "NGAY_BL_BATDAU";//???
//                    p[28].DbType = DbType.DateTime;
//                    p[28].Direction = ParameterDirection.Input;
//                    p[28].Value = DBNull.Value;

//                    p[29] = new OracleParameter();
//                    p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//                    p[29].DbType = DbType.DateTime;
//                    p[29].Direction = ParameterDirection.Input;
//                    p[29].Value = DBNull.Value;

//                    p[30] = new OracleParameter();
//                    p[30].ParameterName = "SONGAY_BL";//???
//                    p[30].DbType = DbType.Int64;
//                    p[30].Direction = ParameterDirection.Input;
//                    p[30].Value = DBNull.Value;


//                    p[31] = new OracleParameter();
//                    p[31].ParameterName = "KIEU_BL";//???
//                    p[31].DbType = DbType.String;
//                    p[31].Direction = ParameterDirection.Input;
//                    p[31].Value = DBNull.Value;


//                    p[32] = new OracleParameter();
//                    p[32].ParameterName = "DIEN_GIAI";
//                    p[32].DbType = DbType.String;
//                    p[32].Direction = ParameterDirection.Input;
//                    p[32].Value = DBNull.Value;
//                    if (itdata.DienGiai != null)
//                        p[32].Value = itdata.DienGiai;

//                    p[33] = new OracleParameter();
//                    p[33].ParameterName = "REQUEST_ID";//???
//                    p[33].DbType = DbType.String;
//                    p[33].Direction = ParameterDirection.Input;
//                    p[33].Value = DBNull.Value;

//                    p[34] = new OracleParameter();
//                    p[34].ParameterName = "ACCEPT_YN";//???
//                    p[34].DbType = DbType.String;
//                    p[34].Direction = ParameterDirection.Input;
//                    p[34].Value = DBNull.Value;

//                    p[35] = new OracleParameter();
//                    p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//                    p[35].DbType = DbType.String;
//                    p[35].Direction = ParameterDirection.Input;
//                    p[35].Value = DBNull.Value;

//                    p[36] = new OracleParameter();
//                    p[36].ParameterName = "MA_HQ_PH";
//                    p[36].DbType = DbType.String;
//                    p[36].Direction = ParameterDirection.Input;
//                    p[36].Value = DBNull.Value;
//                    if (itdata.Ma_HQ_PH != null)
//                        p[36].Value = itdata.Ma_HQ_PH;

//                    p[37] = new OracleParameter();
//                    p[37].ParameterName = "KQ_DC";
//                    p[37].DbType = DbType.String;
//                    p[37].Direction = ParameterDirection.Input;
//                    p[37].Value = DBNull.Value;
//                    if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//                    {
//                        p[37].Value = "OK";
//                    }
//                    else
//                    {
//                        p[37].Value = itdata.KQ_DC;
//                    }

//                    p[38] = new OracleParameter();
//                    p[38].ParameterName = "MA_NV";
//                    p[38].DbType = DbType.Int64;
//                    p[38].Direction = ParameterDirection.Input;
//                    p[38].Value = DBNull.Value;
//                    if (dsCtu.Tables[0].Rows.Count > 0)
//                        if (dsCtu.Tables[0].Rows[0]["MA_NV"] != null)
//                            p[38].Value = Int64.Parse(dsCtu.Tables[0].Rows[0]["MA_NV"].ToString());

//                    p[39] = new OracleParameter();
//                    p[39].ParameterName = "SO_TN_CT";
//                    p[39].DbType = DbType.String;
//                    p[39].Direction = ParameterDirection.Input;
//                    p[39].Value = DBNull.Value;
//                    if (itdata.So_TN_CT != null)
//                        p[39].Value = itdata.So_TN_CT;

//                    p[40] = new OracleParameter();
//                    p[40].ParameterName = "NGAY_TN_CT";
//                    p[40].DbType = DbType.String;
//                    p[40].Direction = ParameterDirection.Input;
//                    p[40].Value = DBNull.Value;
//                    if (itdata.Ngay_TN_CT != null)
//                        p[40].Value = itdata.Ngay_TN_CT;
//                    ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);

//                    //insert tcs_dchieu_hqnh_dtl
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_XK, itdata.TieuMuc_XK, itdata.DuNo_XK, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_NK, itdata.TieuMuc_NK, itdata.DuNo_NK, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_VA, itdata.TieuMuc_VA, itdata.DuNo_VA, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_TD, itdata.TieuMuc_TD, itdata.DuNo_TD, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_TV, itdata.TieuMuc_TV, itdata.DuNo_TV, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_MT, itdata.TieuMuc_MT, itdata.DuNo_MT, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_KH, itdata.TieuMuc_KH, itdata.DuNo_KH, transCT);

//                }
//                transCT.Commit();


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch thành công : Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("insert vao CSDL\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch thành công : Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("insert vao CSDL\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }


//        }

//        public static void getMSG46(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                string strLoai_TT_DC = "M23";
//                MSG43 msg43 = new MSG43();
//                msg43.Data = new CustomsV3.MSG.MSG43.Data();
//                msg43.Header = new CustomsV3.MSG.MSG43.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg43.Header.Message_Version = strMessage_Version;
//                msg43.Header.Sender_Code = strSender_Code;
//                msg43.Header.Sender_Name = strSender_Name;
//                msg43.Header.Transaction_Type = "43";
//                msg43.Header.Transaction_Name = "Thông điệp truy vấn kết quả đối chiếu dữ liệu giao dịch chứng từ";
//                msg43.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg43.Header.Transaction_ID = Guid.NewGuid().ToString();
//                String strReqId = getReqId(pv_NgayDC, "MSG45", strLoai_CT);

//                msg43.Header.Request_ID = strReqId;

//                //Gan du lieu Data
//                msg43.Data.Ma_NH_DC = strSender_Code;
//                msg43.Data.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");
//                string tran_id = msg43.Header.Transaction_ID;
//                msg43.Data.Loai_TT_DC = strLoai_TT_DC;
//                InsertDCInfo(tran_id, "MSG46", pv_NgayDC, strLoai_CT);
//                //Chuyen thanh XML
//                string strXMLOut = msg43.MSG43toXML(msg43);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG46", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG46 msg46 = new MSG46();
//                MSG46 objTemp = msg46.MSGToObject(strXMLIn);

//                pv_ErrorMes = objTemp.Error.Error_Message;
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                //Verify sign                
//                if (verifySignMsg(objTemp, "MSG46"))
//                {
//                    if ("0".Equals(pv_ErrorNum))
//                    {
//                        //Cap nhat CSDL
//                        MSG46ToDB(objTemp, pv_NgayDC);
//                    }
//                }
//                else
//                {
//                    pv_ErrorNum = "00001";
//                    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                }
//                UpdateDCInfo(msg43.Header.Transaction_ID, "MSG46", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//            }

//        }
//        public static void getMSG44(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                string strLoai_TT_DC = "M21";
//                MSG43 msg43 = new MSG43();
//                msg43.Data = new CustomsV3.MSG.MSG43.Data();
//                msg43.Header = new CustomsV3.MSG.MSG43.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg43.Header.Message_Version = strMessage_Version;
//                msg43.Header.Sender_Code = strSender_Code;
//                msg43.Header.Sender_Name = strSender_Name;
//                msg43.Header.Transaction_Type = "43";
//                msg43.Header.Transaction_Name = "Thông điệp truy vấn kết quả đối chiếu dữ liệu giao dịch chứng từ";
//                msg43.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg43.Header.Transaction_ID = Guid.NewGuid().ToString();
//                String strReqId = getReqId(pv_NgayDC, "MSG41", strLoai_CT);

//                msg43.Header.Request_ID = strReqId;

//                //Gan du lieu Data
//                msg43.Data.Ma_NH_DC = strSender_Code;
//                msg43.Data.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");
//                msg43.Data.Loai_TT_DC = strLoai_TT_DC;
//                string tran_id = msg43.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "MSG44", pv_NgayDC, strLoai_CT);
//                //Chuyen thanh XML
//                string strXMLOut = msg43.MSG43toXML(msg43);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG44", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG44 msg44 = new MSG44();
//                MSG44 objTemp = msg44.MSGToObject(strXMLIn);

//                pv_ErrorMes = objTemp.Error.Error_Message;
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                //Verify sign                
//                //if (verifySignMsg(objTemp, "MSG44"))
//                //{
//                if ("0".Equals(pv_ErrorNum))
//                {
//                    //Cap nhat CSDL
//                    MSG44ToDB(objTemp, pv_NgayDC);
//                }
//                //}
//                //else
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //}
//                UpdateDCInfo(msg43.Header.Transaction_ID, "MSG44", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }


//            }

//        }
//        public static void DTLToDB(Int64 hdr_id, String ma_chuong, String ma_nkt, String ma_ndkt, String so_tien, IDbTransaction dbTran)
//        {
//            if (String.Empty.Equals(so_tien.Trim()) || so_tien == null || "0".Equals(so_tien.Trim()))
//            {
//                return;
//            }
//            //insert tcs_dchieu_hqnh_dtl
//            StringBuilder sbsql = new StringBuilder();

//            sbsql.Append("INSERT INTO tcs_dchieu_hqnh_dtl (hdr_id, ");
//            sbsql.Append("ma_quy, ");
//            sbsql.Append("ma_cap, ");
//            sbsql.Append("ma_chuong, ");
//            sbsql.Append("ma_nkt, ");
//            sbsql.Append("ma_nkt_cha, ");
//            sbsql.Append("ma_ndkt, ");
//            sbsql.Append("ma_ndkt_cha, ");
//            sbsql.Append("ma_tlpc, ");
//            sbsql.Append("ma_khtk, ");
//            sbsql.Append("noi_dung, ");
//            sbsql.Append("ma_dp, ");
//            sbsql.Append("ky_thue, ");
//            sbsql.Append("sotien) ");

//            sbsql.Append("VALUES( :hdr_id, ");
//            sbsql.Append(":ma_quy, ");
//            sbsql.Append(":ma_cap, ");
//            sbsql.Append(":ma_chuong, ");
//            sbsql.Append(":ma_nkt, ");
//            sbsql.Append(":ma_nkt_cha, ");
//            sbsql.Append(":ma_ndkt, ");
//            sbsql.Append(":ma_ndkt_cha, ");
//            sbsql.Append(":ma_tlpc, ");
//            sbsql.Append(":ma_khtk, ");
//            sbsql.Append(":noi_dung, ");
//            sbsql.Append(":ma_dp, ");
//            sbsql.Append(":ky_thue, ");
//            sbsql.Append(":sotien) ");

//            IDbDataParameter[] p = new IDbDataParameter[14];

//            p[0] = new OracleParameter();
//            p[0].ParameterName = "HDR_ID";
//            p[0].DbType = DbType.Int64;
//            p[0].Direction = ParameterDirection.Input;
//            p[0].Value = DBNull.Value;
//            p[0].Value = hdr_id;

//            p[1] = new OracleParameter();
//            p[1].ParameterName = "MA_QUY";
//            p[1].DbType = DbType.String;
//            p[1].Direction = ParameterDirection.Input;
//            p[1].Value = DBNull.Value;

//            p[2] = new OracleParameter();
//            p[2].ParameterName = "MA_CAP";
//            p[2].DbType = DbType.String;
//            p[2].Direction = ParameterDirection.Input;
//            p[2].Value = DBNull.Value;

//            p[3] = new OracleParameter();
//            p[3].ParameterName = "MA_CHUONG";
//            p[3].DbType = DbType.String;
//            p[3].Direction = ParameterDirection.Input;
//            p[3].Value = ma_chuong;

//            p[4] = new OracleParameter();
//            p[4].ParameterName = "MA_NKT";
//            p[4].DbType = DbType.String;
//            p[4].Direction = ParameterDirection.Input;
//            p[4].Value = ma_nkt;

//            p[5] = new OracleParameter();
//            p[5].ParameterName = "MA_NKT_CHA";
//            p[5].DbType = DbType.String;
//            p[5].Direction = ParameterDirection.Input;
//            p[5].Value = DBNull.Value;

//            p[6] = new OracleParameter();
//            p[6].ParameterName = "MA_NDKT";
//            p[6].DbType = DbType.String;
//            p[6].Direction = ParameterDirection.Input;
//            p[6].Value = ma_ndkt;

//            p[7] = new OracleParameter();
//            p[7].ParameterName = "MA_NDKT_CHA";
//            p[7].DbType = DbType.String;
//            p[7].Direction = ParameterDirection.Input;
//            p[7].Value = DBNull.Value;

//            p[8] = new OracleParameter();
//            p[8].ParameterName = "MA_TLPC";
//            p[8].DbType = DbType.Int64;
//            p[8].Direction = ParameterDirection.Input;
//            p[8].Value = DBNull.Value;

//            p[9] = new OracleParameter();
//            p[9].ParameterName = "MA_KHTK";
//            p[9].DbType = DbType.String;
//            p[9].Direction = ParameterDirection.Input;
//            p[9].Value = DBNull.Value;

//            p[10] = new OracleParameter();
//            p[10].ParameterName = "NOI_DUNG";
//            p[10].DbType = DbType.String;
//            p[10].Direction = ParameterDirection.Input;
//            p[10].Value = DBNull.Value;

//            p[11] = new OracleParameter();
//            p[11].ParameterName = "MA_DP";
//            p[11].DbType = DbType.String;
//            p[11].Direction = ParameterDirection.Input;
//            p[11].Value = DBNull.Value;

//            p[12] = new OracleParameter();
//            p[12].ParameterName = "KY_THUE";
//            p[12].DbType = DbType.String;
//            p[12].Direction = ParameterDirection.Input;
//            p[12].Value = DBNull.Value;


//            p[13] = new OracleParameter();
//            p[13].ParameterName = "SOTIEN";
//            p[13].DbType = DbType.Int64;
//            p[13].Direction = ParameterDirection.Input;
//            p[13].Value = Int64.Parse(so_tien);

//            ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, dbTran);
//        }

//        #endregion "Truy vấn kết quả đối chiếu dữ liệu giao dịch thành công M43 - M44"
//        #region "Truy vấn kết quả đối chiếu dữ liệu giao dịch thành công M43 - M45"

//        public static void MSG46ToDB(MSG46 msg46, DateTime pv_NgayDC)
//        {
//            OracleConnection conn = GetConnection();
//            IDbTransaction transCT = null;
//            string strTransactionType = "46";
//            try
//            {
//                transCT = conn.BeginTransaction();
//                //Xoa du lieu dc cu
//                xoaDuLieuDC(strTransactionType, pv_NgayDC, transCT);

//                foreach (CustomsV3.MSG.MSG46.ItemData itdata in msg46.Data.Item)
//                {
//                    Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//                    StringBuilder sbsql = null;
//                    IDbDataParameter[] p = null;

//                    DataSet dsCtu = null;
//                    //Lay thong tin trong tcs_ctu_hdr                    
//                    sbsql = new StringBuilder();
//                    sbsql.Append("SELECT ");
//                    sbsql.Append("to_char(ctu.ngay_tk,'RRRR-MM-DD') ngay_tk ");
//                    sbsql.Append(", ctu.ttien ");
//                    sbsql.Append(", ctu.ttien_nt ");
//                    sbsql.Append(", ctu.dvsdns ");
//                    sbsql.Append(", ctu.ma_nv ");
//                    sbsql.Append(", ctu.ten_dvsdns ");
//                    sbsql.Append("FROM   tcs_ctu_hdr ctu ");
//                    sbsql.Append("WHERE TO_NUMBER(ctu.so_ct) = :so_ct ");

//                    p = new IDbDataParameter[1];
//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "SO_CT";
//                    p[0].DbType = DbType.Int64;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = Int64.Parse(itdata.So_CT);

//                    dsCtu = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                    //Insert thong tin header     ds.Tables[0].Rows[0][""];
//                    sbsql = new StringBuilder();
//                    sbsql.Append("INSERT INTO tcs_dchieu_hqnh_hdr (id,");
//                    sbsql.Append("                                 transaction_id,");
//                    sbsql.Append("                                 transaction_type,");
//                    sbsql.Append("                                 shkb,");
//                    sbsql.Append("                                 ten_kb,");
//                    sbsql.Append("                                 ngay_kb,");
//                    sbsql.Append("                                 so_bt,");
//                    sbsql.Append("                                 kyhieu_ct,");
//                    sbsql.Append("                                 so_ct,");
//                    sbsql.Append("                                 ngay_ct,");
//                    sbsql.Append("                                 ngay_bn,");
//                    sbsql.Append("                                 ngay_bc,");
//                    sbsql.Append("                                 ma_nnthue,");
//                    sbsql.Append("                                 ten_nnthue,");
//                    sbsql.Append("                                 ma_cqthu,");
//                    sbsql.Append("                                 ten_cqthu,");
//                    sbsql.Append("                                 so_tk,");
//                    sbsql.Append("                                 ngay_tk,");
//                    sbsql.Append("                                 lhxnk,");
//                    sbsql.Append("                                 vt_lhxnk,");
//                    sbsql.Append("                                 ten_lhxnk,");
//                    sbsql.Append("                                 ttien,");
//                    sbsql.Append("                                 ttien_nt,");
//                    sbsql.Append("                                 trang_thai,");
//                    sbsql.Append("                                 tk_ns,");
//                    sbsql.Append("                                 ten_tk_ns,");
//                    sbsql.Append("                                 ma_nkt,");
//                    sbsql.Append("                                 so_bl,");
//                    sbsql.Append("                                 ngay_bl_batdau,");
//                    sbsql.Append("                                 ngay_bl_ketthuc,");
//                    sbsql.Append("                                 songay_bl,");
//                    sbsql.Append("                                 kieu_bl,");
//                    sbsql.Append("                                 dien_giai,");
//                    sbsql.Append("                                 request_id,");
//                    sbsql.Append("                                 accept_yn,");
//                    sbsql.Append("                                 parent_transaction_id,");
//                    sbsql.Append("                                 ma_hq_ph,");
//                    sbsql.Append("                                 kq_dc, ");
//                    sbsql.Append("                                 ma_nv, ");
//                    sbsql.Append("                                 SO_TN_CT, ");
//                    sbsql.Append("                                NGAY_TN_CT)");
//                    sbsql.Append("  VALUES   (:id,");
//                    sbsql.Append("            :transaction_id,");
//                    sbsql.Append("            :transaction_type,");
//                    sbsql.Append("            :shkb,");
//                    sbsql.Append("            :ten_kb,");
//                    sbsql.Append("            :ngay_kb,");
//                    sbsql.Append("            :so_bt,");
//                    sbsql.Append("            :kyhieu_ct,");
//                    sbsql.Append("            :so_ct,");
//                    sbsql.Append("            :ngay_ct,");
//                    sbsql.Append("            :ngay_bn,");
//                    sbsql.Append("            :ngay_bc,");
//                    sbsql.Append("            :ma_nnthue,");
//                    sbsql.Append("            :ten_nnthue,");
//                    sbsql.Append("            :ma_cqthu,");
//                    sbsql.Append("            :ten_cqthu,");
//                    sbsql.Append("            :so_tk,");
//                    sbsql.Append("            :ngay_tk,");
//                    sbsql.Append("            :lhxnk,");
//                    sbsql.Append("            :vt_lhxnk,");
//                    sbsql.Append("            :ten_lhxnk,");
//                    sbsql.Append("            :ttien,");
//                    sbsql.Append("            :ttien_nt,");
//                    sbsql.Append("            :trang_thai,");
//                    sbsql.Append("            :tk_ns,");
//                    sbsql.Append("            :ten_tk_ns,");
//                    sbsql.Append("            :ma_nkt,");
//                    sbsql.Append("            :so_bl,");
//                    sbsql.Append("            :ngay_bl_batdau,");
//                    sbsql.Append("            :ngay_bl_ketthuc,");
//                    sbsql.Append("            :songay_bl,");
//                    sbsql.Append("            :kieu_bl,");
//                    sbsql.Append("            :dien_giai,");
//                    sbsql.Append("            :request_id,");
//                    sbsql.Append("            :accept_yn,");
//                    sbsql.Append("            :parent_transaction_id,");
//                    sbsql.Append("            :ma_hq_ph,");
//                    sbsql.Append("            :kq_dc,");
//                    sbsql.Append("            :ma_nv,");
//                    sbsql.Append("            :SO_TN_CT,");
//                    sbsql.Append("            :NGAY_TN_CT)");

//                    p = new IDbDataParameter[41];

//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "ID";
//                    p[0].DbType = DbType.Int64;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = DBNull.Value;
//                    p[0].Value = iId;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TRANSACTION_ID";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = DBNull.Value;
//                    if (itdata.Transaction_ID != null)
//                        p[1].Value = itdata.Transaction_ID.ToString().Trim();

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "TRANSACTION_TYPE";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strTransactionType;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "SHKB";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;
//                    if (itdata.Ma_KB != null)
//                        p[3].Value = itdata.Ma_KB;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "TEN_KB";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;
//                    if (itdata.Ten_KB != null)
//                        p[4].Value = itdata.Ten_KB;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "NGAY_KB";
//                    p[5].DbType = DbType.Int64;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = DBNull.Value;
//                    if (itdata.Ngay_BC != null)
//                        p[5].Value = Int64.Parse((DateTime.ParseExact(itdata.Ngay_BC, "yyyy-MM-dd", null)).ToString("yyyyMMdd"));

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "SO_BT";
//                    p[6].DbType = DbType.Int64;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = DBNull.Value;
//                    if (itdata.TTButToan != null)
//                        p[6].Value = Int64.Parse(itdata.TTButToan);//Hoi lai

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "KYHIEU_CT";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = DBNull.Value;
//                    if (itdata.KyHieu_CT != null)
//                        p[7].Value = itdata.KyHieu_CT;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "SO_CT";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = DBNull.Value;
//                    if (itdata.So_CT != null)
//                        p[8].Value = itdata.So_CT;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "NGAY_CT";
//                    p[9].DbType = DbType.Int64;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;
//                    if (itdata.Ngay_CT != null)
//                        p[9].Value = Int64.Parse((DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null)).ToString("yyyyMMdd"));

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "NGAY_BN";
//                    p[10].DbType = DbType.DateTime;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = pv_NgayDC;
//                    if (itdata.Ngay_BN != null)
//                        p[10].Value = DateTime.ParseExact(itdata.Ngay_BN, "yyyy-MM-dd", null);

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_BC";
//                    p[11].DbType = DbType.DateTime;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = DBNull.Value;
//                    if (itdata.Ngay_BC != null)
//                        p[11].Value = DateTime.ParseExact(itdata.Ngay_BC, "yyyy-MM-dd", null);

//                    p[12] = new OracleParameter();
//                    p[12].ParameterName = "MA_NNTHUE";
//                    p[12].DbType = DbType.String;
//                    p[12].Direction = ParameterDirection.Input;
//                    p[12].Value = DBNull.Value;
//                    if (itdata.Ma_DV != null)
//                        p[12].Value = itdata.Ma_DV;

//                    p[13] = new OracleParameter();
//                    p[13].ParameterName = "TEN_NNTHUE";
//                    p[13].DbType = DbType.String;
//                    p[13].Direction = ParameterDirection.Input;
//                    p[13].Value = DBNull.Value;
//                    if (itdata.Ten_DV != null)
//                        p[13].Value = itdata.Ten_DV;

//                    p[14] = new OracleParameter();
//                    p[14].ParameterName = "MA_CQTHU";
//                    p[14].DbType = DbType.String;
//                    p[14].Direction = ParameterDirection.Input;
//                    p[14].Value = DBNull.Value;
//                    if (itdata.Ma_HQ_CQT != null)
//                        p[14].Value = itdata.Ma_HQ_CQT.Trim();

//                    p[15] = new OracleParameter();
//                    p[15].ParameterName = "TEN_CQTHU";
//                    p[15].DbType = DbType.String;
//                    p[15].Direction = ParameterDirection.Input;
//                    p[15].Value = DBNull.Value;

//                    p[16] = new OracleParameter();
//                    p[16].ParameterName = "SO_TK";
//                    p[16].DbType = DbType.String;
//                    p[16].Direction = ParameterDirection.Input;
//                    p[16].Value = DBNull.Value;
//                    if (itdata.SoTK != null)
//                        p[16].Value = itdata.SoTK;

//                    p[17] = new OracleParameter();
//                    p[17].ParameterName = "NGAY_TK";
//                    p[17].DbType = DbType.DateTime;
//                    p[17].Direction = ParameterDirection.Input;
//                    p[17].Value = DBNull.Value;
//                    if (dsCtu.Tables[0].Rows.Count > 0)
//                        if (!String.Empty.Equals(dsCtu.Tables[0].Rows[0]["NGAY_TK"].ToString()))
//                            p[17].Value = DateTime.ParseExact(dsCtu.Tables[0].Rows[0]["NGAY_TK"].ToString(), "yyyy-MM-dd", null);


//                    p[18] = new OracleParameter();
//                    p[18].ParameterName = "LHXNK";
//                    p[18].DbType = DbType.String;
//                    p[18].Direction = ParameterDirection.Input;
//                    p[18].Value = DBNull.Value;
//                    if (itdata.Ma_LH != null)
//                        p[18].Value = itdata.Ma_LH;

//                    p[19] = new OracleParameter();
//                    p[19].ParameterName = "VT_LHXNK";//???
//                    p[19].DbType = DbType.String;
//                    p[19].Direction = ParameterDirection.Input;
//                    p[19].Value = DBNull.Value;


//                    p[20] = new OracleParameter();
//                    p[20].ParameterName = "TEN_LHXNK";
//                    p[20].DbType = DbType.String;
//                    p[20].Direction = ParameterDirection.Input;
//                    p[20].Value = DBNull.Value;

//                    p[21] = new OracleParameter();
//                    p[21].ParameterName = "TTIEN";
//                    p[21].DbType = DbType.Int64;
//                    p[21].Direction = ParameterDirection.Input;
//                    p[21].Value = DBNull.Value;
//                    Int64 iTongDuNo = 0;
//                    if (itdata.DuNo_TO != null)
//                    {
//                        if (!String.Empty.Equals(itdata.DuNo_TO.Trim()))
//                        {
//                            iTongDuNo += Int64.Parse(itdata.DuNo_TO.Trim());
//                        }
//                    }
//                    p[21].Value = iTongDuNo;

//                    p[22] = new OracleParameter();
//                    p[22].ParameterName = "TTIEN_NT";
//                    p[22].DbType = DbType.Int64;
//                    p[22].Direction = ParameterDirection.Input;
//                    p[22].Value = DBNull.Value;


//                    p[23] = new OracleParameter();
//                    p[23].ParameterName = "TRANG_THAI";//???
//                    p[23].DbType = DbType.String;
//                    p[23].Direction = ParameterDirection.Input;
//                    p[23].Value = DBNull.Value;

//                    p[24] = new OracleParameter();
//                    p[24].ParameterName = "TK_NS";//???
//                    p[24].DbType = DbType.String;
//                    p[24].Direction = ParameterDirection.Input;
//                    p[24].Value = DBNull.Value;
//                    if (dsCtu.Tables[0].Rows.Count > 0)
//                        if (dsCtu.Tables[0].Rows[0]["DVSDNS"] != null)
//                            p[24].Value = dsCtu.Tables[0].Rows[0]["DVSDNS"];

//                    p[25] = new OracleParameter();
//                    p[25].ParameterName = "TEN_TK_NS";
//                    p[25].DbType = DbType.String;
//                    p[25].Direction = ParameterDirection.Input;
//                    p[25].Value = DBNull.Value;
//                    if (dsCtu.Tables[0].Rows.Count > 0)
//                        if (dsCtu.Tables[0].Rows[0]["TEN_DVSDNS"] != null)
//                            p[25].Value = dsCtu.Tables[0].Rows[0]["TEN_DVSDNS"];

//                    p[26] = new OracleParameter();
//                    p[26].ParameterName = "MA_NKT";
//                    p[26].DbType = DbType.String;
//                    p[26].Direction = ParameterDirection.Input;
//                    p[26].Value = DBNull.Value;
//                    if (itdata.Ma_NTK != null)
//                        p[26].Value = itdata.Ma_NTK;

//                    p[27] = new OracleParameter();
//                    p[27].ParameterName = "SO_BL";//???
//                    p[27].DbType = DbType.String;
//                    p[27].Direction = ParameterDirection.Input;
//                    p[27].Value = DBNull.Value;


//                    p[28] = new OracleParameter();
//                    p[28].ParameterName = "NGAY_BL_BATDAU";//???
//                    p[28].DbType = DbType.DateTime;
//                    p[28].Direction = ParameterDirection.Input;
//                    p[28].Value = DBNull.Value;

//                    p[29] = new OracleParameter();
//                    p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//                    p[29].DbType = DbType.DateTime;
//                    p[29].Direction = ParameterDirection.Input;
//                    p[29].Value = DBNull.Value;

//                    p[30] = new OracleParameter();
//                    p[30].ParameterName = "SONGAY_BL";//???
//                    p[30].DbType = DbType.Int64;
//                    p[30].Direction = ParameterDirection.Input;
//                    p[30].Value = DBNull.Value;


//                    p[31] = new OracleParameter();
//                    p[31].ParameterName = "KIEU_BL";//???
//                    p[31].DbType = DbType.String;
//                    p[31].Direction = ParameterDirection.Input;
//                    p[31].Value = DBNull.Value;


//                    p[32] = new OracleParameter();
//                    p[32].ParameterName = "DIEN_GIAI";
//                    p[32].DbType = DbType.String;
//                    p[32].Direction = ParameterDirection.Input;
//                    p[32].Value = DBNull.Value;
//                    if (itdata.DienGiai != null)
//                        p[32].Value = itdata.DienGiai;

//                    p[33] = new OracleParameter();
//                    p[33].ParameterName = "REQUEST_ID";//???
//                    p[33].DbType = DbType.String;
//                    p[33].Direction = ParameterDirection.Input;
//                    p[33].Value = DBNull.Value;

//                    p[34] = new OracleParameter();
//                    p[34].ParameterName = "ACCEPT_YN";//???
//                    p[34].DbType = DbType.String;
//                    p[34].Direction = ParameterDirection.Input;
//                    p[34].Value = DBNull.Value;

//                    p[35] = new OracleParameter();
//                    p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//                    p[35].DbType = DbType.String;
//                    p[35].Direction = ParameterDirection.Input;
//                    p[35].Value = DBNull.Value;

//                    p[36] = new OracleParameter();
//                    p[36].ParameterName = "MA_HQ_PH";
//                    p[36].DbType = DbType.String;
//                    p[36].Direction = ParameterDirection.Input;
//                    p[36].Value = DBNull.Value;
//                    if (itdata.Ma_HQ_PH != null)
//                        p[36].Value = itdata.Ma_HQ_PH;

//                    p[37] = new OracleParameter();
//                    p[37].ParameterName = "KQ_DC";
//                    p[37].DbType = DbType.String;
//                    p[37].Direction = ParameterDirection.Input;
//                    p[37].Value = DBNull.Value;
//                    if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//                    {
//                        p[37].Value = "OK";
//                    }
//                    else
//                    {
//                        p[37].Value = itdata.KQ_DC;
//                    }

//                    p[38] = new OracleParameter();
//                    p[38].ParameterName = "MA_NV";
//                    p[38].DbType = DbType.Int64;
//                    p[38].Direction = ParameterDirection.Input;
//                    p[38].Value = DBNull.Value;
//                    if (dsCtu.Tables[0].Rows.Count > 0)
//                        if (dsCtu.Tables[0].Rows[0]["MA_NV"] != null)
//                            p[38].Value = Int64.Parse(dsCtu.Tables[0].Rows[0]["MA_NV"].ToString());

//                    p[39] = new OracleParameter();
//                    p[39].ParameterName = "SO_TN_CT";
//                    p[39].DbType = DbType.String;
//                    p[39].Direction = ParameterDirection.Input;
//                    p[39].Value = DBNull.Value;
//                    if (itdata.So_TN_CT != null)
//                        p[39].Value = itdata.So_TN_CT;

//                    p[40] = new OracleParameter();
//                    p[40].ParameterName = "NGAY_TN_CT";
//                    p[40].DbType = DbType.String;
//                    p[40].Direction = ParameterDirection.Input;
//                    p[40].Value = DBNull.Value;
//                    if (itdata.Ngay_TN_CT != null)
//                        p[40].Value = itdata.Ngay_TN_CT;
//                    ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);

//                    //insert tcs_dchieu_hqnh_dtl
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_XK, itdata.TieuMuc_XK, itdata.DuNo_XK, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_NK, itdata.TieuMuc_NK, itdata.DuNo_NK, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_VA, itdata.TieuMuc_VA, itdata.DuNo_VA, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_TD, itdata.TieuMuc_TD, itdata.DuNo_TD, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_TV, itdata.TieuMuc_TV, itdata.DuNo_TV, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_MT, itdata.TieuMuc_MT, itdata.DuNo_MT, transCT);
//                    DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_KH, itdata.TieuMuc_KH, itdata.DuNo_KH, transCT);

//                }
//                transCT.Commit();


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch thành công : Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("insert vao CSDL\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch thành công : Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("insert vao CSDL\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }


//        }

//        #endregion "Truy vấn kết quả đối chiếu dữ liệu giao dịch thành công M43 - M45"
//        #region "Truy vấn kết quả đối chiếu cho nhiêu tờ khai bằng VNĐ M48"
//        public static void getMSG48(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                string strLoai_TT_DC = "M26";
//                MSG43 msg43 = new MSG43();
//                msg43.Data = new CustomsV3.MSG.MSG43.Data();
//                msg43.Header = new CustomsV3.MSG.MSG43.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg43.Header.Message_Version = strMessage_Version;
//                msg43.Header.Sender_Code = strSender_Code;
//                msg43.Header.Sender_Name = strSender_Name;
//                msg43.Header.Transaction_Type = "43";
//                msg43.Header.Transaction_Name = "Thông điệp truy vấn kết quả đối chiếu dữ liệu giao dịch chứng từ";
//                msg43.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg43.Header.Transaction_ID = Guid.NewGuid().ToString();
//                String strReqId = getReqId(pv_NgayDC, "MSG47", strLoai_CT);

//                msg43.Header.Request_ID = strReqId;

//                //Gan du lieu Data
//                msg43.Data.Ma_NH_DC = strSender_Code;
//                msg43.Data.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");
//                msg43.Data.Loai_TT_DC = strLoai_TT_DC;
//                string tran_id = msg43.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "msg48", pv_NgayDC, strLoai_CT);
//                //Chuyen thanh XML
//                string strXMLOut = msg43.MSG43toXML(msg43);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("msg48", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG48 msg48 = new MSG48();
//                MSG48 objTemp = msg48.MSGToObject(strXMLIn);

//                pv_ErrorMes = objTemp.Error.Error_Message;
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                //Verify sign                
//                //if (verifySignMsg(objTemp, "msg48"))
//                //{
//                if ("0".Equals(pv_ErrorNum))
//                {
//                    //Cap nhat CSDL
//                  StoreDocSigned(strXMLIn,"m48.xml");
//                }
//                //}
//                //else
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //}
//                UpdateDCInfo(msg43.Header.Transaction_ID, "msg48", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }


//            }

//        }
//        private static void StoreDocSigned(string strXML, string strTenfile)
//                {
//                    XmlDocument xmlDoc = new XmlDocument();
//                    xmlDoc.LoadXml(strXML);
//                    XmlWriterSettings xwsSettings = new XmlWriterSettings();
//                    XmlWriter xwWriter = XmlWriter.Create(@"C:/" +strTenfile, xwsSettings);
//                    try
//                    {
//                        xmlDoc.WriteTo(xwWriter);
//                    }
//                    catch (Exception e)
//                    {
//                        //Khong transform duoc
//                        throw e;
//                    }
//                    finally
//                    {
//                        xwWriter.Flush();
//                        xwWriter.Close();
//                    }
//                }
               
//        //public static void MSG48ToDB(MSG48 msg48, DateTime pv_NgayDC)
//        //{
//        //    OracleConnection conn = GetConnection();
//        //    IDbTransaction transCT = null;
//        //    string strTransactionType = "48";
//        //    try
//        //    {
//        //        transCT = conn.BeginTransaction();
//        //        //Xoa du lieu dc cu
//        //        xoaDuLieuDC(strTransactionType, pv_NgayDC, transCT);

//        //        foreach (CustomsV3.MSG.MSG48.ItemData itdata in msg48.Data.Item)
//        //        {
//        //            Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//        //            StringBuilder sbsql = null;
//        //            IDbDataParameter[] p = null;

//        //            DataSet dsCtu = null;
//        //            //Lay thong tin trong tcs_ctu_hdr                    
//        //            sbsql = new StringBuilder();
//        //            sbsql.Append("SELECT ");
//        //            sbsql.Append("to_char(ctu.ngay_tk,'RRRR-MM-DD') ngay_tk ");
//        //            sbsql.Append(", ctu.ttien ");
//        //            sbsql.Append(", ctu.ttien_nt ");
//        //            sbsql.Append(", ctu.dvsdns ");
//        //            sbsql.Append(", ctu.ma_nv ");
//        //            sbsql.Append(", ctu.ten_dvsdns ");
//        //            sbsql.Append("FROM   tcs_ctu_hdr ctu ");
//        //            sbsql.Append("WHERE TO_NUMBER(ctu.so_ct) = :so_ct ");

//        //            p = new IDbDataParameter[1];
//        //            p[0] = new OracleParameter();
//        //            p[0].ParameterName = "SO_CT";
//        //            p[0].DbType = DbType.Int64;
//        //            p[0].Direction = ParameterDirection.Input;
//        //            p[0].Value = Int64.Parse(itdata.So_CT);

//        //            dsCtu = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//        //            //Insert thong tin header     ds.Tables[0].Rows[0][""];
//        //            sbsql = new StringBuilder();
//        //            sbsql.Append("INSERT INTO tcs_dchieu_hqnh_hdr (id,");
//        //            sbsql.Append("                                 transaction_id,");
//        //            sbsql.Append("                                 transaction_type,");
//        //            sbsql.Append("                                 shkb,");
//        //            sbsql.Append("                                 ten_kb,");
//        //            sbsql.Append("                                 ngay_kb,");
//        //            sbsql.Append("                                 so_bt,");
//        //            sbsql.Append("                                 kyhieu_ct,");
//        //            sbsql.Append("                                 so_ct,");
//        //            sbsql.Append("                                 ngay_ct,");
//        //            sbsql.Append("                                 ngay_bn,");
//        //            sbsql.Append("                                 ngay_bc,");
//        //            sbsql.Append("                                 ma_nnthue,");
//        //            sbsql.Append("                                 ten_nnthue,");
//        //            sbsql.Append("                                 ma_cqthu,");
//        //            sbsql.Append("                                 ten_cqthu,");
//        //            sbsql.Append("                                 so_tk,");
//        //            sbsql.Append("                                 ngay_tk,");
//        //            sbsql.Append("                                 lhxnk,");
//        //            sbsql.Append("                                 vt_lhxnk,");
//        //            sbsql.Append("                                 ten_lhxnk,");
//        //            sbsql.Append("                                 ttien,");
//        //            sbsql.Append("                                 ttien_nt,");
//        //            sbsql.Append("                                 trang_thai,");
//        //            sbsql.Append("                                 tk_ns,");
//        //            sbsql.Append("                                 ten_tk_ns,");
//        //            sbsql.Append("                                 ma_nkt,");
//        //            sbsql.Append("                                 so_bl,");
//        //            sbsql.Append("                                 ngay_bl_batdau,");
//        //            sbsql.Append("                                 ngay_bl_ketthuc,");
//        //            sbsql.Append("                                 songay_bl,");
//        //            sbsql.Append("                                 kieu_bl,");
//        //            sbsql.Append("                                 dien_giai,");
//        //            sbsql.Append("                                 request_id,");
//        //            sbsql.Append("                                 accept_yn,");
//        //            sbsql.Append("                                 parent_transaction_id,");
//        //            sbsql.Append("                                 ma_hq_ph,");
//        //            sbsql.Append("                                 kq_dc, ");
//        //            sbsql.Append("                                 ma_nv, ");
//        //            sbsql.Append("                                 SO_TN_CT, ");
//        //            sbsql.Append("                                NGAY_TN_CT)");
//        //            sbsql.Append("  VALUES   (:id,");
//        //            sbsql.Append("            :transaction_id,");
//        //            sbsql.Append("            :transaction_type,");
//        //            sbsql.Append("            :shkb,");
//        //            sbsql.Append("            :ten_kb,");
//        //            sbsql.Append("            :ngay_kb,");
//        //            sbsql.Append("            :so_bt,");
//        //            sbsql.Append("            :kyhieu_ct,");
//        //            sbsql.Append("            :so_ct,");
//        //            sbsql.Append("            :ngay_ct,");
//        //            sbsql.Append("            :ngay_bn,");
//        //            sbsql.Append("            :ngay_bc,");
//        //            sbsql.Append("            :ma_nnthue,");
//        //            sbsql.Append("            :ten_nnthue,");
//        //            sbsql.Append("            :ma_cqthu,");
//        //            sbsql.Append("            :ten_cqthu,");
//        //            sbsql.Append("            :so_tk,");
//        //            sbsql.Append("            :ngay_tk,");
//        //            sbsql.Append("            :lhxnk,");
//        //            sbsql.Append("            :vt_lhxnk,");
//        //            sbsql.Append("            :ten_lhxnk,");
//        //            sbsql.Append("            :ttien,");
//        //            sbsql.Append("            :ttien_nt,");
//        //            sbsql.Append("            :trang_thai,");
//        //            sbsql.Append("            :tk_ns,");
//        //            sbsql.Append("            :ten_tk_ns,");
//        //            sbsql.Append("            :ma_nkt,");
//        //            sbsql.Append("            :so_bl,");
//        //            sbsql.Append("            :ngay_bl_batdau,");
//        //            sbsql.Append("            :ngay_bl_ketthuc,");
//        //            sbsql.Append("            :songay_bl,");
//        //            sbsql.Append("            :kieu_bl,");
//        //            sbsql.Append("            :dien_giai,");
//        //            sbsql.Append("            :request_id,");
//        //            sbsql.Append("            :accept_yn,");
//        //            sbsql.Append("            :parent_transaction_id,");
//        //            sbsql.Append("            :ma_hq_ph,");
//        //            sbsql.Append("            :kq_dc,");
//        //            sbsql.Append("            :ma_nv,");
//        //            sbsql.Append("            :SO_TN_CT,");
//        //            sbsql.Append("            :NGAY_TN_CT)");

//        //            p = new IDbDataParameter[41];

//        //            p[0] = new OracleParameter();
//        //            p[0].ParameterName = "ID";
//        //            p[0].DbType = DbType.Int64;
//        //            p[0].Direction = ParameterDirection.Input;
//        //            p[0].Value = DBNull.Value;
//        //            p[0].Value = iId;

//        //            p[1] = new OracleParameter();
//        //            p[1].ParameterName = "TRANSACTION_ID";
//        //            p[1].DbType = DbType.String;
//        //            p[1].Direction = ParameterDirection.Input;
//        //            p[1].Value = DBNull.Value;
//        //            if (itdata.Transaction_ID != null)
//        //                p[1].Value = itdata.Transaction_ID.ToString().Trim();

//        //            p[2] = new OracleParameter();
//        //            p[2].ParameterName = "TRANSACTION_TYPE";
//        //            p[2].DbType = DbType.String;
//        //            p[2].Direction = ParameterDirection.Input;
//        //            p[2].Value = strTransactionType;

//        //            p[3] = new OracleParameter();
//        //            p[3].ParameterName = "SHKB";
//        //            p[3].DbType = DbType.String;
//        //            p[3].Direction = ParameterDirection.Input;
//        //            p[3].Value = DBNull.Value;
//        //            if (itdata.Ma_KB != null)
//        //                p[3].Value = itdata.Ma_KB;

//        //            p[4] = new OracleParameter();
//        //            p[4].ParameterName = "TEN_KB";
//        //            p[4].DbType = DbType.String;
//        //            p[4].Direction = ParameterDirection.Input;
//        //            p[4].Value = DBNull.Value;
//        //            if (itdata.Ten_KB != null)
//        //                p[4].Value = itdata.Ten_KB;

//        //            p[5] = new OracleParameter();
//        //            p[5].ParameterName = "NGAY_KB";
//        //            p[5].DbType = DbType.Int64;
//        //            p[5].Direction = ParameterDirection.Input;
//        //            p[5].Value = DBNull.Value;
//        //            if (itdata.Ngay_BC != null)
//        //                p[5].Value = Int64.Parse((DateTime.ParseExact(itdata.Ngay_BC, "yyyy-MM-dd", null)).ToString("yyyyMMdd"));

//        //            p[6] = new OracleParameter();
//        //            p[6].ParameterName = "SO_BT";
//        //            p[6].DbType = DbType.Int64;
//        //            p[6].Direction = ParameterDirection.Input;
//        //            p[6].Value = DBNull.Value;
//        //            if (itdata.TTButToan != null)
//        //                p[6].Value = Int64.Parse(itdata.TTButToan);//Hoi lai

//        //            p[7] = new OracleParameter();
//        //            p[7].ParameterName = "KYHIEU_CT";
//        //            p[7].DbType = DbType.String;
//        //            p[7].Direction = ParameterDirection.Input;
//        //            p[7].Value = DBNull.Value;
//        //            if (itdata.KyHieu_CT != null)
//        //                p[7].Value = itdata.KyHieu_CT;

//        //            p[8] = new OracleParameter();
//        //            p[8].ParameterName = "SO_CT";
//        //            p[8].DbType = DbType.String;
//        //            p[8].Direction = ParameterDirection.Input;
//        //            p[8].Value = DBNull.Value;
//        //            if (itdata.So_CT != null)
//        //                p[8].Value = itdata.So_CT;

//        //            p[9] = new OracleParameter();
//        //            p[9].ParameterName = "NGAY_CT";
//        //            p[9].DbType = DbType.Int64;
//        //            p[9].Direction = ParameterDirection.Input;
//        //            p[9].Value = DBNull.Value;
//        //            if (itdata.Ngay_CT != null)
//        //                p[9].Value = Int64.Parse((DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null)).ToString("yyyyMMdd"));

//        //            p[10] = new OracleParameter();
//        //            p[10].ParameterName = "NGAY_BN";
//        //            p[10].DbType = DbType.DateTime;
//        //            p[10].Direction = ParameterDirection.Input;
//        //            p[10].Value = pv_NgayDC;
//        //            if (itdata.Ngay_BN != null)
//        //                p[10].Value = DateTime.ParseExact(itdata.Ngay_BN, "yyyy-MM-dd", null);

//        //            p[11] = new OracleParameter();
//        //            p[11].ParameterName = "NGAY_BC";
//        //            p[11].DbType = DbType.DateTime;
//        //            p[11].Direction = ParameterDirection.Input;
//        //            p[11].Value = DBNull.Value;
//        //            if (itdata.Ngay_BC != null)
//        //                p[11].Value = DateTime.ParseExact(itdata.Ngay_BC, "yyyy-MM-dd", null);

//        //            p[12] = new OracleParameter();
//        //            p[12].ParameterName = "MA_NNTHUE";
//        //            p[12].DbType = DbType.String;
//        //            p[12].Direction = ParameterDirection.Input;
//        //            p[12].Value = DBNull.Value;
//        //            if (itdata.Ma_DV != null)
//        //                p[12].Value = itdata.Ma_DV;

//        //            p[13] = new OracleParameter();
//        //            p[13].ParameterName = "TEN_NNTHUE";
//        //            p[13].DbType = DbType.String;
//        //            p[13].Direction = ParameterDirection.Input;
//        //            p[13].Value = DBNull.Value;
//        //            if (itdata.Ten_DV != null)
//        //                p[13].Value = itdata.Ten_DV;

//        //            p[14] = new OracleParameter();
//        //            p[14].ParameterName = "MA_CQTHU";
//        //            p[14].DbType = DbType.String;
//        //            p[14].Direction = ParameterDirection.Input;
//        //            p[14].Value = DBNull.Value;
//        //            if (itdata.Ma_HQ_CQT != null)
//        //                p[14].Value = itdata.Ma_HQ_CQT.Trim();

//        //            p[15] = new OracleParameter();
//        //            p[15].ParameterName = "TEN_CQTHU";
//        //            p[15].DbType = DbType.String;
//        //            p[15].Direction = ParameterDirection.Input;
//        //            p[15].Value = DBNull.Value;

//        //            p[16] = new OracleParameter();
//        //            p[16].ParameterName = "SO_TK";
//        //            p[16].DbType = DbType.String;
//        //            p[16].Direction = ParameterDirection.Input;
//        //            p[16].Value = DBNull.Value;
//        //            if (itdata.SoTK != null)
//        //                p[16].Value = itdata.SoTK;

//        //            p[17] = new OracleParameter();
//        //            p[17].ParameterName = "NGAY_TK";
//        //            p[17].DbType = DbType.DateTime;
//        //            p[17].Direction = ParameterDirection.Input;
//        //            p[17].Value = DBNull.Value;
//        //            if (dsCtu.Tables[0].Rows.Count > 0)
//        //                if (!String.Empty.Equals(dsCtu.Tables[0].Rows[0]["NGAY_TK"].ToString()))
//        //                    p[17].Value = DateTime.ParseExact(dsCtu.Tables[0].Rows[0]["NGAY_TK"].ToString(), "yyyy-MM-dd", null);


//        //            p[18] = new OracleParameter();
//        //            p[18].ParameterName = "LHXNK";
//        //            p[18].DbType = DbType.String;
//        //            p[18].Direction = ParameterDirection.Input;
//        //            p[18].Value = DBNull.Value;
//        //            if (itdata.Ma_LH != null)
//        //                p[18].Value = itdata.Ma_LH;

//        //            p[19] = new OracleParameter();
//        //            p[19].ParameterName = "VT_LHXNK";//???
//        //            p[19].DbType = DbType.String;
//        //            p[19].Direction = ParameterDirection.Input;
//        //            p[19].Value = DBNull.Value;


//        //            p[20] = new OracleParameter();
//        //            p[20].ParameterName = "TEN_LHXNK";
//        //            p[20].DbType = DbType.String;
//        //            p[20].Direction = ParameterDirection.Input;
//        //            p[20].Value = DBNull.Value;

//        //            p[21] = new OracleParameter();
//        //            p[21].ParameterName = "TTIEN";
//        //            p[21].DbType = DbType.Int64;
//        //            p[21].Direction = ParameterDirection.Input;
//        //            p[21].Value = DBNull.Value;
//        //            Int64 iTongDuNo = 0;
//        //            if (itdata.DuNo_TO != null)
//        //            {
//        //                if (!String.Empty.Equals(itdata.DuNo_TO.Trim()))
//        //                {
//        //                    iTongDuNo += Int64.Parse(itdata.DuNo_TO.Trim());
//        //                }
//        //            }
//        //            p[21].Value = iTongDuNo;

//        //            p[22] = new OracleParameter();
//        //            p[22].ParameterName = "TTIEN_NT";
//        //            p[22].DbType = DbType.Int64;
//        //            p[22].Direction = ParameterDirection.Input;
//        //            p[22].Value = DBNull.Value;


//        //            p[23] = new OracleParameter();
//        //            p[23].ParameterName = "TRANG_THAI";//???
//        //            p[23].DbType = DbType.String;
//        //            p[23].Direction = ParameterDirection.Input;
//        //            p[23].Value = DBNull.Value;

//        //            p[24] = new OracleParameter();
//        //            p[24].ParameterName = "TK_NS";//???
//        //            p[24].DbType = DbType.String;
//        //            p[24].Direction = ParameterDirection.Input;
//        //            p[24].Value = DBNull.Value;
//        //            if (dsCtu.Tables[0].Rows.Count > 0)
//        //                if (dsCtu.Tables[0].Rows[0]["DVSDNS"] != null)
//        //                    p[24].Value = dsCtu.Tables[0].Rows[0]["DVSDNS"];

//        //            p[25] = new OracleParameter();
//        //            p[25].ParameterName = "TEN_TK_NS";
//        //            p[25].DbType = DbType.String;
//        //            p[25].Direction = ParameterDirection.Input;
//        //            p[25].Value = DBNull.Value;
//        //            if (dsCtu.Tables[0].Rows.Count > 0)
//        //                if (dsCtu.Tables[0].Rows[0]["TEN_DVSDNS"] != null)
//        //                    p[25].Value = dsCtu.Tables[0].Rows[0]["TEN_DVSDNS"];

//        //            p[26] = new OracleParameter();
//        //            p[26].ParameterName = "MA_NKT";
//        //            p[26].DbType = DbType.String;
//        //            p[26].Direction = ParameterDirection.Input;
//        //            p[26].Value = DBNull.Value;
//        //            if (itdata.Ma_NTK != null)
//        //                p[26].Value = itdata.Ma_NTK;

//        //            p[27] = new OracleParameter();
//        //            p[27].ParameterName = "SO_BL";//???
//        //            p[27].DbType = DbType.String;
//        //            p[27].Direction = ParameterDirection.Input;
//        //            p[27].Value = DBNull.Value;


//        //            p[28] = new OracleParameter();
//        //            p[28].ParameterName = "NGAY_BL_BATDAU";//???
//        //            p[28].DbType = DbType.DateTime;
//        //            p[28].Direction = ParameterDirection.Input;
//        //            p[28].Value = DBNull.Value;

//        //            p[29] = new OracleParameter();
//        //            p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//        //            p[29].DbType = DbType.DateTime;
//        //            p[29].Direction = ParameterDirection.Input;
//        //            p[29].Value = DBNull.Value;

//        //            p[30] = new OracleParameter();
//        //            p[30].ParameterName = "SONGAY_BL";//???
//        //            p[30].DbType = DbType.Int64;
//        //            p[30].Direction = ParameterDirection.Input;
//        //            p[30].Value = DBNull.Value;


//        //            p[31] = new OracleParameter();
//        //            p[31].ParameterName = "KIEU_BL";//???
//        //            p[31].DbType = DbType.String;
//        //            p[31].Direction = ParameterDirection.Input;
//        //            p[31].Value = DBNull.Value;


//        //            p[32] = new OracleParameter();
//        //            p[32].ParameterName = "DIEN_GIAI";
//        //            p[32].DbType = DbType.String;
//        //            p[32].Direction = ParameterDirection.Input;
//        //            p[32].Value = DBNull.Value;
//        //            if (itdata.DienGiai != null)
//        //                p[32].Value = itdata.DienGiai;

//        //            p[33] = new OracleParameter();
//        //            p[33].ParameterName = "REQUEST_ID";//???
//        //            p[33].DbType = DbType.String;
//        //            p[33].Direction = ParameterDirection.Input;
//        //            p[33].Value = DBNull.Value;

//        //            p[34] = new OracleParameter();
//        //            p[34].ParameterName = "ACCEPT_YN";//???
//        //            p[34].DbType = DbType.String;
//        //            p[34].Direction = ParameterDirection.Input;
//        //            p[34].Value = DBNull.Value;

//        //            p[35] = new OracleParameter();
//        //            p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//        //            p[35].DbType = DbType.String;
//        //            p[35].Direction = ParameterDirection.Input;
//        //            p[35].Value = DBNull.Value;

//        //            p[36] = new OracleParameter();
//        //            p[36].ParameterName = "MA_HQ_PH";
//        //            p[36].DbType = DbType.String;
//        //            p[36].Direction = ParameterDirection.Input;
//        //            p[36].Value = DBNull.Value;
//        //            if (itdata.Ma_HQ_PH != null)
//        //                p[36].Value = itdata.Ma_HQ_PH;

//        //            p[37] = new OracleParameter();
//        //            p[37].ParameterName = "KQ_DC";
//        //            p[37].DbType = DbType.String;
//        //            p[37].Direction = ParameterDirection.Input;
//        //            p[37].Value = DBNull.Value;
//        //            if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//        //            {
//        //                p[37].Value = "OK";
//        //            }
//        //            else
//        //            {
//        //                p[37].Value = itdata.KQ_DC;
//        //            }

//        //            p[38] = new OracleParameter();
//        //            p[38].ParameterName = "MA_NV";
//        //            p[38].DbType = DbType.Int64;
//        //            p[38].Direction = ParameterDirection.Input;
//        //            p[38].Value = DBNull.Value;
//        //            if (dsCtu.Tables[0].Rows.Count > 0)
//        //                if (dsCtu.Tables[0].Rows[0]["MA_NV"] != null)
//        //                    p[38].Value = Int64.Parse(dsCtu.Tables[0].Rows[0]["MA_NV"].ToString());

//        //            p[39] = new OracleParameter();
//        //            p[39].ParameterName = "SO_TN_CT";
//        //            p[39].DbType = DbType.String;
//        //            p[39].Direction = ParameterDirection.Input;
//        //            p[39].Value = DBNull.Value;
//        //            if (itdata.So_TN_CT != null)
//        //                p[39].Value = itdata.So_TN_CT;

//        //            p[40] = new OracleParameter();
//        //            p[40].ParameterName = "NGAY_TN_CT";
//        //            p[40].DbType = DbType.String;
//        //            p[40].Direction = ParameterDirection.Input;
//        //            p[40].Value = DBNull.Value;
//        //            if (itdata.Ngay_TN_CT != null)
//        //                p[40].Value = itdata.Ngay_TN_CT;
//        //            ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);

//        //            //insert tcs_dchieu_hqnh_dtl
//        //            DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_XK, itdata.TieuMuc_XK, itdata.DuNo_XK, transCT);
//        //            DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_NK, itdata.TieuMuc_NK, itdata.DuNo_NK, transCT);
//        //            DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_VA, itdata.TieuMuc_VA, itdata.DuNo_VA, transCT);
//        //            DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_TD, itdata.TieuMuc_TD, itdata.DuNo_TD, transCT);
//        //            DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_TV, itdata.TieuMuc_TV, itdata.DuNo_TV, transCT);
//        //            DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_MT, itdata.TieuMuc_MT, itdata.DuNo_MT, transCT);
//        //            DTLToDB(iId, itdata.Ma_Chuong, itdata.Khoan_KH, itdata.TieuMuc_KH, itdata.DuNo_KH, transCT);

//        //        }
//        //        transCT.Commit();


//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        StringBuilder sbErrMsg = default(StringBuilder);
//        //        sbErrMsg = new StringBuilder();
//        //        sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch thành công : Ngay - ");
//        //        sbErrMsg.Append(pv_NgayDC.ToString());
//        //        sbErrMsg.Append("insert vao CSDL\n");
//        //        sbErrMsg.Append(ex.Message);
//        //        sbErrMsg.Append("\n");
//        //        sbErrMsg.Append(ex.StackTrace);

//        //        LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//        //        if ((ex.InnerException != null))
//        //        {
//        //            sbErrMsg = new StringBuilder();
//        //            sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch thành công : Ngay - ");
//        //            sbErrMsg.Append(pv_NgayDC.ToString());
//        //            sbErrMsg.Append("insert vao CSDL\n");
//        //            sbErrMsg.Append(ex.InnerException.Message);
//        //            sbErrMsg.Append("\n");
//        //            sbErrMsg.Append(ex.InnerException.StackTrace);

//        //            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//        //        }

//        //        throw ex;

//        //    }
//        //    finally
//        //    {
//        //        if ((transCT != null))
//        //        {
//        //            transCT.Dispose();
//        //        }
//        //        if ((conn != null) || conn.State != ConnectionState.Closed)
//        //        {
//        //            conn.Dispose();
//        //            conn.Close();
//        //        }
//        //    }


//        //}
//        #endregion"kết thúc nhận kết quả đối chiếu "
//        #region "Truy vấn kết quả đối chiếu cho nhiêu tờ khai bằng ngoại tệ M50"
//        public static void getMSG50(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                string strLoai_TT_DC = "M27";
//                MSG43 msg43 = new MSG43();
//                msg43.Data = new CustomsV3.MSG.MSG43.Data();
//                msg43.Header = new CustomsV3.MSG.MSG43.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg43.Header.Message_Version = strMessage_Version;
//                msg43.Header.Sender_Code = strSender_Code;
//                msg43.Header.Sender_Name = strSender_Name;
//                msg43.Header.Transaction_Type = "43";
//                msg43.Header.Transaction_Name = "Thông điệp truy vấn kết quả đối chiếu dữ liệu giao dịch chứng từ";
//                msg43.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg43.Header.Transaction_ID = Guid.NewGuid().ToString();
//                String strReqId = getReqId(pv_NgayDC, "MSG49", strLoai_CT);

//                msg43.Header.Request_ID = strReqId;

//                //Gan du lieu Data
//                msg43.Data.Ma_NH_DC = strSender_Code;
//                msg43.Data.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");
//                msg43.Data.Loai_TT_DC = strLoai_TT_DC;
//                string tran_id = msg43.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "msg50", pv_NgayDC, strLoai_CT);
//                //Chuyen thanh XML
//                string strXMLOut = msg43.MSG43toXML(msg43);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG50", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG50 msg50 = new MSG50();
//                MSG50 objTemp = msg50.MSGToObject(strXMLIn);

//                pv_ErrorMes = objTemp.Error.Error_Message;
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                //Verify sign                
//                //if (verifySignMsg(objTemp, "msg48"))
//                //{
//                if ("0".Equals(pv_ErrorNum))
//                {
//                    StoreDocSigned(strXMLIn, "m50.xml");
//                }
//                //}
//                //else
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //}
//                UpdateDCInfo(msg50.Header.Transaction_ID, "msg48", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }


//            }

//        }
//        #endregion"kết thúc nhận kết quả đối chiếu "
//        #region "đối chiếu giao dịch huỷ chứng từ (M51-52)"
//        public static void DOICHIEUToMSG51(DateTime pv_NgayDC, ref MSG51 msg51)
//        {
//            msg51.Data = new CustomsV3.MSG.MSG51.Data();
//            msg51.Data.tem.Ma_NH_DC = strSender_Code;
//            msg51.Data.tem.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");

//            StringBuilder sbsql = new StringBuilder();
//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT  hdr.id,");
//                sbsql.Append("        hdr.accept_yn,");
//                sbsql.Append("        hdr.transaction_id,");
//                sbsql.Append("        hdr.ma_nh_ph,");
//                sbsql.Append("        hdr.ten_nh_ph,");
//                sbsql.Append("        hdr.so_tn_ct,");
//                sbsql.Append("        hdr.ngay_tn_ct,");
//                sbsql.Append("        1 ttbuttoan");
//                sbsql.Append("        FROM   tcs_dchieu_nhhq_hdr hdr");
//                sbsql.Append("        WHERE   hdr.transaction_type = 'M51' " + strDKNGAYDC);

//                DateTime ngayDC1 = pv_NgayDC.AddDays(-1);

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAYDC1";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = ngayDC1.ToString("yyyy-MM-dd") + strGio_DC;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAYDC2";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_NgayDC.ToString("yyyy-MM-dd") + strGio_DC;

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);
//                //MANHNV-14/11/2012
//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                //-----------------
//                int i = 0;
//                int j = 0;
//                CustomsV3.MSG.MSG51.Accept_Transactions accept_Transactions = new CustomsV3.MSG.MSG51.Accept_Transactions();
//                CustomsV3.MSG.MSG51.Reject_Transactions reject_Transactions = new CustomsV3.MSG.MSG51.Reject_Transactions();
//                List<CustomsV3.MSG.MSG51.ItemData> Items_accep = new List<CustomsV3.MSG.MSG51.ItemData>();
//                List<CustomsV3.MSG.MSG51.ItemData> Items_reject = new List<CustomsV3.MSG.MSG51.ItemData>();
//                foreach (DataRow drhdr in ds.Tables[0].Rows)
//                {
//                    if ("Y".Equals(drhdr["ACCEPT_YN"].ToString()))
//                    {

//                        CustomsV3.MSG.MSG51.ItemData item = new CustomsV3.MSG.MSG51.ItemData();

//                        item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                        item.So_TN_CT = drhdr["so_tn_ct"].ToString();
//                        item.Ngay_TN_CT = drhdr["ngay_tn_ct"].ToString();

//                        Items_accep.Add(item);
//                    }
//                    else
//                    {
//                        CustomsV3.MSG.MSG51.ItemData item = new CustomsV3.MSG.MSG51.ItemData();

//                        item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                        item.So_TN_CT = drhdr["so_tn_ct"].ToString();
//                        item.Ngay_TN_CT = drhdr["ngay_tn_ct"].ToString();

//                        Items_reject.Add(item);
//                    }
//                }
//                accept_Transactions.Item = Items_accep;
//                reject_Transactions.Item = Items_reject;
//                msg51.Data.Accept_Transactions = accept_Transactions;
//                msg51.Data.Reject_Transactions = reject_Transactions;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUToMSG51): Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUToMSG51): Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }
//                throw ex;

//            }
//            finally
//            {
//            }
//        }
//        public static void getMSG52(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG51 msg51 = new MSG51();
//                msg51.Data = new CustomsV3.MSG.MSG51.Data();
//                msg51.Header = new CustomsV3.MSG.MSG51.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg51.Header.Message_Version = strMessage_Version;
//                msg51.Header.Sender_Code = strSender_Code;
//                msg51.Header.Sender_Name = strSender_Name;
//                msg51.Header.Transaction_Type = "51";
//                msg51.Header.Transaction_Name = "Thông điệp yêu cầu đối chiếu giao dịch huỷ chứng từ";
//                msg51.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg51.Header.Transaction_ID = Guid.NewGuid().ToString();
//                string tran_id = msg51.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "MSG51", pv_NgayDC, strLoai_CT);
//                //Gan du lieu Data
//                DOICHIEUToMSG51(pv_NgayDC, ref msg51);

//                //Chuyen thanh XML
//                string strXMLOut = msg51.MSG51toXML(msg51);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG51", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //              StringBuilder sb = new StringBuilder();
//                //                sb.Append(strXMLdefin1);
//                //            sb.Append("\r\n");
//                //          sb.Append(strXMLIn);
//                //        strXMLIn = sb.ToString();

//                MSG52 msg52 = new MSG52();
//                MSG52 objTemp = msg52.MSGToObject(strXMLIn);
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;
//                //Verify sign                
//                //if (!verifySignMsg(objTemp, "MSG52"))
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //}
//                UpdateDCInfo(tran_id, "MSG51", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                MSG51 msg51 = new MSG51();
//                //string tran_id = msg51.Header.Transaction_ID;
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 52: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);
//                pv_ErrorNum = "00000";
//                pv_ErrorMes = ex.Message.ToString();
//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 52: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                //pv_ErrorNum = "00000";
//                //pv_ErrorMes = sbErrMsg.ToString();
//                //UpdateDCInfo(tran_id, "MSG51", pv_ErrorNum, pv_ErrorMes);
//            }

//        }
//        #endregion "đối chiếu giao dịch huỷ chứng từ (M51-52)";

//        #region "Truy vấn kết quả đối chiếu dữ liệu giao dịch huy M53 - M54"

//        public static void getMSG54(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG53 msg53 = new MSG53();
//                msg53.Data = new CustomsV3.MSG.MSG53.Data();
//                msg53.Header = new CustomsV3.MSG.MSG53.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg53.Header.Message_Version = strMessage_Version;
//                msg53.Header.Sender_Code = strSender_Code;
//                msg53.Header.Sender_Name = strSender_Name;
//                msg53.Header.Transaction_Type = "53";
//                msg53.Header.Transaction_Name = "Thông điệp truy vấn kết quả đối chiếu dữ liệu giao dịch chứng từ";
//                msg53.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg53.Header.Transaction_ID = Guid.NewGuid().ToString();
//                String strReqId = getReqId(pv_NgayDC, "MSG51", strLoai_CT);

//                msg53.Header.Request_ID = strReqId;

//                //Gan du lieu Data
//                msg53.Data.Ma_NH_DC = strSender_Code;
//                msg53.Data.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");
//                string tran_id = msg53.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "MSG53", pv_NgayDC, strLoai_CT);
//                //Chuyen thanh XML
//                string strXMLOut = msg53.MSG53toXML(msg53);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG53", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                StringBuilder sb = new StringBuilder();
//                //                sb.Append(strXMLdefin1);
//                //              sb.Append("\r\n");
//                //            sb.Append(strXMLIn);
//                //          strXMLIn = sb.ToString();

//                MSG54 msg54 = new MSG54();
//                MSG54 objTemp = msg54.MSGToObject(strXMLIn);

//                pv_ErrorMes = objTemp.Error.Error_Message;
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                //Verify sign                
//                //if (verifySignMsg(objTemp, "MSG54"))
//                //{
//                if ("0".Equals(pv_ErrorNum))
//                {
//                    //Cap nhat CSDL
//                    MSG54ToDB(objTemp, pv_NgayDC);
//                }
//                //}
//                //else
//                //{
//                //    pv_ErrorNum = "00001";
//                //    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                //}
//                UpdateDCInfo(tran_id, "MSG53", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                MSG53 msg53 = new MSG53();
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 54: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 54: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = sbErrMsg.ToString();
//                UpdateDCInfo(msg53.Header.Transaction_ID, "MSG53", pv_ErrorNum, pv_ErrorMes);
//            }

//        }
//        public static TCS_DS_TOKHAI getTTinCtuHuy(String str_ngay_tn_ct, String str_so_tn_ct, String str_tran_type)
//        {
//            TCS_DS_TOKHAI _TCS_DS_TOKHAI = null;
//            StringBuilder sb = new StringBuilder();
//            sb.Append(" SELECT ttien, ma_cqthu from tcs_dchieu_nhhq_hdr ");
//            sb.Append(" where  NGAY_TN_CT = :ngay_tn_ct ");
//            sb.Append(" and  SO_TN_CT = :str_so_tn_ct ");
//            sb.Append(" and TRANSACTION_TYPE = :tran_type");

//            IDbDataParameter[] p = new IDbDataParameter[3];
//            p[0] = new OracleParameter();
//            p[0].ParameterName = "ngay_tn_ct";
//            p[0].DbType = DbType.String;
//            p[0].Direction = ParameterDirection.Input;
//            p[0].Value = DBNull.Value;
//            p[0].Value = str_ngay_tn_ct;

//            p[1] = new OracleParameter();
//            p[1].ParameterName = "str_so_tn_ct";
//            p[1].DbType = DbType.String;
//            p[1].Direction = ParameterDirection.Input;
//            p[1].Value = str_so_tn_ct;

//            p[2] = new OracleParameter();
//            p[2].ParameterName = "TRAN_TYPE";
//            p[2].DbType = DbType.String;
//            p[2].Direction = ParameterDirection.Input;
//            p[2].Value = str_tran_type;

//            DataSet dst = ExecuteReturnDataSet(sb.ToString(), CommandType.Text, p);
//            if (dst.Tables[0].Rows.Count <= 0)
//            {
//                return _TCS_DS_TOKHAI;
//            }
//            _TCS_DS_TOKHAI = new TCS_DS_TOKHAI();

//            _TCS_DS_TOKHAI.ma_cqthu = dst.Tables[0].Rows[0]["ma_cqthu"].ToString();
//            _TCS_DS_TOKHAI.tt_nop = dst.Tables[0].Rows[0]["ttien"].ToString();

//            return _TCS_DS_TOKHAI;
//        }
//        public static void MSG54ToDB(MSG54 msg54, DateTime pv_NgayDC)
//        {
//            OracleConnection conn = GetConnection();
//            IDbTransaction transCT = null;
//            StringBuilder sbsql = null;
//            string strTransactionType = "54";

//            try
//            {
//                TCS_DS_TOKHAI _ttin_huy = null;
//                transCT = conn.BeginTransaction();

//                //Xoa du lieu dc cu
//                xoaDuLieuDC(strTransactionType, pv_NgayDC, transCT);
//                //Insert thong tin header
//                sbsql = new StringBuilder();
//                sbsql.Append("INSERT INTO tcs_dchieu_hqnh_hdr (id,");
//                sbsql.Append("                                 transaction_id,");
//                sbsql.Append("                                 transaction_type,");
//                sbsql.Append("                                 shkb,");
//                sbsql.Append("                                 ten_kb,");
//                sbsql.Append("                                 ngay_kb,");
//                sbsql.Append("                                 so_bt,");
//                sbsql.Append("                                 kyhieu_ct,");
//                sbsql.Append("                                 so_ct,");
//                sbsql.Append("                                 ngay_ct,");
//                sbsql.Append("                                 ngay_bn,");
//                sbsql.Append("                                 ngay_bc,");
//                sbsql.Append("                                 ma_nnthue,");
//                sbsql.Append("                                 ten_nnthue,");
//                sbsql.Append("                                 ma_cqthu,");
//                sbsql.Append("                                 ten_cqthu,");
//                sbsql.Append("                                 so_tk,");
//                sbsql.Append("                                 ngay_tk,");
//                sbsql.Append("                                 lhxnk,");
//                sbsql.Append("                                 vt_lhxnk,");
//                sbsql.Append("                                 ten_lhxnk,");
//                sbsql.Append("                                 ttien,");
//                sbsql.Append("                                 ttien_nt,");
//                sbsql.Append("                                 trang_thai,");
//                sbsql.Append("                                 tk_ns,");
//                sbsql.Append("                                 ten_tk_ns,");
//                sbsql.Append("                                 ma_nkt,");
//                sbsql.Append("                                 so_bl,");
//                sbsql.Append("                                 ngay_bl_batdau,");
//                sbsql.Append("                                 ngay_bl_ketthuc,");
//                sbsql.Append("                                 songay_bl,");
//                sbsql.Append("                                 kieu_bl,");
//                sbsql.Append("                                 dien_giai,");
//                sbsql.Append("                                 request_id,");
//                sbsql.Append("                                 accept_yn,");
//                sbsql.Append("                                 parent_transaction_id,");
//                sbsql.Append("                                 ma_hq_ph,");
//                sbsql.Append("                                 SO_TN_CT, ");
//                sbsql.Append("                                 NGAY_TN_CT,");
//                sbsql.Append("                                 kq_dc)");
//                sbsql.Append("  VALUES   (:id,");
//                sbsql.Append("            :transaction_id,");
//                sbsql.Append("            :transaction_type,");
//                sbsql.Append("            :shkb,");
//                sbsql.Append("            :ten_kb,");
//                sbsql.Append("            :ngay_kb,");
//                sbsql.Append("            :so_bt,");
//                sbsql.Append("            :kyhieu_ct,");
//                sbsql.Append("            :so_ct,");
//                sbsql.Append("            :ngay_ct,");
//                sbsql.Append("            :ngay_bn,");
//                sbsql.Append("            :ngay_bc,");
//                sbsql.Append("            :ma_nnthue,");
//                sbsql.Append("            :ten_nnthue,");
//                sbsql.Append("            :ma_cqthu,");
//                sbsql.Append("            :ten_cqthu,");
//                sbsql.Append("            :so_tk,");
//                sbsql.Append("            :ngay_tk,");
//                sbsql.Append("            :lhxnk,");
//                sbsql.Append("            :vt_lhxnk,");
//                sbsql.Append("            :ten_lhxnk,");
//                sbsql.Append("            :ttien,");
//                sbsql.Append("            :ttien_nt,");
//                sbsql.Append("            :trang_thai,");
//                sbsql.Append("            :tk_ns,");
//                sbsql.Append("            :ten_tk_ns,");
//                sbsql.Append("            :ma_nkt,");
//                sbsql.Append("            :so_bl,");
//                sbsql.Append("            :ngay_bl_batdau,");
//                sbsql.Append("            :ngay_bl_ketthuc,");
//                sbsql.Append("            :songay_bl,");
//                sbsql.Append("            :kieu_bl,");
//                sbsql.Append("            :dien_giai,");
//                sbsql.Append("            :request_id,");
//                sbsql.Append("            :accept_yn,");
//                sbsql.Append("            :parent_transaction_id,");
//                sbsql.Append("            :ma_hq_ph,");
//                sbsql.Append("            :SO_TN_CT, ");
//                sbsql.Append("            :NGAY_TN_CT,");
//                sbsql.Append("            :kq_dc)");
//                foreach (CustomsV3.MSG.MSG54.ItemData itdata in msg54.Data.Accept_Transactions.Item)
//                {
//                    Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//                    _ttin_huy = getTTinCtuHuy(itdata.Ngay_TN_CT, itdata.So_TN_CT, "M51");

//                    IDbDataParameter[] p = null;

//                    p = new IDbDataParameter[40];

//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "ID";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = DBNull.Value;
//                    p[0].Value = iId;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TRANSACTION_ID";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = DBNull.Value;
//                    if (itdata.Transaction_ID != null)
//                        p[1].Value = itdata.Transaction_ID.Trim();

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "TRANSACTION_TYPE";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strTransactionType;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "SHKB";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "TEN_KB";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "NGAY_KB";
//                    p[5].DbType = DbType.Int64;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = DBNull.Value;

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "SO_BT";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = DBNull.Value;

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "KYHIEU_CT";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = DBNull.Value;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "SO_CT";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = DBNull.Value;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "NGAY_CT";
//                    p[9].DbType = DbType.Int64;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "NGAY_BN";
//                    p[10].DbType = DbType.DateTime;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = DBNull.Value;


//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_BC";
//                    p[11].DbType = DbType.DateTime;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = DBNull.Value;

//                    p[12] = new OracleParameter();
//                    p[12].ParameterName = "MA_NNTHUE";
//                    p[12].DbType = DbType.String;
//                    p[12].Direction = ParameterDirection.Input;
//                    p[12].Value = DBNull.Value;

//                    p[13] = new OracleParameter();
//                    p[13].ParameterName = "TEN_NNTHUE";
//                    p[13].DbType = DbType.String;
//                    p[13].Direction = ParameterDirection.Input;
//                    p[13].Value = DBNull.Value;

//                    p[14] = new OracleParameter();
//                    p[14].ParameterName = "MA_CQTHU";
//                    p[14].DbType = DbType.String;
//                    p[14].Direction = ParameterDirection.Input;
//                    p[14].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[14].Value = _ttin_huy.ma_cqthu;
//                    }

//                    p[15] = new OracleParameter();
//                    p[15].ParameterName = "TEN_CQTHU";
//                    p[15].DbType = DbType.String;
//                    p[15].Direction = ParameterDirection.Input;
//                    p[15].Value = DBNull.Value;

//                    p[16] = new OracleParameter();
//                    p[16].ParameterName = "SO_TK";
//                    p[16].DbType = DbType.String;
//                    p[16].Direction = ParameterDirection.Input;
//                    p[16].Value = DBNull.Value;

//                    p[17] = new OracleParameter();
//                    p[17].ParameterName = "NGAY_TK";
//                    p[17].DbType = DbType.DateTime;
//                    p[17].Direction = ParameterDirection.Input;
//                    p[17].Value = DBNull.Value;

//                    p[18] = new OracleParameter();
//                    p[18].ParameterName = "LHXNK";
//                    p[18].DbType = DbType.String;
//                    p[18].Direction = ParameterDirection.Input;
//                    p[18].Value = DBNull.Value;

//                    p[19] = new OracleParameter();
//                    p[19].ParameterName = "VT_LHXNK";//???
//                    p[19].DbType = DbType.String;
//                    p[19].Direction = ParameterDirection.Input;
//                    p[19].Value = DBNull.Value;


//                    p[20] = new OracleParameter();
//                    p[20].ParameterName = "TEN_LHXNK";
//                    p[20].DbType = DbType.String;
//                    p[20].Direction = ParameterDirection.Input;
//                    p[20].Value = DBNull.Value;

//                    p[21] = new OracleParameter();
//                    p[21].ParameterName = "TTIEN";
//                    p[21].DbType = DbType.String;
//                    p[21].Direction = ParameterDirection.Input;
//                    p[21].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[21].Value = Int64.Parse(_ttin_huy.tt_nop);
//                    }

//                    p[22] = new OracleParameter();
//                    p[22].ParameterName = "TTIEN_NT";
//                    p[22].DbType = DbType.String;
//                    p[22].Direction = ParameterDirection.Input;
//                    p[22].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[22].Value = Int64.Parse(_ttin_huy.tt_nop);
//                    }

//                    p[23] = new OracleParameter();
//                    p[23].ParameterName = "TRANG_THAI";//???
//                    p[23].DbType = DbType.String;
//                    p[23].Direction = ParameterDirection.Input;
//                    p[23].Value = DBNull.Value;

//                    p[24] = new OracleParameter();
//                    p[24].ParameterName = "TK_NS";//???
//                    p[24].DbType = DbType.String;
//                    p[24].Direction = ParameterDirection.Input;
//                    p[24].Value = DBNull.Value;

//                    p[25] = new OracleParameter();
//                    p[25].ParameterName = "TEN_TK_NS";
//                    p[25].DbType = DbType.String;
//                    p[25].Direction = ParameterDirection.Input;
//                    p[25].Value = DBNull.Value;

//                    p[26] = new OracleParameter();
//                    p[26].ParameterName = "MA_NKT";
//                    p[26].DbType = DbType.String;
//                    p[26].Direction = ParameterDirection.Input;
//                    p[26].Value = DBNull.Value;

//                    p[27] = new OracleParameter();
//                    p[27].ParameterName = "SO_BL";//???
//                    p[27].DbType = DbType.String;
//                    p[27].Direction = ParameterDirection.Input;
//                    p[27].Value = DBNull.Value;


//                    p[28] = new OracleParameter();
//                    p[28].ParameterName = "NGAY_BL_BATDAU";//???
//                    p[28].DbType = DbType.String;
//                    p[28].Direction = ParameterDirection.Input;
//                    p[28].Value = DBNull.Value;

//                    p[29] = new OracleParameter();
//                    p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//                    p[29].DbType = DbType.String;
//                    p[29].Direction = ParameterDirection.Input;
//                    p[29].Value = DBNull.Value;

//                    p[30] = new OracleParameter();
//                    p[30].ParameterName = "SONGAY_BL";//???
//                    p[30].DbType = DbType.String;
//                    p[30].Direction = ParameterDirection.Input;
//                    p[30].Value = DBNull.Value;


//                    p[31] = new OracleParameter();
//                    p[31].ParameterName = "KIEU_BL";//???
//                    p[31].DbType = DbType.String;
//                    p[31].Direction = ParameterDirection.Input;
//                    p[31].Value = DBNull.Value;


//                    p[32] = new OracleParameter();
//                    p[32].ParameterName = "DIEN_GIAI";
//                    p[32].DbType = DbType.String;
//                    p[32].Direction = ParameterDirection.Input;
//                    p[32].Value = DBNull.Value;

//                    p[33] = new OracleParameter();
//                    p[33].ParameterName = "REQUEST_ID";//???
//                    p[33].DbType = DbType.String;
//                    p[33].Direction = ParameterDirection.Input;
//                    p[33].Value = DBNull.Value;

//                    p[34] = new OracleParameter();
//                    p[34].ParameterName = "ACCEPT_YN";//???
//                    p[34].DbType = DbType.String;
//                    p[34].Direction = ParameterDirection.Input;
//                    p[34].Value = "Y";

//                    p[35] = new OracleParameter();
//                    p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//                    p[35].DbType = DbType.String;
//                    p[35].Direction = ParameterDirection.Input;
//                    p[35].Value = DBNull.Value;

//                    p[36] = new OracleParameter();
//                    p[36].ParameterName = "MA_HQ_PH";
//                    p[36].DbType = DbType.String;
//                    p[36].Direction = ParameterDirection.Input;
//                    p[36].Value = DBNull.Value;

//                    p[37] = new OracleParameter();
//                    p[37].ParameterName = "So_TN_CT";
//                    p[37].DbType = DbType.String;
//                    p[37].Direction = ParameterDirection.Input;
//                    p[37].Value = DBNull.Value;
//                    if (itdata.So_TN_CT != null)
//                        p[37].Value = itdata.So_TN_CT.Trim();

//                    p[38] = new OracleParameter();
//                    p[38].ParameterName = "Ngay_TN_CT";
//                    p[38].DbType = DbType.String;
//                    p[38].Direction = ParameterDirection.Input;
//                    p[38].Value = DBNull.Value;
//                    if (itdata.Ngay_TN_CT != null)
//                        p[38].Value = itdata.Ngay_TN_CT.Trim();

//                    p[39] = new OracleParameter();
//                    p[39].ParameterName = "KQ_DC";
//                    p[39].DbType = DbType.String;
//                    p[39].Direction = ParameterDirection.Input;
//                    p[39].Value = DBNull.Value;
//                    //p[37].Value = itdata.KQ_DC;
//                    if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//                    {
//                        p[39].Value = "OK";
//                    }
//                    else
//                    {
//                        p[39].Value = itdata.KQ_DC;
//                    }

//                    ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);

//                }
//                foreach (CustomsV3.MSG.MSG54.ItemData itdata in msg54.Data.Reject_Transactions.Item)
//                {
//                    Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//                    _ttin_huy = getTTinCtuHuy(itdata.Ngay_TN_CT, itdata.So_TN_CT, "M51");

//                    IDbDataParameter[] p = null;

//                    p = new IDbDataParameter[40];

//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "ID";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = DBNull.Value;
//                    p[0].Value = iId;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TRANSACTION_ID";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = DBNull.Value;
//                    if (itdata.Transaction_ID != null)
//                        p[1].Value = itdata.Transaction_ID.Trim();

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "TRANSACTION_TYPE";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strTransactionType;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "SHKB";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "TEN_KB";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "NGAY_KB";
//                    p[5].DbType = DbType.Int64;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = DBNull.Value;

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "SO_BT";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = DBNull.Value;

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "KYHIEU_CT";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = DBNull.Value;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "SO_CT";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = DBNull.Value;


//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "NGAY_CT";
//                    p[9].DbType = DbType.Int64;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "NGAY_BN";
//                    p[10].DbType = DbType.DateTime;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = DBNull.Value;

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_BC";
//                    p[11].DbType = DbType.DateTime;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = DBNull.Value;

//                    p[12] = new OracleParameter();
//                    p[12].ParameterName = "MA_NNTHUE";
//                    p[12].DbType = DbType.String;
//                    p[12].Direction = ParameterDirection.Input;
//                    p[12].Value = DBNull.Value;

//                    p[13] = new OracleParameter();
//                    p[13].ParameterName = "TEN_NNTHUE";
//                    p[13].DbType = DbType.String;
//                    p[13].Direction = ParameterDirection.Input;
//                    p[13].Value = DBNull.Value;

//                    p[14] = new OracleParameter();
//                    p[14].ParameterName = "MA_CQTHU";
//                    p[14].DbType = DbType.String;
//                    p[14].Direction = ParameterDirection.Input;
//                    p[14].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[14].Value = _ttin_huy.ma_cqthu;
//                    }

//                    p[15] = new OracleParameter();
//                    p[15].ParameterName = "TEN_CQTHU";
//                    p[15].DbType = DbType.String;
//                    p[15].Direction = ParameterDirection.Input;
//                    p[15].Value = DBNull.Value;

//                    p[16] = new OracleParameter();
//                    p[16].ParameterName = "SO_TK";
//                    p[16].DbType = DbType.String;
//                    p[16].Direction = ParameterDirection.Input;
//                    p[16].Value = DBNull.Value;

//                    p[17] = new OracleParameter();
//                    p[17].ParameterName = "NGAY_TK";
//                    p[17].DbType = DbType.DateTime;
//                    p[17].Direction = ParameterDirection.Input;
//                    p[17].Value = DBNull.Value;

//                    p[18] = new OracleParameter();
//                    p[18].ParameterName = "LHXNK";
//                    p[18].DbType = DbType.String;
//                    p[18].Direction = ParameterDirection.Input;
//                    p[18].Value = DBNull.Value;

//                    p[19] = new OracleParameter();
//                    p[19].ParameterName = "VT_LHXNK";//???
//                    p[19].DbType = DbType.String;
//                    p[19].Direction = ParameterDirection.Input;
//                    p[19].Value = DBNull.Value;


//                    p[20] = new OracleParameter();
//                    p[20].ParameterName = "TEN_LHXNK";
//                    p[20].DbType = DbType.String;
//                    p[20].Direction = ParameterDirection.Input;
//                    p[20].Value = DBNull.Value;

//                    p[21] = new OracleParameter();
//                    p[21].ParameterName = "TTIEN";
//                    p[21].DbType = DbType.String;
//                    p[21].Direction = ParameterDirection.Input;
//                    p[21].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[21].Value = Int64.Parse(_ttin_huy.tt_nop);
//                    }

//                    p[22] = new OracleParameter();
//                    p[22].ParameterName = "TTIEN_NT";
//                    p[22].DbType = DbType.String;
//                    p[22].Direction = ParameterDirection.Input;
//                    p[22].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[22].Value = Int64.Parse(_ttin_huy.tt_nop);
//                    }

//                    p[23] = new OracleParameter();
//                    p[23].ParameterName = "TRANG_THAI";//???
//                    p[23].DbType = DbType.String;
//                    p[23].Direction = ParameterDirection.Input;
//                    p[23].Value = DBNull.Value;

//                    p[24] = new OracleParameter();
//                    p[24].ParameterName = "TK_NS";//???
//                    p[24].DbType = DbType.String;
//                    p[24].Direction = ParameterDirection.Input;
//                    p[24].Value = DBNull.Value;

//                    p[25] = new OracleParameter();
//                    p[25].ParameterName = "TEN_TK_NS";
//                    p[25].DbType = DbType.String;
//                    p[25].Direction = ParameterDirection.Input;
//                    p[25].Value = DBNull.Value;

//                    p[26] = new OracleParameter();
//                    p[26].ParameterName = "MA_NKT";
//                    p[26].DbType = DbType.String;
//                    p[26].Direction = ParameterDirection.Input;
//                    p[26].Value = DBNull.Value;

//                    p[27] = new OracleParameter();
//                    p[27].ParameterName = "SO_BL";//???
//                    p[27].DbType = DbType.String;
//                    p[27].Direction = ParameterDirection.Input;
//                    p[27].Value = DBNull.Value;


//                    p[28] = new OracleParameter();
//                    p[28].ParameterName = "NGAY_BL_BATDAU";//???
//                    p[28].DbType = DbType.String;
//                    p[28].Direction = ParameterDirection.Input;
//                    p[28].Value = DBNull.Value;

//                    p[29] = new OracleParameter();
//                    p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//                    p[29].DbType = DbType.String;
//                    p[29].Direction = ParameterDirection.Input;
//                    p[29].Value = DBNull.Value;

//                    p[30] = new OracleParameter();
//                    p[30].ParameterName = "SONGAY_BL";//???
//                    p[30].DbType = DbType.String;
//                    p[30].Direction = ParameterDirection.Input;
//                    p[30].Value = DBNull.Value;


//                    p[31] = new OracleParameter();
//                    p[31].ParameterName = "KIEU_BL";//???
//                    p[31].DbType = DbType.String;
//                    p[31].Direction = ParameterDirection.Input;
//                    p[31].Value = DBNull.Value;


//                    p[32] = new OracleParameter();
//                    p[32].ParameterName = "DIEN_GIAI";
//                    p[32].DbType = DbType.String;
//                    p[32].Direction = ParameterDirection.Input;
//                    p[32].Value = DBNull.Value;

//                    p[33] = new OracleParameter();
//                    p[33].ParameterName = "REQUEST_ID";//???
//                    p[33].DbType = DbType.String;
//                    p[33].Direction = ParameterDirection.Input;
//                    p[33].Value = DBNull.Value;

//                    p[34] = new OracleParameter();
//                    p[34].ParameterName = "ACCEPT_YN";//???
//                    p[34].DbType = DbType.String;
//                    p[34].Direction = ParameterDirection.Input;
//                    p[34].Value = "N";

//                    p[35] = new OracleParameter();
//                    p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//                    p[35].DbType = DbType.String;
//                    p[35].Direction = ParameterDirection.Input;
//                    p[35].Value = DBNull.Value;

//                    p[36] = new OracleParameter();
//                    p[36].ParameterName = "MA_HQ_PH";
//                    p[36].DbType = DbType.String;
//                    p[36].Direction = ParameterDirection.Input;
//                    p[36].Value = DBNull.Value;

//                    p[37] = new OracleParameter();
//                    p[37].ParameterName = "So_TN_CT";
//                    p[37].DbType = DbType.String;
//                    p[37].Direction = ParameterDirection.Input;
//                    p[37].Value = DBNull.Value;
//                    if (itdata.So_TN_CT != null)
//                        p[37].Value = itdata.So_TN_CT.Trim();

//                    p[38] = new OracleParameter();
//                    p[38].ParameterName = "Ngay_TN_CT";
//                    p[38].DbType = DbType.String;
//                    p[38].Direction = ParameterDirection.Input;
//                    p[38].Value = DBNull.Value;
//                    if (itdata.Ngay_TN_CT != null)
//                        p[38].Value = itdata.Ngay_TN_CT.Trim();

//                    p[39] = new OracleParameter();
//                    p[39].ParameterName = "KQ_DC";
//                    p[39].DbType = DbType.String;
//                    p[39].Direction = ParameterDirection.Input;
//                    p[39].Value = DBNull.Value;
//                    //p[37].Value = itdata.KQ_DC;
//                    if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//                    {
//                        p[39].Value = "OK";
//                    }
//                    else
//                    {
//                        p[39].Value = itdata.KQ_DC;
//                    }

//                    ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);
//                }
//                transCT.Commit();


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch huy : Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("insert vao CSDL\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch huy : Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("insert vao CSDL\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }
//                throw ex;
//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }
//        }

//        #endregion "Truy vấn kết quả đối chiếu dữ liệu giao dịch huy M53 - M54"

//        #region "Xác nhận bảo lãnh thuế của Ngân hàng M61 - M62"

//        public static void BLANHToMSG61(String pv_strMaNTT, String pv_strSHKB,
//                                        String pv_strSoCT, String pv_strSoBT,
//                                        String pv_ngay_kb,
//                                        ref MSG61 msg61)
//        {
//            msg61.Data = new CustomsV3.MSG.MSG61.Data();
//            msg61.Data.Ma_NH_PH = strSender_Code;
//            msg61.Data.MST_NH_PH = strSender_TaxNo;
//            msg61.Data.Ten_NH_PH = strSender_Name;
//            msg61.Data.Ma_DV = pv_strMaNTT;

//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            //string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ma_nnthue ma_dv,");
//                sbsql.Append("         h.ma_dv_dd ma_dv_dd,");
//                sbsql.Append("         h.ten_dv_dd ten_dv_dd,");
//                sbsql.Append("         h.SONGAY_BL SONGAY_BL,");
//                sbsql.Append("         TO_CHAR (h.ngay_batdau, 'RRRR-MM-DD') ngay_batdau,");
//                sbsql.Append("         TO_CHAR ( h.ngay_ketthuc, 'RRRR-MM-DD') ngay_ketthuc,");
//                sbsql.Append("         h.SHKB SHKB,");
//                sbsql.Append("         h.MA_NV MA_NV,");
//                sbsql.Append("         h.MA_DTHU MA_DTHU,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         ma_hq ma_hq,");
//                sbsql.Append("         h.lhxnk ma_lh,");
//                sbsql.Append("         h.so_tk sotk,");
//                sbsql.Append("         TO_CHAR (ngay_tk, 'RRRR-MM-DD') ngay_dk,");//???Ngay DK la ngay nao?                        
//                sbsql.Append("         ma_loaitien ma_lt,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         h.ttien,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (h.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD') ngay_ct,");
//                sbsql.Append("         h.dien_giai diengiai");
//                sbsql.Append("  FROM   TCS_BAOLANH_HDR h");
//                sbsql.Append("    WHERE  h.ngay_kb = :ngay_kb");
//                sbsql.Append("     AND h.so_bt = :so_bt");
//                sbsql.Append("     AND h.so_ct = :so_ct");
//                sbsql.Append("     AND h.ma_nnthue = :ma_nnthue");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[4];


//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAY_KB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_ngay_kb;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "SO_BT";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_strSoBT;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "SO_CT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strSoCT;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "MA_NNTHUE";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = pv_strMaNTT;


//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }


//                msg61.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg61.Data.Ma_DV_DD = ds.Tables[0].Rows[0]["MA_DV_DD"].ToString();
//                msg61.Data.Ten_DV_DD = ds.Tables[0].Rows[0]["Ten_DV_DD"].ToString();
//                msg61.Data.Ma_HQ_PH = ds.Tables[0].Rows[0]["MA_HQ_PH"].ToString();
//                //msg61.Data.Ma_HQ_CQT = ds.Tables[0].Rows[0]["MA_HQ_CQT"].ToString();
//                msg61.Data.Ma_HQ = ds.Tables[0].Rows[0]["MA_HQ"].ToString();
//                msg61.Data.Ma_LH = ds.Tables[0].Rows[0]["MA_LH"].ToString();
//                msg61.Data.So_TK = ds.Tables[0].Rows[0]["SOTK"].ToString();
//                msg61.Data.Ngay_DK = ds.Tables[0].Rows[0]["NGAY_DK"].ToString();
//                msg61.Data.Ma_LT = ds.Tables[0].Rows[0]["MA_LT"].ToString();
//                msg61.Data.Loai_CT = strLoai_BLTK;
//                msg61.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg61.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//                msg61.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();// ds.Tables[0].Rows[0]["NGAY_CT"].ToString();//DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg61.Data.TTButToan = ds.Tables[0].Rows[0]["TTBUTTOAN"].ToString();
//                msg61.Data.SNBL = ds.Tables[0].Rows[0]["SONGAY_BL"].ToString();
//                msg61.Data.Ngay_HL = ds.Tables[0].Rows[0]["NGAY_BATDAU"].ToString();
//                msg61.Data.Ngay_HHL = ds.Tables[0].Rows[0]["NGAY_KETTHUC"].ToString();//ds.Tables[0].Rows[0]["NGAY_KETTHUC"].ToString();
//                msg61.Data.SoTien = ds.Tables[0].Rows[0]["TTIEN"].ToString();
//                msg61.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết bao lanh (CTUToMSG61): ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append(", SHKB - ");
//                sbErrMsg.Append(pv_strSHKB);
//                sbErrMsg.Append(", So CT - ");
//                sbErrMsg.Append(pv_strSoCT);
//                sbErrMsg.Append(", So BT - ");
//                sbErrMsg.Append(pv_strSoBT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                //sbErrMsg.Append("\n");
//                //sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết bao lanh (CTUToMSG61): ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append(", SHKB - ");
//                    sbErrMsg.Append(pv_strSHKB);
//                    sbErrMsg.Append(", So CT - ");
//                    sbErrMsg.Append(pv_strSoCT);
//                    sbErrMsg.Append(", So BT - ");
//                    sbErrMsg.Append(pv_strSoBT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    //sbErrMsg.Append("\n");
//                    //sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }
//        public static void BLANHToMSG63(String pv_strMaNTT, String pv_strSHKB,
//                                        String pv_strSoCT, String pv_strSoBT,
//                                        String pv_ngay_kb,
//                                        ref MSG63 msg63)
//        {
//            msg63.Data = new CustomsV3.MSG.MSG63.Data();
//            msg63.Data.Ma_NH_PH = strSender_Code;
//            msg63.Data.MST_NH_PH = strSender_TaxNo;
//            msg63.Data.Ten_NH_PH = strSender_Name;
//            msg63.Data.Ma_DV = pv_strMaNTT;

//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            //string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ma_nnthue ma_dv,");
//                sbsql.Append("         h.ma_dv_dd ma_dv_dd,");
//                sbsql.Append("         h.ten_dv_dd ten_dv_dd,");
//                sbsql.Append("         h.SONGAY_BL SONGAY_BL,");
//                sbsql.Append("         TO_CHAR(h.ngay_batdau,'RRRR-MM-DD') ngay_batdau,");
//                sbsql.Append("         TO_CHAR(h.ngay_ketthuc,'RRRR-MM-DD') ngay_ketthuc,");
//                sbsql.Append("         h.SHKB SHKB,");
//                sbsql.Append("         h.MA_NV MA_NV,");
//                sbsql.Append("         h.MA_DTHU MA_DTHU,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         ma_hq ma_hq,");
//                sbsql.Append("         h.lhxnk ma_lh,");
//                sbsql.Append("         h.HOA_DON HOA_DON,");
//                sbsql.Append("        TO_CHAR(h.NGAY_HDON,'RRRR-MM-DD') NGAY_HD ,");
//                sbsql.Append("         TO_CHAR (ngay_tk, 'RRRR-MM-DD') ngay_dk,");//???Ngay DK la ngay nao?                        
//                sbsql.Append("         KIEU_BL ma_lt,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         h.so_ct so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         h.ttien,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (h.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD') ngay_ct,");
//                sbsql.Append("         h.vt_don vtdon1,");
//                sbsql.Append("         TO_CHAR(h.NGAY_VTD,'RRRR-MM-DD') NGAY_VTD1 ,");
//                sbsql.Append("         h.VT_DON2 VT_DON2,");
//                sbsql.Append("         TO_CHAR(h.NGAY_VTD2,'RRRR-MM-DD') NGAY_VTD2 ,");
//                sbsql.Append("         h.VT_DON3 VT_DON3,");
//                sbsql.Append("         TO_CHAR(h.NGAY_VTD3,'RRRR-MM-DD') NGAY_VTD3 ,");
//                sbsql.Append("         h.VT_DON4 VT_DON4,");
//                sbsql.Append("         TO_CHAR(h.NGAY_VTD4,'RRRR-MM-DD') NGAY_VTD4 ,");
//                sbsql.Append("         h.VT_DON5 VT_DON5,");
//                sbsql.Append("         TO_CHAR(h.NGAY_VTD5,'RRRR-MM-DD') NGAY_VTD5 ,");
//                sbsql.Append("         h.dien_giai diengiai");
//                sbsql.Append("  FROM   TCS_BAOLANH_HDR h");
//                sbsql.Append("    WHERE  h.ngay_kb = :ngay_kb");
//                sbsql.Append("     AND h.so_bt = :so_bt");
//                sbsql.Append("     AND h.so_ct = :so_ct");
//                sbsql.Append("     AND h.ma_nnthue = :ma_nnthue");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[4];


//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAY_KB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_ngay_kb;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "SO_BT";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_strSoBT;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "SO_CT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strSoCT;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "MA_NNTHUE";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = pv_strMaNTT;


//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }


//                msg63.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg63.Data.Ma_DV_DD = ds.Tables[0].Rows[0]["MA_DV_DD"].ToString();
//                msg63.Data.Ten_DV_DD = ds.Tables[0].Rows[0]["Ten_DV_DD"].ToString();
//                msg63.Data.So_HD = ds.Tables[0].Rows[0]["HOA_DON"].ToString();
//                msg63.Data.Ngay_HD = ds.Tables[0].Rows[0]["NGAY_HD"].ToString();
//                msg63.Data.Loai_CT = strLoai_BLVTD;
//                msg63.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg63.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//                msg63.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();
//                msg63.Data.TTButToan = ds.Tables[0].Rows[0]["TTBUTTOAN"].ToString();
//                msg63.Data.SNBL = ds.Tables[0].Rows[0]["SONGAY_BL"].ToString();
//                msg63.Data.Ngay_HL = ds.Tables[0].Rows[0]["NGAY_BATDAU"].ToString(); ;
//                msg63.Data.Ngay_HHL = ds.Tables[0].Rows[0]["NGAY_KETTHUC"].ToString();
//                msg63.Data.SoTien = ds.Tables[0].Rows[0]["TTIEN"].ToString();
//                msg63.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();
//                msg63.Data.Ma_HQ_KB = ds.Tables[0].Rows[0]["ma_hq"].ToString();
//                msg63.Data.So_VD_01 = ds.Tables[0].Rows[0]["vtdon1"].ToString();
//                msg63.Data.Ngay_VD_01 = ds.Tables[0].Rows[0]["ngay_vtd1"].ToString();
//                msg63.Data.So_VD_02 = ds.Tables[0].Rows[0]["VT_DON2"].ToString();
//                msg63.Data.Ngay_VD_02 = ds.Tables[0].Rows[0]["ngay_vtd2"].ToString();
//                msg63.Data.So_VD_03 = ds.Tables[0].Rows[0]["VT_DON3"].ToString();
//                msg63.Data.Ngay_VD_03 = ds.Tables[0].Rows[0]["ngay_vtd3"].ToString();
//                msg63.Data.So_VD_04 = ds.Tables[0].Rows[0]["VT_DON4"].ToString();
//                msg63.Data.Ngay_VD_04 = ds.Tables[0].Rows[0]["ngay_vtd4"].ToString();
//                msg63.Data.So_VD_05 = ds.Tables[0].Rows[0]["VT_DON5"].ToString();
//                msg63.Data.Ngay_VD_05 = ds.Tables[0].Rows[0]["ngay_vtd5"].ToString();
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết bao lanh (CTUToMSG61): ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append(", SHKB - ");
//                sbErrMsg.Append(pv_strSHKB);
//                sbErrMsg.Append(", So CT - ");
//                sbErrMsg.Append(pv_strSoCT);
//                sbErrMsg.Append(", So BT - ");
//                sbErrMsg.Append(pv_strSoBT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                //sbErrMsg.Append("\n");
//                //sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết bao lanh (CTUToMSG61): ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append(", SHKB - ");
//                    sbErrMsg.Append(pv_strSHKB);
//                    sbErrMsg.Append(", So CT - ");
//                    sbErrMsg.Append(pv_strSoCT);
//                    sbErrMsg.Append(", So BT - ");
//                    sbErrMsg.Append(pv_strSoBT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    //sbErrMsg.Append("\n");
//                    //sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }

//        }
//        public static void BLANHToMSG65(String pv_strMaNTT, String pv_strSHKB,
//                                        String pv_strSoCT, String pv_strSoBT,
//                                        String pv_ngay_kb,
//                                        ref MSG65 msg65)
//        {
//            msg65.Data = new CustomsV3.MSG.MSG65.Data();
//            msg65.Data.Ma_NH_PH = strSender_Code;
//            msg65.Data.MST_NH_PH = strSender_TaxNo;
//            msg65.Data.Ten_NH_PH = strSender_Name;
//            msg65.Data.Ma_DV = pv_strMaNTT;

//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            //string v_ngaylv = TCS_GetNgayLV(pv_strMaNV);

//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ma_nnthue ma_dv,");
//                sbsql.Append("         h.ma_dv_dd ma_dv_dd,");
//                sbsql.Append("         h.ten_dv_dd ten_dv_dd,");
//                sbsql.Append("         h.SONGAY_BL SONGAY_BL,");
//                sbsql.Append("         TO_CHAR (h.ngay_batdau, 'RRRR-MM-DD') ngay_batdau,");
//                sbsql.Append("         TO_CHAR ( h.ngay_ketthuc, 'RRRR-MM-DD') ngay_ketthuc,");
//                sbsql.Append("         h.SHKB SHKB,");
//                sbsql.Append("         h.MA_NV MA_NV,");
//                sbsql.Append("         h.MA_DTHU MA_DTHU,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         ma_hq ma_hq,");
//                sbsql.Append("         h.lhxnk ma_lh,");
//                sbsql.Append("         TO_CHAR (ngay_tk, 'RRRR-MM-DD') ngay_dk,");//???Ngay DK la ngay nao?                        
//                sbsql.Append("         KIEU_BL ma_lt,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         h.ttien,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (h.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD') ngay_ct,");
//                sbsql.Append("         h.dien_giai diengiai");
//                sbsql.Append("  FROM   TCS_BAOLANH_HDR h");
//                sbsql.Append("    WHERE  h.ngay_kb = :ngay_kb");
//                sbsql.Append("     AND h.so_bt = :so_bt");
//                sbsql.Append("     AND h.so_ct = :so_ct");
//                sbsql.Append("     AND h.ma_nnthue = :ma_nnthue");

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[4];


//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAY_KB";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = pv_ngay_kb;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "SO_BT";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_strSoBT;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "SO_CT";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = pv_strSoCT;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "MA_NNTHUE";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = pv_strMaNTT;


//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }


//                msg65.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg65.Data.Ma_DV_DD = ds.Tables[0].Rows[0]["MA_DV_DD"].ToString();
//                msg65.Data.Ten_DV_DD = ds.Tables[0].Rows[0]["Ten_DV_DD"].ToString();
//                msg65.Data.Loai_CT = strLoai_BLC;
//                msg65.Data.KyHieu_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg65.Data.So_CT = ds.Tables[0].Rows[0]["SO_CT"].ToString();
//                msg65.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();
//                msg65.Data.TTButToan = ds.Tables[0].Rows[0]["TTBUTTOAN"].ToString();
//                //msg65.Data.SNBL = ds.Tables[0].Rows[0]["SONGAY_BL"].ToString();
//                msg65.Data.Ngay_HL = ds.Tables[0].Rows[0]["NGAY_BATDAU"].ToString(); ;
//                msg65.Data.Ngay_HHL = ds.Tables[0].Rows[0]["NGAY_KETTHUC"].ToString();
//                msg65.Data.SoTien = ds.Tables[0].Rows[0]["TTIEN"].ToString();
//                msg65.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết bao lanh (CTUToMSG61): ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append(", SHKB - ");
//                sbErrMsg.Append(pv_strSHKB);
//                sbErrMsg.Append(", So CT - ");
//                sbErrMsg.Append(pv_strSoCT);
//                sbErrMsg.Append(", So BT - ");
//                sbErrMsg.Append(pv_strSoBT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                //sbErrMsg.Append("\n");
//                //sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết bao lanh (CTUToMSG61): ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append(", SHKB - ");
//                    sbErrMsg.Append(pv_strSHKB);
//                    sbErrMsg.Append(", So CT - ");
//                    sbErrMsg.Append(pv_strSoCT);
//                    sbErrMsg.Append(", So BT - ");
//                    sbErrMsg.Append(pv_strSoBT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    //sbErrMsg.Append("\n");
//                    //sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }

//        }

//        public static string convertDateFormat(String strDate)
//        {
//            String ngay = strDate.Substring(0, 2);
//            String thang = strDate.Substring(3, 2);
//            String nam = strDate.Substring(6, 4);
//            return nam + "-" + thang + "-" + ngay;
//        }
//        public static void getMSG62(String pv_strMaNTT, String pv_strSHKB, String pv_strSoCT, String pv_strSoBT, string pv_ngay_kb,
//                                      string pv_loai_BL, ref String pv_So_TN_CT, ref String pv_Ngay_TN_CT,
//                                      ref String pv_ErrorNum, ref String pv_ErrorMes, ref String pv_transaction_id)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                string strXMLOut = "";
//                string strMSGType = "";
//                if (pv_loai_BL.Equals(strLoai_BLTK))
//                {
//                    MSG61 MSG61 = new MSG61();
//                    MSG61.Data = new CustomsV3.MSG.MSG61.Data();
//                    MSG61.Header = new CustomsV3.MSG.MSG61.Header();

//                    //Gan du lieu
//                    //Gan du lieu Header
//                    MSG61.Header.Message_Version = strMessage_Version;
//                    MSG61.Header.Sender_Code = strSender_Code;
//                    MSG61.Header.Sender_Name = strSender_Name;
//                    MSG61.Header.Transaction_Type = "61";
//                    MSG61.Header.Transaction_Name = "Thông điệp xác nhận bảo lãnh thuế cho tờ khai Hải quan của tổ chức tín dụng ";
//                    MSG61.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                    pv_transaction_id = Guid.NewGuid().ToString();
//                    MSG61.Header.Transaction_ID = pv_transaction_id;
//                    strMSGType = "MSG61";

//                    //Gan du lieu Data
//                    BLANHToMSG61(pv_strMaNTT, pv_strSHKB,
//                                               pv_strSoCT, pv_strSoBT,
//                                                pv_ngay_kb, ref MSG61);

//                    //Chuyen thanh XML
//                    strXMLOut = MSG61.MSG61toXML(MSG61);
//                }
//                else if (pv_loai_BL.Equals(strLoai_BLVTD))
//                {
//                    MSG63 MSG63 = new MSG63();
//                    MSG63.Data = new CustomsV3.MSG.MSG63.Data();
//                    MSG63.Header = new CustomsV3.MSG.MSG63.Header();

//                    //Gan du lieu
//                    //Gan du lieu Header
//                    MSG63.Header.Message_Version = strMessage_Version;
//                    MSG63.Header.Sender_Code = strSender_Code;
//                    MSG63.Header.Sender_Name = strSender_Name;
//                    MSG63.Header.Transaction_Type = "63";
//                    MSG63.Header.Transaction_Name = "Thông điệp xác nhận bảo lãnh thuế cho hóa đơn hoặc vận đơn của tổ chức tín dụng";
//                    MSG63.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                    pv_transaction_id = Guid.NewGuid().ToString();
//                    MSG63.Header.Transaction_ID = pv_transaction_id;
//                    strMSGType = "MSG63";
//                    //Gan du lieu Data
//                    BLANHToMSG63(pv_strMaNTT, pv_strSHKB,
//                                               pv_strSoCT, pv_strSoBT,
//                                                pv_ngay_kb, ref MSG63);

//                    //Chuyen thanh XML
//                    strXMLOut = MSG63.MSG63toXML(MSG63);

//                }
//                else if (pv_loai_BL.Equals(strLoai_BLC))
//                {
//                    MSG65 MSG65 = new MSG65();
//                    MSG65.Data = new CustomsV3.MSG.MSG65.Data();
//                    MSG65.Header = new CustomsV3.MSG.MSG65.Header();

//                    //Gan du lieu
//                    //Gan du lieu Header
//                    MSG65.Header.Message_Version = strMessage_Version;
//                    MSG65.Header.Sender_Code = strSender_Code;
//                    MSG65.Header.Sender_Name = strSender_Name;
//                    MSG65.Header.Transaction_Type = "65";
//                    MSG65.Header.Transaction_Name = "Thông điệp xác nhận bảo lãnh chung cho doanh nghiệp xuất nhập khẩu của tổ chức tín dụng";
//                    MSG65.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                    pv_transaction_id = Guid.NewGuid().ToString();
//                    MSG65.Header.Transaction_ID = pv_transaction_id;
//                    strMSGType = "MSG65";
//                    //Gan du lieu Data
//                    BLANHToMSG65(pv_strMaNTT, pv_strSHKB,
//                                               pv_strSoCT, pv_strSoBT,
//                                                pv_ngay_kb, ref MSG65);

//                    //Chuyen thanh XML
//                    strXMLOut = MSG65.MSG65toXML(MSG65);

//                }

//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                //String strXMLIn = sendMsg(strMSGType, "<CUSTOMS><Header><Message_Version>1.3</Message_Version><Sender_Code>01341001</Sender_Code><Sender_Name>Ngan Hang TMCP Xang Dau Petrolimex</Sender_Name><Transaction_Type>61</Transaction_Type><Transaction_Name>Thông điệp xác nhận bảo lãnh thuế cho tờ khai Hải quan của Ngân hàng</Transaction_Name><Transaction_Date>2014-09-18T10:13:20</Transaction_Date><Transaction_ID>d73d693f-f960-4f04-bc45-02cac1d84df1</Transaction_ID></Header><Data><Ma_NH_PH>01341001</Ma_NH_PH><MST_NH_PH>0102454468</MST_NH_PH><Ten_NH_PH>Ngan Hang TMCP Xang Dau Petrolimex</Ten_NH_PH><Ma_DV>0101519289001</Ma_DV><Ten_DV /><Ma_DV_DD>hoap</Ma_DV_DD><Ten_DV_DD /><Ma_HQ_PH /><Ma_HQ>01BT</Ma_HQ><Ma_LH>B11</Ma_LH><So_TK>30003038884</So_TK><Ngay_DK>2014-09-18T10:13:20</Ngay_DK><Ma_LT>1</Ma_LT><Loai_CT>31</Loai_CT><KyHieu_CT>PG_0000001</KyHieu_CT><So_CT>140300145</So_CT><Ngay_CT>2014-09-18T10:13:20</Ngay_CT><TTButToan>1</TTButToan><SNBL>13</SNBL><Ngay_HL>2014-09-18T10:13:20</Ngay_HL><Ngay_HHL>2014-09-30</Ngay_HHL><SoTien>0</SoTien><DienGiai>Bảo lãnh tiền mặt</DienGiai></Data><Security><Signature></Signature></Security></CUSTOMS>");
//                String strXMLIn = sendMsg(strMSGType, strXMLOut);
//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);

//                MSG62 msg62 = new MSG62();
//                MSG62 objTemp = msg62.MSGToObject(strXMLIn);

//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;
//                if (pv_ErrorNum.Equals("0"))
//                {
//                    pv_So_TN_CT = objTemp.Data.So_TN_CT;
//                    pv_Ngay_TN_CT = objTemp.Data.Ngay_TN_CT;
//                }
//                //// Verify sign                
//                // if (!verifySignMsg(objTemp, "MSG61"))
//                // {
//                //     pv_ErrorNum = "00001";
//                //     pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                // }
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình gửi MSG 62 sang HQ: ĐTNT - ");
//                sbErrMsg.Append(pv_strMaNTT);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                //sbErrMsg.Append("\n");
//                //sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình gửi MSG 62 sang HQ: ĐTNT - ");
//                    sbErrMsg.Append(pv_strMaNTT);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    //sbErrMsg.Append("\n");
//                    //sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = sbErrMsg.ToString();

//                //throw ex;
//            }

//        }
//        #endregion "Xác nhận bảo lãnh thuế của Ngân hàng M61 - M62"

//        #region "Hủy bảo lãnh thuế của ngân hàng M71 - M72"

//        public static void getMSG72(String pv_strSo_TN_YCH, ref String pv_So_TN_CT, ref String pv_Ngay_TN_CT,
//                                   ref String pv_ErrorNum, ref String pv_ErrorMes, ref String pv_transaction_id)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                // pv_strNgayCT = pv_strNgayCT.Substring(0, 4) + "-" + pv_strNgayCT.Substring(4, 2) + "-" + pv_strNgayCT.Substring(6, 2);
//                MSG71 MSG71 = new MSG71();
//                MSG71.Data = new CustomsV3.MSG.MSG71.Data();
//                MSG71.Header = new CustomsV3.MSG.MSG71.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                MSG71.Header.Message_Version = strMessage_Version;
//                MSG71.Header.Sender_Code = strSender_Code;
//                MSG71.Header.Sender_Name = strSender_Name;
//                MSG71.Header.Transaction_Type = "71";
//                MSG71.Header.Transaction_Name = "Thông điệp đề nghị hủy bảo lãnh thuế của ngân hàng";
//                MSG71.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                pv_transaction_id = Guid.NewGuid().ToString();
//                MSG71.Header.Transaction_ID = pv_transaction_id;

//                //Gan du lieu Data
//                MSG71.Data.So_TN_CT_YCH = pv_strSo_TN_YCH;

//                //Chuyen thanh XML
//                string strXMLOut = MSG71.MSG71toXML(MSG71);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsg("MSG71", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //StringBuilder sb = new StringBuilder();
//                //sb.Append(strXMLdefin1);
//                //sb.Append("\r\n");
//                //sb.Append(strXMLIn);
//                //strXMLIn = sb.ToString();

//                MSG72 msg72 = new MSG72();
//                MSG72 objTemp = msg72.MSGToObject(strXMLIn);
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;
//                if (objTemp.Data.So_TN_CT != null && objTemp.Data.Ngay_TN_CT != null)
//                {
//                    pv_So_TN_CT = objTemp.Data.So_TN_CT;
//                    pv_Ngay_TN_CT = objTemp.Data.Ngay_TN_CT;
//                }

//                //Verify sign                
//                if (!verifySignMsg(objTemp, "MSG72"))
//                {
//                    pv_ErrorNum = "00001";
//                    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                }

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình hủy bảo lãnh thuế của ngân hàng (MSG 72): So CT - ");
//                sbErrMsg.Append(pv_strSo_TN_YCH);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình hủy bảo lãnh thuế của ngân hàng (MSG 72): So CT - ");
//                    sbErrMsg.Append(pv_strSo_TN_YCH);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                pv_ErrorNum = "00000";
//                pv_ErrorMes = sbErrMsg.ToString();

//                //throw ex;
//            }

//        }

//        #endregion "Hủy bảo lãnh thuế của ngân hàng M71 - M72"

//        #region "đối chiếu các giao dịch bảo lãnh thành công M81 - M82 -M83 -M84"

//        public static void DOICHIEUToMSG81(DateTime pv_dtNgayDC, ref MSG81 msg81)
//        {
//            msg81.Data = new CustomsV3.MSG.MSG81.Data();
//            msg81.Data.temp.Ma_NH_DC = strSender_Code;
//            msg81.Data.temp.Ngay_DC = pv_dtNgayDC.ToString("yyyy-MM-dd");
//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   hdr.id,");
//                sbsql.Append("         hdr.transaction_id transaction_id,");
//                sbsql.Append("         hdr.so_tn_ct so_tn_ct,");
//                sbsql.Append("         hdr.ngay_tn_ct,");
//                sbsql.Append("         hdr.ma_nnthue ma_dv,");
//                sbsql.Append("         hdr.ten_nnthue ten_dv,");
//                sbsql.Append("         hdr.ma_dv_dd ma_dv_dd,");
//                sbsql.Append("         hdr.ten_dv_dd ten_dv_dd,");
//                sbsql.Append("         hdr.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         hdr.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         hdr.ma_hq ma_hq,");
//                sbsql.Append("         hdr.lhxnk ma_lh,");
//                sbsql.Append("         hdr.so_tk sotk,");
//                sbsql.Append("         to_char(ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         hdr.ma_loaitien ma_lt,");
//                sbsql.Append("         hdr.ma_ntk ma_ntk,");
//                sbsql.Append("         hdr.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         hdr.so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         hdr.ttien ttien,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (hdr.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD') ngay_ct,");
//                sbsql.Append("         hdr.diengiai diengiai,");
//                sbsql.Append("          songay_bl thoigian_bl,");
//                sbsql.Append("          TO_CHAR(ngay_bl_batdau,'YYYY-MM-DD') thoigianbatdau_bl,");
//                sbsql.Append("          TO_CHAR(ngay_bl_ketthuc,'YYYY-MM-DD') thoigianketthuc_bl");
//                sbsql.Append("  FROM   tcs_dchieu_nhhq_hdr hdr");
//                sbsql.Append(" WHERE   hdr.transaction_type = 'M81' " + strDKNGAYDC);

//                DateTime ngayDC1 = pv_dtNgayDC.AddDays(-1);

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAYDC1";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = ngayDC1.ToString("yyyy-MM-dd") + strGio_DC;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAYDC2";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_dtNgayDC.ToString("yyyy-MM-dd") + strGio_DC;

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }

//                int i = 0;
//                List<CustomsV3.MSG.MSG81.ItemData> listItems = new List<CustomsV3.MSG.MSG81.ItemData>();
//                foreach (DataRow drhdr in ds.Tables[0].Rows)
//                {
//                    CustomsV3.MSG.MSG81.ItemData item = new CustomsV3.MSG.MSG81.ItemData();
//                    item.Ma_NH_PH = strSender_Code;
//                    item.MST_NH_PH = strSender_TaxNo;
//                    item.Ten_NH_PH = strSender_Name;
//                    item.Loai_CT = strLoai_BLTK;
//                    item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                    item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                    item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();
//                    item.Ma_DV = drhdr["MA_DV"].ToString();
//                    item.Ten_DV = drhdr["TEN_DV"].ToString();
//                    item.Ten_DV_DD = drhdr["TEN_DV_DD"].ToString();
//                    item.Ma_DV_DD = drhdr["MA_DV_DD"].ToString();
//                    item.Ma_HQ_PH = drhdr["MA_HQ_PH"].ToString();
//                    //item.Ma_HQ_CQT = drhdr["MA_HQ_CQT"].ToString();
//                    item.Ma_HQ = drhdr["MA_HQ"].ToString();
//                    item.Ma_LH = drhdr["MA_LH"].ToString();
//                    item.So_TK = drhdr["SOTK"].ToString();
//                    item.Ngay_DK = drhdr["NGAY_DK"].ToString();
//                    item.Ma_LT = drhdr["MA_LT"].ToString();
//                    item.KyHieu_CT = drhdr["KYHIEU_CT"].ToString();
//                    item.So_CT = drhdr["SO_CT"].ToString();
//                    item.Ngay_CT = drhdr["NGAY_CT"].ToString();
//                    item.TTButToan = drhdr["TTBUTTOAN"].ToString();
//                    item.SNBL = drhdr["THOIGIAN_BL"].ToString();
//                    item.Ngay_HL = drhdr["THOIGIANBATDAU_BL"].ToString();
//                    item.Ngay_HHL = drhdr["THOIGIANKETTHUC_BL"].ToString();
//                    item.DienGiai = drhdr["DIENGIAI"].ToString();
//                    item.SoTien = drhdr["TTIEN"].ToString();

//                    listItems.Add(item);
//                }
//                msg81.Data.Item = listItems;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết đối chiếu các giao dịch bảo lãnh thành công (CTUToMSG81)");
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết đối chiếu các giao dịch bảo lãnh thành công (CTUToMSG81)");
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }
//        }
//        public static void DOICHIEUToMSG85(DateTime pv_dtNgayDC, ref MSG85 msg85)
//        {
//            msg85.Data = new CustomsV3.MSG.MSG85.Data();
//            msg85.Data.temp.Ma_NH_DC = strSender_Code;
//            msg85.Data.temp.Ngay_DC = pv_dtNgayDC.ToString("yyyy-MM-dd");
//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   hdr.id,");
//                sbsql.Append("         hdr.transaction_id transaction_id,");
//                sbsql.Append("         hdr.so_tn_ct so_tn_ct,");
//                sbsql.Append("          hdr.ngay_tn_ct ngay_tn_ct,");
//                sbsql.Append("         hdr.ma_nnthue ma_dv,");
//                sbsql.Append("         hdr.ten_nnthue ten_dv,");
//                sbsql.Append("         hdr.ma_dv_dd ma_dv_dd,");
//                sbsql.Append("         hdr.ten_dv_dd ten_dv_dd,");
//                sbsql.Append("         hdr.so_hd so_hd,");
//                sbsql.Append("         to_char(NGAY_HD,'YYYY-MM-DD') NGAY_HD,");
//                sbsql.Append("         hdr.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         hdr.so_ct,hdr.ma_hq,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         hdr.ttien ttien,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (hdr.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD') ngay_ct,");
//                sbsql.Append("         hdr.diengiai diengiai,");
//                sbsql.Append("          songay_bl thoigian_bl,");
//                sbsql.Append("          TO_CHAR(ngay_bl_batdau,'YYYY-MM-DD') thoigianbatdau_bl,");
//                sbsql.Append("          TO_CHAR(ngay_bl_ketthuc,'YYYY-MM-DD') thoigianketthuc_bl,");
//                sbsql.Append("          hdr.vt_don vtdon1,");
//                sbsql.Append("         TO_CHAR(hdr.NGAY_VTD,'RRRR-MM-DD') NGAY_VTD1 ,");
//                sbsql.Append("         hdr.VT_DON2 VT_DON2,");
//                sbsql.Append("         TO_CHAR(hdr.NGAY_VTD2,'RRRR-MM-DD') NGAY_VTD2 ,");
//                sbsql.Append("         hdr.VT_DON3 VT_DON3,");
//                sbsql.Append("         TO_CHAR(hdr.NGAY_VTD3,'RRRR-MM-DD') NGAY_VTD3 ,");
//                sbsql.Append("         hdr.VT_DON4 VT_DON4,");
//                sbsql.Append("         TO_CHAR(hdr.NGAY_VTD4,'RRRR-MM-DD') NGAY_VTD4 ,");
//                sbsql.Append("         hdr.VT_DON5 VT_DON5,");
//                sbsql.Append("         TO_CHAR(hdr.NGAY_VTD5,'RRRR-MM-DD') NGAY_VTD5 ");
//                sbsql.Append("  FROM   tcs_dchieu_nhhq_hdr hdr");
//                sbsql.Append(" WHERE   hdr.transaction_type = 'M85' " + strDKNGAYDC);

//                DateTime ngayDC1 = pv_dtNgayDC.AddDays(-1);

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAYDC1";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = ngayDC1.ToString("yyyy-MM-dd") + strGio_DC;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAYDC2";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_dtNgayDC.ToString("yyyy-MM-dd") + strGio_DC;
//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }

//                int i = 0;
//                List<CustomsV3.MSG.MSG85.ItemData> listItems = new List<CustomsV3.MSG.MSG85.ItemData>();
//                foreach (DataRow drhdr in ds.Tables[0].Rows)
//                {
//                    CustomsV3.MSG.MSG85.ItemData item = new CustomsV3.MSG.MSG85.ItemData();
//                    item.Ma_NH_PH = strSender_Code;
//                    item.MST_NH_PH = strSender_TaxNo;
//                    item.Ten_NH_PH = strSender_Name;
//                    item.Loai_CT = strLoai_BLVTD;
//                    item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                    item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                    item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();
//                    item.Ma_DV = drhdr["MA_DV"].ToString();
//                    item.Ten_DV = drhdr["TEN_DV"].ToString();
//                    item.Ten_DV_DD = drhdr["TEN_DV_DD"].ToString();
//                    item.Ma_DV_DD = drhdr["MA_DV_DD"].ToString();
//                    item.So_HD = drhdr["so_hd"].ToString();
//                    item.Ngay_HD = drhdr["NGAY_hd"].ToString();
//                    item.KyHieu_CT = drhdr["KYHIEU_CT"].ToString();
//                    item.So_CT = drhdr["SO_CT"].ToString();
//                    item.Ngay_CT = drhdr["NGAY_CT"].ToString();
//                    item.TTButToan = drhdr["TTBUTTOAN"].ToString();
//                    item.SNBL = drhdr["THOIGIAN_BL"].ToString();
//                    item.Ngay_HL = drhdr["THOIGIANBATDAU_BL"].ToString();
//                    item.Ngay_HHL = drhdr["THOIGIANKETTHUC_BL"].ToString();
//                    item.DienGiai = drhdr["DIENGIAI"].ToString();
//                    item.SoTien = drhdr["TTIEN"].ToString();
//                    item.Ma_HQ_KB = drhdr["ma_hq"].ToString();
//                    item.So_VD_01 = drhdr["vtdon1"].ToString();
//                    item.Ngay_VD_01 = drhdr["ngay_vtd1"].ToString();
//                    item.So_VD_02 = drhdr["VT_DON2"].ToString();
//                    item.Ngay_VD_02 = drhdr["ngay_vtd2"].ToString();
//                    item.So_VD_03 = drhdr["VT_DON3"].ToString();
//                    item.Ngay_VD_03 = drhdr["ngay_vtd3"].ToString();
//                    item.So_VD_04 = drhdr["VT_DON4"].ToString();
//                    item.Ngay_VD_04 = drhdr["ngay_vtd4"].ToString();
//                    item.So_VD_05 = drhdr["VT_DON5"].ToString();
//                    item.Ngay_VD_05 = drhdr["ngay_vtd5"].ToString();
//                    listItems.Add(item);
//                }
//                msg85.Data.Item = listItems;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết đối chiếu các giao dịch bảo lãnh thành công (CTUToMSG82)");
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết đối chiếu các giao dịch bảo lãnh thành công (CTUToMSG82)");
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }
//        }
//        public static void DOICHIEUToMSG87(DateTime pv_dtNgayDC, ref MSG87 msg87)
//        {
//            msg87.Data = new CustomsV3.MSG.MSG87.Data();
//            msg87.Data.temp.Ma_NH_DC = strSender_Code;
//            msg87.Data.temp.Ngay_DC = pv_dtNgayDC.ToString("yyyy-MM-dd");
//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   hdr.id,");
//                sbsql.Append("         hdr.transaction_id transaction_id,");
//                sbsql.Append("         hdr.so_tn_ct so_tn_ct,");
//                sbsql.Append("         hdr.ngay_tn_ct ngay_tn_ct,");
//                sbsql.Append("         hdr.ma_nnthue ma_dv,");
//                sbsql.Append("         hdr.ten_nnthue ten_dv,");
//                sbsql.Append("         hdr.ma_dv_dd ma_dv_dd,");
//                sbsql.Append("         hdr.ten_dv_dd ten_dv_dd,");
//                sbsql.Append("         hdr.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         hdr.so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         hdr.ttien ttien,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (hdr.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD') ngay_ct,");
//                sbsql.Append("         hdr.diengiai diengiai,");
//                sbsql.Append("          songay_bl thoigian_bl,");
//                sbsql.Append("          TO_CHAR(ngay_bl_batdau,'YYYY-MM-DD') thoigianbatdau_bl,");
//                sbsql.Append("          TO_CHAR(ngay_bl_ketthuc,'YYYY-MM-DD') thoigianketthuc_bl");
//                sbsql.Append("  FROM   tcs_dchieu_nhhq_hdr hdr");
//                sbsql.Append(" WHERE   hdr.transaction_type = 'M87' " + strDKNGAYDC);

//                DateTime ngayDC1 = pv_dtNgayDC.AddDays(-1);

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAYDC1";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = ngayDC1.ToString("yyyy-MM-dd") + strGio_DC;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAYDC2";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_dtNgayDC.ToString("yyyy-MM-dd") + strGio_DC;

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }

//                int i = 0;
//                List<CustomsV3.MSG.MSG87.ItemData> listItems = new List<CustomsV3.MSG.MSG87.ItemData>();
//                foreach (DataRow drhdr in ds.Tables[0].Rows)
//                {
//                    CustomsV3.MSG.MSG87.ItemData item = new CustomsV3.MSG.MSG87.ItemData();
//                    item.Ma_NH_PH = strSender_Code;
//                    item.MST_NH_PH = strSender_TaxNo;
//                    item.Ten_NH_PH = strSender_Name;
//                    item.Loai_CT = strLoai_BLC;
//                    item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                    item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                    item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();
//                    item.Ma_DV = drhdr["MA_DV"].ToString();
//                    item.Ten_DV = drhdr["TEN_DV"].ToString();
//                    item.Ten_DV_DD = drhdr["TEN_DV_DD"].ToString();
//                    item.Ma_DV_DD = drhdr["MA_DV_DD"].ToString();
//                    item.KyHieu_CT = drhdr["KYHIEU_CT"].ToString();
//                    item.So_CT = drhdr["SO_CT"].ToString();
//                    item.Ngay_CT = drhdr["NGAY_CT"].ToString();
//                    item.TTButToan = drhdr["TTBUTTOAN"].ToString();
//                    //item.SNBL = drhdr["THOIGIAN_BL"].ToString();
//                    item.Ngay_HL = drhdr["THOIGIANBATDAU_BL"].ToString();
//                    item.Ngay_HHL = drhdr["THOIGIANKETTHUC_BL"].ToString();
//                    item.DienGiai = drhdr["DIENGIAI"].ToString();
//                    item.SoTien = drhdr["TTIEN"].ToString();

//                    listItems.Add(item);
//                }
//                msg87.Data.Item = listItems;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết đối chiếu các giao dịch bảo lãnh thành công (CTUToMSG83)");
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết đối chiếu các giao dịch bảo lãnh thành công (CTUToMSG83)");
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }
//        }

//        public static void getMSG82(DateTime pv_dtNgayDC, String pv_Loai_BLDC,
//                                         ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                string strXMLOut = "";
//                string tran_id = "";
//                string msg_type = "";
//                if (pv_Loai_BLDC.Equals(strLoai_BLTK))
//                {
//                    MSG81 MSG81 = new MSG81();
//                    MSG81.Data = new CustomsV3.MSG.MSG81.Data();
//                    MSG81.Header = new CustomsV3.MSG.MSG81.Header();

//                    //Gan du lieu
//                    //Gan du lieu Header
//                    MSG81.Header.Message_Version = strMessage_Version;
//                    MSG81.Header.Sender_Code = strSender_Code;
//                    MSG81.Header.Sender_Name = strSender_Name;
//                    MSG81.Header.Transaction_Type = "81";
//                    MSG81.Header.Transaction_Name = "Thông điệp đối chiếu dữ liệu giao dịch thành công";
//                    MSG81.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                    MSG81.Header.Transaction_ID = Guid.NewGuid().ToString();
//                    tran_id = MSG81.Header.Transaction_ID;
//                    msg_type = "MSG81";
//                    InsertDCInfo(tran_id, msg_type, pv_dtNgayDC, pv_Loai_BLDC);
//                    //Gan du lieu Data

//                    DOICHIEUToMSG81(pv_dtNgayDC, ref MSG81);


//                    //Chuyen thanh XML
//                    strXMLOut = MSG81.MSG81toXML(MSG81);
//                }
//                else
//                    if (pv_Loai_BLDC.Equals(strLoai_BLVTD))
//                    {
//                        MSG85 msg85 = new MSG85();
//                        msg85.Data = new CustomsV3.MSG.MSG85.Data();
//                        msg85.Header = new CustomsV3.MSG.MSG85.Header();

//                        //Gan du lieu
//                        //Gan du lieu Header
//                        msg85.Header.Message_Version = strMessage_Version;
//                        msg85.Header.Sender_Code = strSender_Code;
//                        msg85.Header.Sender_Name = strSender_Name;
//                        msg85.Header.Transaction_Type = "85";
//                        msg85.Header.Transaction_Name = "Thông điệp đối chiếu dữ liệu giao dịch thành công";
//                        msg85.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                        msg85.Header.Transaction_ID = Guid.NewGuid().ToString();
//                        tran_id = msg85.Header.Transaction_ID;
//                        msg_type = "MSG85";
//                        InsertDCInfo(tran_id, msg_type, pv_dtNgayDC, pv_Loai_BLDC);
//                        DOICHIEUToMSG85(pv_dtNgayDC, ref msg85);


//                        //Chuyen thanh XML
//                        strXMLOut = msg85.MSG85toXML(msg85);
//                    }
//                    else
//                        if (pv_Loai_BLDC.Equals(strLoai_BLC))
//                        {
//                            MSG87 msg87 = new MSG87();
//                            msg87.Data = new CustomsV3.MSG.MSG87.Data();
//                            msg87.Header = new CustomsV3.MSG.MSG87.Header();

//                            //Gan du lieu
//                            //Gan du lieu Header
//                            msg87.Header.Message_Version = strMessage_Version;
//                            msg87.Header.Sender_Code = strSender_Code;
//                            msg87.Header.Sender_Name = strSender_Name;
//                            msg87.Header.Transaction_Type = "87";
//                            msg87.Header.Transaction_Name = "Thông điệp đối chiếu dữ liệu giao dịch thành công";
//                            msg87.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                            msg87.Header.Transaction_ID = Guid.NewGuid().ToString();
//                            tran_id = msg87.Header.Transaction_ID;
//                            msg_type = "MSG87";
//                            InsertDCInfo(tran_id, msg_type, pv_dtNgayDC, pv_Loai_BLDC);
//                            DOICHIEUToMSG87(pv_dtNgayDC, ref msg87);


//                            //Chuyen thanh XML
//                            strXMLOut = msg87.MSG87toXML(msg87);
//                        }
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC(msg_type, strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //              StringBuilder sb = new StringBuilder();
//                //                sb.Append(strXMLdefin1);
//                //            sb.Append("\r\n");
//                //          sb.Append(strXMLIn);
//                //        strXMLIn = sb.ToString();

//                MSG84 msg84 = new MSG84();
//                MSG84 objTemp = msg84.MSGToObject(strXMLIn);
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;
//                //Verify sign                
//                if (!verifySignMsg(objTemp, "MSG84"))
//                {
//                    pv_ErrorNum = "00001";
//                    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                }
//                UpdateDCInfo(tran_id, msg_type, pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                MSG81 msg81 = new MSG81();
//                //string tran_id = msg81.Header.Transaction_ID;
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 82: Ngay - ");
//                sbErrMsg.Append(pv_dtNgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);
//                pv_ErrorNum = "00000";
//                pv_ErrorMes = ex.Message.ToString();
//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 82: Ngay - ");
//                    sbErrMsg.Append(pv_dtNgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                //pv_ErrorNum = "00000";
//                //pv_ErrorMes = sbErrMsg.ToString();
//                //UpdateDCInfo(tran_id, "MSG81", pv_ErrorNum, pv_ErrorMes);
//            }

//        }

//        #endregion "đối chiếu các giao dịch bảo lãnh thành công M81 - M82"

//        #region "Truy vấn kết quả kết quả đối chiếu giao dịch bảo lãnh chứng từ thành công M83 - M84"

//        public static void MSG84ToDB(MSG84 msg84, DateTime pv_NgayDC)
//        {
//            OracleConnection conn = null;
//            IDbTransaction transCT = null;
//            String strTransactionType = "84";
//            try
//            {
//                conn = GetConnection();
//                transCT = conn.BeginTransaction();

//                //Xoa du lieu dc cu
//                xoaDuLieuDC(strTransactionType, pv_NgayDC, transCT);

//                foreach (CustomsV3.MSG.MSG84.ItemData itdata in msg84.Data.Item)
//                {
//                    Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//                    StringBuilder sbsql = null;
//                    IDbDataParameter[] p = null;

//                    //Insert thong tin header     ds.Tables[0].Rows[0][""];
//                    sbsql = new StringBuilder();
//                    sbsql.Append("INSERT INTO tcs_dchieu_hqnh_hdr (id,");
//                    sbsql.Append("                                 transaction_id,");
//                    sbsql.Append("                                 transaction_type,");
//                    sbsql.Append("                                 shkb,");
//                    sbsql.Append("                                 ten_kb,");
//                    sbsql.Append("                                 ngay_kb,");
//                    sbsql.Append("                                 so_bt,");
//                    sbsql.Append("                                 kyhieu_ct,");
//                    sbsql.Append("                                 so_ct,");
//                    sbsql.Append("                                 ngay_ct,");
//                    sbsql.Append("                                 ngay_bn,");
//                    sbsql.Append("                                 ngay_bc,");
//                    sbsql.Append("                                 ma_nnthue,");
//                    sbsql.Append("                                 ten_nnthue,");
//                    sbsql.Append("                                 ma_cqthu,");
//                    sbsql.Append("                                 ten_cqthu,");
//                    sbsql.Append("                                 so_tk,");
//                    sbsql.Append("                                 ngay_tk,");
//                    sbsql.Append("                                 lhxnk,");
//                    sbsql.Append("                                 vt_lhxnk,");
//                    sbsql.Append("                                 ten_lhxnk,");
//                    sbsql.Append("                                 ttien,");
//                    sbsql.Append("                                 ttien_nt,");
//                    sbsql.Append("                                 trang_thai,");
//                    sbsql.Append("                                 tk_ns,");
//                    sbsql.Append("                                 ten_tk_ns,");
//                    sbsql.Append("                                 ma_nkt,");
//                    sbsql.Append("                                 so_bl,");
//                    sbsql.Append("                                 ngay_bl_batdau,");
//                    sbsql.Append("                                 ngay_bl_ketthuc,");
//                    sbsql.Append("                                 songay_bl,");
//                    sbsql.Append("                                 kieu_bl,");
//                    sbsql.Append("                                 dien_giai,");
//                    sbsql.Append("                                 request_id,");
//                    sbsql.Append("                                 accept_yn,");
//                    sbsql.Append("                                 parent_transaction_id,");
//                    sbsql.Append("                                 ma_hq_ph,");
//                    sbsql.Append("                                 SO_TN_CT, ");
//                    sbsql.Append("                                 NGAY_TN_CT,");
//                    sbsql.Append("                                 kq_dc)");
//                    sbsql.Append("  VALUES   (:id,");
//                    sbsql.Append("            :transaction_id,");
//                    sbsql.Append("            :transaction_type,");
//                    sbsql.Append("            :shkb,");
//                    sbsql.Append("            :ten_kb,");
//                    sbsql.Append("            :ngay_kb,");
//                    sbsql.Append("            :so_bt,");
//                    sbsql.Append("            :kyhieu_ct,");
//                    sbsql.Append("            :so_ct,");
//                    sbsql.Append("            :ngay_ct,");
//                    sbsql.Append("            :ngay_bn,");
//                    sbsql.Append("            :ngay_bc,");
//                    sbsql.Append("            :ma_nnthue,");
//                    sbsql.Append("            :ten_nnthue,");
//                    sbsql.Append("            :ma_cqthu,");
//                    sbsql.Append("            :ten_cqthu,");
//                    sbsql.Append("            :so_tk,");
//                    sbsql.Append("            :ngay_tk,");
//                    sbsql.Append("            :lhxnk,");
//                    sbsql.Append("            :vt_lhxnk,");
//                    sbsql.Append("            :ten_lhxnk,");
//                    sbsql.Append("            :ttien,");
//                    sbsql.Append("            :ttien_nt,");
//                    sbsql.Append("            :trang_thai,");
//                    sbsql.Append("            :tk_ns,");
//                    sbsql.Append("            :ten_tk_ns,");
//                    sbsql.Append("            :ma_nkt,");
//                    sbsql.Append("            :so_bl,");
//                    sbsql.Append("            :ngay_bl_batdau,");
//                    sbsql.Append("            :ngay_bl_ketthuc,");
//                    sbsql.Append("            :songay_bl,");
//                    sbsql.Append("            :kieu_bl,");
//                    sbsql.Append("            :dien_giai,");
//                    sbsql.Append("            :request_id,");
//                    sbsql.Append("            :accept_yn,");
//                    sbsql.Append("            :parent_transaction_id,");
//                    sbsql.Append("            :ma_hq_ph,");
//                    sbsql.Append("            :SO_TN_CT, ");
//                    sbsql.Append("            :NGAY_TN_CT,");
//                    sbsql.Append("            :kq_dc)");

//                    p = new IDbDataParameter[40];

//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "ID";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = DBNull.Value;
//                    p[0].Value = iId;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TRANSACTION_ID";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = DBNull.Value;
//                    if (itdata.Transaction_ID != null)
//                        p[1].Value = itdata.Transaction_ID.Trim();

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "TRANSACTION_TYPE";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strTransactionType;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "SHKB";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "TEN_KB";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "NGAY_KB";
//                    p[5].DbType = DbType.Int64;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = Int64.Parse(pv_NgayDC.ToString("yyyyMMdd"));

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "SO_BT";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = DBNull.Value;
//                    if (itdata.TTButToan != null)
//                        p[6].Value = itdata.TTButToan;//Hoi lai

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "KYHIEU_CT";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = DBNull.Value;
//                    if (itdata.KyHieu_CT != null)
//                        p[7].Value = itdata.KyHieu_CT;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "SO_CT";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = DBNull.Value;
//                    if (itdata.So_CT != null)
//                        p[8].Value = itdata.So_CT;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "NGAY_CT";
//                    p[9].DbType = DbType.Int64;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;
//                    if (itdata.Ngay_CT != null)
//                        p[9].Value = Int64.Parse((DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null)).ToString("yyyyMMdd"));

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "NGAY_BN";
//                    p[10].DbType = DbType.DateTime;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = DBNull.Value;
//                    if (itdata.Ngay_CT != null)
//                        p[10].Value = DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null);

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_BC";
//                    p[11].DbType = DbType.DateTime;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = DBNull.Value;

//                    p[12] = new OracleParameter();
//                    p[12].ParameterName = "MA_NNTHUE";
//                    p[12].DbType = DbType.String;
//                    p[12].Direction = ParameterDirection.Input;
//                    p[12].Value = DBNull.Value;
//                    if (itdata.Ma_DV != null)
//                        p[12].Value = itdata.Ma_DV;

//                    p[13] = new OracleParameter();
//                    p[13].ParameterName = "TEN_NNTHUE";
//                    p[13].DbType = DbType.String;
//                    p[13].Direction = ParameterDirection.Input;
//                    p[13].Value = DBNull.Value;
//                    if (itdata.Ten_DV != null)
//                        p[13].Value = itdata.Ten_DV;

//                    p[14] = new OracleParameter();
//                    p[14].ParameterName = "MA_CQTHU";
//                    p[14].DbType = DbType.String;
//                    p[14].Direction = ParameterDirection.Input;
//                    p[14].Value = DBNull.Value;
//                    if (itdata.Ma_HQ_CQT != null)
//                        p[14].Value = itdata.Ma_HQ_CQT.Trim();

//                    p[15] = new OracleParameter();
//                    p[15].ParameterName = "TEN_CQTHU";
//                    p[15].DbType = DbType.String;
//                    p[15].Direction = ParameterDirection.Input;
//                    p[15].Value = DBNull.Value;

//                    p[16] = new OracleParameter();
//                    p[16].ParameterName = "SO_TK";
//                    p[16].DbType = DbType.String;
//                    p[16].Direction = ParameterDirection.Input;
//                    p[16].Value = DBNull.Value;
//                    if (itdata.So_TK != null)
//                        p[16].Value = itdata.So_TK;

//                    p[17] = new OracleParameter();
//                    p[17].ParameterName = "NGAY_TK";
//                    p[17].DbType = DbType.DateTime;
//                    p[17].Direction = ParameterDirection.Input;
//                    p[17].Value = DBNull.Value;

//                    p[18] = new OracleParameter();
//                    p[18].ParameterName = "LHXNK";
//                    p[18].DbType = DbType.String;
//                    p[18].Direction = ParameterDirection.Input;
//                    p[18].Value = DBNull.Value;
//                    if (itdata.Ma_LH != null)
//                        p[18].Value = itdata.Ma_LH;

//                    p[19] = new OracleParameter();
//                    p[19].ParameterName = "VT_LHXNK";//???
//                    p[19].DbType = DbType.String;
//                    p[19].Direction = ParameterDirection.Input;
//                    p[19].Value = DBNull.Value;


//                    p[20] = new OracleParameter();
//                    p[20].ParameterName = "TEN_LHXNK";
//                    p[20].DbType = DbType.String;
//                    p[20].Direction = ParameterDirection.Input;
//                    p[20].Value = DBNull.Value;

//                    p[21] = new OracleParameter();
//                    p[21].ParameterName = "TTIEN";
//                    p[21].DbType = DbType.String;
//                    p[21].Direction = ParameterDirection.Input;
//                    p[21].Value = DBNull.Value;
//                    Int64 iTongDuNo = 0;
//                    if (itdata.SoTien != null)
//                    {
//                        if (!String.Empty.Equals(itdata.SoTien.Trim()))
//                        {
//                            iTongDuNo += Int64.Parse(itdata.SoTien.Trim());
//                        }
//                    }
//                    p[21].Value = iTongDuNo;

//                    p[22] = new OracleParameter();
//                    p[22].ParameterName = "TTIEN_NT";
//                    p[22].DbType = DbType.String;
//                    p[22].Direction = ParameterDirection.Input;
//                    p[22].Value = DBNull.Value;

//                    p[23] = new OracleParameter();
//                    p[23].ParameterName = "TRANG_THAI";//???
//                    p[23].DbType = DbType.String;
//                    p[23].Direction = ParameterDirection.Input;
//                    p[23].Value = DBNull.Value;

//                    p[24] = new OracleParameter();
//                    p[24].ParameterName = "TK_NS";//???
//                    p[24].DbType = DbType.String;
//                    p[24].Direction = ParameterDirection.Input;
//                    p[24].Value = DBNull.Value;

//                    p[25] = new OracleParameter();
//                    p[25].ParameterName = "TEN_TK_NS";
//                    p[25].DbType = DbType.String;
//                    p[25].Direction = ParameterDirection.Input;
//                    p[25].Value = DBNull.Value;

//                    p[26] = new OracleParameter();
//                    p[26].ParameterName = "MA_NKT";
//                    p[26].DbType = DbType.String;
//                    p[26].Direction = ParameterDirection.Input;
//                    p[26].Value = DBNull.Value;

//                    p[27] = new OracleParameter();
//                    p[27].ParameterName = "SO_BL";//???
//                    p[27].DbType = DbType.String;
//                    p[27].Direction = ParameterDirection.Input;
//                    p[27].Value = DBNull.Value;

//                    p[28] = new OracleParameter();
//                    p[28].ParameterName = "NGAY_BL_BATDAU";//???
//                    p[28].DbType = DbType.DateTime;
//                    p[28].Direction = ParameterDirection.Input;
//                    p[28].Value = DBNull.Value;
//                    if (itdata.Ngay_HL != null)
//                    {
//                        p[28].Value = DateTime.ParseExact(itdata.Ngay_HL, "yyyy-MM-dd", null);
//                    }

//                    p[29] = new OracleParameter();
//                    p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//                    p[29].DbType = DbType.DateTime;
//                    p[29].Direction = ParameterDirection.Input;
//                    if (itdata.Ngay_HHL != null)
//                    {
//                        p[29].Value = DateTime.ParseExact(itdata.Ngay_HHL, "yyyy-MM-dd", null);
//                    }

//                    p[30] = new OracleParameter();
//                    p[30].ParameterName = "SONGAY_BL";//???
//                    p[30].DbType = DbType.Int64;
//                    p[30].Direction = ParameterDirection.Input;
//                    p[30].Value = Int64.Parse(itdata.SNBL);


//                    p[31] = new OracleParameter();
//                    p[31].ParameterName = "KIEU_BL";//???
//                    p[31].DbType = DbType.String;
//                    p[31].Direction = ParameterDirection.Input;
//                    p[31].Value = itdata.Loai_CT;


//                    p[32] = new OracleParameter();
//                    p[32].ParameterName = "DIEN_GIAI";
//                    p[32].DbType = DbType.String;
//                    p[32].Direction = ParameterDirection.Input;
//                    p[32].Value = DBNull.Value;
//                    if (itdata.DienGiai != null)
//                        p[32].Value = itdata.DienGiai;

//                    p[33] = new OracleParameter();
//                    p[33].ParameterName = "REQUEST_ID";//???
//                    p[33].DbType = DbType.String;
//                    p[33].Direction = ParameterDirection.Input;
//                    p[33].Value = DBNull.Value;

//                    p[34] = new OracleParameter();
//                    p[34].ParameterName = "ACCEPT_YN";//???
//                    p[34].DbType = DbType.String;
//                    p[34].Direction = ParameterDirection.Input;
//                    p[34].Value = DBNull.Value;

//                    p[35] = new OracleParameter();
//                    p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//                    p[35].DbType = DbType.String;
//                    p[35].Direction = ParameterDirection.Input;
//                    p[35].Value = DBNull.Value;

//                    p[36] = new OracleParameter();
//                    p[36].ParameterName = "MA_HQ_PH";
//                    p[36].DbType = DbType.String;
//                    p[36].Direction = ParameterDirection.Input;
//                    p[36].Value = DBNull.Value;
//                    if (itdata.Ma_HQ_PH != null)
//                        p[36].Value = itdata.Ma_HQ_PH;

//                    p[37] = new OracleParameter();
//                    p[37].ParameterName = "So_TN_CT";
//                    p[37].DbType = DbType.String;
//                    p[37].Direction = ParameterDirection.Input;
//                    p[37].Value = DBNull.Value;
//                    if (itdata.So_TN_CT != null)
//                        p[37].Value = itdata.So_TN_CT.Trim();

//                    p[38] = new OracleParameter();
//                    p[38].ParameterName = "Ngay_TN_CT";
//                    p[38].DbType = DbType.String;
//                    p[38].Direction = ParameterDirection.Input;
//                    p[38].Value = DBNull.Value;
//                    if (itdata.Ngay_TN_CT != null)
//                        p[38].Value = itdata.Ngay_TN_CT.Trim();

//                    p[39] = new OracleParameter();
//                    p[39].ParameterName = "KQ_DC";
//                    p[39].DbType = DbType.String;
//                    p[39].Direction = ParameterDirection.Input;
//                    p[39].Value = DBNull.Value;
//                    //p[37].Value = itdata.KQ_DC;
//                    if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//                    {
//                        p[39].Value = "OK";
//                    }
//                    else
//                    {
//                        p[39].Value = itdata.KQ_DC;
//                    }

//                    ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);

//                }
//                transCT.Commit();


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu bao lanh thành công : Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("insert vao CSDL\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu bao lanh thành công : Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("insert vao CSDL\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }


//        }

//        public static void MSG86ToDB(MSG86 msg86, DateTime pv_NgayDC)
//        {
//            OracleConnection conn = null;
//            IDbTransaction transCT = null;
//            String strTransactionType = "86";
//            try
//            {
//                conn = GetConnection();
//                transCT = conn.BeginTransaction();

//                //Xoa du lieu dc cu
//                xoaDuLieuDC(strTransactionType, pv_NgayDC, transCT);

//                foreach (CustomsV3.MSG.MSG86.ItemData itdata in msg86.Data.Item)
//                {
//                    Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//                    StringBuilder sbsql = null;
//                    IDbDataParameter[] p = null;

//                    //Insert thong tin header     ds.Tables[0].Rows[0][""];
//                    sbsql = new StringBuilder();
//                    sbsql.Append("INSERT INTO tcs_dchieu_hqnh_hdr (id,");
//                    sbsql.Append("                                 transaction_id,");
//                    sbsql.Append("                                 transaction_type,");
//                    sbsql.Append("                                 shkb,");
//                    sbsql.Append("                                 ten_kb,");
//                    sbsql.Append("                                 ngay_kb,");
//                    sbsql.Append("                                 so_bt,");
//                    sbsql.Append("                                 kyhieu_ct,");
//                    sbsql.Append("                                 so_ct,");
//                    sbsql.Append("                                 ngay_ct,");
//                    sbsql.Append("                                 ngay_bn,");
//                    sbsql.Append("                                 ngay_bc,");
//                    sbsql.Append("                                 ma_nnthue,");
//                    sbsql.Append("                                 ten_nnthue,");
//                    sbsql.Append("                                 ma_cqthu,");
//                    sbsql.Append("                                 ten_cqthu,");
//                    sbsql.Append("                                 so_hd,");
//                    sbsql.Append("                                 ngay_hd,");
//                    sbsql.Append("                                 lhxnk,");
//                    sbsql.Append("                                 vt_lhxnk,");
//                    sbsql.Append("                                 ten_lhxnk,");
//                    sbsql.Append("                                 ttien,");
//                    sbsql.Append("                                 ttien_nt,");
//                    sbsql.Append("                                 trang_thai,");
//                    sbsql.Append("                                 tk_ns,");
//                    sbsql.Append("                                 ten_tk_ns,");
//                    sbsql.Append("                                 ma_nkt,");
//                    sbsql.Append("                                 so_bl,");
//                    sbsql.Append("                                 ngay_bl_batdau,");
//                    sbsql.Append("                                 ngay_bl_ketthuc,");
//                    sbsql.Append("                                 songay_bl,");
//                    sbsql.Append("                                 kieu_bl,");
//                    sbsql.Append("                                 dien_giai,");
//                    sbsql.Append("                                 request_id,");
//                    sbsql.Append("                                 accept_yn,");
//                    sbsql.Append("                                 parent_transaction_id,");
//                    sbsql.Append("                                 ma_hq_ph,");
//                    sbsql.Append("                                 so_tn_ct, ");
//                    sbsql.Append("                                 ngay_tn_ct,");
//                    sbsql.Append("                                 vt_don,");
//                    sbsql.Append("                                 ngay_vtd,");
//                    sbsql.Append("                                 vt_don2,");
//                    sbsql.Append("                                 ngay_vtd2,");
//                    sbsql.Append("                                 vt_don3,");
//                    sbsql.Append("                                 ngay_vtd3,");
//                    sbsql.Append("                                 vt_don4,");
//                    sbsql.Append("                                 ngay_vtd4,");
//                    sbsql.Append("                                 vt_don5,");
//                    sbsql.Append("                                 ngay_vtd5,");
//                    sbsql.Append("                                 kq_dc)");

//                    sbsql.Append("  VALUES   (:id,");
//                    sbsql.Append("            :transaction_id,");
//                    sbsql.Append("            :transaction_type,");
//                    sbsql.Append("            :shkb,");
//                    sbsql.Append("            :ten_kb,");
//                    sbsql.Append("            :ngay_kb,");
//                    sbsql.Append("            :so_bt,");
//                    sbsql.Append("            :kyhieu_ct,");
//                    sbsql.Append("            :so_ct,");
//                    sbsql.Append("            :ngay_ct,");
//                    sbsql.Append("            :ngay_bn,");
//                    sbsql.Append("            :ngay_bc,");
//                    sbsql.Append("            :ma_nnthue,");
//                    sbsql.Append("            :ten_nnthue,");
//                    sbsql.Append("            :ma_cqthu,");
//                    sbsql.Append("            :ten_cqthu,");
//                    sbsql.Append("            :so_hd,");
//                    sbsql.Append("            :ngay_hd,");
//                    sbsql.Append("            :lhxnk,");
//                    sbsql.Append("            :vt_lhxnk,");
//                    sbsql.Append("            :ten_lhxnk,");
//                    sbsql.Append("            :ttien,");
//                    sbsql.Append("            :ttien_nt,");
//                    sbsql.Append("            :trang_thai,");
//                    sbsql.Append("            :tk_ns,");
//                    sbsql.Append("            :ten_tk_ns,");
//                    sbsql.Append("            :ma_nkt,");
//                    sbsql.Append("            :so_bl,");
//                    sbsql.Append("            :ngay_bl_batdau,");
//                    sbsql.Append("            :ngay_bl_ketthuc,");
//                    sbsql.Append("            :songay_bl,");
//                    sbsql.Append("            :kieu_bl,");
//                    sbsql.Append("            :dien_giai,");
//                    sbsql.Append("            :request_id,");
//                    sbsql.Append("            :accept_yn,");
//                    sbsql.Append("            :parent_transaction_id,");
//                    sbsql.Append("            :ma_hq_ph,");
//                    sbsql.Append("            :SO_TN_CT, ");
//                    sbsql.Append("            :NGAY_TN_CT,");
//                    sbsql.Append("            :vt_don,");
//                    sbsql.Append("            :ngay_vtd,");
//                    sbsql.Append("            :vt_don2,");
//                    sbsql.Append("            :ngay_vtd2,");
//                    sbsql.Append("            :vt_don3,");
//                    sbsql.Append("            :ngay_vtd3,");
//                    sbsql.Append("            :vt_don4,");
//                    sbsql.Append("            :ngay_vtd4,");
//                    sbsql.Append("            :vt_don5,");
//                    sbsql.Append("            :ngay_vtd5,");
//                    sbsql.Append("            :kq_dc)");

//                    p = new IDbDataParameter[50];

//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "ID";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = DBNull.Value;
//                    p[0].Value = iId;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TRANSACTION_ID";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = DBNull.Value;
//                    if (itdata.Transaction_ID != null)
//                        p[1].Value = itdata.Transaction_ID.Trim();

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "TRANSACTION_TYPE";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strTransactionType;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "SHKB";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "TEN_KB";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "NGAY_KB";
//                    p[5].DbType = DbType.Int64;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = Int64.Parse(pv_NgayDC.ToString("yyyyMMdd"));

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "SO_BT";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = DBNull.Value;
//                    if (itdata.TTButToan != null)
//                        p[6].Value = itdata.TTButToan;//Hoi lai

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "KYHIEU_CT";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = DBNull.Value;
//                    if (itdata.KyHieu_CT != null)
//                        p[7].Value = itdata.KyHieu_CT;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "SO_CT";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = DBNull.Value;
//                    if (itdata.So_CT != null)
//                        p[8].Value = itdata.So_CT;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "NGAY_CT";
//                    p[9].DbType = DbType.Int64;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;
//                    if (itdata.Ngay_CT != null)
//                        p[9].Value = Int64.Parse((DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null)).ToString("yyyyMMdd"));

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "NGAY_BN";
//                    p[10].DbType = DbType.DateTime;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = DBNull.Value;
//                    if (itdata.Ngay_CT != null)
//                        p[10].Value = DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null);

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_BC";
//                    p[11].DbType = DbType.DateTime;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = DBNull.Value;

//                    p[12] = new OracleParameter();
//                    p[12].ParameterName = "MA_NNTHUE";
//                    p[12].DbType = DbType.String;
//                    p[12].Direction = ParameterDirection.Input;
//                    p[12].Value = DBNull.Value;
//                    if (itdata.Ma_DV != null)
//                        p[12].Value = itdata.Ma_DV;

//                    p[13] = new OracleParameter();
//                    p[13].ParameterName = "TEN_NNTHUE";
//                    p[13].DbType = DbType.String;
//                    p[13].Direction = ParameterDirection.Input;
//                    p[13].Value = DBNull.Value;
//                    if (itdata.Ten_DV != null)
//                        p[13].Value = itdata.Ten_DV;

//                    p[14] = new OracleParameter();
//                    p[14].ParameterName = "MA_CQTHU";
//                    p[14].DbType = DbType.String;
//                    p[14].Direction = ParameterDirection.Input;
//                    p[14].Value = DBNull.Value;
//                    //if (itdata.Ma_HQ_CQT != null)
//                    //    p[14].Value = itdata.Ma_HQ_CQT.Trim();

//                    p[15] = new OracleParameter();
//                    p[15].ParameterName = "TEN_CQTHU";
//                    p[15].DbType = DbType.String;
//                    p[15].Direction = ParameterDirection.Input;
//                    p[15].Value = DBNull.Value;

//                    p[16] = new OracleParameter();
//                    p[16].ParameterName = "SO_HD";
//                    p[16].DbType = DbType.String;
//                    p[16].Direction = ParameterDirection.Input;
//                    p[16].Value = DBNull.Value;
//                    if (itdata.So_HD != null)
//                        p[16].Value = itdata.So_HD;

//                    p[17] = new OracleParameter();
//                    p[17].ParameterName = "NGAY_HD";
//                    p[17].DbType = DbType.DateTime;
//                    p[17].Direction = ParameterDirection.Input;
//                    p[17].Value = DBNull.Value;
//                    if (itdata.Ngay_HD != null)
//                        p[17].Value = itdata.Ngay_HD;

//                    p[18] = new OracleParameter();
//                    p[18].ParameterName = "LHXNK";
//                    p[18].DbType = DbType.String;
//                    p[18].Direction = ParameterDirection.Input;
//                    p[18].Value = DBNull.Value;
//                    //if (itdata.Ma_LH != null)
//                    //    p[18].Value = itdata.Ma_LH;

//                    p[19] = new OracleParameter();
//                    p[19].ParameterName = "VT_LHXNK";//???
//                    p[19].DbType = DbType.String;
//                    p[19].Direction = ParameterDirection.Input;
//                    p[19].Value = DBNull.Value;


//                    p[20] = new OracleParameter();
//                    p[20].ParameterName = "TEN_LHXNK";
//                    p[20].DbType = DbType.String;
//                    p[20].Direction = ParameterDirection.Input;
//                    p[20].Value = DBNull.Value;

//                    p[21] = new OracleParameter();
//                    p[21].ParameterName = "TTIEN";
//                    p[21].DbType = DbType.String;
//                    p[21].Direction = ParameterDirection.Input;
//                    p[21].Value = DBNull.Value;
//                    Int64 iTongDuNo = 0;
//                    if (itdata.SoTien != null)
//                    {
//                        if (!String.Empty.Equals(itdata.SoTien.Trim()))
//                        {
//                            iTongDuNo += Int64.Parse(itdata.SoTien.Trim());
//                        }
//                    }
//                    p[21].Value = iTongDuNo;

//                    p[22] = new OracleParameter();
//                    p[22].ParameterName = "TTIEN_NT";
//                    p[22].DbType = DbType.String;
//                    p[22].Direction = ParameterDirection.Input;
//                    p[22].Value = DBNull.Value;

//                    p[23] = new OracleParameter();
//                    p[23].ParameterName = "TRANG_THAI";//???
//                    p[23].DbType = DbType.String;
//                    p[23].Direction = ParameterDirection.Input;
//                    p[23].Value = DBNull.Value;

//                    p[24] = new OracleParameter();
//                    p[24].ParameterName = "TK_NS";//???
//                    p[24].DbType = DbType.String;
//                    p[24].Direction = ParameterDirection.Input;
//                    p[24].Value = DBNull.Value;

//                    p[25] = new OracleParameter();
//                    p[25].ParameterName = "TEN_TK_NS";
//                    p[25].DbType = DbType.String;
//                    p[25].Direction = ParameterDirection.Input;
//                    p[25].Value = DBNull.Value;

//                    p[26] = new OracleParameter();
//                    p[26].ParameterName = "MA_NKT";
//                    p[26].DbType = DbType.String;
//                    p[26].Direction = ParameterDirection.Input;
//                    p[26].Value = DBNull.Value;

//                    p[27] = new OracleParameter();
//                    p[27].ParameterName = "SO_BL";//???
//                    p[27].DbType = DbType.String;
//                    p[27].Direction = ParameterDirection.Input;
//                    p[27].Value = DBNull.Value;

//                    p[28] = new OracleParameter();
//                    p[28].ParameterName = "NGAY_BL_BATDAU";//???
//                    p[28].DbType = DbType.DateTime;
//                    p[28].Direction = ParameterDirection.Input;
//                    p[28].Value = DBNull.Value;
//                    if (itdata.Ngay_HL != null)
//                    {
//                        p[28].Value = DateTime.ParseExact(itdata.Ngay_HL, "yyyy-MM-dd", null);
//                    }

//                    p[29] = new OracleParameter();
//                    p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//                    p[29].DbType = DbType.DateTime;
//                    p[29].Direction = ParameterDirection.Input;
//                    if (itdata.Ngay_HHL != null)
//                    {
//                        p[29].Value = DateTime.ParseExact(itdata.Ngay_HHL, "yyyy-MM-dd", null);
//                    }

//                    p[30] = new OracleParameter();
//                    p[30].ParameterName = "SONGAY_BL";//???
//                    p[30].DbType = DbType.Int64;
//                    p[30].Direction = ParameterDirection.Input;
//                    p[30].Value = Int64.Parse(itdata.SNBL);


//                    p[31] = new OracleParameter();
//                    p[31].ParameterName = "KIEU_BL";//???
//                    p[31].DbType = DbType.String;
//                    p[31].Direction = ParameterDirection.Input;
//                    p[31].Value = itdata.Loai_CT;


//                    p[32] = new OracleParameter();
//                    p[32].ParameterName = "DIEN_GIAI";
//                    p[32].DbType = DbType.String;
//                    p[32].Direction = ParameterDirection.Input;
//                    p[32].Value = DBNull.Value;
//                    if (itdata.DienGiai != null)
//                        p[32].Value = itdata.DienGiai;

//                    p[33] = new OracleParameter();
//                    p[33].ParameterName = "REQUEST_ID";//???
//                    p[33].DbType = DbType.String;
//                    p[33].Direction = ParameterDirection.Input;
//                    p[33].Value = DBNull.Value;

//                    p[34] = new OracleParameter();
//                    p[34].ParameterName = "ACCEPT_YN";//???
//                    p[34].DbType = DbType.String;
//                    p[34].Direction = ParameterDirection.Input;
//                    p[34].Value = DBNull.Value;

//                    p[35] = new OracleParameter();
//                    p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//                    p[35].DbType = DbType.String;
//                    p[35].Direction = ParameterDirection.Input;
//                    p[35].Value = DBNull.Value;

//                    p[36] = new OracleParameter();
//                    p[36].ParameterName = "MA_HQ_PH";
//                    p[36].DbType = DbType.String;
//                    p[36].Direction = ParameterDirection.Input;
//                    p[36].Value = DBNull.Value;
//                    if (itdata.Ma_HQ_KB != null)
//                        p[36].Value = itdata.Ma_HQ_KB;

//                    p[37] = new OracleParameter();
//                    p[37].ParameterName = "So_TN_CT";
//                    p[37].DbType = DbType.String;
//                    p[37].Direction = ParameterDirection.Input;
//                    p[37].Value = DBNull.Value;
//                    if (itdata.So_TN_CT != null)
//                        p[37].Value = itdata.So_TN_CT.Trim();

//                    p[38] = new OracleParameter();
//                    p[38].ParameterName = "Ngay_TN_CT";
//                    p[38].DbType = DbType.String;
//                    p[38].Direction = ParameterDirection.Input;
//                    p[38].Value = DBNull.Value;
//                    if (itdata.Ngay_TN_CT != null)
//                        p[38].Value = itdata.Ngay_TN_CT.Trim();

//                    p[39] = new OracleParameter();
//                    p[39].ParameterName = "VT_DON";
//                    p[39].DbType = DbType.String;
//                    p[39].Direction = ParameterDirection.Input;
//                    p[39].Value = DBNull.Value;
//                    if (itdata.So_VD_01 != null)
//                        p[39].Value = itdata.So_VD_01.Trim();

//                    p[40] = new OracleParameter();
//                    p[40].ParameterName = "ngay_vtd";
//                    p[40].DbType = DbType.String;
//                    p[40].Direction = ParameterDirection.Input;
//                    p[40].Value = DBNull.Value;
//                    if (itdata.Ngay_VD_01 != null)
//                        p[40].Value = itdata.Ngay_VD_01.Trim();

//                    p[41] = new OracleParameter();
//                    p[41].ParameterName = "VT_DON2";
//                    p[41].DbType = DbType.String;
//                    p[41].Direction = ParameterDirection.Input;
//                    p[41].Value = DBNull.Value;
//                    if (itdata.So_VD_02 != null)
//                        p[41].Value = itdata.So_VD_02.Trim();

//                    p[42] = new OracleParameter();
//                    p[42].ParameterName = "ngay_vtd2";
//                    p[42].DbType = DbType.String;
//                    p[42].Direction = ParameterDirection.Input;
//                    p[42].Value = DBNull.Value;
//                    if (itdata.Ngay_VD_02 != null)
//                        p[42].Value = itdata.Ngay_VD_02.Trim();

//                    p[43] = new OracleParameter();
//                    p[43].ParameterName = "VT_DON3";
//                    p[43].DbType = DbType.String;
//                    p[43].Direction = ParameterDirection.Input;
//                    p[43].Value = DBNull.Value;
//                    if (itdata.So_VD_03 != null)
//                        p[43].Value = itdata.So_VD_03.Trim();

//                    p[44] = new OracleParameter();
//                    p[44].ParameterName = "ngay_vtd3";
//                    p[44].DbType = DbType.String;
//                    p[44].Direction = ParameterDirection.Input;
//                    p[44].Value = DBNull.Value;
//                    if (itdata.Ngay_VD_03 != null)
//                        p[44].Value = itdata.Ngay_VD_03.Trim();

//                    p[45] = new OracleParameter();
//                    p[45].ParameterName = "VT_DON4";
//                    p[45].DbType = DbType.String;
//                    p[45].Direction = ParameterDirection.Input;
//                    p[45].Value = DBNull.Value;
//                    if (itdata.So_VD_04 != null)
//                        p[45].Value = itdata.So_VD_04.Trim();

//                    p[46] = new OracleParameter();
//                    p[46].ParameterName = "ngay_vtd4";
//                    p[46].DbType = DbType.String;
//                    p[46].Direction = ParameterDirection.Input;
//                    p[46].Value = DBNull.Value;
//                    if (itdata.Ngay_VD_04 != null)
//                        p[46].Value = itdata.Ngay_VD_04.Trim();

//                    p[47] = new OracleParameter();
//                    p[47].ParameterName = "VT_DON5";
//                    p[47].DbType = DbType.String;
//                    p[47].Direction = ParameterDirection.Input;
//                    p[47].Value = DBNull.Value;
//                    if (itdata.So_VD_05 != null)
//                        p[47].Value = itdata.So_VD_05.Trim();

//                    p[48] = new OracleParameter();
//                    p[48].ParameterName = "ngay_vtd5";
//                    p[48].DbType = DbType.String;
//                    p[48].Direction = ParameterDirection.Input;
//                    p[48].Value = DBNull.Value;
//                    if (itdata.Ngay_VD_05 != null)
//                        p[48].Value = itdata.Ngay_VD_05.Trim();

//                    p[49] = new OracleParameter();
//                    p[49].ParameterName = "KQ_DC";
//                    p[49].DbType = DbType.String;
//                    p[49].Direction = ParameterDirection.Input;
//                    p[49].Value = DBNull.Value;
//                    //p[37].Value = itdata.KQ_DC;
//                    if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//                    {
//                        p[49].Value = "OK";
//                    }
//                    else
//                    {
//                        p[49].Value = itdata.KQ_DC;
//                    }
//                    ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);

//                }
//                transCT.Commit();


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu bao lanh thành công : Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("insert vao CSDL\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu bao lanh thành công : Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("insert vao CSDL\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }


//        }
//        public static void MSG88ToDB(MSG88 msg88, DateTime pv_NgayDC)
//        {
//            OracleConnection conn = null;
//            IDbTransaction transCT = null;
//            String strTransactionType = "88";
//            try
//            {
//                conn = GetConnection();
//                transCT = conn.BeginTransaction();

//                //Xoa du lieu dc cu
//                xoaDuLieuDC(strTransactionType, pv_NgayDC, transCT);

//                foreach (CustomsV3.MSG.MSG88.ItemData itdata in msg88.Data.Item)
//                {
//                    Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//                    StringBuilder sbsql = null;
//                    IDbDataParameter[] p = null;

//                    //Insert thong tin header     ds.Tables[0].Rows[0][""];
//                    sbsql = new StringBuilder();
//                    sbsql.Append("INSERT INTO tcs_dchieu_hqnh_hdr (id,");
//                    sbsql.Append("                                 transaction_id,");
//                    sbsql.Append("                                 transaction_type,");
//                    sbsql.Append("                                 shkb,");
//                    sbsql.Append("                                 ten_kb,");
//                    sbsql.Append("                                 ngay_kb,");
//                    sbsql.Append("                                 so_bt,");
//                    sbsql.Append("                                 kyhieu_ct,");
//                    sbsql.Append("                                 so_ct,");
//                    sbsql.Append("                                 ngay_ct,");
//                    sbsql.Append("                                 ngay_bn,");
//                    sbsql.Append("                                 ngay_bc,");
//                    sbsql.Append("                                 ma_nnthue,");
//                    sbsql.Append("                                 ten_nnthue,");
//                    sbsql.Append("                                 ma_cqthu,");
//                    sbsql.Append("                                 ten_cqthu,");
//                    sbsql.Append("                                 so_tk,");
//                    sbsql.Append("                                 ngay_tk,");
//                    sbsql.Append("                                 lhxnk,");
//                    sbsql.Append("                                 vt_lhxnk,");
//                    sbsql.Append("                                 ten_lhxnk,");
//                    sbsql.Append("                                 ttien,");
//                    sbsql.Append("                                 ttien_nt,");
//                    sbsql.Append("                                 trang_thai,");
//                    sbsql.Append("                                 tk_ns,");
//                    sbsql.Append("                                 ten_tk_ns,");
//                    sbsql.Append("                                 ma_nkt,");
//                    sbsql.Append("                                 so_bl,");
//                    sbsql.Append("                                 ngay_bl_batdau,");
//                    sbsql.Append("                                 ngay_bl_ketthuc,");
//                    sbsql.Append("                                 songay_bl,");
//                    sbsql.Append("                                 kieu_bl,");
//                    sbsql.Append("                                 dien_giai,");
//                    sbsql.Append("                                 request_id,");
//                    sbsql.Append("                                 accept_yn,");
//                    sbsql.Append("                                 parent_transaction_id,");
//                    sbsql.Append("                                 ma_hq_ph,");
//                    sbsql.Append("                                 SO_TN_CT, ");
//                    sbsql.Append("                                 NGAY_TN_CT,");
//                    sbsql.Append("                                 kq_dc)");

//                    sbsql.Append("  VALUES   (:id,");
//                    sbsql.Append("            :transaction_id,");
//                    sbsql.Append("            :transaction_type,");
//                    sbsql.Append("            :shkb,");
//                    sbsql.Append("            :ten_kb,");
//                    sbsql.Append("            :ngay_kb,");
//                    sbsql.Append("            :so_bt,");
//                    sbsql.Append("            :kyhieu_ct,");
//                    sbsql.Append("            :so_ct,");
//                    sbsql.Append("            :ngay_ct,");
//                    sbsql.Append("            :ngay_bn,");
//                    sbsql.Append("            :ngay_bc,");
//                    sbsql.Append("            :ma_nnthue,");
//                    sbsql.Append("            :ten_nnthue,");
//                    sbsql.Append("            :ma_cqthu,");
//                    sbsql.Append("            :ten_cqthu,");
//                    sbsql.Append("            :so_tk,");
//                    sbsql.Append("            :ngay_tk,");
//                    sbsql.Append("            :lhxnk,");
//                    sbsql.Append("            :vt_lhxnk,");
//                    sbsql.Append("            :ten_lhxnk,");
//                    sbsql.Append("            :ttien,");
//                    sbsql.Append("            :ttien_nt,");
//                    sbsql.Append("            :trang_thai,");
//                    sbsql.Append("            :tk_ns,");
//                    sbsql.Append("            :ten_tk_ns,");
//                    sbsql.Append("            :ma_nkt,");
//                    sbsql.Append("            :so_bl,");
//                    sbsql.Append("            :ngay_bl_batdau,");
//                    sbsql.Append("            :ngay_bl_ketthuc,");
//                    sbsql.Append("            :songay_bl,");
//                    sbsql.Append("            :kieu_bl,");
//                    sbsql.Append("            :dien_giai,");
//                    sbsql.Append("            :request_id,");
//                    sbsql.Append("            :accept_yn,");
//                    sbsql.Append("            :parent_transaction_id,");
//                    sbsql.Append("            :ma_hq_ph,");
//                    sbsql.Append("            :SO_TN_CT, ");
//                    sbsql.Append("            :NGAY_TN_CT,");
//                    sbsql.Append("            :kq_dc)");

//                    p = new IDbDataParameter[40];

//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "ID";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = DBNull.Value;
//                    p[0].Value = iId;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TRANSACTION_ID";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = DBNull.Value;
//                    if (itdata.Transaction_ID != null)
//                        p[1].Value = itdata.Transaction_ID.Trim();

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "TRANSACTION_TYPE";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strTransactionType;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "SHKB";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "TEN_KB";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "NGAY_KB";
//                    p[5].DbType = DbType.Int64;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = Int64.Parse(pv_NgayDC.ToString("yyyyMMdd"));

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "SO_BT";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = DBNull.Value;
//                    if (itdata.TTButToan != null)
//                        p[6].Value = itdata.TTButToan;//Hoi lai

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "KYHIEU_CT";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = DBNull.Value;
//                    if (itdata.KyHieu_CT != null)
//                        p[7].Value = itdata.KyHieu_CT;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "SO_CT";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = DBNull.Value;
//                    if (itdata.So_CT != null)
//                        p[8].Value = itdata.So_CT;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "NGAY_CT";
//                    p[9].DbType = DbType.Int64;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;
//                    if (itdata.Ngay_CT != null)
//                        p[9].Value = Int64.Parse((DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null)).ToString("yyyyMMdd"));

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "NGAY_BN";
//                    p[10].DbType = DbType.DateTime;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = DBNull.Value;
//                    if (itdata.Ngay_CT != null)
//                        p[10].Value = DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null);

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_BC";
//                    p[11].DbType = DbType.DateTime;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = DBNull.Value;

//                    p[12] = new OracleParameter();
//                    p[12].ParameterName = "MA_NNTHUE";
//                    p[12].DbType = DbType.String;
//                    p[12].Direction = ParameterDirection.Input;
//                    p[12].Value = DBNull.Value;
//                    if (itdata.Ma_DV != null)
//                        p[12].Value = itdata.Ma_DV;

//                    p[13] = new OracleParameter();
//                    p[13].ParameterName = "TEN_NNTHUE";
//                    p[13].DbType = DbType.String;
//                    p[13].Direction = ParameterDirection.Input;
//                    p[13].Value = DBNull.Value;
//                    if (itdata.Ten_DV != null)
//                        p[13].Value = itdata.Ten_DV;

//                    p[14] = new OracleParameter();
//                    p[14].ParameterName = "MA_CQTHU";
//                    p[14].DbType = DbType.String;
//                    p[14].Direction = ParameterDirection.Input;
//                    p[14].Value = DBNull.Value;
//                    //if (itdata.Ma_HQ_CQT != null)
//                    //    p[14].Value = itdata.Ma_HQ_CQT.Trim();

//                    p[15] = new OracleParameter();
//                    p[15].ParameterName = "TEN_CQTHU";
//                    p[15].DbType = DbType.String;
//                    p[15].Direction = ParameterDirection.Input;
//                    p[15].Value = DBNull.Value;

//                    p[16] = new OracleParameter();
//                    p[16].ParameterName = "SO_TK";
//                    p[16].DbType = DbType.String;
//                    p[16].Direction = ParameterDirection.Input;
//                    p[16].Value = DBNull.Value;
//                    //if (itdata.So_TK != null)
//                    //    p[16].Value = itdata.So_TK;

//                    p[17] = new OracleParameter();
//                    p[17].ParameterName = "NGAY_TK";
//                    p[17].DbType = DbType.DateTime;
//                    p[17].Direction = ParameterDirection.Input;
//                    p[17].Value = DBNull.Value;

//                    p[18] = new OracleParameter();
//                    p[18].ParameterName = "LHXNK";
//                    p[18].DbType = DbType.String;
//                    p[18].Direction = ParameterDirection.Input;
//                    p[18].Value = DBNull.Value;
//                    //if (itdata.Ma_LH != null)
//                    //    p[18].Value = itdata.Ma_LH;

//                    p[19] = new OracleParameter();
//                    p[19].ParameterName = "VT_LHXNK";//???
//                    p[19].DbType = DbType.String;
//                    p[19].Direction = ParameterDirection.Input;
//                    p[19].Value = DBNull.Value;


//                    p[20] = new OracleParameter();
//                    p[20].ParameterName = "TEN_LHXNK";
//                    p[20].DbType = DbType.String;
//                    p[20].Direction = ParameterDirection.Input;
//                    p[20].Value = DBNull.Value;

//                    p[21] = new OracleParameter();
//                    p[21].ParameterName = "TTIEN";
//                    p[21].DbType = DbType.String;
//                    p[21].Direction = ParameterDirection.Input;
//                    p[21].Value = DBNull.Value;
//                    Int64 iTongDuNo = 0;
//                    if (itdata.SoTien != null)
//                    {
//                        if (!String.Empty.Equals(itdata.SoTien.Trim()))
//                        {
//                            iTongDuNo += Int64.Parse(itdata.SoTien.Trim());
//                        }
//                    }
//                    p[21].Value = iTongDuNo;

//                    p[22] = new OracleParameter();
//                    p[22].ParameterName = "TTIEN_NT";
//                    p[22].DbType = DbType.String;
//                    p[22].Direction = ParameterDirection.Input;
//                    p[22].Value = DBNull.Value;

//                    p[23] = new OracleParameter();
//                    p[23].ParameterName = "TRANG_THAI";//???
//                    p[23].DbType = DbType.String;
//                    p[23].Direction = ParameterDirection.Input;
//                    p[23].Value = DBNull.Value;

//                    p[24] = new OracleParameter();
//                    p[24].ParameterName = "TK_NS";//???
//                    p[24].DbType = DbType.String;
//                    p[24].Direction = ParameterDirection.Input;
//                    p[24].Value = DBNull.Value;

//                    p[25] = new OracleParameter();
//                    p[25].ParameterName = "TEN_TK_NS";
//                    p[25].DbType = DbType.String;
//                    p[25].Direction = ParameterDirection.Input;
//                    p[25].Value = DBNull.Value;

//                    p[26] = new OracleParameter();
//                    p[26].ParameterName = "MA_NKT";
//                    p[26].DbType = DbType.String;
//                    p[26].Direction = ParameterDirection.Input;
//                    p[26].Value = DBNull.Value;

//                    p[27] = new OracleParameter();
//                    p[27].ParameterName = "SO_BL";//???
//                    p[27].DbType = DbType.String;
//                    p[27].Direction = ParameterDirection.Input;
//                    p[27].Value = DBNull.Value;

//                    p[28] = new OracleParameter();
//                    p[28].ParameterName = "NGAY_BL_BATDAU";//???
//                    p[28].DbType = DbType.DateTime;
//                    p[28].Direction = ParameterDirection.Input;
//                    p[28].Value = DBNull.Value;
//                    if (itdata.Ngay_HL != null)
//                    {
//                        p[28].Value = DateTime.ParseExact(itdata.Ngay_HL, "yyyy-MM-dd", null);
//                    }

//                    p[29] = new OracleParameter();
//                    p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//                    p[29].DbType = DbType.DateTime;
//                    p[29].Direction = ParameterDirection.Input;
//                    if (itdata.Ngay_HHL != null)
//                    {
//                        p[29].Value = DateTime.ParseExact(itdata.Ngay_HHL, "yyyy-MM-dd", null);
//                    }

//                    p[30] = new OracleParameter();
//                    p[30].ParameterName = "SONGAY_BL";//???
//                    p[30].DbType = DbType.Int64;
//                    p[30].Direction = ParameterDirection.Input;
//                    p[30].Value = DBNull.Value;
//                    if (itdata.SNBL != null)
//                        p[30].Value = Int64.Parse(itdata.SNBL);


//                    p[31] = new OracleParameter();
//                    p[31].ParameterName = "KIEU_BL";//???
//                    p[31].DbType = DbType.String;
//                    p[31].Direction = ParameterDirection.Input;
//                    p[31].Value = itdata.Loai_CT;


//                    p[32] = new OracleParameter();
//                    p[32].ParameterName = "DIEN_GIAI";
//                    p[32].DbType = DbType.String;
//                    p[32].Direction = ParameterDirection.Input;
//                    p[32].Value = DBNull.Value;
//                    if (itdata.DienGiai != null)
//                        p[32].Value = itdata.DienGiai;

//                    p[33] = new OracleParameter();
//                    p[33].ParameterName = "REQUEST_ID";//???
//                    p[33].DbType = DbType.String;
//                    p[33].Direction = ParameterDirection.Input;
//                    p[33].Value = DBNull.Value;

//                    p[34] = new OracleParameter();
//                    p[34].ParameterName = "ACCEPT_YN";//???
//                    p[34].DbType = DbType.String;
//                    p[34].Direction = ParameterDirection.Input;
//                    p[34].Value = DBNull.Value;

//                    p[35] = new OracleParameter();
//                    p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//                    p[35].DbType = DbType.String;
//                    p[35].Direction = ParameterDirection.Input;
//                    p[35].Value = DBNull.Value;

//                    p[36] = new OracleParameter();
//                    p[36].ParameterName = "MA_HQ_PH";
//                    p[36].DbType = DbType.String;
//                    p[36].Direction = ParameterDirection.Input;
//                    p[36].Value = DBNull.Value;
//                    //if (itdata.Ma_HQ_PH != null)
//                    //    p[36].Value = itdata.Ma_HQ_PH;

//                    p[37] = new OracleParameter();
//                    p[37].ParameterName = "So_TN_CT";
//                    p[37].DbType = DbType.String;
//                    p[37].Direction = ParameterDirection.Input;
//                    p[37].Value = DBNull.Value;
//                    if (itdata.So_TN_CT != null)
//                        p[37].Value = itdata.So_TN_CT.Trim();

//                    p[38] = new OracleParameter();
//                    p[38].ParameterName = "Ngay_TN_CT";
//                    p[38].DbType = DbType.String;
//                    p[38].Direction = ParameterDirection.Input;
//                    p[38].Value = DBNull.Value;
//                    if (itdata.Ngay_TN_CT != null)
//                        p[38].Value = itdata.Ngay_TN_CT.Trim();

//                    p[39] = new OracleParameter();
//                    p[39].ParameterName = "KQ_DC";
//                    p[39].DbType = DbType.String;
//                    p[39].Direction = ParameterDirection.Input;
//                    p[39].Value = DBNull.Value;
//                    //p[37].Value = itdata.KQ_DC;
//                    if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//                    {
//                        p[39].Value = "OK";
//                    }
//                    else
//                    {
//                        p[39].Value = itdata.KQ_DC;
//                    }

//                    ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);

//                }
//                transCT.Commit();


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu bao lanh thành công : Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("insert vao CSDL\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu bao lanh thành công : Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("insert vao CSDL\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }


//        }

//        public static void getMSG84(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG83 msg83 = new MSG83();
//                msg83.Data = new CustomsV3.MSG.MSG83.Data();
//                msg83.Header = new CustomsV3.MSG.MSG83.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg83.Header.Message_Version = strMessage_Version;
//                msg83.Header.Sender_Code = strSender_Code;
//                msg83.Header.Sender_Name = strSender_Name;
//                msg83.Header.Transaction_Type = "83";
//                msg83.Header.Transaction_Name = "Thông điệp yêu cầu kết quả đối chiếu giao dịch bảo lãnh chứng từ thành công";
//                msg83.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg83.Header.Transaction_ID = Guid.NewGuid().ToString();
//                String strReqId = getReqId(pv_NgayDC, "MSG81", strLoai_BLTK);

//                msg83.Header.Request_ID = strReqId;

//                //Gan du lieu Data
//                msg83.Data.Ma_NH_DC = strSender_Code;
//                msg83.Data.Loai_BLDC = strLoai_BLTK;
//                msg83.Data.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");
//                string tran_id = msg83.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "MSG84", pv_NgayDC, strLoai_BLTK);
//                //Chuyen thanh XML
//                string strXMLOut = msg83.MSG83toXML(msg83);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG84", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //                StringBuilder sb = new StringBuilder();
//                //              sb.Append(strXMLdefin1);
//                //            sb.Append("\r\n");
//                //          sb.Append(strXMLIn);
//                //        strXMLIn = sb.ToString();

//                MSG84 msg84 = new MSG84();
//                MSG84 objTemp = msg84.MSGToObject(strXMLIn);

//                pv_ErrorMes = objTemp.Error.Error_Message;
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                //Verify sign                
//                if (verifySignMsg(objTemp, "MSG84"))
//                {
//                    if ("0".Equals(pv_ErrorNum))
//                    {
//                        //Cap nhat CSDL
//                        MSG84ToDB(objTemp, pv_NgayDC);
//                    }
//                }
//                else
//                {
//                    pv_ErrorNum = "00001";
//                    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                }
//                UpdateDCInfo(tran_id, "MSG84", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }


//            }

//        }

//        public static void getMSG86(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                String str_loaiBLDC = "";

//                MSG83 msg83 = new MSG83();
//                msg83.Data = new CustomsV3.MSG.MSG83.Data();
//                msg83.Header = new CustomsV3.MSG.MSG83.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg83.Header.Message_Version = strMessage_Version;
//                msg83.Header.Sender_Code = strSender_Code;
//                msg83.Header.Sender_Name = strSender_Name;
//                msg83.Header.Transaction_Type = "83";
//                msg83.Header.Transaction_Name = "Thông điệp yêu cầu kết quả đối chiếu giao dịch bảo lãnh chứng từ thành công";
//                msg83.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg83.Header.Transaction_ID = Guid.NewGuid().ToString();
//                String strReqId = getReqId(pv_NgayDC, "MSG85", strLoai_BLVTD);

//                msg83.Header.Request_ID = strReqId;

//                //Gan du lieu Data
//                msg83.Data.Ma_NH_DC = strSender_Code;
//                msg83.Data.Loai_BLDC = strLoai_BLVTD;
//                msg83.Data.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");
//                string tran_id = msg83.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "MSG86", pv_NgayDC, strLoai_BLVTD);
//                //Chuyen thanh XML
//                string strXMLOut = msg83.MSG83toXML(msg83);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG86", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //                StringBuilder sb = new StringBuilder();
//                //              sb.Append(strXMLdefin1);
//                //            sb.Append("\r\n");
//                //          sb.Append(strXMLIn);
//                //        strXMLIn = sb.ToString();

//                MSG86 MSG86 = new MSG86();
//                MSG86 objTemp = MSG86.MSGToObject(strXMLIn);

//                pv_ErrorMes = objTemp.Error.Error_Message;
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                //Verify sign                
//                if (verifySignMsg(objTemp, "MSG86"))
//                {
//                    if ("0".Equals(pv_ErrorNum))
//                    {
//                        //Cap nhat CSDL
//                        MSG86ToDB(objTemp, pv_NgayDC);
//                    }
//                }
//                else
//                {
//                    pv_ErrorNum = "00001";
//                    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                }
//                UpdateDCInfo(tran_id, "MSG86", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {

//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//            }

//        }

//        public static void getMSG88(DateTime pv_NgayDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }

//                MSG83 msg83 = new MSG83();
//                msg83.Data = new CustomsV3.MSG.MSG83.Data();
//                msg83.Header = new CustomsV3.MSG.MSG83.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg83.Header.Message_Version = strMessage_Version;
//                msg83.Header.Sender_Code = strSender_Code;
//                msg83.Header.Sender_Name = strSender_Name;
//                msg83.Header.Transaction_Type = "83";
//                msg83.Header.Transaction_Name = "Thông điệp yêu cầu kết quả đối chiếu giao dịch bảo lãnh chứng từ thành công";
//                msg83.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg83.Header.Transaction_ID = Guid.NewGuid().ToString();
//                String strReqId = getReqId(pv_NgayDC, "MSG87", strLoai_BLC);

//                msg83.Header.Request_ID = strReqId;

//                //Gan du lieu Data
//                msg83.Data.Ma_NH_DC = strSender_Code;
//                msg83.Data.Loai_BLDC = strLoai_BLC;
//                msg83.Data.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");
//                string tran_id = msg83.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "MSG88", pv_NgayDC, strLoai_BLC);
//                //Chuyen thanh XML
//                string strXMLOut = msg83.MSG83toXML(msg83);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG88", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //                StringBuilder sb = new StringBuilder();
//                //              sb.Append(strXMLdefin1);
//                //            sb.Append("\r\n");
//                //          sb.Append(strXMLIn);
//                //        strXMLIn = sb.ToString();

//                MSG88 MSG88 = new MSG88();
//                MSG88 objTemp = MSG88.MSGToObject(strXMLIn);

//                //pv_ErrorMes = objTemp.Error.Error_Message;
//                //pv_ErrorNum = objTemp.Error.Error_Number;
//                //Verify sign                
//                if (verifySignMsg(objTemp, "MSG88"))
//                {
//                    if ("0".Equals(pv_ErrorNum))
//                    {
//                        //Cap nhat CSDL
//                        MSG88ToDB(objTemp, pv_NgayDC);
//                    }
//                }
//                else
//                {
//                    pv_ErrorNum = "00001";
//                    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                }
//                UpdateDCInfo(tran_id, "MSG83", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 44: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//            }

//        }

//        #endregion "Truy vấn kết quả kết quả đối chiếu giao dịch bảo lãnh chứng từ thành công M83 - M84"

//        #region "đối chiếu giao dịch huỷ bao lanh (M91-92)"
//        public static void DOICHIEUToMSG91(DateTime pv_NgayDC, String strLoai_BL, ref MSG91 msg91)
//        {
//            msg91.Data = new CustomsV3.MSG.MSG91.Data();
//            msg91.Data.temp.Ma_NH_DC = strSender_Code;
//            msg91.Data.temp.Loai_BL = strLoai_BL;
//            msg91.Data.temp.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");

//            StringBuilder sbsql = new StringBuilder();
//            DataSet ds = null;
//            string Kieu_BL_M91 = "";
//            if (strLoai_BL.Equals(strLoai_BLTK))
//            {
//                Kieu_BL_M91 = "M91";
//            }
//            else if (strLoai_BL.Equals(strLoai_BLVTD))
//            {
//                Kieu_BL_M91 = "M95";
//            }
//            else
//            {
//                Kieu_BL_M91 = "M97";
//            }

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT  hdr.id,");
//                sbsql.Append("        hdr.accept_yn,");
//                sbsql.Append("        hdr.transaction_id,");
//                sbsql.Append("        hdr.SO_TN_CT,");
//                sbsql.Append("                    hdr.ngay_tn_ct");
//                sbsql.Append("        FROM   tcs_dchieu_nhhq_hdr hdr");
//                sbsql.Append("        WHERE   hdr.transaction_type = '" + Kieu_BL_M91 + "' " + strDKNGAYDC);

//                DateTime ngayDC1 = pv_NgayDC.AddDays(-1);

//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[2];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAYDC1";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = ngayDC1.ToString("yyyy-MM-dd") + strGio_DC;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAYDC2";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = pv_NgayDC.ToString("yyyy-MM-dd") + strGio_DC;
//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);
//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }

//                CustomsV3.MSG.MSG91.Accept_Transactions accept_Transactions = new CustomsV3.MSG.MSG91.Accept_Transactions();
//                CustomsV3.MSG.MSG91.Reject_Transactions reject_Transactions = new CustomsV3.MSG.MSG91.Reject_Transactions();
//                List<CustomsV3.MSG.MSG91.ItemData> Items_accep = new List<CustomsV3.MSG.MSG91.ItemData>();
//                List<CustomsV3.MSG.MSG91.ItemData> Items_reject = new List<CustomsV3.MSG.MSG91.ItemData>();
//                foreach (DataRow drhdr in ds.Tables[0].Rows)
//                {
//                    if ("Y".Equals(drhdr["ACCEPT_YN"].ToString()))
//                    {
//                        CustomsV3.MSG.MSG91.ItemData item = new CustomsV3.MSG.MSG91.ItemData();

//                        item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                        item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                        item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();

//                        Items_accep.Add(item);
//                    }
//                    else
//                    {
//                        CustomsV3.MSG.MSG91.ItemData item = new CustomsV3.MSG.MSG91.ItemData();

//                        item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                        item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                        item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();
//                        Items_reject.Add(item);
//                    }
//                }
//                accept_Transactions.Item = Items_accep;
//                reject_Transactions.Item = Items_reject;
//                msg91.Data.Accept_Transactions = accept_Transactions;
//                msg91.Data.Reject_Transactions = reject_Transactions;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ bao lanh can doi chieu ngan hang va HQ (DOICHIEUToMSG91): Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ bao lanh can doi chieu ngan hang va HQ (DOICHIEUToMSG91): Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }
//        }
//        public static void getMSG92(DateTime pv_NgayDC, String strLoai_BL, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG91 msg91 = new MSG91();
//                msg91.Data = new CustomsV3.MSG.MSG91.Data();
//                msg91.Header = new CustomsV3.MSG.MSG91.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg91.Header.Message_Version = strMessage_Version;
//                msg91.Header.Sender_Code = strSender_Code;
//                msg91.Header.Sender_Name = strSender_Name;
//                msg91.Header.Transaction_Type = "91";
//                msg91.Header.Transaction_Name = "Thông điệp yêu cầu đối chiếu giao dịch huỷ bảo lãnh thuế";
//                msg91.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg91.Header.Transaction_ID = Guid.NewGuid().ToString();
//                string tran_id = msg91.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "MSG91", pv_NgayDC, strLoai_BL);
//                //Gan du lieu Data
//                DOICHIEUToMSG91(pv_NgayDC, strLoai_BL, ref msg91);
//                //InsertDCInfo(msg91.Header.Transaction_ID, "MSG41", pv_NgayDC);
//                //Chuyen thanh XML
//                string strXMLOut = msg91.MSG91toXML(msg91);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG91", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                StringBuilder sb = new StringBuilder();
//                //                sb.Append(strXMLdefin1);
//                //              sb.Append("\r\n");
//                //            sb.Append(strXMLIn);
//                //          strXMLIn = sb.ToString();

//                MSG92 msg92 = new MSG92();
//                MSG92 objTemp = msg92.MSGToObject(strXMLIn);
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;
//                //Verify sign                
//                if (!verifySignMsg(objTemp, "MSG92"))
//                {
//                    pv_ErrorNum = "00001";
//                    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                }
//                UpdateDCInfo(tran_id, "MSG91", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                MSG91 msg91 = new MSG91();
//                string tran_id = msg91.Header.Transaction_ID;
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 92: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);
//                pv_ErrorNum = "00000";
//                pv_ErrorMes = ex.Message.ToString();
//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 92: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                //pv_ErrorNum = "00000";
//                //pv_ErrorMes = sbErrMsg.ToString();
//                //UpdateDCInfo(tran_id, "MSG91", pv_ErrorNum, pv_ErrorMes);
//            }

//        }
//        #endregion "đối chiếu giao dịch huỷ bao lanh (M91-92)"

//        #region "Truy vấn kết quả đối chiếu dữ liệu giao dịch bao lanh huy M93 - M94"

//        public static void getMSG94(DateTime pv_NgayDC, String pv_Loai_BLDC, ref String pv_ErrorNum, ref String pv_ErrorMes)
//        {
//            try
//            {
//                if (strCustomOn == null)
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                else if (strCustomOn.Equals("0"))
//                {
//                    pv_ErrorNum = "0";
//                    return;
//                }
//                MSG93 msg93 = new MSG93();
//                msg93.Data = new CustomsV3.MSG.MSG93.Data();
//                msg93.Header = new CustomsV3.MSG.MSG93.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg93.Header.Message_Version = strMessage_Version;
//                msg93.Header.Sender_Code = strSender_Code;
//                msg93.Header.Sender_Name = strSender_Name;
//                msg93.Header.Transaction_Type = "93";
//                msg93.Header.Transaction_Name = "Thông điệp trả lời kết quả đối chiếu dữ liệu huỷ bảo lãnh thuế";
//                msg93.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg93.Header.Transaction_ID = Guid.NewGuid().ToString();
//                String strReqId = getReqId(pv_NgayDC, "MSG91", pv_Loai_BLDC);
//                msg93.Header.Request_ID = strReqId;

//                //Gan du lieu Data
//                msg93.Data.Ma_NH_DC = strSender_Code;
//                msg93.Data.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");
//                msg93.Data.Loai_BLDC = pv_Loai_BLDC;
//                string tran_id = msg93.Header.Transaction_ID;
//                InsertDCInfo(tran_id, "MSG93", pv_NgayDC, pv_Loai_BLDC);

//                //Chuyen thanh XML
//                string strXMLOut = msg93.MSG93toXML(msg93);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG93", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                StringBuilder sb = new StringBuilder();
//                //                sb.Append(strXMLdefin1);
//                //              sb.Append("\r\n");
//                //            sb.Append(strXMLIn);
//                //          strXMLIn = sb.ToString();

//                MSG94 msg94 = new MSG94();
//                MSG94 objTemp = msg94.MSGToObject(strXMLIn);

//                pv_ErrorMes = objTemp.Error.Error_Message;
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                //Verify sign                
//                if (verifySignMsg(objTemp, "MSG94"))
//                {
//                    if ("0".Equals(pv_ErrorNum))
//                    {
//                        //Cap nhat CSDL
//                        MSG94ToDB(objTemp, pv_NgayDC);
//                    }
//                }
//                else
//                {
//                    pv_ErrorNum = "00001";
//                    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                }
//                UpdateDCInfo(tran_id, "MSG93", pv_ErrorNum, pv_ErrorMes);
//            }
//            catch (Exception ex)
//            {
//                MSG93 msg93 = new MSG93();
//                StringBuilder sbErrMsg;
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 94: Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);
//                pv_ErrorNum = "00000";
//                pv_ErrorMes = ex.Message.ToString();
//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if (ex.InnerException != null)
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy dữ liệu theo MSG 94: Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                //pv_ErrorNum = "00000";
//                //pv_ErrorMes = sbErrMsg.ToString();
//                //UpdateDCInfo(msg93.Header.Transaction_ID, "MSG93", pv_ErrorNum, pv_ErrorMes);
//            }

//        }
//        public static void MSG94ToDB(MSG94 msg94, DateTime pv_NgayDC)
//        {
//            OracleConnection conn = GetConnection();
//            IDbTransaction transCT = null;
//            StringBuilder sbsql = null;
//            String strTransactionType = "94";
//            TCS_DS_TOKHAI _ttin_huy = null;
//            try
//            {
//                transCT = conn.BeginTransaction();

//                //Xoa du lieu dc cu
//                xoaDuLieuDC(strTransactionType, pv_NgayDC, transCT);
//                //Insert thong tin header
//                sbsql = new StringBuilder();
//                sbsql.Append("INSERT INTO tcs_dchieu_hqnh_hdr (id,");
//                sbsql.Append("                                 transaction_id,");
//                sbsql.Append("                                 transaction_type,");
//                sbsql.Append("                                 shkb,");
//                sbsql.Append("                                 ten_kb,");
//                sbsql.Append("                                 ngay_kb,");
//                sbsql.Append("                                 so_bt,");
//                sbsql.Append("                                 kyhieu_ct,");
//                sbsql.Append("                                 so_ct,");
//                sbsql.Append("                                 ngay_ct,");
//                sbsql.Append("                                 ngay_bn,");
//                sbsql.Append("                                 ngay_bc,");
//                sbsql.Append("                                 ma_nnthue,");
//                sbsql.Append("                                 ten_nnthue,");
//                sbsql.Append("                                 ma_cqthu,");
//                sbsql.Append("                                 ten_cqthu,");
//                sbsql.Append("                                 so_tk,");
//                sbsql.Append("                                 ngay_tk,");
//                sbsql.Append("                                 lhxnk,");
//                sbsql.Append("                                 vt_lhxnk,");
//                sbsql.Append("                                 ten_lhxnk,");
//                sbsql.Append("                                 ttien,");
//                sbsql.Append("                                 ttien_nt,");
//                sbsql.Append("                                 trang_thai,");
//                sbsql.Append("                                 tk_ns,");
//                sbsql.Append("                                 ten_tk_ns,");
//                sbsql.Append("                                 ma_nkt,");
//                sbsql.Append("                                 so_bl,");
//                sbsql.Append("                                 ngay_bl_batdau,");
//                sbsql.Append("                                 ngay_bl_ketthuc,");
//                sbsql.Append("                                 songay_bl,");
//                sbsql.Append("                                 kieu_bl,");
//                sbsql.Append("                                 dien_giai,");
//                sbsql.Append("                                 request_id,");
//                sbsql.Append("                                 accept_yn,");
//                sbsql.Append("                                 parent_transaction_id,");
//                sbsql.Append("                                 ma_hq_ph,");
//                sbsql.Append("                                 SO_TN_CT, ");
//                sbsql.Append("                                 NGAY_TN_CT,");
//                sbsql.Append("                                 kq_dc)");
//                sbsql.Append("  VALUES   (:id,");
//                sbsql.Append("            :transaction_id,");
//                sbsql.Append("            :transaction_type,");
//                sbsql.Append("            :shkb,");
//                sbsql.Append("            :ten_kb,");
//                sbsql.Append("            :ngay_kb,");
//                sbsql.Append("            :so_bt,");
//                sbsql.Append("            :kyhieu_ct,");
//                sbsql.Append("            :so_ct,");
//                sbsql.Append("            :ngay_ct,");
//                sbsql.Append("            :ngay_bn,");
//                sbsql.Append("            :ngay_bc,");
//                sbsql.Append("            :ma_nnthue,");
//                sbsql.Append("            :ten_nnthue,");
//                sbsql.Append("            :ma_cqthu,");
//                sbsql.Append("            :ten_cqthu,");
//                sbsql.Append("            :so_tk,");
//                sbsql.Append("            :ngay_tk,");
//                sbsql.Append("            :lhxnk,");
//                sbsql.Append("            :vt_lhxnk,");
//                sbsql.Append("            :ten_lhxnk,");
//                sbsql.Append("            :ttien,");
//                sbsql.Append("            :ttien_nt,");
//                sbsql.Append("            :trang_thai,");
//                sbsql.Append("            :tk_ns,");
//                sbsql.Append("            :ten_tk_ns,");
//                sbsql.Append("            :ma_nkt,");
//                sbsql.Append("            :so_bl,");
//                sbsql.Append("            :ngay_bl_batdau,");
//                sbsql.Append("            :ngay_bl_ketthuc,");
//                sbsql.Append("            :songay_bl,");
//                sbsql.Append("            :kieu_bl,");
//                sbsql.Append("            :dien_giai,");
//                sbsql.Append("            :request_id,");
//                sbsql.Append("            :accept_yn,");
//                sbsql.Append("            :parent_transaction_id,");
//                sbsql.Append("            :ma_hq_ph,");
//                sbsql.Append("            :SO_TN_CT, ");
//                sbsql.Append("            :NGAY_TN_CT,");
//                sbsql.Append("            :kq_dc)");
//                foreach (CustomsV3.MSG.MSG94.ItemData itdata in msg94.Data.Accept_Transactions.Item)
//                {
//                    Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//                    _ttin_huy = getTTinCtuHuy(itdata.Ngay_TN_CT, itdata.So_TN_CT, "M91");
//                    //_ttin_huy = getTTinCtuHuy(itdata.Ngay_CT, itdata.KyHieu_CT, itdata.So_CT, "M91");
//                    IDbDataParameter[] p = null;

//                    p = new IDbDataParameter[40];

//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "ID";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = DBNull.Value;
//                    p[0].Value = iId;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TRANSACTION_ID";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = DBNull.Value;
//                    if (itdata.Transaction_ID != null)
//                        p[1].Value = itdata.Transaction_ID.Trim();

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "TRANSACTION_TYPE";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strTransactionType;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "SHKB";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "TEN_KB";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DBNull.Value;

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "NGAY_KB";
//                    p[5].DbType = DbType.Int64;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = DBNull.Value;

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "SO_BT";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = DBNull.Value;

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "KYHIEU_CT";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = DBNull.Value;
//                    //if (itdata.KyHieu_CT != null)
//                    //    p[7].Value = itdata.KyHieu_CT;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "SO_CT";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = DBNull.Value;
//                    //if (itdata.So_CT != null)
//                    //    p[8].Value = itdata.So_CT;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "NGAY_CT";
//                    p[9].DbType = DbType.Int64;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;
//                    //if (itdata.Ngay_CT != null && !String.Empty.Equals(itdata.Ngay_CT))
//                    //    p[9].Value = Int64.Parse(DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null).ToString("yyyMMdd"));

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "NGAY_BN";
//                    p[10].DbType = DbType.DateTime;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = DBNull.Value;
//                    //if (itdata.Ngay_CT != null)
//                    //    p[10].Value = DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null);

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_BC";
//                    p[11].DbType = DbType.DateTime;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = DBNull.Value;

//                    p[12] = new OracleParameter();
//                    p[12].ParameterName = "MA_NNTHUE";
//                    p[12].DbType = DbType.String;
//                    p[12].Direction = ParameterDirection.Input;
//                    p[12].Value = DBNull.Value;

//                    p[13] = new OracleParameter();
//                    p[13].ParameterName = "TEN_NNTHUE";
//                    p[13].DbType = DbType.String;
//                    p[13].Direction = ParameterDirection.Input;
//                    p[13].Value = DBNull.Value;

//                    p[14] = new OracleParameter();
//                    p[14].ParameterName = "MA_CQTHU";
//                    p[14].DbType = DbType.String;
//                    p[14].Direction = ParameterDirection.Input;
//                    p[14].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[14].Value = _ttin_huy.ma_cqthu;
//                    }


//                    p[15] = new OracleParameter();
//                    p[15].ParameterName = "TEN_CQTHU";
//                    p[15].DbType = DbType.String;
//                    p[15].Direction = ParameterDirection.Input;
//                    p[15].Value = DBNull.Value;

//                    p[16] = new OracleParameter();
//                    p[16].ParameterName = "SO_TK";
//                    p[16].DbType = DbType.String;
//                    p[16].Direction = ParameterDirection.Input;
//                    p[16].Value = DBNull.Value;

//                    p[17] = new OracleParameter();
//                    p[17].ParameterName = "NGAY_TK";
//                    p[17].DbType = DbType.DateTime;
//                    p[17].Direction = ParameterDirection.Input;
//                    p[17].Value = DBNull.Value;

//                    p[18] = new OracleParameter();
//                    p[18].ParameterName = "LHXNK";
//                    p[18].DbType = DbType.String;
//                    p[18].Direction = ParameterDirection.Input;
//                    p[18].Value = DBNull.Value;

//                    p[19] = new OracleParameter();
//                    p[19].ParameterName = "VT_LHXNK";//???
//                    p[19].DbType = DbType.String;
//                    p[19].Direction = ParameterDirection.Input;
//                    p[19].Value = DBNull.Value;


//                    p[20] = new OracleParameter();
//                    p[20].ParameterName = "TEN_LHXNK";
//                    p[20].DbType = DbType.String;
//                    p[20].Direction = ParameterDirection.Input;
//                    p[20].Value = DBNull.Value;

//                    p[21] = new OracleParameter();
//                    p[21].ParameterName = "TTIEN";
//                    p[21].DbType = DbType.Int64;
//                    p[21].Direction = ParameterDirection.Input;
//                    if (_ttin_huy != null)
//                    {
//                        p[21].Value = Int64.Parse(_ttin_huy.tt_nop);
//                    }
//                    else p[21].Value = DBNull.Value;

//                    p[22] = new OracleParameter();
//                    p[22].ParameterName = "TTIEN_NT";
//                    p[22].DbType = DbType.String;
//                    p[22].Direction = ParameterDirection.Input;
//                    if (_ttin_huy != null)
//                    {
//                        p[22].Value = Int64.Parse(_ttin_huy.tt_nop);
//                    }
//                    else p[22].Value = DBNull.Value;

//                    p[23] = new OracleParameter();
//                    p[23].ParameterName = "TRANG_THAI";//???
//                    p[23].DbType = DbType.String;
//                    p[23].Direction = ParameterDirection.Input;
//                    p[23].Value = DBNull.Value;

//                    p[24] = new OracleParameter();
//                    p[24].ParameterName = "TK_NS";//???
//                    p[24].DbType = DbType.String;
//                    p[24].Direction = ParameterDirection.Input;
//                    p[24].Value = DBNull.Value;

//                    p[25] = new OracleParameter();
//                    p[25].ParameterName = "TEN_TK_NS";
//                    p[25].DbType = DbType.String;
//                    p[25].Direction = ParameterDirection.Input;
//                    p[25].Value = DBNull.Value;

//                    p[26] = new OracleParameter();
//                    p[26].ParameterName = "MA_NKT";
//                    p[26].DbType = DbType.String;
//                    p[26].Direction = ParameterDirection.Input;
//                    p[26].Value = DBNull.Value;

//                    p[27] = new OracleParameter();
//                    p[27].ParameterName = "SO_BL";//???
//                    p[27].DbType = DbType.String;
//                    p[27].Direction = ParameterDirection.Input;
//                    p[27].Value = DBNull.Value;


//                    p[28] = new OracleParameter();
//                    p[28].ParameterName = "NGAY_BL_BATDAU";//???
//                    p[28].DbType = DbType.String;
//                    p[28].Direction = ParameterDirection.Input;
//                    p[28].Value = DBNull.Value;

//                    p[29] = new OracleParameter();
//                    p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//                    p[29].DbType = DbType.String;
//                    p[29].Direction = ParameterDirection.Input;
//                    p[29].Value = DBNull.Value;

//                    p[30] = new OracleParameter();
//                    p[30].ParameterName = "SONGAY_BL";//???
//                    p[30].DbType = DbType.String;
//                    p[30].Direction = ParameterDirection.Input;
//                    p[30].Value = DBNull.Value;


//                    p[31] = new OracleParameter();
//                    p[31].ParameterName = "KIEU_BL";//???
//                    p[31].DbType = DbType.String;
//                    p[31].Direction = ParameterDirection.Input;
//                    p[31].Value = DBNull.Value;


//                    p[32] = new OracleParameter();
//                    p[32].ParameterName = "DIEN_GIAI";
//                    p[32].DbType = DbType.String;
//                    p[32].Direction = ParameterDirection.Input;
//                    p[32].Value = DBNull.Value;

//                    p[33] = new OracleParameter();
//                    p[33].ParameterName = "REQUEST_ID";//???
//                    p[33].DbType = DbType.String;
//                    p[33].Direction = ParameterDirection.Input;
//                    p[33].Value = DBNull.Value;

//                    p[34] = new OracleParameter();
//                    p[34].ParameterName = "ACCEPT_YN";//???
//                    p[34].DbType = DbType.String;
//                    p[34].Direction = ParameterDirection.Input;
//                    p[34].Value = "Y";

//                    p[35] = new OracleParameter();
//                    p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//                    p[35].DbType = DbType.String;
//                    p[35].Direction = ParameterDirection.Input;
//                    p[35].Value = DBNull.Value;

//                    p[36] = new OracleParameter();
//                    p[36].ParameterName = "MA_HQ_PH";
//                    p[36].DbType = DbType.String;
//                    p[36].Direction = ParameterDirection.Input;
//                    p[36].Value = DBNull.Value;

//                    p[37] = new OracleParameter();
//                    p[37].ParameterName = "So_TN_CT";
//                    p[37].DbType = DbType.String;
//                    p[37].Direction = ParameterDirection.Input;
//                    p[37].Value = DBNull.Value;
//                    if (itdata.So_TN_CT != null)
//                        p[37].Value = itdata.So_TN_CT.Trim();

//                    p[38] = new OracleParameter();
//                    p[38].ParameterName = "Ngay_TN_CT";
//                    p[38].DbType = DbType.String;
//                    p[38].Direction = ParameterDirection.Input;
//                    p[38].Value = DBNull.Value;
//                    if (itdata.Ngay_TN_CT != null)
//                        p[38].Value = itdata.Ngay_TN_CT.Trim();

//                    p[39] = new OracleParameter();
//                    p[39].ParameterName = "KQ_DC";
//                    p[39].DbType = DbType.String;
//                    p[39].Direction = ParameterDirection.Input;
//                    p[39].Value = DBNull.Value;
//                    //p[37].Value = itdata.KQ_DC;
//                    if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//                    {
//                        p[39].Value = "OK";
//                    }
//                    else
//                    {
//                        p[39].Value = itdata.KQ_DC;
//                    }

//                    ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);

//                }
//                foreach (CustomsV3.MSG.MSG94.ItemData itdata in msg94.Data.Reject_Transactions.Item)
//                {
//                    Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");
//                    _ttin_huy = getTTinCtuHuy(itdata.Ngay_TN_CT, itdata.So_TN_CT, "M91");
//                    IDbDataParameter[] p = null;

//                    p = new IDbDataParameter[40];

//                    p[0] = new OracleParameter();
//                    p[0].ParameterName = "ID";
//                    p[0].DbType = DbType.String;
//                    p[0].Direction = ParameterDirection.Input;
//                    p[0].Value = DBNull.Value;
//                    p[0].Value = iId;

//                    p[1] = new OracleParameter();
//                    p[1].ParameterName = "TRANSACTION_ID";
//                    p[1].DbType = DbType.String;
//                    p[1].Direction = ParameterDirection.Input;
//                    p[1].Value = DBNull.Value;
//                    if (itdata.Transaction_ID != null)
//                        p[1].Value = itdata.Transaction_ID.Trim();

//                    p[2] = new OracleParameter();
//                    p[2].ParameterName = "TRANSACTION_TYPE";
//                    p[2].DbType = DbType.String;
//                    p[2].Direction = ParameterDirection.Input;
//                    p[2].Value = strTransactionType;

//                    p[3] = new OracleParameter();
//                    p[3].ParameterName = "SHKB";
//                    p[3].DbType = DbType.String;
//                    p[3].Direction = ParameterDirection.Input;
//                    p[3].Value = DBNull.Value;

//                    p[4] = new OracleParameter();
//                    p[4].ParameterName = "TEN_KB";
//                    p[4].DbType = DbType.String;
//                    p[4].Direction = ParameterDirection.Input;
//                    p[4].Value = DateTime.Now.ToString("yyyyMMdd");

//                    p[5] = new OracleParameter();
//                    p[5].ParameterName = "NGAY_KB";
//                    p[5].DbType = DbType.Int64;
//                    p[5].Direction = ParameterDirection.Input;
//                    p[5].Value = DBNull.Value;

//                    p[6] = new OracleParameter();
//                    p[6].ParameterName = "SO_BT";
//                    p[6].DbType = DbType.String;
//                    p[6].Direction = ParameterDirection.Input;
//                    p[6].Value = DBNull.Value;

//                    p[7] = new OracleParameter();
//                    p[7].ParameterName = "KYHIEU_CT";
//                    p[7].DbType = DbType.String;
//                    p[7].Direction = ParameterDirection.Input;
//                    p[7].Value = DBNull.Value;
//                    //if (itdata.KyHieu_CT != null)
//                    //    p[7].Value = itdata.KyHieu_CT;

//                    p[8] = new OracleParameter();
//                    p[8].ParameterName = "SO_CT";
//                    p[8].DbType = DbType.String;
//                    p[8].Direction = ParameterDirection.Input;
//                    p[8].Value = DBNull.Value;
//                    //if (itdata.So_CT != null)
//                    //    p[8].Value = itdata.So_CT;

//                    p[9] = new OracleParameter();
//                    p[9].ParameterName = "NGAY_CT";
//                    p[9].DbType = DbType.Int64;
//                    p[9].Direction = ParameterDirection.Input;
//                    p[9].Value = DBNull.Value;
//                    //if (itdata.Ngay_CT != null && !String.Empty.Equals(itdata.Ngay_CT))
//                    //    p[9].Value = Int64.Parse(DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null).ToString("yyyMMdd"));

//                    p[10] = new OracleParameter();
//                    p[10].ParameterName = "NGAY_BN";
//                    p[10].DbType = DbType.DateTime;
//                    p[10].Direction = ParameterDirection.Input;
//                    p[10].Value = DBNull.Value;
//                    //if (itdata.Ngay_CT != null)
//                    //    p[10].Value = DateTime.ParseExact(itdata.Ngay_CT, "yyyy-MM-dd", null);

//                    p[11] = new OracleParameter();
//                    p[11].ParameterName = "NGAY_BC";
//                    p[11].DbType = DbType.DateTime;
//                    p[11].Direction = ParameterDirection.Input;
//                    p[11].Value = DBNull.Value;

//                    p[12] = new OracleParameter();
//                    p[12].ParameterName = "MA_NNTHUE";
//                    p[12].DbType = DbType.String;
//                    p[12].Direction = ParameterDirection.Input;
//                    p[12].Value = DBNull.Value;

//                    p[13] = new OracleParameter();
//                    p[13].ParameterName = "TEN_NNTHUE";
//                    p[13].DbType = DbType.String;
//                    p[13].Direction = ParameterDirection.Input;
//                    p[13].Value = DBNull.Value;

//                    p[14] = new OracleParameter();
//                    p[14].ParameterName = "MA_CQTHU";
//                    p[14].DbType = DbType.String;
//                    p[14].Direction = ParameterDirection.Input;
//                    p[14].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[14].Value = _ttin_huy.ma_cqthu;
//                    }


//                    p[15] = new OracleParameter();
//                    p[15].ParameterName = "TEN_CQTHU";
//                    p[15].DbType = DbType.String;
//                    p[15].Direction = ParameterDirection.Input;
//                    p[15].Value = DBNull.Value;

//                    p[16] = new OracleParameter();
//                    p[16].ParameterName = "SO_TK";
//                    p[16].DbType = DbType.String;
//                    p[16].Direction = ParameterDirection.Input;
//                    p[16].Value = DBNull.Value;

//                    p[17] = new OracleParameter();
//                    p[17].ParameterName = "NGAY_TK";
//                    p[17].DbType = DbType.DateTime;
//                    p[17].Direction = ParameterDirection.Input;
//                    p[17].Value = DBNull.Value;

//                    p[18] = new OracleParameter();
//                    p[18].ParameterName = "LHXNK";
//                    p[18].DbType = DbType.String;
//                    p[18].Direction = ParameterDirection.Input;
//                    p[18].Value = DBNull.Value;

//                    p[19] = new OracleParameter();
//                    p[19].ParameterName = "VT_LHXNK";//???
//                    p[19].DbType = DbType.String;
//                    p[19].Direction = ParameterDirection.Input;
//                    p[19].Value = DBNull.Value;


//                    p[20] = new OracleParameter();
//                    p[20].ParameterName = "TEN_LHXNK";
//                    p[20].DbType = DbType.String;
//                    p[20].Direction = ParameterDirection.Input;
//                    p[20].Value = DBNull.Value;

//                    p[21] = new OracleParameter();
//                    p[21].ParameterName = "TTIEN";
//                    p[21].DbType = DbType.Int64;
//                    p[21].Direction = ParameterDirection.Input;
//                    p[21].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[21].Value = Int64.Parse(_ttin_huy.tt_nop);
//                    }

//                    p[22] = new OracleParameter();
//                    p[22].ParameterName = "TTIEN_NT";
//                    p[22].DbType = DbType.Int64;
//                    p[22].Direction = ParameterDirection.Input;
//                    p[22].Value = DBNull.Value;
//                    if (_ttin_huy != null)
//                    {
//                        p[22].Value = Int64.Parse(_ttin_huy.tt_nop);
//                    }

//                    p[23] = new OracleParameter();
//                    p[23].ParameterName = "TRANG_THAI";//???
//                    p[23].DbType = DbType.String;
//                    p[23].Direction = ParameterDirection.Input;
//                    p[23].Value = DBNull.Value;

//                    p[24] = new OracleParameter();
//                    p[24].ParameterName = "TK_NS";//???
//                    p[24].DbType = DbType.String;
//                    p[24].Direction = ParameterDirection.Input;
//                    p[24].Value = DBNull.Value;

//                    p[25] = new OracleParameter();
//                    p[25].ParameterName = "TEN_TK_NS";
//                    p[25].DbType = DbType.String;
//                    p[25].Direction = ParameterDirection.Input;
//                    p[25].Value = DBNull.Value;

//                    p[26] = new OracleParameter();
//                    p[26].ParameterName = "MA_NKT";
//                    p[26].DbType = DbType.String;
//                    p[26].Direction = ParameterDirection.Input;
//                    p[26].Value = DBNull.Value;

//                    p[27] = new OracleParameter();
//                    p[27].ParameterName = "SO_BL";//???
//                    p[27].DbType = DbType.String;
//                    p[27].Direction = ParameterDirection.Input;
//                    p[27].Value = DBNull.Value;


//                    p[28] = new OracleParameter();
//                    p[28].ParameterName = "NGAY_BL_BATDAU";//???
//                    p[28].DbType = DbType.String;
//                    p[28].Direction = ParameterDirection.Input;
//                    p[28].Value = DBNull.Value;

//                    p[29] = new OracleParameter();
//                    p[29].ParameterName = "NGAY_BL_KETTHUC";//???
//                    p[29].DbType = DbType.String;
//                    p[29].Direction = ParameterDirection.Input;
//                    p[29].Value = DBNull.Value;

//                    p[30] = new OracleParameter();
//                    p[30].ParameterName = "SONGAY_BL";//???
//                    p[30].DbType = DbType.String;
//                    p[30].Direction = ParameterDirection.Input;
//                    p[30].Value = DBNull.Value;


//                    p[31] = new OracleParameter();
//                    p[31].ParameterName = "KIEU_BL";//???
//                    p[31].DbType = DbType.String;
//                    p[31].Direction = ParameterDirection.Input;
//                    p[31].Value = DBNull.Value;


//                    p[32] = new OracleParameter();
//                    p[32].ParameterName = "DIEN_GIAI";
//                    p[32].DbType = DbType.String;
//                    p[32].Direction = ParameterDirection.Input;
//                    p[32].Value = DBNull.Value;

//                    p[33] = new OracleParameter();
//                    p[33].ParameterName = "REQUEST_ID";//???
//                    p[33].DbType = DbType.String;
//                    p[33].Direction = ParameterDirection.Input;
//                    p[33].Value = DBNull.Value;

//                    p[34] = new OracleParameter();
//                    p[34].ParameterName = "ACCEPT_YN";//???
//                    p[34].DbType = DbType.String;
//                    p[34].Direction = ParameterDirection.Input;
//                    p[34].Value = "N";

//                    p[35] = new OracleParameter();
//                    p[35].ParameterName = "PARENT_TRANSACTION_ID";//???
//                    p[35].DbType = DbType.String;
//                    p[35].Direction = ParameterDirection.Input;
//                    p[35].Value = DBNull.Value;

//                    p[36] = new OracleParameter();
//                    p[36].ParameterName = "MA_HQ_PH";
//                    p[36].DbType = DbType.String;
//                    p[36].Direction = ParameterDirection.Input;
//                    p[36].Value = DBNull.Value;

//                    p[37] = new OracleParameter();
//                    p[37].ParameterName = "So_TN_CT";
//                    p[37].DbType = DbType.String;
//                    p[37].Direction = ParameterDirection.Input;
//                    p[37].Value = DBNull.Value;
//                    if (itdata.So_TN_CT != null)
//                        p[37].Value = itdata.So_TN_CT.Trim();

//                    p[38] = new OracleParameter();
//                    p[38].ParameterName = "Ngay_TN_CT";
//                    p[38].DbType = DbType.String;
//                    p[38].Direction = ParameterDirection.Input;
//                    p[38].Value = DBNull.Value;
//                    if (itdata.Ngay_TN_CT != null)
//                        p[38].Value = itdata.Ngay_TN_CT.Trim();

//                    p[39] = new OracleParameter();
//                    p[39].ParameterName = "KQ_DC";
//                    p[39].DbType = DbType.String;
//                    p[39].Direction = ParameterDirection.Input;
//                    p[39].Value = DBNull.Value;
//                    //p[37].Value = itdata.KQ_DC;
//                    if ((null == itdata.KQ_DC) || ("".Equals(itdata.KQ_DC)))
//                    {
//                        p[39].Value = "OK";
//                    }
//                    else
//                    {
//                        p[39].Value = itdata.KQ_DC;
//                    }


//                    ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);
//                }
//                transCT.Commit();


//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch bao lanh huy : Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("insert vao CSDL\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy kết quả đối chiếu dữ liệu giao dịch bao lanh huy : Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("insert vao CSDL\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }
//                throw ex;
//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }
//        }

//        #endregion "Truy vấn kết quả đối chiếu dữ liệu giao dịch bao lanh huy M93 - M94"

//        #region "Gui msg di HQ";
//        //public static String sendMsg(String msgType, String msg)
//        //{
//        //    TTDTPortal.TTDTPortal a = new CustomsService.TTDTPortal.TTDTPortal();
//        //    a.Url = Business_HQ.Common.mdlCommon.GET_URL_HQ();
//        //    //string strPublicKey2 = a.GetPublicKey();
//        //    string strPublicKey = strPLK;
//        //    String strMsgRes = a.WSProcess(strPublicKey, msg);
//        //    //String strMsgRes = "<?xml version='1.0' encoding='UTF-8'?><CUSTOMS><Header><Message_Version>1.3</Message_Version><Sender_Code>01302001</Sender_Code><Sender_Name>Ngan Hang Thuong Mai Co phan Hang Hai Viet Nam</Sender_Name><Transaction_Type>72</Transaction_Type><Transaction_Name>Thông điệp xác nhận hủy bảo lãnh thuế của cơ quan Hải quan</Transaction_Name><Transaction_Date>2012-09-19T04:04:46</Transaction_Date><Transaction_ID>accf82b5-117c-49f1-b091-ed2d6c41af08</Transaction_ID><Request_ID>11bc5f9e-dd29-44e1-bcf5-492e17288416</Request_ID></Header><Error><Error_Number>0</Error_Number><Error_Message>Xử lý thành công</Error_Message></Error><Security><Signature>RNqgPHpMCtF0FTyDygbFMkw+B9YghYOIflplvtZ49vbuH27k71YVwcFcZ0d4Toml/hY7lyYj3OqPegtjv3aA5ObTjX7eINdrYy7dcoLvoaTG8Zzb9fzaNMYp5zDUYM7p2A/bjK9RnTfs6xrW1veaDmRlci1k6xgYvq6CvvdkfzK42F58G1U+zAUKB2+jcjBQGDj3sepB7ZL4JQ/G7K33q0UGm+oSPkW6+ayx6U1UWYO2y16du80E53B1JIUluJgEydUxQ+HHbP9UXbnBNbs+XYURXYvGb+2r8v0w1JVkB3nVUjl8rk02gywh0bxd2q0mzPmzVK1obPJb2WU/8aE4lA==</Signature></Security></CUSTOMS>";

//        //    insertLog(msg, strMsgRes, msgType, "");
//        //    return strMsgRes;
//        //}
//        public static String sendMsg(String msgType, String msg)
//        {
//            String strMsgRes = "";
//            TTDTPortal.TTDTPortal a = new CustomsService.TTDTPortal.TTDTPortal();
//            try
//            {
//                a.Url = Business_HQ.Common.mdlCommon.GET_URL_HQ();
//                //strPublicKey2 = a.GetPublicKey();
//                //string strPublicKey2 = a.GetPublicKey();
//                string strPublicKey = Signature.SignatureProcess.getPublicKey();
//                strMsgRes = a.WSProcess(strPublicKey, msg);
//                //String strMsgRes = "<?xml version='1.0' encoding='UTF-8'?><CUSTOMS><Header><Message_Version>1.3</Message_Version><Sender_Code>01302001</Sender_Code><Sender_Name>Ngan Hang Thuong Mai Co phan Hang Hai Viet Nam</Sender_Name><Transaction_Type>72</Transaction_Type><Transaction_Name>Thông điệp xác nhận hủy bảo lãnh thuế của cơ quan Hải quan</Transaction_Name><Transaction_Date>2012-09-19T04:04:46</Transaction_Date><Transaction_ID>accf82b5-117c-49f1-b091-ed2d6c41af08</Transaction_ID><Request_ID>11bc5f9e-dd29-44e1-bcf5-492e17288416</Request_ID></Header><Error><Error_Number>0</Error_Number><Error_Message>Xử lý thành công</Error_Message></Error><Security><Signature>RNqgPHpMCtF0FTyDygbFMkw+B9YghYOIflplvtZ49vbuH27k71YVwcFcZ0d4Toml/hY7lyYj3OqPegtjv3aA5ObTjX7eINdrYy7dcoLvoaTG8Zzb9fzaNMYp5zDUYM7p2A/bjK9RnTfs6xrW1veaDmRlci1k6xgYvq6CvvdkfzK42F58G1U+zAUKB2+jcjBQGDj3sepB7ZL4JQ/G7K33q0UGm+oSPkW6+ayx6U1UWYO2y16du80E53B1JIUluJgEydUxQ+HHbP9UXbnBNbs+XYURXYvGb+2r8v0w1JVkB3nVUjl8rk02gywh0bxd2q0mzPmzVK1obPJb2WU/8aE4lA==</Signature></Security></CUSTOMS>";

//                insertLog(msg, strMsgRes, msgType, "");

//            }
//            catch (Exception ex)
//            {
//                LogDebug.WriteLog(ex.Message.ToString(), EventLogEntryType.Error);
//            }
//            return strMsgRes;
//        }
//        public static String sendMsgTest(String msgType, String msg)
//        {
//            String strMsgRes = "";
//            TTDTPortal.TTDTPortal a = new CustomsService.TTDTPortal.TTDTPortal();
//            try
//            {
//                a.Url = Business_HQ.Common.mdlCommon.GET_URL_HQ();
//                //strPublicKey2 = a.GetPublicKey();
//                //string strPublicKey2 = a.GetPublicKey();
//                string strPublicKey = Signature.SignatureProcess.getPublicKey();
//                strMsgRes = a.WSProcess(strPublicKey, msg);
//                //String strMsgRes = "<?xml version='1.0' encoding='UTF-8'?><CUSTOMS><Header><Message_Version>1.3</Message_Version><Sender_Code>01302001</Sender_Code><Sender_Name>Ngan Hang Thuong Mai Co phan Hang Hai Viet Nam</Sender_Name><Transaction_Type>72</Transaction_Type><Transaction_Name>Thông điệp xác nhận hủy bảo lãnh thuế của cơ quan Hải quan</Transaction_Name><Transaction_Date>2012-09-19T04:04:46</Transaction_Date><Transaction_ID>accf82b5-117c-49f1-b091-ed2d6c41af08</Transaction_ID><Request_ID>11bc5f9e-dd29-44e1-bcf5-492e17288416</Request_ID></Header><Error><Error_Number>0</Error_Number><Error_Message>Xử lý thành công</Error_Message></Error><Security><Signature>RNqgPHpMCtF0FTyDygbFMkw+B9YghYOIflplvtZ49vbuH27k71YVwcFcZ0d4Toml/hY7lyYj3OqPegtjv3aA5ObTjX7eINdrYy7dcoLvoaTG8Zzb9fzaNMYp5zDUYM7p2A/bjK9RnTfs6xrW1veaDmRlci1k6xgYvq6CvvdkfzK42F58G1U+zAUKB2+jcjBQGDj3sepB7ZL4JQ/G7K33q0UGm+oSPkW6+ayx6U1UWYO2y16du80E53B1JIUluJgEydUxQ+HHbP9UXbnBNbs+XYURXYvGb+2r8v0w1JVkB3nVUjl8rk02gywh0bxd2q0mzPmzVK1obPJb2WU/8aE4lA==</Signature></Security></CUSTOMS>";

//               // insertLog(msg, strMsgRes, msgType, "");

//            }
//            catch (Exception ex)
//            {
//                LogDebug.WriteLog(ex.Message.ToString(), EventLogEntryType.Error);
//            }
//            return strMsgRes;
//        }
//        public static String sendMsgTest(String msg)
//        {
//            TTDTPortal.TTDTPortal a = new CustomsService.TTDTPortal.TTDTPortal();
//            a.Url = Business_HQ.Common.mdlCommon.GET_URL_HQ();
//            //string strPublicKey2 = a.GetPublicKey();
//            string strPublicKey = Signature.SignatureProcess.getPublicKey();
//            if (null == msg || "".Equals(msg))
//            {
//                // msg = "<CUSTOMS><Header><Message_Version>1.3</Message_Version><Sender_Code>01302001</Sender_Code><Sender_Name>Ngan Hang Thuong Mai Co phan Hang Hai Viet Nam</Sender_Name><Transaction_Type>11</Transaction_Type><Transaction_Name>Thông điệp truy vấn số thuế phải thu</Transaction_Name><Transaction_Date>2012-09-19 16:59:23</Transaction_Date><Transaction_ID>38cf3060-acea-4b6b-a0e3-2e4ad417e9cc</Transaction_ID></Header><Data><Ma_DV>0100101308</Ma_DV><Ten_DV /><Ma_HQ>T01E</Ma_HQ><Ten_HQ /><Ma_LH>NKD01</Ma_LH><Nam_DK>2012</Nam_DK><SoTK>101</SoTK></Data><Security><Signature>Woc14wXKLkJc/O+3F3EmVeFN0+qABBc4DyK0iPHyVVLjVBlnsDV4kBcWx6NKdhhyRpOgCqmRO9Dnkz1N4cx3CN23Zei0tTq0kDwz+54IDUHgaMiXopiIH9Co7jAuA26fq8MWXnbd99tZ3swReZcAuKSd3xbRh4JdFfRdwPj4NNY=</Signature></Security></CUSTOMS>";
//                msg = "<CUSTOMS><Header><Message_Version>1.3</Message_Version><Sender_Code>79205002</Sender_Code><Sender_Name>Ngan Hang MHB</Sender_Name><Transaction_Type>22</Transaction_Type><Transaction_Name>Thông điệp xác nhận nộp thuế của cơ quan Hải quan</Transaction_Name><Transaction_Date>2013-07-01T02:26:24</Transaction_Date><Transaction_ID>3ffba974-9643-449a-8286-b55ee97413bd</Transaction_ID><Request_ID>b9f44ba1-3a7c-4301-bc04-c0db2c3eb38d</Request_ID></Header><Error><Error_Number>0</Error_Number><Error_Message>Xử lý thành công</Error_Message></Error><Security><Signature>KvjkuQpa3WwB8nAx9dItU40gAwh1jLl6iFpFDgLTMCIlL+Uob6bYVEr55BMI5JcO6wGJYSRFcFvPccXqnlq+qY5Swctg14ZySAwZcBVohoObXlH9L9FGOLJfVQ2zOYQOLYIqMux4pe+mBVDmXanGtau3E7Gj/Te5lab3+iTF7GDFplYpNGOJwvUCc8EzC3Wi/MqtOTXZYMhtYZfDuwEvRXkWll1sEww0wre43mOoH8CYVsfpeww9+9Bcmibc69JBPANautSYRQ5FZEhrEyBD9zX12yhSy1i37WwyKF6T/cUxNBWIZKrch5xHCKeEioxR7iF175OOBFlqBI5HIR16Yw==</Signature></Security></CUSTOMS>";
//            }
//            //String strMsgRes = a.WSProcess(strPublicKey, msg);
//            insertLog(msg, "", "", "");
//            //strMsgRes = "<?xml version='1.0' encoding='UTF-8'?><CUSTOMS><Header><Message_Version>1.3</Message_Version><Sender_Code>01302001</Sender_Code><Sender_Name>Ngan Hang Thuong Mai Co phan Hang Hai Viet Nam</Sender_Name><Transaction_Type>62</Transaction_Type><Transaction_Name>Thông điệp xác nhận bảo lãnh thuế của cơ quan Hải quan</Transaction_Name><Transaction_Date>2012-09-19T03:56:30</Transaction_Date><Transaction_ID>7e82baef-ccca-47a1-9249-dccad414e49c</Transaction_ID><Request_ID>315d8de0-9f93-4ba3-b7cc-76f30edd59f7</Request_ID></Header><Error><Error_Number>0</Error_Number><Error_Message>Xử lý thành công</Error_Message></Error><Security><Signature>B5MF4g/VmPfbwBY4c3pmFql7xt7qavb1X38LGAG4tMBOaUa7UTvJQHv723AvS2MsZdvZfg113HYpvKcCDoVmvrJs4ejRA+HFEzx4cUdY7pJdHJ6OI9qFG/KHPSWaEKIkUMEf5JyybwZg4E6PNRUqLs5srYz9uAByKFlZwHv9wf6wctTtF+Q86fm1z+BRycdgVucC8AkNijB5BQqFWDMtfyShciklOecXYMcCIGIJKLtbJqf3pzsSUVrw1ohgXAz5zJRVDNF6uZk5PFf8eCEBE7T6LKoKa4mvw3qzJIuIZ37m1InVwz7DqGrb+yN47sHm3TIz4pzfSkonbUdbkPDw2Q==</Signature></Security></CUSTOMS>";
//            return "";
//        }
//        public static String sendMsgDC(String msgType, String msg)
//        {
//            DCTTDTPortal.DCTTDTPortal a = new CustomsService.DCTTDTPortal.DCTTDTPortal();
//            a.Url = Business_HQ.Common.mdlCommon.GET_URL_HQ_DC();
//            //string strPublicKey2 = a.GetPublicKey();
//            string strPublicKey = Signature.SignatureProcess.getPublicKey();
//            String strMsgRes = a.ReconcileProcess(strPublicKey, msg);
//            //String strMsgRes = "<?xml version='1.0' encoding='UTF-8'?><CUSTOMS><Header><Message_Version>1.3</Message_Version><Sender_Code>01302001</Sender_Code><Sender_Name>Ngan Hang Thuong Mai Co phan Hang Hai Viet Nam</Sender_Name><Transaction_Type>82</Transaction_Type><Transaction_Name>Thông điệp trả lời nhận đối chiếu dữ liệu giao dịch bảo lãnh chứng từ</Transaction_Name><Transaction_Date>2012-09-19T04:40:22</Transaction_Date><Transaction_ID>45e2c462-cd43-455f-82c1-62b75d8a1fb8</Transaction_ID><Request_ID>307c11f0-eb5d-46de-b088-86d019f36e38</Request_ID></Header><Error><Error_Number>0</Error_Number><Error_Message>Đã nhận dữ liệu đối chiếu</Error_Message></Error><Security><Signature>owqVoxvp9caFmKDXF7RjVKRtEOz6c3WrD2rhMHPDyhI2HFFWF81tAvoF4g14BdeFdtNF4HLXCpmEKasvSa3ltjcCU04DMufROyVVtsOnfyK8TntaAVfPy3bIYxxu8IVHTm/U+lFHGlcFkBahHvYvjW64jNO3uVrLqGFjp3sRgtg0A+IOjisWee3zKRTOo0g79h4HX4bJ0tydhAzxInXtYsmj8P4KwniIIigkfKDZCIoeT2YIRI+jckBNL7h5DT5Fw0rdrAIGFdxkgOSwZqpp2T8TnrZVfDCWzpklXoBeSHESY1J6NZWr77WYj3UfjazAFk+Fv4kSiJIG95hr+Cy6rg==</Signature></Security></CUSTOMS>";// a.ReconcileProcess(strPublicKey, msg);
//            insertLog(msg, strMsgRes, msgType, "test");
//            return strMsgRes;
//        }
//        #endregion "Gui msg di HQ";

//        #region "Ky va verify chu ky tren msg";
//        public static string signMsg(String msg)
//        {
//            msg = msg.Replace("\r\n        ", String.Empty);
//            msg = msg.Replace("\r\n      ", String.Empty);
//            msg = msg.Replace("\r\n    ", String.Empty);
//            msg = msg.Replace("\r\n  ", String.Empty);
//            msg = msg.Trim();
//            Signature.SignatureProcess.ResultSign resultSign = Signature.SignatureProcess.Sign(msg);
//            if (resultSign.SignCode == 0)
//            {
//                msg = "<CUSTOMS>" + msg + "<Security><Signature>" + resultSign.SignMessage + "</Signature></Security></CUSTOMS>";
//                return msg;
//            }
//            else
//            {
//                throw new Exception("Loi ky tren msg: error_code: " + resultSign.SignCode + "; error_message: " + resultSign.SignMessage);
//            }
//        }
//        public static bool verifySignMsg(object objMsg, String objType)
//        {
//            return true;

//            string strSignData = null;
//            string strSign = null;
//            if (objType.Equals("MSG12"))
//            {
//                MSG12 msg = (MSG12)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSG12toXML(msg);
//            }
//            else if (objType.Equals("MSG22"))
//            {
//                MSG22 msg = (MSG22)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG32"))
//            {
//                MSG32 msg = (MSG32)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG42"))
//            {
//                MSG42 msg = (MSG42)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG44"))
//            {
//                MSG44 msg = (MSG44)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG52"))
//            {
//                MSG52 msg = (MSG52)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG54"))
//            {
//                MSG54 msg = (MSG54)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG62"))
//            {
//                MSG62 msg = (MSG62)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG72"))
//            {
//                MSG72 msg = (MSG72)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG84"))
//            {
//                MSG84 msg = (MSG84)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG84"))
//            {
//                MSG84 msg = (MSG84)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG92"))
//            {
//                MSG92 msg = (MSG92)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG94"))
//            {
//                MSG94 msg = (MSG94)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            else if (objType.Equals("MSG14"))
//            {
//                MSG94 msg = (MSG94)objMsg;
//                //Chu ky
//                strSign = msg.Security.Signature;
//                msg.Security = null;
//                msg.Error = null;
//                //Noi dung ky
//                strSignData = msg.MSGtoXML(msg);
//            }
//            strSignData = strSignData.Replace(strXMLdefout1, String.Empty);
//            strSignData = strSignData.Replace(strXMLdefout2, String.Empty);
//            strSignData = strSignData.Substring(2);
//            strSignData = strSignData.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty);
//            strSignData = strSignData.Replace("\r\n", String.Empty);
//            //Verify
//            bool resultVerify = Signature.SignatureProcess.Verify(strSignData, strSign);
//            //return true;
//            if (resultVerify)
//            {
//                return resultVerify;
//            }
//            else
//            {
//                return false;
//            }
//        }
//        #endregion "Ky va verify chu ky tren msg";



//        //public static void main()
//        //{
//        //    string pv_ErrorNum = "";
//        //    string pv_ErrorMes = "";
//        //    getMSG14("2500242917", "141200301", "DAB0000001", pv_ErrorNum, pv_ErrorMes);
//        //}




//        public static String MSG2_OUT(String msgin)
//        {
//            // Load the xml file into XmlDocument object.
//            XmlDocument xmlDoc = new XmlDocument();
//            try
//            {

//                if (msgin.ToUpper().Equals("MSG11"))
//                    xmlDoc.Load(@"C:\Temp\MG12.xml");

//                else if (msgin.ToUpper().Equals("MSG21"))
//                    xmlDoc.Load(@"C:\Temp\MG22.xml");

//                else if (msgin.ToUpper().Equals("MSG31"))
//                    xmlDoc.Load(@"C:\Temp\MG32.xml");

//                else if (msgin.ToUpper().Equals("MSG41"))
//                    xmlDoc.Load(@"C:\Temp\MG42.xml");

//                else if (msgin.ToUpper().Equals("MSG43"))
//                    xmlDoc.Load(@"C:\Temp\MG44.xml");
//                else if (msgin.ToUpper().Equals("MSG51"))
//                    xmlDoc.Load(@"C:\Temp\MG52.xml");
//                else if (msgin.ToUpper().Equals("MSG53"))
//                    xmlDoc.Load(@"C:\Temp\MG54.xml");
//                else if (msgin.ToUpper().Equals("MSG61"))
//                    xmlDoc.Load(@"C:\Temp\MG62.xml");
//                else if (msgin.ToUpper().Equals("MSG71"))
//                    xmlDoc.Load(@"C:\Temp\MG72.xml");
//                else if (msgin.ToUpper().Equals("MSG81"))
//                    xmlDoc.Load(@"C:\Temp\MG82.xml");
//                else if (msgin.ToUpper().Equals("MSG83"))
//                    xmlDoc.Load(@"C:\Temp\MG84.xml");
//                else if (msgin.ToUpper().Equals("MSG91"))
//                    xmlDoc.Load(@"C:\Temp\MG92.xml");
//                else if (msgin.ToUpper().Equals("MSG93"))
//                    xmlDoc.Load(@"C:\Temp\MG94.xml");


//            }
//            catch (XmlException e)
//            {
//                Console.WriteLine(e.Message);
//            }
//            // Now create StringWriter object to get data from xml document.
//            String tmp = "";
//            try
//            {
//                StringWriter sw = new StringWriter();
//                XmlTextWriter xw = new XmlTextWriter(sw);
//                xmlDoc.WriteTo(xw);
//                tmp = sw.ToString();
//            }
//            catch (Exception e)
//            {
//                throw e;
//            }
//            return tmp;
//        }
//        public static void InsertDCInfo(String transaction_id, String transaction_type, DateTime ngay_dc, String strLoai_BL)
//        {
//            OracleConnection conn = null;
//            IDbTransaction transCT = null;
//            try
//            {
//                Int64 iId = TCS_GetSequenceValue("SEQ_DCHIEU_HQNH_HDR_ID");

//                conn = GetConnection();
//                transCT = conn.BeginTransaction();
//                //insert tcs_dchieu_hqnh_dtl
//                StringBuilder sbsql = new StringBuilder();

//                sbsql.Append("INSERT INTO tcs_qly_dc (id, ");
//                sbsql.Append("ngay_dc, ");
//                sbsql.Append("transaction_id, ");
//                sbsql.Append("LOAI_BL, ");
//                sbsql.Append("loai_gd )");

//                sbsql.Append("VALUES( :id, ");
//                sbsql.Append(":ngay_dc, ");
//                sbsql.Append(":transaction_id, ");
//                sbsql.Append(":LOAI_BL, ");
//                sbsql.Append(":loai_gd) ");

//                IDbDataParameter[] p = new IDbDataParameter[5];

//                p[0] = new OracleParameter();
//                p[0].ParameterName = "ID";
//                p[0].DbType = DbType.Int64;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = DBNull.Value;
//                p[0].Value = iId;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "NGAY_DC";
//                p[1].DbType = DbType.DateTime;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = ngay_dc;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "TRANSACTION_ID";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = transaction_id;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "LOAI_BL";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = strLoai_BL;

//                p[4] = new OracleParameter();
//                p[4].ParameterName = "LOAI_GD";
//                p[4].DbType = DbType.String;
//                p[4].Direction = ParameterDirection.Input;
//                p[4].Value = transaction_type;
//                ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);
//                transCT.Commit();
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Lỗi lưu transaction_id đối chiếu, hãy gửi lại bản ghi. Chi tiết lỗi: " + ex.Message);
//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }
//        }
//        public static void UpdateDCInfo(String transaction_id, String transaction_type, string ErrNUM, string ErrMSG)
//        {
//            OracleConnection conn = null;
//            IDbTransaction transCT = null;
//            try
//            {
//                conn = GetConnection();
//                transCT = conn.BeginTransaction();
//                //insert tcs_dchieu_hqnh_dtl
//                StringBuilder sbsql = new StringBuilder();

//                sbsql.Append("UPDATE  TCS_QLY_DC SET ERRNUM = :ERRNUM, ERRMSG = :ERRMSG");
//                sbsql.Append(" WHERE TRANSACTION_ID = :transaction_id AND  ");
//                sbsql.Append(" LOAI_GD=:loai_gd");

//                IDbDataParameter[] p = new IDbDataParameter[4];

//                p[0] = new OracleParameter();
//                p[0].ParameterName = "ERRNUM";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                //p[0].Value = DBNull.Value;
//                p[0].Value = ErrNUM;

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "ERRMSG";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = ErrMSG;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "TRANSACTION_ID";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = transaction_id;

//                p[3] = new OracleParameter();
//                p[3].ParameterName = "LOAI_GD";
//                p[3].DbType = DbType.String;
//                p[3].Direction = ParameterDirection.Input;
//                p[3].Value = transaction_type;

//                ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, transCT);
//                transCT.Commit();
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Lỗi lưu transaction_id đối chiếu, hãy gửi lại bản ghi. Chi tiết lỗi: " + ex.Message);
//            }
//            finally
//            {
//                if ((transCT != null))
//                {
//                    transCT.Dispose();
//                }
//                if ((conn != null) || conn.State != ConnectionState.Closed)
//                {
//                    conn.Dispose();
//                    conn.Close();
//                }
//            }
//        }
//        public static String getReqId(DateTime pv_NgayDC, String type, String strLoai_BL)
//        {
//            try
//            {
//                StringBuilder sbSQL = new StringBuilder();
//                sbSQL.Append("SELECT transaction_id FROM  tcs_qly_dc where (1=1) ");
//                sbSQL.Append("AND id = (SELECT MAX(id) FROM tcs_qly_dc where (1=1) ");
//                sbSQL.Append("  AND to_char(ngay_dc,'yyyy-MM-dd') = :ngay_dc ");
//                sbSQL.Append("  AND loai_gd = :loai_gd ");
//                sbSQL.Append("  AND LOAI_BL = :LOAI_BL ");
//                sbSQL.Append("  )");

//                IDbDataParameter[] p = new IDbDataParameter[3];

//                p[0] = new OracleParameter();
//                p[0].ParameterName = "NGAY_DC";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = DBNull.Value;
//                p[0].Value = pv_NgayDC.ToString("yyyy-MM-dd");

//                p[1] = new OracleParameter();
//                p[1].ParameterName = "LOAI_GD";
//                p[1].DbType = DbType.String;
//                p[1].Direction = ParameterDirection.Input;
//                p[1].Value = type;

//                p[2] = new OracleParameter();
//                p[2].ParameterName = "LOAI_BL";
//                p[2].DbType = DbType.String;
//                p[2].Direction = ParameterDirection.Input;
//                p[2].Value = strLoai_BL;

//                String strSQL = sbSQL.ToString();
//                DataSet ds = ExecuteReturnDataSet(strSQL, CommandType.Text, p);
//                String strReqId = ds.Tables[0].Rows[0]["TRANSACTION_ID"].ToString();
//                return strReqId;
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Không tìm được Request ID đối chiếu. Hãy thực hiện lại quy trình đối chiếu hoặc liên hệ quản trị tin học\nChi tiết lỗi: " + ex.Message);
//            }
//        }
//        //*****************************TEST TAI*************************************************************
//        public static String getMSG42_TEST(String max)
//        {
//            DateTime pv_NgayDC = DateTime.Now;
//            String pv_ErrorNum;
//            String pv_ErrorMes;
//            try
//            {
//                MSG41 msg41 = new MSG41();
//                msg41.Data = new CustomsV3.MSG.MSG41.Data();
//                msg41.Header = new CustomsV3.MSG.MSG41.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg41.Header.Message_Version = strMessage_Version;
//                msg41.Header.Sender_Code = strSender_Code;
//                msg41.Header.Sender_Name = strSender_Name;
//                msg41.Header.Transaction_Type = "41";
//                msg41.Header.Transaction_Name = "Thông điệp đối chiếu dữ liệu giao dịch thành công";
//                msg41.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                msg41.Header.Transaction_ID = Guid.NewGuid().ToString();

//                //Gan du lieu Data
//                DOICHIEUToMSG41_TEST(pv_NgayDC, ref msg41, max);
//                //InsertDCInfo(msg41.Header.Transaction_ID, "MSG41", pv_NgayDC);

//                //Chuyen thanh XML
//                string strXMLOut = msg41.MSG41toXML(msg41);
//                //Thay the cac chuoi define
//                strXMLOut = strXMLOut.Replace(strXMLdefout1, String.Empty);
//                strXMLOut = strXMLOut.Replace(strXMLdefout2, String.Empty);
//                strXMLOut = strXMLOut.Substring(2);

//                //Ky tren msg
//                strXMLOut = signMsg(strXMLOut.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                //Goi den Web Service
//                String strXMLIn = sendMsgDC("MSG41", strXMLOut);

//                //Lay gia tri tra ve dua vao DB
//                strXMLIn = strXMLIn.Replace("<CUSTOMS>", strXMLdefin2);
//                //                StringBuilder sb = new StringBuilder();
//                //                sb.Append(strXMLdefin1);
//                //                sb.Append("\r\n");
//                //                sb.Append(strXMLIn);
//                //                strXMLIn = sb.ToString();

//                MSG42 msg42 = new MSG42();
//                MSG42 objTemp = msg42.MSGToObject(strXMLIn);
//                //Verify sign                
//                pv_ErrorNum = objTemp.Error.Error_Number;
//                pv_ErrorMes = objTemp.Error.Error_Message;

//                //Verify sign                
//                if (!verifySignMsg(objTemp, "MSG42"))
//                {
//                    pv_ErrorNum = "00001";
//                    pv_ErrorMes = "Lỗi xác thực chữ ký!";
//                }
//                return pv_ErrorNum + " - " + pv_ErrorMes;
//            }
//            catch (Exception ex)
//            {
//                return ex.Message;
//            }

//        }
//        public static void DOICHIEUToMSG41_TEST(DateTime pv_NgayDC, ref MSG41 msg41, String max)
//        {
//            msg41.Data = new CustomsV3.MSG.MSG41.Data();

//            msg41.Data.tem.Ma_NH_DC = strSender_Code;
//            msg41.Data.tem.Ngay_DC = pv_NgayDC.ToString("yyyy-MM-dd");


//            StringBuilder sbsql = new StringBuilder();

//            DataSet ds = null;
//            DataSet ds2 = null;
//            int idxDC = 0;
//            int idxMax = Int32.Parse(max);
//            String tran_id = "";
//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   hdr.id,");
//                sbsql.Append("         hdr.transaction_id transaction_id,");
//                sbsql.Append("         hdr.SO_TN_CT SO_TN_CT,");
//                sbsql.Append("         hdr.NGAY_TN_CT NGAY_TN_CT,");
//                sbsql.Append("         hdr.ma_nnthue ma_dv,");
//                sbsql.Append("         hdr.ten_nnthue ten_dv,");
//                sbsql.Append("         hdr.ma_nh_ph ma_nh_ph,");
//                sbsql.Append("         '' ten_nh_ph,");
//                sbsql.Append("         hdr.ma_nh_th ma_nh_th,");
//                sbsql.Append("         hdr.ten_nh_th ten_nh_th,");
//                sbsql.Append("         hdr.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         hdr.ma_hq_cqt ma_hq_cqt,");
//                sbsql.Append("         hdr.ma_hq ma_hq,");
//                sbsql.Append("         hdr.lhxnk ma_lh,");
//                sbsql.Append("         hdr.so_tk sotk,");
//                sbsql.Append("         to_char(hdr.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         nvl(hdr.ma_loaitien,'1') ma_lt,");
//                //sbsql.Append("         NVL(hdr.ma_nkt,DECODE(SUBSTR(hdr.tkkb_ct,0,3),'741','1','2')) ma_ntk,");
//                sbsql.Append("         hdr.tkkb_ct ma_ntk,");
//                sbsql.Append("         hdr.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(hdr.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         hdr.shkb ma_kb,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (hdr.shkb) ten_kb,");
//                sbsql.Append("         hdr.tkkb_ct tkkb,");
//                sbsql.Append("         hdr.ttien ttien,");
//                sbsql.Append("         hdr.tkkb_ct tkkb_ct,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bn, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (hdr.ngay_bc, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         TO_CHAR (TO_DATE (TO_CHAR (hdr.ngay_ct), 'RRRRMMDD'), 'RRRR-MM-DD')");
//                sbsql.Append("             ngay_ct,");
//                sbsql.Append("         hdr.dien_giai diengiai");
//                sbsql.Append("  FROM   tcs_dchieu_nhhq_hdr hdr");
//                sbsql.Append(" WHERE   hdr.transaction_type = :transaction_type ");


//                IDbDataParameter[] p = null;
//                p = new IDbDataParameter[1];
//                p[0] = new OracleParameter();
//                p[0].ParameterName = "transaction_type";
//                p[0].DbType = DbType.String;
//                p[0].Direction = ParameterDirection.Input;
//                p[0].Value = "M41";

//                ds2 = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds2.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                List<CustomsV3.MSG.MSG41.ItemData> listItems = new List<CustomsV3.MSG.MSG41.ItemData>();
//                int i = 0;


//                while (idxDC < idxMax)
//                {

//                    foreach (DataRow drhdr in ds2.Tables[0].Rows)
//                    {
//                        idxDC++;
//                        if (idxDC > idxMax) break;
//                        CustomsV3.MSG.MSG41.ItemData item = new CustomsV3.MSG.MSG41.ItemData();
//                        item.Transaction_ID = drhdr["TRANSACTION_ID"].ToString();
//                        tran_id = drhdr["TRANSACTION_ID"].ToString();
//                        item.So_TN_CT = drhdr["SO_TN_CT"].ToString();
//                        item.Ngay_TN_CT = drhdr["NGAY_TN_CT"].ToString();
//                        //Muc dich: Them cac truong msg hq can
//                        //Tac gia : Anhld
//                        item.Ma_NH_PH = strSender_Code;
//                        item.Ten_NH_PH = strSender_Name;
//                        item.Ma_NH_TH = drhdr["Ma_NH_TH"].ToString();
//                        item.Ten_NH_TH = drhdr["Ten_NH_TH"].ToString();
//                        item.Ma_DV = drhdr["MA_DV"].ToString();
//                        item.Ten_DV = drhdr["TEN_DV"].ToString();
//                        item.Ma_HQ_CQT = drhdr["MA_HQ_CQT"].ToString();
//                        item.Ma_HQ_PH = drhdr["MA_HQ_PH"].ToString();
//                        item.Ma_HQ = drhdr["MA_HQ"].ToString();
//                        item.Ma_LH = drhdr["MA_LH"].ToString();
//                        item.So_TK = drhdr["SOTK"].ToString();
//                        item.Ngay_DK = drhdr["NGAY_DK"].ToString();
//                        item.Ma_LT = drhdr["MA_LT"].ToString();
//                        //item.Ma_NTK = drhdr["MA_NTK"].ToString();
//                        item.Loai_CT = strLoai_CT;
//                        if (drhdr["MA_NTK"].ToString().Substring(0, 3) == "741" || drhdr["MA_NTK"].ToString().Substring(0, 4) == "7111")
//                        {
//                            item.Ma_NTK = "1";
//                        }
//                        else
//                        {
//                            item.Ma_NTK = "2";
//                        }
//                        item.KyHieu_CT = drhdr["KYHIEU_CT"].ToString();
//                        item.So_CT = drhdr["SO_CT"].ToString();
//                        item.TTButToan = drhdr["TTBUTTOAN"].ToString();//Hoi lai
//                        item.Ma_KB = drhdr["MA_KB"].ToString();//Hoi lai
//                        item.Ten_KB = Globals.RemoveSign4VietnameseString(drhdr["TEN_KB"].ToString());//Hoi lai
//                        item.TKKB = drhdr["TKKB"].ToString();
//                        item.TKKB_CT = drhdr["TKKB_CT"].ToString();
//                        item.Ngay_BN = DateTime.Now.ToString("yyyy-MM-dd");
//                        item.Ngay_BC = DateTime.Now.ToString("yyyy-MM-dd");
//                        item.Ngay_CT = DateTime.Now.ToString("yyyy-MM-dd");
//                        item.DienGiai = drhdr["DIENGIAI"].ToString();
//                        item.DuNo_TO = drhdr["TTIEN"].ToString();

//                        //Lay thong tin detail
//                        StringBuilder sbsqldetail = new StringBuilder();
//                        sbsqldetail.Append("SELECT   dtl.ma_chuong ma_chuong,");
//                        sbsqldetail.Append("         dtl.ma_nkt ma_khoan,");
//                        sbsqldetail.Append("         dtl.ma_ndkt ma_tmuc,");
//                        sbsqldetail.Append("         dtl.sotien sotien");
//                        sbsqldetail.Append("  FROM   tcs_dchieu_nhhq_dtl dtl");
//                        sbsqldetail.Append(" WHERE   ( (dtl.hdr_id = :hdr_id))");

//                        p = new IDbDataParameter[1];
//                        p[0] = new OracleParameter();
//                        p[0].ParameterName = "HDR_ID";
//                        p[0].DbType = DbType.Int64;
//                        p[0].Direction = ParameterDirection.Input;
//                        p[0].Value = Int64.Parse(drhdr["ID"].ToString());

//                        ds = ExecuteReturnDataSet(sbsqldetail.ToString(), CommandType.Text, p);
//                        item.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();

//                        foreach (DataRow row in ds.Tables[0].Rows)
//                        {
//                            //Gan so tien thue XK
//                            if (row["ma_tmuc"].ToString().StartsWith("185"))
//                            {
//                                //item.Chuong_XK = row["ma_chuong"].ToString();
//                                item.Khoan_XK = row["ma_khoan"].ToString();
//                                item.TieuMuc_XK = row["ma_tmuc"].ToString();
//                                item.DuNo_XK = row["sotien"].ToString();
//                            }

//                            //Gan so tien thue XK
//                            if (row["ma_tmuc"].ToString().StartsWith("190") && (!row["ma_tmuc"].ToString().Equals("1903")))
//                            {
//                                // item.Chuong_NK = row["ma_chuong"].ToString();
//                                item.Khoan_NK = row["ma_khoan"].ToString();
//                                item.TieuMuc_NK = row["ma_tmuc"].ToString();
//                                item.DuNo_NK = row["sotien"].ToString();
//                            }

//                            //Gan so tien thue VA
//                            if (row["ma_tmuc"].ToString().StartsWith("170"))
//                            {
//                                // item.Chuong_VA = row["ma_chuong"].ToString();
//                                item.Khoan_VA = row["ma_khoan"].ToString();
//                                item.TieuMuc_VA = row["ma_tmuc"].ToString();
//                                item.DuNo_VA = row["sotien"].ToString();
//                            }

//                            //Gan so tien thue TD
//                            if (row["ma_tmuc"].ToString().StartsWith("175") || row["ma_tmuc"].ToString().Equals("1903"))
//                            {
//                                //  item.Chuong_TD = row["ma_chuong"].ToString();
//                                item.Khoan_TD = row["ma_khoan"].ToString();
//                                item.TieuMuc_TD = row["ma_tmuc"].ToString();
//                                item.DuNo_TD = row["sotien"].ToString();
//                            }

//                            //Gan so tien thue TV
//                            if (row["ma_tmuc"].ToString().StartsWith("195"))
//                            {
//                                //  item.Chuong_TV = row["ma_chuong"].ToString();
//                                item.Khoan_TV = row["ma_khoan"].ToString();
//                                item.TieuMuc_TV = row["ma_tmuc"].ToString();
//                                item.DuNo_TV = row["sotien"].ToString();
//                            }
//                            //Gan so tien thue MT
//                            if (row["ma_tmuc"].ToString().StartsWith("200"))
//                            {
//                                // item.Chuong_MT = row["ma_chuong"].ToString();
//                                item.Khoan_MT = row["ma_khoan"].ToString();
//                                item.TieuMuc_MT = row["ma_tmuc"].ToString();
//                                item.DuNo_MT = row["sotien"].ToString();
//                            }
//                        }
//                        listItems.Add(item);
//                    }
//                }
//                msg41.Data.Item = listItems;

//            }
//            catch (Exception ex)
//            {
//                StringBuilder sbErrMsg = default(StringBuilder);
//                sbErrMsg = new StringBuilder();
//                sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUToMSG41): Ngay - ");
//                sbErrMsg.Append(pv_NgayDC.ToString());
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.Message);
//                sbErrMsg.Append("\n");
//                sbErrMsg.Append(ex.StackTrace);

//                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);

//                if ((ex.InnerException != null))
//                {
//                    sbErrMsg = new StringBuilder();
//                    sbErrMsg.Append("Lỗi trong quá trình lấy thông tin chi tiết chứng từ can doi chieu ngan hang va HQ (DOICHIEUToMSG41): Ngay - ");
//                    sbErrMsg.Append(pv_NgayDC.ToString());
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.Message);
//                    sbErrMsg.Append("\n");
//                    sbErrMsg.Append(ex.InnerException.StackTrace);

//                    LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
//                }

//                throw ex;

//            }
//            finally
//            {
//            }


//        }
//        //**********************************************************************************************
//        public static void CTUToMSG21_Test(int pv_strSoCT, ref MSG21 msg21)
//        {
//            msg21.Data = new CustomsV3.MSG.MSG21.Data();


//            //Lay thong tin Header CTU
//            StringBuilder sbsql = new StringBuilder();
//            DataSet ds = null;

//            try
//            {
//                //Lay thong tin header
//                sbsql.Append("SELECT   h.ten_nnthue ten_dv,");
//                sbsql.Append("         h.ma_nnthue ma_nnthue,");
//                sbsql.Append("         h.ten_nh_b ten_nh_th,");
//                sbsql.Append("         h.ma_cqthu ma_hq_cqt,");
//                sbsql.Append("         h.ma_hq_ph ma_hq_ph,");
//                sbsql.Append("         h.ma_hq ma_hq,");
//                sbsql.Append("         h.lh_xnk ma_lh,");
//                sbsql.Append("         h.so_tk sotk,");
//                sbsql.Append("         to_char(h.ngay_tk,'YYYY-MM-DD') ngay_dk,");
//                sbsql.Append("         h.loai_tt ma_lt,");
//                //sbsql.Append("         NVL(h.ma_ntk,DECODE(SUBSTR(h.tk_co,0,3),'741','1','711','1','2')) ma_ntk,");
//                sbsql.Append("         h.ma_ntk ma_ntk,");
//                sbsql.Append("         h.kyhieu_ct kyhieu_ct,");
//                sbsql.Append("         TO_NUMBER(h.so_ct) so_ct,");
//                sbsql.Append("         1 ttbuttoan,");
//                sbsql.Append("         ma_nh_a ma_nh_ph,");
//                sbsql.Append("         ma_nh_b ma_nh_th,");
//                sbsql.Append("         h.shkb ma_kb,");
//                sbsql.Append("         h.ngay_kb ngay_kb,");
//                sbsql.Append("         h.ma_nv ma_nv,");
//                sbsql.Append("         h.so_bt so_bt,");
//                sbsql.Append("         tcs_pck_util.fnc_get_ten_kbnn (h.shkb) ten_kb,");
//                sbsql.Append("         h.tk_co tkkb,");
//                sbsql.Append("         h.tk_co tkkb_ct,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bn,");
//                sbsql.Append("         TO_CHAR (h.ngay_ht, 'RRRR-MM-DD') ngay_bc,");
//                sbsql.Append("         to_char(to_date(ngay_ct,'YYYYmmDD'),'YYYY-MM-DD') ngay_ct,");
//                sbsql.Append("         hdr.ly_do diengiai,");
//                sbsql.Append("         h.ttien ttien");
//                sbsql.Append("  FROM   tcs_ctu_hdr h where rownum=1 and ma_lthue='04' order by so_ct desc");
//                // sbsql.Append(" WHERE  (h.so_ct := SoCT )");


//                //IDbDataParameter[] p = null;
//                //p = new IDbDataParameter[1];
//                //p[0] = new OracleParameter();
//                //p[0].ParameterName = "SoCT";
//                //p[0].DbType = DbType.String;
//                //p[0].Direction = ParameterDirection.Input;
//                //p[0].Value = pv_strSoCT;

//                IDbDataParameter[] p = new IDbDataParameter[0];
//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg21.Data.Ma_DV = ds.Tables[0].Rows[0]["ma_nnthue"].ToString(); ;
//                msg21.Data.Ma_NH_PH = ds.Tables[0].Rows[0]["MA_NH_PH"].ToString();
//                msg21.Data.Ten_NH_PH = strSender_Name;
//                msg21.Data.Ma_NH_TH = ds.Tables[0].Rows[0]["MA_NH_TH"].ToString();
//                msg21.Data.Ten_NH_TH = ds.Tables[0].Rows[0]["TEN_NH_TH"].ToString();
//                msg21.Data.Ten_DV = ds.Tables[0].Rows[0]["TEN_DV"].ToString();
//                msg21.Data.Ma_HQ_CQT = ds.Tables[0].Rows[0]["MA_HQ_CQT"].ToString();
//                msg21.Data.Ma_HQ_PH = ds.Tables[0].Rows[0]["MA_HQ_PH"].ToString();
//                msg21.Data.Ma_HQ = ds.Tables[0].Rows[0]["MA_HQ"].ToString();
//                msg21.Data.Ma_LH = ds.Tables[0].Rows[0]["MA_LH"].ToString();
//                msg21.Data.So_TK = ds.Tables[0].Rows[0]["SOTK"].ToString();
//                msg21.Data.Ngay_DK = ds.Tables[0].Rows[0]["NGAY_DK"].ToString();
//                msg21.Data.Ma_LT = ds.Tables[0].Rows[0]["MA_LT"].ToString();
//                msg21.Data.Ma_NTK = ds.Tables[0].Rows[0]["MA_NTK"].ToString();
//                //if ((ds.Tables[0].Rows[0]["MA_NTK"].ToString().Substring(0, 3) == "741") || (ds.Tables[0].Rows[0]["MA_NTK"].ToString().Substring(0, 4) == "7111"))
//                //{
//                //    msg21.Data.Ma_NTK = "1";
//                //}
//                //else {
//                //    msg21.Data.Ma_NTK = "2";
//                //}
//                msg21.Data.Loai_CT = strLoai_CT;// GNT từ ngân hàng không có TKKB
//                msg21.Data.KyHieu_CT = "TES" + pv_strSoCT.ToString(); //ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
//                msg21.Data.So_CT = pv_strSoCT.ToString();
//                msg21.Data.TTButToan = ds.Tables[0].Rows[0]["TTBUTTOAN"].ToString();//Hoi lai
//                msg21.Data.Ma_KB = ds.Tables[0].Rows[0]["MA_KB"].ToString();//Hoi lai
//                msg21.Data.Ten_KB = Globals.RemoveSign4VietnameseString(ds.Tables[0].Rows[0]["TEN_KB"].ToString());//Hoi lai
//                msg21.Data.TKKB = ds.Tables[0].Rows[0]["TKKB"].ToString();
//                msg21.Data.TKKB_CT = ds.Tables[0].Rows[0]["TKKB_CT"].ToString();
//                msg21.Data.Ngay_BN = ds.Tables[0].Rows[0]["NGAY_BN"].ToString();
//                msg21.Data.Ngay_BC = ds.Tables[0].Rows[0]["NGAY_BC"].ToString();
//                msg21.Data.Ngay_CT = ds.Tables[0].Rows[0]["NGAY_CT"].ToString();
//                msg21.Data.DienGiai = ds.Tables[0].Rows[0]["DIENGIAI"].ToString();
//                msg21.Data.DuNo_TO = ds.Tables[0].Rows[0]["TTIEN"].ToString();

//                //Lay thong tin detail
//                sbsql = new StringBuilder();
//                sbsql.Append("SELECT   d.ky_thue,");
//                sbsql.Append("         d.ma_chuong,");
//                sbsql.Append("         d.ma_khoan,");
//                sbsql.Append("         d.ma_tmuc,");
//                sbsql.Append("         d.sotien");
//                sbsql.Append("  FROM   tcs_ctu_dtl d");
//                sbsql.Append(" WHERE   (    (d.shkb ='" + ds.Tables[0].Rows[0]["MA_KB"].ToString() + "')");
//                sbsql.Append("          AND (d.ma_nv = '" + ds.Tables[0].Rows[0]["ma_nv"].ToString() + "')");
//                // sbsql.Append("          AND (d.so_ct = :so_ct)");
//                sbsql.Append("          AND (d.so_bt = '" + ds.Tables[0].Rows[0]["so_bt"].ToString() + "')");
//                sbsql.Append("          AND (d.ngay_kb ='" + ds.Tables[0].Rows[0]["ngay_kb"].ToString() + "' ))");


//                //Lay thong tin chi tiet chung tu

//                ds = ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

//                if (ds.Tables[0].Rows.Count <= 0)
//                {
//                    throw new Exception("Không tìm thấy chứng từ!");
//                }
//                msg21.Data.Ma_Chuong = ds.Tables[0].Rows[0]["ma_chuong"].ToString();

//                foreach (DataRow row in ds.Tables[0].Rows)
//                {
//                    bool bFlag_Gan_TMT = false;
//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("185"))
//                    {
//                        msg21.Data.Khoan_XK = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_XK = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_XK = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue XK
//                    if (row["ma_tmuc"].ToString().StartsWith("190"))
//                    {
//                        msg21.Data.Khoan_NK = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_NK = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_NK = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue VA
//                    if (row["ma_tmuc"].ToString().StartsWith("170"))
//                    {
//                        msg21.Data.Khoan_VA = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_VA = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_VA = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TD
//                    if (row["ma_tmuc"].ToString().StartsWith("175"))
//                    {
//                        msg21.Data.Khoan_TD = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_TD = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_TD = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    //Gan so tien thue TV
//                    if (row["ma_tmuc"].ToString().StartsWith("195"))
//                    {
//                        msg21.Data.Khoan_TV = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_TV = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_TV = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                    //Gan so tien thue MT
//                    if (row["ma_tmuc"].ToString().StartsWith("200"))
//                    {
//                        msg21.Data.Khoan_MT = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_MT = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_MT = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }//4253
//                    if (row["ma_tmuc"].ToString().StartsWith("425"))
//                    {
//                        msg21.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_KH = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }

//                    if (bFlag_Gan_TMT == false)
//                    {
//                        msg21.Data.Khoan_KH = row["ma_khoan"].ToString();
//                        msg21.Data.TieuMuc_KH = row["ma_tmuc"].ToString();
//                        msg21.Data.DuNo_KH = row["sotien"].ToString();
//                        bFlag_Gan_TMT = true;
//                    }
//                }


//            }
//            catch (Exception ex)
//            {

//                throw ex;

//            }
//            finally
//            {
//            }


//        }
//        public static void getMSG22_test(string i)
//        {

//            try
//            {
//                string pv_transaction_id = "";
//                MSG21 msg21 = new MSG21();
//                msg21.Data = new CustomsV3.MSG.MSG21.Data();
//                msg21.Header = new CustomsV3.MSG.MSG21.Header();

//                //Gan du lieu
//                //Gan du lieu Header
//                msg21.Header.Message_Version = strMessage_Version;
//                msg21.Header.Sender_Code = strSender_Code;
//                msg21.Header.Sender_Name = strSender_Name;
//                msg21.Header.Transaction_Type = "21";
//                msg21.Header.Transaction_Name = "Thông điệp xác nhận nộp thuế của ngân hàng";
//                msg21.Header.Transaction_Date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
//                pv_transaction_id = Guid.NewGuid().ToString();
//                msg21.Header.Transaction_ID = pv_transaction_id;

//                //Gan du lieu Data
//                //for (int i = int.Parse(tuso); i <= int.Parse(denso); i++)
//                //{
//                    pv_transaction_id = Guid.NewGuid().ToString();
//                    string strmsg21 = "<Header><Message_Version>2.0</Message_Version><Sender_Code>" + strSender_Code + "</Sender_Code><Sender_Name>" + strSender_Name + "</Sender_Name><Transaction_Type>21</Transaction_Type><Transaction_Name>Thông điệp xác nhận nộp thuế của ngân hàng</Transaction_Name><Transaction_Date> " + msg21.Header.Transaction_Date + " </Transaction_Date><Transaction_ID>" + pv_transaction_id + "</Transaction_ID></Header><Data><Ma_NH_PH>" + strSender_Code + "</Ma_NH_PH><Ten_NH_PH>" + strSender_Name + "</Ten_NH_PH><Ma_NH_TH>01201003</Ma_NH_TH><Ten_NH_TH>NH CONG THUONG  DONG DA</Ten_NH_TH><Ma_DV>2600604769</Ma_DV><Ma_Chuong>754</Ma_Chuong><Ten_DV>Công Ty Cổ Phần Hoá Chất - Khoáng Sản Htđ</Ten_DV><Ma_HQ_PH>01BT</Ma_HQ_PH><Ma_HQ_CQT>2995430</Ma_HQ_CQT><Ma_HQ>01BT</Ma_HQ><Ma_LH>B11</Ma_LH><So_TK>30003134272</So_TK><Ngay_DK>2014-06-10</Ngay_DK><Ma_LT>1</Ma_LT><Ma_NTK>1</Ma_NTK><Loai_CT>22</Loai_CT><KyHieu_CT>SHB" + i.ToString() + "</KyHieu_CT><So_CT>" + i.ToString() + "</So_CT><TTButToan>1</TTButToan><Ma_KB>0011</Ma_KB><Ten_KB>VP KBNN Ha Noi</Ten_KB><TKKB>7111</TKKB><TKKB_CT>7111</TKKB_CT><Ngay_BN>2014-11-17</Ngay_BN><Ngay_BC>2014-11-17</Ngay_BC><Ngay_CT>2014-11-17</Ngay_CT><DienGiai>thanh toan tien mat</DienGiai><Khoan_XK>000</Khoan_XK><TieuMuc_XK>1851</TieuMuc_XK><DuNo_XK>8780426</DuNo_XK><Khoan_NK /><TieuMuc_NK /><DuNo_NK>0</DuNo_NK><Khoan_VA /><TieuMuc_VA /><DuNo_VA>0</DuNo_VA><Khoan_TD /><TieuMuc_TD /><DuNo_TD>0</DuNo_TD><Khoan_TV /><TieuMuc_TV /><DuNo_TV>0</DuNo_TV><Khoan_MT /><TieuMuc_MT /><DuNo_MT>0</DuNo_MT><Khoan_KH /><TieuMuc_KH /><DuNo_KH>0</DuNo_KH><DuNo_TO>8780426</DuNo_TO></Data>";
//                    strmsg21 = signMsg(strmsg21.Replace(strCustomsOpenTag, String.Empty).Replace(strCustomsCloseTag, String.Empty));
//                    String strXMLIn = sendMsgTest("MSG21", strmsg21);
//                //}
//            }
//            catch (Exception ex)
//            {

//            }

//        }
//    }
//}
