﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Data;
using System.Collections;
using System.Configuration;

namespace CustomsServiceV3
{
    public class OracleDataHelper
    {
        #region "Khai bao bien"
        private static String strCustomOn = ConfigurationManager.AppSettings.Get("CUSTOMS.ON").ToString();
        private static String strTen_NH_PH = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString();

        private static String strConnection1 = ConfigurationManager.AppSettings.Get("CTC_DB_CONN").ToString();
        private static String strConnection = VBOracleLib.Security.DescryptStr(strConnection1);
        private static String strMessage_Name = ConfigurationManager.AppSettings.Get("CUSTOMS.Application_Name").ToString();
        private static String strMessage_Version = ConfigurationManager.AppSettings.Get("CUSTOMS.Message_Version").ToString();
        private static String strSender_Code = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
        private static String strSender_Name = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
        private static String strSender_TaxNo = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Taxno").ToString();

        #endregion "Khai bao bien"

        #region "Utility"
        /// <summary>
        /// Hàm lấy connection đến CSDL và thực hiện mở connection
        /// </summary>
        /// <returns>Oracle Connection</returns>
        /// <remarks>
        ///</remarks>
        public static OracleConnection GetConnection()
        {
            OracleConnection _connection = default(OracleConnection);
            try
            {
                _connection = new OracleConnection(strConnection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            _connection.Open();
            return _connection;
        }

        /// <summary>
        /// Hàm thực thi câu lệnh không trả về dữ liệu record (dùng để thực hiện lệnh insert, update, delete hoặc các store procedure)
        /// </summary>
        /// <param name="Statement">Câu lệnh thực thi</param>
        /// <param name="cmdType">Loại lệnh thực thi</param>
        /// <param name="arrParamIn">Các tham số truyền vào câu lệnh</param>
        /// <param name="trans">Transaction thực thi câu lệnh</param>
        /// <returns>Kết quả thực hiện</returns>
        /// <remarks>
        ///</remarks>
        public static int ExecuteNonQuery(string Statement, CommandType cmdType, IDataParameter[] arrParamIn, IDbTransaction trans)
        {
            OracleCommand oCommand = null;
            int rows = 0;
            ArrayList outList = new ArrayList();
            bool leaveOpen = false;
            try
            {
                IDataParameter p = null;
                oCommand = new OracleCommand();
                oCommand.CommandText = Statement;
                oCommand.CommandType = cmdType;
                oCommand.Connection = (OracleConnection)trans.Connection;
                oCommand.Transaction = (OracleTransaction)trans;

                foreach (IDataParameter p_loopVariable in arrParamIn)
                {
                    p = p_loopVariable;
                    oCommand.Parameters.Add(p);
                }
                rows = oCommand.ExecuteNonQuery();
                oCommand.Parameters.Clear();
                return rows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCommand.Dispose();
            }

        }

        /// <summary>
        /// Hàm thực thi câu lệnh không trả về dữ liệu record (dùng để thực hiện lệnh insert, update, delete hoặc các store procedure)
        /// </summary>
        /// <param name="Statement">Câu lệnh thực thi</param>
        /// <param name="cmdType">Loại lệnh thực thi</param>
        /// <param name="arrParamIn">Các tham số truyền vào câu lệnh</param>
        /// <param name="trans">Transaction thực thi câu lệnh</param>
        /// <returns>Kết quả thực hiện</returns>
        /// <remarks>
        ///</remarks>
        public static int ExecuteNonQuery(string Statement, CommandType cmdType, OracleParameter[] arrParamIn, IDbTransaction trans)
        {
            OracleCommand oCommand = null;
            int rows = 0;
            ArrayList outList = new ArrayList();
            bool leaveOpen = false;
            try
            {
                IDataParameter p = null;
                oCommand = new OracleCommand();
                oCommand.CommandText = Statement;
                oCommand.CommandType = cmdType;
                oCommand.Connection = (OracleConnection)trans.Connection;
                oCommand.Transaction = (OracleTransaction)trans;

                foreach (OracleParameter p_loopVariable in arrParamIn)
                {
                    p = p_loopVariable;
                    oCommand.Parameters.Add(p);
                }
                rows = oCommand.ExecuteNonQuery();
                oCommand.Parameters.Clear();
                return rows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCommand.Dispose();
            }

        }

        /// <summary>
        /// Hàm thực thi câu lệnh trả về dữ liệu là một Dataset
        /// </summary>
        /// <param name="Statement">Câu lệnh thực thi</param>
        /// <param name="cmdType">Loại lệnh thực thi</param>
        /// <param name="arrParamIn">Các tham số truyền vào câu lệnh</param>
        /// <returns>Dataset chứa dữ liệu</returns>
        /// <remarks>
        ///</remarks>
        public static DataSet ExecuteReturnDataSet(string Statement, CommandType cmdType, IDataParameter[] arrParamIn)
        {
            OracleCommand oCommand = null;
            ArrayList outList = new ArrayList();
            DataSet ds = new DataSet();
            OracleDataAdapter da = default(OracleDataAdapter);

            try
            {
                IDataParameter p = null;
                oCommand = new OracleCommand();
                oCommand.CommandText = Statement;
                oCommand.CommandType = cmdType;
                oCommand.Connection = GetConnection();

                da = new OracleDataAdapter();

                foreach (IDataParameter p_loopVariable in arrParamIn)
                {
                    p = p_loopVariable;
                    oCommand.Parameters.Add(p);
                }

                da.SelectCommand = oCommand;
                da.Fill(ds);
                return ds;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oCommand.Connection != null)
                {
                    oCommand.Connection.Close();
                }
                oCommand.Parameters.Clear();
                oCommand.Dispose();
            }

        }


        public static DataSet ExecuteReturnDataSet(string Statement, CommandType cmdType, IDataParameter[] arrParamIn, OracleConnection conn)
        {
            OracleCommand oCommand = null;
            ArrayList outList = new ArrayList();
            bool leaveOpen = false;
            DataSet ds = new DataSet();
            OracleDataAdapter da = default(OracleDataAdapter);

            try
            {
                IDataParameter p = null;
                oCommand = new OracleCommand();
                oCommand.CommandText = Statement;
                oCommand.CommandType = cmdType;
                oCommand.Connection = conn;

                da = new OracleDataAdapter();

                foreach (IDataParameter p_loopVariable in arrParamIn)
                {
                    p = p_loopVariable;
                    oCommand.Parameters.Add(p);
                }

                da.SelectCommand = oCommand;
                da.Fill(ds);
                return ds;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCommand.Parameters.Clear();
                oCommand.Dispose();
            }

        }
        #endregion
    }

}