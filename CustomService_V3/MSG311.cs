﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
//using System.Data.OracleClient;
using System.Data;
namespace CustomsV3.MSG.MSG311
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG311
    {
        public MSG311()
        {
           
        }
        [XmlElement("Document")]
        public Document Document;
        public MSG311 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG311));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG311 LoadedObjTmp = (MSG311)serializer.Deserialize(reader);
            if (LoadedObjTmp.Document.Data.ThongTin_NNT == null)
            {
                LoadedObjTmp = LoadManual(p_XML, LoadedObjTmp);
            }
            return LoadedObjTmp;

        }
        private MSG311 LoadManual(string p_XML, MSG311 LoadedObjTmp)
        {
            MSG311 obj= LoadedObjTmp ;
            XmlDocument vdoc = new XmlDocument();
            string strXMLTemp = p_XML.Replace(CustomsServiceV3.ProcessMSG.strXMLdefin2, "<Customs>");
            vdoc.LoadXml(strXMLTemp);
            obj.Document.Data.ThongTin_NNT = new ThongTin_NNT();

            //XmlNode vNode = vdoc.SelectSingleNode("/Customs/Data/ThongTin_NTT/So_CMT");
            string str = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT").InnerXml;
            StringReader strReader = new StringReader("<ThongTin_NTT>" + str + "</ThongTin_NTT>");
            DataSet ds = new DataSet();
            ds.ReadXml(strReader);
            obj.Document.Data.ThongTin_NNT.So_CMT = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT/So_CMT").InnerText;
            obj.Document.Data.ThongTin_NNT.Ho_Ten = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT/Ho_Ten").InnerText;
            obj.Document.Data.ThongTin_NNT.NgaySinh = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT/NgaySinh").InnerText;
            obj.Document.Data.ThongTin_NNT.NguyenQuan = vdoc.SelectSingleNode("/Customs/Document/Data/ThongTin_NTT/NguyenQuan").InnerText;
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables["ThongTinLienHe"] != null)
                {
                    obj.Document.Data.ThongTin_NNT.ThongTinLienHe = new List<ThongTinLienHe>();
                    for (int i = 0; i < ds.Tables["ThongTinLienHe"].Rows.Count; i++)
                    {
                        ThongTinLienHe objtmp = new ThongTinLienHe();
                        objtmp.So_DT = ds.Tables["ThongTinLienHe"].Rows[i]["So_DT"].ToString();
                        objtmp.Email = ds.Tables["ThongTinLienHe"].Rows[i]["Email"].ToString();
                        obj.Document.Data.ThongTin_NNT.ThongTinLienHe.Add(objtmp);
                    }
                }
                if (ds.Tables["ChungThuSo"] != null && ds.Tables["ChungThuSo"].Rows.Count > 0)
                {
                    ChungThuSo objtmp = new ChungThuSo();
                    objtmp.SerialNumber = ds.Tables["ChungThuSo"].Rows[0]["SerialNumber"].ToString();
                    objtmp.Noi_Cap = ds.Tables["ChungThuSo"].Rows[0]["Noi_Cap"].ToString();
                    objtmp.Ngay_HL = ds.Tables["ChungThuSo"].Rows[0]["Ngay_HL"].ToString();
                    objtmp.Ngay_HHL = ds.Tables["ChungThuSo"].Rows[0]["Ngay_HHL"].ToString();
                    objtmp.PublicKey = ds.Tables["ChungThuSo"].Rows[0]["PublicKey"].ToString();
                    obj.Document.Data.ThongTin_NNT.ChungThuSo = objtmp;

                }
                if (ds.Tables["ThongTinTaiKhoan"] != null && ds.Tables["ThongTinTaiKhoan"].Rows.Count > 0)
                {
                    ThongTinTaiKhoan objtmp = new ThongTinTaiKhoan();
                    objtmp.Ma_NH_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["Ma_NH_TH"].ToString();
                    objtmp.Ten_NH_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["Ten_NH_TH"].ToString();
                    objtmp.TaiKhoan_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["TaiKhoan_TH"].ToString();
                    objtmp.Ten_TaiKhoan_TH = ds.Tables["ThongTinTaiKhoan"].Rows[0]["Ten_TaiKhoan_TH"].ToString();
                    obj.Document.Data.ThongTin_NNT.ThongTinTaiKhoan = objtmp;

                }

            }

            return obj;

        }
    
        public string MSG311toXML(MSG311 p_MsgIn)
        {

            XmlSerializer serializer = new XmlSerializer(typeof(MSG311));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public string getSoDT()
        {
            try
            {
                if (Document != null)
                {
                    if (Document.Data != null)
                    {
                        if (Document.Data.ThongTin_NNT != null)
                        {
                            if (Document.Data.ThongTin_NNT.ThongTinLienHe != null)
                            {
                                foreach (ThongTinLienHe objx in Document.Data.ThongTin_NNT.ThongTinLienHe)
                                {
                                    if (objx.So_DT.Trim().Length > 0)
                                    {
                                        return objx.So_DT.Trim();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return "";
        }
        public string getEmail()
        {
            try
            {
                if (Document != null)
                {
                    if (Document.Data != null)
                    {
                        if (Document.Data.ThongTin_NNT != null)
                        {
                            if (Document.Data.ThongTin_NNT.ThongTinLienHe != null)
                            {
                                foreach (ThongTinLienHe objx in Document.Data.ThongTin_NNT.ThongTinLienHe)
                                {
                                    if (objx.So_DT.Trim().Length > 0)
                                    {
                                        return objx.Email.Trim();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return "";
        }
        public string GetThongTinLienHe()
        {
            string strTT = "";
            try
            {
                if (Document != null)
                {
                    if (Document.Data != null)
                    {
                        if (Document.Data.ThongTin_NNT != null)
                        {
                            if (Document.Data.ThongTin_NNT.ThongTinLienHe != null)
                            {
                                foreach (ThongTinLienHe objx in Document.Data.ThongTin_NNT.ThongTinLienHe)
                                {
                                    strTT += "<ThongTinLienHe><So_DT>"+ objx.So_DT +"</So_DT><Email>"+ objx.Email +"</Email></ThongTinLienHe>";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return strTT;
        }
        
       
    }
    public class Document
    {
        public Document()
        {}
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Signature")]
        public Signature Signature;
    }
    public class Data
    {
        public Data()
        {  }
        [XmlElement("So_HS")]
        public string So_HS { get; set; }
        [XmlElement("Loai_HS")]
        public string Loai_HS { get; set; }
        [XmlElement("Ma_DV")]
        public string Ma_DV { get; set; }
        [XmlElement("Ten_DV")]
        public string Ten_DV { get; set; }
        [XmlElement("DiaChi")]
        public string DiaChi { get; set; }
        [XmlElement("ThongTin_NNT")]
        public ThongTin_NNT ThongTin_NNT;
      
    }
    public class ThongTin_NNT
    {
        public ThongTin_NNT()
        { }
        [XmlElement("So_CMT")]
        public string So_CMT { get; set; }
        [XmlElement("Ho_Ten")]
        public string Ho_Ten { get; set; }
        [XmlElement("NgaySinh")]
        public string NgaySinh { get; set; }
        [XmlElement("NguyenQuan")]
        public string NguyenQuan { get; set; }
        [XmlElement("ThongTinLienHe")]
        public List<ThongTinLienHe> ThongTinLienHe;
        [XmlElement("ChungThuSo")]
        public ChungThuSo ChungThuSo ;
        [XmlElement("ThongTinTaiKhoan")]
        public ThongTinTaiKhoan ThongTinTaiKhoan ;
    }
    public class ThongTinLienHe
    {
        public ThongTinLienHe()
        {
        }
        [XmlElement("So_DT")]
        public string So_DT { get; set; }
        [XmlElement("Email")]
        public string Email { get; set; }
    }
    public class ChungThuSo
    {
        public ChungThuSo()
        {
        }
        [XmlElement("SerialNumber")]
        public string SerialNumber { get; set; }
        [XmlElement("Noi_Cap")]
        public string Noi_Cap { get; set; }
        [XmlElement("Ngay_HL")]
        public string Ngay_HL { get; set; }
        [XmlElement("Ngay_HHL")]
        public string Ngay_HHL { get; set; }
        [XmlElement("PublicKey")]
        public string PublicKey { get; set; }
    }
    public class ThongTinTaiKhoan
    {
        public ThongTinTaiKhoan()
        {
        }
        [XmlElement("Ma_NH_TH")]
        public string Ma_NH_TH { get; set; }
        [XmlElement("Ten_NH_TH")]
        public string Ten_NH_TH { get; set; }
        [XmlElement("TaiKhoan_TH")]
        public string TaiKhoan_TH { get; set; }
        [XmlElement("Ten_TaiKhoan_TH")]
        public string Ten_TaiKhoan_TH { get; set; }
       
    }
   
   

    public class Signature
    {
        public Signature() { }
    }
}
