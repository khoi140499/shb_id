﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace CustomsV3.MSG.MSG48
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG48
    {
        public MSG48()
        {
        }
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Security")]
        public Security Security;
        [XmlElement("Error")]
        public Error Error;

        public MSG48 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG48.MSG48));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG48.MSG48 LoadedObjTmp = (CustomsV3.MSG.MSG48.MSG48)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSGtoXML(MSG48 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG48));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

    public class Data
    {
        public Data()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Transaction_ID { get; set; }
        public string So_TN_CT { get; set; }
        public string Ngay_TN_CT { get; set; }
        public string Ma_NH_PH { get; set; }
        public string Ten_NH_PH { get; set; }
        public string Ma_NH_TH { get; set; }
        public string Ten_NH_TH { get; set; }
        public string Ma_DV { get; set; }
        public string Ma_Chuong { get; set; }
        public string Ten_DV { get; set; }
        public string Ma_KB { get; set; }
        public string Ten_KB { get; set; }
        public string TKKB { get; set; }
        public string Ma_NTK { get; set; }
        public string Ma_HQ_PH { get; set; }
        public string Ma_HQ_CQT { get; set; }
        public string KyHieu_CT { get; set; }
        public string So_CT { get; set; }
        public string Loai_CT { get; set; }
        public string So_TN_CTS { get; set; }
        public string Ngay_TN_CTS { get; set; }
        public string Ngay_BN { get; set; }
        public string Ngay_BC { get; set; }
        public string Ngay_CT { get; set; }
        public string SoTien_TO { get; set; }
        public string DienGiai { get; set; }
        [XmlElement("GNT_CT")]
        public List<GNT_CT> GNT_CT;

    }
    public class GNT_CT
    {
        public GNT_CT()
        {
        }
        public string TTButToan { get; set; }
        public string Ma_HQ { get; set; }
        public string Ma_LH { get; set; }
        public string Nam_DK { get; set; }
        public string So_TK { get; set; }
        public string Ma_LT { get; set; }
        [XmlElement("ToKhai_CT")]
        public List<ToKhai_CT> ToKhai_CT;
    }
    public class ToKhai_CT
    {
        public ToKhai_CT()
        {
        }
        public string Ma_ST { get; set; }
        public string NDKT { get; set; }
        public string SoTien { get; set; }
        public string KQ_DC { get; set; }
        
    }

    public class Header
    {
        public Header()
        {
        }
        public string Message_Version { get; set; }
        public string Sender_Code { get; set; }
        public string Sender_Name { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Name { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_ID { get; set; }
        public string Request_ID { get; set; }
    }
    public class Security
    {
        public Security()
        {
        }
        public string Signature { get; set; }
    }
    public class Error
    {
        public Error()
        {
        }
        public string Error_Number { get; set; }
        public string Error_Message { get; set; }

    }
}
