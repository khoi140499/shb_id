﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
namespace CustomsV3.MSG.MSG84
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG84
    {
        public MSG84()
        {
        }
        [XmlElement("Header", Order = 1)]
        public Header Header;
        [XmlElement("Data", Order = 2)]
        public Data Data;
        [XmlElement("Error", Order = 3)]
        public Error Error;
        [XmlElement("Signature", Order = 4)]
        public Signature Signature;

        public MSG84 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG84.MSG84));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG84.MSG84 LoadedObjTmp = (CustomsV3.MSG.MSG84.MSG84)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSGtoXML(MSG84 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG84));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

    public class Data
    {
        public Data()
        {
        }
        [XmlElement("Transactions")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Transaction_ID { get; set; }
        public string So_TN_CT { get; set; }
        public string Ngay_TN_CT { get; set; }
        public string Ma_NH_PH { get; set; }
        public string MST_NH_PH { get; set; }
        public string Ten_NH_PH { get; set; }
        public string Ma_DV { get; set; }
        public string Ten_DV { get; set; }
        public string Ma_DV_DD { get; set; }
        public string Ten_DV_DD { get; set; }
        public string Ma_HQ_PH { get; set; }
        public string Ma_HQ_CQT { get; set; }
        public string Ma_HQ { get; set; }
        public string Ma_LH { get; set; }
        public string So_TK { get; set; }
        public string Ngay_DK { get; set; }
        public string Ma_LT { get; set; }
        public string Loai_CT { get; set; }
        public string KyHieu_CT { get; set; }
        public string So_CT { get; set; }
        public string Ngay_CT { get; set; }
        public string TTButToan { get; set; }
        public string SNBL { get; set; }
        public string Ngay_HL { get; set; }
        public string Ngay_HHL { get; set; }
        public string SoTien { get; set; }
        public string DienGiai { get; set; }
        public string KQ_DC { get; set; }
    }

    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
}
