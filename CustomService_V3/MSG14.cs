﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
namespace CustomsV3.MSG.MSG204
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG204
    {
        public MSG204()
        {
        }
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Signature")]
        public Security Signature; 

        public MSG204 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG204.MSG204));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG204.MSG204 LoadedObjTmp = (CustomsV3.MSG.MSG204.MSG204)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
        public string MSG204toXML(MSG204 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG204));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;
        }
        private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_DV { get; set; }
        public string So_CT { get; set; }
        public string KyHieu_CT { get; set; }
        public string Ngay_CT { get; set; }
        public string Ngay_HL { get; set; }
        public string Ngay_HHL { get; set; }
        public string HanMuc { get; set; }
        public string SoDu { get; set; }
        [XmlElement("Item")]
        public List<ItemData> Item{ get; set; }
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string So_TK { get; set; }
        public string Ngay_DK { get; set; }
        public string So_SD { get; set; }
        public string So_PH { get; set; }
    }

    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }

}
