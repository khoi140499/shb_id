﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
using CustomsV3.MSG.SECURITY;

namespace CustomsV3.MSG.MSG102
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG102
    {
        public MSG102()
        {
        }
        [XmlElement("Header")]
        public Header Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Security")]
        public Security Security;        

        public string MSG102toXML(MSG102 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG102));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG102 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG102.MSG102));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG102.MSG102 LoadedObjTmp = (CustomsV3.MSG.MSG102.MSG102)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string Ma_DV { get; set; }
        public string Nam_DK { get; set; }
        public string So_TK { get; set; }
    }
   
}
