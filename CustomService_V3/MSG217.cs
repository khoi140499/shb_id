﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
//using System.Data.OracleClient;
using Business_HQ.nhothu;
using System.Data;
namespace CustomsV3.MSG.MSG217
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG217
    {
        
        public MSG217()
        {
           
        }
       
        [XmlElement("Document")]
        public Document Document;
        [XmlElement("DigitalSignatures")]
        public DigitalSignatures DigitalSignatures;
        public MSG217 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG217));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG217 LoadedObjTmp = (MSG217)serializer.Deserialize(reader);
           
            return LoadedObjTmp;

        }
       

        public string MSG217toXML(MSG217 p_MsgIn)
        {

            XmlSerializer serializer = new XmlSerializer(typeof(MSG217));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public string getSoDT()
        {
            try
            {
                if (Document != null)
                {
                    if (Document.Data != null)
                    {
                        if (Document.Data.ThongTinDK.ThongTin_NNT != null)
                        {
                            if (Document.Data.ThongTinDK.ThongTin_NNT.ThongTinLienHe != null)
                            {
                                foreach (ThongTinLienHe objx in Document.Data.ThongTinDK.ThongTin_NNT.ThongTinLienHe)
                                {
                                    if (objx.So_DT.Trim().Length > 0)
                                    {
                                        return objx.So_DT.Trim();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return "";
        }
        public string getEmail()
        {
            try
            {
                if (Document != null)
                {
                    if (Document.Data != null)
                    {
                        if (Document.Data.ThongTinDK.ThongTin_NNT != null)
                        {
                            if (Document.Data.ThongTinDK.ThongTin_NNT.ThongTinLienHe != null)
                            {
                                foreach (ThongTinLienHe objx in Document.Data.ThongTinDK.ThongTin_NNT.ThongTinLienHe)
                                {
                                    if (objx.So_DT.Trim().Length > 0)
                                    {
                                        return objx.Email.Trim();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return "";
        }
        public string GetThongTinLienHe()
        {
            string strTT = "";
            try
            {
                if (Document != null)
                {
                    if (Document.Data != null)
                    {
                        if (Document.Data.ThongTinDK.ThongTin_NNT != null)
                        {
                            if (Document.Data.ThongTinDK.ThongTin_NNT.ThongTinLienHe != null)
                            {
                                foreach (ThongTinLienHe objx in Document.Data.ThongTinDK.ThongTin_NNT.ThongTinLienHe)
                                {
                                    strTT += "<ThongTinLienHe><So_DT>"+ objx.So_DT +"</So_DT><Email>"+ objx.Email +"</Email></ThongTinLienHe>";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return strTT;
        }
        public string TRANGTHAI { get; set; }
       
    }
    public class Document
    {
        public Document()
        {}
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
     }
    public class Data
    {
        public Data()
        { }
        [XmlElement("TrangThaiDK")]
        public string TrangThaiDK { get; set; }

        [XmlElement("ThongTinDK")]
        public ThongTinDK ThongTinDK { get; set; }
      
    }
    public class ThongTinDK
    {
        public ThongTinDK()
        { }
        [XmlElement("So_HS")]
        public string So_HS { get; set; }
       
        [XmlElement("Ma_DV")]
        public string Ma_DV { get; set; }
        [XmlElement("Ten_DV")]
        public string Ten_DV { get; set; }
        [XmlElement("DiaChi")]
        public string DiaChi { get; set; }
        [XmlElement("Ngay_HL_UQ")]
        public string Ngay_HL_UQ { get; set; }
        [XmlElement("Ngay_HHL_UQ")]
        public string Ngay_HHL_UQ { get; set; }
        [XmlElement("ThongTin_NNT")]
        public ThongTin_NNT ThongTin_NNT;
    
    }
    public class ThongTin_NNT
    {
        public ThongTin_NNT()
        { }
        [XmlElement("So_CMT")]
        public string So_CMT { get; set; }
        [XmlElement("Ho_Ten")]
        public string Ho_Ten { get; set; }
        [XmlElement("NgaySinh")]
        public string NgaySinh { get; set; }
        [XmlElement("NguyenQuan")]
        public string NguyenQuan { get; set; }
        [XmlElement("ThongTinLienHe")]
        public List<ThongTinLienHe> ThongTinLienHe;
        [XmlElement("ChungThuSo")]
        public ChungThuSo ChungThuSo ;
        [XmlElement("ThongTinTaiKhoan")]
        public ThongTinTaiKhoan ThongTinTaiKhoan ;
    }
    public class ThongTinLienHe
    {
        public ThongTinLienHe()
        {
        }
        [XmlElement("So_DT")]
        public string So_DT { get; set; }
        [XmlElement("Email")]
        public string Email { get; set; }
    }
    public class ChungThuSo
    {
        public ChungThuSo()
        {
        }
        [XmlElement("SerialNumber")]
        public string SerialNumber { get; set; }
        [XmlElement("Noi_Cap")]
        public string Noi_Cap { get; set; }
        [XmlElement("Ngay_HL")]
        public string Ngay_HL { get; set; }
        [XmlElement("Ngay_HHL")]
        public string Ngay_HHL { get; set; }
        [XmlElement("PublicKey")]
        public string PublicKey { get; set; }
    }
    public class ThongTinTaiKhoan
    {
        public ThongTinTaiKhoan()
        {
        }
        [XmlElement("Ma_NH_TH")]
        public string Ma_NH_TH { get; set; }
        [XmlElement("Ten_NH_TH")]
        public string Ten_NH_TH { get; set; }
        [XmlElement("TaiKhoan_TH")]
        public string TaiKhoan_TH { get; set; }
        [XmlElement("Ten_TaiKhoan_TH")]
        public string Ten_TaiKhoan_TH { get; set; }
       
    }

    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }
    public class DigitalSignatures
    {

    }
  
}
