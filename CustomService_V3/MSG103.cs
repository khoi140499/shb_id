﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;

namespace CustomsV3.MSG.MSG103
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG103
    {
        public MSG103()
        {
        }

        [XmlElement("Header")]
        public Header Header{ get; set; }
        [XmlElement("Data")]
        public VMLePhiHaiQuanTraCuu Data{ get; set; }        
        [XmlElement("Security")]
        public Security Security{ get; set; }        

        public string MSG103toXML(MSG103 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG103));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG103 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG103.MSG103));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG103.MSG103 LoadedObjTmp = (CustomsV3.MSG.MSG103.MSG103)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }

    public class VMLePhiHaiQuanTraCuu
    {
        public VMLePhiHaiQuanTraCuu()
        {
        }
        public string So_HS { get; set; }
        public string Ma_DVQL { get; set; }
        public string KyHieu_CT { get; set; }
        public string So_CT { get; set; }
        public string Nam_CT { get; set; }
    }
   
}
