﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.MSGError;
namespace CustomsV3.MSG.MSG91
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG91
    {
        public MSG91()
        {
        }

        [XmlElement("Header", Order = 1)]
        public HeaderCustoms Header;
        [XmlElement("Data", Order = 2)]
        public Data Data;
        [XmlElement("Error", Order = 3)]
        public Error Error;
        [XmlElement("Signature", Order = 4)]
        public Signature Signature;

        public string MSG91toXML(MSG91 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG91));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            strTemp = strTemp.Replace("<TEMP>", "");
            strTemp = strTemp.Replace("</TEMP>", "");
            strTemp = strTemp.Replace("<TEMP />", "");
            return strTemp;

        }
    }

    public class Data
    {
        public Data()
        {
            temp = new Temp();
        }
        /*public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }*/
        [XmlElement("TEMP")]
        public Temp temp;
        [XmlElement("Accept_Transactions")]
        public Accept_Transactions Accept_Transactions;
        [XmlElement("Reject_Transactions")]
        public Reject_Transactions Reject_Transactions;
    }
    public class Temp
    {
        public Temp()
        {
        }
        public string Ma_NH_DC { get; set; }
        public string Ngay_DC { get; set; }
        //public string Loai_BL { get; set; }
        public string Loai_TD_DC { get; set; }
        
    }
    public class Accept_Transactions
    {
        public Accept_Transactions()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class Reject_Transactions
    {
        public Reject_Transactions()
        {
        }
        [XmlElement("Transaction")]
        public List<ItemData> Item;
    }
    public class ItemData
    {
        public ItemData()
        {
        }
        public string Transaction_ID { get; set; }
        public string So_TN_CT { get; set; }
        public string Ngay_TN_CT { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
}
