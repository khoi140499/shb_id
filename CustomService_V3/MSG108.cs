﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
using CustomsV3.MSG.MSGHeaderCustoms;
using System.Configuration;
namespace CustomsV3.MSG.MSG108
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG108
    {
        public MSG108()
        {
            Document = new Document();
            Document.Header = new HeaderCustoms("108", "Tra cứu thông tin NNT đăng ký ủy quyền TCHQ thông báo phát sinh nợ tới NHTM)","");
            Document.Header.Application_Name = ConfigurationManager.AppSettings["CUSTOMS.Application_VersionHQ247"];
            Document.Data = new Data();
            Document.Error = new Error();
            //Document.Signature = new Signature();
            DigitalSignatures = new DigitalSignatures();
        }
       
        [XmlElement("Document")]
        public Document Document;
        [XmlElement("DigitalSignatures")]
        public DigitalSignatures DigitalSignatures;
        public string MSG108toXML(MSG108 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG108));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG108 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomsV3.MSG.MSG108.MSG108));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CustomsV3.MSG.MSG108.MSG108 LoadedObjTmp = (CustomsV3.MSG.MSG108.MSG108)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class Document
    {
        public Document()
        { }
        [XmlElement(ElementName = "Header", Order = 1)]
        public HeaderCustoms Header { get; set; }
        [XmlElement(ElementName = "Data", Order = 2)]
        public Data Data { get; set; }
        [XmlElement(ElementName = "Error", Order = 3)]
        public Error Error;
    }
    public class Data
    {
        public Data()
        {
        }
        public string Ma_DV { get; set; }
    }
    public class Error
    {
        public Error() { }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }
    }
    public class DigitalSignatures
    {

    }
    public class Signature
    {
        public Signature() { }

    }
}
