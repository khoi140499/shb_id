﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeaderCustoms;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
namespace CustomsV3.MSG.MSG302
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG302
    {
        public MSG302()
        {
            Error = new Error();
        }
        [XmlElement("Header")]
        public HeaderCustoms Header;
        [XmlElement("Data")]
        public Data Data;
        [XmlElement("Error")]
        public Error Error;
        [XmlElement("Signature")]
        public Signature Signature;

        public string MSG302toXML(MSG302 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG302));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }
    public class Data
    {
        [XmlElement(Order = 1)]
        public string Ma_NH_PH { get; set; }
        [XmlElement(Order = 2)]
        public string Ten_NH_PH { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_NH_TH { get; set; }
        [XmlElement(Order = 4)]
        public string Ten_NH_TH { get; set; }
        [XmlElement(Order = 5)]
        public string Ma_DV { get; set; }
        [XmlElement(Order = 6)]
        public string Ma_Chuong { get; set; }
        [XmlElement(Order = 7)]
        public string Ten_DV { get; set; }
        [XmlElement(Order = 8)]
        public string Ma_KB { get; set; }
        [XmlElement(Order = 9)]
        public string Ten_KB { get; set; }
        [XmlElement(Order = 10)]
        public string TKKB { get; set; }
        [XmlElement(Order = 11)]
        public string Ma_NTK { get; set; }
        [XmlElement(Order = 12)]
        public string Ma_HQ_PH { get; set; }
        [XmlElement(Order = 13)]
        public string Ma_HQ_CQT { get; set; }
        [XmlElement(Order = 14)]
        public string KyHieu_CT { get; set; }
        [XmlElement(Order = 15)]
        public string So_CT { get; set; }
        [XmlElement(Order = 16)]
        public string Loai_CT { get; set; }
        [XmlElement(Order = 17)]
        public string So_TN_CTS { get; set; }
        [XmlElement(Order = 18)]
        public string Ngay_TN_CTS { get; set; }
        [XmlElement(Order = 19)]
        public string Ngay_BN { get; set; }
        [XmlElement(Order = 20)]
        public string Ngay_BC { get; set; }
        [XmlElement(Order = 21)]
        public string Ngay_CT { get; set; }
        [XmlElement(Order = 22)]
        public string SoTien_TO { get; set; }
        [XmlElement(Order = 23)]
        public string DienGiai { get; set; }
        [XmlElement(Order = 24)]
        public List<GNT_CT> GNT_CT;
    }
    public class GNT_CT
    {
        public GNT_CT()
        {
        }
        [XmlElement(Order = 1)]
        public string TTButToan { get; set; }
        [XmlElement(Order = 2)]
        public string Ma_HQ { get; set; }
        [XmlElement(Order = 3)]
        public string Ma_LH { get; set; }
        [XmlElement(Order = 4)]
        public string Nam_DK { get; set; }
        [XmlElement(Order = 5)]
        public string So_TK { get; set; }
        [XmlElement(Order = 6)]
        public string Ma_LT { get; set; }
        [XmlElement(Order = 7)]
        public List<ToKhai_CT> ToKhai_CT;
    }
    public class ToKhai_CT
    {
        public ToKhai_CT()
        {
        }
        public string Ma_ST { get; set; }
        public string NDKT { get; set; }
        public string SoTien { get; set; }
    }

    public class Signature
    {
        public Signature() { }

    }
}
