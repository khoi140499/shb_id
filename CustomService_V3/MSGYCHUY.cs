﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using CustomsV3.MSG.MSGHeader;
using CustomsV3.MSG.SECURITY;
using CustomsV3.MSG.MSGError;
using CustomsV3.MSG.MSGHeaderCustoms;
namespace CustomsV3.MSG.MSG_YCHUY
{
    [Serializable]
    [XmlRootAttribute("Customs", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG_YCHUY
    {
        public MSG_YCHUY()
        {

        }
        [XmlElement("Header")]
        public HeaderCustoms Header { get; set; }
        [XmlElement("Data")]
        public Data Data { get; set; }

        [XmlElement("Error")]
        public Error Error { get; set; }

        [XmlElement("Security")]
        public Security Security { get; set; }

        public string MSG_YCHUYtoXML(MSG_YCHUY p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG_YCHUY));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
    }

    public class Data
    {
        public Data()
        {
        }
        public string So_TN_CT_YCH { get; set; }
    }
}
