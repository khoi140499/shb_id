﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VBOracleLib;
using APIMOBITCTINCTU.Models;
using System.Data.OracleClient;
using Newtonsoft.Json;
using System.Configuration;
using System.Threading;
using System.Collections;
namespace APIMOBITCTINCTU
{
    public class MobiTCTProcess
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Hashtable hsCTU = new Hashtable();
        public static void AddCTU(string pvStrCTU, string pvStrTime)
        {
            try
            {
                hsCTU.Add(pvStrCTU, pvStrTime);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
        }
        public static bool validToken(string pvStrCTU)
        {
            try
            {
                if (hsCTU.ContainsKey(pvStrCTU))
                {
                    hsCTU.Remove(pvStrCTU);
                    return true;
                }

                //return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
            return false;
        }
        public static void ClearCTU()
        {
            try
            {
                int timeLimit = int.Parse(ConfigurationManager.AppSettings["MOBITCT.CLEARTIME"].ToString());
                DateTime dClear = DateTime.Now.AddSeconds(-timeLimit);
                int tClear = int.Parse(dClear.ToString("yyMMddHHmmss"));
                if (hsCTU.Count <= 0)
                    return;
                foreach (DictionaryEntry de in hsCTU)
                {
                    //neu thoi gian nho hon quy dinh thi clear hehe
                    if (int.Parse(de.Value.ToString()) <= tClear)
                    {
                        hsCTU.Remove(de.Key);
                    }
                    // Console.WriteLine("Key = {0}, Value = {1}", de.Key, de.Value);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
        }
        public static string AddCTUIn(string strMaThamChieu)
        {
            try
            {
                string tClear = DateTime.Now.ToString("yyMMddHHmmss");
                string st = strMaThamChieu + "|" + tClear;
                string Ss = VBOracleLib.Security.Encrypt3Des(st);
                AddCTU(Ss, tClear);
                return Ss;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
            return "";
        }
        //public static string getLogID()
        //{
        //    string strResult = DateTime.Now.ToString("yyyyMMdd");
        //    string strSQL = "SELECT LPAD( SEQ_LOG_API_TCT_MOBI.NEXTVAL,8,'0') FROM DUAL";
        //    DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
        //    if (ds != null)
        //        if (ds.Tables.Count > 0)
        //            if (ds.Tables[0].Rows.Count > 0)
        //                strResult = strResult + ds.Tables[0].Rows[0][0].ToString();
        //    return strResult;
        //}
        //public static void InsertLog(string strId, string strMSG, string strMSGType)
        //{
        //    try
        //    {
        //        string strSQL = "INSERT INTO SET_LOG_API_TCT_MOBI(ID, REQ_TYPE, REQ_MSG, REQ_TIME)";
        //        strSQL += "VALUES(:ID, :REQ_TYPE, :REQ_MSG, SYSDATE)";

        //        List<IDbDataParameter> lst = new List<IDbDataParameter>();
        //        lst.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, strId, DbType.String));
        //        lst.Add(DataAccess.NewDBParameter("REQ_TYPE", ParameterDirection.Input, strMSGType, DbType.String));
        //        lst.Add(DataAccess.NewDBParameter("REQ_MSG", ParameterDirection.Input, strMSG, DbType.String));
        //        DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray());
        //        //  return true;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Info("InsertLog :" + strMSG);
        //        log.Error(ex.Message + "-" + ex.StackTrace);
        //        //  return false;
        //    }
        //    //  return false;

        //}
        //public static void UpdateLog(string strId, string strRESMSG, string strErrCode, string strErrMSG, string strMST = "")
        //{
        //    try
        //    {
        //        string strSQL = "UPDATE SET_LOG_API_TCT_MOBI SET RES_TIME=sysdate, RES_MSG=:RES_MSG, ERRCODE=:ERRCODE, ERRMSG=:ERRMSG,MST='" + strMST + "' where id='" + strId + "'";


        //        List<IDbDataParameter> lst = new List<IDbDataParameter>();

        //        lst.Add(DataAccess.NewDBParameter("RES_MSG", ParameterDirection.Input, strRESMSG, DbType.String));
        //        lst.Add(DataAccess.NewDBParameter("ERRCODE", ParameterDirection.Input, strErrCode, DbType.String));
        //        lst.Add(DataAccess.NewDBParameter("ERRMSG", ParameterDirection.Input, strErrMSG, DbType.String));
        //        DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray());
        //        // return true;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Info("Update Log:" + strRESMSG);
        //        log.Error(ex.Message + "-" + ex.StackTrace);
        //        // return false;
        //    }
        //    // return false;

        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static string ConvertDateToNumber(string strDate)
        {
            string[] arr;

            try
            {

                strDate = strDate.Trim();
                if (strDate.Length < 10)
                    return "0";
                arr = strDate.Split('/');

                arr[0] = arr[0].PadLeft(2, '0');
                arr[1] = arr[1].PadLeft(2, '0');
                if (arr[2].Length == 2)
                    arr[2] = "20" + arr[2];
            }
            catch (Exception ex)
            {
                return DateTime.Now.ToString("yyyyMMdd");
            }

            return arr[2] + arr[1] + arr[0];
        }

        //public static string GenerateKey(string strSeq, int intLength = 0)
        //{
        //    string strReSult = "";
        //    try
        //    {
        //        string strSQL = "SELECT LPAD(" + strSeq + ".NEXTVAL," + intLength + ",'0')  FROM DUAL";
        //        if (intLength == 0)
        //            strSQL = "SELECT " + strSeq + ".NEXTVAL  FROM DUAL";
        //        DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
        //        if (ds != null)
        //            if (ds.Tables.Count > 0)
        //                if (ds.Tables[0].Rows.Count > 0)
        //                    strReSult = ds.Tables[0].Rows[0][0].ToString();
        //        if (intLength > 0 && strReSult.Length <= intLength)
        //            strReSult = strReSult.PadLeft(intLength, '0');
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.Message + " " + ex.StackTrace);
        //    }
        //    return strReSult;
        //}
        //public static string TCS_GetMaChiNhanh(string strMa_NV)
        //{
        //    try
        //    {
        //        string strSQL = string.Empty;
        //        strSQL = "SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" + strMa_NV + "'";
        //        return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.Message + " " + ex.StackTrace);
        //        throw ex;

        //    }
        //    // return "";
        //}
        //public static string TCS_GetMaCN_NH(string strMa_PGD)
        //{
        //    try
        //    {
        //        string strSQL = string.Empty;
        //        strSQL = "SELECT a.id, a.name, a.branch_code FROM tcs_dm_chinhanh a where a.id='" + strMa_PGD + "' ";
        //        return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.Message + " " + ex.StackTrace);
        //        throw ex;
        //    }
        //}
        //public static string TCS_GetTenCNNH(string p_strMaCN)
        //{
        //    try
        //    {
        //        string strSQL = string.Empty;
        //        strSQL = "SELECT a.id, a.name, a.branch_id FROM tcs_dm_chinhanh a where a.branch_id='" + p_strMaCN + "' ";
        //        return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][1].ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.Message + " " + ex.StackTrace);
        //        throw ex;
        //    }
        //}
        public static string gf_CorrectString(object data)
        {
            if (data != null)
            {
                return data.ToString();
            }
            else
            {
                return string.Empty;
            }
        }


    }

}