﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Threading;
using System.Configuration;
namespace APIMOBITCTINCTU
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();

            //System.Web.Http.GlobalConfiguration.Configure(WebApiConfig.Register);

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Thread t = new Thread(ClearCache);
            t.IsBackground = true;
            t.Start();
        }
        private void ClearCache() {
            try
            {
                while (true) {
                    if (ConfigurationManager.AppSettings["MOBITCT.ISCLEAR"].ToString().Equals("0"))
                        return;
                    MobiTCTProcess.ClearCTU();
                    Thread.Sleep(10000);
                }
            }
            catch (Exception ex) { 
            
            }
        }
    }
}