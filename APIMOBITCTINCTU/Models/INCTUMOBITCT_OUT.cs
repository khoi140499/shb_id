﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIMOBITCTINCTU.Models
{
    public class INCTUMOBITCT_OUT
    {
        public string MaLoi { get; set; }
        public string MoTaLoi { get; set; }
        public string URLBienLai { get; set; }
    }
}