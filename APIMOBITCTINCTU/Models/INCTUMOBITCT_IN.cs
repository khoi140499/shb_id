﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIMOBITCTINCTU.Models
{
    public class INCTUMOBITCT_IN
    {
        public string MaDoiTac { get; set; }
        public string MaThamChieu { get; set; }
        public string ThoiGianGD { get; set; }
        public string MaXacThuc { get; set; }
    }
}