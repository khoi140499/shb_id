﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIMOBITCTINCTU
{
    public class objInCTU
    {
        public objInCTU()
        { }
        public objHDRInCTU HDR { get; set; }

    }
    public class objHDRInCTU
    {
        public objHDRInCTU()
        { }
        public string TienPhi { get; set; }
        public string TienVAT { get; set; }
        public string KHCT { get; set; }
        public string SoCT { get; set; }
        public string TK_KN_NH { get; set; }
        public string Ten_NNThue { get; set; }
        public string Ma_NNThue { get; set; }
        public string DC_NNThue { get; set; }
        public string Huyen_NNThue { get; set; }
        public string Tinh_NNThue { get; set; }
        public string Ten_NNTien { get; set; }
        public string Ma_NNTien { get; set; }
        public string DC_NNTien { get; set; }
        public string Huyen_NNTien { get; set; }
        public string Tinh_NNTien { get; set; }
        public string NHA { get; set; }
        public string NHB { get; set; }
        public string Ten_KB { get; set; }
        public string Tinh_KB { get; set; }
        public string Ma_CQThu { get; set; }
        public string Ten_CQThu { get; set; }
        public string So_TK { get; set; }
        public string Ngay_TK { get; set; }
        public string Tien_Bang_Chu { get; set; }
        public string SO_FT { get; set; }
        public string Ten_KeToan { get; set; }
        public string Key_MauCT { get; set; }
        public string PT_TT { get; set; }
        public string ma_CN { get; set; }
        public string ten_cn { get; set; }
        public string Ngay_GD { get; set; }
        public string TienTKNO { get; set; }
        public string TienTKNH { get; set; }
        public string TKNo { get; set; }
        public string Ma_NT { get; set; }
        public string LoaiTien { get; set; }
        public string SO_FCC { get; set; }
        public string TEN_GDV { get; set; }
        public string IsTamThu { get; set; }
        public string Ngay_NH_KH { get; set; }
        public string Ngay_noptien { get; set; }
        public string TKCo { get; set; }
        public List<objDTLInCTU> CTU { get; set; }

    }
    public class objDTLInCTU
    {
        public objDTLInCTU()
        { }
        public string STT { get; set; }
        public string SO_CT { get; set; }
        public string NOI_DUNG { get; set; }
        public string CC { get; set; }
        public string LK { get; set; }
        public string MTM { get; set; }
        public string KYTHUE { get; set; }
        public string SOTIEN { get; set; }
        public string SOTIEN_NT { get; set; }
        public string NGAY_TK { get; set; }
        public string MA_DBHC { get; set; }


    }
}