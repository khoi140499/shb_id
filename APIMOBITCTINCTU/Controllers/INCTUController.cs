﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using VBOracleLib;
using Business.Common;
using Business;
using System.IO;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;
using Newtonsoft.Json;

namespace APIMOBITCTINCTU.Controllers
{
    public class INCTUController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ReportDocument rptTCS=null;
        private CrystalDecisions.Shared.ParameterDiscreteValue pdvTCS=null;
        private CrystalDecisions.Shared.ParameterValues pcTCS=null;
        [HttpGet]
       // public HttpResponseMessage Get(object token)
        public string Get(string token)
        {
            try
            {
                log.Info("0. INCTUController token: " + token);
                //log.Info("0. INCTUController token: " + token);
                string strToken = token;
                if (!MobiTCTProcess.validToken(strToken))
                    return null;
                log.Info("1. INCTUController Valid Token OK");
                //phan tich lay so tham chieu
                string strTem = VBOracleLib.Security.Decrypt3Des(strToken);
                string strMaThamChieu = strTem.Split('|')[0];
                string mv_SoCT = "";
                string strMa_Lthue = "";
                
                InGNT.InGNTService svInGNT = new InGNT.InGNTService();
                svInGNT.Url = ConfigurationManager.AppSettings["MOBITCT.URL_INGNT_SERVICE"];
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                string strObjIn = svInGNT.WSProcess(strMaThamChieu);
                log.Info("2.INCTUController -  strObjIn "+strObjIn);
                if (strObjIn.Trim().Length == 0)
                {
                    log.Info("Khong tim thay, hoac trang thai chung tu [" + strMaThamChieu + "] khong dung ");
                    return null;
                }
                objInCTU vobjInCTU = new objInCTU();
                vobjInCTU.HDR = new objHDRInCTU();
                vobjInCTU.HDR.CTU = new List<objDTLInCTU>();

                vobjInCTU = JsonConvert.DeserializeObject<objInCTU>(strObjIn);
                log.Info("3. INCTUController convert obj OK");
                //add data to table CTU
                DataTable dt = new DataTable();
                dt.Columns.Add("STT");
                dt.Columns.Add("SO_CT");
                dt.Columns.Add("NOI_DUNG");
                dt.Columns.Add("CC");
                dt.Columns.Add("LK");
                dt.Columns.Add("MTM");
                dt.Columns.Add("KYTHUE");
                dt.Columns.Add("SOTIEN",typeof(decimal));
                dt.Columns.Add("SOTIEN_NT",typeof(decimal));
                dt.Columns.Add("NGAY_TK");
                dt.Columns.Add("MA_DBHC");
                DataRow dr = null;
                log.Info("4. INCTUController DataTable Add Colum");
                for (int i = 0; i <= vobjInCTU.HDR.CTU.Count - 1; i++)
                {
                    dr = dt.NewRow(); // have new row on each iteration
                    dr["STT"] = vobjInCTU.HDR.CTU[i].STT;
                    dr["SO_CT"] = vobjInCTU.HDR.CTU[i].SO_CT;
                    dr["NOI_DUNG"] = vobjInCTU.HDR.CTU[i].NOI_DUNG;
                    dr["CC"] = vobjInCTU.HDR.CTU[i].CC;
                    dr["LK"] = vobjInCTU.HDR.CTU[i].LK;
                    dr["MTM"] = vobjInCTU.HDR.CTU[i].MTM;
                    dr["KYTHUE"] = vobjInCTU.HDR.CTU[i].KYTHUE;
                    dr["SOTIEN"] = vobjInCTU.HDR.CTU[i].SOTIEN;
                    dr["SOTIEN_NT"] = vobjInCTU.HDR.CTU[i].SOTIEN_NT;
                    dr["NGAY_TK"] = vobjInCTU.HDR.CTU[i].NGAY_TK;
                    dr["MA_DBHC"] = vobjInCTU.HDR.CTU[i].MA_DBHC;
                    dt.Rows.Add(dr);
                }
                log.Info("5. INCTUController DataTable Add Data");
                DataSet ds = new DataSet();
                dt.TableName = "CTU";
                ds.Tables.Add(dt);

                //end add data to table
                log.Info("6. INCTUController end add data to table");
                string strFileName = "";
                string v_strReportName = "In chứng từ thu ngân sách";
                if (vobjInCTU.HDR.Ma_NT.Equals("VND"))
                    strFileName = System.Web.HttpContext.Current.Server.MapPath(@"..\RPT\C102TCTNEW.rpt");
                else
                    strFileName = System.Web.HttpContext.Current.Server.MapPath(@"..\RPT\TCS_CTU_NguyenTe.rpt");
                log.Info("6. INCTUController - strFileName:" + strFileName);
                if (!File.Exists(strFileName))
                    return null;
                log.Info("7. INCTUController - load file OK");
                rptTCS = new ReportDocument();
                pdvTCS = new ParameterDiscreteValue();
                pcTCS = new ParameterValues();
                rptTCS.Load(strFileName);
                ds.Tables[0].TableName = "CTU";
                rptTCS.SetDataSource(ds);

                decimal vSumTTien_NT = 0;
                decimal vSumTTIEN = 0;
                foreach(DataRow vrow   in ds.Tables[0].Rows)
                {
                    vSumTTien_NT += decimal.Parse(vrow["SOTIEN_NT"].ToString());
                    vSumTTIEN += decimal.Parse(vrow["SOTIEN"].ToString());
                }
                string ma_nnthue = vobjInCTU.HDR.Ma_NNThue;
                string ma_nntien = vobjInCTU.HDR.Ma_NNTien;
                //luon la ban sao
                string mCopy = "Bản Sao";
                AddParam("TienPhi", vobjInCTU.HDR.TienPhi);
                AddParam("TienVAT", vobjInCTU.HDR.TienVAT);
                AddParam("KHCT", vobjInCTU.HDR.KHCT);
                AddParam("SoCT", vobjInCTU.HDR.SoCT);
                AddParam("TK_KN_NH", gf_CorrectString(vobjInCTU.HDR.TK_KN_NH));
                AddParam("Ten_NNThue", vobjInCTU.HDR.Ten_NNThue);
                AddParam("Ma_NNThue", vobjInCTU.HDR.Ma_NNThue);
                AddParam("DC_NNThue", vobjInCTU.HDR.DC_NNThue);
                AddParam("Huyen_NNThue", vobjInCTU.HDR.Huyen_NNThue);
                AddParam("Tinh_NNThue", vobjInCTU.HDR.Tinh_NNThue);
                if (ma_nnthue.Equals(ma_nntien))
                {
                    AddParam("Ten_NNTien", "");
                    AddParam("Ma_NNTien", "");
                    AddParam("DC_NNTien", "");
                    AddParam("Huyen_NNTien","");
                    AddParam("Tinh_NNTien", "");
                }
                else
                {
                    AddParam("Ten_NNTien", vobjInCTU.HDR.Ten_NNTien);
                    AddParam("Ma_NNTien", vobjInCTU.HDR.Ma_NNTien);
                    AddParam("DC_NNTien", vobjInCTU.HDR.DC_NNTien);
                    AddParam("Huyen_NNTien", gf_CorrectString(""));
                    AddParam("Tinh_NNTien", gf_CorrectString(""));
                }
              

                AddParam("NHA", vobjInCTU.HDR.NHA);
                AddParam("NHB", vobjInCTU.HDR.NHB);

                AddParam("Ten_KB", vobjInCTU.HDR.Ten_KB);
                AddParam("Tinh_KB", vobjInCTU.HDR.Tinh_KB);
                AddParam("Ma_CQThu", vobjInCTU.HDR.Ma_CQThu);

                AddParam("Ten_CQThu", vobjInCTU.HDR.Ten_CQThu);

                AddParam("So_TK", vobjInCTU.HDR.So_TK);
                AddParam("Ngay_TK", "");
                AddParam("Tien_Bang_Chu", vobjInCTU.HDR.Tien_Bang_Chu);

                AddParam("SO_FT", vobjInCTU.HDR.SO_FT);
                AddParam("Key_MauCT", vobjInCTU.HDR.Key_MauCT);
                AddParam("Ten_KeToan", "");

                AddParam("PT_TT", vobjInCTU.HDR.PT_TT);
                AddParam("Ma_CN", vobjInCTU.HDR.ma_CN);
                AddParam("ten_cn", vobjInCTU.HDR.ten_cn);
                AddParam("ngay_GD", vobjInCTU.HDR.Ngay_GD);

                AddParam("tienTKNo", vobjInCTU.HDR.TienTKNO.Replace('.',','));
                AddParam("TienTKNH", vobjInCTU.HDR.TienTKNH.Replace('.', ','));

                AddParam("TKNo", vobjInCTU.HDR.TKNo);
                AddParam("Ma_NT", vobjInCTU.HDR.Ma_NT);
                AddParam("LoaiTien", vobjInCTU.HDR.LoaiTien);
                AddParam("SO_FCC", vobjInCTU.HDR.SO_FCC);
                AddParam("TEN_GDV", "");
                AddParam("IsTamThu", vobjInCTU.HDR.IsTamThu);
                AddParam("Ngay_NH_KH", vobjInCTU.HDR.Ngay_NH_KH);

                AddParam("Ngay_noptien", vobjInCTU.HDR.Ngay_noptien);
                //AddParam("TKCo", vobjInCTU.HDR.TKCo);
                //AddParam("SumTTIEN_NT", vSumTTien_NT.ToString());
                //AddParam("SumTTIEN", vSumTTIEN.ToString());
                //AddParam("Tinh_NNThue", vobjInCTU.HDR.Tinh_NNThue);
                log.Info("7. INCTUController - load pram OK");
                rptTCS.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
               // HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "value");D:\Source\Exim_mobile\APIMOBITCTINCTU\Controllers\INCTUController.cs
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                //response.Headers.ty
                rptTCS.ExportToHttpResponse(ExportFormatType.PortableDocFormat, response, false, "Giaynoptien");
                log.Info("8. INCTUController - OK");
                return "";
            }
            catch (Exception ex)
            {
                log.Info("error. INCTUController - error : "+ex.StackTrace);
                log.Error(ex.Message + "-" + ex.StackTrace);
            }finally
            {
                if (rptTCS!=null)
                    rptTCS.Close();
                if(rptTCS!=null)
                rptTCS.Dispose();
            }
            return null;
        }
        private string gf_CorrectString(object data)
        {
            if (data != null)
            {
                return data.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        private void AddParam(string strName, string strValue)
        {
            try
            {
                pdvTCS.Value = strValue;
                pcTCS.Add(pdvTCS);
                rptTCS.DataDefinition.ParameterFields[strName].ApplyCurrentValues(pcTCS);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void AddParamNumber(string strName, string strValue)
        {
            try
            {
                pdvTCS.Value = strValue;
                pcTCS.Add(pdvTCS);
                rptTCS.DataDefinition.ParameterFields[strName].ApplyCurrentValues(pcTCS);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
