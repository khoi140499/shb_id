﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using APIMOBITCTINCTU.Models;
using Newtonsoft.Json;
using System.Configuration;

namespace APIMOBITCTINCTU.Controllers
{

    public class APIINCTUController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        public object Get(object pIn)
        {
            log.Info(pIn.ToString());
            INCTUMOBITCT_OUT result = new INCTUMOBITCT_OUT();
            result.MaLoi = "";
            string strLOGID = "";
            string strMST = "";
            log.Info("1.APIINCTUController - in ctu");
            InGNT.InGNTService svInGNT = new InGNT.InGNTService();
            svInGNT.Url = ConfigurationManager.AppSettings["MOBITCT.URL_INGNT_SERVICE"];
            try
            {
                //kiem tra ky so neu sai thi return ""
                //if (!SignatureDigital.ValidSign(pIn.ToString()))
                //    return "";
                log.Info("1.APIINCTUController - in ctu, pIn " + pIn.ToString());
                //   TCTObjectIn vobjTCT = JsonConvert.DeserializeObject<TCTObjectIn>(pIn.ToString());

                // INCTUMOBITCT_IN vIn = JsonConvert.DeserializeObject<INCTUMOBITCT_IN>(SignatureDigital.Base64Decode(vobjTCT.duLieu));
                INCTUMOBITCT_IN vIn = JsonConvert.DeserializeObject<INCTUMOBITCT_IN>(pIn.ToString());// JsonConvert.DeserializeObject<INCTUMOBITCT_IN>(SignatureDigital.Base64Decode(vobjTCT.duLieu));
                if (!SignatureDigital.ValidMaBiMatTCT(vIn.MaDoiTac, vIn.MaThamChieu, vIn.ThoiGianGD, vIn.MaXacThuc))
                    return "";

                //gọi service lấy dữ liệu:
                try
                {
                    log.Info("2.APIINCTUController - in ctu");
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    strLOGID = svInGNT.GetID(""); //MobiTCTProcess.getLogID();
                    //svInGNT.InsertLog("");
                    svInGNT.InsertLog(strLOGID, pIn.ToString(), "INCTU");
                    log.Info("3.APIINCTUController - in ctu");
                    //MobiTCTProcess.InsertLog(strLOGID, pIn.ToString(), "INCTU");
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message + "-" + ex.StackTrace);
                }

                //neu tim thay thong tin giao dich dang cho xu ly
                if (!vIn.MaDoiTac.Equals(ConfigurationManager.AppSettings["MA_NH"]))
                {
                    result.MaLoi = "09";
                    result.MoTaLoi = "Mã đối tác không tồn tại";
                    log.Info("3.1. APIINCTUController - result: " + Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    return Newtonsoft.Json.JsonConvert.SerializeObject(result);
                }
                string strToken = "?token=" + MobiTCTProcess.AddCTUIn(vIn.MaThamChieu);
                strToken = ConfigurationManager.AppSettings["MOBITCT.URL_IN"] + strToken;
                log.Info("3.2. APIINCTUController - in ctu - strToken " + strToken);
                result.MaLoi = "00";
                result.MoTaLoi = "Thanh cong";
                result.URLBienLai = strToken;

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
                result.MaLoi = "06";
                result.MoTaLoi = "Lỗi hệ thống";
            }
            //var   options = new JsonSerializerOptions
            //{
            //    Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
            //    WriteIndented = true
            //};
            string strResult = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            //gọi service lấy dữ liệu:
            try
            {

                log.Info("4. APIINCTUController - in ctu - strResult " + strResult);
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                svInGNT.UpdateLog(strLOGID, strResult, result.MaLoi, result.MoTaLoi, strMST);
                //MobiTCTProcess.UpdateLog(strLOGID, strResult, result.MaLoi, result.MoTaLoi, strMST);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }

            return result;
        }
    }
}
