﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Security.Cryptography.Xml;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Configuration;
using System.IO;
using log4net;
namespace APIMOBITCTINCTU
{
    public class SignatureDigital
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool ValidMaBiMatTCT(string strMaDoiTac, string strMaThamChieu, string strThoiGianGD, string MaXacThuc)
        {
            try
            {
                string strMaBiMat1 = ConfigurationManager.AppSettings["MOBITCT.PASSWORD"];
                string strMaBiMatKey = ConfigurationManager.AppSettings["MOBITCT.KEY"];
                string strMaBiMat = VBOracleLib.Security.DescryptStr(strMaBiMat1, strMaBiMatKey, 1);
                if (strMaBiMat.Length <= 0)
                    return false;
                //ghep chuoi bi mat de ma hoa
                string strTem = strMaDoiTac + "|" + strMaThamChieu + "|" + strThoiGianGD + "|" + strMaBiMat;
                string strTemBase = VBOracleLib.Security.Base64EncodeSHA256(strTem);
                if (strTemBase.ToUpper().Equals(MaXacThuc.ToUpper()))
                    return true;
            }
            catch (Exception ex) { }

            return false;
        }
        public static X509Certificate2 loadCertPathFromBase64String(String aCertChainBase64Encoded)
        {
            byte[] certChainEncoded = System.Convert.FromBase64String(aCertChainBase64Encoded);
            X509Certificate2 xCert01 = new X509Certificate2();
            xCert01.Import(certChainEncoded);
            return xCert01;
        }
        static public X509Certificate2 GetCertTCT()
        {
            X509Certificate2 certificate = new X509Certificate2(ConfigurationManager.AppSettings["CertPathTCT"]);

            return certificate;
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static bool ValidSign(string content)
        {
            string on_off = ConfigurationManager.AppSettings["MOBITCT.Check_CKS"];
            log.Info("1.ValidSign_IN - on_off:" + on_off);
            if (on_off.Equals("1"))
            {
                try
                {

                    bool valid = false;
                    string rs = "";

                    InGNT.InGNTService svInGNT = new InGNT.InGNTService();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    rs = svInGNT.VerifyCKS(content);

                    if (rs.Equals("true"))
                    {
                        valid = true;
                    }
                    else
                    {
                        log.Info("Error source: DigitalSignature | ValidSign_IN()\\n" + "Error code: System error!" + "\\n" + "Error message: " + rs);
                        valid = false;
                    }
                    log.Info("3.getVerifyMobile - rs:" + rs);
                    //output = rs;
                    return valid;
                }
                catch (Exception ex)
                {
                    log.Info("Error source: " + ex.Source + " | ValidSign_IN()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message);
                    return false;
                }
            }
            else
            {
                return true;
            }

        }
    }
    public class TCTObjectIn
    {
        public string duLieu { get; set; }
        public Signature signature;
    }
    public class Signature
    {
        public string value { get; set; }
        public string certificates { get; set; }
    }
}