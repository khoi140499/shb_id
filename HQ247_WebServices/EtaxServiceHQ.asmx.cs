﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.IO;
using System.Configuration;
namespace HQ247_WebServices
{
    /// <summary>
    /// Summary description for EtaxServiceHQ
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
   
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class EtaxServiceHQ : System.Web.Services.WebService
    {

        
        [WebMethod]
        public string WSProcess(string msg)
        {
            string strPublicKey = "xxxxxxxxxxx";
            string path = Server.MapPath(".");
            string result = "";
            try
            {
                if (!Directory.Exists(path + "\\LOG_MSG\\" + DateTime.Now.ToString("yyyyMMdd")))
                {
                    Directory.CreateDirectory(path + "\\LOG_MSG\\" + DateTime.Now.ToString("yyyyMMdd"));
                }
                File.WriteAllText(path + "\\LOG_MSG\\" + DateTime.Now.ToString("yyyyMMdd") + "\\msg_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt", msg);

                result = CustomsServiceV3.ProcessMSG.ProcessMessage(msg, strPublicKey);
            }
            catch (Exception ex)
            {
               
            }
            return result;


        }
       
    }
}
