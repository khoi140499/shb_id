﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using System.Diagnostics;
namespace Signature
{
    class XMLSigner
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void XMLSigning(XmlDocument xmlDoc, X509Certificate2 cert, String xpathSign)
        {
            RSACryptoServiceProvider rsaprovider = new RSACryptoServiceProvider();
            try
            {
                //Tao xml sign va add PrivateKey cho Object SignedXml
                SignedXml signxml = new SignedXml(xmlDoc);
                signxml.SigningKey = cert.PrivateKey;

                //Thiet lap phuong thuc SerializationC14
                signxml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NWithCommentsTransformUrl; // Serialization ca comments.
                signxml.SignedInfo.SignatureMethod = SignedXml.XmlDsigRSASHA1Url;

                //Thiet lap referent
                Reference r = new Reference("");
                r.AddTransform(CreateXPathTransform(@xpathSign));
                signxml.AddReference(r);

                // Thuc hien signing
                signxml.ComputeSignature();

                // Tao va hiet lap thong tin cho the KeyInfo
                KeyInfo keyInfo = new KeyInfo();

                //Lay va dien thong tin ve cert 
                KeyInfoX509Data keyX509 = new KeyInfoX509Data(cert);
                keyX509.AddSubjectName(cert.SubjectName.Name);
                keyInfo.AddClause(keyX509);

                //Them ten signature
                KeyInfoName keyNme = new KeyInfoName();
                keyNme.Value = "etax-shb";
                keyInfo.AddClause(keyNme);

                //Them public key
                rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                RSAKeyValue rkv = new RSAKeyValue(rsaprovider);
                keyInfo.AddClause(rkv);

                //Gan thong tin keyinfo cho object xmlSigned
                signxml.KeyInfo = keyInfo;


                // Lay Node <Signature> tuw xmlSigned objec de gan vao xmlDocument
                XmlElement sig = signxml.GetXml();
                // xmlDoc.DocumentElement.AppendChild(sig);

                XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "SECURITY", "");
                xmlDoc.DocumentElement.AppendChild(node).AppendChild(sig);
               
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                throw ex;
            }
            finally
            {
                rsaprovider.PersistKeyInCsp = false;
                rsaprovider.Clear();
            }
        }
        private static XmlDsigXPathTransform CreateXPathTransform(string xpath)
        {
            // tao represents transform khi sign bang xpath
            XmlDocument doc = new XmlDocument();
            XmlElement xpathElem = doc.CreateElement("XPath");
            xpathElem.InnerText = xpath;

            XmlDsigXPathTransform xform = new XmlDsigXPathTransform();
            xform.LoadInnerXml(xpathElem.SelectNodes("."));

            return xform;
        }
        public static Boolean VerifyXml(XmlDocument Doc, RSA Key)
        {
            // Check arguments. 
            if (Doc == null)
                throw new ArgumentException("Doc");
            if (Key == null)
                throw new ArgumentException("Key");

            // Create a new SignedXml object and pass it 
            // the XML document class.
            SignedXml signedXml = new SignedXml(Doc);

            // Find the "Signature" node and create a new 
            // XmlNodeList object.
            XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");

            // Throw an exception if no signature was found. 
            if (nodeList.Count <= 0)
            {
                throw new CryptographicException("Verification failed: No Signature was found in the document.");
            }

            // This example only supports one signature for 
            // the entire XML document.  Throw an exception  
            // if more than one signature was found. 
            if (nodeList.Count >= 2)
            {
                throw new CryptographicException("Verification failed: More that one signature was found for the document.");
            }

            // Load the first <signature> node.  
            signedXml.LoadXml((XmlElement)nodeList[0]);

            // Check the signature and return the result. 
            return signedXml.CheckSignature(Key);
        }
        public void XMLSigCustom(ref XmlDocument xmlDoc, X509Certificate2 cert, String xpathSign)
        {
            try
            {
                //Tao xml sign va add PrivateKey cho Object SignedXml
                SignedXml signxml = new SignedXml(xmlDoc);
                signxml.SigningKey = cert.PrivateKey;

                //Thiet lap phuong thuc SerializationC14
                signxml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NWithCommentsTransformUrl; // Serialization ca comments.
                signxml.SignedInfo.SignatureMethod = SignedXml.XmlDsigRSASHA1Url;

                //Thiet lap referent

                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                Reference r = new Reference("");
                r.AddTransform(env);
                signxml.AddReference(r);

                // Thuc hien signing
                signxml.ComputeSignature();

                // Tao va hiet lap thong tin cho the KeyInfo
                KeyInfo keyInfo = new KeyInfo();

                //Lay va dien thong tin ve cert 
                KeyInfoX509Data keyX509 = new KeyInfoX509Data(cert);
                keyX509.AddIssuerSerial(cert.IssuerName.Name, cert.SerialNumber);
                keyInfo.AddClause(keyX509);

                ////Them public key
                //RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                //RSAKeyValue rkv = new RSAKeyValue(rsaprovider);
                //keyInfo.AddClause(rkv);

                //Gan thong tin keyinfo cho object xmlSigned
                signxml.KeyInfo = keyInfo;


                // Lay Node <Signature> tuw xmlSigned objec de gan vao xmlDocument
                XmlElement sig = signxml.GetXml();
                // xmlDoc.DocumentElement.AppendChild(sig);
                // XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "SECURITY", "");
                xmlDoc.DocumentElement.AppendChild(sig);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogDebug.WriteLog(ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                throw ex;
            }
        }
        public void XMLSigCustom312(ref XmlDocument xmlDoc, X509Certificate2 cert, String xpathSign)
        {
            try
            {
               
               
                //Tao xml sign va add PrivateKey cho Object SignedXml
                SignedXml signxml = new SignedXml(xmlDoc);
                signxml.SigningKey = cert.PrivateKey;

                //Thiet lap phuong thuc SerializationC14
                signxml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NWithCommentsTransformUrl; // Serialization ca comments.
                signxml.SignedInfo.SignatureMethod = SignedXml.XmlDsigRSASHA1Url;

                //Thiet lap referent

                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                Reference r = new Reference("");
                r.AddTransform(env);
                signxml.AddReference(r);

                // Thuc hien signing
                signxml.ComputeSignature();

                // Tao va hiet lap thong tin cho the KeyInfo
                KeyInfo keyInfo = new KeyInfo();

                //Lay va dien thong tin ve cert 
                KeyInfoX509Data keyX509 = new KeyInfoX509Data(cert);
                keyX509.AddIssuerSerial(cert.IssuerName.Name, cert.SerialNumber);
                keyInfo.AddClause(keyX509);

                ////Them public key
                //RSACryptoServiceProvider rsaprovider = (RSACryptoServiceProvider)cert.PublicKey.Key;
                //RSAKeyValue rkv = new RSAKeyValue(rsaprovider);
                //keyInfo.AddClause(rkv);

                //Gan thong tin keyinfo cho object xmlSigned
                signxml.KeyInfo = keyInfo;


                // Lay Node <Signature> tuw xmlSigned objec de gan vao xmlDocument
                XmlElement sig = signxml.GetXml();
              
              //  XmlNode node = xmlDoc.SelectSingleNode(XmlNodeType.Element, "DigitalSignatures", "");
               
                //xmlDoc.DocumentElement.AppendChild(node).AppendChild(sig);
                xmlDoc.SelectSingleNode("Customs/DigitalSignatures").AppendChild(sig);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//VBOracleLib.LogDebug.WriteLog(ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                throw ex;
            }
        }
    }
}
