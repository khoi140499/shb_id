﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Xml;
using System.Configuration;
using Signature;

namespace SignXMLConsole
{
    class MainTest
    {
        private static String strcertPath = @"C:\PGB_PFX\PGBank.pfx";
        private static String strpassword = "123456789";

        //static void Main(string[] args)
        //{
        //    ////System.Console.WriteLine("Thuc hien Sign to khai xml......");

        //    //////string str = "<DATA><HEADER><VERSION>1.0</VERSION><SENDER_CODE>NHTM</SENDER_CODE><SENDER_NAME>NHTM</SENDER_NAME><RECEIVER_CODE>GIP</RECEIVER_CODE><RECEIVER_NAME>GIP</RECEIVER_NAME><TRAN_CODE>00005</TRAN_CODE><MSG_ID>NHTM01</MSG_ID><MSG_REFID>949fc2eb-59bf-4509-aa07-6ef5c7c5b4e6</MSG_REFID><ID_LINK /><SEND_DATE>16-12-2014 09:53:48</SEND_DATE><ORIGINAL_CODE /><ORIGINAL_NAME /><ORIGINAL_DATE /><ERROR_CODE /><ERROR_DESC /><SPARE1 /><SPARE2 /><SPARE3 /></HEADER><BODY><ROW><TRUYVAN><MST>0100231226</MST></TRUYVAN><TYPE>00005</TYPE><NAME>Truy vấn thu nộp theo MST</NAME></ROW></BODY></DATA>";
        //    ////XmlDocument xmlDoc = new XmlDocument();
        //    ////xmlDoc.Load(@"E:/06007 NH_TTXL_Nghiepvu.xml");

        //    //////Khoi tao Object XMLSigner
        //    ////XMLSigner xmlSigner = new XMLSigner();

        //    //////Ky bang XPath1.0
        //    ////String xpathSign = "ancestor-or-self::TBAO_TKHOAN_NH";

        //    //////Lay cert X509Certificate2 object de co PrivateKey
        //    ////X509Certificate2 cert = getCertificate();

        //    //////goi ham ky
        //    ////xmlSigner.XMLSigning(xmlDoc, cert, xpathSign);
        //    ////StoreDocSigned(xmlDoc);
        //    Boolean rs = XMLVerify.VerifyXmlFile(@"E:/06007 NH_TTXL_Nghiepvu_sign.xml");
        //    //System.Console.In.ReadLine();
        //}

        private static void StoreDocSigned(XmlDocument xmlDoc)
        {
            XmlWriterSettings xwsSettings = new XmlWriterSettings();
            //xwsSettings.Indent = true;
            //xwsSettings.OmitXmlDeclaration = true;
            //xwsSettings.NewLineChars = String.Empty;
            //XmlWriter xwWriter = XmlWriter.Create(@"C:/Users/HaduyCuong/Documents/IHTKK/XML/XML/chung_tu_itas_sign.xml", xwsSettings);
            XmlWriter xwWriter = XmlWriter.Create(@"E:/dangkydaky.xml", xwsSettings);
            try
            {
                xmlDoc.WriteTo(xwWriter);
            }
            catch (Exception e)
            {
                //Khong transform duoc
                throw e;
            }
            finally
            {
                xwWriter.Flush();
                xwWriter.Close();
            }
        }
        static public X509Certificate2 getCertificate()
        {
            X509Certificate2 certificate = new X509Certificate2(strcertPath, strpassword);

            return certificate;
        }

    }
}
//package seatechit.epay.common.untils;
//[9:27:07 AM] dang doanh: public static string compress(string text)
//{
//    byte[] buffer = Encoding.UTF8.GetBytes(text);
//    MemoryStream ms = new MemoryStream();
//    using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
//    {
//        zip.Write(buffer, 0, buffer.Length);
//    }

//    ms.Position = 0;
//    MemoryStream outStream = new MemoryStream();

//    byte[] compressed = new byte[ms.Length];
//    ms.Read(compressed, 0, compressed.Length);

//    byte[] gzBuffer = new byte[compressed.Length + 4];
//    System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
//    System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
//    return Convert.ToBase64String(gzBuffer);
//}

//public static string decompress(string compressedText)
//{
//    byte[] gzBuffer = Convert.FromBase64String(compressedText);
//    using (MemoryStream ms = new MemoryStream())
//    {
//        int msgLength = BitConverter.ToInt32(gzBuffer, 0);
//        ms.Write(gzBuffer, 4, gzBuffer.Length - 4);

//        byte[] buffer = new byte[msgLength];

//        ms.Position = 0;
//        using (GZipStream zip = new GZipStream(ms, CompressionMode.Decompress))
//        {
//            zip.Read(buffer, 0, buffer.Length);
//        }

//        return Encoding.UTF8.GetString(buffer);
//    }
//}