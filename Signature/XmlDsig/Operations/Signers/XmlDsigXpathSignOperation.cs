﻿using System.Security.Cryptography.Xml;
using System.Xml;
using XadesNetLib.Common.Exceptions;

namespace XadesNetLib.XmlDsig.Operations.Signers
{
    internal class XmlDsigXpathSignOperation : XmlDsigSignOperation
    {
        protected override void CreateAndAddReferenceTo(SignedXml signedXml, XmlDocument document, string inputPath, string xpathToNodeToSign)
        {
            if (signedXml == null)
            {
                throw new InvalidParameterException("Signed Xml cannot be null");
            }

            if (xpathToNodeToSign == null) xpathToNodeToSign = "";
            var signatureReference = new Reference("");

            var xpathSign = "ancestor-or-self::"+xpathToNodeToSign;

            signatureReference.AddTransform(CreateXPathTransform(@xpathSign));
            signatureReference.AddTransform(new XmlDsigEnvelopedSignatureTransform());

            //signatureReference.AddTransform(new XmlDsigXPathTransform());
            signedXml.AddReference(signatureReference);
        }
        protected override XmlDocument BuildFinalSignedXmlDocument(XmlDocument inputXml, XmlElement signatureXml, string nodeStoreSignatureTag)
        {
            //var raiz = inputXml.DocumentElement;
            var raiz = inputXml.GetElementsByTagName(nodeStoreSignatureTag).Item(0);
            
            if (raiz != null) raiz.AppendChild(inputXml.ImportNode(signatureXml, true));
            return inputXml;
        }
        private static XmlDsigXPathTransform CreateXPathTransform(string xpath)
        {
            // tao represents transform khi sign bang xpath
            XmlDocument doc = new XmlDocument();
            XmlElement xpathElem = doc.CreateElement("XPath");
            xpathElem.InnerText = xpath;

            XmlDsigXPathTransform xform = new XmlDsigXPathTransform();
            xform.LoadInnerXml(xpathElem.SelectNodes("."));

            return xform;
        }
    }
}