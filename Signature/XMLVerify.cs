﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Security.Cryptography.Xml;

namespace Signature
{
    class XMLVerify
    {
        public static Boolean VerifyXmlFile(String Name)
        {
            // Create a new XML document.
            XmlDocument xmlDocument = new XmlDocument();

            // Format using white spaces.
            xmlDocument.PreserveWhitespace = true;

            // Load the passed XML file into the document. 
            xmlDocument.Load(Name);

            // Create a new SignedXml object and pass it 
            // the XML document class.
            SignedXml signedXml = new SignedXml(xmlDocument);

            // Find the "Signature" node and create a new 
            // XmlNodeList object.
            //xmlDocument.Prefix = "ds";

            //XmlNamespaceManager xmlNPM = new XmlNamespaceManager(xmlDocument.NameTable);
            //xmlNPM.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");

            XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");
          //  XmlNodeList nodeList = xmlDocument.SelectNodes("//ancestor-or-self::ds:Signature", xmlNPM);

            // Load the signature node.
            signedXml.LoadXml((XmlElement)nodeList[0]);

            // Check the signature and return the result. 
            return signedXml.CheckSignature();

        }

        public static Boolean VerifyXmlString(string Name)
        {
            // Create a new XML document.
            XmlDocument xmlDocument = new XmlDocument();

            // Format using white spaces.
            xmlDocument.PreserveWhitespace = true;

            // Load the passed XML file into the document. 
            xmlDocument.LoadXml(Name);

            // Create a new SignedXml object and pass it 
            // the XML document class.
            SignedXml signedXml = new SignedXml(xmlDocument);

            // Find the "Signature" node and create a new 
            // XmlNodeList object.
            XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");

            // Load the signature node.
            signedXml.LoadXml((XmlElement)nodeList[0]);

            // Check the signature and return the result. 
            return signedXml.CheckSignature();

        }
    }
}
