﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using XadesNetLib.Utils.Certificates;
using XadesNetLib.XmlDsig;
using XadesNetLib.XmlDsig.Common;
using System.Xml;
using XadesNetLib.XmlDsig.Dsl;


namespace Signature
{
    class XMLSigXPath_TDT
    { 
    //    public void SigextendTimeStamp
    //    {
    //         string outputPath = "";
    //        var selectedCertificate = GetSelectedCertificate();
    //        var selectedFormat = GetSelectedSignatureFormat();
    //        var inputPath = txtFileToSign.Text;
    //        var xpathToNodeToSign = txtNodeToSign.Text ?? "";
    //        var NodeStoreSignatureTag = txtNodeStoreSignatureTag.Text ?? "";
    //        //if (!"".Equals(xpathToNodeToSign))
    //        //{
    //        //    if (!xpathToNodeToSign.StartsWith("#")) xpathToNodeToSign = "#" + xpathToNodeToSign;
    //        //}

    //        var howToSign =
    //            XmlDsigHelper.Sign(inputPath).Using(selectedCertificate).UsingFormat(selectedFormat)
    //                .IncludingCertificateInSignature().IncludeTimestamp(chkIncludeTimestamp.Checked)
    //                .NodeToSign(xpathToNodeToSign).NodeStoreSignatureTag(NodeStoreSignatureTag);
    //        howToSign.SignToFile(outputPath);
    //    }
        public static string SignXMLEextendTimeStamp(string strXML, string xpathToNodeToSign, string NodeStoreSignatureTag, bool setTimeStamp)
        {
            //  string str = "<DATA><HEADER><VERSION>1.0</VERSION><SENDER_CODE>NHTM</SENDER_CODE><SENDER_NAME>NHTM</SENDER_NAME><RECEIVER_CODE>GIP</RECEIVER_CODE><RECEIVER_NAME>GIP</RECEIVER_NAME><TRAN_CODE>00005</TRAN_CODE><MSG_ID>NHTM01</MSG_ID><MSG_REFID>949fc2eb-59bf-4509-aa07-6ef5c7c5b4e6</MSG_REFID><ID_LINK /><SEND_DATE>16-12-2014 09:53:48</SEND_DATE><ORIGINAL_CODE /><ORIGINAL_NAME /><ORIGINAL_DATE /><ERROR_CODE /><ERROR_DESC /><SPARE1 /><SPARE2 /><SPARE3 /></HEADER><BODY><ROW><TRUYVAN><MST>0100231226</MST></TRUYVAN><TYPE>00005</TYPE><NAME>Truy vấn thu nộp theo MST</NAME></ROW></BODY></DATA>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(strXML);

            //Khoi tao Object XMLSigner
            XMLSigner xmlSigner = new XMLSigner();

            //Ky bang XPath1.0
           // String xpathSign = "ancestor-or-self::HEADER or ancestor-or-self::BODY";
            X509Certificate2 cert =GetSignerCert();
            XmlDsigSignatureFormat selectedFormat = XmlDsigSignatureFormat.Xpath;
            SignDSL howToSign =
                 XmlDsigHelper.Sign(xmlDoc).Using(cert).UsingFormat(selectedFormat)
                 .IncludingCertificateInSignature().IncludeTimestamp(setTimeStamp).NodeToSign(xpathToNodeToSign)
                 .NodeStoreSignatureTag(NodeStoreSignatureTag);
            xmlDoc = howToSign.SignAndGetXml();
            //Signature.SignatureProcess.VerifyXmlFile(xmlDoc.InnerXml);
            return xmlDoc.InnerXml;
        }
        static void Main(string[] args)
        {
             XmlDocument xmlDoc = new XmlDocument();
             xmlDoc.Load(@"E:/xmlntdt/06007 NH_TTXL_Nghiepvu_sign.xml");

             string abc = SignXMLEextendTimeStamp(xmlDoc.InnerXml, "TBAO_TKHOAN_NH", "TBAO_TKHOAN_NH", true);
        }
        static public X509Certificate2 GetSignerCert()
        {
            X509Certificate2 certificate = new X509Certificate2(@"E:/testtct/testtct.p12", "12345678");

            return certificate;
        }
       
    }
}
