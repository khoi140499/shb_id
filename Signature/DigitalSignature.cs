﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using VBOracleLib;
using System.Diagnostics;
using System.Configuration;
using System.Data;

namespace Signature
{
    public class DigitalSignature
    {
        //vinvv
        private static String cert_name = ConfigurationManager.AppSettings.Get("CERT_PATH.SHA256").ToString();
        private static String cert_pw = ConfigurationManager.AppSettings.Get("PASSWORD.SHA256").ToString();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //vinhvv
        public static string SIGNATUREURL = GetThamSoHT("DIGITAL.SIGNATURE.URL");
        public static string GetThamSoHT(string ten_ts)
        {
            string giatri_ts = "";
            try
            {
                DataTable dtts = new DataTable();
                dtts = DataAccess.ExecuteReturnDataSet("select giatri_ts from tcs_thamso_ht where ten_ts='" + ten_ts + "'", CommandType.Text).Tables[0];
                if (dtts.Rows.Count > 0)
                {
                    giatri_ts = dtts.Rows[0]["giatri_ts"].ToString();
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return giatri_ts;
        }
        public static string getSign(string content, string cert_name, string cert_pw, string xpath_node)
        {
            try
            {
                string sinedContent = "";

                byte[] bufferCert_name = Encoding.UTF8.GetBytes(cert_name);
                string maskedCert_name = Convert.ToBase64String(bufferCert_name);

                byte[] bufferCert_pw = Encoding.UTF8.GetBytes(cert_pw);
                string maskedCert_pw = Convert.ToBase64String(bufferCert_pw);

                byte[] bufferXpath_node = Encoding.UTF8.GetBytes(xpath_node);
                string maskedXpath_node = Convert.ToBase64String(bufferXpath_node);

                sinedContent = MessageRequest(SIGNATUREURL + "/Sign/" + maskedCert_name + "/" + maskedCert_pw + "/" + maskedXpath_node, content);

                return sinedContent;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// l("Error source: " + ex.Source + " | getSign()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                return "";
            }
        }
        public static string getSign(string content, string xpath_node)
        {
            try
            {
                string sinedContent = "";

                byte[] bufferCert_name = Encoding.UTF8.GetBytes(cert_name);
                string maskedCert_name = Convert.ToBase64String(bufferCert_name);

                byte[] bufferCert_pw = Encoding.UTF8.GetBytes(cert_pw);
                string maskedCert_pw = Convert.ToBase64String(bufferCert_pw);

                byte[] bufferXpath_node = Encoding.UTF8.GetBytes(xpath_node);
                string maskedXpath_node = Convert.ToBase64String(bufferXpath_node);

                sinedContent = MessageRequest(SIGNATUREURL + "/Sign/" + maskedCert_name + "/" + maskedCert_pw + "/" + maskedXpath_node, content);

                return sinedContent;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//   LogDebug.WriteLog("Error source: " + ex.Source + " | getSign()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                return "";
            }
        }
        public static bool getVerify(string content, string serial_number1, string serial_number2)
        {
            try
            {
                bool valid = false;
                string rs = "";

                byte[] bufferSerial_number1 = Encoding.UTF8.GetBytes(serial_number1);
                string maskedSerial_number1 = Convert.ToBase64String(bufferSerial_number1);

                byte[] bufferSerial_number2 = Encoding.UTF8.GetBytes(serial_number2);
                string maskedSerial_number2 = Convert.ToBase64String(bufferSerial_number2);

                rs = MessageRequest(SIGNATUREURL + "/Verify/" + maskedSerial_number1 + "/" + maskedSerial_number2, content);

                if (rs.Equals("valid"))
                {
                    valid = true;
                }
                else
                {
                   log.Error("Error source: DigitalSignature | getVerify()\\n" + "Error code: System error!" + "\\n" + "Error message: " + rs);
                    valid = false;
                }

                return valid;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + " | getVerify()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                return false;
            }
        }

        public static string MessageRequest(string url, string content)
        {
            string strResponse = "";
            try
            {
                string strURL = url;
                byte[] dataByte = Encoding.UTF8.GetBytes(content);

                HttpWebRequest POSTRequest = (HttpWebRequest)WebRequest.Create(strURL);
                //Method type
                POSTRequest.Method = "POST";
                // Data type - message body coming in xml
                POSTRequest.ContentType = "text/xml";
                POSTRequest.KeepAlive = true;
                //POSTRequest.Timeout = WS_TIMEPOUT '5000
                //Content length of message body
                POSTRequest.ContentLength = dataByte.Length;

                // Get the request stream
                Stream POSTstream = POSTRequest.GetRequestStream();
                // Write the data bytes in the request stream
                POSTstream.Write(dataByte, 0, dataByte.Length);

                //Get response from server
                HttpWebResponse POSTResponse = (HttpWebResponse)POSTRequest.GetResponse();

                StreamReader reader = new StreamReader(POSTResponse.GetResponseStream(), Encoding.UTF8);
                strResponse = reader.ReadToEnd().ToString();


            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("Error source: " + ex.Source + " | MessageRequest()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
            }

            return strResponse;
        }
        public static string getSignNTDT(string content, string xpath_node, string xNode_To_sign)
        {
            try
            {
                string sinedContent = "";

                byte[] bufferCert_name = Encoding.UTF8.GetBytes(cert_name);
                string maskedCert_name = Convert.ToBase64String(bufferCert_name);

                byte[] bufferCert_pw = Encoding.UTF8.GetBytes(cert_pw);
                string maskedCert_pw = Convert.ToBase64String(bufferCert_pw);

                byte[] bufferXpath_node = Encoding.UTF8.GetBytes(xpath_node);
                string maskedXpath_node = Convert.ToBase64String(bufferXpath_node);

                byte[] bufferxNode_To_sign = Encoding.UTF8.GetBytes(xNode_To_sign);
                string maskedxNode_To_sign = Convert.ToBase64String(bufferxNode_To_sign);

                //LogDebug.WriteLog("SIGNATUREURL: " + SIGNATUREURL, EventLogEntryType.Error);
                //LogDebug.WriteLog("cert_name: " + cert_name, EventLogEntryType.Error);
                //LogDebug.WriteLog("cert_pw: " + cert_pw, EventLogEntryType.Error);

                sinedContent = MessageRequest(SIGNATUREURL + "/SignNTDT/" + maskedCert_name + "/" + maskedCert_pw + "/" + maskedXpath_node + "/" + maskedxNode_To_sign, content);

                return sinedContent;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + " | getSign()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                return "";
            }
        }
       

    }
}
