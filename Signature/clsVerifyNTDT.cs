﻿using System;
using System.Configuration;
using System.Data;
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml.Linq;
//using JNI;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;
using System.Text;
using System.Xml;
using VBOracleLib;

namespace Signature
{

    public class clsVerifyNTDT
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static String serialNTDT = ConfigurationManager.AppSettings.Get("NTDT.SERIAL").ToString();
        private static String dllJaVa = ConfigurationManager.AppSettings.Get("NTDT.JVDLL").ToString();
        private static String urlJava = ConfigurationManager.AppSettings.Get("NTDT.urlJava").ToString();
        //JavaNativeInterface Java;
        //  private List<string> jvmOptions;
        private readonly Dictionary<string, string> pair = new Dictionary<string, string>();
        string pathClass = dllJaVa;//@"C:\inetpub\wwwroot\veri_XML\bin";
        public bool VerifyXMLJava(string xmlString)
        {
            List<object> olParameters = new List<object>();

            olParameters.Clear();
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] fileBytes = Encoding.UTF8.GetBytes(xmlString);
            //    byte[] array ={0};
            //    string s = encoding.GetString(array);
            //FileStream stream = File.OpenRead(@"E:\xmlntdt\06006 TTXL-NH.xml");
            //fileBytes = new Byte[stream.Length];
            //stream.Read(fileBytes, 0, fileBytes.Length);
            //stream.Close();
           // olParameters.Add(fileBytes);
           // loadJVM();
            log.Error("Hàm mới Begin: loadJVM() \n" + "Error code: System error!" + "\n" + "Error message: ");
            //string msg = Java.CallMethod<string>("verifySignatureXMLJNI", "([B)Ljava/lang/String;", olParameters).ToString(CultureInfo.InvariantCulture);
            SVverifyXML.VerifyXmlWS clsJava = new Signature.SVverifyXML.VerifyXmlWS();
            clsJava.Url = urlJava;
            log.Error("Call End: loadJVM() \n" + "Error code: System error!" + "\n" + "Error message: ");
            string msg = clsJava.verifySignatureXML(fileBytes);
            bool result = false;
            if (!string.IsNullOrEmpty(msg))
            {
                //sonmt: x? lí chu?i xml ch?a thông tin tài kho?n corebank tr? v?.
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                StringReader strReader = new StringReader(msg);
                log.Error("Hàm mới Begin: loadJVM() \n" + "Error code: System error!" + msg.ToString() + "\n" + "Error message: ");
                ds.ReadXml(strReader);
                dt = ds.Tables["TRANG_THAI_VERIFY"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        if (dt.Rows[i]["SERIAL"].ToString() == serialNTDT && dt.Rows[i]["MA_TRANG_THAI"].ToString() == "1.1")
                        {
                            result = true;
                            break;
                        }

                    }
                }
                ds.Dispose();
            }
            else
            {
                result = false;
            }

            //Java.Dispose();

            return result;
        }

        public void loadJVM()
        {
            //Java = new JavaNativeInterface();
            //try
            //{

            //    //Java.AttachToCurrentJVMThread = true;

            //    // Set the path where the java class is located
            //    //var pathClass = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //    //var pathClass = C:\inetpub\wwwroot\veri_XML\bin";
            //     LogDebug.WriteLog(pathClass, System.Diagnostics.EventLogEntryType.Information);
            //    AddJVMOption("-Djava.class.path", pathClass);// +" -Xmx512m -verbose:gc"
            //    Java.LoadVM(pair, true);
            //     LogDebug.WriteLog("Load thành công", System.Diagnostics.EventLogEntryType.Information);
            //    Java.InstantiateJavaObject("JavaVerifySignXML");
            //}
            //catch (Exception ex)
            //{
            //    //Java.Dispose();
            //    //throw;

            //    LogDebug.WriteLog("Error source: loadJVM() " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);

            //}

        }
        public void AddJVMOption(string name, string value)
        {
            if (!pair.ContainsKey(name))
                pair.Add(name, value);

            //   jvmOptions.Add(name + "=" + value);
        }
    }
}
