﻿#region Using directives

using System;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;

#endregion
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Security.Cryptography.Xml;
using System.Security;
using VBOracleLib;
using System.Data;
using XadesNetLib.XmlDsig.Common;
using XadesNetLib.XmlDsig;
using XadesNetLib.XmlDsig.Dsl;
using System.IO;
using System.Diagnostics;

namespace Signature
{
    public class SignatureProcess
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static String SerialNumber = ConfigurationManager.AppSettings.Get("CUSTOMS.SERIAL");
        private static String strpassword = ConfigurationManager.AppSettings.Get("CUSTOMS.PASSWORD");
        private static String strProviderName = ConfigurationManager.AppSettings.Get("CUSTOMS.PROVIDERNAME");
        private static String strKeyContainerName = ConfigurationManager.AppSettings.Get("CUSTOMS.KEYCONTAINERNAME");
        private static String strcertTCT = ConfigurationManager.AppSettings.Get("GIP.CERT_PATH").ToString(); //GetThamSoHT("GIP.CERT_PATH");
        private static String strCertpass = ConfigurationManager.AppSettings.Get("CUSTOMS.PASSWORD").ToString();
        private static String strCertpath = ConfigurationManager.AppSettings.Get("CUSTOMS.CERT_PATH").ToString();
        private static int so_lan_goi_token=0;
        //static void Main(string[] args)
        //{
        //    VerifyTest("", "");

        //}
        static private string GetThamSoHT(string name)
        {
            string Result = "";
            try
            {
                string strSQL = "SELECT a.giatri_ts FROM tcs_thamso_ht a  where a.ten_ts ='" + name + "' ";
                DataTable dt = DataAccess.ExecuteToTable(strSQL);
                if (dt.Rows.Count > 0)
                {
                    Result = dt.Rows[0]["GIATRI_TS"].ToString();
                }
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }

        static public X509Certificate2 GetSignerCert()
        {
            X509Store store = new X509Store(StoreLocation.CurrentUser);
            try
            {
                //string KeyContainerName = SerialNumber;
              
                //store.Open(OpenFlags.ReadOnly);
                //X509Certificate2 cert = null;
                ////Sign from SmartCard
                ////note : ProviderName and KeyContainerName can be found with the dos command : CertUtil -ScInfo
                //string ProviderName = strProviderName;
                //string KeyContainerName2 = strKeyContainerName;
                //string PinCode = strpassword;
                //RSACryptoServiceProvider rsaCsp;
                //CspParameters csp;
                //foreach (X509Certificate2 cert2 in store.Certificates)
                //{
                //    if (cert2.SerialNumber.ToString() == KeyContainerName.ToUpper())
                //    {
                //        cert = cert2;
                //        break;
                //    }
                //}
                //so_lan_goi_token = so_lan_goi_token + 1;

                //if (PinCode != "" && so_lan_goi_token < 3)
                //{
                //    //if pin code is set then no windows form will popup to ask it
                //    SecureString pwd = GetSecurePin(PinCode);
                //    var privateKey = cert.PrivateKey as RSACryptoServiceProvider;
                //    csp = new CspParameters(privateKey.CspKeyContainerInfo.ProviderType, privateKey.CspKeyContainerInfo.ProviderName, privateKey.CspKeyContainerInfo.KeyContainerName
                //        , new System.Security.AccessControl.CryptoKeySecurity(), pwd);
                   
                //    try
                //    {
                //        rsaCsp = new RSACryptoServiceProvider(csp);
                        
                //        // the pin code will be cached for next access to the smart card
                //    }
                //    catch (Exception ex)
                //    {
                       
                //    }

                   

                //}
                //return cert;
                X509Certificate2 certificate = new X509Certificate2(strCertpath, strpassword);

                return certificate;

            }
            catch
            {
                throw (new Exception("Không tìm thấy chứng thư số"));
            }
            finally
            {
                //store.Close(); 
            }
        }
        private static SecureString GetSecurePin(string PinCode)
        {
            SecureString pwd = new SecureString();
            foreach (var c in PinCode.ToCharArray()) pwd.AppendChar(c);
            return pwd;
        }
        //static public X509Certificate2 GetSignerCert()
        //{
        //    X509Certificate2 certificate = new X509Certificate2(strcertPath, strpassword);

        //    return certificate;
        //}
        //static public X509Certificate2 GetRecipientCert()
        //{
        //    X509Certificate2 certificate = new X509Certificate2(strcertPath, strpassword);

        //    return certificate;
        //}


        static public byte[] SignMsg(
            Byte[] msg,
            X509Certificate2 signerCert)
        {
            ContentInfo contentInfo = new ContentInfo(msg);
            SignedCms signedCms = new SignedCms(contentInfo);
            CmsSigner cmsSigner = new CmsSigner(signerCert);

            //  Sign the PKCS #7 message.
            Console.Write("Computing signature with signer subject " +
                "name {0} ... ", signerCert.SubjectName.Name);
            signedCms.ComputeSignature(cmsSigner);
            Console.WriteLine("Done.");

            //  Encode the PKCS #7 message.
            return signedCms.Encode();
        }

        static public bool VerifyMsg(byte[] encodedSignedCms,
            out byte[] origMsg)
        {
            SignedCms signedCms = new SignedCms();

            signedCms.Decode(encodedSignedCms);
            try
            {
                Console.Write("Checking signature on message ... ");
                signedCms.CheckSignature(true);
                Console.WriteLine("Done.");
            }
            catch (System.Security.Cryptography.CryptographicException e)
            {
                Console.WriteLine("VerifyMsg caught exception:  {0}",
                    e.Message);
                Console.WriteLine("The message may have been modified " +
                    "in transit or storage. Authenticity of the " +
                    "message is not guaranteed.");
                origMsg = null;
                return false;
            }

            origMsg = signedCms.ContentInfo.Content;

            return true;
        }

        static public byte[] EncryptMsg(Byte[] msg,
            X509Certificate2 recipientCert)
        {
            ContentInfo contentInfo = new ContentInfo(msg);
            EnvelopedCms envelopedCms = new EnvelopedCms(contentInfo);

            CmsRecipient recip1 = new CmsRecipient(
                SubjectIdentifierType.IssuerAndSerialNumber,
                recipientCert);

            Console.Write("Encrypting data for a single recipient of " +
                "subject name {0} ... ",
                recip1.Certificate.SubjectName.Name);

            envelopedCms.Encrypt(recip1);
            Console.WriteLine("Done.");
            return envelopedCms.Encode();
        }

        //  Decrypt the encoded EnvelopedCms message.
        static public Byte[] DecryptMsg(byte[] encodedEnvelopedCms)
        {
            EnvelopedCms envelopedCms = new EnvelopedCms();
            envelopedCms.Decode(encodedEnvelopedCms);
            DisplayEnvelopedCms(envelopedCms, false);
            Console.Write("Decrypting Data ... ");
            envelopedCms.Decrypt(envelopedCms.RecipientInfos[0]);
            Console.WriteLine("Done.");

            return envelopedCms.Encode();
        }

        //  Display the ContentInfo property of a SignedCms object.
        private void DisplaySignedCmsContent(String desc,
            SignedCms signedCms)
        {
            Console.WriteLine(desc + " (length {0}):  ",
                signedCms.ContentInfo.Content.Length);
            foreach (byte b in signedCms.ContentInfo.Content)
            {
                Console.Write(b.ToString() + " ");
            }
            Console.WriteLine();
        }

        //  Display the ContentInfo property of an EnvelopedCms object.
        static private void DisplayEnvelopedCmsContent(String desc,
            EnvelopedCms envelopedCms)
        {
            Console.WriteLine(desc + " (length {0}):  ",
                envelopedCms.ContentInfo.Content.Length);
            foreach (byte b in envelopedCms.ContentInfo.Content)
            {
                Console.Write(b.ToString() + " ");
            }
            Console.WriteLine();
        }

        //  Display some properties of an EnvelopedCms object.
        static private void DisplayEnvelopedCms(EnvelopedCms e,
            Boolean displayContent)
        {
            Console.WriteLine("\nEnveloped PKCS #7 Message Information:");
            Console.WriteLine(
                "\tThe number of recipients for the Enveloped PKCS #7 " +
                "is:  {0}",
                e.RecipientInfos.Count);
            for (int i = 0; i < e.RecipientInfos.Count; i++)
            {
                Console.WriteLine(
                    "\tRecipient #{0} has type {1}.",
                    i + 1,
                    e.RecipientInfos[i].RecipientIdentifier.Type);
            }
            if (displayContent)
            {
                DisplayEnvelopedCmsContent("Enveloped PKCS #7 Content", e);
            }
            Console.WriteLine();
        }

        //struct
        public struct ResultSign
        {
            //SignCode = 0 => thanh cong, SignMessage la chu ky
            //SignCode # 0 => that bai, SignMessage la mo ta loi
            public int SignCode;
            public String SignMessage;
        }
        public static ResultSign TokenSign(String signContent, X509Certificate2 X509)
        {
            ResultSign resultSign;
            RSACryptoServiceProvider CSP = new RSACryptoServiceProvider();
            SHA1Managed sha1 = new SHA1Managed();
            Byte[] bInput, bHash, bRes;
            /*******************************************************************************************/
            resultSign.SignCode = 0; resultSign.SignMessage = "Success!";
            /*******************************************************************************************/
            if (!X509.HasPrivateKey)
            {
                resultSign.SignCode = 1; resultSign.SignMessage = "Certificate not valid!";
                return resultSign;
            }
            else
            {
                try
                {
                    CSP = (RSACryptoServiceProvider)X509.PrivateKey;
                    bInput = Encoding.UTF8.GetBytes(signContent);
                    bHash = sha1.ComputeHash(bInput);
                    bRes = CSP.SignHash(bHash, CryptoConfig.MapNameToOID("SHA1"));

                    resultSign.SignMessage = Convert.ToBase64String(bRes);
                    return resultSign;
                }
                catch (Exception ex)
                {
                   
                    resultSign.SignCode = 2;
                    resultSign.SignMessage = "Signing Error!" + "\n\r" + ex.Message;
                    return resultSign;
                }
                finally
                {
                    //CSP.PersistKeyInCsp = false;
                    //CSP.Clear();
                }
            }

        }

        static bool TokenVerify(string signContent, String signature, X509Certificate2 X509)
        {
            // Get its associated CSP and public key
            RSACryptoServiceProvider csp = (RSACryptoServiceProvider)X509.PublicKey.Key;

            // Hash the data
            SHA1Managed sha1 = new SHA1Managed();
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] bsign = Convert.FromBase64String(signature);
            byte[] data = Encoding.UTF8.GetBytes(signContent);
            byte[] hash = sha1.ComputeHash(data);

            // Verify the signature with the hash
            return csp.VerifyHash(hash, CryptoConfig.MapNameToOID("SHA1"), bsign);
        }

        static public ResultSign Sign(String signContent)
        {
            X509Certificate2 signerCert = GetSignerCert(strCertpath, strCertpass);
            ResultSign resultSign = TokenSign(signContent, signerCert);
            return resultSign;
        }
        static public string getPublicKey()
        {
            X509Certificate2 cert = GetSignerCert();
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(Convert.ToBase64String(cert.Export(X509ContentType.Cert), Base64FormattingOptions.InsertLineBreaks));
            return builder.ToString();
        }
        static public bool Verify(String signContent, string signature)
        {
            X509Certificate2 signerCert; ;

            //store.Open(OpenFlags.ReadOnly);
            //X509Certificate2Collection collection = store.Certificates.Find(X509FindType.FindBySerialNumber, cert.SerialNumber, true);
            //signerCert = collection[0];
            //bool resultVerify = TokenVerify(signContent, signature, signerCert);

            //return resultVerify;
            return true;
        }
        static public bool Verify(String signContent)
        {
            X509Certificate2 signerCert = GetSignerCert();
            bool resultVerify = TokenVerify(signContent, signerCert);

            return resultVerify;
        }
        static bool TokenVerify(string signContent, X509Certificate2 X509)
        {
            // Get its associated CSP and public key
            RSACryptoServiceProvider csp = (RSACryptoServiceProvider)X509.PublicKey.Key;

            // Hash the data
            XmlDocument xmlDocument = new XmlDocument();
            // Load the passed XML file into the document. 
            xmlDocument.LoadXml(signContent);
            //SignedXml signedXml = new SignedXml(xmlDocument);
            XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");
            string signature = nodeList.Item(0).InnerText;
            // Load the signature node.
            // signedXml.LoadXml((XmlElement)nodeList[0]);

            SHA1Managed sha1 = new SHA1Managed();
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] bsign = Convert.FromBase64String(signature);
            byte[] data = Encoding.UTF8.GetBytes(signContent);
            byte[] hash = sha1.ComputeHash(data);

            // Verify the signature with the hash
            return csp.VerifyHash(hash, CryptoConfig.MapNameToOID("SHA1"), bsign);
        }
        static public bool VerifyTest(string text, string signature)
        {
            X509Certificate2 cert = new X509Certificate2("c:/temp/MSB02.cer");
            RSACryptoServiceProvider csp = (RSACryptoServiceProvider)cert.PublicKey.Key;
            // Hash the data         
            SHA1Managed sha1 = new SHA1Managed();
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] data = encoding.GetBytes(text);
            byte[] sign = Convert.FromBase64String(signature);
            byte[] hash = sha1.ComputeHash(data);
            bool b = csp.VerifyHash(hash, CryptoConfig.MapNameToOID("SHA1"), sign);
            return b;
        }
        public static string SignXML(string strXML)
        {
            //  string str = "<DATA><HEADER><VERSION>1.0</VERSION><SENDER_CODE>NHTM</SENDER_CODE><SENDER_NAME>NHTM</SENDER_NAME><RECEIVER_CODE>GIP</RECEIVER_CODE><RECEIVER_NAME>GIP</RECEIVER_NAME><TRAN_CODE>00005</TRAN_CODE><MSG_ID>NHTM01</MSG_ID><MSG_REFID>949fc2eb-59bf-4509-aa07-6ef5c7c5b4e6</MSG_REFID><ID_LINK /><SEND_DATE>16-12-2014 09:53:48</SEND_DATE><ORIGINAL_CODE /><ORIGINAL_NAME /><ORIGINAL_DATE /><ERROR_CODE /><ERROR_DESC /><SPARE1 /><SPARE2 /><SPARE3 /></HEADER><BODY><ROW><TRUYVAN><MST>0100231226</MST></TRUYVAN><TYPE>00005</TYPE><NAME>Truy vấn thu nộp theo MST</NAME></ROW></BODY></DATA>";
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(strXML);

                //Khoi tao Object XMLSigner
                XMLSigner xmlSigner = new XMLSigner();

                //Ky bang XPath1.0
                String xpathSign = "ancestor-or-self::HEADER or ancestor-or-self::BODY";
                X509Certificate2 cert = GetSignerCert(strCertpath, strCertpass);
                //goi ham ky
                if (cert == null)
                {
                    throw (new Exception("Không tìm thấy chứng thư số"));
                }
                xmlSigner.XMLSigning(xmlDoc, cert, xpathSign);
                return xmlDoc.InnerXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Boolean VerifyXmlFile(string data)
        {
            // Create a new XML document.
            XmlDocument xmlDocument = new XmlDocument();

            // Format using white spaces.
            xmlDocument.PreserveWhitespace = true;

            // Load the passed XML file into the document. 
            xmlDocument.LoadXml(data);

            // Create a new SignedXml object and pass it 
            // the XML document class.
            SignedXml signedXml = new SignedXml(xmlDocument);

            XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");
            // Load the signature node.
            signedXml.LoadXml((XmlElement)nodeList[0]);
            // get the cert from the signature
            X509Certificate2 certificate = null;
            foreach (KeyInfoClause clause in signedXml.KeyInfo)
            {
                if (clause is KeyInfoX509Data)
                {
                    if (((KeyInfoX509Data)clause).Certificates.Count > 0)
                    {
                        certificate = (X509Certificate2)((KeyInfoX509Data)clause).Certificates[0];
                    }
                }
            }
            //lấy serial để verify
            string a = certificate.SerialNumber;
            //KeyInfo keyinfo = new KeyInfo();
            //keyinfo.
            X509Certificate2 gipcer = GetCertTCT();
            // string serialnumer = gipcer.SubjectName.;

            //CspParameters cspParams = new CspParameters(1,);
            // cspParams.ke
            //cspParams. = serialnumer;

            // Create a new RSA signing key and save it in the container. 
            // RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);
            //X509Certificate2 cert = x509Data.Certificates[0] as X509Certificate2;
            ////verify chữ ký
            //cert.SerialNumber = "";
            //X509Store store = new X509Store(StoreName.TrustedPublisher, StoreLocation.LocalMachine);
            //store.Open(OpenFlags.ReadOnly);
            //X509Certificate2Collection collection = store.Certificates.Find(X509FindType.FindBySerialNumber, cert.SerialNumber, true);
            // Check the signature and return the result. 
            // Check the signature and return the result. 
            return signedXml.CheckSignature(gipcer, true);

        }
        static public X509Certificate2 GetCertTCT()
        {
            X509Certificate2 certificate = new X509Certificate2(strcertTCT);

            return certificate;
        }
        static public X509Certificate2 GetSignerCert(string pathcert, string password)
        {
            try
            {
                X509Certificate2 certificate = new X509Certificate2(pathcert, password);
                return certificate;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog(ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                throw ex;
            }
        }
        public static string SignXMLEextendTimeStamp(string strXML, string xpathToNodeToSign, string NodeStoreSignatureTag, bool setTimeStamp)
        {
            //  string str = "<DATA><HEADER><VERSION>1.0</VERSION><SENDER_CODE>NHTM</SENDER_CODE><SENDER_NAME>NHTM</SENDER_NAME><RECEIVER_CODE>GIP</RECEIVER_CODE><RECEIVER_NAME>GIP</RECEIVER_NAME><TRAN_CODE>00005</TRAN_CODE><MSG_ID>NHTM01</MSG_ID><MSG_REFID>949fc2eb-59bf-4509-aa07-6ef5c7c5b4e6</MSG_REFID><ID_LINK /><SEND_DATE>16-12-2014 09:53:48</SEND_DATE><ORIGINAL_CODE /><ORIGINAL_NAME /><ORIGINAL_DATE /><ERROR_CODE /><ERROR_DESC /><SPARE1 /><SPARE2 /><SPARE3 /></HEADER><BODY><ROW><TRUYVAN><MST>0100231226</MST></TRUYVAN><TYPE>00005</TYPE><NAME>Truy vấn thu nộp theo MST</NAME></ROW></BODY></DATA>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(strXML);

            //Khoi tao Object XMLSigner
            XMLSigner xmlSigner = new XMLSigner();

            //Ky bang XPath1.0
            // String xpathSign = "ancestor-or-self::HEADER or ancestor-or-self::BODY";
            X509Certificate2 cert = GetSignerCert(strCertpath, strCertpass);
            XmlDsigSignatureFormat selectedFormat = XmlDsigSignatureFormat.Xpath;
            SignDSL howToSign =
                 XmlDsigHelper.Sign(xmlDoc).Using(cert).UsingFormat(selectedFormat)
                 .IncludingCertificateInSignature().IncludeTimestamp(setTimeStamp).NodeToSign(xpathToNodeToSign)
                 .NodeStoreSignatureTag(NodeStoreSignatureTag);
            xmlDoc = howToSign.SignAndGetXml();
            return xmlDoc.InnerXml;
        }
        //static public X509Certificate2 GetSignerCertHQ()
        //{
        //    string KeyContainerName = SerialNumber;
        //    //X509Store store = new X509Store(StoreLocation.,);
        //    store.Open(OpenFlags.ReadOnly);
        //    //X509Certificate2 cert = new X509Certificate2( null;
        //    //Sign from SmartCard
        //    //note : ProviderName and KeyContainerName can be found with the dos command : CertUtil -ScInfo
        //    string ProviderName = strProviderName;
        //    string KeyContainerName2 = strKeyContainerName;
        //    string PinCode = strpassword;
        //    if (PinCode != "")
        //    {
        //        //if pin code is set then no windows form will popup to ask it
        //        SecureString pwd = GetSecurePin(PinCode);
        //        CspParameters csp = new CspParameters(1, ProviderName, KeyContainerName2, new System.Security.AccessControl.CryptoKeySecurity(), pwd);
        //        // csp.KeyPassword = GetSecurePin(PinCode);
        //        try
        //        {
        //            RSACryptoServiceProvider rsaCsp = new RSACryptoServiceProvider(csp);
        //            // the pin code will be cached for next access to the smart card
        //        }
        //        catch (Exception ex)
        //        {
        //            // MessageBox.Show("Crypto error: " + ex.Message);
        //            // return;
        //            // throw ex;
        //        }
        //    }
        //    foreach (X509Certificate2 cert2 in store.Certificates)
        //    {
        //        if (cert2.SerialNumber.ToString() == KeyContainerName.ToUpper())
        //        {
        //            cert = cert2;
        //            break;
        //        }
        //    }
        //    return cert;

        //}
        // phải xin lại file cer của hải quan để thục hiện verify chữ ký
        //static void Main(string[] args)
        //{
        //    // Usage sample
        //    try
        //    {
        //        // Sign text
        //        byte[] signature = Sign("Test", "cn=my cert subject");

        //        // Verify signature. Testcert.cer corresponds to "cn=my cert subject"
        //        if (Verify("Test", signature, @"C:\testcert.cer"))
        //        {
        //            Console.WriteLine("Signature verified");
        //        }
        //        else
        //        {
        //            Console.WriteLine("ERROR: Signature not valid!");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("EXCEPTION: " + ex.Message);
        //    }
        //    Console.ReadKey();
        //}
        //static bool Verify(string text, byte[] signature, string certPath)
        //{
        //    // Load the certificate we'll use to verify the signature from a file 
        //    X509Certificate2 cert = new X509Certificate2(certPath);
        //    // Note: 
        //    // If we want to use the client cert in an ASP.NET app, we may use something like this instead:
        //    // X509Certificate2 cert = new X509Certificate2(Request.ClientCertificate.Certificate);

        public static string SignXMLCustom(string strXML)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.PreserveWhitespace = true;
                xmlDoc.LoadXml(strXML);
                // xmlDoc.Load(@"C:\msgHQ\m101.xml");
                //Khoi tao Object XMLSigner

                XMLSigner xmlSigner = new XMLSigner();

                //Ky bang XPath1.0
                String xpathSign = "ancestor-or-self::Customs";
                X509Certificate2 cert = GetSignerCert(strCertpath, strCertpass);

                //goi ham ky
                xmlSigner.XMLSigCustom(ref xmlDoc, cert, xpathSign);

               // return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xmlDoc.InnerXml;
                StringWriter stringWriter = new StringWriter();
                XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);

                xmlDoc.WriteTo(xmlTextWriter);

                string vXML_in = stringWriter.GetStringBuilder().ToString();
                return vXML_in;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//LogDebug.WriteLog(ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                throw ex;
            }
        }
        public static string SignXMLCustom312(string strXML)
        {
            try
            {
             //   LogDebug.WriteLog(strXML,EventLogEntryType.Information);
                XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.PreserveWhitespace = true;
               // VBOracleLib.LogDebug.WriteLog(strXML, EventLogEntryType.Error);
                xmlDoc.LoadXml(strXML);
                // xmlDoc.Load(@"C:\msgHQ\m101.xml");
                //Khoi tao Object XMLSigner

                XMLSigner xmlSigner = new XMLSigner();

                //Ky bang XPath1.0
                String xpathSign = "ancestor-or-self::Customs";
                X509Certificate2 cert = GetSignerCert(strCertpath, strCertpass);
            //    LogDebug.WriteLog("load cert "+cert.SerialNumber, EventLogEntryType.Information);
                //goi ham ky
                xmlSigner.XMLSigCustom312(ref xmlDoc, cert, xpathSign);
               
                // return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xmlDoc.InnerXml;
                StringWriter stringWriter = new StringWriter();
                XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);

                xmlDoc.WriteTo(xmlTextWriter);

                string vXML_in = stringWriter.GetStringBuilder().ToString();
                return vXML_in;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//LogDebug.WriteLog(ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                throw ex;
            }
        }
        public static string ValidateSignature(string XML)
        {
            XmlDocument xDoc = new XmlDocument();
            SignedXml signedXml;
            XmlNodeList nodeList;
            X509Certificate2 xCert = new X509Certificate2();

            try
            {
                using (StringReader reader = new StringReader(XML))
                {
                    //XML = Regex.Replace(XML, @"\r\n?|\n", "");
                    //xDoc.Load(@"C:\TKHQ\HQ_0409.xml");
                    xDoc.Load(reader);
                }
            }
            catch (Exception ex)
            {
                return "Verification False: Invalid XML Input" + "/n/r" + ex.StackTrace;
            }


            //Check signature Tag
            signedXml = new SignedXml(xDoc);
            nodeList = xDoc.GetElementsByTagName("Signature");

            if (nodeList.Count <= 0) return "Verification False: No Signature was found in the document.";
            if (nodeList.Count > 1) return "Verification False: More than one signature was found for the document.";

            try
            {
                signedXml.LoadXml((XmlElement)nodeList[0]);
            }
            catch (Exception ex1)
            {
                return "Verification False: False to load signature tag." + ex1.StackTrace;

            }
            //Check 
            nodeList = null;
            nodeList = xDoc.GetElementsByTagName("X509Certificate");
            if (nodeList.Count == 1)
            {
                try
                {
                    xCert.Import(Convert.FromBase64String(nodeList[0].InnerText));
                }
                catch (Exception exx)
                {
                    return "Verification False: Invalid Public Certificate /n/r " + exx.StackTrace;
                }

            }

            else return "Verification False: More than one public certificate was found for the document.";

            try
            {
                bool bCheck = signedXml.CheckSignature(xCert, true);

                if (bCheck) return "OK";
                else return "Verification Invalid.";
            }
            catch (Exception ex) { return "Lỗi trong quá trình verify chữ ký " + ex.StackTrace; }

        }
        public static string ValidateSignature31(string XML)
        {
            XmlDocument xDoc = new XmlDocument();
            SignedXml signedXml;
            XmlNodeList nodeList;
            X509Certificate2 xCert = new X509Certificate2();

            try
            {
                using (StringReader reader = new StringReader(XML))
                {
                    //XML = Regex.Replace(XML, @"\r\n?|\n", "");
                    //xDoc.Load(@"C:\TKHQ\HQ_0409.xml");
                    xDoc.Load(reader);
                }
            }
            catch (Exception ex)
            {
                return "Verification False: Invalid XML Input" + "/n/r" + ex.StackTrace;
            }


            //Check signature Tag
            signedXml = new SignedXml(xDoc);
            nodeList = xDoc.GetElementsByTagName("Signature");

            if (nodeList.Count <= 0) return "Verification False: No Signature was found in the document.";
            //if (nodeList.Count > 1) return "Verification False: More than one signature was found for the document.";
            for (int i = 0; i < nodeList.Count; i++)
            {
                try
                {
                    signedXml.LoadXml((XmlElement)nodeList[i]);
                }
                catch (Exception ex1)
                {
                    return "Verification False: False to load signature tag." + ex1.StackTrace;

                }
                //Check 
                XmlNodeList  nodeList01 = null;
                nodeList01 = xDoc.GetElementsByTagName("X509Certificate");
                for (int x = 0; x < nodeList01.Count; x++)
                {
                    try
                    {
                        xCert.Import(Convert.FromBase64String(nodeList01[x].InnerText));
                    }
                    catch (Exception exx)
                    {
                        return "Verification False: Invalid Public Certificate /n/r " + exx.StackTrace;
                    }
                    bool bCheck = signedXml.CheckSignature(xCert, true);
                    if (bCheck == false)
                    {
                        return "Verification False: Invalid Public Certificate /n/r number " + x.ToString(); 
                    }
                }
                   
            }
            return "OK";
        }

    }
}
