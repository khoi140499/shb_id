﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TEST_SIGN
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string content = "";
            content = richTextBox1.Text.Trim();

            string signatureType = GetValue_THAMSOHT("SIGN.TYPE.TCT");
            string xpathSign = "/DATA/BODY/ROW/GIAODICH";
            string xpathNodeSign = "GIAODICH";
            if (signatureType.Equals("1"))
            {
                content = Signature.DigitalSignature.getSignNTDT(content, xpathSign, xpathNodeSign);
            }
            else
            {
                content = Signature.SignatureProcess.SignXMLEextendTimeStamp(content, "GIAODICH", "GIAODICH", true);
            }

            richTextBox1.Text = content;
        }

        public static string GetValue_THAMSOHT(string strParmName)
        {
            string Result = "";
            try
            {
                string strSQL = "SELECT a.giatri_ts FROM tcs_thamso_ht a  where a.ten_ts ='" + strParmName + "' ";
                DataTable dt = null;
                dt = VBOracleLib.DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = dt.Rows[0]["GIATRI_TS"].ToString();
                }
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
    }
}
