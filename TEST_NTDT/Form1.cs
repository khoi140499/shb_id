﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TEST_NTDT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string message = richTextBox1.Text.Trim();
            ETAX_GDT.ProcessMSG procmsg = new ETAX_GDT.ProcessMSG();
            string tran_code = "";
            string ms_id = "";
            procmsg.process_Msg(message, ref tran_code, ref ms_id);
            Console.WriteLine("Received Message: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # tran_code=" + tran_code + " # msg_id=" + ms_id);
        }
    }
}
