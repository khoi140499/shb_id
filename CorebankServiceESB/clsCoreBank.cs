﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Configuration;
using VBOracleLib;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Collections;
using System.Diagnostics;
using System.Net;
namespace CorebankServiceESB
{
    public class clsCoreBank
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static String Core_status = ConfigurationManager.AppSettings.Get("CORE.ON").ToString();
        private static String strXMLdefin2 = "<PARAMETER xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
        string Br_Code = "BR0001";
        string prefixSEQ = "TAX";
        string TT_THANHCONG = "00000";

        private static String URL_CORE = ConfigurationManager.AppSettings.Get("URL_CORE").ToString();
        private static String strConnection = ConfigurationManager.AppSettings.Get("CTC_DB_CONN").ToString();
        public clsCoreBank()
        {

        }
        //MOBI NEW
        public MOBI.AccountResp GetTK_XACTHUCNTT(string soTaiKhoan_The, string phuongThuc)
        {
            //phuongthuc : ACCOUNT Or CARD
            //loaiGiayTo: cccd or cmnn or hc

            MOBI.AccountResp ac_xacthuc = new MOBI.AccountResp();
            //core on là 1
            if (Core_status == "1")
            {
                CoreESB a = new CoreESB();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                //call WSProcess ở đây

            }
            else
            {
                ac_xacthuc.errorCode = "0";
                ac_xacthuc.accName = "NGUYEN VAN A";
                ac_xacthuc.accNo = "1966668888";
                ac_xacthuc.avlBalance = "10000000000";
                ac_xacthuc.branchCode = "110000";
                ac_xacthuc.cifNo = "123456789";
                ac_xacthuc.typeAccount = "";
                ac_xacthuc.cCrrcy = "VND";
                ac_xacthuc.dienThoai = "0962342424";
                ac_xacthuc.loaiGiayTo = "CCCD";
                ac_xacthuc.soGiayTo = "1232435353";
            }
            return ac_xacthuc;
        }

        public MOBI.AccountResp GetTK_XACTHUCNTT_NEW(string soTaiKhoan_The, string phuongThuc, string loaiGiayTo)
        {
            //phuongthuc : ACCOUNT Or CARD
            //loaiGiayTo: cccd or cmnn or hc

            MOBI.AccountResp ac_xacthuc = new MOBI.AccountResp();
            //core on là 1
            if (Core_status == "1")
            {
                CoreESB a = new CoreESB();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                //call WSProcess ở đây

            }
            else
            {
                ac_xacthuc.errorCode = "0";
                ac_xacthuc.accName = "NGUYEN VAN A";
                ac_xacthuc.accNo = "1966668888";
                ac_xacthuc.avlBalance = "10000000000";
                ac_xacthuc.branchCode = "110000";
                ac_xacthuc.cifNo = "123456789";
                ac_xacthuc.typeAccount = "";
                ac_xacthuc.cCrrcy = "VND";
                ac_xacthuc.dienThoai = "0962342424";
                ac_xacthuc.loaiGiayTo = "CCCD";
                ac_xacthuc.soGiayTo = "1232435353";
            }
            return ac_xacthuc;
        }

        public MOBI.SendOTPInfo SendOTP(string strSoCif, string aMount)
        {
            //phuongthuc : ACCOUNT Or CARD
            //loaiGiayTo: cccd or cmnn or hc

            MOBI.SendOTPInfo vobj = new MOBI.SendOTPInfo();
            //core on là 1
            if (Core_status == "1")
            {
                CoreESB a = new CoreESB();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                //call WSProcess ở đây

            }
            else
            {
                vobj.ERRCODE = "0";
                vobj.ERRMSG = "Thông tin đã được gửi  qua SMS";
                vobj.TRANID = "TEST" + DateTime.Now.ToString("yyyyMMddHHmmss");
                vobj.loaiOTP = "SMS";
                vobj.DESC = "";
            }
            return vobj;
        }

        public string GetChareEtaxMobile(string strPoAmt, string strDebitAccount)
        {
            //strPoAmt : số tiền
            //strDebitAccount: số tài khoản
            string phi = "";
            //core on là 1
            if (Core_status == "1")
            {
                CoreESB a = new CoreESB();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                //call WSProcess ở đây

            }
            else
            {
                phi = "10000";
            }
            return phi;
        }

        public string validOTP(string strMaGD, string strSoCif, string strOtpCode, string strAmount)
        {
            //core on là 1
            string vStrOPT = "";
            try
            {
                vStrOPT = ConfigurationManager.AppSettings["CORE.OTP"]; //ConfigurationManager.AppSettings.Get("CORE.OTP").ToString();
            }
            catch (Exception ex)
            {
                vStrOPT = "1";
            }

            if (vStrOPT == null)
            {
                vStrOPT = "1";
            }
            {
                vStrOPT = "1";
            }

            if (vStrOPT.Trim().Length < 0)
            {
                vStrOPT = "1";
            }
           

            if (Core_status == "0" || vStrOPT =="0")
            {
                if (strOtpCode.Equals("123456"))
                {
                    return "00";
                }
                else if (strOtpCode.Equals("123456"))
                {
                    return "071";
                }
                else
                {
                    return "072";
                }
            }
            else
            {
                //chạy thật ở dưới đây
                CoreESB a = new CoreESB();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                //call WSProcess ở đây

            }
            return "";
        }

        //END MOBI
        //Kiểm tra số dư-Lay thong tin tai khoan
        public string GetTK_KH_NH(string acountNo, ref string MA_LOI, ref string SO_DU_KD, ref string BRANCHID)
        {
            string msgRes = "";
            string strReturn = "";
            if (Core_status == "1")
            {
                CoreESB a = new CoreESB();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                //VBOracleLib.LogApp.AddDebug("GetTK_KH_NH", "1.Truy van ESB.ACCOUNT:" + msgRes);
                //LogDebug.WriteLog("1.Truy van ESB.ACCOUNT:" + msgRes, EventLogEntryType.Error);
                msgRes = a.WSProcess(acountNo, "ACCOUNT");
                //VBOracleLib.LogApp.AddDebug("GetTK_KH_NH", "2.Truy van ESB:" + msgRes);
                //LogDebug.WriteLog("2.Truy van ESB:" + msgRes, EventLogEntryType.Error);
                System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                VXDOC.LoadXml(msgRes);
                strReturn = VXDOC.SelectSingleNode("Customs/strReturn").InnerXml.ToString();
                MA_LOI = VXDOC.SelectSingleNode("Customs/MA_LOI").InnerText;
                SO_DU_KD = VXDOC.SelectSingleNode("Customs/SO_DU_KD").InnerText;
                BRANCHID = VXDOC.SelectSingleNode("Customs/BRANCHID").InnerText;
                if (MA_LOI == "0")
                {
                    MA_LOI = "00000";
                }
                return strReturn;
            }
            else
            {

                SO_DU_KD = "50000000000";
                MA_LOI = "00000";
                BRANCHID = "110000";
                strReturn = ("<PARAMETER><PARAMS><BANKACCOUNT>"
                            + ("0100100011-12345" + ("</BANKACCOUNT>" + ("<ACY_AVL_BAL>"
                            + ("1000000000" + ("</ACY_AVL_BAL><CRR_CY_CODE>"
                            + ("VND" + ("</CRR_CY_CODE><BRANCHID>"
                            + ("110000" + "</BRANCHID><RETCODE>"
                            + ("00000" + ("</RETCODE><ERRCODE>"
                            + ("00000" + "</ERRCODE></PARAMS></PARAMETER>"))))))))))));
                return strReturn;
            }
            return strReturn;
        }


        //Payment Thue Tai Quay
        public string PaymentTQ(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
           string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
           string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
           string v_str_KS, string v_KenhCT, string v_CustInfo, ref  string v_str_errcode, ref  string v_str_ref_no)
        {
            string msgRes = "";
            try
            {
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<DATA>");
                strBuildXML.Append("<pv_ACCOUNTNO>" + pv_ACCOUNTNO + "</pv_ACCOUNTNO>");
                strBuildXML.Append("<pv_BEN_ACCOUNTNO>" + pv_BEN_ACCOUNTNO + "</pv_BEN_ACCOUNTNO>");
                strBuildXML.Append("<AMOUNT>" + AMOUNT + "</AMOUNT>");
                strBuildXML.Append("<CCY>" + CCY + "</CCY>");
                strBuildXML.Append("<REMARK><![CDATA[" + REMARK + "]]></REMARK>");
                strBuildXML.Append("<TransactionDate>" + TransactionDate + "</TransactionDate>");
                strBuildXML.Append("<BEN_NAME><![CDATA[" + BEN_NAME + "]]></BEN_NAME>");
                strBuildXML.Append("<BEN_CITY>" + BEN_CITY + "</BEN_CITY>");
                strBuildXML.Append("<BEN_ADD1><![CDATA[" + BEN_ADD1 + "]]></BEN_ADD1>");
                strBuildXML.Append("<BEN_ADD2><![CDATA[" + BEN_ADD2 + "]]></BEN_ADD2>");
                strBuildXML.Append("<BANK_BEN_DESC><![CDATA[" + BANK_BEN_DESC + "]]></BANK_BEN_DESC>");
                strBuildXML.Append("<BR_BEN_DESC><![CDATA[" + BR_BEN_DESC + "]]></BR_BEN_DESC>");
                strBuildXML.Append("<CITY_BEN_DESC><![CDATA[" + CITY_BEN_DESC + "]]></CITY_BEN_DESC>");
                strBuildXML.Append("<CITY_BEN_DESC><![CDATA[" + CITY_BEN_DESC + "]]></CITY_BEN_DESC>");
                strBuildXML.Append("<BANK_BEN_CODE>" + BANK_BEN_CODE + "</BANK_BEN_CODE>");
                strBuildXML.Append("<BR_BEN_CODE>" + BR_BEN_CODE + "</BR_BEN_CODE>");
                strBuildXML.Append("<CITY_BEN_CODE>" + CITY_BEN_CODE + "</CITY_BEN_CODE>");
                strBuildXML.Append("<KenhHT>" + KenhHT + "</KenhHT>");
                strBuildXML.Append("<v_strSo_CT>" + v_strSo_CT + "</v_strSo_CT>");
                strBuildXML.Append("<strMa_NH_B>" + strMa_NH_B + "</strMa_NH_B>");
                strBuildXML.Append("<v_strMa_NV>" + v_strMa_NV + "</v_strMa_NV>");
                strBuildXML.Append("<v_str_KS>" + v_str_KS + "</v_str_KS>");
                strBuildXML.Append("<v_KenhCT>" + v_KenhCT + "</v_KenhCT>");
                strBuildXML.Append("<v_CustInfo><![CDATA[" + v_CustInfo + "]]></v_CustInfo>");
                strBuildXML.Append("</DATA>");
                strBuildXML.Append("</Customs>");
                if (Core_status == "1")
                {
                    CoreESB a = new CoreESB();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    //LogDebug.WriteLog("MSG_IN_WEB:" + strBuildXML.ToString(), EventLogEntryType.Error);
                    msgRes = a.WSProcess(strBuildXML.ToString(), "HQ");
                    //LogDebug.WriteLog("MSG_OUT_WEB:" + msgRes, EventLogEntryType.Error);
                    System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    VXDOC.LoadXml(msgRes);
                    v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;
                    v_str_ref_no = VXDOC.SelectSingleNode("Customs/Ref_no").InnerText;
                }
                else
                {
                    //VBOracleLib.LogApp.AddDebug("PaymentTQ", "XXXXXXXX - CORE TEST");
                    //CoreESB a = new CoreESB();
                    //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    ////LogDebug.WriteLog("MSG_IN_WEB:" + strBuildXML.ToString(), EventLogEntryType.Error);
                    //msgRes = a.WSProcess(strBuildXML.ToString(), "HQ");
                    ////LogDebug.WriteLog("MSG_OUT_WEB:" + msgRes, EventLogEntryType.Error);
                    //System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    //VXDOC.LoadXml(msgRes);
                    //v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;
                    //v_str_ref_no = VXDOC.SelectSingleNode("Customs/Ref_no").InnerText;
                    v_str_errcode = "00000";
                    v_str_ref_no = "TEST1234567";
                }
            }
            catch (Exception ex)
            {
                v_str_errcode = "11111";
                v_str_ref_no = ex.Message;
            }
            return msgRes.ToString();
        }

        //Version 2.0 
        // Tach cac chi tiet
        //
        // NGAY_KB,
        public string PaymentTQ_V20(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
           string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
           string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
           string v_str_KS, string v_KenhCT, string v_CustInfo,
           string TaxPymtDate, string TaxType, string TaxCode, string v_MaCQT, string v_TenCQT, string v_MaDBHC, string v_TenDBHC, string TaxPayerName, string TaxPayerAdd, string EcomStr,
           ref  string v_str_errcode, ref  string v_str_ref_no)
        {
            string msgRes = "";
            try
            {
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<DATA>");
                strBuildXML.Append("<pv_ACCOUNTNO>" + pv_ACCOUNTNO + "</pv_ACCOUNTNO>");
                strBuildXML.Append("<pv_BEN_ACCOUNTNO>" + pv_BEN_ACCOUNTNO + "</pv_BEN_ACCOUNTNO>");
                strBuildXML.Append("<AMOUNT>" + AMOUNT + "</AMOUNT>");
                strBuildXML.Append("<CCY>" + CCY + "</CCY>");
                strBuildXML.Append("<REMARK><![CDATA[" + REMARK + "]]></REMARK>");
                strBuildXML.Append("<TransactionDate>" + TransactionDate + "</TransactionDate>");
                strBuildXML.Append("<BEN_NAME><![CDATA[" + BEN_NAME + "]]></BEN_NAME>");
                strBuildXML.Append("<BEN_CITY>" + BEN_CITY + "</BEN_CITY>");
                strBuildXML.Append("<BEN_ADD1><![CDATA[" + BEN_ADD1 + "]]></BEN_ADD1>");
                strBuildXML.Append("<BEN_ADD2><![CDATA[" + BEN_ADD2 + "]]></BEN_ADD2>");
                strBuildXML.Append("<BANK_BEN_DESC><![CDATA[" + BANK_BEN_DESC + "]]></BANK_BEN_DESC>");
                strBuildXML.Append("<BR_BEN_DESC><![CDATA[" + BR_BEN_DESC + "]]></BR_BEN_DESC>");
                strBuildXML.Append("<CITY_BEN_DESC><![CDATA[" + CITY_BEN_DESC + "]]></CITY_BEN_DESC>");
                strBuildXML.Append("<CITY_BEN_DESC><![CDATA[" + CITY_BEN_DESC + "]]></CITY_BEN_DESC>");
                strBuildXML.Append("<BANK_BEN_CODE>" + BANK_BEN_CODE + "</BANK_BEN_CODE>");
                strBuildXML.Append("<BR_BEN_CODE>" + BR_BEN_CODE + "</BR_BEN_CODE>");
                strBuildXML.Append("<CITY_BEN_CODE>" + CITY_BEN_CODE + "</CITY_BEN_CODE>");
                strBuildXML.Append("<KenhHT>" + KenhHT + "</KenhHT>");
                strBuildXML.Append("<v_strSo_CT>" + v_strSo_CT + "</v_strSo_CT>");
                strBuildXML.Append("<strMa_NH_B>" + strMa_NH_B + "</strMa_NH_B>");
                strBuildXML.Append("<v_strMa_NV>" + v_strMa_NV + "</v_strMa_NV>");
                strBuildXML.Append("<v_str_KS>" + v_str_KS + "</v_str_KS>");
                strBuildXML.Append("<v_KenhCT>" + v_KenhCT + "</v_KenhCT>");
                strBuildXML.Append("<v_CustInfo><![CDATA[" + v_CustInfo + "]]></v_CustInfo>");
                strBuildXML.Append("<TaxPymtDate>" + TaxPymtDate + "</TaxPymtDate>");
                strBuildXML.Append("<TaxType>" + TaxType + "</TaxType>");
                strBuildXML.Append("<TaxCode>" + TaxCode + "</TaxCode>");
                strBuildXML.Append("<v_MaCQT>" + v_MaCQT + "</v_MaCQT>");
                strBuildXML.Append("<v_TenCQT><![CDATA[" + v_TenCQT + "]]></v_TenCQT>");
                strBuildXML.Append("<v_MaDBHC>" + v_MaDBHC + "</v_MaDBHC>");
                strBuildXML.Append("<v_TenDBHC><![CDATA[" + v_TenDBHC + "]]></v_TenDBHC>");
                strBuildXML.Append("<TaxPayerName><![CDATA[" + TaxPayerName + "]]></TaxPayerName>");
                strBuildXML.Append("<TaxPayerAdd>" + TaxPayerAdd + "</TaxPayerAdd>");
                strBuildXML.Append("<EcomStr>" + EcomStr + "</EcomStr>");
                strBuildXML.Append("</DATA>");
                strBuildXML.Append("</Customs>");
                if (Core_status == "1")
                {
                    CoreESB a = new CoreESB();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    msgRes = a.WSProcess(strBuildXML.ToString(), "HQ");
                    System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    VXDOC.LoadXml(msgRes);
                    v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;
                    v_str_ref_no = VXDOC.SelectSingleNode("Customs/Ref_no").InnerText;
                }
                else
                {
                    v_str_errcode = "00000";
                    v_str_ref_no = "TEST1234567";
                }
            }
            catch (Exception ex)
            {
                v_str_errcode = "11111";
                v_str_ref_no = ex.Message;
            }
            return msgRes.ToString();
        }
        //Payment Thue Tai Quay
        public string PaymentSP(string KenhHT, string v_strNgayGD, string v_strSo_CT, string strMACN, string strMACN_TK, string pv_ACCOUNTNO, string AMOUNT, string CCY, string v_CustInfo, string REMARK,
           string v_strMa_NV, string v_str_KS, string v_KenhCT, string BANK_BEN_CODE, string pv_BEN_ACCOUNTNO, string strMa_NH_B, string strACC_NH_B, string strMA_CN_SP, ref  string v_str_errcode, ref  string v_str_ref_no)
        {
            string msgRes = "";
            try
            {
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<DATA>");
                strBuildXML.Append("<KenhHT>" + KenhHT + "</KenhHT>");
                strBuildXML.Append("<NGAYGD>" + v_strNgayGD + "</NGAYGD>");
                strBuildXML.Append("<v_strSo_CT>" + v_strSo_CT + "</v_strSo_CT>");
                strBuildXML.Append("<MA_CN>" + strMACN + "</MA_CN>");
                strBuildXML.Append("<MA_CN_TK>" + strMACN_TK + "</MA_CN_TK>");
                strBuildXML.Append("<pv_ACCOUNTNO>" + pv_ACCOUNTNO + "</pv_ACCOUNTNO>");
                strBuildXML.Append("<AMOUNT>" + AMOUNT + "</AMOUNT>");
                strBuildXML.Append("<CCY>" + CCY + "</CCY>");
                strBuildXML.Append("<REMARK><![CDATA[" + REMARK + "]]></REMARK>");
                strBuildXML.Append("<v_strMa_NV>" + v_strMa_NV + "</v_strMa_NV>");
                strBuildXML.Append("<v_str_KS>" + v_str_KS + "</v_str_KS>");
                strBuildXML.Append("<v_KenhCT>" + v_KenhCT + "</v_KenhCT>");
                strBuildXML.Append("<v_CustInfo><![CDATA[" + v_CustInfo + "]]></v_CustInfo>");
                strBuildXML.Append("<BANK_BEN_CODE>" + BANK_BEN_CODE + "</BANK_BEN_CODE>");
                strBuildXML.Append("<pv_BEN_ACCOUNTNO>" + pv_BEN_ACCOUNTNO + "</pv_BEN_ACCOUNTNO>");
                strBuildXML.Append("<strMa_NH_B>" + strMa_NH_B + "</strMa_NH_B>");
                strBuildXML.Append("<strACC_NH_B>" + strACC_NH_B + "</strACC_NH_B>");
                strBuildXML.Append("<strMA_CN_SP>" + strMA_CN_SP + "</strMA_CN_SP>");
                strBuildXML.Append("</DATA>");
                strBuildXML.Append("</Customs>");
                if (Core_status == "1")
                {
                    CoreESB a = new CoreESB();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    //VBOracleLib.LogApp.AddDebug("1.PaymentSP", "MSG_IN_WEB:" + strBuildXML.ToString());
                    //LogDebug.WriteLog("MSG_IN_WEB:" + strBuildXML.ToString(), EventLogEntryType.Error);
                    msgRes = a.WSProcess(strBuildXML.ToString(), "SP");
                    //VBOracleLib.LogApp.AddDebug("2.PaymentSP", "MSG_OUT_WEB:" + msgRes);
                    //LogDebug.WriteLog("MSG_OUT_WEB:" + msgRes, EventLogEntryType.Error);
                    System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    VXDOC.LoadXml(msgRes);
                    v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;
                    v_str_ref_no = VXDOC.SelectSingleNode("Customs/Ref_no").InnerText;
                }
                else
                {
                    //VBOracleLib.LogApp.AddDebug("PaymentSP", "XXXXXXXX - CORE TEST");
                    //CoreESB a = new CoreESB();
                    //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    //LogDebug.WriteLog("MSG_IN_WEB:" + strBuildXML.ToString(), EventLogEntryType.Error);
                    //msgRes = a.WSProcess(strBuildXML.ToString(), "SP");
                    //LogDebug.WriteLog("MSG_OUT_WEB:" + msgRes, EventLogEntryType.Error);
                    //System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    //VXDOC.LoadXml(msgRes);
                    //v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;
                    //v_str_ref_no = VXDOC.SelectSingleNode("Customs/Ref_no").InnerText;
                    v_str_errcode = "00000";
                    v_str_ref_no = "TEST" + DateTime.Now.ToString("yyyyMMddHHmmss");
                }
            }
            catch (Exception ex)
            {
                v_str_errcode = "11111";
                v_str_ref_no = ex.Message;
            }
            return msgRes.ToString();
        }

        //Payment Phí bộ ngành
        public string PaymentPBN(string v_KenhCT, string strMA_CN, string strREF_NO, string strSalary, string strBalancer, string strFEF_SEQ, string strSEQ_NO,
            string FINANCIAL, string strACC_NO, string strCredit, string strTRANSACTIONAMOUNT, string strTRANSACTIONCURRENCY, string strREMARKS, ref  string v_str_errcode, ref  string v_str_ref_no)
        {
            string msgRes = "";
            try
            {
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<DATA>");
                strBuildXML.Append("<v_KenhCT>" + v_KenhCT + "</v_KenhCT>");
                strBuildXML.Append("<strMA_CN>" + strMA_CN + "</strMA_CN>");
                strBuildXML.Append("<strREF_NO>" + strREF_NO + "</strREF_NO>");
                strBuildXML.Append("<strSalary>" + strSalary + "</strSalary>");
                strBuildXML.Append("<strBalancer>" + strBalancer + "</strBalancer>");
                strBuildXML.Append("<strFEF_SEQ>" + strFEF_SEQ + "</strFEF_SEQ>");
                strBuildXML.Append("<strSEQ_NO>" + strSEQ_NO + "</strSEQ_NO>");
                strBuildXML.Append("<FINANCIAL>" + FINANCIAL + "</FINANCIAL>");
                strBuildXML.Append("<strACC_NO>" + strACC_NO + "</strACC_NO>");
                strBuildXML.Append("<strCredit>" + strCredit + "</strCredit>");
                strBuildXML.Append("<strTRANSACTIONAMOUNT>" + strTRANSACTIONAMOUNT + "</strTRANSACTIONAMOUNT>");
                strBuildXML.Append("<strTRANSACTIONCURRENCY>" + strTRANSACTIONCURRENCY + "</strTRANSACTIONCURRENCY>");
                strBuildXML.Append("<strREMARKS><![CDATA[" + strREMARKS + "]]></strREMARKS>");
                strBuildXML.Append("</DATA>");
                strBuildXML.Append("</Customs>");

                if (Core_status == "1")
                {
                    CoreESB a = new CoreESB();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    msgRes = a.WSProcess(strBuildXML.ToString(), "PBN");
                    System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    VXDOC.LoadXml(msgRes);
                    v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;
                    v_str_ref_no = VXDOC.SelectSingleNode("Customs/Ref_no").InnerText;

                }
                else
                {
                    v_str_errcode = "00000";
                    v_str_ref_no = "TEST1234567";
                }
            }
            catch (Exception ex)
            {
                v_str_errcode = "11111";
                v_str_ref_no = ex.Message;
            }
            return msgRes.ToString();
        }


        //Payment Thue Dien Tu
        public string Payment(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
            string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
            string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
            string v_str_KS, string v_KenhCT, string customer_info, ref  string v_str_errcode, ref  string v_str_ref_no)
        {

            string msgRes = "";
            string res_Ref_No = "";
            string res_Curr_Bal = "";
            string res_Result_Code = "";
            string res_Status = "";
            string err_msg = "";
            try
            {
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<DATA>");
                strBuildXML.Append("<pv_ACCOUNTNO>" + pv_ACCOUNTNO + "</pv_ACCOUNTNO>");
                strBuildXML.Append("<pv_BEN_ACCOUNTNO>" + pv_BEN_ACCOUNTNO + "</pv_BEN_ACCOUNTNO>");
                strBuildXML.Append("<AMOUNT>" + AMOUNT + "</AMOUNT>");
                strBuildXML.Append("<CCY>" + CCY + "</CCY>");
                strBuildXML.Append("<REMARK><![CDATA[" + REMARK + "]]></REMARK>");
                strBuildXML.Append("<TransactionDate>" + TransactionDate + "</TransactionDate>");
                strBuildXML.Append("<BEN_NAME><![CDATA[" + BEN_NAME + "]]></BEN_NAME>");
                strBuildXML.Append("<BEN_CITY>" + BEN_CITY + "</BEN_CITY>");
                strBuildXML.Append("<BEN_ADD1><![CDATA[" + BEN_ADD1 + "]]></BEN_ADD1>");
                strBuildXML.Append("<BEN_ADD2><![CDATA[" + BEN_ADD2 + "]]></BEN_ADD2>");
                strBuildXML.Append("<BANK_BEN_DESC><![CDATA[" + BANK_BEN_DESC + "]]></BANK_BEN_DESC>");
                strBuildXML.Append("<BR_BEN_DESC><![CDATA[" + BR_BEN_DESC + "]]></BR_BEN_DESC>");
                strBuildXML.Append("<CITY_BEN_DESC><![CDATA[" + CITY_BEN_DESC + "]]></CITY_BEN_DESC>");
                strBuildXML.Append("<BANK_BEN_CODE>" + BANK_BEN_CODE + "</BANK_BEN_CODE>");
                strBuildXML.Append("<BR_BEN_CODE>" + BR_BEN_CODE + "</BR_BEN_CODE>");
                strBuildXML.Append("<CITY_BEN_CODE>" + CITY_BEN_CODE + "</CITY_BEN_CODE>");
                strBuildXML.Append("<KenhHT>" + KenhHT + "</KenhHT>");
                strBuildXML.Append("<v_strSo_CT>" + v_strSo_CT + "</v_strSo_CT>");
                strBuildXML.Append("<strMa_NH_B>" + strMa_NH_B + "</strMa_NH_B>");
                strBuildXML.Append("<v_strMa_NV>" + v_strMa_NV + "</v_strMa_NV>");
                strBuildXML.Append("<v_str_KS>" + v_str_KS + "</v_str_KS>");
                strBuildXML.Append("<v_KenhCT>" + v_KenhCT + "</v_KenhCT>");
                strBuildXML.Append("<customer_info><![CDATA[" + customer_info + "]]></customer_info>");
                strBuildXML.Append("</DATA>");
                strBuildXML.Append("</Customs>");


                if (Core_status == "1")
                {

                    CoreESB a = new CoreESB();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    msgRes = a.WSProcess(strBuildXML.ToString(), "NTDT");
                    System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    VXDOC.LoadXml(msgRes);
                    v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;
                    v_str_ref_no = VXDOC.SelectSingleNode("Customs/Ref_no").InnerText;

                }
                else
                {

                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                    v_str_errcode = "0";
                    v_str_ref_no = "TEST" + DateTime.Now.ToString("yyyyMMddHHmmss");
                }
            }
            catch (Exception ex)
            {
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }

            return msgRes;
        }
        public string Payment_V20(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
          string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
          string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
          string v_str_KS, string v_KenhCT, string customer_info,
          string TaxPymtDate, string TaxType, string TaxCode, string v_MaCQT, string v_TenCQT, string v_MaDBHC, string v_TenDBHC, string TaxPayerName, string TaxPayerAdd, string EcomStr,
          ref  string v_str_errcode, ref  string v_str_ref_no)
        {

            string msgRes = "";
            string res_Ref_No = "";
            string res_Curr_Bal = "";
            string res_Result_Code = "";
            string res_Status = "";
            string err_msg = "";
            try
            {
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<DATA>");
                strBuildXML.Append("<pv_ACCOUNTNO>" + pv_ACCOUNTNO + "</pv_ACCOUNTNO>");
                strBuildXML.Append("<pv_BEN_ACCOUNTNO>" + pv_BEN_ACCOUNTNO + "</pv_BEN_ACCOUNTNO>");
                strBuildXML.Append("<AMOUNT>" + AMOUNT + "</AMOUNT>");
                strBuildXML.Append("<CCY>" + CCY + "</CCY>");
                strBuildXML.Append("<REMARK><![CDATA[" + REMARK + "]]></REMARK>");
                strBuildXML.Append("<TransactionDate>" + TransactionDate + "</TransactionDate>");
                strBuildXML.Append("<BEN_NAME><![CDATA[" + BEN_NAME + "]]></BEN_NAME>");
                strBuildXML.Append("<BEN_CITY>" + BEN_CITY + "</BEN_CITY>");
                strBuildXML.Append("<BEN_ADD1><![CDATA[" + BEN_ADD1 + "]]></BEN_ADD1>");
                strBuildXML.Append("<BEN_ADD2><![CDATA[" + BEN_ADD2 + "]]></BEN_ADD2>");
                strBuildXML.Append("<BANK_BEN_DESC><![CDATA[" + BANK_BEN_DESC + "]]></BANK_BEN_DESC>");
                strBuildXML.Append("<BR_BEN_DESC><![CDATA[" + BR_BEN_DESC + "]]></BR_BEN_DESC>");
                strBuildXML.Append("<CITY_BEN_DESC><![CDATA[" + CITY_BEN_DESC + "]]></CITY_BEN_DESC>");
                strBuildXML.Append("<BANK_BEN_CODE>" + BANK_BEN_CODE + "</BANK_BEN_CODE>");
                strBuildXML.Append("<BR_BEN_CODE>" + BR_BEN_CODE + "</BR_BEN_CODE>");
                strBuildXML.Append("<CITY_BEN_CODE>" + CITY_BEN_CODE + "</CITY_BEN_CODE>");
                strBuildXML.Append("<KenhHT>" + KenhHT + "</KenhHT>");
                strBuildXML.Append("<v_strSo_CT>" + v_strSo_CT + "</v_strSo_CT>");
                strBuildXML.Append("<strMa_NH_B>" + strMa_NH_B + "</strMa_NH_B>");
                strBuildXML.Append("<v_strMa_NV>" + v_strMa_NV + "</v_strMa_NV>");
                strBuildXML.Append("<v_str_KS>" + v_str_KS + "</v_str_KS>");
                strBuildXML.Append("<v_KenhCT>" + v_KenhCT + "</v_KenhCT>");
                strBuildXML.Append("<customer_info>" + customer_info + "</customer_info>");
                strBuildXML.Append("<TaxPymtDate>" + TaxPymtDate + "</TaxPymtDate>");
                strBuildXML.Append("<TaxType>" + TaxType + "</TaxType>");
                strBuildXML.Append("<TaxCode>" + TaxCode + "</TaxCode>");
                strBuildXML.Append("<v_MaCQT>" + v_MaCQT + "</v_MaCQT>");
                strBuildXML.Append("<v_TenCQT><![CDATA[" + v_TenCQT + "]]></v_TenCQT>");
                strBuildXML.Append("<v_MaDBHC>" + v_MaDBHC + "</v_MaDBHC>");
                strBuildXML.Append("<v_TenDBHC><![CDATA[" + v_TenDBHC + "]]></v_TenDBHC>");
                strBuildXML.Append("<TaxPayerName><![CDATA[" + TaxPayerName + "]]></TaxPayerName>");
                strBuildXML.Append("<TaxPayerAdd>" + TaxPayerAdd + "</TaxPayerAdd>");
                strBuildXML.Append("<EcomStr>" + EcomStr + "</EcomStr>");
                strBuildXML.Append("</DATA>");
                strBuildXML.Append("</Customs>");


                if (Core_status == "1")
                {

                    CoreESB a = new CoreESB();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    msgRes = a.WSProcess(strBuildXML.ToString(), "NTDT");
                    System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    VXDOC.LoadXml(msgRes);
                    v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;
                    v_str_ref_no = VXDOC.SelectSingleNode("Customs/Ref_no").InnerText;

                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }
            }
            catch (Exception ex)
            {
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }

            return msgRes;
        }

        // tuanda
        public string Payment_V20(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
          string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
          string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
          string v_str_KS, string v_KenhCT, string customer_info,
          string TaxPymtDate, string TaxType, string TaxCode, string v_MaCQT, string v_TenCQT, string TaxPayerName, string TaxPayerAdd, string EcomStr,
          ref  string v_str_errcode, ref  string v_str_ref_no)
        {

            string msgRes = "";
            string res_Ref_No = "";
            string res_Curr_Bal = "";
            string res_Result_Code = "";
            string res_Status = "";
            string err_msg = "";
            try
            {
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<DATA>");
                strBuildXML.Append("<pv_ACCOUNTNO>" + pv_ACCOUNTNO + "</pv_ACCOUNTNO>");
                strBuildXML.Append("<pv_BEN_ACCOUNTNO>" + pv_BEN_ACCOUNTNO + "</pv_BEN_ACCOUNTNO>");
                strBuildXML.Append("<AMOUNT>" + AMOUNT + "</AMOUNT>");
                strBuildXML.Append("<CCY>" + CCY + "</CCY>");
                strBuildXML.Append("<REMARK><![CDATA[" + REMARK + "]]></REMARK>");
                strBuildXML.Append("<TransactionDate>" + TransactionDate + "</TransactionDate>");
                strBuildXML.Append("<BEN_NAME><![CDATA[" + BEN_NAME + "]]></BEN_NAME>");
                strBuildXML.Append("<BEN_CITY>" + BEN_CITY + "</BEN_CITY>");
                strBuildXML.Append("<BEN_ADD1><![CDATA[" + BEN_ADD1 + "]]></BEN_ADD1>");
                strBuildXML.Append("<BEN_ADD2><![CDATA[" + BEN_ADD2 + "]]></BEN_ADD2>");
                strBuildXML.Append("<BANK_BEN_DESC><![CDATA[" + BANK_BEN_DESC + "]]></BANK_BEN_DESC>");
                strBuildXML.Append("<BR_BEN_DESC><![CDATA[" + BR_BEN_DESC + "]]></BR_BEN_DESC>");
                strBuildXML.Append("<CITY_BEN_DESC><![CDATA[" + CITY_BEN_DESC + "]]></CITY_BEN_DESC>");
                strBuildXML.Append("<BANK_BEN_CODE>" + BANK_BEN_CODE + "</BANK_BEN_CODE>");
                strBuildXML.Append("<BR_BEN_CODE>" + BR_BEN_CODE + "</BR_BEN_CODE>");
                strBuildXML.Append("<CITY_BEN_CODE>" + CITY_BEN_CODE + "</CITY_BEN_CODE>");
                strBuildXML.Append("<KenhHT>" + KenhHT + "</KenhHT>");
                strBuildXML.Append("<v_strSo_CT>" + v_strSo_CT + "</v_strSo_CT>");
                strBuildXML.Append("<strMa_NH_B>" + strMa_NH_B + "</strMa_NH_B>");
                strBuildXML.Append("<v_strMa_NV>" + v_strMa_NV + "</v_strMa_NV>");
                strBuildXML.Append("<v_str_KS>" + v_str_KS + "</v_str_KS>");
                strBuildXML.Append("<v_KenhCT>" + v_KenhCT + "</v_KenhCT>");
                strBuildXML.Append("<customer_info>" + customer_info + "</customer_info>");
                strBuildXML.Append("<TaxPymtDate>" + TaxPymtDate + "</TaxPymtDate>");
                strBuildXML.Append("<TaxType>" + TaxType + "</TaxType>");
                strBuildXML.Append("<TaxCode>" + TaxCode + "</TaxCode>");
                strBuildXML.Append("<v_MaCQT>" + v_MaCQT + "</v_MaCQT>");
                strBuildXML.Append("<v_TenCQT><![CDATA[" + v_TenCQT + "]]></v_TenCQT>");
                //strBuildXML.Append("<v_MaDBHC>" + v_MaDBHC + "</v_MaDBHC>");
                //strBuildXML.Append("<v_TenDBHC><![CDATA[" + v_TenDBHC + "]]></v_TenDBHC>");
                strBuildXML.Append("<TaxPayerName><![CDATA[" + TaxPayerName + "]]></TaxPayerName>");
                strBuildXML.Append("<TaxPayerAdd>" + TaxPayerAdd + "</TaxPayerAdd>");
                strBuildXML.Append("<EcomStr>" + EcomStr + "</EcomStr>");
                strBuildXML.Append("</DATA>");
                strBuildXML.Append("</Customs>");


                if (Core_status == "1")
                {

                    CoreESB a = new CoreESB();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    msgRes = a.WSProcess(strBuildXML.ToString(), "NTDT");
                    System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    VXDOC.LoadXml(msgRes);
                    v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;
                    v_str_ref_no = VXDOC.SelectSingleNode("Customs/Ref_no").InnerText;

                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }
            }
            catch (Exception ex)
            {
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }

            return msgRes;
        }

        public static string GetSOBL(string pv_strSOBL)
        {
            string strRetrun = string.Empty;
            using (OracleConnection objCon = DataAccess.GetConnection())
            {
                using (OracleCommand objCom = new OracleCommand())
                {

                    try
                    {
                        //objCon.Open();
                        if (Core_status == "1")
                        {
                            //objCom.ExecuteNonQuery();
                            OracleDataAdapter da = new OracleDataAdapter();
                            OracleCommand cmd = new OracleCommand();
                            cmd.Connection = objCon;
                            cmd.CommandText = "tcs_pck_util_shb.GET_GUARANTEE_DTLS";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("mdNO", OracleType.VarChar).Value = pv_strSOBL;
                            cmd.Parameters.Add("aoutcursordata", OracleType.Cursor).Direction = ParameterDirection.Output;

                            da.SelectCommand = cmd;
                            cmd.ExecuteNonQuery();
                            DataTable dt = new DataTable();
                            da.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                strRetrun = ("<PARAMETER><RETCODE>0</RETCODE><NGAYLAP>"
                            + (dt.Rows[0]["Create_Date"].ToString() + ("</NGAYLAP>" + ("<NGAYHL>"
                            + (dt.Rows[0]["Approve_Date"].ToString() + ("</NGAYHL><NGAYHH>"
                            + (dt.Rows[0]["Expire_Date"].ToString() + ("</NGAYHH>" + ("<DIENGIAI>"
                            + (dt.Rows[0]["Description"].ToString() + ("</DIENGIAI><AMOUNT>"
                            + (dt.Rows[0]["AMOUNT"].ToString() + ("</AMOUNT><MST>"
                            + (dt.Rows[0]["MST"].ToString() + ("</MST><CIFID>"
                            + (dt.Rows[0]["CIF"].ToString() + ("</CIFID></PARAMETER>")))))))))))))))));
                            }
                        }
                        else
                        {
                            strRetrun = "<PARAMETER><RETCODE>999</RETCODE></PARAMETER>";
                        }

                    }
                    catch
                    {
                        strRetrun = "<PARAMETER><RETCODE>999</RETCODE></PARAMETER>";
                    }
                    finally
                    {
                        objCon.Close();
                    }

                }
            }


            return strRetrun;

        }

        public static DataTable GetFeeVat(string pv_strLPSF)
        {
            DataTable dt = new DataTable();
            using (OracleConnection objCon = DataAccess.GetConnection())
            {
                using (OracleCommand objCom = new OracleCommand())
                {

                    try
                    {
                        //objCon.Open();
                        if (Core_status == "1")
                        {
                            //objCom.ExecuteNonQuery();
                            OracleDataAdapter da = new OracleDataAdapter();
                            OracleCommand cmd = new OracleCommand();
                            cmd.Connection = objCon;
                            cmd.CommandText = "tcs_pck_util_shb.GET_CHARGES";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("mdNO", OracleType.VarChar).Value = pv_strLPSF;
                            cmd.Parameters.Add("aoutcursordata", OracleType.Cursor).Direction = ParameterDirection.Output;

                            da.SelectCommand = cmd;
                            cmd.ExecuteNonQuery();

                            da.Fill(dt);
                        }


                    }
                    catch
                    {

                    }
                    finally
                    {
                        objCon.Close();
                    }

                }
            }


            return dt;

        }
        public string Get_VALUE_DATE()
        {
            string Value_date = "";
            try
            {

                DataSet ds = null;

                string sbsql = "select tcs_pck_util_shb.fnc_get_val_date('SHB') value_date from dual";
                try
                {
                    if (Core_status == "1")
                    {
                        ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text);
                        Value_date = ds.Tables[0].Rows[0]["value_date"].ToString();
                    }
                    else
                    {
                        Value_date = DateTime.Now.ToString("yyyyMMdd");
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogDebug.WriteLog("Loi khi lay Core date" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                    return "Khong lay duoc VAL DATE";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogDebug.WriteLog("Loi khi lay Core date" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
            return Value_date;

        }
        public static string GetTen_TK_KH_NH(string pv_strMST)
        {
            string strRetrun = string.Empty;
            using (OracleConnection objCon = DataAccess.GetConnection())
            {
                using (OracleCommand objCom = new OracleCommand())
                {
                    DataSet ds = null;
                    log.Error("0-Lay ten tai khoan connect: " + VBOracleLib.DataAccess.strConnection);
                    string sbsql = "SELECT legacy_ac, ccy_cd, ac_name, pos_cd,STATUS FROM table (tcs_pck_util_shb.get_ups('" + pv_strMST + "','1','A'))";
                    log.Error("1-Lay ten tai khoan query: " + sbsql);
                    try
                    {
                        if (Core_status == "1")
                        {
                            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql, CommandType.Text);
                            if ((ds != null) && ds.Tables.Count > 0)
                            {
                                strRetrun = ds.Tables[0].Rows[0]["ac_name"].ToString();
                            }
                        }
                        else
                        {
                            strRetrun = "TK TEST CORE OFF";
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("3-Lay ten tai khoan lỗi: " + ex.Message);
                        return "Khong lay duoc Ten TK";
                    }
                    finally
                    {
                        objCon.Close();
                    }

                }
            }
            return strRetrun;
        }
        public static Int64 TCS_GetSequence(string p_Seq)
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT ");
                sbSQL.Append(p_Seq);
                sbSQL.Append(".nextval FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                return Int64.Parse(strRet);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }


        public string DongBoNgayNghi()
        {
            string msgRes = "";
            string strReturn = "";

            string requestId = TCS_GetSequence("seq_dongbo_ngay_nghi").ToString();
            CoreESB a = new CoreESB();
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            if (Core_status == "1")
            {
                msgRes = a.WSProcess(requestId, "NGAYNGHI");
            }
            else
            {
                msgRes = "<PARAMETER><errorCode>0</errorCode><errorDesc>Successfull</errorDesc><listHolidayDt><element><holiDesc>w</holiDesc><holiDt>2020-12-06T00:00:00+07:00</holiDt></element><element><holiDesc>w</holiDesc><holiDt>2020-12-13T00:00:00+07:00</holiDt></element><element><holiDesc>w</holiDesc><holiDt>2020-12-20T00:00:00+07:00</holiDt></element><element><holiDesc>holiday</holiDesc><holiDt>2020-12-27T00:00:00+07:00</holiDt></element></listHolidayDt></PARAMETER>";
            }

            try
            {
                if (msgRes.Length > 0)
                {
                    CorebankServiceESB.MSGNN.MSGNN msgNN = new CorebankServiceESB.MSGNN.MSGNN();

                    msgRes = msgRes.Replace("<PARAMETER>", strXMLdefin2);
                    CorebankServiceESB.MSGNN.MSGNN objTemp = msgNN.MSGToObject(msgRes);
                    if (objTemp.errorCode.Equals("0"))
                    {
                        InsertNgayNghi(objTemp);
                        return "Đồng bộ danh mục thành công";
                    }
                    else
                    {
                        return "Đồng bộ danh mục không thành công";
                    }
                }

            }
            catch (Exception exx)
            {
                return exx.Message;
            }

            return strReturn;

        }
        public void InsertNgayNghi(CorebankServiceESB.MSGNN.MSGNN msgNN)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO TCS_DM_NgayNghi (");
            sbSqlInsert.Append("ngay_nghi,");
            sbSqlInsert.Append("ghi_chu)");
            sbSqlInsert.Append(" VALUES(:ngay_nghi,");
            sbSqlInsert.Append(":ghi_chu)");
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();

                //msgNN.listHolidayDt = new MSGNN.listHolidayDt();
                //msgNN.listHolidayDt.element = new List<MSGNN.element>();


                if (msgNN.listHolidayDt.element == null)
                {
                    throw new Exception("Lỗi đồng bộ danh mục ngày nghỉ");
                }

                //Lay thong tin danh muc ngay nghi dua vao CSDL
                if ((msgNN.listHolidayDt.element != null))
                {
                    //Xoa du lieu truoc
                    StringBuilder sbSqlDelete = new StringBuilder("");
                    sbSqlDelete.Append("DELETE FROM TCS_DM_NgayNghi");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    VBOracleLib.DataAccess.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    //insert du lieu moi
                    for (int i = 0; i < msgNN.listHolidayDt.element.Count; i++)
                    {
                        string holiDesc = msgNN.listHolidayDt.element[i].holiDesc;
                        string holiDt = msgNN.listHolidayDt.element[i].holiDt;

                        holiDt = holiDt.Substring(0, 10);
                        DateTime oDate = DateTime.ParseExact(holiDt, "yyyy-MM-dd", null);

                        p = new IDbDataParameter[2];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "ngay_nghi";
                        p[0].DbType = DbType.Date;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = oDate;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "ghi_chu";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = holiDesc;
                        VBOracleLib.DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    }
                    transCT.Commit();
                }
                else
                {
                    transCT.Rollback();
                }

            }
            catch (Exception ex)
            {
                transCT.Rollback();
                throw ex;
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }


        }

        public string GetDienCITAD()
        {
            string msgRes = "";
            string strReturn = "";

            string ngay_lv = TCS_GetNgayLV();
            string nam = "";
            string thang = "";
            if (ngay_lv.Length != 8)
            {
                ngay_lv = DateTime.Now.ToString("yyyyMMdd");
            }

            nam = ngay_lv.Substring(0, 2);
            thang = ngay_lv.Substring(4, 2);


            string requestId = TCS_GetSequence("seq_get_dien_nganhang_khac").ToString();

            requestId = nam + thang + requestId.PadLeft(10, '0');

            CoreESB a = new CoreESB();
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            msgRes = a.WSProcess(requestId, "CITAD");

            return strReturn;

        }
        private string TCS_GetNgayLV()
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV'";
                return (ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString())).ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private long ConvertDateToNumber(string strDate)
        {
            //-----------------------------------------------------
            // Mục đích: Chuyển một string ngày tháng 'dd/MM/yyyy' --> dạng số 'yyyyMMdd'.
            // Tham số: string đầu vào
            // Giá trị trả về: 
            // Ngày viết: 18/10/2007
            // Người viết: Lê Hồng Hà
            // ----------------------------------------------------
            string[] arr = null;

            try
            {
                string sep = "/";
                strDate = strDate.Trim();
                if (strDate.Length < 10)
                    return 0;
                arr = strDate.Split(sep.ToCharArray());
                sep = "/";
                arr[0] = arr[0].PadLeft(2, '0');
                arr[1] = arr[1].PadLeft(2, '0');
                if (arr[2].Length == 2)
                {
                    arr[2] = "20" + arr[2];
                }

            }
            catch (Exception ex)
            {
                return Convert.ToInt64(DateTime.Now.ToString("yyyyMMdd"));
            }

            return long.Parse(arr[2] + arr[1] + arr[0]);
        }

        //Send content_ex to corebank
        public string SendContentEx(string v_strSo_CT, string v_Ref, string v_KenhCT, string v_ContentEx, ref  string v_str_errcode)
        {
            string msgRes = "";
            try
            {
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<DATA>");
                strBuildXML.Append("<v_strSo_CT>" + v_strSo_CT + "</v_strSo_CT>");
                strBuildXML.Append("<v_Ref>" + v_Ref + "</v_Ref>");
                strBuildXML.Append("<v_ContentEx><![CDATA[" + v_ContentEx + "]]></v_ContentEx>");
                strBuildXML.Append("<v_KenhCT>" + v_KenhCT + "</v_KenhCT>");
                strBuildXML.Append("</DATA>");
                strBuildXML.Append("</Customs>");
                if (Core_status == "1")
                {
                    CoreESB a = new CoreESB();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    //LogDebug.WriteLog("MSG_IN_WEB:" + strBuildXML.ToString(), EventLogEntryType.Error);
                    msgRes = a.WSProcess(strBuildXML.ToString(), "CONTENTEX");
                    //LogDebug.WriteLog("MSG_OUT_WEB:" + msgRes, EventLogEntryType.Error);
                    System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                    VXDOC.LoadXml(msgRes);
                    v_str_errcode = VXDOC.SelectSingleNode("Customs/Errcode").InnerText;

                }
                else
                {
                    v_str_errcode = "00000";
                }
            }
            catch (Exception ex)
            {
                v_str_errcode = "11111";
            }
            return msgRes.ToString();
        }
    }

}
