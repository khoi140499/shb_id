﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Xml;
using Business;

namespace CorebankServiceESB
{
    public class JsonUtils
    {
        public static string ToJSONNgayNghi(string fromDate, string requestId )
        {
            StringBuilder sb = new StringBuilder();
            JsonWriter jw = new JsonTextWriter(new StringWriter(sb));
            jw.Formatting = Newtonsoft.Json.Formatting.Indented;
            jw.WriteStartObject();

            jw.WritePropertyName("fromDate");
            jw.WriteValue(fromDate);
            jw.WritePropertyName("requestId");
            jw.WriteValue(requestId);

            jw.WriteEndObject();
            return sb.ToString();
        }
        public static Business.NgayNghi.infNgayNghi JsonToObjectNgayNghi(string content)
        {
           
            Business.NgayNghi.infNgayNghi objResponse = new Business.NgayNghi.infNgayNghi();

            // Get data from obj
            objResponse.listHolidayDt = new Business.NgayNghi.listHolidayDt();
            
            objResponse.listHolidayDt.element = new List<Business.NgayNghi.element>();

            objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Business.NgayNghi.infNgayNghi>(content);

            return objResponse;
        }


    }



}
