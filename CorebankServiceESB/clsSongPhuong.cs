﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VBOracleLib;

namespace CorebankServiceESB
{
    public class clsSongPhuong
    {
        private static String strXMLdefout1 = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
        private static String strXMLdefout2 = " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\"";
        private static String strXMLdefout3 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        private static String strXMLdefout4 = "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.cpandl.com\"";
        private static String strSender_Name = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString();
        private static String strSender_Code = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static String Core_status = ConfigurationManager.AppSettings.Get("CORE.ON").ToString();
        public clsSongPhuong()
        {

        }

        public string ValiadSHB(string pv_strSHKB)
        {
            //if (Core_status == "0")
            //{
            //    return "01348005;2000588888;111600;0987364526";
            //}
            //0011
            string strNH_SHB = "";
            string ID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_songphuong.NextVal");
            insertLogSP(ID, "VALIDATE", pv_strSHKB, "", "", "", pv_strSHKB);
            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();
            sbsql.Append("SELECT VALIDDATACHUNGTU.fnc_valid_Nop_NH(:SHKB,NULL,NULL,NULL,NULL) FROM DUAL");
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SHKB";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = pv_strSHKB;

            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            if (ds.Tables[0].Rows.Count <= 0)
            {
                throw new Exception("Không lấy được thông tin kho bạc từ song phương!");
            }
            strNH_SHB = ds.Tables[0].Rows[0][0].ToString();
            if (strNH_SHB.Equals("0"))
            {
                strNH_SHB = "FALSE";
            }
            updateLogSP(ID, strNH_SHB);
            return strNH_SHB;
        }
        public string ValiadSHBBienLai(string pv_strSHKB)
        {

            //0011

            string strNH_SHB = "";
            string ID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_songphuong.NextVal");
            insertLogSP(ID, "VALIDATE", pv_strSHKB, "", "", "", pv_strSHKB);
            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();
            sbsql.Append("SELECT VALIDDATACHUNGTU.fnc_rt_citad_NH_KB(:SHKB) FROM DUAL");
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SHKB";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = pv_strSHKB;

            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            if (ds.Tables[0].Rows.Count <= 0)
            {
                throw new Exception("Không lấy được thông tin kho bạc từ song phương!");
            }
            strNH_SHB = ds.Tables[0].Rows[0][0].ToString();
            if (strNH_SHB.Equals("0"))
            {
                strNH_SHB = "";
            }
            updateLogSP(ID, strNH_SHB);
            return strNH_SHB;
        }
        //public string ValidMSB_SP_BIENLAI(string pv_strSHKB)
        //{
        //    //0011
        //    string strNH_SHB = "";
        //    string ID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_songphuong.NextVal");
        //    insertLogSP(ID, "VALIDATE_SO_BL", pv_strSHKB, "", "", "", pv_strSHKB);
        //    DataSet ds = null;
        //    StringBuilder sbsql = new StringBuilder();
        //    sbsql.Append("SELECT VALIDDATACHUNGTU.fnc_rt_so_bl(:SHKB) FROM DUAL");
        //    IDbDataParameter[] p = null;
        //    p = new IDbDataParameter[1];
        //    p[0] = new OracleParameter();
        //    p[0].ParameterName = "SHKB";
        //    p[0].DbType = DbType.String;
        //    p[0].Direction = ParameterDirection.Input;
        //    p[0].Value = pv_strSHKB;

        //    ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

        //    if (ds.Tables[0].Rows.Count <= 0)
        //    {
        //        throw new Exception("Không lấy được thông tin kho bạc từ song phương!");
        //    }
        //    strNH_SHB = ds.Tables[0].Rows[0][0].ToString();
        //    if (strNH_SHB.Equals("0"))
        //    {
        //      return  strNH_SHB = "";
        //    }
        //    // SHKB 0; SO_BL 1; KYHIEU_BL 2; CITAD_CN 3; TK_KB 4; MA_CN 5; TK_TG 6;
        //    updateLogSP(ID, strNH_SHB);
        //    return strNH_SHB;
        //}
        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="so_ct"></param>
        /// <param name="rm_ref_no"></param>
        /// <param name="type"></param>
        public string SendChungTuSP(string so_ct, string type)
        {
            string REF_ID = "";
            string SO_TAI_KHOAN = "";
            string TEN_NNOP = "";
            string SO_REF_CORE = "";
            string SO_TIEN = "";
            string MA_NT = "";
            string SHKB = "";
            string MA_NH_PHANH = "";
            string TEN_NH_PHANH = "";
            string CONTEN_EX = "";
            string PAYMENT_DETAIL = "";
            string SOURCE_V = "";
            string CITAD_TYPE = "";
            string TRANG_THAI = "";
            string MSG = "";
            //Lấy thông tin từ hdr để lấy thông tin
            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();
            if (type.Equals("TQ"))
            {
                sbsql.Append("SELECT tk_kh_nh,ten_nnthue, so_ct_nh,ttien,ma_nt,shkb,ma_nh_a,remarks FROM TCS_CTU_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI='01' ");
            }
            else if (type.Equals("NTDT"))
            {
                sbsql.Append("SELECT  stk_nhang_nop as tk_kh_nh,ten_nnop as ten_nnthue, so_ct_nh,tong_tien ttien,ma_nguyente ma_nt,ma_hieu_kbac shkb,'' ma_nh_a,remarks FROM TCS_CTU_NTDT_HDR WHERE SO_CTU =:SO_CT AND tthai_ctu='11' ");
            }
            else if (type.Equals("HQ247"))
            {
                sbsql.Append("SELECT a.taikhoan_th tk_kh_nh,a.ten_dv ten_nnthue, a.so_ct_nh so_ct_nh, a.hq_sotien_to ttien, a.ma_nt, hdr.shkb, hdr.ma_nh_a,a.dien_giai remarks ");
                sbsql.Append(" FROM tcs_hq247_304 a, TCS_CTU_NTDT_HQ247_HDR hdr WHERE a.so_ctu = hdr.so_ct AND a.trangthai in('02','03','04', '09','10') AND a.SO_CTU =:SO_CT");
            }
            else if (type.Equals("NHOTHU"))
            {
                sbsql.Append("SELECT a.taikhoan_th tk_kh_nh,a.ten_dv ten_nnthue, a.so_ct_nh so_ct_nh, a.hq_sotien_to ttien, a.ma_nt, hdr.shkb, hdr.ma_nh_a,hdr.remarks remarks ");
                sbsql.Append(" FROM TCS_HQ247_201 a, TCS_CTU_HQ_NHOTHU_HDR hdr WHERE a.so_ctu = hdr.so_ct AND a.trangthai in('02','03','04', '09','10') AND a.SO_CTU =:SO_CT");
            }
            else if (type.Equals("MOBILE"))
            {
                sbsql.Append("SELECT tk_kh_nh,ten_nnthue,so_ct_nh,ttien,ma_nt,shkb,ma_nh_a,remarks ");
                sbsql.Append("  from tcs_ctu_hdr_mobitct where so_ct =:SO_CT and trang_thai in ('07','01')");

            }
            else if (type.Equals("CITAD"))
            {
                sbsql.Append("select  hdr.tk_kh_nh tk_kh_nh,hdr.ten_nnthue ten_nnthue, hdr.REF_NO as so_ct_nh, hdr.ttien ttien, hdr.ma_nt, hdr.shkb, hdr.nh_giu_tknnt as ma_nh_a,hdr.remark remarks ");
                sbsql.Append(" from  tcs_ctu_songphuong_hdr hdr where  hdr.trang_thai='03' and hdr.so_ct =:SO_CT");
            }
            else if (type.Equals("CITAD_02"))
            {
                sbsql.Append("select  hdr.tk_kh_nh tk_kh_nh,hdr.ten_nnthue ten_nnthue, hdr.REF_NO as so_ct_nh, hdr.ttien ttien, hdr.ma_nt, hdr.shkb, hdr.nh_giu_tknnt as ma_nh_a,hdr.remark remarks ");
                sbsql.Append(" from  tcs_ctu_songphuong_hdr hdr where  hdr.trang_thai='02' and hdr.so_ct =:SO_CT");
            }

            if (type.Equals("CITAD") || type.Equals("CITAD_02"))
            {
                SOURCE_V = "CITAD";
            }
            else
            {
                SOURCE_V = "SHB";
            }

            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SO_CT";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = so_ct;

            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            if (ds.Tables[0].Rows.Count > 0)
            {
                REF_ID = so_ct;
                SO_TAI_KHOAN = ds.Tables[0].Rows[0]["tk_kh_nh"].ToString();
                TEN_NNOP = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                SO_REF_CORE = ds.Tables[0].Rows[0]["so_ct_nh"].ToString();
                SO_TIEN = ds.Tables[0].Rows[0]["ttien"].ToString();
                MA_NT = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                SHKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                MA_NH_PHANH = ds.Tables[0].Rows[0]["ma_nh_a"].ToString();
                if (MA_NH_PHANH.Length <= 0)
                {
                    MA_NH_PHANH = strSender_Code;
                }
                TEN_NH_PHANH = strSender_Name;
                CONTEN_EX = "";
                PAYMENT_DETAIL = ds.Tables[0].Rows[0]["remarks"].ToString();
                TRANG_THAI = "00";


            }
            else
            {
                throw new Exception("Lỗi trong quá trình lấy thông tin chứng từ hdr");

            }

            MSG = getMSG063(so_ct, type);

            //MSG = "XXXX";
            if (SO_REF_CORE.Length <= 0)
            {
                SO_REF_CORE = "";
            }
            string ID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_songphuong.NextVal");
            insertLogSP(ID, "CHUNGTU", SHKB, SO_REF_CORE, so_ct, "", MSG);

            string result = "";
            //if (Core_status == "1")
            //{
                OracleConnection conn = GetConnection();
                String strS = "SELECT INSERTDATAFROMSHB.proc_insert_datashb(:REF_ID,:SO_TAI_KHOAN,:TEN_NNOP,:SO_REF_CORE,:SO_TIEN,:MA_NT,:SHKB,:MA_NH_PHANH,:TEN_NH_PHANH,:CONTEN_EX,:PAYMENT_DETAIL,:SOURCE_V,:CITAD_TYPE,:TRANG_THAI,:MSG,sysdate) FROM DUAL";
                var cmd = new OracleCommand("strS", conn);
                //cmd.CommandType = CommandType.Text;
                cmd.CommandText = strS;
                var xmlParam = new OracleParameter("MSG", OracleType.Clob);
                cmd.Parameters.Add(xmlParam);

                // DO NOT assign the parameter value yet in this place

                cmd.Transaction = conn.BeginTransaction();
                try
                {
                    // Assign value here, AFTER starting the TX
                    xmlParam.Value = MSG;
                    cmd.Parameters.Add("REF_ID", OracleType.NVarChar).Value = CheckEmty(REF_ID);
                    cmd.Parameters.Add("SO_TAI_KHOAN", OracleType.NVarChar).Value = CheckEmty(SO_TAI_KHOAN);
                    cmd.Parameters.Add("TEN_NNOP", OracleType.NVarChar).Value = CheckEmty(TEN_NNOP);
                    cmd.Parameters.Add("SO_REF_CORE", OracleType.NVarChar).Value = SO_REF_CORE;
                    cmd.Parameters.Add("SO_TIEN", OracleType.NVarChar).Value = CheckEmty(SO_TIEN);
                    cmd.Parameters.Add("MA_NT", OracleType.NVarChar).Value = CheckEmty(MA_NT);
                    cmd.Parameters.Add("SHKB", OracleType.NVarChar).Value = CheckEmty(SHKB);
                    cmd.Parameters.Add("MA_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(MA_NH_PHANH);
                    cmd.Parameters.Add("TEN_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(TEN_NH_PHANH);
                    cmd.Parameters.Add("CONTEN_EX", OracleType.NVarChar).Value = CheckEmty(CONTEN_EX);
                    cmd.Parameters.Add("PAYMENT_DETAIL", OracleType.NVarChar).Value = CheckEmty(PAYMENT_DETAIL);
                    cmd.Parameters.Add("SOURCE_V", OracleType.NVarChar).Value = CheckEmty(SOURCE_V);
                    cmd.Parameters.Add("CITAD_TYPE", OracleType.NVarChar).Value = CheckEmty(CITAD_TYPE);
                    cmd.Parameters.Add("TRANG_THAI", OracleType.NVarChar).Value = CheckEmty(TRANG_THAI);
                    result = (String)cmd.ExecuteScalar();
                    cmd.Transaction.Commit();
                }
                catch (OracleException)
                {
                    cmd.Transaction.Rollback();
                    result = "FALSE";
                    return result;
                }
                finally
                {
                    if ((conn != null) || conn.State != ConnectionState.Closed)
                    {
                        conn.Dispose();
                        conn.Close();
                    }
                }
            //}
            //else
            //{
            //    //off TTSP
            //    result = "SUCCESS";
            //}
           

            updateLogSP(ID, result);
            if (result.ToUpper().Equals("SUCCESS"))
            {
                string strSQL = "";
                if (type.Equals("TQ"))
                {
                    strSQL = "UPDATE tcs_ctu_hdr SET TT_SP='2' WHERE so_ct='" + so_ct + "' ";
                }
                else if (type.Equals("NTDT"))
                {
                    strSQL = "UPDATE tcs_ctu_ntdt_hdr SET TT_SP='2' WHERE so_ctu='" + so_ct + "' ";
                }
                else if (type.Equals("HQ247"))
                {
                    strSQL = "UPDATE tcs_hq247_304 SET TT_SP='2' WHERE so_ctu='" + so_ct + "' ";
                }
                else if (type.Equals("NHOTHU"))
                {
                    strSQL = "UPDATE TCS_HQ247_201 SET TT_SP='2' WHERE so_ctu='" + so_ct + "' ";
                }
                else if (type.Equals("CITAD") || type.Equals("CITAD_02"))
                {
                    strSQL = "UPDATE tcs_chungtu_ttsp SET TT_SP='1' WHERE so_ct='" + so_ct + "' ";
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);

                    strSQL = "UPDATE tcs_ctu_songphuong_hdr SET TT_SP='1' WHERE so_ct='" + so_ct + "' ";
                }
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            else
            {
                string strSQL = "";
                if (type.Equals("CITAD") || type.Equals("CITAD_02"))
                {
                    strSQL = "UPDATE tcs_ctu_songphuong_hdr SET TT_SP='0' WHERE so_ct='" + so_ct + "' ";
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                }

            }

            return result;
        }
        public string getMSG063(string so_ct, string type)
        {
            string table = "";
            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();
            if (type.Equals("TQ"))
            {
                table = "tcs_ctu_dtl";
                sbsql.Append("SELECT shkb,ngay_kb, so_ct_nh,to_char(ngay_kh_nh,'DD-MM-YYYY HH24:MI:SS') as ngay_kh_nh,tk_no,tk_co,ma_nnthue,ten_nnthue,dc_nnthue,ma_xa as DBHC,");
                sbsql.Append("ma_nntien,ten_nntien,dc_nntien,'0001' MA_NV, SO_BT,'01' as MA_DTHU, KYHIEU_CT,MA_HQ,MA_HQ_PH,pt_tt,");
                sbsql.Append("ma_lthue,ma_cqthu,tk_kh_nh,ma_nh_a,ma_nh_b,ma_nt,ty_gia,ttien,ttien_nt,ten_hq,ten_hq_ph,'' ten_cqthu,");
                sbsql.Append("TENHUYEN_NNT as QUAN_NNTHUE,TENTINH_NNT as TINH_NNTHUE,TENHUYEN_NNTHAY as QUAN_NNTIEN,TENTINH_NNTHAY as TINH_NNTIEN,remarks, dac_diem_ptien,so_khung, so_may ");
                sbsql.Append("  FROM TCS_CTU_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI='01'");
            }
            else if (type.Equals("NTDT"))
            {
                table = "tcs_ctu_ntdt_dtl";
                // tuanda - 04/03/24
                sbsql.Append("SELECT ma_thamchieu, ma_hieu_kbac as shkb,ngay_kb, so_ct_nh,to_char(ngay_nhang,'DD-MM-YYYY HH24:MI:SS') as ngay_kh_nh,'' tk_no, stk_thu tk_co, mst_nnop ma_nnthue,ten_nnop ten_nnthue,diachi_nnop dc_nnthue,ma_dbhc_thu as DBHC,");
                sbsql.Append("mst_nthay as ma_nntien,ten_nthay as ten_nntien,diachi_nthay as dc_nntien,'0001' as MA_NV,'' SO_BT,'01' as MA_DTHU,mahieu_ctu as KYHIEU_CT,'' MA_HQ,'' MA_HQ_PH,'01'as pt_tt,");
                sbsql.Append("ma_lthue,ma_cqthu,stk_nhang_nop tk_kh_nh,'' ma_nh_a,ma_nhang_unt ma_nh_b,ma_nguyente ma_nt,'' ty_gia,tong_tien ttien,tong_tien ttien_nt,'' ten_hq,'' ten_hq_ph , ten_cqthu,");
                sbsql.Append("TEN_HUYEN_NNOP as QUAN_NNTHUE,TEN_TINH_NNOP as TINH_NNTHUE,TEN_HUYEN_NTHAY as QUAN_NNTIEN,TEN_TINH_NTHAY as TINH_NNTIEN,TEN_TINH_KBAC,remarks");
                sbsql.Append(" FROM TCS_CTU_NTDT_HDR WHERE SO_CTU =:SO_CT AND tthai_ctu='11'");
            }
            else if (type.Equals("HQ247"))
            {
                table = "tcs_ctu_ntdt_hq247_dtl";
                sbsql.Append("SELECT hdr.shkb,hdr.ngay_kb,a.so_ct_nh,to_char(hdr.ngay_kh_nh,'DD-MM-YYYY HH24:MI:SS') as ngay_kh_nh,'' tk_no,hdr.tk_co,a.ma_dv ma_nnthue, a.ten_dv ten_nnthue, hdr.dc_nnthue dc_nnthue,'' AS dbhc, ");
                sbsql.Append("hdr.ma_lthue,hdr.ma_cqthu,a.taikhoan_th tk_kh_nh, hdr.ma_nh_a, hdr.ma_nh_b,hdr.ma_nt,hdr.ty_gia,hdr.ttien,hdr.ttien_nt, ");
                sbsql.Append("hdr.ma_nntien,hdr.ten_nntien,hdr.dc_nntien,'0001' as MA_NV, hdr.SO_BT,'01' as MA_DTHU, hdr.KYHIEU_CT,hdr.MA_HQ,hdr.MA_HQ_PH,'01' as pt_tt,");
                sbsql.Append("hdr.ten_hq,hdr.ten_hq_ph,hdr.ten_cqthu,'' as QUAN_NNTHUE,'' as TINH_NNTHUE,'' as QUAN_NNTIEN,'' as TINH_NNTIEN,a.dien_giai as remarks");
                sbsql.Append("  FROM tcs_hq247_304 a, TCS_CTU_NTDT_HQ247_HDR hdr ");
                sbsql.Append("WHERE a.so_ctu = hdr.so_ct AND a.trangthai in('02','03','04', '09','10') AND a.SO_CTU =:SO_CT");
            }
            else if (type.Equals("NHOTHU"))
            {
                table = "TCS_CTU_HQ_NHOTHU_DTL";
                sbsql.Append("SELECT hdr.shkb,hdr.ngay_kb,a.so_ct_nh,to_char(hdr.ngay_kh_nh,'DD-MM-YYYY HH24:MI:SS') as ngay_kh_nh,'' tk_no,hdr.tk_co,a.ma_dv ma_nnthue, a.ten_dv ten_nnthue, hdr.dc_nnthue dc_nnthue,'' AS dbhc, ");
                sbsql.Append("hdr.ma_lthue,hdr.ma_cqthu,a.taikhoan_th tk_kh_nh, hdr.ma_nh_a, hdr.ma_nh_b,hdr.ma_nt,hdr.ty_gia,hdr.ttien,hdr.ttien_nt, ");
                sbsql.Append("hdr.ma_nntien,hdr.ten_nntien,hdr.dc_nntien,'0001' as MA_NV, hdr.SO_BT,'01' as MA_DTHU, hdr.KYHIEU_CT,hdr.MA_HQ,hdr.MA_HQ_PH,'01' as pt_tt,");
                sbsql.Append("hdr.ten_hq,hdr.ten_hq_ph,hdr.ten_cqthu,'' as QUAN_NNTHUE,'' as TINH_NNTHUE,'' as QUAN_NNTIEN,'' as TINH_NNTIEN,hdr.remarks");
                sbsql.Append(" FROM TCS_HQ247_201 a, TCS_CTU_HQ_NHOTHU_HDR hdr ");
                sbsql.Append("WHERE a.so_ctu = hdr.so_ct AND a.trangthai in('02','03','04', '09','10') AND a.SO_CTU =:SO_CT");
            }
            else if (type.Equals("MOBILE"))
            {
                table = "tcs_ctu_dtl_mobitct";
                sbsql.Append("SELECT shkb,ngay_kb, so_ct_nh,to_char(tg_ky,'DD-MM-YYYY HH24:MI:SS') as ngay_kh_nh,to_char(tg_ky,  'DD-MM-YYYY HH24:MI:SS') as tg_ky,tk_no,tk_co,ma_nnthue,ten_nnthue,");
                sbsql.Append("dc_nnthue,ma_xa as DBHC, ma_nntien,ten_nntien,dc_nntien,MA_NV,SO_BT,MA_DTHU,KYHIEU_CT,'' MA_HQ,'' MA_HQ_PH,'01' pt_tt,");
                sbsql.Append("'01' ma_lthue, ma_cqthu,tk_kh_nh,ma_nh_a,ma_nh_b,ma_nt,ty_gia,ttien,ttien_nt,'' ten_hq,'' ten_hq_ph,ten_cqthu,");
                sbsql.Append("'' as QUAN_NNTHUE,'' as TINH_NNTHUE,'' as QUAN_NNTIEN,'' as TINH_NNTIEN,remarks,MA_CN ");
                sbsql.Append("  from tcs_ctu_hdr_mobitct where so_ct =:SO_CT and trang_thai in ('07','01')");

            }
            else if (type.Equals("CITAD"))
            {
                table = "tcs_ctu_songphuong_dtl";
                sbsql.Append("SELECT hdr.shkb,hdr.ngay_kb,hdr.REF_NO as so_ct_nh,to_char(hdr.ngay_kh_nh,'DD-MM-YYYY HH24:MI:SS') as ngay_kh_nh,to_char(hdr.tg_ky,  'DD-MM-YYYY HH24:MI:SS') as tg_ky,hdr.tk_no,hdr.tk_co,hdr.ma_nnthue , hdr.ten_nnthue , hdr.dc_nnthue,hdr.ma_dbhc AS dbhc, ");
                sbsql.Append("hdr.ma_lthue,hdr.ma_cqthu,hdr.tk_kh_nh, hdr.nh_giu_tknnt as ma_nh_a, hdr.nh_giu_tkkb as ma_nh_b,hdr.ma_nt,hdr.ty_gia,hdr.ttien,hdr.ttien_nt,TO_CHAR(to_date(hdr.NGAY_CTU,'DD-MM-YYYY'),'DD-MM-YYYY HH24:MI:SS') as NGAY_CTU , TO_CHAR(to_date(hdr.ngay_ntien,'DD-MM-YYYY'),'DD-MM-YYYY HH24:MI:SS') as ngay_ntien, ");
                sbsql.Append("hdr.ma_nntien,hdr.ten_nntien,hdr.dc_nntien,'0002' as MA_NV, hdr.SO_BT,'02' as MA_DTHU, hdr.KYHIEU_CT,hdr.MA_HQ,hdr.MA_HQ_PH,'01' as pt_tt,");
                sbsql.Append("hdr.ten_hq,hdr.ten_hq_ph,hdr.ten_cqthu,'' as QUAN_NNTHUE,'' as TINH_NNTHUE,'' as QUAN_NNTIEN,'' as TINH_NNTIEN,remark as remarks");
                sbsql.Append(" FROM  tcs_ctu_songphuong_hdr hdr ");
                sbsql.Append("WHERE  hdr.trang_thai in('03') AND hdr.SO_CT =:SO_CT");
            }
            else if (type.Equals("CITAD_02"))
            {
                table = "tcs_ctu_songphuong_dtl";
                sbsql.Append("SELECT hdr.shkb,hdr.ngay_kb,hdr.REF_NO as so_ct_nh,to_char(hdr.ngay_kh_nh,'dd-MM-yyyy') as ngay_kh_nh,to_char(hdr.tg_ky,  'dd-MM-yyyy HH24:MI:SS') as tg_ky,hdr.tk_no,hdr.tk_co,hdr.ma_nnthue , hdr.ten_nnthue , hdr.dc_nnthue,hdr.ma_dbhc AS dbhc, ");
                sbsql.Append("hdr.ma_lthue,hdr.ma_cqthu,hdr.tk_kh_nh, hdr.nh_giu_tknnt as ma_nh_a, hdr.nh_giu_tkkb as ma_nh_b,hdr.ma_nt,hdr.ty_gia,hdr.ttien,hdr.ttien_nt,TO_CHAR(to_date(hdr.NGAY_CTU,'DD-MM-YYYY'),'DD-MM-YYYY HH24:MI:SS') as NGAY_CTU , TO_CHAR(to_date(hdr.ngay_ntien,'DD-MM-YYYY'),'DD-MM-YYYY HH24:MI:SS') as ngay_ntien, ");
                sbsql.Append("hdr.ma_nntien,hdr.ten_nntien,hdr.dc_nntien,'0002' as MA_NV, hdr.SO_BT,'02' as MA_DTHU, hdr.KYHIEU_CT,hdr.MA_HQ,hdr.MA_HQ_PH,'01' as pt_tt, ");
                sbsql.Append("hdr.ten_hq,hdr.ten_hq_ph,hdr.ten_cqthu,'' as QUAN_NNTHUE,'' as TINH_NNTHUE,'' as QUAN_NNTIEN,'' as TINH_NNTIEN,remark as remarks ");
                sbsql.Append("FROM  tcs_ctu_songphuong_hdr hdr ");
                sbsql.Append("WHERE  hdr.trang_thai in('02') AND hdr.SO_CT =:SO_CT");
            }
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SO_CT";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = so_ct;
            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);
            if (ds.Tables[0].Rows.Count <= 0)
            {
                throw new Exception("Không lấy được thông tin chứng từ HDR!");
            }
            if (ds.Tables[0].Rows.Count > 0)
            {

                string strSHKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                string strNgay_kb = ds.Tables[0].Rows[0]["ngay_kb"].ToString();
                string strMa_nh_a = ds.Tables[0].Rows[0]["ma_nh_a"].ToString();
                string strDBHC = ds.Tables[0].Rows[0]["DBHC"].ToString();
                string strPTTT = ds.Tables[0].Rows[0]["pt_tt"].ToString();
                string strMaNV = ds.Tables[0].Rows[0]["MA_NV"].ToString();
                string strSO_BT = ds.Tables[0].Rows[0]["SO_BT"].ToString();

                string strMa_CQThu = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
                string strMA_HQ_PH = ds.Tables[0].Rows[0]["MA_HQ_PH"].ToString();
                string strTen_CQthu = ds.Tables[0].Rows[0]["ten_cqthu"].ToString();

                string strTKCo = ds.Tables[0].Rows[0]["tk_co"].ToString();
                if (strTKCo.Length > 4)
                {
                    strTKCo = strTKCo.Substring(0, 4);
                }
                //string strMaDThu = ds.Tables[0].Rows[0]["MA_DTHU"].ToString();
                if (strTen_CQthu.Length <= 0)
                {
                    strTen_CQthu = GetTenCQThu(strMa_CQThu, strMA_HQ_PH);
                }

                string strTrantype = "";
                if (strPTTT.Equals("05"))
                {
                    strTrantype = "TM";
                }
                else
                {
                    strTrantype = "CK";
                }

                if (strSO_BT.Length <= 0)
                {
                    strSO_BT = "0";
                }
                string strMa_DThu = ds.Tables[0].Rows[0]["MA_DTHU"].ToString();
                if (strMa_nh_a.Length <= 0)
                {
                    strMa_nh_a = strSender_Code;
                }
                MSG063 msg063 = new MSG063();
                msg063.Header = new HEADER();
                msg063.Header.VERSION = "1.0";
                msg063.Header.SENDER_CODE = "";
                msg063.Header.SENDER_NAME = "";
                msg063.Header.RECEIVER_CODE = "";
                msg063.Header.RECEIVER_NAME = "";
                msg063.Header.TRAN_CODE = "063";
                msg063.Header.MSG_ID = "";
                msg063.Header.SEND_DATE = DateTime.Now.ToString();

                msg063.Body = new BODY();
                msg063.Body.CTU_HDR = new CTU_HDR();

                // tuanda - 04/03/24
                msg063.Body.CTU_HDR.SO_THAM_CHIEU = ds.Tables[0].Rows[0]["ma_thamchieu"].ToString();

                msg063.Body.CTU_HDR.SHKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                msg063.Body.CTU_HDR.NGAY_KB = strNgay_kb;
                msg063.Body.CTU_HDR.TRAN_TYPE = strTrantype;

                msg063.Body.CTU_HDR.TK_NO = ds.Tables[0].Rows[0]["tk_no"].ToString();
                msg063.Body.CTU_HDR.TK_CO = ds.Tables[0].Rows[0]["tk_co"].ToString();
                msg063.Body.CTU_HDR.MA_NNTHUE = ds.Tables[0].Rows[0]["ma_nnthue"].ToString();
                msg063.Body.CTU_HDR.TEN_NNTHUE = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                msg063.Body.CTU_HDR.DC_NNTHUE = ds.Tables[0].Rows[0]["dc_nnthue"].ToString();

                string ma_lthue = ds.Tables[0].Rows[0]["ma_lthue"].ToString();
                if (ma_lthue.Equals("04"))
                {
                    ma_lthue = "04";
                }
                else if (ma_lthue.Equals("01") || ma_lthue.Equals("02") || ma_lthue.Equals("03") || ma_lthue.Equals("05") || ma_lthue.Equals("06") || ma_lthue.Equals("08"))
                {
                    //không thì gán mặc định là 01
                    ma_lthue = "01";
                }
                else if (ma_lthue.Equals("07"))
                {
                    ma_lthue = "03";
                }

                string dac_diem_ptien = "";
                string so_khung = "";
                string so_may = "";
                string tb_oto_xemay = "";
                if (type.Equals("TQ"))
                {
                    dac_diem_ptien = ds.Tables[0].Rows[0]["dac_diem_ptien"].ToString();
                    so_khung = ds.Tables[0].Rows[0]["so_khung"].ToString();
                    so_may = ds.Tables[0].Rows[0]["so_may"].ToString();

                    if (so_khung.Trim().Length > 0 && so_may.Trim().Length > 0)
                    {
                        tb_oto_xemay = "SK " + so_khung + " SM " + so_may + " DDPT " + dac_diem_ptien + " ";
                    }
                }
               
                msg063.Body.CTU_HDR.MA_LTHUE = ma_lthue;
                msg063.Body.CTU_HDR.MA_CQTHU = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
               


                if (type.Equals("CITAD") || type.Equals("CITAD_02"))
                {
                    msg063.Body.CTU_HDR.NGAY_CT = ds.Tables[0].Rows[0]["ngay_ctu"].ToString();
                    msg063.Body.CTU_HDR.NGAY_NNTIEN = ds.Tables[0].Rows[0]["ngay_ntien"].ToString();
                    msg063.Body.CTU_HDR.TG_KY = ds.Tables[0].Rows[0]["TG_KY"].ToString();
                }
                else
                {
                    msg063.Body.CTU_HDR.NGAY_CT = ds.Tables[0].Rows[0]["ngay_kh_nh"].ToString();
                    msg063.Body.CTU_HDR.NGAY_NNTIEN = ds.Tables[0].Rows[0]["ngay_kh_nh"].ToString();
                    msg063.Body.CTU_HDR.TG_KY = ds.Tables[0].Rows[0]["ngay_kh_nh"].ToString();
                }

                if (type.Equals("NTDT") || strDBHC.Trim().Length == 5)
                {
                    msg063.Body.CTU_HDR.MA_DBHC = strDBHC;
                }
                else
                {
                    strDBHC = GetMaDBHC(strSHKB);
                    if (strDBHC.Length <= 0)
                    {
                        msg063.Body.CTU_HDR.MA_DBHC = "00000";
                    }
                    else
                    {
                        msg063.Body.CTU_HDR.MA_DBHC = strDBHC;
                    }

                }

                msg063.Body.CTU_HDR.TK_KH_NHAN = strTrantype;
                msg063.Body.CTU_HDR.MA_NH_A = ds.Tables[0].Rows[0]["ma_nh_a"].ToString();
                msg063.Body.CTU_HDR.MA_NH_B = ds.Tables[0].Rows[0]["ma_nh_b"].ToString();
                msg063.Body.CTU_HDR.MA_NT = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                msg063.Body.CTU_HDR.TY_GIA = ds.Tables[0].Rows[0]["ty_gia"].ToString();
                msg063.Body.CTU_HDR.TTIEN = ds.Tables[0].Rows[0]["ttien"].ToString();
                msg063.Body.CTU_HDR.TTIEN_NT = ds.Tables[0].Rows[0]["ttien_nt"].ToString();
                msg063.Body.CTU_HDR.TRANG_THAI = "00";
                msg063.Body.CTU_HDR.MA_NNTIEN = ds.Tables[0].Rows[0]["ma_nntien"].ToString();
                msg063.Body.CTU_HDR.TEN_NNTIEN = ds.Tables[0].Rows[0]["ten_nntien"].ToString();
                msg063.Body.CTU_HDR.DC_NNTIEN = ds.Tables[0].Rows[0]["dc_nntien"].ToString();
                msg063.Body.CTU_HDR.MA_NV = ds.Tables[0].Rows[0]["MA_NV"].ToString();
                msg063.Body.CTU_HDR.SO_BT = strSO_BT;
                //nếu SHB truyền sang thì mặc định là 01, của ngân hàng khác là 02
                msg063.Body.CTU_HDR.MA_DTHU = strMa_DThu;
                msg063.Body.CTU_HDR.KYHIEU_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
                msg063.Body.CTU_HDR.SO_BK = "";
                msg063.Body.CTU_HDR.NGAY_BK = "";

                string tk_kh = ds.Tables[0].Rows[0]["tk_kh_nh"].ToString();
                if (tk_kh.Length > 21)
                {
                    tk_kh = tk_kh.Substring(0,21);
                }

                msg063.Body.CTU_HDR.TK_KH_NH = tk_kh;

                msg063.Body.CTU_HDR.NGAY_KH_NH = ds.Tables[0].Rows[0]["ngay_kh_nh"].ToString();
                msg063.Body.CTU_HDR.TEN_KH_NHAN = "";
                msg063.Body.CTU_HDR.DIACHI_KH_NHAN = "";
                msg063.Body.CTU_HDR.MA_KHTK = "";
                msg063.Body.CTU_HDR.MA_HQ = ds.Tables[0].Rows[0]["ma_hq"].ToString();
                msg063.Body.CTU_HDR.MA_HQ_PH = ds.Tables[0].Rows[0]["ma_hq_ph"].ToString();
                msg063.Body.CTU_HDR.CTU_BLT = "0";
                msg063.Body.CTU_HDR.DIEN_GIAI = "";  //ten_hq,ten_hq_ph
                msg063.Body.CTU_HDR.TEN_CQTHU = strTen_CQthu;
                msg063.Body.CTU_HDR.TEN_HQ = ds.Tables[0].Rows[0]["ten_hq"].ToString();
                msg063.Body.CTU_HDR.TEN_HQ_PH = ds.Tables[0].Rows[0]["ten_hq_ph"].ToString();

                msg063.Body.CTU_HDR.QUAN_NNTHUE = ds.Tables[0].Rows[0]["QUAN_NNTHUE"].ToString();
                msg063.Body.CTU_HDR.TINH_NNTHUE = ds.Tables[0].Rows[0]["TINH_NNTHUE"].ToString();
                msg063.Body.CTU_HDR.QUAN_NNTIEN = ds.Tables[0].Rows[0]["QUAN_NNTIEN"].ToString();
                msg063.Body.CTU_HDR.TINH_NNTIEN = ds.Tables[0].Rows[0]["TINH_NNTIEN"].ToString();
                string DC_KB = "";
                if (type.Equals("TQ"))
                {
                    DC_KB = Business.Common.mdlCommon.Get_TenTinh_KB_BL(Business.SP.SongPhuongController.GetMa_DBHC(strSHKB));
                }
                if (type.Equals("NTDT"))
                {
                    DC_KB = ds.Tables[0].Rows[0]["TEN_TINH_KBAC"].ToString();
                }
                msg063.Body.CTU_HDR.DC_KB = DC_KB;
                msg063.Body.CTU_HDR.DIEN_GIAI = ds.Tables[0].Rows[0]["remarks"].ToString();
                msg063.Body.CTU_HDR.CTU_DTL = new CTU_DTL();
                msg063.Body.CTU_HDR.CTU_DTL.ROW = new List<ROW>();
                StringBuilder sbsql_dtl = new StringBuilder();
                if (type.Equals("TQ"))
                {
                    sbsql_dtl.Append("SELECT dtl.ma_chuong,dtl.ma_tmuc,dtl.noi_dung,dtl.sotien,NVL(dtl.sotien_nt,dtl.sotien) sotien_nt,NVL(dtl.so_tk, hdr.so_qd) as so_tk,NVL(TO_CHAR(dtl.ngay_tk,'dd/MM/yyyy'),dtl.ky_thue) as ngay_tk,dtl.sac_thue   FROM  tcs_ctu_dtl dtl, tcs_ctu_hdr hdr  WHERE hdr.so_ct = dtl.so_ct and dtl.SO_CT =:SO_CT");
                }
                else if (type.Equals("NTDT"))
                {
                    sbsql_dtl.Append("SELECT ma_chuong,ma_tmuc,noi_dung,sotien,  NVL(sotien_nt,sotien) sotien_nt,so_tk_tb_qd so_tk,kythue as ngay_tk,'' sac_thue   FROM  tcs_ctu_ntdt_dtl  WHERE SO_CT =:SO_CT");
                }
                else if (type.Equals("HQ247"))
                {
                    sbsql_dtl.Append("SELECT ma_chuong,ma_tmuc,noi_dung,sotien,NVL(sotien_nt,sotien) sotien_nt, so_tk,TO_CHAR(ngay_tk,'dd/MM/yyyy') as ngay_tk,sac_thue   FROM  tcs_ctu_ntdt_hq247_dtl  WHERE SO_CT =:SO_CT");
                }
                else if (type.Equals("NHOTHU"))
                {
                    sbsql_dtl.Append("SELECT ma_chuong,ma_tmuc,noi_dung,sotien,NVL(sotien_nt,sotien) sotien_nt,so_tk,TO_CHAR(ngay_tk,'dd/MM/yyyy') as ngay_tk,sac_thue    FROM  TCS_CTU_HQ_NHOTHU_DTL  WHERE SO_CT =:SO_CT");
                }
                else if (type.Equals("MOBILE"))
                {
                    sbsql_dtl.Append("SELECT ma_chuong,ma_tmuc,noi_dung,sotien,NVL(sotien_nt,sotien) sotien_nt,so_tk,ky_thue as ngay_tk,sac_thue   FROM  tcs_ctu_dtl_mobitct  WHERE SO_CT =:SO_CT");
                }
                else if (type.Equals("CITAD") || type.Equals("CITAD_02"))
                {
                    sbsql_dtl.Append("SELECT ma_chuong,ma_tmuc,noi_dung,sotien,NVL(sotien_nt,sotien) sotien_nt,so_tk,NVL(TO_CHAR(ngay_tk,'dd/MM/yyyy'),ky_thue) as ngay_tk,sac_thue    FROM  tcs_ctu_songphuong_dtl  WHERE SO_CT =:SO_CT");
                }
                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_CT";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = so_ct;

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql_dtl.ToString(), CommandType.Text, p);
                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không lấy được thông tin chứng từ DTL!");
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ROW row = new ROW();
                        row.SHKB = strSHKB;
                        row.NGAY_KB = strNgay_kb;
                        if (strTKCo.Equals("3591"))
                        {
                            row.MA_CHUONG = "";
                        }
                        else
                        {
                            row.MA_CHUONG = ds.Tables[0].Rows[i]["ma_chuong"].ToString();
                        }
                        string noi_dung  = ds.Tables[0].Rows[i]["noi_dung"].ToString();
                        if (type.Equals("TQ"))
                        {
                            if (tb_oto_xemay.Trim().Length > 0)
                            {
                                noi_dung = tb_oto_xemay + noi_dung;
                            }
                            if (noi_dung.Length > 100)
                            {
                                noi_dung = noi_dung.Substring(0, 100);
                            }
                        }
                        

                        row.MA_NDKT = ds.Tables[0].Rows[i]["ma_tmuc"].ToString();
                        row.NOI_DUNG = noi_dung;
                        row.SOTIEN = ds.Tables[0].Rows[i]["sotien"].ToString();
                        row.SOTIEN_NT = ds.Tables[0].Rows[i]["sotien_nt"].ToString();
                        row.MA_NV = strMaNV;
                        row.SO_BT = strSO_BT;
                        //nếu SHB truyền sang thì mặc định là 01, của ngân hàng khác là 02
                        row.MA_DTHU = strMa_DThu;
                        row.MA_QUY = "01";
                        row.STT_TK = "";
                        row.SO_TK = ds.Tables[0].Rows[i]["so_tk"].ToString();
                        row.NAM_DK = ds.Tables[0].Rows[i]["ngay_tk"].ToString();
                        row.SAC_THUE = ds.Tables[0].Rows[i]["sac_thue"].ToString();
                        msg063.Body.CTU_HDR.CTU_DTL.ROW.Add(row);
                    }
                }
                msg063.Security = new SECURITY();
                msg063.Security.SIGNATURE = "";
                string xmlOut = msg063.MSG063toXML(msg063);
                xmlOut = xmlOut.Replace(strXMLdefout1, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout2, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout3, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout4, String.Empty);
                return xmlOut;
            }
            return "";
        }

        public string SendBienLaiSP(string so_ct)
        {

            string REF_ID = "";
            string SO_TAI_KHOAN = "";
            string TEN_NNOP = "";
            string SO_REF_CORE = "";
            string SO_TIEN = "";
            string MA_NT = "";
            string SHKB = "";
            string MA_NH_PHANH = "";
            string TEN_NH_PHANH = "";
            string CONTEN_EX = "";
            string PAYMENT_DETAIL = "";
            string SOURCE_V = "";
            string CITAD_TYPE = "";
            string TRANG_THAI = "";
            string MSG = "";
            SOURCE_V = "SHB";
            //Lấy thông tin từ hdr để lấy thông tin
            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();

            sbsql.Append("SELECT tk_kh_nh,ten_nnthue, so_ct_nh,ttien,ma_nt,shkb,ma_nh_a,dien_giai FROM TCS_BIENLAI_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI in ('03','08') ");

            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SO_CT";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = so_ct;

            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            if (ds.Tables[0].Rows.Count > 0)
            {
                REF_ID = so_ct;
                SO_TAI_KHOAN = ds.Tables[0].Rows[0]["tk_kh_nh"].ToString();
                TEN_NNOP = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                SO_REF_CORE = ds.Tables[0].Rows[0]["so_ct_nh"].ToString();
                SO_TIEN = ds.Tables[0].Rows[0]["ttien"].ToString();
                MA_NT = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                SHKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                MA_NH_PHANH = ds.Tables[0].Rows[0]["ma_nh_a"].ToString();
                if (MA_NH_PHANH.Length <= 0)
                {
                    MA_NH_PHANH = strSender_Code;
                }
                TEN_NH_PHANH = strSender_Name;
                CONTEN_EX = "";
                PAYMENT_DETAIL = ds.Tables[0].Rows[0]["dien_giai"].ToString();
                TRANG_THAI = "00";


            }
            else
            {
                throw new Exception("Lỗi trong quá trình lấy thông tin chứng từ hdr");

            }

            MSG = getMSG055(so_ct);

            //MSG = "XXXX";

            if (SO_REF_CORE.Length <= 0)
            {
                SO_REF_CORE = "";
            }
            string ID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_songphuong.NextVal");
            insertLogSP(ID, "CHUNGTU", SHKB, SO_REF_CORE, so_ct, "", MSG);

            string result = "";
            //OracleConnection conn = GetConnection();
            //OracleCommand myCMD = new OracleCommand();
            //try
            //{



            //    StringBuilder sbSqlInsert = new StringBuilder("");

            //    String strS = "SELECT INSERTDATAFROMSHB.proc_insert_datashb(:REF_ID,:SO_TAI_KHOAN,:TEN_NNOP,:SO_REF_CORE,:SO_TIEN,:MA_NT,:SHKB,:MA_NH_PHANH,:TEN_NH_PHANH,:CONTEN_EX,:PAYMENT_DETAIL,:SOURCE_V,:CITAD_TYPE,:TRANG_THAI,:MSG,sysdate) FROM DUAL";

            //    myCMD.Connection = conn;
            //    myCMD.CommandText = strS;
            //    myCMD.CommandType = CommandType.Text;
            //    myCMD.Parameters.Add("REF_ID", OracleType.NVarChar).Value = CheckEmty(REF_ID);
            //    myCMD.Parameters.Add("SO_TAI_KHOAN", OracleType.NVarChar).Value = CheckEmty(SO_TAI_KHOAN);
            //    myCMD.Parameters.Add("TEN_NNOP", OracleType.NVarChar).Value = CheckEmty(TEN_NNOP);
            //    myCMD.Parameters.Add("SO_REF_CORE", OracleType.NVarChar).Value = SO_REF_CORE;
            //    myCMD.Parameters.Add("SO_TIEN", OracleType.NVarChar).Value = CheckEmty(SO_TIEN);
            //    myCMD.Parameters.Add("MA_NT", OracleType.NVarChar).Value = CheckEmty(MA_NT);
            //    myCMD.Parameters.Add("SHKB", OracleType.NVarChar).Value = CheckEmty(SHKB);
            //    myCMD.Parameters.Add("MA_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(MA_NH_PHANH);
            //    myCMD.Parameters.Add("TEN_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(TEN_NH_PHANH);
            //    myCMD.Parameters.Add("CONTEN_EX", OracleType.NVarChar).Value = CheckEmty(CONTEN_EX);
            //    myCMD.Parameters.Add("PAYMENT_DETAIL", OracleType.NVarChar).Value = CheckEmty(PAYMENT_DETAIL);
            //    myCMD.Parameters.Add("SOURCE_V", OracleType.NVarChar).Value = CheckEmty(SOURCE_V);
            //    myCMD.Parameters.Add("CITAD_TYPE", OracleType.NVarChar).Value = CheckEmty(CITAD_TYPE);
            //    myCMD.Parameters.Add("TRANG_THAI", OracleType.NVarChar).Value = CheckEmty(TRANG_THAI);
            //    myCMD.Parameters.Add("MSG", OracleType.Clob, 32767).Value = MSG;
            //    //myCMD.ExecuteReader();

            //    result = (String)myCMD.ExecuteScalar();

            //}
            //catch (Exception ex)
            //{
            //    result = "FALSE";
            //    return result;
            //}
            //finally
            //{

            //    if ((conn != null) || conn.State != ConnectionState.Closed)
            //    {
            //        myCMD.Dispose();
            //        conn.Dispose();

            //        conn.Close();
            //    }
            //}

            OracleConnection conn = GetConnection();
            String strS = "SELECT INSERTDATAFROMSHB.proc_insert_datashb(:REF_ID,:SO_TAI_KHOAN,:TEN_NNOP,:SO_REF_CORE,:SO_TIEN,:MA_NT,:SHKB,:MA_NH_PHANH,:TEN_NH_PHANH,:CONTEN_EX,:PAYMENT_DETAIL,:SOURCE_V,:CITAD_TYPE,:TRANG_THAI,:MSG,sysdate) FROM DUAL";
            var cmd = new OracleCommand("strS", conn);
            //cmd.CommandType = CommandType.Text;
            cmd.CommandText = strS;
            var xmlParam = new OracleParameter("MSG", OracleType.Clob);
            cmd.Parameters.Add(xmlParam);

            // DO NOT assign the parameter value yet in this place

            cmd.Transaction = conn.BeginTransaction();
            try
            {
                // Assign value here, AFTER starting the TX
                xmlParam.Value = MSG;
                cmd.Parameters.Add("REF_ID", OracleType.NVarChar).Value = CheckEmty(REF_ID);
                cmd.Parameters.Add("SO_TAI_KHOAN", OracleType.NVarChar).Value = CheckEmty(SO_TAI_KHOAN);
                cmd.Parameters.Add("TEN_NNOP", OracleType.NVarChar).Value = CheckEmty(TEN_NNOP);
                cmd.Parameters.Add("SO_REF_CORE", OracleType.NVarChar).Value = SO_REF_CORE;
                cmd.Parameters.Add("SO_TIEN", OracleType.NVarChar).Value = CheckEmty(SO_TIEN);
                cmd.Parameters.Add("MA_NT", OracleType.NVarChar).Value = CheckEmty(MA_NT);
                cmd.Parameters.Add("SHKB", OracleType.NVarChar).Value = CheckEmty(SHKB);
                cmd.Parameters.Add("MA_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(MA_NH_PHANH);
                cmd.Parameters.Add("TEN_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(TEN_NH_PHANH);
                cmd.Parameters.Add("CONTEN_EX", OracleType.NVarChar).Value = CheckEmty(CONTEN_EX);
                cmd.Parameters.Add("PAYMENT_DETAIL", OracleType.NVarChar).Value = CheckEmty(PAYMENT_DETAIL);
                cmd.Parameters.Add("SOURCE_V", OracleType.NVarChar).Value = CheckEmty(SOURCE_V);
                cmd.Parameters.Add("CITAD_TYPE", OracleType.NVarChar).Value = CheckEmty(CITAD_TYPE);
                cmd.Parameters.Add("TRANG_THAI", OracleType.NVarChar).Value = CheckEmty(TRANG_THAI);
                result = (String)cmd.ExecuteScalar();
                cmd.Transaction.Commit();
            }
            catch (OracleException)
            {
                cmd.Transaction.Rollback();
                result = "FALSE";
                return result;
            }
            finally
            {
                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    conn.Dispose();
                    conn.Close();
                }
            }

            updateLogSP(ID, result);


            if (result.ToUpper().Equals("SUCCESS"))
            {
                string strSQL = "UPDATE tcs_bienlai_hdr SET TT_SP='2',trang_thai='09' WHERE so_ct='" + so_ct + "' ";
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            else
            {
                string strSQL = "UPDATE tcs_bienlai_hdr SET trang_thai='08' WHERE so_ct='" + so_ct + "' ";
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }

            return result;
        }

        public string SendBienLaiSP1(string so_ct, string v_strRefCoreTTSP)
        {

            string REF_ID = "";
            string SO_TAI_KHOAN = "";
            string TEN_NNOP = "";
            string SO_REF_CORE = "";
            string SO_TIEN = "";
            string MA_NT = "";
            string SHKB = "";
            string MA_NH_PHANH = "";
            string TEN_NH_PHANH = "";
            string CONTEN_EX = "";
            string PAYMENT_DETAIL = "";
            string SOURCE_V = "";
            string CITAD_TYPE = "";
            string TRANG_THAI = "";
            string MSG = "";
           
            SOURCE_V = "SHB";
            //Lấy thông tin từ hdr để lấy thông tin
            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();

            sbsql.Append("SELECT tk_kh_nh,ten_nnthue, so_ct_nh,ttien,ma_nt,shkb,ma_nh_a,dien_giai FROM TCS_BIENLAI_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI in ('03','08') ");

            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SO_CT";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = so_ct;

            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            if (ds.Tables[0].Rows.Count > 0)
            {
                REF_ID = so_ct;
                SO_TAI_KHOAN = ds.Tables[0].Rows[0]["tk_kh_nh"].ToString();
                TEN_NNOP = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                SO_REF_CORE = ds.Tables[0].Rows[0]["so_ct_nh"].ToString();
                SO_TIEN = ds.Tables[0].Rows[0]["ttien"].ToString();
                MA_NT = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                SHKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                MA_NH_PHANH = ds.Tables[0].Rows[0]["ma_nh_a"].ToString();
                if (MA_NH_PHANH.Length <= 0)
                {
                    MA_NH_PHANH = strSender_Code;
                }
                TEN_NH_PHANH = strSender_Name;
                CONTEN_EX = "";
                PAYMENT_DETAIL = ds.Tables[0].Rows[0]["dien_giai"].ToString();
                TRANG_THAI = "00";


            }
            else
            {
                throw new Exception("Lỗi trong quá trình lấy thông tin chứng từ hdr");

            }

            MSG = getMSG055(so_ct, v_strRefCoreTTSP);

            //MSG = "XXXX";

            if (SO_REF_CORE.Length <= 0)
            {
                SO_REF_CORE = "";
            }
            string ID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_songphuong.NextVal");
            insertLogSP(ID, "CHUNGTU", SHKB, SO_REF_CORE, so_ct, "", MSG);

            string result = "";
            //OracleConnection conn = GetConnection();
            //OracleCommand myCMD = new OracleCommand();
            //try
            //{



            //    StringBuilder sbSqlInsert = new StringBuilder("");

            //    String strS = "SELECT INSERTDATAFROMSHB.proc_insert_datashb(:REF_ID,:SO_TAI_KHOAN,:TEN_NNOP,:SO_REF_CORE,:SO_TIEN,:MA_NT,:SHKB,:MA_NH_PHANH,:TEN_NH_PHANH,:CONTEN_EX,:PAYMENT_DETAIL,:SOURCE_V,:CITAD_TYPE,:TRANG_THAI,:MSG,sysdate) FROM DUAL";

            //    myCMD.Connection = conn;
            //    myCMD.CommandText = strS;
            //    myCMD.CommandType = CommandType.Text;
            //    myCMD.Parameters.Add("REF_ID", OracleType.NVarChar).Value = CheckEmty(REF_ID);
            //    myCMD.Parameters.Add("SO_TAI_KHOAN", OracleType.NVarChar).Value = CheckEmty(SO_TAI_KHOAN);
            //    myCMD.Parameters.Add("TEN_NNOP", OracleType.NVarChar).Value = CheckEmty(TEN_NNOP);
            //    myCMD.Parameters.Add("SO_REF_CORE", OracleType.NVarChar).Value = SO_REF_CORE;
            //    myCMD.Parameters.Add("SO_TIEN", OracleType.NVarChar).Value = CheckEmty(SO_TIEN);
            //    myCMD.Parameters.Add("MA_NT", OracleType.NVarChar).Value = CheckEmty(MA_NT);
            //    myCMD.Parameters.Add("SHKB", OracleType.NVarChar).Value = CheckEmty(SHKB);
            //    myCMD.Parameters.Add("MA_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(MA_NH_PHANH);
            //    myCMD.Parameters.Add("TEN_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(TEN_NH_PHANH);
            //    myCMD.Parameters.Add("CONTEN_EX", OracleType.NVarChar).Value = CheckEmty(CONTEN_EX);
            //    myCMD.Parameters.Add("PAYMENT_DETAIL", OracleType.NVarChar).Value = CheckEmty(PAYMENT_DETAIL);
            //    myCMD.Parameters.Add("SOURCE_V", OracleType.NVarChar).Value = CheckEmty(SOURCE_V);
            //    myCMD.Parameters.Add("CITAD_TYPE", OracleType.NVarChar).Value = CheckEmty(CITAD_TYPE);
            //    myCMD.Parameters.Add("TRANG_THAI", OracleType.NVarChar).Value = CheckEmty(TRANG_THAI);
            //    myCMD.Parameters.Add("MSG", OracleType.Clob, 32767).Value = MSG;
            //    //myCMD.ExecuteReader();

            //    result = (String)myCMD.ExecuteScalar();

            //}
            //catch (Exception ex)
            //{
            //    result = "FALSE";
            //    return result;
            //}
            //finally
            //{

            //    if ((conn != null) || conn.State != ConnectionState.Closed)
            //    {
            //        myCMD.Dispose();
            //        conn.Dispose();

            //        conn.Close();
            //    }
            //}

            OracleConnection conn = GetConnection();
            String strS = "SELECT INSERTDATAFROMSHB.proc_insert_datashb(:REF_ID,:SO_TAI_KHOAN,:TEN_NNOP,:SO_REF_CORE,:SO_TIEN,:MA_NT,:SHKB,:MA_NH_PHANH,:TEN_NH_PHANH,:CONTEN_EX,:PAYMENT_DETAIL,:SOURCE_V,:CITAD_TYPE,:TRANG_THAI,:MSG,sysdate) FROM DUAL";
            var cmd = new OracleCommand("strS", conn);
            //cmd.CommandType = CommandType.Text;
            cmd.CommandText = strS;
            var xmlParam = new OracleParameter("MSG", OracleType.Clob);
            cmd.Parameters.Add(xmlParam);

            // DO NOT assign the parameter value yet in this place

            cmd.Transaction = conn.BeginTransaction();
            try
            {
                // Assign value here, AFTER starting the TX
                xmlParam.Value = MSG;
                cmd.Parameters.Add("REF_ID", OracleType.NVarChar).Value = CheckEmty(REF_ID);
                cmd.Parameters.Add("SO_TAI_KHOAN", OracleType.NVarChar).Value = CheckEmty(SO_TAI_KHOAN);
                cmd.Parameters.Add("TEN_NNOP", OracleType.NVarChar).Value = CheckEmty(TEN_NNOP);
                cmd.Parameters.Add("SO_REF_CORE", OracleType.NVarChar).Value = SO_REF_CORE;
                cmd.Parameters.Add("SO_TIEN", OracleType.NVarChar).Value = CheckEmty(SO_TIEN);
                cmd.Parameters.Add("MA_NT", OracleType.NVarChar).Value = CheckEmty(MA_NT);
                cmd.Parameters.Add("SHKB", OracleType.NVarChar).Value = CheckEmty(SHKB);
                cmd.Parameters.Add("MA_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(MA_NH_PHANH);
                cmd.Parameters.Add("TEN_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(TEN_NH_PHANH);
                cmd.Parameters.Add("CONTEN_EX", OracleType.NVarChar).Value = CheckEmty(CONTEN_EX);
                cmd.Parameters.Add("PAYMENT_DETAIL", OracleType.NVarChar).Value = CheckEmty(PAYMENT_DETAIL);
                cmd.Parameters.Add("SOURCE_V", OracleType.NVarChar).Value = CheckEmty(SOURCE_V);
                cmd.Parameters.Add("CITAD_TYPE", OracleType.NVarChar).Value = CheckEmty(CITAD_TYPE);
                cmd.Parameters.Add("TRANG_THAI", OracleType.NVarChar).Value = CheckEmty(TRANG_THAI);
                result = (String)cmd.ExecuteScalar();
                cmd.Transaction.Commit();
            }
            catch (OracleException)
            {
                cmd.Transaction.Rollback();
                result = "FALSE";
                return result;
            }
            finally
            {
                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    conn.Dispose();
                    conn.Close();
                }
            }

            updateLogSP(ID, result);


            if (result.ToUpper().Equals("SUCCESS"))
            {
                string strSQL = "UPDATE tcs_bienlai_hdr SET TT_SP='2',trang_thai='09' WHERE so_ct='" + so_ct + "' ";
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }
            else
            {
                string strSQL = "UPDATE tcs_bienlai_hdr SET trang_thai='08' WHERE so_ct='" + so_ct + "' ";
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }

            return result;
        }

        public string getMSG055(string so_ct, string v_strRefCoreTTSP)
        {

            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();

            sbsql.Append("SELECT shkb,ngay_kb, so_ct_nh,'0001' MA_NV,to_char(ngay_kh_nh,'DD-MM-YYYY') as ngay_ct,tk_co,ma_nnthue,ten_nnthue,dc_nnthue,ma_nntien,ten_nntien,dc_nntien,");
            sbsql.Append("cq_qd,ma_lthue,ma_cqthu,ma_nh_tt,ma_nh_gt,ma_nt,ty_gia,ttien,ttien_nt,dien_giai,");
            sbsql.Append("to_char(ngay_kh_nh,'DD-MM-YYYY HH24:MI:SS') as ngay_kh_nh,pt_tt,");
            sbsql.Append("KYHIEU_CT,TEN_HUYEN_NNTHUE,TEN_TINH_NNTHUE,TEN_HUYEN_NNTIEN,TEN_TINH_NNTIEN,TK_KH_NH,TEN_CQTHU,ht_thu");
            sbsql.Append(" FROM TCS_BIENLAI_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI in ('03','08')");
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SO_CT";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = so_ct;

            p[1] = new OracleParameter();
            p[1].ParameterName = "ref_core_ttsp";
            p[1].DbType = DbType.String;
            p[1].Direction = ParameterDirection.Input;
            p[1].Value = v_strRefCoreTTSP;
            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            //ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            if (ds.Tables[0].Rows.Count <= 0)
            {
                throw new Exception("Không lấy được thông tin chứng từ biên lai!");
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                string strPTTT = ds.Tables[0].Rows[0]["pt_tt"].ToString();
                string shkb = ds.Tables[0].Rows[0]["shkb"].ToString();
                string ma_dbhc = GetMaDBHC(shkb);
                string ngay_kb = ds.Tables[0].Rows[0]["ngay_kb"].ToString();
                ngay_kb = FormatNgay(ngay_kb);
                string strTrantype = "";
                if (strPTTT.Equals("05"))
                {
                    strTrantype = "TM";
                }
                else
                {
                    strTrantype = "CK";
                }


                CorebankServiceESB055.MSG055 msg055 = new CorebankServiceESB055.MSG055();
                msg055.Header = new CorebankServiceESB055.HEADER();
                msg055.Header.VERSION = "1.0";
                msg055.Header.SENDER_CODE = "";
                msg055.Header.SENDER_NAME = "";
                msg055.Header.RECEIVER_CODE = "";
                msg055.Header.RECEIVER_NAME = "";
                msg055.Header.TRAN_CODE = "055";
                msg055.Header.MSG_ID = "";
                msg055.Header.SEND_DATE = DateTime.Now.ToString();

                msg055.Body = new CorebankServiceESB055.BODY();
                msg055.Body.CTU_HDR = new CorebankServiceESB055.CTU_HDR();
                msg055.Body.CTU_HDR.SO_THAM_CHIEU = ds.Tables[0].Rows[0]["so_ct_nh"].ToString();
                msg055.Body.CTU_HDR.SHKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                msg055.Body.CTU_HDR.NGAY_KB = ngay_kb;
                msg055.Body.CTU_HDR.MA_NV = ds.Tables[0].Rows[0]["ma_nv"].ToString();
                msg055.Body.CTU_HDR.NGAY_CT = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                msg055.Body.CTU_HDR.TK_CO = ds.Tables[0].Rows[0]["tk_co"].ToString();
                msg055.Body.CTU_HDR.MA_NNTHUE = ds.Tables[0].Rows[0]["ma_nnthue"].ToString();
                msg055.Body.CTU_HDR.TEN_NNTHUE = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                msg055.Body.CTU_HDR.DC_NNTHUE = ds.Tables[0].Rows[0]["dc_nnthue"].ToString();
                msg055.Body.CTU_HDR.MA_NNTIEN = ds.Tables[0].Rows[0]["ma_nntien"].ToString();
                msg055.Body.CTU_HDR.TEN_NNTIEN = ds.Tables[0].Rows[0]["ten_nntien"].ToString();
                msg055.Body.CTU_HDR.DC_NNTIEN = ds.Tables[0].Rows[0]["dc_nntien"].ToString();
                msg055.Body.CTU_HDR.NGAY_NNTIEN = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                msg055.Body.CTU_HDR.CQ_QD = ds.Tables[0].Rows[0]["cq_qd"].ToString();
                msg055.Body.CTU_HDR.MA_LTHUE = ds.Tables[0].Rows[0]["ma_lthue"].ToString(); //ma_lthue
                msg055.Body.CTU_HDR.MA_CQTHU = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
                msg055.Body.CTU_HDR.MA_DBHC = ma_dbhc;
                msg055.Body.CTU_HDR.MA_NH_A = strSender_Code;
                msg055.Body.CTU_HDR.MA_NH_B = ds.Tables[0].Rows[0]["ma_nh_gt"].ToString();
                msg055.Body.CTU_HDR.MA_NT = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                msg055.Body.CTU_HDR.TY_GIA = ds.Tables[0].Rows[0]["ty_gia"].ToString();
                msg055.Body.CTU_HDR.TTIEN = ds.Tables[0].Rows[0]["ttien"].ToString();
                msg055.Body.CTU_HDR.TTIEN_NT = ds.Tables[0].Rows[0]["ttien_nt"].ToString();
                msg055.Body.CTU_HDR.DIEN_GIAI = ds.Tables[0].Rows[0]["dien_giai"].ToString();
                msg055.Body.CTU_HDR.TG_KY = ds.Tables[0].Rows[0]["ngay_kh_nh"].ToString();
                msg055.Body.CTU_HDR.TRAN_TYPE = strTrantype;
                msg055.Body.CTU_HDR.KYHIEU_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
                msg055.Body.CTU_HDR.QUAN_NNTHUE = ds.Tables[0].Rows[0]["TEN_HUYEN_NNTHUE"].ToString();
                msg055.Body.CTU_HDR.TINH_NNTHUE = ds.Tables[0].Rows[0]["TEN_TINH_NNTHUE"].ToString();
                msg055.Body.CTU_HDR.QUAN_NNTIEN = ds.Tables[0].Rows[0]["TEN_HUYEN_NNTIEN"].ToString();
                msg055.Body.CTU_HDR.TINH_NNTIEN = ds.Tables[0].Rows[0]["TEN_TINH_NNTIEN"].ToString();

                string tk_kh = ds.Tables[0].Rows[0]["TK_KH_NH"].ToString();
                if (tk_kh.Length > 21)
                {
                    tk_kh = tk_kh.Substring(0, 21);
                }

                msg055.Body.CTU_HDR.TK_TRICHNO_KH = tk_kh;

                shkb = ds.Tables[0].Rows[0]["SHKB"].ToString();
                string DC_KB = Business.Common.mdlCommon.Get_TenTinh_KB_BL(Business.SP.SongPhuongController.GetMa_DBHC(shkb));
                msg055.Body.CTU_HDR.DC_KB = DC_KB;
                msg055.Body.CTU_HDR.TEN_CQTHU = ds.Tables[0].Rows[0]["TEN_CQTHU"].ToString();
                msg055.Body.CTU_HDR.LOAI_THU = ds.Tables[0].Rows[0]["HT_THU"].ToString();
                //build dtl
                msg055.Body.CTU_HDR.CTU_DTL = new CorebankServiceESB055.CTU_DTL();
                msg055.Body.CTU_HDR.CTU_DTL.ROW = new List<CorebankServiceESB055.ROW_DTL>();

                StringBuilder sbsql_dtl = new StringBuilder();
                sbsql_dtl.Append("SELECT ma_chuong,ma_ndkt,noi_dung,ttien sotien,ky_thue FROM  tcs_bienlai_hdr  WHERE SO_CT =:SO_CT");

                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_CT";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = so_ct;

                p[1] = new OracleParameter();
                p[1].ParameterName = "ref_core_ttsp";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = v_strRefCoreTTSP;

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql_dtl.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không lấy được thông tin chứng từ DTL!");
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        //for
                        CorebankServiceESB055.ROW_DTL row_dtl = new CorebankServiceESB055.ROW_DTL();
                        row_dtl.MA_CHUONG = ds.Tables[0].Rows[i]["ma_chuong"].ToString();
                        row_dtl.MA_NDKT = ds.Tables[0].Rows[i]["ma_ndkt"].ToString();
                        row_dtl.NOI_DUNG = ds.Tables[0].Rows[i]["noi_dung"].ToString();
                        row_dtl.SOTIEN = ds.Tables[0].Rows[i]["sotien"].ToString();
                        row_dtl.KY_THUE = ds.Tables[0].Rows[i]["ky_thue"].ToString();
                        msg055.Body.CTU_HDR.CTU_DTL.ROW.Add(row_dtl);
                        //end for
                    }
                }

                //build ctbl
                msg055.Body.CTU_HDR.CTBL = new CorebankServiceESB055.CTBL();
                msg055.Body.CTU_HDR.CTBL.ROW = new List<CorebankServiceESB055.ROW_CTBL>();

                StringBuilder sbsql_ctbl = new StringBuilder();

                sbsql_ctbl.Append("SELECT to_char(ngay_kh_nh,'DD-MM-YYYY') as ngay_bl,so_bienlai,ma_tinh_nnthue,ten_tinh_nnthue,ma_huyen_nnthue,ten_huyen_nnthue,");
                sbsql_ctbl.Append("ma_cqqd,ten_cqqd,so_qd,to_char(ngay_qd,'dd/MM/yyyy') as ngay_qd,ma_lhthu,ten_lhthu,");
                sbsql_ctbl.Append("ly_do_vphc,ttien_vphc,ly_do_nop_cham,ttien_nop_cham ");
                sbsql_ctbl.Append(" FROM TCS_BIENLAI_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI in ('03','08')");


                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_CT";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = so_ct;

                p[1] = new OracleParameter();
                p[1].ParameterName = "ref_core_ttsp";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = v_strRefCoreTTSP;

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql_ctbl.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không lấy được thông tin chứng từ biên lai ctbl!");
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //for
                    CorebankServiceESB055.ROW_CTBL row_ctbl = new CorebankServiceESB055.ROW_CTBL();
                    row_ctbl.NGAY_BL = ds.Tables[0].Rows[0]["ngay_bl"].ToString();
                    row_ctbl.KYHIEU_BL = "";
                    row_ctbl.SO_BL = ds.Tables[0].Rows[0]["so_bienlai"].ToString();
                    row_ctbl.MA_TINH_NNTIEN = ds.Tables[0].Rows[0]["ma_tinh_nnthue"].ToString();
                    row_ctbl.TEN_TINH_NNTIEN = ds.Tables[0].Rows[0]["ten_tinh_nnthue"].ToString();
                    row_ctbl.MA_HUYEN_NNTIEN = ds.Tables[0].Rows[0]["ma_huyen_nnthue"].ToString();
                    row_ctbl.TEN_HUYEN_NNTIEN = ds.Tables[0].Rows[0]["ten_huyen_nnthue"].ToString();
                    row_ctbl.MA_CQQD = ds.Tables[0].Rows[0]["ma_cqqd"].ToString();
                    row_ctbl.TEN_CQQD = ds.Tables[0].Rows[0]["ten_cqqd"].ToString();
                    row_ctbl.SO_QD = ds.Tables[0].Rows[0]["so_qd"].ToString();
                    row_ctbl.NGAY_QD = ds.Tables[0].Rows[0]["ngay_qd"].ToString();
                    row_ctbl.MA_LHTHU = ds.Tables[0].Rows[0]["ma_lhthu"].ToString();
                    row_ctbl.TEN_LHTHU = ds.Tables[0].Rows[0]["ten_lhthu"].ToString();
                    row_ctbl.LY_DO = ds.Tables[0].Rows[0]["ly_do_vphc"].ToString();
                    row_ctbl.TTIEN_VPHC = ds.Tables[0].Rows[0]["ttien_vphc"].ToString();
                    row_ctbl.LY_DO_NOP_CHAM = ds.Tables[0].Rows[0]["ly_do_nop_cham"].ToString();
                    row_ctbl.TTIEN_NOP_CHAM = ds.Tables[0].Rows[0]["ttien_nop_cham"].ToString();
                    msg055.Body.CTU_HDR.CTBL.ROW.Add(row_ctbl);
                    //end for
                }



                string xmlOut = msg055.MSG055toXML(msg055);

                xmlOut = xmlOut.Replace(strXMLdefout1, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout2, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout3, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout4, String.Empty);
                return xmlOut;

            }
            return "";
        }
        
        
        public string getMSG055(string so_ct)
        {

            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();

            sbsql.Append("SELECT shkb,ngay_kb, so_ct_nh,'0001' MA_NV,to_char(tg_ky,'DD-MM-YYYY') as ngay_ct,tk_co,ma_nnthue,ten_nnthue,dc_nnthue,ma_nntien,ten_nntien,dc_nntien,");
            sbsql.Append("cq_qd,ma_lthue,ma_cqthu,ma_nh_tt,ma_nh_gt,ma_nt,ty_gia,ttien,ttien_nt,dien_giai,");
            sbsql.Append("to_char(tg_ky,'DD-MM-YYYY HH24:MI:SS') as tg_ky,pt_tt,");
            sbsql.Append("KYHIEU_CT,TEN_HUYEN_NNTHUE,TEN_TINH_NNTHUE,TEN_HUYEN_NNTIEN,TEN_TINH_NNTIEN,TK_KH_NH,TEN_CQTHU,ht_thu");
            sbsql.Append(" FROM TCS_BIENLAI_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI in ('03','08')");
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SO_CT";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = so_ct;

            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            if (ds.Tables[0].Rows.Count <= 0)
            {
                throw new Exception("Không lấy được thông tin chứng từ biên lai!");
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                string strPTTT = ds.Tables[0].Rows[0]["pt_tt"].ToString();
                string shkb = ds.Tables[0].Rows[0]["shkb"].ToString();
                string ma_dbhc = GetMaDBHC(shkb);
                string ngay_kb = ds.Tables[0].Rows[0]["ngay_kb"].ToString();
                ngay_kb = FormatNgay(ngay_kb);
                string strTrantype = "";
                if (strPTTT.Equals("05"))
                {
                    strTrantype = "TM";
                }
                else
                {
                    strTrantype = "CK";
                }


                CorebankServiceESB055.MSG055 msg055 = new CorebankServiceESB055.MSG055();
                msg055.Header = new CorebankServiceESB055.HEADER();
                msg055.Header.VERSION = "1.0";
                msg055.Header.SENDER_CODE = "";
                msg055.Header.SENDER_NAME = "";
                msg055.Header.RECEIVER_CODE = "";
                msg055.Header.RECEIVER_NAME = "";
                msg055.Header.TRAN_CODE = "055";
                msg055.Header.MSG_ID = "";
                msg055.Header.SEND_DATE = DateTime.Now.ToString();

                msg055.Body = new CorebankServiceESB055.BODY();
                msg055.Body.CTU_HDR = new CorebankServiceESB055.CTU_HDR();
                msg055.Body.CTU_HDR.SO_THAM_CHIEU = ds.Tables[0].Rows[0]["so_ct_nh"].ToString();
                msg055.Body.CTU_HDR.SHKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                msg055.Body.CTU_HDR.NGAY_KB = ngay_kb;
                msg055.Body.CTU_HDR.MA_NV = ds.Tables[0].Rows[0]["ma_nv"].ToString();
                msg055.Body.CTU_HDR.NGAY_CT = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                msg055.Body.CTU_HDR.TK_CO = ds.Tables[0].Rows[0]["tk_co"].ToString();
                msg055.Body.CTU_HDR.MA_NNTHUE = ds.Tables[0].Rows[0]["ma_nnthue"].ToString();
                msg055.Body.CTU_HDR.TEN_NNTHUE = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                msg055.Body.CTU_HDR.DC_NNTHUE = ds.Tables[0].Rows[0]["dc_nnthue"].ToString();
                msg055.Body.CTU_HDR.MA_NNTIEN = ds.Tables[0].Rows[0]["ma_nntien"].ToString();
                msg055.Body.CTU_HDR.TEN_NNTIEN = ds.Tables[0].Rows[0]["ten_nntien"].ToString();
                msg055.Body.CTU_HDR.DC_NNTIEN = ds.Tables[0].Rows[0]["dc_nntien"].ToString();
                msg055.Body.CTU_HDR.NGAY_NNTIEN = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                msg055.Body.CTU_HDR.CQ_QD = ds.Tables[0].Rows[0]["cq_qd"].ToString();
                msg055.Body.CTU_HDR.MA_LTHUE = ds.Tables[0].Rows[0]["ma_lthue"].ToString(); //ma_lthue
                msg055.Body.CTU_HDR.MA_CQTHU = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
                msg055.Body.CTU_HDR.MA_DBHC = ma_dbhc;
                msg055.Body.CTU_HDR.MA_NH_A = strSender_Code;
                msg055.Body.CTU_HDR.MA_NH_B = ds.Tables[0].Rows[0]["ma_nh_gt"].ToString();
                msg055.Body.CTU_HDR.MA_NT = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                msg055.Body.CTU_HDR.TY_GIA = ds.Tables[0].Rows[0]["ty_gia"].ToString();
                msg055.Body.CTU_HDR.TTIEN = ds.Tables[0].Rows[0]["ttien"].ToString();
                msg055.Body.CTU_HDR.TTIEN_NT = ds.Tables[0].Rows[0]["ttien_nt"].ToString();
                msg055.Body.CTU_HDR.DIEN_GIAI = ds.Tables[0].Rows[0]["dien_giai"].ToString();
                msg055.Body.CTU_HDR.TG_KY = ds.Tables[0].Rows[0]["tg_ky"].ToString();
                msg055.Body.CTU_HDR.TRAN_TYPE = strTrantype;
                msg055.Body.CTU_HDR.KYHIEU_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
                msg055.Body.CTU_HDR.QUAN_NNTHUE = ds.Tables[0].Rows[0]["TEN_HUYEN_NNTHUE"].ToString();
                msg055.Body.CTU_HDR.TINH_NNTHUE = ds.Tables[0].Rows[0]["TEN_TINH_NNTHUE"].ToString();
                msg055.Body.CTU_HDR.QUAN_NNTIEN = ds.Tables[0].Rows[0]["TEN_HUYEN_NNTIEN"].ToString();
                msg055.Body.CTU_HDR.TINH_NNTIEN = ds.Tables[0].Rows[0]["TEN_TINH_NNTIEN"].ToString();

                string tk_kh = ds.Tables[0].Rows[0]["TK_KH_NH"].ToString();
                if (tk_kh.Length > 21)
                {
                    tk_kh = tk_kh.Substring(0, 21);
                }

                msg055.Body.CTU_HDR.TK_TRICHNO_KH = tk_kh;

                shkb = ds.Tables[0].Rows[0]["SHKB"].ToString();
                string DC_KB = Business.Common.mdlCommon.Get_TenTinh_KB_BL(Business.SP.SongPhuongController.GetMa_DBHC(shkb));
                msg055.Body.CTU_HDR.DC_KB = DC_KB;
                msg055.Body.CTU_HDR.TEN_CQTHU = ds.Tables[0].Rows[0]["TEN_CQTHU"].ToString();
                msg055.Body.CTU_HDR.LOAI_THU = ds.Tables[0].Rows[0]["HT_THU"].ToString();
                //build dtl
                msg055.Body.CTU_HDR.CTU_DTL = new CorebankServiceESB055.CTU_DTL();
                msg055.Body.CTU_HDR.CTU_DTL.ROW = new List<CorebankServiceESB055.ROW_DTL>();

                StringBuilder sbsql_dtl = new StringBuilder();
                sbsql_dtl.Append("SELECT ma_chuong,ma_ndkt,noi_dung,ttien sotien,ky_thue FROM  tcs_bienlai_hdr  WHERE SO_CT =:SO_CT");

                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_CT";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = so_ct;

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql_dtl.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không lấy được thông tin chứng từ DTL!");
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        //for
                        CorebankServiceESB055.ROW_DTL row_dtl = new CorebankServiceESB055.ROW_DTL();
                        row_dtl.MA_CHUONG = ds.Tables[0].Rows[i]["ma_chuong"].ToString();
                        row_dtl.MA_NDKT = ds.Tables[0].Rows[i]["ma_ndkt"].ToString();
                        row_dtl.NOI_DUNG = ds.Tables[0].Rows[i]["noi_dung"].ToString();
                        row_dtl.SOTIEN = ds.Tables[0].Rows[i]["sotien"].ToString();
                        //row_dtl.KY_THUE = ds.Tables[0].Rows[i]["ky_thue"].ToString();
                        row_dtl.KY_THUE = ds.Tables[0].Rows[i]["ky_thue"].ToString();
                        msg055.Body.CTU_HDR.CTU_DTL.ROW.Add(row_dtl);
                        //end for
                    }
                }

                //build ctbl
                msg055.Body.CTU_HDR.CTBL = new CorebankServiceESB055.CTBL();
                msg055.Body.CTU_HDR.CTBL.ROW = new List<CorebankServiceESB055.ROW_CTBL>();

                StringBuilder sbsql_ctbl = new StringBuilder();

                sbsql_ctbl.Append("SELECT to_char(tg_ky,'DD-MM-YYYY') as ngay_bl,so_bienlai,ma_tinh_nnthue,ten_tinh_nnthue,ma_huyen_nnthue,ten_huyen_nnthue,");
                sbsql_ctbl.Append("ma_cqqd,ten_cqqd,so_qd,to_char(ngay_qd,'dd/MM/yyyy') as ngay_qd,ma_lhthu,ten_lhthu,");
                sbsql_ctbl.Append("ly_do_vphc,ttien_vphc,ly_do_nop_cham,ttien_nop_cham ");
                sbsql_ctbl.Append(" FROM TCS_BIENLAI_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI in ('03','08')");


                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_CT";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = so_ct;

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql_ctbl.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không lấy được thông tin chứng từ biên lai ctbl!");
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //for
                    CorebankServiceESB055.ROW_CTBL row_ctbl = new CorebankServiceESB055.ROW_CTBL();
                    row_ctbl.NGAY_BL = ds.Tables[0].Rows[0]["ngay_bl"].ToString();
                    row_ctbl.KYHIEU_BL = "";
                    row_ctbl.SO_BL = ds.Tables[0].Rows[0]["so_bienlai"].ToString();
                    row_ctbl.MA_TINH_NNTIEN = ds.Tables[0].Rows[0]["ma_tinh_nnthue"].ToString();
                    row_ctbl.TEN_TINH_NNTIEN = ds.Tables[0].Rows[0]["ten_tinh_nnthue"].ToString();
                    row_ctbl.MA_HUYEN_NNTIEN = ds.Tables[0].Rows[0]["ma_huyen_nnthue"].ToString();
                    row_ctbl.TEN_HUYEN_NNTIEN = ds.Tables[0].Rows[0]["ten_huyen_nnthue"].ToString();
                    row_ctbl.MA_CQQD = ds.Tables[0].Rows[0]["ma_cqqd"].ToString();
                    row_ctbl.TEN_CQQD = ds.Tables[0].Rows[0]["ten_cqqd"].ToString();
                    row_ctbl.SO_QD = ds.Tables[0].Rows[0]["so_qd"].ToString();
                    row_ctbl.NGAY_QD = ds.Tables[0].Rows[0]["ngay_qd"].ToString();
                    row_ctbl.MA_LHTHU = ds.Tables[0].Rows[0]["ma_lhthu"].ToString();
                    row_ctbl.TEN_LHTHU = ds.Tables[0].Rows[0]["ten_lhthu"].ToString();
                    row_ctbl.LY_DO = ds.Tables[0].Rows[0]["ly_do_vphc"].ToString();
                    row_ctbl.TTIEN_VPHC = ds.Tables[0].Rows[0]["ttien_vphc"].ToString();
                    row_ctbl.LY_DO_NOP_CHAM = ds.Tables[0].Rows[0]["ly_do_nop_cham"].ToString();
                    row_ctbl.TTIEN_NOP_CHAM = ds.Tables[0].Rows[0]["ttien_nop_cham"].ToString();
                    msg055.Body.CTU_HDR.CTBL.ROW.Add(row_ctbl);
                    //end for
                }



                string xmlOut = msg055.MSG055toXML(msg055);

                xmlOut = xmlOut.Replace(strXMLdefout1, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout2, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout3, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout4, String.Empty);
                return xmlOut;

            }
            return "";
        }
        private static OracleConnection GetConnection()
        {
            OracleConnection _connection = null;
            try
            {
                _connection = new OracleConnection(VBOracleLib.DataAccess.strConnection);
                _connection.Open();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _connection;
        }

        public string CheckEmty(string svalue)
        {
            if (svalue.Trim().Length <= 0)
            {
                svalue = "EMPTY";
            }

            return svalue;
        }

        public void insertLogSP(string ID, string function_name, string shkb, string so_ct_nh, string so_ct_etax, string msg_out, string msg_in)
        {
            OracleConnection conn = GetConnection();
            OracleCommand myCMD = new OracleCommand();
            try
            {

                StringBuilder sbSqlInsert = new StringBuilder("");

                String strS = "INSERT INTO tcs_log_songphuong(id,time_update,shkb,function_name,so_ct_nh,so_ct_etax, msg_in, msg_out) values (:id, :time_update, :shkb, :function_name, :so_ct_nh, :so_ct_etax,:msg_in, :msg_out)";

                myCMD.Connection = conn;
                myCMD.CommandText = strS;
                myCMD.CommandType = CommandType.Text;
                myCMD.Parameters.Add("id", OracleType.VarChar).Value = ID;
                myCMD.Parameters.Add("time_update", OracleType.DateTime).Value = DateTime.Now;
                myCMD.Parameters.Add("shkb", OracleType.VarChar).Value = CheckEmty(shkb);
                myCMD.Parameters.Add("function_name", OracleType.VarChar).Value = CheckEmty(function_name);
                myCMD.Parameters.Add("so_ct_nh", OracleType.VarChar).Value = CheckEmty(so_ct_nh);
                myCMD.Parameters.Add("so_ct_etax", OracleType.VarChar).Value = CheckEmty(so_ct_etax);
                myCMD.Parameters.Add("msg_in", OracleType.Clob, 32767).Value = CheckEmty(msg_in);
                myCMD.Parameters.Add("msg_out", OracleType.Clob, 32767).Value = CheckEmty(msg_out);

                myCMD.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }
            finally
            {

                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    myCMD.Dispose();
                    conn.Dispose();

                    conn.Close();
                }
            }
        }
        public void updateLogSP(string ID, string msg_out)
        {
            OracleConnection conn = GetConnection();
            OracleCommand myCMD = new OracleCommand();

            try
            {

                StringBuilder sbSqlInsert = new StringBuilder("");

                String strS = "UPDATE tcs_log_songphuong SET msg_out =: msg_out,time_update =: time_update   WHERE ID =: id";

                myCMD.Connection = conn;
                myCMD.CommandText = strS;
                myCMD.CommandType = CommandType.Text;
                myCMD.Parameters.Add("id", OracleType.VarChar).Value = ID;
                myCMD.Parameters.Add("time_update", OracleType.DateTime).Value = DateTime.Now;
                myCMD.Parameters.Add("msg_out", OracleType.Clob, 32767).Value = CheckEmty(msg_out);

                myCMD.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }
            finally
            {

                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    myCMD.Dispose();
                    conn.Dispose();

                    conn.Close();
                }
            }
        }
        public string GetTenCQThu(string strMaCQThu, string strMa_HQ_PH)
        {
            string ten_cqthu = "";
            DataSet ds = null;

            try
            {
                StringBuilder sbSqlSelect = new StringBuilder("");
                if (strMa_HQ_PH.Length > 0)
                {
                    sbSqlSelect.Append("select ten from TCS_DM_CQTHU where MA_CQTHU='" + strMaCQThu + "' and ma_hq ='" + strMa_HQ_PH + "'");
                }
                else
                {
                    sbSqlSelect.Append("select ten from TCS_DM_CQTHU where MA_CQTHU='" + strMaCQThu + "' ");
                }

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbSqlSelect.ToString(), CommandType.Text);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ten_cqthu = ds.Tables[0].Rows[0][0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //log.Error("Lỗi khi SP GetTenCQThu: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                return "";
            }

            return ten_cqthu;
        }

        public string GetMaDBHC(string strSHKB)
        {
            string ma_db = "";
            DataSet ds = null;

            try
            {
                StringBuilder sbSqlSelect = new StringBuilder("");
                if (strSHKB.Length > 0)
                {
                    sbSqlSelect.Append("select ma_db from TCS_DM_KHOBAC where SHKB='" + strSHKB + "'");
                }
                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbSqlSelect.ToString(), CommandType.Text);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ma_db = ds.Tables[0].Rows[0][0].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); // LogDebug.WriteLog("Lỗi khi SP GetMaDBHC: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                return "";
            }

            return ma_db;
        }

        public string FormatNgay(string input)
        {
            //yyyyMMdd -> DD-MM-YYYY
            string result = "";
            string ngay = "";
            string thang = "";
            string nam = "";
            if (input.Length >= 8)
            {
                nam = input.Substring(0, 4);
                thang = input.Substring(4, 2);
                ngay = input.Substring(6, 2);
                result = ngay + "-" + thang + "-" + nam;
            }

            return result;
        }
        /// <summary>
        /// ham nay hiepvx sua lai do bo het dtl chi dung hdr
        /// </summary>
        /// <param name="so_ct"></param>
        /// <returns></returns>
        public string getMSG055_new(string so_ct)
        {

            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();

            sbsql.Append("SELECT shkb,ngay_kb, so_ct_nh,'0001' MA_NV,to_char(tg_ky,'DD-MM-YYYY') as ngay_ct,tk_co,ma_nnthue,ten_nnthue,dc_nnthue,ma_nntien,ten_nntien,dc_nntien,");
            sbsql.Append("cq_qd,ma_lthue,ma_cqthu,ma_nh_tt,ma_nh_gt,ma_nt,ty_gia,ttien,ttien_nt,dien_giai,");
            sbsql.Append("to_char(tg_ky,'DD-MM-YYYY HH24:MI:SS') as tg_ky,pt_tt,");
            sbsql.Append("KYHIEU_CT,TEN_HUYEN_NNTHUE,TEN_TINH_NNTHUE,TEN_HUYEN_NNTIEN,TEN_TINH_NNTIEN,TK_KH_NH,TEN_CQTHU,ht_thu");
            sbsql.Append(" FROM TCS_BIENLAI_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI in('03','08')");
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SO_CT";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = so_ct;

            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            if (ds.Tables[0].Rows.Count <= 0)
            {
                throw new Exception("Không lấy được thông tin chứng từ biên lai!");
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                string strPTTT = ds.Tables[0].Rows[0]["pt_tt"].ToString();
                string shkb = ds.Tables[0].Rows[0]["shkb"].ToString();
                string ma_dbhc = GetMaDBHC(shkb);
                string ngay_kb = ds.Tables[0].Rows[0]["ngay_kb"].ToString();
                ngay_kb = FormatNgay(ngay_kb);
                string strTrantype = "";
                if (strPTTT.Equals("05"))
                {
                    strTrantype = "TM";
                }
                else
                {
                    strTrantype = "CK";
                }


                CorebankServiceESB055.MSG055 msg055 = new CorebankServiceESB055.MSG055();
                msg055.Header = new CorebankServiceESB055.HEADER();
                msg055.Header.VERSION = "1.0";
                msg055.Header.SENDER_CODE = "";
                msg055.Header.SENDER_NAME = "";
                msg055.Header.RECEIVER_CODE = "";
                msg055.Header.RECEIVER_NAME = "";
                msg055.Header.TRAN_CODE = "055";
                msg055.Header.MSG_ID = "";
                msg055.Header.SEND_DATE = DateTime.Now.ToString();

                msg055.Body = new CorebankServiceESB055.BODY();
                msg055.Body.CTU_HDR = new CorebankServiceESB055.CTU_HDR();
                msg055.Body.CTU_HDR.SO_THAM_CHIEU = ds.Tables[0].Rows[0]["so_ct_nh"].ToString();
                msg055.Body.CTU_HDR.SHKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                msg055.Body.CTU_HDR.NGAY_KB = ngay_kb;
                msg055.Body.CTU_HDR.MA_NV = ds.Tables[0].Rows[0]["ma_nv"].ToString();
                msg055.Body.CTU_HDR.NGAY_CT = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                msg055.Body.CTU_HDR.TK_CO = ds.Tables[0].Rows[0]["tk_co"].ToString();
                msg055.Body.CTU_HDR.MA_NNTHUE = ds.Tables[0].Rows[0]["ma_nnthue"].ToString();
                msg055.Body.CTU_HDR.TEN_NNTHUE = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                msg055.Body.CTU_HDR.DC_NNTHUE = ds.Tables[0].Rows[0]["dc_nnthue"].ToString();
                msg055.Body.CTU_HDR.MA_NNTIEN = ds.Tables[0].Rows[0]["ma_nntien"].ToString();
                msg055.Body.CTU_HDR.TEN_NNTIEN = ds.Tables[0].Rows[0]["ten_nntien"].ToString();
                msg055.Body.CTU_HDR.DC_NNTIEN = ds.Tables[0].Rows[0]["dc_nntien"].ToString();
                msg055.Body.CTU_HDR.NGAY_NNTIEN = ds.Tables[0].Rows[0]["ngay_ct"].ToString();
                msg055.Body.CTU_HDR.CQ_QD = ds.Tables[0].Rows[0]["cq_qd"].ToString();
                msg055.Body.CTU_HDR.MA_LTHUE = ds.Tables[0].Rows[0]["ma_lthue"].ToString(); //ma_lthue
                msg055.Body.CTU_HDR.MA_CQTHU = ds.Tables[0].Rows[0]["ma_cqthu"].ToString();
                msg055.Body.CTU_HDR.MA_DBHC = ma_dbhc;
                msg055.Body.CTU_HDR.MA_NH_A = strSender_Code;
                msg055.Body.CTU_HDR.MA_NH_B = ds.Tables[0].Rows[0]["ma_nh_gt"].ToString();
                msg055.Body.CTU_HDR.MA_NT = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                msg055.Body.CTU_HDR.TY_GIA = ds.Tables[0].Rows[0]["ty_gia"].ToString();
                msg055.Body.CTU_HDR.TTIEN = ds.Tables[0].Rows[0]["ttien"].ToString();
                msg055.Body.CTU_HDR.TTIEN_NT = ds.Tables[0].Rows[0]["ttien_nt"].ToString();
                msg055.Body.CTU_HDR.DIEN_GIAI = ds.Tables[0].Rows[0]["dien_giai"].ToString();
                msg055.Body.CTU_HDR.TG_KY = ds.Tables[0].Rows[0]["tg_ky"].ToString();
                msg055.Body.CTU_HDR.TRAN_TYPE = strTrantype;
                msg055.Body.CTU_HDR.KYHIEU_CT = ds.Tables[0].Rows[0]["KYHIEU_CT"].ToString();
                msg055.Body.CTU_HDR.QUAN_NNTHUE = ds.Tables[0].Rows[0]["TEN_HUYEN_NNTHUE"].ToString();
                msg055.Body.CTU_HDR.TINH_NNTHUE = ds.Tables[0].Rows[0]["TEN_TINH_NNTHUE"].ToString();
                msg055.Body.CTU_HDR.QUAN_NNTIEN = ds.Tables[0].Rows[0]["TEN_HUYEN_NNTIEN"].ToString();
                msg055.Body.CTU_HDR.TINH_NNTIEN = ds.Tables[0].Rows[0]["TEN_TINH_NNTIEN"].ToString();

                string tk_kh = ds.Tables[0].Rows[0]["TK_KH_NH"].ToString();
                if (tk_kh.Length > 21)
                {
                    tk_kh = tk_kh.Substring(0, 21);
                }

                msg055.Body.CTU_HDR.TK_TRICHNO_KH = tk_kh;


                shkb = ds.Tables[0].Rows[0]["SHKB"].ToString();
                string DC_KB = Business.Common.mdlCommon.Get_TenTinh_KB_BL(Business.SP.SongPhuongController.GetMa_DBHC(shkb));
                msg055.Body.CTU_HDR.DC_KB = DC_KB;
                msg055.Body.CTU_HDR.TEN_CQTHU = ds.Tables[0].Rows[0]["TEN_CQTHU"].ToString();
                msg055.Body.CTU_HDR.LOAI_THU = ds.Tables[0].Rows[0]["HT_THU"].ToString();
                //build dtl
                msg055.Body.CTU_HDR.CTU_DTL = new CorebankServiceESB055.CTU_DTL();
                msg055.Body.CTU_HDR.CTU_DTL.ROW = new List<CorebankServiceESB055.ROW_DTL>();

                StringBuilder sbsql_dtl = new StringBuilder();
                sbsql_dtl.Append("SELECT ma_chuong,ma_ndkt,Ttien sotien,ky_thue,NOI_DUNG FROM  tcs_bienlai_hdr  WHERE SO_CT =:SO_CT");

                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_CT";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = so_ct;

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql_dtl.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không lấy được thông tin chứng từ DTL!");
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        //for
                        CorebankServiceESB055.ROW_DTL row_dtl = new CorebankServiceESB055.ROW_DTL();
                        row_dtl.MA_CHUONG = ds.Tables[0].Rows[i]["ma_chuong"].ToString();
                        row_dtl.MA_NDKT = ds.Tables[0].Rows[i]["ma_ndkt"].ToString();
                        row_dtl.NOI_DUNG = ds.Tables[0].Rows[i]["NOI_DUNG"].ToString();//Business.Common.mdlCommon.Get_TenMTM(row_dtl.MA_NDKT); //ds.Tables[0].Rows[i]["noi_dung"].ToString();
                        row_dtl.SOTIEN = ds.Tables[0].Rows[i]["sotien"].ToString();
                        row_dtl.KY_THUE = ds.Tables[0].Rows[i]["ky_thue"].ToString();
                        msg055.Body.CTU_HDR.CTU_DTL.ROW.Add(row_dtl);
                        //end for
                    }
                }

                //build ctbl
                msg055.Body.CTU_HDR.CTBL = new CorebankServiceESB055.CTBL();
                msg055.Body.CTU_HDR.CTBL.ROW = new List<CorebankServiceESB055.ROW_CTBL>();

                StringBuilder sbsql_ctbl = new StringBuilder();

                sbsql_ctbl.Append("SELECT to_char(tg_ky,'DD-MM-YYYY') as ngay_bl,so_bienlai,ma_tinh_nnthue,ten_tinh_nnthue,ma_huyen_nnthue,ten_huyen_nnthue,");
                sbsql_ctbl.Append("ma_cqqd,ten_cqqd,so_qd,to_char(ngay_qd,'dd/MM/yyyy') as ngay_qd,ma_lhthu,ten_lhthu,");
                sbsql_ctbl.Append("ly_do_vphc,ttien_vphc,ly_do_nop_cham,ttien_nop_cham ");
                sbsql_ctbl.Append(" FROM TCS_BIENLAI_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI in('03','08')");


                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_CT";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = so_ct;

                ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql_ctbl.ToString(), CommandType.Text, p);

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    throw new Exception("Không lấy được thông tin chứng từ biên lai ctbl!");
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //for
                    CorebankServiceESB055.ROW_CTBL row_ctbl = new CorebankServiceESB055.ROW_CTBL();
                    row_ctbl.NGAY_BL = ds.Tables[0].Rows[0]["ngay_bl"].ToString();
                    row_ctbl.KYHIEU_BL = "";
                    row_ctbl.SO_BL = ds.Tables[0].Rows[0]["so_bienlai"].ToString();
                    row_ctbl.MA_TINH_NNTIEN = ds.Tables[0].Rows[0]["ma_tinh_nnthue"].ToString();
                    row_ctbl.TEN_TINH_NNTIEN = ds.Tables[0].Rows[0]["ten_tinh_nnthue"].ToString();
                    row_ctbl.MA_HUYEN_NNTIEN = ds.Tables[0].Rows[0]["ma_huyen_nnthue"].ToString();
                    row_ctbl.TEN_HUYEN_NNTIEN = ds.Tables[0].Rows[0]["ten_huyen_nnthue"].ToString();
                    row_ctbl.MA_CQQD = ds.Tables[0].Rows[0]["ma_cqqd"].ToString();
                    row_ctbl.TEN_CQQD = ds.Tables[0].Rows[0]["ten_cqqd"].ToString();
                    row_ctbl.SO_QD = ds.Tables[0].Rows[0]["so_qd"].ToString();
                    row_ctbl.NGAY_QD = ds.Tables[0].Rows[0]["ngay_qd"].ToString();
                    row_ctbl.MA_LHTHU = ds.Tables[0].Rows[0]["ma_lhthu"].ToString();
                    row_ctbl.TEN_LHTHU = ds.Tables[0].Rows[0]["ten_lhthu"].ToString();
                    row_ctbl.LY_DO = ds.Tables[0].Rows[0]["ly_do_vphc"].ToString();
                    row_ctbl.TTIEN_VPHC = ds.Tables[0].Rows[0]["ttien_vphc"].ToString();
                    row_ctbl.LY_DO_NOP_CHAM = ds.Tables[0].Rows[0]["ly_do_nop_cham"].ToString();
                    row_ctbl.TTIEN_NOP_CHAM = ds.Tables[0].Rows[0]["ttien_nop_cham"].ToString();
                    msg055.Body.CTU_HDR.CTBL.ROW.Add(row_ctbl);
                    //end for
                }



                string xmlOut = msg055.MSG055toXML(msg055);

                xmlOut = xmlOut.Replace(strXMLdefout1, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout2, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout3, String.Empty);
                xmlOut = xmlOut.Replace(strXMLdefout4, String.Empty);
                return xmlOut;

            }
            return "";
        }
        public string SendBienLaiSP_New(string so_ct)
        {

            string REF_ID = "";
            string SO_TAI_KHOAN = "";
            string TEN_NNOP = "";
            string SO_REF_CORE = "";
            string SO_TIEN = "";
            string MA_NT = "";
            string SHKB = "";
            string MA_NH_PHANH = "";
            string TEN_NH_PHANH = "";
            string CONTEN_EX = "";
            string PAYMENT_DETAIL = "";
            string SOURCE_V = "";
            string CITAD_TYPE = "";
            string TRANG_THAI = "";
            string MSG = "";
            SOURCE_V = "SHB";
            //Lấy thông tin từ hdr để lấy thông tin
            DataSet ds = null;
            StringBuilder sbsql = new StringBuilder();

            sbsql.Append("SELECT tk_kh_nh,ten_nnthue, so_ct_nh,ttien,ma_nt,shkb,ma_nh_a,dien_giai FROM TCS_BIENLAI_HDR WHERE SO_CT =:SO_CT AND TRANG_THAI in('03','08') ");

            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SO_CT";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = so_ct;

            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text, p);

            if (ds.Tables[0].Rows.Count > 0)
            {
                REF_ID = so_ct;
                SO_TAI_KHOAN = ds.Tables[0].Rows[0]["tk_kh_nh"].ToString();
                TEN_NNOP = ds.Tables[0].Rows[0]["ten_nnthue"].ToString();
                SO_REF_CORE = ds.Tables[0].Rows[0]["so_ct_nh"].ToString();
                SO_TIEN = ds.Tables[0].Rows[0]["ttien"].ToString();
                MA_NT = ds.Tables[0].Rows[0]["ma_nt"].ToString();
                SHKB = ds.Tables[0].Rows[0]["shkb"].ToString();
                MA_NH_PHANH = ds.Tables[0].Rows[0]["ma_nh_a"].ToString();
                if (MA_NH_PHANH.Length <= 0)
                {
                    MA_NH_PHANH = strSender_Code;
                }
                TEN_NH_PHANH = strSender_Name;
                CONTEN_EX = "";
                PAYMENT_DETAIL = ds.Tables[0].Rows[0]["dien_giai"].ToString();
                TRANG_THAI = "00";


            }
            else
            {
                throw new Exception("Lỗi trong quá trình lấy thông tin chứng từ hdr");

            }

            MSG = getMSG055_new(so_ct);

            //MSG = "XXXX";

            if (SO_REF_CORE.Length <= 0)
            {
                SO_REF_CORE = "";
            }
            string ID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_songphuong.NextVal");
            insertLogSP(ID, "CHUNGTU", SHKB, SO_REF_CORE, so_ct, "", MSG);

            string result = "";

            OracleConnection conn = GetConnection();
            String strS = "SELECT INSERTDATAFROMSHB.proc_insert_datashb(:REF_ID,:SO_TAI_KHOAN,:TEN_NNOP,:SO_REF_CORE,:SO_TIEN,:MA_NT,:SHKB,:MA_NH_PHANH,:TEN_NH_PHANH,:CONTEN_EX,:PAYMENT_DETAIL,:SOURCE_V,:CITAD_TYPE,:TRANG_THAI,:MSG,sysdate) FROM DUAL";
            var cmd = new OracleCommand("strS", conn);
            //cmd.CommandType = CommandType.Text;
            cmd.CommandText = strS;
            var xmlParam = new OracleParameter("MSG", OracleType.Clob);
            cmd.Parameters.Add(xmlParam);

            // DO NOT assign the parameter value yet in this place

            cmd.Transaction = conn.BeginTransaction();
            try
            {
                // Assign value here, AFTER starting the TX
                xmlParam.Value = MSG;
                cmd.Parameters.Add("REF_ID", OracleType.NVarChar).Value = CheckEmty(REF_ID);
                cmd.Parameters.Add("SO_TAI_KHOAN", OracleType.NVarChar).Value = CheckEmty(SO_TAI_KHOAN);
                cmd.Parameters.Add("TEN_NNOP", OracleType.NVarChar).Value = CheckEmty(TEN_NNOP);
                cmd.Parameters.Add("SO_REF_CORE", OracleType.NVarChar).Value = SO_REF_CORE;
                cmd.Parameters.Add("SO_TIEN", OracleType.NVarChar).Value = CheckEmty(SO_TIEN);
                cmd.Parameters.Add("MA_NT", OracleType.NVarChar).Value = CheckEmty(MA_NT);
                cmd.Parameters.Add("SHKB", OracleType.NVarChar).Value = CheckEmty(SHKB);
                cmd.Parameters.Add("MA_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(MA_NH_PHANH);
                cmd.Parameters.Add("TEN_NH_PHANH", OracleType.NVarChar).Value = CheckEmty(TEN_NH_PHANH);
                cmd.Parameters.Add("CONTEN_EX", OracleType.NVarChar).Value = CheckEmty(CONTEN_EX);
                cmd.Parameters.Add("PAYMENT_DETAIL", OracleType.NVarChar).Value = CheckEmty(PAYMENT_DETAIL);
                cmd.Parameters.Add("SOURCE_V", OracleType.NVarChar).Value = CheckEmty(SOURCE_V);
                cmd.Parameters.Add("CITAD_TYPE", OracleType.NVarChar).Value = CheckEmty(CITAD_TYPE);
                cmd.Parameters.Add("TRANG_THAI", OracleType.NVarChar).Value = CheckEmty(TRANG_THAI);
                result = (String)cmd.ExecuteScalar();
                cmd.Transaction.Commit();
            }
            catch (OracleException)
            {
                cmd.Transaction.Rollback();
                result = "FALSE";
                return result;
            }
            finally
            {
                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    conn.Dispose();
                    conn.Close();
                }
            }

            updateLogSP(ID, result);


            if (result.ToUpper().Equals("SUCCESS"))
            {
                string strSQL = "";

                strSQL = "UPDATE tcs_bienlai_hdr SET TT_SP='2' WHERE so_ct='" + so_ct + "' ";
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
            }

            return result;
        }
    }
}
