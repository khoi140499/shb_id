﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CorebankServiceESB.MSGNN
{
    [Serializable]
    [XmlRootAttribute("PARAMETER", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSGNN
    {
        public MSGNN()
        {

        }
         [XmlElement("errorCode")]
        public string errorCode { get; set; }
         [XmlElement("errorDesc")]
        public string errorDesc { get; set; }
        [XmlElement("listHolidayDt")]
        public listHolidayDt listHolidayDt;


        public string MSGNNtoXML(MSGNN p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSGNN));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSGNN MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CorebankServiceESB.MSGNN.MSGNN));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CorebankServiceESB.MSGNN.MSGNN LoadedObjTmp = (CorebankServiceESB.MSGNN.MSGNN)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }

    }
    public class listHolidayDt
    {
        public listHolidayDt()
        { }
         [XmlElement("element")]
        public List<element> element;

    }
    public class element
    {
        public element()
        { }
        [XmlElement("holiDesc")]
        public string holiDesc { get; set; }
         [XmlElement("holiDt")]
        public string holiDt { get; set; }
    }


}
