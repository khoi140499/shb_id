﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CorebankServiceESB
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG063
    {
        public MSG063()
        {

        }

        [XmlElement("HEADER")]
        public HEADER Header;
        [XmlElement("BODY")]
        public BODY Body;
        [XmlElement("SECURITY")]
        public SECURITY Security;

        public string MSG063toXML(MSG063 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG063));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG063 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CorebankServiceESB.MSG063));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CorebankServiceESB.MSG063 LoadedObjTmp = (CorebankServiceESB.MSG063)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }
    }
    public class BODY
    {
        public BODY()
        {
        }

        [XmlElement("CTU_HDR")]
        public CTU_HDR CTU_HDR;
    }
    public class CTU_HDR
    {
        public CTU_HDR()
        {

        }
        [XmlElement(Order = 1)]
        public string SO_THAM_CHIEU { get; set; }
        [XmlElement(Order = 2)]
        public string SHKB { get; set; }
        [XmlElement(Order = 3)]
        public string NGAY_KB { get; set; }
        [XmlElement(Order = 4)]
        public string TRAN_TYPE { get; set; }
        [XmlElement(Order = 5)]
        public string NGAY_CT { get; set; }
        [XmlElement(Order = 6)]
        public string TK_NO { get; set; }
        [XmlElement(Order = 7)]
        public string TK_CO { get; set; }
        [XmlElement(Order = 8)]
        public string MA_NNTHUE { get; set; }
        [XmlElement(Order = 9)]
        public string TEN_NNTHUE { get; set; }
        [XmlElement(Order = 10)]
        public string DC_NNTHUE { get; set; }
        [XmlElement(Order = 11)]
        public string NGAY_NNTIEN { get; set; }
        [XmlElement(Order = 12)]
        public string MA_LTHUE { get; set; }
        [XmlElement(Order = 13)]
        public string MA_CQTHU { get; set; }
        [XmlElement(Order = 14)]
        public string MA_DBHC { get; set; }
        [XmlElement(Order = 15)]
        public string TK_KH_NHAN { get; set; }
        [XmlElement(Order = 16)]
        public string MA_NH_A { get; set; }
        [XmlElement(Order = 17)]
        public string MA_NH_B { get; set; }
        [XmlElement(Order = 18)]
        public string MA_NT { get; set; }
        [XmlElement(Order = 19)]
        public string TY_GIA { get; set; }
        [XmlElement(Order = 20)]
        public string TTIEN { get; set; }
        [XmlElement(Order = 21)]
        public string TTIEN_NT { get; set; }
        [XmlElement(Order = 22)]
        public string TRANG_THAI { get; set; }
        [XmlElement(Order = 23)]
        public string TG_KY { get; set; }
        [XmlElement(Order = 24)]
        public string MA_NNTIEN { get; set; }
        [XmlElement(Order = 25)]
        public string TEN_NNTIEN { get; set; }
        [XmlElement(Order = 26)]
        public string DC_NNTIEN { get; set; }
        [XmlElement(Order = 27)]
        public string MA_NV { get; set; }
        [XmlElement(Order = 28)]
        public string SO_BT { get; set; }
        [XmlElement(Order = 29)]
        public string MA_DTHU { get; set; }
        [XmlElement(Order = 30)]
        public string KYHIEU_CT { get; set; }
        [XmlElement(Order = 31)]
        public string SO_BK { get; set; }
        [XmlElement(Order = 32)]
        public string NGAY_BK { get; set; }
        [XmlElement(Order = 33)]
        public string TK_KH_NH { get; set; }
        [XmlElement(Order = 34)]
        public string NGAY_KH_NH { get; set; }
        [XmlElement(Order = 35)]
        public string TEN_KH_NHAN { get; set; }
        [XmlElement(Order = 36)]
        public string DIACHI_KH_NHAN { get; set; }
        [XmlElement(Order = 37)]
        public string MA_KHTK { get; set; }
        [XmlElement(Order = 38)]
        public string MA_HQ { get; set; }
        [XmlElement(Order = 39)]
        public string MA_HQ_PH { get; set; }
        [XmlElement(Order = 40)]
        public string CTU_BLT { get; set; }
        [XmlElement(Order = 41)]
        public string DIEN_GIAI { get; set; }
        [XmlElement(Order = 42)]
        public string TEN_CQTHU { get; set; }
        [XmlElement(Order = 43)]
        public string TEN_HQ { get; set; }
        [XmlElement(Order = 44)]
        public string TEN_HQ_PH { get; set; }
        
        [XmlElement(Order =45)]
        public string QUAN_NNTHUE { get; set; }
        [XmlElement(Order =46)]
        public string TINH_NNTHUE { get; set; }
        [XmlElement(Order = 47)]
        public string QUAN_NNTIEN { get; set; }
        [XmlElement(Order =48)]
        public string TINH_NNTIEN { get; set; }
        [XmlElement(Order =49)]
        public string DC_KB { get; set; }
        [XmlElement(Order = 50)]
        public CTU_DTL CTU_DTL;
    }
    public class CTU_DTL
    {
        public CTU_DTL()
        {

        }
        [XmlElement("ROW")]
        public List<ROW> ROW;


    }
    public class ROW
    {
        public ROW()
        {

        }
        [XmlElement(Order = 1)]
        public string SHKB { get; set; }
        [XmlElement(Order = 2)]
        public string NGAY_KB { get; set; }
        [XmlElement(Order = 3)]
        public string MA_CHUONG { get; set; }
        [XmlElement(Order = 4)]
        public string MA_NDKT { get; set; }
        [XmlElement(Order = 5)]
        public string NOI_DUNG { get; set; }
        [XmlElement(Order = 6)]
        public string SOTIEN { get; set; }
        [XmlElement(Order = 7)]
        public string SOTIEN_NT { get; set; }
        [XmlElement(Order = 8)]
        public string MA_NV { get; set; }
        [XmlElement(Order = 9)]
        public string SO_BT { get; set; }
        [XmlElement(Order = 10)]
        public string MA_DTHU { get; set; }
        [XmlElement(Order = 11)]
        public string MA_QUY { get; set; }
        [XmlElement(Order = 12)]
        public string STT_TK { get; set; }
        [XmlElement(Order = 13)]
        public string SO_TK { get; set; }
        [XmlElement(Order = 14)]
        public string NAM_DK { get; set; }
        [XmlElement(Order = 15)]
        public string SAC_THUE { get; set; }
    }

    public class HEADER
    {
        public HEADER()
        {
        }
        [XmlElement(Order = 1)]
        public string VERSION { get; set; }
        [XmlElement(Order = 2)]
        public string SENDER_CODE { get; set; }
        [XmlElement(Order = 3)]
        public string SENDER_NAME { get; set; }
        [XmlElement(Order = 4)]
        public string RECEIVER_CODE { get; set; }
        [XmlElement(Order = 5)]
        public string RECEIVER_NAME { get; set; }
        [XmlElement(Order = 6)]
        public string TRAN_CODE { get; set; }
        [XmlElement(Order = 7)]
        public string MSG_ID { get; set; }
        [XmlElement(Order = 8)]
        public string MSG_REFID { get; set; }
        [XmlElement(Order = 9)]
        public string ID_LINK { get; set; }
        [XmlElement(Order = 10)]
        public string SEND_DATE { get; set; }

    }
    public class SECURITY
    {
        public SECURITY()
        {
        }
        public string SIGNATURE { get; set; }

    }
}
