﻿using System.Reflection;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using VBOracleLib;
using System.Text;
using System.Diagnostics;

namespace CorebankServiceESB
{
    class clsLogCoreBank
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// InserLogToDataBase
        /// </summary>
        /// <param name="mesageIn"></param>
        /// <param name="messageOut"></param>
        /// <param name="date"></param>
        /// <param name="time"></param>
        /// <param name="teller"></param>
        /// <param name="methodName"></param>
        /// <param name="keyValueIn"></param>
        /// <param name="loainghiepvu"></param>
        /// <param name="idnghiepvu"></param>
        /// <param name="keyValueOut"></param>
        /// <param name="loaithamso"></param>
        public void InserLogToDataBase(string mesageIn, string messageOut, string date, string time, string teller, string methodName, string keyValueIn, string loainghiepvu, string idnghiepvu, string keyValueOut = null, string loaithamso = null)
        {
            try
            {
                var strIn = XmlToStringIn(mesageIn);
                var strOut = XmlToStringOut(messageOut);
                var id = Guid.NewGuid().ToString();
                const string sql = "insert into tcs_log_ts_corebank(id,xmltranfer,xmltranfer_output,tran_date,tran_time,teller,methodName,loai_ts,keyValueIn,keyValueOut,loai_nghiep_vu,id_nghiep_vu,ngay_ht) values(:id,:xmltranfer,:xmltranfer_output,:tran_date,:tran_time,:teller,:methodName,:loai_ts,:keyValueIn,:keyValueOut,:loai_nghiep_vu,:id_nghiep_vu,sysdate)";
                var arr = new List<IDataParameter>
                {
                    DataAccess.NewDBParameter("id", ParameterDirection.Input, id, DbType.String),
                    DataAccess.NewDBParameter("xmltranfer", ParameterDirection.Input, strIn, DbType.String),
                    DataAccess.NewDBParameter("xmltranfer_output", ParameterDirection.Input, strOut,
                        DbType.String),
                    DataAccess.NewDBParameter("tran_date", ParameterDirection.Input, date, DbType.String),
                    DataAccess.NewDBParameter("tran_time", ParameterDirection.Input, time, DbType.String),
                    DataAccess.NewDBParameter("teller", ParameterDirection.Input, teller, DbType.String),
                    DataAccess.NewDBParameter("methodName", ParameterDirection.Input, methodName,
                        DbType.String),
                    DataAccess.NewDBParameter("loai_ts", ParameterDirection.Input, loaithamso, DbType.String),
                    DataAccess.NewDBParameter("keyValueIn", ParameterDirection.Input, keyValueIn,
                        DbType.String),
                    DataAccess.NewDBParameter("keyValueOut", ParameterDirection.Input, keyValueOut,
                        DbType.String),
                    DataAccess.NewDBParameter("loai_nghiep_vu", ParameterDirection.Input, loainghiepvu,
                        DbType.String),
                    DataAccess.NewDBParameter("id_nghiep_vu", ParameterDirection.Input, idnghiepvu,
                        DbType.String)
                };
                DataAccess.ExecuteNonQuery(sql, CommandType.Text, arr.ToArray());
            }
            catch (Exception ex)
            {
                var sbErrMsg = new StringBuilder();
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null)
                    sbErrMsg.Append(MethodBase.GetCurrentMethod() + "@" +
                                    declaringType);
                sbErrMsg.Append("\n" + ex.Message);
                sbErrMsg.Append("\n" + ex.StackTrace);
                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// 
        public void InserLogToDataBaseTTSP(string mesageIn, string messageOut, string date, string time, string teller, string methodName, string keyValueIn, string loainghiepvu, string idnghiepvu, string keyValueOut = null, string loaithamso = null)
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                const string sql = "insert into tcs_log_ts_corebank(id,xmltranfer,xmltranfer_output,tran_date,tran_time,teller,methodName,loai_ts,keyValueIn,keyValueOut,loai_nghiep_vu,id_nghiep_vu,ngay_ht) values(:id,:xmltranfer,:xmltranfer_output,:tran_date,:tran_time,:teller,:methodName,:loai_ts,:keyValueIn,:keyValueOut,:loai_nghiep_vu,:id_nghiep_vu,sysdate)";
                var arr = new List<IDataParameter>
                {
                    DataAccess.NewDBParameter("id", ParameterDirection.Input, id, DbType.String),
                    DataAccess.NewDBParameter("xmltranfer", ParameterDirection.Input, mesageIn, DbType.String),
                    DataAccess.NewDBParameter("xmltranfer_output", ParameterDirection.Input, messageOut,
                        DbType.String),
                    DataAccess.NewDBParameter("tran_date", ParameterDirection.Input, date, DbType.String),
                    DataAccess.NewDBParameter("tran_time", ParameterDirection.Input, time, DbType.String),
                    DataAccess.NewDBParameter("teller", ParameterDirection.Input, teller, DbType.String),
                    DataAccess.NewDBParameter("methodName", ParameterDirection.Input, methodName,
                        DbType.String),
                    DataAccess.NewDBParameter("loai_ts", ParameterDirection.Input, loaithamso, DbType.String),
                    DataAccess.NewDBParameter("keyValueIn", ParameterDirection.Input, keyValueIn,
                        DbType.String),
                    DataAccess.NewDBParameter("keyValueOut", ParameterDirection.Input, keyValueOut,
                        DbType.String),
                    DataAccess.NewDBParameter("loai_nghiep_vu", ParameterDirection.Input, loainghiepvu,
                        DbType.String),
                    DataAccess.NewDBParameter("id_nghiep_vu", ParameterDirection.Input, idnghiepvu,
                        DbType.String)
                };
                DataAccess.ExecuteNonQuery(sql, CommandType.Text, arr.ToArray());
            }
            catch (Exception ex)
            {
                var sbErrMsg = new StringBuilder();
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null)
                    sbErrMsg.Append(MethodBase.GetCurrentMethod() + "@" +
                                    declaringType);
                sbErrMsg.Append("\n" + ex.Message);
                sbErrMsg.Append("\n" + ex.StackTrace);
                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void UpdateLogToDataBaseTTSP(string messageOut, string idnghiepvu)
        {
            try
            {
                const string sql = "update  tcs_log_ts_corebank set xmltranfer_output =: xmltranfer_output, ngay_ht=sysdate where id_nghiep_vu =: id_nghiep_vu and (ngay_ht > (sysdate - 150)) ";
                var arr = new List<IDataParameter>
                {
                    DataAccess.NewDBParameter("xmltranfer_output", ParameterDirection.Input, messageOut,DbType.String),
                    DataAccess.NewDBParameter("id_nghiep_vu", ParameterDirection.Input, idnghiepvu,DbType.String)
                };
                DataAccess.ExecuteNonQuery(sql, CommandType.Text, arr.ToArray());
            }
            catch (Exception ex)
            {
                var sbErrMsg = new StringBuilder();
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null)
                    sbErrMsg.Append(MethodBase.GetCurrentMethod() + "@" +
                                    declaringType);
                sbErrMsg.Append("\n" + ex.Message);
                sbErrMsg.Append("\n" + ex.StackTrace);
                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void WriteLog(string message)
        {
            log.Debug(message);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logDesc"></param>
        public void WriteLog(string message, string logDesc)
        {
            switch (logDesc)
            {
                case "Info":
                    log.Info(message);
                    break;

                case "Error":
                    log.Error(message);
                    break;

                case "Warn":
                    log.Warn(message);
                    break;

                default:
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="formName"></param>
        /// <param name="outputValue"></param>
        /// <param name="inputValue"></param>
        /// <param name="user"></param>
        /// <param name="logDesc"></param>
        public void WriteLog(string methodName, string formName, string outputValue, string inputValue, string user, string logDesc)
        {
            var message = BuildLogMessage(methodName, formName, outputValue, inputValue, user);
            switch (logDesc)
            {
                case "Info":
                    log.Info(message);
                    break;

                case "Error":
                    log.Error(message);
                    break;

                case "Warn":
                    log.Warn(message);
                    break;

                default:
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="formName"></param>
        /// <param name="outputValue"></param>
        /// <param name="inputValue"></param>
        /// <param name="user"></param>
        /// <returns>string</returns>
        private string BuildLogMessage(string methodName, string formName,string outputValue,string inputValue, string user)
        { 
        return "@Method : " +methodName +" - @Form :" + formName +" - @Input : "+inputValue +" - @Output : "+outputValue+" - @By : " + user;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns>string</returns>
        public string XmlToStringIn(string xmlString)
        {
            var strXmlOutput = xmlString.Replace("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:esb='http://esb.tud.m1tech.com.vn/'>", string.Empty);            
             strXmlOutput = strXmlOutput.Replace("<soap:Body>", string.Empty);
             strXmlOutput = strXmlOutput.Replace("</soap:Body>", string.Empty);
             strXmlOutput = strXmlOutput.Replace("</soap:Envelope>", string.Empty);        
        return strXmlOutput;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns>string</returns>
        public string XmlToStringOut(string xmlString)
        {
            var strXmlOutput = xmlString.Replace("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">", string.Empty);
            strXmlOutput = strXmlOutput.Replace("<soap:Body>", string.Empty);
            strXmlOutput = strXmlOutput.Replace("</soap:Body>", string.Empty);
            strXmlOutput = strXmlOutput.Replace("</soap:Envelope>", string.Empty);
            return strXmlOutput;
        }
    }
}
