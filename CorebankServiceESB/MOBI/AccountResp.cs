﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorebankServiceESB.MOBI
{
    public class AccountResp
    {
        public AccountResp()
        { }

        public string errorMess { get; set; }
        public string errorCode { get; set; }
        public string cCrrcy { get; set; }
        public string typeAccount { get; set; }
        public string cifNo { get; set; }
        public string branchCode { get; set; }
        public string avlBalance { get; set; }
        public string accNo { get; set; }
        public string accName { get; set; }
        public string mst { get; set; }
        public string loaiGiayTo { get; set; }
        public string soGiayTo { get; set; }
        public string dienThoai { get; set; }
        public string email { get; set; }
        public string ngayPhatHanh { get; set; }
    }
}
