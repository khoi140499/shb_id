﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorebankServiceESB.MOBI
{
    public class SendOTPInfo
    {
        public SendOTPInfo()
        { 
        }

        public string ERRCODE { get; set; }
        public string ERRMSG { get; set; }
        public string DESC { get; set; }
        public string TRANID { get; set; }
        public string loaiOTP { get; set; }
    }
}
