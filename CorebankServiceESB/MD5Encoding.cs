﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace CorebankServiceESB
{
    public class MD5Encoding
    {
        public static string Hash(String input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                if (hash[i] < 16)
                    sb.Append("0" + hash[i].ToString("x"));
                //ret += "0" + a.ToString ("x");
                else
                    sb.Append(hash[i].ToString("x"));
            }
            string str = sb.ToString().ToUpper();
            return str;
        }
    }
}
