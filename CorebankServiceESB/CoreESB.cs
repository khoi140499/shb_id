﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using VBOracleLib;
using System.Diagnostics;
using System.Data;
using System.Web;
using System.Data.OracleClient;
namespace CorebankServiceESB
{
    public class CoreESB
    {
      
        private static string Core_version = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString();
        string CHANNELID = ConfigurationManager.AppSettings.Get("CHANEL.CORE").ToString();
        string INTERFACEID = ConfigurationManager.AppSettings.Get("INTERFACE.CORE").ToString();
        string CHANNELID_NTDT = ConfigurationManager.AppSettings.Get("CHANEL.CORE.NTDT").ToString();
        string INTERFACEID_NTDT = ConfigurationManager.AppSettings.Get("INTERFACE.CORE.NTDT").ToString();
        string CHANNELID_HQ = ConfigurationManager.AppSettings.Get("CHANEL.CORE.HQ").ToString();
        string INTERFACEID_HQ = ConfigurationManager.AppSettings.Get("INTERFACE.CORE.HQ").ToString();
        string CHANNELID_HQ247 = ConfigurationManager.AppSettings.Get("CHANEL.CORE.HQ247").ToString();
        string INTERFACEID_HQ247 = ConfigurationManager.AppSettings.Get("INTERFACE.CORE.HQ247").ToString();
        string CORE_RPSF = ConfigurationManager.AppSettings.Get("CORE.LPSF").ToString();
        string CHANNEL_FEE = ConfigurationManager.AppSettings.Get("CHANEL.CORE.FEE").ToString();
        string INTERFACE_FEE = ConfigurationManager.AppSettings.Get("INTERFACE.CORE.FEE").ToString();
        //ItfId, ChnlId, SrcBranchCd
        string ItfId = ConfigurationManager.AppSettings.Get("FinancialPosting.ItfId").ToString();
        string ChnlId = ConfigurationManager.AppSettings.Get("FinancialPosting.ChnlId").ToString();
        string SrcBranchCd = ConfigurationManager.AppSettings.Get("FinancialPosting.SrcBranchCd").ToString();

        string Br_Code = "BR0001";
        string prefixSEQ = "TAX";
        string TT_THANHCONG = "00000";
        private static String Core_status = ConfigurationManager.AppSettings.Get("CORE.ON").ToString();
        private static String strConnection = ConfigurationManager.AppSettings.Get("CTC_DB_CONN").ToString();
        private static string MODE_CORETIME = "INTER";
        private static string SharedKeyMD5 = "A2C75E5C3A4C0E23D01871A7DB4A8D9E";

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string WSProcess(string msg, string type)
        {
            try
            {
                if (type == "HQ")
                {
                    if (Core_version == "2")
                    {
                        return send_PaymentHQ_V20(msg);
                    }
                    else
                    {
                        return send_PaymentHQ(msg);
                    }
                }
                if (type == "NTDT")
                {
                    if (Core_version == "2")
                    {
                        return send_PaymentNTDT_V20(msg);
                    }
                    else
                    {

                        return send_PaymentNTDT(msg);
                    }
                }
                if (type == "PBN")
                {
                    return send_PaymentPBN(msg);
                }
                if (type == "ACCOUNT")
                {
                    return GetTK_KH_NH(msg);
                }
                if (type == "SP")
                {
                    return send_PaymentSP(msg);
                }

                if (type == "NGAYNGHI")
                {

                    return GetDMNgayNghi(msg);
                }

                if (type == "CITAD")
                {

                    return GetDSCitad(msg);
                }

                if (type == "CONTENTEX")
                {
                    return SendContentEx(msg);
                }

                if (type == "test")
                {

                    TEST_V20();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("CoreESB.WSProcess: " + ex.Message, EventLogEntryType.Error);
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private string send_PaymentNTDT_V20(string msg)
        {

            try
            {
                System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                VXDOC.LoadXml(msg);
                string pv_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_ACCOUNTNO").InnerText;
                string pv_BEN_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_BEN_ACCOUNTNO").InnerText;
                string AMOUNT = VXDOC.SelectSingleNode("Customs/DATA/AMOUNT").InnerText;
                string CCY = VXDOC.SelectSingleNode("Customs/DATA/CCY").InnerText;
                string REMARK = VXDOC.SelectSingleNode("Customs/DATA/REMARK").InnerText;
                string TransactionDate = VXDOC.SelectSingleNode("Customs/DATA/TransactionDate").InnerText;

                string BEN_NAME = VXDOC.SelectSingleNode("Customs/DATA/BEN_NAME").InnerText;
                string BEN_CITY = VXDOC.SelectSingleNode("Customs/DATA/BEN_CITY").InnerText;
                string BEN_ADD1 = VXDOC.SelectSingleNode("Customs/DATA/BEN_ADD1").InnerText;
                string BEN_ADD2 = VXDOC.SelectSingleNode("Customs/DATA/BEN_ADD2").InnerText;
                string BANK_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/BANK_BEN_DESC").InnerText;
                string BR_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/BR_BEN_DESC").InnerText;
                string CITY_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/CITY_BEN_DESC").InnerText;
                string BANK_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/BANK_BEN_CODE").InnerText;
                string BR_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/BR_BEN_CODE").InnerText;
                string CITY_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/CITY_BEN_CODE").InnerText;
                string KenhHT = VXDOC.SelectSingleNode("Customs/DATA/KenhHT").InnerText;
                string v_strSo_CT = VXDOC.SelectSingleNode("Customs/DATA/v_strSo_CT").InnerText;
                string strMa_NH_B = VXDOC.SelectSingleNode("Customs/DATA/strMa_NH_B").InnerText;
                string v_strMa_NV = VXDOC.SelectSingleNode("Customs/DATA/v_strMa_NV").InnerText;

                string v_str_KS = VXDOC.SelectSingleNode("Customs/DATA/v_str_KS").InnerText;
                string v_KenhCT = VXDOC.SelectSingleNode("Customs/DATA/v_KenhCT").InnerText;
                string v_CustInfo = VXDOC.SelectSingleNode("Customs/DATA/customer_info").InnerText;
                string TaxPymtDate = VXDOC.SelectSingleNode("Customs/DATA/TaxPymtDate").InnerText;
                string TaxType = VXDOC.SelectSingleNode("Customs/DATA/TaxType").InnerText;
                string TaxCode = VXDOC.SelectSingleNode("Customs/DATA/TaxCode").InnerText;
                string v_MaCQT = VXDOC.SelectSingleNode("Customs/DATA/v_MaCQT").InnerText;
                string v_TenCQT = VXDOC.SelectSingleNode("Customs/DATA/v_TenCQT").InnerText;
                string v_MaDBHC = VXDOC.SelectSingleNode("Customs/DATA/v_MaDBHC").InnerText;
                string v_TenDBHC = VXDOC.SelectSingleNode("Customs/DATA/v_TenDBHC").InnerText;
                string TaxPayerName = VXDOC.SelectSingleNode("Customs/DATA/TaxPayerName").InnerText;
                string TaxPayerAdd = VXDOC.SelectSingleNode("Customs/DATA/TaxPayerAdd").InnerText;
                string EcomStr = VXDOC.SelectSingleNode("Customs/DATA/EcomStr").InnerText;
                string v_str_errcode = "";
                string v_str_ref_no = "";

                PaymentNTDT_V20(pv_ACCOUNTNO, pv_BEN_ACCOUNTNO, AMOUNT, CCY, REMARK, TransactionDate,
                                    BEN_NAME, BEN_CITY, BEN_ADD1, BEN_ADD2, BANK_BEN_DESC, BR_BEN_DESC, CITY_BEN_DESC,
                                    BANK_BEN_CODE, BR_BEN_CODE, CITY_BEN_CODE, KenhHT, v_strSo_CT, strMa_NH_B, v_strMa_NV,
                                    v_str_KS, v_KenhCT, v_CustInfo,
                                     TaxPymtDate, TaxType, TaxCode, v_MaCQT, v_TenCQT, v_MaDBHC, v_TenDBHC, TaxPayerName, TaxPayerAdd, EcomStr,
                                    ref  v_str_errcode, ref  v_str_ref_no);
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<Errcode>" + v_str_errcode + "</Errcode>");
                strBuildXML.Append("<Ref_no>" + v_str_ref_no + "</Ref_no>");
                strBuildXML.Append("</Customs>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                //VBOracleLib.LogApp.AddDebug("send_PaymentNTDT_V20", ex.StackTrace +" - " + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog(ex.Message, EventLogEntryType.Error);
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pv_ACCOUNTNO"></param>
        /// <param name="pv_BEN_ACCOUNTNO"></param>
        /// <param name="AMOUNT"></param>
        /// <param name="CCY"></param>
        /// <param name="REMARK"></param>
        /// <param name="TransactionDate"></param>
        /// <param name="BEN_NAME"></param>
        /// <param name="BEN_CITY"></param>
        /// <param name="BEN_ADD1"></param>
        /// <param name="BEN_ADD2"></param>
        /// <param name="BANK_BEN_DESC"></param>
        /// <param name="BR_BEN_DESC"></param>
        /// <param name="CITY_BEN_DESC"></param>
        /// <param name="BANK_BEN_CODE"></param>
        /// <param name="BR_BEN_CODE"></param>
        /// <param name="CITY_BEN_CODE"></param>
        /// <param name="KenhHT"></param>
        /// <param name="v_strSo_CT"></param>
        /// <param name="strMa_NH_B"></param>
        /// <param name="v_strMa_NV"></param>
        /// <param name="v_str_KS"></param>
        /// <param name="v_KenhCT"></param>
        /// <param name="v_CustInfo"></param>
        /// <param name="TaxPymtDate"></param>
        /// <param name="TaxType"></param>
        /// <param name="TaxCode"></param>
        /// <param name="v_MaCQT"></param>
        /// <param name="v_TenCQT"></param>
        /// <param name="v_MaDBHC"></param>
        /// <param name="v_TenDBHC"></param>
        /// <param name="TaxPayerName"></param>
        /// <param name="TaxPayerAdd"></param>
        /// <param name="EcomStr"></param>
        /// <param name="v_str_errcode"></param>
        /// <param name="v_str_ref_no"></param>
        /// <returns></returns>
        private string PaymentNTDT_V20(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
          string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
          string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
          string v_str_KS, string v_KenhCT, string v_CustInfo,
          string TaxPymtDate, string TaxType, string TaxCode, string v_MaCQT, string v_TenCQT, string v_MaDBHC, string v_TenDBHC, string TaxPayerName, string TaxPayerAdd, string EcomStr,
           ref string v_str_errcode, ref string v_str_ref_no)
        {

            string nodevale = "";
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = KenhHT + TCS_GetSequence("TCS_REF_NO_SEQ");
            string sysdate = DateTime.Now.ToString("yyyyMMdd");


            DomXfer.AppHdrType appHdr = new DomXfer.AppHdrType();
            appHdr.CharSet = "UTF-8";
            appHdr.SvcVer = "2.0";
            DomXfer.PairsType nsFrom = new DomXfer.PairsType();
            nsFrom.Id = "ETAX";
            nsFrom.Name = "ETAX";
            DomXfer.PairsType nsTo = new DomXfer.PairsType();
            nsTo.Id = "CORE";
            nsTo.Name = "CORE";
            DomXfer.PairsType[] listOfNsTo = new DomXfer.PairsType[1];
            listOfNsTo[0] = nsTo;
            DomXfer.PairsType BizSvc = new DomXfer.PairsType();
            BizSvc.Id = "DomXfer";
            BizSvc.Name = "DomXfer";

            DateTime TransDt = DateTime.Now;
            appHdr.From = nsFrom;
            appHdr.To = listOfNsTo;
            appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");// +TCS_GetSequence("TCS_REF_NO_SEQ");
            appHdr.MsgPreId = "";
            appHdr.BizSvc = BizSvc;
            appHdr.TransDt = TransDt;

            //Body            
            DomXfer.DomXferAddReqType msgReq = new DomXfer.DomXferAddReqType();
            msgReq.AppHdr = appHdr;


            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = CHANNELID_HQ;
            }
            else
            {
                if (v_KenhCT.Equals("ETAX_HQ247"))
                {
                    nodevale = CHANNELID_HQ247;
                }
                else
                {
                    nodevale = CHANNELID;
                }
                if (v_KenhCT.Equals("NTDT"))
                {
                    nodevale = CHANNELID_NTDT;
                }
            }
            msgReq.ChnlId = nodevale;//"NET";

            msgReq.BrCd = Br_Code; //"BR0001";

            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = INTERFACEID_HQ;
            }
            else
            {
                if (v_KenhCT.Equals("ETAX_HQ247"))
                {
                    nodevale = INTERFACEID_HQ247;
                }
                else
                {
                    nodevale = INTERFACEID;
                }
                if (v_KenhCT.Equals("NTDT"))
                {
                    nodevale = INTERFACEID_NTDT;
                }
            }
            msgReq.ItfId = nodevale; //"NETBNK";
            msgReq.RefNo = SEQUENCE_NO;//"NET" + TCS_GetSequence("TCS_REF_NO_SEQ");
            msgReq.DbAcct = pv_ACCOUNTNO.Trim(); ;//"1000000001";
            msgReq.CrAcct = pv_BEN_ACCOUNTNO; ;//"7111.0.2995342";
            msgReq.TxAmt = Decimal.Parse(AMOUNT);//AMOUNT
            msgReq.TxCur = CCY; //704
            msgReq.TxDt = VALUEDATE;//"20171029";
            //msgReq.TxDt = sysdate;//"20171029";
            msgReq.ValDt = VALUEDATE;//msgReq.ValDt = VALUEDATE;// "20171029";
            msgReq.WIB = "N";
            msgReq.BenName = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME); //"CC HQ CK Cang Sai Gon KV I";
            msgReq.BenAddr1 = "";
            msgReq.BenAdd2 = "";
            msgReq.BenCity = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_CITY);// "";
            msgReq.IdType = "";
            msgReq.IdNo = "";
            msgReq.IdIssuePlace = "";
            msgReq.IdExpDt = "";
            msgReq.CustRmk = VBOracleLib.Globals.RemoveSign4VietnameseString(REMARK);//"Test Seatech MST0305750381LThue04NgayNT20180130;NDKT1702C754S35891784NDThue GTGT hang nhap khau tru thue GTGT hang NK qua,KT;TK:10120893216NDK:30/01/2018LH:.";
            msgReq.IntRmk = VBOracleLib.Globals.RemoveSign4VietnameseString(v_CustInfo);// "";
            msgReq.BnkDesc = VBOracleLib.Globals.RemoveSign4VietnameseString(BANK_BEN_DESC);// "Agribank Dong Sai Gon";
            msgReq.BrDesc = "";
            msgReq.CtyCd = CITY_BEN_CODE;//"79";
            msgReq.CtyDesc = VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC);
            msgReq.BnkCode = Decimal.Parse(BANK_BEN_CODE);//Decimal.Parse("204");
            msgReq.BrCode = BR_BEN_CODE;//"002";
            msgReq.Mode = "INTER";
            msgReq.DealType = "";
            msgReq.ChrgType = "";
            msgReq.ConvRt = "";
            msgReq.EchoField = "E";
            //cac truong moi cua 2.0
            msgReq.BgInfoFlg = "Y";//"Y";
            msgReq.TaxPymtDate = TaxPymtDate;//"20180329";//Ngay_KB cua chung tu
            msgReq.TaxType = TaxType;//"04";//Ma loai thue
            msgReq.TaxCode = TaxCode;//"0300709284";//MST
            msgReq.ColACode = v_MaCQT;// "";
            msgReq.ColAName = VBOracleLib.Globals.RemoveSign4VietnameseString(v_TenCQT);// "";
            msgReq.LocaCode = v_MaDBHC;// "";
            msgReq.APenalty = VBOracleLib.Globals.RemoveSign4VietnameseString(v_TenDBHC); //"0";
            msgReq.TaxPayerName = VBOracleLib.Globals.RemoveSign4VietnameseString(TaxPayerName);//"Cong Ty Trach Nhiem Huu Han Mercedes - Benz - Viet Nam";//Ten nguoi nop thue
            msgReq.TaxPayerAdd = VBOracleLib.Globals.RemoveSign4VietnameseString(TaxPayerAdd);//"13 Duong Quang Trung";//Dic chi NNT
            msgReq.EcomStr = VBOracleLib.Globals.RemoveSign4VietnameseString(EcomStr);//"####1901C999~286916~Thue nhap khau~10095454380~29-03-2018###1702C999~267788~Thue gia tri gia tang hang nhap khau~10095454380~29-03-2018";

            appHdr.Signature = MD5Encoding.Hash(msgReq.DbAcct + msgReq.CrAcct + msgReq.TxAmt + msgReq.CustRmk + msgReq.CtyCd + msgReq.BnkCode + msgReq.BrCode + "A2C75E5C3A4C0E23D01871A7DB4A8D9E");


            string hdr_Tran_Id = "";
            string res_Status = "";
            string res_Result_Code = "";
            string res_Ref_No = "";
            string res_Avail_Bal = "";
            string res_Curr_Bal = "";
            string res_Acc_CCY_Cd = "";
            string res_Lcy_Amt = "";
            string err_code = "";
            string rm_ref_no = "";
            string so_du = "";
            string err_msg = "";
            //Get Response
            DomXfer.DomXferAddResType msgRes = new DomXfer.DomXferAddResType();
            if (Core_status == "1")
            {

                msgRes = sendMsg_Payment_V20(msgReq);
            }
            try
            {
                if (Core_status == "1")
                {
                    if (msgRes == null)
                    {
                        res_Result_Code = "TO999";
                        res_Ref_No = CORE_RPSF;
                        res_Status = "TO999";
                        v_str_errcode = "TO999";
                        err_code = "TO999";
                    }
                    else
                    {
                        hdr_Tran_Id = msgRes.AppHdr.MsgId;
                        res_Status = msgRes.RespSts.Sts;
                        //res_Result_Code = msgRes.RespSts.;
                        res_Result_Code = "00000";
                        res_Ref_No = msgRes.RefNo;
                        res_Avail_Bal = msgRes.AvailBal;
                        res_Curr_Bal = msgRes.CurrBal;
                        res_Lcy_Amt = msgRes.LcyAmt;
                        err_code = msgRes.RespSts.ErrCd;
                        err_msg = msgRes.RespSts.ErrMsg;
                        if (res_Status == "0")
                        {
                            res_Status = "00000";
                        }
                    }
                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }

                rm_ref_no = res_Ref_No;
                so_du = res_Curr_Bal;
                v_str_errcode = err_code;
                v_str_ref_no = rm_ref_no;

                string v_strSQLInsert;
                if (res_Status == "00000")
                {
                    v_str_ref_no = rm_ref_no;

                    v_strSQLInsert = ("INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," + (" CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SE" +
                    "LL_RATE," + (" AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO,USER_INPUT,KENH_CT,USE" +
                    "R_CHECKER) " + (" VALUES(\'TA001\',\'\',\'1\',SYSDATE,\'\',\'\',"
                                + ((" \'\',\'\',\'"
                                + (BANK_BEN_CODE + ("\',\'"
                                + (pv_ACCOUNTNO + ("\',\'\',\'VND\',\'10000000\',\'"
                                + (pv_BEN_ACCOUNTNO + ("\',\'"
                                + (strMa_NH_B + "\',\'10000000\',")))))))) + (" "
                                + (AMOUNT + (", 0 ,0 ,\'"
                                + (REMARK + ("\',\'"
                                + (res_Result_Code + ("\',\'"
                                + (rm_ref_no + ("\',\'\',\'\',\'"
                                + (v_strSo_CT + ("\',\'"
                                + (v_strMa_NV + ("\',\'"
                                + (v_KenhCT + ("\',\'"
                                + (v_str_KS + "\')")))))))))))))))))))));
                    VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                    v_str_errcode = "00000";
                }


            }
            catch (Exception ex)
            {
              //  VBOracleLib.LogApp.AddDebug("PaymentNTDT_V20", ex.StackTrace + "-" + ex.Message);
                //VBOracleLib.LogDebug.WriteLog(ex, "Loi khi hach toan chung tu", "clsCoreBank");
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog(ex.Message, EventLogEntryType.Error);
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }
            if (msgRes != null)
                return msgRes.ToString();
            else
                return "";
            //return msgRes.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private string send_PaymentNTDT(string msg)
        {

            try
            {
                System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                VXDOC.LoadXml(msg);
                string pv_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_ACCOUNTNO").InnerText;
                string pv_BEN_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_BEN_ACCOUNTNO").InnerText;
                string AMOUNT = VXDOC.SelectSingleNode("Customs/DATA/AMOUNT").InnerText;
                string CCY = VXDOC.SelectSingleNode("Customs/DATA/CCY").InnerText;
                string REMARK = VXDOC.SelectSingleNode("Customs/DATA/REMARK").InnerText;
                string TransactionDate = VXDOC.SelectSingleNode("Customs/DATA/TransactionDate").InnerText;

                string BEN_NAME = VXDOC.SelectSingleNode("Customs/DATA/BEN_NAME").InnerText;

                string BEN_CITY = VXDOC.SelectSingleNode("Customs/DATA/BEN_CITY").InnerText;
                string BEN_ADD1 = VXDOC.SelectSingleNode("Customs/DATA/BEN_ADD1").InnerText;
                string BEN_ADD2 = VXDOC.SelectSingleNode("Customs/DATA/BEN_ADD2").InnerText;
                string BANK_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/BANK_BEN_DESC").InnerText;
                string BR_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/BR_BEN_DESC").InnerText;
                string CITY_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/CITY_BEN_DESC").InnerText;

                string BANK_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/BANK_BEN_CODE").InnerText;
                string BR_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/BR_BEN_CODE").InnerText;
                string CITY_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/CITY_BEN_CODE").InnerText;
                string KenhHT = VXDOC.SelectSingleNode("Customs/DATA/KenhHT").InnerText;
                string v_strSo_CT = VXDOC.SelectSingleNode("Customs/DATA/v_strSo_CT").InnerText;
                string strMa_NH_B = VXDOC.SelectSingleNode("Customs/DATA/strMa_NH_B").InnerText;
                string v_strMa_NV = VXDOC.SelectSingleNode("Customs/DATA/v_strMa_NV").InnerText;

                string v_str_KS = VXDOC.SelectSingleNode("Customs/DATA/v_str_KS").InnerText;
                string v_KenhCT = VXDOC.SelectSingleNode("Customs/DATA/v_KenhCT").InnerText;
                string v_CustInfo = VXDOC.SelectSingleNode("Customs/DATA/customer_info").InnerText;
                string v_str_errcode = "";
                string v_str_ref_no = "";

                PaymentNTDT(pv_ACCOUNTNO, pv_BEN_ACCOUNTNO, AMOUNT, CCY, REMARK, TransactionDate,
                                    BEN_NAME, BEN_CITY, BEN_ADD1, BEN_ADD2, BANK_BEN_DESC, BR_BEN_DESC, CITY_BEN_DESC,
                                    BANK_BEN_CODE, BR_BEN_CODE, CITY_BEN_CODE, KenhHT, v_strSo_CT, strMa_NH_B, v_strMa_NV,
                                    v_str_KS, v_KenhCT, v_CustInfo, ref  v_str_errcode, ref  v_str_ref_no);
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<Errcode>" + v_str_errcode + "</Errcode>");
                strBuildXML.Append("<Ref_no>" + v_str_ref_no + "</Ref_no>");
                strBuildXML.Append("</Customs>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
              //  VBOracleLib.LogApp.AddDebug("send_PaymentNTDT", ex.StackTrace + "-" + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("send_PaymentNTDT: " + ex.Message, EventLogEntryType.Error);
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pv_ACCOUNTNO"></param>
        /// <param name="pv_BEN_ACCOUNTNO"></param>
        /// <param name="AMOUNT"></param>
        /// <param name="CCY"></param>
        /// <param name="REMARK"></param>
        /// <param name="TransactionDate"></param>
        /// <param name="BEN_NAME"></param>
        /// <param name="BEN_CITY"></param>
        /// <param name="BEN_ADD1"></param>
        /// <param name="BEN_ADD2"></param>
        /// <param name="BANK_BEN_DESC"></param>
        /// <param name="BR_BEN_DESC"></param>
        /// <param name="CITY_BEN_DESC"></param>
        /// <param name="BANK_BEN_CODE"></param>
        /// <param name="BR_BEN_CODE"></param>
        /// <param name="CITY_BEN_CODE"></param>
        /// <param name="KenhHT"></param>
        /// <param name="v_strSo_CT"></param>
        /// <param name="strMa_NH_B"></param>
        /// <param name="v_strMa_NV"></param>
        /// <param name="v_str_KS"></param>
        /// <param name="v_KenhCT"></param>
        /// <param name="v_CustInfo"></param>
        /// <param name="v_str_errcode"></param>
        /// <param name="v_str_ref_no"></param>
        /// <returns></returns>

        private string PaymentNTDT(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
             string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
             string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
             string v_str_KS, string v_KenhCT, string v_CustInfo, ref string v_str_errcode, ref string v_str_ref_no)
        {

            string nodevale = "";
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = KenhHT + TCS_GetSequence("TCS_REF_NO_SEQ");
            string sysdate = DateTime.Now.ToString("yyyyMMdd");

            DomXfer.AppHdrType appHdr = new DomXfer.AppHdrType();
            appHdr.CharSet = "UTF-8";
            appHdr.SvcVer = "1.0";
            DomXfer.PairsType nsFrom = new DomXfer.PairsType();
            nsFrom.Id = "ETAX";
            nsFrom.Name = "ETAX";
            DomXfer.PairsType nsTo = new DomXfer.PairsType();
            nsTo.Id = "CORE";
            nsTo.Name = "CORE";
            DomXfer.PairsType[] listOfNsTo = new DomXfer.PairsType[1];
            listOfNsTo[0] = nsTo;
            DomXfer.PairsType BizSvc = new DomXfer.PairsType();
            BizSvc.Id = "DomXfer";
            BizSvc.Name = "DomXfer";

            DateTime TransDt = DateTime.Now;
            appHdr.From = nsFrom;
            appHdr.To = listOfNsTo;
            appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");// +TCS_GetSequence("TCS_REF_NO_SEQ");
            appHdr.MsgPreId = "";
            appHdr.BizSvc = BizSvc;
            appHdr.TransDt = TransDt;

            //Body            
            DomXfer.DomXferAddReqType msgReq = new DomXfer.DomXferAddReqType();
            msgReq.AppHdr = appHdr;


            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = CHANNELID_HQ;
            }
            else
            {
                if (v_KenhCT.Equals("ETAX_HQ247"))
                {
                    nodevale = CHANNELID_HQ247;
                }
                else
                {
                    nodevale = CHANNELID;
                }
                if (v_KenhCT.Equals("NTDT"))
                {
                    nodevale = CHANNELID_NTDT;
                }
            }
            msgReq.ChnlId = nodevale;//"NET";

            msgReq.BrCd = Br_Code; //"BR0001";

            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = INTERFACEID_HQ;
            }
            else
            {
                if (v_KenhCT.Equals("ETAX_HQ247"))
                {
                    nodevale = INTERFACEID_HQ247;
                }
                else
                {
                    nodevale = INTERFACEID;
                }
                if (v_KenhCT.Equals("NTDT"))
                {
                    nodevale = INTERFACEID_NTDT;
                }
            }
            msgReq.ItfId = nodevale; //"NETBNK";
            msgReq.RefNo = SEQUENCE_NO;//"NET" + TCS_GetSequence("TCS_REF_NO_SEQ");
            msgReq.DbAcct = pv_ACCOUNTNO.Trim(); ;//"1000000001";
            msgReq.CrAcct = pv_BEN_ACCOUNTNO; ;//"7111.0.2995342";
            msgReq.TxAmt = Decimal.Parse(AMOUNT);//AMOUNT
            msgReq.TxCur = CCY;
            msgReq.TxDt = VALUEDATE;//"20171029";
            //msgReq.TxDt = sysdate;//"20171029";
            msgReq.ValDt = VALUEDATE;// VALUEDATE;// "20171029";
            msgReq.WIB = "N";
            msgReq.BenName = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME); //"CC HQ CK Cang Sai Gon KV I";
            msgReq.BenAddr1 = "";
            msgReq.BenAdd2 = "";
            msgReq.BenCity = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_CITY);// "";
            msgReq.IdType = "";
            msgReq.IdNo = "";
            msgReq.IdIssuePlace = "";
            msgReq.IdExpDt = "";
            msgReq.CustRmk = VBOracleLib.Globals.RemoveSign4VietnameseString(REMARK);//"Test Seatech MST0305750381LThue04NgayNT20180130;NDKT1702C754S35891784NDThue GTGT hang nhap khau tru thue GTGT hang NK qua,KT;TK:10120893216NDK:30/01/2018LH:.";
            msgReq.IntRmk = VBOracleLib.Globals.RemoveSign4VietnameseString(v_CustInfo);// "";
            msgReq.BnkDesc = VBOracleLib.Globals.RemoveSign4VietnameseString(BANK_BEN_DESC);// "Agribank Dong Sai Gon";
            msgReq.BrDesc = "";
            msgReq.CtyCd = CITY_BEN_CODE;//"79";
            msgReq.CtyDesc = VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC);
            msgReq.BnkCode = Decimal.Parse(BANK_BEN_CODE);//Decimal.Parse("204");
            msgReq.BrCode = BR_BEN_CODE;//"002";
            msgReq.Mode = "INTER";
            msgReq.DealType = "";
            msgReq.ChrgType = "";
            msgReq.ConvRt = "";
            msgReq.EchoField = "E";
            appHdr.Signature = MD5Encoding.Hash(msgReq.DbAcct + msgReq.CrAcct + msgReq.TxAmt + msgReq.CustRmk + msgReq.CtyCd + msgReq.BnkCode + msgReq.BrCode + "A2C75E5C3A4C0E23D01871A7DB4A8D9E");
            string hdr_Tran_Id = "";
            string res_Status = "";
            string res_Result_Code = "";
            string res_Ref_No = "";
            string res_Avail_Bal = "";
            string res_Curr_Bal = "";
            string res_Acc_CCY_Cd = "";
            string res_Lcy_Amt = "";
            string err_code = "";
            string rm_ref_no = "";
            string so_du = "";
            string err_msg = "";
            //Get Response
            DomXfer.DomXferAddResType msgRes = new DomXfer.DomXferAddResType();
            if (Core_status == "1")
            {

                msgRes = sendMsg_Payment(msgReq, v_strSo_CT);
            }
            try
            {
                if (Core_status == "1")
                {
                    if (msgRes == null)
                    {
                        res_Result_Code = "TO999";
                        res_Ref_No = CORE_RPSF;
                        res_Status = "TO999";
                        v_str_errcode = "TO999";
                        err_code =  "TO999";
                     
                    }
                    else
                    {
                        hdr_Tran_Id = msgRes.AppHdr.MsgId;
                        res_Status = msgRes.RespSts.Sts;
                        //res_Result_Code = msgRes.RespSts.;
                        res_Result_Code = "00000";
                        res_Ref_No = msgRes.RefNo;
                        res_Avail_Bal = msgRes.AvailBal;
                        res_Curr_Bal = msgRes.CurrBal;
                        res_Lcy_Amt = msgRes.LcyAmt;
                        err_code = msgRes.RespSts.ErrCd;
                        err_msg = msgRes.RespSts.ErrMsg;
                        if (res_Status == "0")
                        {
                            res_Status = "00000";
                        }
                    }
                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                   
                }

                rm_ref_no = res_Ref_No;
                so_du = res_Curr_Bal;
                v_str_errcode = err_code;
                 v_str_ref_no= rm_ref_no;

                string v_strSQLInsert;
                if (res_Status == "00000")
                {
                    v_str_ref_no = rm_ref_no;

                    v_strSQLInsert = ("INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," + (" CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SE" +
                    "LL_RATE," + (" AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO,USER_INPUT,KENH_CT,USE" +
                    "R_CHECKER) " + (" VALUES(\'TA001\',\'\',\'1\',SYSDATE,\'\',\'\',"
                                + ((" \'\',\'\',\'"
                                + (BANK_BEN_CODE + ("\',\'"
                                + (pv_ACCOUNTNO + ("\',\'\',\'VND\',\'10000000\',\'"
                                + (pv_BEN_ACCOUNTNO + ("\',\'"
                                + (strMa_NH_B + "\',\'10000000\',")))))))) + (" "
                                + (AMOUNT + (", 0 ,0 ,\'"
                                + (REMARK + ("\',\'"
                                + (res_Result_Code + ("\',\'"
                                + (rm_ref_no + ("\',\'\',\'\',\'"
                                + (v_strSo_CT + ("\',\'"
                                + (v_strMa_NV + ("\',\'"
                                + (v_KenhCT + ("\',\'"
                                + (v_str_KS + "\')")))))))))))))))))))));
                    VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                    v_str_errcode = "00000";
                }


            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
               // VBOracleLib.LogApp.AddDebug("PaymentNTDT", ex.StackTrace + "-" + ex.Message);
                //VBOracleLib.LogDebug.WriteLog(ex, "Loi khi hach toan chung tu", "clsCoreBank");
              //  LogDebug.WriteLog("send_PaymentNTDT: " + ex.Message, EventLogEntryType.Error);
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }
            if (msgRes != null)
                return msgRes.ToString();
            else
                return "";
            //return msgRes.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private string send_PaymentHQ_V20(string msg)
        {

            try
            {
                System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                VXDOC.LoadXml(msg);
                string pv_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_ACCOUNTNO").InnerText;
                string pv_BEN_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_BEN_ACCOUNTNO").InnerText;
                string AMOUNT = VXDOC.SelectSingleNode("Customs/DATA/AMOUNT").InnerText;
                string CCY = VXDOC.SelectSingleNode("Customs/DATA/CCY").InnerText;
                string REMARK = VXDOC.SelectSingleNode("Customs/DATA/REMARK").InnerText;
                string TransactionDate = VXDOC.SelectSingleNode("Customs/DATA/TransactionDate").InnerText;

                string BEN_NAME = VXDOC.SelectSingleNode("Customs/DATA/BEN_NAME").InnerText;
                string BEN_CITY = VXDOC.SelectSingleNode("Customs/DATA/BEN_CITY").InnerText;
                string BEN_ADD1 = VXDOC.SelectSingleNode("Customs/DATA/BEN_ADD1").InnerText;
                string BEN_ADD2 = VXDOC.SelectSingleNode("Customs/DATA/BEN_ADD2").InnerText;
                string BANK_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/BANK_BEN_DESC").InnerText;
                string BR_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/BR_BEN_DESC").InnerText;
                string CITY_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/CITY_BEN_DESC").InnerText;

                string BANK_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/BANK_BEN_CODE").InnerText;
                string BR_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/BR_BEN_CODE").InnerText;
                string CITY_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/CITY_BEN_CODE").InnerText;
                string KenhHT = VXDOC.SelectSingleNode("Customs/DATA/KenhHT").InnerText;
                string v_strSo_CT = VXDOC.SelectSingleNode("Customs/DATA/v_strSo_CT").InnerText;
                string strMa_NH_B = VXDOC.SelectSingleNode("Customs/DATA/strMa_NH_B").InnerText;
                string v_strMa_NV = VXDOC.SelectSingleNode("Customs/DATA/v_strMa_NV").InnerText;

                string v_str_KS = VXDOC.SelectSingleNode("Customs/DATA/v_str_KS").InnerText;
                string v_KenhCT = VXDOC.SelectSingleNode("Customs/DATA/v_KenhCT").InnerText;
                string v_CustInfo = VXDOC.SelectSingleNode("Customs/DATA/v_CustInfo").InnerText;
                string TaxPymtDate = VXDOC.SelectSingleNode("Customs/DATA/TaxPymtDate").InnerText;
                string TaxType = VXDOC.SelectSingleNode("Customs/DATA/TaxType").InnerText;
                string TaxCode = VXDOC.SelectSingleNode("Customs/DATA/TaxCode").InnerText;
                string v_MaCQT = VXDOC.SelectSingleNode("Customs/DATA/v_MaCQT").InnerText;
                string v_TenCQT = VXDOC.SelectSingleNode("Customs/DATA/v_TenCQT").InnerText;
                string v_MaDBHC = VXDOC.SelectSingleNode("Customs/DATA/v_MaDBHC").InnerText;
                string v_TenDBHC = VXDOC.SelectSingleNode("Customs/DATA/v_TenDBHC").InnerText;
                string TaxPayerName = VXDOC.SelectSingleNode("Customs/DATA/TaxPayerName").InnerText;
                string TaxPayerAdd = VXDOC.SelectSingleNode("Customs/DATA/TaxPayerAdd").InnerText;
                string EcomStr = VXDOC.SelectSingleNode("Customs/DATA/EcomStr").InnerText;
                string v_str_errcode = "";
                string v_str_ref_no = "";
                PaymentTQ_V20(pv_ACCOUNTNO, pv_BEN_ACCOUNTNO, AMOUNT, CCY, REMARK, TransactionDate,
                                    BEN_NAME, BEN_CITY, BEN_ADD1, BEN_ADD2, BANK_BEN_DESC, BR_BEN_DESC, CITY_BEN_DESC,
                                    BANK_BEN_CODE, BR_BEN_CODE, CITY_BEN_CODE, KenhHT, v_strSo_CT, strMa_NH_B, v_strMa_NV,
                                    v_str_KS, v_KenhCT, v_CustInfo,
                                    TaxPymtDate, TaxType, TaxCode, v_MaCQT, v_TenCQT, v_MaDBHC, v_TenDBHC, TaxPayerName, TaxPayerAdd, EcomStr,
                                    ref  v_str_errcode, ref  v_str_ref_no);
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<Errcode>" + v_str_errcode + "</Errcode>");
                strBuildXML.Append("<Ref_no>" + v_str_ref_no + "</Ref_no>");
                strBuildXML.Append("</Customs>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("CoreESB.SendPaymentHQ_V20", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("CoreESB.SendPaymentHQ_V20: " + ex.Message, EventLogEntryType.Error);
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pv_ACCOUNTNO"></param>
        /// <param name="pv_BEN_ACCOUNTNO"></param>
        /// <param name="AMOUNT"></param>
        /// <param name="CCY"></param>
        /// <param name="REMARK"></param>
        /// <param name="TransactionDate"></param>
        /// <param name="BEN_NAME"></param>
        /// <param name="BEN_CITY"></param>
        /// <param name="BEN_ADD1"></param>
        /// <param name="BEN_ADD2"></param>
        /// <param name="BANK_BEN_DESC"></param>
        /// <param name="BR_BEN_DESC"></param>
        /// <param name="CITY_BEN_DESC"></param>
        /// <param name="BANK_BEN_CODE"></param>
        /// <param name="BR_BEN_CODE"></param>
        /// <param name="CITY_BEN_CODE"></param>
        /// <param name="KenhHT"></param>
        /// <param name="v_strSo_CT"></param>
        /// <param name="strMa_NH_B"></param>
        /// <param name="v_strMa_NV"></param>
        /// <param name="v_str_KS"></param>
        /// <param name="v_KenhCT"></param>
        /// <param name="v_CustInfo"></param>
        /// <param name="TaxPymtDate"></param>
        /// <param name="TaxType"></param>
        /// <param name="TaxCode"></param>
        /// <param name="v_MaCQT"></param>
        /// <param name="v_TenCQT"></param>
        /// <param name="v_MaDBHC"></param>
        /// <param name="v_TenDBHC"></param>
        /// <param name="TaxPayerName"></param>
        /// <param name="TaxPayerAdd"></param>
        /// <param name="EcomStr"></param>
        /// <param name="v_str_errcode"></param>
        /// <param name="v_str_ref_no"></param>
        /// <returns></returns>
        private string PaymentTQ_V20(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
             string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
             string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
             string v_str_KS, string v_KenhCT, string v_CustInfo,
             string TaxPymtDate, string TaxType, string TaxCode, string v_MaCQT, string v_TenCQT, string v_MaDBHC, string v_TenDBHC, string TaxPayerName, string TaxPayerAdd, string EcomStr,
            ref string v_str_errcode, ref string v_str_ref_no)
        {

            string nodevale = "";
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = KenhHT + TCS_GetSequence("TCS_REF_NO_SEQ");
            string sysdate = DateTime.Now.ToString("yyyyMMdd");
            DomXfer.AppHdrType appHdr = new DomXfer.AppHdrType();
            appHdr.CharSet = "UTF-8";
            appHdr.SvcVer = "2.0";
            DomXfer.PairsType nsFrom = new DomXfer.PairsType();
            nsFrom.Id = "ETAX";
            nsFrom.Name = "ETAX";
            DomXfer.PairsType nsTo = new DomXfer.PairsType();
            nsTo.Id = "CORE";
            nsTo.Name = "CORE";
            DomXfer.PairsType[] listOfNsTo = new DomXfer.PairsType[1];
            listOfNsTo[0] = nsTo;
            DomXfer.PairsType BizSvc = new DomXfer.PairsType();
            BizSvc.Id = "DomXfer";
            BizSvc.Name = "DomXfer";
            DateTime TransDt = DateTime.Now;
            appHdr.From = nsFrom;
            appHdr.To = listOfNsTo;
            appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");// +TCS_GetSequence("TCS_REF_NO_SEQ");
            appHdr.MsgPreId = "";
            appHdr.BizSvc = BizSvc;
            appHdr.TransDt = TransDt;
            //Body            
            DomXfer.DomXferAddReqType msgReq = new DomXfer.DomXferAddReqType();
            msgReq.AppHdr = appHdr;
            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = CHANNELID_HQ;
            }
            else
            {
                if (v_KenhCT.Equals("ETAX_HQ247"))
                {
                    nodevale = CHANNELID_HQ247;
                }
                else
                {
                    nodevale = CHANNELID;
                }
                if (v_KenhCT.Equals("NTDT"))
                {
                    nodevale = CHANNELID_NTDT;
                }
            }
            msgReq.ChnlId = nodevale;//"NET";

            msgReq.BrCd = Br_Code; //"BR0001";

            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = INTERFACEID_HQ;
            }
            else
            {
                if (v_KenhCT.Equals("ETAX_HQ247"))
                {
                    nodevale = INTERFACEID_HQ247;
                }
                else
                {
                    nodevale = INTERFACEID;
                }
                if (v_KenhCT.Equals("NTDT"))
                {
                    nodevale = INTERFACEID_NTDT;
                }
            }
            msgReq.ItfId = nodevale; //"NETBNK";
            msgReq.RefNo = SEQUENCE_NO;//"NET" + TCS_GetSequence("TCS_REF_NO_SEQ");
            msgReq.DbAcct = pv_ACCOUNTNO.Trim(); ;//"1000000001";
            msgReq.CrAcct = pv_BEN_ACCOUNTNO; ;//"7111.0.2995342";
            msgReq.TxAmt = Decimal.Parse(AMOUNT);//AMOUNT
            msgReq.TxCur = "704";
            msgReq.TxDt = VALUEDATE;//"20171029";
            //msgReq.TxDt = sysdate;//"20171029";
            msgReq.ValDt = VALUEDATE;//msgReq.ValDt = VALUEDATE;// "20171029";
            msgReq.WIB = "N";
            msgReq.BenName = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME); //"CC HQ CK Cang Sai Gon KV I";
            msgReq.BenAddr1 = "";
            msgReq.BenAdd2 = "";
            msgReq.BenCity = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_CITY);// "";
            msgReq.IdType = "";
            msgReq.IdNo = "";
            msgReq.IdIssuePlace = "";
            msgReq.IdExpDt = "";
            msgReq.CustRmk = VBOracleLib.Globals.RemoveSign4VietnameseString(REMARK);//"Test Seatech MST0305750381LThue04NgayNT20180130;NDKT1702C754S35891784NDThue GTGT hang nhap khau tru thue GTGT hang NK qua,KT;TK:10120893216NDK:30/01/2018LH:.";
            msgReq.IntRmk = VBOracleLib.Globals.RemoveSign4VietnameseString(v_CustInfo);// "";
            msgReq.BnkDesc = VBOracleLib.Globals.RemoveSign4VietnameseString(BANK_BEN_DESC);// "Agribank Dong Sai Gon";
            msgReq.BrDesc = "";
            msgReq.CtyCd = CITY_BEN_CODE;//"79";
            msgReq.CtyDesc = VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC);
            msgReq.BnkCode = Decimal.Parse(BANK_BEN_CODE);//Decimal.Parse("204");
            msgReq.BrCode = BR_BEN_CODE;//"";"002";
            msgReq.Mode = "INTER";
            msgReq.DealType = "";
            msgReq.ChrgType = "";
            msgReq.ConvRt = "";
            msgReq.EchoField = "E";
            //cac truong moi cua 2.0
            msgReq.BgInfoFlg = "Y";//"Y";
            msgReq.TaxPymtDate = TaxPymtDate;//"20180329";//Ngay_KB cua chung tu
            msgReq.TaxType = TaxType;//"04";//Ma loai thue
            msgReq.TaxCode = TaxCode;//"0300709284";//MST
            msgReq.ColACode = v_MaCQT;//"";
            msgReq.ColAName = VBOracleLib.Globals.RemoveSign4VietnameseString(v_TenCQT);// "";
            msgReq.LocaCode = v_MaDBHC;//"";
            msgReq.APenalty = VBOracleLib.Globals.RemoveSign4VietnameseString(v_TenDBHC);//"0";
            msgReq.TaxPayerName = VBOracleLib.Globals.RemoveSign4VietnameseString(TaxPayerName);//"Cong Ty Trach Nhiem Huu Han Mercedes - Benz - Viet Nam";//Ten nguoi nop thue
            msgReq.TaxPayerAdd = VBOracleLib.Globals.RemoveSign4VietnameseString(TaxPayerAdd);//"13 Duong Quang Trung";//Dic chi NNT
            msgReq.EcomStr = VBOracleLib.Globals.RemoveSign4VietnameseString(EcomStr);//"####1901C999~286916~Thue nhap khau~10095454380~29-03-2018###1702C999~267788~Thue gia tri gia tang hang nhap khau~10095454380~29-03-2018";
            appHdr.Signature = MD5Encoding.Hash(msgReq.DbAcct + msgReq.CrAcct + msgReq.TxAmt + msgReq.CustRmk + msgReq.CtyCd + msgReq.BnkCode + msgReq.BrCode + "A2C75E5C3A4C0E23D01871A7DB4A8D9E");
            string hdr_Tran_Id = "";
            string res_Status = "";
            string res_Result_Code = "";
            string res_Ref_No = "";
            string res_Avail_Bal = "";
            string res_Curr_Bal = "";
            string res_Acc_CCY_Cd = "";
            string res_Lcy_Amt = "";
            string err_code = "";
            string rm_ref_no = "";
            string so_du = "";
            string err_msg = "";
            //Get Response
            DomXfer.DomXferAddResType msgRes = new DomXfer.DomXferAddResType();
            if (Core_status == "1")
            {
                msgRes = sendMsg_Payment_V20(msgReq);
            }
            try
            {
                if (Core_status == "1")
                {
                    if (msgRes == null)
                    {
                        res_Result_Code = "TO999";
                        res_Ref_No = CORE_RPSF;
                        res_Status = "TO999";
                        err_code = "TO999";
                    }
                    else
                    {
                        hdr_Tran_Id = msgRes.AppHdr.MsgId;
                        res_Status = msgRes.RespSts.Sts;
                        //res_Result_Code = msgRes.RespSts.ErrCd;
                        res_Result_Code = "00000";
                        res_Ref_No = msgRes.RefNo;
                        res_Avail_Bal = msgRes.AvailBal;
                        res_Curr_Bal = msgRes.CurrBal;
                        res_Lcy_Amt = msgRes.LcyAmt;
                        err_code = msgRes.RespSts.ErrCd;
                        err_msg = msgRes.RespSts.ErrMsg;
                        if (res_Status == "0")
                        {
                            res_Status = "00000";
                        }
                    }
                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }
                rm_ref_no = res_Ref_No;
                so_du = res_Curr_Bal;
                v_str_errcode = err_code;
                v_str_ref_no = rm_ref_no;
                string v_strSQLInsert;
                if (res_Status == "00000")
                {
                    v_str_ref_no = rm_ref_no;
                    v_strSQLInsert = ("INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," + (" CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SE" +
                    "LL_RATE," + (" AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO,USER_INPUT,KENH_CT,USE" +
                    "R_CHECKER) " + (" VALUES(\'TA001\',\'\',\'1\',SYSDATE,\'\',\'\',"
                                + ((" \'\',\'\',\'"
                                + (BANK_BEN_CODE + ("\',\'"
                                + (pv_ACCOUNTNO + ("\',\'\',\'VND\',\'10000000\',\'"
                                + (pv_BEN_ACCOUNTNO + ("\',\'"
                                + (strMa_NH_B + "\',\'10000000\',")))))))) + (" "
                                + (AMOUNT + (", 0 ,0 ,\'"
                                + (REMARK + ("\',\'"
                                + (res_Result_Code + ("\',\'"
                                + (rm_ref_no + ("\',\'\',\'\',\'"
                                + (v_strSo_CT + ("\',\'"
                                + (v_strMa_NV + ("\',\'"
                                + (v_KenhCT + ("\',\'"
                                + (v_str_KS + "\')")))))))))))))))))))));
                    VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                    v_str_errcode = "00000";
                }
            }
            catch (Exception ex)
            {
                //VBOracleLib.LogDebug.WriteLog(ex, "Loi khi hach toan chung tu", "clsCoreBank");
                log.Error(ex.Message + "-" + ex.StackTrace);//  VBOracleLib.LogApp.AddDebug("CoreESB.PaymentTQ_V20", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("CoreESB.PaymentTQ_V20: " + ex.Message, EventLogEntryType.Error);
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }
            if (msgRes != null)
                return msgRes.ToString();
            else
                return "";
        }
        private DomXfer.DomXferAddResType sendMsg_Payment_V20(DomXfer.DomXferAddReqType MsgCnt)
        {
            string strID = Business.Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_HDR_ID.NextVal");
            string strSQL = "";
            strSQL = "INSERT INTO TCS_LOG_CORE (ID,TIME_UPDATE,DESCRIPTION) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += DomXferAddReqTypeToString_V20(MsgCnt);
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                DomXfer.PortTypeClient porttypeClient = new DomXfer.PortTypeClient();
                DomXfer.DomXferAddResType returndata = porttypeClient.Create(MsgCnt);
                strSQL = "UPDATE TCS_LOG_CORE SET MSG_IN = '";
                string kq = DomXferAddResTypeToString(returndata);
                strSQL += kq.ToString().Trim().Replace("\0", "") + "' WHERE ID=";
                strSQL += strID;
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                return returndata;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//  VBOracleLib.LogApp.AddDebug("CoreESB.sendMsg_Payment_V20", ex.StackTrace + "-" + ex.Message);
              //  LogDebug.WriteLog("CoreESB.sendMsg_Payment_V20: " + ex.Message, EventLogEntryType.Error);
                return null;
            }

        }
        private string send_PaymentPBN(string msg)
        {
            try
            {
                System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                VXDOC.LoadXml(msg);
                string v_KenhCT = VXDOC.SelectSingleNode("Customs/DATA/v_KenhCT").InnerText;
                string strMA_CN = VXDOC.SelectSingleNode("Customs/DATA/strMA_CN").InnerText;
                string strREF_NO = VXDOC.SelectSingleNode("Customs/DATA/strREF_NO").InnerText;
                string strSalary = VXDOC.SelectSingleNode("Customs/DATA/strSalary").InnerText;
                string strBalancer = VXDOC.SelectSingleNode("Customs/DATA/strBalancer").InnerText;
                string strFEF_SEQ = VXDOC.SelectSingleNode("Customs/DATA/strFEF_SEQ").InnerText;
                string strSEQ_NO = VXDOC.SelectSingleNode("Customs/DATA/strSEQ_NO").InnerText;
                string FINANCIAL = VXDOC.SelectSingleNode("Customs/DATA/FINANCIAL").InnerText;
                string strACC_NO = VXDOC.SelectSingleNode("Customs/DATA/strACC_NO").InnerText;
                string strCredit = VXDOC.SelectSingleNode("Customs/DATA/strCredit").InnerText;
                string strTRANSACTIONAMOUNT = VXDOC.SelectSingleNode("Customs/DATA/strTRANSACTIONAMOUNT").InnerText;
                string strTRANSACTIONCURRENCY = VXDOC.SelectSingleNode("Customs/DATA/strTRANSACTIONCURRENCY").InnerText;
                string strREMARKS = VXDOC.SelectSingleNode("Customs/DATA/strREMARKS").InnerText;
                string v_str_errcode = "";
                string v_str_ref_no = "";

                PaymentPBN(v_KenhCT, strMA_CN, strREF_NO, strSalary, strBalancer, strFEF_SEQ, strSEQ_NO,
                     FINANCIAL, strACC_NO, strCredit, strTRANSACTIONAMOUNT, strTRANSACTIONCURRENCY, strREMARKS, ref  v_str_errcode, ref  v_str_ref_no);
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<Errcode>" + v_str_errcode + "</Errcode>");
                strBuildXML.Append("<Ref_no>" + v_str_ref_no + "</Ref_no>");
                strBuildXML.Append("</Customs>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("CoreESB.send_PaymentPBN: " + ex.Message, EventLogEntryType.Error);
            }
            return null;
        }
        private string PaymentPBN(string v_KenhCT, string strMA_CN, string strREF_NO, string strSalary, string strBalancer, string strFEF_SEQ, string strSEQ_NO,
           string FINANCIAL, string strACC_NO, string strCredit, string strTRANSACTIONAMOUNT, string strTRANSACTIONCURRENCY, string strREMARKS, ref string v_str_errcode, ref string v_str_ref_no)
        {
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = v_KenhCT + TCS_GetSequence("TCS_REF_NO_SEQ");
            return null;
        }
        private string GetTK_KH_NH(string acountNo)
        {
            try
            {
                string MA_LOI = "";
                string SO_DU_KD = "";
                string BRANCHID = "";
                //VBOracleLib.LogApp.AddDebug("1.GetTK_KH_NH", "1.GetTK_KH_NH.ACCOUNT:" + acountNo);
                //LogDebug.WriteLog("1.GetTK_KH_NH.ACCOUNT:" + acountNo, EventLogEntryType.Error);
                string strReturn = fnc_GetTK_KH_NH(acountNo, ref MA_LOI, ref SO_DU_KD, ref BRANCHID);
               // VBOracleLib.LogApp.AddDebug("2.GetTK_KH_NH", "1.GetTK_KH_NH.ACCOUNT.BRANCHID:" + BRANCHID);
                //LogDebug.WriteLog("1.GetTK_KH_NH.ACCOUNT.BRANCHID:" + BRANCHID, EventLogEntryType.Error);
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<strReturn>" + strReturn + "</strReturn>");
                strBuildXML.Append("<MA_LOI>" + MA_LOI + "</MA_LOI>");
                strBuildXML.Append("<SO_DU_KD>" + SO_DU_KD + "</SO_DU_KD>");
                strBuildXML.Append("<BRANCHID>" + BRANCHID + "</BRANCHID>");
                strBuildXML.Append("</Customs>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("Exception.GetTK_KH_NH", ex.StackTrace + "-" + ex.Message);
              //  LogDebug.WriteLog("Exception.GetTK_KH_NH: " + ex.Message, EventLogEntryType.Error);
            }
            return null;
        }
        private string fnc_GetTK_KH_NH(string acountNo, ref string MA_LOI, ref string SO_DU_KD, ref string BRANCHID)
        {
            try
            {
                string CIF_NO = "";
                DataSet ds = null;
                //VBOracleLib.LogApp.AddDebug("fnc_GetTK_KH_NH", "0.fnc_GetTK_KH_NH.ACCOUNT:" + acountNo);
                //LogDebug.WriteLog("0.fnc_GetTK_KH_NH.ACCOUNT:" + acountNo, EventLogEntryType.Error);
                string sbsql = "SELECT  cif_no  FROM table (tcs_pck_util_shb.get_ups('" + acountNo + "','1','A'))";
                try
                {
                    if (Core_status == "1")
                    {
                        ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text);
                        CIF_NO = ds.Tables[0].Rows[0]["cif_no"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message + "-" + ex.StackTrace);//VBOracleLib.LogApp.AddDebug("fnc_GetTK_KH_NH", ex.StackTrace + "-" + ex.Message);
                   // VBOracleLib.LogDebug.WriteLog(ex, "Loi khi lay thong tin tai khoan", "clsCoreBank");
                }
                //VBOracleLib.LogApp.AddDebug("fnc_GetTK_KH_NH", "1.fnc_GetTK_KH_NH.CIF_NO:" + CIF_NO);
                //VBOracleLib.LogApp.AddDebug("fnc_GetTK_KH_NH", "2.fnc_GetTK_KH_NH.ACCOUNT:" + acountNo);
                //LogDebug.WriteLog("1.fnc_GetTK_KH_NH.CIF_NO:" + CIF_NO, EventLogEntryType.Error);
                //LogDebug.WriteLog("2.fnc_GetTK_KH_NH.ACCOUNT:" + acountNo, EventLogEntryType.Error);
                AcctInfo.AppHdrType appHdr = new AcctInfo.AppHdrType();
                appHdr.CharSet = "UTF-8";
                appHdr.SvcVer = "1.0";

                AcctInfo.PairsType nsFrom = new AcctInfo.PairsType();
                nsFrom.Id = "ETAX";
                nsFrom.Name = "ETAX";

                AcctInfo.PairsType nsTo = new AcctInfo.PairsType();
                nsTo.Id = "CORE";
                nsTo.Name = "CORE";

                AcctInfo.PairsType[] listOfNsTo = new AcctInfo.PairsType[1];
                listOfNsTo[0] = nsTo;

                AcctInfo.PairsType BizSvc = new AcctInfo.PairsType();
                BizSvc.Id = "AcctInfo";
                BizSvc.Name = "AcctInfo";
                DateTime TransDt = DateTime.Now;
                appHdr.From = nsFrom;
                appHdr.To = listOfNsTo;
                appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff"); //+ TCS_GetSequence("TCS_REF_NO_SEQ");
                appHdr.MsgPreId = "11111111";
                appHdr.BizSvc = BizSvc;
                appHdr.TransDt = TransDt;

                AcctInfo.AcctInfoInqReqType msgReq = new AcctInfo.AcctInfoInqReqType();
                msgReq.AppHdr = appHdr;
                msgReq.CustId = CIF_NO;

                AcctInfo.BankAcctIdType bankAcct = new AcctInfo.BankAcctIdType();
                //bankAcct.AcctCur = "VND";
                bankAcct.AcctId = acountNo;
                bankAcct.AcctType = "CA";
                // bankAcct.AcctType = "ACTIVE";
                msgReq.AcctInfo = bankAcct;
                //AcctInfo.PortTypeClient ptc = new AcctInfo.PortTypeClient();
                //VBOracleLib.LogApp.AddDebug("fnc_GetTK_KH_NH", "msg truy vấn account" + msgReq);
                //LogDebug.WriteLog("msg truy vấn account" + msgReq, System.Diagnostics.EventLogEntryType.Information);
                string sErrorCode = "";
                string sErrorMSg = "";
                AcctInfo.AcctInfoInqResType msgRes = sendMsg(msgReq, ref sErrorCode, ref sErrorMSg);
                if (sErrorCode == "TO999")
                {
                    SO_DU_KD = "0";
                    MA_LOI = sErrorCode;
                    return ("<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE>" +
                    "<RETCODE>299</RETCODE><ERRCODE>" + sErrorMSg + "</ERRCODE></PARAMS></PARAMETER>");
                }
                //VBOracleLib.LogApp.AddDebug("fnc_GetTK_KH_NH", "msg respon" + msgRes);
                //LogDebug.WriteLog("msg respon" + msgRes, System.Diagnostics.EventLogEntryType.Information);
                if (msgRes.RespSts.Sts == "0")
                {
                    //Tim account dung trong list account tra ve

                    for (int i = 0; i < msgRes.AcctRec.Length; i++)
                    {
                        //if (msgRes.AcctRec[i].AcctInfo.AcctCur == CIF_NO && msgRes.AcctRec[i].AcctInfo.AcctId == acountNo)
                        //{
                        if (msgRes.AcctRec[i].DepositAcctRec.AcctSts == "INACTIVE")
                        {
                            return ("<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><" +
                               "RETCODE>999</RETCODE><ERRCODE>ACCOUNT IS INACTIVE</ERRCODE></PARAMS></PARAMETER>");

                        }
                        else
                        {
                            //if (msgRes.AcctRec[i].DepositAcctRec.CcyCd != "VND")
                            //{
                            //    return ("<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><" +
                            //    "RETCODE>999</RETCODE><ERRCODE>CURENTCY NOT AVAILABLE</ERRCODE></PARAMS></PARAMETER>");
                            //}
                            //else
                            //{
                            //    MA_LOI = msgRes.RespSts.Sts;
                            //    SO_DU_KD = msgRes.AcctRec[i].DepositAcctRec.AvaiBal;
                            //    return ("<PARAMETER><PARAMS><BANKACCOUNT>"
                            //                    + (msgRes.AcctRec[i].AcctInfo.AcctId + ("</BANKACCOUNT>" + ("<ACY_AVL_BAL>"
                            //                    + (SO_DU_KD + ("</ACY_AVL_BAL><CRR_CY_CODE>"
                            //                    + (msgRes.AcctRec[i].DepositAcctRec.CcyCd + ("</CRR_CY_CODE>" + ("<RETCODE>"
                            //                    + ("00000" + ("</RETCODE><ERRCODE>"
                            //                    + ("00000" + "</ERRCODE></PARAMS></PARAMETER>"))))))))))));
                            //}
                            ////}
                            MA_LOI = msgRes.RespSts.Sts;
                            SO_DU_KD = msgRes.AcctRec[i].DepositAcctRec.AvaiBal;
                            BRANCHID = msgRes.AcctRec[i].DepositAcctRec.BranchInfo.BankId;
                            return ("<PARAMETER><PARAMS><BANKACCOUNT>"
                                            + (msgRes.AcctRec[i].AcctInfo.AcctId + ("</BANKACCOUNT>" + ("<ACY_AVL_BAL>"
                                            + (SO_DU_KD + ("</ACY_AVL_BAL><CRR_CY_CODE>"
                                            + (msgRes.AcctRec[i].DepositAcctRec.CcyCd + ("</CRR_CY_CODE><BRANCHID>"
                                            + (BRANCHID + ("</BRANCHID>" + ("<RETCODE>"
                                            + ("00000" + ("</RETCODE><ERRCODE>"
                                            + ("00000" + "</ERRCODE></PARAMS></PARAMETER>"))))))))))))));
                        }

                    }
                    SO_DU_KD = "0";
                    MA_LOI = "NO ACCOUNT";
                    return ("<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><" +
                   "RETCODE>"
                               + ("999" + ("</RETCODE><ERRCODE>"
                               + ("NO ACCOUNT" + "</ERRCODE></PARAMS></PARAMETER>"))));

                }
                else
                { //Nêu xay ra loi
                    SO_DU_KD = "0";
                    MA_LOI = msgRes.RespSts.ErrCd;
                    return ("<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><" +
                           "RETCODE>"
                                       + (msgRes.AppHdr.MsgPreId + ("</RETCODE><ERRCODE>"
                                       + (msgRes.RespSts.Sts + "</ERRCODE></PARAMS></PARAMETER>"))));

                }

            }
            catch (Exception ex)
            {
                SO_DU_KD = "0";
                MA_LOI = ex.Message.ToString();
                string ret = ("<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><" +
                "RETCODE>99</RETCODE><ERRCODE>" + (ex.Message.ToString() + "</ERRCODE></PARAMS></PARAMETER>"));
              //  VBOracleLib.LogApp.AddDebug("fnc_GetTK_KH_NH", ex.StackTrace + "-" + ex.Message);
                //VBOracleLib.LogDebug.WriteLog(ex, "Loi khi lay thong tin tai khoan", "clsCoreBank");
                log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("fnc_GetTK_KH_NH: " + ex.Message, EventLogEntryType.Error);
                return ret;
            }
            return null;
        }
        private string send_PaymentHQ(string msg)
        {

            try
            {
                VBOracleLib.LogApp.AddDebug("send_PaymentHQ", "1.send_PaymentHQ:" + msg);
                //LogDebug.WriteLog("1.send_PaymentHQ:" + msg, EventLogEntryType.Information);
                System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                VXDOC.LoadXml(msg);
                string pv_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_ACCOUNTNO").InnerText;
                string pv_BEN_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_BEN_ACCOUNTNO").InnerText;
                string AMOUNT = VXDOC.SelectSingleNode("Customs/DATA/AMOUNT").InnerText;
                string CCY = VXDOC.SelectSingleNode("Customs/DATA/CCY").InnerText;
                string REMARK = VXDOC.SelectSingleNode("Customs/DATA/REMARK").InnerText;
                string TransactionDate = VXDOC.SelectSingleNode("Customs/DATA/TransactionDate").InnerText;

                string BEN_NAME = VXDOC.SelectSingleNode("Customs/DATA/BEN_NAME").InnerText;
                string BEN_CITY = VXDOC.SelectSingleNode("Customs/DATA/BEN_CITY").InnerText;
                string BEN_ADD1 = VXDOC.SelectSingleNode("Customs/DATA/BEN_ADD1").InnerText;
                string BEN_ADD2 = VXDOC.SelectSingleNode("Customs/DATA/BEN_ADD2").InnerText;
                string BANK_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/BANK_BEN_DESC").InnerText;
                string BR_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/BR_BEN_DESC").InnerText;
                string CITY_BEN_DESC = VXDOC.SelectSingleNode("Customs/DATA/CITY_BEN_DESC").InnerText;

                string BANK_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/BANK_BEN_CODE").InnerText;
                string BR_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/BR_BEN_CODE").InnerText;
                string CITY_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/CITY_BEN_CODE").InnerText;
                string KenhHT = VXDOC.SelectSingleNode("Customs/DATA/KenhHT").InnerText;
                string v_strSo_CT = VXDOC.SelectSingleNode("Customs/DATA/v_strSo_CT").InnerText;
                string strMa_NH_B = VXDOC.SelectSingleNode("Customs/DATA/strMa_NH_B").InnerText;
                string v_strMa_NV = VXDOC.SelectSingleNode("Customs/DATA/v_strMa_NV").InnerText;

                string v_str_KS = VXDOC.SelectSingleNode("Customs/DATA/v_str_KS").InnerText;
                string v_KenhCT = VXDOC.SelectSingleNode("Customs/DATA/v_KenhCT").InnerText;
                string v_CustInfo = VXDOC.SelectSingleNode("Customs/DATA/v_CustInfo").InnerText;
                string v_str_errcode = "";
                string v_str_ref_no = "";
                PaymentTQ(pv_ACCOUNTNO, pv_BEN_ACCOUNTNO, AMOUNT, CCY, REMARK, TransactionDate,
                                    BEN_NAME, BEN_CITY, BEN_ADD1, BEN_ADD2, BANK_BEN_DESC, BR_BEN_DESC, CITY_BEN_DESC,
                                    BANK_BEN_CODE, BR_BEN_CODE, CITY_BEN_CODE, KenhHT, v_strSo_CT, strMa_NH_B, v_strMa_NV,
                                    v_str_KS, v_KenhCT, v_CustInfo, ref  v_str_errcode, ref  v_str_ref_no);
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<Errcode>" + v_str_errcode + "</Errcode>");
                strBuildXML.Append("<Ref_no>" + v_str_ref_no + "</Ref_no>");
                strBuildXML.Append("</Customs>");
                VBOracleLib.LogApp.AddDebug("send_PaymentHQ", "2.send_PaymentHQ:" + strBuildXML.ToString());
                //LogDebug.WriteLog("2.send_PaymentHQ:" + strBuildXML.ToString(), EventLogEntryType.Information);
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("Exception.send_PaymentHQ", ex.StackTrace + "-" + ex.Message);
               // VBOracleLib.LogDebug.WriteLog("Lỗi Khi Hạch Toán" + ex.StackTrace + " Errmess: " + ex.Message, EventLogEntryType.Information);
            }
            return null;
        }
        private string PaymentTQ(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
            string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
            string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
            string v_str_KS, string v_KenhCT, string v_CustInfo, ref string v_str_errcode, ref string v_str_ref_no)
        {
            //VBOracleLib.LogApp.AddDebug("PaymentTQ", "0.PaymentTQ");
            //VBOracleLib.LogDebug.WriteLog("0.PaymentTQ", EventLogEntryType.Information);
            string nodevale = "";
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = KenhHT + TCS_GetSequence("TCS_REF_NO_SEQ");
            string sysdate = DateTime.Now.ToString("yyyyMMdd");
            DomXfer.AppHdrType appHdr = new DomXfer.AppHdrType();
            appHdr.CharSet = "UTF-8";
            appHdr.SvcVer = "1.0";
            DomXfer.PairsType nsFrom = new DomXfer.PairsType();
            nsFrom.Id = "ETAX";
            nsFrom.Name = "ETAX";
            DomXfer.PairsType nsTo = new DomXfer.PairsType();
            nsTo.Id = "CORE";
            nsTo.Name = "CORE";
            DomXfer.PairsType[] listOfNsTo = new DomXfer.PairsType[1];
            listOfNsTo[0] = nsTo;
            DomXfer.PairsType BizSvc = new DomXfer.PairsType();
            BizSvc.Id = "DomXfer";
            BizSvc.Name = "DomXfer";

            DateTime TransDt = DateTime.Now;
            appHdr.From = nsFrom;
            appHdr.To = listOfNsTo;
            appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");// +TCS_GetSequence("TCS_REF_NO_SEQ");
            appHdr.MsgPreId = "";
            appHdr.BizSvc = BizSvc;
            appHdr.TransDt = TransDt;
            //Body            
            DomXfer.DomXferAddReqType msgReq = new DomXfer.DomXferAddReqType();
            msgReq.AppHdr = appHdr;

            VBOracleLib.LogApp.AddDebug("PaymentTQ", "0.1.PaymentTQ");
            //VBOracleLib.LogDebug.WriteLog("0.1.PaymentTQ", EventLogEntryType.Information);
            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = CHANNELID_HQ;
            }
            else
            {
                if (v_KenhCT.Equals("ETAX_HQ247"))
                {
                    nodevale = CHANNELID_HQ247;
                }
                else
                {
                    nodevale = CHANNELID;
                }
                if (v_KenhCT.Equals("NTDT"))
                {
                    nodevale = CHANNELID_NTDT;
                }
            }
            msgReq.ChnlId = nodevale;//"NET";

            msgReq.BrCd = Br_Code; //"BR0001";

            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = INTERFACEID_HQ;
            }
            else
            {
                if (v_KenhCT.Equals("ETAX_HQ247"))
                {
                    nodevale = INTERFACEID_HQ247;
                }
                else
                {
                    nodevale = INTERFACEID;
                }
                if (v_KenhCT.Equals("NTDT"))
                {
                    nodevale = INTERFACEID_NTDT;
                }
            }
            msgReq.ItfId = nodevale; //"NETBNK";
            msgReq.RefNo = SEQUENCE_NO;//"NET" + TCS_GetSequence("TCS_REF_NO_SEQ");
            msgReq.DbAcct = pv_ACCOUNTNO.Trim(); ;//"1000000001";
            msgReq.CrAcct = pv_BEN_ACCOUNTNO; ;//"7111.0.2995342";
            msgReq.TxAmt = Decimal.Parse(AMOUNT);//AMOUNT
            msgReq.TxCur = "704";
            msgReq.TxDt = VALUEDATE;//"20171029";
            //msgReq.TxDt = sysdate;//"20171029";
            msgReq.ValDt = VALUEDATE;//msgReq.ValDt = VALUEDATE;// "20171029";
            msgReq.WIB = "N";
            msgReq.BenName = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME); //"CC HQ CK Cang Sai Gon KV I";
            msgReq.BenAddr1 = "";
            msgReq.BenAdd2 = "";
            msgReq.BenCity = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_CITY);// "";
            msgReq.IdType = "";
            msgReq.IdNo = "";
            msgReq.IdIssuePlace = "";
            msgReq.IdExpDt = "";
            if (v_KenhCT.Equals("ETAX_HQ247"))
            {
                if (REMARK.Length > 210)
                {
                    msgReq.CustRmk = REMARK.Substring(0, 210);
                    msgReq.IntRmk = REMARK.Substring(210);
                }
                else
                {
                    msgReq.CustRmk = REMARK;
                }
            }
            else
            {
                msgReq.CustRmk = VBOracleLib.Globals.RemoveSign4VietnameseString(REMARK);
                msgReq.IntRmk = VBOracleLib.Globals.RemoveSign4VietnameseString(v_CustInfo);
            }
            msgReq.BnkDesc = VBOracleLib.Globals.RemoveSign4VietnameseString(BANK_BEN_DESC);
            msgReq.BrDesc = "";
            msgReq.CtyCd = CITY_BEN_CODE;//"79";
            msgReq.CtyDesc = VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC);
            msgReq.BnkCode = Decimal.Parse(BANK_BEN_CODE);//Decimal.Parse("204");
            msgReq.BrCode = BR_BEN_CODE;//"";"002";
            msgReq.Mode = "INTER";
            msgReq.DealType = "";
            msgReq.ChrgType = "";
            msgReq.ConvRt = "";
            msgReq.EchoField = "E";
            appHdr.Signature = MD5Encoding.Hash(msgReq.DbAcct + msgReq.CrAcct + msgReq.TxAmt + msgReq.CustRmk + msgReq.CtyCd + msgReq.BnkCode + msgReq.BrCode + "A2C75E5C3A4C0E23D01871A7DB4A8D9E");
           log.Info( "0.2.PaymentTQ");
            //VBOracleLib.LogDebug.WriteLog("0.2.PaymentTQ", EventLogEntryType.Information);
            string hdr_Tran_Id = "";
            string res_Status = "";
            string res_Result_Code = "";
            string res_Ref_No = "";
            string res_Avail_Bal = "";
            string res_Curr_Bal = "";
            string res_Acc_CCY_Cd = "";
            string res_Lcy_Amt = "";
            string err_code = "";
            string rm_ref_no = "";
            string so_du = "";
            string err_msg = "";
            //Get Response
            DomXfer.DomXferAddResType msgRes = new DomXfer.DomXferAddResType();
            if (Core_status == "1")
            {
                log.Error( "1.PaymentTQ:" + msgReq);
                //VBOracleLib.LogDebug.WriteLog("1.PaymentTQ:" + msgReq, EventLogEntryType.Information);
                msgRes = sendMsg_Payment(msgReq, v_strSo_CT);
                log.Error( "2.PaymentTQ:" + msgRes);
                //VBOracleLib.LogDebug.WriteLog("2.PaymentTQ:" + msgRes, EventLogEntryType.Information);
            }
            try
            {
                if (Core_status == "1")
                {
                    if (msgRes == null)
                    {
                        res_Result_Code = "TO999";
                        res_Ref_No = CORE_RPSF;
                        res_Status = "TO999";
                        err_code = "TO999";
                    }
                    else
                    {
                        hdr_Tran_Id = msgRes.AppHdr.MsgId;
                        res_Status = msgRes.RespSts.Sts;
                        //res_Result_Code = msgRes.RespSts.ErrCd;
                        res_Result_Code = "00000";
                        res_Ref_No = msgRes.RefNo;
                        res_Avail_Bal = msgRes.AvailBal;
                        res_Curr_Bal = msgRes.CurrBal;
                        res_Lcy_Amt = msgRes.LcyAmt;
                        err_code = msgRes.RespSts.ErrCd;
                        err_msg = msgRes.RespSts.ErrMsg;
                        if (res_Status == "0")
                        {
                            res_Status = "00000";
                        }
                    }
                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }
                rm_ref_no = res_Ref_No;
                so_du = res_Curr_Bal;
                v_str_errcode = err_code;
                v_str_ref_no = rm_ref_no;
                string v_strSQLInsert;
                if (res_Status == "00000")
                {
                    v_str_ref_no = rm_ref_no;
                    v_strSQLInsert = ("INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," + (" CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SE" +
                    "LL_RATE," + (" AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO,USER_INPUT,KENH_CT,USE" +
                    "R_CHECKER) " + (" VALUES(\'TA001\',\'\',\'1\',SYSDATE,\'\',\'\',"
                                + ((" \'\',\'\',\'"
                                + (BANK_BEN_CODE + ("\',\'"
                                + (pv_ACCOUNTNO + ("\',\'\',\'VND\',\'10000000\',\'"
                                + (pv_BEN_ACCOUNTNO + ("\',\'"
                                + (strMa_NH_B + "\',\'10000000\',")))))))) + (" "
                                + (AMOUNT + (", 0 ,0 ,\'"
                                + (REMARK + ("\',\'"
                                + (res_Result_Code + ("\',\'"
                                + (rm_ref_no + ("\',\'\',\'\',\'"
                                + (v_strSo_CT + ("\',\'"
                                + (v_strMa_NV + ("\',\'"
                                + (v_KenhCT + ("\',\'"
                                + (v_str_KS + "\')")))))))))))))))))))));
                    VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                    v_str_errcode = "00000";
                }


            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("PaymentTQ", ex.StackTrace + "-" + ex.Message);
               // VBOracleLib.LogDebug.WriteLog(ex, "Loi khi hach toan chung tu", "clsCoreBank");
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }
            if (msgRes != null)
                return msgRes.ToString();
            else
                return "";
        }
        private string send_PaymentSP(string msg)
        {

            try
            {
               log.Error("1.send_PaymentSP:" + msg);
                //LogDebug.WriteLog("1.send_PaymentSP:" + msg, EventLogEntryType.Information);
                System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                VXDOC.LoadXml(msg);
                string KenhHT = VXDOC.SelectSingleNode("Customs/DATA/KenhHT").InnerText;
                string NGAYGD = VXDOC.SelectSingleNode("Customs/DATA/NGAYGD").InnerText;
                string v_strSo_CT = VXDOC.SelectSingleNode("Customs/DATA/v_strSo_CT").InnerText;
                string MA_CN = VXDOC.SelectSingleNode("Customs/DATA/MA_CN").InnerText;
                string MA_CN_TK = VXDOC.SelectSingleNode("Customs/DATA/MA_CN_TK").InnerText;
                string pv_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_ACCOUNTNO").InnerText;
                string AMOUNT = VXDOC.SelectSingleNode("Customs/DATA/AMOUNT").InnerText;
                string CCY = VXDOC.SelectSingleNode("Customs/DATA/CCY").InnerText;
                string REMARK = VXDOC.SelectSingleNode("Customs/DATA/REMARK").InnerText;
                string v_strMa_NV = VXDOC.SelectSingleNode("Customs/DATA/v_strMa_NV").InnerText;

                string v_str_KS = VXDOC.SelectSingleNode("Customs/DATA/v_str_KS").InnerText;
                string v_KenhCT = VXDOC.SelectSingleNode("Customs/DATA/v_KenhCT").InnerText;
                string v_CustInfo = VXDOC.SelectSingleNode("Customs/DATA/v_CustInfo").InnerText;
                string BANK_BEN_CODE = VXDOC.SelectSingleNode("Customs/DATA/BANK_BEN_CODE").InnerText;
                string pv_BEN_ACCOUNTNO = VXDOC.SelectSingleNode("Customs/DATA/pv_BEN_ACCOUNTNO").InnerText;
                string strMa_NH_B = VXDOC.SelectSingleNode("Customs/DATA/strMa_NH_B").InnerText;
                string strACC_NH_B = VXDOC.SelectSingleNode("Customs/DATA/strACC_NH_B").InnerText;
                string strMA_CN_SP = VXDOC.SelectSingleNode("Customs/DATA/strMA_CN_SP").InnerText;
                string v_str_errcode = "";
                string v_str_ref_no = "";
                PaymentSP(KenhHT, NGAYGD, v_strSo_CT, MA_CN, MA_CN_TK, pv_ACCOUNTNO, AMOUNT,
                                    CCY, REMARK, v_strMa_NV, v_str_KS, v_KenhCT, v_CustInfo, BANK_BEN_CODE, pv_BEN_ACCOUNTNO, strMa_NH_B, strACC_NH_B, strMA_CN_SP, ref  v_str_errcode, ref  v_str_ref_no);
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<Errcode>" + v_str_errcode + "</Errcode>");
                strBuildXML.Append("<Ref_no>" + v_str_ref_no + "</Ref_no>");
                strBuildXML.Append("</Customs>");
                VBOracleLib.LogApp.AddDebug("2.send_PaymentSP", "2.send_PaymentSP:" + strBuildXML.ToString());
                //LogDebug.WriteLog("2.send_PaymentSP:" + strBuildXML.ToString(), EventLogEntryType.Information);
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //VBOracleLib.LogDebug.WriteLog("Lỗi Khi Hạch Toán" + ex.StackTrace + " Errmess: " + ex.Message, EventLogEntryType.Information);
            }
            return null;
        }
        private string PaymentSP(string KenhHT, string NGAYGD, string v_strSo_CT, string MA_CN, string MA_CN_TK, string pv_ACCOUNTNO, string AMOUNT,
            string CCY, string REMARK, string v_strMa_NV, string v_str_KS, string v_KenhCT, string v_CustInfo, string BANK_BEN_CODE, string pv_BEN_ACCOUNTNO, string strMa_NH_B, string strACC_NH_B, string strMA_CN_SP, ref string v_str_errcode, ref string v_str_ref_no)
        {
            //VBOracleLib.LogApp.AddDebug("0.PaymentSP", "0.PaymentSP");
            //VBOracleLib.LogDebug.WriteLog("0.PaymentSP", EventLogEntryType.Information);
            string nodevale = "";
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = KenhHT + TCS_GetRefNo("TCS_REF_NO_SEQ");
            string sysdate = DateTime.Now.ToString("yyyyMMdd");

            //if (CCY.ToUpper().Equals("VND"))
            //{
            //    CCY = "704";
            //}
            //else if (CCY.ToUpper().Equals("USD"))
            //{
            //    CCY = "840";
            //}

            FinancialPosting.AppHdrType appHdr = new FinancialPosting.AppHdrType();
            appHdr.CharSet = "UTF-8";
            appHdr.SvcVer = "1.0";

            FinancialPosting.PairsType nsFrom = new FinancialPosting.PairsType();
            nsFrom.Id = "ETAX";
            nsFrom.Name = "ETAX";

            FinancialPosting.PairsType nsTo = new FinancialPosting.PairsType();
            nsTo.Id = "CORE";
            nsTo.Name = "CORE";

            FinancialPosting.PairsType[] listOfNsTo = new FinancialPosting.PairsType[1];
            listOfNsTo[0] = nsTo;

            FinancialPosting.PairsType BizSvc = new FinancialPosting.PairsType();
            BizSvc.Id = "FinancialPosting";
            BizSvc.Name = "FinancialPosting";

            DateTime TransDt = DateTime.Now;

            appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");

            appHdr.From = nsFrom;
            appHdr.To = listOfNsTo;
            appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");// +TCS_GetSequence("TCS_REF_NO_SEQ");
            //appHdr.MsgPreId = "";
            appHdr.BizSvc = BizSvc;
            appHdr.TransDt = TransDt;
            //Body            
            FinancialPosting.FinancialPostingCreateReqType msgReq = new FinancialPosting.FinancialPostingCreateReqType();
            // msgReq.AppHdr = appHdr;
            VBOracleLib.LogApp.AddDebug("0.1.PaymentSP", "0.1.PaymentSP");
            //VBOracleLib.LogDebug.WriteLog("0.1.PaymentSP", EventLogEntryType.Information);

            if (v_KenhCT.Equals("ETAX_ND"))
            {
                nodevale = CHANNELID;
                ItfId = INTERFACEID;
            }
            else if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = CHANNELID_HQ;
                ItfId = INTERFACEID_HQ;
            }
            else if (v_KenhCT.Equals("NTDT"))
            {

                nodevale = CHANNELID_NTDT;
                ItfId = INTERFACEID_NTDT;
            }
            else if (v_KenhCT.Equals("ETAX_HQ247"))
            {

                nodevale = CHANNELID_HQ247;
                ItfId = INTERFACEID_HQ247;
            }
            else
            {
                nodevale = ChnlId;
            }


            msgReq.ChnlId = nodevale;//"NET";


         
            msgReq.ItfId = ItfId; //"NETBNK";
            msgReq.RefNo = SEQUENCE_NO;//"NET" + TCS_GetSequence("TCS_REF_NO_SEQ");
            msgReq.TxnDt = sysdate;
            msgReq.SrcBranchCd = MA_CN;
            msgReq.PostingFlg = "F";
            msgReq.TxnBalFlg = "Y";
            msgReq.PostAllSegFlg = "N";
            msgReq.NoOfSeq = "2";

            List<FinancialPosting.FinancialPostingCreateReqTypeSegInfo> listSegInfo = new List<FinancialPosting.FinancialPostingCreateReqTypeSegInfo>();

            for (int i = 0; i < 2; i++)
            {
                listSegInfo.Add(new FinancialPosting.FinancialPostingCreateReqTypeSegInfo());
            }

            FinancialPosting.FinancialPostingCreateReqTypeSegInfo segInfo = new FinancialPosting.FinancialPostingCreateReqTypeSegInfo();

            listSegInfo[0].SegNo = "1";
            listSegInfo[0].TxnCd = "";
            listSegInfo[0].AcctBrCd = strMA_CN_SP;
            listSegInfo[0].AcctId = strACC_NH_B;
            listSegInfo[0].DrCrFlg = "C";
            listSegInfo[0].TxnAmt = Decimal.Parse(AMOUNT);
            listSegInfo[0].TxnCur = CCY;
            listSegInfo[0].IntRemark = "";
            listSegInfo[0].ExtRemark = REMARK;

            listSegInfo[1].SegNo = "2";
            listSegInfo[1].TxnCd = "";
            listSegInfo[1].AcctBrCd = MA_CN_TK;
            listSegInfo[1].AcctId = pv_ACCOUNTNO;
            listSegInfo[1].DrCrFlg = "D";
            listSegInfo[1].TxnAmt = Decimal.Parse(AMOUNT);
            listSegInfo[1].TxnCur = CCY;
            listSegInfo[1].IntRemark = "";
            listSegInfo[1].ExtRemark = REMARK;

            //segInfo.SegNo = "1";
            //segInfo.TxnCd = "";
            //segInfo.AcctBrCd = MA_CN_TK; //MA_CN_TK
            //segInfo.AcctId = pv_ACCOUNTNO;
            //segInfo.DrCrFlg = "D";
            //segInfo.TxnAmt = Decimal.Parse(AMOUNT);
            //segInfo.TxnCur = CCY;
            //segInfo.IntRemark = "";
            //segInfo.ExtRemark = REMARK;//pv_ACCOUNTNO + "(GD#" + SEQUENCE_NO + ")";

            //listSegInfo.Add(segInfo);

            msgReq.SegInfo = listSegInfo.ToArray();

            string strListSegInfo = "";
            //(accountNum + transAmount + extRemark)(accountNum + transAmount + extRemark)
            for (int i = 0; i < listSegInfo.Count; i++)
            {
                //strListSegInfo += "(";
                strListSegInfo += listSegInfo[i].AcctId + listSegInfo[i].TxnAmt.ToString() + listSegInfo[i].ExtRemark;
                //strListSegInfo += ")";
            }

            appHdr.Signature = MD5Encoding.Hash(strListSegInfo + "A2C75E5C3A4C0E23D01871A7DB4A8D9E");
            msgReq.AppHdr = appHdr;
           log.Error( "0.2.PaymentSP");
            //VBOracleLib.LogDebug.WriteLog("0.2.PaymentSP", EventLogEntryType.Information);
            string hdr_Tran_Id = "";
            string res_Status = "";
            string res_Result_Code = "";
            string res_Ref_No = "";
            string res_Avail_Bal = "";
            string res_Curr_Bal = "";
            string res_Acc_CCY_Cd = "";
            string res_Lcy_Amt = "";
            string err_code = "";
            string rm_ref_no = "";
            string so_du = "";
            string err_msg = "";
            //Get Response

            FinancialPosting.FinancialPostingCreateResType msgRes = new FinancialPosting.FinancialPostingCreateResType();

            if (Core_status == "1")
            {
               log.Error( "1.PaymentSP:" + msgReq);
                //VBOracleLib.LogDebug.WriteLog("1.PaymentSP:" + msgReq, EventLogEntryType.Information);
                msgRes = sendMsg_Payment_SP(msgReq, v_strSo_CT);
                log.Error( "2.PaymentSP:" + msgRes);
                //VBOracleLib.LogDebug.WriteLog("2.PaymentSP:" + msgRes, EventLogEntryType.Information);
            }
            try
            {
                if (Core_status == "1")
                {
                    if (msgRes == null)
                    {
                        res_Result_Code = "TO999";
                        res_Ref_No = CORE_RPSF;
                        res_Status = "TO999";
                        err_code = "TO999";
                    }
                    else
                    {
                        hdr_Tran_Id = msgRes.AppHdr.MsgId;
                        res_Status = msgRes.RespSts.Sts;
                        //res_Result_Code = msgRes.RespSts.ErrCd;
                        res_Result_Code = "00000";
                        res_Ref_No = msgRes.RefNo;
                        //res_Avail_Bal = msgRes.AvailBal;
                        //res_Curr_Bal = msgRes.CurrBal;
                        //res_Lcy_Amt = msgRes.LcyAmt;
                        err_code = msgRes.RespSts.ErrCd;
                        err_msg = msgRes.RespSts.ErrMsg;
                        if (res_Status == "0")
                        {
                            res_Status = "00000";
                        }
                    }
                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }
                rm_ref_no = res_Ref_No;
                so_du = res_Curr_Bal;
                v_str_errcode = err_code;
                v_str_ref_no = rm_ref_no;
                string v_strSQLInsert;
                if (res_Status == "00000")
                {
                    v_str_ref_no = rm_ref_no;
                    v_strSQLInsert = ("INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," + (" CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SE" +
                    "LL_RATE," + (" AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO,USER_INPUT,KENH_CT,USE" +
                    "R_CHECKER) " + (" VALUES(\'TA001\',\'\',\'1\',SYSDATE,\'\',\'\',"
                                + ((" \'\',\'\',\'"
                                + (BANK_BEN_CODE + ("\',\'"
                                + (pv_ACCOUNTNO + ("\',\'\',\'VND\',\'10000000\',\'"
                                + (pv_BEN_ACCOUNTNO + ("\',\'"
                                + (strMa_NH_B + "\',\'10000000\',")))))))) + (" "
                                + (AMOUNT + (", 0 ,0 ,\'"
                                + (REMARK + ("\',\'"
                                + (res_Result_Code + ("\',\'"
                                + (rm_ref_no + ("\',\'\',\'\',\'"
                                + (v_strSo_CT + ("\',\'"
                                + (v_strMa_NV + ("\',\'"
                                + (v_KenhCT + ("\',\'"
                                + (v_str_KS + "\')")))))))))))))))))))));
                    VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                    v_str_errcode = "00000";
                }


            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("PaymentSP", ex.StackTrace + "-" + ex.Message);
             //   VBOracleLib.LogDebug.WriteLog(ex, "Loi khi hach toan chung tu", "clsCoreBank");
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }
            if (msgRes != null)
                return msgRes.ToString();
            else
                return "";
        }
        private string Get_VALUE_DATE()
        {
            string Value_date = "";
            try
            {

                DataSet ds = null;

                string sbsql = "select tcs_pck_util_shb.fnc_get_val_date('SHB') value_date from dual";
                try
                {
                    if (Core_status == "1")
                    {
                        ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text);
                        Value_date = ds.Tables[0].Rows[0]["value_date"].ToString();
                    }
                    else
                    {
                        Value_date = DateTime.Now.ToString("yyyyMMdd");
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogDebug.WriteLog("Loi khi lay Core date" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                    return "Khong lay duoc VAL DATE";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogDebug.WriteLog("Loi khi lay Core date" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
            return Value_date;
        }
        private static Int64 TCS_GetSequence(string p_Seq)
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT ");
                sbSQL.Append(p_Seq);
                sbSQL.Append(".nextval FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                return Int64.Parse(strRet);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        private static string TCS_GetRefNo(string p_Seq)
        {

            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("select LPAD(" + p_Seq + ".NEXTVAL,12,'0') from dual");
                //sbSQL.Append(p_Seq);
                //sbSQL.Append(".nextval FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                return strRet;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        private FinancialPosting.FinancialPostingCreateResType sendMsg_Payment_SP(FinancialPosting.FinancialPostingCreateReqType MsgCnt, string so_ct)
        {
            string strID = TCS_GetSequence("SEQ_DCHIEU_NHHQ_HDR_ID").ToString();
            string strSQL = "";
            strSQL = "INSERT INTO TCS_LOG_CORE (ID,TIME_UPDATE,DESCRIPTION,SO_CT) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += DomXferAddReqTypeToStringSP(MsgCnt);
            strSQL += "','";
            strSQL += so_ct;
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                FinancialPosting.PortTypeClient porttypeClient = new FinancialPosting.PortTypeClient();
                FinancialPosting.FinancialPostingCreateResType returndata = porttypeClient.Create(MsgCnt);
                strSQL = "UPDATE TCS_LOG_CORE SET MSG_IN = '";
                string kq = FinancialPostingCreateResTypeToString(returndata);
                strSQL += kq.ToString().Trim().Replace("\0", "") + "' WHERE ID=";
                strSQL += strID;
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                return returndata;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("FinancialPostingCreateResType", ex.StackTrace + "-" + ex.Message);
               // VBOracleLib.LogDebug.WriteLog(ex, "Loi khi gui hach toan vao core", "clsCoreBank");
                return null;
            }

        }
        private DomXfer.DomXferAddResType sendMsg_Payment(DomXfer.DomXferAddReqType MsgCnt, string so_ct)
        {
            string strID = TCS_GetSequence("SEQ_DCHIEU_NHHQ_HDR_ID").ToString();
            string strSQL = "";
            strSQL = "INSERT INTO TCS_LOG_CORE (ID,TIME_UPDATE,DESCRIPTION, SO_CT) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += DomXferAddReqTypeToString(MsgCnt);
            strSQL += "','";
            strSQL += so_ct;
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                DomXfer.PortTypeClient porttypeClient = new DomXfer.PortTypeClient();
                DomXfer.DomXferAddResType returndata = porttypeClient.Create(MsgCnt);
                strSQL = "UPDATE TCS_LOG_CORE SET MSG_IN = '";
                string kq = DomXferAddResTypeToString(returndata);
                strSQL += kq.ToString().Trim().Replace("\0", "") + "' WHERE ID=";
                strSQL += strID;
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                return returndata;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("DomXfer.DomXferAddResType", ex.StackTrace + "-" + ex.Message);
            //    VBOracleLib.LogDebug.WriteLog(ex, "Loi khi gui hach toan vao core", "clsCoreBank");
                return null;
            }

        }
        private string DomXferAddReqTypeToStringSP(FinancialPosting.FinancialPostingCreateReqType MsgCnt)
        {
            string msg = "";
            try
            {
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<FinancialPostingCreateReqType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<ItfId>" + MsgCnt.ItfId + "</ItfId>");
                strBuildXML.Append("<ChnlId>" + MsgCnt.ChnlId + "</ChnlId>");
                strBuildXML.Append("<RefNo>" + MsgCnt.RefNo + "</RefNo>");
                strBuildXML.Append("<TxnDt>" + MsgCnt.TxnDt + "</TxnDt>");
                strBuildXML.Append("<SrcBranchCd>" + MsgCnt.SrcBranchCd + "</SrcBranchCd>");
                strBuildXML.Append("<PostingFlg>" + MsgCnt.PostingFlg + "</PostingFlg>");
                strBuildXML.Append("<TxnBalFlg>" + MsgCnt.TxnBalFlg + "</TxnBalFlg>");
                strBuildXML.Append("<PostAllSegFlg>" + MsgCnt.PostAllSegFlg + "</PostAllSegFlg>");
                strBuildXML.Append("<NoOfSeq>" + MsgCnt.NoOfSeq + "</NoOfSeq>");


                for (int i = 0; i < MsgCnt.SegInfo.Length; i++)
                {
                    //chi co 1 dong, tam dong vong for
                    strBuildXML.Append("<SegInfo>");
                    strBuildXML.Append("<SegNo>" + MsgCnt.SegInfo[i].SegNo + "</SegNo>");
                    strBuildXML.Append("<TxnCd>" + MsgCnt.SegInfo[i].TxnCd + "</TxnCd>");
                    strBuildXML.Append("<AcctBrCd>" + MsgCnt.SegInfo[i].AcctBrCd + "</AcctBrCd>");
                    strBuildXML.Append("<AcctId>" + MsgCnt.SegInfo[i].AcctId + "</AcctId>");
                    strBuildXML.Append("<DrCrFlg>" + MsgCnt.SegInfo[i].DrCrFlg + "</DrCrFlg>");
                    strBuildXML.Append("<TxnAmt>" + MsgCnt.SegInfo[i].TxnAmt.ToString() + "</TxnAmt>");
                    strBuildXML.Append("<TxnCur>" + MsgCnt.SegInfo[i].TxnCur + "</TxnCur>");
                    strBuildXML.Append("<IntRemark>" + MsgCnt.SegInfo[i].IntRemark + "</IntRemark>");
                    strBuildXML.Append("<ExtRemark>" + MsgCnt.SegInfo[i].ExtRemark + "</ExtRemark>");
                    strBuildXML.Append("</SegInfo>");
                }
                strBuildXML.Append("</FinancialPostingCreateReqType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("EX.CoreESB.DomXferAddReqTypeToStringSP", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("EX.CoreESB.DomXferAddReqTypeToStringSP: " + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string FinancialPostingCreateResTypeToString(FinancialPosting.FinancialPostingCreateResType MsgCnt)
        {
            string msg = "";
            try
            {
                //var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(MsgCnt);
                //LogDebug.WriteLog("FinancialPostingCreateResTypeToString" + jsonString.ToString(), EventLogEntryType.Error);
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<FinancialPostingCreateResType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<ChnlId>" + MsgCnt.ChnlId + "</ChnlId>");
                strBuildXML.Append("<RefNo>" + MsgCnt.RefNo + "</RefNo>");
                strBuildXML.Append("<RespSts>");
                strBuildXML.Append("<Sts>" + MsgCnt.RespSts.Sts + "</Sts>");
                if (MsgCnt.RespSts.Sts == "0")
                {
                    strBuildXML.Append("<ErrCd></ErrCd>");
                    strBuildXML.Append("<ErrMsg></ErrMsg>");
                    strBuildXML.Append("<ErrInfo></ErrInfo>");
                }
                else
                {
                    strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrCd + "</ErrCd>");
                    strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrMsg + "</ErrMsg>");
                    strBuildXML.Append("<ErrInfo>");
                    strBuildXML.Append("<Id>" + MsgCnt.RespSts.ErrInfo[0].Id + "</Id>");
                    strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrInfo[0].ErrCd + "</ErrCd>");
                    strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrInfo[0].ErrMsg + "</ErrMsg>");
                    strBuildXML.Append("</ErrInfo>");
                }

                strBuildXML.Append("</RespSts>");
                strBuildXML.Append("</FinancialPostingCreateResType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //VBOracleLib.LogApp.AddDebug("CoreESB.DomXferAddResTypeToString", ex.StackTrace + "-" + ex.Message);
                //LogDebug.WriteLog("CoreESB.DomXferAddResTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string DomXferAddReqTypeToString_V20(DomXfer.DomXferAddReqType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<DomXferAddReqType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<ChnlId>" + MsgCnt.ChnlId + "</ChnlId>");
                strBuildXML.Append("<BrCd>" + MsgCnt.BrCd + "</BrCd>");
                strBuildXML.Append("<ItfId>" + MsgCnt.ItfId + "</ItfId>");
                strBuildXML.Append("<RefNo>" + MsgCnt.RefNo + "</RefNo>");
                strBuildXML.Append("<DbAcct>" + MsgCnt.DbAcct + "</DbAcct>");
                strBuildXML.Append("<CrAcct>" + MsgCnt.CrAcct + "</CrAcct>");
                strBuildXML.Append("<TxAmt>" + MsgCnt.TxAmt + "</TxAmt>");
                strBuildXML.Append("<TxCur>" + MsgCnt.TxCur + "</TxCur>");
                strBuildXML.Append("<TxDt>" + MsgCnt.TxDt + "</TxDt>");
                strBuildXML.Append("<ValDt>" + MsgCnt.ValDt + "</ValDt>");
                strBuildXML.Append("<WIB>" + MsgCnt.WIB + "</WIB>");
                strBuildXML.Append("<BenName>" + MsgCnt.BenName + "</BenName>");
                strBuildXML.Append("<BenAddr1>" + MsgCnt.BenAddr1 + "</BenAddr1>");
                strBuildXML.Append("<BenAdd2>" + MsgCnt.BenAdd2 + "</BenAdd2>");
                strBuildXML.Append("<BenCity>" + MsgCnt.BenCity + "</BenCity>");
                strBuildXML.Append("<IdType>" + MsgCnt.IdType + "</IdType>");
                strBuildXML.Append("<IdNo>" + MsgCnt.IdNo + "</IdNo>");
                strBuildXML.Append("<IdIssuePlace>" + MsgCnt.IdIssuePlace + "</IdIssuePlace>");
                strBuildXML.Append("<IdExpDt>" + MsgCnt.IdExpDt + "</IdExpDt>");
                strBuildXML.Append("<CustRmk>" + MsgCnt.CustRmk + "</CustRmk>");
                strBuildXML.Append("<IntRmk>" + MsgCnt.IntRmk + "</IntRmk>");
                strBuildXML.Append("<BnkDesc>" + MsgCnt.BnkDesc + "</BnkDesc>");
                strBuildXML.Append("<BrDesc>" + MsgCnt.BrDesc + "</BrDesc>");
                strBuildXML.Append("<CtyCd>" + MsgCnt.CtyCd + "</CtyCd>");
                strBuildXML.Append("<CtyDesc>" + MsgCnt.CtyDesc + "</CtyDesc>");
                strBuildXML.Append("<BnkCode>" + MsgCnt.BnkCode + "</BnkCode>");
                strBuildXML.Append("<BrCode>" + MsgCnt.BrCode + "</BrCode>");
                strBuildXML.Append("<Mode>" + MsgCnt.Mode + "</Mode>");
                strBuildXML.Append("<DealType>" + MsgCnt.DealType + "</DealType>");
                strBuildXML.Append("<ChrgType>" + MsgCnt.ChrgType + "</ChrgType>");
                strBuildXML.Append("<ConvRt>" + MsgCnt.ConvRt + "</ConvRt>");
                strBuildXML.Append("<EchoField>" + MsgCnt.EchoField + "</EchoField>");
                strBuildXML.Append("<Mode>" + MsgCnt.Mode + "</Mode>");
                strBuildXML.Append("<BgInfoFlg>" + MsgCnt.BgInfoFlg + "</BgInfoFlg>");
                strBuildXML.Append("<TaxPymtDate>" + MsgCnt.TaxPymtDate + "</TaxPymtDate>");
                strBuildXML.Append("<TaxType>" + MsgCnt.TaxType + "</TaxType>");
                strBuildXML.Append("<TaxCode>" + MsgCnt.TaxCode + "</TaxCode>");
                strBuildXML.Append("<ColACode>" + MsgCnt.ColACode + "</ColACode>");
                strBuildXML.Append("<ColAName>" + MsgCnt.ColAName + "</ColAName>");
                strBuildXML.Append("<LocaCode>" + MsgCnt.LocaCode + "</LocaCode>");
                strBuildXML.Append("<APenalty>" + MsgCnt.APenalty + "</APenalty>");
                strBuildXML.Append("<TaxPayerName>" + MsgCnt.TaxPayerName + "</TaxPayerName>");
                strBuildXML.Append("<TaxPayerAdd>" + MsgCnt.TaxPayerAdd + "</TaxPayerAdd>");
                strBuildXML.Append("<EcomStr>" + MsgCnt.EcomStr + "</EcomStr>");
                strBuildXML.Append("</DomXferAddReqType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("CoreESB.DomXferAddReqTypeToString_V20", ex.StackTrace + "-" + ex.Message);
              //  LogDebug.WriteLog("CoreESB.DomXferAddReqTypeToString_V20" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string DomXferAddReqTypeToString(DomXfer.DomXferAddReqType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<DomXferAddReqType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<ChnlId>" + MsgCnt.ChnlId + "</ChnlId>");
                strBuildXML.Append("<BrCd>" + MsgCnt.BrCd + "</BrCd>");
                strBuildXML.Append("<ItfId>" + MsgCnt.ItfId + "</ItfId>");
                strBuildXML.Append("<RefNo>" + MsgCnt.RefNo + "</RefNo>");
                strBuildXML.Append("<DbAcct>" + MsgCnt.DbAcct + "</DbAcct>");
                strBuildXML.Append("<CrAcct>" + MsgCnt.CrAcct + "</CrAcct>");
                strBuildXML.Append("<TxAmt>" + MsgCnt.TxAmt + "</TxAmt>");
                strBuildXML.Append("<TxCur>" + MsgCnt.TxCur + "</TxCur>");
                strBuildXML.Append("<TxDt>" + MsgCnt.TxDt + "</TxDt>");
                strBuildXML.Append("<ValDt>" + MsgCnt.ValDt + "</ValDt>");
                strBuildXML.Append("<WIB>" + MsgCnt.WIB + "</WIB>");
                strBuildXML.Append("<BenName>" + MsgCnt.BenName + "</BenName>");
                strBuildXML.Append("<BenAddr1>" + MsgCnt.BenAddr1 + "</BenAddr1>");
                strBuildXML.Append("<BenAdd2>" + MsgCnt.BenAdd2 + "</BenAdd2>");
                strBuildXML.Append("<BenCity>" + MsgCnt.BenCity + "</BenCity>");
                strBuildXML.Append("<IdType>" + MsgCnt.IdType + "</IdType>");
                strBuildXML.Append("<IdNo>" + MsgCnt.IdNo + "</IdNo>");
                strBuildXML.Append("<IdIssuePlace>" + MsgCnt.IdIssuePlace + "</IdIssuePlace>");
                strBuildXML.Append("<IdExpDt>" + MsgCnt.IdExpDt + "</IdExpDt>");
                strBuildXML.Append("<CustRmk>" + MsgCnt.CustRmk + "</CustRmk>");
                strBuildXML.Append("<IntRmk>" + MsgCnt.IntRmk + "</IntRmk>");
                strBuildXML.Append("<BnkDesc>" + MsgCnt.BnkDesc + "</BnkDesc>");
                strBuildXML.Append("<BrDesc>" + MsgCnt.BrDesc + "</BrDesc>");
                strBuildXML.Append("<CtyCd>" + MsgCnt.CtyCd + "</CtyCd>");
                strBuildXML.Append("<CtyDesc>" + MsgCnt.CtyDesc + "</CtyDesc>");
                strBuildXML.Append("<BnkCode>" + MsgCnt.BnkCode + "</BnkCode>");
                strBuildXML.Append("<BrCode>" + MsgCnt.BrCode + "</BrCode>");
                strBuildXML.Append("<Mode>" + MsgCnt.Mode + "</Mode>");
                strBuildXML.Append("<DealType>" + MsgCnt.DealType + "</DealType>");
                strBuildXML.Append("<ChrgType>" + MsgCnt.ChrgType + "</ChrgType>");
                strBuildXML.Append("<ConvRt>" + MsgCnt.ConvRt + "</ConvRt>");
                strBuildXML.Append("<EchoField>" + MsgCnt.EchoField + "</EchoField>");
                strBuildXML.Append("</DomXferAddReqType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //VBOracleLib.LogApp.AddDebug("CoreESB.DomXferAddReqTypeToString", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("CoreESB.DomXferAddReqTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string DomXferAddResTypeToString(DomXfer.DomXferAddResType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<DomXferAddResType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<ChnlId>" + MsgCnt.ChnlId + "</ChnlId>");
                strBuildXML.Append("<RefNo>" + MsgCnt.RefNo + "</RefNo>");
                strBuildXML.Append("<AvailBal>" + MsgCnt.AvailBal + "</AvailBal>");
                strBuildXML.Append("<CurrBal>" + MsgCnt.CurrBal + "</CurrBal>");
                strBuildXML.Append("<CurCd>" + MsgCnt.CurCd + "</CurCd>");
                strBuildXML.Append("<LcyAmt>" + MsgCnt.LcyAmt + "</LcyAmt>");
                strBuildXML.Append("<RespSts>");
                strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrCd + "</ErrCd>");
                strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrMsg + "</ErrMsg>");
                strBuildXML.Append("<Sts>" + MsgCnt.RespSts.Sts + "</Sts>");
                strBuildXML.Append("<ErrInfo>");
                strBuildXML.Append("<Id>" + MsgCnt.RespSts.ErrInfo[0].Id + "</Id>");
                strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrInfo[0].ErrCd + "</ErrCd>");
                strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrInfo[0].ErrMsg + "</ErrMsg>");
                strBuildXML.Append("</ErrInfo>");
                strBuildXML.Append("</RespSts>");
                strBuildXML.Append("</DomXferAddResType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("CoreESB.DomXferAddResTypeToString", ex.StackTrace + "-" + ex.Message);
              //  LogDebug.WriteLog("CoreESB.DomXferAddResTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string TEST_V20()
        {

            string nodevale = "";
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = "ETAX" + TCS_GetSequence("TCS_REF_NO_SEQ");


            DomXfer.AppHdrType appHdr = new DomXfer.AppHdrType();
            appHdr.CharSet = "UTF-8";
            appHdr.SvcVer = "2.0";
            DomXfer.PairsType nsFrom = new DomXfer.PairsType();
            nsFrom.Id = "ETAX";
            nsFrom.Name = "ETAX";
            DomXfer.PairsType nsTo = new DomXfer.PairsType();
            nsTo.Id = "CORE";
            nsTo.Name = "CORE";
            DomXfer.PairsType[] listOfNsTo = new DomXfer.PairsType[1];
            listOfNsTo[0] = nsTo;
            DomXfer.PairsType BizSvc = new DomXfer.PairsType();
            BizSvc.Id = "DomXfer";
            BizSvc.Name = "DomXfer";

            DateTime TransDt = DateTime.Now;
            appHdr.From = nsFrom;
            appHdr.To = listOfNsTo;
            appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff") + TCS_GetSequence("TCS_REF_NO_SEQ");
            appHdr.MsgPreId = "";
            appHdr.BizSvc = BizSvc;
            appHdr.TransDt = TransDt;

            //Body            
            DomXfer.DomXferAddReqType msgReq = new DomXfer.DomXferAddReqType();
            msgReq.AppHdr = appHdr;
            msgReq.ChnlId = "NET";//nodevale;
            msgReq.BrCd = "BR0001";//Br_Code; 
            msgReq.ItfId = "CUSMKR";//"NETBNK"; //nodevale;
            msgReq.RefNo = "TAX82331";//"NET" + TCS_GetSequence("TCS_REF_NO_SEQ");//SEQUENCE_NO;
            msgReq.DbAcct = "1000000001";//pv_ACCOUNTNO.Trim();;
            msgReq.CrAcct = "7111.0.2995342";//pv_BEN_ACCOUNTNO; ;
            msgReq.TxAmt = 554704;//Decimal.Parse(AMOUNT);//AMOUNT
            msgReq.TxCur = "704";
            msgReq.TxDt = "20180417";//TransactionDate;
            msgReq.ValDt = "2017";//VALUEDATE;
            msgReq.WIB = "N";
            msgReq.BenName = "Chi cuc HQ Quan ly hang dau tu";//VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME); 
            msgReq.BenAddr1 = "";
            msgReq.BenAdd2 = "";
            msgReq.BenCity = ""; // BEN_CITY;
            msgReq.IdType = "";
            msgReq.IdNo = "";
            msgReq.IdIssuePlace = "";
            msgReq.IdExpDt = "";
            msgReq.CustRmk = "LH:.VP KBNN Ho Chi Minh";
            msgReq.IntRmk = "";//v_CustInfo; 
            msgReq.BnkDesc = "Vietcombank Tp. HCM";//BANK_BEN_DESC; 
            msgReq.BrDesc = "";
            msgReq.CtyCd = "79";//CITY_BEN_CODE;
            msgReq.CtyDesc = ""; //VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC);
            msgReq.BnkCode = Decimal.Parse("203");//Decimal.Parse(BANK_BEN_CODE);
            msgReq.BrCode = "001";//BR_BEN_CODE;"";
            msgReq.Mode = "INTER";
            msgReq.DealType = "";
            msgReq.ChrgType = "";
            msgReq.ConvRt = "";
            msgReq.EchoField = "E";
            //cac truong moi cua 2.0
            msgReq.BgInfoFlg = "Y";//"Y";
            msgReq.TaxPymtDate = "20180329";//Ngay_KB cua chung tu
            msgReq.TaxType = "04";//Ma loai thue
            msgReq.TaxCode = "0300709284";//MST
            msgReq.ColACode = "";
            msgReq.ColAName = "";
            msgReq.LocaCode = "";
            msgReq.APenalty = "0";
            msgReq.TaxPayerName = "Cong Ty Trach Nhiem Huu Han Mercedes - Benz - Viet Nam";//Ten nguoi nop thue
            msgReq.TaxPayerAdd = "13 Duong Quang Trung";//Dic chi NNT
            // ####NDKT~MAChuong~sotien~
            msgReq.EcomStr = "####1901C999~286916~Thue nhap khau~10095454380~29-03-2018###1702C999~267788~Thue gia tri gia tang hang nhap khau~10095454380~29-03-2018";

            appHdr.Signature = MD5Encoding.Hash(msgReq.DbAcct + msgReq.CrAcct + msgReq.TxAmt + msgReq.CustRmk + msgReq.CtyCd + msgReq.BnkCode + msgReq.BrCode + "A2C75E5C3A4C0E23D01871A7DB4A8D9E");


            string hdr_Tran_Id = "";
            string res_Status = "";
            string res_Result_Code = "";
            string res_Ref_No = "";
            string res_Avail_Bal = "";
            string res_Curr_Bal = "";
            string res_Acc_CCY_Cd = "";
            string res_Lcy_Amt = "";
            string err_code = "";
            string rm_ref_no = "";
            string so_du = "";
            string err_msg = "";
            //Get Response
            DomXfer.DomXferAddResType msgRes = new DomXfer.DomXferAddResType();
            if (Core_status == "1")
            {

                msgRes = sendMsg_Payment(msgReq,"");
            }
            try
            {
                if (Core_status == "1")
                {
                    if (msgRes == null)
                    {
                        res_Result_Code = "00000";
                        res_Ref_No = CORE_RPSF;
                        res_Status = "00000";
                        //v_str_errcode = "TO999";
                    }
                    else
                    {
                        hdr_Tran_Id = msgRes.AppHdr.MsgId;
                        res_Status = msgRes.RespSts.Sts;
                        res_Result_Code = msgRes.RespSts.ErrCd;
                        res_Ref_No = msgRes.RefNo;
                        res_Avail_Bal = msgRes.AvailBal;
                        res_Curr_Bal = msgRes.CurrBal;
                        res_Lcy_Amt = msgRes.LcyAmt;
                        err_code = msgRes.RespSts.ErrCd;
                        err_msg = msgRes.RespSts.ErrMsg;

                    }
                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }


            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogDebug.WriteLog(ex, "Loi khi hach toan chung tu", "clsCoreBank");
                //v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }

            return msgRes.ToString();
        }
        private AcctList.AcctListInqResType sendMsg(AcctList.AcctListInqReqType MsgCnt)
        {


            string strID = Business.Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_HDR_ID.NextVal");
            string strSQL = "";
            strSQL = "INSERT INTO TCS_LOG_CORE (ID,TIME_UPDATE,DESCRIPTION) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += AcctListInqReqTypeToString(MsgCnt);
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                AcctList.PortTypeClient porttypeClient = new AcctList.PortTypeClient();
                AcctList.AcctListInqResType returndata = porttypeClient.Inquiry(MsgCnt);

                strSQL = "UPDATE TCS_LOG_CORE SET MSG_IN = '";
                string kq = AcctListInqResTypeToString(returndata);


                strSQL += kq.ToString().Trim().Replace("\0", "") + "' WHERE ID=";
                strSQL += strID;
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                return returndata;

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogDebug.WriteLog(ex, "Loi khi gui hach toan vao core", "clsCoreBank");
                return null;
            }

        }
        private AcctInfo.AcctInfoInqResType sendMsg(AcctInfo.AcctInfoInqReqType MsgCnt, ref string errCode, ref string errMsg)
        {

            string strID = Business.Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_HDR_ID.NextVal");
            string strSQL = "";
            strSQL = "INSERT INTO TCS_LOG_CORE (ID,TIME_UPDATE,DESCRIPTION) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += AcctInfoInqReqTypeToString(MsgCnt);
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                AcctInfo.PortTypeClient porttypeClient;
                AcctInfo.AcctInfoInqResType returndata;
                try
                {
                    porttypeClient = new AcctInfo.PortTypeClient();
                    returndata = porttypeClient.Inquiry(MsgCnt);
                }
                catch (Exception ex1)
                {
                    errCode = "TO999";
                    log.Error(ex1.Message + "-" + ex1.StackTrace); //LogDebug.WriteLog("AcctInfo.PortTypeClient porttypeClient: " + ex1.Message, EventLogEntryType.Error);
                    throw ex1;
                }

                string kq = AcctInfoInqResTypeToString(returndata);

                if (kq.Trim().Length <= 0)
                {
                    try
                    {
                        porttypeClient = new AcctInfo.PortTypeClient();
                        returndata = porttypeClient.Inquiry(MsgCnt);
                    }
                    catch (Exception ex1)
                    {
                        errCode = "TO999";
                        log.Error(ex1.Message + "-" + ex1.StackTrace); //LogDebug.WriteLog("AcctInfo.PortTypeClient porttypeClient: " + ex1.Message, EventLogEntryType.Error);
                        throw ex1;
                    }
                    kq = AcctInfoInqResTypeToString(returndata);
                }


                strSQL = "UPDATE TCS_LOG_CORE SET MSG_IN = '";
                strSQL += kq.ToString().Trim().Replace("\0", "") + "' WHERE ID=";
                strSQL += strID;
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                return returndata;

            }
            catch (Exception ex)
            {
               // VBOracleLib.LogApp.AddDebug("AcctInfo.AcctInfoInqResType", ex.StackTrace + "-" + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("CoreESB.sendMsg(AcctInfo.AcctInfoInqReqType MsgCnt)" + ex.Message, EventLogEntryType.Error);
                errMsg = "CoreESb.sendMsg function error:" + ex.Message;
                return null;
            }

        }
        private string AcctInfoInqReqTypeToString(AcctInfo.AcctInfoInqReqType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<AcctInfoInqReqType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<CustId>" + MsgCnt.CustId.ToString() + "</CustId>");

                strBuildXML.Append("<AcctInfo>");
                strBuildXML.Append("<AcctCur>" + MsgCnt.AcctInfo.AcctCur + "</AcctCur>");
                strBuildXML.Append("<AcctId>" + MsgCnt.AcctInfo.AcctId + "</AcctId>");
                strBuildXML.Append("<AcctSts>" + MsgCnt.AcctInfo.AcctSts + "</AcctSts>");
                strBuildXML.Append("<AcctType>" + MsgCnt.AcctInfo.AcctType + "</AcctType>");
                strBuildXML.Append("</AcctInfo>");

                strBuildXML.Append("</AcctInfoInqReqType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("CoreESB.AcctInfoInqReqTypeToString", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("CoreESB.AcctInfoInqReqTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string AcctInfoInqResTypeToString(AcctInfo.AcctInfoInqResType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<AcctInfoInqResType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                for (int i = 0; i < MsgCnt.AcctRec.Length; i++)
                {
                    strBuildXML.Append("<AcctRec>");
                    strBuildXML.Append("<AcctInfo>");
                    strBuildXML.Append("<AcctCur>" + MsgCnt.AcctRec[i].AcctInfo.AcctCur + "</AcctCur>");
                    strBuildXML.Append("<AcctId>" + MsgCnt.AcctRec[i].AcctInfo.AcctId + "</AcctId>");
                    strBuildXML.Append("<AcctSts>" + MsgCnt.AcctRec[i].AcctInfo.AcctSts + "</AcctSts>");
                    strBuildXML.Append("<AcctType>" + MsgCnt.AcctRec[i].AcctInfo.AcctType + "</AcctType>");
                    strBuildXML.Append("</AcctInfo>");
                    strBuildXML.Append("<CustName>" + MsgCnt.AcctRec[i].CustName.ToString() + "</CustName>");
                    strBuildXML.Append("<DepositAcctRec>");
                    if (MsgCnt.AcctRec[i].DepositAcctRec != null)
                    {
                        strBuildXML.Append("<AcctSts>" + MsgCnt.AcctRec[i].DepositAcctRec.AcctSts + "</AcctSts>");
                        strBuildXML.Append("<AutoRenNo>" + MsgCnt.AcctRec[i].DepositAcctRec.AutoRenNo + "</AutoRenNo>");
                        strBuildXML.Append("<AvaiBal>" + MsgCnt.AcctRec[i].DepositAcctRec.AvaiBal + "</AvaiBal>");
                        strBuildXML.Append("<BranchInfo>");
                        strBuildXML.Append("<BankId>" + MsgCnt.AcctRec[i].DepositAcctRec.BranchInfo.BankId + "</BankId>");
                        strBuildXML.Append("<BankName>" + MsgCnt.AcctRec[i].DepositAcctRec.BranchInfo.BankName + "</BankName>");
                        strBuildXML.Append("<BankType>" + MsgCnt.AcctRec[i].DepositAcctRec.BranchInfo.BankType + "</BankType>");
                        strBuildXML.Append("<BranchId>" + MsgCnt.AcctRec[i].DepositAcctRec.BranchInfo.BranchId + "</BranchId>");
                        strBuildXML.Append("<BranchName>" + MsgCnt.AcctRec[i].DepositAcctRec.BranchInfo.BranchName + "</BranchName>");
                        strBuildXML.Append("</BranchInfo>");
                    }
                    strBuildXML.Append("</DepositAcctRec>");
                    strBuildXML.Append("<LoanAcctRec>");
                    //if (MsgCnt.AcctRec[i].DepositAcctRec != null)
                    //{
                    //    strBuildXML.Append(MsgCnt.AcctRec[i].LoanAcctRec.ToString());
                    //}
                    strBuildXML.Append("</LoanAcctRec>");
                    strBuildXML.Append("<SavingAcctRec>");
                    //if (MsgCnt.AcctRec[i].DepositAcctRec != null)
                    //{
                    //    strBuildXML.Append(MsgCnt.AcctRec[i].SavingAcctRec.ToString());
                    //}
                    strBuildXML.Append("</SavingAcctRec>");
                    strBuildXML.Append("</AcctRec>");
                }
                strBuildXML.Append("<RespSts>");
                strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrCd + "</ErrCd>");
                strBuildXML.Append("<ErrInfo>" + MsgCnt.RespSts.ErrInfo + "</ErrInfo>");
                strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrMsg + "</ErrMsg>");
                strBuildXML.Append("<Sts>" + MsgCnt.RespSts.Sts + "</Sts>");
                strBuildXML.Append("</RespSts>");

                strBuildXML.Append("</AcctInfoInqResType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("CoreESB.AcctInfoInqResTypeToString", ex.StackTrace + "-" + ex.Message);
              //  LogDebug.WriteLog("CoreESB.AcctInfoInqResTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string AcctListInqReqTypeToString(AcctList.AcctListInqReqType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<AcctListInqReqType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                for (int i = 0; i < MsgCnt.CustId.Length; i++)
                {
                    strBuildXML.Append("<CustId>" + MsgCnt.CustId[i] + "</CustId>");
                }

                strBuildXML.Append("<CustType>" + MsgCnt.CustType + "</CustType>");

                strBuildXML.Append("</AcctListInqReqType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("CoreESB.AcctListInqReqTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string AcctListInqResTypeToString(AcctList.AcctListInqResType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<AcctListInqResType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                for (int i = 0; i < MsgCnt.AcctRec.Length; i++)
                {
                    strBuildXML.Append("<AcctRec>");
                    strBuildXML.Append("<CustID>" + MsgCnt.AcctRec[i].CustId + "</CustID>");
                    strBuildXML.Append("<CustName>" + MsgCnt.AcctRec[i].CustName + "</CustName>");
                    strBuildXML.Append("<BankAcctId>");
                    strBuildXML.Append("<AcctId>" + MsgCnt.AcctRec[i].BankAcctId.AcctId + "</AcctId>");
                    strBuildXML.Append("<AcctType>" + MsgCnt.AcctRec[i].BankAcctId.AcctType + "</AcctType>");
                    strBuildXML.Append("<AcctSts>" + MsgCnt.AcctRec[i].BankAcctId.AcctSts + "</AcctSts>");
                    strBuildXML.Append("</BankAcctId>");
                    strBuildXML.Append("<BankInfo>");
                    strBuildXML.Append("<BranchId>" + MsgCnt.AcctRec[i].BankInfo.BranchId + "</BranchId>");
                    strBuildXML.Append("<BankName>" + MsgCnt.AcctRec[i].BankInfo.BankName + "</BankName>");
                    strBuildXML.Append("<ProdCD>" + MsgCnt.AcctRec[i].ProdCD + "</ProdCD>");
                    strBuildXML.Append("<ProdDesc>" + MsgCnt.AcctRec[i].ProdDesc + "</ProdDesc>");
                    strBuildXML.Append("<AvailBal>" + MsgCnt.AcctRec[i].AvailBal + "</AvailBal>");
                    strBuildXML.Append("</BankInfo>");
                    strBuildXML.Append("</AcctRec>");
                }
                strBuildXML.Append("</AcctListInqResType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //LogDebug.WriteLog("CoreESB.AcctListInqResTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }

        private string GetDMNgayNghi(string requestID)
        {
            try
            {
                string fromDate = DateTime.Now.ToString("dd/MM/yyyy");

                TreasuryConnect.AppHdrType appHdr = new TreasuryConnect.AppHdrType();
                appHdr.CharSet = "UTF-8";
                appHdr.SvcVer = "1.0";

                TreasuryConnect.PairsType nsFrom = new TreasuryConnect.PairsType();
                nsFrom.Id = "ETAX";
                nsFrom.Name = "ETAX";

                TreasuryConnect.PairsType nsTo = new TreasuryConnect.PairsType();
                nsTo.Id = "CORE";
                nsTo.Name = "CORE";

                TreasuryConnect.PairsType[] listOfNsTo = new TreasuryConnect.PairsType[1];
                listOfNsTo[0] = nsTo;

                TreasuryConnect.PairsType BizSvc = new TreasuryConnect.PairsType();
                BizSvc.Id = "TreasuryConnect";
                BizSvc.Name = "TreasuryConnect";
                DateTime TransDt = DateTime.Now;
                appHdr.From = nsFrom;
                appHdr.To = listOfNsTo;
                appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                appHdr.MsgPreId = "11111111";
                appHdr.BizSvc = BizSvc;
                appHdr.TransDt = TransDt;

                TreasuryConnect.GetHolidayReqType msgReq = new TreasuryConnect.GetHolidayReqType();

                msgReq.AppHdr = appHdr;
                msgReq.requestId = requestID;
                msgReq.fromDate = fromDate;


                log.Info("msg truy vấn ngày nghỉ: " + msgReq);
                string sErrorCode = "";
                string sErrorMSg = "";
                TreasuryConnect.GetHolidayRespType msgRes = sendMsg(msgReq, ref sErrorCode, ref sErrorMSg);

                log.Info("msg respon" + msgRes);
                if (msgRes.RespSts.Sts == "0")
                {

                    string result = "";
                    result += "<PARAMETER>";
                    result += "<errorCode>0</errorCode>";
                    result += "<errorDesc>Successfull</errorDesc>";
                    result += "<listHolidayDt>";
                    for (int i = 0; i < msgRes.listHolidays.Length; i++)
                    {
                        string holiDesc = msgRes.listHolidays[i].holiDesc;
                        string holiDt = msgRes.listHolidays[i].holiDt;
                        result += "<element>";
                        result += "<holiDesc>" + holiDesc.Replace("&", "").Replace("'", "") + "</holiDesc>";
                        result += "<holiDt>" + holiDt + "</holiDt>";
                        result += "</element>";
                    }
                    result += "</listHolidayDt></PARAMETER>";

                    return result;
                }
                else
                {
                   log.Error("GetDMNgayNghi: " + msgRes.RespSts.Sts + " ErrMsg:" + msgRes.RespSts.ErrMsg);
                    string ret = ("<PARAMETER><errorCode>99</errorCode><errorDesc>False</errorDesc><listHolidayDt><element><holiDesc/><holiDt/></element></listHolidayDt></PARAMETER>");
                    return ret;
                }

            }
            catch (Exception ex)
            {

                string ret = ("<PARAMETER><PARAMS><errorCode>99</errorCode><errorDesc>" + (ex.Message.ToString() + "</errorDesc><listHolidayDt><element><holiDesc/><holiDt/></element></listHolidayDt></PARAMS></PARAMETER>"));

                log.Error(ex.Message + "-" + ex.StackTrace); //LogDebug.WriteLog("GetDMNgayNghi: " + ex.Message, EventLogEntryType.Error);
                return ret;
            }
            return "";
        }

        private TreasuryConnect.GetHolidayRespType sendMsg(TreasuryConnect.GetHolidayReqType MsgCnt, ref string errCode, ref string errMsg)
        {

            string strID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_nhandien_core.NextVal");
            string strSQL = "";
            strSQL = "INSERT INTO tcs_log_nhandien_core (ID,TIME_UPDATE,DESCRIPTION) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += GetHolidayReqTypeToString(MsgCnt);
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                TreasuryConnect.PortTypeClient porttypeClient;
                TreasuryConnect.GetHolidayRespType returndata;
                try
                {
                    porttypeClient = new TreasuryConnect.PortTypeClient();
                    returndata = porttypeClient.GetHoliday(MsgCnt);

                }
                catch (Exception ex1)
                {
                    errCode = "TO999";
                    throw ex1;
                }
                strSQL = "UPDATE tcs_log_nhandien_core SET MSG_IN = '";
                string kq = GetHolidayRespTypeToString(returndata);
                strSQL += kq.ToString().Trim().Replace("\0", "") + "' WHERE ID=";
                strSQL += strID;
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                return returndata;

            }
            catch (Exception ex)
            {
               // VBOracleLib.LogApp.AddDebug("TreasuryConnect.GetHolidayRespType", ex.StackTrace + "-" + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace); //LogDebug.WriteLog("TreasuryConnect.GetHolidayRespType" + ex.Message, EventLogEntryType.Error);
                errMsg = "CoreESb.sendMsg function error:" + ex.Message;
                return null;
            }

        }
        private string GetHolidayReqTypeToString(TreasuryConnect.GetHolidayReqType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<GetHolidayReqType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<requestId>" + MsgCnt.requestId + "</requestId>");
                strBuildXML.Append("<fromDate>" + MsgCnt.fromDate + "</fromDate>");

                strBuildXML.Append("</GetHolidayReqType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("CoreESB.GetHolidayReqTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string GetHolidayRespTypeToString(TreasuryConnect.GetHolidayRespType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<GetHolidayRespType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                for (int i = 0; i < MsgCnt.listHolidays.Length; i++)
                {
                    strBuildXML.Append("<listHolidays>");
                    strBuildXML.Append("<holiDesc>" + MsgCnt.listHolidays[i].holiDesc + "</holiDesc>");
                    strBuildXML.Append("<holiDt>" + MsgCnt.listHolidays[i].holiDt + "</holiDt>");
                    strBuildXML.Append("</listHolidays>");

                }
                strBuildXML.Append("<RespSts>");
                strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrCd + "</ErrCd>");
                strBuildXML.Append("<ErrInfo>" + MsgCnt.RespSts.ErrInfo + "</ErrInfo>");
                strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrMsg + "</ErrMsg>");
                strBuildXML.Append("<Sts>" + MsgCnt.RespSts.Sts + "</Sts>");
                strBuildXML.Append("</RespSts>");

                strBuildXML.Append("</GetHolidayRespType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("CoreESB.GetHolidayRespTypeToString", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("CoreESB.GetHolidayRespTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        //phần này làm cho get danh sách điện từ ngân hàng khác
        private Boolean CheckRef(string ref_core)
        {
            bool result = true;
            DataSet ds = new DataSet();
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT * FROM tcs_chungtu_ttsp WHERE SO_REF_CORE='" + ref_core + "'";
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error(ex.Message + "-" + ex.StackTrace); //LogDebug.WriteLog("CheckRef: " + ex.Message, EventLogEntryType.Error);
                return false;
            }
           
            return result;
        }
        public void InsertDienNHKhac(string v_ref_core, string tk_no, string tk_co, string ten_nnthue, string ma_nt, string ty_gia, string ttien, string payment_detail, string ten_cqthu, string benAcNo, string senAcNo, string fileTrfRefNo, string sortCode, string content_ex)
        {

            //insert tcs_chungtu_ttsp
            StringBuilder sbsql = new StringBuilder();

            sbsql.Append("INSERT INTO tcs_chungtu_ttsp (ID, ");
            sbsql.Append("SO_REF_CORE, ");
            sbsql.Append("SO_CT, ");
            sbsql.Append("TK_NO, ");
            sbsql.Append("TK_CO, ");
            sbsql.Append("TEN_NNTHUE, ");
            sbsql.Append("TK_KH_NH, ");
            sbsql.Append("BT_CITAD, ");
            sbsql.Append("MA_NH_A, ");
            sbsql.Append("MA_NT, ");
            sbsql.Append("ty_gia, ");
            sbsql.Append("ttien, ");
            sbsql.Append("PAYMENT_DETAIL, ");
            sbsql.Append("content_ex, ");
            sbsql.Append("TRANG_THAI, ");
            sbsql.Append("ngay_ht, ");
            sbsql.Append("TEN_CQTHU) ");

            sbsql.Append("VALUES(TO_CHAR(SYSDATE,'RRRRMMDD')||LPAD(seq_chungtu_ttsp.NEXTVAL,10,'0'), ");
            sbsql.Append(":SO_REF_CORE, ");
            sbsql.Append(":SO_CT, ");
            sbsql.Append(":TK_NO, ");
            sbsql.Append(":TK_CO, ");
            sbsql.Append(":TEN_NNTHUE, ");
            sbsql.Append(":TK_KH_NH, ");
            sbsql.Append(":BT_CITAD, ");
            sbsql.Append(":MA_NH_A, ");
            sbsql.Append(":MA_NT, ");
            sbsql.Append(":ty_gia, ");
            sbsql.Append(":ttien, ");
            sbsql.Append(":PAYMENT_DETAIL, ");
            sbsql.Append(":content_ex, ");
            sbsql.Append(":TRANG_THAI, ");
            sbsql.Append("sysdate, ");
            sbsql.Append(":TEN_CQTHU) ");
            try
            {
                IDbDataParameter[] p = new IDbDataParameter[15];

                string trang_thai = DBNull.Value.ToString();
                trang_thai = "01";

                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_REF_CORE";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = v_ref_core;

                p[1] = new OracleParameter();
                p[1].ParameterName = "SO_CT";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = DBNull.Value;


                p[2] = new OracleParameter();
                p[2].ParameterName = "TK_NO";
                p[2].DbType = DbType.String;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = "";

                p[3] = new OracleParameter();
                p[3].ParameterName = "TK_CO";
                p[3].DbType = DbType.String;
                p[3].Direction = ParameterDirection.Input;
                p[3].Value = "";

                p[4] = new OracleParameter();
                p[4].ParameterName = "TEN_NNTHUE";
                p[4].DbType = DbType.String;
                p[4].Direction = ParameterDirection.Input;
                p[4].Value = ten_nnthue;

                p[5] = new OracleParameter();
                p[5].ParameterName = "TK_KH_NH";
                p[5].DbType = DbType.String;
                p[5].Direction = ParameterDirection.Input;
                p[5].Value = senAcNo;

                p[6] = new OracleParameter();
                p[6].ParameterName = "BT_CITAD";
                p[6].DbType = DbType.String;
                p[6].Direction = ParameterDirection.Input;
                p[6].Value = fileTrfRefNo;

                p[7] = new OracleParameter();
                p[7].ParameterName = "MA_NH_A";
                p[7].DbType = DbType.String;
                p[7].Direction = ParameterDirection.Input;
                p[7].Value = sortCode;

                p[8] = new OracleParameter();
                p[8].ParameterName = "MA_NT";
                p[8].DbType = DbType.String;
                p[8].Direction = ParameterDirection.Input;
                p[8].Value = ma_nt;

                p[9] = new OracleParameter();
                p[9].ParameterName = "ty_gia";
                p[9].DbType = DbType.String;
                p[9].Direction = ParameterDirection.Input;
                p[9].Value = ty_gia;

                p[10] = new OracleParameter();
                p[10].ParameterName = "ttien";
                p[10].DbType = DbType.String;
                p[10].Direction = ParameterDirection.Input;
                p[10].Value = ttien;

                p[11] = new OracleParameter();
                p[11].ParameterName = "PAYMENT_DETAIL";
                p[11].DbType = DbType.String;
                p[11].Direction = ParameterDirection.Input;
                p[11].Value = payment_detail;

                p[12] = new OracleParameter();
                p[12].ParameterName = "content_ex";
                p[12].DbType = DbType.String;
                p[12].Direction = ParameterDirection.Input;
                p[12].Value = content_ex;

                p[13] = new OracleParameter();
                p[13].ParameterName = "TRANG_THAI";
                p[13].DbType = DbType.String;
                p[13].Direction = ParameterDirection.Input;
                p[13].Value = trang_thai;

                p[14] = new OracleParameter();
                p[14].ParameterName = "TEN_CQTHU";
                p[14].DbType = DbType.String;
                p[14].Direction = ParameterDirection.Input;
                p[14].Value = ten_cqthu;

                DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("CoreESB.InsertDienNHKhac", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("InsertDienNHKhac.Error: " + ex.Message, EventLogEntryType.Error);
            }
           

        }
        private string GetDSCitad(string requestID)
        {
            try
            {
      
                int day = 1;

                try
                {
                    day = Int16.Parse(Business.Common.mdlCommon.TCS_GETTS("SO_NGAY_NHAN_DIEN"));
                }
                catch (Exception exx)
                {
                    day = 1;
                }

                string toDate = Get_core_date(); // DateTime.Now.ToString("yyyyMMdd");
                string fromDate = "";
                DateTime tDate = DateTime.ParseExact(toDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                DateTime fDate = tDate.AddDays(-day);

                //string fromDate = DateTime.Now.ToString("dd/MM/yyyy");
                //Core chỉ trả ra date, sau nhiều lệnh sẽ phải truyền giờ để hạn chế số lượng lệnh mỗi lần truy vấn dd/MM/yyyy hh24:mi:ss
                toDate = tDate.ToString("dd/MM/yyyy"); // 23:59:59
                fromDate = fDate.ToString("dd/MM/yyyy"); // 00:00:00 // hh24:mi:ss


                TreasuryConnect.AppHdrType appHdr = new TreasuryConnect.AppHdrType();
                appHdr.CharSet = "UTF-8";
                appHdr.SvcVer = "1.0";

                TreasuryConnect.PairsType nsFrom = new TreasuryConnect.PairsType();
                nsFrom.Id = "ETAX";
                nsFrom.Name = "ETAX";

                TreasuryConnect.PairsType nsTo = new TreasuryConnect.PairsType();
                nsTo.Id = "CORE";
                nsTo.Name = "CORE";

                TreasuryConnect.PairsType[] listOfNsTo = new TreasuryConnect.PairsType[1];
                listOfNsTo[0] = nsTo;

                TreasuryConnect.PairsType BizSvc = new TreasuryConnect.PairsType();
                BizSvc.Id = "TreasuryConnect";
                BizSvc.Name = "TreasuryConnect";
                DateTime TransDt = DateTime.Now;
                appHdr.From = nsFrom;
                appHdr.To = listOfNsTo;
                appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                appHdr.MsgPreId = "11111111";
                appHdr.BizSvc = BizSvc;
                appHdr.TransDt = TransDt;

                TreasuryConnect.GetTranCitadReqType msgReq = new TreasuryConnect.GetTranCitadReqType();

                msgReq.AppHdr = appHdr;
                msgReq.requestId = requestID;
                //nêu chỉ lấy điện trong ngày (fromtime - totime giống nhau)
                msgReq.fromTime = fromDate;
                msgReq.toTime = toDate;

                log.Info("msg truy vấn điện từ ngân hàng khác: " + msgReq);
                string sErrorCode = "";
                string sErrorMSg = "";
                TreasuryConnect.GetTranCitadRespType msgRes = sendMsg(msgReq, ref sErrorCode, ref sErrorMSg);
                //LogDebug.WriteLog("msg respon" + msgRes, System.Diagnostics.EventLogEntryType.Information);

                if (sErrorCode.Equals("TO999"))
                {
                    return "Lỗi khi nhận điện citad từ corebank";
                }

                if (msgRes.RespSts.Sts == "0")
                {
                    string result = "";
                    if (msgRes.listCitadTransactions.Length > 0)
                    {
                        for (int i = 0; i < msgRes.listCitadTransactions.Length; i++)
                        {
                            string refNo = msgRes.listCitadTransactions[i].refNo;   //số ref core là key
                            string processStatus = msgRes.listCitadTransactions[i].processStatus; //trạng thái lệnh
                            //check đã có số ref core chưa, có rồi thì không insert nữa
                            if (CheckRef(refNo) && processStatus.ToUpper().Equals("CONFIRMED"))
                            {
                                string senAcNo = msgRes.listCitadTransactions[i].senAcNo; //Số Tài khoản KH chuyển 
                                string senName = msgRes.listCitadTransactions[i].senName;    //tên người gửi -----------
                                string fileTrfRefNo = msgRes.listCitadTransactions[i].fileTrfRefNo; //Mã NH giữ TK NNT
                                string sortCode = msgRes.listCitadTransactions[i].sortCode;        //Số buttoan trên CITAD

                                //Không trùng số ref thì tiến hành lấy các thông tin khác rồi insert vào bảng dữ liệu
                                string benAcNo = msgRes.listCitadTransactions[i].benAcNo;  //Tài khoản người nhận   //<STK_THU>7111</STK_THU> ?
                                string benName = msgRes.listCitadTransactions[i].benName;   //Tên người nhận	Tên cơ quan thu (*)
                                string transferAmount = msgRes.listCitadTransactions[i].transferAmount; // số tiền giao dịch
                                string transferCcyCd = msgRes.listCitadTransactions[i].transferCcyCd;   // mã tiền tệ
                                
                                string customerRemarks = msgRes.listCitadTransactions[i].customerRemarks; //diễn giải
                                string crAcNo = msgRes.listCitadTransactions[i].crAcNo;  //tk_co //ko phải?
                                string drAcNo = msgRes.listCitadTransactions[i].drAcNo;  //tk ghi nợ //ko phải?
                                string finalRate = msgRes.listCitadTransactions[i].finalRate; //tỷ giá
                                //các thông tin dưới có thể không cần insert
                                string crCcyCd = msgRes.listCitadTransactions[i].crCcyCd; //nguyên tên tài khoản có
                                string crLcyAmount = msgRes.listCitadTransactions[i].crLcyAmount; //số tiền quy đổi ra local ccy
                                string drCcyCd = msgRes.listCitadTransactions[i].drCcyCd; //nguyên tên tài khoản ghi nợ
                                string drLcyAmount = msgRes.listCitadTransactions[i].drLcyAmount; //số tiền quy đổi ra local ccy
                                string posCd = msgRes.listCitadTransactions[i].posCd; //mã chi nhánh

                                string extraData = msgRes.listCitadTransactions[i].extraData; //chuỗi  3000 ký tự của citad 2.5
                                //if (extraData.Trim().Length > 0)
                                //{
                                //    //extraData = Business.Common.mdlCommon.DecodeBase64_ContentEx(extraData);
                                //    extraData = Business.Common.mdlCommon.DecodeBase64_ContentEx(extraData);
                                //}
                                //else {
                                //    extraData = "";
                                //}

                                if (extraData.Trim().Length <= 0)
                                {
                                    extraData = "";
                                }


                                transferCcyCd = "VND";
                                //if (transferCcyCd.ToUpper().Equals("704"))
                                //{
                                //    transferCcyCd = "VND";
                                //}
                                //else if (transferCcyCd.ToUpper().Equals("840"))
                                //{
                                //    transferCcyCd = "USD";
                                //}
                                //tiến hành insert dữ liệu
                                InsertDienNHKhac(refNo, drAcNo, crAcNo, senName, transferCcyCd, finalRate, transferAmount, customerRemarks, benName, benAcNo, senAcNo, fileTrfRefNo, sortCode, extraData);

                            
                            }
                        }
                    }
                    else
                    {
                        return result;
                    }
                   

                    return result;
                }
                else
                {
                    
                    log.Error("GetDSCitad: " + msgRes.RespSts.Sts + " ErrMsg:" + msgRes.RespSts.ErrMsg);
                    return "";
                }

            }
            catch (Exception ex)
            {
              //  VBOracleLib.LogApp.AddDebug("CoreESB.GetDSCitad", ex.StackTrace + "-" + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("GetDSCitad: " + ex.Message, EventLogEntryType.Error);
                return "";
            }
            return "";
        }

        public void InsertLogDienNHKhac(string msg_in, string ID)
        {

            try
            {
                string islog = ConfigurationManager.AppSettings.Get("LOGCORE.ON").ToString();
                if (islog == "1")
                {
                    string path = ConfigurationManager.AppSettings.Get("FILE_LOG_CITAD").ToString();
                    string dir = System.IO.Path.GetDirectoryName(path);
                    dir = dir + "\\" + "LogCore";
                    if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);
                    path = dir + "\\msg_core" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".txt";
                    System.IO.File.WriteAllText(path, msg_in);
                }
            }
            catch (Exception ex) {
                log.Error(ex.Message + "-" + ex.StackTrace); //LogDebug.WriteLog("InsertLogDienNHKhac: " + ex.Message, EventLogEntryType.Error);
            }
        }
        public TreasuryConnect.GetTranCitadRespType sendMsg(TreasuryConnect.GetTranCitadReqType MsgCnt, ref string errCode, ref string errMsg)
        {

            string strID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_nhandien_core.NextVal");
            string strSQL = "";
            strSQL = "INSERT INTO tcs_log_nhandien_core (ID,TIME_UPDATE,DESCRIPTION) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += GetTranCitadReqTypeToString(MsgCnt);
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                TreasuryConnect.PortTypeClient porttypeClient;
                TreasuryConnect.GetTranCitadRespType returndata;
                try
                {
                    porttypeClient = new TreasuryConnect.PortTypeClient();
                    returndata = porttypeClient.GetTranCitad(MsgCnt);
                   // returndata.listCitadTransactions.

                }
                catch (Exception ex1)
                {
                    errCode = "TO999";
                    throw ex1;
                }

                try
                {
                    string kq = GetTranCitadRespTypeToString(returndata).ToString().Trim().Replace("\0", "");

                    InsertLogDienNHKhac(kq, strID);
                }
                catch (Exception exx)
                {
                    log.Error(exx.Message + "-" + exx.StackTrace); //LogDebug.WriteLog("GetTranCitadRespTypeToString: " + exx.Message, EventLogEntryType.Error);
                }
               
                return returndata;

            }
            catch (Exception ex)
            {
              //  VBOracleLib.LogApp.AddDebug("TreasuryConnect.GetTranCitadRespType", ex.StackTrace + "-" + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace); //LogDebug.WriteLog("TreasuryConnect.GetTranCitadRespType" + ex.Message, EventLogEntryType.Error);
                errMsg = "CoreESb.sendMsg function error:" + ex.Message;
                return null;
            }

        }
        private string GetTranCitadReqTypeToString(TreasuryConnect.GetTranCitadReqType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<GetTranCitadReqType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<requestId>" + MsgCnt.requestId + "</requestId>");
                strBuildXML.Append("<fromTime>" + MsgCnt.fromTime + "</fromTime>");
                strBuildXML.Append("<toTime>" + MsgCnt.toTime + "</toTime>");
                strBuildXML.Append("</GetTranCitadReqType>");

                //LogDebug.WriteLog("MESSAGE: " + strBuildXML.ToString(), EventLogEntryType.Error);
                return strBuildXML.ToString();

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("GetTranCitadReqTypeToString", ex.StackTrace + "-" + ex.Message);
             //   LogDebug.WriteLog("CoreESB.GetTranCitadReqTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string GetTranCitadRespTypeToString(TreasuryConnect.GetTranCitadRespType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<GetTranCitadRespType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");

                if (MsgCnt.listCitadTransactions.Length > 0)
                {
                    strBuildXML.Append("<listCitadTransactions>");
                    for (int i = 0; i < MsgCnt.listCitadTransactions.Length; i++)
                    {
                        strBuildXML.Append("<element>");
                        strBuildXML.Append("<authDt>" + MsgCnt.listCitadTransactions[i].authDt + "</authDt>");
                        strBuildXML.Append("<authId>" + MsgCnt.listCitadTransactions[i].authId + "</authId>");
                        strBuildXML.Append("<benAcNo>" + MsgCnt.listCitadTransactions[i].benAcNo + "</benAcNo>");
                        strBuildXML.Append("<benAdd1>" + MsgCnt.listCitadTransactions[i].benAdd1 + "</benAdd1>");
                        strBuildXML.Append("<benAdd2>" + MsgCnt.listCitadTransactions[i].benAdd2 + "</benAdd2>");
                        strBuildXML.Append("<benCity>" + MsgCnt.listCitadTransactions[i].benCity + "</benCity>");
                        strBuildXML.Append("<benIdExpiryDt>" + MsgCnt.listCitadTransactions[i].benIdExpiryDt + "</benIdExpiryDt>");
                        strBuildXML.Append("<benIdIssuePlace>" + MsgCnt.listCitadTransactions[i].benIdIssuePlace + "</benIdIssuePlace>");
                        strBuildXML.Append("<benIdNo>" + MsgCnt.listCitadTransactions[i].benIdNo + "</benIdNo>");
                        strBuildXML.Append("<benIdType>" + MsgCnt.listCitadTransactions[i].benIdType + "</benIdType>");
                        strBuildXML.Append("<benName>" + MsgCnt.listCitadTransactions[i].benName + "</benName>");
                        strBuildXML.Append("<conversionRate>" + MsgCnt.listCitadTransactions[i].conversionRate + "</conversionRate>");
                        strBuildXML.Append("<crAcNo>" + MsgCnt.listCitadTransactions[i].crAcNo + "</crAcNo>");
                        strBuildXML.Append("<crAmount>" + MsgCnt.listCitadTransactions[i].crAmount + "</crAmount>");
                        strBuildXML.Append("<crCcyCd>" + MsgCnt.listCitadTransactions[i].crCcyCd + "</crCcyCd>");
                        strBuildXML.Append("<crLcyAmount>" + MsgCnt.listCitadTransactions[i].crLcyAmount + "</crLcyAmount>");
                        strBuildXML.Append("<customerRemarks>" + MsgCnt.listCitadTransactions[i].customerRemarks + "</customerRemarks>");
                        strBuildXML.Append("<drAcNo>" + MsgCnt.listCitadTransactions[i].drAcNo + "</drAcNo>");
                        strBuildXML.Append("<drAmount>" + MsgCnt.listCitadTransactions[i].drAmount + "</drAmount>");
                        strBuildXML.Append("<drCcyCd>" + MsgCnt.listCitadTransactions[i].drCcyCd + "</drCcyCd>");
                        strBuildXML.Append("<drLcyAmount>" + MsgCnt.listCitadTransactions[i].drLcyAmount + "</drLcyAmount>");
                        strBuildXML.Append("<finalRate>" + MsgCnt.listCitadTransactions[i].finalRate + "</finalRate>");
                        strBuildXML.Append("<internalRemarks>" + MsgCnt.listCitadTransactions[i].internalRemarks + "</internalRemarks>");
                        strBuildXML.Append("<mkrDt>" + MsgCnt.listCitadTransactions[i].mkrDt + "</mkrDt>");
                        strBuildXML.Append("<mkrId>" + MsgCnt.listCitadTransactions[i].mkrId + "</mkrId>");
                        strBuildXML.Append("<modCd>" + MsgCnt.listCitadTransactions[i].modCd + "</modCd>");
                        strBuildXML.Append("<posCd>" + MsgCnt.listCitadTransactions[i].posCd + "</posCd>");
                        strBuildXML.Append("<processStatus>" + MsgCnt.listCitadTransactions[i].processStatus + "</processStatus>");
                        strBuildXML.Append("<refNo>" + MsgCnt.listCitadTransactions[i].refNo + "</refNo>");
                        strBuildXML.Append("<senAcNo>" + MsgCnt.listCitadTransactions[i].senAcNo + "</senAcNo>");
                        strBuildXML.Append("<senAdd1>" + MsgCnt.listCitadTransactions[i].senAdd1 + "</senAdd1>");
                        strBuildXML.Append("<senAdd2>" + MsgCnt.listCitadTransactions[i].senAdd2 + "</senAdd2>");
                        strBuildXML.Append("<senCity>" + MsgCnt.listCitadTransactions[i].senCity + "</senCity>");
                        strBuildXML.Append("<senIdExpiryDt>" + MsgCnt.listCitadTransactions[i].senIdExpiryDt + "</senIdExpiryDt>");
                        strBuildXML.Append("<senIdIssuePlace>" + MsgCnt.listCitadTransactions[i].senIdIssuePlace + "</senIdIssuePlace>");
                        strBuildXML.Append("<senIdNo>" + MsgCnt.listCitadTransactions[i].senIdNo + "</senIdNo>");
                        strBuildXML.Append("<senIdType>" + MsgCnt.listCitadTransactions[i].senIdType + "</senIdType>");
                        strBuildXML.Append("<senName>" + MsgCnt.listCitadTransactions[i].senName + "</senName>");
                        strBuildXML.Append("<transferAmount>" + MsgCnt.listCitadTransactions[i].transferAmount + "</transferAmount>");
                        strBuildXML.Append("<transferCcyCd>" + MsgCnt.listCitadTransactions[i].transferCcyCd + "</transferCcyCd>");
                        strBuildXML.Append("<transferType>" + MsgCnt.listCitadTransactions[i].transferType + "</transferType>");
                        strBuildXML.Append("<treasuryRate>" + MsgCnt.listCitadTransactions[i].treasuryRate + "</treasuryRate>");
                        strBuildXML.Append("<valDt>" + MsgCnt.listCitadTransactions[i].valDt + "</valDt>");
                        strBuildXML.Append("<walkInBen>" + MsgCnt.listCitadTransactions[i].walkInBen + "</walkInBen>");
                        strBuildXML.Append("<fileTrfRefNo>" + MsgCnt.listCitadTransactions[i].fileTrfRefNo + "</fileTrfRefNo>");
                        strBuildXML.Append("<sortCode>" + MsgCnt.listCitadTransactions[i].sortCode + "</sortCode>");
                        strBuildXML.Append("<extraData>" + MsgCnt.listCitadTransactions[i].extraData + "</extraData>");
                        strBuildXML.Append("</element>");
                    }
                    strBuildXML.Append("</listCitadTransactions>");
                }
                strBuildXML.Append("<RespSts>");
                strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrCd + "</ErrCd>");
                strBuildXML.Append("<ErrInfo>" + MsgCnt.RespSts.ErrInfo + "</ErrInfo>");
                strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrMsg + "</ErrMsg>");
                strBuildXML.Append("<Sts>" + MsgCnt.RespSts.Sts + "</Sts>");
                strBuildXML.Append("</RespSts>");

                strBuildXML.Append("</GetTranCitadRespType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //VBOracleLib.LogApp.AddDebug("GetTranCitadRespTypeToString", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("CoreESB.GetTranCitadRespTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }

        private string Get_core_date()
        {
            try
            {

                GetCoreDate.AppHdrType appHdr = new GetCoreDate.AppHdrType();
                appHdr.CharSet = "UTF-8";
                appHdr.SvcVer = "1.0";

                GetCoreDate.PairsType nsFrom = new GetCoreDate.PairsType();
                nsFrom.Id = "ETAX";
                nsFrom.Name = "ETAX";

                GetCoreDate.PairsType nsTo = new GetCoreDate.PairsType();
                nsTo.Id = "CORE";
                nsTo.Name = "CORE";

                GetCoreDate.PairsType[] listOfNsTo = new GetCoreDate.PairsType[1];
                listOfNsTo[0] = nsTo;

                GetCoreDate.PairsType BizSvc = new GetCoreDate.PairsType();
                BizSvc.Id = "TreasuryConnect";
                BizSvc.Name = "TreasuryConnect";
                DateTime TransDt = DateTime.Now;
                appHdr.From = nsFrom;
                appHdr.To = listOfNsTo;
                appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                appHdr.MsgPreId = "11111111";
                appHdr.BizSvc = BizSvc;
                appHdr.TransDt = TransDt;

                GetCoreDate.CoreDateInquiryReqType msgReq = new GetCoreDate.CoreDateInquiryReqType();

                msgReq.AppHdr = appHdr;
                msgReq.BrCode = "BR0001";
                msgReq.Mode = "INTER";


                string sErrorCode = "";
                string sErrorMSg = "";
                GetCoreDate.CoreDateInquiryResType msgRes = sendMsg(msgReq, ref sErrorCode, ref sErrorMSg);

                if (msgRes.RespSts.Sts == "0")
                {
                    return msgRes.ReturnDate;
                }
                else
                {
                    return DateTime.Now.ToString("yyyyMMdd");
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("Get_core_date", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("Get_core_date: " + ex.Message, EventLogEntryType.Error);
                return DateTime.Now.ToString("yyyyMMdd");
            }
            return DateTime.Now.ToString("yyyyMMdd");
        }

        private GetCoreDate.CoreDateInquiryResType sendMsg( GetCoreDate.CoreDateInquiryReqType MsgCnt, ref string errCode, ref string errMsg)
        {

            string strID = Business.Common.mdlCommon.getDataKey("seq_tcs_log_nhandien_core.NextVal");
            string strSQL = "";
            strSQL = "INSERT INTO tcs_log_nhandien_core (ID,TIME_UPDATE,DESCRIPTION) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += CoreDateInquiryReqTypeToString(MsgCnt);
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                GetCoreDate.PortTypeClient porttypeClient;
                GetCoreDate.CoreDateInquiryResType returndata;
                try
                {
                    porttypeClient = new GetCoreDate.PortTypeClient();
                    returndata = porttypeClient.Inquiry(MsgCnt);

                }
                catch (Exception ex1)
                {
                    errCode = "TO999";
                    throw ex1;
                }
                strSQL = "UPDATE tcs_log_nhandien_core SET MSG_IN = '";
                string kq = CoreDateInquiryResTypeToString(returndata);
                strSQL += kq.ToString().Trim().Replace("\0", "") + "' WHERE ID=";
                strSQL += strID;
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                return returndata;

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("CoreDateInquiryResType", ex.StackTrace + "-" + ex.Message);
              //  LogDebug.WriteLog("GetCoreDate.CoreDateInquiryResType " + ex.Message, EventLogEntryType.Error);
                errMsg = "CoreESb.sendMsg function error:" + ex.Message;
                return null;
            }

        }
        private string CoreDateInquiryReqTypeToString(GetCoreDate.CoreDateInquiryReqType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<CoreDateInquiryReqType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<BrCode>" + MsgCnt.BrCode + "</BrCode>");
                strBuildXML.Append("<Mode>" + MsgCnt.Mode + "</Mode>");

                strBuildXML.Append("</CoreDateInquiryReqType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //VBOracleLib.LogApp.AddDebug("CoreDateInquiryReqTypeToString", ex.StackTrace + "-" + ex.Message);
               // LogDebug.WriteLog("CoreESB.CoreDateInquiryReqTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string CoreDateInquiryResTypeToString(GetCoreDate.CoreDateInquiryResType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<CoreDateInquiryResType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<ReturnDate>" + MsgCnt.ReturnDate + "</ReturnDate>");
                strBuildXML.Append("<RespSts>");
                strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrCd + "</ErrCd>");
                strBuildXML.Append("<ErrInfo>" + MsgCnt.RespSts.ErrInfo + "</ErrInfo>");
                strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrMsg + "</ErrMsg>");
                strBuildXML.Append("<Sts>" + MsgCnt.RespSts.Sts + "</Sts>");
                strBuildXML.Append("</RespSts>");

                strBuildXML.Append("</CoreDateInquiryResType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //VBOracleLib.LogApp.AddDebug("CoreDateInquiryResTypeToString", ex.StackTrace + "-" + ex.Message);
              //  LogDebug.WriteLog("CoreESB.CoreDateInquiryResTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }


        private string SendContentEx(string msg)
        {
            try
            {
                log.Info("1.SendContentEx:" + msg);
                System.Xml.XmlDocument VXDOC = new System.Xml.XmlDocument();
                VXDOC.LoadXml(msg);
                string v_strSo_CT = VXDOC.SelectSingleNode("Customs/DATA/v_strSo_CT").InnerText;
                string v_Ref = VXDOC.SelectSingleNode("Customs/DATA/v_Ref").InnerText;
                string v_ContentEx = VXDOC.SelectSingleNode("Customs/DATA/v_ContentEx").InnerText;
                string v_KenhCT = VXDOC.SelectSingleNode("Customs/DATA/v_KenhCT").InnerText;
                string v_str_errcode = "";
                ContentEx(v_strSo_CT, v_Ref, v_ContentEx, v_KenhCT, ref  v_str_errcode);
                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<Customs>");
                strBuildXML.Append("<Errcode>" + v_str_errcode + "</Errcode>");
                strBuildXML.Append("</Customs>");
                log.Info("2.SendContentEx:" + strBuildXML.ToString());
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //VBOracleLib.LogApp.AddDebug("Lỗi Khi send ContentEx", ex.StackTrace + "-" + ex.Message);
               // VBOracleLib.LogDebug.WriteLog("Lỗi Khi send ContentEx " + ex.StackTrace + " Errmess: " + ex.Message, EventLogEntryType.Information);
            }
            return null;
            
        }

        private string ContentEx(string v_strSo_CT, string v_Ref, string v_ContentEx, string v_KenhCT,ref string v_str_errcode)
        {
            try
            {
                TreasuryConnect.AppHdrType appHdr = new TreasuryConnect.AppHdrType();
                appHdr.CharSet = "UTF-8";
                appHdr.SvcVer = "1.0";

                TreasuryConnect.PairsType nsFrom = new TreasuryConnect.PairsType();
                nsFrom.Id = "ETAX";
                nsFrom.Name = "ETAX";

                TreasuryConnect.PairsType nsTo = new TreasuryConnect.PairsType();
                nsTo.Id = "CORE";
                nsTo.Name = "CORE";

                TreasuryConnect.PairsType[] listOfNsTo = new TreasuryConnect.PairsType[1];
                listOfNsTo[0] = nsTo;

                TreasuryConnect.PairsType BizSvc = new TreasuryConnect.PairsType();
                BizSvc.Id = "TreasuryConnect";
                BizSvc.Name = "TreasuryConnect";
                DateTime TransDt = DateTime.Now;
                appHdr.From = nsFrom;
                appHdr.To = listOfNsTo;
                appHdr.MsgId = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                appHdr.MsgPreId = "11111111";
                appHdr.BizSvc = BizSvc;
                appHdr.TransDt = TransDt;

                TreasuryConnect.PushToCoreReqType msgReq = new TreasuryConnect.PushToCoreReqType();

                msgReq.AppHdr = appHdr;
                msgReq.i_req_XML = v_ContentEx;
                //msgReq.i_reqTime = DateTime.Now.ToString("ddMMyyyy HH24:MI:ss").Replace(":","");
                msgReq.i_reqTime = DateTime.Now.ToString("ddMMyyyy HH24MIss");

               log.Info("msg truy gửi thông tin contentEx vào corebank: " + msgReq);
                string sErrorCode = "";
                string sErrorMSg = "";
                TreasuryConnect.PushToCoreRespType msgRes = sendMsg(msgReq, v_strSo_CT, v_ContentEx);
                log.Info("msg respon" + msgRes);

                if (sErrorCode.Equals("TO999"))
                {
                    return "Lỗi khi gửi thông tin content_ex cho corebank";
                }

                if (msgRes.RespSts.Sts == "0" && msgRes.o_err_cd == "0")
                {
                    v_str_errcode = "00000";
                }
                else
                {
                    log.Error("SendContentEx: " + msgRes.RespSts.Sts + " ErrMsg:" + msgRes.RespSts.ErrMsg);
                    return "TO999";
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //VBOracleLib.LogApp.AddDebug("SendContentEx: ", ex.StackTrace + "-" + ex.Message);
              //  LogDebug.WriteLog("SendContentEx: " + ex.Message, EventLogEntryType.Error);
                return "";
            }
            return "";
        }

        private TreasuryConnect.PushToCoreRespType sendMsg(TreasuryConnect.PushToCoreReqType MsgCnt, string so_ct, string v_ContentEx)
        {


            string strID = Business.Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_HDR_ID.NextVal");
            string strSQL = "";
            InsertSendContentEx(PushToCoreRespTypeToString(MsgCnt), strID, so_ct, v_ContentEx);
            //strSQL = "INSERT INTO TCS_LOG_CORE (ID,TIME_UPDATE,CONTENT_EX,SO_CT) VALUES(";
            //strSQL += strID;
            //strSQL += ",SYSDATE,'";
            //strSQL += PushToCoreRespTypeToString(MsgCnt);
            //strSQL += "','";
            //strSQL += so_ct;
            //strSQL += "')";
            //VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {

                TreasuryConnect.PortTypeClient porttypeClient = new TreasuryConnect.PortTypeClient();
                TreasuryConnect.PushToCoreRespType returndata = porttypeClient.PushToCore(MsgCnt);

                strSQL = "UPDATE TCS_LOG_CORE SET MSG_IN = '";
                string kq = PushToCoreRespTypeToString(returndata);


                strSQL += kq.ToString().Trim().Replace("\0", "") + "' WHERE ID=";
                strSQL += strID;
                VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                return returndata;

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace); //VBOracleLib.LogApp.AddDebug("PushToCoreRespType: ", ex.StackTrace + "-" + ex.Message);
               // VBOracleLib.LogDebug.WriteLog(ex, "Loi khi gui hach toan vao core", "clsCoreBank");
                return null;
            }

        }
        public void InsertSendContentEx(string msg_in, string ID, string so_ct, string v_ContentEx)
        {

            string msg = msg_in;
            if (msg.Length > 2000 && msg.Length < 4001)
            {
                msg = (msg + " ".PadRight(4001)).Substring(0, 4001);
            }

            string v_ContentEx_Decode = Business.Common.mdlCommon.DecodeBase64_ContentEx(v_ContentEx);

            StringBuilder sbsql = new StringBuilder();
            sbsql.Append("INSERT INTO TCS_LOG_CORE (ID,TIME_UPDATE,CONTENT_EX,SO_CT,description) VALUES(:ID,SYSDATE,:CONTENT_EX,:SO_CT,:description) ");
            try
            {
                IDbDataParameter[] p = new IDbDataParameter[4];

                p[0] = new OracleParameter();
                p[0].ParameterName = "ID";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = ID;

                p[1] = new OracleParameter();
                p[1].ParameterName = "CONTENT_EX";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = msg_in;

                p[2] = new OracleParameter();
                p[2].ParameterName = "SO_CT";
                p[2].DbType = DbType.String;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = so_ct;

                p[3] = new OracleParameter();
                p[3].ParameterName = "description";
                p[3].DbType = DbType.String;
                p[3].Direction = ParameterDirection.Input;
                p[3].Value = v_ContentEx_Decode;

                DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p);
            }
            catch (Exception ex)
            {
                try
                {
                    string islog = ConfigurationManager.AppSettings.Get("LOGCORE.ON").ToString();
                    if (islog == "1")
                    {
                        string path = ConfigurationManager.AppSettings.Get("FILE_LOG_LOGIN").ToString();
                        string dir = System.IO.Path.GetDirectoryName(path);
                        dir = dir + "\\" + "LogCore";
                        if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);
                        path = dir + "\\msg_core" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".txt";
                        System.IO.File.WriteAllText(path, msg_in);
                    }
                }
                catch (Exception e) { }
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("InsertSendContentEx.Error: " + ex.Message, EventLogEntryType.Error);
            }
        }
        private string PushToCoreRespTypeToString(TreasuryConnect.PushToCoreReqType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<PushToCoreReqType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<i_req_XML>" + MsgCnt.i_req_XML + "</i_req_XML>");
                strBuildXML.Append("<i_reqTime>" + MsgCnt.i_reqTime + "</i_reqTime>");
                strBuildXML.Append("</PushToCoreReqType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
              //  VBOracleLib.LogApp.AddDebug("PushToCoreRespTypeToString: ", ex.StackTrace + "-" + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("CoreESB.PushToCoreRespTypeToString: " + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
        private string PushToCoreRespTypeToString(TreasuryConnect.PushToCoreRespType MsgCnt)
        {
            string msg = "";
            try
            {

                StringBuilder strBuildXML = new StringBuilder();
                strBuildXML.Append("<PushToCoreRespType>");
                strBuildXML.Append("<AppHdr>");
                strBuildXML.Append("<CharSet>" + MsgCnt.AppHdr.CharSet + "</CharSet>");
                strBuildXML.Append("<SvcVer>" + MsgCnt.AppHdr.SvcVer + "</SvcVer>");
                strBuildXML.Append("<From>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.From.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.From.Name + "</Name>");
                strBuildXML.Append("</From>");
                strBuildXML.Append("<To>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.To[0].Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.To[0].Name + "</Name>");
                strBuildXML.Append("</To>");
                strBuildXML.Append("<MsgId>" + MsgCnt.AppHdr.MsgId + "</MsgId>");
                strBuildXML.Append("<MsgPreId>" + MsgCnt.AppHdr.MsgPreId + "</MsgPreId>");
                strBuildXML.Append("<BizSvc>");
                strBuildXML.Append("<Id>" + MsgCnt.AppHdr.BizSvc.Id + "</Id>");
                strBuildXML.Append("<Name>" + MsgCnt.AppHdr.BizSvc.Name + "</Name>");
                strBuildXML.Append("</BizSvc>");
                strBuildXML.Append("<TransDt>" + MsgCnt.AppHdr.TransDt.ToString() + "</TransDt>");
                strBuildXML.Append("<Signature>" + MsgCnt.AppHdr.Signature + "</Signature>");
                strBuildXML.Append("</AppHdr>");
                strBuildXML.Append("<o_err_cd>" + MsgCnt.o_err_cd + "</o_err_cd>");
                strBuildXML.Append("<o_err_txt>" + MsgCnt.o_err_txt + "</o_err_txt>");
                strBuildXML.Append("<RespSts>");
                strBuildXML.Append("<Sts>" + MsgCnt.RespSts.Sts + "</Sts>");
                if (MsgCnt.RespSts.Sts == "0")
                {
                    strBuildXML.Append("<ErrCd></ErrCd>");
                    strBuildXML.Append("<ErrMsg></ErrMsg>");
                    strBuildXML.Append("<ErrInfo></ErrInfo>");
                }
                else
                {
                    strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrCd + "</ErrCd>");
                    strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrMsg + "</ErrMsg>");
                    strBuildXML.Append("<ErrInfo>");
                    strBuildXML.Append("<Id>" + MsgCnt.RespSts.ErrInfo[0].Id + "</Id>");
                    strBuildXML.Append("<ErrCd>" + MsgCnt.RespSts.ErrInfo[0].ErrCd + "</ErrCd>");
                    strBuildXML.Append("<ErrMsg>" + MsgCnt.RespSts.ErrInfo[0].ErrMsg + "</ErrMsg>");
                    strBuildXML.Append("</ErrInfo>");
                }

                strBuildXML.Append("</RespSts>");
                strBuildXML.Append("</PushToCoreRespType>");
                return strBuildXML.ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// VBOracleLib.LogApp.AddDebug("PushToCoreRespTypeToString: ", ex.StackTrace + "-" + ex.Message);
              //  LogDebug.WriteLog("CoreESB.PushToCoreRespTypeToString" + ex.Message, EventLogEntryType.Error);
            }
            return msg;
        }
    }
}
