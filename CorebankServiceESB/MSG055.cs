﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CorebankServiceESB055
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG055
    {
        public MSG055()
        { 
        
        }

        [XmlElement("HEADER")]
        public HEADER Header;
        [XmlElement("BODY")]
        public BODY Body;



        public string MSG055toXML(MSG055 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG055));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG055 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CorebankServiceESB055.MSG055));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            CorebankServiceESB055.MSG055 LoadedObjTmp = (CorebankServiceESB055.MSG055)serializer.Deserialize(reader);

            return LoadedObjTmp;

        }

    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("CTU_HDR")]
        public CTU_HDR CTU_HDR;

    }

    public class CTU_HDR
    {
        public CTU_HDR()
        {

        }
        [XmlElement(Order = 1)]
        public string SO_THAM_CHIEU { get; set; }
        [XmlElement(Order = 2)]
        public string SHKB { get; set; }
        [XmlElement(Order = 3)]
        public string NGAY_KB { get; set; }
        [XmlElement(Order = 4)]
        public string MA_NV { get; set; }
        [XmlElement(Order = 5)]
        public string NGAY_CT { get; set; }
        [XmlElement(Order = 6)]
        public string TK_CO { get; set; }
        [XmlElement(Order = 7)]
        public string MA_NNTHUE { get; set; }
        [XmlElement(Order = 8)]
        public string TEN_NNTHUE { get; set; }
        [XmlElement(Order = 9)]
        public string DC_NNTHUE { get; set; }
        [XmlElement(Order = 10)]
        public string MA_NNTIEN { get; set; }
        [XmlElement(Order = 11)]
        public string TEN_NNTIEN { get; set; }
        [XmlElement(Order = 12)]
        public string DC_NNTIEN { get; set; }
        [XmlElement(Order = 13)]
        public string NGAY_NNTIEN { get; set; }
        [XmlElement(Order = 14)]
        public string CQ_QD { get; set; }
        [XmlElement(Order = 15)]
        public string MA_LTHUE { get; set; }
        [XmlElement(Order = 16)]
        public string MA_CQTHU { get; set; }
        [XmlElement(Order = 17)]
        public string MA_DBHC { get; set; }
        [XmlElement(Order = 18)]
        public string MA_NH_A { get; set; }
        [XmlElement(Order = 19)]
        public string MA_NH_B { get; set; }
        [XmlElement(Order = 20)]
        public string MA_NT { get; set; }
        [XmlElement(Order = 21)]
        public string TY_GIA { get; set; }
        [XmlElement(Order = 22)]
        public string TTIEN { get; set; }
        [XmlElement(Order = 23)]
        public string TTIEN_NT { get; set; }
        [XmlElement(Order = 24)]
        public string DIEN_GIAI { get; set; }
        [XmlElement(Order = 25)]
        public string TG_KY { get; set; }
        [XmlElement(Order = 26)]
        public string TRAN_TYPE { get; set; }
        //---moi bo sung--------
        [XmlElement(Order =27 )]
        public string  KYHIEU_CT { get; set; }
        [XmlElement(Order =28 )]
        public string  QUAN_NNTHUE { get; set; } 
        [XmlElement(Order =29 )]
        public string  TINH_NNTHUE { get; set; }
        [XmlElement(Order =30 )]
        public string  QUAN_NNTIEN  { get; set; }
        [XmlElement(Order = 31)]
        public string  TINH_NNTIEN  { get; set; }
        [XmlElement(Order =32 )]
        public string TK_TRICHNO_KH  { get; set; }
        [XmlElement(Order =33 )]
        public string DC_KB  { get; set; }
        [XmlElement(Order =34 )]
        public string TEN_CQTHU { get; set; }
        [XmlElement(Order = 35)]
        public string LOAI_THU { get; set; }
        [XmlElement(Order = 36)]
        public CTU_DTL CTU_DTL;
        [XmlElement(Order = 37)]
        public CTBL CTBL;


    }
    public class CTU_DTL
    {
        public CTU_DTL()
        { 
        
        }
        [XmlElement("ROW")]
        public List<ROW_DTL> ROW;
    
    
    }
    public class CTBL
    {
        public CTBL()
        { 
        
        }
        [XmlElement("ROW")]
        public List<ROW_CTBL> ROW;


    }
    public class ROW_DTL
    {
        public ROW_DTL()
        { 
        
        }
        [XmlElement(Order = 1)]
        public string MA_CHUONG { get; set; }
        [XmlElement(Order = 2)]
        public string MA_NDKT { get; set; }
        [XmlElement(Order = 3)]
        public string NOI_DUNG { get; set; }
        [XmlElement(Order = 4)]
        public string SOTIEN { get; set; }
        [XmlElement(Order = 5)]
        public string KY_THUE { get; set; }

    }

    public class ROW_CTBL
    {
        public ROW_CTBL()
        { 
        
        }
        [XmlElement(Order = 1)]
        public string NGAY_BL { get; set; }
        [XmlElement(Order = 2)]
        public string KYHIEU_BL { get; set; }
        [XmlElement(Order = 3)]
        public string SO_BL { get; set; }
        [XmlElement(Order = 4)]
        public string MA_TINH_NNTIEN { get; set; }
        [XmlElement(Order = 5)]
        public string TEN_TINH_NNTIEN { get; set; }
        [XmlElement(Order = 6)]
        public string MA_HUYEN_NNTIEN { get; set; }
        [XmlElement(Order = 7)]
        public string TEN_HUYEN_NNTIEN { get; set; }
        [XmlElement(Order = 8)]
        public string MA_CQQD { get; set; }
        [XmlElement(Order = 9)]
        public string TEN_CQQD { get; set; }
        [XmlElement(Order = 10)]
        public string SO_QD { get; set; }
        [XmlElement(Order = 11)]
        public string NGAY_QD { get; set; }
        [XmlElement(Order = 12)]
        public string MA_LHTHU { get; set; }
        [XmlElement(Order = 13)]
        public string TEN_LHTHU { get; set; }
        [XmlElement(Order = 14)]
        public string LY_DO { get; set; }
        [XmlElement(Order = 15)]
        public string TTIEN_VPHC { get; set; }
        [XmlElement(Order = 16)]
        public string LY_DO_NOP_CHAM { get; set; }
        [XmlElement(Order = 17)]
        public string TTIEN_NOP_CHAM { get; set; }

    }
    public class HEADER
    {
        public HEADER()
        {
        }
        [XmlElement(Order = 1)]
        public string VERSION { get; set; }
        [XmlElement(Order = 2)]
        public string SENDER_CODE { get; set; }
        [XmlElement(Order = 3)]
        public string SENDER_NAME { get; set; }
        [XmlElement(Order = 4)]
        public string RECEIVER_CODE { get; set; }
        [XmlElement(Order = 5)]
        public string RECEIVER_NAME { get; set; }
        [XmlElement(Order = 6)]
        public string TRAN_CODE { get; set; }
        [XmlElement(Order = 7)]
        public string MSG_ID { get; set; }
        [XmlElement(Order = 8)]
        public string SEND_DATE { get; set; }

    }
}
