﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using VBOracleLib;

namespace GDT.MSG.MSG07000_NDUNG_DC
{
    [Serializable]
    [XmlRoot(ElementName = "NDUNG_DC")]
    public class NDUNGDC
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public NDUNGDC()
        {
        }

        [XmlElement("GIAODICH")]
        public GIAODICH GIAODICH;

        public string MSGtoXML(NDUNGDC p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(NDUNGDC));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public NDUNGDC MSGToObject(string p_XML)
        {

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(NDUNGDC));

                //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
                //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

                XmlReader reader = XmlReader.Create(new StringReader(p_XML));

                NDUNGDC LoadedObjTmp = (NDUNGDC)serializer.Deserialize(reader);
                return LoadedObjTmp;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                return null;
            }

        }
    }

    public class NDUNG_DC
    {
        public NDUNG_DC()
        { }

        [XmlElement("GIAODICH")]
        public GIAODICH GIAODICH;
    }

    public class GIAODICH
    {
        public GIAODICH()
        { }
        public string MA_GDICH { get; set; }
        public string NGAY_GUI_GDICH { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string TTHAI_GDICH { get; set; }
        public string TTHAI_CTU { get; set; }
        [XmlElement("CHUNGTU")]
        public CHUNGTU CHUNGTU;

    }

    public class CHUNGTU
    {
        public CHUNGTU()
        {
            
        }
        [XmlElement("NDUNG_CTU_NH")]
        public NDUNG_CTU_NH NDUNG_CTU_NH;
        public List<string> Signature { get; set; }

    }

    public class NDUNG_CTU_NH
    {
        public NDUNG_CTU_NH() 
        {
        }
        public string MAHIEU_CTU { get; set; }
        public string SO_CTU { get; set; }
        [XmlElement("NDUNG_CTU")]

        // tuanda - 05/02/24
        public string NGAY_CTU { get; set; }

        public NDUNG_CTU NDUNG_CTU;

    } 

    public class NDUNG_CTU
    {
        public NDUNG_CTU()
        { 
        }
        [XmlElement("CHUNGTU_HDR")]
        public CHUNGTU_HDR CHUNGTU_HDR;
        [XmlElement("CHUNGTU_CTIET")]
        public CHUNGTU_CTIET CHUNGTU_CTIET;

    }

    public class CHUNGTU_HDR
    {
        public CHUNGTU_HDR()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string ID_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string MA_CTU { get; set; }
        public string HTHUC_NOP { get; set; }
        public string MST_NNOP { get; set; }
        public string TEN_NNOP { get; set; }
        public string MA_CQT { get; set; }
        public string TEN_CQT { get; set; }
        public string DIACHI_NNOP { get; set; }
        public string MA_XA_NNOP { get; set; }
        public string TEN_XA_NNOP { get; set; }
        public string MA_HUYEN_NNOP { get; set; }
        public string TEN_HUYEN_NNOP { get; set; }
        public string MA_TINH_NNOP { get; set; }
        public string TEN_TINH_NNOP { get; set; }
        public string MST_NTHAY { get; set; }
        public string TEN_NTHAY { get; set; }
        public string DIACHI_NTHAY { get; set; }
        public string TEN_HUYEN_NTHAY { get; set; }
        public string TEN_TINH_NTHAY { get; set; }
        public string MA_NHANG_NOP { get; set; }
        public string TEN_NHANG_NOP { get; set; }
        public string STK_NHANG_NOP { get; set; }
        public string MA_HIEU_KBAC { get; set; }
        public string TEN_KBAC { get; set; }
        public string MA_TINH_KBAC { get; set; }
        public string TEN_TINH_KBAC { get; set; }
        public string LOAI_TK_THU { get; set; }
        public string TEN_TK_THU { get; set; }
        public string STK_THU { get; set; }
        public string ID_TK_KNGHI { get; set; }
        public string TK_KNGHI { get; set; }
        public string MA_CQTHU { get; set; }
        public string TEN_CQTHU { get; set; }
        public string NGAY_LAP { get; set; }
        public string TONG_TIEN { get; set; }
        public string VAN_ID { get; set; }
        public string MA_DBHC_THU { get; set; }
        public string TEN_DBHC_THU { get; set; }
        public string MA_LOAI_THUE { get; set; }
        public string MA_NGUYENTE { get; set; }
        public string MA_NHANG_UNT { get; set; }
        public string TEN_NHANG_UNT { get; set; }
        public string MA_THAMCHIEU { get; set; }

        // tuanda - 05/02/24
        public string LHINH_NNT { get; set; }
        
    }

    public class CHUNGTU_CTIET
    {
        public CHUNGTU_CTIET()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET> ROW_CTIET;

    }

    public class ROW_CTIET
    {
        public ROW_CTIET()
        { 
        
        }
        public string ID_CTU_CTIET { get; set; }
        public string ID_CTU { get; set; }
        public string SO_TK_TB_QD { get; set; }
        public string NDUNG_NOP { get; set; }
        public string MA_NDKT { get; set; }
        public string MA_CHUONG { get; set; }
        public string KY_THUE { get; set; }
        public string TIEN_PNOP { get; set; }

        // tuanda - 05/02/24
        public string GHI_CHU { get; set; }
        public string MA_DBHC_THU { get; set; }
        public string TEN_DBHC_THU { get; set; }
        public string ID_KNOP { get; set; }

    }

}
