﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;
using System.Configuration;
using System.Collections;
using System.Data;
using VBOracleLib;
using System.IO.Compression;
using System.IO;
using GDT.MSG.MSG06004;
using GDT.MSG.MSGHeader;
using System.Xml.Schema;
using System.Xml;
namespace ETAX_GDT
{
    public class Utility
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static String strConnection = ConfigurationManager.AppSettings.Get("CTC_DB_CONN").ToString();
        public static String strLogPath = ConfigurationManager.AppSettings.Get("FILE_LOGPATH").ToString();
        public static String MessageBackupPath = ConfigurationManager.AppSettings.Get("MESSAGE_FOLDER").ToString();
        //public static String strConnection = VBOracleLib.Security.DescryptStr(strConnection1);
        public static String strXMLdefout1 = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
        public static String strXMLdefout2 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
        public static String strXMLdefin2 = "<DATA xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
        public static String strXMLNDDCdefin = "<NDUNG_DC xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
        public static String strPBAN_TLIEU_XML = "4.0.2";
        #region "Khai bao bien"
        private static string strTran_Type = "";
        private static string strMSG_TEXT = "";
        private static string strMSG_ID = "";
       
        #endregion "Khai bao bien"

        #region "Utility"
        
        /// <summary>
        /// Hàm chuyển từ dữ liệu kiểu ngày về dạng số (dd/MM/yyyy thành yyyyMMdd)
        /// </summary>
        /// <param name="strDate">Ngày cần chuyển với định dạng dd/MM/yyyy</param>
        /// <returns>Kết quả dạng yyyyMMdd</returns>
        /// <remarks>
        ///</remarks>
        public static long ConvertDateToNumber(string strDate)
        {
            //-----------------------------------------------------
            // Mục đích: Chuyển một string ngày tháng 'dd/MM/yyyy' --> dạng số 'yyyyMMdd'.
            // Tham số: string đầu vào
            // Giá trị trả về: 
            // Ngày viết: 18/10/2007
            // Người viết: Lê Hồng Hà
            // ----------------------------------------------------
            string[] arr = null;

            try
            {
                string sep = "/";
                strDate = strDate.Trim();
                if (strDate.Length < 10)
                    return 0;
                arr = strDate.Split(sep.ToCharArray());
                sep = "/";
                arr[0] = arr[0].PadLeft(2, '0');
                arr[1] = arr[1].PadLeft(2, '0');
                if (arr[2].Length == 2)
                {
                    arr[2] = "20" + arr[2];
                }

            }
            catch (Exception ex)
            {
                return Convert.ToInt64(DateTime.Now.ToString("yyyyMMdd"));
            }

            return long.Parse(arr[2] + arr[1] + arr[0]);
        }
        public static DataSet ExecuteReturnDataSet(string strCommandText, CommandType cmdType)
        {
            OracleConnection oConnect = null;
            OracleDataAdapter oDA = null;
            DataSet ds = null;
            try
            {
                oConnect = new OracleConnection(strConnection);
                oDA = new OracleDataAdapter(strCommandText, oConnect);
                ds = new DataSet();
                oDA.Fill(ds);

            }
            catch (Exception ex)
            {
                //thong bao loi ra o day
            }
            finally
            {
                if ((oDA != null))
                    oDA.Dispose();
                if ((oConnect != null))
                    oConnect.Dispose();
            }
            return ds;
        }
        public static int TCS_GetSequence(string p_Seq)
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT ");
                sbSQL.Append(p_Seq);
                sbSQL.Append(".nextval FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();

                return int.Parse(strRet);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public static string convertDayofYear(string strInput)
        {
            if (strInput.Length < 13)
            { return ""; }
            int year = 0;
            int dayOfYear = 0;
            year = int.Parse(strInput.Substring(0, 4));
            dayOfYear = int.Parse(strInput.Substring(4, 3));
            string hh = strInput.Substring(7, 2);
            string mm = strInput.Substring(9, 2);
            string ss = strInput.Substring(11, 2);
            string theDate = new DateTime(year, 1, 1).AddDays(dayOfYear - 1).ToString("dd/MM/yyyy");
            theDate = theDate + " " + hh + ":" + mm + ":" + ss;
            return theDate;
        }

        public static string RomoveXML_Des(string xmlInput)
        {
            string xmlReturn = "";
            xmlInput = xmlInput.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            xmlInput = xmlInput.Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
            xmlInput = xmlInput.Replace(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\"", "");
            xmlInput = xmlInput.Replace(" xmlns=\"http://www.cpandl.com\"", "");
            xmlInput = xmlInput.Replace(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");

            xmlReturn = xmlInput;
            return xmlReturn;
        }

        public static void Update_CTU(string SO_CTU, string MAHIEU_CTU, string MA_GD_CTU, string TT_CHUYEN)
        {
            string strSQL = "";
            strSQL = "UPDATE TCS_CTU_NTDT_HDR SET MA_GDICH_CTU=:MA_GDICH_CTU,TT_CHUYEN=:TT_CHUYEN, ngay_chuyen=:ngay_chuyen ";
            strSQL += " WHERE SO_CTU=:SO_CTU AND MAHIEU_CTU=:MAHIEU_CTU";
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();

                IDbDataParameter[] p = null;
                p = new IDbDataParameter[5];
                p[0] = new OracleParameter();
                p[0].ParameterName = "MA_GDICH_CTU";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = MA_GD_CTU;

                p[1] = new OracleParameter();
                p[1].ParameterName = "TT_CHUYEN";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = TT_CHUYEN;

                p[2] = new OracleParameter();
                p[2].ParameterName = "SO_CTU";
                p[2].DbType = DbType.String;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = SO_CTU;

                p[3] = new OracleParameter();
                p[3].ParameterName = "MAHIEU_CTU";
                p[3].DbType = DbType.String;
                p[3].Direction = ParameterDirection.Input;
                p[3].Value = MAHIEU_CTU;

                p[4] = new OracleParameter();
                p[4].ParameterName = "ngay_chuyen";
                p[4].DbType = DbType.DateTime;
                p[4].Direction = ParameterDirection.Input;
                p[4].Value = DateTime.Now;

                DataAccess.ExecuteNonQuery(strSQL.ToString(), CommandType.Text, p, transCT);
                transCT.Commit();
            }
            catch (Exception ex)
            {
                transCT.Rollback();
                log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                //throw ex;
            }
            finally
            {
               
                conn.Close();
                conn.Dispose();
            }
        }

        public static string getTen_TT(string strTT_CT)
        {
            string strReturn = "";
            if (strTT_CT == GDICH_NTDT_Constants.TT_CT_KB)
            {
                strReturn = GDICH_NTDT_Constants.TEN_TT_CT_KB;
            }
            if (strTT_CT == GDICH_NTDT_Constants.TT_CT_LOI)
            {
                strReturn = GDICH_NTDT_Constants.TEN_TT_CT_LOI;
            }
            if (strTT_CT == GDICH_NTDT_Constants.TT_CT_NHAN)
            {
                strReturn = GDICH_NTDT_Constants.TEN_TT_CT_NHAN;
            }
             if (strTT_CT == GDICH_NTDT_Constants.TT_CT_THANHCONG)
            {
                strReturn = GDICH_NTDT_Constants.TEN_TT_CT_THANHCONG;
            }
            if (strTT_CT == GDICH_NTDT_Constants.TT_CT_THIEU_TIEN)
            {
                strReturn = GDICH_NTDT_Constants.TEN_TT_CT_THIEU_TIEN;
            }
            if (strTT_CT == GDICH_NTDT_Constants.TT_CT_TIMEOUT)
            {
                strReturn = GDICH_NTDT_Constants.TEN_TT_CT_TIMEOUT;
            }
            return strReturn;
        }


        #endregion

        public static string compress(string text)
        {
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(text);
                MemoryStream ms = new MemoryStream();
                using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
                {
                    zip.Write(buffer, 0, buffer.Length);
                }
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                return "";
            }
        }

        //public static string decompress(string compressedText)
        //{

        //    try
        //    {
        //        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();

        //        byte[] gzBuffer = encoding.GetBytes(compressedText);

        //        using (MemoryStream ms = new MemoryStream())
        //        { 
        //            //int msgLength = BitConverter.ToInt32(gzBuffer, 0); //gzBuffer.Length;
        //            ms.Write(gzBuffer, 0, gzBuffer.Length);

                   

        //            ms.Position = 0;
        //            byte[] buffer;
        //            using (GZipStream zip = new GZipStream(ms, CompressionMode.Decompress))
        //            {
        //                const int bufferSize = 65536;
        //                 buffer = new byte[bufferSize];
        //                int bytesRead = 0;
        //                do
        //                    {
        //                      bytesRead = zip.Read(buffer, 0, bufferSize);
        //                    }
        //                    while (bytesRead == bufferSize);

        //            }
                    
        //            return Encoding.UTF8.GetString(buffer);
        //        }
                
        //    }
        //    catch (Exception ex)
        //    {
        //        LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
        //        return "";
        //    }

        //}
        public static string decompress(string compressedText)
        {
            try
            {
                byte[] gzBuffer = Convert.FromBase64String(compressedText);

                using (MemoryStream ms = new MemoryStream())
                {
                    int msgLength = BitConverter.ToInt32(gzBuffer, 0)*4; //gzBuffer.Length;
                    ms.Write(gzBuffer, 0, gzBuffer.Length);

                    byte[] buffer = new byte[msgLength];

                    ms.Position = 0;
                    using (GZipStream zip = new GZipStream(ms, CompressionMode.Decompress))
                    {
                        zip.Read(buffer, 0, buffer.Length);
                    }

                    return Encoding.UTF8.GetString(buffer);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                return "";
            }

        }
        public static string GetNgayKhoBac(string strSHKB)
        {
            long lRet = 0;
            string sSQL = "";
            string result = "";
            List<IDataParameter> listParam = null;
            try
            {
                sSQL = " SELECT TO_NUMBER (TO_CHAR (TO_DATE (giatri_ts, 'DD/MM/YYYY'), 'YYYYMMDD')) FROM tcs_thamso " + " WHERE ten_ts = 'NGAY_LV' AND ma_dthu = (SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS = :GIATRI_TS)";
                listParam = new List<IDataParameter>();
                listParam.Add(DataAccess.NewDBParameter("GIATRI_TS", ParameterDirection.Input, System.Convert.ToString(strSHKB), DbType.String));

                DataTable dt = Business.Common.mdlCommon.DataSet2Table(DataAccess.ExecuteReturnDataSet(sSQL, CommandType.Text, listParam.ToArray()));
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (!long.TryParse(dt.Rows[0][0].ToString(), out lRet))
                    {
                        lRet = long.Parse(DateTime.Now.ToString("yyyyMMdd"));
                    }
                }
                else
                {
                    lRet = long.Parse(DateTime.Now.ToString("yyyyMMdd"));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                lRet = long.Parse(DateTime.Now.ToString("yyyyMMdd"));
            }

            int kq = int.Parse(lRet.ToString());

            return kq.ToString();
        }
        public static string insertMSGOutLog(GDT.MSG.MSG msg, ref string ma_TT)
        {
            OracleConnection conn =DataAccess.GetConnection();
            OracleCommand myCMD = new OracleCommand();
            try
            {
                StringBuilder sbSqlInsert = new StringBuilder("");
                String strS = "INSERT INTO tcs_dm_msg_out_ntdt (ID_GDICH, SENDER_CODE, SENDER_NAME, RECEIVER_CODE, RECEIVER_NAME, TRAN_CODE, MSG_ID, MSG_REFID, ID_LINK, SEND_DATE, ORIGINAL_CODE, ORIGINAL_NAME, ORIGINAL_DATE, ERROR_CODE, ERROR_DESC, SPARE1, SPARE2, SPARE3, TRANGTHAI, NGAY_GDICH, CONTENT_MESS) values (:ID_GDICH, :SENDER_CODE, :SENDER_NAME, :RECEIVER_CODE, :RECEIVER_NAME, :TRAN_CODE, :MSG_ID, :MSG_REFID, :ID_LINK, :SEND_DATE, :ORIGINAL_CODE, :ORIGINAL_NAME, :ORIGINAL_DATE, :ERROR_CODE, :ERROR_DESC, :SPARE1, :SPARE2, :SPARE3, :TRANGTHAI, :NGAY_GDICH, :CONTENT_MESS)";
                myCMD.Connection = conn;
                myCMD.CommandText = strS;
                myCMD.CommandType = CommandType.Text;


                if (msg == null)
                {
                    ma_TT = "02";
                    return "Lỗi cấu trúc XML";
                }
                else
                {
                    //Gan cac thong tin
                    int id_gd = Utility.TCS_GetSequence("tcs_dm_msg_out_ntdt_seq");
                    string SENDER_CODE = DBNull.Value.ToString();
                    string SENDER_NAME = DBNull.Value.ToString();
                    string RECEIVER_CODE = DBNull.Value.ToString();
                    string RECEIVER_NAME = DBNull.Value.ToString();
                    string TRAN_CODE = DBNull.Value.ToString();
                    string MSG_ID = DBNull.Value.ToString();
                    string MSG_REFID = DBNull.Value.ToString();
                    string ID_LINK = DBNull.Value.ToString();
                    string SEND_DATE = DBNull.Value.ToString();
                    string ORIGINAL_CODE = DBNull.Value.ToString();
                    string ORIGINAL_NAME = DBNull.Value.ToString();
                    string ORIGINAL_DATE = DBNull.Value.ToString();
                    string ERROR_CODE = DBNull.Value.ToString();
                    string ERROR_DESC = DBNull.Value.ToString();
                    string SPARE1 = DBNull.Value.ToString();
                    string SPARE2 = DBNull.Value.ToString();
                    string SPARE3 = DBNull.Value.ToString();
                    string CONTENT_MESS = DBNull.Value.ToString();
                    string TRANGTHAI = DBNull.Value.ToString();
                    string NGAY_GDICH = DBNull.Value.ToString();

                    if (msg.sender_code != null)
                    {
                        if (!msg.sender_code.Equals(string.Empty))
                        {
                            SENDER_CODE = msg.sender_code;
                        }
                    }
                    if (msg.sender_name != null)
                    {
                        if (!msg.sender_name.Equals(string.Empty))
                        {
                            SENDER_NAME = msg.sender_name;
                        }
                    }
                    if (msg.receiver_code != null)
                    {
                        if (!msg.receiver_code.Equals(string.Empty))
                        {
                            RECEIVER_CODE = msg.receiver_code;
                        }
                    }
                    if (msg.receiver_name != null)
                    {
                        if (!msg.receiver_name.Equals(string.Empty))
                        {
                            RECEIVER_NAME = msg.receiver_name;
                        }
                    }
                    if (msg.tran_code != null)
                    {
                        if (!msg.tran_code.Equals(string.Empty))
                        {
                            TRAN_CODE = msg.tran_code;
                        }
                    }
                    if (msg.msg_id != null)
                    {
                        if (!msg.msg_id.Equals(string.Empty))
                        {
                            MSG_ID = msg.msg_id;
                        }
                    }
                    if (msg.msg_refid != null)
                    {
                        if (!msg.msg_refid.Equals(string.Empty))
                        {
                            MSG_REFID = msg.msg_refid;
                        }
                    }
                    if (msg.id_link != null)
                    {
                        if (!msg.id_link.Equals(string.Empty))
                        {
                            ID_LINK = msg.id_link;
                        }
                    }
                    if (msg.send_date != null)
                    {
                        if (!msg.send_date.Equals(string.Empty))
                        {
                            SEND_DATE = msg.send_date;
                        }
                    }
                    if (msg.original_code != null)
                    {
                        if (!msg.original_code.Equals(string.Empty))
                        {
                            ORIGINAL_CODE = msg.original_code;
                        }
                    }
                    if (msg.original_name != null)
                    {
                        if (!msg.original_name.Equals(string.Empty))
                        {
                            ORIGINAL_NAME = msg.original_name;
                        }
                    }
                    if (msg.original_date != null)
                    {
                        if (!msg.original_date.Equals(string.Empty))
                        {
                            ORIGINAL_DATE = msg.original_date;
                        }
                    }
                    if (msg.error_code != null)
                    {
                        if (!msg.error_code.Equals(string.Empty))
                        {
                            ERROR_CODE = msg.error_code;
                        }
                    }
                    if (msg.error_desc != null)
                    {
                        if (!msg.error_desc.Equals(string.Empty))
                        {
                            ERROR_DESC = msg.error_desc;
                        }
                    }
                    if (msg.spare1 != null)
                    {
                        if (!msg.spare1.Equals(string.Empty))
                        {
                            SPARE1 = msg.spare1;
                        }
                    }
                    if (msg.spare2 != null)
                    {
                        if (!msg.spare2.Equals(string.Empty))
                        {
                            SPARE2 = msg.spare2;
                        }
                    }
                    if (msg.spare3 != null)
                    {
                        if (!msg.spare3.Equals(string.Empty))
                        {
                            SPARE3 = msg.spare3;
                        }
                    }
                    if (msg.content_mess != null)
                    {
                        if (!msg.content_mess.Equals(string.Empty))
                        {
                            CONTENT_MESS = msg.content_mess;
                        }
                    }
                    if (msg.trangthai != null)
                    {
                        if (!msg.trangthai.Equals(string.Empty))
                        {
                            TRANGTHAI = msg.trangthai;
                        }
                    }
                    NGAY_GDICH = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    myCMD.Parameters.Add("ID_GDICH", OracleType.Int32).Value = id_gd;
                    myCMD.Parameters.Add("SENDER_CODE", OracleType.VarChar).Value = SENDER_CODE;
                    myCMD.Parameters.Add("SENDER_NAME", OracleType.VarChar).Value = SENDER_NAME;
                    myCMD.Parameters.Add("RECEIVER_CODE", OracleType.VarChar).Value = RECEIVER_CODE;
                    myCMD.Parameters.Add("RECEIVER_NAME", OracleType.VarChar).Value = RECEIVER_NAME;
                    myCMD.Parameters.Add("TRAN_CODE", OracleType.VarChar).Value = TRAN_CODE;
                    myCMD.Parameters.Add("MSG_ID", OracleType.VarChar).Value = MSG_ID;
                    myCMD.Parameters.Add("MSG_REFID", OracleType.VarChar).Value = MSG_REFID;
                    myCMD.Parameters.Add("ID_LINK", OracleType.VarChar).Value = ID_LINK;
                    myCMD.Parameters.Add("SEND_DATE", OracleType.VarChar).Value = SEND_DATE;
                    myCMD.Parameters.Add("ORIGINAL_CODE", OracleType.VarChar).Value = ORIGINAL_CODE;
                    myCMD.Parameters.Add("ORIGINAL_NAME", OracleType.VarChar).Value = ORIGINAL_NAME;
                    myCMD.Parameters.Add("ORIGINAL_DATE", OracleType.VarChar).Value = ORIGINAL_DATE;
                    myCMD.Parameters.Add("ERROR_CODE", OracleType.VarChar).Value = ERROR_CODE;
                    myCMD.Parameters.Add("ERROR_DESC", OracleType.VarChar).Value = ERROR_DESC;
                    myCMD.Parameters.Add("SPARE1", OracleType.VarChar).Value = SPARE1;
                    myCMD.Parameters.Add("SPARE2", OracleType.VarChar).Value = SPARE2;
                    myCMD.Parameters.Add("SPARE3", OracleType.VarChar).Value = SPARE3;
                    myCMD.Parameters.Add("TRANGTHAI", OracleType.VarChar).Value = TRANGTHAI;
                    myCMD.Parameters.Add("NGAY_GDICH", OracleType.VarChar).Value = NGAY_GDICH;
                    myCMD.Parameters.Add("CONTENT_MESS", OracleType.Clob).Value = CONTENT_MESS;

                    myCMD.ExecuteNonQuery();

                    ma_TT = "01";
                    return "Gửi message thành công";

                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                ma_TT = "02";
                return "Lỗi gửi message";
            }
            finally
            {

                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    myCMD.Dispose();
                  

                    conn.Close();
                    conn.Dispose();
                }
            }

        }
        public static string insertMSGLog(GDT.MSG.MSG msg, ref string ma_TT)
        {
            OracleConnection conn = DataAccess.GetConnection();
            OracleCommand myCMD = new OracleCommand();
            try
            {
                StringBuilder sbSqlInsert = new StringBuilder("");
                String strS = "INSERT INTO tcs_dm_msg_ntdt (ID_GDICH, SENDER_CODE, SENDER_NAME, RECEIVER_CODE, RECEIVER_NAME, TRAN_CODE, MSG_ID, MSG_REFID, ID_LINK, SEND_DATE, ORIGINAL_CODE, ORIGINAL_NAME, ORIGINAL_DATE, ERROR_CODE, ERROR_DESC, SPARE1, SPARE2, SPARE3, TRANGTHAI, NGAY_GDICH, CONTENT_MESS) values (:ID_GDICH, :SENDER_CODE, :SENDER_NAME, :RECEIVER_CODE, :RECEIVER_NAME, :TRAN_CODE, :MSG_ID, :MSG_REFID, :ID_LINK, :SEND_DATE, :ORIGINAL_CODE, :ORIGINAL_NAME, :ORIGINAL_DATE, :ERROR_CODE, :ERROR_DESC, :SPARE1, :SPARE2, :SPARE3, :TRANGTHAI, :NGAY_GDICH, :CONTENT_MESS)";
                myCMD.Connection = conn;
                myCMD.CommandText = strS;
                myCMD.CommandType = CommandType.Text;


                if (msg == null)
                {
                    ma_TT = "07";
                    return "Không đúng khuôn dạng message";
                }
                else
                {
                    //Gan cac thong tin
                    int id_gd = Utility.TCS_GetSequence("tcs_dm_msg_ntdt_seq");
                    string SENDER_CODE = DBNull.Value.ToString();
                    string SENDER_NAME = DBNull.Value.ToString();
                    string RECEIVER_CODE = DBNull.Value.ToString();
                    string RECEIVER_NAME = DBNull.Value.ToString();
                    string TRAN_CODE = DBNull.Value.ToString();
                    string MSG_ID = DBNull.Value.ToString();
                    string MSG_REFID = DBNull.Value.ToString();
                    string ID_LINK = DBNull.Value.ToString();
                    string SEND_DATE = DBNull.Value.ToString();
                    string ORIGINAL_CODE = DBNull.Value.ToString();
                    string ORIGINAL_NAME = DBNull.Value.ToString();
                    string ORIGINAL_DATE = DBNull.Value.ToString();
                    string ERROR_CODE = DBNull.Value.ToString();
                    string ERROR_DESC = DBNull.Value.ToString();
                    string SPARE1 = DBNull.Value.ToString();
                    string SPARE2 = DBNull.Value.ToString();
                    string SPARE3 = DBNull.Value.ToString();
                    string CONTENT_MESS = DBNull.Value.ToString();
                    string TRANGTHAI = DBNull.Value.ToString();
                    string NGAY_GDICH = DBNull.Value.ToString();

                    if (msg.sender_code != null)
                    {
                        if (!msg.sender_code.Equals(string.Empty))
                        {
                            SENDER_CODE = msg.sender_code;
                        }
                    }
                    if (msg.sender_name != null)
                    {
                        if (!msg.sender_name.Equals(string.Empty))
                        {
                            SENDER_NAME = msg.sender_name;
                        }
                    }
                    if (msg.receiver_code != null)
                    {
                        if (!msg.receiver_code.Equals(string.Empty))
                        {
                            RECEIVER_CODE = msg.receiver_code;
                        }
                    }
                    if (msg.receiver_name != null)
                    {
                        if (!msg.receiver_name.Equals(string.Empty))
                        {
                            RECEIVER_NAME = msg.receiver_name;
                        }
                    }
                    if (msg.tran_code != null)
                    {
                        if (!msg.tran_code.Equals(string.Empty))
                        {
                            TRAN_CODE = msg.tran_code;
                        }
                    }
                    if (msg.msg_id != null)
                    {
                        if (!msg.msg_id.Equals(string.Empty))
                        {
                            MSG_ID = msg.msg_id;
                        }
                    }
                    if (msg.msg_refid != null)
                    {
                        if (!msg.msg_refid.Equals(string.Empty))
                        {
                            MSG_REFID = msg.msg_refid;
                        }
                    }
                    if (msg.id_link != null)
                    {
                        if (!msg.id_link.Equals(string.Empty))
                        {
                            ID_LINK = msg.id_link;
                        }
                    }
                    if (msg.send_date != null)
                    {
                        if (!msg.send_date.Equals(string.Empty))
                        {
                            SEND_DATE = msg.send_date;
                        }
                    }
                    if (msg.original_code != null)
                    {
                        if (!msg.original_code.Equals(string.Empty))
                        {
                            ORIGINAL_CODE = msg.original_code;
                        }
                    }
                    if (msg.original_name != null)
                    {
                        if (!msg.original_name.Equals(string.Empty))
                        {
                            ORIGINAL_NAME = msg.original_name;
                        }
                    }
                    if (msg.original_date != null)
                    {
                        if (!msg.original_date.Equals(string.Empty))
                        {
                            ORIGINAL_DATE = msg.original_date;
                        }
                    }
                    if (msg.error_code != null)
                    {
                        if (!msg.error_code.Equals(string.Empty))
                        {
                            ERROR_CODE = msg.error_code;
                        }
                    }
                    if (msg.error_desc != null)
                    {
                        if (!msg.error_desc.Equals(string.Empty))
                        {
                            ERROR_DESC = msg.error_desc;
                        }
                    }
                    if (msg.spare1 != null)
                    {
                        if (!msg.spare1.Equals(string.Empty))
                        {
                            SPARE1 = msg.spare1;
                        }
                    }
                    if (msg.spare2 != null)
                    {
                        if (!msg.spare2.Equals(string.Empty))
                        {
                            SPARE2 = msg.spare2;
                        }
                    }
                    if (msg.spare3 != null)
                    {
                        if (!msg.spare3.Equals(string.Empty))
                        {
                            SPARE3 = msg.spare3;
                        }
                    }
                    if (msg.content_mess != null)
                    {
                        if (!msg.content_mess.Equals(string.Empty))
                        {
                            CONTENT_MESS = msg.content_mess;
                        }
                    }
                    if (msg.trangthai != null)
                    {
                        if (!msg.trangthai.Equals(string.Empty))
                        {
                            TRANGTHAI = msg.trangthai;
                        }
                    }
                    if (msg.ngay_gdich != null)
                    {
                        if (!msg.ngay_gdich.Equals(string.Empty))
                        {
                            NGAY_GDICH = msg.ngay_gdich;
                        }
                    }

                    myCMD.Parameters.Add("ID_GDICH", OracleType.Int32).Value = id_gd;
                    myCMD.Parameters.Add("SENDER_CODE", OracleType.VarChar).Value = SENDER_CODE;
                    myCMD.Parameters.Add("SENDER_NAME", OracleType.VarChar).Value = SENDER_NAME;
                    myCMD.Parameters.Add("RECEIVER_CODE", OracleType.VarChar).Value = RECEIVER_CODE;
                    myCMD.Parameters.Add("RECEIVER_NAME", OracleType.VarChar).Value = RECEIVER_NAME;
                    myCMD.Parameters.Add("TRAN_CODE", OracleType.VarChar).Value = TRAN_CODE;
                    myCMD.Parameters.Add("MSG_ID", OracleType.VarChar).Value = MSG_ID;
                    myCMD.Parameters.Add("MSG_REFID", OracleType.VarChar).Value = MSG_REFID;
                    myCMD.Parameters.Add("ID_LINK", OracleType.VarChar).Value = ID_LINK;
                    myCMD.Parameters.Add("SEND_DATE", OracleType.VarChar).Value = SEND_DATE;
                    myCMD.Parameters.Add("ORIGINAL_CODE", OracleType.VarChar).Value = ORIGINAL_CODE;
                    myCMD.Parameters.Add("ORIGINAL_NAME", OracleType.VarChar).Value = ORIGINAL_NAME;
                    myCMD.Parameters.Add("ORIGINAL_DATE", OracleType.VarChar).Value = ORIGINAL_DATE;
                    myCMD.Parameters.Add("ERROR_CODE", OracleType.VarChar).Value = ERROR_CODE;
                    myCMD.Parameters.Add("ERROR_DESC", OracleType.VarChar).Value = ERROR_DESC;
                    myCMD.Parameters.Add("SPARE1", OracleType.VarChar).Value = SPARE1;
                    myCMD.Parameters.Add("SPARE2", OracleType.VarChar).Value = SPARE2;
                    myCMD.Parameters.Add("SPARE3", OracleType.VarChar).Value = SPARE3;
                    myCMD.Parameters.Add("TRANGTHAI", OracleType.VarChar).Value = TRANGTHAI;
                    myCMD.Parameters.Add("NGAY_GDICH", OracleType.VarChar).Value = NGAY_GDICH;
                    myCMD.Parameters.Add("CONTENT_MESS", OracleType.Clob).Value = CONTENT_MESS;

                    myCMD.ExecuteNonQuery();

                    ma_TT = "01";
                    return "Nhận message thành công";

                }

            }
            catch (Exception ex)
            {
                //ghi noi dung msg ra file
                log.Error(ex.Message + "-" + ex.StackTrace);//
                Utility.WriteTextToFile(msg.content_mess, MessageBackupPath + "/" + msg.tran_code + "_" + msg.msg_id + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt");

                ma_TT = "02";
                return "Lỗi nhận message";
            }
            finally
            {

                if ((conn != null) || conn.State != ConnectionState.Closed)
                {
                    myCMD.Dispose();
                  

                    conn.Close();
                    conn.Dispose();
                }
            }

        }

        public static void WriteTextToFile(string text, string filename)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(filename, true);
                sw.Write(text);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }
        

        public static void Log_Action(string Tran_Type,string MSG_TEXT)
        {
            StreamWriter fileWriter = null;
            string filePath = strLogPath + DateTime.Now.ToString("_yyyy_MM_dd") + ".txt";
            string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

            if (File.Exists(filePath))
                fileWriter = File.AppendText(filePath);
            else
                fileWriter = File.CreateText(filePath);
            try
            {
                fileWriter.Write(datetime + " : " + Tran_Type + " MSG=[" + MSG_TEXT + "]");
                fileWriter.WriteLine();
            }
            finally
            {
                fileWriter.Close();
            }
        }

        public static void Log_Action_CustomPath(string strLogContent,string strLog_File_Path)
        {
            StreamWriter fileWriter = null;

            if (File.Exists(strLog_File_Path))
                fileWriter = File.AppendText(strLog_File_Path);
            else
                fileWriter = File.CreateText(strLog_File_Path);
            try
            {
                fileWriter.Write(strLogContent);
                fileWriter.WriteLine();
            }
            finally
            {
                fileWriter.Close();
            }
        }


        public static void replyReceivedMSG(MSGHeader msg, string ma_gdich_tchieu, string strTrangThai, string mota_trangthai)
        {
            try
            {
                if (msg != null)
                {
                    string xmlOut = "";
                    MSG06004 replyMSG = new MSG06004();
                    replyMSG.HEADER = new GDT.MSG.MSG06004.HEADER();
                    replyMSG.BODY = new GDT.MSG.MSG06004.BODY();
                    replyMSG.BODY.ROW = new GDT.MSG.MSG06004.ROW();
                    replyMSG.BODY.ROW.GIAODICH = new GDT.MSG.MSG06004.GIAODICH();


                    //replyMSG.HEADER.VERSION = "1.0";
                    replyMSG.HEADER.SENDER_CODE = GDICH_NTDT_Constants.HEADER_SENDER_CODE;
                    replyMSG.HEADER.SENDER_NAME = GDICH_NTDT_Constants.HEADER_SENDER_NAME;
                    replyMSG.HEADER.RECEIVER_CODE = GDICH_NTDT_Constants.HEADER_RECEIVER_CODE;
                    replyMSG.HEADER.RECEIVER_NAME = GDICH_NTDT_Constants.HEADER_RECEIVER_NAME;
                    replyMSG.HEADER.TRAN_CODE = msg.HEADER.TRAN_CODE;
                    replyMSG.HEADER.MSG_ID = "999";
                    replyMSG.HEADER.MSG_REFID = msg.HEADER.MSG_ID;
                    replyMSG.HEADER.ID_LINK = msg.HEADER.ID_LINK;
                    replyMSG.HEADER.SEND_DATE = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                    replyMSG.HEADER.ORIGINAL_CODE = "";
                    replyMSG.HEADER.ORIGINAL_NAME = "";
                    replyMSG.HEADER.ORIGINAL_DATE = "";
                    replyMSG.HEADER.ERROR_CODE = "";
                    replyMSG.HEADER.ERROR_DESC = "";
                    replyMSG.HEADER.SPARE1 = "";
                    replyMSG.HEADER.SPARE2 = "";
                    replyMSG.HEADER.SPARE3 = "";

                    replyMSG.BODY.ROW.GIAODICH.PBAN_TLIEU_XML = GDICH_NTDT_Constants.HEADER_PHIENBAN_XML;
                    replyMSG.BODY.ROW.GIAODICH.MA_GDICH = replyMSG.HEADER.SENDER_CODE + Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ").ToString();
                    replyMSG.BODY.ROW.GIAODICH.NGAY_GUI_GDICH = DateTime.Now.ToString("dd/MM/yyyy:HH:mm:ss");
                    replyMSG.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU = ma_gdich_tchieu;
                    replyMSG.BODY.ROW.GIAODICH.MESS_CODE = strTrangThai;
                    replyMSG.BODY.ROW.GIAODICH.MESS_CONTENT = mota_trangthai;

                    xmlOut = replyMSG.MSGtoXML(replyMSG);
                    xmlOut = Utility.RomoveXML_Des(xmlOut);
                    //vinhvv sign
                    string signatureType = GetValue_THAMSOHT("SIGN.TYPE.TCT");
                    string xpathSign = "/DATA/BODY/ROW/GIAODICH";
                    string xpathNodeSign = "GIAODICH";
                    if (signatureType.Equals("1"))
                    {
                        xmlOut = Signature.DigitalSignature.getSignNTDT(xmlOut, xpathSign, xpathNodeSign);
                    }
                    else
                    {
                        xmlOut = Signature.SignatureProcess.SignXMLEextendTimeStamp(xmlOut, "GIAODICH", "GIAODICH", true);
                    }
                    //vinhvv
                    strMSG_ID = msg.HEADER.MSG_ID;
                    strTran_Type = msg.HEADER.TRAN_CODE;

                    strMSG_TEXT = "PutMQ=StartTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);

                    clsMQHelper mq = new clsMQHelper();
                    string strReturn = "";
                    strReturn = mq.PutMQMessage(xmlOut);

                    strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "MQ Return: " + strReturn;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);

                    //insert msg out log

                    GDT.MSG.MSG msg_log = new GDT.MSG.MSG();

                    msg_log.sender_code = replyMSG.HEADER.SENDER_CODE;
                    msg_log.sender_name = replyMSG.HEADER.SENDER_NAME;
                    msg_log.receiver_code = replyMSG.HEADER.RECEIVER_CODE;
                    msg_log.receiver_name = replyMSG.HEADER.RECEIVER_NAME;
                    msg_log.tran_code = replyMSG.HEADER.TRAN_CODE;
                    msg_log.msg_id = replyMSG.HEADER.MSG_ID;
                    msg_log.msg_refid = replyMSG.HEADER.MSG_REFID;
                    msg_log.id_link = replyMSG.HEADER.ID_LINK;
                    msg_log.send_date = replyMSG.HEADER.SEND_DATE;
                    msg_log.original_code = replyMSG.HEADER.ORIGINAL_CODE;
                    msg_log.original_name = replyMSG.HEADER.ORIGINAL_NAME;
                    msg_log.original_date = replyMSG.HEADER.ORIGINAL_DATE;
                    msg_log.error_code = replyMSG.HEADER.ERROR_CODE;
                    msg_log.error_desc = replyMSG.HEADER.ERROR_DESC;
                    if (!strReturn.Equals(""))
                    {
                        msg_log.error_code = "02";
                        if (strReturn.Length > 231)
                        {
                            strReturn = strReturn.Substring(0, 230);
                        }
                        msg_log.error_desc = strReturn;
                    }
                    msg_log.spare1 = replyMSG.HEADER.SPARE1;
                    msg_log.spare2 = replyMSG.HEADER.SPARE2;
                    msg_log.spare3 = replyMSG.HEADER.SPARE3;
                    msg_log.trangthai = "00";
                    msg_log.content_mess = xmlOut;

                    string ma_tt = "";
                    Utility.insertMSGOutLog(msg_log, ref ma_tt);

                }


            }
            catch (Exception ex)
            {

                log.Error(ex.Message + "-" + ex.StackTrace);//LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }

        }


        public static bool validXML(string xsdValidation, string strXML)
        {
            bool containsErrors = false;
            try
            {
                ValidationEventHandler validator = delegate(object sender, ValidationEventArgs e)
                {
                    containsErrors = true;
                };
                string path = @"C:/xsd/" + xsdValidation ;
                StreamReader stream = new StreamReader(@"C:/xsd/dky_nthue.xsd");

                XmlSchema schema = XmlSchema.Read(stream, validator);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(strXML);
                xmldoc.Schemas.Add(schema);

                xmldoc.Validate(validator);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "-" + e.StackTrace);//
            }


            return containsErrors;
        }
        public static DateTime ConvertNumberToDate(string strText)
        {

            DateTime dt = new DateTime();
            string strDate = null;
            string strMonth = null;
            string strYear = null;
            try
            {
                strText = strText.Trim();
                if (strText.Length < 8)
                {
                    return DateTime.Now;
                }
                strDate = strText.Substring(6, 2);
                strMonth = strText.Substring(4, 2);
                strYear = strText.Substring(0, 4);

                int intDate = 0;
                int intMonth = 0;
                int intYear = 0;
                intDate = Convert.ToInt32(strDate);
                intMonth = Convert.ToInt32(strMonth);
                intYear = Convert.ToInt32(strYear);

                dt = new DateTime(intYear, intMonth, intDate);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                dt = DateTime.Now;
            }
            return dt;
        }
        public static string GetValue_THAMSOHT(string strParmName)
        {
            string Result = "";
            try
            {
                string strSQL = "SELECT a.giatri_ts FROM tcs_thamso_ht a  where a.ten_ts ='" + strParmName + "' ";
                DataTable dt = null;
                dt = DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = dt.Rows[0]["GIATRI_TS"].ToString();
                }
                return Result;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return Result;
            }
        }
        //
        //
        public static string GetValue_Ten_NH_GT(string strshkb, string strMa_GT)
        {
            string Result = "";
            try
            {
                string strSQL = "SELECT ten_giantiep FROM tcs_dm_nh_giantiep_tructiep   where shkb ='" + strshkb + "' and ma_giantiep='" + strMa_GT + "' ";
                DataTable dt = null;
                dt = DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = dt.Rows[0]["ten_giantiep"].ToString();
                }
                return Result;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return Result;
            }
        }

        public static Boolean checkExist_NH_GT_TT(string strshkb, string strMa_GT)
        {
            bool Result = false;
            try
            {
                string strSQL = "SELECT * FROM tcs_dm_nh_giantiep_tructiep   where shkb ='" + strshkb + "' and ma_giantiep='" + strMa_GT + "' ";
                DataTable dt = null;
                dt = DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = true;
                }
                return Result;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return false;
            }
        }
        public static Boolean checkExist_NH_GT_TT_SHB(string strshkb, string strMa_GT)
        {
            bool Result = false;
            try
            {
                string strSQL = "SELECT * FROM tcs_dm_nh_giantiep_tructiep   where shkb ='" + strshkb + "' and ma_giantiep='" + strMa_GT + "' and TT_SHB = '1' ";
                DataTable dt = null;
                dt = DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = true;
                }
                return Result;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return false;
            }
        }

        // tuanda - 23/02/24
        public static string GetNoiXuLyChungTu(string ma_tiepnhan)
        {
            string Result = "";
            try
            {
                string strSQL = "SELECT ma_noinhan FROM tcs_dm_donvi_nhan_chungtu   where ma_nh ='" + ma_tiepnhan + "'";
                DataTable dt = null;
                dt = DataAccess.ExecuteToTable(strSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = dt.Rows[0]["ma_noinhan"].ToString();
                }
                return Result;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return Result;
            }
        }
    }
}
