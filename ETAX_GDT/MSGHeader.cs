﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace GDT.MSG.MSGHeader
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSGHeader
    {
        public MSGHeader()
        {
        }
        [XmlElement("HEADER")]
        public HEADER HEADER;
        //[XmlElement("BODY")]
        //public BODY BODY;
        //[XmlElement("SECURITY")]
        //public SECURITY SECURITY;

        public string MSGtoXML(MSGHeader p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSGHeader));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSGHeader MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSGHeader.MSGHeader));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            GDT.MSG.MSGHeader.MSGHeader LoadedObjTmp = (GDT.MSG.MSGHeader.MSGHeader)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    public class HEADER
    {
        public HEADER()
        {
        }
        public string MSGtoXML(HEADER p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(HEADER));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }
    }

    //public class BODY
    //{
    //    public BODY()
    //    {
    //    }
        
    //}

    //public class SECURITY
    //{
    //    public SECURITY() 
    //    { 
    //    }
    //}
}
