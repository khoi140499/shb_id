﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG;
using GDT.MSG.MSG07001_Lite;
using GDT.MSG.MSG07001_ND_DC;
using System.Data.OracleClient;
using System.Data;
using Business.NTDT.NNTDT;
using VBOracleLib;
using System.Configuration;
using Business;
using System.IO;
using System.Globalization;

namespace ETAX_GDT
{
    public class ProcessBaoCaoDoiChieu_NH_TTXL
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string xmlOut = "";
        MSG07001_Lite msg07001_lite;
        MSG07001_ND_DC msg07001_nd_dc;

        public ProcessBaoCaoDoiChieu_NH_TTXL()
        {
            msg07001_lite = new MSG07001_Lite();
            msg07001_nd_dc = new MSG07001_ND_DC();
        }

        public void BaoCaoDoiChieu(string ngaydoichieu, ref int tong_tc, ref int tong_tn_tct, ref int tong_tn_nh, ref int tong_lech_tct, ref int tong_lech_nh, ref string errorCode, ref string errorMSG, string msg_id)
        {
            errorCode = "00";
            DataTable dt = new DataTable();
            string sql = "select * from tcs_qly_dc_ntdt where to_char(ngay_dc,'dd/MM/yyyy') = '" + ngaydoichieu + "' order by ID_QLDC_NTDT desc";
            try
            {
                dt = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text).Tables[0];
                if (dt.Rows.Count > 0)
                {

                    #region tao xml bao cao header
                    msg07001_lite.HEADER = new HEADER();
                    msg07001_lite.BODY = new BODY();
                    msg07001_lite.BODY.ROW = new ROW();
                    msg07001_lite.BODY.ROW.GIAODICH_DC = new GIAODICH_DC();
                    //msg07001_lite.BODY.ROW.GIAODICH_DC.NDUNG_DC = new NDUNG_DC();

                    msg07001_lite.HEADER.VERSION = "1.0";
                    msg07001_lite.HEADER.SENDER_CODE = GDICH_NTDT_Constants.HEADER_SENDER_CODE;
                    msg07001_lite.HEADER.SENDER_NAME = GDICH_NTDT_Constants.HEADER_SENDER_NAME;
                    msg07001_lite.HEADER.RECEIVER_CODE = GDICH_NTDT_Constants.HEADER_RECEIVER_CODE;
                    msg07001_lite.HEADER.RECEIVER_NAME = GDICH_NTDT_Constants.HEADER_RECEIVER_NAME;
                    msg07001_lite.HEADER.TRAN_CODE = "07001";
                    msg07001_lite.HEADER.MSG_ID = msg07001_lite.HEADER.SENDER_CODE + Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ");
                    msg07001_lite.HEADER.MSG_REFID = msg_id;
                    msg07001_lite.HEADER.ID_LINK = dt.Rows[0]["MA_GDICH_DC"].ToString();
                    msg07001_lite.HEADER.SEND_DATE = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                    msg07001_lite.HEADER.ORIGINAL_CODE = "";
                    msg07001_lite.HEADER.ORIGINAL_NAME = "";
                    msg07001_lite.HEADER.ORIGINAL_DATE = "";
                    msg07001_lite.HEADER.ERROR_CODE = "";
                    msg07001_lite.HEADER.ERROR_DESC = "";
                    msg07001_lite.HEADER.SPARE1 = "";
                    msg07001_lite.HEADER.SPARE2 = "";
                    msg07001_lite.HEADER.SPARE3 = "";

                    msg07001_lite.BODY.ROW.GIAODICH_DC.PBAN_TLIEU_XML_DC = dt.Rows[0]["PBAN_TLIEU_XML_DC"].ToString();
                    msg07001_lite.BODY.ROW.GIAODICH_DC.MA_GDICH_DC = msg07001_lite.HEADER.MSG_ID;
                    msg07001_lite.BODY.ROW.GIAODICH_DC.NGAY_GDICH_DC = Utility.convertDayofYear(dt.Rows[0]["NGAY_GDICH_DC"].ToString());
                    msg07001_lite.BODY.ROW.GIAODICH_DC.MA_NHANG_DC = dt.Rows[0]["MA_NHANG_DC"].ToString();
                    msg07001_lite.BODY.ROW.GIAODICH_DC.NGAY_DC = ((DateTime)(dt.Rows[0]["NGAY_DC"])).ToString("dd/MM/yyyy");
                    msg07001_lite.BODY.ROW.GIAODICH_DC.TU_NGAY_DC = dt.Rows[0]["TU_NGAY_DC"].ToString();
                    msg07001_lite.BODY.ROW.GIAODICH_DC.DEN_NGAY_DC = dt.Rows[0]["DEN_NGAY_DC"].ToString();
                    msg07001_lite.BODY.ROW.GIAODICH_DC.MA_GDICH_TCHIEU_DC = dt.Rows[0]["MA_GDICH_DC"].ToString();
                    #endregion

                    // Tao xml bao cao;
                    //msg07001_nd_dc = new NDUNG_DC();

                    #region Tao xml bc 01
                    msg07001_nd_dc.BAOCAO01 = new BAOCAO01();
                    msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01 = new BAOCAO_HDR01();
                    msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS = new TONGTIEN_TCONGS();
                    msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS = new List<TONGTIEN_TCONG_CTIETS>();
                    msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS = new TONGTIEN_HTRUOCS();
                    msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS.TONGTIEN_HTRUOC_CTIETS = new List<TONGTIEN_HTRUOC_CTIETS>();
                    msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS = new TONGTIEN_HSAUS();
                    msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS.TONGTIEN_HSAU_CTIETS = new List<TONGTIEN_HSAU_CTIETS>();
                    msg07001_nd_dc.BAOCAO01.BAOCAO_CTIET01 = new BAOCAO_CTIET01();
                    msg07001_nd_dc.BAOCAO01.BAOCAO_CTIET01.ROW_CTIET01 = new List<ROW_CTIET01>();
                    #endregion

                    #region tao xml bc 02
                    msg07001_nd_dc.BAOCAO02 = new BAOCAO02();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02 = new BAOCAO_HDR02();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS = new TONGTIEN_LECHS();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS.TONGTIEN_LECH_CTIETS = new List<TONGTIEN_LECH_CTIETS>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS = new TONGTIEN_TCTCO_NHTMKOS();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS = new List<TONGTIEN_TCTCO_NHTMKO_CTIETS>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS = new TONGTIEN_NHTMCO_TCTKOS();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS = new List<TONGTIEN_NHTMCO_TCTKO_CTIETS>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS = new TONGTIEN_LECH_TTHAIS();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS = new List<TONGTIEN_LECH_TTHAI_CTIETS>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02 = new BAOCAO_CTIET02();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02.TCTCO_NHTMKO = new TCTCO_NHTMKO();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02.NHTMCO_TCTKO = new NHTMCO_TCTKO();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02.LECH_TTHAI = new LECH_TTHAI();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02.TCTCO_NHTMKO.ROW_CTIET02 = new List<ROW_CTIET02>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02.NHTMCO_TCTKO.ROW_CTIET02 = new List<ROW_CTIET02>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02.LECH_TTHAI.ROW_CTIET02 = new List<ROW_CTIET02>();
                    #endregion

                    #region tao xml bc 03
                    msg07001_nd_dc.BAOCAO03 = new BAOCAO03();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03 = new BAOCAO_HDR03();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS = new TONGTIEN_TNHANS();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS.TONGTIEN_TNHAN_CTIETS = new List<TONGTIEN_TNHAN_CTIETS>();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS = new TONGTIEN_TCTCO_NHTMCOS();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS.TONGTIEN_TCTCO_NHTMCO_CTIETS = new List<TONGTIEN_TCTCO_NHTMCO_CTIETS>();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS = new TONGTIEN_TCTCO_NHTMKOS();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS = new List<TONGTIEN_TCTCO_NHTMKO_CTIETS>();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS = new TONGTIEN_NHTMCO_TCTKOS();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS = new List<TONGTIEN_NHTMCO_TCTKO_CTIETS>();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS = new TONGTIEN_LECH_TTHAIS();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS = new List<TONGTIEN_LECH_TTHAI_CTIETS>();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03 = new BAOCAO_CTIET03();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.TCTCO_NHTMKO = new TCTCO_NHTMKO();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.NHTMCO_TCTKO = new NHTMCO_TCTKO();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.LECH_TTHAI = new LECH_TTHAI();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.TCTCO_NHTMCO = new TCTCO_NHTMCO();

                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.TCTCO_NHTMKO.ROW_CTIET02 = new List<ROW_CTIET02>();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.NHTMCO_TCTKO.ROW_CTIET02 = new List<ROW_CTIET02>();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.LECH_TTHAI.ROW_CTIET02 = new List<ROW_CTIET02>();
                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.TCTCO_NHTMCO.ROW_CTIET02 = new List<ROW_CTIET02>();
                    #endregion

                    #region tao xml bc 04
                    msg07001_nd_dc.BAOCAO04 = new BAOCAO04();
                    msg07001_nd_dc.BAOCAO04.BAOCAO_HDR04 = new BAOCAO_HDR04();
                    msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04 = new BAOCAO_CTIET04();
                    msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04.DIEN_KO_TCONG = new DIEN_KO_TCONG();
                    msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04.DIEN_TCONG = new DIEN_TCONG();
                    msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04.DIEN_TUCHOI = new DIEN_TUCHOI();

                    msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04.DIEN_KO_TCONG.ROW_CTIET04 = new List<ROW_CTIET04>();
                    msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04.DIEN_TCONG.ROW_CTIET04 = new List<ROW_CTIET04>();
                    msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04.DIEN_TUCHOI.ROW_CTIET04 = new List<ROW_CTIET04>();
                    #endregion

                    #region tao xml bc 05
                    msg07001_nd_dc.BAOCAO05 = new BAOCAO05();
                    msg07001_nd_dc.BAOCAO05.BAOCAO_HDR05 = new BAOCAO_HDR05();
                    msg07001_nd_dc.BAOCAO05.BAOCAO_CTIET05 = new BAOCAO_CTIET05();
                    msg07001_nd_dc.BAOCAO05.BAOCAO_CTIET05.ROW_CTIET05 = new List<ROW_CTIET05>();
                    #endregion

                    #region Nap du lieu vao bao cao

                    tong_tc = 0;
                    tong_tn_tct = 0;
                    tong_tn_nh = 0;
                    tong_lech_tct = 0;
                    tong_lech_nh = 0;

                    BaoCaoDoiChieu01(dt.Rows[0]["MA_GDICH_DC"].ToString(), dt);
                    BaoCaoDoiChieu02(dt.Rows[0]["MA_GDICH_DC"].ToString(), dt);
                    BaoCaoDoiChieu03(dt.Rows[0]["MA_GDICH_DC"].ToString(), dt, ref tong_tc, ref tong_tn_tct, ref tong_tn_nh, ref tong_lech_tct, ref tong_lech_nh);
                    BaoCaoDoiChieu04(dt.Rows[0]["MA_GDICH_DC"].ToString(), dt);
                    BaoCaoDoiChieu05(dt.Rows[0]["MA_GDICH_DC"].ToString(), dt);
                    #endregion

                    #region Gui msg di
                    string xmlND_DC = "";
                    xmlND_DC = msg07001_nd_dc.MSGtoXML(msg07001_nd_dc);
                    log.Info("1.NDUNG_DC : "+ xmlND_DC);
                    if (xmlND_DC.Length > 0)
                    {
                        xmlND_DC = xmlND_DC.Replace(Utility.strXMLdefin2, "").Trim();
                        xmlND_DC = xmlND_DC.Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
                        xmlND_DC = xmlND_DC.Replace("</DATA>", "");
                        xmlND_DC = Utility.RomoveXML_Des(xmlND_DC).Trim();
                        log.Info("2.NDUNG_DC : " + xmlND_DC);
                        xmlND_DC = Utility.compress(xmlND_DC);
                        msg07001_lite.BODY.ROW.GIAODICH_DC.NDUNG_DC = xmlND_DC;
                    }

                    xmlOut = msg07001_lite.MSGtoXML(msg07001_lite);

                    //xmlOut = xmlOut.Replace(Utility.strXMLdefout1, Utility.strXMLdefout2);
                    xmlOut = Utility.RomoveXML_Des(xmlOut);
                    //vinhvv sign
                    string signatureType = Utility.GetValue_THAMSOHT("SIGN.TYPE.TCT");
                    string xpathSign = "/DATA/BODY/ROW/GIAODICH_DC";
                    string xpathNodeSign = "GIAODICH_DC";
                    if (signatureType.Equals("1"))
                    {
                        xmlOut = Signature.DigitalSignature.getSignNTDT(xmlOut, xpathSign, xpathNodeSign);
                    }
                    else
                    {
                        xmlOut = Signature.SignatureProcess.SignXMLEextendTimeStamp(xmlOut, "GIAODICH_DC", "GIAODICH_DC", true);
                    }
                    //vinhvv
                    clsMQHelper mq = new clsMQHelper();
                    string strReturn = "";
                    strReturn = mq.PutMQMessage(xmlOut);
                    #endregion

                    #region insert log
                    GDT.MSG.MSG msg_log = new GDT.MSG.MSG();

                    msg_log.sender_code = msg07001_lite.HEADER.SENDER_CODE;
                    msg_log.sender_name = msg07001_lite.HEADER.SENDER_NAME;
                    msg_log.receiver_code = msg07001_lite.HEADER.RECEIVER_CODE;
                    msg_log.receiver_name = msg07001_lite.HEADER.RECEIVER_NAME;
                    msg_log.tran_code = msg07001_lite.HEADER.TRAN_CODE;
                    msg_log.msg_id = msg07001_lite.HEADER.MSG_ID;
                    msg_log.msg_refid = msg07001_lite.HEADER.MSG_REFID;
                    msg_log.id_link = msg07001_lite.HEADER.ID_LINK;
                    msg_log.send_date = msg07001_lite.HEADER.SEND_DATE;
                    msg_log.original_code = msg07001_lite.HEADER.ORIGINAL_CODE;
                    msg_log.original_name = msg07001_lite.HEADER.ORIGINAL_NAME;
                    msg_log.original_date = msg07001_lite.HEADER.ORIGINAL_DATE;
                    msg_log.error_code = msg07001_lite.HEADER.ERROR_CODE;
                    msg_log.error_desc = msg07001_lite.HEADER.ERROR_DESC;
                    if (!strReturn.Equals(""))
                    {
                        msg_log.error_code = "02";
                        if (strReturn.Length > 231)
                        {
                            strReturn = strReturn.Substring(0, 230);
                        }
                        msg_log.error_desc = strReturn;
                    }
                    msg_log.spare1 = msg07001_lite.HEADER.SPARE1;
                    msg_log.spare2 = msg07001_lite.HEADER.SPARE2;
                    msg_log.spare3 = msg07001_lite.HEADER.SPARE3;
                    msg_log.trangthai = "00";
                    msg_log.content_mess = xmlOut;

                    string ma_tt = "";
                    Utility.insertMSGOutLog(msg_log, ref ma_tt);

                    #endregion


                }
                else
                {
                    errorCode = "02";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                errorCode = "01";
                errorMSG = ex.Message;
                throw ex;
            }

        }

        public void BaoCaoDoiChieu01(string ma_gdich_dc, DataTable dtQLDC)
        {
            DataTable dtDC = new DataTable();
            string sqlDC = @"select dc.*, ct.ngay_nhang from tcs_dchieu_ctu_ntdt_hdr dc, tcs_ctu_ntdt_hdr ct
                            where dc.so_gnt = ct.so_gnt 
                            and dc.mst_nnop = ct.mst_nnop 
                            and dc.tong_tien = ct.tong_tien 
                            and dc.tthai_ctu = ct.tthai_ctu 
                            and SUBSTR(dc.ngay_gui_gdich,1,7)  =  SUBSTR(ct.ngay_gui_gd,1,7) 
                            and dc.ma_gdich_dc ='" + ma_gdich_dc + "'";
            try
            {
                dtDC = DataAccess.ExecuteReturnDataSet(sqlDC, CommandType.Text).Tables[0];
                
                double tongtien = 0;
                //tt84
                double tongtien_vnd = 0;
                double tongtien_usd = 0;
                string vnd = "";
                string usd = "";
                string nguyen_te = "";
                //
                int id_bc = Utility.TCS_GetSequence("NTDT_BAOCAO_HDR_SEQ");
                    
                if (dtDC.Rows.Count > 0)
                {

                    int id_bc_ct = -1;
                

                    string sqlInsertCT = "";
                    for (int i = 0; i < dtDC.Rows.Count; i++)
                    {
                        //tt84
                        nguyen_te = dtDC.Rows[i]["MA_NGUYENTE"].ToString();
                        if (nguyen_te.ToUpper().Equals("USD"))
                        {
                            tongtien_usd += double.Parse(dtDC.Rows[i]["tong_tien"].ToString());

                        }
                        else if (nguyen_te.ToUpper().Equals("VND"))
                        {
                            tongtien_vnd += double.Parse(dtDC.Rows[i]["tong_tien"].ToString());
                        
                        }
                        //
                        tongtien += double.Parse(dtDC.Rows[i]["tong_tien"].ToString());
                        //
                        id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R1_SEQ");

                        string tgian_tct_gui = dtDC.Rows[i]["tgian_tct_gui"].ToString();
                        if (tgian_tct_gui.Length.Equals(9))
                        {
                            tgian_tct_gui= DateTime.ParseExact(tgian_tct_gui, "dd-MMM-yy",null).ToString("dd/MM/yyyy HH:mm:ss");
                        }

                        string tgian_nh_nhan = dtDC.Rows[i]["tgian_nh_nhan"].ToString();
                        if (tgian_nh_nhan.Length.Equals(9))
                        {
                            tgian_nh_nhan = DateTime.ParseExact(tgian_nh_nhan, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                        }


                        sqlInsertCT = @"insert into ntdt_baocao_ctiet_r1 (ID_BCAO_CTIET, ID_BCAO, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, TGIAN_TCT, TGIAN_NHTM, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, DIEN_HSAU, MA_HIEU_CTU, TGIAN_TVAN,MA_NGUYENTE,MA_THAMCHIEU) 
                                        values(" + id_bc_ct + ", " + id_bc + ", '" + dtDC.Rows[i]["so_ctu"].ToString() + "', '" + dtDC.Rows[i]["so_gnt"].ToString() + "', '" + dtDC.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtDC.Rows[i]["mst_nnop"].ToString() +
                                            "', '" + dtDC.Rows[i]["ma_cqt"].ToString() + "', '" + dtDC.Rows[i]["ma_cqthu"].ToString() + "', TO_DATE('" + tgian_tct_gui + "', 'dd/MM/yyyy hh24:mi:ss'), TO_DATE('" + tgian_nh_nhan + "', 'dd/MM/yyyy hh24:mi:ss'), " + dtDC.Rows[i]["tong_tien"].ToString() + ", '" + dtDC.Rows[i]["tthai_ctu"].ToString() + "', '" + dtDC.Rows[i]["ngay_nop_gnt"].ToString() + "', '" + ((DateTime)(dtDC.Rows[i]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '', '" + dtDC.Rows[i]["mahieu_ctu"].ToString() + "', null,'" + dtDC.Rows[i]["MA_NGUYENTE"].ToString() + "','" + dtDC.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                        DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                        //add du lieu vao xml ct
                        ROW_CTIET01 row_ct = new ROW_CTIET01();
                        row_ct.MA_THAMCHIEU = dtDC.Rows[i]["MA_THAMCHIEU"].ToString();
                        row_ct.SO_CTU = dtDC.Rows[i]["so_ctu"].ToString();
                        row_ct.MAHIEU_CTU = dtDC.Rows[i]["mahieu_ctu"].ToString();
                        row_ct.SO_GNT = dtDC.Rows[i]["SO_GNT"].ToString();
                        row_ct.TEN_NNT = dtDC.Rows[i]["ten_nnop"].ToString();
                        row_ct.MST = dtDC.Rows[i]["mst_nnop"].ToString();
                        row_ct.MA_CQT = dtDC.Rows[i]["ma_cqt"].ToString();
                        row_ct.MA_CQTHU = dtDC.Rows[i]["MA_CQTHU"].ToString();
                        row_ct.NGAY_NOP_GNT = dtDC.Rows[i]["NGAY_NOP_GNT"].ToString();
                        
                        row_ct.NGAY_NOP_THUE = dtDC.Rows[i]["NGAY_NOP_THUE"].ToString();

                        row_ct.TGIAN_TCT = dtDC.Rows[i]["tgian_tct_gui"].ToString();
                        if (row_ct.TGIAN_TCT.Length.Equals(9))
                        {
                            row_ct.TGIAN_TCT = DateTime.ParseExact(row_ct.TGIAN_TCT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                        }

                        row_ct.TGIAN_NHTM = dtDC.Rows[i]["tgian_nh_nhan"].ToString();
                        if (row_ct.TGIAN_NHTM.Length.Equals(9))
                        {
                            row_ct.TGIAN_NHTM = DateTime.ParseExact(row_ct.TGIAN_NHTM, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                        }

                        row_ct.SO_TIEN = dtDC.Rows[i]["tong_tien"].ToString();
                        row_ct.MA_NGUYENTE = dtDC.Rows[i]["MA_NGUYENTE"].ToString();
                        row_ct.TTHAI = dtDC.Rows[i]["tthai_ctu"].ToString();
                        row_ct.DIEN_HSAU = "";

                        msg07001_nd_dc.BAOCAO01.BAOCAO_CTIET01.ROW_CTIET01.Add(row_ct);
                    }

                }

                //add du lieu vao xml hdr
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.PBAN_TLIEU_XML = GDICH_NTDT_Constants.HEADER_PHIENBAN_XML_TT84; 
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.MA_BCAO = "01DC-NHTM-NTDT";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TEN_BCAO = "Truyền nhận thành công, khớp đúng đối với NHTM";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TGIAN_DCHIEU = DateTime.Now.ToString("dd/MM/yyyy");
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.MA_NHANG_DCHIEU = GDICH_NTDT_Constants.MA_NHANG_DCHIEU;
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TEN_NHANG_DCHIEU = GDICH_NTDT_Constants.TEN_NHANG_DCHIEU;
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.SODIEN_TCONG = dtDC.Rows.Count.ToString();
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONG = "0";

                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS = new TONGTIEN_TCONGS();
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS = new List<TONGTIEN_TCONG_CTIETS>();
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS.Add(new TONGTIEN_TCONG_CTIETS());
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS[0].MA_NGUYENTE = "VND";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS[0].TONGTIEN_TCONG_CTIET = tongtien_vnd.ToString();
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS.Add(new TONGTIEN_TCONG_CTIETS());
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS[1].MA_NGUYENTE = "USD";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS[1].TONGTIEN_TCONG_CTIET = tongtien_usd.ToString();

                //msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS  = new List<TONGTIEN_TCONG_CTIETS>();
                //TONGTIEN_TCONG_CTIETS objVND = new TONGTIEN_TCONG_CTIETS();
                //objVND.MA_NGUYENTE = "VND";
                //objVND.TONGTIEN_TCONG_CTIET = tongtien_vnd.ToString();
                //msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS.Add(objVND);

                //TONGTIEN_TCONG_CTIETS objUSD = new TONGTIEN_TCONG_CTIETS();
                //objUSD.MA_NGUYENTE = "USD";
                //objUSD.TONGTIEN_TCONG_CTIET = tongtien_usd.ToString();
                //msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_TCONGS.TONGTIEN_TCONG_CTIETS.Add(objUSD);

                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.SODIEN_HTRUOC = "0";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOC = "0";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS = new TONGTIEN_HTRUOCS();
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS.TONGTIEN_HTRUOC_CTIETS = new List<TONGTIEN_HTRUOC_CTIETS>();
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS.TONGTIEN_HTRUOC_CTIETS.Add(new TONGTIEN_HTRUOC_CTIETS());
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS.TONGTIEN_HTRUOC_CTIETS[0].MA_NGUYENTE = "VND";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS.TONGTIEN_HTRUOC_CTIETS[0].TONGTIEN_HTRUOC_CTIET = "0";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS.TONGTIEN_HTRUOC_CTIETS.Add(new TONGTIEN_HTRUOC_CTIETS());
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS.TONGTIEN_HTRUOC_CTIETS[1].MA_NGUYENTE = "USD";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HTRUOCS.TONGTIEN_HTRUOC_CTIETS[1].TONGTIEN_HTRUOC_CTIET = "0";

                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.SODIEN_HSAU = "0";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAU = "0";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS = new TONGTIEN_HSAUS();
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS.TONGTIEN_HSAU_CTIETS = new List<TONGTIEN_HSAU_CTIETS>();
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS.TONGTIEN_HSAU_CTIETS.Add(new TONGTIEN_HSAU_CTIETS());
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS.TONGTIEN_HSAU_CTIETS[0].MA_NGUYENTE = "VND";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS.TONGTIEN_HSAU_CTIETS[0].TONGTIEN_HSAU_CTIET = "0";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS.TONGTIEN_HSAU_CTIETS.Add(new TONGTIEN_HSAU_CTIETS());
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS.TONGTIEN_HSAU_CTIETS[1].MA_NGUYENTE = "USD";
                msg07001_nd_dc.BAOCAO01.BAOCAO_HDR01.TONGTIEN_HSAUS.TONGTIEN_HSAU_CTIETS[1].TONGTIEN_HSAU_CTIET = "0";


                string sqlInsertBCDC_HDR = @"insert into NTDT_BAOCAO_HDR (ID_BCAO, MA_GDICH_DC, MA_GDICH_TCHIEU_DC, NGAY_GDICH_DC, NGAY_DC, TU_NGAY_DC, DEN_NGAY_DC, MA_BCAO, TEN_BCAO, SODIEN_TCONG, TONGTIEN_TCONG,tongtien_tcong_vnd,tongtien_tcong_usd, SODIEN_HTRUOC, TONGTIEN_HTRUOC, SODIEN_TTOAN_HSAU, TONGTIEN_TTOAN_HSAU) 
                                                values(" + id_bc + ", '" + dtQLDC.Rows[0]["MA_GDICH_DC"].ToString() + "', '" + dtQLDC.Rows[0]["MA_GDICH_TCHIEU_DC"].ToString() + "', " + dtQLDC.Rows[0]["NGAY_GDICH_DC"].ToString() + ", TO_DATE('" + ((DateTime)(dtQLDC.Rows[0]["NGAY_DC"])).ToString("dd/MM/yyyy") + "', 'dd/MM/yyyy'), " + dtQLDC.Rows[0]["TU_NGAY_DC"].ToString() + ", " + dtQLDC.Rows[0]["DEN_NGAY_DC"].ToString() + ", '01DC-NHTM-NTDT', 'Truyền nhận thành công, khớp đúng đối với NHTM', " + dtDC.Rows.Count.ToString() +
                                                ", " + tongtien + ","+tongtien_vnd+","+tongtien_usd+", 0, 0, 0, 0)";
                DataAccess.ExecuteNonQuery(sqlInsertBCDC_HDR, CommandType.Text);
                
            }
            catch (Exception ex)
            {
                log.Error("BaoCaoDoiChieu01:" + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace);//  
                throw ex;
            }


        }
        
        public void BaoCaoDoiChieu02(string ma_gdich_dc, DataTable dtQLDC)
        {

            int tongdien_tct = 0;
            double tongtien_tct = 0;
            double tongtien_tct_vnd = 0;
            double tongtien_tct_usd = 0;
            int tongdien_nh = 0;
            double tongtien_nh = 0;
            double tongtien_nh_vnd = 0;
            double tongtien_nh_usd = 0;
            int tongdien_lech = 0;
            double tongtien_lech = 0;
            double tongtien_lech_vnd = 0;
            double tongtien_lech_usd = 0;
            int tongdien_lech_tt = 0;
            double tongtien_lech_tt = 0;
            double tongtien_lech_tt_vnd = 0;
            double tongtien_lech_tt_usd = 0;
            string ma_nguyente = "";

            int id_bc = Utility.TCS_GetSequence("NTDT_BAOCAO_HDR_SEQ");
            int id_bc_ct = -1;

            string ngay_gui_gd = "";
            
            string tu_ngay = "";
            string den_ngay = "";

            if (dtQLDC.Rows.Count > 0)
            {
                tu_ngay = dtQLDC.Rows[0]["tu_ngay_dc"].ToString();
                den_ngay = dtQLDC.Rows[0]["den_ngay_dc"].ToString();
            }

            DataTable dtDC = new DataTable();
            string sqlDC = "select * from tcs_dchieu_ctu_ntdt_hdr where ma_gdich_dc = '" + ma_gdich_dc + @"'";
            try
            {
                dtDC = DataAccess.ExecuteReturnDataSet(sqlDC, CommandType.Text).Tables[0];
                if (dtDC.Rows.Count > 0)
                {
                    ngay_gui_gd = dtDC.Rows[0]["ngay_gui_gdich"].ToString().Substring(0, 7);
                    
                }
            }
            catch (Exception ex)
            {
                log.Error("1.BaoCaoDoiChieu02:" + ex.Message);
                log.Error(ex.Message + "-" + ex.StackTrace);// 
                //throw ex;
            }



            if (dtDC.Rows.Count > 0)
            {

                #region "Bao cao doi chieu TCT_CO"

                DataTable dtDCTCT = new DataTable();
                string sqlDCTCT = "select so_gnt from tcs_dchieu_ctu_ntdt_hdr where ma_gdich_dc = '" + ma_gdich_dc + @"'
                                minus
                                select so_gnt from tcs_ctu_ntdt_hdr where '" + tu_ngay + "' <= ngay_gui_gd and ngay_gui_gd <= '" + den_ngay + "' and tthai_ctu <> '04'";
                try
                {
                    //LogDebug.WriteLog("2.BaoCaoDoiChieu02: 0" + sqlDCTCT, System.Diagnostics.EventLogEntryType.Error);
                    dtDCTCT = DataAccess.ExecuteReturnDataSet(sqlDCTCT, CommandType.Text).Tables[0];
                    //LogDebug.WriteLog("2.BaoCaoDoiChieu02: 1" + sqlDCTCT, System.Diagnostics.EventLogEntryType.Error);
                    if (dtDCTCT.Rows.Count > 0)
                    {
                        string so_gnt = "";
                        for (int i = 0; i < dtDCTCT.Rows.Count; i++)
                        {
                            so_gnt += "'" + dtDCTCT.Rows[i]["so_gnt"].ToString() + "',";
                        }
                        if (so_gnt.Length > 0)
                        {
                            so_gnt = so_gnt.Remove(so_gnt.Length - 1, 1);
                            DataTable dtTCT = new DataTable();
                            string sqlTCT = "select * from tcs_dchieu_ctu_ntdt_hdr where so_gnt in(" + so_gnt + ")";
                            dtTCT = DataAccess.ExecuteReturnDataSet(sqlTCT, CommandType.Text).Tables[0];
                            //LogDebug.WriteLog("2.BaoCaoDoiChieu02: 2" + sqlTCT, System.Diagnostics.EventLogEntryType.Error);
                            tongdien_tct = dtTCT.Rows.Count;
                            if (dtTCT.Rows.Count > 0)
                            {
                                string sqlInsertCT = "";
                                for (int i = 0; i < dtTCT.Rows.Count; i++)
                                {
                                    //tt84
                                   // LogDebug.WriteLog("2.BaoCaoDoiChieu02: 3" + dtTCT.Rows[i]["tong_tien"].ToString(), System.Diagnostics.EventLogEntryType.Error);
                                    ma_nguyente = dtTCT.Rows[i]["MA_NGUYENTE"].ToString();
                                    if (ma_nguyente.ToUpper().Equals("USD"))
                                    {
                                        tongtien_tct_usd += double.Parse(dtTCT.Rows[i]["tong_tien"].ToString());
                                    }
                                    else if (ma_nguyente.ToUpper().Equals("VND"))
                                    {
                                        tongtien_tct_vnd += double.Parse(dtTCT.Rows[i]["tong_tien"].ToString());
                                    }
                                    tongtien_tct += double.Parse(dtTCT.Rows[i]["tong_tien"].ToString());
                                    //
                                    id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R2_SEQ");
                                    //LogDebug.WriteLog("2.BaoCaoDoiChieu02: 4" + id_bc_ct, System.Diagnostics.EventLogEntryType.Error);
                                    sqlInsertCT = @"insert into ntdt_baocao_ctiet_r2 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, TGIAN_TCT, TGIAN_NHTM, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, TTHAI_TCT, TTHAI_NHTM, MA_HIEU_CTU, TTHAI_TVAN, TGIAN_TVAN,MA_NGUYENTE,MA_THAMCHIEU) 
                                            values(" + id_bc_ct + ", " + id_bc + ", '1', '" + dtTCT.Rows[i]["so_ctu"].ToString() + "', '" + dtTCT.Rows[i]["so_gnt"].ToString() + "', '" + dtTCT.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtTCT.Rows[i]["mst_nnop"].ToString() +
                                                        "', '" + dtTCT.Rows[i]["ma_cqt"].ToString() + "', '" + dtTCT.Rows[i]["ma_cqthu"].ToString() + "', '" + dtTCT.Rows[i]["tgian_tct_gui"].ToString() + "', '" + dtTCT.Rows[i]["tgian_nh_nhan"].ToString() + "', " + dtTCT.Rows[i]["tong_tien"].ToString() + ", '" + dtTCT.Rows[i]["tthai_ctu"].ToString() + "', '" + dtTCT.Rows[i]["ngay_nop_gnt"].ToString() +
                                                        "', '" + dtTCT.Rows[i]["ngay_nop_thue"].ToString() + "', '" + dtTCT.Rows[i]["tthai_ctu"].ToString() + "', '', '" + dtTCT.Rows[i]["mahieu_ctu"].ToString() + "', '', null,'" + dtTCT.Rows[i]["MA_NGUYENTE"].ToString() + "','" + dtTCT.Rows[i]["MA_THAMCHIEU"].ToString() + "')";
                                    //writeLog(sqlInsertCT);
                                    //LogDebug.WriteLog("2.BaoCaoDoiChieu02: 5" + sqlInsertCT, System.Diagnostics.EventLogEntryType.Error);
                                    DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);
                                   // LogDebug.WriteLog("2.BaoCaoDoiChieu02: 6", System.Diagnostics.EventLogEntryType.Error);
                                    //add du lieu xml ct tct_co
                                    ROW_CTIET02 row_ct = new ROW_CTIET02();
                                    row_ct.MA_THAMCHIEU = dtTCT.Rows[i]["MA_THAMCHIEU"].ToString(); 
                                    row_ct.SO_CTU = dtTCT.Rows[i]["so_ctu"].ToString();
                                    row_ct.MAHIEU_CTU = dtTCT.Rows[i]["MAHIEU_CTU"].ToString();
                                    row_ct.SO_GNT = dtTCT.Rows[i]["SO_GNT"].ToString();
                                    row_ct.TEN_NNT = dtTCT.Rows[i]["ten_nnop"].ToString();
                                    row_ct.MST = dtTCT.Rows[i]["mst_nnop"].ToString();
                                    row_ct.MA_CQT = dtTCT.Rows[i]["MA_CQT"].ToString();
                                    row_ct.MA_CQTHU = dtTCT.Rows[i]["MA_CQTHU"].ToString();
                                    row_ct.NGAY_NOP_GNT = dtTCT.Rows[i]["NGAY_NOP_GNT"].ToString();

                                    row_ct.NGAY_NOP_THUE = dtTCT.Rows[i]["NGAY_NOP_THUE"].ToString();

                                    row_ct.TGIAN_TCT = dtTCT.Rows[i]["tgian_tct_gui"].ToString();
                                    if (row_ct.TGIAN_TCT.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_TCT = DateTime.ParseExact(row_ct.TGIAN_TCT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.TGIAN_NHTM = dtTCT.Rows[i]["tgian_nh_nhan"].ToString();
                                    if (row_ct.TGIAN_NHTM.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_NHTM = DateTime.ParseExact(row_ct.TGIAN_NHTM, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.SO_TIEN = dtTCT.Rows[i]["tong_tien"].ToString();
                                    row_ct.MA_NGUYENTE = dtTCT.Rows[i]["MA_NGUYENTE"].ToString();
                                    row_ct.TTHAI_TCT = dtTCT.Rows[i]["tthai_ctu"].ToString();
                                    row_ct.TTHAI_NHTM = "";
                                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02.TCTCO_NHTMKO.ROW_CTIET02.Add(row_ct);

                                }
                            }

                        }

                    }
                }
                catch (Exception ex)
                {
                   log.Error("2.BaoCaoDoiChieu02 - Bao cao doi chieu TCT_CO:" + ex.Message);
                    log.Error(ex.Message + "-" + ex.StackTrace);// 
                    throw ex;
                }

                #endregion

                #region "Bao cao doi chieu NH_CO"

                DataTable dtDCNH = new DataTable();
                string sqlDCNH = "select so_gnt from tcs_ctu_ntdt_hdr where '" + tu_ngay + "' <= ngay_gui_gd and ngay_gui_gd <= '" + den_ngay + @"' and tthai_ctu <> '04' 
                                minus 
                                select so_gnt from tcs_dchieu_ctu_ntdt_hdr where ma_gdich_dc = '" + ma_gdich_dc + "'";

                log.Info("Bao cao 02 doi chieu NHTMCO_TCTKO - "+ sqlDCNH);
                try
                {
                    dtDCNH = DataAccess.ExecuteReturnDataSet(sqlDCNH, CommandType.Text).Tables[0];
                    if (dtDCNH.Rows.Count > 0)
                    {
                        string so_gnt = "";
                        for (int i = 0; i < dtDCNH.Rows.Count; i++)
                        {
                            so_gnt += "'" + dtDCNH.Rows[i]["so_gnt"].ToString() + "',";
                        }
                        if (so_gnt.Length > 0)
                        {
                            so_gnt = so_gnt.Remove(so_gnt.Length - 1, 1);
                            DataTable dtNH = new DataTable();
                            string sqlNH = "select * from tcs_ctu_ntdt_hdr where so_gnt in(" + so_gnt + ")";
                            dtNH = DataAccess.ExecuteReturnDataSet(sqlNH, CommandType.Text).Tables[0];
                            tongdien_nh = dtNH.Rows.Count;
                            if (dtNH.Rows.Count > 0)
                            {
                                string sqlInsertCT = "";
                                for (int i = 0; i < dtNH.Rows.Count; i++)
                                {
                                    //tt84
                                    ma_nguyente = dtNH.Rows[i]["MA_NGUYENTE"].ToString();
                                    if (ma_nguyente.ToUpper().Equals("USD"))
                                    {
                                        tongtien_nh_usd += double.Parse(dtNH.Rows[i]["tong_tien"].ToString());
                                    }
                                    else if (ma_nguyente.ToUpper().Equals("VND"))
                                    {
                                        tongtien_nh_vnd += double.Parse(dtNH.Rows[i]["tong_tien"].ToString());
                                    }
                                    tongtien_nh += double.Parse(dtNH.Rows[i]["tong_tien"].ToString());
                                    id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R2_SEQ");
                                    sqlInsertCT = @"insert into ntdt_baocao_ctiet_r2 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, TGIAN_TCT, TGIAN_NHTM, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, TTHAI_TCT, TTHAI_NHTM, MA_HIEU_CTU, TTHAI_TVAN, TGIAN_TVAN,MA_NGUYENTE,MA_THAMCHIEU) 
                                            values(" + id_bc_ct + ", " + id_bc + ", '2', '" + dtNH.Rows[i]["so_ctu"].ToString() + "', '" + dtNH.Rows[i]["so_gnt"].ToString() + "', '" + dtNH.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtNH.Rows[i]["mst_nnop"].ToString() +
                                                "', '" + dtNH.Rows[i]["ma_cqt"].ToString() + "', '" + dtNH.Rows[i]["ma_cqthu"].ToString() + "', '" + dtNH.Rows[i]["tgian_tct_gui"].ToString() + "', '" + dtNH.Rows[i]["tgian_nh_nhan"].ToString() + "', " + dtNH.Rows[i]["tong_tien"].ToString() + ", '" + dtNH.Rows[i]["tthai_ctu"].ToString() + "', '' , '" + ((DateTime)(dtNH.Rows[i]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '', '" + dtNH.Rows[i]["tthai_ctu"] + "', '" + dtNH.Rows[i]["mahieu_ctu"].ToString() + "', '', null,'" + dtNH.Rows[i]["MA_NGUYENTE"].ToString() + "','" + dtNH.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                                    DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                                    //add du lieu xml ct nh_co
                                    ROW_CTIET02 row_ct = new ROW_CTIET02();
                                    row_ct.MA_THAMCHIEU = dtNH.Rows[i]["MA_THAMCHIEU"].ToString();
                                    row_ct.SO_CTU = dtNH.Rows[i]["so_ctu"].ToString();
                                    row_ct.MAHIEU_CTU = dtNH.Rows[i]["MAHIEU_CTU"].ToString();
                                    row_ct.SO_GNT = dtNH.Rows[i]["SO_GNT"].ToString();
                                    row_ct.TEN_NNT = dtNH.Rows[i]["ten_nnop"].ToString();
                                    row_ct.MST = dtNH.Rows[i]["mst_nnop"].ToString();
                                    row_ct.MA_CQT = dtNH.Rows[i]["MA_CQT"].ToString();
                                    row_ct.MA_CQTHU = dtNH.Rows[i]["MA_CQTHU"].ToString();

                                    row_ct.NGAY_NOP_GNT = dtNH.Rows[i]["TGIAN_TCT_GUI"].ToString();
                                    if (row_ct.NGAY_NOP_GNT.Length.Equals(9))
                                    {
                                        row_ct.NGAY_NOP_GNT = DateTime.ParseExact(row_ct.NGAY_NOP_GNT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    if(dtNH.Rows[i]["NGAY_HT"].ToString().Length > 0)
                                    {
                                        row_ct.NGAY_NOP_THUE = DateTime.Parse(dtNH.Rows[i]["NGAY_HT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss");
                                    }
                                    else
                                    {
                                        row_ct.NGAY_NOP_THUE = "";
                                    }

                                    row_ct.TGIAN_TCT = dtNH.Rows[i]["tgian_tct_gui"].ToString();
                                    if (row_ct.TGIAN_TCT.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_TCT = DateTime.ParseExact(row_ct.TGIAN_TCT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.TGIAN_NHTM = dtNH.Rows[i]["tgian_nh_nhan"].ToString();
                                    if (row_ct.TGIAN_NHTM.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_NHTM = DateTime.ParseExact(row_ct.TGIAN_NHTM, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.SO_TIEN = dtNH.Rows[i]["tong_tien"].ToString();
                                    row_ct.MA_NGUYENTE = dtNH.Rows[i]["MA_NGUYENTE"].ToString();
                                    row_ct.TTHAI_TCT = "";
                                    row_ct.TTHAI_NHTM = dtNH.Rows[i]["tthai_ctu"].ToString();
                                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02.NHTMCO_TCTKO.ROW_CTIET02.Add(row_ct);
                                }
                            }
                            
                        }

                    }
                }
                catch (Exception ex)
                {
                    log.Error("3.BaoCaoDoiChieu02 - Bao cao doi chieu NH_CO:" + ex.Message);
                    log.Error(ex.Message + "-" + ex.StackTrace);//  
                    throw ex;
                }


                #endregion

                #region "Bao cao doi chieu Lech"
                DataTable dtDCLech = new DataTable();
                string sqlDCLech = "select * from tcs_ctu_ntdt_hdr where '" + tu_ngay + "' <= ngay_gui_gd and ngay_gui_gd <= '" + den_ngay + "' and tthai_ctu <> '04'";
                try
                {
                    dtDCLech = DataAccess.ExecuteReturnDataSet(sqlDCLech, CommandType.Text).Tables[0];
                    if (dtDCLech.Rows.Count > 0)
                    {
                        string sqlInsertCT = "";

                        for (int i = 0; i < dtDC.Rows.Count; i++)
                        {
                            for (int j = 0; j < dtDCLech.Rows.Count; j++)
                            {
                                if (dtDC.Rows[i]["so_gnt"].ToString() == dtDCLech.Rows[j]["so_gnt"].ToString() && dtDC.Rows[i]["tthai_ctu"].ToString() != dtDCLech.Rows[j]["tthai_ctu"].ToString())
                                {
                                    tongdien_lech_tt += 1;

                                    //tt84
                                    ma_nguyente = dtDC.Rows[i]["MA_NGUYENTE"].ToString();
                                    if (ma_nguyente.ToUpper().Equals("USD"))
                                    {
                                        tongtien_lech_tt_usd += double.Parse(dtDC.Rows[i]["tong_tien"].ToString());
                                    }
                                    else if (ma_nguyente.ToUpper().Equals("VND"))
                                    {
                                        tongtien_lech_tt_vnd += double.Parse(dtDC.Rows[i]["tong_tien"].ToString());
                                    }

                                    tongtien_lech_tt += double.Parse(dtDC.Rows[i]["tong_tien"].ToString());
                                    id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R2_SEQ");
                                    sqlInsertCT = @"insert into ntdt_baocao_ctiet_r2 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, TGIAN_TCT, TGIAN_NHTM, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, TTHAI_TCT, TTHAI_NHTM, MA_HIEU_CTU, TTHAI_TVAN, TGIAN_TVAN,MA_NGUYENTE,MA_THAMCHIEU) 
                                                                            values(" + id_bc_ct + ", " + id_bc + ", '3', '" + dtDC.Rows[i]["so_ctu"].ToString() + "', '" + dtDC.Rows[i]["so_gnt"].ToString() + "', '" + dtDC.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtDC.Rows[i]["mst_nnop"].ToString() +
                                                        "', '" + dtDC.Rows[i]["ma_cqt"].ToString() + "', '" + dtDC.Rows[i]["ma_cqthu"].ToString() + "', '" + dtDC.Rows[i]["tgian_tct_gui"].ToString() + "', '" + dtDC.Rows[i]["tgian_nh_nhan"].ToString() + "', " + dtDC.Rows[i]["tong_tien"].ToString() + ", '" + dtDC.Rows[i]["tthai_ctu"].ToString() + "', '" + dtDC.Rows[i]["ngay_nop_gnt"].ToString() + "' , '" + ((DateTime)(dtDCLech.Rows[j]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '" + dtDC.Rows[i]["tthai_ctu"] + "', '" + dtDCLech.Rows[j]["tthai_ctu"] + "', '" + dtDC.Rows[i]["mahieu_ctu"].ToString() + "', '', null,'" + dtDC.Rows[i]["MA_NGUYENTE"].ToString() + "','" + dtDC.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                                    DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                                    //add du lieu xml ct lech
                                    ROW_CTIET02 row_ct = new ROW_CTIET02();
                                    row_ct.MA_THAMCHIEU = dtDC.Rows[i]["MA_THAMCHIEU"].ToString();
                                    row_ct.SO_CTU = dtDC.Rows[i]["so_ctu"].ToString();
                                    row_ct.MAHIEU_CTU = dtDC.Rows[i]["MAHIEU_CTU"].ToString();
                                    row_ct.SO_GNT = dtDC.Rows[i]["SO_GNT"].ToString();
                                    row_ct.TEN_NNT = dtDC.Rows[i]["ten_nnop"].ToString();
                                    row_ct.MST = dtDC.Rows[i]["mst_nnop"].ToString();
                                    row_ct.MA_CQT = dtDC.Rows[i]["MA_CQT"].ToString();
                                    row_ct.MA_CQTHU = dtDC.Rows[i]["MA_CQTHU"].ToString();
                                    row_ct.NGAY_NOP_GNT = dtDC.Rows[i]["NGAY_NOP_GNT"].ToString();

                                    row_ct.NGAY_NOP_THUE = dtDC.Rows[i]["NGAY_NOP_THUE"].ToString();

                                    row_ct.TGIAN_TCT = dtDC.Rows[i]["tgian_tct_gui"].ToString();
                                    if (row_ct.TGIAN_TCT.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_TCT = DateTime.ParseExact(row_ct.TGIAN_TCT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.TGIAN_NHTM = dtDC.Rows[i]["tgian_nh_nhan"].ToString();
                                    if (row_ct.TGIAN_NHTM.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_NHTM = DateTime.ParseExact(row_ct.TGIAN_NHTM, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.SO_TIEN = dtDC.Rows[i]["tong_tien"].ToString();
                                    row_ct.MA_NGUYENTE = dtDC.Rows[i]["MA_NGUYENTE"].ToString();
                                    row_ct.TTHAI_TCT = dtDC.Rows[i]["tthai_ctu"].ToString();
                                    row_ct.TTHAI_NHTM = dtDCLech.Rows[j]["tthai_ctu"].ToString();
                                    msg07001_nd_dc.BAOCAO02.BAOCAO_CTIET02.LECH_TTHAI.ROW_CTIET02.Add(row_ct);
                                }
                            }
                        }

                    }
                    
                }
                catch (Exception e)
                {
                    log.Error("4.BaoCaoDoiChieu02 - Bao cao doi chieu Lech:" + e.Message);
                    log.Error(e.Message + "-" + e.StackTrace);//  
                    throw e;
                }


                #endregion

            }

            #region "doi chieu header"

            tongdien_lech = tongdien_tct + tongdien_nh + tongdien_lech_tt;
           
            tongtien_lech = tongtien_tct + tongtien_nh + tongtien_lech_tt;
            tongtien_lech_vnd = tongtien_tct_vnd + tongtien_nh_vnd + tongtien_lech_tt_vnd;
            tongtien_lech_usd = tongtien_tct_usd + tongtien_nh_usd + tongtien_lech_tt_usd;

            if (dtQLDC.Rows.Count > 0)
            {
                try
                {

                    string sqlInsertBCDC_HDR = @"insert into NTDT_BAOCAO_HDR (ID_BCAO, MA_GDICH_DC, MA_GDICH_TCHIEU_DC, NGAY_GDICH_DC, NGAY_DC, TU_NGAY_DC, DEN_NGAY_DC, MA_BCAO, TEN_BCAO, TONGDIEN_LECH, TONGTIEN_LECH,TONGTIEN_LECH_VND,TONGTIEN_LECH_USD, TONGDIEN_TCTCO_NHTMKO,TONGTIEN_TCTCO_NHTMKO_VND,TONGTIEN_TCTCO_NHTMKO_USD, TONGTIEN_TCTCO_NHTMKO, TONGDIEN_NHTMCO_TCTKO, TONGTIEN_NHTMCO_TCTKO,TONGTIEN_NHTMCO_TCTKO_VND,TONGTIEN_NHTMCO_TCTKO_USD, TONGDIEN_LECH_TTHAI, TONGTIEN_LECH_TTHAI,TONGTIEN_LECH_TTHAI_VND,TONGTIEN_LECH_TTHAI_USD) 
                                                            values(" + id_bc + ", '" + dtQLDC.Rows[0]["MA_GDICH_DC"].ToString() + "', '" + dtQLDC.Rows[0]["MA_GDICH_TCHIEU_DC"].ToString() + "', " + dtQLDC.Rows[0]["NGAY_GDICH_DC"].ToString() + ", TO_DATE('" + ((DateTime)(dtQLDC.Rows[0]["NGAY_DC"])).ToString("dd/MM/yyyy") + "', 'dd/MM/yyyy'), " + dtQLDC.Rows[0]["TU_NGAY_DC"].ToString() + ", " + dtQLDC.Rows[0]["DEN_NGAY_DC"].ToString() + ", '02DC-NHTM-NTDT', 'Truyền nhận chênh lệch đối với NHTM', "
                                                                     + tongdien_lech + ", " + tongtien_lech + ","+ tongtien_lech_vnd +","+tongtien_lech_usd+", " + tongdien_tct + ", " + tongtien_tct + ","+tongtien_tct_vnd+","+tongtien_tct_usd+", " + tongdien_nh + ", " + tongtien_nh + ","+tongtien_nh_vnd+","+tongtien_nh_usd+", " + tongdien_lech_tt + ", " + tongtien_lech_tt + ","+tongtien_lech_tt_vnd+","+tongtien_lech_tt_usd+")";
                    DataAccess.ExecuteNonQuery(sqlInsertBCDC_HDR, CommandType.Text);

                    //add du lieu vao xml hdr
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.PBAN_TLIEU_XML = GDICH_NTDT_Constants.HEADER_PHIENBAN_XML_TT84;
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.MA_BCAO = "02DC-NHTM-NTDT";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TEN_BCAO = "Truyền nhận chênh lệch đối với NHTM";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TGIAN_DCHIEU = DateTime.Now.ToString("dd/MM/yyyy");
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.MA_NHANG_DCHIEU = GDICH_NTDT_Constants.MA_NHANG_DCHIEU;
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TEN_NHANG_DCHIEU = GDICH_NTDT_Constants.TEN_NHANG_DCHIEU;
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGDIEN_LECH = tongdien_lech.ToString();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH = "0";

                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS = new TONGTIEN_LECHS();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS.TONGTIEN_LECH_CTIETS = new List<TONGTIEN_LECH_CTIETS>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS.TONGTIEN_LECH_CTIETS.Add(new TONGTIEN_LECH_CTIETS());
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS.TONGTIEN_LECH_CTIETS[0].MA_NGUYENTE = "VND";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS.TONGTIEN_LECH_CTIETS[0].TONGTIEN_LECH_CTIET = tongtien_lech_vnd.ToString();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS.TONGTIEN_LECH_CTIETS.Add(new TONGTIEN_LECH_CTIETS());
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS.TONGTIEN_LECH_CTIETS[1].MA_NGUYENTE = "USD";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECHS.TONGTIEN_LECH_CTIETS[1].TONGTIEN_LECH_CTIET = tongtien_lech_usd.ToString();


                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGDIEN_TCTCO_NHTMKO = tongdien_tct.ToString();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKO = "0";

                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS = new TONGTIEN_TCTCO_NHTMKOS();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS = new List<TONGTIEN_TCTCO_NHTMKO_CTIETS>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS.Add(new TONGTIEN_TCTCO_NHTMKO_CTIETS());
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS[0].MA_NGUYENTE = "VND";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS[0].TONGTIEN_TCTCO_NHTMKO_CTIET = tongtien_tct_vnd.ToString();

                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS.Add(new TONGTIEN_TCTCO_NHTMKO_CTIETS());
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS[1].MA_NGUYENTE = "USD";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS[1].TONGTIEN_TCTCO_NHTMKO_CTIET = tongtien_tct_usd.ToString();

                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGDIEN_NHTMCO_TCTKO = tongdien_nh.ToString();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKO = "0";

                  
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS = new TONGTIEN_NHTMCO_TCTKOS();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS = new List<TONGTIEN_NHTMCO_TCTKO_CTIETS>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS.Add(new TONGTIEN_NHTMCO_TCTKO_CTIETS());
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS[0].MA_NGUYENTE = "VND";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS[0].TONGTIEN_NHTMCO_TCTKO_CTIET = tongtien_nh_vnd.ToString();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS.Add(new TONGTIEN_NHTMCO_TCTKO_CTIETS());
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS[1].MA_NGUYENTE = "USD";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS[1].TONGTIEN_NHTMCO_TCTKO_CTIET = tongtien_nh_usd.ToString();


                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGDIEN_LECH_TTHAI = tongdien_lech_tt.ToString();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAI = "0";

                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS = new TONGTIEN_LECH_TTHAIS();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS = new List<TONGTIEN_LECH_TTHAI_CTIETS>();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS.Add(new TONGTIEN_LECH_TTHAI_CTIETS());
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS[0].MA_NGUYENTE = "VND";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS[0].TONGTIEN_LECH_TTHAI_CTIET = tongtien_lech_tt_vnd.ToString();
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS.Add(new TONGTIEN_LECH_TTHAI_CTIETS());
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS[1].MA_NGUYENTE = "USD";
                    msg07001_nd_dc.BAOCAO02.BAOCAO_HDR02.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS[1].TONGTIEN_LECH_TTHAI_CTIET = tongtien_lech_tt_usd.ToString();

                    
                }
                catch (Exception e)
                {
                    log.Error("5.BaoCaoDoiChieu02 - oi chieu header:" + e.Message);
                    log.Error(e.Message + "-" + e.StackTrace);// 
                    
                    throw e;
                }
            }


            #endregion

        }

        public void BaoCaoDoiChieu03(string ma_gdich_dc, DataTable dtQLDC, ref int tong_tc, ref int tong_tn_tct, ref int tong_tn_nh, ref int tong_lech_tct, ref int tong_lech_nh)
        {

            int tongdien_tct = 0;
            double tongtien_tct = 0;
            double tongtien_tct_vnd = 0;
            double tongtien_tct_usd = 0;
            int tongdien_nh = 0;
            double tongtien_nh = 0;
            double tongtien_nh_vnd = 0;
            double tongtien_nh_usd = 0;
            int tongdien_lech = 0;
            double tongtien_lech = 0;
            double tongtien_lech_vnd = 0;
            double tongtien_lech_usd = 0;
            int tongdien_khop = 0;
            double tongtien_khop = 0;
            double tongtien_khop_vnd = 0;
            double tongtien_khop_usd = 0;
            int tongdien_tn = 0;
            double tongtien_tn = 0;
            double tongtien_tn_vnd = 0;
            double tongtien_tn_usd = 0;

            string ma_nguyente = "";
            int id_bc = Utility.TCS_GetSequence("NTDT_BAOCAO_HDR_SEQ");
            int id_bc_ct = -1;

            string ngay_gui_gd = "";

            string tu_ngay = "";
            string den_ngay = "";

            if (dtQLDC.Rows.Count > 0)
            {
                tu_ngay = dtQLDC.Rows[0]["tu_ngay_dc"].ToString();
                den_ngay = dtQLDC.Rows[0]["den_ngay_dc"].ToString();
            }


            DataTable dtDC = new DataTable();
            string sqlDC = "select * from tcs_dchieu_ctu_ntdt_hdr where ma_gdich_dc = '" + ma_gdich_dc + @"'";
            try
            {
                dtDC = DataAccess.ExecuteReturnDataSet(sqlDC, CommandType.Text).Tables[0];
                if (dtDC.Rows.Count > 0)
                {
                    ngay_gui_gd = dtDC.Rows[0]["ngay_gui_gdich"].ToString().Substring(0, 7);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                //throw ex;
            }

            if (dtDC.Rows.Count > 0)
            {

                #region "Bao cao doi chieu TCT_CO"

                DataTable dtDCTCT = new DataTable();
                string sqlDCTCT = "select so_gnt from tcs_dchieu_ctu_ntdt_hdr where ma_gdich_dc = '" + ma_gdich_dc + @"'
                                minus 
                                select so_gnt from tcs_ctu_ntdt_hdr where '" + tu_ngay + "' <= ngay_gui_gd and ngay_gui_gd <= '" + den_ngay + "' and tthai_ctu <> '04'";
                try
                {
                    dtDCTCT = DataAccess.ExecuteReturnDataSet(sqlDCTCT, CommandType.Text).Tables[0];
                    if (dtDCTCT.Rows.Count > 0)
                    {
                        string so_gnt = "";
                        for (int i = 0; i < dtDCTCT.Rows.Count; i++)
                        {
                            so_gnt += "'" + dtDCTCT.Rows[i]["so_gnt"].ToString() + "',";
                        }
                        if (so_gnt.Length > 0)
                        {
                            so_gnt = so_gnt.Remove(so_gnt.Length - 1, 1);
                            DataTable dtTCT = new DataTable();
                            string sqlTCT = "select * from tcs_dchieu_ctu_ntdt_hdr where so_gnt in(" + so_gnt + ")";
                            dtTCT = DataAccess.ExecuteReturnDataSet(sqlTCT, CommandType.Text).Tables[0];
                            tongdien_tct = dtTCT.Rows.Count;
                            if (dtTCT.Rows.Count > 0)
                            {
                                string sqlInsertCT = "";
                                for (int i = 0; i < dtTCT.Rows.Count; i++)
                                {
                                    //tt84
                                    ma_nguyente = dtTCT.Rows[i]["MA_NGUYENTE"].ToString();
                                    if (ma_nguyente.ToUpper().Equals("USD"))
                                    {
                                        tongtien_tct_usd += double.Parse(dtTCT.Rows[i]["tong_tien"].ToString());
                                    }
                                    else if (ma_nguyente.ToUpper().Equals("VND"))
                                    {
                                        tongtien_tct_vnd += double.Parse(dtTCT.Rows[i]["tong_tien"].ToString());
                                    }
                                    tongtien_tct += double.Parse(dtTCT.Rows[i]["tong_tien"].ToString());
                                    id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R3_SEQ");
                                    sqlInsertCT = @"insert into ntdt_baocao_ctiet_r3 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, TGIAN_TCT, TGIAN_NHTM, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, TTHAI_TCT, TTHAI_NHTM, MA_HIEU_CTU, TTHAI_TVAN, TGIAN_TVAN,MA_NGUYENTE,MA_THAMCHIEU) 
                                            values(" + id_bc_ct + ", " + id_bc + ", '1', '" + dtTCT.Rows[i]["so_ctu"].ToString() + "', '" + dtTCT.Rows[i]["so_gnt"].ToString() + "', '" + dtTCT.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtTCT.Rows[i]["mst_nnop"].ToString() +
                                                        "', '" + dtTCT.Rows[i]["ma_cqt"].ToString() + "', '" + dtTCT.Rows[i]["ma_cqthu"].ToString() + "', '" + dtTCT.Rows[i]["tgian_tct_gui"].ToString() + "', '" + dtTCT.Rows[i]["tgian_nh_nhan"].ToString() + "', " + dtTCT.Rows[i]["tong_tien"].ToString() + ", '" + dtTCT.Rows[i]["tthai_ctu"].ToString() + "', '" + dtTCT.Rows[i]["ngay_nop_gnt"].ToString() +
                                                        "', '" + dtTCT.Rows[i]["ngay_nop_thue"].ToString() + "', '" + dtTCT.Rows[i]["tthai_ctu"].ToString() + "', '', '" + dtTCT.Rows[i]["mahieu_ctu"].ToString() + "', '', null,'" + dtTCT.Rows[i]["MA_NGUYENTE"].ToString() + "','" + dtTCT.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                                    DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                                    //add du lieu xml ct tct_co
                                    ROW_CTIET02 row_ct = new ROW_CTIET02();
                                    row_ct.MA_THAMCHIEU = dtTCT.Rows[i]["MA_THAMCHIEU"].ToString();
                                    row_ct.SO_CTU = dtTCT.Rows[i]["so_ctu"].ToString();
                                    row_ct.MAHIEU_CTU = dtTCT.Rows[i]["MAHIEU_CTU"].ToString();
                                    row_ct.SO_GNT = dtTCT.Rows[i]["SO_GNT"].ToString();
                                    row_ct.TEN_NNT = dtTCT.Rows[i]["ten_nnop"].ToString();
                                    row_ct.MST = dtTCT.Rows[i]["mst_nnop"].ToString();
                                    row_ct.MA_CQT = dtTCT.Rows[i]["MA_CQT"].ToString();
                                    row_ct.MA_CQTHU = dtTCT.Rows[i]["MA_CQTHU"].ToString();
                                    row_ct.NGAY_NOP_GNT = dtTCT.Rows[i]["NGAY_NOP_GNT"].ToString();

                                    row_ct.NGAY_NOP_THUE = dtTCT.Rows[i]["NGAY_NOP_THUE"].ToString();

                                    row_ct.TGIAN_TCT = dtTCT.Rows[i]["tgian_tct_gui"].ToString();
                                    if (row_ct.TGIAN_TCT.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_TCT = DateTime.ParseExact(row_ct.TGIAN_TCT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.TGIAN_NHTM = dtTCT.Rows[i]["tgian_nh_nhan"].ToString();
                                    if (row_ct.TGIAN_NHTM.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_NHTM = DateTime.ParseExact(row_ct.TGIAN_NHTM, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.SO_TIEN = dtTCT.Rows[i]["tong_tien"].ToString();
                                    row_ct.MA_NGUYENTE = dtTCT.Rows[i]["MA_NGUYENTE"].ToString();
                                    row_ct.TTHAI_TCT = dtTCT.Rows[i]["tthai_ctu"].ToString();
                                    row_ct.TTHAI_NHTM = "";
                                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.TCTCO_NHTMKO.ROW_CTIET02.Add(row_ct);

                                }
                            }

                        }

                    }
                }
                catch (Exception e)
                {
                    log.Error("1.BaoCaoDoiChieu03 - Bao cao doi chieu TCT_CO:" + e.Message);
                    log.Error(e.Message + "-" + e.StackTrace);//
                    throw e;
                }

                #endregion

                #region "Bao cao doi chieu NH_CO"

                DataTable dtDCNH = new DataTable();
                string sqlDCNH = "select so_gnt from tcs_ctu_ntdt_hdr where '" + tu_ngay + "' <= ngay_gui_gd and ngay_gui_gd <= '" + den_ngay + @"' and tthai_ctu <> '04' 
                                minus 
                                select so_gnt from tcs_dchieu_ctu_ntdt_hdr where ma_gdich_dc = '" + ma_gdich_dc + "'";

                log.Info("Bao cao 03 doi chieu NHTMCO_TCTKO - " + sqlDCNH);
                try
                {
                    dtDCNH = DataAccess.ExecuteReturnDataSet(sqlDCNH, CommandType.Text).Tables[0];
                    if (dtDCNH.Rows.Count > 0)
                    {
                        string so_gnt = "";
                        for (int i = 0; i < dtDCNH.Rows.Count; i++)
                        {
                            so_gnt += "'"  + dtDCNH.Rows[i]["so_gnt"].ToString() + "',";
                        }
                        if (so_gnt.Length > 0)
                        {
                            so_gnt = so_gnt.Remove(so_gnt.Length - 1, 1);
                            DataTable dtNH = new DataTable();
                            string sqlNH = "select * from tcs_ctu_ntdt_hdr where so_gnt in(" + so_gnt + ")";
                            dtNH = DataAccess.ExecuteReturnDataSet(sqlNH, CommandType.Text).Tables[0];
                            tongdien_nh = dtNH.Rows.Count;
                            if (dtNH.Rows.Count > 0)
                            {
                                string sqlInsertCT = "";
                                for (int i = 0; i < dtNH.Rows.Count; i++)
                                {
                                    //tt84
                                    ma_nguyente = dtNH.Rows[i]["MA_NGUYENTE"].ToString();
                                    if (ma_nguyente.ToUpper().Equals("USD"))
                                    {
                                        tongtien_nh_usd += double.Parse(dtNH.Rows[i]["tong_tien"].ToString());
                                    }
                                    else if (ma_nguyente.ToUpper().Equals("VND"))
                                    {
                                        tongtien_nh_vnd += double.Parse(dtNH.Rows[i]["tong_tien"].ToString());
                                    }
                                    tongtien_nh += double.Parse(dtNH.Rows[i]["tong_tien"].ToString());
                                    id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R3_SEQ");
                                    sqlInsertCT = @"insert into ntdt_baocao_ctiet_r3 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, TGIAN_TCT, TGIAN_NHTM, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, TTHAI_TCT, TTHAI_NHTM, MA_HIEU_CTU, TTHAI_TVAN, TGIAN_TVAN,MA_NGUYENTE,MA_THAMCHIEU) 
                                            values(" + id_bc_ct + ", " + id_bc + ", '2', '" + dtNH.Rows[i]["so_ctu"].ToString() + "', '" + dtNH.Rows[i]["so_gnt"].ToString() + "', '" + dtNH.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtNH.Rows[i]["mst_nnop"].ToString() +
                                                "', '" + dtNH.Rows[i]["ma_cqt"].ToString() + "', '" + dtNH.Rows[i]["ma_cqthu"].ToString() + "', '" + dtNH.Rows[i]["tgian_tct_gui"].ToString() + "', '" + dtNH.Rows[i]["tgian_nh_nhan"].ToString() + "', " + dtNH.Rows[i]["tong_tien"].ToString() + ", '" + dtNH.Rows[i]["tthai_ctu"].ToString() + "', '' , '" + ((DateTime)(dtNH.Rows[i]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '', '" + dtNH.Rows[i]["tthai_ctu"] + "', '" + dtNH.Rows[i]["mahieu_ctu"].ToString() + "', '', null,'" + dtNH.Rows[i]["MA_NGUYENTE"].ToString() + "','" + dtNH.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                                    DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                                    //add du lieu xml ct nh_co
                                    ROW_CTIET02 row_ct = new ROW_CTIET02();
                                    row_ct.MA_THAMCHIEU = dtNH.Rows[i]["MA_THAMCHIEU"].ToString();
                                    row_ct.SO_CTU = dtNH.Rows[i]["so_ctu"].ToString();
                                    row_ct.MAHIEU_CTU = dtNH.Rows[i]["MAHIEU_CTU"].ToString();
                                    row_ct.SO_GNT = dtNH.Rows[i]["SO_GNT"].ToString();
                                    row_ct.TEN_NNT = dtNH.Rows[i]["ten_nnop"].ToString();
                                    row_ct.MST = dtNH.Rows[i]["mst_nnop"].ToString();
                                    row_ct.MA_CQT = dtNH.Rows[i]["MA_CQT"].ToString();
                                    row_ct.MA_CQTHU = dtNH.Rows[i]["MA_CQTHU"].ToString();

                                    row_ct.NGAY_NOP_GNT = dtNH.Rows[i]["tgian_tct_gui"].ToString();
                                    if (row_ct.NGAY_NOP_GNT.Length.Equals(9))
                                    {
                                        row_ct.NGAY_NOP_GNT = DateTime.ParseExact(row_ct.NGAY_NOP_GNT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    if(dtNH.Rows[i]["NGAY_HT"].ToString().Length > 0)
                                    {
                                        row_ct.NGAY_NOP_THUE = DateTime.Parse(dtNH.Rows[i]["NGAY_HT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss");
                                    }
                                    else
                                    {
                                        row_ct.NGAY_NOP_THUE = "";
                                    }

                                    row_ct.TGIAN_TCT = dtNH.Rows[i]["tgian_tct_gui"].ToString();
                                    if (row_ct.TGIAN_TCT.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_TCT = DateTime.ParseExact(row_ct.TGIAN_TCT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.TGIAN_NHTM = dtNH.Rows[i]["tgian_nh_nhan"].ToString();
                                    if (row_ct.TGIAN_NHTM.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_NHTM = DateTime.ParseExact(row_ct.TGIAN_NHTM, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.SO_TIEN = dtNH.Rows[i]["tong_tien"].ToString();
                                    row_ct.MA_NGUYENTE = dtNH.Rows[i]["MA_NGUYENTE"].ToString();
                                    row_ct.TTHAI_TCT = "";
                                    row_ct.TTHAI_NHTM = dtNH.Rows[i]["tthai_ctu"].ToString();
                                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.NHTMCO_TCTKO.ROW_CTIET02.Add(row_ct);
                                }
                            }

                        }

                    }
                }
                catch (Exception e)
                {
                    log.Error("2.BaoCaoDoiChieu03 - Bao cao doi chieu NH_CO:" + e.Message);
                    log.Error(e.Message + "-" + e.StackTrace);//
                    throw e;
                }


                #endregion

                #region "Bao cao doi chieu Lech"
                DataTable dtDCLech = new DataTable();
                string sqlDCLech = "select * from tcs_ctu_ntdt_hdr where '" + tu_ngay + "' <= ngay_gui_gd and ngay_gui_gd <= '" + den_ngay + "' and tthai_ctu <> '04' ";
                try
                {
                    dtDCLech = DataAccess.ExecuteReturnDataSet(sqlDCLech, CommandType.Text).Tables[0];
                    if (dtDCLech.Rows.Count > 0)
                    {
                        string sqlInsertCT = "";

                        for (int i = 0; i < dtDC.Rows.Count; i++)
                        {
                            for (int j = 0; j < dtDCLech.Rows.Count; j++)
                            {
                                if (dtDC.Rows[i]["so_gnt"].ToString() == dtDCLech.Rows[j]["so_gnt"].ToString() && dtDC.Rows[i]["tthai_ctu"].ToString() != dtDCLech.Rows[j]["tthai_ctu"].ToString())
                                {
                                    tongdien_lech += 1;

                                    //tt84
                                    ma_nguyente = dtDC.Rows[i]["MA_NGUYENTE"].ToString();
                                    if (ma_nguyente.ToUpper().Equals("USD"))
                                    {
                                        tongtien_lech_usd += double.Parse(dtDC.Rows[i]["tong_tien"].ToString());
                                    }
                                    else if (ma_nguyente.ToUpper().Equals("VND"))
                                    {
                                        tongtien_lech_vnd += double.Parse(dtDC.Rows[i]["tong_tien"].ToString());
                                    }

                                    tongtien_lech += double.Parse(dtDC.Rows[i]["tong_tien"].ToString());
                                    id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R3_SEQ");
                                    sqlInsertCT = @"insert into ntdt_baocao_ctiet_r3 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, TGIAN_TCT, TGIAN_NHTM, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, TTHAI_TCT, TTHAI_NHTM, MA_HIEU_CTU, TTHAI_TVAN, TGIAN_TVAN,MA_NGUYENTE,MA_THAMCHIEU) 
                                                                            values(" + id_bc_ct + ", " + id_bc + ", '3', '" + dtDC.Rows[i]["so_ctu"].ToString() + "', '" + dtDC.Rows[i]["so_gnt"].ToString() + "', '" + dtDC.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtDC.Rows[i]["mst_nnop"].ToString() +
                                                        "', '" + dtDC.Rows[i]["ma_cqt"].ToString() + "', '" + dtDC.Rows[i]["ma_cqthu"].ToString() + "', '" + dtDC.Rows[i]["tgian_tct_gui"].ToString() + "', '" + dtDC.Rows[i]["tgian_nh_nhan"].ToString() + "', " + dtDC.Rows[i]["tong_tien"].ToString() + ", '" + dtDC.Rows[i]["tthai_ctu"].ToString() + "', '" + dtDC.Rows[i]["ngay_nop_gnt"].ToString() + "' , '" + ((DateTime)(dtDCLech.Rows[j]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '" + dtDC.Rows[i]["tthai_ctu"] + "', '" + dtDCLech.Rows[j]["tthai_ctu"] + "', '" + dtDC.Rows[i]["mahieu_ctu"].ToString() + "', '', null,'" + dtDC.Rows[i]["MA_NGUYENTE"].ToString() + "', '" + dtDC.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                                    DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                                    //add du lieu xml ct lech
                                    ROW_CTIET02 row_ct = new ROW_CTIET02();
                                    row_ct.MA_THAMCHIEU = dtDC.Rows[i]["MA_THAMCHIEU"].ToString();
                                    row_ct.SO_CTU = dtDC.Rows[i]["so_ctu"].ToString();
                                    row_ct.MAHIEU_CTU = dtDC.Rows[i]["MAHIEU_CTU"].ToString();
                                    row_ct.SO_GNT = dtDC.Rows[i]["SO_GNT"].ToString();
                                    row_ct.TEN_NNT = dtDC.Rows[i]["ten_nnop"].ToString();
                                    row_ct.MST = dtDC.Rows[i]["mst_nnop"].ToString();
                                    row_ct.MA_CQT = dtDC.Rows[i]["MA_CQT"].ToString();
                                    row_ct.MA_CQTHU = dtDC.Rows[i]["MA_CQTHU"].ToString();
                                    row_ct.NGAY_NOP_GNT = dtDC.Rows[i]["NGAY_NOP_GNT"].ToString();

                                    row_ct.NGAY_NOP_THUE = dtDC.Rows[i]["NGAY_NOP_THUE"].ToString();

                                    row_ct.TGIAN_TCT = dtDC.Rows[i]["tgian_tct_gui"].ToString();
                                    if (row_ct.TGIAN_TCT.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_TCT = DateTime.ParseExact(row_ct.TGIAN_TCT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.TGIAN_NHTM = dtDC.Rows[i]["tgian_nh_nhan"].ToString();
                                    if (row_ct.TGIAN_NHTM.Length.Equals(9))
                                    {
                                        row_ct.TGIAN_NHTM = DateTime.ParseExact(row_ct.TGIAN_NHTM, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                                    }

                                    row_ct.SO_TIEN = dtDC.Rows[i]["tong_tien"].ToString();
                                    row_ct.MA_NGUYENTE = dtDC.Rows[i]["MA_NGUYENTE"].ToString();
                                    row_ct.TTHAI_TCT = dtDC.Rows[i]["tthai_ctu"].ToString();
                                    row_ct.TTHAI_NHTM = dtDCLech.Rows[j]["tthai_ctu"].ToString();
                                    msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.LECH_TTHAI.ROW_CTIET02.Add(row_ct);
                                }
                            }
                        }

                    }
                }
                catch (Exception e)
                {
                    log.Error("3.BaoCaoDoiChieu03 - Bao cao doi chieu Lech:" + e.Message);
                    log.Error(e.Message + "-" + e.StackTrace);//
                    throw e;
                }


                #endregion

                #region "Bao cao doi chieu Khop"


                DataTable dtDCCT = new DataTable();
                string sqlDCCT = @"select dc.*, ct.ngay_nhang from tcs_dchieu_ctu_ntdt_hdr dc, tcs_ctu_ntdt_hdr ct 
                            where dc.so_gnt = ct.so_gnt 
                            and dc.mst_nnop = ct.mst_nnop 
                            and dc.tong_tien = ct.tong_tien 
                            and dc.tthai_ctu = ct.tthai_ctu 
                            and SUBSTR(dc.ngay_gui_gdich, 1, 7) = SUBSTR(ct.ngay_gui_gd, 1, 7)
                            and dc.ma_gdich_dc ='" + ma_gdich_dc + "'";
                try
                {

                    dtDCCT = DataAccess.ExecuteReturnDataSet(sqlDCCT, CommandType.Text).Tables[0];

                    tongdien_khop = dtDCCT.Rows.Count;
                    if (dtDCCT.Rows.Count > 0)
                    {

                        string sqlInsertCT = "";
                        for (int i = 0; i < dtDCCT.Rows.Count; i++)
                        {

                            //tt84
                            ma_nguyente = dtDCCT.Rows[i]["MA_NGUYENTE"].ToString();
                            if (ma_nguyente.ToUpper().Equals("USD"))
                            {
                                tongtien_khop_usd += double.Parse(dtDCCT.Rows[i]["tong_tien"].ToString());
                            }
                            else if (ma_nguyente.ToUpper().Equals("VND"))
                            {
                                tongtien_khop_vnd += double.Parse(dtDCCT.Rows[i]["tong_tien"].ToString());
                            }


                            tongtien_khop += double.Parse(dtDCCT.Rows[i]["tong_tien"].ToString());

                            id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R3_SEQ");
                            sqlInsertCT = @"insert into ntdt_baocao_ctiet_r3 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, TGIAN_TCT, TGIAN_NHTM, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, TTHAI_TCT, TTHAI_NHTM, MA_HIEU_CTU, TTHAI_TVAN, TGIAN_TVAN,MA_NGUYENTE,MA_THAMCHIEU) 
                                            values(" + id_bc_ct + ", " + id_bc + ", '4', '" + dtDCCT.Rows[i]["so_ctu"].ToString() + "', '" + dtDCCT.Rows[i]["so_gnt"].ToString() + "', '" + dtDCCT.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtDCCT.Rows[i]["mst_nnop"].ToString().Replace("'", " ") +
                                                "', '" + dtDCCT.Rows[i]["ma_cqt"].ToString() + "', '" + dtDCCT.Rows[i]["ma_cqthu"].ToString() + "', '" + dtDCCT.Rows[i]["tgian_tct_gui"].ToString() + "', '" + dtDCCT.Rows[i]["tgian_nh_nhan"].ToString() + "', " + dtDCCT.Rows[i]["tong_tien"].ToString() + ", '" + dtDCCT.Rows[i]["tthai_ctu"].ToString() + "', '" + dtDCCT.Rows[i]["ngay_nop_gnt"].ToString() + "' , '" + ((DateTime)(dtDCCT.Rows[i]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '" + dtDCCT.Rows[i]["tthai_ctu"] + "', '" + dtDCCT.Rows[i]["tthai_ctu"] + "', '" + dtDCCT.Rows[i]["mahieu_ctu"].ToString() + "', '', null,'" + dtDCCT.Rows[i]["MA_NGUYENTE"].ToString() + "', '" + dtDCCT.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                            DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                            //add du lieu xml ct khop
                            ROW_CTIET02 row_ct = new ROW_CTIET02();
                            row_ct.MA_THAMCHIEU = dtDCCT.Rows[i]["MA_THAMCHIEU"].ToString();
                            row_ct.SO_CTU = dtDCCT.Rows[i]["so_ctu"].ToString();
                            row_ct.MAHIEU_CTU = dtDCCT.Rows[i]["MAHIEU_CTU"].ToString();
                            row_ct.SO_GNT = dtDCCT.Rows[i]["SO_GNT"].ToString();
                            row_ct.TEN_NNT = dtDCCT.Rows[i]["ten_nnop"].ToString();
                            row_ct.MST = dtDCCT.Rows[i]["mst_nnop"].ToString();
                            row_ct.MA_CQT = dtDCCT.Rows[i]["MA_CQT"].ToString();
                            row_ct.MA_CQTHU = dtDCCT.Rows[i]["MA_CQTHU"].ToString();
                            row_ct.NGAY_NOP_GNT = dtDCCT.Rows[i]["NGAY_NOP_GNT"].ToString();

                            row_ct.NGAY_NOP_THUE = dtDCCT.Rows[i]["NGAY_NOP_THUE"].ToString();

                            row_ct.TGIAN_TCT = dtDCCT.Rows[i]["tgian_tct_gui"].ToString();
                            if (row_ct.TGIAN_TCT.Length.Equals(9))
                            {
                                row_ct.TGIAN_TCT = DateTime.ParseExact(row_ct.TGIAN_TCT, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                            }

                            row_ct.TGIAN_NHTM = dtDCCT.Rows[i]["tgian_nh_nhan"].ToString();
                            if (row_ct.TGIAN_NHTM.Length.Equals(9))
                            {
                                row_ct.TGIAN_NHTM = DateTime.ParseExact(row_ct.TGIAN_NHTM, "dd-MMM-yy", null).ToString("dd/MM/yyyy HH:mm:ss");
                            }

                            row_ct.SO_TIEN = dtDCCT.Rows[i]["tong_tien"].ToString();
                            row_ct.MA_NGUYENTE = dtDCCT.Rows[i]["MA_NGUYENTE"].ToString();
                            row_ct.TTHAI_TCT = dtDCCT.Rows[i]["tthai_ctu"].ToString();
                            row_ct.TTHAI_NHTM = dtDCCT.Rows[i]["tthai_ctu"].ToString();
                            msg07001_nd_dc.BAOCAO03.BAOCAO_CTIET03.TCTCO_NHTMCO.ROW_CTIET02.Add(row_ct);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("4.BaoCaoDoiChieu03 - Bao cao doi chieu Khop:" + ex.Message);
                    log.Error(ex.Message + "-" + ex.StackTrace);//
                    throw ex;
                }
                #endregion

                tong_tc = tongdien_khop;
                tong_tn_tct = tongdien_tct + tongdien_khop + tongdien_lech;
                tong_tn_nh = tongdien_nh + tongdien_khop + tongdien_lech;
                tong_lech_tct = tongdien_tct + tongdien_lech;
                tong_lech_nh = tongdien_nh + tongdien_lech;
            }

            #region "Bao cao doi chieu Header"
            try
            {

                tongdien_tn = tongdien_khop + tongdien_lech + tongdien_nh + tongdien_tct;
                tongtien_tn = tongtien_khop + tongtien_lech + tongtien_nh + tongtien_tct;
                tongtien_tn_vnd = tongtien_khop_vnd + tongtien_lech_vnd + tongtien_nh_vnd + tongtien_tct_vnd;
                tongtien_tn_usd = tongtien_khop_usd + tongtien_lech_usd + tongtien_nh_usd + tongtien_tct_usd;


                string sqlInsertBCDC_HDR = @"insert into NTDT_BAOCAO_HDR (ID_BCAO, MA_GDICH_DC, MA_GDICH_TCHIEU_DC, NGAY_GDICH_DC, NGAY_DC, TU_NGAY_DC, DEN_NGAY_DC, MA_BCAO, TEN_BCAO, TONGDIEN_TNHAN, TONGTIEN_TNHAN,TONGTIEN_TNHAN_VND,TONGTIEN_TNHAN_USD,  TONGDIEN_TCTCO_NHTMKO, TONGTIEN_TCTCO_NHTMKO,TONGTIEN_TCTCO_NHTMKO_VND,TONGTIEN_TCTCO_NHTMKO_USD, TONGDIEN_NHTMCO_TCTKO, TONGTIEN_NHTMCO_TCTKO,TONGTIEN_NHTMCO_TCTKO_VND,TONGTIEN_NHTMCO_TCTKO_USD, tongdien_lech, tongtien_lech,tongtien_lech_vnd,tongtien_lech_usd, TONGDIEN_TCTCO_NHTMCO, TONGTIEN_TCTCO_NHTMCO,TONGTIEN_TCTCO_NHTMCO_VND,TONGTIEN_TCTCO_NHTMCO_USD) 
                                                values(" + id_bc + ", '" + dtQLDC.Rows[0]["MA_GDICH_DC"].ToString() + "', '" + dtQLDC.Rows[0]["MA_GDICH_TCHIEU_DC"].ToString() + "', " + dtQLDC.Rows[0]["NGAY_GDICH_DC"].ToString() + ", TO_DATE('" + ((DateTime)(dtQLDC.Rows[0]["NGAY_DC"])).ToString("dd/MM/yyyy") + "', 'dd/MM/yyyy'), " + dtQLDC.Rows[0]["TU_NGAY_DC"].ToString() + ", " + dtQLDC.Rows[0]["DEN_NGAY_DC"].ToString() + ", '03DC-NHTM-NTDT', 'Truyền nhận tổng hợp đối với NHTM', " + tongdien_tn + ", " + tongtien_tn + ","+tongtien_tn_vnd+","+tongtien_tn_usd+", " + tongdien_tct +
                                         ", " + tongtien_tct + "," + tongtien_tct_vnd + "," + tongtien_tct_usd + ", " + tongdien_nh + ", " + tongtien_nh + "," + tongtien_nh_vnd + "," + tongtien_nh_usd + ", " + tongdien_lech + ", " + tongtien_lech + "," + tongtien_lech_vnd + "," + tongtien_lech_usd + ", " + tongdien_khop + ", " + tongtien_khop + "," + tongtien_khop_vnd + "," + tongtien_khop_usd+ ")";

                DataAccess.ExecuteNonQuery(sqlInsertBCDC_HDR, CommandType.Text);
                //add du lieu vao xml hdr
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.PBAN_TLIEU_XML = GDICH_NTDT_Constants.HEADER_PHIENBAN_XML_TT84;
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.MA_BCAO = "03DC-NHTM-NTDT";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TEN_BCAO = "Truyền nhận tổng hợp đối với NHTM";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TGIAN_DCHIEU = DateTime.Now.ToString("dd/MM/yyyy");
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.MA_NHANG_DCHIEU = GDICH_NTDT_Constants.MA_NHANG_DCHIEU;
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TEN_NHANG_DCHIEU = GDICH_NTDT_Constants.TEN_NHANG_DCHIEU;
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGDIEN_TCTCO_NHTMKO = tongdien_tct.ToString();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKO = "0";

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS = new TONGTIEN_TCTCO_NHTMKOS();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS = new List<TONGTIEN_TCTCO_NHTMKO_CTIETS>();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS.Add(new TONGTIEN_TCTCO_NHTMKO_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS[0].MA_NGUYENTE = "VND";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS[0].TONGTIEN_TCTCO_NHTMKO_CTIET = tongtien_tct_vnd.ToString();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS.Add(new TONGTIEN_TCTCO_NHTMKO_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS[1].MA_NGUYENTE = "USD";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMKOS.TONGTIEN_TCTCO_NHTMKO_CTIETS[1].TONGTIEN_TCTCO_NHTMKO_CTIET = tongtien_tct_usd.ToString();

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGDIEN_NHTMCO_TCTKO = tongdien_nh.ToString();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKO = "0";

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS = new TONGTIEN_NHTMCO_TCTKOS();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS = new List<TONGTIEN_NHTMCO_TCTKO_CTIETS>();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS.Add(new TONGTIEN_NHTMCO_TCTKO_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS[0].MA_NGUYENTE = "VND";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS[0].TONGTIEN_NHTMCO_TCTKO_CTIET = tongtien_nh_vnd.ToString();

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS.Add(new TONGTIEN_NHTMCO_TCTKO_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS[1].MA_NGUYENTE = "USD";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_NHTMCO_TCTKOS.TONGTIEN_NHTMCO_TCTKO_CTIETS[1].TONGTIEN_NHTMCO_TCTKO_CTIET = tongtien_nh_usd.ToString();

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGDIEN_LECH_TTHAI = tongdien_lech.ToString();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAI = "0";

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS = new TONGTIEN_LECH_TTHAIS();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS = new List<TONGTIEN_LECH_TTHAI_CTIETS>();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS.Add(new TONGTIEN_LECH_TTHAI_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS[0].MA_NGUYENTE = "VND";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS[0].TONGTIEN_LECH_TTHAI_CTIET = tongtien_lech_vnd.ToString();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS.Add(new TONGTIEN_LECH_TTHAI_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS[1].MA_NGUYENTE = "USD";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_LECH_TTHAIS.TONGTIEN_LECH_TTHAI_CTIETS[1].TONGTIEN_LECH_TTHAI_CTIET = tongtien_lech_usd.ToString();

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGDIEN_TCTCO_NHTMCO = tongdien_khop.ToString();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCO = "0";

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS = new TONGTIEN_TCTCO_NHTMCOS();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS.TONGTIEN_TCTCO_NHTMCO_CTIETS = new List<TONGTIEN_TCTCO_NHTMCO_CTIETS>();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS.TONGTIEN_TCTCO_NHTMCO_CTIETS.Add(new TONGTIEN_TCTCO_NHTMCO_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS.TONGTIEN_TCTCO_NHTMCO_CTIETS[0].MA_NGUYENTE = "VND";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS.TONGTIEN_TCTCO_NHTMCO_CTIETS[0].TONGTIEN_TCTCO_NHTMCO_CTIET = tongtien_khop_vnd.ToString();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS.TONGTIEN_TCTCO_NHTMCO_CTIETS.Add(new TONGTIEN_TCTCO_NHTMCO_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS.TONGTIEN_TCTCO_NHTMCO_CTIETS[1].MA_NGUYENTE = "USD";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TCTCO_NHTMCOS.TONGTIEN_TCTCO_NHTMCO_CTIETS[1].TONGTIEN_TCTCO_NHTMCO_CTIET = tongtien_khop_usd.ToString();

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGDIEN_TNHAN = tongdien_tn.ToString();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHAN = "0";

                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS = new TONGTIEN_TNHANS();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS.TONGTIEN_TNHAN_CTIETS = new List<TONGTIEN_TNHAN_CTIETS>();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS.TONGTIEN_TNHAN_CTIETS.Add(new TONGTIEN_TNHAN_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS.TONGTIEN_TNHAN_CTIETS[0].MA_NGUYENTE = "VND";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS.TONGTIEN_TNHAN_CTIETS[0].TONGTIEN_TNHAN_CTIET = tongtien_tn_vnd.ToString();
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS.TONGTIEN_TNHAN_CTIETS.Add(new TONGTIEN_TNHAN_CTIETS());
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS.TONGTIEN_TNHAN_CTIETS[1].MA_NGUYENTE = "USD";
                msg07001_nd_dc.BAOCAO03.BAOCAO_HDR03.TONGTIEN_TNHANS.TONGTIEN_TNHAN_CTIETS[1].TONGTIEN_TNHAN_CTIET = tongtien_tn_usd.ToString();
                
            }
            catch (Exception e)
            {
                log.Error("5.BaoCaoDoiChieu03 - Bao cao doi chieu Header:" + e.Message);
                throw e;
            }

            #endregion

        }

        public void BaoCaoDoiChieu04(string ma_gdich_dc, DataTable dtQLDC)
        {

            int id_bc = Utility.TCS_GetSequence("NTDT_BAOCAO_HDR_SEQ");
            int id_bc_ct = -1;

            string ngay_gui_gd = "";

            string tu_ngay = "";
            string den_ngay = "";

            if (dtQLDC.Rows.Count > 0)
            {
                tu_ngay = dtQLDC.Rows[0]["tu_ngay_dc"].ToString();
                den_ngay = dtQLDC.Rows[0]["den_ngay_dc"].ToString();
            }

            DataTable dtDC = new DataTable();
            string sqlDC = "select * from tcs_dchieu_ctu_ntdt_hdr where ma_gdich_dc = '" + ma_gdich_dc + @"'";
            try
            {
                dtDC = DataAccess.ExecuteReturnDataSet(sqlDC, CommandType.Text).Tables[0];
                if (dtDC.Rows.Count > 0)
                {
                    ngay_gui_gd = dtDC.Rows[0]["ngay_gui_gdich"].ToString().Substring(0, 7);
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }



            if (dtDC.Rows.Count > 0)
            {
                
                #region "Bao cao doi chieu chi tiet thanh cong"
                DataTable dtDCTC = new DataTable();
                string sqlDCTC = @"select DISTINCT dchdr.so_ctu, dchdr.mahieu_ctu, dchdr.so_gnt, dchdr.ngay_nop_gnt, dchdr.ngay_nop_thue, dchdr.ten_nnop, dchdr.mst_nnop, dchdr.ma_cqt, dchdr.ma_cqthu, dchdr.ma_hieu_kbac, dchdr.tthai_ctu, dcdt.*, cthdr.ngay_nhang,cthdr.ma_nguyente,cthdr.ma_thamchieu  
                    from tcs_dchieu_ctu_ntdt_hdr dchdr, tcs_dchieu_ctu_ntdt_dtl dcdt, tcs_ctu_ntdt_hdr cthdr, tcs_ctu_ntdt_dtl ctdt 
                    where dchdr.so_ctu = dcdt.id_ctu 
                    and cthdr.so_ctu = ctdt.so_ct 
                    and dchdr.so_gnt = cthdr.so_gnt 
                    and dchdr.tthai_ctu = cthdr.tthai_ctu 
                    and SUBSTR(dchdr.ngay_gui_gdich,1,7) = SUBSTR(cthdr.ngay_gui_gd,1,7) 
                    " + /*and dcdt.ma_chuong = ctdt.ma_chuong 
                    and dcdt.ma_ndkt = ctdt.ma_tmuc 
                    and dcdt.tien_pnop = ctdt.sotien */
                    "and dchdr.ma_gdich_dc ='" + ma_gdich_dc + "' and cthdr.tthai_ctu ='11'";
                try
                {
                    dtDCTC = DataAccess.ExecuteReturnDataSet(sqlDCTC, CommandType.Text).Tables[0];
                    if (dtDCTC.Rows.Count > 0)
                    {

                        string sqlInsertCT = "";
                        for (int i = 0; i < dtDCTC.Rows.Count; i++)
                        {
                            id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R4_SEQ");

                            sqlInsertCT = @"insert into ntdt_baocao_ctiet_r4 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, NGAY_CTU, TEN_NNT, MST, MA_CQT, MA_CQTHU, MA_KBNN, MA_CHUONG, MA_TMUC, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, MA_HIEU_CTU,MA_NGUYENTE,MA_THAMCHIEU) 
                                        values(" + id_bc_ct + ", " + id_bc + ", '1', '" + dtDCTC.Rows[i]["so_ctu"].ToString() + "', '" + dtDCTC.Rows[i]["so_gnt"].ToString() + "', null, '" + dtDCTC.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtDCTC.Rows[i]["mst_nnop"].ToString() +
                                                "', '" + dtDCTC.Rows[i]["ma_cqt"].ToString() + "', '" + dtDCTC.Rows[i]["ma_cqthu"].ToString() + "', '" + dtDCTC.Rows[i]["ma_hieu_kbac"].ToString() + "', '" + dtDCTC.Rows[i]["ma_chuong"].ToString() + "', '" + dtDCTC.Rows[i]["ma_ndkt"].ToString() + "', " + dtDCTC.Rows[i]["tien_pnop"].ToString() + ",'" + dtDCTC.Rows[i]["tthai_ctu"].ToString() + "' , '" + dtDCTC.Rows[i]["ngay_nop_gnt"].ToString() +
                                                "', '" + ((DateTime)(dtDCTC.Rows[i]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '" + dtDCTC.Rows[i]["mahieu_ctu"].ToString() + "','" + dtDCTC.Rows[i]["MA_NGUYENTE"].ToString() + "', '" + dtDCTC.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                            DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                            //add du lieu xml ct tc
                            ROW_CTIET04 row_ct = new ROW_CTIET04();
                            row_ct.MA_THAMCHIEU = dtDCTC.Rows[i]["MA_THAMCHIEU"].ToString();
                            row_ct.SO_CTU = dtDCTC.Rows[i]["so_ctu"].ToString();
                            row_ct.MAHIEU_CTU = dtDCTC.Rows[i]["MAHIEU_CTU"].ToString();
                            row_ct.SO_GNT = dtDCTC.Rows[i]["SO_GNT"].ToString();
                            row_ct.NGAY_NOP_GNT = dtDCTC.Rows[i]["NGAY_NOP_GNT"].ToString();

                            row_ct.NGAY_NOP_THUE = dtDCTC.Rows[i]["NGAY_NOP_THUE"].ToString();

                            row_ct.TEN_NNT = dtDCTC.Rows[i]["ten_nnop"].ToString();
                            row_ct.MST = dtDCTC.Rows[i]["mst_nnop"].ToString();
                            row_ct.MA_CQT = dtDCTC.Rows[i]["MA_CQT"].ToString();
                            row_ct.MA_CQTHU = dtDCTC.Rows[i]["MA_CQTHU"].ToString();
                            row_ct.MA_KBNN = dtDCTC.Rows[i]["ma_hieu_kbac"].ToString();
                            row_ct.MA_CHUONG = dtDCTC.Rows[i]["ma_chuong"].ToString();
                            row_ct.MA_TMUC = dtDCTC.Rows[i]["ma_ndkt"].ToString();
                            row_ct.SO_TIEN = dtDCTC.Rows[i]["tien_pnop"].ToString();
                            row_ct.MA_NGUYENTE = dtDCTC.Rows[i]["MA_NGUYENTE"].ToString();
                            row_ct.TTHAI = dtDCTC.Rows[i]["tthai_ctu"].ToString();

                            row_ct.SO_TK_TB_QD = dtDCTC.Rows[i]["SO_TK_TB_QD"].ToString();

                            row_ct.ID_KNOP = dtDCTC.Rows[i]["ID_KNOP"].ToString();
                            row_ct.MA_DBHC_THU = dtDCTC.Rows[i]["MA_DBHC_THU"].ToString();
                            row_ct.TEN_DBHC_THU = dtDCTC.Rows[i]["TEN_DBHC_THU"].ToString();

                            msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04.DIEN_TCONG.ROW_CTIET04.Add(row_ct);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("1.BaoCaoDoiChieu04 - Bao cao doi chieu chi tiet thanh cong:" + ex.Message);
                    log.Error(ex.Message + "-" + ex.StackTrace);//
                    throw ex;
                }
                #endregion

                #region "Bao cao doi chieu chi tiet ko thanh cong"
                DataTable dtDCKO = new DataTable();
                string sqlDCKO = @"select DISTINCT dchdr.so_ctu, dchdr.mahieu_ctu, dchdr.so_gnt, dchdr.ngay_nop_gnt, dchdr.ngay_nop_thue, dchdr.ten_nnop, dchdr.mst_nnop, dchdr.ma_cqt, dchdr.ma_cqthu, dchdr.ma_hieu_kbac, dchdr.tthai_ctu, dcdt.*, cthdr.ngay_nhang,cthdr.ma_nguyente,cthdr.ma_thamchieu   
                    from tcs_dchieu_ctu_ntdt_hdr dchdr, tcs_dchieu_ctu_ntdt_dtl dcdt, tcs_ctu_ntdt_hdr cthdr, tcs_ctu_ntdt_dtl ctdt 
                    where dchdr.so_ctu = dcdt.id_ctu 
                    and cthdr.so_ctu = ctdt.so_ct
                    and dchdr.so_gnt = cthdr.so_gnt 
                    and dchdr.tthai_ctu = cthdr.tthai_ctu 
                    and SUBSTR(dchdr.ngay_gui_gdich,1,7) = SUBSTR(cthdr.ngay_gui_gd,1,7) 
                    " + /*and dcdt.ma_chuong = ctdt.ma_chuong 
                    and dcdt.ma_ndkt = ctdt.ma_tmuc 
                    and dcdt.tien_pnop = ctdt.sotien */
                    "and dchdr.ma_gdich_dc ='" + ma_gdich_dc + @"' 
                    and cthdr.tthai_ctu not in ('04', '05', '06', '07', '11')";
                try
                {
                    dtDCKO = DataAccess.ExecuteReturnDataSet(sqlDCKO, CommandType.Text).Tables[0];
                    if (dtDCKO.Rows.Count > 0)
                    {

                        string sqlInsertCT = "";
                        for (int i = 0; i < dtDCKO.Rows.Count; i++)
                        {
                            id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R4_SEQ");

                            sqlInsertCT = @"insert into ntdt_baocao_ctiet_r4 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, NGAY_CTU, TEN_NNT, MST, MA_CQT, MA_CQTHU, MA_KBNN, MA_CHUONG, MA_TMUC, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, MA_HIEU_CTU,MA_NGUYENTE,MA_THAMCHIEU) 
                                        values(" + id_bc_ct + ", " + id_bc + ", '2', '" + dtDCKO.Rows[i]["so_ctu"].ToString() + "', '" + dtDCKO.Rows[i]["so_gnt"].ToString() + "', null, '" + dtDCKO.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtDCKO.Rows[i]["mst_nnop"].ToString() +
                                                "', '" + dtDCKO.Rows[i]["ma_cqt"].ToString() + "', '" + dtDCKO.Rows[i]["ma_cqthu"].ToString() + "', '" + dtDCKO.Rows[i]["ma_hieu_kbac"].ToString() + "', '" + dtDCKO.Rows[i]["ma_chuong"].ToString() + "', " + dtDCKO.Rows[i]["ma_ndkt"].ToString() + ", '" + dtDCKO.Rows[i]["tien_pnop"].ToString() + "', '" + dtDCKO.Rows[i]["tthai_ctu"].ToString() + "', '" + dtDCKO.Rows[i]["ngay_nop_gnt"].ToString() +
                                                "', '" + ((DateTime)(dtDCKO.Rows[i]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '" + dtDCKO.Rows[i]["mahieu_ctu"].ToString() + "','" + dtDCKO.Rows[i]["MA_NGUYENTE"].ToString() + "', '" + dtDCKO.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                            DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                            //add du lieu xml ct ko
                            ROW_CTIET04 row_ct = new ROW_CTIET04();
                            row_ct.MA_THAMCHIEU = dtDCKO.Rows[i]["MA_THAMCHIEU"].ToString();
                            row_ct.SO_CTU = dtDCKO.Rows[i]["so_ctu"].ToString();
                            row_ct.MAHIEU_CTU = dtDCKO.Rows[i]["MAHIEU_CTU"].ToString();
                            row_ct.SO_GNT = dtDCKO.Rows[i]["SO_GNT"].ToString();
                            row_ct.NGAY_NOP_GNT = dtDCKO.Rows[i]["NGAY_NOP_GNT"].ToString();

                            row_ct.TEN_NNT = dtDCKO.Rows[i]["ten_nnop"].ToString();
                            row_ct.MST = dtDCKO.Rows[i]["mst_nnop"].ToString();
                            row_ct.MA_CQT = dtDCKO.Rows[i]["MA_CQT"].ToString();
                            row_ct.MA_CQTHU = dtDCKO.Rows[i]["MA_CQTHU"].ToString();
                            row_ct.MA_KBNN = dtDCKO.Rows[i]["ma_hieu_kbac"].ToString();
                            row_ct.MA_CHUONG = dtDCKO.Rows[i]["ma_chuong"].ToString();
                            row_ct.MA_TMUC = dtDCKO.Rows[i]["ma_ndkt"].ToString();
                            row_ct.SO_TIEN = dtDCKO.Rows[i]["tien_pnop"].ToString();
                            row_ct.MA_NGUYENTE = dtDCKO.Rows[i]["MA_NGUYENTE"].ToString();
                            row_ct.TTHAI = dtDCKO.Rows[i]["tthai_ctu"].ToString();

                            row_ct.NGAY_NOP_THUE = dtDCKO.Rows[i]["NGAY_NOP_THUE"].ToString();
                            row_ct.SO_TK_TB_QD = dtDCKO.Rows[i]["SO_TK_TB_QD"].ToString();

                            row_ct.ID_KNOP = dtDCKO.Rows[i]["ID_KNOP"].ToString();
                            row_ct.MA_DBHC_THU = dtDCKO.Rows[i]["MA_DBHC_THU"].ToString();
                            row_ct.TEN_DBHC_THU = dtDCKO.Rows[i]["TEN_DBHC_THU"].ToString();

                            msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04.DIEN_KO_TCONG.ROW_CTIET04.Add(row_ct);

                        }
                    }
                }
                catch (Exception ex)
                {
                   log.Error("2.BaoCaoDoiChieu04 - Bao cao doi chieu chi tiet ko thanh cong:" + ex.Message);
                   log.Error(ex.Message + "-" + ex.StackTrace);//
                    throw ex;
                }
                #endregion

                #region "Bao cao doi chieu chi tiet tu choi"
                DataTable dtDCx = new DataTable();
                string sqlDCx = @"select DISTINCT dchdr.so_ctu, dchdr.mahieu_ctu, dchdr.so_gnt, dchdr.ngay_nop_gnt, dchdr.ngay_nop_thue, dchdr.ten_nnop, dchdr.mst_nnop, dchdr.ma_cqt, dchdr.ma_cqthu, dchdr.ma_hieu_kbac, dchdr.tthai_ctu, dcdt.*, cthdr.ngay_nhang,cthdr.ma_nguyente,cthdr.ma_thamchieu   
                from tcs_dchieu_ctu_ntdt_hdr dchdr, tcs_dchieu_ctu_ntdt_dtl dcdt, tcs_ctu_ntdt_hdr cthdr, tcs_ctu_ntdt_dtl ctdt 
                where dchdr.so_ctu = dcdt.id_ctu 
                and cthdr.so_ctu = ctdt.so_ct 
                and dchdr.so_gnt = cthdr.so_gnt 
                and dchdr.tthai_ctu = cthdr.tthai_ctu 
                and SUBSTR(dchdr.ngay_gui_gdich,1,7) = SUBSTR(cthdr.ngay_gui_gd,1,7) 
                " + /*and dcdt.ma_chuong = ctdt.ma_chuong 
                and dcdt.ma_ndkt = ctdt.ma_tmuc 
                and dcdt.tien_pnop = ctdt.sotien */
                "and dchdr.ma_gdich_dc ='" + ma_gdich_dc + @"' 
                and cthdr.tthai_ctu in ('04', '05', '06', '07')";
                try
                {
                    dtDCx = DataAccess.ExecuteReturnDataSet(sqlDCx, CommandType.Text).Tables[0];
                    if (dtDCx.Rows.Count > 0)
                    {

                        string sqlInsertCT = "";
                        for (int i = 0; i < dtDCx.Rows.Count; i++)
                        {
                            id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R4_SEQ");

                            sqlInsertCT = @"insert into ntdt_baocao_ctiet_r4 (ID_BCAO_CTIET, ID_BCAO, CODE_CTIET, SO_CTU, SO_GNT, NGAY_CTU, TEN_NNT, MST, MA_CQT, MA_CQTHU, MA_KBNN, MA_CHUONG, MA_TMUC, SO_TIEN, TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, MA_HIEU_CTU,MA_NGUYENTE,MA_THAMCHIEU) 
                                        values(" + id_bc_ct + ", " + id_bc + ", '3', '" + dtDCx.Rows[i]["so_ctu"].ToString() + "', '" + dtDCx.Rows[i]["so_gnt"].ToString() + "', null, '" + dtDCx.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtDCx.Rows[i]["mst_nnop"].ToString() +
                                                "', '" + dtDCx.Rows[i]["ma_cqt"].ToString() + "', '" + dtDCx.Rows[i]["ma_cqthu"].ToString() + "', '" + dtDCx.Rows[i]["ma_hieu_kbac"].ToString() + "', '" + dtDCx.Rows[i]["ma_chuong"].ToString() + "', " + dtDCx.Rows[i]["ma_ndkt"].ToString() + ", '" + dtDCx.Rows[i]["tien_pnop"].ToString() + "', '" + dtDCx.Rows[i]["tthai_ctu"].ToString() + "', '" + dtDCx.Rows[i]["ngay_nop_gnt"].ToString() +
                                                "', '" + ((DateTime)(dtDCx.Rows[i]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '" + dtDCx.Rows[i]["mahieu_ctu"].ToString() + "','" + dtDCx.Rows[i]["MA_NGUYENTE"].ToString() + "', '" + dtDCx.Rows[i]["MA_THAMCHIEU"].ToString() + "')";

                            DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                            //add du lieu xml ct ko
                            ROW_CTIET04 row_ct = new ROW_CTIET04();
                            row_ct.MA_THAMCHIEU = dtDCx.Rows[i]["MA_THAMCHIEU"].ToString();
                            row_ct.SO_CTU = dtDCx.Rows[i]["so_ctu"].ToString();
                            row_ct.MAHIEU_CTU = dtDCx.Rows[i]["MAHIEU_CTU"].ToString();
                            row_ct.SO_GNT = dtDCx.Rows[i]["SO_GNT"].ToString();
                            row_ct.NGAY_NOP_GNT = dtDCx.Rows[i]["NGAY_NOP_GNT"].ToString();

                            row_ct.NGAY_NOP_THUE = dtDCx.Rows[i]["NGAY_NOP_THUE"].ToString();
                            row_ct.SO_TK_TB_QD = dtDCx.Rows[i]["SO_TK_TB_QD"].ToString();

                            row_ct.TEN_NNT = dtDCx.Rows[i]["ten_nnop"].ToString();
                            row_ct.MST = dtDCx.Rows[i]["mst_nnop"].ToString();
                            row_ct.MA_CQT = dtDCx.Rows[i]["MA_CQT"].ToString();
                            row_ct.MA_CQTHU = dtDCx.Rows[i]["MA_CQTHU"].ToString();
                            row_ct.MA_KBNN = dtDCx.Rows[i]["ma_hieu_kbac"].ToString();
                            row_ct.MA_CHUONG = dtDCx.Rows[i]["ma_chuong"].ToString();
                            row_ct.MA_TMUC = dtDCx.Rows[i]["ma_ndkt"].ToString();
                            row_ct.SO_TIEN = dtDCx.Rows[i]["tien_pnop"].ToString();
                            row_ct.MA_NGUYENTE = dtDCx.Rows[i]["MA_NGUYENTE"].ToString();
                            row_ct.TTHAI = dtDCx.Rows[i]["tthai_ctu"].ToString();

                            row_ct.ID_KNOP = dtDCx.Rows[i]["ID_KNOP"].ToString();
                            row_ct.MA_DBHC_THU = dtDCx.Rows[i]["MA_DBHC_THU"].ToString();
                            row_ct.TEN_DBHC_THU = dtDCx.Rows[i]["TEN_DBHC_THU"].ToString();

                            msg07001_nd_dc.BAOCAO04.BAOCAO_CTIET04.DIEN_TUCHOI.ROW_CTIET04.Add(row_ct);

                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("3.BaoCaoDoiChieu04 - Bao cao doi chieu chi tiet tu choi:" + ex.Message);
                    log.Error(ex.Message + "-" + ex.StackTrace);//
                    throw ex;
                }
                #endregion

                #region "Bao cao doi chieu Header"
                if (dtQLDC.Rows.Count > 0)
                {
                    try
                    {
                        string sqlInsertBCDC_HDR = @"insert into NTDT_BAOCAO_HDR (ID_BCAO, MA_GDICH_DC, MA_GDICH_TCHIEU_DC, NGAY_GDICH_DC, NGAY_DC, TU_NGAY_DC, DEN_NGAY_DC, MA_BCAO, TEN_BCAO) 
                                                values(" + id_bc + ", '" + dtQLDC.Rows[0]["MA_GDICH_DC"].ToString() + "', '" + dtQLDC.Rows[0]["MA_GDICH_TCHIEU_DC"].ToString() + "', " + dtQLDC.Rows[0]["NGAY_GDICH_DC"].ToString() + ", TO_DATE('" + ((DateTime)(dtQLDC.Rows[0]["NGAY_DC"])).ToString("dd/MM/yyyy") + "', 'dd/MM/yyyy'), " + dtQLDC.Rows[0]["TU_NGAY_DC"].ToString() + ", " + dtQLDC.Rows[0]["DEN_NGAY_DC"].ToString() + ", '04DC-NHTM-NTDT', 'Chi tiết thành công, khớp đúng đối với NHTM')";
                        DataAccess.ExecuteNonQuery(sqlInsertBCDC_HDR, CommandType.Text);

                        //add du lieu vao xml hdr
                        msg07001_nd_dc.BAOCAO04.BAOCAO_HDR04.PBAN_TLIEU_XML = GDICH_NTDT_Constants.HEADER_PHIENBAN_XML_TT84;
                        msg07001_nd_dc.BAOCAO04.BAOCAO_HDR04.MA_BCAO = "04DC-NHTM-NTDT";
                        msg07001_nd_dc.BAOCAO04.BAOCAO_HDR04.TEN_BCAO = "Chi tiết thành công, khớp đúng đối với NHTM";
                        msg07001_nd_dc.BAOCAO04.BAOCAO_HDR04.TGIAN_DCHIEU = DateTime.Now.ToString("dd/MM/yyyy");
                        msg07001_nd_dc.BAOCAO04.BAOCAO_HDR04.MA_NHANG_DCHIEU = GDICH_NTDT_Constants.MA_NHANG_DCHIEU;
                        msg07001_nd_dc.BAOCAO04.BAOCAO_HDR04.TEN_NHANG_DCHIEU = GDICH_NTDT_Constants.TEN_NHANG_DCHIEU;
                        
                    }
                    catch (Exception e)
                    {
                        log.Error("4.BaoCaoDoiChieu04 - Bao cao doi chieu Header:" + e.Message);
                        log.Error(e.Message + "-" + e.StackTrace);//
                        throw e;
                    }
                }
                #endregion

            }
        }

        public void BaoCaoDoiChieu05(string ma_gdich_dc, DataTable dtQLDC)
        {

            int id_bc = Utility.TCS_GetSequence("NTDT_BAOCAO_HDR_SEQ");
            int id_bc_ct = -1;

            string ngay_gui_gd = "";

            string tu_ngay = "";
            string den_ngay = "";

            if (dtQLDC.Rows.Count > 0)
            {
                tu_ngay = dtQLDC.Rows[0]["tu_ngay_dc"].ToString();
                den_ngay = dtQLDC.Rows[0]["den_ngay_dc"].ToString();
            }

            DataTable dtDC = new DataTable();
            string sqlDC = "select dchdr.*, dcdt.* from tcs_dchieu_ctu_ntdt_hdr dchdr, tcs_dchieu_ctu_ntdt_dtl dcdt where dchdr.so_ctu = dcdt.id_ctu and dchdr.ma_gdich_dc = '" + ma_gdich_dc + @"'  order by dchdr.so_ctu, dcdt.ma_chuong, dcdt.ma_ndkt";
            try
            {
                dtDC = DataAccess.ExecuteReturnDataSet(sqlDC, CommandType.Text).Tables[0];
                if (dtDC.Rows.Count > 0)
                {
                    ngay_gui_gd = dtDC.Rows[0]["ngay_gui_gdich"].ToString().Substring(0,7);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                //throw ex;
            }

            if (dtDC.Rows.Count > 0)
            {

                #region "Bao cao doi chieu chi tiet TCT_CO"

                DataTable dtDCTCT = new DataTable();
                string sqlDCTCT = "select dchdr.so_gnt from tcs_dchieu_ctu_ntdt_hdr dchdr where dchdr.ma_gdich_dc = '" + ma_gdich_dc + @"'
                                minus
                                select cthdr.so_gnt from tcs_ctu_ntdt_hdr cthdr where '" + tu_ngay + "' <= cthdr.ngay_gui_gd and cthdr.ngay_gui_gd <= '" + den_ngay + "' and cthdr.tthai_ctu <> '04'";
                try
                {
                    dtDCTCT = DataAccess.ExecuteReturnDataSet(sqlDCTCT, CommandType.Text).Tables[0];
                    if (dtDCTCT.Rows.Count > 0)
                    {
                        string so_gnt = "";
                        for (int i = 0; i < dtDCTCT.Rows.Count; i++)
                        {
                            so_gnt += "'" + dtDCTCT.Rows[i]["so_gnt"].ToString() + "',";
                        }
                        if (so_gnt.Length > 0)
                        {
                            so_gnt = so_gnt.Remove(so_gnt.Length - 1, 1);
                            DataTable dtTCT = new DataTable();
                            string sqlTCT = "select dchdr.so_ctu, dchdr.mahieu_ctu, dchdr.so_gnt, dchdr.ngay_nop_gnt, dchdr.ngay_nop_thue, dchdr.ten_nnop, dchdr.mst_nnop, dchdr.ma_cqt, dchdr.ma_cqthu, dchdr.ma_hieu_kbac, dchdr.tthai_ctu,dchdr.ma_nguyente,dchdr.ma_thamchieu, dcdt.* from tcs_dchieu_ctu_ntdt_hdr dchdr, tcs_dchieu_ctu_ntdt_dtl dcdt where dchdr.so_ctu = dcdt.id_ctu and dchdr.so_gnt in(" + so_gnt + ")";
                            dtTCT = DataAccess.ExecuteReturnDataSet(sqlTCT, CommandType.Text).Tables[0];
                            if (dtTCT.Rows.Count > 0)
                            {
                                string sqlInsertCT = "";
                                for (int i = 0; i < dtTCT.Rows.Count; i++)
                                {
                                    id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R5_SEQ");
                                    sqlInsertCT = @"insert into ntdt_baocao_ctiet_r5 (ID_BCAO_CTIET, ID_BCAO, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, MA_KBNN, TCT_MA_CHUONG, TCT_MA_TMUC, TCT_SO_TIEN, TCT_TTHAI, NHTM_MA_CHUONG, NHTM_MA_TMUC, NHTM_SO_TIEN, NHTM_TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, MA_HIEU_CTU,TCT_MA_NGUYENTE,MA_THAMCHIEU) 
                                                                            values(" + id_bc_ct + ", " + id_bc + ", '" + dtTCT.Rows[i]["so_ctu"].ToString() + "', '" + dtTCT.Rows[i]["so_gnt"].ToString() + "', '" + dtTCT.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtTCT.Rows[i]["mst_nnop"].ToString() +
                                                    "', '" + dtTCT.Rows[i]["ma_cqt"].ToString() + "', '" + dtTCT.Rows[i]["ma_cqthu"].ToString() + "', '" + dtTCT.Rows[i]["ma_hieu_kbac"].ToString() + "', '" + dtTCT.Rows[i]["ma_chuong"].ToString() + "', '" + dtTCT.Rows[i]["ma_ndkt"].ToString() + "', " + dtTCT.Rows[i]["tien_pnop"].ToString() + ", '" + dtTCT.Rows[i]["tthai_ctu"] + "' , null, null, null, null, '" + dtTCT.Rows[i]["ngay_nop_gnt"].ToString() + "', '" + dtTCT.Rows[i]["ngay_nop_thue"].ToString() + "', '" + dtTCT.Rows[i]["mahieu_ctu"].ToString() + "','" + dtTCT.Rows[i]["MA_NGUYENTE"].ToString() + "', '" + dtTCT.Rows[i]["MA_THAMCHIEU"].ToString() + "'   )";

                                    DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                                    //add du lieu xml ct ko
                                    ROW_CTIET05 row_ct = new ROW_CTIET05();
                                    row_ct.MA_THAMCHIEU = dtTCT.Rows[i]["MA_THAMCHIEU"].ToString();
                                    row_ct.SO_CTU = dtTCT.Rows[i]["so_ctu"].ToString();
                                    row_ct.MAHIEU_CTU = dtTCT.Rows[i]["MAHIEU_CTU"].ToString();
                                    row_ct.SO_GNT = dtTCT.Rows[i]["SO_GNT"].ToString();
                                    row_ct.NGAY_NOP_GNT = dtTCT.Rows[i]["NGAY_NOP_GNT"].ToString();

                                    row_ct.NGAY_NOP_THUE = dtTCT.Rows[i]["NGAY_NOP_THUE"].ToString();

                                    row_ct.TEN_NNT = dtTCT.Rows[i]["ten_nnop"].ToString();
                                    row_ct.MST = dtTCT.Rows[i]["mst_nnop"].ToString();
                                    row_ct.MA_CQT = dtTCT.Rows[i]["MA_CQT"].ToString();
                                    row_ct.MA_CQTHU = dtTCT.Rows[i]["MA_CQTHU"].ToString();
                                    row_ct.MA_KBNN = dtTCT.Rows[i]["ma_hieu_kbac"].ToString();
                                    row_ct.TCT_MA_CHUONG = dtTCT.Rows[i]["ma_chuong"].ToString();
                                    row_ct.TCT_MA_TMUC = dtTCT.Rows[i]["ma_ndkt"].ToString();
                                    row_ct.TCT_SO_TIEN = dtTCT.Rows[i]["tien_pnop"].ToString();
                                    row_ct.TCT_MA_NGUYENTE = dtTCT.Rows[i]["MA_NGUYENTE"].ToString();
                                    row_ct.TCT_TTHAI = dtTCT.Rows[i]["tthai_ctu"].ToString();

                                    row_ct.TCT_ID_KNOP = dtTCT.Rows[i]["ID_KNOP"].ToString();
                                    row_ct.TCT_MA_DBHC_THU = dtTCT.Rows[i]["MA_DBHC_THU"].ToString();
                                    row_ct.TCT_SO_TK = dtTCT.Rows[i]["SO_TK_TB_QD"].ToString();

                                    row_ct.NHTM_ID_KNOP = "";
                                    row_ct.NHTM_MA_DBHC_THU = "";
                                    row_ct.NHTM_SO_TK = "";

                                    row_ct.NHTM_MA_CHUONG = "";
                                    row_ct.NHTM_MA_TMUC = "";
                                    row_ct.NHTM_SO_TIEN = "";
                                    row_ct.NHTM_MA_NGUYENTE = "";
                                    row_ct.NHTM_TTHAI = "";
                                    msg07001_nd_dc.BAOCAO05.BAOCAO_CTIET05.ROW_CTIET05.Add(row_ct);

                                }
                            }

                        }

                    }
                }
                catch (Exception e)
                {
                   log.Error("1.BaoCaoDoiChieu05 - Bao cao doi chieu chi tiet TCT_CO:" + e.Message);
                   log.Error(e.Message + "-" + e.StackTrace);//
                    throw e;
                }

                #endregion

                #region "Bao cao doi chieu chi tiet NH_CO"

                DataTable dtDCNH = new DataTable();
                string sqlDCNH = "select cthdr.so_gnt from tcs_ctu_ntdt_hdr cthdr where '" + tu_ngay + "' <= cthdr.ngay_gui_gd and cthdr.ngay_gui_gd <= '" + den_ngay + @"' and cthdr.tthai_ctu <> '04' 
                                minus
                                select dchdr.so_gnt from tcs_dchieu_ctu_ntdt_hdr dchdr where dchdr.ma_gdich_dc = '" + ma_gdich_dc + "'";
                try
                {
                    dtDCNH = DataAccess.ExecuteReturnDataSet(sqlDCNH, CommandType.Text).Tables[0];
                    if (dtDCNH.Rows.Count > 0)
                    {
                        string so_gnt = "";
                        for (int i = 0; i < dtDCNH.Rows.Count; i++)
                        {
                            so_gnt += "'" + dtDCNH.Rows[i]["so_gnt"].ToString() + "',";
                        }
                        if (so_gnt.Length > 0)
                        {
                            so_gnt = so_gnt.Remove(so_gnt.Length - 1, 1);
                            DataTable dtNH = new DataTable();
                            string sqlNH = "select ctdt.MA_DBHC_THU, cthdr.so_ctu, cthdr.mahieu_ctu, cthdr.so_gnt, cthdr.ngay_lap, cthdr.ngay_nhang, cthdr.ten_nnop, cthdr.mst_nnop, cthdr.ma_cqt, cthdr.ma_cqthu, cthdr.ma_hieu_kbac, cthdr.tthai_ctu, cthdr.ngay_ht,cthdr.ma_nguyente,cthdr.ma_thamchieu, ctdt.* from tcs_ctu_ntdt_hdr cthdr, tcs_ctu_ntdt_dtl ctdt where cthdr.so_ctu = ctdt.so_ct and cthdr.so_gnt in(" + so_gnt + ")";
                            dtNH = DataAccess.ExecuteReturnDataSet(sqlNH, CommandType.Text).Tables[0];
                            if (dtNH.Rows.Count > 0)
                            {
                                string sqlInsertCT = "";
                                for (int i = 0; i < dtNH.Rows.Count; i++)
                                {
                                    id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R5_SEQ");
                                    sqlInsertCT = @"insert into ntdt_baocao_ctiet_r5 (ID_BCAO_CTIET, ID_BCAO, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, MA_KBNN, TCT_MA_CHUONG, TCT_MA_TMUC, TCT_SO_TIEN, TCT_TTHAI, NHTM_MA_CHUONG, NHTM_MA_TMUC, NHTM_SO_TIEN, NHTM_TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, MA_HIEU_CTU,MA_NGUYENTE,MA_THAMCHIEU) 
                                                                            values(" + id_bc_ct + ", " + id_bc + ", '" + dtNH.Rows[i]["so_ctu"].ToString() + "', '" + dtNH.Rows[i]["so_gnt"].ToString() + "', '" + dtNH.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtNH.Rows[i]["mst_nnop"].ToString() +
                                                    "', '" + dtNH.Rows[i]["ma_cqt"].ToString() + "', '" + dtNH.Rows[i]["ma_cqthu"].ToString() + "', '" + dtNH.Rows[i]["ma_hieu_kbac"].ToString() + "', null, null, null, null , '" + dtNH.Rows[i]["ma_chuong"].ToString() + "', '" + dtNH.Rows[i]["ma_tmuc"].ToString() + "', " + dtNH.Rows[i]["sotien"].ToString() + ", '" + dtNH.Rows[i]["tthai_ctu"].ToString() + "', '" + ((DateTime)(dtNH.Rows[i]["ngay_lap"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '" + ((DateTime)(dtNH.Rows[i]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '" + dtNH.Rows[i]["mahieu_ctu"].ToString() + "','" + dtNH.Rows[i]["MA_NGUYENTE"].ToString() + "', '" + dtNH.Rows[i]["MA_THAMCHIEU"].ToString() + "' )";

                                    DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                                    //add du lieu xml ct ko
                                    ROW_CTIET05 row_ct = new ROW_CTIET05();
                                    row_ct.MA_THAMCHIEU = dtNH.Rows[i]["MA_THAMCHIEU"].ToString();
                                    row_ct.SO_CTU = dtNH.Rows[i]["so_ctu"].ToString();
                                    row_ct.MAHIEU_CTU = dtNH.Rows[i]["MAHIEU_CTU"].ToString();
                                    row_ct.SO_GNT = dtNH.Rows[i]["SO_GNT"].ToString();

                                    row_ct.NGAY_NOP_GNT = DateTime.Parse(dtNH.Rows[i]["NGAY_NHANG"].ToString()).ToString("dd/MM/yyyy HH:mm:ss");

                                    if(dtNH.Rows[i]["NGAY_HT"].ToString().Length > 0)
                                    {
                                        row_ct.NGAY_NOP_THUE = DateTime.Parse(dtNH.Rows[i]["NGAY_HT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss");
                                    }
                                    else
                                    {
                                        row_ct.NGAY_NOP_THUE = "";
                                    }

                                    row_ct.TEN_NNT = dtNH.Rows[i]["ten_nnop"].ToString();
                                    row_ct.MST = dtNH.Rows[i]["mst_nnop"].ToString();
                                    row_ct.MA_CQT = dtNH.Rows[i]["MA_CQT"].ToString();
                                    row_ct.MA_CQTHU = dtNH.Rows[i]["MA_CQTHU"].ToString();
                                    row_ct.MA_KBNN = dtNH.Rows[i]["ma_hieu_kbac"].ToString();
                                    row_ct.TCT_MA_CHUONG = "";
                                    row_ct.TCT_MA_TMUC = "";
                                    row_ct.TCT_SO_TIEN = "";
                                    row_ct.TCT_MA_NGUYENTE = "";
                                    row_ct.TCT_TTHAI = "";

                                    row_ct.TCT_ID_KNOP = "";
                                    row_ct.TCT_MA_DBHC_THU = "";
                                    row_ct.TCT_SO_TK = "";

                                    row_ct.NHTM_ID_KNOP = dtNH.Rows[i]["ID_KNOP"].ToString();
                                    row_ct.NHTM_MA_DBHC_THU = dtNH.Rows[i]["MA_DBHC_THU"].ToString();
                                    row_ct.NHTM_SO_TK = dtNH.Rows[i]["SO_TK_TB_QD"].ToString();

                                    row_ct.NHTM_MA_CHUONG = dtNH.Rows[i]["ma_chuong"].ToString();
                                    row_ct.NHTM_MA_TMUC = dtNH.Rows[i]["ma_tmuc"].ToString();
                                    row_ct.NHTM_SO_TIEN = dtNH.Rows[i]["sotien"].ToString();
                                    row_ct.NHTM_MA_NGUYENTE = dtNH.Rows[i]["MA_NGUYENTE"].ToString();
                                    row_ct.NHTM_TTHAI = dtNH.Rows[i]["tthai_ctu"].ToString();

                                    msg07001_nd_dc.BAOCAO05.BAOCAO_CTIET05.ROW_CTIET05.Add(row_ct);

                                }
                            }

                        }

                    }
                }
                catch (Exception e)
                {
                    log.Error("2.BaoCaoDoiChieu05 - Bao cao doi chieu chi tiet NH_CO:" + e.Message);
                    log.Error(e.Message + "-" + e.StackTrace);//
                    throw e;
                }

                #endregion

                #region "Bao cao doi chieu chi tiet Lech"
                DataTable dtDCLech = new DataTable();
                string sqlDCLech = "select ctdt.MA_DBHC_THU, cthdr.*, ctdt.* from tcs_ctu_ntdt_hdr cthdr, tcs_ctu_ntdt_dtl ctdt where cthdr.so_ctu = ctdt.so_ct and '" + tu_ngay + "' <= cthdr.ngay_gui_gd and cthdr.ngay_gui_gd <= '" + den_ngay + "' and cthdr.tthai_ctu <> '04' order by cthdr.so_ctu, ctdt.ma_chuong, ctdt.ma_tmuc";
                try
                {
                    dtDCLech = DataAccess.ExecuteReturnDataSet(sqlDCLech, CommandType.Text).Tables[0];
                    if (dtDCLech.Rows.Count > 0)
                    {
                        string sqlInsertCT = "";

                        for (int i = 0; i < dtDC.Rows.Count; i++)
                        {
                            bool lech = true;
                            int lechtai = 0;
                            
                            /*
                            for (int j = 0; j < dtDCLech.Rows.Count; j++)
                            {
                                if (dtDC.Rows[i]["so_gnt"].ToString() == dtDCLech.Rows[j]["so_gnt"].ToString() && dtDC.Rows[i]["tthai_ctu"].ToString() == dtDCLech.Rows[j]["tthai_ctu"].ToString() && dtDC.Rows[i]["ma_chuong"].ToString() == dtDCLech.Rows[j]["ma_chuong"].ToString() && dtDC.Rows[i]["ma_ndkt"].ToString() == dtDCLech.Rows[j]["ma_tmuc"].ToString() && dtDC.Rows[i]["tien_pnop"].ToString() == dtDCLech.Rows[j]["sotien"].ToString())
                                {
                                    lech = false;
                                    if (dtDC.Rows[i]["so_gnt"].ToString() == dtDCLech.Rows[j]["so_gnt"].ToString() || dtDC.Rows[i]["ma_chuong"].ToString() == dtDCLech.Rows[j]["ma_chuong"].ToString() && dtDC.Rows[i]["ma_tmuc"].ToString() == dtDCLech.Rows[j]["ma_tmuc"].ToString() && dtDC.Rows[i]["sotien"].ToString() == dtDCLech.Rows[j]["tien_pnop"].ToString())
                                    {
                                        lechtai = j;
                                    }
                                }

                            }*/

                            lech = false;
                            for (int j = 0; j < dtDCLech.Rows.Count; j++)
                            {
                                if (dtDC.Rows[i]["so_gnt"].ToString() == dtDCLech.Rows[j]["so_gnt"].ToString() && dtDC.Rows[i]["ma_chuong"].ToString() == dtDCLech.Rows[j]["ma_chuong"].ToString() && dtDC.Rows[i]["ma_ndkt"].ToString() == dtDCLech.Rows[j]["ma_tmuc"].ToString() && dtDC.Rows[i]["tthai_ctu"].ToString() != dtDCLech.Rows[j]["tthai_ctu"].ToString())
                                {
                                    lech = true;
                                    lechtai = j;
                                }
                            }

                            if (lech)
                            {
                                id_bc_ct = Utility.TCS_GetSequence("NTDT_BAOCAO_CTIET_R5_SEQ");
                                sqlInsertCT = @"insert into ntdt_baocao_ctiet_r5 (ID_BCAO_CTIET, ID_BCAO, SO_CTU, SO_GNT, TEN_NNT, MST, MA_CQT, MA_CQTHU, MA_KBNN, TCT_MA_CHUONG, TCT_MA_TMUC, TCT_SO_TIEN, TCT_TTHAI, NHTM_MA_CHUONG, NHTM_MA_TMUC, NHTM_SO_TIEN, NHTM_TTHAI, NGAY_NOP_GNT, NGAY_NOP_THUE, MA_HIEU_CTU,TCT_MA_NGUYENTE,MA_THAMCHIEU, MA_NGUYENTE) 
                                                                            values(" + id_bc_ct + ", " + id_bc + ", '" + dtDC.Rows[i]["so_ctu"].ToString() + "', '" + dtDC.Rows[i]["so_gnt"].ToString() + "', '" + dtDC.Rows[i]["ten_nnop"].ToString().Replace("'", " ") + "', '" + dtDC.Rows[i]["mst_nnop"].ToString() +
                                                    "', '" + dtDC.Rows[i]["ma_cqt"].ToString() + "', '" + dtDC.Rows[i]["ma_cqthu"].ToString() + "', '" + dtDC.Rows[i]["ma_hieu_kbac"].ToString() + "', '" + dtDC.Rows[i]["ma_chuong"].ToString() + "', '" + dtDC.Rows[i]["ma_ndkt"].ToString() + "', " + dtDC.Rows[i]["tien_pnop"].ToString() + ", '" + dtDC.Rows[i]["tthai_ctu"] + "' , '" + dtDCLech.Rows[lechtai]["ma_chuong"].ToString() + "', '" + dtDCLech.Rows[lechtai]["ma_tmuc"].ToString() + "', " + dtDCLech.Rows[lechtai]["sotien"].ToString() + ", '" + dtDCLech.Rows[lechtai]["tthai_ctu"].ToString() + "', '" + dtDC.Rows[i]["ngay_nop_gnt"].ToString() + "', '" + ((DateTime)(dtDCLech.Rows[i]["ngay_nhang"])).ToString("dd/MM/yyyy HH:mm:ss") + "', '" + dtDC.Rows[i]["mahieu_ctu"].ToString() + "','" + dtDC.Rows[i]["MA_NGUYENTE"].ToString() + "' , '" + dtDC.Rows[i]["MA_THAMCHIEU"].ToString() + "' ,'" + dtDCLech.Rows[lechtai]["MA_NGUYENTE"].ToString()+"')";

                                DataAccess.ExecuteNonQuery(sqlInsertCT, CommandType.Text);

                                //add du lieu xml ct ko
                                ROW_CTIET05 row_ct = new ROW_CTIET05();
                                row_ct.MA_THAMCHIEU = dtDC.Rows[i]["MA_THAMCHIEU"].ToString();
                                row_ct.SO_CTU = dtDC.Rows[i]["so_ctu"].ToString();
                                row_ct.MAHIEU_CTU = dtDC.Rows[i]["MAHIEU_CTU"].ToString();
                                row_ct.SO_GNT = dtDC.Rows[i]["SO_GNT"].ToString();
                                row_ct.NGAY_NOP_GNT = dtDC.Rows[i]["NGAY_NOP_GNT"].ToString();

                                row_ct.NGAY_NOP_THUE = dtDC.Rows[i]["NGAY_NOP_THUE"].ToString();

                                row_ct.TEN_NNT = dtDC.Rows[i]["ten_nnop"].ToString();
                                row_ct.MST = dtDC.Rows[i]["mst_nnop"].ToString();
                                row_ct.MA_CQT = dtDC.Rows[i]["MA_CQT"].ToString();
                                row_ct.MA_CQTHU = dtDC.Rows[i]["MA_CQTHU"].ToString();
                                row_ct.MA_KBNN = dtDC.Rows[i]["ma_hieu_kbac"].ToString();
                                row_ct.TCT_MA_CHUONG = dtDC.Rows[i]["ma_chuong"].ToString();
                                row_ct.TCT_MA_TMUC = dtDC.Rows[i]["ma_ndkt"].ToString();
                                row_ct.TCT_SO_TIEN = dtDC.Rows[i]["tien_pnop"].ToString();
                                row_ct.TCT_MA_NGUYENTE = dtDC.Rows[i]["MA_NGUYENTE"].ToString();
                                row_ct.TCT_TTHAI = dtDC.Rows[i]["tthai_ctu"].ToString();

                                row_ct.TCT_ID_KNOP = dtDC.Rows[i]["ID_KNOP"].ToString();
                                row_ct.TCT_MA_DBHC_THU = dtDC.Rows[i]["MA_DBHC_THU"].ToString();
                                row_ct.TCT_SO_TK = dtDC.Rows[i]["SO_TK_TB_QD"].ToString();

                                row_ct.NHTM_ID_KNOP = dtDCLech.Rows[i]["ID_KNOP"].ToString();
                                row_ct.NHTM_MA_DBHC_THU = dtDCLech.Rows[i]["MA_DBHC_THU"].ToString();
                                row_ct.NHTM_SO_TK = dtDCLech.Rows[i]["SO_TK_TB_QD"].ToString();

                                row_ct.NHTM_MA_CHUONG = dtDCLech.Rows[lechtai]["ma_chuong"].ToString();
                                row_ct.NHTM_MA_TMUC = dtDCLech.Rows[lechtai]["ma_tmuc"].ToString();
                                row_ct.NHTM_SO_TIEN = dtDCLech.Rows[lechtai]["sotien"].ToString();
                                row_ct.NHTM_MA_NGUYENTE = dtDCLech.Rows[lechtai]["MA_NGUYENTE"].ToString();
                                row_ct.NHTM_TTHAI = dtDCLech.Rows[lechtai]["tthai_ctu"].ToString();

                                msg07001_nd_dc.BAOCAO05.BAOCAO_CTIET05.ROW_CTIET05.Add(row_ct);

                            }
                        }

                    }
                }
                catch (Exception e)
                {
                    log.Error("3.BaoCaoDoiChieu05 - Bao cao doi chieu chi tiet Lech:" + e.Message);
                    throw e;
                }
                #endregion
            }
            #region "Bao cao doi chieu Header"
            if (dtQLDC.Rows.Count > 0)
            {
                try
                {
                    string sqlInsertBCDC_HDR = @"insert into NTDT_BAOCAO_HDR (ID_BCAO, MA_GDICH_DC, MA_GDICH_TCHIEU_DC, NGAY_GDICH_DC, NGAY_DC, TU_NGAY_DC, DEN_NGAY_DC, MA_BCAO, TEN_BCAO) 
                                                values(" + id_bc + ", '" + dtQLDC.Rows[0]["MA_GDICH_DC"].ToString() + "', '" + dtQLDC.Rows[0]["MA_GDICH_TCHIEU_DC"].ToString() + "', " + dtQLDC.Rows[0]["NGAY_GDICH_DC"].ToString() + ", TO_DATE('" + ((DateTime)(dtQLDC.Rows[0]["NGAY_DC"])).ToString("dd/MM/yyyy") + "', 'dd/MM/yyyy'), " + dtQLDC.Rows[0]["TU_NGAY_DC"].ToString() + ", " + dtQLDC.Rows[0]["DEN_NGAY_DC"].ToString() + ", '05DC-NHTM-NTDT', 'Chi tiết sai lệch đối với NHTM')";
                    DataAccess.ExecuteNonQuery(sqlInsertBCDC_HDR, CommandType.Text);

                    //add du lieu vao xml hdr
                    msg07001_nd_dc.BAOCAO05.BAOCAO_HDR05.PBAN_TLIEU_XML = GDICH_NTDT_Constants.HEADER_PHIENBAN_XML_TT84;
                    msg07001_nd_dc.BAOCAO05.BAOCAO_HDR05.MA_BCAO = "05DC-NHTM-NTDT";
                    msg07001_nd_dc.BAOCAO05.BAOCAO_HDR05.TEN_BCAO = "Chi tiết sai lệch đối với NHTM";
                    msg07001_nd_dc.BAOCAO05.BAOCAO_HDR05.TGIAN_DCHIEU = DateTime.Now.ToString("dd/MM/yyyy");
                    msg07001_nd_dc.BAOCAO05.BAOCAO_HDR05.MA_NHANG_DCHIEU = GDICH_NTDT_Constants.MA_NHANG_DCHIEU;
                    msg07001_nd_dc.BAOCAO05.BAOCAO_HDR05.TEN_NHANG_DCHIEU = GDICH_NTDT_Constants.TEN_NHANG_DCHIEU;
                }
                catch (Exception e)
                {
                    log.Error("4.BaoCaoDoiChieu05 - Bao cao doi chieu Header:" + e.Message);
                    log.Error(e.Message + "-" + e.StackTrace);//
                    throw e;
                }
            }
            #endregion
        }
        
        
    }
}
