﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETAX_GDT
{
    class GDICH_NTDT_Constants
    {
            public static string ESB_TYPE_MESS_CTU_TVAN_TTXL = "03001";
            public static string ESB_TYPE_MESS_CTU_TTXL_NH = "03002"; 
            public static string ESB_TYPE_MESS_TBAO_NH_TTXL = "06000";
            public static string ESB_TYPE_MESS_TBAO_TTXL_TVAN = "06001";
            public static string ESB_TYPE_MESS_TBAO_REPLY_CTU_TTXL_NH = "06002";
            public static string ESB_TYPE_MESS_TBAO_REPLY_NH_TTXL = "06004"; 
            public static string ESB_TYPE_DL_DOICHIEU_TTXL_NH = "07000"; 
            public static string ESB_TYPE_BCAO_DOICHIEU_NH_TTXL = "07001"; 
            //MESS DANG KY
            public static string ESB_TYPE_MESS_DKY_TVAN_TTXL = "06005"; 
            public static string ESB_TYPE_MESS_DKY_TTXL_NH = "06006"; 
            public static string ESB_TYPE_MESS_TBAO_DKY_NH_TTXL = "06007";
            public static string ESB_TYPE_MESS_TBAO_DKY_TTXL_TVAN = "06008";
            //MESS THAY DOI THONG TIN CKS
            public static string ESB_TYPE_MESS_TTIN_CTS_TVAN_TTXL = "06009";
            public static string ESB_TYPE_MESS_TTIN_CTS_TTXL_NH = "06010"; 
            public static string ESB_TYPE_MESS_TBAO_TTIN_CTS_NH_TTXL = "06011"; 
            public static string ESB_TYPE_MESS_TBAO_TTIN_CTS_TTXL_TVAN = "06012";
            //MESS THAY DOI THONG TIN TAI KHOAN
            public static string ESB_TYPE_MESS_TTIN_TKHOAN_NH_TTXL = "06013"; 
            public static string ESB_TYPE_MESS_TTIN_TKHOAN_TTXL_TVAN = "06014";
            //MESS DANG KY NGUNG
            public static string ESB_TYPE_MESS_DKY_NGUNG_TVAN_TTXL = "06015"; 
            public static string ESB_TYPE_MESS_DKY_NGUNG_TTXL_NH = "06016"; 
            public static string ESB_TYPE_MESS_DKY_NGUNG_TTXL_TVAN = "06017";

            public static string ESB_TYPE_MESS_TBAO_UNT = "06048";

            //Tham so header
            public static string HEADER_PHIENBAN_XML = "4.0.0";
            public static string HEADER_PHIENBAN_XML_TT84 = "4.0.2";
            public static string HEADER_VERSION = "1.0";
            public static string HEADER_SENDER_CODE = "NTDT_SHB";
            public static string HEADER_SENDER_NAME = "Hệ thống nộp thuế điện tử SHB";
            public static string HEADER_RECEIVER_CODE = "TTXL";
            public static string HEADER_RECEIVER_NAME = "Trung tâm xử lý-Ứng dụng Nộp thuế điện tử";

            public static string MA_NHANG_DCHIEU = "SHB";
            public static string TEN_NHANG_DCHIEU = "Ngân hàng TMCP Sài Gòn - Hà Nội";

            //TRANG THAI GIAO DICH
            public static string TT_CT_NHAN = "15";
            public static string TT_CT_SAI_CKS = "05";
            public static string TT_CT_THIEU_TIEN = "09";
            public static string TT_CT_KB = "10";
            public static string TT_CT_LOI = "13";
            public static string TT_CT_THANHCONG = "11";
            //public static string TT_CT_SAITK = "13";
            public static string TT_CT_TIMEOUT = "TO999";
            public static string TEN_TT_CT_NHAN = "Mới nhận từ trung tâm xử lý";
            public static string TEN_TT_CT_THIEU_TIEN = "Lỗi GD không thành công do không đủ số dư";
            public static string TEN_TT_CT_KB = "Lỗi GD không thành công do không tìm thấy thông tin KBNN";
            public static string TEN_TT_CT_LOI = "Xử lý chứng từ không thành công tại Ngân hàng";
            public static string TEN_TT_CT_THANHCONG = "Nộp thuế thành công";
            //public static string TEN_TT_CT_SAITK = "Sai tài khoản";
            public static string TEN_TT_CT_TIMEOUT = "Xử lý chứng từ không thành công tại Ngân hàng. Lỗi kết nối Corebank.";
            public static string TEN_TT_TRUNG_GNT = "Trùng giấy nộp tiền";
            public static string TEN_TT_SAI_CKS = "SAI CHỮ KÝ SỐ";
            public static string TT_SAI_XML = "07";
            public static string TEN_TT_SAI_XML = "Sai cấu trúc XML";



            public static string FORMAT_NGAY_TB = "dd/MM/yyyy HH:mm:ss";
            public static string FORMAT_NGAY_GD_HEADER = "dd-MMM-yyyy HH:mm:ss";
            public static string get_thongbao(string strMa_TB)
            {
                string strReturn;
                switch (strMa_TB)
                {
                    case "01":
                        strReturn = "01;V/v Xác nhận trạng thái giao dịch;01/TB-NHTM;01";
                        break;
                    case "02":
                        strReturn = "02;V/v Kết quả đăng ký sử dụng dịch vụ nộp thuế điện tử với NHTM;02/TB-NHTM;02";
                        break;
                    case "03":
                        strReturn = "03;V/v Không chấp thuận đăng ký sử dụng hình thức giao dịch điện tử trong lĩnh vực thuế;03/TB-NHTM;03";
                        break;
                    case "04":
                        strReturn = "04;V/v Xác nhận thay đổi thông tin đăng ký sử dụng dịch vụ nộp thuế điện tử với NHTM;04/TB-NHTM;04";
                        break;
                    default:
                        strReturn = "05;V/v Không chấp thuận thông tin thay đổi đăng ký sử dụng dịch vụ nộp thuế điện tử với NHTM;05/TB-NHTM;05";
                        break;
                }
                return strReturn;

            }
    }
}
