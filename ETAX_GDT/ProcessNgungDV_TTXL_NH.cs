﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG;
using GDT.MSG.MSG06016;
using System.Data.OracleClient;
using System.Data;
using Business.NTDT.NNTDT;
using GDT.MSG.MSGHeader;
using VBOracleLib;
namespace ETAX_GDT
{
    public class ProcessNgungDV_TTXL_NH
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string strTran_Type = "";
        private string strMSG_TEXT = "";
        public ProcessNgungDV_TTXL_NH()
        {}

        public void Process(MSG msg, bool checkCK)
        {
            try
            {

                if (msg != null)
                {
                    string xmlIn = "";
                    xmlIn = msg.content_mess;

                    if (xmlIn.Length > 0)
                    {
                        xmlIn = xmlIn.Replace("<DATA>", Utility.strXMLdefin2);
                        MSG06016 msg06016 = new MSG06016();
                        MSG06016 objTemp = null;

                        try
                        {
                            objTemp = msg06016.MSGToObject(xmlIn);
                            if (objTemp != null)
                            {

                                TBAO_NGUNG_NT TBAO_NGUNG_NT = objTemp.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT;

                                MSGHeader msgheader = new MSGHeader();
                                msgheader.HEADER = new GDT.MSG.MSGHeader.HEADER();

                                msgheader.HEADER.ERROR_CODE = objTemp.HEADER.ERROR_CODE;
                                msgheader.HEADER.ERROR_DESC = objTemp.HEADER.ERROR_DESC;
                                msgheader.HEADER.ID_LINK = objTemp.HEADER.ID_LINK;
                                msgheader.HEADER.MSG_ID = objTemp.HEADER.MSG_ID;
                                msgheader.HEADER.MSG_REFID = objTemp.HEADER.MSG_REFID;
                                msgheader.HEADER.ORIGINAL_CODE = objTemp.HEADER.ORIGINAL_CODE;
                                msgheader.HEADER.ORIGINAL_DATE = objTemp.HEADER.ORIGINAL_DATE;
                                msgheader.HEADER.ORIGINAL_NAME = objTemp.HEADER.ORIGINAL_NAME;
                                msgheader.HEADER.RECEIVER_CODE = objTemp.HEADER.RECEIVER_CODE;
                                msgheader.HEADER.RECEIVER_NAME = objTemp.HEADER.RECEIVER_NAME;
                                msgheader.HEADER.SEND_DATE = objTemp.HEADER.SEND_DATE;
                                msgheader.HEADER.SENDER_CODE = objTemp.HEADER.SENDER_CODE;
                                msgheader.HEADER.SENDER_NAME = objTemp.HEADER.SENDER_NAME;
                                msgheader.HEADER.SPARE1 = objTemp.HEADER.SPARE1;
                                msgheader.HEADER.SPARE2 = objTemp.HEADER.SPARE2;
                                msgheader.HEADER.SPARE3 = objTemp.HEADER.SPARE3;
                                msgheader.HEADER.TRAN_CODE = objTemp.HEADER.TRAN_CODE;
                                msgheader.HEADER.VERSION = objTemp.HEADER.VERSION;
                                if (checkCK == false)
                                {
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "02", "05 - Sai chữ ký số");
                                }
                                else 
                                if (TBAO_NGUNG_NT.PBAN_TLIEU_XML == null || TBAO_NGUNG_NT.MA_TBAO == null || TBAO_NGUNG_NT.MAU_TBAO == null || TBAO_NGUNG_NT.SO_TBAO == null || TBAO_NGUNG_NT.TEN_TBAO == null || TBAO_NGUNG_NT.NGAY_TBAO == null || TBAO_NGUNG_NT.MST == null || TBAO_NGUNG_NT.TEN_NNT == null || TBAO_NGUNG_NT.MA_CQT == null || TBAO_NGUNG_NT.TEN_CQT == null || TBAO_NGUNG_NT.TGIAN_NGUNG == null || TBAO_NGUNG_NT.LYDO_NGUNG == null || TBAO_NGUNG_NT.VAN_ID == null || TBAO_NGUNG_NT.TEN_TVAN == null || TBAO_NGUNG_NT.MA_NHANG == null || TBAO_NGUNG_NT.TEN_NHANG == null)
                                {

                                    //phản hồi nhận dữ liệu bị lỗi cấu trúc cho tct
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "02", "07 - Cấu trúc ngừng dịch vụ không đúng khuôn dạng");

                                }
                                else
                                {

                                    capnhatNgungDV(objTemp);

                                    //phản hồi đã nhận dữ liệu
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "01", "Thành công");
                                }
                                
                            }
                        }
                        catch (Exception e)
                        {
                            log.Error(e.Message + "-" + e.StackTrace);//
                        }

                        strTran_Type = "MSG06016";
                        strMSG_TEXT = "Process=Time:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "capnhatNgungDV=" + objTemp.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.MST .ToString();
                        Utility.Log_Action(strTran_Type, strMSG_TEXT);
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                strTran_Type = "MSG06016";
                strMSG_TEXT = "Process=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "Exception=" + ex.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            }
        }


        private static void capnhatNgungDV(MSG06016 msg06016)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("UPDATE TCS_DM_NNTDT set TRANG_THAI = :TRANG_THAI,");
            sbSqlInsert.Append("TGIAN_NGUNG = :TGIAN_NGUNG,");
            sbSqlInsert.Append("LDO_NGUNG = :LDO_NGUNG,");
            sbSqlInsert.Append("SIGNATURE_NNT = :SIGNATURE_NNT,");
            sbSqlInsert.Append("SIGNATURE_TCT = :SIGNATURE_TCT,");
            sbSqlInsert.Append("MA_GDICH = :MA_GDICH,");
            sbSqlInsert.Append("NGAY_GUI_GDICH = :NGAY_GUI_GDICH,");
            sbSqlInsert.Append("MA_GDICH_TCHIEU = :MA_GDICH_TCHIEU");
            sbSqlInsert.Append(" WHERE MST = :MST");

            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT == null)
                {
                    throw new Exception("Lỗi lấy thông tin ngừng sử dụng dịch vụ NTDT");
                }

                if (msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT != null)
                {

                    ////Khai bao cac cau lenh
                    IDbDataParameter[] p = new IDbDataParameter[0];

                    //Gan cac thong tin
                    string TRANG_THAI = DBNull.Value.ToString();
                    string MST = DBNull.Value.ToString();
                    string MA_CQT = DBNull.Value.ToString();
                    string TGIAN_NGUNG = DBNull.Value.ToString();
                    string LDO_NGUNG = DBNull.Value.ToString();
                    string SIGNATURE_NNT = DBNull.Value.ToString();
                    string SIGNATURE_TCT = DBNull.Value.ToString();
                    string MA_GDICH = DBNull.Value.ToString();
                    string NGAY_GUI_GDICH = DBNull.Value.ToString();
                    string MA_GDICH_TCHIEU = DBNull.Value.ToString();

                    //foreach (Customs.MSG.MSG16.ItemData itdata in msg16.Data.Item)
                    //{
                    TRANG_THAI = "06";
                    if (msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.MST != null)
                    {
                        if (!msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.MST.Equals(string.Empty))
                        {
                            MST = msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.MST;
                        }
                    }
                    if (msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.MA_CQT != null)
                    {
                        if (!msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.MA_CQT.Equals(string.Empty))
                        {
                            MA_CQT = msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.MA_CQT;
                        }
                    }
                    
                    if (msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.Signature != null)
                    {
                        if (msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.Signature.Count > 0)
                        {
                            if (!msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.Signature[0].Equals(string.Empty))
                            {
                                SIGNATURE_NNT = msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.Signature[0];
                            }
                        }
                        if (msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.Signature.Count > 1)
                        {
                            if (!msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.Signature[1].Equals(string.Empty))
                            {
                                SIGNATURE_TCT = msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.Signature[1];
                            }

                        }
                    }

                    if (msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.TGIAN_NGUNG != null)
                    {
                        if (!msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.TGIAN_NGUNG.Equals(string.Empty))
                        {
                            TGIAN_NGUNG = msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.TGIAN_NGUNG;
                        }
                    }
                    if (msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.LYDO_NGUNG != null)
                    {
                        if (!msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.LYDO_NGUNG.Equals(string.Empty))
                        {
                            LDO_NGUNG = msg06016.BODY.ROW.GIAODICH.TBAO_NGUNG.TBAO_NGUNG_NT.LYDO_NGUNG;
                        }
                    }

                    if (msg06016.BODY.ROW.GIAODICH.MA_GDICH != null)
                    {
                        if (!msg06016.BODY.ROW.GIAODICH.MA_GDICH.Equals(string.Empty))
                        {
                            MA_GDICH = msg06016.BODY.ROW.GIAODICH.MA_GDICH;
                        }
                    }
                    if (msg06016.BODY.ROW.GIAODICH.NGAY_GUI_GDICH != null)
                    {
                        if (!msg06016.BODY.ROW.GIAODICH.NGAY_GUI_GDICH.Equals(string.Empty))
                        {
                            NGAY_GUI_GDICH = msg06016.BODY.ROW.GIAODICH.NGAY_GUI_GDICH;
                        }
                    }

                    if (msg06016.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU != null)
                    {
                        if (!msg06016.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU.Equals(string.Empty))
                        {
                            MA_GDICH_TCHIEU = msg06016.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU;
                        }
                    }
                    
                    p = new IDbDataParameter[9];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "TRANG_THAI";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = TRANG_THAI;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "TGIAN_NGUNG";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = TGIAN_NGUNG;


                    p[2] = new OracleParameter();
                    p[2].ParameterName = "LDO_NGUNG";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = LDO_NGUNG;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "SIGNATURE_NNT";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = SIGNATURE_NNT;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "SIGNATURE_TCT";
                    p[4].DbType = DbType.String;
                    p[4].Direction = ParameterDirection.Input;
                    p[4].Value = SIGNATURE_TCT;

                    p[5] = new OracleParameter();
                    p[5].ParameterName = "MA_GDICH";
                    p[5].DbType = DbType.String;
                    p[5].Direction = ParameterDirection.Input;
                    p[5].Value = MA_GDICH;

                    p[6] = new OracleParameter();
                    p[6].ParameterName = "NGAY_GUI_GDICH";
                    p[6].DbType = DbType.String;
                    p[6].Direction = ParameterDirection.Input;
                    p[6].Value = NGAY_GUI_GDICH;

                    p[7] = new OracleParameter();
                    p[7].ParameterName = "MA_GDICH_TCHIEU";
                    p[7].DbType = DbType.String;
                    p[7].Direction = ParameterDirection.Input;
                    p[7].Value = MA_GDICH_TCHIEU;

                    p[8] = new OracleParameter();
                    p[8].ParameterName = "MST";
                    p[8].DbType = DbType.String;
                    p[8].Direction = ParameterDirection.Input;
                    p[8].Value = MST;

                    //p[9] = new OracleParameter();
                    //p[9].ParameterName = "MA_CQT";
                    //p[9].DbType = DbType.String;
                    //p[9].Direction = ParameterDirection.Input;
                    //p[9].Value = MA_CQT;

                    DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    //}
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
                
                conn.Close();
                conn.Dispose();
            }

        }
    }
}
