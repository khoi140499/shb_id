﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG.MSG06000;
using System.Data.OracleClient;
using System.Data;
using Business.NTDT.NNTDT;
using System.Configuration;
using GDT.MSG.MSGHeader;
using VBOracleLib;
using System.Xml;
namespace ETAX_GDT
{

    class ProcessXuLyTBOnline
    {
        private string so_TB = "";
        private string strMa_Gdich = "";
        private string strTran_Type = "";
        private string strMSG_TEXT = "";
        private string strMSG_ID = "";
        public static string strSignReplace = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";

        public ProcessXuLyTBOnline()
        {


        }
        //public void GuiThongBao(string xmlChungTu, GDT.MSG.MSG03002.CHUNGTU_HDR objCTu_hdr, GDT.MSG.MSG03002.CHUNGTU_CTIET objCTu_dtl,
        //   GDT.MSG.MSG03002.HEADER objHeader, string so_ct, string KHCT, string trangthai)
        //{
        //    try
        //    {

        //        clsMQHelper mq = new clsMQHelper();
        //        so_TB = Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ").ToString().PadLeft(7, '0');
        //        string xmlput = "";//TaoXMLThongBao(ma_ctu, mahieu_ctu);
        //        string xmlthongbao = "";

        //        GDT.MSG.MSGHeader.HEADER header = new GDT.MSG.MSGHeader.HEADER();
        //        string xmlHeader = TaoXMLHeader(objHeader, ref header);
        //        //XmlNode DATA_Node = xmlDoc.SelectSingleNode("//DATA");
        //        //XmlNode HEADER_node = xmlDoc.SelectSingleNode("//HEADER");
        //        ////XmlNode TBAO_TKHOAN_NH_Node = xmlDoc.SelectSingleNode("//HEADER");
        //        ////XmlNode LDO_TCHOI_code = xmlDoc.SelectSingleNode("//LDO_TCHOI");
        //        //DATA_Node.InsertBefore(node, HEADER_node);
        //        xmlthongbao = "<DATA>";
        //        xmlthongbao += xmlHeader;
        //        xmlthongbao += TaoGiaoDich(xmlChungTu, objCTu_hdr, objCTu_dtl, objHeader, so_ct, KHCT, trangthai, ten_trangthai, objHead);
        //        xmlthongbao += "</DATA>";

        //        xmlthongbao = Utility.RomoveXML_Des(xmlthongbao);
        //        //vinhvv
        //        xmlthongbao = strSignReplace + "" + xmlthongbao;
        //        //vinhvv
        //        strMSG_ID = header.MSG_ID;
        //        strTran_Type = "MSG03002";
        //        strMSG_TEXT = "PutMQ=StartTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
        //        Utility.Log_Action(strTran_Type, strMSG_TEXT);
        //        string strReturn = "";
        //        strReturn = mq.PutMQMessage(xmlthongbao);
        //        // tuanda - 28/02/24
        //        Utility.Log_Action(strTran_Type, "MSG 06000" + xmlthongbao);

        //        strTran_Type = "MSG03002";
        //        strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "MQ Return: " + strReturn;
        //        Utility.Log_Action(strTran_Type, strMSG_TEXT);

        //        Utility.Update_CTU(so_ct, KHCT, strMa_Gdich, "1");

        //        //Ghi bao bang log msg gui di
        //        GDT.MSG.MSG msg_log = new GDT.MSG.MSG();
        //        msg_log.error_code = header.ERROR_CODE;
        //        msg_log.error_desc = header.ERROR_DESC;
        //        if (!strReturn.Equals(""))
        //        {
        //            msg_log.error_code = "02";
        //            if (strReturn.Length > 231)
        //            {
        //                strReturn = strReturn.Substring(0, 230);
        //            }
        //            msg_log.error_desc = strReturn;
        //        }
        //        msg_log.sender_code = header.SENDER_CODE;
        //        msg_log.sender_name = header.SENDER_NAME;
        //        msg_log.receiver_code = header.RECEIVER_CODE;
        //        msg_log.receiver_name = header.RECEIVER_NAME;
        //        msg_log.tran_code = header.TRAN_CODE;
        //        msg_log.msg_id = header.MSG_ID;
        //        msg_log.msg_refid = header.MSG_REFID;
        //        msg_log.id_link = header.ID_LINK;
        //        msg_log.send_date = header.SEND_DATE;
        //        msg_log.original_code = header.ORIGINAL_CODE;
        //        msg_log.original_name = header.ORIGINAL_NAME;
        //        msg_log.original_date = header.ORIGINAL_DATE;

        //        msg_log.spare1 = header.SPARE1;
        //        msg_log.spare2 = header.SPARE2;
        //        msg_log.spare3 = header.SPARE3;
        //        msg_log.trangthai = "00";
        //        msg_log.content_mess = xmlthongbao;

        //        string ma_tt = "";
        //        Utility.insertMSGOutLog(msg_log, ref ma_tt);

        //    }
        //    catch (Exception e)
        //    {
        //        strTran_Type = "MSG03002";
        //        strMSG_TEXT = "PutMQ=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "Exception=" + e.Message.ToString();
        //        Utility.Log_Action(strTran_Type, strMSG_TEXT);
        //    }
        //}
        public string TaoGiaoDich(string xmlChungTu, GDT.MSG.MSG03002.CHUNGTU_HDR objCTu_hdr, GDT.MSG.MSG03002.CHUNGTU_CTIET objCTu_dtl,
           GDT.MSG.MSG03002.HEADER objHeader, string so_ct, string KHCT, string trangthai, string ten_TT, GDT.MSG.MSG03002.HEADER objHead)
        {
            string xmloutput = "";
            string ma_GD = GDICH_NTDT_Constants.HEADER_SENDER_CODE + so_TB;
            string ngay_GD = DateTime.Now.ToString(GDICH_NTDT_Constants.FORMAT_NGAY_TB);
            string Ma_GD_ThamChieu = objHeader.ID_LINK;
            //string ten_TT = Utility.getTen_TT(trangthai);
            string xmlThongBao = TaoXMLThongBao_HDR(objCTu_hdr, objCTu_dtl, so_ct, KHCT, trangthai, ten_TT, "", objHead);
            xmloutput = "<BODY><ROW><GIAODICH>";
            xmloutput += "<MA_GDICH>" + ma_GD + "</MA_GDICH>";
            xmloutput += "<NGAY_GUI_GDICH>" + ngay_GD + "</NGAY_GUI_GDICH>";
            xmloutput += "<MA_GDICH_TCHIEU>" + Ma_GD_ThamChieu + "</MA_GDICH_TCHIEU>";
            if (trangthai == GDICH_NTDT_Constants.TT_CT_THANHCONG)
            {

                string xmlNoiDungCT = TaoCT(xmlChungTu, so_ct, KHCT);
                xmloutput += xmlNoiDungCT;
            }

            xmloutput += xmlThongBao;
            xmloutput += "</GIAODICH></ROW></BODY>";
            return xmloutput;

        }
        public string TaoCT(string xmlChungTu, string so_CT, string KHCT)
        {
            try
            {
                string output = "";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.LoadXml(xmlChungTu);
                XmlNode xmlnode1 = null;
                XmlNode xmlnode2 = null;
                xmlnode1 = xmlDoc.SelectSingleNode("/CHUNGTU/NDUNG_CTU_NH/MAHIEU_CTU");
                xmlnode1.InnerText = KHCT;
                xmlnode2 = xmlDoc.SelectSingleNode("/CHUNGTU/NDUNG_CTU_NH/SO_CTU");
                xmlnode2.InnerText = so_CT;
                XmlNode xmlnode3 = xmlDoc.SelectSingleNode("/CHUNGTU");
                output = "<CHUNGTU>" + xmlnode3.InnerXml + "</CHUNGTU>";
                //vinhvv sign
                string signatureType = Utility.GetValue_THAMSOHT("SIGN.TYPE.TCT");
                string xpathSign = "/CHUNGTU";
                string xpathNodeSign = "CHUNGTU";
                if (signatureType.Equals("1"))
                {
                    output = Signature.DigitalSignature.getSignNTDT(output, xpathSign, xpathNodeSign);
                }
                else
                {
                    output = Signature.SignatureProcess.SignXMLEextendTimeStamp(output, "NDUNG_CTU_NH", "CHUNGTU", true);
                }
                //vinhvv

                // bool veryfy= Signature.SignatureProcess.VerifyXmlFile(xmlDoc.InnerXml);
                //thêm phần chữ ký
                output = output.Replace(strSignReplace, "");
                return output;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string TaoXMLHeader(GDT.MSG.MSG03002.HEADER objHeader, ref GDT.MSG.MSGHeader.HEADER header)
        {
            string xmlOut = "";
            try
            {

                GDT.MSG.MSGHeader.HEADER xmlHeader = new GDT.MSG.MSGHeader.HEADER();

                //xmlHeader.HEADER = new GDT.MSG.MSGHeader.HEADER();
                xmlHeader.VERSION = GDICH_NTDT_Constants.HEADER_VERSION;
                xmlHeader.SENDER_CODE = GDICH_NTDT_Constants.HEADER_SENDER_CODE;
                xmlHeader.SENDER_NAME = GDICH_NTDT_Constants.HEADER_SENDER_NAME;
                xmlHeader.RECEIVER_CODE = GDICH_NTDT_Constants.HEADER_RECEIVER_CODE;
                xmlHeader.RECEIVER_NAME = GDICH_NTDT_Constants.HEADER_RECEIVER_NAME;
                xmlHeader.TRAN_CODE = GDICH_NTDT_Constants.ESB_TYPE_MESS_TBAO_NH_TTXL;
                xmlHeader.MSG_ID = GDICH_NTDT_Constants.HEADER_SENDER_CODE + so_TB;
                strMa_Gdich = xmlHeader.MSG_ID;
                xmlHeader.MSG_REFID = objHeader.MSG_ID;
                xmlHeader.ID_LINK = objHeader.ID_LINK;//objNNTDT.MA_GDICH
                xmlHeader.SEND_DATE = DateTime.Now.ToString(GDICH_NTDT_Constants.FORMAT_NGAY_GD_HEADER);
                xmlHeader.ORIGINAL_CODE = objHeader.ORIGINAL_CODE;
                xmlHeader.ORIGINAL_NAME = objHeader.ORIGINAL_NAME;
                xmlHeader.ORIGINAL_DATE = objHeader.ORIGINAL_DATE;
                xmlHeader.ERROR_CODE = "";
                xmlHeader.ERROR_DESC = "";
                xmlHeader.SPARE1 = "";
                xmlHeader.SPARE2 = "";
                xmlHeader.SPARE3 = "";

                xmlOut = xmlHeader.MSGtoXML(xmlHeader);
                xmlOut = Utility.RomoveXML_Des(xmlOut);

                header = xmlHeader;

                return xmlOut;

            }
            catch (Exception ex)
            {

            }
            return xmlOut;

        }

        public string TaoXMLThongBao_HDR(GDT.MSG.MSG03002.CHUNGTU_HDR objCtu_hdr, GDT.MSG.MSG03002.CHUNGTU_CTIET objCtu_ctiet, string SO_CT, string KH_CT, string str_TrangThai, string ten_TT, string SoTienPhi, GDT.MSG.MSG03002.HEADER obj )
        {
            try
            {
                string xmlOut = "";
                GDT.MSG.MSG06000.THONGBAO objThongBao = new GDT.MSG.MSG06000.THONGBAO();
                objThongBao.NDUNG_TBAO = new GDT.MSG.MSG06000.NDUNG_TBAO();
                objThongBao.NDUNG_TBAO.THONGBAO_HDR = new GDT.MSG.MSG06000.THONGBAO_HDR();
                objThongBao.NDUNG_TBAO.THONGBAO_CTIET = new GDT.MSG.MSG06000.THONGBAO_CTIET();
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.PBAN_TLIEU_XML = GDICH_NTDT_Constants.HEADER_PHIENBAN_XML_TT84;
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.MA_THONGBAO = "01";
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.MAU_THONGBAO = "05/TB-NHTM";
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.TEN_THONGBAO = "V/v Xác nhận trạng thái giao dịch nộp thuế điện tử";
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.SO_THONGBAO = GDICH_NTDT_Constants.HEADER_SENDER_CODE + so_TB;
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.NGAY_THONGBAO = DateTime.Now.ToString(GDICH_NTDT_Constants.FORMAT_NGAY_TB);
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.MST_NNOP = objCtu_hdr.MST_NNOP;
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.TEN_NNOP = objCtu_hdr.TEN_NNOP;
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.MA_CQT = objCtu_hdr.MA_CQT;
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.MST_NTHAY = objCtu_hdr.MST_NTHAY;
                objThongBao.NDUNG_TBAO.THONGBAO_HDR.TEN_NTHAY = objCtu_hdr.TEN_NTHAY;

                int tong_so_mon = objCtu_ctiet.ROW_CTIET.Count;

                List<GDT.MSG.MSG06000.ROW_CTIET_TB> listItems = new List<GDT.MSG.MSG06000.ROW_CTIET_TB>();
                {
                    GDT.MSG.MSG06000.ROW_CTIET_TB item = new GDT.MSG.MSG06000.ROW_CTIET_TB();
                    item.ID_CHUNGTU = objCtu_hdr.ID_CTU;
                    item.SO_GNT = objCtu_hdr.SO_GNT;
                    item.SHIEU_KHOBAC_NOP = objCtu_hdr.MA_HIEU_KBAC;
                    item.TEN_KHOBAC_NOP = objCtu_hdr.TEN_KBAC;
                    item.STK_KHOBAC_NOP = objCtu_hdr.STK_THU;
                    item.MA_NGHANG_NOP = objCtu_hdr.MA_NHANG_NOP;
                    item.STK_NGHANG_NOP = objCtu_hdr.STK_NHANG_NOP;
                    item.TONGSOMON = tong_so_mon.ToString();
                    item.TONGTIENNOP = objCtu_hdr.TONG_TIEN;
                    item.PHI = SoTienPhi;

                    // tuanda - 28/02/24


                    item.MA_TTHAI = str_TrangThai;
                    item.TEN_TTHAI = ten_TT;

                    if (str_TrangThai == GDICH_NTDT_Constants.TT_CT_THANHCONG)
                    {
                        item.MAHIEU_CTU = KH_CT;
                        item.SO_CTU = SO_CT;
                        item.NGAY_GUI = DateTime.Parse(obj.SEND_DATE).ToString("dd/MM/yyyy HH:mm:ss");
                        item.NGAY_NTHUE = DateTime.Now.ToString(GDICH_NTDT_Constants.FORMAT_NGAY_TB);

                    }
                    else
                    {
                        item.MAHIEU_CTU = "";
                        item.SO_CTU = "";
                        item.NGAY_GUI = "";
                        item.NGAY_NTHUE = "";
                    }
                    item.MA_NGUYENTE = objCtu_hdr.MA_NGUYENTE;
                    item.MA_THAMCHIEU = objCtu_hdr.MA_THAMCHIEU;



                    listItems.Add(item);
                }
                objThongBao.NDUNG_TBAO.THONGBAO_CTIET.ROW_CTIET = listItems;
                //objThongBao.Signature = "Chữ ký NHTM";
                // xmlHeader.GIAODICH.Signature = "Chữ ký xác thực NHTM vao ND truyền nhận (nếu cần)";
                xmlOut = objThongBao.MSGtoXML(objThongBao);
                xmlOut = Utility.RomoveXML_Des(xmlOut);
                //vinhvv sign
                string signatureType = Utility.GetValue_THAMSOHT("SIGN.TYPE.TCT");
                string xpathSign = "/THONGBAO";
                string xpathNodeSign = "THONGBAO";
                if (signatureType.Equals("1"))
                {
                    xmlOut = Signature.DigitalSignature.getSignNTDT(xmlOut, xpathSign, xpathNodeSign);
                }
                else
                {
                    xmlOut = Signature.SignatureProcess.SignXMLEextendTimeStamp(xmlOut, "NDUNG_TBAO", "THONGBAO", true);
                }
                //vinhvv
                //
                xmlOut = xmlOut.Replace(strSignReplace, "");
                return xmlOut;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        // tuanda - 01/02/24
        public void GuiThongBao(string xmlChungTu, GDT.MSG.MSG03002.CHUNGTU_HDR objCTu_hdr, GDT.MSG.MSG03002.CHUNGTU_CTIET objCTu_dtl,
           GDT.MSG.MSG03002.HEADER objHeader, string so_ct, string KHCT, string trangthai, string ten_trangthai, string ngay_ctu)
        {
            try
            {

                clsMQHelper mq = new clsMQHelper();
                so_TB = Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ").ToString().PadLeft(7, '0');
                string xmlput = "";//TaoXMLThongBao(ma_ctu, mahieu_ctu);
                string xmlthongbao = "";

                GDT.MSG.MSGHeader.HEADER header = new GDT.MSG.MSGHeader.HEADER();
                string xmlHeader = TaoXMLHeader(objHeader, ref header);
                //XmlNode DATA_Node = xmlDoc.SelectSingleNode("//DATA");
                //XmlNode HEADER_node = xmlDoc.SelectSingleNode("//HEADER");
                ////XmlNode TBAO_TKHOAN_NH_Node = xmlDoc.SelectSingleNode("//HEADER");
                ////XmlNode LDO_TCHOI_code = xmlDoc.SelectSingleNode("//LDO_TCHOI");
                //DATA_Node.InsertBefore(node, HEADER_node);
                xmlthongbao = "<DATA>";
                xmlthongbao += xmlHeader;
                xmlthongbao += TaoGiaoDich(xmlChungTu, objCTu_hdr, objCTu_dtl, objHeader, so_ct, KHCT, trangthai, ten_trangthai, ngay_ctu);
                xmlthongbao += "</DATA>";

                xmlthongbao = Utility.RomoveXML_Des(xmlthongbao);
                //vinhvv
                xmlthongbao = strSignReplace + "" + xmlthongbao;
                //vinhvv
                strMSG_ID = header.MSG_ID;
                strTran_Type = "MSG03002";
                strMSG_TEXT = "PutMQ=StartTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
                string strReturn = "";
                strReturn = mq.PutMQMessage(xmlthongbao);

                strTran_Type = "MSG03002";
                strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "MQ Return: " + strReturn;
                Utility.Log_Action(strTran_Type, strMSG_TEXT);

                Utility.Update_CTU(so_ct, KHCT, strMa_Gdich, "1");

                //Ghi bao bang log msg gui di
                GDT.MSG.MSG msg_log = new GDT.MSG.MSG();
                msg_log.error_code = header.ERROR_CODE;
                msg_log.error_desc = header.ERROR_DESC;
                if (!strReturn.Equals(""))
                {
                    msg_log.error_code = "02";
                    if (strReturn.Length > 231)
                    {
                        strReturn = strReturn.Substring(0, 230);
                    }
                    msg_log.error_desc = strReturn;
                }
                msg_log.sender_code = header.SENDER_CODE;
                msg_log.sender_name = header.SENDER_NAME;
                msg_log.receiver_code = header.RECEIVER_CODE;
                msg_log.receiver_name = header.RECEIVER_NAME;
                msg_log.tran_code = header.TRAN_CODE;
                msg_log.msg_id = header.MSG_ID;
                msg_log.msg_refid = header.MSG_REFID;
                msg_log.id_link = header.ID_LINK;
                msg_log.send_date = header.SEND_DATE;
                msg_log.original_code = header.ORIGINAL_CODE;
                msg_log.original_name = header.ORIGINAL_NAME;
                msg_log.original_date = header.ORIGINAL_DATE;

                msg_log.spare1 = header.SPARE1;
                msg_log.spare2 = header.SPARE2;
                msg_log.spare3 = header.SPARE3;
                msg_log.trangthai = "00";
                msg_log.content_mess = xmlthongbao;

                string ma_tt = "";
                Utility.insertMSGOutLog(msg_log, ref ma_tt);

            }
            catch (Exception e)
            {
                strTran_Type = "MSG03002";
                strMSG_TEXT = "PutMQ=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "Exception=" + e.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            }
        }

        public string TaoGiaoDich(string xmlChungTu, GDT.MSG.MSG03002.CHUNGTU_HDR objCTu_hdr, GDT.MSG.MSG03002.CHUNGTU_CTIET objCTu_dtl,
           GDT.MSG.MSG03002.HEADER objHeader, string so_ct, string KHCT, string trangthai, string ten_TT, string ngay_ctu)
        {
            string xmloutput = "";
            string ma_GD = GDICH_NTDT_Constants.HEADER_SENDER_CODE + so_TB;
            string ngay_GD = DateTime.Now.ToString(GDICH_NTDT_Constants.FORMAT_NGAY_TB);
            string Ma_GD_ThamChieu = objHeader.ID_LINK;
            //string ten_TT = Utility.getTen_TT(trangthai);
            string xmlThongBao = TaoXMLThongBao_HDR(objCTu_hdr, objCTu_dtl, so_ct, KHCT, trangthai, ten_TT, "", objHeader);
            xmloutput = "<BODY><ROW><GIAODICH>";
            xmloutput += "<MA_GDICH>" + ma_GD + "</MA_GDICH>";
            xmloutput += "<NGAY_GUI_GDICH>" + ngay_GD + "</NGAY_GUI_GDICH>";
            xmloutput += "<MA_GDICH_TCHIEU>" + Ma_GD_ThamChieu + "</MA_GDICH_TCHIEU>";
            if (trangthai == GDICH_NTDT_Constants.TT_CT_THANHCONG)
            {

                string xmlNoiDungCT = TaoCT(xmlChungTu, so_ct, KHCT, ngay_ctu);
                xmloutput += xmlNoiDungCT;
            }

            xmloutput += xmlThongBao;
            xmloutput += "</GIAODICH></ROW></BODY>";
            return xmloutput;

        }

        public string TaoCT(string xmlChungTu, string so_CT, string KHCT, string ngay_ctu)
        {
            try
            {
                string output = "";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.LoadXml(xmlChungTu);
                XmlNode xmlnode1 = null;
                XmlNode xmlnode2 = null;

                // tuanda - 01/02/24
                XmlNode xmlnode4 = null;

                xmlnode1 = xmlDoc.SelectSingleNode("/CHUNGTU/NDUNG_CTU_NH/MAHIEU_CTU");
                xmlnode1.InnerText = KHCT;
                xmlnode2 = xmlDoc.SelectSingleNode("/CHUNGTU/NDUNG_CTU_NH/SO_CTU");
                xmlnode2.InnerText = so_CT;

                // tuanda - 01/02/24
                xmlnode4 = xmlDoc.SelectSingleNode("/CHUNGTU/NDUNG_CTU_NH/NGAY_CTU");
                xmlnode4.InnerText = DateTime.Now.ToString(GDICH_NTDT_Constants.FORMAT_NGAY_TB);

                XmlNode xmlnode3 = xmlDoc.SelectSingleNode("/CHUNGTU");
                output = "<CHUNGTU>" + xmlnode3.InnerXml + "</CHUNGTU>";
                //vinhvv sign
                string signatureType = Utility.GetValue_THAMSOHT("SIGN.TYPE.TCT");
                string xpathSign = "/CHUNGTU";
                string xpathNodeSign = "CHUNGTU";
                if (signatureType.Equals("1"))
                {
                    output = Signature.DigitalSignature.getSignNTDT(output, xpathSign, xpathNodeSign);
                }
                else
                {
                    output = Signature.SignatureProcess.SignXMLEextendTimeStamp(output, "NDUNG_CTU_NH", "CHUNGTU", true);
                }
                //vinhvv

                // bool veryfy= Signature.SignatureProcess.VerifyXmlFile(xmlDoc.InnerXml);
                //thêm phần chữ ký
                output = output.Replace(strSignReplace, "");
                return output;
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        //private void UpDateTTGui(string strTrang_Thai, string So_CT, string id_ctu)
        //{
        //    OracleConnection conn = null;
        //    IDbTransaction transCT = null;
        //    try
        //    {

        //        conn = Utility.GetConnection();
        //        transCT = conn.BeginTransaction();
        //        //Tao cau lenh Insert
        //        StringBuilder sbSqlUpDate = new StringBuilder("");
        //        sbSqlUpDate.Append("update tcs_ctu_ntdt_hdr set tthai_ctu = :TRANG_THAI,ten_tthai_ctu=:TEN_TRANGTHAI,ngay_ht=:NGAY_HT,so_ref_no=:so_ref_no   where so_ctu= :so_CT and id_ctu =:id_ctu ");
        //        IDbDataParameter[] p = null;
        //        p = new IDbDataParameter[6];
        //        p[0] = new OracleParameter();
        //        p[0].ParameterName = "TRANG_THAI";
        //        p[0].DbType = DbType.String;
        //        p[0].Direction = ParameterDirection.Input;
        //        p[0].Value = strTrang_Thai;

        //        p[1] = new OracleParameter();
        //        p[1].ParameterName = "TEN_TRANGTHAI";
        //        p[1].DbType = DbType.String;
        //        p[1].Direction = ParameterDirection.Input;
        //        p[1].Value = strTen_Trang_Thai;

        //        p[2] = new OracleParameter();
        //        p[2].ParameterName = "NGAY_HT";
        //        p[2].DbType = DbType.DateTime;
        //        p[2].Direction = ParameterDirection.Input;
        //        p[2].Value = DateTime.Now;

        //        p[3] = new OracleParameter();
        //        p[3].ParameterName = "so_ref_no";
        //        p[3].DbType = DbType.String;
        //        p[3].Direction = ParameterDirection.Input;
        //        p[3].Value = so_ref_no;

        //        p[4] = new OracleParameter();
        //        p[4].ParameterName = "so_CT";
        //        p[4].DbType = DbType.String;
        //        p[4].Direction = ParameterDirection.Input;
        //        p[4].Value = So_CT;

        //        p[5] = new OracleParameter();
        //        p[5].ParameterName = "id_ctu";
        //        p[5].DbType = DbType.String;
        //        p[5].Direction = ParameterDirection.Input;
        //        p[5].Value = id_ctu;
        //        Utility.ExecuteNonQuery(sbSqlUpDate.ToString(), CommandType.Text, p, transCT);
        //        transCT.Commit();
        //    }
        //    catch
        //    {
        //        transCT.Rollback();
        //        conn.Close();
        //    }
        //}


    }
}
