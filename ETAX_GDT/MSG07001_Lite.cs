﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace GDT.MSG.MSG07001_Lite
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG07001_Lite
    {
        public MSG07001_Lite()
        {
        }
        [XmlElement("HEADER")]
        public HEADER HEADER;
        [XmlElement("BODY")]
        public BODY BODY;
        

        public string MSGtoXML(MSG07001_Lite p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG07001_Lite));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG07001_Lite MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSG07001_Lite.MSG07001_Lite));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);
            
            XmlReader reader = XmlReader.Create(new StringReader(p_XML));
            
            GDT.MSG.MSG07001_Lite.MSG07001_Lite LoadedObjTmp = (GDT.MSG.MSG07001_Lite.MSG07001_Lite)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }
    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW ROW;
    }

    public class ROW
    {
        public ROW()
        { }
        [XmlElement("GIAODICH_DC")]
        public GIAODICH_DC GIAODICH_DC;
    }

    public class GIAODICH_DC
    {
        public GIAODICH_DC()
        { }
        [XmlElementAttribute(Order = 1)]
        public string PBAN_TLIEU_XML_DC { get; set; }
        [XmlElementAttribute(Order = 2)]
        public string MA_GDICH_DC { get; set; }
        [XmlElementAttribute(Order = 3)]
        public string NGAY_GDICH_DC { get; set; }
        [XmlElementAttribute(Order = 4)]
        public string MA_NHANG_DC { get; set; }
        [XmlElementAttribute(Order = 5)]
        public string NGAY_DC { get; set; }
        [XmlElementAttribute(Order = 6)]
        public string TU_NGAY_DC { get; set; }
        [XmlElementAttribute(Order = 7)]
        public string DEN_NGAY_DC { get; set; }
        [XmlElementAttribute(Order = 8)]
        public string MA_GDICH_TCHIEU_DC { get; set; }
        [XmlElementAttribute(Order = 9)]
        public string NDUNG_DC;
        //public string Signature { get; set; }
    }

}
