﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace GDT.MSG.MSG07001_ND_DC
{
    [Serializable]
    //[XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    [XmlRoot("NDUNG_DC")]
    public class MSG07001_ND_DC
    {
        public MSG07001_ND_DC()
        {
        }
        //[XmlElement("NDUNG_DC")]
        //public NDUNG_DC NDUNG_DC;

        [XmlElement(ElementName = "BAOCAO", Order = 1)]
        public BAOCAO01 BAOCAO01;

        [XmlElement(ElementName = "BAOCAO", Order = 2)]
        public BAOCAO02 BAOCAO02;

        [XmlElement(ElementName = "BAOCAO", Order = 3)]
        public BAOCAO03 BAOCAO03;

        [XmlElement(ElementName = "BAOCAO", Order = 4)]
        public BAOCAO04 BAOCAO04;

        [XmlElement(ElementName = "BAOCAO", Order = 5)]
        public BAOCAO05 BAOCAO05;


        public string MSGtoXML(MSG07001_ND_DC p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG07001_ND_DC));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG07001_ND_DC MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSG07001_ND_DC.MSG07001_ND_DC));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);
            
            XmlReader reader = XmlReader.Create(new StringReader(p_XML));
            
            GDT.MSG.MSG07001_ND_DC.MSG07001_ND_DC LoadedObjTmp = (GDT.MSG.MSG07001_ND_DC.MSG07001_ND_DC)serializer.Deserialize(reader);
            return LoadedObjTmp;
        }
    }

    public class NDUNG_DC
    {
        public NDUNG_DC()
        { }


    }

    #region "Bao cao doi chieu 01"
    public class BAOCAO01
    {
        public BAOCAO01()
        { }


        [XmlElement(ElementName="BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR01 BAOCAO_HDR01;

        
        [XmlElement(ElementName="BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET01 BAOCAO_CTIET01;


    }

    public class BAOCAO_HDR01
    {
        public BAOCAO_HDR01()
        { }
        [XmlElement(ElementName = "PBAN_TLIEU_XML", Order = 1)]
        public string PBAN_TLIEU_XML { get; set; }
        [XmlElement(ElementName = "MA_BCAO", Order = 2)]
        public string MA_BCAO { get; set; }
        [XmlElement(ElementName = "TEN_BCAO", Order = 3)]
        public string TEN_BCAO { get; set; }
        [XmlElement(ElementName = "TGIAN_DCHIEU", Order = 4)]
        public string TGIAN_DCHIEU { get; set; }
        [XmlElement(ElementName = "MA_NHANG_DCHIEU", Order = 5)]
        public string MA_NHANG_DCHIEU { get; set; }
        [XmlElement(ElementName = "TEN_NHANG_DCHIEU", Order = 6)]
        public string TEN_NHANG_DCHIEU { get; set; }
        [XmlElement(ElementName = "SODIEN_TCONG", Order = 7)]
        public string SODIEN_TCONG { get; set; }
         [XmlElement(ElementName = "TONGTIEN_TCONG", Order = 8)]
        public string TONGTIEN_TCONG { get; set; }
        [XmlElement(ElementName = "TONGTIEN_TCONGS", Order = 9)]
         public TONGTIEN_TCONGS TONGTIEN_TCONGS;
        [XmlElement(ElementName = "SODIEN_HTRUOC", Order = 10)]
        public string SODIEN_HTRUOC { get; set; }
        [XmlElement(ElementName = "TONGTIEN_HTRUOC", Order = 11)]
        public string TONGTIEN_HTRUOC { get; set; }
        [XmlElement(ElementName = "TONGTIEN_HTRUOCS", Order = 12)]
        public TONGTIEN_HTRUOCS TONGTIEN_HTRUOCS;
        [XmlElement(ElementName = "SODIEN_HSAU", Order = 13)]
        public string SODIEN_HSAU { get; set; }
        [XmlElement(ElementName = "TONGTIEN_HSAU", Order = 14)]
        public string TONGTIEN_HSAU { get; set; }
        [XmlElement(ElementName = "TONGTIEN_HSAUS", Order = 15)]
        public TONGTIEN_HSAUS TONGTIEN_HSAUS;

    }
    public class TONGTIEN_TCONGS
    {
        public TONGTIEN_TCONGS() { }
        [XmlElement("ROW")]
        public List<TONGTIEN_TCONG_CTIETS> TONGTIEN_TCONG_CTIETS;
    }
    public class TONGTIEN_TCONG_CTIETS
    {
        public TONGTIEN_TCONG_CTIETS() { }
        public string TONGTIEN_TCONG_CTIET { get; set; }
        public string MA_NGUYENTE { get; set; }
    }
   
    public class TONGTIEN_HTRUOCS
    {
        [XmlElement("ROW")]
        public List<TONGTIEN_HTRUOC_CTIETS> TONGTIEN_HTRUOC_CTIETS;
    }
    public class TONGTIEN_HTRUOC_CTIETS
    {
        public TONGTIEN_HTRUOC_CTIETS() { }
        public string TONGTIEN_HTRUOC_CTIET { get; set; }
        public string MA_NGUYENTE { get; set; }
    }
    public class TONGTIEN_HSAUS
    {
        [XmlElement("ROW")]
        public List<TONGTIEN_HSAU_CTIETS> TONGTIEN_HSAU_CTIETS;
    }
    public class TONGTIEN_HSAU_CTIETS
    {
        public TONGTIEN_HSAU_CTIETS() { }
        public string TONGTIEN_HSAU_CTIET { get; set; }
        public string MA_NGUYENTE { get; set; }
    }
    public class BAOCAO_CTIET01
    {
        public BAOCAO_CTIET01()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET01> ROW_CTIET01;
    }

    public class ROW_CTIET01
    {
        public ROW_CTIET01()
        { }
        public string MA_THAMCHIEU { get; set; }
        public string SO_CTU { get; set; }
        public string MAHIEU_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string TEN_NNT { get; set; }
        public string MST { get; set; }
        public string MA_CQT { get; set; }
        public string MA_CQTHU { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string NGAY_NOP_THUE { get; set; }
        public string TGIAN_TCT { get; set; }
        public string TGIAN_NHTM { get; set; }
        public string SO_TIEN { get; set; }
        public string MA_NGUYENTE { get; set; }
        public string TTHAI { get; set; }
        public string DIEN_HSAU { get; set; }

        public string ID_KNOP { get; set; }
        public string MA_DBHC_THU { get; set; }
        public string TEN_DBHC_THU { get; set; }

    }
    #endregion
    
    #region "Bao cao doi chieu 02"
    public class BAOCAO02
    {
        public BAOCAO02()
        { }
        [XmlElement(ElementName="BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR02 BAOCAO_HDR02;
        [XmlElement(ElementName="BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET02 BAOCAO_CTIET02;

    }

    public class BAOCAO_HDR02
    {
        public BAOCAO_HDR02()
        { }
        [XmlElement(ElementName = "PBAN_TLIEU_XML", Order = 1)]
        public string PBAN_TLIEU_XML { get; set; }
        [XmlElement(ElementName = "MA_BCAO", Order = 2)]
        public string MA_BCAO { get; set; }
        [XmlElement(ElementName = "TEN_BCAO", Order = 3)]
        public string TEN_BCAO { get; set; }
        [XmlElement(ElementName = "TGIAN_DCHIEU", Order = 4)]
        public string TGIAN_DCHIEU { get; set; }
        [XmlElement(ElementName = "MA_NHANG_DCHIEU", Order = 5)]
        public string MA_NHANG_DCHIEU { get; set; }
        [XmlElement(ElementName = "TEN_NHANG_DCHIEU", Order = 6)]
        public string TEN_NHANG_DCHIEU { get; set; }
        [XmlElement(ElementName = "TONGDIEN_LECH", Order = 7)]
        public string TONGDIEN_LECH { get; set; }
        [XmlElement(ElementName = "TONGTIEN_LECH", Order = 8)]
        public string TONGTIEN_LECH { get; set; }
        [XmlElement(ElementName = "TONGTIEN_LECHS", Order = 9)]
        public TONGTIEN_LECHS TONGTIEN_LECHS;
        [XmlElement(ElementName = "TONGDIEN_TCTCO_NHTMKO", Order = 10)]
        public string TONGDIEN_TCTCO_NHTMKO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_TCTCO_NHTMKO", Order = 11)]
        public string TONGTIEN_TCTCO_NHTMKO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_TCTCO_NHTMKOS", Order = 12)]
        public TONGTIEN_TCTCO_NHTMKOS TONGTIEN_TCTCO_NHTMKOS;
        [XmlElement(ElementName = "TONGDIEN_NHTMCO_TCTKO", Order = 13)]
        public string TONGDIEN_NHTMCO_TCTKO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_NHTMCO_TCTKO", Order = 14)]
        public string TONGTIEN_NHTMCO_TCTKO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_NHTMCO_TCTKOS", Order = 15)]
        public TONGTIEN_NHTMCO_TCTKOS TONGTIEN_NHTMCO_TCTKOS;
        [XmlElement(ElementName = "TONGDIEN_LECH_TTHAI", Order = 16)]
        public string TONGDIEN_LECH_TTHAI { get; set; }
        [XmlElement(ElementName = "TONGTIEN_LECH_TTHAI", Order = 17)]
        public string TONGTIEN_LECH_TTHAI { get; set; }
        [XmlElement(ElementName = "TONGTIEN_LECH_TTHAIS", Order = 18)]
        public TONGTIEN_LECH_TTHAIS TONGTIEN_LECH_TTHAIS;


    }
    public class TONGTIEN_LECHS
    {
        public TONGTIEN_LECHS() { }
        [XmlElement(ElementName = "ROW")]
        public List<TONGTIEN_LECH_CTIETS> TONGTIEN_LECH_CTIETS;

    }
    public class TONGTIEN_LECH_CTIETS
    {
        public TONGTIEN_LECH_CTIETS() { }
        public string TONGTIEN_LECH_CTIET{ get; set; }
        public string MA_NGUYENTE { get; set; }

    }
    public class TONGTIEN_TCTCO_NHTMKOS
    {
        public TONGTIEN_TCTCO_NHTMKOS() { }
        [XmlElement(ElementName = "ROW")]
        public List<TONGTIEN_TCTCO_NHTMKO_CTIETS> TONGTIEN_TCTCO_NHTMKO_CTIETS;

    }
    public class TONGTIEN_TCTCO_NHTMKO_CTIETS
    {
        public TONGTIEN_TCTCO_NHTMKO_CTIETS() { }
        public string TONGTIEN_TCTCO_NHTMKO_CTIET { get; set; }
        public string MA_NGUYENTE { get; set; }

    }
    public class TONGTIEN_NHTMCO_TCTKOS
    {
        public TONGTIEN_NHTMCO_TCTKOS() { }
        [XmlElement(ElementName = "ROW")]
        public List<TONGTIEN_NHTMCO_TCTKO_CTIETS> TONGTIEN_NHTMCO_TCTKO_CTIETS;

    }
    public class TONGTIEN_NHTMCO_TCTKO_CTIETS
    {
        public TONGTIEN_NHTMCO_TCTKO_CTIETS() { }
        public string TONGTIEN_NHTMCO_TCTKO_CTIET { get; set; }
        public string MA_NGUYENTE { get; set; }

    }
    public class TONGTIEN_LECH_TTHAIS
    {
        public TONGTIEN_LECH_TTHAIS() { }
        [XmlElement(ElementName = "ROW")]
        public List<TONGTIEN_LECH_TTHAI_CTIETS> TONGTIEN_LECH_TTHAI_CTIETS;

    }
    public class TONGTIEN_LECH_TTHAI_CTIETS
    {
        public TONGTIEN_LECH_TTHAI_CTIETS() { }
        public string TONGTIEN_LECH_TTHAI_CTIET { get; set; }
        public string MA_NGUYENTE { get; set; }

    }
    public class BAOCAO_CTIET02
    {
        public BAOCAO_CTIET02()
        { }

        [XmlElement(ElementName = "NHTMCO_TCTKO", Order = 1)]
        public NHTMCO_TCTKO NHTMCO_TCTKO;

        [XmlElement(ElementName="TCTCO_NHTMKO", Order=2)]
        public TCTCO_NHTMKO TCTCO_NHTMKO;

        [XmlElement(ElementName="LECH_TTHAI", Order=3)]
        public LECH_TTHAI LECH_TTHAI;
            
    }

    public class TCTCO_NHTMKO
    {
        public TCTCO_NHTMKO()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET02> ROW_CTIET02;
    }

    public class NHTMCO_TCTKO
    {
        public NHTMCO_TCTKO()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET02> ROW_CTIET02;
    }

    public class LECH_TTHAI
    {
        public LECH_TTHAI()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET02> ROW_CTIET02;
    }

    public class ROW_CTIET02
    {
        public ROW_CTIET02()
        { }
        public string MA_THAMCHIEU { get; set; }
        public string SO_CTU { get; set; }
        public string MAHIEU_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string TEN_NNT { get; set; }
        public string MST { get; set; }
        public string MA_CQT { get; set; }
        public string MA_CQTHU { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string NGAY_NOP_THUE { get; set; }
        public string TGIAN_TCT { get; set; }
        public string TGIAN_NHTM { get; set; }
        public string SO_TIEN { get; set; }
        public string MA_NGUYENTE { get; set; }
        public string TTHAI_TCT { get; set; }
        public string TTHAI_NHTM { get; set; }

        public string ID_KNOP { get; set; }
        public string MA_DBHC_THU { get; set; }
        public string TEN_DBHC_THU { get; set; }
    }
    #endregion

    #region "Bao cao doi chieu 03"
    public class BAOCAO03
    {
        public BAOCAO03()
        { }
        [XmlElement(ElementName="BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR03 BAOCAO_HDR03;

        [XmlElement(ElementName="BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET03 BAOCAO_CTIET03;


    }

    public class BAOCAO_HDR03
    {
        public BAOCAO_HDR03()
        { }
        [XmlElement(ElementName = "PBAN_TLIEU_XML", Order = 1)]
        public string PBAN_TLIEU_XML { get; set; }
        [XmlElement(ElementName = "MA_BCAO", Order = 2)]
        public string MA_BCAO { get; set; }
        [XmlElement(ElementName = "TEN_BCAO", Order = 3)]
        public string TEN_BCAO { get; set; }
        [XmlElement(ElementName = "TGIAN_DCHIEU", Order = 4)]
        public string TGIAN_DCHIEU { get; set; }
        [XmlElement(ElementName = "MA_NHANG_DCHIEU", Order = 5)]
        public string MA_NHANG_DCHIEU { get; set; }
        [XmlElement(ElementName = "TEN_NHANG_DCHIEU", Order = 6)]
        public string TEN_NHANG_DCHIEU { get; set; }
        [XmlElement(ElementName = "TONGDIEN_TNHAN", Order = 7)]
        public string TONGDIEN_TNHAN { get; set; }
        [XmlElement(ElementName = "TONGTIEN_TNHAN", Order = 8)]
        public string TONGTIEN_TNHAN { get; set; }
        [XmlElement(ElementName = "TONGTIEN_TNHANS", Order = 9)]
        public TONGTIEN_TNHANS TONGTIEN_TNHANS;
        [XmlElement(ElementName = "TONGDIEN_TCTCO_NHTMCO", Order = 10)]
        public string TONGDIEN_TCTCO_NHTMCO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_TCTCO_NHTMCO", Order = 11)]
        public string TONGTIEN_TCTCO_NHTMCO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_TCTCO_NHTMCOS", Order = 12)]
        public TONGTIEN_TCTCO_NHTMCOS TONGTIEN_TCTCO_NHTMCOS;
        [XmlElement(ElementName = "TONGDIEN_TCTCO_NHTMKO", Order = 13)]
        public string TONGDIEN_TCTCO_NHTMKO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_TCTCO_NHTMKO", Order = 14)]
        public string TONGTIEN_TCTCO_NHTMKO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_TCTCO_NHTMKOS", Order = 15)]
        public TONGTIEN_TCTCO_NHTMKOS TONGTIEN_TCTCO_NHTMKOS;
        [XmlElement(ElementName = "TONGDIEN_NHTMCO_TCTKO", Order = 16)]
        public string TONGDIEN_NHTMCO_TCTKO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_NHTMCO_TCTKO", Order = 17)]
        public string TONGTIEN_NHTMCO_TCTKO { get; set; }
        [XmlElement(ElementName = "TONGTIEN_NHTMCO_TCTKOS", Order = 18)]
        public TONGTIEN_NHTMCO_TCTKOS TONGTIEN_NHTMCO_TCTKOS;
        [XmlElement(ElementName = "TONGDIEN_LECH_TTHAI", Order = 19)]
        public string TONGDIEN_LECH_TTHAI { get; set; }
        [XmlElement(ElementName = "TONGTIEN_LECH_TTHAI", Order = 20)]
        public string TONGTIEN_LECH_TTHAI { get; set; }
        [XmlElement(ElementName = "TONGTIEN_LECH_TTHAIS", Order = 21)]
        public TONGTIEN_LECH_TTHAIS TONGTIEN_LECH_TTHAIS;
    }
    public class TONGTIEN_TNHANS {
        public TONGTIEN_TNHANS() { }
        [XmlElement(ElementName = "ROW")]
        public List<TONGTIEN_TNHAN_CTIETS> TONGTIEN_TNHAN_CTIETS;
    }
    public class TONGTIEN_TNHAN_CTIETS
    {
         public TONGTIEN_TNHAN_CTIETS() { }
         public string TONGTIEN_TNHAN_CTIET { get; set; }
         public string MA_NGUYENTE { get; set; }
    }

    public class TONGTIEN_TCTCO_NHTMCOS
    {
        public TONGTIEN_TCTCO_NHTMCOS() { }
        [XmlElement(ElementName = "ROW")]
        public List<TONGTIEN_TCTCO_NHTMCO_CTIETS> TONGTIEN_TCTCO_NHTMCO_CTIETS;
    }
    public class TONGTIEN_TCTCO_NHTMCO_CTIETS
    {
        public TONGTIEN_TCTCO_NHTMCO_CTIETS() { }
        public string TONGTIEN_TCTCO_NHTMCO_CTIET { get; set; }
        public string MA_NGUYENTE { get; set; }
    }


    public class BAOCAO_CTIET03
    {
        public BAOCAO_CTIET03()
        { }
        [XmlElement(ElementName="TCTCO_NHTMCO", Order=1)]
        public TCTCO_NHTMCO TCTCO_NHTMCO;

        [XmlElement(ElementName = "LECH_TTHAI", Order=2)]
        public LECH_TTHAI LECH_TTHAI;

        [XmlElement(ElementName = "TCTCO_NHTMKO", Order=3)]
        public TCTCO_NHTMKO TCTCO_NHTMKO;

        [XmlElement(ElementName = "NHTMCO_TCTKO", Order=4)]
        public NHTMCO_TCTKO NHTMCO_TCTKO;

    }

    public class TCTCO_NHTMCO
    {
        public TCTCO_NHTMCO()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET02> ROW_CTIET02;
    }
    #endregion
   
    #region Bao cao doi chieu 04
    public class BAOCAO04
    {
        public BAOCAO04()
        { }
        [XmlElement(ElementName = "BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR04 BAOCAO_HDR04;

        [XmlElement(ElementName = "BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET04 BAOCAO_CTIET04;


    }

    public class BAOCAO_HDR04
    {
        public BAOCAO_HDR04()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string MA_BCAO { get; set; }
        public string TEN_BCAO { get; set; }
        public string TGIAN_DCHIEU { get; set; }
        public string MA_NHANG_DCHIEU { get; set; }
        public string TEN_NHANG_DCHIEU { get; set; }
    }

    public class BAOCAO_CTIET04
    {
        public BAOCAO_CTIET04()
        { }
        [XmlElement(ElementName = "DIEN_TCONG", Order=1)]
        public DIEN_TCONG DIEN_TCONG;

        [XmlElement(ElementName = "DIEN_KO_TCONG", Order=2)]
        public DIEN_KO_TCONG DIEN_KO_TCONG;

        [XmlElement(ElementName = "DIEN_TUCHOI", Order=3)]
        public DIEN_TUCHOI DIEN_TUCHOI;
    }

    public class DIEN_TCONG
    {
        public DIEN_TCONG()
        { }

        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET04> ROW_CTIET04;
    }

    public class DIEN_KO_TCONG
    {
        public DIEN_KO_TCONG()
        { }

        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET04> ROW_CTIET04;
    }

    public class DIEN_TUCHOI
    {
        public DIEN_TUCHOI()
        { }

        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET04> ROW_CTIET04;
    }

    public class ROW_CTIET04
    {
        public ROW_CTIET04()
        { }
        public string MA_THAMCHIEU { get; set; }
        public string SO_CTU { get; set; }
        public string MAHIEU_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string NGAY_NOP_THUE { get; set; }
        public string TEN_NNT { get; set; }
        public string MST { get; set; }
        public string MA_CQT { get; set; }
        public string MA_CQTHU { get; set; }
        public string MA_KBNN { get; set; }
        public string MA_CHUONG { get; set; }
        public string MA_TMUC { get; set; }
        public string SO_TIEN { get; set; }
        public string MA_NGUYENTE { get; set; }
        public string TTHAI { get; set; }

        public string SO_TK_TB_QD { get; set; }
        public string ID_KNOP { get; set; }
        public string MA_DBHC_THU { get; set; }
        public string TEN_DBHC_THU { get; set; }

    }
    #endregion

    #region "Bao cao doi chieu 05"
    public class BAOCAO05
    {
        public BAOCAO05()
        { }
        [XmlElement(ElementName = "BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR05 BAOCAO_HDR05;

        [XmlElement(ElementName = "BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET05 BAOCAO_CTIET05;


    }

    public class BAOCAO_HDR05
    {
        public BAOCAO_HDR05()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string MA_BCAO { get; set; }
        public string TEN_BCAO { get; set; }
        public string TGIAN_DCHIEU { get; set; }
        public string MA_NHANG_DCHIEU { get; set; }
        public string TEN_NHANG_DCHIEU { get; set; }
    }

    public class BAOCAO_CTIET05
    {
        public BAOCAO_CTIET05()
        { }

        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET05> ROW_CTIET05;

    }

    public class ROW_CTIET05
    {
        public ROW_CTIET05()
        { }
        public string MA_THAMCHIEU { get; set; }
        public string SO_CTU { get; set; }
        public string MAHIEU_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string NGAY_NOP_THUE { get; set; }
        public string TEN_NNT { get; set; }
        public string MST { get; set; }
        public string MA_CQT { get; set; }
        public string MA_CQTHU { get; set; }
        public string MA_KBNN { get; set; }
        public string TCT_MA_CHUONG { get; set; }
        public string TCT_MA_TMUC { get; set; }
        public string TCT_SO_TIEN { get; set; }
        public string TCT_MA_NGUYENTE { get; set; }
        public string TCT_TTHAI { get; set; }
        public string TCT_ID_KNOP { get; set; }
        public string TCT_MA_DBHC_THU { get; set; }
        public string TCT_SO_TK { get; set; }
        public string NHTM_MA_CHUONG { get; set; }
        public string NHTM_MA_TMUC { get; set; }
        public string NHTM_SO_TIEN { get; set; }
        public string NHTM_MA_NGUYENTE { get; set; }
        public string NHTM_TTHAI { get; set; }
        public string NHTM_ID_KNOP { get; set; }
        public string NHTM_MA_DBHC_THU { get; set; }
        public string NHTM_SO_TK { get; set; }

    }
    #endregion
       
}
