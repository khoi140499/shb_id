﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace GDT.MSG.MSG06006
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG06006
    {
        public MSG06006()
        {
        }
        [XmlElement("HEADER")]
        public HEADER HEADER;
        [XmlElement("BODY")]
        public BODY BODY;
        //[XmlElement("SECURITY")]
        //public SECURITY SECURITY;

        public string MSGtoXML(MSG06006 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG06006));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG06006 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSG06006.MSG06006));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            GDT.MSG.MSG06006.MSG06006 LoadedObjTmp = (GDT.MSG.MSG06006.MSG06006)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    
    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }
    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW ROW;
    }

    public class ROW
    {
        public ROW()
        { }
        [XmlElement("GIAODICH")]
        public GIAODICH GIAODICH;
    }

    public class GIAODICH
    {
        public GIAODICH()
        { }
        public string MA_GDICH { get; set; }
        public string NGAY_GUI_GDICH { get; set; }
        public string MA_GDICH_TCHIEU { get; set; }
        [XmlElement("DKY_NTHUE_NH")]
        public DKY_NTHUE_NH DKY_NTHUE_NH;
    }

    public class DKY_NTHUE_NH
    {
        public DKY_NTHUE_NH()
        { }
        [XmlElement("DKY_NTHUE")]
        public DKY_NTHUE DKY_NTHUE;
        public List<string> Signature { get; set; }
    }

    public class DKY_NTHUE
    {
        public DKY_NTHUE()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string MST { get; set; }
        public string TEN_NNT { get; set; }
        public string DIACHI_NNT { get; set; }
        public string MA_CQT { get; set; }
        public string EMAIL_NNT { get; set; }
        public string SDT_NNT { get; set; }
        public string TEN_LHE_NTHUE { get; set; }
        public string SERIAL_CERT_NTHUE { get; set; }
        public string SUBJECT_CERT_NTHUE { get; set; }
        public string ISSUER_CERT_NTHUE { get; set; }
        public string MA_NHANG { get; set; }
        public string TEN_NHANG { get; set; }
        public string VAN_ID { get; set; }
        public string TEN_TVAN { get; set; }
        public string NGAY_GUI { get; set; }
        public string STK_NGHANG { get; set; }
    }

    //public class SECURITY
    //{
    //    public SECURITY() 
    //    { 
    //    }
    //    public string SIGNATURE { get; set; }
    //}
}
