﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG.MSG06000;
using System.Data.OracleClient;
using System.Data;
using Business.NTDT.NNTDT;
using System.Configuration;
using GDT.MSG.MSGHeader;
using VBOracleLib;
using System.Xml;
using GDT.MSG.MSG03002;
namespace ETAX_GDT
{

    public class ProcessXuLyTB
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string strTran_Type = "";
        private string strMSG_TEXT = "";
        private string strMSG_ID = "";
        private static String strXMLdefine = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>";
        private static String strXMLdefin2 = "<DATA xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
        public ProcessXuLyTB()
        {


        }
        string strMA_GIAO = "";
        public string GuiThongBao()
        {
            DataSet dsCT;
            string strSQL = "SELECT SO_CTU,MAHIEU_CTU FROM tcs_ctu_ntdt_hdr WHERE tt_chuyen=0 and to_char(ngay_nhang,'dd/MM/rrrr')='" + DateTime.Now.ToString("dd/MM/yyyy") + "'";
            string ma_ctu, mahieu_ctu;
            string str_return_gdich = DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss");
            try
            {
                clsMQHelper mq = new clsMQHelper();
                dsCT = Utility.ExecuteReturnDataSet(strSQL, CommandType.Text);
                foreach (DataRow drhdr in dsCT.Tables[0].Rows)
                {
                    ma_ctu = drhdr["SO_CTU"].ToString();
                    mahieu_ctu = drhdr["MAHIEU_CTU"].ToString();
                    MSGHeader header = new MSGHeader();
                    string xmlput = TaoXMLThongBao(ma_ctu, mahieu_ctu, ref header);

                    xmlput = Utility.RomoveXML_Des(xmlput);

                    strMSG_ID = strMA_GIAO;
                    strTran_Type = "MSG06000";
                    strMSG_TEXT = "PutMQ=StartTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);


                    mq.PutMQMessage(xmlput);


                    strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);

                    Utility.Update_CTU(ma_ctu, mahieu_ctu, strMA_GIAO, "1");
                    str_return_gdich += "[SO_CTU:" + ma_ctu + ", MAHIEU_CTU:" + mahieu_ctu + "];";

                    //Ghi bao bang log msg gui di
                    GDT.MSG.MSG msg_log = new GDT.MSG.MSG();

                    msg_log.sender_code = header.HEADER.SENDER_CODE;
                    msg_log.sender_name = header.HEADER.SENDER_NAME;
                    msg_log.receiver_code = header.HEADER.RECEIVER_CODE;
                    msg_log.receiver_name = header.HEADER.RECEIVER_NAME;
                    msg_log.tran_code = header.HEADER.TRAN_CODE;
                    msg_log.msg_id = header.HEADER.MSG_ID;
                    msg_log.msg_refid = header.HEADER.MSG_REFID;
                    msg_log.id_link = header.HEADER.ID_LINK;
                    msg_log.send_date = header.HEADER.SEND_DATE;
                    msg_log.original_code = header.HEADER.ORIGINAL_CODE;
                    msg_log.original_name = header.HEADER.ORIGINAL_NAME;
                    msg_log.original_date = header.HEADER.ORIGINAL_DATE;
                    msg_log.error_code = header.HEADER.ERROR_CODE;
                    msg_log.error_desc = header.HEADER.ERROR_DESC;
                    msg_log.spare1 = header.HEADER.SPARE1;
                    msg_log.spare2 = header.HEADER.SPARE2;
                    msg_log.spare3 = header.HEADER.SPARE3;
                    msg_log.trangthai = "00";
                    msg_log.content_mess = xmlput;

                    string ma_tt = "";
                    Utility.insertMSGOutLog(msg_log, ref ma_tt);


                }
            }
            catch (Exception e)
            {
                return "Loi khi gui thong bao NTDT sang TCT.";
                strMSG_TEXT = "PutMQ=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "Exception=Loi khi gui thong bao NTDT sang TCT:" + e.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
                throw e;
            }
            return str_return_gdich;
        }

        public string GuiThongBaoTungCT(string soct, ref string err_code, ref string err_msg)
        {
            err_code = "0";
            err_msg = "";
            DataSet dsCT;
            string strSQL = "";
            strSQL = "SELECT a.NGAY_CTU, a.SO_CTU,a.MAHIEU_CTU,a.filecontent,a.tthai_ctu,a.ten_tthai_ctu,b.content_mess FROM tcs_ctu_ntdt_hdr a,tcs_dm_msg_ntdt b WHERE so_ctu ='" + soct + "' and a.ma_gd=b.id_link and b.tran_code='03002' ";

            string str_return_gdich = DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss");
            try
            {
                clsMQHelper mq = new clsMQHelper();
                dsCT = Utility.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (dsCT != null)
                {

                    foreach (DataRow drhdr in dsCT.Tables[0].Rows)
                    {
                        // tuanda - 01/02/24
                        string ngay_ctu = drhdr["NGAY_CTU"].ToString();

                        string so_ct = drhdr["SO_CTU"].ToString();
                        string mahieu_ct = drhdr["MAHIEU_CTU"].ToString();
                        string xmlIn = drhdr["content_mess"].ToString();
                        string trang_thaiGD = drhdr["tthai_ctu"].ToString();
                        string ten_tthai = drhdr["ten_tthai_ctu"].ToString();
                        string noidungCT = drhdr["filecontent"].ToString();
                        xmlIn = Utility.RomoveXML_Des(xmlIn);
                        xmlIn = xmlIn.Replace("<DATA>", strXMLdefin2);

                        if (xmlIn.Length > 0)
                        {
                            MSG03002 MSG03002 = new MSG03002();
                            MSG03002 objTemp = null;

                            try
                            {
                                objTemp = MSG03002.MSGToObject(xmlIn);
                                GDT.MSG.MSG03002.HEADER objHEADER = objTemp.HEADER;
                                GDT.MSG.MSG03002.CHUNGTU objChungTu = objTemp.BODY.ROW.GIAODICH.CHUNGTU;
                                GDT.MSG.MSG03002.CHUNGTU_HDR objCTu_hdr = objTemp.BODY.ROW.GIAODICH.CHUNGTU.NDUNG_CTU_NH.NDUNG_CTU.CHUNGTU_HDR;
                                GDT.MSG.MSG03002.CHUNGTU_CTIET objCTu_dtl = objTemp.BODY.ROW.GIAODICH.CHUNGTU.NDUNG_CTU_NH.NDUNG_CTU.CHUNGTU_CTIET;

                                //sonmt: gui thong bao 999
                                MSGHeader msgheader = new MSGHeader();
                                msgheader.HEADER = new GDT.MSG.MSGHeader.HEADER();

                                msgheader.HEADER.ERROR_CODE = objTemp.HEADER.ERROR_CODE;
                                msgheader.HEADER.ERROR_DESC = objTemp.HEADER.ERROR_DESC;
                                msgheader.HEADER.ID_LINK = objTemp.HEADER.ID_LINK;
                                msgheader.HEADER.MSG_ID = objTemp.HEADER.MSG_ID;
                                msgheader.HEADER.MSG_REFID = objTemp.HEADER.MSG_REFID;
                                msgheader.HEADER.ORIGINAL_CODE = objTemp.HEADER.ORIGINAL_CODE;
                                msgheader.HEADER.ORIGINAL_DATE = objTemp.HEADER.ORIGINAL_DATE;
                                msgheader.HEADER.ORIGINAL_NAME = objTemp.HEADER.ORIGINAL_NAME;
                                msgheader.HEADER.RECEIVER_CODE = objTemp.HEADER.RECEIVER_CODE;
                                msgheader.HEADER.RECEIVER_NAME = objTemp.HEADER.RECEIVER_NAME;
                                msgheader.HEADER.SEND_DATE = objTemp.HEADER.SEND_DATE;
                                msgheader.HEADER.SENDER_CODE = objTemp.HEADER.SENDER_CODE;
                                msgheader.HEADER.SENDER_NAME = objTemp.HEADER.SENDER_NAME;
                                msgheader.HEADER.SPARE1 = objTemp.HEADER.SPARE1;
                                msgheader.HEADER.SPARE2 = objTemp.HEADER.SPARE2;
                                msgheader.HEADER.SPARE3 = objTemp.HEADER.SPARE3;
                                msgheader.HEADER.TRAN_CODE = objTemp.HEADER.TRAN_CODE;
                                msgheader.HEADER.VERSION = objTemp.HEADER.VERSION;

                                Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "01", "Thành công");

                                //end sonmt

                                ProcessXuLyTBOnline xulyTB = new ProcessXuLyTBOnline();
                                //xulyTB.GuiThongBao(noidungCT, objCTu_hdr, objCTu_dtl, objHEADER, so_ct, mahieu_ct, trang_thaiGD, ten_tthai);

                                // tuanda - 01/02/24
                                xulyTB.GuiThongBao(noidungCT, objCTu_hdr, objCTu_dtl, objHEADER, so_ct, mahieu_ct, trang_thaiGD, ten_tthai, ngay_ctu);

                            }
                            catch (Exception e)
                            {
                                err_code = "1";
                                err_msg = e.Message;
                                return "Loi khi gui thong bao NTDT sang TCT.";
                                strMSG_TEXT = "PutMQ=Exception MSG_ID=" + strMSG_ID + "Exception=Loi khi gui thong bao NTDT sang TCT:" + e.Message.ToString();
                                Utility.Log_Action(strTran_Type, strMSG_TEXT);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                err_code = "1";
                err_msg = e.Message;
                return "Loi khi lấy chứng từ NTDT sang TCT.";
                strMSG_TEXT = "PutMQ=Exception MSG_ID=" + strMSG_ID + "Exception=Loi khi gui thong bao NTDT sang TCT:" + e.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
                //throw e;
            }
            return str_return_gdich;
        }

        public string TaoXMLThongBao(string ma_ctu, string mahieu_ctu, ref MSGHeader header)
        {
            strMA_GIAO = GDICH_NTDT_Constants.HEADER_SENDER_CODE + Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ");
            try
            {
                string xmlOutput = "";
                string xmlGiaoDich = TaoXMLGiaoDich(ma_ctu, mahieu_ctu);
                string xmlThongBao = TaoXMLThongBao_HDR(ma_ctu, mahieu_ctu);

                xmlOutput = TaoXMLHeader(ref header);
                xmlOutput = Utility.RomoveXML_Des(xmlOutput);

                //Giao dịch
                DataTable dtCT = null;
                DataSet dsCT = null;
                string xmlOut_GD = "";
                string d_NDUNG_CTU = "";
                string d_MA_GDICH = "";
                string d_NGAY_GUI_GDICH = "";
                string d_MA_GDICH_TCHIEU = "";
                string strSQL = "";
                strSQL = "SELECT FILECONTENT AS CONTENT,2 AS MA_GDICH,3 as NGAY_GUI_GDICH,4 as MA_GDICH_TCHIEU FROM tcs_ctu_ntdt_hdr";
                strSQL += " Where so_ctu=:SO_CTU and mahieu_ctu=:MAHIEU_CTU";

                IDbDataParameter[] p = null;
                p = new IDbDataParameter[2];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_CTU";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = ma_ctu;

                p[1] = new OracleParameter();
                p[1].ParameterName = "MAHIEU_CTU";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = mahieu_ctu;

                dsCT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, p);
                foreach (DataRow drhdr in dsCT.Tables[0].Rows)
                {
                    d_NDUNG_CTU = drhdr["CONTENT"].ToString();
                    d_MA_GDICH = drhdr["MA_GDICH"].ToString();
                    d_NGAY_GUI_GDICH = drhdr["NGAY_GUI_GDICH"].ToString();
                    d_MA_GDICH_TCHIEU = drhdr["MA_GDICH_TCHIEU"].ToString();
                }




                XmlDocument xmlDoc = new XmlDocument();
                XmlNode xmlNode;
                XmlElement elmBody;

                XmlElement elmCHUNGTU;


                xmlDoc.LoadXml(xmlOutput);
                xmlNode = xmlDoc.SelectSingleNode("/DATA");

                elmBody = xmlDoc.CreateElement("BODY");
                elmBody.InnerXml = xmlThongBao;

                xmlNode.AppendChild(elmBody);

                xmlNode = xmlDoc.SelectSingleNode("/DATA/BODY/ROW/GIAODICH/CHUNGTU/NDUNG_CTU_NH");

                elmCHUNGTU = xmlDoc.CreateElement("NDUNG_CTU");
                d_NDUNG_CTU = d_NDUNG_CTU.Replace("<NDUNG_CTU>", "").Replace("</NDUNG_CTU>", "");

                elmCHUNGTU.InnerXml = d_NDUNG_CTU;


                xmlNode.AppendChild(elmCHUNGTU);
                xmlOutput = xmlDoc.InnerXml;

                return xmlOutput;

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return "";

            }
        }
        public string TaoXMLHeader(ref MSGHeader header)
        {
            string xmlOut = "";
            try
            {

                MSGHeader xmlHeader = new MSGHeader();
                xmlHeader.HEADER = new GDT.MSG.MSGHeader.HEADER();
                xmlHeader.HEADER.VERSION = "1.0";
                xmlHeader.HEADER.SENDER_CODE = GDICH_NTDT_Constants.HEADER_SENDER_CODE;
                xmlHeader.HEADER.SENDER_NAME = GDICH_NTDT_Constants.HEADER_SENDER_NAME;
                xmlHeader.HEADER.RECEIVER_CODE = GDICH_NTDT_Constants.HEADER_RECEIVER_CODE;
                xmlHeader.HEADER.RECEIVER_NAME = GDICH_NTDT_Constants.HEADER_RECEIVER_NAME;
                xmlHeader.HEADER.TRAN_CODE = "06000";
                xmlHeader.HEADER.MSG_ID = strMA_GIAO;
                xmlHeader.HEADER.MSG_REFID = "";
                xmlHeader.HEADER.ID_LINK = "";//objNNTDT.MA_GDICH
                xmlHeader.HEADER.SEND_DATE = DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss");
                xmlHeader.HEADER.ORIGINAL_CODE = "";
                xmlHeader.HEADER.ORIGINAL_NAME = "";
                xmlHeader.HEADER.ORIGINAL_DATE = "";
                xmlHeader.HEADER.ERROR_CODE = "";
                xmlHeader.HEADER.ERROR_DESC = "";
                xmlHeader.HEADER.SPARE1 = "";
                xmlHeader.HEADER.SPARE2 = "";
                xmlHeader.HEADER.SPARE3 = "";

                xmlOut = xmlHeader.MSGtoXML(xmlHeader);



                return xmlOut;

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
            }
            return xmlOut;
        }
        public string TaoXMLGiaoDich(string ma_CT, string KH_Ctu)
        {
            DataTable dtCT = null;
            DataSet dsCT = null;
            string xmlOut = "";
            string d_NDUNG_CTU = "";
            string d_MA_GDICH = "";
            string d_NGAY_GUI_GDICH = "";
            string d_MA_GDICH_TCHIEU = "";
            string strSQL = "";
            strSQL = "SELECT FILECONTENT AS CONTENT,2 AS MA_GDICH,3 as NGAY_GUI_GDICH,4 as MA_GDICH_TCHIEU FROM TCS_DCHIEU_CTU_NTDT_HDR";
            strSQL += " Where so_ctu=:SO_CTU and mahieu_ctu=:MAHIEU_CTU";
            try
            {
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[2];
                p[0] = new OracleParameter();
                p[0].ParameterName = "SO_CTU";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = ma_CT;

                p[1] = new OracleParameter();
                p[1].ParameterName = "MAHIEU_CTU";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = KH_Ctu;

                dsCT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, p);
                foreach (DataRow drhdr in dsCT.Tables[0].Rows)
                {
                    d_NDUNG_CTU = drhdr["CONTENT"].ToString();
                    d_MA_GDICH = drhdr["MA_GDICH"].ToString();
                    d_NGAY_GUI_GDICH = drhdr["NGAY_GUI_GDICH"].ToString();
                    d_MA_GDICH_TCHIEU = drhdr["MA_GDICH_TCHIEU"].ToString();
                }
                xmlOut += "<MA_GDICH>";
                xmlOut += d_MA_GDICH;
                xmlOut += "</MA_GDICH>";
                xmlOut += "<NGAY_GUI_GDICH>";
                xmlOut += d_NGAY_GUI_GDICH;
                xmlOut += "</NGAY_GUI_GDICH>";
                xmlOut += "<MA_GDICH_TCHIEU>";
                xmlOut += d_MA_GDICH_TCHIEU;
                xmlOut += "</MA_GDICH_TCHIEU>";
                xmlOut += "<CHUNGTU>";
                xmlOut += "<NDUNG_CTU_NH>";
                xmlOut += "<MHIEU_CTU>";
                xmlOut += KH_Ctu;
                xmlOut += "</MHIEU_CTU>";
                xmlOut += "<SO_CTU>";
                xmlOut += ma_CT;
                xmlOut += "</SO_CTU>";
                xmlOut += "<NDUNG_CTU>";
                xmlOut += d_NDUNG_CTU;
                xmlOut += "</NDUNG_CTU>";
                xmlOut += "</NDUNG_CTU_NH>";
                xmlOut += "<Signature>";
                xmlOut += "Chữ ký NNT";
                xmlOut += "</Signature>";
                xmlOut += "<Signature>";
                xmlOut += "Chữ ký TCT vào nội dung chứng từ";
                xmlOut += "</Signature>";
                xmlOut += "<Signature>";
                xmlOut += "Chứ ký NHTM vào nội dung chứng từ";
                xmlOut += "</Signature>";
                xmlOut += "</CHUNGTU>";


                return xmlOut;

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return "";
            }
        }

        public string TaoXMLThongBao_HDR(string ma_CT, string KH_Ctu)
        {
            DataSet dsCT = null;
            string xmlOut = "";

            string strSQL = "";
            strSQL += ("SELECT ma_ctu as MA_CTU, phienban_xml AS PBAN_TLIEU_XML,2 AS MA_THONGBAO,3 as MAU_THONGBAO,4 as TEN_THONGBAO,");
            strSQL += (" 5 AS SO_THONGBAO,6 AS NGAY_THONGBAO,mst_nnop as MST_NNOP,ten_nnop as TEN_NNOP,");
            strSQL += (" ma_cqt AS MA_CQT,mst_nthay AS MST_NTHAY,ten_nthay as TEN_NTHAY, id_ctu as ID_CHUNGTU,");
            strSQL += (" so_gnt AS SO_GNT,mahieu_ctu AS MAHIEU_CTU,so_ctu as SO_CTU, ma_hieu_kbac as SHIEU_KHOBAC_NOP,");
            strSQL += (" ten_kbac AS TEN_KHOBAC_NOP,stk_thu AS STK_KHOBAC_NOP,ma_nhang_nop as MA_NGHANG_NOP, stk_nhang_nop as STK_NGHANG_NOP,");
            strSQL += (" 21 AS TONGSOMON,tong_tien AS TONGTIENNOP,23 as PHI, ngay_nhan as NGAY_XLY,");
            strSQL += (" tthai_ctu AS MA_TTHAI,ten_tthai_ctu AS TEN_TTHAI, MA_THAMCHIEU, MA_THAMCHIEU_KNOP ");
            strSQL += (" FROM tcs_ctu_ntdt_hdr");
            strSQL += (" Where mahieu_ctu=:mahieu_ctu and so_ctu=:so_ctu");
            try
            {
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[2];
                p[0] = new OracleParameter();
                p[0].ParameterName = "so_ctu";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = ma_CT;

                p[1] = new OracleParameter();
                p[1].ParameterName = "mahieu_ctu";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = KH_Ctu;

                dsCT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text, p);
                MSG06000 xmlHeader = new MSG06000();
                //xmlHeader.BODY = new GDT.MSG.MSG06000.BODY();
                xmlHeader.GIAODICH = new GDT.MSG.MSG06000.GIAODICH();
                //xmlHeader.GIAODICH = new GDT.MSG.MSG06000.GIAODICH();
                xmlHeader.GIAODICH.THONGBAO = new GDT.MSG.MSG06000.THONGBAO();
                xmlHeader.GIAODICH.CHUNGTU = new GDT.MSG.MSG06000.CHUNGTU();
                xmlHeader.GIAODICH.CHUNGTU.NDUNG_CTU_NH = new GDT.MSG.MSG06000.NDUNG_CTU_NH();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO = new GDT.MSG.MSG06000.NDUNG_TBAO();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR = new GDT.MSG.MSG06000.THONGBAO_HDR();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_CTIET = new GDT.MSG.MSG06000.THONGBAO_CTIET();

                DataRow drhdr1;
                drhdr1 = dsCT.Tables[0].Rows[0];
                xmlHeader.GIAODICH.MA_GDICH = strMA_GIAO;
                xmlHeader.GIAODICH.NGAY_GUI_GDICH = DateTime.Now.ToString("dd/MM/yyyy:HH:MM:ss");
                //xmlHeader.GIAODICH.MA_GDICH_TCHIEU = "";
                xmlHeader.GIAODICH.CHUNGTU.NDUNG_CTU_NH.MA_HIEU_CTU = KH_Ctu;
                xmlHeader.GIAODICH.CHUNGTU.NDUNG_CTU_NH.SO_CTU = ma_CT;
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.PBAN_TLIEU_XML = drhdr1["PBAN_TLIEU_XML"].ToString();

                string strTitle_TB = GDICH_NTDT_Constants.get_thongbao(drhdr1["MA_CTU"].ToString());
                string[] strTB = strTitle_TB.Split(';');
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.MA_THONGBAO = strTB[0].ToString();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.MAU_THONGBAO = strTB[2].ToString();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.TEN_THONGBAO = strTB[1].ToString();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.SO_THONGBAO = strTB[3].ToString();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.NGAY_THONGBAO = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.MST_NNOP = drhdr1["MST_NNOP"].ToString();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.TEN_NNOP = drhdr1["TEN_NNOP"].ToString();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.MA_CQT = drhdr1["MA_CQT"].ToString();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.MST_NTHAY = drhdr1["MST_NTHAY"].ToString();
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_HDR.TEN_NTHAY = drhdr1["TEN_NTHAY"].ToString();


                List<GDT.MSG.MSG06000.ROW_CTIET_TB> listItems = new List<GDT.MSG.MSG06000.ROW_CTIET_TB>();

                foreach (DataRow drhdr in dsCT.Tables[0].Rows)
                {
                    GDT.MSG.MSG06000.ROW_CTIET_TB item = new GDT.MSG.MSG06000.ROW_CTIET_TB();


                    item.ID_CHUNGTU = drhdr["ID_CHUNGTU"].ToString();
                    item.SO_GNT = drhdr["SO_GNT"].ToString();
                    item.SHIEU_KHOBAC_NOP = drhdr["SHIEU_KHOBAC_NOP"].ToString();
                    item.TEN_KHOBAC_NOP = drhdr["TEN_KHOBAC_NOP"].ToString();
                    item.STK_KHOBAC_NOP = drhdr["STK_KHOBAC_NOP"].ToString();
                    item.MA_NGHANG_NOP = drhdr["MA_NGHANG_NOP"].ToString();
                    item.STK_NGHANG_NOP = drhdr["STK_NGHANG_NOP"].ToString();
                    item.TONGSOMON = drhdr["TONGSOMON"].ToString();
                    item.TONGTIENNOP = drhdr["TONGTIENNOP"].ToString();
                    item.PHI = drhdr["PHI"].ToString();
                    item.MA_TTHAI = drhdr["MA_TTHAI"].ToString();

                    item.MA_THAMCHIEU = drhdr["MA_THAMCHIEU"].ToString();
                    //item.MA_THAMCHIEU_KNOP = drhdr["MA_THAMCHIEU_KNOP"].ToString();

                    if (drhdr["MA_TTHAI"].ToString() == GDICH_NTDT_Constants.TT_CT_THANHCONG)
                    {
                        item.MAHIEU_CTU = drhdr["MAHIEU_CTU"].ToString();
                        item.SO_CTU = drhdr["SO_CTU"].ToString();

                        item.NGAY_GUI = drhdr["NGAY_LAP"].ToString();
                        item.NGAY_NTHUE = drhdr["NGAY_LAP"].ToString();

                    }
                    else
                    {
                        item.MAHIEU_CTU = "";
                        item.SO_CTU = "";

                        item.NGAY_GUI = "";
                        item.NGAY_NTHUE = "";
                    }

                    item.TEN_TTHAI = drhdr["TEN_TTHAI"].ToString();
                    listItems.Add(item);
                }
                xmlHeader.GIAODICH.THONGBAO.NDUNG_TBAO.THONGBAO_CTIET.ROW_CTIET = listItems;
                xmlHeader.GIAODICH.THONGBAO.Signature = "Chữ ký NHTM";
                xmlHeader.GIAODICH.Signature = "Chữ ký xác thực NHTM vao ND truyền nhận (nếu cần)";
                xmlOut = xmlHeader.MSGtoXML(xmlHeader);
                xmlOut = Utility.RomoveXML_Des(xmlOut);
                return xmlOut;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return "";
            }
        }


    }
}
