﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VBOracleLib;
using GDT.MSG.MSG03002;
using GDT.MSG.MSGHeader;
using System.Runtime.InteropServices;

namespace ETAX_GDT
{
    public class ProcessCitad
    {
        private static string delayTask = getConfig("TaskCitad.delay");
        private static string stopTime = getConfig("TaskCitad.stopTime");
        private static String strXMLdefin2 = "<DATA xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
        public static string strSignReplace = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void BackgroundJobCitad()
        {
            try
            {
                int stopT = int.Parse(stopTime);
                foreach (var item in GetSo_RefCoreCitad(stopT))
                {
                    Console.WriteLine("Query status Citad - ref_no: " + item);
                    string check_code = GetStatusSendCitad(item.Item1);
                    
                    if (!string.IsNullOrEmpty(check_code) && check_code.Length==2)
                    {
                        UpdateStatusCitad(item.Item1, check_code);

                        Console.WriteLine("Query status Citad - ref_no: "+ item +" - status process: "+ Build06048(item.Item2));
                    }
                }

            }catch(Exception ex)
            {
                log.Info("Error BackgroundJobCitad: " + ex.Message + " " + ex.StackTrace);
            }
        }

        public static void Process()
        {
            Task task = Task.Run((Action)BackgroundJobCitad);
            int hour = int.Parse(delayTask) * 3600000;
            Task.Delay(hour);
        }

        public static string getConfig(string name)
        {
            try
            {
                return ConfigurationManager.AppSettings[name].ToString();

            }catch(Exception ex)
            {
                return "";
            }
        }

        public static List<Tuple<string,string>> GetSo_RefCoreCitad(int stopTimex)
        {
            List<Tuple<string, string>> ls = new List<Tuple<string, string>>();
            try
            {
                string kq = "";
                string sql = "select so_ct_nh, so_ct from tcs_ctu_ntdt_hdr where to_char(ngay_nhang, 'yyyy/MM/dd') >= to_char(sysdate-" + stopTimex + ", 'yyyy/MM/dd') " +
                    "and to_char(ngay_nhang, 'yyyy/MM/dd') <= to_char(sysdate, 'yyyy/MM/dd') and " +
                    " tthai_ctu='11' and tt_sp ='0' and so_ct_nh is not null " +
                    " AND (trangthai_citad IN ('00') AND autosendcitad = 1) or (trangthai_citad IN ('00', '01', '04') AND autosendcitad > 1)" ;
                DataSet ds = DataAccess.ExecuteReturnDataSet(sql, System.Data.CommandType.Text);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ls.Add(new Tuple<string, string>(ds.Tables[0].Rows[i][0].ToString(), ds.Tables[0].Rows[i][1].ToString()));
                }
                return ls;
            }catch(Exception ex)
            {
                return ls;
            }
        }

        public static void UpdateStatusCitad(string soCtNh, string checkCode)
        {
            try
            {
                string sql = "update tcs_ctu_ntdt_hdr set trangthai_citad='" + checkCode + "', autoSendCitad=autoSendCitad+1 where so_ct_nh='" + soCtNh + "'";
                DataAccess.ExecuteNonQuery(sql, CommandType.Text);
            }
            catch(Exception ex)
            {

            }
        }

        public static string GetStatusSendCitad(string ref_core)
        {
            try
            {
                string sql = "select check_code from TBL_TRANS_OUT_GTW where relation_no='" + ref_core + "'";
                return DataAccess.ExecuteReturnDataSet(sql, CommandType.Text).Tables[0].Rows[0][0].ToString();
            }
            catch(Exception ex)
            {
                log.Info("Error GetStatusSendCitad: " + ex.Message + " " + ex.StackTrace);
                return "";
            }
        }

        public  static Boolean Build06048(string soCtu, [Optional] string type)
        {
            string hThuc_Nop = "";
            string mst_Nnop = "";
            string ten_Nnop = "";
            string ma_nhang_nop = "";
            string tgan_tct_Gui = "";
            string content_mess = "";
            string strReturn = "";
            string trangThai = "";
            string msgId = Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ").ToString().PadLeft(7, '0');
            clsMQHelper mq = new clsMQHelper();
            try
            {
                string sql = "SELECT a.hthuc_nop, a.mst_nnop, a.ten_nnop, a.so_ctu,a.ma_nhang_nop, a.ngay_nhang, b.content_mess, a.trangthai_06048 FROM " +
                    "tcs_ctu_ntdt_hdr a,tcs_dm_msg_ntdt b WHERE a.so_ct ='" + soCtu + "' and a.ma_gd=b.id_link and b.tran_code='03002'";
                DataSet ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text);
                if (ds != null && ds.Tables.Count > 0)
                {
                    hThuc_Nop = ds.Tables[0].Rows[0]["hthuc_nop"].ToString();
                    mst_Nnop = ds.Tables[0].Rows[0]["mst_nnop"].ToString();
                    ten_Nnop = ds.Tables[0].Rows[0]["ten_nnop"].ToString();
                    ma_nhang_nop = ds.Tables[0].Rows[0]["ma_nhang_nop"].ToString();
                    tgan_tct_Gui = ds.Tables[0].Rows[0]["ngay_nhang"].ToString();
                    content_mess = ds.Tables[0].Rows[0]["content_mess"].ToString();
                    trangThai = ds.Tables[0].Rows[0]["trangthai_06048"].ToString();
                }

                content_mess = Utility.RomoveXML_Des(content_mess);
                content_mess = content_mess.Replace("<DATA>", strXMLdefin2);
                string xmlthongbao = "";
                if (content_mess.Length > 0)
                {
                    MSG03002 MSG03002 = new MSG03002();
                    MSG03002 objTemp = null;

                    try
                    {
                        objTemp = MSG03002.MSGToObject(content_mess);
                        GDT.MSG.MSG03002.HEADER objHEADER = objTemp.HEADER;
                        GDT.MSG.MSG03002.CHUNGTU objChungTu = objTemp.BODY.ROW.GIAODICH.CHUNGTU;
                        GDT.MSG.MSG03002.CHUNGTU_HDR objCTu_hdr = objTemp.BODY.ROW.GIAODICH.CHUNGTU.NDUNG_CTU_NH.NDUNG_CTU.CHUNGTU_HDR;
                        GDT.MSG.MSG03002.CHUNGTU_CTIET objCTu_dtl = objTemp.BODY.ROW.GIAODICH.CHUNGTU.NDUNG_CTU_NH.NDUNG_CTU.CHUNGTU_CTIET;

                        string xmlHeader= CreateHeader(objHEADER, msgId);
                        xmlthongbao = "<DATA>";
                        xmlthongbao += xmlHeader;
                        xmlthongbao += CreateBODY(objCTu_hdr, objHEADER, soCtu, tgan_tct_Gui, trangThai, msgId, ma_nhang_nop);
                        xmlthongbao += "</DATA>";

                        xmlthongbao = Utility.RomoveXML_Des(xmlthongbao);
                        

                        xmlthongbao = strSignReplace + "" + xmlthongbao;

                        log.Info("Build06048 xmlThongBao: " + xmlthongbao);
                        strReturn = mq.PutMQMessage(xmlthongbao);
                        //Ghi bao bang log msg gui di
                        GDT.MSG.MSG msg_log = new GDT.MSG.MSG();
                        msg_log.error_code = objHEADER.ERROR_CODE;
                        msg_log.error_desc = objHEADER.ERROR_DESC;
                        if (!strReturn.Equals(""))
                        {
                            msg_log.error_code = "02";
                            if (strReturn.Length > 231)
                            {
                                strReturn = strReturn.Substring(0, 230);
                            }
                            msg_log.error_desc = strReturn;
                        }
                        msg_log.sender_code = objHEADER.SENDER_CODE;
                        msg_log.sender_name = objHEADER.SENDER_NAME;
                        msg_log.receiver_code = objHEADER.RECEIVER_CODE;
                        msg_log.receiver_name = objHEADER.RECEIVER_NAME;
                        msg_log.tran_code = objHEADER.TRAN_CODE;
                        msg_log.msg_id = objHEADER.MSG_ID;
                        msg_log.msg_refid = objHEADER.MSG_REFID;
                        msg_log.id_link = objHEADER.ID_LINK;
                        msg_log.send_date = objHEADER.SEND_DATE;
                        msg_log.original_code = objHEADER.ORIGINAL_CODE;
                        msg_log.original_name = objHEADER.ORIGINAL_NAME;
                        msg_log.original_date = objHEADER.ORIGINAL_DATE;

                        msg_log.spare1 = objHEADER.SPARE1;
                        msg_log.spare2 = objHEADER.SPARE2;
                        msg_log.spare3 = objHEADER.SPARE3;
                        msg_log.trangthai = "00";
                        msg_log.content_mess = xmlthongbao;

                        string ma_tt = "";
                        Utility.insertMSGOutLog(msg_log, ref ma_tt);
                        return true;
                    }
                    catch (Exception e)
                    {
                        log.Info("Fail Build06048: " + e.Message + " " + e.StackTrace);
                        return false;
                    }
                }

            }catch(Exception ex)
            {
                log.Info("Fail Build06048: " + ex.Message + " " + ex.StackTrace);
                return false;
            }
            return false;
        }

        private static string CreateHeader(GDT.MSG.MSG03002.HEADER obj, string msgId)
        {
            string xmlOut = "";

            try
            {
                GDT.MSG.MSGHeader.HEADER xmlHeader = new GDT.MSG.MSGHeader.HEADER();

                //xmlHeader.HEADER = new GDT.MSG.MSGHeader.HEADER();
                xmlHeader.VERSION = GDICH_NTDT_Constants.HEADER_VERSION;
                xmlHeader.SENDER_CODE = GDICH_NTDT_Constants.HEADER_SENDER_CODE;
                xmlHeader.SENDER_NAME = GDICH_NTDT_Constants.HEADER_SENDER_NAME;
                xmlHeader.RECEIVER_CODE = GDICH_NTDT_Constants.HEADER_RECEIVER_CODE;
                xmlHeader.RECEIVER_NAME = GDICH_NTDT_Constants.HEADER_RECEIVER_NAME;
                xmlHeader.TRAN_CODE = GDICH_NTDT_Constants.ESB_TYPE_MESS_TBAO_UNT;
                xmlHeader.MSG_ID = GDICH_NTDT_Constants.HEADER_SENDER_CODE + msgId;
                xmlHeader.MSG_REFID = xmlHeader.MSG_ID;
                xmlHeader.ID_LINK = obj.ID_LINK;//objNNTDT.MA_GDICH
                xmlHeader.SEND_DATE = DateTime.Now.ToString(GDICH_NTDT_Constants.FORMAT_NGAY_GD_HEADER);
                xmlHeader.ORIGINAL_CODE = obj.ORIGINAL_CODE;
                xmlHeader.ORIGINAL_NAME = obj.ORIGINAL_NAME;
                xmlHeader.ORIGINAL_DATE = obj.ORIGINAL_DATE;
                xmlHeader.ERROR_CODE = "";
                xmlHeader.ERROR_DESC = "";
                xmlHeader.SPARE1 = "";
                xmlHeader.SPARE2 = "";
                xmlHeader.SPARE3 = "";

                xmlOut = xmlHeader.MSGtoXML(xmlHeader);
                xmlOut = Utility.RomoveXML_Des(xmlOut);

                //header = xmlHeader;

                return xmlOut;
            }
            catch(Exception ex)
            {
                return "";
            }
        }

        private static string CreateBODY(CHUNGTU_HDR obj, GDT.MSG.MSG03002.HEADER objHeader, string soCT, string tgianNhan, string statusSend, string msgId, string maNhangNop)
        {
            try
            {
                string xmloutput = "";
                string ma_GD = GDICH_NTDT_Constants.HEADER_SENDER_CODE + msgId; 
                string ngay_GD = DateTime.Now.ToString(GDICH_NTDT_Constants.FORMAT_NGAY_TB);
                string Ma_GD_ThamChieu = objHeader.ID_LINK;
                //string ten_TT = Utility.getTen_TT(trangthai);
                string xmlThongBao = CreateXMLBody(obj, soCT, tgianNhan, statusSend, maNhangNop);
                xmloutput = "<BODY><ROW><GIAODICH>";
                xmloutput += "<MA_GDICH>" + ma_GD + "</MA_GDICH>";
                xmloutput += "<NGAY_GUI_GDICH>" + ngay_GD + "</NGAY_GUI_GDICH>";
                xmloutput += xmlThongBao;
                xmloutput += "</GIAODICH></ROW></BODY>";
                return xmloutput;
            }catch(Exception ex)
            {

                return "";
            }
        }
        private static string CreateXMLBody(CHUNGTU_HDR obj, string soct, string tgNhan, string statusSend, string maNhangNop)
        {
            try
            {
                string xmlOut = "";

                NoiDung06048 objNoiDung = new NoiDung06048();
                objNoiDung.NDUNG_CTIET = new NoiDungCTIET06048();
                objNoiDung.NDUNG_CTIET.HTHUC_NOP = obj.HTHUC_NOP;
                objNoiDung.NDUNG_CTIET.MA_THAMCHIEU = obj.MA_THAMCHIEU;
                objNoiDung.NDUNG_CTIET.MST_NNOP = obj.MST_NNOP;
                objNoiDung.NDUNG_CTIET.TEN_NNOP = obj.TEN_NNOP;
                objNoiDung.NDUNG_CTIET.SO_CTU = soct;
                objNoiDung.NDUNG_CTIET.NGAY_GUI = DateTime.Parse(tgNhan).ToString("dd/MM/yyyy HH:mm:ss");
                objNoiDung.NDUNG_CTIET.NOI_NHAN = maNhangNop;
                objNoiDung.NDUNG_CTIET.TRANG_THAI = statusSend;

                xmlOut = new NoiDung06048().MSGtoXML(objNoiDung);
                xmlOut = Utility.RomoveXML_Des(xmlOut);

                string signatureType = Utility.GetValue_THAMSOHT("SIGN.TYPE.TCT");
                string xpathSign = "/NOIDUNG";
                string xpathNodeSign = "NOIDUNG";
                if (signatureType.Equals("1"))
                {
                    xmlOut = Signature.DigitalSignature.getSignNTDT(xmlOut, xpathSign, xpathNodeSign);
                }
                else
                {
                    xmlOut = Signature.SignatureProcess.SignXMLEextendTimeStamp(xmlOut, "NDUNG_CTIET", "NOIDUNG", true);
                }
                //vinhvv
                //
                xmlOut = xmlOut.Replace(strSignReplace, "");
                return xmlOut;
            }catch(Exception ex)
            {
                return "";
            }
        }
    }
}
