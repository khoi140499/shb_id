﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace GDT.MSG.MSG06007
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG06007
    {
        public MSG06007()
        {
        }
        [XmlElement("HEADER")]
        public HEADER HEADER;
        [XmlElement("BODY")]
        public BODY BODY;
        //[XmlElement("SECURITY")]
        //public SECURITY SECURITY;

        public string MSGtoXML(MSG06007 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG06007));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG06007 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSG06007.MSG06007));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            GDT.MSG.MSG06007.MSG06007 LoadedObjTmp = (GDT.MSG.MSG06007.MSG06007)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }
    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW ROW;
    }

    public class ROW
    {
        public ROW()
        { }
        [XmlElement("GIAODICH")]
        public GIAODICH GIAODICH;
    }

    public class GIAODICH
    {
        public GIAODICH()
        { }
        [XmlElementAttribute(Order = 1)]
        public string MA_GDICH { get; set; }
        [XmlElementAttribute(Order = 2)]
        public string NGAY_GUI_GDICH { get; set; }
        [XmlElementAttribute(Order = 3)]
        public string MA_GDICH_TCHIEU { get; set; }
        [XmlElementAttribute(Order = 4)]
        public TBAO_TKHOAN_NH TBAO_TKHOAN_NH;
    }

    public class TBAO_TKHOAN_NH
    {
        public TBAO_TKHOAN_NH()
        { }
        [XmlElementAttribute(Order = 1)]
        public string PBAN_TLIEU_XML { get; set; }
        [XmlElementAttribute(Order = 2)]
        public string MA_TBAO { get; set; }
        [XmlElementAttribute(Order = 3)]
        public string MAU_TBAO { get; set; }
        [XmlElementAttribute(Order = 4)]
        public string SO_TBAO { get; set; }
        [XmlElementAttribute(Order = 5)]
        public string TEN_TBAO { get; set; }
        [XmlElementAttribute(Order = 6)]
        public string NGAY_TBAO { get; set; }
        [XmlElementAttribute(Order = 7)]
        public string MST { get; set; }
        [XmlElementAttribute(Order = 8)]
        public string TEN_NNT { get; set; }
        [XmlElementAttribute(Order = 9)]
        public string DIACHI_NNT { get; set; }
        [XmlElementAttribute(Order = 10)]
        public string MA_CQT { get; set; }
        [XmlElementAttribute(Order = 11)]
        public string EMAIL_NNT { get; set; }
        [XmlElementAttribute(Order = 12)]
        public string SDT_NNT { get; set; }
        [XmlElementAttribute(Order = 13)]
        public string TEN_LHE_NTHUE { get; set; }
        [XmlElementAttribute(Order = 14)]
        public string SERIAL_CERT_NTHUE { get; set; }
        [XmlElementAttribute(Order = 15)]
        public string SUBJECT_CERT_NTHUE { get; set; }
        [XmlElementAttribute(Order = 16)]
        public string ISSUER_CERT_NTHUE { get; set; }
        [XmlElementAttribute(Order = 17)]
        public string MA_NHANG { get; set; }
        [XmlElementAttribute(Order = 18)]
        public string TEN_NHANG { get; set; }
        [XmlElementAttribute(Order = 19)]
        public string VAN_ID { get; set; }
        [XmlElementAttribute(Order = 20)]
        public string TEN_TVAN { get; set; }
        [XmlElementAttribute(Order = 21)]
        public TTIN_TKHOAN TTIN_TKHOAN;
        [XmlElementAttribute(Order = 22)]
        public string TTHAI_XNHAN { get; set; }
        [XmlElementAttribute(Order = 23)]
        public string LDO_TCHOI { get; set; }
        //public List<string> Signature { get; set; }
    }
    
    public class TTIN_TKHOAN
    {
        public TTIN_TKHOAN()
        { }
        [XmlElement("STK")]
        public List<String> STK { get; set; }

    }

    
    //public class SECURITY
    //{
    //    public SECURITY() 
    //    { 
    //    }
    //    public string SIGNATURE { get; set; }
    //}
}
