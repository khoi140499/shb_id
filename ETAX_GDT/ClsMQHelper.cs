﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;
using IBM.WMQ;
using System.Configuration;
using VBOracleLib;

namespace ETAX_GDT
{
    public class clsMQHelper
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string mv_strHostIP;
        private string mv_HostPort;
        private string mv_strQmgrName;
        private string mv_strQInputName;
        private string mv_strQOutputName;

        private string mv_strChannelName;
        private MQQueueManager mv_objQMgr;
        private MQQueue mv_objQueueIn;
        private MQQueue mv_objQueueOut;
        private MQGetMessageOptions mv_objGetOptions;

        private MQPutMessageOptions mv_objPutOptions;
        public clsMQHelper()
        {
            InitParam();
        }

        private void InitParam()
        {
            try
            {
                mv_strHostIP = ConfigurationManager.AppSettings.Get("mq.hostIP").ToString();
                mv_HostPort = ConfigurationManager.AppSettings.Get("mq.hostPort").ToString();
                mv_strQmgrName = ConfigurationManager.AppSettings.Get("mq.qmgrName").ToString();
                mv_strQInputName = ConfigurationManager.AppSettings.Get("mq.qlinputName").ToString();
                mv_strQOutputName = ConfigurationManager.AppSettings.Get("mq.qloutputName").ToString();
                mv_strChannelName = ConfigurationManager.AppSettings.Get("mq.svrchannelName").ToString();

                //mv_strHostIP = "103.9.200.66";
                //mv_HostPort = "1717";
                //mv_strQmgrName = "ESB.EXT.QMGR";
                //mv_strQInputName = "EXT.BANK.INBOX.QUEUE";
                //mv_strQOutputName = "NTDT_SHB.OUTBOX.QUEUE";
                //mv_strChannelName = "NTDT_SHB.SVRCONN";
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
            }
        }

        private bool ConnectMQ()
        {
            try
            {
                Hashtable queueProperties = new Hashtable();
                queueProperties.Add(MQC.HOST_NAME_PROPERTY, mv_strHostIP);
                queueProperties.Add(MQC.PORT_PROPERTY, Convert.ToInt32(mv_HostPort));
                queueProperties.Add(MQC.CHANNEL_PROPERTY, mv_strChannelName);
                queueProperties.Add(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT);
                mv_objQMgr = new MQQueueManager(mv_strQmgrName, queueProperties);

                int openInputOptions = MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING;
                int openOutputOptions = MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING +MQC.MQOO_INQUIRE;

                mv_objQueueIn = mv_objQMgr.AccessQueue(mv_strQInputName, openInputOptions);
                mv_objQueueOut = mv_objQMgr.AccessQueue(mv_strQOutputName, openOutputOptions);


                mv_objGetOptions = new MQGetMessageOptions();
                mv_objGetOptions.Options += MQC.MQGMO_WAIT;
                mv_objGetOptions.WaitInterval = 60000;
                mv_objPutOptions = new MQPutMessageOptions();
              
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                return false;
            }
            return true;
        }

        private bool DisconnectMQ()
        {
            try
            {
                if ((mv_objQueueOut != null))
                {
                    mv_objQueueOut.Close();
                }
                if ((mv_objQueueIn != null))
                {
                    mv_objQueueIn.Close();
                }
                if ((mv_objQMgr != null))
                {
                    mv_objQMgr.Disconnect();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                return false;
            }
            return true;
        }

        public string PutAndGetMQMessage(string pv_strMessage)
        {
            string v_strRequest = pv_strMessage;
            string v_strResponse = "";
            try
            {
                if (ConnectMQ())
                {
                    MQMessage mqPutMsg = new MQMessage();
                    mqPutMsg.CharacterSet = IBM.WMQ.MQC.CODESET_UTF;
                    mqPutMsg.Format = MQC.MQFMT_STRING;
                    mqPutMsg.WriteString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + v_strRequest);
                    mv_objQueueIn.Put(mqPutMsg, mv_objPutOptions);
                    MQMessage mqGetMsg = new MQMessage();
                    mqGetMsg.CorrelationId = mqPutMsg.MessageId;
                    mv_objQueueOut.Get(mqGetMsg, mv_objGetOptions);
                    v_strResponse = mqGetMsg.ReadString(mqGetMsg.MessageLength);
                }
                DisconnectMQ();
            }
            catch (IBM.WMQ.MQException ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                if ((ex.Reason == MQC.MQRC_NO_MSG_AVAILABLE))
                {
                   // log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                }
                else
                {
                   // log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                    return v_strResponse;
                }
            }
            return v_strResponse;
        }

        public string GetAndPutMQMessage(string pv_strMessage)
        {
            string v_strRequest = pv_strMessage;
            string v_strResponse = "";
            try
            {
                if (ConnectMQ())
                {
                    MQMessage mqGetMsg = new MQMessage();
                    mv_objQueueOut.Get(mqGetMsg, mv_objGetOptions);
                    v_strResponse = mqGetMsg.ReadString(mqGetMsg.MessageLength);

                    MQMessage mqPutMsg = new MQMessage();
                    mqPutMsg.CharacterSet = IBM.WMQ.MQC.CODESET_UTF;
                    mqPutMsg.Format = MQC.MQFMT_STRING;
                    mqPutMsg.WriteString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + v_strRequest);
                    mv_objQueueIn.Put(mqPutMsg, mv_objPutOptions);

                }
                DisconnectMQ();
            }
            catch (IBM.WMQ.MQException ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                if ((ex.Reason == MQC.MQRC_NO_MSG_AVAILABLE))
                {
                  //  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                }
                else
                {
                  //  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                    return v_strResponse;
                }
            }
            return v_strResponse;
        }

        public string PutMQMessage(string pv_strMessage)
        {
            string v_strRequest = pv_strMessage;
            string v_strResponse = "";
            try
            {
                if (ConnectMQ())
                {
                    MQMessage mqPutMsg = new MQMessage();
                    mqPutMsg.CharacterSet = IBM.WMQ.MQC.CODESET_UTF;
                    mqPutMsg.Format = MQC.MQFMT_STRING;
                    mqPutMsg.WriteString(pv_strMessage);
                    mv_objQueueIn.Put(mqPutMsg, mv_objPutOptions);
                    mqPutMsg.ClearMessage();
                }
                else
                {
                    v_strResponse = "Không connect được MQ put";
                }
            }
            catch (IBM.WMQ.MQException ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
               // LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                v_strResponse = ex.Message;
         
            }
            finally
            {
                DisconnectMQ();
            }
            return v_strResponse;
        }

        public string GetMQMessage()
        {
            string v_strResponse = "";
            try
            {
                if (ConnectMQ())
                {
                    MQMessage mqGetMsg = new MQMessage();
                    //if (mv_objQueueOut.CurrentDepth > 0)
                    //{
                        mv_objQueueOut.Get(mqGetMsg, mv_objGetOptions);
                        v_strResponse = mqGetMsg.ReadString(mqGetMsg.MessageLength);
                        mqGetMsg.ClearMessage();
                    //}
                }
                DisconnectMQ();
            }
            catch (IBM.WMQ.MQException ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                DisconnectMQ();
                if ((ex.Reason == MQC.MQRC_NO_MSG_AVAILABLE) || ex.Message == "2033" || ex.Message == "MQRC_NO_MSG_AVAILABLE")
                { }
                else
                {
                  //  log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, EventLogEntryType.Error);
                    return v_strResponse;
                }
            }
            return v_strResponse;
        }

    }
}
