﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG;
using GDT.MSG.MSG06006;
using GDT.MSG.MSG06004;
using System.Data.OracleClient;
using System.Data;
using GDT.MSG.MSGHeader;
using Business.NTDT.NNTDT;
using VBOracleLib;
namespace ETAX_GDT
{
    
    class ProcessDangKyNNTDT_TTXL_NH
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string strTran_Type = "";
        private string strMSG_TEXT = "";
        public ProcessDangKyNNTDT_TTXL_NH()
        {
           
        }

        public void Process(MSG msg, bool checkCK)
        {
            try
            {

                if (msg != null)
                {
                    string xmlIn = "";
                    xmlIn = msg.content_mess;
                    if (xmlIn.Length > 0)
                    {
                        xmlIn = xmlIn.Replace("<DATA>", Utility.strXMLdefin2);
                    
                        MSG06006 msg06006 = new MSG06006();
                        MSG06006 objTemp = null;
                        try
                        {
                            objTemp = msg06006.MSGToObject(xmlIn);

                            if (objTemp != null)
                            {
                                DKY_NTHUE DKY_NTHUE = objTemp.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE;

                                MSGHeader msgheader = new MSGHeader();
                                msgheader.HEADER = new GDT.MSG.MSGHeader.HEADER();

                                msgheader.HEADER.ERROR_CODE = objTemp.HEADER.ERROR_CODE;
                                msgheader.HEADER.ERROR_DESC = objTemp.HEADER.ERROR_DESC;
                                msgheader.HEADER.ID_LINK = objTemp.HEADER.ID_LINK;
                                msgheader.HEADER.MSG_ID = objTemp.HEADER.MSG_ID;
                                msgheader.HEADER.MSG_REFID = objTemp.HEADER.MSG_REFID;
                                msgheader.HEADER.ORIGINAL_CODE = objTemp.HEADER.ORIGINAL_CODE;
                                msgheader.HEADER.ORIGINAL_DATE = objTemp.HEADER.ORIGINAL_DATE;
                                msgheader.HEADER.ORIGINAL_NAME = objTemp.HEADER.ORIGINAL_NAME;
                                msgheader.HEADER.RECEIVER_CODE = objTemp.HEADER.RECEIVER_CODE;
                                msgheader.HEADER.RECEIVER_NAME = objTemp.HEADER.RECEIVER_NAME;
                                msgheader.HEADER.SEND_DATE = objTemp.HEADER.SEND_DATE;
                                msgheader.HEADER.SENDER_CODE = objTemp.HEADER.SENDER_CODE;
                                msgheader.HEADER.SENDER_NAME = objTemp.HEADER.SENDER_NAME;
                                msgheader.HEADER.SPARE1 = objTemp.HEADER.SPARE1;
                                msgheader.HEADER.SPARE2 = objTemp.HEADER.SPARE2;
                                msgheader.HEADER.SPARE3 = objTemp.HEADER.SPARE3;
                                msgheader.HEADER.TRAN_CODE = objTemp.HEADER.TRAN_CODE;
                                msgheader.HEADER.VERSION = objTemp.HEADER.VERSION;

                                if (DKY_NTHUE.DIACHI_NNT == null || DKY_NTHUE.EMAIL_NNT == null || DKY_NTHUE.ISSUER_CERT_NTHUE == null || DKY_NTHUE.MA_CQT == null || DKY_NTHUE.MA_NHANG == null || DKY_NTHUE.MST == null || DKY_NTHUE.NGAY_GUI == null || DKY_NTHUE.PBAN_TLIEU_XML == null || DKY_NTHUE.SDT_NNT == null || DKY_NTHUE.SERIAL_CERT_NTHUE == null || DKY_NTHUE.STK_NGHANG == null || DKY_NTHUE.SUBJECT_CERT_NTHUE == null || DKY_NTHUE.TEN_LHE_NTHUE == null || DKY_NTHUE.TEN_NHANG == null || DKY_NTHUE.TEN_NNT == null || DKY_NTHUE.TEN_TVAN == null || DKY_NTHUE.VAN_ID == null)
                                {

                                    //phản hồi nhận dữ liệu bị lỗi cấu trúc cho tct
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "02", "07 - Cấu trúc đăng kí dịch vụ NTDT không đúng khuôn dạng");

                                    infNNTDT objNNTDT = new infNNTDT();
                                    objNNTDT.DIACHI_NNT = DKY_NTHUE.DIACHI_NNT + "";
                                    objNNTDT.EMAIL_NNT = DKY_NTHUE.EMAIL_NNT + "";
                                    objNNTDT.ISSUER_CERT_NTHUE = DKY_NTHUE.ISSUER_CERT_NTHUE + "";
                                    objNNTDT.LDO_TCHOI = "Cấu trúc đăng kí dịch vụ NTDT không đúng khuôn dạng";
                                    objNNTDT.MA_CQT = DKY_NTHUE.MA_CQT + "";
                                    objNNTDT.MA_GDICH = objTemp.BODY.ROW.GIAODICH.MA_GDICH + "";
                                    objNNTDT.MA_GDICH_TCHIEU = objTemp.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU + "";
                                    objNNTDT.MA_NHANG = DKY_NTHUE.MA_NHANG + "";
                                    objNNTDT.MST = DKY_NTHUE.MST + "";
                                    objNNTDT.NGAY_GUI = DKY_NTHUE.NGAY_GUI + "";
                                    objNNTDT.NGAY_GUI_GDICH = objTemp.BODY.ROW.GIAODICH.NGAY_GUI_GDICH + "";
                                    objNNTDT.SDT_NNT = DKY_NTHUE.SDT_NNT + "";
                                    objNNTDT.SERIAL_CERT_NTHUE = DKY_NTHUE.SERIAL_CERT_NTHUE + "";
                                    objNNTDT.STK_NHANG = DKY_NTHUE.SDT_NNT + "";
                                    objNNTDT.SUBJECT_CERT_NTHUE = DKY_NTHUE.SUBJECT_CERT_NTHUE + "";
                                    objNNTDT.TEN_LHE_NTHUE = DKY_NTHUE.TEN_LHE_NTHUE + "";
                                    objNNTDT.TEN_NHANG = DKY_NTHUE.TEN_NHANG + "";
                                    objNNTDT.TEN_NNT = DKY_NTHUE.TEN_NNT + "";
                                    objNNTDT.TEN_TVAN = DKY_NTHUE.TEN_TVAN + "";
                                    objNNTDT.TRANG_THAI = "04";
                                    objNNTDT.VAN_ID = DKY_NTHUE.VAN_ID + "";

                                    //thông báo chi tiết lỗi cấu trúc msg cho tct
                                    string errcode = "";
                                    string errmsg = "";
                                    ProcessThongBaoDangKyNNTDT_NH_TTXL guithongbao = new ProcessThongBaoDangKyNNTDT_NH_TTXL();
                                    guithongbao.sendMSG06007(objNNTDT, ref errcode, ref errmsg);

                                }
                                else if (checkCK == false)
                                {
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "02", "05 - Sai chữ ký số");

                                    infNNTDT objNNTDT = new infNNTDT();
                                    objNNTDT.DIACHI_NNT = DKY_NTHUE.DIACHI_NNT;
                                    objNNTDT.EMAIL_NNT = DKY_NTHUE.EMAIL_NNT;
                                    objNNTDT.ISSUER_CERT_NTHUE = DKY_NTHUE.ISSUER_CERT_NTHUE;
                                    objNNTDT.LDO_TCHOI = "Sai chữ ký số";
                                    objNNTDT.MA_CQT = DKY_NTHUE.MA_CQT;
                                    objNNTDT.MA_GDICH = objTemp.BODY.ROW.GIAODICH.MA_GDICH;
                                    objNNTDT.MA_GDICH_TCHIEU = objTemp.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU;
                                    objNNTDT.MA_NHANG = DKY_NTHUE.MA_NHANG;
                                    objNNTDT.MST = DKY_NTHUE.MST;
                                    objNNTDT.NGAY_GUI = DKY_NTHUE.NGAY_GUI;
                                    objNNTDT.NGAY_GUI_GDICH = objTemp.BODY.ROW.GIAODICH.NGAY_GUI_GDICH;
                                    objNNTDT.SDT_NNT = DKY_NTHUE.SDT_NNT;
                                    objNNTDT.SERIAL_CERT_NTHUE = DKY_NTHUE.SERIAL_CERT_NTHUE;
                                    objNNTDT.STK_NHANG = DKY_NTHUE.SDT_NNT;
                                    objNNTDT.SUBJECT_CERT_NTHUE = DKY_NTHUE.SUBJECT_CERT_NTHUE;
                                    objNNTDT.TEN_LHE_NTHUE = DKY_NTHUE.TEN_LHE_NTHUE;
                                    objNNTDT.TEN_NHANG = DKY_NTHUE.TEN_NHANG;
                                    objNNTDT.TEN_NNT = DKY_NTHUE.TEN_NNT;
                                    objNNTDT.TEN_TVAN = DKY_NTHUE.TEN_TVAN;
                                    objNNTDT.TRANG_THAI = "04";
                                    objNNTDT.VAN_ID = DKY_NTHUE.VAN_ID;

                                    //thông báo chi tiết lỗi msg cho tct
                                    string errcode = "";
                                    string errmsg = "";
                                    ProcessThongBaoDangKyNNTDT_NH_TTXL guithongbao = new ProcessThongBaoDangKyNNTDT_NH_TTXL();
                                    guithongbao.sendMSG06007(objNNTDT, ref errcode, ref errmsg);

                                }


                                else
                                {
                                    capnhatDKNNTDT(objTemp);

                                    //phản hồi đã nhận dữ liệu
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "01", "Thành công");
                                }
                            }
                        }
                        catch (Exception e)
                        {

                        }
                        
                        strTran_Type = "MSG06006";
                        strMSG_TEXT = "Process=Time:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "capnhatDKNNTDT=" + objTemp.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MST.ToString() ;
                        Utility.Log_Action(strTran_Type, strMSG_TEXT);
                    }
                }
                
                

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                strTran_Type = "MSG06006";
                strMSG_TEXT = "Process=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "Exception=" + ex.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            }
        }

        #region "Dang ki su dung dich vu"
        
        private static void capnhatDKNNTDT(MSG06006 msg06006)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO TCS_DM_NNTDT (");
            sbSqlInsert.Append("PBAN_TLIEU_XML,");
            sbSqlInsert.Append("MST,");
            sbSqlInsert.Append("TEN_NNT,");
            sbSqlInsert.Append("DIACHI_NNT,");
            sbSqlInsert.Append("MA_CQT,");
            sbSqlInsert.Append("EMAIL_NNT,");
            sbSqlInsert.Append("SDT_NNT,");
            sbSqlInsert.Append("TEN_LHE_NTHUE,");
            sbSqlInsert.Append("SERIAL_CERT_NTHUE,");
            sbSqlInsert.Append("SUBJECT_CERT_NTHUE,");
            sbSqlInsert.Append("ISSUER_CERT_NTHUE,");
            sbSqlInsert.Append("MA_NHANG,");
            sbSqlInsert.Append("TEN_NHANG,");
            sbSqlInsert.Append("VAN_ID,");
            sbSqlInsert.Append("TEN_TVAN,");
            sbSqlInsert.Append("NGAY_GUI,");
            sbSqlInsert.Append("STK_NHANG,");
            sbSqlInsert.Append("SIGNATURE_NNT,");
            sbSqlInsert.Append("SIGNATURE_TCT,");
            sbSqlInsert.Append("TRANG_THAI,");
            sbSqlInsert.Append("NGAY_TAO,");
            sbSqlInsert.Append("MA_GDICH,");
            sbSqlInsert.Append("NGAY_GUI_GDICH,");
            sbSqlInsert.Append("MA_GDICH_TCHIEU,");
            sbSqlInsert.Append("NNTDT_ID)");

            sbSqlInsert.Append(" VALUES(");
            sbSqlInsert.Append(":PBAN_TLIEU_XML,");
            sbSqlInsert.Append(":MST,");
            sbSqlInsert.Append(":TEN_NNT,");
            sbSqlInsert.Append(":DIACHI_NNT,");
            sbSqlInsert.Append(":MA_CQT,");
            sbSqlInsert.Append(":EMAIL_NNT,");
            sbSqlInsert.Append(":SDT_NNT,");
            sbSqlInsert.Append(":TEN_LHE_NTHUE,");
            sbSqlInsert.Append(":SERIAL_CERT_NTHUE,");
            sbSqlInsert.Append(":SUBJECT_CERT_NTHUE,");
            sbSqlInsert.Append(":ISSUER_CERT_NTHUE,");
            sbSqlInsert.Append(":MA_NHANG,");
            sbSqlInsert.Append(":TEN_NHANG,");
            sbSqlInsert.Append(":VAN_ID,");
            sbSqlInsert.Append(":TEN_TVAN,");
            sbSqlInsert.Append(":NGAY_GUI,");
            sbSqlInsert.Append(":STK_NHANG,");
            sbSqlInsert.Append(":SIGNATURE_NNT,");
            sbSqlInsert.Append(":SIGNATURE_TCT,");
            sbSqlInsert.Append(":TRANG_THAI,");
            sbSqlInsert.Append(":NGAY_TAO,");
            sbSqlInsert.Append(":MA_GDICH,");
            sbSqlInsert.Append(":NGAY_GUI_GDICH,");
            sbSqlInsert.Append(":MA_GDICH_TCHIEU,");
            sbSqlInsert.Append(":NNTDT_ID)");

            
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE == null)
                {
                    throw new Exception("Lỗi lấy thông tin đăng kí sử dụng dịch vụ NTDT");
                }

                if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE != null)
                {

                    ////Xoa du lieu truoc
                    ////Khai bao cac cau lenh
                    //StringBuilder sbSqlDelete = new StringBuilder("");
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    //sbSqlDelete = new StringBuilder("");
                    //sbSqlDelete.Append("DELETE FROM TCS_DM_HQ ");
                    //p = new IDbDataParameter[0];
                    //Utility.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                    //Gan cac thong tin
                    string PBAN_TLIEU_XML = DBNull.Value.ToString();
                    string MST = DBNull.Value.ToString();
                    string TEN_NNT = DBNull.Value.ToString();
                    string DIACHI_NNT = DBNull.Value.ToString();
                    string MA_CQT = DBNull.Value.ToString();
                    string EMAIL_NNT = DBNull.Value.ToString();
                    string SDT_NNT = DBNull.Value.ToString();
                    string TEN_LHE_NTHUE = DBNull.Value.ToString();
                    string SERIAL_CERT_NTHUE = DBNull.Value.ToString();
                    string SUBJECT_CERT_NTHUE = DBNull.Value.ToString();
                    string ISSUER_CERT_NTHUE = DBNull.Value.ToString();
                    string MA_NHANG = DBNull.Value.ToString();
                    string TEN_NHANG = DBNull.Value.ToString();
                    string VAN_ID = DBNull.Value.ToString();
                    string TEN_TVAN = DBNull.Value.ToString();
                    string NGAY_GUI = DBNull.Value.ToString();
                    string STK_NHANG = DBNull.Value.ToString();
                    string SIGNATURE_NNT = DBNull.Value.ToString();
                    string SIGNATURE_TCT = DBNull.Value.ToString();
                    string TRANG_THAI = DBNull.Value.ToString();
                    string NGAY_TAO = DBNull.Value.ToString();
                    string MA_GDICH = DBNull.Value.ToString();
                    string NGAY_GUI_GDICH = DBNull.Value.ToString();
                    string MA_GDICH_TCHIEU = DBNull.Value.ToString();


                    //foreach (Customs.MSG.MSG16.ItemData itdata in msg16.Data.Item)
                    //{

                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.PBAN_TLIEU_XML != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.PBAN_TLIEU_XML.Equals(string.Empty))
                        {
                            PBAN_TLIEU_XML = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.PBAN_TLIEU_XML;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MST != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MST.Equals(string.Empty))
                        {
                            MST = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MST;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_NNT != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_NNT.Equals(string.Empty))
                        {
                            TEN_NNT = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_NNT;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.DIACHI_NNT != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.DIACHI_NNT.Equals(string.Empty))
                        {
                            DIACHI_NNT = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.DIACHI_NNT;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MA_CQT != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MA_CQT.Equals(string.Empty))
                        {
                            MA_CQT = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MA_CQT;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.EMAIL_NNT != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.EMAIL_NNT.Equals(string.Empty))
                        {
                            EMAIL_NNT = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.EMAIL_NNT;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.SDT_NNT != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.SDT_NNT.Equals(string.Empty))
                        {
                            SDT_NNT = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.SDT_NNT;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_LHE_NTHUE != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_LHE_NTHUE.Equals(string.Empty))
                        {
                            TEN_LHE_NTHUE = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_LHE_NTHUE;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.SERIAL_CERT_NTHUE != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.SERIAL_CERT_NTHUE.Equals(string.Empty))
                        {
                            SERIAL_CERT_NTHUE = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.SERIAL_CERT_NTHUE;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.SUBJECT_CERT_NTHUE != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.SUBJECT_CERT_NTHUE.Equals(string.Empty))
                        {
                            SUBJECT_CERT_NTHUE = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.SUBJECT_CERT_NTHUE;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.ISSUER_CERT_NTHUE != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.ISSUER_CERT_NTHUE.Equals(string.Empty))
                        {
                            ISSUER_CERT_NTHUE = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.ISSUER_CERT_NTHUE;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MA_NHANG != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MA_NHANG.Equals(string.Empty))
                        {
                            MA_NHANG = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.MA_NHANG;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_NHANG != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_NHANG.Equals(string.Empty))
                        {
                            TEN_NHANG = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_NHANG;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.VAN_ID != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.VAN_ID.Equals(string.Empty))
                        {
                            VAN_ID = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.VAN_ID;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_TVAN != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_TVAN.Equals(string.Empty))
                        {
                            TEN_TVAN = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.TEN_TVAN;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.NGAY_GUI != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.NGAY_GUI.Equals(string.Empty))
                        {
                            NGAY_GUI = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.NGAY_GUI;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.STK_NGHANG != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.STK_NGHANG.Equals(string.Empty))
                        {
                            STK_NHANG = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.DKY_NTHUE.STK_NGHANG;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.Signature != null)
                    {
                        if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.Signature.Count > 0)
                        {
                            if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.Signature[0].Equals(string.Empty))
                            {
                                SIGNATURE_NNT = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.Signature[0];
                            }
                        }
                        if (msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.Signature.Count > 1)
                        {
                            if (!msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.Signature[1].Equals(string.Empty))
                            {
                                SIGNATURE_TCT = msg06006.BODY.ROW.GIAODICH.DKY_NTHUE_NH.Signature[1];
                            }
                        }
                    }
                    TRANG_THAI = "00";

                    if (msg06006.BODY.ROW.GIAODICH.MA_GDICH != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.MA_GDICH.Equals(string.Empty))
                        {
                            MA_GDICH = msg06006.BODY.ROW.GIAODICH.MA_GDICH;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.NGAY_GUI_GDICH != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.NGAY_GUI_GDICH.Equals(string.Empty))
                        {
                            NGAY_GUI_GDICH = msg06006.BODY.ROW.GIAODICH.NGAY_GUI_GDICH;
                        }
                    }
                    if (msg06006.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU != null)
                    {
                        if (!msg06006.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU.Equals(string.Empty))
                        {
                            MA_GDICH_TCHIEU = msg06006.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU;
                        }
                    }

                    string NNTDT_ID = Utility.TCS_GetSequence("tcs_dm_nntdt_seq").ToString(); 
                    
                    NGAY_TAO = Utility.ConvertDateToNumber(DateTime.Now.ToString("dd/MM/yyyy")).ToString();

                    p = new IDbDataParameter[25];

                    p[0] = new OracleParameter();
                    p[0].ParameterName = "PBAN_TLIEU_XML";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = PBAN_TLIEU_XML;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "MST";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = MST;


                    p[2] = new OracleParameter();
                    p[2].ParameterName = "TEN_NNT";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = TEN_NNT;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "DIACHI_NNT";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = DIACHI_NNT;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "MA_CQT";
                    p[4].DbType = DbType.String;
                    p[4].Direction = ParameterDirection.Input;
                    p[4].Value = MA_CQT;

                    p[5] = new OracleParameter();
                    p[5].ParameterName = "EMAIL_NNT";
                    p[5].DbType = DbType.String;
                    p[5].Direction = ParameterDirection.Input;
                    p[5].Value = EMAIL_NNT;

                    p[6] = new OracleParameter();
                    p[6].ParameterName = "SDT_NNT";
                    p[6].DbType = DbType.String;
                    p[6].Direction = ParameterDirection.Input;
                    p[6].Value = SDT_NNT;

                    p[7] = new OracleParameter();
                    p[7].ParameterName = "TEN_LHE_NTHUE";
                    p[7].DbType = DbType.String;
                    p[7].Direction = ParameterDirection.Input;
                    p[7].Value = TEN_LHE_NTHUE;

                    p[8] = new OracleParameter();
                    p[8].ParameterName = "SERIAL_CERT_NTHUE";
                    p[8].DbType = DbType.String;
                    p[8].Direction = ParameterDirection.Input;
                    p[8].Value = SERIAL_CERT_NTHUE;

                    p[9] = new OracleParameter();
                    p[9].ParameterName = "SUBJECT_CERT_NTHUE";
                    p[9].DbType = DbType.String;
                    p[9].Direction = ParameterDirection.Input;
                    p[9].Value = SUBJECT_CERT_NTHUE;
                    

                    p[10] = new OracleParameter();
                    p[10].ParameterName = "ISSUER_CERT_NTHUE";
                    p[10].DbType = DbType.String;
                    p[10].Direction = ParameterDirection.Input;
                    p[10].Value = ISSUER_CERT_NTHUE;

                    p[11] = new OracleParameter();
                    p[11].ParameterName = "MA_NHANG";
                    p[11].DbType = DbType.String;
                    p[11].Direction = ParameterDirection.Input;
                    p[11].Value = MA_NHANG;

                    p[12] = new OracleParameter();
                    p[12].ParameterName = "TEN_NHANG";
                    p[12].DbType = DbType.String;
                    p[12].Direction = ParameterDirection.Input;
                    p[12].Value = TEN_NHANG;

                    p[13] = new OracleParameter();
                    p[13].ParameterName = "VAN_ID";
                    p[13].DbType = DbType.String;
                    p[13].Direction = ParameterDirection.Input;
                    p[13].Value = VAN_ID;

                    p[14] = new OracleParameter();
                    p[14].ParameterName = "TEN_TVAN";
                    p[14].DbType = DbType.String;
                    p[14].Direction = ParameterDirection.Input;
                    p[14].Value = TEN_TVAN;

                    p[15] = new OracleParameter();
                    p[15].ParameterName = "NGAY_GUI";
                    p[15].DbType = DbType.String;
                    p[15].Direction = ParameterDirection.Input;
                    p[15].Value = NGAY_GUI;

                    p[16] = new OracleParameter();
                    p[16].ParameterName = "STK_NHANG";
                    p[16].DbType = DbType.String;
                    p[16].Direction = ParameterDirection.Input;
                    p[16].Value = STK_NHANG;

                    p[17] = new OracleParameter();
                    p[17].ParameterName = "SIGNATURE_NNT";
                    p[17].DbType = DbType.String;
                    p[17].Direction = ParameterDirection.Input;
                    p[17].Value = SIGNATURE_NNT;

                    p[18] = new OracleParameter();
                    p[18].ParameterName = "SIGNATURE_TCT";
                    p[18].DbType = DbType.String;
                    p[18].Direction = ParameterDirection.Input;
                    p[18].Value = SIGNATURE_TCT;

                    p[19] = new OracleParameter();
                    p[19].ParameterName = "TRANG_THAI";
                    p[19].DbType = DbType.String;
                    p[19].Direction = ParameterDirection.Input;
                    p[19].Value = TRANG_THAI;

                    p[20] = new OracleParameter();
                    p[20].ParameterName = "NGAY_TAO";
                    p[20].DbType = DbType.String;
                    p[20].Direction = ParameterDirection.Input;
                    p[20].Value = NGAY_TAO;

                    p[21] = new OracleParameter();
                    p[21].ParameterName = "MA_GDICH";
                    p[21].DbType = DbType.String;
                    p[21].Direction = ParameterDirection.Input;
                    p[21].Value = MA_GDICH;

                    p[22] = new OracleParameter();
                    p[22].ParameterName = "NGAY_GUI_GDICH";
                    p[22].DbType = DbType.String;
                    p[22].Direction = ParameterDirection.Input;
                    p[22].Value = NGAY_GUI_GDICH;

                    p[23] = new OracleParameter();
                    p[23].ParameterName = "MA_GDICH_TCHIEU";
                    p[23].DbType = DbType.String;
                    p[23].Direction = ParameterDirection.Input;
                    p[23].Value = MA_GDICH_TCHIEU;

                    p[24] = new OracleParameter();
                    p[24].ParameterName = "NNTDT_ID";
                    p[24].DbType = DbType.String;
                    p[24].Direction = ParameterDirection.Input;
                    p[24].Value = NNTDT_ID;

                    VBOracleLib.DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    //}
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
             
                conn.Close();
                conn.Dispose();
            }

        }

        #endregion
    }
}
