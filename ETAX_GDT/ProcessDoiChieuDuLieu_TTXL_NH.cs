﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG;
using GDT.MSG.MSG07000;
using GDT.MSG.MSG07000_Lite;
using GDT.MSG.MSG07000_NDUNG_DC;
using System.Data.OracleClient;
using System.Data;
using GDT.MSG.MSG06004;
using VBOracleLib;
using GDT.MSG.MSGHeader;
using System.Globalization;

namespace ETAX_GDT
{
    
    class ProcessDoiChieuDuLieu_TTXL_NH
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string KHCT = "SHB";
        private string strTran_Type = "";
        private string strMSG_TEXT = "";
        public ProcessDoiChieuDuLieu_TTXL_NH()
        {
           
        }

        public void Process(MSG msg, bool checkCK)
        {
            try
            {
                if (msg != null)
                {
                    string xmlIn = "";
                    xmlIn = msg.content_mess;

                    if (xmlIn.Length > 0)
                    {
                        
                        string nd_dc = "";
                        string xmlnd_dc = "";
                        xmlIn = xmlIn.Replace("<DATA>", Utility.strXMLdefin2);
                        MSG07000_Lite msgLite = new MSG07000_Lite();
                        MSG07000_Lite objLite = null;
                        try
                        {
                            objLite = msgLite.MSGToObject(xmlIn);
                            if (objLite != null)
                            {
                                GDT.MSG.MSG07000_Lite.GIAODICH_DC GIAODICH_DC = objLite.BODY.ROW.GIAODICH_DC;

                                MSGHeader msgheader = new MSGHeader();
                                msgheader.HEADER = new GDT.MSG.MSGHeader.HEADER();

                                msgheader.HEADER.ERROR_CODE = objLite.HEADER.ERROR_CODE;
                                msgheader.HEADER.ERROR_DESC = objLite.HEADER.ERROR_DESC;
                                msgheader.HEADER.ID_LINK = objLite.HEADER.ID_LINK;
                                msgheader.HEADER.MSG_ID = objLite.HEADER.MSG_ID;
                                msgheader.HEADER.MSG_REFID = objLite.HEADER.MSG_REFID;
                                msgheader.HEADER.ORIGINAL_CODE = objLite.HEADER.ORIGINAL_CODE;
                                msgheader.HEADER.ORIGINAL_DATE = objLite.HEADER.ORIGINAL_DATE;
                                msgheader.HEADER.ORIGINAL_NAME = objLite.HEADER.ORIGINAL_NAME;
                                msgheader.HEADER.RECEIVER_CODE = objLite.HEADER.RECEIVER_CODE;
                                msgheader.HEADER.RECEIVER_NAME = objLite.HEADER.RECEIVER_NAME;
                                msgheader.HEADER.SEND_DATE = objLite.HEADER.SEND_DATE;
                                msgheader.HEADER.SENDER_CODE = objLite.HEADER.SENDER_CODE;
                                msgheader.HEADER.SENDER_NAME = objLite.HEADER.SENDER_NAME;
                                msgheader.HEADER.SPARE1 = objLite.HEADER.SPARE1;
                                msgheader.HEADER.SPARE2 = objLite.HEADER.SPARE2;
                                msgheader.HEADER.SPARE3 = objLite.HEADER.SPARE3;
                                msgheader.HEADER.TRAN_CODE = objLite.HEADER.TRAN_CODE;
                                msgheader.HEADER.VERSION = objLite.HEADER.VERSION;
                                if (checkCK == false)
                                {
                                    Utility.replyReceivedMSG(msgheader, objLite.BODY.ROW.GIAODICH_DC.MA_GDICH_DC, "02", "05 - Sai chữ ký số");
                                }
                                else 
                                if (GIAODICH_DC.DEN_NGAY_DC == null || GIAODICH_DC.MA_GDICH_DC == null || GIAODICH_DC.MA_GDICH_TCHIEU_DC == null || GIAODICH_DC.MA_NHANG_DC == null || GIAODICH_DC.NDUNG_DC == null || GIAODICH_DC.NGAY_DC == null || GIAODICH_DC.NGAY_GDICH_DC == null || GIAODICH_DC.PBAN_TLIEU_XML_DC == null || GIAODICH_DC.TU_NGAY_DC == null)
                                {

                                    //phản hồi nhận dữ liệu bị lỗi cấu trúc cho tct
                                    Utility.replyReceivedMSG(msgheader, objLite.BODY.ROW.GIAODICH_DC.MA_GDICH_DC, "02", "07 - Cấu trúc đối chiếu dữ liệu không đúng khuôn dạng");

                                }
                                else if (objLite.BODY.ROW.GIAODICH_DC.NDUNG_DC != null)
                                {

                                    nd_dc = objLite.BODY.ROW.GIAODICH_DC.NDUNG_DC;


                                    if (nd_dc.Length > 0)
                                    {
                                        nd_dc = Utility.decompress(nd_dc).Replace("\0", "");

                                        GDT.MSG.MSG07000.NDUNG_DC objNDDC = new GDT.MSG.MSG07000.NDUNG_DC().MSGToObject(nd_dc);

                                        if (objNDDC != null)
                                        {
                                            ThemDoiChieuDL(objLite, objNDDC);
                                        }
                                        strTran_Type = "MSG07000_NDUNG_DC";
                                        strMSG_TEXT = "Process=Time:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "ThemDoiChieuDL=";
                                        Utility.Log_Action(strTran_Type, strMSG_TEXT);
                                    }

                                    //phản hồi đã nhận dữ liệu
                                    Utility.replyReceivedMSG(msgheader, objLite.BODY.ROW.GIAODICH_DC.MA_GDICH_DC, "01", "Thành công");

                                    //Sau khi đã nhận dữ liệu đối chiếu thì gửi luôn báo cáo.
                                    ProcessBaoCaoDoiChieu_NH_TTXL baocaodc = new ProcessBaoCaoDoiChieu_NH_TTXL();
                                    int tong1 = 0;
                                    int tong2 = 0;
                                    int tong3 = 0;
                                    int tong4 = 0;
                                    int tong5 = 0;
                                    string errc = "";
                                    string errm = "";
                                    baocaodc.BaoCaoDoiChieu(objLite.BODY.ROW.GIAODICH_DC.NGAY_DC.ToString(), ref tong1, ref tong2, ref tong3, ref tong4, ref tong5, ref errc, ref errm, objLite.HEADER.MSG_ID);

                                }

                            }
                        }
                        catch (Exception e)
                        {

                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);

                strTran_Type = "MSG07000_NDUNG_DC";
                strMSG_TEXT = "Process=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "Exception=" + ex.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            }
        }

        #region "Them giao dich doi chieu du lieu"

        public static void ThemDoiChieuDL(MSG07000_Lite msgDC, GDT.MSG.MSG07000.NDUNG_DC msgND)
        {

            //sonmt: Kiểm tra nếu gói đối chiếu cùng ngày thì xóa dữ liệu cũ của ngày đó đi
            try
            {
                DataTable dtqly = DataAccess.ExecuteReturnDataSet("select ma_gdich_dc from tcs_qly_dc_ntdt where to_char(ngay_dc, 'dd/MM/yyyy') = '" + msgDC.BODY.ROW.GIAODICH_DC.NGAY_DC + "'", CommandType.Text).Tables[0];
                if (dtqly.Rows.Count > 0)
                {
                    string ma_gdich_dc = "";
                    ma_gdich_dc = dtqly.Rows[0]["ma_gdich_dc"].ToString();
                    if (ma_gdich_dc.Trim().Length > 0)
                    {
                        DataTable dtdc_hdr = DataAccess.ExecuteReturnDataSet("select * from tcs_dchieu_ctu_ntdt_hdr where ma_gdich_dc = '" + ma_gdich_dc + "'", CommandType.Text).Tables[0];
                        if (dtdc_hdr.Rows.Count > 0)
                        {
                            string ct_dc_list = "";
                            for (int i = 0; i < dtdc_hdr.Rows.Count; i++)
                            {
                                ct_dc_list += "'" + dtdc_hdr.Rows[i]["so_ctu"].ToString() + "',";
                            }
                            if (ct_dc_list.Length > 0)
                            {
                                ct_dc_list = ct_dc_list.Remove(ct_dc_list.Length - 1, 1);

                                //xoa bang chung tu doi chieu chi tiet
                                DataAccess.ExecuteNonQuery("delete tcs_dchieu_ctu_ntdt_dtl where id_ctu in (" + ct_dc_list + ")", CommandType.Text);

                                //xoa bang chung tu doi chieu hdr
                                DataAccess.ExecuteNonQuery("delete tcs_dchieu_ctu_ntdt_hdr where so_ctu in (" + ct_dc_list + ")", CommandType.Text);

                            }

                        }
                    }

                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);

                string strTran_Type1 = "ThemDoiChieuDL_XOA_DL_DC_CU";
                string strMSG_TEXT1 = "Process=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "Exception=" + ex.Message.ToString();
                Utility.Log_Action(strTran_Type1, strMSG_TEXT1);
            }

            if (msgDC != null)
            {
                OracleConnection conn = new OracleConnection();
                IDbTransaction transCT = null;
                //Tao cau lenh Insert
                StringBuilder sbSqlInsert = new StringBuilder("");

                sbSqlInsert.Append("INSERT INTO tcs_qly_dc_ntdt (PBAN_TLIEU_XML_DC, ");
                sbSqlInsert.Append("MA_GDICH_DC, ");
                sbSqlInsert.Append("NGAY_GDICH_DC, ");
                sbSqlInsert.Append("MA_NHANG_DC, ");
                sbSqlInsert.Append("NGAY_DC, ");
                sbSqlInsert.Append("TU_NGAY_DC,");
                sbSqlInsert.Append("DEN_NGAY_DC,");
                sbSqlInsert.Append("MA_GDICH_TCHIEU_DC, ID_QLDC_NTDT)");

                sbSqlInsert.Append(" VALUES( :PBAN_TLIEU_XML_DC, ");
                sbSqlInsert.Append(":MA_GDICH_DC, ");
                sbSqlInsert.Append(":NGAY_GDICH_DC, ");
                sbSqlInsert.Append(":MA_NHANG_DC, ");
                sbSqlInsert.Append(":NGAY_DC, ");
                sbSqlInsert.Append(":TU_NGAY_DC, ");
                sbSqlInsert.Append(":DEN_NGAY_DC, ");
                sbSqlInsert.Append(":MA_GDICH_TCHIEU_DC, :ID_QLDC_NTDT)");

                try
                {
                    conn = DataAccess.GetConnection();
                    transCT = conn.BeginTransaction();

                    if (msgDC.BODY.ROW.GIAODICH_DC == null)
                    {
                        throw new Exception("Lỗi lấy thông tin giao dịch đối chiếu");
                    }
                    if (msgDC.BODY.ROW.GIAODICH_DC != null)
                    {

                        ////Xoa du lieu truoc
                        ////Khai bao cac cau lenh
                        //StringBuilder sbSqlDelete = new StringBuilder("");
                        IDbDataParameter[] p = new IDbDataParameter[0];
                        //sbSqlDelete = new StringBuilder("");
                        //sbSqlDelete.Append("DELETE FROM tcs_qly_dc_ntdt");
                        //p = new IDbDataParameter[0];
                        //Utility.ExecuteNonQuery(sbSqlDelete.ToString(), CommandType.Text, p, transCT);

                        //Gan cac thong tin
                        string PBAN_TLIEU_XML_DC = DBNull.Value.ToString();
                        string MA_GDICH_DC = DBNull.Value.ToString();
                        string NGAY_GDICH_DC = DBNull.Value.ToString();
                        string MA_NHANG_DC = DBNull.Value.ToString();
                        string NGAY_DC = DBNull.Value.ToString();
                        string TU_NGAY_DC = DBNull.Value.ToString();
                        string DEN_NGAY_DC = DBNull.Value.ToString();
                        string MA_GDICH_TCHIEU_DC = DBNull.Value.ToString();

                        string QLY_DC_NTDT_SEQ = Utility.TCS_GetSequence("TCS_QLY_DC_NTDT_SEQ").ToString();

                        if (msgDC.BODY.ROW.GIAODICH_DC.PBAN_TLIEU_XML_DC != null)
                        {
                            if (!msgDC.BODY.ROW.GIAODICH_DC.PBAN_TLIEU_XML_DC.Equals(string.Empty))
                            {
                                PBAN_TLIEU_XML_DC = msgDC.BODY.ROW.GIAODICH_DC.PBAN_TLIEU_XML_DC;
                            }
                        }
                        if (msgDC.BODY.ROW.GIAODICH_DC.MA_GDICH_DC != null)
                        {
                            if (!msgDC.BODY.ROW.GIAODICH_DC.MA_GDICH_DC.Equals(string.Empty))
                            {
                                MA_GDICH_DC = msgDC.BODY.ROW.GIAODICH_DC.MA_GDICH_DC;
                            }
                        }
                        if (msgDC.BODY.ROW.GIAODICH_DC.NGAY_GDICH_DC != null)
                        {
                            if (!msgDC.BODY.ROW.GIAODICH_DC.NGAY_GDICH_DC.Equals(string.Empty))
                            {
                                NGAY_GDICH_DC = msgDC.BODY.ROW.GIAODICH_DC.NGAY_GDICH_DC;
                            }
                        }
                        if (msgDC.BODY.ROW.GIAODICH_DC.MA_NHANG_DC != null)
                        {
                            if (!msgDC.BODY.ROW.GIAODICH_DC.MA_NHANG_DC.Equals(string.Empty))
                            {
                                MA_NHANG_DC = msgDC.BODY.ROW.GIAODICH_DC.MA_NHANG_DC;
                            }
                        }
                        if (msgDC.BODY.ROW.GIAODICH_DC.NGAY_DC != null)
                        {
                            if (!msgDC.BODY.ROW.GIAODICH_DC.NGAY_DC.Equals(string.Empty))
                            {
                                NGAY_DC = msgDC.BODY.ROW.GIAODICH_DC.NGAY_DC;
                            }
                        }
                        if (msgDC.BODY.ROW.GIAODICH_DC.TU_NGAY_DC != null)
                        {
                            if (!msgDC.BODY.ROW.GIAODICH_DC.TU_NGAY_DC.Equals(string.Empty))
                            {
                                TU_NGAY_DC = msgDC.BODY.ROW.GIAODICH_DC.TU_NGAY_DC;
                            }
                        }
                        if (msgDC.BODY.ROW.GIAODICH_DC.DEN_NGAY_DC != null)
                        {
                            if (!msgDC.BODY.ROW.GIAODICH_DC.DEN_NGAY_DC.Equals(string.Empty))
                            {
                                DEN_NGAY_DC = msgDC.BODY.ROW.GIAODICH_DC.DEN_NGAY_DC;
                            }
                        }
                        if (msgDC.BODY.ROW.GIAODICH_DC.MA_GDICH_TCHIEU_DC != null)
                        {
                            if (!msgDC.BODY.ROW.GIAODICH_DC.MA_GDICH_TCHIEU_DC.Equals(string.Empty))
                            {
                                MA_GDICH_TCHIEU_DC = msgDC.BODY.ROW.GIAODICH_DC.MA_GDICH_TCHIEU_DC;
                            }
                        }

                        p = new IDbDataParameter[9];
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "PBAN_TLIEU_XML_DC";
                        p[0].DbType = DbType.String;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = DBNull.Value;
                        p[0].Value = PBAN_TLIEU_XML_DC;

                        p[1] = new OracleParameter();
                        p[1].ParameterName = "MA_GDICH_DC";
                        p[1].DbType = DbType.String;
                        p[1].Direction = ParameterDirection.Input;
                        p[1].Value = MA_GDICH_DC;

                        p[2] = new OracleParameter();
                        p[2].ParameterName = "NGAY_GDICH_DC";
                        p[2].DbType = DbType.String;
                        p[2].Direction = ParameterDirection.Input;
                        p[2].Value = NGAY_GDICH_DC;

                        p[3] = new OracleParameter();
                        p[3].ParameterName = "MA_NHANG_DC";
                        p[3].DbType = DbType.String;
                        p[3].Direction = ParameterDirection.Input;
                        p[3].Value = MA_NHANG_DC;

                        p[4] = new OracleParameter();
                        p[4].ParameterName = "NGAY_DC";
                        p[4].DbType = DbType.Date;
                        p[4].Direction = ParameterDirection.Input;
                        p[4].Value = NGAY_DC;

                        if (NGAY_DC != null && NGAY_DC != "")
                        {
                            p[4].Value = DateTime.ParseExact(NGAY_DC, "dd/MM/yyyy", null); ;
                        }
                        else
                        {
                            p[4].Value = DBNull.Value;
                        }


                        p[5] = new OracleParameter();
                        p[5].ParameterName = "TU_NGAY_DC";
                        p[5].DbType = DbType.String;
                        p[5].Direction = ParameterDirection.Input;
                        p[5].Value = TU_NGAY_DC;

                        p[6] = new OracleParameter();
                        p[6].ParameterName = "DEN_NGAY_DC";
                        p[6].DbType = DbType.String;
                        p[6].Direction = ParameterDirection.Input;
                        p[6].Value = DEN_NGAY_DC;

                        p[7] = new OracleParameter();
                        p[7].ParameterName = "MA_GDICH_TCHIEU_DC";
                        p[7].DbType = DbType.String;
                        p[7].Direction = ParameterDirection.Input;
                        p[7].Value = MA_GDICH_TCHIEU_DC;

                        p[8] = new OracleParameter();
                        p[8].ParameterName = "ID_QLDC_NTDT";
                        p[8].DbType = DbType.String;
                        p[8].Direction = ParameterDirection.Input;
                        p[8].Value = QLY_DC_NTDT_SEQ;

                        DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);


                        //Them du lieu vao bang DC_HDR
                        foreach (GDT.MSG.MSG07000.GIAODICH gd in msgND.GIAODICH)
                        {
                            HDRToDB(gd, msgDC.BODY.ROW.GIAODICH_DC.MA_GDICH_DC, transCT);
                        }

                    }

                    transCT.Commit();

                }
                catch (Exception ex)
                {
                    transCT.Rollback();
                    log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                }
                finally
                {
                    
                    conn.Close();
                    conn.Dispose();
                }

            }


        }

        private static void HDRToDB(GDT.MSG.MSG07000.GIAODICH msgGDDC, string ma_gddc, IDbTransaction transCT)
        {
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO TCS_DCHIEU_CTU_NTDT_HDR (MA_GDICH, ");
            sbSqlInsert.Append("NGAY_GUI_GDICH,");
            sbSqlInsert.Append("NGAY_NOP_GNT,");
            sbSqlInsert.Append("NGAY_NOP_THUE,");
            sbSqlInsert.Append("TTHAI_GDICH,");
            sbSqlInsert.Append("TTHAI_CTU,");
            sbSqlInsert.Append("MAHIEU_CTU,");
            sbSqlInsert.Append("SO_CTU,");
            sbSqlInsert.Append("PBAN_TLIEU_XML,");
            sbSqlInsert.Append("ID_CTU,");
            sbSqlInsert.Append("SO_GNT,");
            sbSqlInsert.Append("MA_CTU,");
            sbSqlInsert.Append("HTHUC_NOP,");
            sbSqlInsert.Append("MST_NNOP,");
            sbSqlInsert.Append("TEN_NNOP,");
            sbSqlInsert.Append("MA_CQT,");
            sbSqlInsert.Append("TEN_CQT,");
            sbSqlInsert.Append("DIACHI_NNOP,");
            sbSqlInsert.Append("MA_XA_NNOP,");
            sbSqlInsert.Append("TEN_XA_NNOP,");
            sbSqlInsert.Append("MA_HUYEN_NNOP,");
            sbSqlInsert.Append("TEN_HUYEN_NNOP,");
            sbSqlInsert.Append("MA_TINH_NNOP,");
            sbSqlInsert.Append("TEN_TINH_NNOP,");
            sbSqlInsert.Append("MST_NTHAY,");
            sbSqlInsert.Append("TEN_NTHAY,");
            sbSqlInsert.Append("DIACHI_NTHAY,");
            sbSqlInsert.Append("TEN_HUYEN_NTHAY,");
            sbSqlInsert.Append("TEN_TINH_NTHAY,");
            sbSqlInsert.Append("MA_NHANG_NOP,");
            sbSqlInsert.Append("TEN_NHANG_NOP,");
            sbSqlInsert.Append("STK_NHANG_NOP,");
            sbSqlInsert.Append("MA_HIEU_KBAC,");
            sbSqlInsert.Append("TEN_KBAC,");
            sbSqlInsert.Append("MA_TINH_KBAC,");
            sbSqlInsert.Append("TEN_TINH_KBAC,");
            sbSqlInsert.Append("LOAI_TK_THU,");
            sbSqlInsert.Append("TEN_TK_THU,");
            sbSqlInsert.Append("STK_THU,");
            sbSqlInsert.Append("ID_TK_KNGHI,");
            sbSqlInsert.Append("TK_KNGHI,");
            sbSqlInsert.Append("MA_CQTHU,");
            sbSqlInsert.Append("TEN_CQTHU,");
            sbSqlInsert.Append("NGAY_LAP,");
            sbSqlInsert.Append("TONG_TIEN,");
            sbSqlInsert.Append("VAN_ID,");
            sbSqlInsert.Append("SIGNATUE_NNT,");
            sbSqlInsert.Append("SIGNATUE_TCT,");
            sbSqlInsert.Append("SIGNATUE_NH,");

            // tuanda - 05/02/24
            sbSqlInsert.Append("NGAY_CTU,");
            sbSqlInsert.Append("LHINH_NNT,");

            // tuanda - 04/03/24
            sbSqlInsert.Append("MA_LOAI_THUE,");
            sbSqlInsert.Append("MA_NHANG_UNT,");
            sbSqlInsert.Append("TEN_NHANG_UNT,");

            sbSqlInsert.Append("MA_GDICH_DC, TGIAN_TCT_GUI, TGIAN_NH_NHAN,MA_NGUYENTE,MA_THAMCHIEU)");

            sbSqlInsert.Append(" VALUES(:MA_GDICH,");
            sbSqlInsert.Append(":NGAY_GUI_GDICH,");
            sbSqlInsert.Append(":NGAY_NOP_GNT,");
            sbSqlInsert.Append(":NGAY_NOP_THUE,");
            sbSqlInsert.Append(":TTHAI_GDICH,");
            sbSqlInsert.Append(":TTHAI_CTU,");
            sbSqlInsert.Append(":MAHIEU_CTU,");
            sbSqlInsert.Append(":SO_CTU,");
            sbSqlInsert.Append(":PBAN_TLIEU_XML,");
            sbSqlInsert.Append(":ID_CTU,");
            sbSqlInsert.Append(":SO_GNT,");
            sbSqlInsert.Append(":MA_CTU,");
            sbSqlInsert.Append(":HTHUC_NOP,");
            sbSqlInsert.Append(":MST_NNOP,");
            sbSqlInsert.Append(":TEN_NNOP,");
            sbSqlInsert.Append(":MA_CQT,");
            sbSqlInsert.Append(":TEN_CQT,");
            sbSqlInsert.Append(":DIACHI_NNOP,");
            sbSqlInsert.Append(":MA_XA_NNOP,");
            sbSqlInsert.Append(":TEN_XA_NNOP,");
            sbSqlInsert.Append(":MA_HUYEN_NNOP,");
            sbSqlInsert.Append(":TEN_HUYEN_NNOP,");
            sbSqlInsert.Append(":MA_TINH_NNOP,");
            sbSqlInsert.Append(":TEN_TINH_NNOP,");
            sbSqlInsert.Append(":MST_NTHAY,");
            sbSqlInsert.Append(":TEN_NTHAY,");
            sbSqlInsert.Append(":DIACHI_NTHAY,");
            sbSqlInsert.Append(":TEN_HUYEN_NTHAY,");
            sbSqlInsert.Append(":TEN_TINH_NTHAY,");
            sbSqlInsert.Append(":MA_NHANG_NOP,");
            sbSqlInsert.Append(":TEN_NHANG_NOP,");
            sbSqlInsert.Append(":STK_NHANG_NOP,");
            sbSqlInsert.Append(":MA_HIEU_KBAC,");
            sbSqlInsert.Append(":TEN_KBAC,");
            sbSqlInsert.Append(":MA_TINH_KBAC,");
            sbSqlInsert.Append(":TEN_TINH_KBAC,");
            sbSqlInsert.Append(":LOAI_TK_THU,");
            sbSqlInsert.Append(":TEN_TK_THU,");
            sbSqlInsert.Append(":STK_THU,");
            sbSqlInsert.Append(":ID_TK_KNGHI,");
            sbSqlInsert.Append(":TK_KNGHI,");
            sbSqlInsert.Append(":MA_CQTHU,");
            sbSqlInsert.Append(":TEN_CQTHU,");
            sbSqlInsert.Append(":NGAY_LAP,");
            sbSqlInsert.Append(":TONG_TIEN,");
            sbSqlInsert.Append(":VAN_ID,");
            sbSqlInsert.Append(":SIGNATUE_NNT,");
            sbSqlInsert.Append(":SIGNATUE_TCT,");
            sbSqlInsert.Append(":SIGNATUE_NH,");

            // tuanda - 05/02/24
            sbSqlInsert.Append(":NGAY_CTU,");
            sbSqlInsert.Append(":LHINH_NNT,");

            // tuanda - 04/03/24
            sbSqlInsert.Append(":MA_LOAI_THUE,");
            sbSqlInsert.Append(":MA_NHANG_UNT,");
            sbSqlInsert.Append(":TEN_NHANG_UNT,");

            sbSqlInsert.Append(":MA_GDICH_DC, :TGIAN_TCT_GUI, :TGIAN_NH_NHAN,:MA_NGUYENTE,:MA_THAMCHIEU)");
            try
            {
                GDT.MSG.MSG07000.GIAODICH GIAODICH = msgGDDC;
                GDT.MSG.MSG07000.CHUNGTU CHUNGTU = GIAODICH.CHUNGTU;
                GDT.MSG.MSG07000.NDUNG_CTU_NH NDUNG_CTU_NH = CHUNGTU.NDUNG_CTU_NH;
                GDT.MSG.MSG07000.CHUNGTU_HDR CHUNGTU_HDR = NDUNG_CTU_NH.NDUNG_CTU.CHUNGTU_HDR;
                GDT.MSG.MSG07000.CHUNGTU_CTIET CHUNGTU_CTIET = NDUNG_CTU_NH.NDUNG_CTU.CHUNGTU_CTIET;

                if (GIAODICH != null)
                {
                    IDbDataParameter[] p;

                    //Gan cac thong tin
                    string MA_GDICH = DBNull.Value.ToString();
                    string NGAY_GUI_GDICH = DBNull.Value.ToString();
                    string NGAY_NOP_GNT = DBNull.Value.ToString();
                    string NGAY_NOP_THUE = DBNull.Value.ToString();
                    string TTHAI_GDICH = DBNull.Value.ToString();
                    string TTHAI_CTU = DBNull.Value.ToString();
                    string MAHIEU_CTU = DBNull.Value.ToString();
                    string SO_CTU = DBNull.Value.ToString();
                    string PBAN_TLIEU_XML = DBNull.Value.ToString();
                    string ID_CTU = DBNull.Value.ToString();
                    string SO_GNT = DBNull.Value.ToString();
                    string MA_CTU = DBNull.Value.ToString();
                    string HTHUC_NOP = DBNull.Value.ToString();
                    string MST_NNOP = DBNull.Value.ToString();
                    string TEN_NNOP = DBNull.Value.ToString();
                    string MA_CQT = DBNull.Value.ToString();
                    string TEN_CQT = DBNull.Value.ToString();
                    string DIACHI_NNOP = DBNull.Value.ToString();
                    string MA_XA_NNOP = DBNull.Value.ToString();
                    string TEN_XA_NNOP = DBNull.Value.ToString();
                    string MA_HUYEN_NNOP = DBNull.Value.ToString();
                    string TEN_HUYEN_NNOP = DBNull.Value.ToString();
                    string MA_TINH_NNOP = DBNull.Value.ToString();
                    string TEN_TINH_NNOP = DBNull.Value.ToString();
                    string MST_NTHAY = DBNull.Value.ToString();
                    string TEN_NTHAY = DBNull.Value.ToString();
                    string DIACHI_NTHAY = DBNull.Value.ToString();
                    string TEN_HUYEN_NTHAY = DBNull.Value.ToString();
                    string TEN_TINH_NTHAY = DBNull.Value.ToString();
                    string MA_NHANG_NOP = DBNull.Value.ToString();
                    string TEN_NHANG_NOP = DBNull.Value.ToString();
                    string STK_NHANG_NOP = DBNull.Value.ToString();
                    string MA_HIEU_KBAC = DBNull.Value.ToString();
                    string TEN_KBAC = DBNull.Value.ToString();
                    string MA_TINH_KBAC = DBNull.Value.ToString();
                    string TEN_TINH_KBAC = DBNull.Value.ToString();
                    string LOAI_TK_THU = DBNull.Value.ToString();
                    string TEN_TK_THU = DBNull.Value.ToString();
                    string STK_THU = DBNull.Value.ToString();
                    string ID_TK_KNGHI = DBNull.Value.ToString();
                    string TK_KNGHI = DBNull.Value.ToString();
                    string MA_CQTHU = DBNull.Value.ToString();
                    string TEN_CQTHU = DBNull.Value.ToString();
                    string NGAY_LAP = DBNull.Value.ToString();
                    string TONG_TIEN = DBNull.Value.ToString();
                    string VAN_ID = DBNull.Value.ToString();
                    string SIGNATUE_NNT = DBNull.Value.ToString();
                    string SIGNATUE_TCT = DBNull.Value.ToString();
                    string SIGNATUE_NH = DBNull.Value.ToString();
                    string MA_GDICH_DC = DBNull.Value.ToString();
                    string MA_NGUYENTE = DBNull.Value.ToString();
                    string MA_THAMCHIEU = DBNull.Value.ToString();
                    string tgian_tct_gui = "";
                    string tgian_nh_nhan = "";

                    // tuanda - 05/02/24
                    string NGAY_CTU = DBNull.Value.ToString();
                    string LHINH_NNT = DBNull.Value.ToString();

                    // tuanda - 04/03/24
                    string MA_LOAI_THUE = DBNull.Value.ToString();
                    string MA_NHANG_UNT = DBNull.Value.ToString();
                    string TEN_NHANG_UNT = DBNull.Value.ToString();


                    if (GIAODICH != null)
                    {
                        MA_GDICH = GIAODICH.MA_GDICH;
                        NGAY_GUI_GDICH = GIAODICH.NGAY_GUI_GDICH;
                        NGAY_NOP_GNT = GIAODICH.NGAY_NOP_GNT;
                        TTHAI_GDICH = GIAODICH.TTHAI_GDICH;
                        TTHAI_CTU = GIAODICH.TTHAI_CTU;
                        if (NDUNG_CTU_NH != null)
                        {
                            
                            string str_nam_kb ="";
                            string str_thang_kb ="";
                            string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                            str_nam_kb = datetime.Substring(8,2);
                            str_thang_kb = datetime.Substring(3, 2);

                            if (CHUNGTU_HDR.SO_GNT != null && CHUNGTU_HDR.SO_GNT != "")
                            {
                                try
                                {
                                    //DataTable dtct = DataAccess.ExecuteReturnDataSet("select ngay_ctu, so_ctu, mahieu_ctu, tgian_tct_gui, TO_CHAR(tgian_nh_nhan,'DD/MM/RRRR HH24:MI:SS') from tcs_ctu_ntdt_hdr where so_gnt = '" + CHUNGTU_HDR.SO_GNT + "' order by id_ctu desc ", CommandType.Text).Tables[0];

                                    //tuanda - 20/02/24
                                    DataTable dtct = DataAccess.ExecuteReturnDataSet("select ngay_ctu , so_ctu, mahieu_ctu, tgian_tct_gui, tgian_nh_nhan, NGAY_HT  from tcs_ctu_ntdt_hdr where so_gnt = '" + CHUNGTU_HDR.SO_GNT + "' order by id_ctu desc ", CommandType.Text).Tables[0];

                                    if (dtct.Rows.Count > 0)
                                    {
                                        log.Info("ngay_ht : " + dtct.Rows[0]["NGAY_HT"].ToString());
                                        if (dtct.Rows[0]["NGAY_HT"].ToString().Length > 0)
                                        {
                                            string ngay_ht = dtct.Rows[0]["NGAY_HT"].ToString();
                                            NGAY_NOP_THUE = DateTime.Parse(ngay_ht).ToString("dd/MM/yyyy HH:mm:ss");
                                        }

                                        tgian_tct_gui = dtct.Rows[0]["tgian_tct_gui"].ToString();
                                        tgian_nh_nhan = dtct.Rows[0]["tgian_nh_nhan"].ToString();

                                        SO_CTU = dtct.Rows[0]["so_ctu"].ToString();
                                        MAHIEU_CTU = dtct.Rows[0]["mahieu_ctu"].ToString();

                                        // tuanda - 05/02/24
                                        //NGAY_CTU = dtct.Rows[0]["ngay_ctu"].ToString();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    log.Error(ex.Message + "-" + ex.StackTrace);//
                                    throw;
                                }
                            }

                            // tuanda - 05/02/24
                            if (NDUNG_CTU_NH.SO_CTU != null && NDUNG_CTU_NH.SO_CTU != "" && NDUNG_CTU_NH.MAHIEU_CTU != null && NDUNG_CTU_NH.MAHIEU_CTU != "" && NDUNG_CTU_NH.NGAY_CTU != null && NDUNG_CTU_NH.NGAY_CTU != "")
                            {
                                SO_CTU = NDUNG_CTU_NH.SO_CTU;
                                MAHIEU_CTU = NDUNG_CTU_NH.MAHIEU_CTU;

                                // tuanda - 05/02/24
                                NGAY_CTU = NDUNG_CTU_NH.NGAY_CTU;
                            }
                            else if (SO_CTU != null && SO_CTU != "" && MAHIEU_CTU != null && MAHIEU_CTU != "" && NGAY_CTU != null && NGAY_CTU != "")
                            {

                            }
                            else
                            {
                                SO_CTU = str_nam_kb + str_thang_kb + Utility.TCS_GetSequence("DC_NTDT_SQ_SO_CT").ToString().PadLeft(5, '0').ToString();
                                MAHIEU_CTU = KHCT + SO_CTU;

                                // tuanda - 05/02/24
                                //NGAY_CTU = DateTime.Now.ToString();
                            }

                            if (CHUNGTU_HDR != null)
                            {
                                if (CHUNGTU_HDR.PBAN_TLIEU_XML != null)
                                {
                                    PBAN_TLIEU_XML = CHUNGTU_HDR.PBAN_TLIEU_XML;
                                }
                                if (CHUNGTU_HDR.ID_CTU != null)
                                {
                                    ID_CTU = CHUNGTU_HDR.ID_CTU;
                                }
                                if (CHUNGTU_HDR.SO_GNT != null)
                                {
                                    SO_GNT = CHUNGTU_HDR.SO_GNT;
                                }
                                if (CHUNGTU_HDR.MA_CTU != null)
                                {
                                    MA_CTU = CHUNGTU_HDR.MA_CTU;
                                }
                                if (CHUNGTU_HDR.HTHUC_NOP != null)
                                {
                                    HTHUC_NOP = CHUNGTU_HDR.HTHUC_NOP;
                                }
                                if (CHUNGTU_HDR.MST_NNOP != null)
                                {
                                    MST_NNOP = CHUNGTU_HDR.MST_NNOP;
                                }
                                if (CHUNGTU_HDR.TEN_NNOP != null)
                                {
                                    TEN_NNOP = CHUNGTU_HDR.TEN_NNOP;
                                }
                                if (CHUNGTU_HDR.MA_CQT != null)
                                {
                                    MA_CQT = CHUNGTU_HDR.MA_CQT;
                                }
                                if (CHUNGTU_HDR.TEN_CQT != null)
                                {
                                    TEN_CQT = CHUNGTU_HDR.TEN_CQT;
                                }
                                if (CHUNGTU_HDR.DIACHI_NNOP != null)
                                {
                                    DIACHI_NNOP = CHUNGTU_HDR.DIACHI_NNOP;
                                }
                                if (CHUNGTU_HDR.MA_XA_NNOP != null)
                                {
                                    MA_XA_NNOP = CHUNGTU_HDR.MA_XA_NNOP;
                                }
                                if (CHUNGTU_HDR.TEN_XA_NNOP != null)
                                {
                                    TEN_XA_NNOP = CHUNGTU_HDR.TEN_XA_NNOP;
                                }
                                if (CHUNGTU_HDR.MA_HUYEN_NNOP != null)
                                {
                                    MA_HUYEN_NNOP = CHUNGTU_HDR.MA_HUYEN_NNOP;
                                }
                                if (CHUNGTU_HDR.TEN_HUYEN_NNOP != null)
                                {
                                    TEN_HUYEN_NNOP = CHUNGTU_HDR.TEN_HUYEN_NNOP;
                                }
                                if (CHUNGTU_HDR.MA_TINH_NNOP != null)
                                {
                                    MA_TINH_NNOP = CHUNGTU_HDR.MA_TINH_NNOP;
                                }

                                if (CHUNGTU_HDR.TEN_TINH_NNOP != null)
                                {
                                    TEN_TINH_NNOP = CHUNGTU_HDR.TEN_TINH_NNOP;
                                }

                                if (CHUNGTU_HDR.MST_NTHAY != null)
                                {
                                    MST_NTHAY = CHUNGTU_HDR.MST_NTHAY;
                                }
                                if (CHUNGTU_HDR.TEN_NTHAY != null)
                                {
                                    TEN_NTHAY = CHUNGTU_HDR.TEN_NTHAY;
                                }
                                if (CHUNGTU_HDR.DIACHI_NTHAY != null)
                                {
                                    DIACHI_NTHAY = CHUNGTU_HDR.DIACHI_NTHAY;
                                }
                                if (CHUNGTU_HDR.TEN_HUYEN_NTHAY != null)
                                {
                                    TEN_HUYEN_NTHAY = CHUNGTU_HDR.TEN_HUYEN_NTHAY;
                                }
                                if (CHUNGTU_HDR.TEN_TINH_NTHAY != null)
                                {
                                    TEN_TINH_NTHAY = CHUNGTU_HDR.TEN_TINH_NTHAY;
                                }
                                if (CHUNGTU_HDR.MA_NHANG_NOP != null)
                                {
                                    MA_NHANG_NOP = CHUNGTU_HDR.MA_NHANG_NOP;
                                }
                                if (CHUNGTU_HDR.TEN_NHANG_NOP != null)
                                {
                                    TEN_NHANG_NOP = CHUNGTU_HDR.TEN_NHANG_NOP;
                                }
                                if (CHUNGTU_HDR.STK_NHANG_NOP != null)
                                {
                                    STK_NHANG_NOP = CHUNGTU_HDR.STK_NHANG_NOP;
                                }
                                if (CHUNGTU_HDR.MA_HIEU_KBAC != null)
                                {
                                    MA_HIEU_KBAC = CHUNGTU_HDR.MA_HIEU_KBAC;
                                }

                                if (CHUNGTU_HDR.TEN_KBAC != null)
                                {
                                    TEN_KBAC = CHUNGTU_HDR.TEN_KBAC;
                                }
                                if (CHUNGTU_HDR.MA_TINH_KBAC != null)
                                {
                                    MA_TINH_KBAC = CHUNGTU_HDR.MA_TINH_KBAC;
                                }
                                if (CHUNGTU_HDR.TEN_TINH_KBAC != null)
                                {
                                    TEN_TINH_KBAC = CHUNGTU_HDR.TEN_TINH_KBAC;
                                }
                                if (CHUNGTU_HDR.LOAI_TK_THU != null)
                                {
                                    LOAI_TK_THU = CHUNGTU_HDR.LOAI_TK_THU;
                                }
                                if (CHUNGTU_HDR.TEN_TK_THU != null)
                                {
                                    TEN_TK_THU = CHUNGTU_HDR.TEN_TK_THU;
                                }
                                if (CHUNGTU_HDR.STK_THU != null)
                                {
                                    STK_THU = CHUNGTU_HDR.STK_THU;
                                }
                                if (CHUNGTU_HDR.ID_TK_KNGHI != null)
                                {
                                    ID_TK_KNGHI = CHUNGTU_HDR.ID_TK_KNGHI;
                                }
                                if (CHUNGTU_HDR.TK_KNGHI != null)
                                {
                                    TK_KNGHI = CHUNGTU_HDR.TK_KNGHI;
                                }
                                if (CHUNGTU_HDR.MA_CQTHU != null)
                                {
                                    MA_CQTHU = CHUNGTU_HDR.MA_CQTHU;
                                }
                                if (CHUNGTU_HDR.TEN_CQTHU != null)
                                {
                                    TEN_CQTHU = CHUNGTU_HDR.TEN_CQTHU;
                                }
                                if (CHUNGTU_HDR.NGAY_LAP != null)
                                {
                                    NGAY_LAP = CHUNGTU_HDR.NGAY_LAP;
                                }
                                if (CHUNGTU_HDR.TONG_TIEN != null)
                                {
                                    TONG_TIEN = CHUNGTU_HDR.TONG_TIEN;
                                }
                                if (CHUNGTU_HDR.VAN_ID != null)
                                {
                                    VAN_ID = CHUNGTU_HDR.VAN_ID;
                                }
                                if (CHUNGTU_HDR.MA_NGUYENTE != null)
                                {
                                    MA_NGUYENTE = CHUNGTU_HDR.MA_NGUYENTE;
                                }
                                if (CHUNGTU_HDR.MA_THAMCHIEU != null)
                                {
                                    MA_THAMCHIEU = CHUNGTU_HDR.MA_THAMCHIEU;
                                }

                                MA_GDICH_DC = ma_gddc;

                                // tuanda - 05/02/24
                                if (CHUNGTU_HDR.LHINH_NNT != null)
                                {
                                    LHINH_NNT = CHUNGTU_HDR.LHINH_NNT;
                                }

                                // tuanda - 04/03/24
                                if (CHUNGTU_HDR.MA_LOAI_THUE != null)
                                {
                                    MA_LOAI_THUE = CHUNGTU_HDR.MA_LOAI_THUE;
                                }
                                if (CHUNGTU_HDR.MA_NHANG_UNT != null)
                                {
                                    MA_NHANG_UNT = CHUNGTU_HDR.MA_NHANG_UNT;
                                }
                                if (CHUNGTU_HDR.TEN_NHANG_UNT != null)
                                {
                                    TEN_NHANG_UNT = CHUNGTU_HDR.TEN_NHANG_UNT;
                                }
                            }

                        }
                        else
                        {
                            //Lỗi cấu trúc xml
                        }
                    }
                    else
                    {
                        //Lỗi cấu trúc xml
                    }


                    p = new IDbDataParameter[59];

                    p[0] = new OracleParameter();
                    p[0].ParameterName = "MA_GDICH";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = MA_GDICH;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "NGAY_GUI_GDICH";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = NGAY_GUI_GDICH;


                    p[2] = new OracleParameter();
                    p[2].ParameterName = "NGAY_NOP_GNT";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = NGAY_NOP_GNT;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "NGAY_NOP_THUE";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = NGAY_NOP_THUE;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "TTHAI_GDICH";
                    p[4].DbType = DbType.String;
                    p[4].Direction = ParameterDirection.Input;
                    p[4].Value = TTHAI_GDICH;

                    p[5] = new OracleParameter();
                    p[5].ParameterName = "TTHAI_CTU";
                    p[5].DbType = DbType.String;
                    p[5].Direction = ParameterDirection.Input;
                    p[5].Value = TTHAI_CTU;

                    p[6] = new OracleParameter();
                    p[6].ParameterName = "MAHIEU_CTU";
                    p[6].DbType = DbType.String;
                    p[6].Direction = ParameterDirection.Input;
                    p[6].Value = MAHIEU_CTU;

                    p[7] = new OracleParameter();
                    p[7].ParameterName = "SO_CTU";
                    p[7].DbType = DbType.String;
                    p[7].Direction = ParameterDirection.Input;
                    p[7].Value = SO_CTU;

                    p[8] = new OracleParameter();
                    p[8].ParameterName = "PBAN_TLIEU_XML";
                    p[8].DbType = DbType.String;
                    p[8].Direction = ParameterDirection.Input;
                    p[8].Value = PBAN_TLIEU_XML;

                    p[9] = new OracleParameter();
                    p[9].ParameterName = "ID_CTU";
                    p[9].DbType = DbType.String;
                    p[9].Direction = ParameterDirection.Input;
                    p[9].Value = ID_CTU;


                    p[10] = new OracleParameter();
                    p[10].ParameterName = "SO_GNT";
                    p[10].DbType = DbType.String;
                    p[10].Direction = ParameterDirection.Input;
                    p[10].Value = SO_GNT;

                    p[11] = new OracleParameter();
                    p[11].ParameterName = "MA_CTU";
                    p[11].DbType = DbType.String;
                    p[11].Direction = ParameterDirection.Input;
                    p[11].Value = MA_CTU;

                    p[12] = new OracleParameter();
                    p[12].ParameterName = "HTHUC_NOP";
                    p[12].DbType = DbType.String;
                    p[12].Direction = ParameterDirection.Input;
                    p[12].Value = HTHUC_NOP;

                    p[13] = new OracleParameter();
                    p[13].ParameterName = "MST_NNOP";
                    p[13].DbType = DbType.String;
                    p[13].Direction = ParameterDirection.Input;
                    p[13].Value = MST_NNOP;

                    p[14] = new OracleParameter();
                    p[14].ParameterName = "TEN_NNOP";
                    p[14].DbType = DbType.String;
                    p[14].Direction = ParameterDirection.Input;
                    p[14].Value = TEN_NNOP.Replace("'","");

                    p[15] = new OracleParameter();
                    p[15].ParameterName = "MA_CQT";
                    p[15].DbType = DbType.String;
                    p[15].Direction = ParameterDirection.Input;
                    p[15].Value = MA_CQT;

                    p[16] = new OracleParameter();
                    p[16].ParameterName = "TEN_CQT";
                    p[16].DbType = DbType.String;
                    p[16].Direction = ParameterDirection.Input;
                    p[16].Value = TEN_CQT.Replace("'", "");

                    p[17] = new OracleParameter();
                    p[17].ParameterName = "DIACHI_NNOP";
                    p[17].DbType = DbType.String;
                    p[17].Direction = ParameterDirection.Input;
                    p[17].Value = DIACHI_NNOP.Replace("'", "");

                    p[18] = new OracleParameter();
                    p[18].ParameterName = "MA_XA_NNOP";
                    p[18].DbType = DbType.String;
                    p[18].Direction = ParameterDirection.Input;
                    p[18].Value = MA_XA_NNOP;

                    p[19] = new OracleParameter();
                    p[19].ParameterName = "TEN_XA_NNOP";
                    p[19].DbType = DbType.String;
                    p[19].Direction = ParameterDirection.Input;
                    p[19].Value = TEN_XA_NNOP.Replace("'", "");

                    p[20] = new OracleParameter();
                    p[20].ParameterName = "MA_HUYEN_NNOP";
                    p[20].DbType = DbType.String;
                    p[20].Direction = ParameterDirection.Input;
                    p[20].Value = MA_HUYEN_NNOP;

                    p[21] = new OracleParameter();
                    p[21].ParameterName = "TEN_HUYEN_NNOP";
                    p[21].DbType = DbType.String;
                    p[21].Direction = ParameterDirection.Input;
                    p[21].Value = TEN_HUYEN_NNOP.Replace("'", "");

                    p[22] = new OracleParameter();
                    p[22].ParameterName = "MA_TINH_NNOP";
                    p[22].DbType = DbType.String;
                    p[22].Direction = ParameterDirection.Input;
                    p[22].Value = MA_TINH_NNOP;

                    p[23] = new OracleParameter();
                    p[23].ParameterName = "TEN_TINH_NNOP";
                    p[23].DbType = DbType.String;
                    p[23].Direction = ParameterDirection.Input;
                    p[23].Value = TEN_TINH_NNOP.Replace("'", "");

                    p[24] = new OracleParameter();
                    p[24].ParameterName = "MST_NTHAY";
                    p[24].DbType = DbType.String;
                    p[24].Direction = ParameterDirection.Input;
                    p[24].Value = MST_NTHAY;


                    p[25] = new OracleParameter();
                    p[25].ParameterName = "TEN_NTHAY";
                    p[25].DbType = DbType.String;
                    p[25].Direction = ParameterDirection.Input;
                    p[25].Value = TEN_NTHAY.Replace("'", ""); 

                    p[26] = new OracleParameter();
                    p[26].ParameterName = "DIACHI_NTHAY";
                    p[26].DbType = DbType.String;
                    p[26].Direction = ParameterDirection.Input;
                    p[26].Value = DIACHI_NTHAY.Replace("'", ""); 

                    p[27] = new OracleParameter();
                    p[27].ParameterName = "TEN_HUYEN_NTHAY";
                    p[27].DbType = DbType.String;
                    p[27].Direction = ParameterDirection.Input;
                    p[27].Value = TEN_HUYEN_NTHAY.Replace("'", "");

                    p[28] = new OracleParameter();
                    p[28].ParameterName = "TEN_TINH_NTHAY";
                    p[28].DbType = DbType.String;
                    p[28].Direction = ParameterDirection.Input;
                    p[28].Value = TEN_TINH_NTHAY.Replace("'", "");

                    p[29] = new OracleParameter();
                    p[29].ParameterName = "MA_NHANG_NOP";
                    p[29].DbType = DbType.String;
                    p[29].Direction = ParameterDirection.Input;
                    p[29].Value = MA_NHANG_NOP;

                    p[30] = new OracleParameter();
                    p[30].ParameterName = "TEN_NHANG_NOP";
                    p[30].DbType = DbType.String;
                    p[30].Direction = ParameterDirection.Input;
                    p[30].Value = TEN_NHANG_NOP;

                    p[31] = new OracleParameter();
                    p[31].ParameterName = "STK_NHANG_NOP";
                    p[31].DbType = DbType.String;
                    p[31].Direction = ParameterDirection.Input;
                    p[31].Value = STK_NHANG_NOP;

                    p[32] = new OracleParameter();
                    p[32].ParameterName = "MA_HIEU_KBAC";
                    p[32].DbType = DbType.String;
                    p[32].Direction = ParameterDirection.Input;
                    p[32].Value = MA_HIEU_KBAC;


                    p[33] = new OracleParameter();
                    p[33].ParameterName = "TEN_KBAC";
                    p[33].DbType = DbType.String;
                    p[33].Direction = ParameterDirection.Input;
                    p[33].Value = TEN_KBAC.Replace("'", "");

                    p[34] = new OracleParameter();
                    p[34].ParameterName = "MA_TINH_KBAC";
                    p[34].DbType = DbType.String;
                    p[34].Direction = ParameterDirection.Input;
                    p[34].Value = MA_TINH_KBAC;

                    p[35] = new OracleParameter();
                    p[35].ParameterName = "TEN_TINH_KBAC";
                    p[35].DbType = DbType.String;
                    p[35].Direction = ParameterDirection.Input;
                    p[35].Value = TEN_TINH_KBAC.Replace("'", "");

                    p[36] = new OracleParameter();
                    p[36].ParameterName = "LOAI_TK_THU";
                    p[36].DbType = DbType.String;
                    p[36].Direction = ParameterDirection.Input;
                    p[36].Value = LOAI_TK_THU;

                    p[37] = new OracleParameter();
                    p[37].ParameterName = "TEN_TK_THU";
                    p[37].DbType = DbType.String;
                    p[37].Direction = ParameterDirection.Input;
                    p[37].Value = TEN_TK_THU;

                    p[38] = new OracleParameter();
                    p[38].ParameterName = "STK_THU";
                    p[38].DbType = DbType.String;
                    p[38].Direction = ParameterDirection.Input;
                    p[38].Value = STK_THU;

                    p[39] = new OracleParameter();
                    p[39].ParameterName = "ID_TK_KNGHI";
                    p[39].DbType = DbType.String;
                    p[39].Direction = ParameterDirection.Input;
                    p[39].Value = ID_TK_KNGHI;

                    p[40] = new OracleParameter();
                    p[40].ParameterName = "TK_KNGHI";
                    p[40].DbType = DbType.String;
                    p[40].Direction = ParameterDirection.Input;
                    p[40].Value = TK_KNGHI;

                    p[41] = new OracleParameter();
                    p[41].ParameterName = "MA_CQTHU";
                    p[41].DbType = DbType.String;
                    p[41].Direction = ParameterDirection.Input;
                    p[41].Value = MA_CQTHU;

                    p[42] = new OracleParameter();
                    p[42].ParameterName = "TEN_CQTHU";
                    p[42].DbType = DbType.String;
                    p[42].Direction = ParameterDirection.Input;
                    p[42].Value = TEN_CQTHU;

                    p[43] = new OracleParameter();
                    p[43].ParameterName = "NGAY_LAP";
                    p[43].DbType = DbType.Date;
                    p[43].Direction = ParameterDirection.Input;
                    p[43].Value = NGAY_LAP;
                    if (NGAY_LAP != null && NGAY_LAP != "")
                    {
                        p[43].Value = DateTime.ParseExact(NGAY_LAP, "dd/MM/yyyy", null);
                    }
                    else
                    {
                        p[43].Value = DBNull.Value;
                    }

                    p[44] = new OracleParameter();
                    p[44].ParameterName = "TONG_TIEN";
                    p[44].DbType = DbType.String;
                    p[44].Direction = ParameterDirection.Input;
                    p[44].Value = TONG_TIEN;

                    p[45] = new OracleParameter();
                    p[45].ParameterName = "VAN_ID";
                    p[45].DbType = DbType.String;
                    p[45].Direction = ParameterDirection.Input;
                    p[45].Value = VAN_ID;

                    p[46] = new OracleParameter();
                    p[46].ParameterName = "SIGNATUE_NNT";
                    p[46].DbType = DbType.String;
                    p[46].Direction = ParameterDirection.Input;
                    p[46].Value = SIGNATUE_NNT;

                    p[47] = new OracleParameter();
                    p[47].ParameterName = "SIGNATUE_TCT";
                    p[47].DbType = DbType.String;
                    p[47].Direction = ParameterDirection.Input;
                    p[47].Value = SIGNATUE_TCT;

                    p[48] = new OracleParameter();
                    p[48].ParameterName = "SIGNATUE_NH";
                    p[48].DbType = DbType.String;
                    p[48].Direction = ParameterDirection.Input;
                    p[48].Value = SIGNATUE_NH;

                    p[49] = new OracleParameter();
                    p[49].ParameterName = "MA_GDICH_DC";
                    p[49].DbType = DbType.String;
                    p[49].Direction = ParameterDirection.Input;
                    p[49].Value = MA_GDICH_DC;

                    p[50] = new OracleParameter();
                    p[50].ParameterName = "TGIAN_TCT_GUI";
                    p[50].DbType = DbType.String;
                    p[50].Direction = ParameterDirection.Input;
                    p[50].Value = tgian_tct_gui;

                    p[51] = new OracleParameter();
                    p[51].ParameterName = "TGIAN_NH_NHAN";
                    p[51].DbType = DbType.String;
                    p[51].Direction = ParameterDirection.Input;
                    p[51].Value = tgian_nh_nhan;

                    p[52] = new OracleParameter();
                    p[52].ParameterName = "MA_NGUYENTE";
                    p[52].DbType = DbType.String;
                    p[52].Direction = ParameterDirection.Input;
                    p[52].Value = MA_NGUYENTE;

                    p[53] = new OracleParameter();
                    p[53].ParameterName = "MA_THAMCHIEU";
                    p[53].DbType = DbType.String;
                    p[53].Direction = ParameterDirection.Input;
                    p[53].Value = MA_THAMCHIEU;

                    // tuanda - 05/02/24
                    p[54] = new OracleParameter();
                    p[54].ParameterName = "NGAY_CTU";
                    p[54].DbType = DbType.Date;
                    p[54].Direction = ParameterDirection.Input;
                    p[54].Value = NGAY_CTU;
                    if (NGAY_CTU != null && NGAY_CTU != "")
                    {
                        p[54].Value = DateTime.ParseExact(NGAY_CTU, "dd/MM/yyyy HH:mm:ss", null);
                    }
                    else
                    {
                        p[54].Value = DBNull.Value;
                    }

                    p[55] = new OracleParameter();
                    p[55].ParameterName = "LHINH_NNT";
                    p[55].DbType = DbType.String;
                    p[55].Direction = ParameterDirection.Input;
                    p[55].Value = LHINH_NNT;

                    // tuanda - 04/03/24
                    p[56] = new OracleParameter();
                    p[56].ParameterName = "MA_LOAI_THUE";
                    p[56].DbType = DbType.String;
                    p[56].Direction = ParameterDirection.Input;
                    p[56].Value = MA_LOAI_THUE;

                    p[57] = new OracleParameter();
                    p[57].ParameterName = "MA_NHANG_UNT";
                    p[57].DbType = DbType.String;
                    p[57].Direction = ParameterDirection.Input;
                    p[57].Value = MA_NHANG_UNT;

                    p[58] = new OracleParameter();
                    p[58].ParameterName = "TEN_NHANG_UNT";
                    p[58].DbType = DbType.String;
                    p[58].Direction = ParameterDirection.Input;
                    p[58].Value = TEN_NHANG_UNT;

                    DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);

                    foreach (GDT.MSG.MSG07000.ROW_CTIET itdata in CHUNGTU_CTIET.ROW_CTIET)
                    {
                        DTLToDB(itdata, SO_CTU, transCT);
                    }

                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }

        }

        public static void DTLToDB(GDT.MSG.MSG07000.ROW_CTIET objCTu_CT, string id_ct, IDbTransaction dbTran)
        {

            try
            {
                if (objCTu_CT != null)
                {
                    //insert tcs_dchieu_hqnh_dtl
                    StringBuilder sbsql = new StringBuilder();

                    sbsql.Append("INSERT INTO TCS_DCHIEU_CTU_NTDT_DTL (ID_CTU_CTIET, ");
                    sbsql.Append("ID_CTU,");
                    sbsql.Append("NDUNG_NOP,");
                    sbsql.Append("MA_NDKT,");
                    sbsql.Append("MA_CHUONG,");
                    sbsql.Append("TIEN_PNOP,");

                    // tuanda - 04/03/24
                    sbsql.Append("KY_THUE,");
                    sbsql.Append("GHI_CHU,");
                    sbsql.Append("SO_TK_TB_QD,");
                    sbsql.Append("MA_DBHC_THU,");
                    sbsql.Append("TEN_DBHC_THU,");
                    sbsql.Append("ID_KNOP)");

                    sbsql.Append("VALUES(:ID_CTU_CTIET, ");
                    sbsql.Append(":ID_CTU,");
                    sbsql.Append(":NDUNG_NOP,");
                    sbsql.Append(":MA_NDKT,");
                    sbsql.Append(":MA_CHUONG,");
                    sbsql.Append(":TIEN_PNOP,");

                    // tuanda - 03/04/24
                    sbsql.Append(":KY_THUE,");
                    sbsql.Append(":GHI_CHU,");
                    sbsql.Append(":SO_TK_TB_QD,");
                    sbsql.Append(":MA_DBHC_THU,");
                    sbsql.Append(":TEN_DBHC_THU,");
                    sbsql.Append(":ID_KNOP)");

                    IDbDataParameter[] p = new IDbDataParameter[12];

                    p[0] = new OracleParameter();
                    p[0].ParameterName = "ID_CTU_CTIET";
                    p[0].DbType = DbType.Int64;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = DBNull.Value;
                    p[0].Value = objCTu_CT.ID_CTU_CTIET;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "ID_CTU";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = id_ct;

                    p[2] = new OracleParameter();
                    p[2].ParameterName = "NDUNG_NOP";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = objCTu_CT.NDUNG_NOP;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "MA_NDKT";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = objCTu_CT.MA_NDKT;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "MA_CHUONG";
                    p[4].DbType = DbType.String;
                    p[4].Direction = ParameterDirection.Input;
                    p[4].Value = objCTu_CT.MA_CHUONG;

                    p[5] = new OracleParameter();
                    p[5].ParameterName = "TIEN_PNOP";
                    p[5].DbType = DbType.String;
                    p[5].Direction = ParameterDirection.Input;
                    p[5].Value = objCTu_CT.TIEN_PNOP;

                    // tuanda - 05/02/24
                    p[6] = new OracleParameter();
                    p[6].ParameterName = "MA_DBHC_THU";
                    p[6].DbType = DbType.String;
                    p[6].Direction = ParameterDirection.Input;
                    p[6].Value = objCTu_CT.MA_DBHC_THU;

                    p[7] = new OracleParameter();
                    p[7].ParameterName = "TEN_DBHC_THU";
                    p[7].DbType = DbType.String;
                    p[7].Direction = ParameterDirection.Input;
                    p[7].Value = objCTu_CT.TEN_DBHC_THU;

                    p[8] = new OracleParameter();
                    p[8].ParameterName = "SO_TK_TB_QD";
                    p[8].DbType = DbType.String;
                    p[8].Direction = ParameterDirection.Input;
                    p[8].Value = objCTu_CT.SO_TK_TB_QD;

                    p[9] = new OracleParameter();
                    p[9].ParameterName = "ID_KNOP";
                    p[9].DbType = DbType.String;
                    p[9].Direction = ParameterDirection.Input;
                    p[9].Value = objCTu_CT.ID_KNOP;

                    // tuanda - 06/02/24
                    p[10] = new OracleParameter();
                    p[10].ParameterName = "KY_THUE";
                    p[10].DbType = DbType.String;
                    p[10].Direction = ParameterDirection.Input;
                    p[10].Value = objCTu_CT.KY_THUE;

                    p[11] = new OracleParameter();
                    p[11].ParameterName = "GHI_CHU";
                    p[11].DbType = DbType.String;
                    p[11].Direction = ParameterDirection.Input;
                    p[11].Value = objCTu_CT.GHI_CHU;

                    DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, dbTran);
                }
            }
            catch (Exception ex)
            {

                log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
        }

        #endregion
    }
}