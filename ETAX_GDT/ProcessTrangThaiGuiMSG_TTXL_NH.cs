﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG;
using GDT.MSG.MSG06004;
using System.Data.OracleClient;
using System.Data;
using Business.NTDT.NNTDT;
using VBOracleLib;
namespace ETAX_GDT
{
    public class ProcessTrangThaiGuiMSG_TTXL_NH
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string strTran_Type = "MSG06004";
        private string strMSG_TEXT = "";
        public ProcessTrangThaiGuiMSG_TTXL_NH()
        {}

        public void Process(MSG msg)
        {
            try
            {

                if (msg != null)
                {
                    string xmlIn = "";
                    xmlIn = msg.content_mess;

                    if (xmlIn.Length > 0)
                    {
                        xmlIn = xmlIn.Replace("<DATA>", Utility.strXMLdefin2);
                        MSG06004 msg06004 = new MSG06004();
                        MSG06004 objTemp = msg06004.MSGToObject(xmlIn);
                        if (objTemp != null)
                        {
                            capnhatNhanMSG(objTemp, msg.msg_refid);

                            if (msg.tran_code == ETAX_GDT.GDICH_NTDT_Constants.ESB_TYPE_MESS_TTIN_TKHOAN_NH_TTXL || msg.tran_code == ETAX_GDT.GDICH_NTDT_Constants.ESB_TYPE_MESS_TBAO_DKY_NH_TTXL)
                            {
                                xacNhanThayDoiTKNH(objTemp, msg.msg_refid);
                            }
                            //if (msg.tran_code == ETAX_GDT.GDICH_NTDT_Constants.ESB_TYPE_MESS_TBAO_NH_TTXL || msg.tran_code.Equals(ETAX_GDT.GDICH_NTDT_Constants.ESB_TYPE_MESS_TBAO_NH_TTXL))
                            //{
                                //xacNhanChungTu(objTemp);
                            //}
                        }
                        strTran_Type = "MSG06004";
                        strMSG_TEXT = "Process=Time:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "xacNhanThayDoiTKNH=" + objTemp.HEADER.MSG_REFID .ToString();
                        Utility.Log_Action(strTran_Type, strMSG_TEXT);
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                strMSG_TEXT = "Process=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "Exception=" + ex.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            }
        }


        private static void   capnhatNhanMSG(MSG06004 msg06004, string msg_id)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("UPDATE TCS_DM_MSG_OUT_NTDT set TRANGTHAI = :TRANGTHAI, ");
            sbSqlInsert.Append("NOTE_GDICH = :NOTE_GDICH");
            sbSqlInsert.Append(" WHERE MSG_ID = :MSG_ID");

            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg06004.BODY == null)
                {
                    throw new Exception("Lỗi lấy thông tin nhận dữ liệu");
                }

                if (msg06004.BODY!= null)
                {

                    ////Khai bao cac cau lenh
                    IDbDataParameter[] p = new IDbDataParameter[0];

                    //Gan cac thong tin
                    string MESS_CODE = DBNull.Value.ToString();
                    string MESS_CONTENT = DBNull.Value.ToString();
                    string MSG_ID = DBNull.Value.ToString();

                    //foreach (Customs.MSG.MSG16.ItemData itdata in msg16.Data.Item)
                    //{
                    if (msg06004.BODY.ROW.GIAODICH.MESS_CODE != null)
                    {
                        if (!msg06004.BODY.ROW.GIAODICH.MESS_CODE.Equals(string.Empty))
                        {
                            MESS_CODE = msg06004.BODY.ROW.GIAODICH.MESS_CODE;
                        }
                    }
                    if (msg06004.BODY.ROW.GIAODICH.MESS_CONTENT != null)
                    {
                        if (!msg06004.BODY.ROW.GIAODICH.MESS_CONTENT.Equals(string.Empty))
                        {
                            MESS_CONTENT = msg06004.BODY.ROW.GIAODICH.MESS_CONTENT;
                        }
                    }
                    
                    p = new IDbDataParameter[3];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "TRANGTHAI";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = MESS_CODE;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "NOTE_GDICH";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = MESS_CONTENT;


                    p[2] = new OracleParameter();
                    p[2].ParameterName = "MSG_ID";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = msg_id;

                    DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    //}
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                transCT.Rollback();
                throw ex;
            }
            finally
            {
                
                conn.Close();
                conn.Dispose();
            }

        }

        private static void xacNhanThayDoiTKNH(MSG06004 msg06004, string msg_id)
        {
            string id_link = "";
            try
            {
                string sqlID = "select * from tcs_dm_msg_out_ntdt where msg_id = '" + msg_id + "' and trangthai = '01'";
                DataTable dtID = DataAccess.ExecuteReturnDataSet(sqlID, CommandType.Text).Tables[0];
                if (dtID.Rows.Count > 0)
                {
                    id_link = dtID.Rows[0]["id_link"].ToString();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }


            if (id_link.Length > 0)
            {
                OracleConnection conn = new OracleConnection();
                IDbTransaction transCT = null;
                //Tao cau lenh Insert
                StringBuilder sbSqlInsert = new StringBuilder("");
                sbSqlInsert.Append("UPDATE TCS_DM_NNTDT set TRANG_THAI = case when trang_thai = '08' then '03' when trang_thai = '09' then '04' when trang_thai <> '08' and trang_thai <> '09' then trang_thai end, update_tt='00' ");
                sbSqlInsert.Append(" WHERE MA_GDICH = :MA_GDICH");

                try
                {
                    conn = DataAccess.GetConnection();
                    transCT = conn.BeginTransaction();
                    if (msg06004.BODY == null)
                    {
                        throw new Exception("Lỗi lấy thông tin nhận dữ liệu");
                    }

                    if (msg06004.BODY != null)
                    {

                        ////Khai bao cac cau lenh
                        IDbDataParameter[] p = new IDbDataParameter[0];

                        //Gan cac thong tin
                        string UPDATE_TT = DBNull.Value.ToString();
                        string MA_GDICH = DBNull.Value.ToString();

                        //foreach (Customs.MSG.MSG16.ItemData itdata in msg16.Data.Item)
                        //{
                        //if (msg06004.BODY.ROW.GIAODICH.MESS_CODE != null)
                        //{
                        //    if (!msg06004.BODY.ROW.GIAODICH.MESS_CODE.Equals(string.Empty))
                        //    {
                        //        UPDATE_TT = msg06004.BODY.ROW.GIAODICH.MESS_CODE;
                        //    }
                        //}

                        MA_GDICH = id_link;

                        p = new IDbDataParameter[1];
                      
                        p[0] = new OracleParameter();
                        p[0].ParameterName = "MA_GDICH";
                        p[0].DbType = DbType.String;
                        p[0].Direction = ParameterDirection.Input;
                        p[0].Value = MA_GDICH;


                        DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                        //}
                    }
                    transCT.Commit();
                }
                catch (Exception ex)
                {
                    transCT.Rollback();
                    //throw ex;
                    log.Error(ex.Message + "-" + ex.StackTrace);//  LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                }
                finally
                {
                 
                    conn.Close();
                    conn.Dispose();
                }
            }
            

        }

        private static void xacNhanChungTu(MSG06004 msg06004)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("UPDATE TCS_CTU_NTDT_HDR set MA_GD_PHANHOI = :MA_GD_PHANHOI, MA_PHANHOI = :MA_PHANHOI, ND_PHANHOI = :ND_PHANHOI, NGAY_PHANHOI = :NGAY_PHANHOI");
            sbSqlInsert.Append(" WHERE MA_GDICH_CTU = :MA_GDICH_CTU");

            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg06004.BODY == null)
                {
                    throw new Exception("Lỗi lấy thông tin nhận dữ liệu");
                }

                if (msg06004.BODY != null)
                {

                    ////Khai bao cac cau lenh
                    IDbDataParameter[] p = new IDbDataParameter[0];

                    //Gan cac thong tin
                    string MA_GD_PHANHOI = DBNull.Value.ToString();
                    string MA_PHANHOI = DBNull.Value.ToString();
                    string ND_PHANHOI = DBNull.Value.ToString();
                    string MA_GDICH_CTU = DBNull.Value.ToString();
                    string NGAY_PHOI = DBNull.Value.ToString();

                    //foreach (Customs.MSG.MSG16.ItemData itdata in msg16.Data.Item)
                    //{
                    if (msg06004.BODY.ROW.GIAODICH.MA_GDICH != null)
                    {
                        if (!msg06004.BODY.ROW.GIAODICH.MA_GDICH.Equals(string.Empty))
                        {
                            MA_GD_PHANHOI = msg06004.BODY.ROW.GIAODICH.MA_GDICH;
                        }
                    }
                    if (msg06004.BODY.ROW.GIAODICH.MESS_CODE != null)
                    {
                        if (!msg06004.BODY.ROW.GIAODICH.MESS_CODE.Equals(string.Empty))
                        {
                            MA_PHANHOI = msg06004.BODY.ROW.GIAODICH.MESS_CODE;
                        }
                    }
                    if (msg06004.BODY.ROW.GIAODICH.MESS_CONTENT != null)
                    {
                        if (!msg06004.BODY.ROW.GIAODICH.MESS_CONTENT.Equals(string.Empty))
                        {
                            ND_PHANHOI = msg06004.BODY.ROW.GIAODICH.MESS_CONTENT;
                        }
                    }
                    if (msg06004.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU != null)
                    {
                        if (!msg06004.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU.Equals(string.Empty))
                        {
                            MA_GDICH_CTU = msg06004.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU;
                        }
                    }
                    if (msg06004.BODY.ROW.GIAODICH.NGAY_GUI_GDICH != null)
                    {
                        if (!msg06004.BODY.ROW.GIAODICH.NGAY_GUI_GDICH.Equals(string.Empty))
                        {
                            NGAY_PHOI = msg06004.BODY.ROW.GIAODICH.NGAY_GUI_GDICH;
                        }
                    }

                    p = new IDbDataParameter[5];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "MA_GD_PHANHOI";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = MA_GD_PHANHOI;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "MA_PHANHOI";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = MA_PHANHOI;

                    p[2] = new OracleParameter();
                    p[2].ParameterName = "ND_PHANHOI";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = ND_PHANHOI;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "MA_GDICH_CTU";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = MA_GDICH_CTU;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "NGAY_PHANHOI";
                    p[4].DbType = DbType.String;
                    p[4].Direction = ParameterDirection.Input;
                    p[4].Value = NGAY_PHOI;

                    DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    //}
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                transCT.Rollback();
                //throw ex;
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
            finally
            {
                
                conn.Close();
                conn.Dispose();
            }

        }
    }
}
