﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDT.MSG
{
    public class MSG
    {
        //private string _id_gdich;
        private string _sender_code;
        private string _sender_name;
        private string _receiver_code;
        private string _receiver_name;
        private string _tran_code;
        private string _msg_id;
        private string _msg_refid;
        private string _id_link;
        private string _send_date;
        private string _original_code;
        private string _original_name;
        private string _original_date;
        private string _error_code;
        private string _error_desc;
        private string _spare1;
        private string _spare2;
        private string _spare3;
        private string _content_mess;
        private string _content_messctu;
        private string _trangthai;
        private string _loai_gdich;
        private string _ngay_gdich;
        private string _note_gdich;

        public MSG()
        {
            //_id_gdich = "";
            _sender_code = "";
            _sender_name = "";
            _receiver_code = "";
            _receiver_name = "";
            _tran_code = "";
            _msg_id = "";
            _msg_refid = "";
            _id_link = "";
            _send_date = "";
            _original_code = "";
            _original_name = "";
            _original_date = "";
            _error_code = "";
            _error_desc = "";
            _spare1 = "";
            _spare2 = "";
            _spare3 = "";
            _content_mess = "";
            _content_messctu = "";
            _trangthai = "";
            _loai_gdich = "";
            _ngay_gdich = "";
            _note_gdich = "";

        }

        //public string id_gdich
        //{
        //    get { return _id_gdich; }
        //    set { _id_gdich = value; }
        //}
        public string sender_code
        {
            get { return _sender_code; }
            set { _sender_code = value; }
        }
        public string sender_name
        {
            get { return _sender_name; }
            set { _sender_name = value; }
        }
        public string receiver_code
        {
            get { return _receiver_code; }
            set { _receiver_code = value; }
        }
        public string receiver_name
        {
            get { return _receiver_name; }
            set { _receiver_name = value; }
        }
        public string tran_code
        {
            get { return _tran_code; }
            set { _tran_code = value; }
        }
        public string msg_id
        {
            get { return _msg_id; }
            set { _msg_id = value; }
        }
        public string msg_refid
        {
            get { return _msg_refid; }
            set { _msg_refid = value; }
        }
        public string id_link
        {
            get { return _id_link; }
            set { _id_link = value; }
        }
        public string send_date
        {
            get { return _send_date; }
            set { _send_date = value; }
        }
        public string original_code
        {
            get { return _original_code; }
            set { _original_code = value; }
        }
        public string original_name
        {
            get { return _original_name; }
            set { _original_name = value; }
        }
        public string original_date
        {
            get { return _original_date; }
            set { _original_date = value; }
        }
        public string error_code
        {
            get { return _error_code; }
            set { _error_code = value; }
        }
        public string error_desc
        {
            get { return _error_desc; }
            set { _error_desc = value; }
        }
        public string spare1
        {
            get { return _spare1; }
            set { _spare1 = value; }
        }
        public string spare2
        {
            get { return _spare2; }
            set { _spare2 = value; }
        }
        public string spare3
        {
            get { return _spare3; }
            set { _spare3 = value; }
        }
        public string content_mess
        {
            get { return _content_mess; }
            set { _content_mess = value; }
        }
        public string content_messctu
        {
            get { return _content_messctu; }
            set { _content_messctu = value; }
        }
        public string trangthai
        {
            get { return _trangthai; }
            set { _trangthai = value; }
        }
        public string loai_gdich
        {
            get { return _loai_gdich; }
            set { _loai_gdich = value; }
        }
        public string ngay_gdich
        {
            get { return _ngay_gdich; }
            set { _ngay_gdich = value; }
        }
        public string note_gdich
        {
            get { return _note_gdich; }
            set { _note_gdich = value; }
        }

    }
}
