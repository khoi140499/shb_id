﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG.MSG06011;
using System.Data.OracleClient;
using System.Data;
using Business.NTDT.NNTDT;
using System.Configuration;
namespace ETAX_GDT
{

    public class ProcessThongBaoThayDoiTTNT_NH_TTXL
    {
        private string strTran_Type = "";
        private string strMSG_TEXT = "";
        private string strMSG_ID = "";
        public ProcessThongBaoThayDoiTTNT_NH_TTXL()
        {
           
        }

        public void sendMSG06011(infNNTDT objNNTDT)
        {
            try
            {

                if (objNNTDT != null)
                {
                    string xmlOut = "";
                    MSG06011 MSG06011 = new MSG06011();
                    MSG06011.HEADER = new HEADER();
                    MSG06011.BODY = new BODY();
                    MSG06011.BODY.ROW = new ROW();
                    MSG06011.BODY.ROW.GIAODICH = new GIAODICH();
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT = new TBAO_TKHOAN_NT();
                    
                    MSG06011.HEADER.VERSION = "1.0";
                    MSG06011.HEADER.SENDER_CODE = GDICH_NTDT_Constants.HEADER_SENDER_CODE;
                    MSG06011.HEADER.SENDER_NAME = GDICH_NTDT_Constants.HEADER_SENDER_NAME;
                    MSG06011.HEADER.RECEIVER_CODE = GDICH_NTDT_Constants.HEADER_RECEIVER_CODE;
                    MSG06011.HEADER.RECEIVER_NAME = GDICH_NTDT_Constants.HEADER_RECEIVER_NAME;
                    MSG06011.HEADER.TRAN_CODE = "06011";
                    MSG06011.HEADER.MSG_ID = MSG06011.HEADER.SENDER_CODE + Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ");
                    MSG06011.HEADER.MSG_REFID = "";
                    MSG06011.HEADER.ID_LINK = objNNTDT.MA_GDICH;
                    MSG06011.HEADER.SEND_DATE = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                    MSG06011.HEADER.ORIGINAL_CODE = "";
                    MSG06011.HEADER.ORIGINAL_NAME = "";
                    MSG06011.HEADER.ORIGINAL_DATE = "";
                    MSG06011.HEADER.ERROR_CODE = "";
                    MSG06011.HEADER.ERROR_DESC = "";
                    MSG06011.HEADER.SPARE1 = "";
                    MSG06011.HEADER.SPARE2 = "";
                    MSG06011.HEADER.SPARE3 = "";

                    MSG06011.BODY.ROW.GIAODICH.MA_GDICH = MSG06011.HEADER.MSG_ID;
                    
                    string year = DateTime.Now.ToString("yyyy");
                    string dayofyear = DateTime.Now.DayOfYear.ToString("000");
                    MSG06011.BODY.ROW.GIAODICH.NGAY_GUI_GDICH = year + dayofyear + DateTime.Now.ToString("HHmmss");


                    MSG06011.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU = objNNTDT.MA_GDICH;

                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.PBAN_TLIEU_XML = objNNTDT.PBAN_TLIEU_XML;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.MA_TBAO = "04";

                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.MAU_TBAO = "03/TB-NHTM";
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.SO_TBAO = "";
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.TEN_TBAO = "Thông báo xác nhận thay đổi thông tin nộp thuế điện tử của NHTM";
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.NGAY_TBAO = DateTime.Now.ToString("dd/MM/yyy HH:mm:ss");

                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.MST = objNNTDT.MST;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.TEN_NNT = objNNTDT.TEN_NNT;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.DIACHI_NNT = objNNTDT.DIACHI_NNT;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.MA_CQT = objNNTDT.MA_CQT;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.EMAIL_NNT = objNNTDT.EMAIL_NNT;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.SDT_NNT = objNNTDT.SDT_NNT;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.TEN_LHE_NTHUE = objNNTDT.TEN_LHE_NTHUE;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.SERIAL_CERT_NTHUE = objNNTDT.SERIAL_CERT_NTHUE;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.SUBJECT_CERT_NTHUE = objNNTDT.SUBJECT_CERT_NTHUE;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.ISSUER_CERT_NTHUE = objNNTDT.ISSUER_CERT_NTHUE;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.MA_NHANG = objNNTDT.MA_NHANG;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.TEN_NHANG = objNNTDT.TEN_NHANG;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.VAN_ID = objNNTDT.VAN_ID;
                    MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.TEN_TVAN = objNNTDT.TEN_TVAN;

                    if (objNNTDT.TRANG_THAI == "04" || objNNTDT.TRANG_THAI == "09")
                    {
                        MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.TTHAI_XNHAN = "N";
                        MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.LDO_TCHOI = objNNTDT.LDO_TCHOI;
                    }
                    else
                    {
                        MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.TTHAI_XNHAN = "Y";
                        MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.LDO_TCHOI = "";
                    }
                    
                    
                    //MSG06011.BODY.ROW.GIAODICH.TBAO_TKHOAN_NT.Signature[0] = "";

                    xmlOut = MSG06011.MSGtoXML(MSG06011);
                    xmlOut = Utility.RomoveXML_Des(xmlOut);
                    //vinhvv sign
                    string signatureType = Utility.GetValue_THAMSOHT("SIGN.TYPE.TCT");
                    string xpathSign = "/DATA/BODY/ROW/GIAODICH/TBAO_TKHOAN_NT";
                    string xpathNodeSign = "TBAO_TKHOAN_NT";
                    if (signatureType.Equals("1"))
                    {
                        xmlOut = Signature.DigitalSignature.getSignNTDT(xmlOut, xpathSign, xpathNodeSign);
                    }
                    else
                    {
                        xmlOut = Signature.SignatureProcess.SignXMLEextendTimeStamp(xmlOut, "TBAO_TKHOAN_NT", "TBAO_TKHOAN_NT", true);
                    }
                    //vinhvv
                    strMSG_ID = MSG06011.HEADER.MSG_ID;
                    strTran_Type = "MSG06011";
                    strMSG_TEXT = "PutMQ=StartTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);

                    clsMQHelper mq = new clsMQHelper();
                    string strReturn = "";
                    strReturn = mq.PutMQMessage(xmlOut);

                    strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);

                    //insert log
                    GDT.MSG.MSG msg_log = new GDT.MSG.MSG();

                    msg_log.sender_code = MSG06011.HEADER.SENDER_CODE;
                    msg_log.sender_name = MSG06011.HEADER.SENDER_NAME;
                    msg_log.receiver_code = MSG06011.HEADER.RECEIVER_CODE;
                    msg_log.receiver_name = MSG06011.HEADER.RECEIVER_NAME;
                    msg_log.tran_code = MSG06011.HEADER.TRAN_CODE;
                    msg_log.msg_id = MSG06011.HEADER.MSG_ID;
                    msg_log.msg_refid = MSG06011.HEADER.MSG_REFID;
                    msg_log.id_link = MSG06011.HEADER.ID_LINK;
                    msg_log.send_date = MSG06011.HEADER.SEND_DATE;
                    msg_log.original_code = MSG06011.HEADER.ORIGINAL_CODE;
                    msg_log.original_name = MSG06011.HEADER.ORIGINAL_NAME;
                    msg_log.original_date = MSG06011.HEADER.ORIGINAL_DATE;
                    msg_log.error_code = MSG06011.HEADER.ERROR_CODE;
                    msg_log.error_desc = MSG06011.HEADER.ERROR_DESC;
                    if (!strReturn.Equals(""))
                    {
                        msg_log.error_code = "02";
                        if (strReturn.Length > 231)
                        {
                            strReturn = strReturn.Substring(0, 230);
                        }
                        msg_log.error_desc = strReturn;
                    }
                    msg_log.spare1 = MSG06011.HEADER.SPARE1;
                    msg_log.spare2 = MSG06011.HEADER.SPARE2;
                    msg_log.spare3 = MSG06011.HEADER.SPARE3;
                    msg_log.trangthai = "00";
                    msg_log.content_mess = xmlOut;

                    string ma_tt = "";
                    Utility.insertMSGOutLog(msg_log, ref ma_tt);

                }
            }
            catch (Exception ex)
            {
                strMSG_TEXT = "PutMQ=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "Exception=" + ex.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            }
        }
        
    }
}
