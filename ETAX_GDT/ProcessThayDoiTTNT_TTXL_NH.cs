﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG;
using GDT.MSG.MSG06010;
using GDT.MSG.MSG06004;
using System.Data.OracleClient;
using System.Data;
using Business.NTDT.NNTDT;
using GDT.MSG.MSGHeader;
using VBOracleLib;
namespace ETAX_GDT
{
    class ProcessThayDoiTTNT_TTXL_NH
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string strTran_Type = "";
        private string strMSG_TEXT = "";
        public ProcessThayDoiTTNT_TTXL_NH()
        {}

        public void Process(MSG msg, bool checkCK)
        {
            try
            {

                if (msg != null)
                {
                    string xmlIn = "";
                    xmlIn = msg.content_mess;

                    if (xmlIn.Length > 0)
                    {
                        xmlIn = xmlIn.Replace("<DATA>", Utility.strXMLdefin2);
                        MSG06010 msg06010 = new MSG06010();
                        MSG06010 objTemp = null;
                        try
                        {
                            objTemp = msg06010.MSGToObject(xmlIn);
                            if (objTemp != null)
                            {
                                TDOI_TKHOAN_NT TDOI_TKHOAN_NT = objTemp.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT;

                                MSGHeader msgheader = new MSGHeader();
                                msgheader.HEADER = new GDT.MSG.MSGHeader.HEADER();

                                msgheader.HEADER.ERROR_CODE = objTemp.HEADER.ERROR_CODE;
                                msgheader.HEADER.ERROR_DESC = objTemp.HEADER.ERROR_DESC;
                                msgheader.HEADER.ID_LINK = objTemp.HEADER.ID_LINK;
                                msgheader.HEADER.MSG_ID = objTemp.HEADER.MSG_ID;
                                msgheader.HEADER.MSG_REFID = objTemp.HEADER.MSG_REFID;
                                msgheader.HEADER.ORIGINAL_CODE = objTemp.HEADER.ORIGINAL_CODE;
                                msgheader.HEADER.ORIGINAL_DATE = objTemp.HEADER.ORIGINAL_DATE;
                                msgheader.HEADER.ORIGINAL_NAME = objTemp.HEADER.ORIGINAL_NAME;
                                msgheader.HEADER.RECEIVER_CODE = objTemp.HEADER.RECEIVER_CODE;
                                msgheader.HEADER.RECEIVER_NAME = objTemp.HEADER.RECEIVER_NAME;
                                msgheader.HEADER.SEND_DATE = objTemp.HEADER.SEND_DATE;
                                msgheader.HEADER.SENDER_CODE = objTemp.HEADER.SENDER_CODE;
                                msgheader.HEADER.SENDER_NAME = objTemp.HEADER.SENDER_NAME;
                                msgheader.HEADER.SPARE1 = objTemp.HEADER.SPARE1;
                                msgheader.HEADER.SPARE2 = objTemp.HEADER.SPARE2;
                                msgheader.HEADER.SPARE3 = objTemp.HEADER.SPARE3;
                                msgheader.HEADER.TRAN_CODE = objTemp.HEADER.TRAN_CODE;
                                msgheader.HEADER.VERSION = objTemp.HEADER.VERSION;

                                if (TDOI_TKHOAN_NT.DIACHI_NNT == null || TDOI_TKHOAN_NT.EMAIL_NNT == null || TDOI_TKHOAN_NT.ISSUER_CERT_NTHUE == null || TDOI_TKHOAN_NT.MA_CQT == null || TDOI_TKHOAN_NT.MA_NHANG == null || TDOI_TKHOAN_NT.MST == null || TDOI_TKHOAN_NT.PBAN_TLIEU_XML == null || TDOI_TKHOAN_NT.SDT_NNT == null || TDOI_TKHOAN_NT.SERIAL_CERT_NTHUE == null || TDOI_TKHOAN_NT.SUBJECT_CERT_NTHUE == null || TDOI_TKHOAN_NT.TEN_LHE_NTHUE == null || TDOI_TKHOAN_NT.TEN_NHANG == null || TDOI_TKHOAN_NT.TEN_NNT == null || TDOI_TKHOAN_NT.TEN_TVAN == null || TDOI_TKHOAN_NT.VAN_ID == null)
                                {

                                    //phản hồi nhận dữ liệu bị lỗi cấu trúc cho tct
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "02", "07 - Cấu trúc thay đổi thông tin nộp thuế không đúng khuôn dạng");

                                    infNNTDT objNNTDT = new infNNTDT();
                                    objNNTDT.DIACHI_NNT = TDOI_TKHOAN_NT.DIACHI_NNT + "";
                                    objNNTDT.EMAIL_NNT = TDOI_TKHOAN_NT.EMAIL_NNT + "";
                                    objNNTDT.ISSUER_CERT_NTHUE = TDOI_TKHOAN_NT.ISSUER_CERT_NTHUE + "";
                                    objNNTDT.LDO_TCHOI = "Cấu trúc thay đôi thông tin nộp thuế không đúng khuôn dạng";
                                    objNNTDT.MA_CQT = TDOI_TKHOAN_NT.MA_CQT + "";
                                    objNNTDT.MA_GDICH = objTemp.BODY.ROW.GIAODICH.MA_GDICH + "";
                                    objNNTDT.MA_GDICH_TCHIEU = objTemp.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU + "";
                                    objNNTDT.MA_NHANG = TDOI_TKHOAN_NT.MA_NHANG + "";
                                    objNNTDT.MST = TDOI_TKHOAN_NT.MST + "";
                                    objNNTDT.NGAY_GUI_GDICH = objTemp.BODY.ROW.GIAODICH.NGAY_GUI_GDICH + "";
                                    objNNTDT.SDT_NNT = TDOI_TKHOAN_NT.SDT_NNT + "";
                                    objNNTDT.SERIAL_CERT_NTHUE = TDOI_TKHOAN_NT.SERIAL_CERT_NTHUE + "";
                                    objNNTDT.STK_NHANG = TDOI_TKHOAN_NT.SDT_NNT + "";
                                    objNNTDT.SUBJECT_CERT_NTHUE = TDOI_TKHOAN_NT.SUBJECT_CERT_NTHUE + "";
                                    objNNTDT.TEN_LHE_NTHUE = TDOI_TKHOAN_NT.TEN_LHE_NTHUE + "";
                                    objNNTDT.TEN_NHANG = TDOI_TKHOAN_NT.TEN_NHANG + "";
                                    objNNTDT.TEN_NNT = TDOI_TKHOAN_NT.TEN_NNT + "";
                                    objNNTDT.TEN_TVAN = TDOI_TKHOAN_NT.TEN_TVAN + "";
                                    objNNTDT.TRANG_THAI = "04";
                                    objNNTDT.VAN_ID = TDOI_TKHOAN_NT.VAN_ID + "";

                                    //thông báo chi tiết lỗi cấu trúc msg cho tct
                                    ProcessThongBaoThayDoiTTNT_NH_TTXL guithongbao = new ProcessThongBaoThayDoiTTNT_NH_TTXL();
                                    guithongbao.sendMSG06011(objNNTDT);

                                }
                                else if (checkCK == false)
                                {
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "02", "05 - Sai chữ ký số");

                                    infNNTDT objNNTDT = new infNNTDT();
                                    objNNTDT.DIACHI_NNT = TDOI_TKHOAN_NT.DIACHI_NNT;
                                    objNNTDT.EMAIL_NNT = TDOI_TKHOAN_NT.EMAIL_NNT;
                                    objNNTDT.ISSUER_CERT_NTHUE = TDOI_TKHOAN_NT.ISSUER_CERT_NTHUE;
                                    objNNTDT.LDO_TCHOI = "Sai chữ ký số";
                                    objNNTDT.MA_CQT = TDOI_TKHOAN_NT.MA_CQT;
                                    objNNTDT.MA_GDICH = objTemp.BODY.ROW.GIAODICH.MA_GDICH;
                                    objNNTDT.MA_GDICH_TCHIEU = objTemp.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU;
                                    objNNTDT.MA_NHANG = TDOI_TKHOAN_NT.MA_NHANG;
                                    objNNTDT.MST = TDOI_TKHOAN_NT.MST;
                                    objNNTDT.NGAY_GUI_GDICH = objTemp.BODY.ROW.GIAODICH.NGAY_GUI_GDICH;
                                    objNNTDT.SDT_NNT = TDOI_TKHOAN_NT.SDT_NNT;
                                    objNNTDT.SERIAL_CERT_NTHUE = TDOI_TKHOAN_NT.SERIAL_CERT_NTHUE;
                                    objNNTDT.STK_NHANG = TDOI_TKHOAN_NT.SDT_NNT;
                                    objNNTDT.SUBJECT_CERT_NTHUE = TDOI_TKHOAN_NT.SUBJECT_CERT_NTHUE;
                                    objNNTDT.TEN_LHE_NTHUE = TDOI_TKHOAN_NT.TEN_LHE_NTHUE;
                                    objNNTDT.TEN_NHANG = TDOI_TKHOAN_NT.TEN_NHANG;
                                    objNNTDT.TEN_NNT = TDOI_TKHOAN_NT.TEN_NNT;
                                    objNNTDT.TEN_TVAN = TDOI_TKHOAN_NT.TEN_TVAN;
                                    objNNTDT.TRANG_THAI = "04";
                                    objNNTDT.VAN_ID = TDOI_TKHOAN_NT.VAN_ID;

                                    //thông báo chi tiết lỗi cấu trúc msg cho tct
                                    ProcessThongBaoThayDoiTTNT_NH_TTXL guithongbao = new ProcessThongBaoThayDoiTTNT_NH_TTXL();
                                    guithongbao.sendMSG06011(objNNTDT);
                                }


                                else
                                {
                                    capnhatDKNNTDT(objTemp);

                                    //phản hồi đã nhận dữ liệu
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "01", "Thành công");
                                }


                            }
                        }
                        catch (Exception e)
                        {
                            log.Error(e.Message + "-" + e.StackTrace);//
                        }
                        strTran_Type = "MSG06010";
                        strMSG_TEXT = "Process=Time:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "capnhatDKNNTDT=" + objTemp.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MST .ToString();
                        Utility.Log_Action(strTran_Type, strMSG_TEXT);
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                strTran_Type = "MSG06010";
                strMSG_TEXT = "Process=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "Exception=" + ex.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            } 
        }

        #region "Cap nhat thong tin Dang ki su dung dich vu"

        private static void capnhatDKNNTDT(MSG06010 msg06010)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("UPDATE TCS_DM_NNTDT set PBAN_TLIEU_XML = :PBAN_TLIEU_XML,");
            sbSqlInsert.Append("TEN_NNT = :TEN_NNT,");
            sbSqlInsert.Append("DIACHI_NNT = :DIACHI_NNT,");
            sbSqlInsert.Append("MA_CQT = :MA_CQT,");
            sbSqlInsert.Append("EMAIL_NNT = :EMAIL_NNT,");
            sbSqlInsert.Append("SDT_NNT = :SDT_NNT,");
            sbSqlInsert.Append("TEN_LHE_NTHUE = :TEN_LHE_NTHUE,");
            sbSqlInsert.Append("SERIAL_CERT_NTHUE = :SERIAL_CERT_NTHUE,");
            sbSqlInsert.Append("SUBJECT_CERT_NTHUE = :SUBJECT_CERT_NTHUE,");
            sbSqlInsert.Append("ISSUER_CERT_NTHUE = :ISSUER_CERT_NTHUE,");
            sbSqlInsert.Append("MA_NHANG = :MA_NHANG,");
            sbSqlInsert.Append("TEN_NHANG = :TEN_NHANG,");
            sbSqlInsert.Append("VAN_ID = :VAN_ID,");
            sbSqlInsert.Append("TEN_TVAN = :TEN_TVAN,");
            sbSqlInsert.Append("SIGNATURE_NNT = :SIGNATURE_NNT,");
            sbSqlInsert.Append("SIGNATURE_TCT = :SIGNATURE_TCT,");
            sbSqlInsert.Append("TRANG_THAI = :TRANG_THAI,");
            sbSqlInsert.Append("MA_GDICH = :MA_GDICH,");
            sbSqlInsert.Append("NGAY_GUI_GDICH = :NGAY_GUI_GDICH,");
            sbSqlInsert.Append("MA_GDICH_TCHIEU = :MA_GDICH_TCHIEU");
            sbSqlInsert.Append(" WHERE MST = :MST");


            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT == null)
                {
                    throw new Exception("Lỗi lấy thay đổi thông tin đăng kí sử dụng dịch vụ NTDT");
                }

                if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT != null)
                {

                    ////Khai bao cac cau lenh
                    IDbDataParameter[] p = new IDbDataParameter[0];
                    
                    //Gan cac thong tin
                    string PBAN_TLIEU_XML = DBNull.Value.ToString();
                    string MST = DBNull.Value.ToString();
                    string TEN_NNT = DBNull.Value.ToString();
                    string DIACHI_NNT = DBNull.Value.ToString();
                    string MA_CQT = DBNull.Value.ToString();
                    string EMAIL_NNT = DBNull.Value.ToString();
                    string SDT_NNT = DBNull.Value.ToString();
                    string TEN_LHE_NTHUE = DBNull.Value.ToString();
                    string SERIAL_CERT_NTHUE = DBNull.Value.ToString();
                    string SUBJECT_CERT_NTHUE = DBNull.Value.ToString();
                    string ISSUER_CERT_NTHUE = DBNull.Value.ToString();
                    string MA_NHANG = DBNull.Value.ToString();
                    string TEN_NHANG = DBNull.Value.ToString();
                    string VAN_ID = DBNull.Value.ToString();
                    string TEN_TVAN = DBNull.Value.ToString();
                    string SIGNATURE_NNT = DBNull.Value.ToString();
                    string SIGNATURE_TCT = DBNull.Value.ToString();
                    string TRANG_THAI = DBNull.Value.ToString();
                    string MA_GDICH = DBNull.Value.ToString();
                    string NGAY_GUI_GDICH = DBNull.Value.ToString();
                    string MA_GDICH_TCHIEU = DBNull.Value.ToString();

                    //foreach (Customs.MSG.MSG16.ItemData itdata in msg16.Data.Item)
                    //{

                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.PBAN_TLIEU_XML != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.PBAN_TLIEU_XML.Equals(string.Empty))
                        {
                            PBAN_TLIEU_XML = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.PBAN_TLIEU_XML;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MST != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MST.Equals(string.Empty))
                        {
                            MST = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MST;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_NNT != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_NNT.Equals(string.Empty))
                        {
                            TEN_NNT = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_NNT;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.DIACHI_NNT != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.DIACHI_NNT.Equals(string.Empty))
                        {
                            DIACHI_NNT = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.DIACHI_NNT;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MA_CQT != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MA_CQT.Equals(string.Empty))
                        {
                            MA_CQT = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MA_CQT;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.EMAIL_NNT != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.EMAIL_NNT.Equals(string.Empty))
                        {
                            EMAIL_NNT = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.EMAIL_NNT;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.SDT_NNT != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.SDT_NNT.Equals(string.Empty))
                        {
                            SDT_NNT = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.SDT_NNT;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_LHE_NTHUE != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_LHE_NTHUE.Equals(string.Empty))
                        {
                            TEN_LHE_NTHUE = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_LHE_NTHUE;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.SERIAL_CERT_NTHUE != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.SERIAL_CERT_NTHUE.Equals(string.Empty))
                        {
                            SERIAL_CERT_NTHUE = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.SERIAL_CERT_NTHUE;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.SUBJECT_CERT_NTHUE != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.SUBJECT_CERT_NTHUE.Equals(string.Empty))
                        {
                            SUBJECT_CERT_NTHUE = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.SUBJECT_CERT_NTHUE;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.ISSUER_CERT_NTHUE != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.ISSUER_CERT_NTHUE.Equals(string.Empty))
                        {
                            ISSUER_CERT_NTHUE = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.ISSUER_CERT_NTHUE;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MA_NHANG != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MA_NHANG.Equals(string.Empty))
                        {
                            MA_NHANG = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.MA_NHANG;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_NHANG != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_NHANG.Equals(string.Empty))
                        {
                            TEN_NHANG = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_NHANG;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.VAN_ID != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.VAN_ID.Equals(string.Empty))
                        {
                            VAN_ID = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.VAN_ID;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_TVAN != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_TVAN.Equals(string.Empty))
                        {
                            TEN_TVAN = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.TDOI_TKHOAN_NT.TEN_TVAN;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.Signature != null)
                    {
                        if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.Signature.Count > 0)
                        {
                            if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.Signature[0].Equals(string.Empty))
                            {
                                SIGNATURE_NNT = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.Signature[0];
                            }
                        }
                        if (msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.Signature.Count > 1)
                        {
                            if (!msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.Signature[1].Equals(string.Empty))
                            {
                                SIGNATURE_TCT = msg06010.BODY.ROW.GIAODICH.TDOI_TKHOAN.Signature[1];
                            }

                        }
                    }
                    TRANG_THAI = "03";

                    if (msg06010.BODY.ROW.GIAODICH.MA_GDICH != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.MA_GDICH.Equals(string.Empty))
                        {
                            MA_GDICH = msg06010.BODY.ROW.GIAODICH.MA_GDICH;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.NGAY_GUI_GDICH != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.NGAY_GUI_GDICH.Equals(string.Empty))
                        {
                            NGAY_GUI_GDICH = msg06010.BODY.ROW.GIAODICH.NGAY_GUI_GDICH;
                        }
                    }
                    if (msg06010.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU != null)
                    {
                        if (!msg06010.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU.Equals(string.Empty))
                        {
                            MA_GDICH_TCHIEU = msg06010.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU;
                        }
                    }
                    
                    p = new IDbDataParameter[21];
                    p[0] = new OracleParameter();
                    p[0].ParameterName = "PBAN_TLIEU_XML";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = PBAN_TLIEU_XML;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "TEN_NNT";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = TEN_NNT;


                    p[2] = new OracleParameter();
                    p[2].ParameterName = "DIACHI_NNT";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = DIACHI_NNT;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "MA_CQT";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = MA_CQT;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "EMAIL_NNT";
                    p[4].DbType = DbType.String;
                    p[4].Direction = ParameterDirection.Input;
                    p[4].Value = EMAIL_NNT;

                    p[5] = new OracleParameter();
                    p[5].ParameterName = "SDT_NNT";
                    p[5].DbType = DbType.String;
                    p[5].Direction = ParameterDirection.Input;
                    p[5].Value = SDT_NNT;

                    p[6] = new OracleParameter();
                    p[6].ParameterName = "TEN_LHE_NTHUE";
                    p[6].DbType = DbType.String;
                    p[6].Direction = ParameterDirection.Input;
                    p[6].Value = TEN_LHE_NTHUE;

                    p[7] = new OracleParameter();
                    p[7].ParameterName = "SERIAL_CERT_NTHUE";
                    p[7].DbType = DbType.String;
                    p[7].Direction = ParameterDirection.Input;
                    p[7].Value = SERIAL_CERT_NTHUE;

                    p[8] = new OracleParameter();
                    p[8].ParameterName = "SUBJECT_CERT_NTHUE";
                    p[8].DbType = DbType.String;
                    p[8].Direction = ParameterDirection.Input;
                    p[8].Value = SUBJECT_CERT_NTHUE;

                    p[9] = new OracleParameter();
                    p[9].ParameterName = "ISSUER_CERT_NTHUE";
                    p[9].DbType = DbType.String;
                    p[9].Direction = ParameterDirection.Input;
                    p[9].Value = ISSUER_CERT_NTHUE;

                    p[10] = new OracleParameter();
                    p[10].ParameterName = "MA_NHANG";
                    p[10].DbType = DbType.String;
                    p[10].Direction = ParameterDirection.Input;
                    p[10].Value = MA_NHANG;

                    p[11] = new OracleParameter();
                    p[11].ParameterName = "TEN_NHANG";
                    p[11].DbType = DbType.String;
                    p[11].Direction = ParameterDirection.Input;
                    p[11].Value = TEN_NHANG;

                    p[12] = new OracleParameter();
                    p[12].ParameterName = "VAN_ID";
                    p[12].DbType = DbType.String;
                    p[12].Direction = ParameterDirection.Input;
                    p[12].Value = VAN_ID;

                    p[13] = new OracleParameter();
                    p[13].ParameterName = "TEN_TVAN";
                    p[13].DbType = DbType.String;
                    p[13].Direction = ParameterDirection.Input;
                    p[13].Value = TEN_TVAN;

                    p[14] = new OracleParameter();
                    p[14].ParameterName = "SIGNATURE_NNT";
                    p[14].DbType = DbType.String;
                    p[14].Direction = ParameterDirection.Input;
                    p[14].Value = SIGNATURE_NNT;

                    p[15] = new OracleParameter();
                    p[15].ParameterName = "SIGNATURE_TCT";
                    p[15].DbType = DbType.String;
                    p[15].Direction = ParameterDirection.Input;
                    p[15].Value = SIGNATURE_TCT;

                    p[16] = new OracleParameter();
                    p[16].ParameterName = "TRANG_THAI";
                    p[16].DbType = DbType.String;
                    p[16].Direction = ParameterDirection.Input;
                    p[16].Value = TRANG_THAI;

                    p[17] = new OracleParameter();
                    p[17].ParameterName = "MA_GDICH";
                    p[17].DbType = DbType.String;
                    p[17].Direction = ParameterDirection.Input;
                    p[17].Value = MA_GDICH;

                    p[18] = new OracleParameter();
                    p[18].ParameterName = "NGAY_GUI_GDICH";
                    p[18].DbType = DbType.String;
                    p[18].Direction = ParameterDirection.Input;
                    p[18].Value = NGAY_GUI_GDICH;

                    p[19] = new OracleParameter();
                    p[19].ParameterName = "MA_GDICH_TCHIEU";
                    p[19].DbType = DbType.String;
                    p[19].Direction = ParameterDirection.Input;
                    p[19].Value = MA_GDICH_TCHIEU;

                    p[20] = new OracleParameter();
                    p[20].ParameterName = "MST";
                    p[20].DbType = DbType.String;
                    p[20].Direction = ParameterDirection.Input;
                    p[20].Value = MST;


                   VBOracleLib.DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);

                    //Thông báo chấp nhận thay đổi

                    Business.NTDT.NNTDT.infNNTDT objNT = new Business.NTDT.NNTDT.infNNTDT();
                    objNT.DIACHI_NNT = DIACHI_NNT;
                    objNT.EMAIL_NNT = EMAIL_NNT;
                    objNT.ISSUER_CERT_NTHUE = ISSUER_CERT_NTHUE;
                    objNT.MA_CQT = MA_CQT;
                    objNT.MA_GDICH = MA_GDICH;
                    objNT.MA_GDICH_TCHIEU = MA_GDICH_TCHIEU;
                    objNT.MA_NHANG = MA_NHANG;
                    objNT.MST = MST;
                    objNT.SDT_NNT = SDT_NNT;
                    objNT.SERIAL_CERT_NTHUE = SERIAL_CERT_NTHUE;
                    objNT.SUBJECT_CERT_NTHUE = SUBJECT_CERT_NTHUE;
                    objNT.TEN_LHE_NTHUE = TEN_LHE_NTHUE;
                    objNT.TEN_NHANG = TEN_NHANG;
                    objNT.TEN_NNT = TEN_NNT;
                    objNT.TEN_TVAN = TEN_TVAN;
                    objNT.TENTK_NHANG = TEN_NHANG;
                    objNT.VAN_ID = VAN_ID;
                    ProcessThongBaoThayDoiTTNT_NH_TTXL msgtb = new ProcessThongBaoThayDoiTTNT_NH_TTXL();
                    msgtb.sendMSG06011(objNT);

                    //}
                }
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                throw ex;
            }
            finally
            {
              
                conn.Close();
                conn.Dispose();
            }

        }

        #endregion

    }
}
