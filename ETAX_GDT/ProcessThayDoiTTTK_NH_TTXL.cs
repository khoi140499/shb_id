﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG;
using GDT.MSG.MSG06013;
using System.Data.OracleClient;
using System.Data;
using Business.NTDT.NNTDT;
namespace ETAX_GDT
{
    public class ProcessThayDoiTTTK_NH_TTXL
    {
        private string strTran_Type = "";
        private string strMSG_TEXT = "";
        private string strMSG_ID = "";
        public ProcessThayDoiTTTK_NH_TTXL()
        {}

        public void sendmsg06013(infNNTDT objNNTDT, ref string errCode, ref string errMSG)
        {
            errCode = "00";
            try
            {

                if (objNNTDT != null)
                {
                    string xmlOut = "";
                    MSG06013 msg06013 = new MSG06013();
                    msg06013.HEADER = new HEADER();
                    msg06013.BODY = new BODY();
                    msg06013.BODY.ROW = new ROW();
                    msg06013.BODY.ROW.GIAODICH = new GIAODICH();
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH = new TBAO_TKHOAN_NH();
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN = new TTIN_TKHOAN();
                    

                    msg06013.HEADER.VERSION = "1.0";
                    msg06013.HEADER.SENDER_CODE = GDICH_NTDT_Constants.HEADER_SENDER_CODE;
                    msg06013.HEADER.SENDER_NAME = GDICH_NTDT_Constants.HEADER_SENDER_NAME;
                    msg06013.HEADER.RECEIVER_CODE = GDICH_NTDT_Constants.HEADER_RECEIVER_CODE;
                    msg06013.HEADER.RECEIVER_NAME = GDICH_NTDT_Constants.HEADER_RECEIVER_NAME;
                    msg06013.HEADER.TRAN_CODE = "06013";
                    msg06013.HEADER.MSG_ID = msg06013.HEADER.SENDER_CODE + Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ");
                    msg06013.HEADER.MSG_REFID = "";
                    msg06013.HEADER.ID_LINK = objNNTDT.MA_GDICH;
                    msg06013.HEADER.SEND_DATE = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                    msg06013.HEADER.ORIGINAL_CODE = "";
                    msg06013.HEADER.ORIGINAL_NAME = "";
                    msg06013.HEADER.ORIGINAL_DATE = "";
                    msg06013.HEADER.ERROR_CODE = "";
                    msg06013.HEADER.ERROR_DESC = "";
                    msg06013.HEADER.SPARE1 = "";
                    msg06013.HEADER.SPARE2 = "";
                    msg06013.HEADER.SPARE3 = "";

                    msg06013.BODY.ROW.GIAODICH.MA_GDICH = msg06013.HEADER.MSG_ID;
                    string year = DateTime.Now.ToString("yyyy");
                    string dayofyear = DateTime.Now.DayOfYear.ToString("000");
                    msg06013.BODY.ROW.GIAODICH.NGAY_GUI_GDICH = year + dayofyear + DateTime.Now.ToString("HHmmss");

                    msg06013.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU = objNNTDT.MA_GDICH;

                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.PBAN_TLIEU_XML = Utility.strPBAN_TLIEU_XML;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MST = objNNTDT.MST;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_NNT = objNNTDT.TEN_NNT;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.DIACHI_NNT = objNNTDT.DIACHI_NNT;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MA_CQT = objNNTDT.MA_CQT;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.EMAIL_NNT = objNNTDT.EMAIL_NNT;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.SDT_NNT = objNNTDT.SDT_NNT;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_LHE_NTHUE = objNNTDT.TEN_LHE_NTHUE;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.SERIAL_CERT_NTHUE = objNNTDT.SERIAL_CERT_NTHUE;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.SUBJECT_CERT_NTHUE = objNNTDT.SUBJECT_CERT_NTHUE;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.ISSUER_CERT_NTHUE = objNNTDT.ISSUER_CERT_NTHUE;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MA_NHANG = objNNTDT.MA_NHANG;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_NHANG = objNNTDT.TEN_NHANG;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.VAN_ID = objNNTDT.VAN_ID;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_TVAN = objNNTDT.TEN_TVAN;
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN.STK  = new List<String>();
                    if (!string.IsNullOrEmpty(objNNTDT.STK_NHANG))
                    {
                        if (objNNTDT.STK_NHANG.Contains(";"))
                        {
                            string[] arrTK = objNNTDT.STK_NHANG.Split(';');
                            foreach (string item in arrTK)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN.STK.Add(item);
                                }
                            }
                        }
                        else
                        {
                            msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN.STK.Add(objNNTDT.STK_NHANG);
                        }
                    }
                    else
                    {
                        msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN.STK.Add(string.Empty);
                    }
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTHAI_XNHAN = "";
                    msg06013.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.LDO_TCHOI = "";

                    xmlOut = msg06013.MSGtoXML(msg06013);



                    xmlOut = Utility.RomoveXML_Des(xmlOut);
                    //vinhvv sign
                    string signatureType = Utility.GetValue_THAMSOHT("SIGN.TYPE.TCT");
                    string xpathSign = "/DATA/BODY/ROW/GIAODICH/TBAO_TKHOAN_NH";
                    string xpathNodeSign = "TBAO_TKHOAN_NH";
                    if (signatureType.Equals("1"))
                    {
                        xmlOut = Signature.DigitalSignature.getSignNTDT(xmlOut, xpathSign, xpathNodeSign);
                    }
                    else
                    {
                        xmlOut = Signature.SignatureProcess.SignXMLEextendTimeStamp(xmlOut, "TBAO_TKHOAN_NH", "TBAO_TKHOAN_NH", true);

                    }
                    //vinhvv


                    strMSG_ID = msg06013.HEADER.MSG_ID;
                    strTran_Type = "MSG06013";
                    strMSG_TEXT = "PutMQ=StartTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);

                    clsMQHelper mq = new clsMQHelper();
                    string strReturn = "";
                    strReturn = mq.PutMQMessage(xmlOut);


                    strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "MQ Return: " + strReturn;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);

                    //insert log
                    GDT.MSG.MSG msg_log = new GDT.MSG.MSG();

                    msg_log.sender_code = msg06013.HEADER.SENDER_CODE;
                    msg_log.sender_name = msg06013.HEADER.SENDER_NAME;
                    msg_log.receiver_code = msg06013.HEADER.RECEIVER_CODE;
                    msg_log.receiver_name = msg06013.HEADER.RECEIVER_NAME;
                    msg_log.tran_code = msg06013.HEADER.TRAN_CODE;
                    msg_log.msg_id = msg06013.HEADER.MSG_ID;
                    msg_log.msg_refid = msg06013.HEADER.MSG_REFID;
                    msg_log.id_link = msg06013.HEADER.ID_LINK;
                    msg_log.send_date = msg06013.HEADER.SEND_DATE;
                    msg_log.original_code = msg06013.HEADER.ORIGINAL_CODE;
                    msg_log.original_name = msg06013.HEADER.ORIGINAL_NAME;
                    msg_log.original_date = msg06013.HEADER.ORIGINAL_DATE;
                    msg_log.error_code = msg06013.HEADER.ERROR_CODE;
                    msg_log.error_desc = msg06013.HEADER.ERROR_DESC;
                    if (!strReturn.Equals(""))
                    {
                        msg_log.error_code = "02";
                        if (strReturn.Length > 231)
                        {
                            strReturn = strReturn.Substring(0, 230);
                        }
                        msg_log.error_desc = strReturn;
                    }
                    msg_log.spare1 = msg06013.HEADER.SPARE1;
                    msg_log.spare2 = msg06013.HEADER.SPARE2;
                    msg_log.spare3 = msg06013.HEADER.SPARE3;
                    msg_log.trangthai = "00";
                    msg_log.content_mess = xmlOut;

                    string ma_tt = "";
                    Utility.insertMSGOutLog(msg_log, ref ma_tt);

                }
            }
            catch (Exception ex)
            {
                errCode = "01";
                errMSG = ex.Message;
                strMSG_TEXT = "PutMQ=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID +  ".Exception=" + ex.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            }

        }

    }
}
