﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace GDT.MSG.MSG07000
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG07000
    {
        public MSG07000()
        {
        }
        [XmlElement("HEADER")]
        public HEADER HEADER;
        [XmlElement("BODY")]
        public BODY BODY;
        //[XmlElement("SECURITY")]
        //public SECURITY SECURITY;

        public string MSGtoXML(MSG07000 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG07000));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG07000 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSG07000.MSG07000));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);
            
            XmlReader reader = XmlReader.Create(new StringReader(p_XML));
            
            GDT.MSG.MSG07000.MSG07000 LoadedObjTmp = (GDT.MSG.MSG07000.MSG07000)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }
    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW ROW;
    }

    public class ROW
    {
        public ROW()
        { }
        [XmlElement("GIAODICH_DC")]
        public GIAODICH_DC GIAODICH_DC;
    }

    public class GIAODICH_DC
    {
        public GIAODICH_DC()
        { }

        public string PBAN_TLIEU_XML_DC { get; set; }
        public string MA_GDICH_DC { get; set; }
        public string NGAY_GDICH_DC { get; set; }
        public string MA_NHANG_DC { get; set; }
        public string NGAY_DC { get; set; }
        public string TU_NGAY_DC { get; set; }
        public string DEN_NGAY_DC { get; set; }
        public string MA_GDICH_TCHIEU_DC { get; set; }
        [XmlElement("NDUNG_DC")]
        public NDUNG_DC NDUNG_DC;
    }

    public class NDUNG_DC
    {
        public NDUNG_DC()
        { }
        public string MSGtoXML(NDUNG_DC p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(NDUNG_DC));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public NDUNG_DC MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(NDUNG_DC));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            NDUNG_DC LoadedObjTmp = (NDUNG_DC)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
        [XmlElement("GIAODICH")]
        public List<GIAODICH> GIAODICH;
    }

    public class GIAODICH
    {
        public GIAODICH()
        { }
        public string MA_GDICH { get; set; }
        public string NGAY_GUI_GDICH { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string TTHAI_GDICH { get; set; }
        public string TTHAI_CTU { get; set; }
        [XmlElement("CHUNGTU")]
        public CHUNGTU CHUNGTU;

    }

    public class CHUNGTU
    {
        public CHUNGTU()
        {
            
        }
        [XmlElement("NDUNG_CTU_NH")]
        public NDUNG_CTU_NH NDUNG_CTU_NH;
        public List<string> Signature { get; set; }

    }

    public class NDUNG_CTU_NH
    {
        public NDUNG_CTU_NH() 
        {
        }
        public string MAHIEU_CTU { get; set; }
        public string SO_CTU { get; set; }

        // tuanda - 05/02/24
        public string NGAY_CTU { get; set; }

        [XmlElement("NDUNG_CTU")]
        public NDUNG_CTU NDUNG_CTU;

    }

    public class NDUNG_CTU
    {
        public NDUNG_CTU()
        { 
        }
        [XmlElement("CHUNGTU_HDR")]
        public CHUNGTU_HDR CHUNGTU_HDR;
        [XmlElement("CHUNGTU_CTIET")]
        public CHUNGTU_CTIET CHUNGTU_CTIET;

    }

    public class CHUNGTU_HDR
    {
        public CHUNGTU_HDR()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string ID_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string MA_CTU { get; set; }
        public string HTHUC_NOP { get; set; }
        public string MST_NNOP { get; set; }
        public string TEN_NNOP { get; set; }
        public string MA_CQT { get; set; }
        public string TEN_CQT { get; set; }
        public string DIACHI_NNOP { get; set; }
        public string MA_XA_NNOP { get; set; }
        public string TEN_XA_NNOP { get; set; }
        public string MA_HUYEN_NNOP { get; set; }
        public string TEN_HUYEN_NNOP { get; set; }
        public string MA_TINH_NNOP { get; set; }
        public string TEN_TINH_NNOP { get; set; }
        public string MST_NTHAY { get; set; }
        public string TEN_NTHAY { get; set; }
        public string DIACHI_NTHAY { get; set; }
        public string TEN_HUYEN_NTHAY { get; set; }
        public string TEN_TINH_NTHAY { get; set; }
        public string MA_NHANG_NOP { get; set; }
        public string TEN_NHANG_NOP { get; set; }
        public string STK_NHANG_NOP { get; set; }
        public string MA_HIEU_KBAC { get; set; }
        public string TEN_KBAC { get; set; }
        public string MA_TINH_KBAC { get; set; }
        public string TEN_TINH_KBAC { get; set; }
        public string LOAI_TK_THU { get; set; }
        public string TEN_TK_THU { get; set; }
        public string STK_THU { get; set; }
        public string ID_TK_KNGHI { get; set; }
        public string TK_KNGHI { get; set; }
        public string MA_CQTHU { get; set; }
        public string TEN_CQTHU { get; set; }
        public string NGAY_LAP { get; set; }
        public string TONG_TIEN { get; set; }
        public string VAN_ID { get; set; }

        // tuanda - 05/02/24
        public string MA_LOAI_THUE { get; set; }
        public string MA_NGUYENTE { get; set; }
        public string MA_NHANG_UNT { get; set; }
        public string TEN_NHANG_UNT { get; set; }

        public string MA_THAMCHIEU { get; set; }
        public string LHINH_NNT { get; set; }

    }

    public class CHUNGTU_CTIET
    {
        public CHUNGTU_CTIET()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET> ROW_CTIET;

    }

    public class ROW_CTIET
    {
        public ROW_CTIET()
        { 
        
        }
        public string ID_CTU_CTIET { get; set; }
        public string ID_CTU { get; set; }
        public string NDUNG_NOP { get; set; }
        public string MA_NDKT { get; set; }
        public string MA_CHUONG { get; set; }
        public string KY_THUE { get; set; }
        public string TIEN_PNOP { get; set; }

        // tuanda - 05/02/24
        public string GHI_CHU { get; set; }
        public string SO_TK_TB_QD { get; set; }
        public string MA_DBHC_THU { get; set; }
        public string TEN_DBHC_THU { get; set; }
        public string ID_KNOP { get; set; }
       
    }

    //public class SECURITY
    //{
    //    public SECURITY() 
    //    { 
    //    }
    //    public string SIGNATURE { get; set; }
    //}
}
