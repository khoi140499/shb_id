﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace GDT.MSG.MSG07001
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG07001
    {
        public MSG07001()
        {
        }
        [XmlElement("HEADER")]
        public HEADER HEADER;
        [XmlElement("BODY")]
        public BODY BODY;
        

        public string MSGtoXML(MSG07001 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG07001));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG07001 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSG07001.MSG07001));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);
            
            XmlReader reader = XmlReader.Create(new StringReader(p_XML));
            
            GDT.MSG.MSG07001.MSG07001 LoadedObjTmp = (GDT.MSG.MSG07001.MSG07001)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }
    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW ROW;
    }

    public class ROW
    {
        public ROW()
        { }
        [XmlElement("GIAODICH_DC")]
        public GIAODICH_DC GIAODICH_DC;
    }

    public class GIAODICH_DC
    {
        public GIAODICH_DC()
        { }
        [XmlElementAttribute(Order = 1)]
        public string PBAN_TLIEU_XML_DC { get; set; }
        [XmlElementAttribute(Order = 2)]
        public string MA_GDICH_DC { get; set; }
        [XmlElementAttribute(Order = 3)]
        public string NGAY_GDICH_DC { get; set; }
        [XmlElementAttribute(Order = 4)]
        public string MA_NHANG_DC { get; set; }
        [XmlElementAttribute(Order = 5)]
        public string NGAY_DC { get; set; }
        [XmlElementAttribute(Order = 6)]
        public string TU_NGAY_DC { get; set; }
        [XmlElementAttribute(Order = 7)]
        public string DEN_NGAY_DC { get; set; }
        [XmlElementAttribute(Order = 8)]
        public string MA_GDICH_TCHIEU_DC { get; set; }
        [XmlElementAttribute(Order = 9)]
        public NDUNG_DC NDUNG_DC;
        //public string Signature { get; set; }
    }

    public class NDUNG_DC
    {
        public NDUNG_DC()
        { }
        [XmlElement(ElementName = "BAOCAO", Order = 1)]
        public BAOCAO01 BAOCAO01;

        [XmlElement(ElementName = "BAOCAO", Order = 2)]
        public BAOCAO02 BAOCAO02;

        [XmlElement(ElementName = "BAOCAO", Order = 3)]
        public BAOCAO03 BAOCAO03;

        [XmlElement(ElementName = "BAOCAO", Order = 4)]
        public BAOCAO04 BAOCAO04;

        [XmlElement(ElementName = "BAOCAO", Order = 5)]
        public BAOCAO05 BAOCAO05;

    }

    #region "Bao cao doi chieu 01"
    public class BAOCAO01
    {
        public BAOCAO01()
        { }


        [XmlElement(ElementName="BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR01 BAOCAO_HDR01;

        
        [XmlElement(ElementName="BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET01 BAOCAO_CTIET01;


    }

    public class BAOCAO_HDR01
    {
        public BAOCAO_HDR01()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string MA_BCAO { get; set; }
        public string TEN_BCAO { get; set; }
        public string TGIAN_DCHIEU { get; set; }
        public string MA_NHANG_DCHIEU { get; set; }
        public string TEN_NHANG_DCHIEU { get; set; }
        public string SODIEN_TCONG { get; set; }
        public string TONGTIEN_TCONG { get; set; }
        public string SODIEN_HTRUOC { get; set; }
        public string TONGTIEN_HTRUOC { get; set; }
        public string SODIEN_HSAU { get; set; }
        public string TONGTIEN_HSAU { get; set; }
    }

    public class BAOCAO_CTIET01
    {
        public BAOCAO_CTIET01()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET01> ROW_CTIET01;
    }

    public class ROW_CTIET01
    {
        public ROW_CTIET01()
        { }
        public string SO_CTU { get; set; }
        public string MAHIEU_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string TEN_NNT { get; set; }
        public string MST { get; set; }
        public string MA_CQT { get; set; }
        public string MA_CQTHU { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string NGAY_NOP_THUE { get; set; }
        public string TGIAN_TCT { get; set; }
        public string TGIAN_NHTM { get; set; }
        public string SO_TIEN { get; set; }
        public string TTHAI { get; set; }
        public string DIEN_HSAU { get; set; }
    }
    #endregion
    
    
    #region "Bao cao doi chieu 02"
    public class BAOCAO02
    {
        public BAOCAO02()
        { }
        [XmlElement(ElementName="BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR02 BAOCAO_HDR02;
        [XmlElement(ElementName="BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET02 BAOCAO_CTIET02;

    }

    public class BAOCAO_HDR02
    {
        public BAOCAO_HDR02()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string MA_BCAO { get; set; }
        public string TEN_BCAO { get; set; }
        public string TGIAN_DCHIEU { get; set; }
        public string MA_NHANG_DCHIEU { get; set; }
        public string TEN_NHANG_DCHIEU { get; set; }
        public string TONGDIEN_LECH { get; set; }
        public string TONGTIEN_LECH { get; set; }
        public string TONGDIEN_TCTCO_NHTMKO { get; set; }
        public string TONGTIEN_TCTCO_NHTMKO { get; set; }
        public string TONGDIEN_NHTMCO_TCTKO { get; set; }
        public string TONGTIEN_NHTMCO_TCTKO { get; set; }
        public string TONGDIEN_LECH_TTHAI { get; set; }
        public string TONGTIEN_LECH_TTHAI { get; set; }

    }

    public class BAOCAO_CTIET02
    {
        public BAOCAO_CTIET02()
        { }
        [XmlElement(ElementName = "NHTMCO_TCTKO", Order = 1)]
        public NHTMCO_TCTKO NHTMCO_TCTKO;

        
        [XmlElement(ElementName="TCTCO_NHTMKO", Order=2)]
        public TCTCO_NHTMKO TCTCO_NHTMKO;

        [XmlElement(ElementName="LECH_TTHAI", Order=3)]
        public LECH_TTHAI LECH_TTHAI;
            
    }

    public class TCTCO_NHTMKO
    {
        public TCTCO_NHTMKO()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET02> ROW_CTIET02;
    }

    public class NHTMCO_TCTKO
    {
        public NHTMCO_TCTKO()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET02> ROW_CTIET02;
    }

    public class LECH_TTHAI
    {
        public LECH_TTHAI()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET02> ROW_CTIET02;
    }

    public class ROW_CTIET02
    {
        public ROW_CTIET02()
        { }
        public string SO_CTU { get; set; }
        public string MAHIEU_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string TEN_NNT { get; set; }
        public string MST { get; set; }
        public string MA_CQT { get; set; }
        public string MA_CQTHU { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string NGAY_NOP_THUE { get; set; }
        public string TGIAN_TCT { get; set; }
        public string TGIAN_NHTM { get; set; }
        public string SO_TIEN { get; set; }
        public string TTHAI_TCT { get; set; }
        public string TTHAI_NHTM { get; set; }
    }
    #endregion

    
    #region "Bao cao doi chieu 03"
    public class BAOCAO03
    {
        public BAOCAO03()
        { }
        [XmlElement(ElementName="BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR03 BAOCAO_HDR03;

        [XmlElement(ElementName="BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET03 BAOCAO_CTIET03;


    }

    public class BAOCAO_HDR03
    {
        public BAOCAO_HDR03()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string MA_BCAO { get; set; }
        public string TEN_BCAO { get; set; }
        public string TGIAN_DCHIEU { get; set; }
        public string MA_NHANG_DCHIEU { get; set; }
        public string TEN_NHANG_DCHIEU { get; set; }
        public string TONGDIEN_TNHAN { get; set; }
        public string TONGTIEN_TNHAN { get; set; }
        public string TONGDIEN_TCTCO_NHTMCO { get; set; }
        public string TONGTIEN_TCTCO_NHTMCO { get; set; }
        public string TONGDIEN_TCTCO_NHTMKO { get; set; }
        public string TONGTIEN_TCTCO_NHTMKO { get; set; }
        public string TONGDIEN_NHTMCO_TCTKO { get; set; }
        public string TONGTIEN_NHTMCO_TCTKO { get; set; }
        public string TONGDIEN_LECH_TTHAI { get; set; }
        public string TONGTIEN_LECH_TTHAI { get; set; }
    
    }

    public class BAOCAO_CTIET03
    {
        public BAOCAO_CTIET03()
        { }
        [XmlElement(ElementName="TCTCO_NHTMCO", Order=1)]
        public TCTCO_NHTMCO TCTCO_NHTMCO;

        [XmlElement(ElementName = "LECH_TTHAI", Order=2)]
        public LECH_TTHAI LECH_TTHAI;

        [XmlElement(ElementName = "TCTCO_NHTMKO", Order=3)]
        public TCTCO_NHTMKO TCTCO_NHTMKO;

        [XmlElement(ElementName = "NHTMCO_TCTKO", Order=4)]
        public NHTMCO_TCTKO NHTMCO_TCTKO;

    }

    public class TCTCO_NHTMCO
    {
        public TCTCO_NHTMCO()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET02> ROW_CTIET03;
    }
    #endregion

   
    #region Bao cao doi chieu 04
    public class BAOCAO04
    {
        public BAOCAO04()
        { }
        [XmlElement(ElementName = "BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR04 BAOCAO_HDR04;

        [XmlElement(ElementName = "BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET04 BAOCAO_CTIET04;


    }

    public class BAOCAO_HDR04
    {
        public BAOCAO_HDR04()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string MA_BCAO { get; set; }
        public string TEN_BCAO { get; set; }
        public string TGIAN_DCHIEU { get; set; }
        public string MA_NHANG_DCHIEU { get; set; }
        public string TEN_NHANG_DCHIEU { get; set; }
    }

    public class BAOCAO_CTIET04
    {
        public BAOCAO_CTIET04()
        { }
        [XmlElement(ElementName = "DIEN_TCONG", Order=1)]
        public DIEN_TCONG DIEN_TCONG;

        [XmlElement(ElementName = "DIEN_KO_TCONG", Order=2)]
        public DIEN_KO_TCONG DIEN_KO_TCONG;

        [XmlElement(ElementName = "DIEN_TUCHOI", Order=3)]
        public DIEN_TUCHOI DIEN_TUCHOI;
    }

    public class DIEN_TCONG
    {
        public DIEN_TCONG()
        { }

        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET04> ROW_CTIET04;
    }

    public class DIEN_KO_TCONG
    {
        public DIEN_KO_TCONG()
        { }

        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET04> ROW_CTIET04;
    }

    public class DIEN_TUCHOI
    {
        public DIEN_TUCHOI()
        { }

        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET04> ROW_CTIET04;
    }

    public class ROW_CTIET04
    {
        public ROW_CTIET04()
        { }
        public string SO_CTU { get; set; }
        public string MAHIEU_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string NGAY_NOP_THUE { get; set; }
        public string TEN_NNT { get; set; }
        public string MST { get; set; }
        public string MA_CQT { get; set; }
        public string MA_CQTHU { get; set; }
        public string MA_KBNN { get; set; }
        public string MA_CHUONG { get; set; }
        public string MA_TMUC { get; set; }
        public string SO_TIEN { get; set; }
        public string TTHAI { get; set; }

    }
    #endregion

    #region "Bao cao doi chieu 05"
    public class BAOCAO05
    {
        public BAOCAO05()
        { }
        [XmlElement(ElementName = "BAOCAO_HDR", Order=1)]
        public BAOCAO_HDR05 BAOCAO_HDR05;

        [XmlElement(ElementName = "BAOCAO_CTIET", Order=2)]
        public BAOCAO_CTIET05 BAOCAO_CTIET05;


    }

    public class BAOCAO_HDR05
    {
        public BAOCAO_HDR05()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string MA_BCAO { get; set; }
        public string TEN_BCAO { get; set; }
        public string TGIAN_DCHIEU { get; set; }
        public string MA_NHANG_DCHIEU { get; set; }
        public string TEN_NHANG_DCHIEU { get; set; }
    }

    public class BAOCAO_CTIET05
    {
        public BAOCAO_CTIET05()
        { }

        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET05> ROW_CTIET05;

    }

    public class ROW_CTIET05
    {
        public ROW_CTIET05()
        { }
        public string SO_CTU { get; set; }
        public string MAHIEU_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string NGAY_NOP_GNT { get; set; }
        public string NGAY_NOP_THUE { get; set; }
        public string TEN_NNT { get; set; }
        public string MST { get; set; }
        public string MA_CQT { get; set; }
        public string MA_CQTHU { get; set; }
        public string MA_KBNN { get; set; }
        public string TCT_MA_CHUONG { get; set; }
        public string TCT_MA_TMUC { get; set; }
        public string TCT_SO_TIEN { get; set; }
        public string TCT_TTHAI { get; set; }
        public string NHTM_MA_CHUONG { get; set; }
        public string NHTM_MA_TMUC { get; set; }
        public string NHTM_SO_TIEN { get; set; }
        public string NHTM_TTHAI { get; set; }

    }
    #endregion
    
    
}
