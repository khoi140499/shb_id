﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace GDT.MSG.MSGXXXX
{

    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSGXXXX
    {
        public MSGXXXX()
        {
        }
        [XmlElement("HEADER")]
        public HEADER HEADER;
        [XmlElement("BODY")]
        public BODY BODY;
        //[XmlElement("SECURITY")]
        //public SECURITY SECURITY;

        public string MSGtoXML(MSGXXXX p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSGXXXX));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSGXXXX MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSGXXXX.MSGXXXX));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            GDT.MSG.MSGXXXX.MSGXXXX LoadedObjTmp = (GDT.MSG.MSGXXXX.MSGXXXX)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }


    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }
    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW ROW;
    }

    public class ROW
    {
        public ROW()
        { }
        [XmlElement("GIAODICH")]
        public GIAODICH GIAODICH;
    }

    public class GIAODICH
    {
        public GIAODICH()
        { }
        public string MA_GDICH { get; set; }
        public string NGAY_GUI_GDICH { get; set; }
        [XmlElement("NOIDUNG")]
        public NOIDUNG NOIDUNG;
    }

    public class NOIDUNG
    {
        public NOIDUNG()
        { }
        [XmlElement("NDUNG_CTIET")]
        public NDUNG_CTIET NDUNG_CTIET;
    }

    public class NDUNG_CTIET
    {
        public NDUNG_CTIET()
        { }
        public string MA_THAMCHIEU { get; set; }
        public string MST_NNOP { get; set; }
        public string TEN_NNOP { get; set; }
        public string SO_CTU { get; set; }
        public string NGAY_GUI { get; set; }
        public string NOI_NHAN { get; set; }
        public string TRANG_THAI { get; set; }
        public string HTHUC_NOP { get; set; }
    }

}
