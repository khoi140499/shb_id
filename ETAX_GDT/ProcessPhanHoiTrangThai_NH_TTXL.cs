﻿using GDT.MSG.MSGXXXX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ETAX_GDT
{
    public class ProcessPhanHoiTrangThai_NH_TTXL
    {
        private string ma_cn;
        private string sender_code;
        private string sender_name;
        public ProcessPhanHoiTrangThai_NH_TTXL(string ma_cn1, string sender_code1, string sender_name1)
        {
            ma_cn = ma_cn1;
            sender_code = sender_code1;
            sender_name = sender_name1;
        }

        public void sendTBTrangThaiToTCT(string so_tham_chieu)
        {
            string strMSG_ID = "";
            string strTran_Type = "";
            string strMSG_TEXT = "";
            try
            {

                DataSet dsCT;
                string strSQL = "SELECT * FROM tcs_ctu_ntdt_hdr WHERE MA_THAMCHIEU_KNOP = '" + so_tham_chieu + "'";
                dsCT = Utility.ExecuteReturnDataSet(strSQL, CommandType.Text);

                if (dsCT != null)
                {
                    if (dsCT.Tables[0].Rows.Count > 0)
                    {
                        string xmlOut = "";
                        MSGXXXX msgxxxx = new MSGXXXX();
                        msgxxxx.HEADER = new HEADER();
                        msgxxxx.BODY = new BODY();
                        msgxxxx.BODY.ROW = new ROW();
                        msgxxxx.BODY.ROW.GIAODICH = new GIAODICH();


                        msgxxxx.HEADER.VERSION = "1.0";
                        msgxxxx.HEADER.SENDER_CODE = GDICH_NTDT_Constants.HEADER_SENDER_CODE;
                        msgxxxx.HEADER.SENDER_NAME = GDICH_NTDT_Constants.HEADER_SENDER_NAME;
                        msgxxxx.HEADER.RECEIVER_CODE = GDICH_NTDT_Constants.HEADER_RECEIVER_CODE;
                        msgxxxx.HEADER.RECEIVER_NAME = GDICH_NTDT_Constants.HEADER_RECEIVER_NAME;
                        msgxxxx.HEADER.TRAN_CODE = "XXXX";
                        msgxxxx.HEADER.MSG_ID = msgxxxx.HEADER.SENDER_CODE + Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ");
                        msgxxxx.HEADER.MSG_REFID = "";
                        msgxxxx.HEADER.ID_LINK = ""; // xxxx chua bit dien gia tri gi
                        msgxxxx.HEADER.SEND_DATE = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                        msgxxxx.HEADER.ORIGINAL_CODE = "";
                        msgxxxx.HEADER.ORIGINAL_NAME = "";
                        msgxxxx.HEADER.ORIGINAL_DATE = "";
                        msgxxxx.HEADER.ERROR_CODE = "";
                        msgxxxx.HEADER.ERROR_DESC = "";
                        msgxxxx.HEADER.SPARE1 = "";
                        msgxxxx.HEADER.SPARE2 = "";
                        msgxxxx.HEADER.SPARE3 = "";

                        msgxxxx.BODY.ROW.GIAODICH.MA_GDICH = msgxxxx.HEADER.MSG_ID;
                        string year = DateTime.Now.ToString("yyyy");
                        string dayofyear = DateTime.Now.DayOfYear.ToString("000");
                        msgxxxx.BODY.ROW.GIAODICH.NGAY_GUI_GDICH = year + dayofyear + DateTime.Now.ToString("HHmmss");

                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG = new NOIDUNG();
                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG.NDUNG_CTIET = new NDUNG_CTIET();

                        string TT_SP = dsCT.Tables[0].Rows[0]["TT_SP"].ToString();
                        string ma_unt = dsCT.Tables[0].Rows[0]["ma_nhang_unt"].ToString();
                        ma_unt = ma_unt.Substring(2, 3);
                        if (TT_SP.Equals("1") || TT_SP.Equals("2"))
                        {
                            //KBNN
                            ma_unt = "000";
                        }

                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG.NDUNG_CTIET.MA_THAMCHIEU = so_tham_chieu;
                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG.NDUNG_CTIET.MST_NNOP = dsCT.Tables[0].Rows[0]["MST_NNOP"].ToString();
                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG.NDUNG_CTIET.TEN_NNOP = dsCT.Tables[0].Rows[0]["TEN_NNOP"].ToString();
                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG.NDUNG_CTIET.SO_CTU = dsCT.Tables[0].Rows[0]["SO_CTU"].ToString();
                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG.NDUNG_CTIET.NGAY_GUI = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG.NDUNG_CTIET.NOI_NHAN = Utility.GetNoiXuLyChungTu(ma_unt);
                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG.NDUNG_CTIET.TRANG_THAI = dsCT.Tables[0].Rows[0]["trangthai_sendkb"].ToString();  //01,02,03,04
                        msgxxxx.BODY.ROW.GIAODICH.NOIDUNG.NDUNG_CTIET.HTHUC_NOP = "00";



                        xmlOut = msgxxxx.MSGtoXML(msgxxxx);



                        xmlOut = Utility.RomoveXML_Des(xmlOut);
                        //vinhvv sign
                        string signatureType = Utility.GetValue_THAMSOHT("SIGN.TYPE.TCT");
                        string xpathSign = "/DATA/BODY/ROW/GIAODICH/NOIDUNG/NDUNG_CTIET";
                        string xpathNodeSign = "NDUNG_CTIET";

                        if (signatureType.Equals("1"))
                        {
                            xmlOut = Signature.DigitalSignature.getSignNTDT(xmlOut, xpathSign, xpathNodeSign);
                        }
                        else
                        {
                            xmlOut = Signature.SignatureProcess.SignXMLEextendTimeStamp(xmlOut, "TBAO_TKHOAN_NT", "TBAO_TKHOAN_NT", true);
                        }

                        //vinhvv
                        strMSG_ID = msgxxxx.HEADER.MSG_ID;
                        strTran_Type = "XXXXX";
                        strMSG_TEXT = "PutMQ=StartTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
                        Utility.Log_Action(strTran_Type, strMSG_TEXT);

                        //clsMQHelper mq = new clsMQHelper();
                        string strReturn = "";
                        //strReturn = mq.PutMQMessage(xmlOut);

                        if (msgxxxx.HEADER.SENDER_CODE.Equals(ConfigurationManager.AppSettings.Get("HEADER_SENDER_CODE").ToString()))
                        {
                            clsMQHelper mq = new clsMQHelper();

                            
                            strReturn = mq.PutMQMessage(xmlOut);

                            strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "MQ Return: " + strReturn;
                            Utility.Log_Action(strTran_Type, strMSG_TEXT);

                        }
                        else
                        {
                            string Ma_CN = ConfigurationManager.AppSettings.Get("MA_CN_HCM").ToString();
                            clsMQHelper mq = new clsMQHelper();

                            strReturn = mq.PutMQMessage(xmlOut);

                            strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "MQ Return: " + strReturn;
                            Utility.Log_Action(strTran_Type, strMSG_TEXT);
                        }



                        strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "MQ Return: " + strReturn;
                        Utility.Log_Action(strTran_Type, strMSG_TEXT);

                        //insert log
                        GDT.MSG.MSG msg_log = new GDT.MSG.MSG();

                        msg_log.sender_code = msgxxxx.HEADER.SENDER_CODE;
                        msg_log.sender_name = msgxxxx.HEADER.SENDER_NAME;
                        msg_log.receiver_code = msgxxxx.HEADER.RECEIVER_CODE;
                        msg_log.receiver_name = msgxxxx.HEADER.RECEIVER_NAME;
                        msg_log.tran_code = msgxxxx.HEADER.TRAN_CODE;
                        msg_log.msg_id = msgxxxx.HEADER.MSG_ID;
                        msg_log.msg_refid = msgxxxx.HEADER.MSG_REFID;
                        msg_log.id_link = msgxxxx.HEADER.ID_LINK;
                        msg_log.send_date = msgxxxx.HEADER.SEND_DATE;
                        msg_log.original_code = msgxxxx.HEADER.ORIGINAL_CODE;
                        msg_log.original_name = msgxxxx.HEADER.ORIGINAL_NAME;
                        msg_log.original_date = msgxxxx.HEADER.ORIGINAL_DATE;
                        msg_log.error_code = msgxxxx.HEADER.ERROR_CODE;
                        msg_log.error_desc = msgxxxx.HEADER.ERROR_DESC;
                        if (!strReturn.Equals(""))
                        {
                            msg_log.error_code = "02";
                            if (strReturn.Length > 231)
                            {
                                strReturn = strReturn.Substring(0, 230);
                            }
                            msg_log.error_desc = strReturn;
                        }
                        msg_log.spare1 = msgxxxx.HEADER.SPARE1;
                        msg_log.spare2 = msgxxxx.HEADER.SPARE2;
                        msg_log.spare3 = msgxxxx.HEADER.SPARE3;
                        msg_log.trangthai = "00";
                        msg_log.content_mess = xmlOut;

                        string ma_tt = "";
                        Utility.insertMSGOutLog(msg_log, ref ma_tt);
                    }

                }
            }
            catch (Exception ex)
            {

                strMSG_TEXT = "PutMQ=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + ".Exception=" + ex.Message.ToString() + ". " + ex.StackTrace.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
                throw ex;
            }

            //return "0";
        }
    }
}
