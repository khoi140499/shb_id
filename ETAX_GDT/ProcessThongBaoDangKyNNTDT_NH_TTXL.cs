﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDT.MSG.MSG06007;
using System.Data.OracleClient;
using System.Data;
using Business.NTDT.NNTDT;
using System.Configuration;
namespace ETAX_GDT
{
    
    public class ProcessThongBaoDangKyNNTDT_NH_TTXL
    {
        private string strTran_Type = "";
        private string strMSG_TEXT = "";
        private string strMSG_ID = "";
        public ProcessThongBaoDangKyNNTDT_NH_TTXL()
        {
           
        }

        public void sendMSG06007(infNNTDT objNNTDT, ref string errorCode, ref string errorMsg)
        {
            errorCode = "00";
            try
            {

                if (objNNTDT != null)
                {
                    string xmlOut = "";
                    MSG06007 msg06007 = new MSG06007();
                    msg06007.HEADER = new HEADER();
                    msg06007.BODY = new BODY();
                    msg06007.BODY.ROW = new ROW();
                    msg06007.BODY.ROW.GIAODICH = new GIAODICH();
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH = new TBAO_TKHOAN_NH();
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN = new TTIN_TKHOAN();
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN.STK = new List<String>();
                   // msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.Signature = new List<string>();

                    msg06007.HEADER.VERSION = "1.0";
                    msg06007.HEADER.SENDER_CODE = GDICH_NTDT_Constants.HEADER_SENDER_CODE;
                    msg06007.HEADER.SENDER_NAME = GDICH_NTDT_Constants.HEADER_SENDER_NAME;
                    msg06007.HEADER.RECEIVER_CODE = GDICH_NTDT_Constants.HEADER_RECEIVER_CODE;
                    msg06007.HEADER.RECEIVER_NAME = GDICH_NTDT_Constants.HEADER_RECEIVER_NAME;
                    msg06007.HEADER.TRAN_CODE = "06007";
                    msg06007.HEADER.MSG_ID = msg06007.HEADER.SENDER_CODE + Utility.TCS_GetSequence("NTDT_MSG_ID_SEQ");
                    msg06007.HEADER.MSG_REFID = "";
                    msg06007.HEADER.ID_LINK = objNNTDT.MA_GDICH;
                    msg06007.HEADER.SEND_DATE = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                    msg06007.HEADER.ORIGINAL_CODE = "";
                    msg06007.HEADER.ORIGINAL_NAME = "";
                    msg06007.HEADER.ORIGINAL_DATE = "";
                    msg06007.HEADER.ERROR_CODE = "";
                    msg06007.HEADER.ERROR_DESC = "";
                    msg06007.HEADER.SPARE1 = "";
                    msg06007.HEADER.SPARE2 = "";
                    msg06007.HEADER.SPARE3 = "";

                    msg06007.BODY.ROW.GIAODICH.MA_GDICH = msg06007.HEADER.MSG_ID;


                    string year = DateTime.Now.ToString("yyyy");
                    string dayofyear = DateTime.Now.DayOfYear.ToString("000");
                    msg06007.BODY.ROW.GIAODICH.NGAY_GUI_GDICH = year + dayofyear + DateTime.Now.ToString("HHmmss");


                    msg06007.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU = objNNTDT.MA_GDICH;

                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.PBAN_TLIEU_XML = objNNTDT.PBAN_TLIEU_XML;

                    if (objNNTDT.TRANG_THAI == "03" || objNNTDT.TRANG_THAI == "08")
                    {
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MA_TBAO = "02";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MAU_TBAO = "01/TB-NHTM";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.SO_TBAO = msg06007.HEADER.MSG_ID;
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_TBAO = "Thông báo chấp nhận đăng ký sử dụng dịch vụ nộp thuế điện tử qua NHTM";
                    }
                    else if (objNNTDT.TRANG_THAI == "04" || objNNTDT.TRANG_THAI == "09")
                    {
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MA_TBAO = "03";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MAU_TBAO = "02/TB-NHTM";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.SO_TBAO = msg06007.HEADER.MSG_ID;
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_TBAO = "Thông báo không chấp nhận đăng ký sử dụng dịch vụ nộp thuế điện tử của NHTM";
                    }
                    else
                    {
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MA_TBAO = "";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MAU_TBAO = "";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.SO_TBAO = "";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_TBAO = "";

                    }
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.NGAY_TBAO = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");


                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MST = objNNTDT.MST;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_NNT = objNNTDT.TEN_NNT;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.DIACHI_NNT = objNNTDT.DIACHI_NNT;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MA_CQT = objNNTDT.MA_CQT;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.EMAIL_NNT = objNNTDT.EMAIL_NNT;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.SDT_NNT = objNNTDT.SDT_NNT;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_LHE_NTHUE = objNNTDT.TEN_LHE_NTHUE;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.SERIAL_CERT_NTHUE = objNNTDT.SERIAL_CERT_NTHUE;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.SUBJECT_CERT_NTHUE = objNNTDT.SUBJECT_CERT_NTHUE;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.ISSUER_CERT_NTHUE = objNNTDT.ISSUER_CERT_NTHUE;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.MA_NHANG = objNNTDT.MA_NHANG;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_NHANG = objNNTDT.TEN_NHANG;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.VAN_ID = objNNTDT.VAN_ID;
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TEN_TVAN = objNNTDT.TEN_TVAN;
                    if (!string.IsNullOrEmpty(objNNTDT.STK_NHANG))
                    {
                        if (objNNTDT.STK_NHANG.Contains(";"))
                        {
                            string[] arrTK = objNNTDT.STK_NHANG.Split(';');
                            foreach (string item in arrTK)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN.STK.Add(item);
                                }
                            }
                        }
                        else
                        {
                            msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN.STK.Add(objNNTDT.STK_NHANG);
                        }
                    }
                    else
                    {
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTIN_TKHOAN.STK.Add(string.Empty);
                    }
                    msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.PBAN_TLIEU_XML = Utility.strPBAN_TLIEU_XML;

                    if (objNNTDT.TRANG_THAI == "03" || objNNTDT.TRANG_THAI == "08")
                    {
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTHAI_XNHAN = "Y";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.LDO_TCHOI = "";
                    }
                    else if (objNNTDT.TRANG_THAI == "04" || objNNTDT.TRANG_THAI == "09")
                    {
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTHAI_XNHAN = "N";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.LDO_TCHOI = objNNTDT.LDO_TCHOI;
                    }
                    else
                    {
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.TTHAI_XNHAN = "";
                        msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.LDO_TCHOI = "";
                    }

                    //msg06007.BODY.ROW.GIAODICH.TBAO_TKHOAN_NH.Signature.Add("");

                    xmlOut = msg06007.MSGtoXML(msg06007);

                    xmlOut = Utility.RomoveXML_Des(xmlOut);
                    //vinhvv sign
                    string signatureType = Utility.GetValue_THAMSOHT("SIGN.TYPE.TCT");
                    string xpathSign = "/DATA/BODY/ROW/GIAODICH/TBAO_TKHOAN_NH";
                    string xpathNodeSign = "TBAO_TKHOAN_NH";
                    if (signatureType.Equals("1"))
                    {
                        xmlOut = Signature.DigitalSignature.getSignNTDT(xmlOut, xpathSign, xpathNodeSign);
                    }
                    else
                    {
                        xmlOut = Signature.SignatureProcess.SignXMLEextendTimeStamp(xmlOut, "TBAO_TKHOAN_NH", "TBAO_TKHOAN_NH", true);

                    }
                    //vinhvv

                    strMSG_ID = msg06007.HEADER.MSG_ID;
                    strTran_Type = "MSG06007";
                    strMSG_TEXT = "PutMQ=StartTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);

                    clsMQHelper mq = new clsMQHelper();
                    string strReturn = "";
                    strReturn = mq.PutMQMessage(xmlOut);

                    strMSG_TEXT = "PutMQ=EndTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "MQ return:" + strReturn;
                    Utility.Log_Action(strTran_Type, strMSG_TEXT);
                    
                    //insert log
                    GDT.MSG.MSG msg_log = new GDT.MSG.MSG();
                    msg_log.error_code = msg06007.HEADER.ERROR_CODE;
                    msg_log.error_desc = msg06007.HEADER.ERROR_DESC;
                    if (!strReturn.Equals(""))
                    {
                        msg_log.error_code = "02";
                        if (strReturn.Length > 231)
                        {
                            strReturn = strReturn.Substring(0, 230);
                        }
                        msg_log.error_desc = strReturn;
                        errorCode = "9999";
                        errorMsg = strReturn;
                    }
                    msg_log.sender_code= msg06007.HEADER.SENDER_CODE;
                    msg_log.sender_name = msg06007.HEADER.SENDER_NAME;
                    msg_log.receiver_code = msg06007.HEADER.RECEIVER_CODE;
                    msg_log.receiver_name = msg06007.HEADER.RECEIVER_NAME;
                    msg_log.tran_code = msg06007.HEADER.TRAN_CODE;
                    msg_log.msg_id = msg06007.HEADER.MSG_ID;
                    msg_log.msg_refid = msg06007.HEADER.MSG_REFID;
                    msg_log.id_link = msg06007.HEADER.ID_LINK;
                    msg_log.send_date = msg06007.HEADER.SEND_DATE;
                    msg_log.original_code = msg06007.HEADER.ORIGINAL_CODE;
                    msg_log.original_name = msg06007.HEADER.ORIGINAL_NAME;
                    msg_log.original_date = msg06007.HEADER.ORIGINAL_DATE;
                   
                    msg_log.spare1 = msg06007.HEADER.SPARE1;
                    msg_log.spare2 = msg06007.HEADER.SPARE2;
                    msg_log.spare3 = msg06007.HEADER.SPARE3;
                    msg_log.trangthai = "00";
                    msg_log.content_mess = xmlOut;

                    string ma_tt = "";
                    Utility.insertMSGOutLog(msg_log, ref ma_tt);

                }
            }
            catch (Exception ex)
            {
                errorCode = "01";
                errorMsg = ex.Message;
                strMSG_TEXT = "PutMQ=Exception:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "MSG_ID=" + strMSG_ID + "Exception=" + ex.Message.ToString();
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            }
        }
        
    }
}
