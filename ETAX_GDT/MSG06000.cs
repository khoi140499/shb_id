﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace GDT.MSG.MSG06000
{
    [Serializable]
    [XmlRootAttribute("ROW", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG06000
    {
        public MSG06000()
        {
        }
        //[XmlElement("HEADER")]
        //public HEADER HEADER;
        [XmlElement("GIAODICH")]
        public GIAODICH GIAODICH;
        //[XmlElement("SECURITY")]
        //public SECURITY SECURITY;

        public string MSGtoXML(MSG06000 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG06000));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG06000 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSG06000.MSG06000));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            GDT.MSG.MSG06000.MSG06000 LoadedObjTmp = (GDT.MSG.MSG06000.MSG06000)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }
    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW ROW;
    }

    public class ROW
    {
        public ROW()
        { }
        [XmlElement("GIAODICH")]
        public GIAODICH GIAODICH;
    }

    public class GIAODICH
    {
        public GIAODICH()
        { }
        [XmlElement(Order = 1)]
        public string MA_GDICH { get; set; }
        [XmlElement(Order = 2)]
        public string NGAY_GUI_GDICH { get; set; }
        [XmlElement(Order = 3)]
        public string MA_GDICH_TCHIEU { get; set; }
        [XmlElement("CHUNGTU", Order = 4)]
        public CHUNGTU CHUNGTU;
        [XmlElement("THONGBAO", Order = 5)]
        public THONGBAO THONGBAO;
        [XmlElement(Order = 6)]
        public string Signature { get; set; }
    }

    public class CHUNGTU
    {
        public CHUNGTU()
        { }
        [XmlElement("NDUNG_CTU_NH")]
        public NDUNG_CTU_NH NDUNG_CTU_NH;
        public List<string> Signature { get; set; }
    }

    public class NDUNG_CTU_NH
    {
        public NDUNG_CTU_NH()
        { }
        public string MA_HIEU_CTU { get; set; }
        public string SO_CTU { get; set; }

        // tuanda - 01/02/24
        public string NGAY_CTU { get; set; }

        [XmlElement("NDUNG_CTU")]
        public NDUNG_CTU NDUNG_CTU;

    }

    public class NDUNG_CTU
    {
        public NDUNG_CTU()
        {

        }
        [XmlElement("CHUNGTU_HDR")]
        public CHUNGTU_HDR CHUNGTU_HDR;
        [XmlElement("CHUNGTU_CTIET")]
        public CHUNGTU_CTIET CHUNGTU_CTIET;
    }

    public class CHUNGTU_HDR
    {
        public CHUNGTU_HDR()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string ID_CTU { get; set; }
        public string SO_GNT { get; set; }
        public string MA_CTU { get; set; }
        public string HTHUC_NOP { get; set; }
        public string MST_NNOP { get; set; }
        public string TEN_NNOP { get; set; }
        public string DIACHI_NNOP { get; set; }
        public string MA_CQT { get; set; }
        public string TEN_CQT { get; set; }
        public string MA_XA_NNOP { get; set; }
        public string TEN_XA_NNOP { get; set; }
        public string MA_HUYEN_NNOP { get; set; }
        public string TEN_HUYEN_NNOP { get; set; }
        public string MA_TINH_NNOP { get; set; }
        public string TEN_TINH_NNOP { get; set; }
        public string MST_NTHAY { get; set; }
        public string TEN_NTHAY { get; set; }
        public string DIACHI_NTHAY { get; set; }
        public string TEN_HUYEN_NTHAY { get; set; }
        public string TEN_TINH_NTHAY { get; set; }
        public string MA_NHANG_NOP { get; set; }
        public string TEN_NHANG_NOP { get; set; }
        public string STK_NHANG_NOP { get; set; }
        public string MA_HIEU_KBAC { get; set; }
        public string TEN_KBAC { get; set; }
        public string MA_TINH_KBAC { get; set; }
        public string TEN_TINH_KBAC { get; set; }
        public string LOAI_TK_THU { get; set; }
        public string TEN_TK_THU { get; set; }
        public string STK_THU { get; set; }
        public string ID_TK_KNGHI { get; set; }
        public string TK_KNGHI { get; set; }
        public string MA_CQTHU { get; set; }
        public string TEN_CQTHU { get; set; }
        public string NGAY_LAP { get; set; }
        public string TONG_TIEN { get; set; }
        public string VAN_ID { get; set; }
        //public string MA_DBHC_THU { get; set; }
        //public string TEN_DBHC_THU { get; set; }
        public string MA_LOAI_THUE { get; set; }
        public string MA_NGUYENTE { get; set; }
        public string MA_NHANG_UNT { get; set; }
        public string TEN_NHANG_UNT { get; set; }
        public string MA_THAMCHIEU { get; set; }

        // tuanda - 01/02/24
        public string LHINH_NNT { get; set; }

    }

    public class CHUNGTU_CTIET
    {
        public CHUNGTU_CTIET()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET> ROW_CTIET;

    }

    public class ROW_CTIET
    {
        public ROW_CTIET()
        {

        }
        public string ID_CTU_CTIET { get; set; }
        public string ID_CTU { get; set; }

        // tuanda
        // ID khoản phải nộp
        public string ID_KNOP { get; set; }
        // Mã địa bàn nơi phát sinh khoản thu
        public string MA_DBHC_THU { get; set; }
        // Tên địa bàn nơi phát sinh khoản thu
        public string TEN_DBHC_THU { get; set; }

        public string SO_TK_TB_QD { get; set; }
        public string NDUNG_NOP { get; set; }
        public string MA_NDKT { get; set; }
        public string MA_CHUONG { get; set; }
        public string KY_THUE { get; set; }
        public string TIEN_PNOP { get; set; }
        public string GHI_CHU { get; set; }

        //// tuanda - thêm trường nhận từ GNT
        //public string MA_THAMCHIEU { get; set; }

    }

    public class THONGBAO
    {
        public THONGBAO()
        { }
        public string MSGtoXML(THONGBAO p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(THONGBAO));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        [XmlElement("NDUNG_TBAO")]
        public NDUNG_TBAO NDUNG_TBAO;
        public string Signature { get; set; }

    }

    public class NDUNG_TBAO
    {
        public NDUNG_TBAO()
        { }
        [XmlElement("THONGBAO_HDR")]
        public THONGBAO_HDR THONGBAO_HDR;
        [XmlElement("THONGBAO_CTIET")]
        public THONGBAO_CTIET THONGBAO_CTIET;

    }
    public class THONGBAO_HDR
    {
        public THONGBAO_HDR()
        { }

        public string PBAN_TLIEU_XML { get; set; }
        public string MA_THONGBAO { get; set; }
        public string MAU_THONGBAO { get; set; }
        public string TEN_THONGBAO { get; set; }
        public string SO_THONGBAO { get; set; }
        public string NGAY_THONGBAO { get; set; }
        public string MST_NNOP { get; set; }
        public string TEN_NNOP { get; set; }
        public string MA_CQT { get; set; }
        public string MST_NTHAY { get; set; }
        public string TEN_NTHAY { get; set; }
    }
    public class THONGBAO_CTIET
    {
        public THONGBAO_CTIET()
        { }
        [XmlElement("ROW_CTIET")]
        public List<ROW_CTIET_TB> ROW_CTIET;
    }

    public class ROW_CTIET_TB
    {
        public ROW_CTIET_TB()
        { }
        public string ID_CHUNGTU { get; set; }
        public string SO_GNT { get; set; }
        public string MAHIEU_CTU { get; set; }
        public string SO_CTU { get; set; }
        public string SHIEU_KHOBAC_NOP { get; set; }
        public string TEN_KHOBAC_NOP { get; set; }
        public string STK_KHOBAC_NOP { get; set; }
        public string MA_NGHANG_NOP { get; set; }
        public string STK_NGHANG_NOP { get; set; }
        public string TONGSOMON { get; set; }
        public string TONGTIENNOP { get; set; }
        public string PHI { get; set; }
        //public string NGAY_XLY { get; set; }
        public string NGAY_GUI { get; set; }
        public string NGAY_NTHUE { get; set; }
        public string MA_TTHAI { get; set; }
        public string TEN_TTHAI { get; set; }
        public string MA_NGUYENTE { get; set; }
        public string MA_THAMCHIEU { get; set; }

    }

    public class SECURITY
    {
        public SECURITY()
        {
        }
        public string SIGNATURE { get; set; }
    }
}
