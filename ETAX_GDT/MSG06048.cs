﻿using GDT.MSG.MSG03002;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ETAX_GDT
{

    [XmlRoot("DATA")]
    public class MSG06048
    {
        [XmlElement("HEADER")]
        public HEADER Header { get; set; }
        [XmlElement("BODY")]
        public BODY_06048 Body { get; set; }

        public string MSGtoXML(MSG06048 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG06048));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG06048 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG06048));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            MSG06048 LoadedObjTmp = (MSG06048)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    public class BODY_06048
    {
        [XmlElement("ROW")]
        public ROW_06048 ROW { get; set; }
    }
    public class ROW_06048
    {
        [XmlElement("GIAODICH")]
        public GIAODICH_06048 GIAODICH { get; set; }
    }
    public class GIAODICH_06048
    {
        [XmlElement("MA_GDICH")]
        public string MA_GDICH { get; set; }
        [XmlElement("NGAY_GUI_GDICH")]
        public string NGAY_GUI_GDICH { get; set; }
        [XmlElement("NOIDUNG")]
        public NoiDung06048 NOIDUNG { get; set; }
        public string MSGtoXML(GIAODICH_06048 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GIAODICH_06048));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public GIAODICH_06048 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GIAODICH_06048));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            GIAODICH_06048 LoadedObjTmp = (GIAODICH_06048)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    [XmlRoot("NOIDUNG")]
    public class NoiDung06048
    {
        [XmlElement("NDUNG_CTIET")]
        public NoiDungCTIET06048 NDUNG_CTIET { get;set;}
        public string MSGtoXML(NoiDung06048 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(NoiDung06048));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public NoiDung06048 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(NoiDung06048));

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            NoiDung06048 LoadedObjTmp = (NoiDung06048)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    public class NoiDungCTIET06048
    {
        [XmlElement("MA_THAMCHIEU")]
        public string MA_THAMCHIEU { get; set; }
        [XmlElement("MST_NNOP")]
        public string MST_NNOP { get; set; }
        [XmlElement("TEN_NNOP")]
        public string TEN_NNOP { get; set; }
        [XmlElement("SO_CTU")]
        public string SO_CTU { get; set; }
        [XmlElement("NGAY_GUI")]
        public string NGAY_GUI { get; set; }
        [XmlElement("NOI_NHAN")]
        public string NOI_NHAN { get; set; }
        [XmlElement("TRANG_THAI")]
        public string TRANG_THAI { get; set; }
        [XmlElement("HTHUC_NOP")]
        public string HTHUC_NOP { get; set; }
    }

}
