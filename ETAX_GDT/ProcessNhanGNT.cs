﻿using System;
using System.Text;
using GDT.MSG;
using GDT.MSG.MSG03002;
using System.Data.OracleClient;
using System.Data;
using System.Xml;
using GDT.MSG.MSG06004;
using VBOracleLib;
using System.Diagnostics;
using System.Configuration;
using GDT.MSG.MSGHeader;

namespace ETAX_GDT
{

    public class ProcessNhanGNT
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string KHCT = "SHB";
        //private static string TT_CT_NHAN = "00";
        //private static string TT_CT_THIEU_TIEN = "09";
        //private static string TT_CT_LOI = "03";
        //private static string TT_CT_THANHCONG = "11";
        //private static string TT_CT_SAITK = "02";
        private static string trangthaiHT = ConfigurationManager.AppSettings.Get("CORE.ON").ToString();
        private string TT_THANHCONG = "00000";
        private string TT_THANHCONG_TO999 = "TO999";
        private static String strXMLdefine = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>";
        private static String strXMLdefin2 = "<DATA xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.cpandl.com\">";
        private static string strTran_Type = "";
        private static string strMSG_TEXT = "";
        private static string Core_version = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString();

        public void Process(MSG msg, bool checkCK)
        {
            try
            {

                if (msg != null)
                {
                    string xmlIn = "";
                    xmlIn = msg.content_mess;
                    xmlIn = Utility.RomoveXML_Des(xmlIn);
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.PreserveWhitespace = true;
                    xmlDoc.LoadXml(xmlIn);
                    XmlNode xmlnode = xmlDoc.SelectSingleNode("/DATA/BODY/ROW/GIAODICH/CHUNGTU");
                    xmlIn = xmlIn.Replace("<DATA>", strXMLdefin2);
                    string noidungCT = "<CHUNGTU>" + xmlnode.InnerXml + "</CHUNGTU>"; ;

                    string mahieu_ct = "";
                    string so_ct = "";
                    string trang_thaiGD = "11";
                    string ten_tthai = "";
                    if (xmlIn.Length > 0)
                    {
                        MSG03002 MSG03002 = new MSG03002();
                        MSG03002 objTemp = null;

                        try
                        {
                            objTemp = MSG03002.MSGToObject(xmlIn);
                            GDT.MSG.MSG03002.HEADER objHEADER = objTemp.HEADER;
                            GDT.MSG.MSG03002.CHUNGTU objChungTu = objTemp.BODY.ROW.GIAODICH.CHUNGTU;
                            GDT.MSG.MSG03002.CHUNGTU_HDR objCTu_hdr = objTemp.BODY.ROW.GIAODICH.CHUNGTU.NDUNG_CTU_NH.NDUNG_CTU.CHUNGTU_HDR;
                            GDT.MSG.MSG03002.CHUNGTU_CTIET objCTu_dtl = objTemp.BODY.ROW.GIAODICH.CHUNGTU.NDUNG_CTU_NH.NDUNG_CTU.CHUNGTU_CTIET;

                            if (objTemp.BODY != null)
                            {

                                CHUNGTU_HDR CHUNGTU_HDR = objTemp.BODY.ROW.GIAODICH.CHUNGTU.NDUNG_CTU_NH.NDUNG_CTU.CHUNGTU_HDR;

                                MSGHeader msgheader = new MSGHeader();
                                msgheader.HEADER = new GDT.MSG.MSGHeader.HEADER();

                                msgheader.HEADER.ERROR_CODE = objTemp.HEADER.ERROR_CODE;
                                msgheader.HEADER.ERROR_DESC = objTemp.HEADER.ERROR_DESC;
                                msgheader.HEADER.ID_LINK = objTemp.HEADER.ID_LINK;
                                msgheader.HEADER.MSG_ID = objTemp.HEADER.MSG_ID;
                                msgheader.HEADER.MSG_REFID = objTemp.HEADER.MSG_REFID;
                                msgheader.HEADER.ORIGINAL_CODE = objTemp.HEADER.ORIGINAL_CODE;
                                msgheader.HEADER.ORIGINAL_DATE = objTemp.HEADER.ORIGINAL_DATE;
                                msgheader.HEADER.ORIGINAL_NAME = objTemp.HEADER.ORIGINAL_NAME;
                                msgheader.HEADER.RECEIVER_CODE = objTemp.HEADER.RECEIVER_CODE;
                                msgheader.HEADER.RECEIVER_NAME = objTemp.HEADER.RECEIVER_NAME;
                                msgheader.HEADER.SEND_DATE = objTemp.HEADER.SEND_DATE;
                                msgheader.HEADER.SENDER_CODE = objTemp.HEADER.SENDER_CODE;
                                msgheader.HEADER.SENDER_NAME = objTemp.HEADER.SENDER_NAME;
                                msgheader.HEADER.SPARE1 = objTemp.HEADER.SPARE1;
                                msgheader.HEADER.SPARE2 = objTemp.HEADER.SPARE2;
                                msgheader.HEADER.SPARE3 = objTemp.HEADER.SPARE3;
                                msgheader.HEADER.TRAN_CODE = objTemp.HEADER.TRAN_CODE;
                                msgheader.HEADER.VERSION = objTemp.HEADER.VERSION;

                                if (checkCK == false)
                                {
                                    Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "02", "05 - Sai chữ ký số");
                                    trang_thaiGD = GDICH_NTDT_Constants.TT_CT_SAI_CKS;
                                    ten_tthai = GDICH_NTDT_Constants.TEN_TT_SAI_CKS;
                                }
                                else

                                    //if (CHUNGTU_HDR.DIACHI_NNOP == null || CHUNGTU_HDR.DIACHI_NTHAY == null || CHUNGTU_HDR.HTHUC_NOP == null || CHUNGTU_HDR.ID_CTU == null || CHUNGTU_HDR.ID_TK_KNGHI == null || CHUNGTU_HDR.LOAI_TK_THU == null || CHUNGTU_HDR.MA_CQT == null || CHUNGTU_HDR.MA_CQTHU == null || CHUNGTU_HDR.MA_CTU == null || CHUNGTU_HDR.MA_CTU == null || CHUNGTU_HDR.MA_DBHC_THU == null || CHUNGTU_HDR.MA_HIEU_KBAC == null || CHUNGTU_HDR.MA_HUYEN_NNOP == null || CHUNGTU_HDR.MA_LOAI_THUE == null || CHUNGTU_HDR.MA_NHANG_NOP == null || CHUNGTU_HDR.MA_TINH_KBAC == null || CHUNGTU_HDR.MA_TINH_NNOP == null || CHUNGTU_HDR.MA_XA_NNOP == null || CHUNGTU_HDR.MST_NNOP == null || CHUNGTU_HDR.MST_NTHAY == null || CHUNGTU_HDR.NGAY_LAP == null || CHUNGTU_HDR.PBAN_TLIEU_XML == null || CHUNGTU_HDR.SO_GNT == null || CHUNGTU_HDR.STK_NHANG_NOP == null || CHUNGTU_HDR.STK_THU == null || CHUNGTU_HDR.TEN_CQT == null || CHUNGTU_HDR.TEN_CQTHU == null || CHUNGTU_HDR.TEN_DBHC_THU == null || CHUNGTU_HDR.TEN_HUYEN_NNOP == null || CHUNGTU_HDR.TEN_HUYEN_NTHAY == null || CHUNGTU_HDR.TEN_KBAC == null || CHUNGTU_HDR.TEN_NHANG_NOP == null || CHUNGTU_HDR.TEN_NNOP == null || CHUNGTU_HDR.TEN_NTHAY == null || CHUNGTU_HDR.TEN_TINH_KBAC == null || CHUNGTU_HDR.TEN_TINH_NNOP == null || CHUNGTU_HDR.TEN_TINH_NTHAY == null || CHUNGTU_HDR.TEN_TK_THU == null || CHUNGTU_HDR.TEN_XA_NNOP == null || CHUNGTU_HDR.TK_KNGHI == null || CHUNGTU_HDR.TONG_TIEN == null || CHUNGTU_HDR.VAN_ID == null)
                                    if (CHUNGTU_HDR.DIACHI_NNOP == null || CHUNGTU_HDR.DIACHI_NTHAY == null || CHUNGTU_HDR.HTHUC_NOP == null || CHUNGTU_HDR.ID_CTU == null || CHUNGTU_HDR.ID_TK_KNGHI == null || CHUNGTU_HDR.LOAI_TK_THU == null || CHUNGTU_HDR.MA_CQT == null || CHUNGTU_HDR.MA_CQTHU == null || CHUNGTU_HDR.MA_CTU == null || CHUNGTU_HDR.MA_CTU == null || CHUNGTU_HDR.MA_HIEU_KBAC == null || CHUNGTU_HDR.MA_HUYEN_NNOP == null || CHUNGTU_HDR.MA_LOAI_THUE == null || CHUNGTU_HDR.MA_NHANG_NOP == null || CHUNGTU_HDR.MA_TINH_KBAC == null || CHUNGTU_HDR.MA_TINH_NNOP == null || CHUNGTU_HDR.MA_XA_NNOP == null || CHUNGTU_HDR.MST_NNOP == null || CHUNGTU_HDR.MST_NTHAY == null || CHUNGTU_HDR.NGAY_LAP == null || CHUNGTU_HDR.PBAN_TLIEU_XML == null || CHUNGTU_HDR.SO_GNT == null || CHUNGTU_HDR.STK_NHANG_NOP == null || CHUNGTU_HDR.STK_THU == null || CHUNGTU_HDR.TEN_CQT == null || CHUNGTU_HDR.TEN_CQTHU == null || CHUNGTU_HDR.TEN_HUYEN_NNOP == null || CHUNGTU_HDR.TEN_HUYEN_NTHAY == null || CHUNGTU_HDR.TEN_KBAC == null || CHUNGTU_HDR.TEN_NHANG_NOP == null || CHUNGTU_HDR.TEN_NNOP == null || CHUNGTU_HDR.TEN_NTHAY == null || CHUNGTU_HDR.TEN_TINH_KBAC == null || CHUNGTU_HDR.TEN_TINH_NNOP == null || CHUNGTU_HDR.TEN_TINH_NTHAY == null || CHUNGTU_HDR.TEN_TK_THU == null || CHUNGTU_HDR.TEN_XA_NNOP == null || CHUNGTU_HDR.TK_KNGHI == null || CHUNGTU_HDR.TONG_TIEN == null || CHUNGTU_HDR.VAN_ID == null)
                                    {

                                        //phản hồi nhận dữ liệu bị lỗi cấu trúc cho tct
                                        Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "02", "07 - Cấu trúc GNT không đúng khuôn dạng");
                                        trang_thaiGD = GDICH_NTDT_Constants.TT_SAI_XML;
                                        ten_tthai = GDICH_NTDT_Constants.TEN_TT_SAI_XML;
                                    }
                                    else
                                    {

                                        //sonmt: thời gian tct gửi và thời gian nh nhận
                                        string send_date = "";
                                        string received_date = "";
                                        send_date = msg.send_date;
                                        received_date = msg.ngay_gdich;

                                        ThemGNT_NTDT_TT84(objTemp, noidungCT, send_date, received_date, ref so_ct, ref mahieu_ct, ref trang_thaiGD, ref ten_tthai);
                                        //xủ lý hạch toán

                                        if (trang_thaiGD == GDICH_NTDT_Constants.TT_CT_NHAN)
                                        {
                                            //1 trangthaiHT phải là 1, 0 để test.
                                            if (trangthaiHT == "1")
                                            {
                                                payment_tt84(objCTu_hdr, objCTu_dtl, so_ct, ref trang_thaiGD, ref ten_tthai);
                                                strTran_Type = "MSG03002";
                                                strMSG_TEXT = "SO_CT=" + so_ct + ".Trang_thai=" + trang_thaiGD;
                                                Utility.Log_Action(strTran_Type, strMSG_TEXT);
                                            }
                                            else
                                            {
                                                trang_thaiGD = "11";
                                                strTran_Type = "MSG03002";
                                                strMSG_TEXT = "SO_CT=" + so_ct + ".Trang_thai=" + trang_thaiGD;
                                                string strTT_GD = GDICH_NTDT_Constants.TT_CT_THANHCONG;
                                                string ten_trangthai = Utility.getTen_TT(strTT_GD);
                                                UpDateTTChungTu(strTT_GD, ten_trangthai, so_ct, objCTu_hdr.ID_CTU);
                                                
                                                // giả lập số ref_no - so_ct
                                                UpDateTTChungTu(strTT_GD, ten_trangthai, so_ct, so_ct, objCTu_hdr.ID_CTU);
                                                Utility.Log_Action(strTran_Type, strMSG_TEXT);
                                            }

                                        }

                                        //phản hồi nhận dữ liệu sau khi đã xử lí cho TCT
                                        if (trang_thaiGD == GDICH_NTDT_Constants.TT_CT_THANHCONG)
                                        {
                                            Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "01", "Nhận message giấy nộp tiền thành công");
                                        }
                                        else
                                        {
                                            Utility.replyReceivedMSG(msgheader, objTemp.BODY.ROW.GIAODICH.MA_GDICH, "02", trang_thaiGD + " - " + ten_tthai);
                                        }

                                    }

                                //truyền lại thông báo cho tổng cục thuế
                                ProcessXuLyTBOnline xulyTB = new ProcessXuLyTBOnline();
                                //xulyTB.GuiThongBao(noidungCT, objCTu_hdr, objCTu_dtl, objHEADER, so_ct, mahieu_ct, trang_thaiGD, ten_tthai);
                                xulyTB.GuiThongBao(noidungCT, objCTu_hdr, objCTu_dtl, objHEADER, so_ct, mahieu_ct, trang_thaiGD, ten_tthai, "");

                            }
                        }
                        catch (Exception e)
                        {

                        }

                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                throw ex;
                // Nếu sai gọi hàm thong báo với nội dung sai
            }
        }
        #region "Nhận giấy nộp tiền"

        private void ThemGNT_NTDT_TT84(MSG03002 msgGNT, string strfilecontent, string send_date, string received_date, ref string so_ctu, ref string mahieu_ctu, ref string tthai_ctu, ref string ten_tthai_ctu)
        {
            OracleConnection conn = new OracleConnection();
            IDbTransaction transCT = null;
            //Tao cau lenh Insert
            // tuanda
            // thêm trường MA_THAMCHIEU_KNOP
            StringBuilder sbSqlInsert = new StringBuilder("");
            sbSqlInsert.Append("INSERT INTO TCS_CTU_NTDT_HDR (id_ctu, ");
            //sbSqlInsert.Append("MA_THAMCHIEU_KNOP,");
            sbSqlInsert.Append("so_ctu,");
            sbSqlInsert.Append("ma_ctu,");
            sbSqlInsert.Append("mahieu_ctu,");
            sbSqlInsert.Append("hthuc_nop,");
            sbSqlInsert.Append("mst_nnop,");
            sbSqlInsert.Append("ten_nnop,");
            sbSqlInsert.Append("diachi_nnop,");
            sbSqlInsert.Append("ma_nhang_nop,");
            sbSqlInsert.Append("ten_nhang_nop,");
            sbSqlInsert.Append("stk_nhang_nop,");
            sbSqlInsert.Append("loai_tk_thu,");
            sbSqlInsert.Append("ten_tk_thu,");
            sbSqlInsert.Append("stk_thu,");
            sbSqlInsert.Append("ma_cqthu,");
            sbSqlInsert.Append("ten_cqthu,");
            sbSqlInsert.Append("ngay_lap,");
            sbSqlInsert.Append("ngay_gui,");
            sbSqlInsert.Append("ngay_nhang,");
            sbSqlInsert.Append("tong_tien,");
            sbSqlInsert.Append("filecontent,");
            sbSqlInsert.Append("van_id,");
            sbSqlInsert.Append("tthai_ctu,");
            sbSqlInsert.Append("ma_gd,");
            sbSqlInsert.Append("ma_cqt,");
            sbSqlInsert.Append("ten_cqt,");
            sbSqlInsert.Append("ten_tthai_ctu,");
            sbSqlInsert.Append("mst_nthay,");
            sbSqlInsert.Append("ten_nthay,");
            sbSqlInsert.Append("diachi_nthay,");
            sbSqlInsert.Append("id_tk_knghi,");
            sbSqlInsert.Append("ten_tk_knghi,");
            sbSqlInsert.Append("phienban_xml,");
            sbSqlInsert.Append("ngay_gui_gd,");
            sbSqlInsert.Append("ma_xa_nnop,");
            sbSqlInsert.Append("ma_huyen_nnop,");
            sbSqlInsert.Append("ma_tinh_nnop,");
            sbSqlInsert.Append("ten_huyen_nnop,");
            sbSqlInsert.Append("ten_tinh_nnop,");
            sbSqlInsert.Append("ten_huyen_nthay,");
            sbSqlInsert.Append("ten_tinh_nthay,");
            sbSqlInsert.Append("ma_hieu_kbac,");
            sbSqlInsert.Append("ten_kbac,");
            sbSqlInsert.Append("ma_tinh_kbac,");
            sbSqlInsert.Append("ten_tinh_kbac,");

            //tuanda - 01/02/24
            sbSqlInsert.Append("ngay_ctu,");
            sbSqlInsert.Append("lhinh_nnt,");

            sbSqlInsert.Append("so_gnt,");
            sbSqlInsert.Append("ma_lthue, TGIAN_TCT_GUI, TGIAN_NH_NHAN,MA_NGUYENTE,MA_THAMCHIEU,ma_nhang_unt,ten_nhang_unt, MA_THAMCHIEU_KNOP) ");

            sbSqlInsert.Append(" VALUES(:id_ctu,");
            //sbSqlInsert.Append(":MA_THAMCHIEU_KNOP,");
            sbSqlInsert.Append(":so_ctu,");
            sbSqlInsert.Append(":ma_ctu,");
            sbSqlInsert.Append(":mahieu_ctu,");
            sbSqlInsert.Append(":hthuc_nop,");
            sbSqlInsert.Append(":mst_nnop,");
            sbSqlInsert.Append(":ten_nnop,");
            sbSqlInsert.Append(":diachi_nnop,");
            sbSqlInsert.Append(":ma_nhang_nop,");
            sbSqlInsert.Append(":ten_nhang_nop,");
            sbSqlInsert.Append(":stk_nhang_nop,");
            sbSqlInsert.Append(":loai_tk_thu,");
            sbSqlInsert.Append(":ten_tk_thu,");
            sbSqlInsert.Append(":stk_thu,");
            sbSqlInsert.Append(":ma_cqthu,");
            sbSqlInsert.Append(":ten_cqthu,");
            sbSqlInsert.Append(":ngay_lap,");
            sbSqlInsert.Append(":ngay_gui,");
            sbSqlInsert.Append(":ngay_nhang,");
            sbSqlInsert.Append(":tong_tien,");
            sbSqlInsert.Append(":filecontent,");
            sbSqlInsert.Append(":van_id,");
            sbSqlInsert.Append(":tthai_ctu,");
            sbSqlInsert.Append(":ma_gd,");
            sbSqlInsert.Append(":ma_cqt,");
            sbSqlInsert.Append(":ten_cqt,");
            sbSqlInsert.Append(":ten_tthai_ctu,");
            sbSqlInsert.Append(":mst_nthay,");
            sbSqlInsert.Append(":ten_nthay,");
            sbSqlInsert.Append(":diachi_nthay,");
            sbSqlInsert.Append(":id_tk_knghi,");
            sbSqlInsert.Append(":ten_tk_knghi,");
            sbSqlInsert.Append(":phienban_xml,");
            sbSqlInsert.Append(":ngay_gui_gd,");
            sbSqlInsert.Append(":ma_xa_nnop,");
            sbSqlInsert.Append(":ma_huyen_nnop,");
            sbSqlInsert.Append(":ma_tinh_nnop,");
            sbSqlInsert.Append(":ten_huyen_nnop,");
            sbSqlInsert.Append(":ten_tinh_nnop,");
            sbSqlInsert.Append(":ten_huyen_nthay,");
            sbSqlInsert.Append(":ten_tinh_nthay,");
            sbSqlInsert.Append(":ma_hieu_kbac,");
            sbSqlInsert.Append(":ten_kbac,");
            sbSqlInsert.Append(":ma_tinh_kbac,");
            sbSqlInsert.Append(":ten_tinh_kbac,");

            //tuanda - 01/02/24
            sbSqlInsert.Append(":ngay_ctu,");
            sbSqlInsert.Append(":lhinh_nnt,");

            sbSqlInsert.Append(":so_gnt,");
            sbSqlInsert.Append(":ma_lthue, :TGIAN_TCT_GUI, :TGIAN_NH_NHAN,:MA_NGUYENTE,:MA_THAMCHIEU,:ma_nhang_unt,:ten_nhang_unt, :MA_THAMCHIEU_KNOP) ");
            try
            {
                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                GDT.MSG.MSG03002.GIAODICH objGDChungTu = msgGNT.BODY.ROW.GIAODICH;
                GDT.MSG.MSG03002.NDUNG_CTU_NH objNoiDung_CT = objGDChungTu.CHUNGTU.NDUNG_CTU_NH;
                GDT.MSG.MSG03002.CHUNGTU_HDR objCTu_hdr = objNoiDung_CT.NDUNG_CTU.CHUNGTU_HDR;
                GDT.MSG.MSG03002.CHUNGTU_CTIET objCTu_dtl = objNoiDung_CT.NDUNG_CTU.CHUNGTU_CTIET;
                if (objGDChungTu != null)
                {
                    IDbDataParameter[] p;

                    //Gan cac thong tin
                    string id_ctu = DBNull.Value.ToString();
                    // string so_ctu = DBNull.Value.ToString();

                    // tuanda
                    // thêm trường MA_THAMCHIEU_KNOP
                    string MA_THAMCHIEU_KNOP = DBNull.Value.ToString();
                    //string mahieu_ctu = DBNull.Value.ToString();
                    string ma_ctu = DBNull.Value.ToString();//mã loại chứng từ
                    string hthuc_nop = DBNull.Value.ToString();
                    string mst_nnop = DBNull.Value.ToString();
                    string ten_nnop = DBNull.Value.ToString();
                    string diachi_nnop = DBNull.Value.ToString();
                    string ma_nhang_nop = DBNull.Value.ToString();
                    string ten_nhang_nop = DBNull.Value.ToString();
                    string stk_nhang_nop = DBNull.Value.ToString();
                    string loai_tk_thu = DBNull.Value.ToString();
                    string ten_tk_thu = DBNull.Value.ToString();
                    string stk_thu = DBNull.Value.ToString();
                    string ma_cqthu = DBNull.Value.ToString();
                    string ten_cqthu = DBNull.Value.ToString();
                    string ngay_lap = DBNull.Value.ToString();
                    string ngay_gui = DBNull.Value.ToString();
                    string ngay_nhang = DBNull.Value.ToString();
                    string tong_tien = DBNull.Value.ToString();
                    string filecontent = DBNull.Value.ToString();
                    string van_id = DBNull.Value.ToString();
                    string ma_gd = DBNull.Value.ToString();
                    string ma_cqt = DBNull.Value.ToString();
                    string ten_cqt = DBNull.Value.ToString();
                    //string tthai_ctu = DBNull.Value.ToString();
                    //string ten_tthai_ctu = DBNull.Value.ToString();
                    string mst_nthay = DBNull.Value.ToString();
                    string ten_nthay = DBNull.Value.ToString();
                    string diachi_nthay = DBNull.Value.ToString();
                    string id_tk_knghi = DBNull.Value.ToString();
                    string ten_tk_knghi = DBNull.Value.ToString();
                    string phienban_xml = DBNull.Value.ToString();
                    string ngay_gui_gd = DBNull.Value.ToString();

                    string ma_xa_nnop = DBNull.Value.ToString();
                    string ten_xa_nnop = DBNull.Value.ToString();
                    string ma_huyen_nnop = DBNull.Value.ToString();
                    string ten_huyen_nnop = DBNull.Value.ToString();
                    string ma_tinh_nnop = DBNull.Value.ToString();
                    string ten_tinh_nnop = DBNull.Value.ToString();

                    string ten_huyen_nthay = DBNull.Value.ToString();
                    string ten_tinh_nthay = DBNull.Value.ToString();

                    string ma_hieu_kb = DBNull.Value.ToString();
                    string ten_khobac = DBNull.Value.ToString();
                    string ma_tinh_khobac = DBNull.Value.ToString();
                    string ten_tinh_khobac = DBNull.Value.ToString();
                    //string ma_dbhc_thu = DBNull.Value.ToString();
                    //string ten_dbhc_thu = DBNull.Value.ToString();
                    string ma_loai_thue = DBNull.Value.ToString();
                    string so_gnt = DBNull.Value.ToString();
                    string str_nam_kb = "";
                    string str_thang_kb = "";
                    string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    string MA_NGUYENTE = "VND";
                    string MA_THAMCHIEU = DBNull.Value.ToString();
                    string ma_nhang_unt = DBNull.Value.ToString();
                    string ten_nhang_unt = DBNull.Value.ToString();

                    // tuanda - 01/02/24
                    string ngay_ctu = DBNull.Value.ToString();
                    string lhinh_nnt = DBNull.Value.ToString();

                    str_nam_kb = datetime.Substring(8, 2);
                    str_thang_kb = datetime.Substring(3, 2);
                    so_ctu = str_nam_kb + str_thang_kb + Utility.TCS_GetSequence("NTDT_SQ_SO_CT").ToString().PadLeft(5, '0').ToString(); ;// xử lý đoạn lấy mã hiệu
                    mahieu_ctu = KHCT + so_ctu;// xử lý đoạn lấy mã hiệu
                    tthai_ctu = GDICH_NTDT_Constants.TT_CT_NHAN;// trạng thái vừa insert vào db
                    ten_tthai_ctu = GDICH_NTDT_Constants.TEN_TT_CT_NHAN;

                    // tuanda - 21/02/24

                    //// sinh mã tham chiếu ngay khi nhận GNT
                    //// “Số tham chiếu” 20 ký tự, gồm:
                    ////  - 4 ký tự đầu là mã của đơn vị 
                    ////  - 4 ký tự tiếp theo là năm lập chứng từ nộp NSNN
                    ////  - 10 ký tự tiếp theo là số tự sinh tăng dần của hệ thống các đơn vị
                    ////  - 2 ký tự cuối là ký hiệu hình thức lập chứng từ nộp NSNN

                    //// mã đơn vị -> Ngân hàng Thương mại Cổ phần Kiên Long - 3530
                    string MaDV = "";
                    DataTable MaDonVi = DataAccess.ExecuteToTable("select * from tcs_thamso_ht where ten_ts='MA_DONVI'");
                    if (MaDonVi.Rows.Count > 0)
                    {
                        MaDV = MaDonVi.Rows[0]["giatri_ts"].ToString();
                    }

                    string seq = Utility.TCS_GetSequence("TCS_MA_TC_SEQ").ToString().PadLeft(10, '0').ToString();

                    string year = datetime.Substring(6, 4);
                    string Hthuc = objCTu_hdr.HTHUC_NOP;

                    string ma_thamchieu_knop = MaDV + year + seq + Hthuc;
                    //

                    if (objGDChungTu != null)
                    {
                        // tuanda - 01/02/24
                        if (objNoiDung_CT != null)
                        {
                            ngay_ctu = objNoiDung_CT.NGAY_CTU;
                        }

                        if (objCTu_hdr.ID_CTU != null)
                        {
                            so_gnt = objCTu_hdr.SO_GNT;
                        }

                        if (CheckTrungGNT(so_gnt) == false)
                        {
                            tthai_ctu = GDICH_NTDT_Constants.TT_CT_LOI;
                            ten_tthai_ctu = GDICH_NTDT_Constants.TEN_TT_TRUNG_GNT;
                            return;
                        } 
                        ma_gd = objGDChungTu.MA_GDICH;
                        ngay_gui_gd = objGDChungTu.NGAY_GUI_GDICH;
                        if (objCTu_hdr != null)
                        {
                            // tuanda - 01/02/24
                            if (objCTu_hdr.LHINH_NNT != null)
                            {
                                lhinh_nnt = objCTu_hdr.LHINH_NNT;
                            }

                            if (objCTu_hdr.ID_CTU != null)
                            {
                                id_ctu = objCTu_hdr.ID_CTU;
                            }

                            if (objCTu_hdr.MA_CTU != null)
                            {
                                ma_ctu = objCTu_hdr.MA_CTU;
                            }
                            if (objCTu_hdr.HTHUC_NOP != null)
                            {
                                hthuc_nop = objCTu_hdr.HTHUC_NOP;
                            }
                            if (objCTu_hdr.MST_NNOP != null)
                            {
                                mst_nnop = objCTu_hdr.MST_NNOP.Trim();
                            }
                            if (objCTu_hdr.TEN_NNOP != null)
                            {
                                ten_nnop = objCTu_hdr.TEN_NNOP;
                            }
                            if (objCTu_hdr.DIACHI_NNOP != null)
                            {
                                diachi_nnop = objCTu_hdr.DIACHI_NNOP;
                            }
                            if (objCTu_hdr.MA_NHANG_NOP != null)
                            {
                                ma_nhang_nop = objCTu_hdr.MA_NHANG_NOP;
                            }
                            if (objCTu_hdr.TEN_NHANG_NOP != null)
                            {
                                ten_nhang_nop = objCTu_hdr.TEN_NHANG_NOP;
                            }
                            if (objCTu_hdr.STK_NHANG_NOP != null)
                            {
                                stk_nhang_nop = objCTu_hdr.STK_NHANG_NOP;
                            }
                            if (objCTu_hdr.LOAI_TK_THU != null)
                            {
                                loai_tk_thu = objCTu_hdr.LOAI_TK_THU;
                            }
                            if (objCTu_hdr.TEN_TK_THU != null)
                            {
                                ten_tk_thu = objCTu_hdr.TEN_TK_THU;
                            }
                            if (objCTu_hdr.STK_THU != null)
                            {
                                stk_thu = objCTu_hdr.STK_THU;
                            }
                            if (objCTu_hdr.MA_CQTHU != null)
                            {
                                ma_cqthu = objCTu_hdr.MA_CQTHU;
                            }
                            if (objCTu_hdr.TEN_CQTHU != null)
                            {
                                ten_cqthu = objCTu_hdr.TEN_CQTHU;
                            }
                            if (objCTu_hdr.NGAY_LAP != null)
                            {
                                ngay_lap = objCTu_hdr.NGAY_LAP;
                            }

                            //ngay_gui = "";
                            //ngay_nhang = "";
                            if (objCTu_hdr.TONG_TIEN != null)
                            {
                                tong_tien = objCTu_hdr.TONG_TIEN;
                            }

                            filecontent = strfilecontent;
                            // filecontent = "";//xử lý để lấy dữ liệu từ thẻ CHUNGTU
                            if (objCTu_hdr.VAN_ID != null)
                            {
                                van_id = objCTu_hdr.VAN_ID;
                            }

                            if (objCTu_hdr.VAN_ID != null)
                            {
                                van_id = objCTu_hdr.VAN_ID;
                            }
                            if (objCTu_hdr.MA_CQT != null)
                            {
                                ma_cqt = objCTu_hdr.MA_CQT;
                            }
                            if (objCTu_hdr.TEN_CQT != null)
                            {
                                ten_cqt = objCTu_hdr.TEN_CQT;
                            }
                            if (objCTu_hdr.MA_XA_NNOP != null)
                            {
                                ma_xa_nnop = objCTu_hdr.MA_XA_NNOP;
                            }
                            if (objCTu_hdr.TEN_XA_NNOP != null)
                            {
                                ten_xa_nnop = objCTu_hdr.TEN_XA_NNOP;
                            }
                            if (objCTu_hdr.MA_HUYEN_NNOP != null)
                            {
                                ma_huyen_nnop = objCTu_hdr.MA_HUYEN_NNOP;
                            }
                            if (objCTu_hdr.TEN_HUYEN_NNOP != null)
                            {
                                ten_huyen_nnop = objCTu_hdr.TEN_HUYEN_NNOP;
                            }
                            if (objCTu_hdr.MA_TINH_NNOP != null)
                            {
                                ma_tinh_nnop = objCTu_hdr.MA_TINH_NNOP;
                            }
                            if (objCTu_hdr.TEN_TINH_NNOP != null)
                            {
                                ten_tinh_nnop = objCTu_hdr.TEN_TINH_NNOP;
                            }
                            // người nộp thay
                            if (objCTu_hdr.MST_NTHAY != null)
                            {
                                mst_nthay = objCTu_hdr.MST_NTHAY;
                            }
                            if (objCTu_hdr.TEN_NTHAY != null)
                            {
                                ten_nthay = objCTu_hdr.TEN_NTHAY;
                            }
                            if (objCTu_hdr.DIACHI_NTHAY != null)
                            {
                                diachi_nthay = objCTu_hdr.DIACHI_NTHAY;
                            }
                            if (objCTu_hdr.TEN_HUYEN_NTHAY != null)
                            {
                                ten_huyen_nthay = objCTu_hdr.DIACHI_NTHAY;
                            }
                            if (objCTu_hdr.TEN_TINH_NTHAY != null)
                            {
                                ten_tinh_nthay = objCTu_hdr.TEN_TINH_NTHAY;
                            }
                            // số hiệu kho bạc
                            if (objCTu_hdr.MA_HIEU_KBAC != null)
                            {
                                ma_hieu_kb = objCTu_hdr.MA_HIEU_KBAC;
                            }
                            if (objCTu_hdr.TEN_KBAC != null)
                            {
                                ten_khobac = objCTu_hdr.TEN_KBAC;
                            }
                            if (objCTu_hdr.MA_TINH_KBAC != null)
                            {
                                ma_tinh_khobac = objCTu_hdr.MA_TINH_KBAC;
                            }
                            if (objCTu_hdr.TEN_TINH_KBAC != null)
                            {
                                ten_tinh_khobac = objCTu_hdr.TEN_TINH_KBAC;
                            }
                            //if (objCTu_hdr.MA_DBHC_THU != null)
                            //{
                            //    ma_dbhc_thu = objCTu_hdr.MA_DBHC_THU;
                            //}
                            //if (objCTu_hdr.TEN_DBHC_THU != null)
                            //{
                            //    ten_dbhc_thu = objCTu_hdr.TEN_DBHC_THU;
                            //}
                            if (objCTu_hdr.ID_TK_KNGHI != null)
                            {
                                id_tk_knghi = objCTu_hdr.ID_TK_KNGHI;
                            }
                            if (objCTu_hdr.TK_KNGHI != null)
                            {
                                ten_tk_knghi = objCTu_hdr.TK_KNGHI;
                            }
                            if (objCTu_hdr.PBAN_TLIEU_XML != null)
                            {
                                phienban_xml = objCTu_hdr.PBAN_TLIEU_XML;
                            }

                            //sonmt: them ma_loai_thue
                            if (objCTu_hdr.MA_LOAI_THUE != null)
                            {
                                ma_loai_thue = objCTu_hdr.MA_LOAI_THUE;
                            }
                            //tt84
                            if (objCTu_hdr.MA_NGUYENTE != null)
                            {
                                MA_NGUYENTE = objCTu_hdr.MA_NGUYENTE;
                            }
                            if (objCTu_hdr.MA_THAMCHIEU != null)
                            {
                                MA_THAMCHIEU = objCTu_hdr.MA_THAMCHIEU;
                            }

                            //if (objCTu_hdr.MA_THAMCHIEU_KNOP != null)
                            //{
                            //    MA_THAMCHIEU_KNOP = objCTu_hdr.MA_THAMCHIEU_KNOP;
                            //}
                            //them moi
                            if (objCTu_hdr.MA_NHANG_UNT != null)
                            {
                                ma_nhang_unt = objCTu_hdr.MA_NHANG_UNT;
                            }
                            if (objCTu_hdr.TEN_NHANG_UNT != null)
                            {
                                ten_nhang_unt = objCTu_hdr.TEN_NHANG_UNT;
                            }

                        }
                        else
                        {
                            //Lỗi cấu trúc xml
                            tthai_ctu = GDICH_NTDT_Constants.TT_SAI_XML;// trạng thái vừa insert vào db
                            ten_tthai_ctu = GDICH_NTDT_Constants.TEN_TT_SAI_XML;
                        }
                    }
                    else
                    {
                        //Lỗi cấu trúc xml
                        tthai_ctu = GDICH_NTDT_Constants.TT_SAI_XML;// trạng thái vừa insert vào db
                        ten_tthai_ctu = GDICH_NTDT_Constants.TEN_TT_SAI_XML;
                    }


                    p = new IDbDataParameter[56];

                    p[0] = new OracleParameter();
                    p[0].ParameterName = "id_ctu";
                    p[0].DbType = DbType.String;
                    p[0].Direction = ParameterDirection.Input;
                    p[0].Value = id_ctu;

                    p[1] = new OracleParameter();
                    p[1].ParameterName = "so_ctu";
                    p[1].DbType = DbType.String;
                    p[1].Direction = ParameterDirection.Input;
                    p[1].Value = so_ctu;


                    p[2] = new OracleParameter();
                    p[2].ParameterName = "ma_ctu";
                    p[2].DbType = DbType.String;
                    p[2].Direction = ParameterDirection.Input;
                    p[2].Value = ma_ctu;

                    p[3] = new OracleParameter();
                    p[3].ParameterName = "mahieu_ctu";
                    p[3].DbType = DbType.String;
                    p[3].Direction = ParameterDirection.Input;
                    p[3].Value = mahieu_ctu;

                    p[4] = new OracleParameter();
                    p[4].ParameterName = "hthuc_nop";
                    p[4].DbType = DbType.String;
                    p[4].Direction = ParameterDirection.Input;
                    p[4].Value = hthuc_nop;

                    p[5] = new OracleParameter();
                    p[5].ParameterName = "mst_nnop";
                    p[5].DbType = DbType.String;
                    p[5].Direction = ParameterDirection.Input;
                    p[5].Value = mst_nnop;

                    p[6] = new OracleParameter();
                    p[6].ParameterName = "ten_nnop";
                    p[6].DbType = DbType.String;
                    p[6].Direction = ParameterDirection.Input;
                    p[6].Value = ten_nnop;

                    p[7] = new OracleParameter();
                    p[7].ParameterName = "diachi_nnop";
                    p[7].DbType = DbType.String;
                    p[7].Direction = ParameterDirection.Input;
                    p[7].Value = diachi_nnop;

                    p[8] = new OracleParameter();
                    p[8].ParameterName = "ma_nhang_nop";
                    p[8].DbType = DbType.String;
                    p[8].Direction = ParameterDirection.Input;
                    p[8].Value = ma_nhang_nop;

                    p[9] = new OracleParameter();
                    p[9].ParameterName = "ten_nhang_nop";
                    p[9].DbType = DbType.String;
                    p[9].Direction = ParameterDirection.Input;
                    p[9].Value = ten_nhang_nop;


                    p[10] = new OracleParameter();
                    p[10].ParameterName = "stk_nhang_nop";
                    p[10].DbType = DbType.String;
                    p[10].Direction = ParameterDirection.Input;
                    p[10].Value = stk_nhang_nop;

                    p[11] = new OracleParameter();
                    p[11].ParameterName = "loai_tk_thu";
                    p[11].DbType = DbType.String;
                    p[11].Direction = ParameterDirection.Input;
                    p[11].Value = loai_tk_thu;

                    p[12] = new OracleParameter();
                    p[12].ParameterName = "ten_tk_thu";
                    p[12].DbType = DbType.String;
                    p[12].Direction = ParameterDirection.Input;
                    p[12].Value = ten_tk_thu;

                    p[13] = new OracleParameter();
                    p[13].ParameterName = "stk_thu";
                    p[13].DbType = DbType.String;
                    p[13].Direction = ParameterDirection.Input;
                    p[13].Value = stk_thu;

                    p[14] = new OracleParameter();
                    p[14].ParameterName = "ma_cqthu";
                    p[14].DbType = DbType.String;
                    p[14].Direction = ParameterDirection.Input;
                    p[14].Value = ma_cqthu;

                    p[15] = new OracleParameter();
                    p[15].ParameterName = "ten_cqthu";
                    p[15].DbType = DbType.String;
                    p[15].Direction = ParameterDirection.Input;
                    p[15].Value = ten_cqthu;

                    p[16] = new OracleParameter();
                    p[16].ParameterName = "ngay_lap";
                    p[16].DbType = DbType.DateTime;
                    p[16].Direction = ParameterDirection.Input;
                    p[16].Value = ngay_lap;
                    if (ngay_lap != null && ngay_lap != "")
                    {
                        p[16].Value = DateTime.ParseExact(ngay_lap, "dd/MM/yyyy", null);
                    }
                    else
                    {
                        p[16].Value = DBNull.Value;
                    }

                    p[17] = new OracleParameter();
                    p[17].ParameterName = "ngay_gui";
                    p[17].DbType = DbType.String;
                    p[17].Direction = ParameterDirection.Input;
                    if (ngay_gui != null && ngay_gui != "")
                    {
                        p[17].Value = ngay_gui;
                    }
                    else
                    {
                        p[17].Value = DBNull.Value;
                    }


                    p[18] = new OracleParameter();
                    p[18].ParameterName = "ngay_nhang";
                    p[18].DbType = DbType.DateTime;
                    p[18].Direction = ParameterDirection.Input;
                    p[18].Value = DateTime.Now;

                    p[19] = new OracleParameter();
                    p[19].ParameterName = "tong_tien";
                    p[19].DbType = DbType.String;
                    p[19].Direction = ParameterDirection.Input;
                    p[19].Value = tong_tien;

                    p[20] = new OracleParameter();
                    p[20].ParameterName = "filecontent";
                    p[20].DbType = DbType.String;
                    p[20].Direction = ParameterDirection.Input;
                    p[20].Value = filecontent;

                    p[21] = new OracleParameter();
                    p[21].ParameterName = "van_id";
                    p[21].DbType = DbType.String;
                    p[21].Direction = ParameterDirection.Input;
                    p[21].Value = van_id;

                    p[22] = new OracleParameter();
                    p[22].ParameterName = "tthai_ctu";
                    p[22].DbType = DbType.String;
                    p[22].Direction = ParameterDirection.Input;
                    p[22].Value = tthai_ctu;

                    p[23] = new OracleParameter();
                    p[23].ParameterName = "ma_gd";
                    p[23].DbType = DbType.String;
                    p[23].Direction = ParameterDirection.Input;
                    p[23].Value = ma_gd;

                    p[24] = new OracleParameter();
                    p[24].ParameterName = "ma_cqt";
                    p[24].DbType = DbType.String;
                    p[24].Direction = ParameterDirection.Input;
                    p[24].Value = ma_cqt;


                    p[25] = new OracleParameter();
                    p[25].ParameterName = "ten_cqt";
                    p[25].DbType = DbType.String;
                    p[25].Direction = ParameterDirection.Input;
                    p[25].Value = ten_cqt;

                    p[26] = new OracleParameter();
                    p[26].ParameterName = "ten_tthai_ctu";
                    p[26].DbType = DbType.String;
                    p[26].Direction = ParameterDirection.Input;
                    p[26].Value = ten_tthai_ctu;

                    p[27] = new OracleParameter();
                    p[27].ParameterName = "mst_nthay";
                    p[27].DbType = DbType.String;
                    p[27].Direction = ParameterDirection.Input;
                    p[27].Value = mst_nthay;

                    p[28] = new OracleParameter();
                    p[28].ParameterName = "ten_nthay";
                    p[28].DbType = DbType.String;
                    p[28].Direction = ParameterDirection.Input;
                    p[28].Value = ten_nthay;

                    p[29] = new OracleParameter();
                    p[29].ParameterName = "diachi_nthay";
                    p[29].DbType = DbType.String;
                    p[29].Direction = ParameterDirection.Input;
                    p[29].Value = diachi_nthay;

                    p[30] = new OracleParameter();
                    p[30].ParameterName = "id_tk_knghi";
                    p[30].DbType = DbType.String;
                    p[30].Direction = ParameterDirection.Input;
                    p[30].Value = id_tk_knghi;

                    p[31] = new OracleParameter();
                    p[31].ParameterName = "ten_tk_knghi";
                    p[31].DbType = DbType.String;
                    p[31].Direction = ParameterDirection.Input;
                    p[31].Value = ten_tk_knghi;

                    p[32] = new OracleParameter();
                    p[32].ParameterName = "phienban_xml";
                    p[32].DbType = DbType.String;
                    p[32].Direction = ParameterDirection.Input;
                    p[32].Value = phienban_xml;


                    p[33] = new OracleParameter();
                    p[33].ParameterName = "ngay_gui_gd";
                    p[33].DbType = DbType.String;
                    p[33].Direction = ParameterDirection.Input;
                    p[33].Value = ngay_gui_gd;

                    p[34] = new OracleParameter();
                    p[34].ParameterName = "ma_xa_nnop";
                    p[34].DbType = DbType.String;
                    p[34].Direction = ParameterDirection.Input;
                    p[34].Value = ma_xa_nnop;

                    p[35] = new OracleParameter();
                    p[35].ParameterName = "ma_huyen_nnop";
                    p[35].DbType = DbType.String;
                    p[35].Direction = ParameterDirection.Input;
                    p[35].Value = ma_huyen_nnop;

                    p[36] = new OracleParameter();
                    p[36].ParameterName = "ma_tinh_nnop";
                    p[36].DbType = DbType.String;
                    p[36].Direction = ParameterDirection.Input;
                    p[36].Value = ma_tinh_nnop;

                    p[37] = new OracleParameter();
                    p[37].ParameterName = "ten_huyen_nnop";
                    p[37].DbType = DbType.String;
                    p[37].Direction = ParameterDirection.Input;
                    p[37].Value = ten_huyen_nnop;

                    p[38] = new OracleParameter();
                    p[38].ParameterName = "ten_tinh_nnop";
                    p[38].DbType = DbType.String;
                    p[38].Direction = ParameterDirection.Input;
                    p[38].Value = ten_tinh_nnop;

                    p[39] = new OracleParameter();
                    p[39].ParameterName = "ten_huyen_nthay";
                    p[39].DbType = DbType.String;
                    p[39].Direction = ParameterDirection.Input;
                    p[39].Value = ten_huyen_nthay;

                    p[40] = new OracleParameter();
                    p[40].ParameterName = "ten_tinh_nthay";
                    p[40].DbType = DbType.String;
                    p[40].Direction = ParameterDirection.Input;
                    p[40].Value = ten_tinh_nthay;

                    p[41] = new OracleParameter();
                    p[41].ParameterName = "ma_hieu_kbac";
                    p[41].DbType = DbType.String;
                    p[41].Direction = ParameterDirection.Input;
                    p[41].Value = ma_hieu_kb;

                    p[42] = new OracleParameter();
                    p[42].ParameterName = "ten_kbac";
                    p[42].DbType = DbType.String;
                    p[42].Direction = ParameterDirection.Input;
                    p[42].Value = ten_khobac;

                    p[43] = new OracleParameter();
                    p[43].ParameterName = "ma_tinh_kbac";
                    p[43].DbType = DbType.String;
                    p[43].Direction = ParameterDirection.Input;
                    p[43].Value = ma_tinh_khobac;

                    p[44] = new OracleParameter();
                    p[44].ParameterName = "ten_tinh_kbac";
                    p[44].DbType = DbType.String;
                    p[44].Direction = ParameterDirection.Input;
                    p[44].Value = ten_tinh_khobac;

                    //p[45] = new OracleParameter();
                    //p[45].ParameterName = "ma_dbhc_thu";
                    //p[45].DbType = DbType.String;
                    //p[45].Direction = ParameterDirection.Input;
                    //p[45].Value = ma_dbhc_thu;

                    //p[46] = new OracleParameter();
                    //p[46].ParameterName = "ten_dbhc_thu";
                    //p[46].DbType = DbType.String;
                    //p[46].Direction = ParameterDirection.Input;
                    //p[46].Value = ten_dbhc_thu;

                    p[45] = new OracleParameter();
                    p[45].ParameterName = "so_gnt";
                    p[45].DbType = DbType.String;
                    p[45].Direction = ParameterDirection.Input;
                    p[45].Value = so_gnt;

                    p[46] = new OracleParameter();
                    p[46].ParameterName = "ma_lthue";
                    p[46].DbType = DbType.String;
                    p[46].Direction = ParameterDirection.Input;
                    p[46].Value = ma_loai_thue;

                    p[47] = new OracleParameter();
                    p[47].ParameterName = "TGIAN_TCT_GUI";
                    // p[49].DbType = DbType.String;
                    p[47].DbType = DbType.String;
                    p[47].Direction = ParameterDirection.Input;
                    p[47].Value = send_date;
                    p[47].Value = DateTime.Parse(send_date).ToString("dd/MM/yyyy HH:mm:ss"); 

                    p[48] = new OracleParameter();
                    p[48].ParameterName = "TGIAN_NH_NHAN";
                    //p[50].DbType = DbType.String;
                    p[48].DbType = DbType.String;
                    p[48].Direction = ParameterDirection.Input;
                    //p[50].Value = received_date;
                    p[48].Value = received_date;


                    p[49] = new OracleParameter();
                    p[49].ParameterName = "MA_NGUYENTE";
                    p[49].DbType = DbType.String;
                    p[49].Direction = ParameterDirection.Input;
                    p[49].Value = MA_NGUYENTE;

                    p[50] = new OracleParameter();
                    p[50].ParameterName = "MA_THAMCHIEU";
                    p[50].DbType = DbType.String;
                    p[50].Direction = ParameterDirection.Input;
                    p[50].Value = MA_THAMCHIEU;
                    //
                    p[51] = new OracleParameter();
                    p[51].ParameterName = "ma_nhang_unt";
                    p[51].DbType = DbType.String;
                    p[51].Direction = ParameterDirection.Input;
                    p[51].Value = ma_nhang_unt;

                    p[52] = new OracleParameter();
                    p[52].ParameterName = "ten_nhang_unt";
                    p[52].DbType = DbType.String;
                    p[52].Direction = ParameterDirection.Input;
                    p[52].Value = ten_nhang_unt;

                    // tuanda - 01/02/24
                    p[53] = new OracleParameter();
                    p[53].ParameterName = "ngay_ctu";
                    p[53].DbType = DbType.DateTime;
                    p[53].Direction = ParameterDirection.Input;
                    p[53].Value = ngay_ctu;
                    if (ngay_ctu != null && ngay_ctu != "")
                    {
                        p[53].Value = DateTime.ParseExact(ngay_ctu, "dd/MM/yyyy", null);
                    }
                    else
                    {
                        p[53].Value = DateTime.Now;
                    }

                    p[54] = new OracleParameter();
                    p[54].ParameterName = "lhinh_nnt";
                    p[54].DbType = DbType.String;
                    p[54].Direction = ParameterDirection.Input;
                    p[54].Value = lhinh_nnt;

                    // tuanda -23/02/24
                    p[55] = new OracleParameter();
                    p[55].ParameterName = "MA_THAMCHIEU_KNOP";
                    p[55].DbType = DbType.String;
                    p[55].Direction = ParameterDirection.Input;
                    p[55].Value = MA_THAMCHIEU;

                    DataAccess.ExecuteNonQuery(sbSqlInsert.ToString(), CommandType.Text, p, transCT);
                    foreach (GDT.MSG.MSG03002.ROW_CTIET itdata in objCTu_dtl.ROW_CTIET)
                    {
                        DTLToDB(itdata, so_ctu, transCT);
                    }
                    //}
                }
                transCT.Commit();
                //Gọi hàm hạch toán
                //payment(objCTu_hdr,objCTu_dtl,so_ctu);
            }
            catch (Exception ex)
            {
                tthai_ctu = GDICH_NTDT_Constants.TT_CT_LOI;// 
                ten_tthai_ctu = GDICH_NTDT_Constants.TEN_TT_CT_LOI;

                transCT.Rollback();
                //throw ex;
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: ThemGNT_NTDT " + ex.Message, System.Diagnostics.EventLogEntryType.Error);

            }
            finally
            {

                conn.Close();
                conn.Dispose();
            }

        }
        public static void DTLToDB(GDT.MSG.MSG03002.ROW_CTIET objCTu_CT, string so_CT, IDbTransaction dbTran)
        {

            //insert tcs_dchieu_hqnh_dtl  so_tk_tb_qd
            StringBuilder sbsql = new StringBuilder();

            sbsql.Append("INSERT INTO tcs_ctu_ntdt_dtl (ID, ");
            sbsql.Append("ID_CU, ");
            sbsql.Append("SO_CT, ");
            sbsql.Append("MA_CHUONG, ");
            sbsql.Append("MA_TMUC, ");
            sbsql.Append("NOI_DUNG, ");
            sbsql.Append("SOTIEN, ");
            sbsql.Append("SOTIEN_NT, ");
            sbsql.Append("KYTHUE, ");
            sbsql.Append("so_tk_tb_qd, ");
            sbsql.Append("GHICHU, ");

            sbsql.Append("ID_KNOP, ");
            sbsql.Append("MA_DBHC_THU, ");
            sbsql.Append("TEN_DBHC_THU) ");

            sbsql.Append("VALUES( :ID, ");
            sbsql.Append(":ID_CU, ");
            sbsql.Append(":SO_CT, ");
            sbsql.Append(":MA_CHUONG, ");
            sbsql.Append(":MA_TMUC, ");
            sbsql.Append(":NOI_DUNG, ");
            sbsql.Append(":SOTIEN, ");
            sbsql.Append(":SOTIEN_NT, ");
            sbsql.Append(":KYTHUE, ");
            sbsql.Append(":so_tk_tb_qd, ");
            sbsql.Append(":GHICHU, ");

            sbsql.Append(":ID_KNOP, ");
            sbsql.Append(":MA_DBHC_THU, ");
            sbsql.Append(":TEN_DBHC_THU) ");

            IDbDataParameter[] p = new IDbDataParameter[14];

            p[0] = new OracleParameter();
            p[0].ParameterName = "ID";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = DBNull.Value;
            p[0].Value = objCTu_CT.ID_CTU_CTIET;

            p[1] = new OracleParameter();
            p[1].ParameterName = "ID_CU";
            p[1].DbType = DbType.String;
            p[1].Direction = ParameterDirection.Input;
            p[1].Value = objCTu_CT.ID_CTU;

            p[2] = new OracleParameter();
            p[2].ParameterName = "SO_CT";
            p[2].DbType = DbType.String;
            p[2].Direction = ParameterDirection.Input;
            p[2].Value = so_CT;

            p[3] = new OracleParameter();
            p[3].ParameterName = "MA_CHUONG";
            p[3].DbType = DbType.String;
            p[3].Direction = ParameterDirection.Input;
            p[3].Value = objCTu_CT.MA_CHUONG;

            p[4] = new OracleParameter();
            p[4].ParameterName = "MA_TMUC";
            p[4].DbType = DbType.String;
            p[4].Direction = ParameterDirection.Input;
            p[4].Value = objCTu_CT.MA_NDKT;

            p[5] = new OracleParameter();
            p[5].ParameterName = "NOI_DUNG";
            p[5].DbType = DbType.String;
            p[5].Direction = ParameterDirection.Input;
            p[5].Value = objCTu_CT.NDUNG_NOP;

            p[6] = new OracleParameter();
            p[6].ParameterName = "SOTIEN";
            p[6].DbType = DbType.String;
            p[6].Direction = ParameterDirection.Input;
            p[6].Value = objCTu_CT.TIEN_PNOP;

            p[7] = new OracleParameter();
            p[7].ParameterName = "SOTIEN_NT";
            p[7].DbType = DbType.String;
            p[7].Direction = ParameterDirection.Input;
            p[7].Value = DBNull.Value;

            p[8] = new OracleParameter();
            p[8].ParameterName = "KYTHUE";
            p[8].DbType = DbType.String;
            p[8].Direction = ParameterDirection.Input;
            p[8].Value = objCTu_CT.KY_THUE;

            p[9] = new OracleParameter();
            p[9].ParameterName = "so_tk_tb_qd";
            p[9].DbType = DbType.String;
            p[9].Direction = ParameterDirection.Input;
            p[9].Value = objCTu_CT.SO_TK_TB_QD;

            p[10] = new OracleParameter();
            p[10].ParameterName = "GHICHU";
            p[10].DbType = DbType.String;
            p[10].Direction = ParameterDirection.Input;
            p[10].Value = objCTu_CT.GHI_CHU;

            // thêm ID_KNOP, MA_DBHC_THU, TEN_DBHC_THU 
            p[11] = new OracleParameter();
            p[11].ParameterName = "ID_KNOP";
            p[11].DbType = DbType.String;
            p[11].Direction = ParameterDirection.Input;
            p[11].Value = objCTu_CT.ID_KNOP;

            p[12] = new OracleParameter();
            p[12].ParameterName = "MA_DBHC_THU";
            p[12].DbType = DbType.String;
            p[12].Direction = ParameterDirection.Input;
            p[12].Value = objCTu_CT.MA_DBHC_THU;

            p[13] = new OracleParameter();
            p[13].ParameterName = "TEN_DBHC_THU";
            p[13].DbType = DbType.String;
            p[13].Direction = ParameterDirection.Input;
            p[13].Value = objCTu_CT.TEN_DBHC_THU;

            DataAccess.ExecuteNonQuery(sbsql.ToString(), CommandType.Text, p, dbTran);
        }
        #endregion
        #region "Xử lý hạch toán"
        //build remark
        private string buildRemark(GDT.MSG.MSG03002.CHUNGTU_HDR objHdr, string ngayNT)
        {
            string result = "";
            // string ngayNT =ClsCoreBank.;
            // if (objHdr.NGAY_LAP != "")
            //{
            //    ngayNT = objHdr.NGAY_LAP.Replace("/","");
            //}else
            //    {
            //        ngayNT = DateTime.Now.ToString("ddMMyyyy").ToString();
            //    }
            //ngayNT = DateTime.Now.ToString("ddMMyyyy").ToString();

            result += "NTDT+KB:" + objHdr.MA_HIEU_KBAC;
            result += "-" + objHdr.TEN_KBAC;
            //result += "-" + objHdr.TEN_TINH_KBAC;
            result += "+NgayNT:" + ngayNT.Replace("/", "");
            result += "+MST:" + objHdr.MST_NNOP;
            //result += "+DBHC:" + objHdr.MA_DBHC_THU;
            result += "+TKNS:" + objHdr.STK_THU;
            result += "+CQT:" + objHdr.MA_CQTHU;
            result += "+LThue:" + objHdr.MA_LOAI_THUE;
            return result;
        }
        private string buildRemark_V20(GDT.MSG.MSG03002.CHUNGTU_HDR objHdr, string ngayNT)
        {
            string result = "";

            ngayNT = DateTime.Now.ToString("ddMMyyyy").ToString();
            result += "NTDT+KB:" + objHdr.MA_HIEU_KBAC;
            result += "-" + objHdr.TEN_KBAC;

            return result;
        }
        private string builremakchitiet(GDT.MSG.MSG03002.ROW_CTIET objChitiet)
        {
            string result = "";
            result += "(C:" + objChitiet.MA_CHUONG;
            result += "-TM:" + objChitiet.MA_NDKT;
            result += "-KT:" + objChitiet.KY_THUE;
            result += "-ST:" + objChitiet.TIEN_PNOP;
            result += "-GChu:" + objChitiet.GHI_CHU;
            result += ")";
            return result;
        }
        private string builremakchitiet_V20(GDT.MSG.MSG03002.ROW_CTIET objChitiet)
        {
            string result = "";
            result += "+(GChu:" + objChitiet.GHI_CHU;
            result += ")";
            return result;
        }
        private string builecomchitiet_V20(GDT.MSG.MSG03002.ROW_CTIET objChitiet)
        {
            string result = "";
            result += V_SubString(objChitiet.MA_NDKT, 4);
            result += "~" + V_SubString(objChitiet.MA_CHUONG, 3);
            result += "~" + V_SubString(objChitiet.TIEN_PNOP, 20);
            result += "~" + V_SubString(objChitiet.NDUNG_NOP, 100);
            result += "~";//Khong co so quyet dinh /so to khai
            //Định dạng 00/MM/yyyy
            //result += "~" + "00-" + DateTime.ParseExact(objChitiet.KY_THUE, "MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM-yyyy");
            result += "~" + objChitiet.KY_THUE;
            return result;
        }
        // hạch toán
        private DataSet GetNHNhan(string strSHKB)
        {
            string strSql = "Select MA_GIANTIEP MA, TEN_GIANTIEP TEN, MA_TINH  From TCS_DM_NH_GIANTIEP_TRUCTIEP Where SHKB= :SHKB";
            DataSet ds;
            IDbDataParameter[] p = null;
            p = new IDbDataParameter[1];
            p[0] = new OracleParameter();
            p[0].ParameterName = "SHKB";
            p[0].DbType = DbType.String;
            p[0].Direction = ParameterDirection.Input;
            p[0].Value = strSHKB;



            ds = DataAccess.ExecuteReturnDataSet(strSql.ToString(), CommandType.Text, p);
            return ds;
        }
        private string GetTinhThanhNHTH(string ma_tinhthanh)
        {
            DataSet ds = null;
            try
            {
                string strSql = "Select ten  From TCS_DM_XA Where MA_XA= :MA_XA";
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "MA_XA";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = ma_tinhthanh + "TTT";
                ds = DataAccess.ExecuteReturnDataSet(strSql.ToString(), CommandType.Text, p);
                return ds.Tables[0].Rows[0][0].ToString();
            }
            catch
            {
                return "";
            }

        }
        private void payment_tt84(GDT.MSG.MSG03002.CHUNGTU_HDR objhdr, GDT.MSG.MSG03002.CHUNGTU_CTIET objchitiet, string So_CT, ref string strTT_GD, ref string ten_trangthai)
        {
            try
            {
                //Tham so khong thanh toan USD
                // 1: ko cho thanh toan bang USD, 0 la cho thanh toan bang USD
                string strRunTT84 = "0";
                try
                {
                    strRunTT84 = ConfigurationManager.AppSettings.Get("NTDT.RUNTT84").ToString();
                }
                catch (Exception exc) { strRunTT84 = "0"; }

                string ma_nguyente = "VND";
                if (objhdr.MA_NGUYENTE != null)
                {
                    ma_nguyente = objhdr.MA_NGUYENTE;
                }

                if (strRunTT84.Equals("1"))
                {
                    if (ma_nguyente.ToUpper().Equals("USD"))
                    {
                        strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                        ten_trangthai = GDICH_NTDT_Constants.TEN_TT_CT_LOI;
                        UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                        return;
                    }
                }
                //end tu choi nop USD tt84
                //string ten_trangthai = "";
                string mast_tk = objhdr.MST_NNOP.Trim();
                //kiểm tra số tài khoản có đúng với tài khoản đã đăng ký không?
                if (objhdr.MST_NTHAY != null && objhdr.MST_NTHAY != "")
                { mast_tk = objhdr.MST_NTHAY; }
                if (CheckTK(mast_tk, objhdr.STK_NHANG_NOP) == false)
                {
                    strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                    ten_trangthai = Utility.getTen_TT(strTT_GD) + "Sai tài khoản đăng ký";
                    UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                    //Gọi mess thông báo không đúng số tài khoản ở đây??
                    return;

                }
                //bool checkTTGT = Utility.checkExist_NH_GT_TT(objhdr.MA_HIEU_KBAC, objhdr.MA_NHANG_UNT);
                //if (checkTTGT == false || objhdr.MA_NHANG_UNT.Length <= 0)
                //{
                //    strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                //    ten_trangthai = "Xử lý chứng từ không thành công tại Ngân hàng (Hạch toán không thành công). Mã lỗi: 13. Mô tả :Không tồn tại danh mục ngân hàng";
                //    UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                //    return;
                //}

                // kiểm tra số dư
                CorebankServiceESB.clsCoreBank clscorebank = new CorebankServiceESB.clsCoreBank();
                string maloi = "";
                string sodu_KD = "";
                string nguyente_tk = "";
                string branch_tk = "";
                string strSoDuTK = clscorebank.GetTK_KH_NH(objhdr.STK_NHANG_NOP, ref maloi, ref sodu_KD, ref branch_tk);
                if (maloi == TT_THANHCONG)
                {
                    if (!string.IsNullOrEmpty(strSoDuTK))
                    {
                        //sonmt: lay thong tin tai khoan corebank
                        XmlDocument xmldoc = new XmlDocument();
                        XmlNode xmlNode = null;
                        try
                        {
                            xmldoc.LoadXml(strSoDuTK);
                            xmlNode = xmldoc.SelectSingleNode("PARAMETER/PARAMS/CRR_CY_CODE");
                            nguyente_tk = xmlNode.InnerText.Trim();

                            if (!ma_nguyente.ToUpper().Equals(nguyente_tk.ToUpper()))
                            {
                                strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                                ten_trangthai = Utility.getTen_TT(strTT_GD) + ". Nguyên tệ tài khoản khác nguyên tệ của giấy nộp tiền";
                                UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                                return;
                            }
                        }
                        catch (Exception ex)
                        {
                            strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                            ten_trangthai = Utility.getTen_TT(strTT_GD) + "(Kiểm tra thông tin tài khoản không thành công) ";
                            UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                            //Gọi mess thông báo không đúng số tài khoản ở đây??
                            return;
                        }
                    }
                    else
                    {
                        strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                        ten_trangthai = Utility.getTen_TT(strTT_GD) + "(Kiểm tra thông tin tài khoản không thành công) ";
                        UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                        //Gọi mess thông báo không đúng số tài khoản ở đây??
                        return;
                    }

                    if (double.Parse(objhdr.TONG_TIEN) > double.Parse(sodu_KD))
                    {
                        strTT_GD = GDICH_NTDT_Constants.TT_CT_THIEU_TIEN;
                        ten_trangthai = Utility.getTen_TT(strTT_GD);
                        UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                        return;
                        //Gọi mess thông báo không đủ số dư ở đây ở đây??
                    }

                }
                else
                {
                    strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                    ten_trangthai = Utility.getTen_TT(strTT_GD) + " (Kiểm tra số dư không thành công.)";
                    UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                    //Gọi mess thông báo không lấy được thông tin tài khoản đủ số dư ở đây ở đây??
                    return;
                }
                // Hạch toán

                string pv_ACCOUNTNO = objhdr.STK_NHANG_NOP;
                string pv_BEN_ACCOUNTNO = objhdr.STK_THU;
                string AMOUNT = objhdr.TONG_TIEN;

                string CCY = "704";
                string REMARK = "";
                string ngay_ht = "";

                if (ma_nguyente.ToUpper().Equals("VND"))
                {
                    CCY = "704";
                }
                else if (ma_nguyente.ToUpper().Equals("USD"))
                {
                    CCY = "840";
                }

                ngay_ht = clscorebank.Get_VALUE_DATE();
                //Utility.Log_Action("NGAY_HT", clscorebank.Get_VALUE_DATE());
                ngay_ht = Utility.ConvertNumberToDate(ngay_ht).ToString("dd/MM/yyyy");
                if (Core_version.Equals("2"))
                {
                    REMARK = buildRemark_V20(objhdr, ngay_ht);
                    foreach (GDT.MSG.MSG03002.ROW_CTIET objdtl in objchitiet.ROW_CTIET)
                    {
                        REMARK += builremakchitiet_V20(objdtl);
                    }
                }
                else
                {
                    REMARK = buildRemark(objhdr, ngay_ht);
                    foreach (GDT.MSG.MSG03002.ROW_CTIET objdtl in objchitiet.ROW_CTIET)
                    {
                        REMARK += builremakchitiet(objdtl);
                    }
                }
                REMARK = VBOracleLib.Globals.RemoveSign4VietnameseString(REMARK).Replace("'", "");
                if (REMARK.Length > 210)
                {
                    strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                    ten_trangthai = Utility.getTen_TT(strTT_GD) + " (Quá 210 ký tự, số ký tự hiện tại: " + REMARK.Length + ").";
                    UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                    //Gọi mess thông báo không lấy được thông tin tài khoản đủ số dư ở đây ở đây??
                    Utility.Log_Action(strTran_Type, REMARK);
                    return;
                }

                string TransactionDate = Utility.ConvertDateToNumber(DateTime.Now.ToString("dd/MM/yyyy")).ToString();
                string BEN_NAME = objhdr.TEN_CQTHU;
                string BEN_CITY = objhdr.TEN_TINH_KBAC;
                string BEN_ADD1 = objhdr.TEN_CQTHU;
                string BEN_ADD2 = BEN_ADD1;

                string strMa_NH_B = "";
                string BANK_BEN_DESC = "";
                string BR_BEN_DESC = "";
                string CITY_BEN_DESC = "";
                string BANK_BEN_CODE = "";
                string BR_BEN_CODE = "";
                string CITY_BEN_CODE = "";
                //Utility.Log_Action("GetNHNhan", objhdr.MA_HIEU_KBAC);
                DataSet ds = GetNHNhan(objhdr.MA_HIEU_KBAC);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //Utility.Log_Action("GetTinhThanhNHTH", objhdr.MA_HIEU_KBAC);
                        CITY_BEN_DESC = GetTinhThanhNHTH(ds.Tables[0].Rows[0]["MA_TINH"].ToString());
                    }
                }
                //
                strMa_NH_B = objhdr.MA_NHANG_UNT; //ds.Tables[0].Rows[0]["MA"].ToString();
                BANK_BEN_DESC = objhdr.TEN_NHANG_UNT; //ds.Tables[0].Rows[0]["TEN"].ToString();
                BR_BEN_DESC = objhdr.TEN_NHANG_UNT; //ds.Tables[0].Rows[0]["TEN"].ToString();
                BANK_BEN_CODE = strMa_NH_B.ToString().Substring(2, 3);
                CITY_BEN_CODE = strMa_NH_B.ToString().Substring(0, 2);
                BR_BEN_CODE = strMa_NH_B.ToString().Substring(5, 3);

                //
                string KENH_HT = "TDT";
                string v_strSo_CT = So_CT;

                string v_strMa_NV = "";
                string v_strMa_KSV = "";
                string v_strErrorCode = "";
                string v_strRef_No = "";

                string thong_tin_nnt_duoc_nop_thay = "";
                if (objhdr.MST_NTHAY.Trim().Length > 0)
                {
                    thong_tin_nnt_duoc_nop_thay = "[nop thay cho mst:]" + objhdr.MST_NNOP + "-" + objhdr.TEN_NNOP;
                }

                //Valid xem co dc nop tai SHB
                CorebankServiceESB.clsSongPhuong clsSP = new CorebankServiceESB.clsSongPhuong();
                //Utility.Log_Action("ValiadSHB", objhdr.MA_HIEU_KBAC);
                string strResult = clsSP.ValiadSHB(objhdr.MA_HIEU_KBAC);

                string[] arrResult;
                string strNHB = "";
                string strACC_NHB = "";
                string strMA_CN_SP = "";
                if (strResult.Length > 8)
                {
                    arrResult = strResult.Split(';');
                    strNHB = arrResult[0].ToString();
                    strACC_NHB = arrResult[1].ToString();
                    strMA_CN_SP = arrResult[2].ToString();
                }

                bool checkThuSHB = false;

                if (strNHB.Length >= 8)
                {
                    if (strNHB.Equals(objhdr.MA_NHANG_UNT))
                    {
                        //Đi theo hàm thanh toán mới kho bạc tại SHB
                        checkThuSHB = true;
                    }
                    else
                    {
                        //Đi theo hàm thanh toán cũ.

                    }
                }
                else
                {
                    //TH1  1. chọn KB, UNT SHB nhưng SHB ko được thu
                    //check tiếp xem có ngân hàng ủy nhiệm thu có phải SHB hay không, nếu là SHB sẽ từ chối luôn với TCT.
                    //Utility.Log_Action("checkExist_NH_GT_TT_SHB", "TRUE");
                    bool checkTTGT = Utility.checkExist_NH_GT_TT_SHB(objhdr.MA_HIEU_KBAC, objhdr.MA_NHANG_UNT);
                    if (checkTTGT == true || objhdr.MA_NHANG_UNT.Length <= 0)
                    {
                        strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                        ten_trangthai = "Xử lý chứng từ không thành công tại Ngân hàng. Mã lỗi: 13. Mô tả : GNT không được nộp tại ngân hàng ủy nhiệm thu SHB.";
                        UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                        return;
                    }

                }

                //
                string ngay_GD_SP = Utility.ConvertNumberToDate(ngay_ht).ToString("yyyyMMdd");
                if (checkThuSHB == true)
                {
                    //cap nhat TTSP la 1
                    // Utility.Log_Action("checkThuSHB", "TRUE");

                    string strSQL = "UPDATE tcs_ctu_ntdt_hdr SET TT_SP='1' WHERE so_ctu='" + v_strSo_CT + "' ";
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);

                    //Utility.Log_Action("UPDATE tcs_ctu_ntdt_hdr", objhdr.MA_HIEU_KBAC);
                    //Gọi hàm thanh toán mới vào kho bạc SHB
                    clscorebank.PaymentSP(KENH_HT, ngay_GD_SP, v_strSo_CT, "110000", branch_tk, objhdr.STK_NHANG_NOP, objhdr.TONG_TIEN, objhdr.MA_NGUYENTE, "", REMARK,
                             v_strMa_NV, v_strMa_KSV, "NTDT", pv_BEN_ACCOUNTNO, strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B, strACC_NHB, strMA_CN_SP, ref v_strErrorCode, ref v_strRef_No);
                    //Utility.Log_Action("PaymentSP", objhdr.MA_HIEU_KBAC);
                }
                else
                {
                    if (Core_version.Equals("2"))
                    {
                        //Core V2.0 20180330---------------------------
                        string EcomStr = String.Empty;

                        //int v_count = 0;
                        foreach (GDT.MSG.MSG03002.ROW_CTIET objdtl in objchitiet.ROW_CTIET)
                        {


                            EcomStr += builecomchitiet_V20(objdtl);
                            //if (v_count != 0)
                            //{
                            EcomStr += "####";
                            //}
                            //v_count++;
                        }

                        REMARK = V_SubString(REMARK, 210);
                        string str_TenMST = V_SubString(objhdr.TEN_NNOP, 200);
                        string str_MST = V_SubString(objhdr.MST_NNOP, 14);
                        string str_MaLThue = V_SubString(objhdr.MA_LOAI_THUE, 2);
                        //Ngay nop thue dinh dang dd-MM-yyyy
                        string str_NNT = DateTime.Now.ToString("yyyyMMdd");
                        string str_AddNNT = V_SubString(objhdr.DIACHI_NNOP, 200);
                        string strMaCQT = V_SubString(objhdr.MA_CQTHU, 7);
                        string strTenCQT = V_SubString(objhdr.TEN_CQTHU, 200);
                        //string strMaDBHC = V_SubString(objhdr.MA_DBHC_THU, 5);
                        //string strTenDBHC = V_SubString(VBOracleLib.Globals.RemoveSign4VietnameseString(objhdr.TEN_DBHC_THU), 200);
                        //clscorebank.Payment_V20(pv_ACCOUNTNO, pv_BEN_ACCOUNTNO, AMOUNT, CCY, REMARK, TransactionDate, VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_CITY),
                        //                         VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_ADD1).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_ADD2).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BANK_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BR_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC), BANK_BEN_CODE, BR_BEN_CODE, CITY_BEN_CODE,
                        //                         KENH_HT, v_strSo_CT, strMa_NH_B, v_strMa_NV, v_strMa_KSV, "NTDT", thong_tin_nnt_duoc_nop_thay,
                        //                         str_NNT, str_MaLThue, str_MST, strMaCQT, strTenCQT, strMaDBHC, strTenDBHC, str_TenMST, str_AddNNT, EcomStr,
                        //                         ref v_strErrorCode, ref v_strRef_No);

                        // tuanda
                        clscorebank.Payment_V20(pv_ACCOUNTNO, pv_BEN_ACCOUNTNO, AMOUNT, CCY, REMARK, TransactionDate, VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_CITY),
                                                 VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_ADD1).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_ADD2).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BANK_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BR_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC), BANK_BEN_CODE, BR_BEN_CODE, CITY_BEN_CODE,
                                                 KENH_HT, v_strSo_CT, strMa_NH_B, v_strMa_NV, v_strMa_KSV, "NTDT", thong_tin_nnt_duoc_nop_thay,
                                                 str_NNT, str_MaLThue, str_MST, strMaCQT, strTenCQT, str_TenMST, str_AddNNT, EcomStr,
                                                 ref v_strErrorCode, ref v_strRef_No);

                        //Core V2.0 20180330--------------------------------
                    }
                    else
                    {
                        clscorebank.Payment(pv_ACCOUNTNO, pv_BEN_ACCOUNTNO, AMOUNT, CCY, REMARK, TransactionDate, VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_CITY),
                                                 VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_ADD1).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_ADD2).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BANK_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(BR_BEN_DESC).Replace("'", ""), VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC), BANK_BEN_CODE, BR_BEN_CODE, CITY_BEN_CODE,
                                                 KENH_HT, v_strSo_CT, strMa_NH_B, v_strMa_NV, v_strMa_KSV, "NTDT", thong_tin_nnt_duoc_nop_thay, ref v_strErrorCode, ref v_strRef_No);
                    }
                }

                if (v_strErrorCode == TT_THANHCONG || v_strErrorCode == TT_THANHCONG_TO999)
                {// hạch toán thành công
                    if (v_strErrorCode == TT_THANHCONG)
                    {
                        //update remaks to tcs_ctu_ntdt_hdr
                        UpDateRemarks(So_CT, objhdr.ID_CTU, REMARK, Utility.GetNgayKhoBac(objhdr.MA_HIEU_KBAC));
                        strTT_GD = GDICH_NTDT_Constants.TT_CT_THANHCONG;
                        ten_trangthai = Utility.getTen_TT(strTT_GD);

                    }
                    else
                    {
                        //timeout thì return luôn, đợi check xem cắt tiền core chưa rồi mới xử lý tiếp.
                        return;
                        //strTT_GD = GDICH_NTDT_Constants.TT_CT_TIMEOUT;
                        //ten_trangthai = Utility.getTen_TT(strTT_GD);
                        //strTT_GD = "13";
                    }

                    log.Info("Bắt đầu cập nhật trạng thái gd\n ttgd" + strTT_GD + "\n tentt:" + ten_trangthai);
                    UpDateTTChungTu(strTT_GD, ten_trangthai, v_strRef_No, So_CT, objhdr.ID_CTU);
                    log.Info("Kết thúc cập nhật trạng thái:");
                    //Nếu hạch thoán thành công thì gửi đi song phương
                    if (checkThuSHB == true && strTT_GD == GDICH_NTDT_Constants.TT_CT_THANHCONG)
                    {
                        //Utility.Log_Action("SendChungTuSP", "TRUE");
                        clsSP.SendChungTuSP(v_strSo_CT, "NTDT");
                    }
                    else if (checkThuSHB == false && strTT_GD == GDICH_NTDT_Constants.TT_CT_THANHCONG)
                    {
                        Business.CitadV25 citad25 = new Business.CitadV25();
                        log.Info("ntdt_sendcontentex");
                        string core_date = clscorebank.Get_VALUE_DATE();
                        string content_ex = citad25.CreateContentEx(v_strSo_CT, "NTDT", core_date);

                        content_ex = Business.Common.mdlCommon.EncodeBase64_ContentEx(content_ex);

                        clscorebank.SendContentEx(v_strSo_CT, v_strRef_No, "NTDT", content_ex, ref v_strErrorCode);
                        if (v_strErrorCode.Equals("00000"))
                        {
                            //cap nhap send core thanh cong: 1 la thanh cong, 0 la chua thanh cong
                            //VBOracleLib.DataAccess.ExecuteNonQuery("update TCS_CTU_NTDT_HDR set SEND_CONTENT_EX = '1' where so_ctu='" + v_strSo_CT + "'", CommandType.Text);

                            // tuanda - 27/02/24
                            // cập nhật trạng thái chứng từ gửi đi kbnn
                            VBOracleLib.DataAccess.ExecuteNonQuery("update TCS_CTU_NTDT_HDR set TRANGTHAI_SENDKB = '01', TEN_TRANGTHAI_SENDKB = 'Đã gửi thành công đến NH UNT', SEND_CONTENT_EX = '1' where so_ctu='" + v_strSo_CT + "'", CommandType.Text);

                        }
                        else {
                            // tuanda - 27/02/24
                            // cập nhật trạng thái chứng từ gửi đi kbnn
                            VBOracleLib.DataAccess.ExecuteNonQuery("update TCS_CTU_NTDT_HDR set TRANGTHAI_SENDKB = '03', TEN_TRANGTHAI_SENDKB = 'Gửi lỗi đến NH UNT' where so_ctu='" + v_strSo_CT + "'", CommandType.Text);
                        }

                        // tuanda - 27/02/24
                        // auto process gửi TCT msg trạng thái phản hồi CT đi Đơn vị UNT 

                    }
                    return;
                }
                else if (v_strErrorCode.Equals("005"))
                {
                    strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                    ten_trangthai = Utility.getTen_TT(strTT_GD) + " (Hạch toán không thành công - Tài khoản không đủ để hạch toán)."; ;
                    UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                    return;
                }
                else if (v_strErrorCode.Equals("TO999"))
                {
                    //timeout thì để nguyên trạng thái 15
                    return;
                }
                else
                {// hạch toán không thành công
                    strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                    ten_trangthai = Utility.getTen_TT(strTT_GD) + " (Hạch toán không thành công)."; ;
                    UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                    return;
                }
            }
            catch (Exception ex)
            {
                strTT_GD = GDICH_NTDT_Constants.TT_CT_LOI;
                ten_trangthai = Utility.getTen_TT(strTT_GD) + " (" + ex.Message + ").";
                UpDateTTChungTu(strTT_GD, ten_trangthai, So_CT, objhdr.ID_CTU);
                //throw ex;
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog(ex.Message + "---------" + ex.StackTrace, EventLogEntryType.Error);


                //param log_action
                string strMSG_ID = "Lỗi trong quá trình hạch toán:" + objhdr.MA_HIEU_KBAC + "; SỐ CT: " + So_CT + " (" + ex.Message + ")";
                string strTran_Type = "Payment";
                //param log_action
                strMSG_TEXT = "GetMQ=StartTime:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "Error = " + strMSG_ID;
                Utility.Log_Action(strTran_Type, strMSG_TEXT);
            }
        }
        //lấy tên trạng thái
        private string getTen_TrangThai(string strTrang_Thai)
        {
            DataSet ds = null;
            try
            {
                //Tao cau lenh Insert
                StringBuilder sbSqlSelect = new StringBuilder("");
                sbSqlSelect.Append("select TEN_TT from  tcs_dm_trangthai_ntdt WHERE  TRANG_THAI = :TRANG_THAI AND GHICHU ='CT'");
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[1];
                p[0] = new OracleParameter();
                p[0].ParameterName = "TRANG_THAI";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = strTrang_Thai;

                ds = DataAccess.ExecuteReturnDataSet(sbSqlSelect.ToString(), CommandType.Text, p);
                return ds.Tables[0].Rows[0][0].ToString();
            }
            catch
            {
                return "";
            }
        }
        // update trạng thái
        private void UpDateTTChungTu(string strTrang_Thai, string strTen_Trang_Thai, string So_CT, string id_ctu)
        {
            OracleConnection conn = null;
            IDbTransaction transCT = null;
            try
            {

                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                //Tao cau lenh Insert
                StringBuilder sbSqlUpDate = new StringBuilder("");
                sbSqlUpDate.Append("update tcs_ctu_ntdt_hdr set tthai_ctu = :TRANG_THAI,ten_tthai_ctu=:TEN_TRANGTHAI  where so_ctu= :so_CT and id_ctu =:id_ctu ");
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[4];
                p[0] = new OracleParameter();
                p[0].ParameterName = "TRANG_THAI";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = strTrang_Thai;

                p[1] = new OracleParameter();
                p[1].ParameterName = "TEN_TRANGTHAI";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = strTen_Trang_Thai;

                p[2] = new OracleParameter();
                p[2].ParameterName = "so_CT";
                p[2].DbType = DbType.String;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = So_CT;

                p[3] = new OracleParameter();
                p[3].ParameterName = "id_ctu";
                p[3].DbType = DbType.String;
                p[3].Direction = ParameterDirection.Input;
                p[3].Value = id_ctu;
                DataAccess.ExecuteNonQuery(sbSqlUpDate.ToString(), CommandType.Text, p, transCT);
                transCT.Commit();
            }
            catch
            {
                transCT.Rollback();
                conn.Close();
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        private void UpDateTTChungTu(string strTrang_Thai, string strTen_Trang_Thai, string so_ref_no, string So_CT, string id_ctu)
        {
            OracleConnection conn = null;
            IDbTransaction transCT = null;
            try
            {

                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                //Tao cau lenh Insert
                StringBuilder sbSqlUpDate = new StringBuilder("");
                sbSqlUpDate.Append("update tcs_ctu_ntdt_hdr set tthai_ctu = :TRANG_THAI,ten_tthai_ctu=:TEN_TRANGTHAI,ngay_ht=:NGAY_HT,SO_CT_NH=:so_ref_no   where so_ctu= :so_CT and id_ctu =:id_ctu ");
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[6];
                p[0] = new OracleParameter();
                p[0].ParameterName = "TRANG_THAI";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = strTrang_Thai;

                p[1] = new OracleParameter();
                p[1].ParameterName = "TEN_TRANGTHAI";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = strTen_Trang_Thai;

                p[2] = new OracleParameter();
                p[2].ParameterName = "NGAY_HT";
                p[2].DbType = DbType.DateTime;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = DateTime.Now;

                p[3] = new OracleParameter();
                p[3].ParameterName = "so_ref_no";
                p[3].DbType = DbType.String;
                p[3].Direction = ParameterDirection.Input;
                p[3].Value = so_ref_no;

                p[4] = new OracleParameter();
                p[4].ParameterName = "so_CT";
                p[4].DbType = DbType.String;
                p[4].Direction = ParameterDirection.Input;
                p[4].Value = So_CT;

                p[5] = new OracleParameter();
                p[5].ParameterName = "id_ctu";
                p[5].DbType = DbType.String;
                p[5].Direction = ParameterDirection.Input;
                p[5].Value = id_ctu;
                DataAccess.ExecuteNonQuery(sbSqlUpDate.ToString(), CommandType.Text, p, transCT);
                transCT.Commit();
            }
            catch (Exception e)
            {
                transCT.Rollback();
                //  conn.Close();
                log.Error(e.Message + "-" + e.StackTrace);// LogDebug.WriteLog(e.Message, EventLogEntryType.Error);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
        private void UpDateRemarks(string So_CT, string id_ctu, string remarks, string ngay_kbac)
        {
            OracleConnection conn = null;
            IDbTransaction transCT = null;
            try
            {

                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                //Tao cau lenh Insert
                StringBuilder sbSqlUpDate = new StringBuilder("");
                sbSqlUpDate.Append("update tcs_ctu_ntdt_hdr set remarks = :remarks,ngay_nhang=sysdate  where so_ctu= :so_CT and id_ctu =:id_ctu ");
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[3];
                p[0] = new OracleParameter();
                p[0].ParameterName = "remarks";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = remarks;

                p[1] = new OracleParameter();
                p[1].ParameterName = "so_CT";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = So_CT;

                p[2] = new OracleParameter();
                p[2].ParameterName = "id_ctu";
                p[2].DbType = DbType.String;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = id_ctu;
                DataAccess.ExecuteNonQuery(sbSqlUpDate.ToString(), CommandType.Text, p, transCT);
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();

            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
        /// <summary>
        /// Check số tài khoản có đúng số tài khoản đã đăng ký không
        /// </summary>
        /// <param name="strTrang_Thai"></param>
        /// <returns></returns>
        private bool CheckTK(string strMST, string soTK)
        {
            // viết lại câu lệnh 
            string sotaikhoan_dk = "";
            DataSet ds = null;

            try
            {
                //Tao cau lenh Insert
                StringBuilder sbSqlSelect = new StringBuilder("");
                sbSqlSelect.Append("select a.so_tk_nh from  ntdt_map_mst_tknh a,TCS_DM_NNTDT b  WHERE a.nntdt_id= b.nntdt_id and  a.mst= :strMST and a.so_tk_nh=: SO_TK and b.trang_thai = '03'");
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[2];
                p[0] = new OracleParameter();
                p[0].ParameterName = "strMST";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = strMST;

                p[1] = new OracleParameter();
                p[1].ParameterName = "SO_TK";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = soTK;

                ds = DataAccess.ExecuteReturnDataSet(sbSqlSelect.ToString(), CommandType.Text, p);
                sotaikhoan_dk = ds.Tables[0].Rows[0][0].ToString();
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return false;
            }
        }
        private bool CheckTrungGNT(string strGNTNumber)
        {
            // viết lại câu lệnh 
            string sotaikhoan_dk = "";
            DataSet ds = null;

            try
            {
                //Tao cau lenh Insert
                StringBuilder sbSqlSelect = new StringBuilder("");
                sbSqlSelect.Append("select so_GNT from tcs_ctu_ntdt_hdr where  so_gnt = '" + strGNTNumber + "'");
                ds = DataAccess.ExecuteReturnDataSet(sbSqlSelect.ToString(), CommandType.Text);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                return false;
            }
        }
        #endregion
        #region "Xử lý phản hồi thông báo từ tct"
        public void XuLyPhaHoiCT_TCT(MSG msg)
        {
            string xmlIn = msg.content_mess;
            if (xmlIn.Length > 0)
            {
                try
                {
                    xmlIn = xmlIn.Replace("<DATA>", strXMLdefin2);
                    MSG06004 MSG03002 = new MSG06004();
                    MSG06004 objTemp = MSG03002.MSGToObject(xmlIn);
                    if (objTemp.BODY != null)
                    {
                        string ma_GD_PhanHoi = objTemp.BODY.ROW.GIAODICH.MA_GDICH;
                        string ma_PhanHoi = objTemp.BODY.ROW.GIAODICH.MESS_CODE;
                        string NoiDung_PhanHoi = objTemp.BODY.ROW.GIAODICH.MESS_CONTENT;
                        string ngay_PhanHoi = objTemp.BODY.ROW.GIAODICH.NGAY_GUI_GDICH;
                        string ma_ThamChieu = objTemp.BODY.ROW.GIAODICH.MA_GDICH_TCHIEU;
                        UpDateTTPhanHoi(ma_GD_PhanHoi, ma_PhanHoi, NoiDung_PhanHoi, ngay_PhanHoi, ma_ThamChieu);
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Error source: " + ex.Source + "\n" + "Error code: System error!" + "\n" + "Error message: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);

                }
            }
        }
        // update trạng thái
        private void UpDateTTPhanHoi(string strMa_GD_PhanHoi, string strMa_PhanHoi, string strND_PhanHoi, string ngay_phanhoi, string ma_gdich_ctu)
        {
            OracleConnection conn = null;
            IDbTransaction transCT = null;
            try
            {

                conn = DataAccess.GetConnection();
                transCT = conn.BeginTransaction();
                //Tao cau lenh Insert
                StringBuilder sbSqlUpDate = new StringBuilder("");
                sbSqlUpDate.Append("update tcs_ctu_ntdt_hdr set MA_GD_PHANHOI = :MA_GD_PHANHOI,MA_PHANHOI=:MA_PHANHOI ,ND_PHANHOI=:ND_PHANHOI,NGAY_PHANHOI=:NGAY_PHANHOI  where ma_gdich_ctu= :ma_gdich_ctu  ");
                IDbDataParameter[] p = null;
                p = new IDbDataParameter[5];
                p[0] = new OracleParameter();
                p[0].ParameterName = "MA_GD_PHANHOI";
                p[0].DbType = DbType.String;
                p[0].Direction = ParameterDirection.Input;
                p[0].Value = strMa_GD_PhanHoi;

                p[1] = new OracleParameter();
                p[1].ParameterName = "MA_PHANHOI";
                p[1].DbType = DbType.String;
                p[1].Direction = ParameterDirection.Input;
                p[1].Value = strMa_PhanHoi;

                p[2] = new OracleParameter();
                p[2].ParameterName = "ND_PHANHOI";
                p[2].DbType = DbType.String;
                p[2].Direction = ParameterDirection.Input;
                p[2].Value = strND_PhanHoi;

                p[3] = new OracleParameter();
                p[3].ParameterName = "NGAY_PHANHOI";
                p[3].DbType = DbType.String;
                p[3].Direction = ParameterDirection.Input;
                p[3].Value = ngay_phanhoi;

                p[4] = new OracleParameter();
                p[4].ParameterName = "ma_gdich_ctu";
                p[4].DbType = DbType.String;
                p[4].Direction = ParameterDirection.Input;
                p[4].Value = ma_gdich_ctu;

                DataAccess.ExecuteNonQuery(sbSqlUpDate.ToString(), CommandType.Text, p, transCT);
                transCT.Commit();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);//
                transCT.Rollback();
                //  conn.Close();
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        #endregion"Kêt thúc xử lý phản hồi"

        //Nhamlt_20180330
        private string V_SubString(String s, int lenght)
        {
            if (s != null && s.Length > lenght)
            {
                s = s.Substring(0, lenght);
            }
            return s;

        }
        private bool Check_NH_SHB(string strSHKB, string strMA_NH_TH)
        {
            DataSet ds = null;

            try
            {
                //Tao cau lenh Insert
                StringBuilder sbSqlSelect = new StringBuilder("");
                sbSqlSelect.Append("select * from tcs_dm_nh_giantiep_tructiep where shkb='" + strSHKB + "' and ma_giantiep ='" + strMA_NH_TH + "'");
                ds = DataAccess.ExecuteReturnDataSet(sbSqlSelect.ToString(), CommandType.Text);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);// LogDebug.WriteLog("Lỗi khi Check_NH_SHB: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                return false;
            }
        }
    }
}
