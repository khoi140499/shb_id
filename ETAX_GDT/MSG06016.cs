﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace GDT.MSG.MSG06016
{
    [Serializable]
    [XmlRootAttribute("DATA", Namespace = "http://www.cpandl.com", IsNullable = false)]
    public class MSG06016
    {
        public MSG06016()
        {
        }
        [XmlElement("HEADER")]
        public HEADER HEADER;
        [XmlElement("BODY")]
        public BODY BODY;
        //[XmlElement("SECURITY")]
        //public SECURITY SECURITY;

        public string MSGtoXML(MSG06016 p_MsgIn)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MSG06016));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, p_MsgIn);
            string strTemp = sw.ToString();
            return strTemp;

        }
        public MSG06016 MSGToObject(string p_XML)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GDT.MSG.MSG06016.MSG06016));

            //serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            //serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            XmlReader reader = XmlReader.Create(new StringReader(p_XML));

            GDT.MSG.MSG06016.MSG06016 LoadedObjTmp = (GDT.MSG.MSG06016.MSG06016)serializer.Deserialize(reader);
            return LoadedObjTmp;

        }
    }

    public class HEADER
    {
        public HEADER()
        {
        }
        public string VERSION { get; set; }
        public string SENDER_CODE { get; set; }
        public string SENDER_NAME { get; set; }
        public string RECEIVER_CODE { get; set; }
        public string RECEIVER_NAME { get; set; }
        public string TRAN_CODE { get; set; }
        public string MSG_ID { get; set; }

        public string MSG_REFID { get; set; }
        public string ID_LINK { get; set; }
        public string SEND_DATE { get; set; }
        public string ORIGINAL_CODE { get; set; }
        public string ORIGINAL_NAME { get; set; }
        public string ORIGINAL_DATE { get; set; }
        public string ERROR_CODE { get; set; }

        public string ERROR_DESC { get; set; }
        public string SPARE1 { get; set; }
        public string SPARE2 { get; set; }
        public string SPARE3 { get; set; }
    }

    public class BODY
    {
        public BODY()
        {
        }
        [XmlElement("ROW")]
        public ROW ROW;
    }

    public class ROW
    {
        public ROW()
        { }
        [XmlElement("GIAODICH")]
        public GIAODICH GIAODICH;
    }

    public class GIAODICH
    {
        public GIAODICH()
        { }
        public string MA_GDICH { get; set; }
        public string NGAY_GUI_GDICH { get; set; }
        public string MA_GDICH_TCHIEU { get; set; }
        [XmlElement("TBAO_NGUNG")]
        public TBAO_NGUNG TBAO_NGUNG;
    }

    public class TBAO_NGUNG
    {
        public TBAO_NGUNG()
        { }
        [XmlElement("TBAO_NGUNG_NT")]
        public TBAO_NGUNG_NT TBAO_NGUNG_NT;
        public List<string> Signature { get; set; }
    }

    public class TBAO_NGUNG_NT
    {
        public TBAO_NGUNG_NT()
        { }
        public string PBAN_TLIEU_XML { get; set; }
        public string MA_TBAO { get; set; }
        public string MAU_TBAO { get; set; }
        public string SO_TBAO { get; set; }
        public string TEN_TBAO { get; set; }
        public string NGAY_TBAO { get; set; }
        public string MST { get; set; }
        public string TEN_NNT { get; set; }
        public string MA_CQT { get; set; }
        public string TEN_CQT { get; set; }
        public string TGIAN_NGUNG { get; set; }
        public string LYDO_NGUNG { get; set; }
        public string VAN_ID { get; set; }
        public string TEN_TVAN { get; set; }
        public string MA_NHANG { get; set; }
        public string TEN_NHANG { get; set; }

    }

    //public class SECURITY
    //{
    //    public SECURITY() 
    //    { 
    //    }
    //    public string SIGNATURE { get; set; }
    //}
}
