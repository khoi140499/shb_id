﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Configuration;
using VBOracleLib;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Collections;
using System.Diagnostics;
namespace CorebankService
{
    public class ClsCoreBank
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //int TimeOut = 60000;
        string ServerIP = ConfigurationManager.AppSettings.Get("IP.CORE").ToString();
        int ServerPort = int.Parse(ConfigurationManager.AppSettings.Get("PORT.CORE").ToString());
        int CORE_SEND_TIMEOUT = int.Parse(ConfigurationManager.AppSettings.Get("CORE.SENDTIMEOUT").ToString());
        int CORE_RECIVE_TIMEOUT = int.Parse(ConfigurationManager.AppSettings.Get("CORE.RECIVETIMEOUT").ToString());
        string CHANNELID = ConfigurationManager.AppSettings.Get("CHANEL.CORE").ToString();
        string INTERFACEID = ConfigurationManager.AppSettings.Get("INTERFACE.CORE").ToString();
        string CHANNELID_HQ = ConfigurationManager.AppSettings.Get("CHANEL.CORE.HQ").ToString();
        string INTERFACEID_HQ = ConfigurationManager.AppSettings.Get("INTERFACE.CORE.HQ").ToString();
        string CORE_RPSF = ConfigurationManager.AppSettings.Get("CORE.LPSF").ToString();
        private static String Core_status = ConfigurationManager.AppSettings.Get("CORE.ON").ToString();
        string CHANNEL_FEE = ConfigurationManager.AppSettings.Get("CHANEL.CORE.FEE").ToString();
        string INTERFACE_FEE = ConfigurationManager.AppSettings.Get("INTERFACE.CORE.FEE").ToString();
        string Br_Code = "BR0001";
        string prefixSEQ = "TAX";
        string TT_THANHCONG = "00000";
        private static String strConnection = ConfigurationManager.AppSettings.Get("CTC_DB_CONN").ToString();





        //private static String strConnection = VBOracleLib.Security.DescryptStr(strConnection1);
        public string GetTK_KH_NH(string acountNo, ref string MA_LOI, ref string SO_DU_KD)
        {
            string result = "";
            string strInput = "";
            string strOutput = "";
            string spliter = "~*";
            string node = "";
            string nodevale = "";
            //1
            node = "hdr_Tran_Id";
            nodevale = "ACCQRY";
            strInput += node + "=" + nodevale + spliter;
            //2
            node = "hdr_Status";
            nodevale = "NULL";
            strInput += node + "=" + nodevale + spliter;
            //3
            node = "req_Chnl_Id";
            nodevale = CHANNELID;
            strInput += node + "=" + nodevale + spliter;
            //4
            node = "req_Txn_Dt";
            nodevale = DateTime.Now.ToString("yyyyMMdd");
            strInput += node + "=" + nodevale + spliter;
            //5
            node = "req_Interface_Id";
            nodevale = INTERFACEID;
            strInput += node + "=" + nodevale + spliter;


            //6
            node = "req_Ref_No";
            nodevale = "0";
            strInput += node + "=" + nodevale + spliter;
            //7
            node = "req_Echo_Field";
            nodevale = "392";
            strInput += node + "=" + nodevale + spliter;
            //8
            node = "req_Acc_No";
            nodevale = acountNo;
            strInput += node + "=" + nodevale + spliter;
            //9

            node = "req_Remarks";
            nodevale = "TESTSHBTRANSACTION";
            strInput += node + "=" + nodevale + spliter;
            //10
            node = "req_Lvl_Flg";
            nodevale = "1";
            strInput += node + "=" + nodevale + spliter;

            string strReturn = "";
            if (Core_status == "1")
            {
                strOutput = sendMsg(ServerIP, ServerPort, strInput);
            }
            else
            {
                SO_DU_KD = "1000000000";
                MA_LOI = "00000";
                strReturn = ("<PARAMETER><PARAMS><BANKACCOUNT>"
                            + ("0100100011" + ("</BANKACCOUNT>" + ("<ACY_AVL_BAL>"
                            + ("1000000000" + ("</ACY_AVL_BAL><CRR_CY_CODE>"
                            + ("VND" + ("</CRR_CY_CODE>" + ("<RETCODE>"
                            + ("00000" + ("</RETCODE><ERRCODE>"
                            + ("00000" + "</ERRCODE></PARAMS></PARAMETER>"))))))))))));
                return strReturn;
            }


            result = strOutput;

            string res_Result_Code = "";
            string res_Echo_Field = "";
            string res_Ref_No = "";
            string res_Acc_No = "";
            string res_Acc_CCY_Cd = "";
            string res_Curr_Bal = "";
            string res_Avail_Bal = "";
            string res_Fin_Status = "";
            string res_Legal_Status = "";
            string res_AcInact_Status = "";
            string res_Contigent_Flg = "";
            string res_Blockamt = "";
            string res_Casa_Acc_Pos_Cd_Desc = "";
            string res_Acc_CCY = "";
            string req_Exists_Flg = "";




            char charsplit = '*';
            result = result.Replace("~", "");
            try
            {
                string[] arrParams = result.Split(charsplit);
                for (int i_arr = 0; (i_arr
                            <= (arrParams.Length - 1)); i_arr++)
                {
                    if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Acc_CCY_Cd"))
                    {
                        if (!(arrParams[i_arr].ToString().Split('=')[1].ToString() == "VND"))
                        {
                            return ("<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><" +
                            "RETCODE>999</RETCODE><ERRCODE>CURENTCY NOT AVAILABLE</ERRCODE></PARAMS></PARAMETER>");
                        }
                    }
                    if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Result_Code"))
                    {
                        if (!(arrParams[i_arr].ToString().Split('=')[1].ToString() == "00000"))
                        {
                            return ("<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><" +
                            "RETCODE>"
                                        + (arrParams[i_arr].ToString().Split('=')[1].ToString() + ("</RETCODE><ERRCODE>"
                                        + (arrParams[i_arr].ToString().Split('=')[1].ToString() + "</ERRCODE></PARAMS></PARAMETER>"))));
                        }
                    }
                }

                for (int i_arr = 0; (i_arr
                            <= (arrParams.Length - 1)); i_arr++)
                {
                    if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Result_Code"))
                    {
                        res_Result_Code = arrParams[i_arr].ToString().Split('=')[1].ToString();
                    }

                    if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Acc_No"))
                    {
                        res_Acc_No = arrParams[i_arr].ToString().Split('=')[1].ToString();
                    }

                    if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Avail_Bal"))
                    {
                        res_Avail_Bal = arrParams[i_arr].ToString().Split('=')[1].ToString();
                    }

                    if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Acc_CCY_Cd"))
                    {
                        res_Acc_CCY_Cd = arrParams[i_arr].ToString().Split('=')[1].ToString();
                    }

                    if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Result_Code"))
                    {
                        res_Result_Code = arrParams[i_arr].ToString().Split('=')[1].ToString();
                    }
                }


                //res_Result_Code = arrParams[3].ToString().Split('=')[1].ToString();
                //res_Echo_Field = arrParams[1].ToString().Split('=')[1].ToString();
                //res_Ref_No = arrParams[2].ToString().Split('=')[1].ToString();
                //res_Acc_No = arrParams[3].ToString().Split('=')[1].ToString();
                //res_Acc_CCY_Cd = arrParams[4].ToString().Split('=')[1].ToString();
                //res_Curr_Bal = arrParams[5].ToString().Split('=')[1].ToString();
                //res_Avail_Bal = arrParams[6].ToString().Split('=')[1].ToString();
                //res_Fin_Status = arrParams[7].ToString().Split('=')[1].ToString();
                //res_Legal_Status = arrParams[8].ToString().Split('=')[1].ToString();
                //res_AcInact_Status = arrParams[9].ToString().Split('=')[1].ToString();
                //res_Contigent_Flg = arrParams[10].ToString().Split('=')[1].ToString();
                //res_Blockamt = arrParams[11].ToString().Split('=')[1].ToString();
                //res_Casa_Acc_Pos_Cd_Desc = arrParams[12].ToString().Split('=')[1].ToString();
                //res_Acc_CCY = arrParams[13].ToString().Split('=')[1].ToString();
                //req_Exists_Flg = arrParams[14].ToString().Split('=')[1].ToString();
                MA_LOI = res_Result_Code;
                if (res_Result_Code == TT_THANHCONG)
                {
                    SO_DU_KD = res_Avail_Bal;
                    strReturn = ("<PARAMETER><PARAMS><BANKACCOUNT>"
                                + (res_Acc_No + ("</BANKACCOUNT>" + ("<ACY_AVL_BAL>"
                                + (res_Avail_Bal + ("</ACY_AVL_BAL><CRR_CY_CODE>"
                                + (res_Acc_CCY_Cd + ("</CRR_CY_CODE>" + ("<RETCODE>"
                                + (res_Result_Code + ("</RETCODE><ERRCODE>"
                                + (res_Result_Code + "</ERRCODE></PARAMS></PARAMETER>"))))))))))));
                }
            }
            catch (Exception ex)
            {
                return ("<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><" +
                "RETCODE>99</RETCODE><ERRCODE>"
                            + (ex.Message.ToString() + "</ERRCODE></PARAMS></PARAMETER>"));
            }

            return strReturn;
        }
        public static Int64 TCS_GetSequence(string p_Seq)
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT ");
                sbSQL.Append(p_Seq);
                sbSQL.Append(".nextval FROM DUAL");
                String strSQL = sbSQL.ToString();
                String strRet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
                return Int64.Parse(strRet);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        //Payment Thue Tai Quay
        public string PaymentTQ(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
            string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
            string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
            string v_str_KS, string v_KenhCT, string v_CustInfo, ref  string v_str_errcode, ref  string v_str_ref_no)
        {
            string result = "";
            string strInput = "";
            string strOutput = "";
            string spliter = "~*";
            string node = "";
            string nodevale = "";
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = KenhHT + TCS_GetSequence("TCS_REF_NO_SEQ");

            


            //1
            node = "hdr_Tran_Id";
            nodevale = "LPSOUTPLAIN";
            strInput += node + "=" + nodevale + spliter;
            //2
            node = "hdr_Status";
            nodevale = "NULL";
            strInput += node + "=" + nodevale + spliter;
            //3 mã chương trình fixcode
            node = "req_Br_Cd";
            nodevale = Br_Code;
            strInput += node + "=" + nodevale + spliter;
            //4 kênh hạch toán
            node = "req_Chnl_Id";
            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = CHANNELID_HQ;
            }
            else {
                nodevale = CHANNELID;
            }
            
            strInput += node + "=" + nodevale + spliter;
            ////5 ngày hạch toán
            //node = "req_Txn_Dt";
            //nodevale = DateTime.Now.ToString("yyyyMMdd"); 
            //strInput += node + "=" + nodevale + spliter;
            //5 Kênh hạch toán
            node = "req_Interface_Id";
            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = INTERFACEID_HQ;
            }
            else
            {
                nodevale = INTERFACEID;
            }
            
            strInput += node + "=" + nodevale + spliter;
            //6 Số sequence duy nhất trong hệ thống
            node = "req_Ref_No";
            nodevale = SEQUENCE_NO;
            strInput += node + "=" + nodevale + spliter;
            //7 req_Acc_No: tài khoản khách hàng
            node = "req_Acc_No";
            nodevale = pv_ACCOUNTNO.Trim();
            strInput += node + "=" + nodevale + spliter;
            //8 Tài khoản kho bạc 7111
            node = "req_Credit_Acc_No";
            nodevale = pv_BEN_ACCOUNTNO;
            strInput += node + "=" + nodevale + spliter;
            //9 req_Amt số tiền thuế
            node = "req_Amt";
            nodevale = AMOUNT;
            strInput += node + "=" + nodevale + spliter;
            //10 req_Txn_Ccy: loại tiền hạch toán
            node = "req_Txn_Ccy";
            nodevale = CCY;
            strInput += node + "=" + nodevale + spliter;
            //11 req_Txn_Dt: ngày hạch toán= lấy bằng ngày làm việc
            node = "req_Txn_Dt";
            nodevale = TransactionDate;
            strInput += node + "=" + nodevale + spliter;
            //12 req_Val_Dt: Ngày value date
            node = "req_Val_Dt";
            nodevale = VALUEDATE;
            strInput += node + "=" + nodevale + spliter;
            //13 req_WIB:  không hiểu là gì
            node = "req_WIB";
            nodevale = "N";
            strInput += node + "=" + nodevale + spliter;
            //14 req_Ben_Name: Tên người thụ hưởng
            node = "req_Ben_Name";
            nodevale = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME);
            strInput += node + "=" + nodevale + spliter;
            //15 req_Ben_Add1: Địa chỉ người thụ hưởng
            //node = "req_Ben_Add1";
            //nodevale = " ";
            //strInput += node + "=" + nodevale + spliter;
            //16 req_Ben_Add2: Địa chỉ người thụ hưởng
            //node = "req_Ben_Add2";
            //nodevale = " ";
            //strInput += node + "=" + nodevale + spliter;
            //17 req_Ben_City: Thành phố người thụ hưởng
            node = "req_Ben_City";
            nodevale = BEN_CITY;
            strInput += node + "=" + nodevale + spliter;
            //18 req_Id_Type: Không biết để rỗng
            node = "req_Id_Type";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //19 req_Id_No: Không biết để rỗng
            node = "req_Id_No";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //20 req_IdIssuePlace: =""
            node = "req_IdIssuePlace";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //21 req_Id_Exp_Dt=""
            node = "req_Id_Exp_Dt";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //22 req_Cust_Rmk: remark của khách hàng
            node = "req_Cust_Rmk";
            nodevale = VBOracleLib.Globals.RemoveSign4VietnameseString(REMARK);
            strInput += node + "=" + nodevale + spliter;
            //23 req_Internal_Rmk: remark của Ngân hàng
            node = "req_Internal_Rmk";
            nodevale = v_CustInfo;
            strInput += node + "=" + nodevale + spliter;
            //24 req_Bnk_Desc: Tên ngân hàng thụ hưởng: ngân hàng nông nghiệp
            node = "req_Bnk_Desc";
            nodevale = BANK_BEN_DESC;
            strInput += node + "=" + nodevale + spliter;
            //25 req_Br_Desc: Chi nhánh ngân hàng thụ hưởng: chi nhanh hà nội
            node = "req_Br_Desc";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //26 req_Cty_Desc: Tên thành phố của ngân hàng thụ hưởng' Hà nội
            node = "req_Cty_Desc";
            nodevale = VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC);
            strInput += node + "=" + nodevale + spliter;
            //27 req_Cty_Code: remark của khách hàng 27+28+29= mã ngân hàng thụ hưởng
            node = "req_Cty_Code";
            nodevale = CITY_BEN_CODE;
            strInput += node + "=" + nodevale + spliter;
            //28 req_Bank_Code: remark của khách hàng
            node = "req_Bank_Code";
            nodevale = BANK_BEN_CODE;
            strInput += node + "=" + nodevale + spliter;
            //29 req_Br_Code: mã chi nhánh
            node = "req_Br_Code";
            nodevale = BR_BEN_CODE;
            strInput += node + "=" + nodevale + spliter;
            //30 req_Deal_Type: =?
            node = "req_Deal_Type";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //31 req_Chrg_type=?
            node = "req_Chrg_type";
            nodevale = "E";
            strInput += node + "=" + nodevale + spliter;
            ////32 req_Conv_Rrate=?
            //node = "req_Cust_Rmk";
            //nodevale = CHANNELID;
            //strInput += node + "=" + nodevale + spliter;
            ////33 req_Echo_Field=?
            //node = "req_Echo_Field";
            //nodevale = CHANNELID;
            //strInput += node + "=" + nodevale + spliter;
            char charsplit = '*';
            string hdr_Tran_Id = "";
            string res_Status = "";
            string res_Result_Code = "";
            string res_Ref_No = "";
            string res_Avail_Bal = "";
            string res_Curr_Bal = "";
            string res_Acc_CCY_Cd = "";
            string res_Lcy_Amt = "";
            string err_code = "";
            string rm_ref_no = "";
            string so_du = "";
            string err_msg = "";
            strInput = Globals.RemoveSign4VietnameseString(strInput);
            if (Core_status == "1")
            {
                strOutput = sendMsg_Payment(ServerIP, ServerPort, strInput);
                result = strOutput;
            }




            try
            {
                if (Core_status == "1")
                {
                    if (result == "TO999")
                    {
                        res_Result_Code = "00000";
                        res_Ref_No = CORE_RPSF;
                        res_Status = "00000";
                        v_str_errcode = "TO999";
                    }
                    else
                    {
                        string[] arrParams = result.Replace("~", "").Split(charsplit);
                        for (int i_arr = 0; (i_arr
                                    <= (arrParams.Length - 1)); i_arr++)
                        {
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Result_Code"))
                            {
                                if (!(arrParams[i_arr].ToString().Split('=')[1].ToString() == "00000"))
                                {
                                    err_code = arrParams[i_arr].ToString().Split('=')[1].ToString();
                                    return err_code;
                                }
                            }
                        }

                        for (int i_arr = 0; (i_arr
                                    <= (arrParams.Length - 1)); i_arr++)
                        {
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "hdr_Tran_Id"))
                            {
                                hdr_Tran_Id = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Status"))
                            {
                                res_Status = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Result_Code"))
                            {
                                res_Result_Code = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Ref_No"))
                            {
                                res_Ref_No = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Avail_Bal"))
                            {
                                res_Avail_Bal = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Curr_Bal"))
                            {
                                res_Curr_Bal = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Acc_CCY_Cd"))
                            {
                                res_Acc_CCY_Cd = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Lcy_Amt"))
                            {
                                res_Lcy_Amt = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                        }
                    }

                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }




                //hdr_Tran_Id = arrParams[0].ToString().Split('=')[1].ToString();
                //res_Status = arrParams[1].ToString().Split('=')[1].ToString();
                //res_Result_Code = arrParams[2].ToString().Split('=')[1].ToString();
                //res_Ref_No = arrParams[3].ToString().Split('=')[1].ToString();
                //res_Avail_Bal = arrParams[4].ToString().Split('=')[1].ToString();
                //res_Curr_Bal = arrParams[5].ToString().Split('=')[1].ToString();
                //res_Acc_CCY_Cd = arrParams[6].ToString().Split('=')[1].ToString();
                //res_Lcy_Amt = arrParams[7].ToString().Split('=')[1].ToString();
                rm_ref_no = res_Ref_No;
                so_du = res_Curr_Bal;
                if (result == "TO999")
                {
                    v_str_errcode = result;
                }
                else
                {
                    v_str_errcode = res_Result_Code;
                }

                string v_strSQLInsert;
                if (res_Status == "00000")
                {
                    v_str_ref_no = rm_ref_no;
                    ////{// nếu thành công update lại số refno
                    ////    v_str_ref_no = rm_ref_no;
                    ////    v_strSQLInsert = ((" UPDATE  TCS_CTU_HDR SET SO_CT_NH=\'"
                    ////+ (rm_ref_no + ("\' WHERE SO_CT=\'"
                    ////+ (v_strSo_CT + "\' and tk_kh_nh=\'"))))
                    ////+ (pv_ACCOUNTNO + "\'"));
                    ////    VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                    //}
                    v_strSQLInsert = ("INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," + (" CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SE" +
                    "LL_RATE," + (" AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO,USER_INPUT,KENH_CT,USE" +
                    "R_CHECKER) " + (" VALUES(\'TA001\',\'\',\'1\',SYSDATE,\'\',\'\',"
                                + ((" \'\',\'\',\'"
                                + (BANK_BEN_CODE + ("\',\'"
                                + (pv_ACCOUNTNO + ("\',\'\',\'VND\',\'10000000\',\'"
                                + (pv_BEN_ACCOUNTNO + ("\',\'"
                                + (strMa_NH_B + "\',\'10000000\',")))))))) + (" "
                                + (AMOUNT + (", 0 ,0 ,\'"
                                + (REMARK + ("\',\'"
                                + (res_Result_Code + ("\',\'"
                                + (rm_ref_no + ("\',\'\',\'\',\'"
                                + (v_strSo_CT + ("\',\'"
                                + (v_strMa_NV + ("\',\'"
                                + (v_KenhCT + ("\',\'"
                                + (v_str_KS + "\')")))))))))))))))))))));
                    VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                }
                else {
                    v_str_errcode = res_Status;
                    err_msg = res_Ref_No;
                }
                
            }
            catch (Exception ex)
            {
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }

            return result;
        }

        //Payment Thue Dien Tu
        public string Payment(string pv_ACCOUNTNO, string pv_BEN_ACCOUNTNO, string AMOUNT, string CCY, string REMARK, string TransactionDate,
            string BEN_NAME, string BEN_CITY, string BEN_ADD1, string BEN_ADD2, string BANK_BEN_DESC, string BR_BEN_DESC, string CITY_BEN_DESC,
            string BANK_BEN_CODE, string BR_BEN_CODE, string CITY_BEN_CODE, string KenhHT, string v_strSo_CT, string strMa_NH_B, string v_strMa_NV,
            string v_str_KS, string v_KenhCT, string customer_info, ref  string v_str_errcode, ref  string v_str_ref_no)
        {
            string result = "";
            string strInput = "";
            string strOutput = "";
            string spliter = "~*";
            string node = "";
            string nodevale = "";
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = KenhHT + TCS_GetSequence("TCS_REF_NO_SEQ");




            //1
            node = "hdr_Tran_Id";
            nodevale = "LPSOUTPLAIN";
            strInput += node + "=" + nodevale + spliter;
            //2
            node = "hdr_Status";
            nodevale = "NULL";
            strInput += node + "=" + nodevale + spliter;
            //3 mã chương trình fixcode
            node = "req_Br_Cd";
            nodevale = Br_Code;
            strInput += node + "=" + nodevale + spliter;
            //4 kênh hạch toán
            node = "req_Chnl_Id";
            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = CHANNELID_HQ;
            }
            else
            {
                nodevale = CHANNELID;
            }

            strInput += node + "=" + nodevale + spliter;
            ////5 ngày hạch toán
            //node = "req_Txn_Dt";
            //nodevale = DateTime.Now.ToString("yyyyMMdd"); 
            //strInput += node + "=" + nodevale + spliter;
            //5 Kênh hạch toán
            node = "req_Interface_Id";
            if (v_KenhCT.Equals("ETAX_HQ"))
            {
                nodevale = INTERFACEID_HQ;
            }
            else
            {
                nodevale = INTERFACEID;
            }

            strInput += node + "=" + nodevale + spliter;
            //6 Số sequence duy nhất trong hệ thống
            node = "req_Ref_No";
            nodevale = SEQUENCE_NO;
            strInput += node + "=" + nodevale + spliter;
            //7 req_Acc_No: tài khoản khách hàng
            node = "req_Acc_No";
            nodevale = pv_ACCOUNTNO.Trim();
            strInput += node + "=" + nodevale + spliter;
            //8 Tài khoản kho bạc 7111
            node = "req_Credit_Acc_No";
            nodevale = pv_BEN_ACCOUNTNO;
            strInput += node + "=" + nodevale + spliter;
            //9 req_Amt số tiền thuế
            node = "req_Amt";
            nodevale = AMOUNT;
            strInput += node + "=" + nodevale + spliter;
            //10 req_Txn_Ccy: loại tiền hạch toán
            node = "req_Txn_Ccy";
            nodevale = CCY;
            strInput += node + "=" + nodevale + spliter;
            //11 req_Txn_Dt: ngày hạch toán= lấy bằng ngày làm việc
            node = "req_Txn_Dt";
            nodevale = TransactionDate;
            strInput += node + "=" + nodevale + spliter;
            //12 req_Val_Dt: Ngày value date
            node = "req_Val_Dt";
            nodevale = VALUEDATE;
            strInput += node + "=" + nodevale + spliter;
            //13 req_WIB:  không hiểu là gì
            node = "req_WIB";
            nodevale = "N";
            strInput += node + "=" + nodevale + spliter;
            //14 req_Ben_Name: Tên người thụ hưởng
            node = "req_Ben_Name";
            nodevale = VBOracleLib.Globals.RemoveSign4VietnameseString(BEN_NAME);
            strInput += node + "=" + nodevale + spliter;
            //15 req_Ben_Add1: Địa chỉ người thụ hưởng
            //node = "req_Ben_Add1";
            //nodevale = " ";
            //strInput += node + "=" + nodevale + spliter;
            //16 req_Ben_Add2: Địa chỉ người thụ hưởng
            //node = "req_Ben_Add2";
            //nodevale = " ";
            //strInput += node + "=" + nodevale + spliter;
            //17 req_Ben_City: Thành phố người thụ hưởng
            node = "req_Ben_City";
            nodevale = BEN_CITY;
            strInput += node + "=" + nodevale + spliter;
            //18 req_Id_Type: Không biết để rỗng
            node = "req_Id_Type";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //19 req_Id_No: Không biết để rỗng
            node = "req_Id_No";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //20 req_IdIssuePlace: =""
            node = "req_IdIssuePlace";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //21 req_Id_Exp_Dt=""
            node = "req_Id_Exp_Dt";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //22 req_Cust_Rmk: remark của khách hàng
            node = "req_Cust_Rmk";
            nodevale = VBOracleLib.Globals.RemoveSign4VietnameseString(REMARK);
            strInput += node + "=" + nodevale + spliter;
            //23 req_Internal_Rmk: remark của Ngân hàng
            node = "req_Internal_Rmk";
            nodevale = customer_info; //+spliter;
            strInput += node + "=" + nodevale + spliter;
            //24 req_Bnk_Desc: Tên ngân hàng thụ hưởng: ngân hàng nông nghiệp
            node = "req_Bnk_Desc";
            nodevale = BANK_BEN_DESC;
            strInput += node + "=" + nodevale + spliter;
            //25 req_Br_Desc: Chi nhánh ngân hàng thụ hưởng: chi nhanh hà nội
            node = "req_Br_Desc";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //26 req_Cty_Desc: Tên thành phố của ngân hàng thụ hưởng' Hà nội
            node = "req_Cty_Desc";
            nodevale = VBOracleLib.Globals.RemoveSign4VietnameseString(CITY_BEN_DESC);
            strInput += node + "=" + nodevale + spliter;
            //27 req_Cty_Code: remark của khách hàng 27+28+29= mã ngân hàng thụ hưởng
            node = "req_Cty_Code";
            nodevale = CITY_BEN_CODE;
            strInput += node + "=" + nodevale + spliter;
            //28 req_Bank_Code: remark của khách hàng
            node = "req_Bank_Code";
            nodevale = BANK_BEN_CODE;
            strInput += node + "=" + nodevale + spliter;
            //29 req_Br_Code: mã chi nhánh
            node = "req_Br_Code";
            nodevale = BR_BEN_CODE;
            strInput += node + "=" + nodevale + spliter;
            //30 req_Deal_Type: =?
            node = "req_Deal_Type";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //31 req_Chrg_type=?
            node = "req_Chrg_type";
            nodevale = "E";
            strInput += node + "=" + nodevale + spliter;
            ////32 req_Conv_Rrate=?
            //node = "req_Cust_Rmk";
            //nodevale = CHANNELID;
            //strInput += node + "=" + nodevale + spliter;
            ////33 req_Echo_Field=?
            //node = "req_Echo_Field";
            //nodevale = CHANNELID;
            //strInput += node + "=" + nodevale + spliter;
            char charsplit = '*';
            string hdr_Tran_Id = "";
            string res_Status = "";
            string res_Result_Code = "";
            string res_Ref_No = "";
            string res_Avail_Bal = "";
            string res_Curr_Bal = "";
            string res_Acc_CCY_Cd = "";
            string res_Lcy_Amt = "";
            string err_code = "";
            string rm_ref_no = "";
            string so_du = "";
            string err_msg = "";
            strInput = Globals.RemoveSign4VietnameseString(strInput);
            if (Core_status == "1")
            {
                strOutput = sendMsg_Payment(ServerIP, ServerPort, strInput);
                result = strOutput;
            }




            try
            {
                if (Core_status == "1")
                {
                    if (result == "TO999")
                    {
                        res_Result_Code = "00000";
                        res_Ref_No = CORE_RPSF;
                        res_Status = "00000";
                        v_str_errcode = "TO999";
                    }
                    else
                    {
                        string[] arrParams = result.Replace("~", "").Split(charsplit);
                        for (int i_arr = 0; (i_arr
                                    <= (arrParams.Length - 1)); i_arr++)
                        {
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Result_Code"))
                            {
                                if (!(arrParams[i_arr].ToString().Split('=')[1].ToString() == "00000"))
                                {
                                    err_code = arrParams[i_arr].ToString().Split('=')[1].ToString();
                                    return err_code;
                                }
                            }
                        }

                        for (int i_arr = 0; (i_arr
                                    <= (arrParams.Length - 1)); i_arr++)
                        {
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "hdr_Tran_Id"))
                            {
                                hdr_Tran_Id = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Status"))
                            {
                                res_Status = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Result_Code"))
                            {
                                res_Result_Code = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Ref_No"))
                            {
                                res_Ref_No = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Avail_Bal"))
                            {
                                res_Avail_Bal = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Curr_Bal"))
                            {
                                res_Curr_Bal = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Acc_CCY_Cd"))
                            {
                                res_Acc_CCY_Cd = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Lcy_Amt"))
                            {
                                res_Lcy_Amt = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                        }
                    }

                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }




                //hdr_Tran_Id = arrParams[0].ToString().Split('=')[1].ToString();
                //res_Status = arrParams[1].ToString().Split('=')[1].ToString();
                //res_Result_Code = arrParams[2].ToString().Split('=')[1].ToString();
                //res_Ref_No = arrParams[3].ToString().Split('=')[1].ToString();
                //res_Avail_Bal = arrParams[4].ToString().Split('=')[1].ToString();
                //res_Curr_Bal = arrParams[5].ToString().Split('=')[1].ToString();
                //res_Acc_CCY_Cd = arrParams[6].ToString().Split('=')[1].ToString();
                //res_Lcy_Amt = arrParams[7].ToString().Split('=')[1].ToString();
                rm_ref_no = res_Ref_No;
                so_du = res_Curr_Bal;
                if (result == "TO999")
                {
                    v_str_errcode = result;
                }
                else
                {
                    v_str_errcode = res_Result_Code;
                }

                string v_strSQLInsert;
                if (res_Status == "00000")
                {
                    v_str_ref_no = rm_ref_no;
                    ////{// nếu thành công update lại số refno
                    ////    v_str_ref_no = rm_ref_no;
                    ////    v_strSQLInsert = ((" UPDATE  TCS_CTU_HDR SET SO_CT_NH=\'"
                    ////+ (rm_ref_no + ("\' WHERE SO_CT=\'"
                    ////+ (v_strSo_CT + "\' and tk_kh_nh=\'"))))
                    ////+ (pv_ACCOUNTNO + "\'"));
                    ////    VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                    //}
                    v_strSQLInsert = ("INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," + (" CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SE" +
                    "LL_RATE," + (" AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO,USER_INPUT,KENH_CT,USE" +
                    "R_CHECKER) " + (" VALUES(\'TA001\',\'\',\'1\',SYSDATE,\'\',\'\',"
                                + ((" \'\',\'\',\'"
                                + (BANK_BEN_CODE + ("\',\'"
                                + (pv_ACCOUNTNO + ("\',\'\',\'VND\',\'10000000\',\'"
                                + (pv_BEN_ACCOUNTNO + ("\',\'"
                                + (strMa_NH_B + "\',\'10000000\',")))))))) + (" "
                                + (AMOUNT + (", 0 ,0 ,\'"
                                + (REMARK + ("\',\'"
                                + (res_Result_Code + ("\',\'"
                                + (rm_ref_no + ("\',\'\',\'\',\'"
                                + (v_strSo_CT + ("\',\'"
                                + (v_strMa_NV + ("\',\'"
                                + (v_KenhCT + ("\',\'"
                                + (v_str_KS + "\')")))))))))))))))))))));
                    VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                }
                else
                {
                    v_str_errcode = res_Status;
                    err_msg = res_Ref_No;
                }

            }
            catch (Exception ex)
            {
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }

            return result;
        }

        //Payment Phí bộ ngành
        /// <summary>
        /// Biennt - 21/12/2016
        /// Thanh toán dùng cho phí bộ ngành
        /// </summary>
        /// <param name="pv_ACCOUNTNO"></param>
        /// <param name="pv_BEN_ACCOUNTNO"></param>
        /// <param name="AMOUNT"></param>
        /// <param name="CCY"></param>
        /// <param name="REMARK"></param>
        /// <param name="TransactionDate"></param>
        /// <param name="BEN_NAME"></param>
        /// <param name="BEN_CITY"></param>
        /// <param name="BEN_ADD1"></param>
        /// <param name="BEN_ADD2"></param>
        /// <param name="BANK_BEN_DESC"></param>
        /// <param name="BR_BEN_DESC"></param>
        /// <param name="CITY_BEN_DESC"></param>
        /// <param name="BANK_BEN_CODE"></param>
        /// <param name="BR_BEN_CODE"></param>
        /// <param name="CITY_BEN_CODE"></param>
        /// <param name="KenhHT"></param>
        /// <param name="v_strSo_CT"></param>
        /// <param name="strMa_NH_B"></param>
        /// <param name="v_strMa_NV"></param>
        /// <param name="v_str_KS"></param>
        /// <param name="v_KenhCT"></param>
        /// <param name="v_CustInfo"></param>
        /// <param name="v_str_errcode"></param>
        /// <param name="v_str_ref_no"></param>
        /// <returns></returns>
        public string PaymentPBN(string v_KenhCT,string strMA_CN, string strREF_NO, string strSalary, string strBalancer, string strFEF_SEQ, string strSEQ_NO,
            string FINANCIAL, string strACC_NO, string strCredit, string strTRANSACTIONAMOUNT, string strTRANSACTIONCURRENCY, string strREMARKS, ref  string v_str_errcode, ref  string v_str_ref_no)
        {
            string result = "";
            string strInput = "";
            string strOutput = "";
            string spliter = "~*";
            string node = "";
            string nodevale = "";
            string VALUEDATE = Get_VALUE_DATE();
            string SEQUENCE_NO = v_KenhCT + TCS_GetSequence("TCS_REF_NO_SEQ");




            //1
            node = "hdr_Tran_Id";
            nodevale = "FINPOST";
            strInput += node + "=" + nodevale + spliter;
            //2
            node = "hdr_Status";
            nodevale = "NULL";
            strInput += node + "=" + nodevale + spliter;
            //3
            node = "req_Chnl_Id";
            if (v_KenhCT.Equals("FINPOST"))
            {
                nodevale = CHANNEL_FEE;
            }
            else
            {
                nodevale = INTERFACE_FEE;
            }

            strInput += node + "=" + nodevale + spliter;
            //4 
            node = "req_Txn_Dt";
            nodevale = DateTime.Now.ToString("YyyyMMddHHmmss"); 
            strInput += node + "=" + nodevale + spliter;
            //5 Kênh hạch toán
            node = "req_Interface_Id";
            if (v_KenhCT.Equals("FINPOST"))
            {
                nodevale = INTERFACE_FEE;
            }
            else
            {
                nodevale = CHANNEL_FEE;
            }
            strInput += node + "=" + nodevale + spliter;
            //6
            node = "req_Src_Br_Cd";
            nodevale = strMA_CN;
            strInput += node + "=" + nodevale + spliter;

            //7
            node = "req_Ref_No";
            nodevale = strREF_NO;
            strInput += node + "=" + nodevale + spliter;
            //8
            node = "req_Echo_Field";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //9
            node = "req_Posting_Flg";
            nodevale = strSalary;
            strInput += node + "=" + nodevale + spliter;
            //10
            node = "req_Txn_Bal_Flg";
            nodevale = strBalancer;
            strInput += node + "=" + nodevale + spliter;

            //11
            node = "req_Post_All_Seg_Flg";
            nodevale = strBalancer;
            strInput += node + "=" + nodevale + spliter;

            //12
            node = "req_Cr_Transit_Acc";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //13
            node = "req_Dr_Transit_Acc";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //14
            node = "req_No_Of_Seg";
            nodevale = "N";
            strInput += node + "=" + nodevale + spliter;
            //15
            node = "req_Seg_No";
            nodevale = strFEF_SEQ;
            strInput += node + "=" + nodevale + spliter;
            //16
            node = "req_Txn_Cd";
            nodevale = "FP";
            strInput += node + "=" + nodevale + spliter;
            //17
            node = "req_Freeze_Rel_Flg";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //18
            node = "req_Freeze_Cd";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //19
            node = "req_Acc_Br_Cd";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //20
            node = "req_Acc_No";
            nodevale = strCredit;
            strInput += node + "=" + nodevale + spliter;
            //21
            node = "req_Dr_Cr_Flg";
            nodevale = "C";
            strInput += node + "=" + nodevale + spliter;
            //22
            node = "req_Amt";
            nodevale = strTRANSACTIONAMOUNT;
            strInput += node + "=" + nodevale + spliter;
            //23
            node = "req_CCY";
            nodevale = strTRANSACTIONCURRENCY;
            strInput += node + "=" + nodevale + spliter;
            //24
            node = "req_Conv_Mode";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //25
            node = "req_Conv_Rate";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //26
            node = "req_Deal_Type";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //27
            node = "req_Int_Remarks";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //28
            node = "req_Ext_Remarks";
            nodevale = strREMARKS;
            strInput += node + "=" + nodevale + spliter;
            //29
            node = "req_Comp_Cd";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //30
            node = "req_Comp_Acc";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //31
            node = "req_Freeze_Flg";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //32
            node = "req_Freeze_Period";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //33
            node = "req_Freeze_Reason";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //34
            node = "req_Contra_Acc_No";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;
            //35
            node = "req_Seg_Ref_No";
            nodevale = "";
            strInput += node + "=" + nodevale + spliter;

            char charsplit = '*';
            string hdr_Tran_Id = "";
            string res_Status = "";
            string res_Result_Code = "";
            string res_Ref_No = "";
            string res_Avail_Bal = "";
            string res_Curr_Bal = "";
            string res_Acc_CCY_Cd = "";
            string res_Lcy_Amt = "";
            string err_code = "";
            string rm_ref_no = "";
            string so_du = "";
            string err_msg = "";
            strInput = Globals.RemoveSign4VietnameseString(strInput);
            if (Core_status == "1")
            {
                strOutput = sendMsg_Payment(ServerIP, ServerPort, strInput);
                result = strOutput;
            }




            try
            {
                if (Core_status == "1")
                {
                    if (result == "TO999")
                    {
                        res_Result_Code = "00000";
                        res_Ref_No = CORE_RPSF;
                        res_Status = "00000";
                        v_str_errcode = "TO999";
                    }
                    else
                    {
                        string[] arrParams = result.Replace("~", "").Split(charsplit);
                        for (int i_arr = 0; (i_arr
                                    <= (arrParams.Length - 1)); i_arr++)
                        {
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Result_Code"))
                            {
                                if (!(arrParams[i_arr].ToString().Split('=')[1].ToString() == "00000"))
                                {
                                    err_code = arrParams[i_arr].ToString().Split('=')[1].ToString();
                                    return err_code;
                                }
                            }
                        }

                        for (int i_arr = 0; (i_arr
                                    <= (arrParams.Length - 1)); i_arr++)
                        {
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "hdr_Tran_Id"))
                            {
                                hdr_Tran_Id = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Status"))
                            {
                                res_Status = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Result_Code"))
                            {
                                res_Result_Code = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Ref_No"))
                            {
                                res_Ref_No = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Avail_Bal"))
                            {
                                res_Avail_Bal = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Curr_Bal"))
                            {
                                res_Curr_Bal = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Acc_CCY_Cd"))
                            {
                                res_Acc_CCY_Cd = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                            if ((arrParams[i_arr].ToString().Split('=')[0].ToString() == "res_Lcy_Amt"))
                            {
                                res_Lcy_Amt = arrParams[i_arr].ToString().Split('=')[1].ToString();
                            }
                        }
                    }

                }
                else
                {
                    res_Ref_No = TCS_GetSequence("TCS_REF_NO_SEQ").ToString();
                    res_Curr_Bal = "1000000000";
                    res_Result_Code = "00000";
                    res_Status = "00000";
                }

                rm_ref_no = res_Ref_No;
                so_du = res_Curr_Bal;
                if (result == "TO999")
                {
                    v_str_errcode = result;
                }
                else
                {
                    v_str_errcode = res_Result_Code;
                }

                string v_strSQLInsert;
                if (res_Status == "00000")
                {
                    v_str_ref_no = rm_ref_no;

                    //v_strSQLInsert = ("INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," + (" CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SE" +
                    //"LL_RATE," + (" AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO,USER_INPUT,KENH_CT,USE" +
                    //"R_CHECKER) " + (" VALUES(\'TA001\',\'\',\'1\',SYSDATE,\'\',\'\',"
                    //            + ((" \'\',\'\',\'"
                    //            + (BANK_BEN_CODE + ("\',\'"
                    //            + (pv_ACCOUNTNO + ("\',\'\',\'VND\',\'10000000\',\'"
                    //            + (pv_BEN_ACCOUNTNO + ("\',\'"
                    //            + (strMa_NH_B + "\',\'10000000\',")))))))) + (" "
                    //            + (AMOUNT + (", 0 ,0 ,\'"
                    //            + (REMARK + ("\',\'"
                    //            + (res_Result_Code + ("\',\'"
                    //            + (rm_ref_no + ("\',\'\',\'\',\'"
                    //            + (v_strSo_CT + ("\',\'"
                    //            + (v_strMa_NV + ("\',\'"
                    //            + (v_KenhCT + ("\',\'"
                    //            + (v_str_KS + "\')")))))))))))))))))))));
                    //VBOracleLib.DataAccess.ExecuteNonQuery(v_strSQLInsert, System.Data.CommandType.Text);
                }
                else
                {
                    v_str_errcode = res_Status;
                    err_msg = res_Ref_No;
                }

            }
            catch (Exception ex)
            {
                v_str_errcode = res_Result_Code;
                err_msg = ex.Message.ToString();
            }

            return result;
        }

        public static string GetTen_TK_KH_NH(string pv_strMST)
        {
            string strRetrun = string.Empty;
          

                    string sbsql = "";
                    DataSet ds = null;
                    log.Info("0-Lay ten tai khoan connect: " + VBOracleLib.DataAccess.strConnection);
                    sbsql = "SELECT  legacy_ac, ccy_cd, ac_name, pos_cd,  STATUS FROM table (tcs_pck_util_shb.get_ups('" + pv_strMST + "','1','A'))";
                    log.Info("1-Lay ten tai khoan query: " + sbsql);
                    try
                    {
                        //objCon.Open();
                        if (Core_status == "1")
                        {
                            //objCom.ExecuteNonQuery();
                            ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sbsql.ToString(), CommandType.Text);
                            if (ds == null)
                            {
                                //VBOracleLib.LogDebug.WriteLog("2-Lay ten tai khoan ket qua tra ra rỗng ", EventLogEntryType.Error);
                            }
                            else
                            {
                                if (ds.Tables.Count < 0)
                                {
                                    //VBOracleLib.LogDebug.WriteLog("2-Lay ten tai khoan ket qua tra ra rỗng ", EventLogEntryType.Error);
                                }
                                else
                                {
                                    if (ds.Tables[0].Rows.Count  < 0)
                                    {
                                        //VBOracleLib.LogDebug.WriteLog("2-Lay ten tai khoan ket qua tra ra rỗng ", EventLogEntryType.Error);
                                    }
                                }
                            }
                            //strRetrun = objCom.Parameters["RETURN"].Value.ToString();
                            //string[] arrParams = strRetrun.Split(',');
                            strRetrun = ds.Tables[0].Rows[0]["ac_name"].ToString();
                        }
                        else
                        {
                            strRetrun = "TK TEST CORE OFF";
                        }

                    }
                    catch(Exception ex)
                    {
                       log.Error("3-Lay ten tai khoan lỗi: " + ex.Message );
                        return "Khong lay duoc Ten TK";
                    }
                   

             
          


            return strRetrun;

        }
        public string Get_VALUE_DATE()
        {
            string strRetrun = string.Empty;
            using (OracleConnection objCon =DataAccess.GetConnection())
            {
                using (OracleCommand objCom = new OracleCommand())
                {
                    objCom.Connection = objCon;
                    objCom.CommandText = "tcs_pck_util_shb.fnc_get_val_date";
                    objCom.CommandType = CommandType.StoredProcedure;

                    OracleParameter codeReturn = new OracleParameter("RETURN", OracleType.VarChar, 1000);
                    codeReturn.Direction = ParameterDirection.ReturnValue;


                    OracleParameter code1 = new OracleParameter("p_legacy_ac", OracleType.VarChar, 20);
                    code1.Direction = ParameterDirection.Input;
                    code1.Value = "SHB";


                    objCom.Parameters.Add(codeReturn);
                    objCom.Parameters.Add(code1);

                    try
                    {
                        //objCon.Open();
                        if (Core_status == "1")
                        {
                            objCom.ExecuteNonQuery();

                            strRetrun = objCom.Parameters["RETURN"].Value.ToString();
                        }
                        else
                        {
                            strRetrun = "20150202";
                        }

                    }
                    catch
                    {
                        return "Khong lay duoc VAL DATE";
                    }
                    finally
                    {
                        objCon.Close();
                    }

                }
            }

            return strRetrun;
        }

        public static string GetSOBL(string pv_strSOBL)
        {
            string  strRetrun = string.Empty;
            using (OracleConnection objCon =DataAccess.GetConnection())
            {
                using (OracleCommand objCom = new OracleCommand())
                {
                    
                    try
                    {
                        //objCon.Open();
                        if (Core_status == "1")
                        {
                            //objCom.ExecuteNonQuery();
                            OracleDataAdapter da = new OracleDataAdapter();
                            OracleCommand cmd = new OracleCommand();
                            cmd.Connection = objCon;
                            cmd.CommandText = "tcs_pck_util_shb.GET_GUARANTEE_DTLS";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("mdNO", OracleType.VarChar ).Value = pv_strSOBL;
                            cmd.Parameters.Add("aoutcursordata",OracleType.Cursor ).Direction = ParameterDirection.Output;

                            da.SelectCommand = cmd;
                            cmd.ExecuteNonQuery();
                            DataTable dt = new DataTable();
                            da.Fill(dt);
                            if (dt.Rows.Count > 0) {
                                strRetrun = ("<PARAMETER><RETCODE>0</RETCODE><NGAYLAP>"
                            + (dt.Rows[0]["Create_Date"].ToString() + ("</NGAYLAP>" + ("<NGAYHL>"
                            + (dt.Rows[0]["Approve_Date"].ToString() + ("</NGAYHL><NGAYHH>"
                            + (dt.Rows[0]["Expire_Date"].ToString() + ("</NGAYHH>" + ("<DIENGIAI>"
                            + (dt.Rows[0]["Description"].ToString() + ("</DIENGIAI><AMOUNT>"
                            + (dt.Rows[0]["AMOUNT"].ToString() + ("</AMOUNT><MST>"
                            + (dt.Rows[0]["MST"].ToString() + ("</MST><CIFID>"
                            + (dt.Rows[0]["CIF"].ToString() + ("</CIFID></PARAMETER>")))))))))))))))));
                            }
                        }
                        else
                        {
                            strRetrun = "<PARAMETER><RETCODE>999</RETCODE></PARAMETER>";
                        }

                    }
                    catch
                    {
                        strRetrun = "<PARAMETER><RETCODE>999</RETCODE></PARAMETER>";
                    }
                    finally
                    {
                        objCon.Close();
                    }

                }
            }


            return strRetrun;

        }

        public static DataTable GetFeeVat(string pv_strLPSF)
        {
            DataTable dt = new DataTable();
            using (OracleConnection objCon =DataAccess.GetConnection())
            {
                using (OracleCommand objCom = new OracleCommand())
                {

                    try
                    {
                        //objCon.Open();
                        if (Core_status == "1")
                        {
                            //objCom.ExecuteNonQuery();
                            OracleDataAdapter da = new OracleDataAdapter();
                            OracleCommand cmd = new OracleCommand();
                            cmd.Connection = objCon;
                            cmd.CommandText = "tcs_pck_util_shb.GET_CHARGES";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("mdNO", OracleType.VarChar).Value = pv_strLPSF;
                            cmd.Parameters.Add("aoutcursordata", OracleType.Cursor).Direction = ParameterDirection.Output;

                            da.SelectCommand = cmd;
                            cmd.ExecuteNonQuery();
                            
                            da.Fill(dt);
                        }
                        

                    }
                    catch
                    {
                        
                    }
                    finally
                    {
                        objCon.Close();
                    }

                }
            }


            return dt;

        }

        private string sendMsg(string ServerIP, int ServerPort, string MsgCnt)
        {
            System.Net.Sockets.TcpClient tcpClient;
            NetworkStream networkStream;
            string strID = Business.Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_HDR_ID.NextVal");
            string strSQL = "";
            strSQL = "INSERT INTO TCS_LOG_CORE (ID,TIME_UPDATE,DESCRIPTION) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += MsgCnt;
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                tcpClient = new System.Net.Sockets.TcpClient();
                System.Net.IPEndPoint myEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(ServerIP), ServerPort);
                tcpClient.SendTimeout = CORE_SEND_TIMEOUT;
                tcpClient.ReceiveTimeout = CORE_RECIVE_TIMEOUT;
                tcpClient.NoDelay = true;
                tcpClient.Connect(myEP);

                networkStream = tcpClient.GetStream();
                if (networkStream.CanWrite & networkStream.CanRead)
                {

                    MsgCnt = MsgCnt.Remove(MsgCnt.Length - 2, 2);
                    Byte[] sendBytes = Encoding.UTF8.GetBytes(MsgCnt);
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                    byte[] bytes;
                    string returndata = "";
                    tcpClient.ReceiveBufferSize = 8912 * 5;
                    bytes = new byte[tcpClient.ReceiveBufferSize + 1];
                    networkStream.Read(bytes, 0, (int)tcpClient.ReceiveBufferSize);
                    returndata = Encoding.UTF8.GetString(bytes);
                    strSQL = "UPDATE TCS_LOG_CORE SET MSG_IN = '";
                    strSQL += returndata.Trim().Replace("\0", "") + "' WHERE ID=";
                    strSQL += strID;
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                    tcpClient.Close();
                    tcpClient = null;
                    networkStream.Close();
                    networkStream = null;

                    return returndata.Trim().Replace("\0", "");
                }
                else
                {
                    tcpClient.Close();
                    tcpClient = null;
                    networkStream.Close();
                    networkStream = null;
                    return "";
                }

            }
            catch (Exception ex)
            {
                //Funcs.WriteLog(" * Error: + [" + ex.Message + "]");
                tcpClient = null;
                networkStream = null;
                return "";
            }
        }
        private string sendMsg_Payment(string ServerIP, int ServerPort, string MsgCnt)
        {
            System.Net.Sockets.TcpClient tcpClient;
            NetworkStream networkStream;
            string strID = Business.Common.mdlCommon.getDataKey("SEQ_DCHIEU_NHHQ_HDR_ID.NextVal");
            string strSQL = "";
            strSQL = "INSERT INTO TCS_LOG_CORE (ID,TIME_UPDATE,DESCRIPTION) VALUES(";
            strSQL += strID;
            strSQL += ",SYSDATE,'";
            strSQL += MsgCnt;
            strSQL += "')";
            VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
            try
            {
                tcpClient = new System.Net.Sockets.TcpClient();
                System.Net.IPEndPoint myEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(ServerIP), ServerPort);
                tcpClient.SendTimeout = CORE_SEND_TIMEOUT;
                tcpClient.ReceiveTimeout = CORE_RECIVE_TIMEOUT;
                tcpClient.NoDelay = true;
                tcpClient.Connect(myEP);

                networkStream = tcpClient.GetStream();
                if (networkStream.CanWrite & networkStream.CanRead)
                {

                    MsgCnt = MsgCnt.Remove(MsgCnt.Length - 2, 2);
                    Byte[] sendBytes = Encoding.UTF8.GetBytes(MsgCnt);
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                    byte[] bytes;
                    string returndata = "";
                    tcpClient.ReceiveBufferSize = 8912 * 5;
                    bytes = new byte[tcpClient.ReceiveBufferSize + 1];
                    networkStream.Read(bytes, 0, (int)tcpClient.ReceiveBufferSize);
                    returndata = Encoding.UTF8.GetString(bytes);
                    strSQL = "UPDATE TCS_LOG_CORE SET MSG_IN = '";
                    strSQL += returndata.Trim().Replace("\0", "") + "' WHERE ID=";
                    strSQL += strID;
                    VBOracleLib.DataAccess.ExecuteNonQuery(strSQL, System.Data.CommandType.Text);
                    tcpClient.Close();
                    tcpClient = null;
                    networkStream.Close();
                    networkStream = null;

                    return returndata.Trim().Replace("\0", "");
                }
                else
                {
                    tcpClient.Close();
                    tcpClient = null;
                    networkStream.Close();
                    networkStream = null;
                    return "";
                }

            }
            catch (Exception ex)
            {
                //Funcs.WriteLog(" * Error: + [" + ex.Message + "]");
                tcpClient = null;
                networkStream = null;
                return "TO999";
            }
        }
       

    }
}
