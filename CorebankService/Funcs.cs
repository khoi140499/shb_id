using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SHB_UCS
{
	/// <summary>
	/// Summary description for Funcs.
	/// </summary>
	public class Funcs
	{
        public static string gLogFile = "C:\\LOG\\SHB_UCS\\SHB_UCS";

		//public static string gIntellectCnn= "User Id=SHBUATTWO;Data Source=INTELLECTUAT;Password=SHBUATTWO;Persist Security Info=true;";
		//public static string gIntellectSchema= "SHBUATTWO.";
		//public static String gCOREConnstr = "User Id=ebank;Data Source=SBANK_LIVE;Password=;Persist Security Info=true;";

		public Funcs()
		{
			//
			// TODO: Add constructor logic here
			//
		}

//		retXML = "<SO_CANCELLATION><HEADER>";
//		retXML += "<res_Result_Code>" + errGeneral + "<res_Result_Code>";
//		retXML += "<res_Err_Desc>" + errGeneralDesc + "<res_Err_Desc>";
//		retXML += "</HEADER></SO_CANCELLATION>";

		public static string getXMLerror(string tran_type, string error_code, string error_desc)
		{
			string ret = string.Empty;
			ret += "<"  + tran_type + ">" + "<HEADER>";
			ret += "<res_Result_Code>" + error_code + "</res_Result_Code>";
			ret += "<res_Err_Desc>" + error_desc + "</res_Err_Desc>";
			ret += "</HEADER>" + "</"  + tran_type + ">" ;
			return ret;
		}

		public static string removeDuplicateKey( string inputStr, string key)
		{
			string ret = string.Empty;
			int pos = 0;
			int pos1 = 0;
			pos1 = inputStr.IndexOf( key);
			pos = inputStr.IndexOf( key);
			string next1 = string.Empty;
			string next2 = string.Empty;
			if (pos >=0)
			{
				while ( ( next1 != "~") || ( next2 != "*"))
				{
					pos ++;
					next1 = inputStr.Substring(pos, 1);
					next2 = inputStr.Substring(pos + 1, 1);
				}
				ret = inputStr.Substring(0,pos1) + inputStr.Substring(pos+2);
			}	
			else
			{
				ret = inputStr;
			}		
			return ret;
		}

		public static void WriteLog(String msg)
		{
			try
			{
				StreamWriter SW;
				SW=File.AppendText( gLogFile + "_" + DateTime.Now.ToString("yyyyMMdd") + ".log");
				SW.WriteLine("\r\n ====================================================================");
				SW.WriteLine("\r\n [" + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "]: " + msg );
				SW.Close();
			}
			catch ( Exception ex) 
			{
				//do nothing: khong the ghi dc log
			}
			}
//		public static void WriteLog(String msg)
//		{
//			try
//			{
//				StreamWriter SW;
//				Console.WriteLine("\r\n[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "]: " +  msg);
//				//return;
//				SW=File.AppendText(gLogFile);
//				SW.WriteLine("\r\n[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "]: " +  msg);
//				SW.Close();
//			}
//			catch (Exception ex)
//			{
//				Console.Write(ex.Message);
//			}
//		}
	}
}
