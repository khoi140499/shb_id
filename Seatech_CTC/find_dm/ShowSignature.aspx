﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage06.master" AutoEventWireup="false"
    CodeFile="ShowSignature.aspx.vb" Inherits="find_dm_ShowSignature" Title="Kiểm tra chữ ký khách hàng" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg06_MainContent" runat="Server">
    <asp:HiddenField ID="hdfAcc" runat="server" Value="" />
    <table width="100%" align="center">
        <tr>
            <td height="20px">
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvImages" CssClass="Gridview" runat="server" AutoGenerateColumns="False"
                    HeaderStyle-BackColor="#7779AF" HeaderStyle-ForeColor="white">
                    <Columns>
                        <asp:BoundField HeaderText = "Họ và tên" DataField="CIF_SIG_NAME" />
                        <asp:TemplateField HeaderText="Image">
                            <ItemTemplate>
                                <img src='data:image/jpg;base64,<%# System.Convert.ToBase64String(Eval("SIGN_IMG")) %>' alt="image" height="250" width="250"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText = "Số CIF" DataField="CIF_ID" /> 
                        <asp:BoundField HeaderText = "Ghi chú" DataField="CIF_SIG_TITLE" /> 
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr >
            <td height="50px">
            <asp:Label ID='lbErrr' runat="server" Visible="false" Font-Size="X-Large" ForeColor="Green" ></asp:Label>
            </td>
        </tr>
        <tr><td height="20px"></td></tr>
        <tr>
            <td>
                 <span style="display:none"><asp:Button ID="cmdLoad" runat="server" Text="Lấy chữ ký mới" /></span>
            </td>
        </tr>
    </table>
</asp:Content>
