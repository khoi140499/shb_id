﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage06.master" AutoEventWireup="false"
    CodeFile="Find_NNT_NEW.aspx.vb" Inherits="Find_NNT_NEW" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Tìm danh mục</title>

    <script type="text/javascript">
        
        function SelectNNT(strID, strTitle, strNguonDL) {
            var arr = new Array();
            arr["ID"] = strID;
            arr["Title"] = strTitle;
            arr["NguonDL"] = strNguonDL;
            window.parent.returnValue = arr;
            window.parent.close();
        }
        
        function SearchNNT(pageIndex) {
            //document.getElementById('btnNext').disabled =false;
            //document.getElementById('btnPrev').disabled =false;
            if ($get('<%=txtCurrPage.ClientID %>').value - pageIndex >0 ) {
                $get('<%=txtCurrPage.ClientID %>').value = $get('<%=txtCurrPage.ClientID %>').value - pageIndex;    
            }else
            {
                $get('<%=txtCurrPage.ClientID %>').value ="0";
            }            
            var strKey = $get('<%=txtIDNNT.ClientID %>').value + '|' + $get('<%=selNguonDL.ClientID %>').value + '|' + $get('<%=txtSHKB.ClientID %>').value + '|' + ($get('<%=txtCurrPage.ClientID %>').value);       
            PageMethods.SearchNNT(strKey,SearchNNT_Complete,SearchNNT_Error);     
        }                       
        function SearchNNT_Complete(result,methodName)
        {                                 
            document.getElementById('divNNT').innerHTML = jsBuildResultTable(result);
            /*if (result.length()>0)
            {
                document.getElementById('btnNext').disabled =true;
                if ($get('<%=txtCurrPage.ClientID %>').value =="0")
                {
                    document.getElementById('btnPrev').disabled =false;
                }
                else
                {
                    document.getElementById('btnPrev').disabled =false;
                }
            }*/
        }
        function SearchNNT_Error(error,userContext,methodName)
        {
            if(error !== null) 
            {
               document.getElementById('divNNT').innerHTML="Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
	    function jsBuildResultTable(strInput){
	        var strRet ='';
	        if (strInput.length>0){
	            strRet +=" <table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td width='150' align='left' class='nav'>MÃ SỐ THUẾ</td><td align='left' class='nav'>TÊN ĐỐI TƯỢNG NỘP THUẾ</td></tr><tr><td colspan='2' class='line'></td></tr>";
	            var arr = strInput.split("|");
	            for (var i=0;i<arr.length-1;i++){	                
	                var strID = arr[i].split(";")[0];
                    var strTitle = arr[i].split(";")[1];                    
                    strRet += "<tr onclick=\"SelectNNT('" + strID + "','" + strTitle + "','');\" class='PH'>" + "<td width='150' align='left'><b>" + strID + "</b></td><td  align='left'>" + strTitle + "</td></tr><tr><td colspan='2' height='8' ></td></tr> <tr><td colspan='2' class='line'></td></tr>";                                        
	            }
	            strRet +="</table>";
	        }else{
	            strRet +=" <table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td width='100%' align='left' class='nav'>Không tồn tại Mã số Thuế này !</td></tr></table>";    
	        }	  
	        return strRet;    	          	        
	    }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg06_MainContent" runat="Server">
    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
        <tr>
            <td width='100%' align="left" valign="top" style="padding-left: 10; padding-right: 10;
                padding-top: 10; padding-bottom: 10;">
                <table cellpadding='0' cellspacing='0' width='700'>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="70px">
                                        Mã KBNN:
                                    </td>
                                    <td colspan="4">
                                        <input id='txtSHKB' class="inputflat" disabled runat="server" type="text" style="width: 200" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="70px">
                                        Mã NNT:
                                    </td>
                                    <td width="120px">
                                        <input id='txtIDNNT' class="inputflat" style="width: 150" runat="server" type="text"
                                            onkeydown="if(event.keyCode==13) {SearchNNT(0); event.keyCode=9;}" />
                                    </td>
                                    <td width="80px">
                                        Nguồn DL:
                                    </td>
                                    <td width="150px">
                                        <select id='selNguonDL' runat="server" class="inputflat">
                                            <option value="NT" selected>Danh mục người nộp thuế</option>
                                            <option value="VL">Danh mục người nộp thế vãng lai</option>
                                            <option value="TB">Trước bạ</option>
                                            <option value="TK">Tờ khai Hải Quan</option>
                                            <option value="DB">Danh mục đặc biệt</option>
                                        </select>
                                    </td>
                                    <td align="left">
                                        <input type="button" class="ButtonCommand" value="Tìm kiếm" onclick="SearchNNT();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height='15'>
                        </td>
                    </tr>
                    <tr>
                        <td width='100%' align="left" valign="top" style="padding-left: 10px;">
                            <div id="divNNT" class='text'>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height='15'>
                            <table align="right">
                                <tr>
                                    <td>
                                        Current page:
                                    </td>
                                    <td>
                                        <input id='txtCurrPage' class="inputflat" disabled value="0" runat="server" type="text"
                                            style="width: 50px" />
                                    </td>
                                    <td>
                                        <input type="button" id="btnPrev" class="ButtonCommand" value="Prev" onclick="SearchNNT(1);" />
                                    </td>
                                    <td>
                                        <input type="button" id="btnNext" class="ButtonCommand" value="Next" onclick="SearchNNT(-1);" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
