Imports VBOracleLib
Imports Business
Imports System.Data
Partial Class Find_NNT_NEW

    Inherits System.Web.UI.Page

    'Private Shared strSHKB As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not Request.QueryString.Item("initParam") Is Nothing Then txtIDNNT.Value = Request.QueryString.Item("initParam").ToString()
        If Not Request.QueryString.Item("SHKB") Is Nothing Then txtSHKB.Value = Request.QueryString.Item("SHKB").ToString()

        If Not Request.QueryString.Item("Src") Is Nothing Then
            If Request.QueryString.Item("Src") = "TK" Then
                selNguonDL.SelectedIndex = 3 'Hai Quan
            End If
        End If

        If Not Request.QueryString.Item("page") Is Nothing Then
            selNguonDL.SelectedIndex = 4
            selNguonDL.Disabled = True
        End If
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function SearchNNT(ByVal strInput As String) As String
        Dim strResult As String = String.Empty
        Dim ds As New DataSet
        Dim v_strMaNNT As String = strInput.Split("|")(0).ToString
        Dim v_strNguonDL As String = strInput.Split("|")(1).ToString
        Dim v_strSHKB As String = strInput.Split("|")(2).ToString
        'Kienvt : 
        Dim v_intCurrPageInx As Integer = CInt(IIf(strInput.Split("|")(3).ToString.Length > 0, strInput.Split("|")(3).ToString, 0))


        Select Case v_strNguonDL
            Case "NT"
                ds = Load_DM_NNT(v_strMaNNT, v_strSHKB, v_intCurrPageInx)
            Case "VL"
                ds = Load_DM_NNT_VL(v_strMaNNT, v_strSHKB)
            Case "TK"
                ds = Load_DM_NNT_ToKhaiHQ(v_strMaNNT)
            Case "TB"
                ds = Load_DM_NNT_TruocBa(v_strMaNNT)
            Case "DB"
                ds = Load_DM_NNT_DB(v_strMaNNT)
        End Select

        If Not Globals.IsNullOrEmpty(ds) Then
            Dim dt As DataTable = ds.Tables(0)
            If Not Globals.IsNullOrEmpty(dt) Then
                Dim strID, strTitle As String
                Dim i As Integer = 0
                Dim intRowCount As Integer = 0
                For i = (v_intCurrPageInx * 100) To dt.Rows.Count - 1
                    If intRowCount <= 100 Then
                        strID = dt.Rows(i)(0).ToString
                        strTitle = dt.Rows(i)(1).ToString
                        strResult += strID & ";" & strTitle & "|"
                    Else
                        Exit For
                    End If
                    intRowCount = intRowCount + 1
                Next
            Else
                strResult = ""
            End If
        Else
            strResult = ""
        End If

        Return strResult
    End Function


    Private Shared Function Load_DM_NNT(ByVal strInput As String, ByVal v_strSHKB As String, _
                Optional ByVal currPageIndex As Integer = 0) As DataSet
        Dim strWhere As String = ""

        strWhere = strWhere & " where (MA_NNT like '" & strInput & "%') "

        'If Not v_strSHKB Is Nothing Then
        '    strWhere = strWhere & " and (SHKB like '" & v_strSHKB & "%') "
        'End If

        Dim strSQL As String
        Try
            strSQL = " select ma_nnt as ID, ten_nnt from " & _
                    " (select ma_nnt, ten_nnt,SHKB from tcs_dm_nnt " & strWhere & _
                    " union " & _
                    " select ma_nnt, ten_nnt,SHKB from tcs_sothue " & strWhere & ") dm_nnt " & strWhere & _
                    " order by ma_nnt "

            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Shared Function Load_DM_NNT_VL(ByVal strInput As String, ByVal v_strSHKB As String) As DataSet
        Dim strWhere As String = ""
        strWhere = strWhere & " where (MA_NNT like '" & strInput & "%') "
        Dim strSQL As String
        'If Not v_strSHKB Is Nothing Then
        '    strWhere = strWhere & " and (SHKB like '" & v_strSHKB & "%') "
        'End If
        Try
            strSQL = " Select distinct MA_NNT as ID, TEN_NNT as Title " & _
                " From TCS_DM_NNT_VL " & _
                strWhere & _
                " Order By MA_NNT"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Shared Function Load_DM_NNT_ToKhaiHQ(ByVal strInput As String) As DataSet
        Dim strWhere As String = ""
        strWhere = strWhere & " where (MA_NNT like '" & strInput & "%') "
        Dim strSQL As String
        Try
            strSQL = " Select distinct MA_NNT as ID, TEN_NNT as Title " & _
                " From TCS_DS_TOKHAI " & _
                strWhere & _
                " Order By MA_NNT"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Shared Function Load_DM_NNT_TruocBa(ByVal strInput As String) As DataSet
        Dim strWhere As String = ""
        strWhere = strWhere & " where (MADTNT like '" & strInput & "%') "
        Dim strSQL As String
        Try
            strSQL = "Select distinct MADTNT as ID, TENDTNT as Title " & _
                " From TCS_TRUOCBA " & _
                strWhere & _
                " Order By MADTNT"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Shared Function Load_DM_NNT_DB(ByVal strInput As String) As DataSet
        Dim strWhere As String = ""
        strWhere = strWhere & " where (MA_NNT like '" & strInput & "%') "
        Dim strSQL As String
        Try
            strSQL = "Select distinct MA_NNT as ID, TEN_NNT as Title " & _
                " From TCS_DM_NNT_KB " & _
                strWhere & _
                " Order By MA_NNT"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class
