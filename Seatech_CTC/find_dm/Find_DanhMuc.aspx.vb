Imports System.Data
Imports VBOracleLib
Imports Business

Partial Class Find_DanhMuc
    Inherits System.Web.UI.Page

   
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Dim mv_strMaDiemThu As String = String.Empty
        'Dim mv_strMaDBHC As String = String.Empty
        'If Not Session.Item("User") Is Nothing Then
        '    Dim v_dt As DataTable = clsCTU.TCS_GetParams(CType(Session("User"), Integer))
        '    For Each v_dr As DataRow In v_dt.Rows
        '        Select Case v_dr("TEN_TS").ToString
        '            Case "MA_DT"
        '                mv_strMaDiemThu = v_dr("GIATRI_TS").ToString
        '            Case "MA_DBHC"
        '                mv_strMaDBHC = v_dr("GIATRI_TS").ToString
        '        End Select
        '    Next
        'End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        Try
            If Not IsPostBack Then
                If (Not ClientScript.IsStartupScriptRegistered(Me.GetType(), "InitDefValues")) Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "InitDefValues", PopulateDefaultValues(clsCTU.TCS_GetMaDT(CType(Session("User"), Integer)), clsCTU.TCS_GetMaDBHC(CType(Session("User"), Integer))), True)
                End If
                Dim strPage As String = CType(Request.QueryString("page"), String)
                Dim strMaDanhMuc As String = ""
                Dim strTenDanhMuc As String = ""
                Dim lovDM As New Business.lovDanhMuc(clsCTU.TCS_GetMaDT(CType(Session("User"), Integer)), clsCTU.TCS_GetMaDBHC(CType(Session("User"), Integer)))
                lovDM.ShowTitle(strPage, strMaDanhMuc, strTenDanhMuc)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function PopulateDefaultValues(ByVal strMa_Dthu As String, ByVal strMa_DBHC As String) As String
        Dim myJavaScript As StringBuilder = New StringBuilder()
        myJavaScript.Append(" var curMa_Dthu ='" & strMa_Dthu & "';")
        myJavaScript.Append(" var curMa_DBHC ='" & strMa_DBHC & "';")
        Return myJavaScript.ToString
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function SearchDanhMuc(ByVal strMa_DThu As String, ByVal strMa_DBHC As String, ByVal strPage As String, ByVal strKey As String, ByVal strTenDM As String, ByVal strSHKB As String) As String
        Try
            Dim strResult As String = ""
            Dim dt As DataTable
            Dim strMaDanhMuc As String = strKey.Trim
            Dim strTenDanhMuc As String = strTenDM
            Dim lovDM As New Business.lovDanhMuc(strMa_DThu, strSHKB)
            'dt = lovDM.ShowDanhMuc(strPage, strMaDanhMuc, strTenDanhMuc, "", strMa_DBHC)
            dt = lovDM.ShowDanhMuc(strPage, strMaDanhMuc, strTenDanhMuc, "", strMa_DBHC)


            If Not Globals.IsNullOrEmpty(dt) Then
                Dim strID, strTitle As String
                Dim i As Integer = 0
                For i = 0 To dt.Rows.Count - 1
                    If i <= 900 Then
                        strID = dt.Rows(i)(0).ToString
                        strTitle = dt.Rows(i)(1).ToString
                        strResult += strID & ";" & strTitle.Replace("'", " ") & "|"
                    Else
                        Exit For
                    End If
                Next
            Else
                strResult = ""
            End If

            Return strResult
        Catch ex As Exception
            Throw ex
            Return String.Empty
        End Try
    End Function
End Class

