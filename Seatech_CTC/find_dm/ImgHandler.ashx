﻿<%@ WebHandler Language="VB" Class="ImgHandler" %>

Imports System
Imports System.Web
Imports System.Data
Imports VBOracleLib
Imports System.Web.Services
Imports System.Data.OracleClient
Imports System.Data.OracleClient.OracleType

<WebService([Namespace]:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class ImgHandler : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        If Not context.Request.QueryString.Item("ImID") Is Nothing Then
            Dim v_strId As String = context.Request.QueryString.Item("ImID")
            
            Dim v_strSql As String
            Dim v_dt As DataTable = Nothing
            
            v_strSql = "SElECT SIGN_IMG FROM TBL_TEMP_SIGNATURE WHERE CIF_SIG_ID = '" & v_strId & "'"

            v_dt = DataAccess.ExecuteToTable(v_strSql)
            
            If (v_dt.Rows.Count > 0) Then
                context.Response.BinaryWrite(v_dt.Rows(0)("SIGN_IMG"))
                context.Response.End()
            End If
            
        Else
            context.Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Private Function ConvertImageFiletoBytes(ByVal ImageFilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(ImageFilePath) = True Then
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(ImageFilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(ImageFilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
End Class