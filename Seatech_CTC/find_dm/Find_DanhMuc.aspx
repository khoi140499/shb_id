﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage06.master" AutoEventWireup="false"
    CodeFile="Find_DanhMuc.aspx.vb" Inherits="Find_DanhMuc" Title="Danh mục" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Tìm danh mục</title>
    <link rel="Stylesheet" type="text/css" href="../css/gridStyle.css" />
    <script language="javascript" src="../javascript/popup.js" type="text/javascript"></script>
    <script language="javascript" src="../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        $(document).ready(function () {
            $("#txtMaDanhMuc").keyup(function () {
                $(this).val($(this).val().replace(/([a-z])/, function (s) { return s.toUpperCase() }));
            });
        });

        function jsSearch() {
            var strKey = document.getElementById('txtMaDanhMuc').value;
            var strTenDM = document.getElementById('txtTenDanhMuc').value;
            var strPage = getParam('page');
            var strSHKB = getParam2('SHKB');
            PageMethods.SearchDanhMuc(curMa_Dthu, curMa_DBHC, strPage, strKey, strTenDM, strSHKB, SearchDanhMuc_Complete, SearchDanhMuc_Error);
        }
        function SearchDanhMuc_Complete(result, methodName) {
            //document.getElementById('divDanhMuc').innerHTML = result;          
            document.getElementById('divDanhMuc').innerHTML = jsBuildResultTable(result);
        }
        function SearchDanhMuc_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
        function SelectDanhMuc(strID, strTitle) {
            var arr = new Array();
            arr["ID"] = strID;
            arr["Title"] = strTitle;
            window.parent.returnValue = arr;
            window.parent.close();
        }
        function getParam(param) {
            var query = window.location.search.substring(1);
            var parms = query.split('&');
            for (var i = 0; i < parms.length; i++) {
                var pos = parms[i].indexOf('=');
                if (pos > 0) {
                    var key = parms[i].substring(0, pos).toLowerCase();
                    var val = parms[i].substring(pos + 1);
                    if (key == param.toLowerCase())
                        return val;
                }
            }
            return '';
        }
        function getParam2(param) {
            var query = window.location.search.substring(1);
            var parms = query.split('&');
            for (var i = 0; i < parms.length; i++) {
                var pos = parms[i].indexOf('=');
                if (pos > 0) {
                    var key = parms[i].substring(0, pos).toLowerCase();
                    var val = parms[i].substring(pos + 1);
                    if (key == param.toLowerCase())
                        return val;
                }
            }
            return '';
        }

        function jsBuildResultTable(strInput) {
            var strRet = '';
            if (strInput.length > 0) {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                var arr = strInput.split("|");
                for (var i = 0; i < arr.length - 1; i++) {
                    var strID = arr[i].split(";")[0];
                    var strTitle = arr[i].split(";")[1];
                    strRet += "<tr onclick=\"SelectDanhMuc('" + strID + "','" + strTitle + "');\" class='PH'>" + "<td width='150' align='left'><b>" + strID + "</b></td><td  align='left'>" + strTitle + "</td></tr><tr><td colspan='2' height='8' ></td></tr> <tr><td colspan='2' class='line'></td></tr>";
                }
                strRet += "</table>";
            } else {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td width='100%' align='left' class='nav'>Không tồn tại danh mục này !</td></tr></table>";
            }
            return strRet;
        }
	            
    </script>

    <style type="text/css">
        .style1
        {
            height: 31px;
        }
        .style2
        {
            width: 35%;
            height: 24px;
        }
        .style3
        {
            width: 65%;
            height: 24px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg06_MainContent" runat="Server">
    <table class="grid-border" style="width: 600px" cellpadding="1" cellspacing="1">
        <tr class="grid-heading">
            <td align="center" class="style2" >
                <b>Mã danh mục</b> 
            </td>
            <td  align="center" class="style3" >
                <b>Tên danh mục</b>
            </td>
        </tr>
        <tr class="grid-even-row">
            <td>
              <b>Mã danh mục</b> 
            </td>
            <td>
            <input type="text" id="txtMaDanhMuc" style ="width:30%" onkeydown="if(event.keyCode==13) {jsSearch(); event.keyCode=9;}" class="inputflat" />&nbsp;
            </td>
        </tr>
        <tr>    
            <td valign="top">
            <b>Tên danh mục</b>
           
            </td>
            <td>
             <input type="text" id="txtTenDanhMuc" style ="width:50%" class="inputflat" />&nbsp;
             <input type="button" id="btnSearch" value=" Tìm kiếm " class="inputflat" onclick="jsSearch();" />
            </td>
        </tr>
        <tr>
            <td colspan="2" class="style1">
                <div id='divDanhMuc'>
                </div>
                <div id='divStatus'>
                </div>
                
            </td>
        </tr>
    </table>
</asp:Content>
