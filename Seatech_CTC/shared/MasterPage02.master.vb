﻿Imports System.Data
Imports VBOracleLib
Imports Business
Imports System.Diagnostics
Partial Class shared_MasterPage02
    Inherits System.Web.UI.MasterPage

    Public imageFolder As String

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim dtNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")
        Dim vStrRootPath As String = ConfigurationManager.AppSettings("APP_ROOT_PATH").ToString
        If Not vStrRootPath.Contains("~") Then
            vStrRootPath = "~" & vStrRootPath
        End If
        Dim iResult As Integer = 0
        Dim user = Session.Item("User")
        If String.IsNullOrEmpty(user) Then
            Response.Redirect("~/pages/frmLogout.aspx", False)
            Exit Sub
        End If
        Dim sChiNhanh As String = String.Empty
        sChiNhanh = CType(Session.Item("MA_CN_USER_LOGON"), String)
        If String.IsNullOrEmpty(sChiNhanh) Then

            Response.Redirect("~/pages/frmNoAuthorize.aspx", False)
            Exit Sub
        End If
        Dim url As String = HttpContext.Current.Request.Url.AbsolutePath.Replace(vStrRootPath, "~")
        If Not url.Contains("~") Then
            url = "~" & url
        End If
        url = url.Replace(vStrRootPath, "~")
        Try

            Dim strSql As String = "SELECT COUNT (1) kiemtra " & "  FROM (SELECT ma_cn " & " FROM tcs_phanquyen WHERE ma_nhom = (SELECT ma_nhom " & " FROM tcs_dm_nhanvien " & " WHERE ma_nv"
            strSql += "=" + Session.Item("User") + ")) " & " WHERE ma_cn In ( " & " Select nodeid node " & " FROM tcs_site_map " & " WHERE status = 1 " & " And navigateurl = '" + url + "' "
            strSql += " UNION " & " SELECT parentnodeid node " & " FROM tcs_site_map " & " WHERE status = 1 " & " AND navigateurl = '" + url + "') "

            If Not url.Contains("~") Then
                url = "~" & url
            End If
            If url <> "~/pages/frmHomeNV.aspx" Then
                Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
                If Not Globals.IsNullOrEmpty(dt) Then
                    iResult = CInt(dt.Rows(0)(0).ToString())
                End If
                If iResult <= 0 Then

                    Response.Redirect("~/pages/frmNoAuthorize.aspx", False)
                    Exit Sub
                End If
            End If
            imageFolder = Request.ApplicationPath + "/images/"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class

