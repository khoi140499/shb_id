﻿Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports System.Data
'Imports OboutInc.TextMenu


Partial Class shared_ucTopPG
    Inherits System.Web.UI.UserControl
    Private user As CTuUser
    Public imageFolder As String

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            'when i am going to upload it to a server :imageFolder =  "/images/";   // without adding "path" variable.
            imageFolder = Request.ApplicationPath + "/images/"
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Session.Item("User").ToString.Length = 0 Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            user = New CTuUser(Session.Item("User").ToString())
            If Not IsPostBack Then
                Me.lblNguoiDung.Text = "Tên NSD: " & user.Ten_DN.Trim.ToString
                Me.lblChucDanh.Text = "Chức vụ: " & user.Chuc_Danh.ToUpper()
                Me.lblDiemThu.Text = "Chi nhánh: " & user.CN_NH.ToString
                'Me.lblWorkingDate.Text = "Ngày làm việc: " & ConvertNumberToDate(gdtmNgayLV).ToString(gc_DEFAULT_FORMAT_DATE)
                Me.lblWorkingDate.Text = "Ngày làm việc: " & ConvertNumberToDate(user.Ngay_LV.ToString()).ToString("dd/MM/yyyy")
                
            End If
        Catch ex As Exception
            'Throw ex
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If Not IsPostBack Then
            'Dim tm As TextMenu = New TextMenu()
            'tm.ID = "TextMenu1"
            'tm.StyleFolder = "../MenuStyles/submenuicon"
            'tm.SubMenuText = ""

            Dim ds As DataSet
            Dim strSQL As String = "SELECT * FROM (SELECT NodeId,parentnodeid,Text,NavigateUrl,LookId,LeftIcon,LeftIconHover,RightIcon,RightIconHover,TemplateID,KeyboardShortcut,MENU_ORDER,PARENT_ORDER " & _
                      " FROM TCS_SITE_MAP WHERE ParentNodeID IS NULL OR UPPER(LOOKID)=UPPER('BREAKITEM') UNION SELECT NodeId,ParentNodeID,Text,NavigateUrl,LookId,LeftIcon,LeftIconHover,RightIcon,RightIconHover,TemplateID,KeyboardShortcut,MENU_ORDER,PARENT_ORDER FROM " & _
                      " (SELECT distinct A.TEN_BUTTON,A.TEN_CN_CON,C.NodeId,C.ParentNodeID,C.Text,C.NavigateUrl,C.LookId,C.LeftIcon,C.LeftIconHover,C.RightIcon,C.RightIconHover,TemplateID,KeyboardShortcut,MENU_ORDER,PARENT_ORDER  " & _
                      " FROM TCS_DM_CHUCNANG A,TCS_PHANQUYEN B,TCS_SITE_MAP C " & _
                      " WHERE A.MA_CN =B.MA_CN AND C.STATUS='1' AND (C.NODEID = B.MA_CN ) AND instr('" & user.Ma_Nhom & "', B.MA_NHOM)>0 ORDER BY C.NODEID )) ORDER BY PARENT_ORDER,menu_order,NODEID"
            '" WHERE A.MA_CN =B.MA_CN AND C.STATUS='1' AND (C.NODEID = B.MA_CN OR C.PARENTNODEID = B.MA_CN) AND instr('" & user.Ma_Nhom & "', B.MA_NHOM)>0 ORDER BY C.NODEID )) ORDER BY PARENT_ORDER,menu_order,NODEID"
            If Session.Item("dsMenu") Is Nothing Then
                ds = New DataSet()
                ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                Session.Item("dsMenu") = ds
            End If

            'If Not Globals.IsNullOrEmpty(ds) Then
            '    For Each dbRow In ds.Tables(0).Rows
            '        If dbRow("ParentNodeID").ToString.Length > 0 Then
            '            tm.Add(dbRow("ParentNodeID").ToString, dbRow("NodeId").ToString, dbRow("Text").ToString, Request.ApplicationPath & dbRow("NavigateUrl").ToString.Replace("~", ""))
            '        Else
            '            tm.Add("", dbRow("NodeId").ToString, dbRow("Text").ToString, Request.ApplicationPath & dbRow("NavigateUrl").ToString.Replace("~", ""))
            '        End If
            '    Next dbRow
            'End If
            'placeHolder1.Controls.Add(tm)
            If ds Is Nothing Then
                ds = New DataSet()
                ds = Session.Item("dsMenu")
            End If
            If Not Globals.IsNullOrEmpty(ds) Then
                ds.Relations.Clear()
                ds.Relations.Add("NodeRelation", ds.Tables(0).Columns("NodeId"), ds.Tables(0).Columns("ParentNodeID"))
                Dim dbRow As System.Data.DataRow
                For Each dbRow In ds.Tables(0).Rows
                    If (dbRow.IsNull("ParentNodeID")) Then
                        Dim newItem As ComponentArt.Web.UI.MenuItem
                        newItem = CreateItem(dbRow)
                        Menu1.Items.Add(newItem)
                        PopulateSubMenu(dbRow, newItem)
                    End If
                Next dbRow
            End If

            'End If 
        Catch ex As Exception
            Exit Sub
            'Throw ex
        End Try
    End Sub

    Private Sub PopulateSubMenu(ByVal dbRow As System.Data.DataRow, ByVal item As ComponentArt.Web.UI.MenuItem)
        Try
            Dim childRow As System.Data.DataRow
            For Each childRow In dbRow.GetChildRows("NodeRelation")
                Dim childItem As ComponentArt.Web.UI.MenuItem
                childItem = CreateItem(childRow)
                item.Items.Add(childItem)
                PopulateSubMenu(childRow, childItem)
            Next childRow
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CreateItem(ByVal dbRow As System.Data.DataRow) As ComponentArt.Web.UI.MenuItem
        Try
            Dim item As New ComponentArt.Web.UI.MenuItem()
            item.Text = dbRow("Text").ToString()
            item.NavigateUrl = dbRow("NavigateUrl").ToString() 'VBOracleLib.Security.EncryptStr(dbRow("NavigateUrl").ToString())
            If Not dbRow("LookId").ToString() Is Nothing Then item.LookId = dbRow("LookId").ToString()
            If dbRow("LookId").ToString() Is Nothing Then item.LookId = dbRow("ShortcutItem").ToString()
            item.Look.LeftIconUrl = dbRow("LeftIcon").ToString()
            'item.Look.HoverLeftIconUrl = dbRow("LeftIconHover").ToString()
            If dbRow("RightIcon").ToString().Length > 0 Then
                item.Look.RightIconUrl = dbRow("RightIcon").ToString()
                item.Look.RightIconWidth = 20
            End If
            'item.Look.HoverRightIconUrl = dbRow("RightIconHover").ToString()
            If dbRow("KeyboardShortcut").ToString().Length > 0 Then
                item.KeyboardShortcut = dbRow("KeyboardShortcut").ToString()
            End If
            Return item
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
