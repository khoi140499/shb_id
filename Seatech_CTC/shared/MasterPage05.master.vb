﻿Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports System.Data
'Imports OboutInc.TextMenu

Partial Class shared_MasterPage05
    Inherits System.Web.UI.MasterPage
    Public imageFolder As String
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim dtNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")
        Dim v_strRootPath As String = ConfigurationManager.AppSettings("APP_ROOT_PATH").ToString
        If Not v_strRootPath.Contains("~") Then
            v_strRootPath = "~" & v_strRootPath
        End If
        'Response.Write(HttpContext.Current.Request.Url.AbsolutePath.Replace("/Seatech_CTC", "~") + "  " + Session.Item("User"))
        Dim iResult As Integer = 0

        Dim user = Session.Item("User")


        If String.IsNullOrEmpty(user) Then
            Response.Redirect("~/pages/frmLogout.aspx", False)
            Exit Sub
        End If

        Dim sChiNhanh As String = String.Empty
        sChiNhanh = Session.Item("MA_CN_USER_LOGON")

        If String.IsNullOrEmpty(sChiNhanh) Then

            Response.Redirect("~/pages/frmNoAuthorize.aspx", False)
            Exit Sub
        End If
        Dim url As String = HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~")
        If Not url.Contains("~") Then
            url = "~" & url
        End If
        url = url.Replace(v_strRootPath, "~")
        Try

            Dim strSql As String = "SELECT COUNT (1) kiemtra " & "  FROM (SELECT ma_cn " & " FROM tcs_phanquyen WHERE ma_nhom = (SELECT ma_nhom " & " FROM tcs_dm_nhanvien " & " WHERE ma_nv"
            strSql += "=" + Session.Item("User") + ")) " & " WHERE ma_cn In ( " & " Select nodeid node " & " FROM tcs_site_map " & " WHERE status = 1 " & " And navigateurl = '" + url + "' "
            strSql += " UNION " & " SELECT parentnodeid node " & " FROM tcs_site_map " & " WHERE status = 1 " & " AND navigateurl = '" + url + "') "

            If Not url.Contains("~") Then
                url = "~" & url
            End If
            If url <> "~/pages/frmHomeNV.aspx" Then
                Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
                If Not Globals.IsNullOrEmpty(dt) Then
                    iResult = CInt(dt.Rows(0)(0).ToString())
                End If

                If url = "~/pages/BaoLanhHQ/frmKetQuaTraCuuBLHQ.aspx" Or url = "~/NTDT/frmHoanThienNNTDT.aspx" Then
                    iResult = 1
                End If

                If iResult <= 0 Then
                    Response.Redirect("~/pages/frmNoAuthorize.aspx", False)
                    Exit Sub
                End If
            End If

            imageFolder = Request.ApplicationPath + "/images/"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        Dim tm As TextMenu = New TextMenu()
    '        tm.ID = "TextMenu1"
    '        tm.StyleFolder = "../styles/submenuicon"
    '        tm.SubMenuText = ""

    '        'tm.ColorBackground = "#444444"
    '        'tm.ColorFont = "gold"

    '        Dim ds As New System.Data.DataSet()
    '        Dim strSQL As String = "SELECT * FROM (SELECT NodeId,parentnodeid,Text,NavigateUrl,LookId,LeftIcon,LeftIconHover,RightIcon,RightIconHover,TemplateID,KeyboardShortcut " & _
    '                  " FROM TCS_SITE_MAP WHERE ParentNodeID IS NULL OR UPPER(LOOKID)=UPPER('BREAKITEM') UNION SELECT NodeId,ParentNodeID,Text,NavigateUrl,LookId,LeftIcon,LeftIconHover,RightIcon,RightIconHover,TemplateID,KeyboardShortcut FROM " & _
    '                  " (SELECT distinct A.TEN_BUTTON,A.TEN_CN_CON,C.NodeId,C.ParentNodeID,C.Text,C.NavigateUrl,C.LookId,C.LeftIcon,C.LeftIconHover,C.RightIcon,C.RightIconHover,TemplateID,KeyboardShortcut  " & _
    '                  " FROM TCS_DM_CHUCNANG A,TCS_PHANQUYEN B,TCS_SITE_MAP C " & _
    '                  " WHERE A.MA_CN =B.MA_CN AND (C.NODEID = B.MA_CN OR C.PARENTNODEID = B.MA_CN) AND instr('01', B.MA_NHOM)>0 ORDER BY C.NODEID )) ORDER BY NODEID"
    '        ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '        If Not Globals.IsNullOrEmpty(ds) Then
    '            For Each dbRow In ds.Tables(0).Rows
    '                tm.Add(dbRow("ParentNodeID").ToString, dbRow("NodeId").ToString, dbRow("Text").ToString, dbRow("NavigateUrl").ToString)
    '            Next dbRow
    '        End If
    '        placeHolder1.Controls.Add(tm)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

End Class

