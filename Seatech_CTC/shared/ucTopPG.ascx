﻿<%--<%@ OutputCache Duration ="10" VaryByParam="*"  %>--%>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucTopPG.ascx.vb" Inherits="shared_ucTopPG" %>
<%--<%@ Register TagPrefix="tm" Namespace="OboutInc.TextMenu" Assembly="obout_TextMenu" %>--%>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<table style="border-collapse: collapse" cellspacing="0" cellpadding="0" width="100%"
    border="0">
    <tr>
        <td align="center">
            <table cellspacing="0" cellpadding="0" width="100%" border="0" height="20px">
                <tr>
                    <td align="right" background="<%=imageFolder %>bg1.gif">
                        <asp:Label ID="lblWorkingDate" runat="server" CssClass="label"></asp:Label>&nbsp;-
                        <asp:Label ID="lblNguoiDung" runat="server" CssClass="label"></asp:Label>&nbsp;-
                        <asp:Label ID="lblChucDanh" runat="server" CssClass="label"></asp:Label>&nbsp;
                        <asp:Label ID="lblDiemThu" runat="server" CssClass="label"></asp:Label>&nbsp;
                        <asp:Label Visible="false" ID="lbBDS" runat="server" CssClass="label" ForeColor="Red" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <%--<tr>
                    <td align="right">
                        
                    </td>
                </tr>--%>
            </table>
        </td>
    </tr>
    <tr>
        <td height="1px" bgcolor="#eeeeee">
        </td>
    </tr>
    <tr>
        <td align="left" style="height: 80px">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="background-color:White " align="left">
                        <%--<asp:Image ID="imgHome" runat="server" ImageUrl="~/images/pg_bg.png" />--%>
                        <asp:Image ID="imgHome" runat="server" ImageUrl="~/images/SHBLogo.png" />
                    </td>
                    <td style="background-color:White " align="right">
                        <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/abb-tax1.png" />--%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="top-menu" style="background:url(../images/menu/menu_bg.png) repeat-x; width:100%" align="left">
            <ComponentArt:Menu ID="Menu1" Orientation="Horizontal" CssClass="TopGroup" DefaultGroupCssClass="MenuGroup"
                DefaultSubGroupExpandOffsetX="-10" DefaultSubGroupExpandOffsetY="-5" DefaultItemLookId="DefaultItemLook"
                EnableViewState="true" AutoPostBackOnSelect="true" TopGroupItemSpacing="0" DefaultGroupItemSpacing="0"
                ImagesBaseUrl="~/images/menu/" ExpandDelay="100" runat="server" Width="100%">
                <ServerTemplates>
                    <ComponentArt:NavigationCustomTemplate ID="ShortcutItem">
                        <Template>
                            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-style: none;">
                                <tr>
                                    <td style="font-family: Arial; font-size: 12px;">
                                        <%# DataBinder.Eval(Container.DataItem, "Text") %>
                                    </td>
                                    <td style="font-family: Arial; font-size: 12px;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td style="font-family: Arial; font-size: 12px; color: gray;" align="right">
                                        <%# DataBinder.Eval(Container.DataItem, "KeyboardShortcut") %>
                                    </td>
                                </tr>
                            </table>
                        </Template>
                    </ComponentArt:NavigationCustomTemplate>
                </ServerTemplates>
                <ItemLooks>
                    <ComponentArt:ItemLook LookId="TopItemLook" CssClass="TopMenuItem" HoverCssClass="TopMenuItemHover" />
                    <ComponentArt:ItemLook LookId="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" />
                    <ComponentArt:ItemLook LookID="BreakItem" ImageUrl="~/images/menu/menu_sep.png" CssClass="MenuBreak" ImageHeight="34px" ImageWidth="2px" />
                </ItemLooks>
            </ComponentArt:Menu>
        </td>
    </tr>
</table>
