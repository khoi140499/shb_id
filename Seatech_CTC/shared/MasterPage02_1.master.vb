﻿Imports System.Data
Imports VBOracleLib
Imports Business
Imports System.Diagnostics
Partial Class shared_MasterPage02_1
    Inherits System.Web.UI.MasterPage

    Public imageFolder As String

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        Dim v_strRootPath As String = ConfigurationManager.AppSettings("APP_ROOT_PATH").ToString
        'Response.Write(HttpContext.Current.Request.Url.AbsolutePath.Replace("/Seatech_CTC", "~") + "  " + Session.Item("User"))
        Dim iResult As Integer = 0
        Dim strSql As String = "SELECT COUNT (1) kiemtra " & _
                               "  FROM (SELECT ma_cn " & _
                               "          FROM tcs_phanquyen " & _
                               "         WHERE ma_nhom = (SELECT ma_nhom " & _
                               "                            FROM tcs_dm_nhanvien " & _
                               "                           WHERE ma_nv = " + Session.Item("User") + ")) " & _
                               " WHERE ma_cn IN ( " & _
                               "          SELECT nodeid node " & _
                               "            FROM tcs_site_map " & _
                               "           WHERE status = 1 " & _
                               "             AND navigateurl = '" + HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") + "' " & _
                               "          UNION " & _
                               "          SELECT parentnodeid node " & _
                               "            FROM tcs_site_map " & _
                               "           WHERE status = 1 " & _
                               "             AND navigateurl = '" + HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") + "') "
        Dim a = HttpContext.Current.Request.Url.AbsolutePath
        'VBOracleLib.LogDebug.WriteLog(a,EventLogEntryType.Error)
        If HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") <> "~/pages/frmHomeNV.aspx" Then
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString())
            End If

            If iResult <= 0 Then
                Response.Redirect("~/pages/frmNoAuthorize.aspx", False)
                Exit Sub
            End If
        End If

        imageFolder = Request.ApplicationPath + "/images/"
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
    End Sub
End Class

