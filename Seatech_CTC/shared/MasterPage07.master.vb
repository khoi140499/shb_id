﻿Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports System.Data
Partial Class shared_MasterPage07
    Inherits System.Web.UI.MasterPage
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        Dim ma_nv As String = Session("User").ToString
        Dim ma_nhom = DataAccess.ExecuteReturnDataSet("select ma_nhom from tcs_dm_nhanvien where ma_nv = '" & ma_nv & "'", CommandType.Text).Tables(0).Rows(0)("ma_nhom").ToString
        Dim v_strRootPath As String = ConfigurationManager.AppSettings("APP_ROOT_PATH").ToString
        Dim iResult As Integer = 0
        Dim strSql As String = "SELECT COUNT (1) kiemtra " & _
                               "  FROM (SELECT ma_nhom,ma_cn " & _
                               "          FROM tcs_phanquyen) " & _
                               " WHERE ma_cn IN ( " & _
                               "          SELECT nodeid node " & _
                               "            FROM tcs_site_map " & _
                               "           WHERE status = 1 " & _
                               "             AND navigateurl = '" + HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") + "' " & _
                               "          UNION " & _
                               "          SELECT parentnodeid node " & _
                               "            FROM tcs_site_map " & _
                               "           WHERE status = 1 " & _
                               "             AND navigateurl = '" + HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") + "') " & _
                               " AND MA_NHOM = '" & ma_nhom & "'"

        If HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") <> "~/pages/frmHomeNV.aspx" Then
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString())
            End If

            If HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") = "~/pages/BaoLanhHQ/frmKetQuaTraCuuBLHQ.aspx" Or HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") = "~/NTDT/frmHoanThienNNTDT.aspx" Or HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") = "~/NTDT/frmTest.aspx" Then
                iResult = 1
            End If

            If iResult <= 0 Then
                Response.Redirect("~/pages/frmNoAuthorize.aspx", False)
                Exit Sub
            End If
        End If

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
    End Sub
End Class

