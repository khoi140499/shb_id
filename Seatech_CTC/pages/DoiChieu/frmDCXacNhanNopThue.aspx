﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false" CodeFile="frmDCXacNhanNopThue.aspx.vb"
    Inherits="pages_DoiChieu_frmDCXacNhanNopThue" title="Đối chiếu xác nhận nộp thuế" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
    </script>
    <style type="text/css">
        .container{width:100%;}
        .inputflat_tong {
            background-color: #ffff;
            border: 1px solid #e8e7e1;
            font-family: Arial;
            font-size: 8pt;
        }
        .inputflat_thanhcong{
            background-color: #b0ffcc;
            border: 1px solid #e8e7e1;
            font-family: Arial;
            font-size: 8pt;
        }
        .inputflat_lech{
            background-color: bisque;
            border: 1px solid #e8e7e1;
            font-family: Arial;
            font-size: 8pt;
            color:Red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" Runat="Server">
    <p class="pageTitle"> Đối chiếu cuối ngày </p>
    <div class="container">
        <table cellpadding="0" cellspacing="10" width="100%">
            <tr>
                <td align="center">
                    <table width="60%" border="0" cellpadding="2" cellspacing="1" class="form_input">
                        <tr>
                            <td align="center" class="form_label">Ngày đối chiếu</td>
                            <td class="form_control"><span style="display:block; width:80%; position:relative">
                                <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 88%; display:block; float:left; margin-right:5px"
                                onblur="CheckDate(this);" onfocus="this.select()" />
                            </span></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4" class="form_control">
                                <asp:Button ID="btnNhanDC" runat="server" Text="Nhận dữ liệu" Visible="false" /> &nbsp;&nbsp;
                                <asp:Button ID="btnGuiDC" runat="server" Text="Gửi kết quả" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
            <tr>
                <td colspan="4">
                    <table width="100%"  cellpadding="2" cellspacing="1" class="form_input">
                        <tr>
                            <td class="form_label" align="center">Loại đối chiếu</td>
                            <td class="form_label" align="center">Tổng cục thuế</td>
                            <td class="form_label" align="center">Ngân hàng SHB</td>
                        </tr>
                        <tr>
                            <td class="form_label">Tổng số điện truyền nhận</td>
                            <td class="inputflat_tong" align="center"><asp:Label ID="lblTruyen_TCT" runat="server"></asp:Label></td>
                            <td class="inputflat_tong" align="center"><asp:Label ID="lblTruyen_SHB" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="form_label">Tổng số điện truyền nhận thành công</td>
                            <td class="inputflat_thanhcong" align="center"><asp:Label ID="lblNhanTC_TCT" runat="server"></asp:Label></td>
                            <td class="inputflat_thanhcong" align="center"><asp:Label ID="lblNhanTC_SHB" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="form_label">Tổng số điện truyền nhận chênh lệch</td>
                            <td class="inputflat_lech" align="center"><asp:Label ID="lblNhanLech_TCT" runat="server"></asp:Label></td>
                            <td class="inputflat_lech" align="center"><asp:Label ID="lblNhanLech_SHB" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>
