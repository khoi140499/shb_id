﻿Imports System.Data
Imports VBOracleLib
Imports Business.BaoCao
Imports Business.XLCuoiNgay
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Partial Class pages_DoiChieu_frmDChieu02BKDC
    Inherits System.Web.UI.Page

    Private mv_strNgay As String
    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String
    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    Private Shared strMaNhom_GDV As String = ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                DM_DiemThu()
                txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                clsCommon.addPopUpCalendarToImage(btnCalendar1, txtTuNgay, "dd/mm/yyyy")
                txtDenNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                clsCommon.addPopUpCalendarToImage(btnCalendar2, txtDenNgay, "dd/mm/yyyy")
                'get_NguyenTe()
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub
    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            TCS_BC_02_DCBK()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub
#Region "Call Report"
    Private Sub TCS_BC_02_DCBK()
        Try
            Dim ds As DataSet
            mv_strLoaiBaoCao = Report_Type.TCS_BC_DC_02BKDC
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If

            ds = New DataSet
            Session.Item("WhereAll") = strWhere
            ds = buBaoCao.TCS_BC_DC_02_BKDC(strWhere)
            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            frm.TCS_Tungay = txtTuNgay.Text.ToString()
            frm.TCS_Denngay = txtDenNgay.Text.ToString()
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao, "ManPower")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



#End Region
#Region "Where Clause"
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Private Function WhereNgay() As String
        Dim strWhereNgay As String
        Dim strNgayStart As String
        Dim strNgayEnd As String
        strNgayStart = ConvertDateToNumber(txtTuNgay.Text.ToString())
        strNgayEnd = ConvertDateToNumber(txtDenNgay.Text.ToString())
        strWhereNgay = " and a.ngay_kb >=" & strNgayStart
        strWhereNgay = strWhereNgay & " and a.ngay_kb <=" & strNgayEnd


        mv_strNgay = "Ngày " & txtTuNgay.Text.ToString()
        Return strWhereNgay
    End Function
    Private Function WhereAll() As String
        Dim strWhereAll As String
        strWhereAll = WhereNgay() & WhereMaDthu() & WhereMa_NTK()
        Return strWhereAll
    End Function
#End Region
    Private Function WhereMaDthu() As String
        Dim strWhereMaNV As String = ""
        Dim strcheckHSC As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If cboDiemThu.SelectedValue <> "" Then
            strWhereMaNV = " And (cn.id= '" & cboDiemThu.SelectedValue & "') "
        Else
            If strcheckHSC <> "1" Then
                If strcheckHSC = "2" Then
                    strWhereMaNV = " And (cn.branch_id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "') "
                Else
                    strWhereMaNV = " And (cn.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "') "
                End If

            End If
        End If

        Return strWhereMaNV
    End Function
    Public Function LOAD_MN() As String
        Dim str_return As String = ""
        Dim strSQL_MaNhom As String = "select ma_nhom from tcs_dm_nhanvien where ma_nv='" & Session.Item("User").ToString & "'"
        Dim dt As New DataTable
        dt = DataAccess.ExecuteToTable(strSQL_MaNhom)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            str_return = dt.Rows(0)("ma_nhom").ToString
        End If
        Return str_return
    End Function
    Private Function WhereMa_NTK() As String
        Dim strWhereMa_NTK As String = ""
        If Not cboMA_NTK.SelectedValue().Trim().Equals("") Then
            strWhereMa_NTK = " And (a.ma_ntk = " & cboMA_NTK.SelectedValue().Trim() & ") "
        End If
        Return strWhereMa_NTK
    End Function
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        Dim str_Ma_Nhom As String = ""
        Dim dt As New DataTable
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        str_Ma_Nhom = LOAD_MN()
        If str_Ma_Nhom = strMaNhom_GDV Then
            strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a where a.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "' order by a.id"
        Else
            If strcheckHSC = "1" Then
                'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
                ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
                strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a order by a.id "

            ElseIf strcheckHSC = "2" Then

                strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' order by a.id"
            Else
                strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' order by a.id"
            End If
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If str_Ma_Nhom = strMaNhom_GDV Then
                dt = dsDiemThu.Tables(0)
                cboDiemThu.DataSource = dt
                cboDiemThu.DataTextField = "name"
                cboDiemThu.DataValueField = "id"
                cboDiemThu.DataBind()
            Else
                clsCommon.ComboBoxWithValue(cboDiemThu, dsDiemThu, 1, 0, "Tất cả")
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub
End Class
