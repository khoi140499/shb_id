﻿Imports Business_HQ.Common
Imports System.Data
Imports VBOracleLib
Imports Business_HQ

Partial Class pages_DoiChieu_frmDCPhiBoNganh
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            LoadForm()
            txtNgayDC.Text = DateTime.Now.ToString("dd/MM/yyyy")
            clsCommon.addPopUpCalendarToImage(btnCalendar1, txtNgayDC, "dd/mm/yyyy")
        End If
    End Sub

#Region "Function"

    Sub LoadForm()
        loadTTinDChieuTuDong()
        LoadTTDCTheoNgay(ConvertDateToNumber(Date.Now.ToString("dd/MM/yyyy")))
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Request.QueryString("errmsg") Is Nothing Or Request.QueryString("errmsg") = "" Then
            If Not Request.QueryString("msg") Is Nothing Then
                lblSuccess.Text = Request.QueryString("msg")
            End If
        Else
            lblError.Text = Request.QueryString("errmsg")
        End If
        If Not IsPostBack Then
            'Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
            ResetField()
        End If
    End Sub
    Public Shared Function ConvertDateToNumber(ByVal strDate As String) As Long
        '-----------------------------------------------------
        ' Mục đích: Chuyển một string ngày tháng 'dd/MM/yyyy' --> dạng số 'yyyyMMdd'.
        ' Tham số: string đầu vào
        ' Giá trị trả về: 
        ' Ngày viết: 18/10/2007
        ' Người viết: Lê Hồng Hà
        ' ----------------------------------------------------
        Dim arr() As String

        Try
            Dim sep As String = "/"
            strDate = strDate.Trim()
            If Len(strDate) < 10 Then Return 0
            arr = strDate.Split(sep)
            sep = "/"
            arr(0) = arr(0).PadLeft(2, "0")
            arr(1) = arr(1).PadLeft(2, "0")
            If arr(2).Length = 2 Then
                arr(2) = "20" & arr(2)
            End If

        Catch ex As Exception
            Return CLng(Now().ToString("yyyyMMdd"))
        End Try

        Return Long.Parse(arr(2) & arr(1) & arr(0))
    End Function
    Sub ResetField()
        txtNgayDC.Text = ""
        cboLoaiGD.SelectedIndex = 0
        cbNhanDL.Checked = False
    End Sub
#End Region

    Protected Sub btnGuiDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuiDC.Click

        Dim errorCode As String = ""
        Dim errorMSG As String = ""
        Dim ngay_dc As DateTime = New DateTime()
        Try

            If txtNgayDC.Text.Trim.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Hãy nhập ngày đối chiếu.")
                Exit Sub
            End If
            ngay_dc = DateTime.ParseExact(txtNgayDC.Text, "dd/MM/yyyy", Nothing)

            Dim test As String = cboLoaiGD.SelectedValue
            If cboLoaiGD.SelectedValue.Equals("803") Then
                CustomsServiceV3.ProcessMSG.guiTDDoiChieuPhi(ngay_dc, errorCode, errorMSG)
            ElseIf cboLoaiGD.SelectedValue.Equals("903") Then
                CustomsServiceV3.ProcessMSG.guiTDDoiChieuPhiHuy(ngay_dc, errorCode, errorMSG)
            End If

            If errorCode = "0" Then
                clsCommon.ShowMessageBox(Me, "Gửi đối chiếu thành công")
            Else
                clsCommon.ShowMessageBox(Me, "Gửi đối chiếu không thành công. Lỗi: " + errorMSG)
            End If
            LoadTTDCTheoNgay(ConvertDateToNumber(Date.Now.ToString("dd/MM/yyyy")))
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Gửi đối chiếu thất bại. Lỗi: " + errorMSG)
        End Try

    End Sub

    Protected Sub btnNhanDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNhanDC.Click
        Dim errorCode As String = ""
        Dim errorMSG As String = ""
        Dim ngay_dc As DateTime = New DateTime()
        Try

            If txtNgayDC.Text.Trim.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Hãy nhập ngày đối chiếu.")
                Exit Sub
            End If

            ngay_dc = DateTime.ParseExact(txtNgayDC.Text, "dd/MM/yyyy", Nothing)
            CustomsServiceV3.ProcessMSG.getKetQuaDC(ngay_dc, "03", errorCode, errorMSG)

            If errorCode = "0" Then
                clsCommon.ShowMessageBox(Me, "Nhận kết quả đối chiếu thành công")
            Else
                clsCommon.ShowMessageBox(Me, "Nhận kết quả đối chiếu không thành công. Lỗi: " + errorMSG)
            End If
            LoadTTDCTheoNgay(ConvertDateToNumber(Date.Now.ToString("dd/MM/yyyy")))
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Nhận kết quả đối chiếu thất bại. Lỗi: " + errorMSG)
        End Try
    End Sub

    Protected Sub loadTTinDChieuTuDong()
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strTrangThai As String = ""
        Try
            strSQL = "SELECT a.stt, a.ngay_th, a.trang_thai, a.loai_dc, a.error_msg, " & _
            "a.error_code  FROM tcs_log_auto_dc a WHERE a.stt IN (SELECT max(b.stt) FROM tcs_log_auto_dc b " & _
            "where b.ngay_th >= trunc(sysdate) GROUP BY b.loai_dc) "

            dt = DatabaseHelp.ExecuteToTable(strSQL)

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    If row("LOAI_DC").ToString() = "42" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblSend41.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "44" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblReceive44.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "52" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblSend51.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "54" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblReceive54.Text = strTrangThai
                    
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub getTenTrangThai(ByVal strStatus As String, ByVal strErrCode As String, ByRef strStatusName As String)
        If strStatus = "0" Then
            strStatusName = "Thành Công"
        ElseIf strStatus = "1" Then
            strStatusName = "Không thành Công (Mã lỗi: " & strErrCode & ") - Hãy thực hiện đối chiếu thủ công"
        Else
            strStatusName = strStatus & "(Mã lỗi: " & strErrCode
        End If
    End Sub

    Protected Sub LoadTTDCTheoNgay(ByVal strDate As String)
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strTrangThai As String = ""
        Try
            strSQL = "select * from tcs_qly_dc where id in ( SELECT   MAX (id)" & _
            " FROM   tcs_qly_dc" & _
            " WHERE   TO_CHAR (ngay_dc, 'yyyymmdd') = '" & strDate & "'" & _
            " GROUP BY   loai_gd)"
            dt = DatabaseHelp.ExecuteToTable(strSQL)

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    If row("LOAI_GD").ToString() = "803" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblSend41.Text = "Gửi thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblSend41.Text = "Gửi không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "800" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblReceive44.Text = "Nhận thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblReceive44.Text = "Nhận không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "MSG51" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblSend51.Text = "Gửi thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblSend51.Text = "Gửi không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "MSG53" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblReceive54.Text = "Nhận thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblReceive54.Text = "Nhận không thành công"
                        End If

                    End If
                Next
            Else
                lblSend41.Text = "Chưa đối chiếu"
                lblSend51.Text = "Chưa đối chiếu"
                
                lblReceive44.Text = "Chưa đối chiếu"
                lblReceive54.Text = "Chưa đối chiếu"
                
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class
