﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDCPhiBoNganh.aspx.vb"
    Inherits="pages_DoiChieu_frmDCPhiBoNganh" title="Gửi đối chiếu phí bộ ngành" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
    </script>
    <style type="text/css">
        .container{width:100%;}
        .inputflat_tong {
            background-color: #ffff;
            border: 1px solid #e8e7e1;
            font-family: Arial;
            font-size: 8pt;
        }
        .inputflat_thanhcong{
            background-color: #b0ffcc;
            border: 1px solid #e8e7e1;
            font-family: Arial;
            font-size: 8pt;
        }
        .inputflat_lech{
            background-color: bisque;
            border: 1px solid #e8e7e1;
            font-family: Arial;
            font-size: 8pt;
            color:Red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" Runat="Server">
    <p class="pageTitle"> Gửi đối chiếu phí bộ ngành </p>
    <div class="container">
        <table cellpadding="0" cellspacing="10" width="100%">
            <tr>
                <td align="center">
                    <table width="60%" border="0" cellpadding="2" cellspacing="1" class="form_input">
                        <tr>
                            <td align="center" class="form_label">Ngày đối chiếu</td>
                            <td class="form_control"><span style="display:block; width:80%; position:relative">
                                <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 88%; display:block; float:left; margin-right:5px"
                                onblur="CheckDate(this);" onfocus="this.select()" />
                            </span></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4" class="form_control">
                                <asp:Button ID="btnGuiDC" runat="server" Text="Gửi đối chiếu" /> &nbsp;&nbsp;
                                <asp:Button ID="btnNhanDC" runat="server" Text="Nhận kết quả đối chiếu" />
                                
                                <asp:Button ID="btnGuiDCHuy" runat="server" Text="Gửi đối chiếu hủy" /> &nbsp;&nbsp;
                                <asp:Button ID="btnNhanDCHuy" runat="server" Text="Nhận kết quả đối chiếu hủy" />
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
        </table>
    </div>
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>
--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

<script language="javascript" type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
                }
                textbox.value = str
        }
    </script>

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>

    <style type="text/css">
        .ButtonCommand
        {
            height: 26px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height: 450px">
        <tr>
            <td valign="top">
                <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="pageTitle">
                            <asp:Label ID="Label1" runat="server">ĐỐI CHIỀU CUỐI NGÀY</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="errorMessage">
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: Blue" align="left">
                            <asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td>
                                        <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                            <tr align="left">
                                                <td class="form_label" style="width: 33%">
                                                    <asp:Label ID="Label10" runat="server" Text="Ngày đối chiếu" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="form_control" style="width: 51%">
                                                    <asp:TextBox runat="server" ID="txtNgayDC" Width="287px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                        onblur="CheckDate(this);" MaxLength="10" Height="21px"></asp:TextBox>
                                                    <asp:Image ID="btnCalendar1" ImageAlign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"
                                                        TabIndex="20" />
                                                </td>
                                            </tr>
                                            <tr align="left">
                                                <td class="form_label" style="width: 33%">
                                                    <asp:Label ID="Label3" runat="server" Text="Giao dịch" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="form_control" style="width: 51%">
                                                    <asp:DropDownList ID="cboLoaiGD" Width="305px" runat="server" CssClass="inputflat">
                                                        <asp:ListItem Text="Chọn giao dịch" Value="M00" Selected="True"></asp:ListItem>                                                       
                                                        <asp:ListItem Text="803-Thông điệp gửi yêu cầu đối chiếu nộp thuế, phí, lệ phí của bộ ngành" Value="803"></asp:ListItem>                                                        
                                                        <asp:ListItem Text="903-Thông điệp hỏi kết quả đối chiếu huỷ giao dịch" Value="903"></asp:ListItem>                                                       
                                                        
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr align="left" style="display: none">
                                                <td class="form_label" style="width: 33%; display: none">
                                                    <asp:Label ID="Label2" runat="server" Text="Thực hiện nhận dữ liệu" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="form_control" style="width: 51%">
                                                    <asp:CheckBox ID="cbNhanDL" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="center" style="height: 40px">
                            <asp:Button ID="btnGuiDC" runat="server" CssClass="ButtonCommand" Text="[Gửi bảng kê ĐC]" />&nbsp;&nbsp;
                            <asp:Button ID="btnNhanDC" runat="server" CssClass="ButtonCommand" Text="[Nhận kết quả ĐC]" />&nbsp;&nbsp;
                            <asp:Button ID="btnGuiDCHuy" runat="server" CssClass="ButtonCommand" Visible="false"
                                Text="[Thoát]"></asp:Button>&nbsp;&nbsp;
                            <asp:Button ID="btnNhanDCHuy" runat="server" CssClass="ButtonCommand" Text="[Truy vấn ĐC]" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <table border="1" width="100%">
                    <tr>
                        <td colspan="3">
                            <b>THÔNG TIN ĐỐI CHIẾU</b>
                        </td>
                    </tr>
                    <tr align="left">
                        <td rowspan="2" width="30%">
                            <asp:Label ID="Label7" runat="server" Text="Xác nhận nộp thuế XNK" CssClass="label"></asp:Label>
                        </td>
                        <td width="20%">
                            <asp:Label ID="Label5" runat="server" Text="Gửi bảng kê" CssClass="label"></asp:Label>
                        </td>
                        <td width="40%">
                            <asp:Label ID="lblSend41" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <asp:Label ID="Label8" runat="server" Text="Nhận kế quả ĐC" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblReceive44" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td rowspan="2">
                            <asp:Label ID="Label4" runat="server" Text="Hủy xác nhận nộp thuế XNK" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Gửi bảng kê" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblSend51" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <asp:Label ID="Label11" runat="server" Text="Nhận kế quả ĐC" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblReceive54" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <%--<tr align="left">
                        <td rowspan="2">
                            <asp:Label ID="Label13" runat="server" Text="Bảo lãnh thuế XNK" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label14" runat="server" Text="Gửi bảng kê" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblSend81" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <asp:Label ID="Label16" runat="server" Text="Nhận kế quả ĐC" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblReceive84" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left"  >
                        <td rowspan="2">
                            <asp:Label ID="Label9" runat="server" Text="Hủy bảo lãnh thuế XNK" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label12" runat="server" Text="Gửi bảng kê" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblSend91" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left"  >
                        <td>
                            <asp:Label ID="Label17" runat="server" Text="Nhận kế quả ĐC" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblRecieve94" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>--%>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
