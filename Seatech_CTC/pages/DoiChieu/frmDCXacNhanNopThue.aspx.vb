﻿Imports ETAX_GDT

Partial Class pages_DoiChieu_frmDCXacNhanNopThue
    Inherits System.Web.UI.Page

    Protected Sub btnGuiDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuiDC.Click
        Dim tong_tc As Integer = 0
        Dim tong_tn_tct As Integer = 0
        Dim tong_tn_nh As Integer = 0
        Dim tong_lech_tct As Integer = 0
        Dim tong_lech_nh As Integer = 0
        Dim errorCode As String = ""
        Dim errorMSG As String = ""
        Try
            Dim guibaocao As ProcessBaoCaoDoiChieu_NH_TTXL = New ProcessBaoCaoDoiChieu_NH_TTXL()

            guibaocao.BaoCaoDoiChieu(txtTuNgay.Value, tong_tc, tong_tn_tct, tong_tn_nh, tong_lech_tct, tong_lech_nh, errorCode, errorMSG)

            If errorCode = "00" Then
                lblNhanTC_SHB.Text = tong_tc
                lblNhanTC_TCT.Text = tong_tc
                lblTruyen_SHB.Text = tong_tn_nh
                lblTruyen_TCT.Text = tong_tn_tct
                lblNhanLech_SHB.Text = tong_lech_nh
                lblNhanLech_TCT.Text = tong_lech_tct

                clsCommon.ShowMessageBox(Me, "Gửi báo cáo thành công")

            ElseIf errorCode = "02" Then
                clsCommon.ShowMessageBox(Me, "Chưa nhận được dữ liệu đối chiếu từ TCT")
            Else
                clsCommon.ShowMessageBox(Me, "Gửi cáo cáo đối chiếu không thành công. Lỗi: " + errorMSG)
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Gửi cáo cáo thất bại. Lỗi: " + errorMSG)
        End Try

    End Sub
End Class
