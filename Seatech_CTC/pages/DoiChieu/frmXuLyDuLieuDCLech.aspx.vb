﻿Imports System.Data
Imports VBOracleLib
Partial Class pages_DoiChieu_frmXuLyDuLieuDCLech
    Inherits System.Web.UI.Page

    
    Protected Sub loaddata()
        Try
            Dim sql As String = "select DISTINCT(r3.so_ctu), r3.mst, r3.code_ctiet, r3.tt_chuyen, to_char(r3.ngay_chuyen, 'dd/MM/yyyy') ngay_chuyen, hdr.ngay_dc from ntdt_baocao_ctiet_r3 r3, ntdt_baocao_hdr hdr where r3.id_bcao = hdr.id_bcao and r3.code_ctiet in('2','3') and tthai <> '04' and to_char(hdr.ngay_dc,'dd/MM/yyyy') = '" & txtTuNgay.Value & "'"
            Dim where As String = ""
            If txtSO_CT.Text.Trim.Length > 0 Then
                where = " and r3.so_ctu='" & txtSO_CT.Text & "'"
            End If
            If txtMST.Text.Trim.Length > 0 Then
                where &= " and r3.mst='" & txtMST.Text & "'"
            End If
            sql &= where

            Dim dt As DataTable = New DataTable()
            dt = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text).Tables(0)
            dtgdata.DataSource = dt
            dtgdata.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        loaddata()
    End Sub

    Protected Sub btnGuiLai_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuiLai.Click

        Dim i As Integer
        Dim strSoCT As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        Dim thongbao As ETAX_GDT.ProcessXuLyTB = New ETAX_GDT.ProcessXuLyTB()
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                Dim loai As String = Trim(dtgdata.Items(i).Cells(0).Text)
                strSoCT = Trim(dtgdata.Items(i).Cells(3).Text)
                Try
                    Dim err_code As String = ""
                    Dim err_msg As String = ""
                    thongbao.GuiThongBaoTungCT(strSoCT, err_code, err_msg)

                    If err_code = "0" Then
                        Dim sql As String = "update tcs_ctu_ntdt_hdr set tt_chuyen = (tt_chuyen+1), ngay_chuyen = TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy") & "', 'dd/MM/yyyy') where so_ctu='" & strSoCT & "'"
                        DataAccess.ExecuteNonQuery(sql, CommandType.Text)
                        sql = "update ntdt_baocao_ctiet_r3 set tt_chuyen = (tt_chuyen+1), ngay_chuyen = TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy") & "', 'dd/MM/yyyy') where so_ctu='" & strSoCT & "'"
                        DataAccess.ExecuteNonQuery(sql, CommandType.Text)
                    Else
                        clsCommon.ShowMessageBox(Me, "Gửi lại chứng từ lệch thất bại! " & err_msg)
                    End If


                Catch ex As Exception
                    clsCommon.ShowMessageBox(Me, "Gửi lại chứng từ lệch thất bại! " & ex.Message)
                End Try
                v_count += 1
            End If
        Next
        loaddata()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Gửi lại chứng từ lệch thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để gửi lại!")
        End If


    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        loaddata()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub btnHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHuy.Click
        Dim i As Integer
        Dim strSoCT As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strSoCT = Trim(dtgdata.Items(i).Cells(3).Text)
                Dim sql As String = "update tcs_ctu_ntdt_hdr set tthai_ctu = '04' where so_ctu='" & strSoCT & "'"
                Try
                    DataAccess.ExecuteNonQuery(sql, CommandType.Text)
                    sql = "update ntdt_baocao_ctiet_r3 set tthai='04' where so_ctu='" & strSoCT & "'"
                    DataAccess.ExecuteNonQuery(sql, CommandType.Text)
                Catch ex As Exception
                    clsCommon.ShowMessageBox(Me, "Hủy chứng từ lệch thất bại! " & ex.Message)
                End Try
            v_count += 1
            End If
        Next
        loaddata()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Hủy chứng từ lệch thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để hủy!")
        End If
    End Sub
End Class
