﻿Imports System.Data
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.HeThong
Imports Business.Common
Imports CustomsService


Partial Class pages_DoiChieu_frmDChieuBLHQ
    Inherits System.Web.UI.Page
    Private Const str_Ma_BL_TK As String = "31"
    Private Const str_Ma_BL_VD As String = "32"
    Private Const str_Ma_BL_Chung As String = "33"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            LoadForm()
            txtNgayDC.Text = DateTime.Now.ToString("dd/MM/yyyy")
            clsCommon.addPopUpCalendarToImage(btnCalendar1, txtNgayDC, "dd/mm/yyyy")
        End If

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        ViewState("update") = Session("update")
    End Sub
    Public Shared Sub ShowMessageBoxConfirmDoiChieu(ByVal pPage As System.Web.UI.Page, ByVal pMessage As String, ByVal strDate As String)
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "var result = confirm('" & pMessage & "');"
        strScript += "if (result) {"
        strScript += " document.getElementById('ctl00_MstPg05_MainContent_doichieuConfirm').value = 'OK';"
        strScript += " document.getElementById('ctl00_MstPg05_MainContent_cmdDC').click();"
        strScript += "}else { "
        strScript += " document.getElementById('ctl00_MstPg05_MainContent_doichieuConfirm').value = '';}"
        strScript += "</script>"
        If Not pPage.ClientScript.IsStartupScriptRegistered("clientScript") Then
            pPage.ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
        End If
    End Sub
    Protected Sub cmdDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDC.Click
        Dim strErrMessage As String = ""
        Dim strMessage As String = ""
        Dim strNgayDC As String = txtNgayDC.Text
        Dim strGD As String = cboLoaiGD.SelectedValue
        Dim bNhanDL As Boolean = cbNhanDL.Checked
        Dim strLoaiBL As String = ddlLoaiBL.SelectedValue
        Try
            'Validate

            lblError.Text = ""
            lblSuccess.Text = ""
            If strNgayDC = "" Then
                lblError.Text = "Bạn chưa nhập ngày đối chiếu"
                txtNgayDC.Focus()
                Return
            ElseIf strGD = "M00" Then
                lblError.Text = "Bạn chưa chọn loại giao dịch"
                cboLoaiGD.Focus()
                Return
            End If
            If strLoaiBL = "M30" Then
                lblError.Text = "Bạn chưa chọn loại Bảo lãnh"
                Return
            End If
            Dim dtNgayDC As Date = DateTime.ParseExact(strNgayDC, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat)
            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            If TestDoiChieuLai(txtNgayDC.Text, strGD) And doichieuConfirm.Value <> "OK" Then

                ShowMessageBoxConfirmDoiChieu(Me, "Bảng kê đã đuợc gửi thành công truớc đó. Bạn có muốn gửi lai bảng kê không ?", txtNgayDC.Text)
                Return

            Else
                doichieuConfirm.Value = ""
                'Gui bang ke doi chieu
                If strGD = "M81" Then
                    CustomsServiceV3.ProcessMSG.getMSG82(dtNgayDC, strLoaiBL, strErrorNum, strErrorMes)
                ElseIf strGD = "M91" Then
                    CustomsServiceV3.ProcessMSG.getMSG92(dtNgayDC, strLoaiBL, strErrorNum, strErrorMes)
                End If
                'Xu ly xac nhan HQ
                If strErrorNum = "0" Then
                    strErrMessage = "Gửi bảng kê đối chiếu(" + strGD + ") thành công."
                Else
                    strErrMessage = "Gửi bảng kê đối chiếu không thành công (Lỗi từ HQ: " + strErrorNum + " - " + strErrorMes + ")"
                End If
            End If
        Catch ex As Exception
            strErrMessage = ex.Message
        End Try
        'ResetField()
        If strErrMessage <> "" Then
            lblError.Text = strErrMessage
        Else
            Response.Redirect(Request.Url.AbsolutePath.ToString() & "?msg=" & strMessage & "&errmsg=" & strErrMessage, False)
        End If
        'Response.Redirect(Request.Url.AbsolutePath.ToString() & "?msg=" & strMessage & "&errmsg=" & strErrMessage, False)
        LoadTTDCTheoNgay(ConvertDateToNumber(txtNgayDC.Text))
    End Sub
#Region "Function"
    Protected Function TestDoiChieuLai(ByVal strDate As String, ByVal strGD As String) As Boolean
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strTrangThai As String = ""
        Dim strLoaiGD As String = ""
        If strGD = "M81" Then
            strLoaiGD = "901"
        ElseIf strGD = "M91" Then
            strLoaiGD = "801"
        End If
        Try
            strSQL = "select * from tcs_qly_dc where id in ( SELECT   MAX (id)" & _
            " FROM   tcs_qly_dc" & _
            " WHERE   TO_CHAR (ngay_dc, 'yyyymmdd') = '" & strDate & "' AND loai_gd = '" & strLoaiGD & "'" & " AND ERRNUM = '0' )"
            dt = DatabaseHelp.ExecuteToTable(strSQL)

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                Return True
            End If
        Catch ex As Exception

        End Try
        Return False
    End Function
    Sub LoadForm()
        loadTTinDChieuTuDong()
        LoadTTDCTheoNgay(ConvertDateToNumber(Date.Now.ToString("dd/MM/yyyy")))
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Request.QueryString("errmsg") Is Nothing Or Request.QueryString("errmsg") = "" Then
            If Not Request.QueryString("msg") Is Nothing Then
                lblSuccess.Text = Request.QueryString("msg")
            End If
        Else
            lblError.Text = Request.QueryString("errmsg")
        End If
        If Not IsPostBack Then
            'Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
            ResetField()
        End If
    End Sub
    Sub ResetField()
        txtNgayDC.Text = ""
        cboLoaiGD.SelectedIndex = 0
        cbNhanDL.Checked = False
    End Sub
#End Region
    Protected Sub cmdNhanKQDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNhanKQDC.Click
        Dim strErrMessage As String = ""
        Dim strMessage As String = ""
        Try
            Dim strNgayDC As String = txtNgayDC.Text
            Dim dtNgayDC As Date = DateTime.ParseExact(strNgayDC, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat)
            Dim strGD As String = cboLoaiGD.SelectedValue
            Dim bNhanDL As Boolean = cbNhanDL.Checked
            Dim strLoaiBL As String = ddlLoaiBL.SelectedValue
            lblError.Text = ""
            lblSuccess.Text = ""
            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            'Nhan ket qua doi chieu tu HQ
            If strLoaiBL = "M30" Then
                lblError.Text = "Bạn chưa chọn loại Bảo lãnh"
                Return
            End If
            Dim strGDNhanDL As String = ""
            If strGD = "M81" Then
                strGDNhanDL = "M84"
                If strLoaiBL = "31" Then
                    CustomsServiceV3.ProcessMSG.getMSG84(dtNgayDC, strErrorNum, strErrorMes)
                ElseIf strLoaiBL = "32" Then
                    CustomsServiceV3.ProcessMSG.getMSG86(dtNgayDC, strErrorNum, strErrorMes)
                Else
                    CustomsServiceV3.ProcessMSG.getMSG88(dtNgayDC, strErrorNum, strErrorMes)
                End If
            ElseIf strGD = "M91" Then
                strGDNhanDL = "M94"
                CustomsServiceV3.ProcessMSG.getMSG94(dtNgayDC, strLoaiBL, strErrorNum, strErrorMes)
            End If
            'Xu ly xac nhan HQ
            If strErrorNum = "0" Then
                strErrMessage = "Đã gửi bảng kê (" + strGD + ") và nhận dữ liệu đối chiếu (" + strGDNhanDL + ") từ Hải Quan thành công."
            Else
                strErrMessage = "Nhận dữ liệu đối chiếu từ Hải Quan không thành công (Lỗi từ HQ: " + strErrorNum + " - " + strErrorMes + ")"
            End If

        Catch ex As Exception
            strErrMessage = ex.Message
        End Try
        'ResetField()
        If strErrMessage <> "" Then
            lblError.Text = strErrMessage
        Else
            Response.Redirect(Request.Url.AbsolutePath.ToString() & "?msg=" & strMessage & "&errmsg=" & strErrMessage, False)
        End If
        LoadTTDCTheoNgay(ConvertDateToNumber(txtNgayDC.Text))
        'Response.Redirect(Request.Url.AbsolutePath.ToString() & "?msg=" & strMessage & "&errmsg=" & strErrMessage, False)
    End Sub
    Protected Sub loadTTinDChieuTuDong()
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strTrangThai As String = ""
        Try
            strSQL = "SELECT a.stt, a.ngay_th, a.trang_thai, a.loai_dc, a.error_msg, " & _
            "a.error_code  FROM tcs_log_auto_dc a WHERE a.stt IN (SELECT max(b.stt) FROM tcs_log_auto_dc b " & _
            "where b.ngay_th >= trunc(sysdate) GROUP BY b.loai_dc) "

            dt = DatabaseHelp.ExecuteToTable(strSQL)

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    If row("LOAI_DC").ToString() = "42" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblSend41.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "44" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblReceive44.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "52" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblSend51.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "54" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblReceive54.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "82" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblSend81.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "84" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblReceive84.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "92" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblSend91.Text = strTrangThai
                    ElseIf row("LOAI_DC").ToString() = "94" Then
                        getTenTrangThai(row("TRANG_THAI").ToString(), row("ERROR_CODE").ToString(), strTrangThai)
                        lblRecieve94.Text = strTrangThai
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub getTenTrangThai(ByVal strStatus As String, ByVal strErrCode As String, ByRef strStatusName As String)
        If strStatus = "0" Then
            strStatusName = "Thành Công"
        ElseIf strStatus = "1" Then
            strStatusName = "Không thành Công (Mã lỗi: " & strErrCode & ") - Hãy thực hiện đối chiếu thủ công"
        Else
            strStatusName = strStatus & "(Mã lỗi: " & strErrCode
        End If
    End Sub
    Protected Sub LoadTTDCTheoNgay(ByVal strDate As String)
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strTrangThai As String = ""
        Try
            strSQL = "select * from tcs_qly_dc where id in ( SELECT   MAX (id)" & _
            " FROM   tcs_qly_dc" & _
            " WHERE   TO_CHAR (ngay_dc, 'yyyymmdd') = '" & strDate & "'" & _
            " GROUP BY   loai_gd)"
            dt = DatabaseHelp.ExecuteToTable(strSQL)

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    If row("LOAI_GD").ToString() = "MSG41" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblSend41.Text = "Gửi thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblSend41.Text = "Gửi không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "MSG43" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblReceive44.Text = "Nhận thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblReceive44.Text = "Nhận không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "MSG51" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblSend51.Text = "Gửi thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblSend51.Text = "Gửi không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "MSG53" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblReceive54.Text = "Nhận thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblReceive54.Text = "Nhận không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "MSG81" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblSend81.Text = "Gửi thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblSend81.Text = "Gửi không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "MSG83" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblReceive84.Text = "Nhận thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblReceive84.Text = "Nhận không thành công"
                        End If
                        'Nhamlt them ----------
                    ElseIf row("LOAI_GD").ToString() = "MSG84" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblReceive84.Text = "Nhận thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblReceive84.Text = "Nhận không thành công"
                        End If
                        '----------------
                    ElseIf row("LOAI_GD").ToString() = "MSG91" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblSend91.Text = "Gửi thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblSend91.Text = "Gửi không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "MSG93" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblRecieve94.Text = "Nhận thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblRecieve94.Text = "Nhận không thành công"
                        End If
                    End If
                Next
            Else
                lblSend41.Text = "Chưa đối chiếu"
                lblSend51.Text = "Chưa đối chiếu"
                lblSend81.Text = "Chưa đối chiếu"
                lblSend91.Text = "Chưa đối chiếu"
                lblReceive44.Text = "Chưa đối chiếu"
                lblReceive54.Text = "Chưa đối chiếu"
                lblReceive84.Text = "Chưa đối chiếu"
                lblRecieve94.Text = "Chưa đối chiếu"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub cmdTruyVan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTruyVan.Click
        LoadTTDCTheoNgay(ConvertDateToNumber(txtNgayDC.Text))
    End Sub
End Class
