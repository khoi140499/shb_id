﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmDChieu02BKDC.aspx.vb" Inherits="pages_DoiChieu_frmDChieu02BKDC"
    Title="Bảng kê đối chiếu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
                }
                textbox.value = str
        }
    </script>

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="80%" border="0" style="height:400px">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">BÁO CÁO ĐỐI CHIẾU BẢNG KÊ</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                <tr align="left">
                                    <td width='20%' class="form_label">
                                        <asp:Label ID="Label10" runat="server" Text="Từ ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='35%' class="form_control">
                                        <asp:TextBox runat="server" ID="txtTuNgay" Width="120px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"></asp:TextBox>
                                        <asp:Image ID="btnCalendar1" ImageAlign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"
                                            TabIndex="20" />
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="Label2" runat="server" Text="Đến ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox runat="server" ID="txtDenNgay" Width="120px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"></asp:TextBox>
                                        <asp:Image ID="btnCalendar2" ImageAlign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"
                                            TabIndex="20" />
                                    </td>
                                </tr>
                                <tr align="left" >
                                    <td class="form_label" >
                                        <asp:Label ID="Label3" runat="server" Text="Chi nhánh(PGD)" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control"  style="width:30%">
                                        <asp:DropDownList ID="cboDiemThu" Width="150px" runat="server" CssClass="inputflat"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td >
                                    <td class="form_label" >
                                    <asp:Label ID="Label9" runat="server" Text="Mã nhóm tài khoản" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_label" >
                                     <asp:DropDownList ID="cboMA_NTK" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Tài khoản thu NSNN" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Tài khoản tạm thu" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <asp:Button ID="cmdIn" runat="server" CssClass="ButtonCommand" Text="[In BC]"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand" Text="[Thoát]">
                </asp:Button>&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
