﻿Imports Business_HQ
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business_HQ.NewChungTu
Imports System.Xml
Imports GIPBankService
Imports System.IO


Partial Class pages_ChungTu_frmTraCuuTK_HQ
    Inherits System.Web.UI.Page
    Private mv_objUser As New CTuUser
    Private Shared strMaNhom_GDV As String = ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then

        End If
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))
        End If

        divKQNK.Visible = False
        divKQXK.Visible = False

    End Sub

    Protected Sub btnTraCuu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTraCuu.Click

        If txtSo_TK_TK.Value.Trim().Length <= 0 Then
            clsCommon.ShowMessageBox(Me, "Hãy nhập số tờ khai cần tra cứu")
            Exit Sub
        End If

        Dim strXmlResult As String = "" '"<Customs><Header><Application_Name>Payment</Application_Name><Application_Version>3.0</Application_Version><Sender_Code>HQ</Sender_Code><Sender_Name>Tổng cục Hải quan</Sender_Name><Message_Version>2.0</Message_Version><Message_Type>206</Message_Type><Message_Name>Thông điệp trả lời thông tin tờ khai nhập khẩu</Message_Name><Transaction_Date>2020-06-04T11:36:03</Transaction_Date><Transaction_ID>da6af933-e36f-49c7-aa06-b04f1b0e8eb8</Transaction_ID><Request_ID>4335383b-57b2-4b1a-b470-70ce30bbc005</Request_ID></Header><Data><MA_HQ>27F1</MA_HQ><TEN_HQ>CTHANHHOA</TEN_HQ><MA_LH>A12</MA_LH><MA_XN>N</MA_XN><NGAY_DK>2018-11-13T10:32:24</NGAY_DK><SO_TK>102328377060</SO_TK><SO_TK_DAU_TIEN>102328377060</SO_TK_DAU_TIEN><SO_TK_TAM_NHAP_TX/><THOI_HAN_TAI_NHAP_TAI_XUAT/><NGAY_THAY_DOI_DK/><NGAY_THAY_DOI_KT/><NGAY_HOAN_THANH_KT>2018-11-13T14:59:08</NGAY_HOAN_THANH_KT><NGAY_THONG_QUAN>2018-11-14T09:15:38</NGAY_THONG_QUAN><NGAY_HH_QUA_KVGS>2018-11-16T05:40:39</NGAY_HH_QUA_KVGS><MA_PHAN_LOAI_KT>2</MA_PHAN_LOAI_KT><NGUOI_NHAP_KHAU><MA_SO_THUE>0100103866005</MA_SO_THUE><TEN>NHà MáY ô Tô VEAM-TổNG CôNG TY MáY ĐộNG LựC Và MáY NôNG NGHIệP VIệT NAM-CTCP</TEN><DIA_CHI_1>Bắc Sơn - Bỉm Sơn - Thanh Hóa - Việt Nam</DIA_CHI_1></NGUOI_NHAP_KHAU><NGUOI_UY_THAC_XNK><MA_SO_THUE/><TEN/></NGUOI_UY_THAC_XNK><NGUOI_XUAT_KHAU><MA_SO_THUE/><TEN>SHANDONG TANGJUN OULING AUTOMOBILE MANUFACTURE CO.,LTD</TEN><DIA_CHI_1>NO. ZICHUAN ECONOMIC AND DEVELOPMEN</DIA_CHI_1><DIA_CHI_2>ZIBO</DIA_CHI_2><DIA_CHI_3>SHANDONG</DIA_CHI_3><DIA_CHI_4>CHINA</DIA_CHI_4><MA_NUOC>CN</MA_NUOC></NGUOI_XUAT_KHAU><NGUOI_UY_THAC_XUAT_KHAU/><MA_DAI_LY_HQ>A6323</MA_DAI_LY_HQ><TEN_DAI_LY_HQ/><SO_VAN_DON_1>201018SITGTAHP636745</SO_VAN_DON_1><SO_VAN_DON_2/><SO_VAN_DON_3/><SO_VAN_DON_4/><SO_VAN_DON_5/><SO_LUONG>245</SO_LUONG><DVT_SO_LUONG>PK</DVT_SO_LUONG><TONG_TRONG_LUONG>81450</TONG_TRONG_LUONG><DVT_TONG_TRONG_LUONG>KGM</DVT_TONG_TRONG_LUONG><MA_DIA_DIEM_LUU_KHO>03EES01</MA_DIA_DIEM_LUU_KHO><TEN_DIA_DIEM_LUU_KHO>CANG DINH VU</TEN_DIA_DIEM_LUU_KHO><MA_DIA_DIEM_DO_HANG>DVU</MA_DIA_DIEM_DO_HANG><TEN_DIA_DIEM_DO_HANG>CANG DINH VU - HP</TEN_DIA_DIEM_DO_HANG><MA_DIA_DIEM_XEP_HANG>CNTAO</MA_DIA_DIEM_XEP_HANG><TEN_DIA_DIEM_XEP_HANG>QINGDAO</TEN_DIA_DIEM_XEP_HANG><MA_PHUONG_TIEN_VC>1844S</MA_PHUONG_TIEN_VC><TEN_PHUONG_TIEN_VC>SITC MOJI</TEN_PHUONG_TIEN_VC><NGAY_HANG_DEN>2018-11-09</NGAY_HANG_DEN><NGAY_DUOC_PHEP_NHAP_KHO_DAU_TIEN/><SO_HOA_DON>TKCI020918</SO_HOA_DON><NGAY_PHAT_HANH>2018-10-09</NGAY_PHAT_HANH><PHUONG_THUC_THANH_TOAN>DP</PHUONG_THUC_THANH_TOAN><TONG_TRI_GIA_HOA_DON>345456</TONG_TRI_GIA_HOA_DON><NGUYEN_TE_TONG_TRI_GIA_HOA_DON>USD</NGUYEN_TE_TONG_TRI_GIA_HOA_DON><TONG_TRI_GIA_TINH_THUE>435333042.7141</TONG_TRI_GIA_TINH_THUE><GIAY_PHEP_XUAT_NHAP_KHAU_1/><GIAY_PHEP_XUAT_NHAP_KHAU_2/><GIAY_PHEP_XUAT_NHAP_KHAU_3/><GIAY_PHEP_XUAT_NHAP_KHAU_4/><GIAY_PHEP_XUAT_NHAP_KHAU_5/><TT_HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Tấm sàn hoàn thiện, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>4124926.1232</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>31624433.6114</TRI_GIA_TINH_THUE><TIEN_THUE>3162443.3611</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082918</MA_HANG><TEN_HANG>Tấm cạnh trái kèm cánh cửa, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>4399162.9396</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>33726915.8705</TRI_GIA_TINH_THUE><TIEN_THUE>3372691.5871</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082918</MA_HANG><TEN_HANG>Tấm cạnh phải kèm cánh cửa, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>4399162.9396</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>33726915.8705</TRI_GIA_TINH_THUE><TIEN_THUE>3372691.5871</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Tấm trước phía trong, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>3178366.7892</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>24367478.7170</TRI_GIA_TINH_THUE><TIEN_THUE>2436747.8717</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Tấm sau, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>3831732.8449</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>29376618.4773</TRI_GIA_TINH_THUE><TIEN_THUE>2937661.8477</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Tấm nóc, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>3656069.6307</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>28029867.1685</TRI_GIA_TINH_THUE><TIEN_THUE>2802986.7169</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>83021000</MA_HANG><TEN_HANG>Bản lề trái, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>397664.4465</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>4374308.9116</TRI_GIA_TINH_THUE><TIEN_THUE>437430.8912</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>83021000</MA_HANG><TEN_HANG>Bản lề phải, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>397664.4465</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>4374308.9116</TRI_GIA_TINH_THUE><TIEN_THUE>437430.8912</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>83023090</MA_HANG><TEN_HANG>Gía bắt xương táp lô trái, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>800383.9496</TRI_GIA_TINH_THUE><TIEN_THUE>80038.3950</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>83023090</MA_HANG><TEN_HANG>Gía bắt xương táp lô phải, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>800383.9496</TRI_GIA_TINH_THUE><TIEN_THUE>80038.3950</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>83023090</MA_HANG><TEN_HANG>Gíá bắt bình dầu côn, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>800383.9496</TRI_GIA_TINH_THUE><TIEN_THUE>80038.3950</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>83023090</MA_HANG><TEN_HANG>Gíá bắt giá đựng đồ, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>1449116.2034</TRI_GIA_TINH_THUE><TIEN_THUE>144911.6203</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>83023090</MA_HANG><TEN_HANG>Gíá bắt gương chiếu hậu bên trái, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>1499666.7687</TRI_GIA_TINH_THUE><TIEN_THUE>149966.6769</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>83023090</MA_HANG><TEN_HANG>Gíá bắt gương chiếu hậu bên phải, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>1499666.7687</TRI_GIA_TINH_THUE><TIEN_THUE>149966.6769</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Thanh nẹp trần, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>209784.8457</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>1608350.4839</TRI_GIA_TINH_THUE><TIEN_THUE>160835.0484</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87081090</MA_HANG><TEN_HANG>Ba đờ sốc , lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>12039459.6202</TRI_GIA_TINH_THUE><TIEN_THUE>1203945.9620</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Tấm ốp trước cabin, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>846721.9677</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>6491535.0857</TRI_GIA_TINH_THUE><TIEN_THUE>649153.5086</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>68079090</MA_HANG><TEN_HANG>Tấm chống ồn rung phía trước trái, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>6175594.0530</TRI_GIA_TINH_THUE><TIEN_THUE>617559.4053</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>68079090</MA_HANG><TEN_HANG>Tấm chống ồn rung phía trước phải, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>6175594.0530</TRI_GIA_TINH_THUE><TIEN_THUE>617559.4053</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>68079090</MA_HANG><TEN_HANG>Tấm chống ồn rung phía sau trái, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>5333084.6324</TRI_GIA_TINH_THUE><TIEN_THUE>533308.4632</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>68079090</MA_HANG><TEN_HANG>Tấm chống ồn rung phía sau phải, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>5333084.6324</TRI_GIA_TINH_THUE><TIEN_THUE>533308.4632</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>68079090</MA_HANG><TEN_HANG>Tấm chống ồn rung sàn trước, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>6428346.8792</TRI_GIA_TINH_THUE><TIEN_THUE>642834.6879</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>68079090</MA_HANG><TEN_HANG>Tấm chống ồn rung sàn trung tâm bên trái, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>10329165.4964</TRI_GIA_TINH_THUE><TIEN_THUE>1032916.5496</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>68079090</MA_HANG><TEN_HANG>Tấm chống ồn rung sàn trung tâm bên phải, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>5164582.7483</TRI_GIA_TINH_THUE><TIEN_THUE>516458.2748</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Nẹp trang trí hông Cabin bên trái , lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>246434.0055</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>1889327.3756</TRI_GIA_TINH_THUE><TIEN_THUE>188932.7376</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Nẹp trang trí hông Cabin bên phải, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>246434.0055</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>1889327.3756</TRI_GIA_TINH_THUE><TIEN_THUE>188932.7376</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Ốp trang trí phía dưới chân gương bên trái, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>32857.8674</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>251910.3167</TRI_GIA_TINH_THUE><TIEN_THUE>25191.0317</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Ốp trang trí phía dưới chân gương bên phải, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>32857.8674</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>251910.3167</TRI_GIA_TINH_THUE><TIEN_THUE>25191.0317</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87089370</MA_HANG><TEN_HANG>Tổng côn, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>41173435.3843</TRI_GIA_TINH_THUE><TIEN_THUE>4117343.5384</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Cơ cấu khóa lật cabin, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>1089364.6808</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>8351795.8863</TRI_GIA_TINH_THUE><TIEN_THUE>835179.5886</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>85011029</MA_HANG><TEN_HANG>Mô tơ gạt mưa, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>4490575.2117</TRI_GIA_TINH_THUE><TIEN_THUE>449057.5212</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87089499</MA_HANG><TEN_HANG>Vành lái kèm ốp, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>26496921.2776</TRI_GIA_TINH_THUE><TIEN_THUE>2649692.1278</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>85122099</MA_HANG><TEN_HANG>Đèn nóc, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>1364865.2614</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>8189191.5682</TRI_GIA_TINH_THUE><TIEN_THUE>818919.1568</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082918</MA_HANG><TEN_HANG>Cụm cơ cấu khóa cửa bên trái, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>371546.6545</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>2848524.3510</TRI_GIA_TINH_THUE><TIEN_THUE>284852.4351</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082918</MA_HANG><TEN_HANG>Cụm cơ cấu khoá cửa bên phải , lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>371546.6545</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>2848524.3510</TRI_GIA_TINH_THUE><TIEN_THUE>284852.4351</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082918</MA_HANG><TEN_HANG>Cơ cấu hạn chế hành trình cánh cửa, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>823974.2133</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>6317135.6355</TRI_GIA_TINH_THUE><TIEN_THUE>631713.5636</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082918</MA_HANG><TEN_HANG>Tấm ốp cánh cửa trái kèm phụ kiện, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>515615.7654</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>3953054.2014</TRI_GIA_TINH_THUE><TIEN_THUE>395305.4201</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082918</MA_HANG><TEN_HANG>Tấm ốp cánh cửa phải kèm phụ kiện, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>515615.7654</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>3953054.2014</TRI_GIA_TINH_THUE><TIEN_THUE>395305.4201</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>70091000</MA_HANG><TEN_HANG>Gương chiếu hậu trong Cabin kèm ốp, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>360172.7773</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>1800863.8864</TRI_GIA_TINH_THUE><TIEN_THUE>180086.3886</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082100</MA_HANG><TEN_HANG>Dây đai an toàn trái kèm phụ kiện, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>2679179.9575</TRI_GIA_TINH_THUE><TIEN_THUE>267917.9958</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082100</MA_HANG><TEN_HANG>Dây đai an toàn phải kèm phụ kiện, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>2679179.9575</TRI_GIA_TINH_THUE><TIEN_THUE>267917.9958</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Tấm lót sàn cabin, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>641992.1785</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>4921940.0351</TRI_GIA_TINH_THUE><TIEN_THUE>492194.0035</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87083090</MA_HANG><TEN_HANG>Tổng phanh trợ lực chân không, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>3562972.3397</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>39192695.7366</TRI_GIA_TINH_THUE><TIEN_THUE>3919269.5737</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87089980</MA_HANG><TEN_HANG>Cút nối chia dầu trên Cabin kèm phụ kiện, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>1373290.3556</TRI_GIA_TINH_THUE><TIEN_THUE>137329.0356</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>70091000</MA_HANG><TEN_HANG>Gương chiếu hậu ngoài bên trái kèm nắp chụp , lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>935185.4569</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>4675927.2843</TRI_GIA_TINH_THUE><TIEN_THUE>467592.7284</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>70091000</MA_HANG><TEN_HANG>Gương chiếu hậu ngoài bên phải kèm nắp chụp , lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>935185.4569</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>4675927.2843</TRI_GIA_TINH_THUE><TIEN_THUE>467592.7284</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>85365059</MA_HANG><TEN_HANG>Công tắc cánh cửa, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>5408910.4803</TRI_GIA_TINH_THUE><TIEN_THUE>540891.0480</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87082998</MA_HANG><TEN_HANG>Tổng thành táp lô kèm phụ kiện, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B01</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>3333809.7773</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>25559208.2925</TRI_GIA_TINH_THUE><TIEN_THUE>2555920.8293</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>87089980</MA_HANG><TEN_HANG>Bình nước rửa kính kèm mô tơ và phụ  kiện, lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>7312981.7707</TRI_GIA_TINH_THUE><TIEN_THUE>731298.1771</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG><HANG><MA_HANG>83023090</MA_HANG><TEN_HANG>Giá đỡ tấm chắn nắng bên trái , lắp cho xe vận tải hàng hóa,  có tổng trọng tải dưới 10 tấn, mới 100%</TEN_HANG><THUE><LOAI_THUE>B05</LOAI_THUE><TRI_GIA_TINH_THUE>0</TRI_GIA_TINH_THUE><TIEN_THUE>0</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE><THUE><LOAI_THUE>VB901</LOAI_THUE><TRI_GIA_TINH_THUE>1440691.1091</TRI_GIA_TINH_THUE><TIEN_THUE>144069.1109</TIEN_THUE><SO_TIEN_MIEN_GIAM>0</SO_TIEN_MIEN_GIAM></THUE></HANG></TT_HANG></Data><Error><ErrorNumber>0</ErrorNumber><ErrorMessage>Xử lý thành công</ErrorMessage></Error><Signature xmlns=""http://www.w3.org/2000/09/xmldsig#""><SignedInfo><CanonicalizationMethod Algorithm=""http://www.w3.org/TR/2001/REC-xml-c14n-20010315""/><SignatureMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#rsa-sha1""/><Reference URI=""""><Transforms><Transform Algorithm=""http://www.w3.org/2000/09/xmldsig#enveloped-signature""/></Transforms><DigestMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#sha1""/><DigestValue>pnTA3kbmes3JRiZXOlvYJdEA/uw=</DigestValue></Reference></SignedInfo><SignatureValue>u9+ovv7aUg5IZwj7r3ZBWsXq30nR5Y2R/fps9EQoGN57dsCdEnkgDGVxuvtNliqXryB/EK74FKJU3vOeAtNsUry20dYeKw0gfk4BRrIj4MmRgeBxBT/yi9aP4S0KqNTBSVMQQ0tck4ZAe92c3d08QcD62TU+2B3GsbmO3GUIUH7rZANmNfTQ6lcS6u4CDJGZI50/LfS6vavaqkpY+JkhH35yjn4lH1gyktlLSXd0CymWY50W76dcWG5j1E/7G6RC4r+lP4AHCLlYY3SLKSE+tRwCWZk9mnuJZ7X9VfN5zzccIzJGBPI9K2+xZCVll3jFc0JUj0l2tU6uhaQSMUtddA==</SignatureValue><KeyInfo><X509Data><X509IssuerSerial><X509IssuerName>CN=VNPT Certification Authority, OU=VNPT-CA Trust Network, O=VNPT Group, C=VN</X509IssuerName><X509SerialNumber>111661711467037078643656049484791041769</X509SerialNumber></X509IssuerSerial><X509Certificate>MIIGWjCCBEKgAwIBAgIQVAFDbJUnKkrOAPTcUfhO6TANBgkqhkiG9w0BAQUFADBpMQswCQYDVQQGEwJWTjETMBEGA1UEChMKVk5QVCBHcm91cDEeMBwGA1UECxMVVk5QVC1DQSBUcnVzdCBOZXR3b3JrMSUwIwYDVQQDExxWTlBUIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE3MDkyNjA5MDMzNFoXDTE5MTIxNjA2NTgwOFowgaUxCzAJBgNVBAYTAlZOMRIwEAYDVQQIDAlIw4AgTuG7mEkxFTATBgNVBAcMDEhvw6BuIEtp4bq/bTFLMEkGA1UEAwxCTkfDgk4gSMOATkcgVEjGr8agTkcgTeG6oEkgQ+G7lCBQSOG6pk4gTkdP4bqgSSBUSMavxqBORyBWSeG7hlQgTkFNMR4wHAYKCZImiZPyLGQBAQwOTVNUOjAxMDAxMTI0MzcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC/4RV2ukLUENDA0G4ivIEaIYonbUcrpP1CPmLPFFwep8wlaP06zu+f7FEdqPjaBbsVmLlZvnq8LdF6RBAT6xZEEOFh4m3/O52AjUTisVO6jB8Ey02UhsRwIC+k8RiPopZuGiLTktKCNyXFIZcZoMajhqUe+20DBlgjONx0xT6yb+hziR9C6RQsKiR9HCFrcFvDfc6urtdl1I/8whUp0Z/qTh84qy1lPBXRAnoDXlBc6FQn5THomPgSMcA3t/ncFUbNWpQSsoCVEfzc7CRKrIs+MidvhV5bPLiI1eFZOTFqyDmpQyXB2sZXkJceIMCaliBEl1u69dT8iKjSui3H8YtVAgMBAAGjggG/MIIBuzBwBggrBgEFBQcBAQRkMGIwMgYIKwYBBQUHMAKGJmh0dHA6Ly9wdWIudm5wdC1jYS52bi9jZXJ0cy92bnB0Y2EuY2VyMCwGCCsGAQUFBzABhiBodHRwOi8vb2NzcC52bnB0LWNhLnZuL3Jlc3BvbmRlcjAdBgNVHQ4EFgQUNqkFaQ95J/BzQW+1ZiAKhOn5bDQwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBQGacDV1QKKFY1Gfel84mgKVaxqrzBrBgNVHSAEZDBiMGAGDSsGAQQBge0DAQEDAQUwTzAmBggrBgEFBQcCAjAaHhgARABJAEQALQBCADIALgAwAC0AMwA2AG0wJQYIKwYBBQUHAgEWGWh0dHA6Ly9wdWIudm5wdC1jYS52bi9ycGEwMQYDVR0fBCowKDAmoCSgIoYgaHR0cDovL2NybC52bnB0LWNhLnZuL3ZucHRjYS5jcmwwDgYDVR0PAQH/BAQDAgTwMB8GA1UdJQQYMBYGCCsGAQUFBwMCBgorBgEEAYI3CgMMMCgGA1UdEQQhMB+BHWNoaW5obnQuaG9AdmlldGNvbWJhbmsuY29tLnZuMA0GCSqGSIb3DQEBBQUAA4ICAQAMPzm4gXM28n97OdGmiDUsRZ4ZOXu80evnkG31CnUDrrRkdkAQMm/FUvnvL84+H4XDlcmnU8alBC3VyNzgnqnWyLvauUwivAGf5jHErXYHFDaWT6YCIvborlvxk5biA05Wxvp2GKRyfg5qRQ6tcbByDjm4Bd/GLTo0ybVoTSGkrFr0pzxPS9gO1nEFV5z9Q3s5lyfH5OI7myshUS1g2hiPn3zhooY8Ee3blHlhGpm5WgEd4ac9khNwT974oJHnQN77T9o70SjdkrVo3djNznuwlJWX9AuhHzi5Qzj8lGnfb9NXARe4pSijPiRlGnMMGoFfOp71T9LCPgAJM0tMQDOxdUsIU+h+IXFy2/7Oh8UFlM3EK0l6HeK78Els3giNJscJIDGBRuNneSHEbfd2RQujZ/51Z49Ra4bLcqnd56mpXidjmkLgatilbQ9atwBmLciDJBlhR9cpapGKaN5jeER/xzPor2DuA2ofM1vZlNIRXeNhU+Spr+26OL9mr1jAyHCezWzo6qk47EqndAqDqefKgTlyka+X8VS5SD4h2o8TEHbIbivvZVLahSzaoFR1NqSZlAs8nxsujYE3oIjYxowi+53XKffqq+ide7kvqI8N18jNpEqzxRldVGQihO6EvOUzLd8L+uu1ocVRQEwiQbnWgP7l+dGdeq7Xpuc+cJE3EQ==</X509Certificate></X509Data></KeyInfo></Signature></Customs>"
        'strXmlResult = txtTestMSG.Text

        Dim err_num As String = "0"
        Dim err_desc As String = ""


        Try
            strXmlResult = CustomsServiceV3.ProcessMSG.getMSG105(txtMaNNT_TK.Value, txtNamDK.Value, txtSo_TK_TK.Value, err_num, err_desc)

            hdTT_TK.Value = "xxx"

            If Not err_num.Equals("0") Then
                clsCommon.ShowMessageBox(Me, "Không tìm thấy tờ khai: " & err_desc)
                Exit Sub
            End If

            If strXmlResult.Trim().Length > 0 Then
                Dim msg_type As String = ""
                Dim ds As DataSet = New DataSet()
                Dim strReader As New StringReader(strXmlResult)
                Dim vxmlDoc As XmlDocument = New XmlDocument()
                Dim strXmlResult_rp As String = strXmlResult
                strXmlResult_rp = strXmlResult_rp.Replace("&apos;", "'")
                strXmlResult_rp = strXmlResult_rp.Replace("&gt;", ">")
                strXmlResult_rp = strXmlResult_rp.Replace("&lt;", "<")

                If strXmlResult_rp.Contains("&amp;") Then
                ElseIf strXmlResult_rp.Contains("&") Then
                    strXmlResult_rp = strXmlResult_rp.Replace("&amp;", "&")
                End If

                vxmlDoc.LoadXml(strXmlResult_rp)
                ds.ReadXml(strReader)
                Dim dtHeader As DataTable
                dtHeader = ds.Tables("Header")
                If dtHeader.Rows.Count > 0 Then
                    msg_type = dtHeader.Rows(0)("Message_type").ToString()
                    hdTT_TK.Value = dtHeader.Rows(0)("Request_ID").ToString()

                End If
                Dim dtData As DataTable
                dtData = ds.Tables("Data")

                If msg_type.Equals("205") Then
                    divKQXK.Visible = True
                    divKQNK.Visible = False

                    Dim NGUOI_XUAT_KHAU As DataTable = New DataTable()
                    NGUOI_XUAT_KHAU = ds.Tables("NGUOI_XUAT_KHAU")

                    Dim NGUOI_UY_THAC_XNK As DataTable = New DataTable()
                    NGUOI_UY_THAC_XNK = ds.Tables("NGUOI_UY_THAC_XNK")

                    Dim NGUOI_NHAP_KHAU As DataTable = New DataTable()
                    NGUOI_NHAP_KHAU = ds.Tables("NGUOI_NHAP_KHAU")

                    Dim HANG As DataTable = New DataTable()
                    HANG = ds.Tables("HANG")

                    Dim THUE_XUAT_KHAU As DataTable = New DataTable()
                    THUE_XUAT_KHAU = ds.Tables("THUE_XUAT_KHAU")

                    If dtData.Rows.Count > 0 Then
                        txtMaHQ.Text = dtData.Rows(0)("MA_HQ").ToString()
                        txtTenHQ.Text = dtData.Rows(0)("TEN_HQ").ToString()
                        txtMaLH.Text = dtData.Rows(0)("MA_LH").ToString()
                        txtMaXN.Text = dtData.Rows(0)("MA_XN").ToString()
                        txtNgayDK.Text = dtData.Rows(0)("NGAY_DK").ToString()
                        txtSoTK.Text = dtData.Rows(0)("SO_TK").ToString()
                        txtSoTKDauTien.Text = dtData.Rows(0)("SO_TK_DAU_TIEN").ToString()
                        txtSoTK_TamN_TX.Text = dtData.Rows(0)("SO_TK_TAM_NHAP_TX").ToString()
                        txtThoiHan_TaiN_TX.Text = dtData.Rows(0)("THOI_HAN_TAI_NHAP_TAI_XUAT").ToString()
                        txtNgayTD_DK.Text = dtData.Rows(0)("NGAY_THAY_DOI_DK").ToString()
                        txtNgayTD_KT.Text = dtData.Rows(0)("NGAY_THAY_DOI_KT").ToString()
                        txtNgayHT_KT.Text = dtData.Rows(0)("NGAY_HOAN_THANH_KT").ToString()
                        txtNgay_thong_quan.Text = dtData.Rows(0)("NGAY_THONG_QUAN").ToString()
                        txtNgayHHQua_KVGS.Text = dtData.Rows(0)("NGAY_HH_QUA_KVGS").ToString()
                        txtMaPL_KT.Text = dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString()

                        If NGUOI_XUAT_KHAU.Rows.Count > 0 Then
                            txtMST_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("MA_SO_THUE").ToString()
                            txtTenNNT_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("TEN").ToString()
                            txtDiaChiNNT_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_1").ToString()
                        End If

                        If NGUOI_UY_THAC_XNK.Rows.Count > 0 Then
                            txtMST_UT.Text = NGUOI_UY_THAC_XNK.Rows(0)("MA_SO_THUE").ToString()
                            txtTenNNT_UT.Text = NGUOI_UY_THAC_XNK.Rows(0)("TEN").ToString()
                        End If

                        If NGUOI_NHAP_KHAU.Rows.Count > 0 Then
                            txtMST_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("MA_SO_THUE").ToString()
                            txtTenNNT_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("TEN").ToString()
                            txtDiaChi_NK1.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_1").ToString()
                            txtDiaChi_NK2.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_2").ToString()
                            txtDiaChi_NK3.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_3").ToString()
                            txtDiaChi_NK4.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_4").ToString()
                            txtMaNuoc.Text = NGUOI_NHAP_KHAU.Rows(0)("MA_NUOC").ToString()
                        End If

                        txtMaDL_HQ.Text = dtData.Rows(0)("MA_DAI_LY_HQ").ToString()
                        txtTenDL_HQ.Text = dtData.Rows(0)("TEN_DAI_LY_HQ").ToString()
                        txtSoVD1.Text = dtData.Rows(0)("SO_VAN_DON_1").ToString()
                        txtSoLuong.Text = dtData.Rows(0)("SO_LUONG").ToString()
                        txtDVT_SL.Text = dtData.Rows(0)("DVT_SO_LUONG").ToString()
                        txtTong_TL.Text = dtData.Rows(0)("TONG_TRONG_LUONG").ToString()
                        txtDVT_TTL.Text = dtData.Rows(0)("DVT_TONG_TRONG_LUONG").ToString()
                        txtMaDD_LuuKho.Text = dtData.Rows(0)("MA_DIA_DIEM_LUU_KHO").ToString()
                        txtTenDD_LuuKho.Text = dtData.Rows(0)("TEN_DIA_DIEM_LUU_KHO").ToString()
                        txtMaDD_NH.Text = dtData.Rows(0)("MA_DIA_DIEM_NHAN_HANG_CUOI_CUNG").ToString()
                        txtTenDD_NH.Text = dtData.Rows(0)("TEN_DIA_DIEM_NHAN_HANG_CUOI_CUNG").ToString()
                        txtMaDD_XH.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG").ToString()
                        txtTenDD_XH.Text = dtData.Rows(0)("TEN_DIA_DIEM_XEP_HANG").ToString()
                        txtMa_PT_VC.Text = dtData.Rows(0)("MA_PHUONG_TIEN_VC").ToString()
                        txtTen_PT_VC.Text = dtData.Rows(0)("TEN_PHUONG_TIEN_VC").ToString()
                        txtSo_HD.Text = dtData.Rows(0)("SO_HOA_DON").ToString()
                        txtNgay_PH.Text = dtData.Rows(0)("NGAY_PHAT_HANH").ToString()
                        txtPT_TT.Text = dtData.Rows(0)("PHUONG_THUC_THANH_TOAN").ToString()
                        txtTong_GT_HD.Text = dtData.Rows(0)("TONG_TRI_GIA_HOA_DON").ToString()
                        txtNT_Tong_GT_HD.Text = dtData.Rows(0)("NGUYEN_TE_TONG_TRI_GIA_HOA_DON").ToString()
                        txtTong_GT_Thue.Text = dtData.Rows(0)("TONG_TRI_GIA_TINH_THUE").ToString()
                        txtGP_XNK1.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_1").ToString()
                        txtGP_XNK2.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_2").ToString()
                        txtGP_XNK3.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_3").ToString()
                        txtGP_XNK4.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_4").ToString()
                        txtGP_XNK5.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_5").ToString()
                        txtMaDD_XH1.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_1").ToString()
                        txtMaDD_XH2.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_2").ToString()
                        txtMaDD_XH3.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_3").ToString()
                        txtMaDD_XH4.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_4").ToString()
                        txtMaDD_XH5.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_5").ToString()
                        txtTenDD_LXH.Text = dtData.Rows(0)("TEN_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG").ToString()

                    End If

                    ltHangXK.Text = ""

                    'If HANG.Rows.Count > 0 Then
                    '    For index As Integer = 0 To HANG.Rows.Count - 1
                    '        For index1 As Integer = 0 To THUE_XUAT_KHAU.Rows.Count - 1
                    '            If THUE_XUAT_KHAU.Rows(index1)("hang_id").ToString().Equals(HANG.Rows(index)("hang_id").ToString()) Then

                    '                ltHangXK.Text &= "<tr>"

                    '                If index1 = 0 Then
                    '                    ltHangXK.Text &= "<td rowspan='" & HANG.Rows.Count & "'>" & HANG.Rows(index)("MA_HANG").ToString() & "</td>" & _
                    '            "<td rowspan='" & HANG.Rows.Count & "'>" & HANG.Rows(index)("TEN_HANG").ToString() & "</td>"
                    '                End If

                    '                ltHangXK.Text &= "<td>" & THUE_XUAT_KHAU.Rows(index1)("TRI_GIA_TINH_THUE").ToString() & "</td>" & _
                    '            "<td>" & THUE_XUAT_KHAU.Rows(index1)("TIEN_THUE").ToString() & "</td>" & _
                    '            "<td>" & THUE_XUAT_KHAU.Rows(index1)("MA_TIEN_TE").ToString() & "</td>"

                    '                ltHangXK.Text &= "</tr>"
                    '            End If

                    '        Next
                    '    Next

                    'End If
                 
                    For Each vnode As XmlNode In vxmlDoc.SelectNodes("Customs/Data/TT_HANG/HANG")
                        ltHangXK.Text &= "<tr>"
                        ltHangXK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE_XUAT_KHAU").Count & "'>" & vnode.SelectSingleNode("MA_HANG").InnerText & "</td>"
                        ltHangXK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE_XUAT_KHAU").Count & "'>" & vnode.SelectSingleNode("TEN_HANG").InnerText & "</td>"
                        Dim vcount As Integer = 0
                        For Each vnodex As XmlNode In vnode.SelectNodes("THUE_XUAT_KHAU")
                            If (vcount > 0) Then
                                ltHangXK.Text &= "<tr>"
                            End If
                            ltHangXK.Text &= "<td >" & vnodex.SelectSingleNode("TRI_GIA_TINH_THUE").InnerText & "</td>"
                            ltHangXK.Text &= "<td >" & vnodex.SelectSingleNode("TIEN_THUE").InnerText & "</td>"
                            ltHangXK.Text &= "<td >" & vnodex.SelectSingleNode("MA_TIEN_TE").InnerText & "</td>"

                            vcount += 1
                            If (vcount > 1) Then
                                ltHangXK.Text &= "</tr>"
                            End If
                        Next
                        If vcount <= 1 Then
                            ltHangXK.Text &= "</tr>"
                        End If

                    Next
                    'For Each vnode As XmlNode In vxmlDoc.SelectNodes("Customs/Data/TT_HANG/HANG")
                    '    ltHangXK.Text &= "<tr>"
                    '    ltHangXK.Text &= "<td >" & vnode.SelectSingleNode("MA_HANG").InnerText & "</td>"
                    '    ltHangXK.Text &= "<td >" & vnode.SelectSingleNode("TEN_HANG").InnerText & "</td>"
                    '    ltHangXK.Text &= "<td >" & vnode.SelectSingleNode("THUE_XUAT_KHAU/TRI_GIA_TINH_THUE").InnerText & "</td>"
                    '    ltHangXK.Text &= "<td >" & vnode.SelectSingleNode("THUE_XUAT_KHAU/TIEN_THUE").InnerText & "</td>"
                    '    ltHangXK.Text &= "<td >" & vnode.SelectSingleNode("THUE_XUAT_KHAU/MA_TIEN_TE").InnerText & "</td>"
                    '    ltHangXK.Text &= "</tr>"
                    'Next
                ElseIf msg_type.Equals("206") Then
                    divKQXK.Visible = False
                    divKQNK.Visible = True
                  
                    Dim NGUOI_XUAT_KHAU As DataTable
                    NGUOI_XUAT_KHAU = ds.Tables("NGUOI_XUAT_KHAU")

                    Dim NGUOI_UY_THAC_XNK As DataTable
                    NGUOI_UY_THAC_XNK = ds.Tables("NGUOI_UY_THAC_XNK")

                    Dim NGUOI_NHAP_KHAU As DataTable
                    NGUOI_NHAP_KHAU = ds.Tables("NGUOI_NHAP_KHAU")

                    Dim HANG As DataTable
                    HANG = ds.Tables("HANG")

                    Dim THUE As DataTable
                    THUE = ds.Tables("THUE")

                    If dtData.Rows.Count > 0 Then
                        txtNK_MaHQ.Text = dtData.Rows(0)("MA_HQ").ToString()
                        txtNK_TenHQ.Text = dtData.Rows(0)("TEN_HQ").ToString()
                        txtNK_MaLH.Text = dtData.Rows(0)("MA_LH").ToString()
                        txtNK_MaXN.Text = dtData.Rows(0)("MA_XN").ToString()
                        txtNK_NgayDK.Text = dtData.Rows(0)("NGAY_DK").ToString()
                        txtNK_SoTK.Text = dtData.Rows(0)("SO_TK").ToString()

                        If NGUOI_NHAP_KHAU.Rows.Count > 0 Then
                            txtNK_MST_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("MA_SO_THUE").ToString()
                            txtNKTenNNT_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("TEN").ToString()
                            txtNK_DiaChiNNT_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_1").ToString()
                        End If

                        If NGUOI_UY_THAC_XNK.Rows.Count > 0 Then
                            txtNK_MST_UT.Text = NGUOI_UY_THAC_XNK.Rows(0)("MA_SO_THUE").ToString()
                            txtNK_TenNNT_UT.Text = NGUOI_UY_THAC_XNK.Rows(0)("TEN").ToString()
                        End If

                        If NGUOI_XUAT_KHAU.Rows.Count > 0 Then
                            txtNK_MST_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("MA_SO_THUE").ToString()
                            txtNK_TenNNT_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("TEN").ToString()
                            txtNK_DiaChi_XK1.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_1").ToString()
                            txtNK_DiaChi_XK2.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_2").ToString()
                            txtNK_DiaChi_XK3.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_3").ToString()
                            txtNK_DiaChi_XK4.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_4").ToString()
                            txtNK_MaNuoc.Text = NGUOI_XUAT_KHAU.Rows(0)("MA_NUOC").ToString()
                        End If
                        txtNK_TenUT_XK.Text = dtData.Rows(0)("NGUOI_UY_THAC_XUAT_KHAU").ToString()
                        txtNK_MaDL_HQ.Text = dtData.Rows(0)("MA_DAI_LY_HQ").ToString()
                        txtNK_TenDL_HQ.Text = dtData.Rows(0)("TEN_DAI_LY_HQ").ToString()
                        txtNK_SoVD1.Text = dtData.Rows(0)("SO_VAN_DON_1").ToString()
                        txtNK_SoVD2.Text = dtData.Rows(0)("SO_VAN_DON_2").ToString()
                        txtNK_SoVD3.Text = dtData.Rows(0)("SO_VAN_DON_3").ToString()
                        txtNK_SoVD4.Text = dtData.Rows(0)("SO_VAN_DON_4").ToString()
                        txtNK_SoVD5.Text = dtData.Rows(0)("SO_VAN_DON_5").ToString()
                        txtNK_SoLuong.Text = dtData.Rows(0)("SO_LUONG").ToString()
                        txtNK_DVT_SL.Text = dtData.Rows(0)("DVT_SO_LUONG").ToString()
                        txtNK_Tong_TL.Text = dtData.Rows(0)("TONG_TRONG_LUONG").ToString()
                        txtNK_DVT_TTL.Text = dtData.Rows(0)("DVT_TONG_TRONG_LUONG").ToString()
                        txtNK_MaDD_LuuKho.Text = dtData.Rows(0)("MA_DIA_DIEM_LUU_KHO").ToString()
                        txtNK_TenDD_LuuKho.Text = dtData.Rows(0)("TEN_DIA_DIEM_LUU_KHO").ToString()
                        txtMaDD_DH.Text = dtData.Rows(0)("MA_DIA_DIEM_DO_HANG").ToString()
                        txtTenDD_DH.Text = dtData.Rows(0)("TEN_DIA_DIEM_DO_HANG").ToString()
                        txtNK_MaDD_XH.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG").ToString()
                        txtNK_TenDD_XH.Text = dtData.Rows(0)("TEN_DIA_DIEM_XEP_HANG").ToString()
                        txtNK_Ma_PT_VC.Text = dtData.Rows(0)("MA_PHUONG_TIEN_VC").ToString()
                        txtNK_Ten_PT_VC.Text = dtData.Rows(0)("TEN_PHUONG_TIEN_VC").ToString()
                        txtNgay_Hang_Den.Text = dtData.Rows(0)("NGAY_HANG_DEN").ToString()
                        txtNgay_Duoc_Phep_Nhap_Kho_DT.Text = dtData.Rows(0)("NGAY_DUOC_PHEP_NHAP_KHO_DAU_TIEN").ToString()
                        txtNK_So_HD.Text = dtData.Rows(0)("SO_HOA_DON").ToString()
                        txtNK_Ngay_PH.Text = dtData.Rows(0)("NGAY_PHAT_HANH").ToString()
                        txtNK_PT_TT.Text = dtData.Rows(0)("PHUONG_THUC_THANH_TOAN").ToString()
                        txtNK_Tong_GT_HD.Text = dtData.Rows(0)("TONG_TRI_GIA_HOA_DON").ToString()
                        txtNK_NT_Tong_GT_HD.Text = dtData.Rows(0)("NGUYEN_TE_TONG_TRI_GIA_HOA_DON").ToString()
                        txtNK_Tong_GT_Thue.Text = dtData.Rows(0)("TONG_TRI_GIA_TINH_THUE").ToString()
                        txtNK_GP_XNK1.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_1").ToString()
                        txtNK_GP_XNK2.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_2").ToString()
                        txtNK_GP_XNK3.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_3").ToString()
                        txtNK_GP_XNK4.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_4").ToString()
                        txtNK_GP_XNK5.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_5").ToString()
                        txtNK_SO_TK_DAU_TIEN.Text = dtData.Rows(0)("SO_TK_DAU_TIEN").ToString()
                        txtNK_SO_TK_TAM_NHAP_TX.Text = dtData.Rows(0)("SO_TK_TAM_NHAP_TX").ToString()
                        txtNK_THOI_HAN_TAI_NHAP_TAI_XUAT.Text = dtData.Rows(0)("THOI_HAN_TAI_NHAP_TAI_XUAT").ToString()
                        txtNK_NGAY_THAY_DOI_DK.Text = dtData.Rows(0)("NGAY_THAY_DOI_DK").ToString()
                        txtNK_NGAY_THAY_DOI_KT.Text = dtData.Rows(0)("NGAY_THAY_DOI_KT").ToString()
                        txtNK_NGAY_HOAN_THANH_KT.Text = dtData.Rows(0)("NGAY_HOAN_THANH_KT").ToString()
                        txtNK_NGAY_THONG_QUAN.Text = dtData.Rows(0)("NGAY_THONG_QUAN").ToString()
                        txtNK_NGAY_HH_QUA_KVGS.Text = dtData.Rows(0)("NGAY_HH_QUA_KVGS").ToString()
                        txtNK_MA_PHAN_LOAI_KT.Text = dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString()
                        If dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString().Equals("1") Then
                            txtNK_MA_PHAN_LOAI_KT_DESC.Text = "Xanh"
                        ElseIf dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString().Equals("2") Then
                            txtNK_MA_PHAN_LOAI_KT_DESC.Text = "Vàng"
                        ElseIf dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString().Equals("2") Then
                            txtNK_MA_PHAN_LOAI_KT_DESC.Text = "Đỏ"
                        End If

                        If Not vxmlDoc.SelectSingleNode("Customs/Data/PHI_VAN_CHUYEN") Is Nothing Then
                            txtNK_PHI_VAN_CHUYEN.Text = vxmlDoc.SelectSingleNode("Customs/Data/PHI_VAN_CHUYEN").InnerText
                        End If

                        If Not vxmlDoc.SelectSingleNode("Customs/Data/NGUYEN_TE_PHI_VAN_CHUYEN") Is Nothing Then
                            txtNK_NGUYEN_TE_PHI_VAN_CHUYEN.Text = vxmlDoc.SelectSingleNode("Customs/Data/NGUYEN_TE_PHI_VAN_CHUYEN").InnerText
                        End If

                        If Not vxmlDoc.SelectSingleNode("Customs/Data/PHI_BAO_HIEM") Is Nothing Then
                            txtNK_PHI_BAO_HIEM.Text = vxmlDoc.SelectSingleNode("Customs/Data/PHI_BAO_HIEM").InnerText
                        End If
                        If Not vxmlDoc.SelectSingleNode("Customs/Data/NGUYEN_TE_PHI_BAO_HIEM") Is Nothing Then
                            txtNK_NGUYEN_TE_PHI_BAO_HIEM.Text = vxmlDoc.SelectSingleNode("Customs/Data/NGUYEN_TE_PHI_BAO_HIEM").InnerText
                        End If
                        If Not vxmlDoc.SelectSingleNode("Customs/Data/MA_PL_BAO_HIEM") Is Nothing Then
                            txtNK_MA_PL_BAO_HIEM.Text = vxmlDoc.SelectSingleNode("Customs/Data/MA_PL_BAO_HIEM").InnerText
                        End If
                        If Not vxmlDoc.SelectSingleNode("Customs/Data/CHI_TIET_KHAI_TRI_GIA") Is Nothing Then
                            txtNK_CHI_TIET_KHAI_TRI_GIA.Text = vxmlDoc.SelectSingleNode("Customs/Data/CHI_TIET_KHAI_TRI_GIA").InnerText
                        End If
                        If Not vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT") Is Nothing Then
                            If Not vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT") Is Nothing Then
                                txtNK_TY_GIA_TINH_THUE.Text = vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT/TY_GIA_TINH_THUE").InnerText
                            End If
                            If Not vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT") Is Nothing Then
                                txtNK_MA_NGUYEN_TE_TGTT.Text = vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT/MA_NGUYEN_TE_TGTT").InnerText
                            End If
                        End If





                    End If

                    ltHang_NK.Text = ""

                    'If HANG.Rows.Count > 0 Then
                    '    For index As Integer = 0 To HANG.Rows.Count - 1
                    '        For index1 As Integer = 0 To THUE.Rows.Count - 1
                    '            If THUE.Rows(index1)("hang_id").ToString().Equals(HANG.Rows(index)("hang_id").ToString()) Then

                    '                ltHang_NK.Text &= "<tr>"

                    '                If (index1 = 0) Then
                    '                    ltHang_NK.Text &= "<td rowspan='" & THUE.Rows.Count & "'>" & HANG.Rows(index)("MA_HANG").ToString() & "</td>" & _
                    '            "<td rowspan='" & THUE.Rows.Count & "'>" & HANG.Rows(index)("TEN_HANG").ToString() & "</td>" & _
                    '            "<td rowspan='" & THUE.Rows.Count & "'>" & HANG.Rows(index)("NUOC_NHAP_KHAU").ToString() & "</td>"
                    '                End If
                    '                ltHang_NK.Text &= "<td>" & THUE.Rows(index1)("LOAI_THUE").ToString() & "</td>" & _
                    '            "<td>" & THUE.Rows(index1)("TRI_GIA_TINH_THUE").ToString() & "</td>" & _
                    '            "<td>" & THUE.Rows(index1)("TIEN_THUE").ToString() & "</td>" & _
                    '            "<td>" & THUE.Rows(index1)("SO_TIEN_MIEN_GIAM").ToString() & "</td>"

                    '                ltHang_NK.Text &= "</tr>"

                    '            End If
                    '        Next
                    '    Next

                    'End If

                    For Each vnode As XmlNode In vxmlDoc.SelectNodes("Customs/Data/TT_HANG/HANG")
                        ltHang_NK.Text &= "<tr>"
                        ltHang_NK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE").Count & "'>" & vnode.SelectSingleNode("MA_HANG").InnerText & "</td>"
                        ltHang_NK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE").Count & "'>" & vnode.SelectSingleNode("TEN_HANG").InnerText & "</td>"
                        If Not vnode.SelectSingleNode("NUOC_NHAP_KHAU") Is Nothing Then
                            ltHang_NK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE").Count & "'>" & vnode.SelectSingleNode("NUOC_NHAP_KHAU").InnerText & "</td>"
                        Else
                            ltHang_NK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE").Count & "'></td>"
                        End If

                        Dim vcount As Integer = 0
                        For Each vnodex As XmlNode In vnode.SelectNodes("THUE")
                            If (vcount > 0) Then
                                ltHang_NK.Text &= "<tr>"
                            End If
                            ltHang_NK.Text &= "<td >" & vnodex.SelectSingleNode("LOAI_THUE").InnerText & "</td>"
                            ltHang_NK.Text &= "<td >" & vnodex.SelectSingleNode("TRI_GIA_TINH_THUE").InnerText & "</td>"
                            ltHang_NK.Text &= "<td >" & vnodex.SelectSingleNode("TIEN_THUE").InnerText & "</td>"
                            ltHang_NK.Text &= "<td >" & vnodex.SelectSingleNode("SO_TIEN_MIEN_GIAM").InnerText & "</td>"
                            vcount += 1
                            If (vcount > 1) Then
                                ltHang_NK.Text &= "</tr>"
                            End If
                        Next
                        If vcount <= 1 Then
                            ltHang_NK.Text &= "</tr>"
                        End If

                    Next

                    End If

            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi khi truy vấn tờ khai: " & ex.Message)
        End Try

    End Sub

    Protected Sub btnIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIn.Click
        If hdTT_TK.Value.Trim.Length > 0 Then
            Try
                Dim tt_tk As String = hdTT_TK.Value
                'tt_tk = tt_tk.Replace("/", 
                Session("tt_tk") = tt_tk
                clsCommon.OpenNewWindow(Me, "../HaiQuan/frmBanInTT_TK_HQ.aspx", "ManPower")
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi khi in thông tin tờ khai. " & ex.Message)
            End Try
        Else
            clsCommon.ShowMessageBox(Me, "Không có thông tin tờ khai")
        End If
    End Sub
End Class
