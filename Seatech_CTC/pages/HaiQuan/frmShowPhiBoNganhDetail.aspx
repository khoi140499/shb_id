﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage06.master" AutoEventWireup="false" CodeFile="frmShowPhiBoNganhDetail.aspx.vb" Inherits="pages_HaiQuan_frmShowPhiBoNganhDetail" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg06_MainContent" Runat="Server">
 <script type="text/javascript" src="../../javascript/jquery/1.11.2/jquery.min.js"></script>

    <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
            <td class="pageTitle" colspan="2">
                <asp:Label ID="Label1" runat="server">CHI TIẾT LỆ PHÍ BỘ NGÀNH SỐ CT: </asp:Label>
            </td>
        </tr>
        <tr>
            <td width='100%' align="left" valign="top" class='nav' colspan="2">
                <div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan='2' align="left" class="errorMessage">
                <table border="0" width="100%">
                    <tr id="rowProgress">
                        <td colspan="2" align="left">
                            <div style="background-color: Aqua; display: none;" id="divProgress">
                                <b style="font-weight: bold; font-size: 15pt">Đang xử lý. Xin chờ một lát...</b>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <div id="divStatus" runat="server" style="font-weight: bold; width: 100%;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
           
            <td valign="top">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td colspan="2" width='100%' align="left">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr class="grid_header">
                                    <td align="left">
                                        Chi tiết thông tin thuế, phí bộ ngành số chứng từ:
                                        <asp:Label ID="labelSoCT" runat="server" Text=""></asp:Label>
                                        <div id="divTTBDS" style="display: none">
                                        </div>
                                    </td>
                                    <td height='15' align="right">
                                    </td>
                                </tr>
                            </table>
                            <div id="pnlHeader" style="height: auto; width: 100%;">
                                <table id="Table1" cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px;
                                    border-style: solid; font-family: Verdana; font-size: 8pt; width: 99%; border-collapse: collapse;" runat="server">
                                    <tr id="rowMaNNT">
                                        <td width="24%">
                                            <b>Số hồ sơ</b>
                                        </td>
                                        <td width="35%">
                                            <asp:TextBox ID="textBoxSoHoso" ReadOnly="true" runat="server" class="inputflat validate[required,minSize[1],maxSize[14],custom[integer]]"
                                                Style="width: 98%;" onblur="{checkMaNNT(this);}"></asp:TextBox>
                                        </td>
                                        <td width="35%" style="height: 25px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã/ Tên DVQL</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxMa_DVQL" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTen_DVQL" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã/ Tên NH PH</b>
                                        </td>
                                        <td width="35%">
                                            <asp:TextBox ID="textBoxMa_NH_PH" runat="server" ReadOnly="true" class="inputflat"
                                                Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td width="35%">
                                            <asp:TextBox ID="textBoxTen_NH_PH" runat="server" ReadOnly="true" class="inputflat"
                                                Style="width: 98%;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Ký hiệu CT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxKyHieu_CT" ReadOnly="true" runat="server" class="inputflat"
                                                Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <b><i>Thông tin người nộp tiền</i></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Số/ Năm CT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxSo_CT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                            <asp:HiddenField ID="hdSO_CT" runat="server" />
                                            <asp:HiddenField ID="hdSO_TN_CT" runat="server" />
                                            <asp:HiddenField ID="hdTrangThai_CT" runat="server" />
                                            <asp:HiddenField ID="hdTrangThai_XuLy" runat="server" />
                                            <asp:HiddenField ID="hdfMa_NV" runat="server" />
                                            <asp:HiddenField ID="hdfRM_REF_NO" runat="server" />
                                            <asp:HiddenField ID="hdfREF_NO" runat="server" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxNam_CT" ReadOnly="true" runat="server" class="inputflat"
                                                Style="width: 98%;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>MST/ Tên NNT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxMST" runat="server" ReadOnly="true" class="inputflat" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTen_NNT" runat="server" class="inputflat validate[required]"
                                                ReadOnly="true" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Quận/ Huyện</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="NNT_QUAN_HUYEN" runat="server" class="inputflat validate[required]"
                                                ReadOnly="true" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="NNT_TINH_TP" runat="server" class="inputflat validate[required]"
                                                ReadOnly="true" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Địa chỉ</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxDiaChi" runat="server" class="inputflat validate[required]"
                                                ReadOnly="true" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_Khac" runat="server" class="inputflat validate[required]" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã NT/Tỷ giá</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxMa_NT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTyGia" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <b><i>Tài khoản chuyển tiền</i></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Ngày lập chứng từ</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="NgayLap_CT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Hình thức CT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxPTTT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <%--<tr id="tableRowTienMat">
                                        <td>
                                            <b>Số CMND/ Số ĐT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_NT_So_CMND" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_NT_So_DT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr1">
                                        <td>
                                            <b>Địa chỉ thanh toán</b>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="TK_Thanh_Toan_DiaChi" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="tableRowQuanHuyen">
                                        <td>
                                            <b>Quận(Huyện) / Tỉnh (Thành phố)</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_NT_TK_THANH_TOAN_QUAN_HUYEN" runat="server" class="inputflat"
                                                Style="width: 98%;" ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_NT_TK_THANH_TOAN_TINH_TP" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td>
                                            <b>TKC/ Tên TK</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTKC" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTen_TKC" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="trSoDuTK">
                                        <td>
                                            <b>Số dư</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxSoDu" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <b><i>Tài khoản nộp tiền</i></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã/ Tên NH TH(*)</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBox_Ma_NH_TH" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBox_Ten_NH_TH" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã/ Tên TK TH</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBox_TaiKhoan_TH" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBox_Ten_TaiKhoan_TH" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' height='2' align="left">
                            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <%--<tr class="grid_header" runat="server" id="Tr1">
                        <td colspan="4" align="left">
                            Thông tin chi tiết tài khoản nộp tiền
                        </td>
                    </tr>--%>
                    <tr runat="server" id="Tr2">
                        <td align="center" colspan="4">
                            <div id="div1" style="min-height: 200px; height: auto; width: 100%;">
                                <asp:GridView ID="dataGridViewLePhi" runat="server" BackColor="White" Width="100%"
                                    BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical"
                                    AutoGenerateColumns="False">
                                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                    <Columns>
                                        <asp:BoundField DataField="STT" HeaderText="ID" />
                                        <asp:TemplateField HeaderText="Chọn">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" Enabled="False" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NDKT" HeaderText="NDKT" />
                                        <asp:BoundField DataField="TEN_NDKT" HeaderText="Tên NDKT" />
                                        <asp:BoundField DataField="SOTIEN_NT" HeaderText="Số tiền NT" />
                                        <asp:BoundField DataField="SOTIEN_VND" HeaderText="Số tiền VNĐ" />
                                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi chú" />
                                    </Columns>
                                    <%--<FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="Gainsboro" />--%>
                                    <HeaderStyle CssClass="grid_header"></HeaderStyle>
                                    <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
                                    <RowStyle CssClass="grid_item" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Lý do trả</b> <span class="requiredField">(*)</span>
                        </td>
                        <td>
                            <asp:TextBox ID="textBoxLyDoTra" runat="server" class="inputflat" Width="100%" Style="width: 98%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <table width="600px">
                                <tr>
                                   
                                    <td>
                                        Phí
                                    </td>
                                    <td>
                                         <asp:TextBox ID="txtCharge" runat="server" class="inputflat" Style="width: 98%;
                                            text-align: right" ReadOnly="true"></asp:TextBox>
                                        
                                       
                                    </td>
                                    <td>
                                        VAT:
                                    </td>
                                    <td>
                                    <asp:TextBox ID="txtVAT" runat="server" class="inputflat" Style="width: 98%;
                                            text-align: right" ReadOnly="true"></asp:TextBox>
                                       
                                    </td>
                                    <td>
                                        Tổng trích nợ:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTongTrichNo" runat="server" class="inputflat" Style="width: 98%;
                                            text-align: right" ReadOnly="true"></asp:TextBox>
                                       
                                     
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <table>
                                <tr align="left">
                                    <td>
                                        <b>Tổng tiền NT</b>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textBoxTongTien_NT" runat="server" class="inputflat" Style="width: 98%;
                                            text-align: right" ReadOnly="true"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr align="right">
                                    <td style="" align="left">
                                        <b>Tổng tiền VNĐ</b>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textBoxTongTien_VND" runat="server" class="inputflat" Style="width: 98%;
                                            text-align: right" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                <tr align="right">
                                    <td style="" align="left">
                                        <b>Bằng chữ</b>
                                    </td>
                                    <td>
                                        <asp:Label ID="labelMoneyByText" runat="server" Style="font-weight: bold"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

