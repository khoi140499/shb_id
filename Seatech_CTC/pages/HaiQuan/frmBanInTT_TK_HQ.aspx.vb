﻿Imports Business_HQ
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business_HQ.NewChungTu
Imports System.Xml
Imports GIPBankService
Imports System.IO


Partial Class pages_ChungTu_frmBanInTT_TK_HQ
    Inherits System.Web.UI.Page
    Private mv_objUser As New CTuUser
    Private Shared strMaNhom_GDV As String = ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then

        End If
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))
        End If

        divKQNK.Visible = False
        divKQXK.Visible = False

        Dim tt_tk As String = ""

        Try
            'If Not Request.QueryString.Item("tttk") Is Nothing Then
            '    tt_tk = Request.QueryString.Item("tttk").ToString
            '    tt_tk = tt_tk.Replace(" ", "+")
            '    'tt_tk = tt_tk.Replace("*", "/")
            'End If

            If Not Session("tt_tk") Is Nothing Then
                tt_tk = Session("tt_tk").ToString()
            End If

            If tt_tk.Trim.Length > 0 Then

                Dim result_tt_tk As String = Business_HQ.HaiQuan.HaiQuanController.fnc_getTCS_LOG_MSG_HQ(tt_tk, "MSG_IN")
                If result_tt_tk.Trim.Length > 0 Then
                    Dim msg_type As String = ""
                    Dim ds As DataSet = New DataSet()
                    Dim strReader As New StringReader(result_tt_tk)
                    Dim vxmlDoc As XmlDocument = New XmlDocument()

                    Dim result_tt_tk_rp As String = result_tt_tk
                    result_tt_tk_rp = result_tt_tk_rp.Replace("&apos;", "'")
                    result_tt_tk_rp = result_tt_tk_rp.Replace("&gt;", ">")
                    result_tt_tk_rp = result_tt_tk_rp.Replace("&lt;", "<")
                    If result_tt_tk_rp.Contains("&amp;") Then

                    ElseIf result_tt_tk_rp.Contains("&") Then
                        result_tt_tk_rp = result_tt_tk_rp.Replace("&amp;", "&")
                    End If


                    vxmlDoc.LoadXml(result_tt_tk_rp)
                    ds.ReadXml(strReader)
                    Dim dtHeader As DataTable
                    dtHeader = ds.Tables("Header")
                    If dtHeader.Rows.Count > 0 Then
                        msg_type = dtHeader.Rows(0)("Message_type").ToString()
                    End If
                    Dim dtData As DataTable
                    dtData = ds.Tables("Data")

                    If msg_type.Equals("205") Then
                        divKQXK.Visible = True
                        divKQNK.Visible = False

                        Dim NGUOI_XUAT_KHAU As DataTable = New DataTable()
                        NGUOI_XUAT_KHAU = ds.Tables("NGUOI_XUAT_KHAU")

                        Dim NGUOI_UY_THAC_XNK As DataTable = New DataTable()
                        NGUOI_UY_THAC_XNK = ds.Tables("NGUOI_UY_THAC_XNK")

                        Dim NGUOI_NHAP_KHAU As DataTable = New DataTable()
                        NGUOI_NHAP_KHAU = ds.Tables("NGUOI_NHAP_KHAU")

                        Dim HANG As DataTable = New DataTable()
                        HANG = ds.Tables("HANG")

                        Dim THUE_XUAT_KHAU As DataTable = New DataTable()
                        THUE_XUAT_KHAU = ds.Tables("THUE_XUAT_KHAU")

                        If dtData.Rows.Count > 0 Then
                            txtMaHQ.Text = dtData.Rows(0)("MA_HQ").ToString()
                            txtTenHQ.Text = dtData.Rows(0)("TEN_HQ").ToString()
                            txtMaLH.Text = dtData.Rows(0)("MA_LH").ToString()
                            txtMaXN.Text = dtData.Rows(0)("MA_XN").ToString()
                            txtNgayDK.Text = dtData.Rows(0)("NGAY_DK").ToString()
                            txtSoTK.Text = dtData.Rows(0)("SO_TK").ToString()
                            txtSoTKDauTien.Text = dtData.Rows(0)("SO_TK_DAU_TIEN").ToString()
                            txtSoTK_TamN_TX.Text = dtData.Rows(0)("SO_TK_TAM_NHAP_TX").ToString()
                            txtThoiHan_TaiN_TX.Text = dtData.Rows(0)("THOI_HAN_TAI_NHAP_TAI_XUAT").ToString()
                            txtNgayTD_DK.Text = dtData.Rows(0)("NGAY_THAY_DOI_DK").ToString()
                            txtNgayTD_KT.Text = dtData.Rows(0)("NGAY_THAY_DOI_KT").ToString()
                            txtNgayHT_KT.Text = dtData.Rows(0)("NGAY_HOAN_THANH_KT").ToString()
                            txtNgay_thong_quan.Text = dtData.Rows(0)("NGAY_THONG_QUAN").ToString()
                            txtNgayHHQua_KVGS.Text = dtData.Rows(0)("NGAY_HH_QUA_KVGS").ToString()
                            txtMaPL_KT.Text = dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString()

                            If NGUOI_XUAT_KHAU.Rows.Count > 0 Then
                                txtMST_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("MA_SO_THUE").ToString()
                                txtTenNNT_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("TEN").ToString()
                                txtDiaChiNNT_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_1").ToString()
                            End If

                            If NGUOI_UY_THAC_XNK.Rows.Count > 0 Then
                                txtMST_UT.Text = NGUOI_UY_THAC_XNK.Rows(0)("MA_SO_THUE").ToString()
                                txtTenNNT_UT.Text = NGUOI_UY_THAC_XNK.Rows(0)("TEN").ToString()
                            End If

                            If NGUOI_NHAP_KHAU.Rows.Count > 0 Then
                                txtMST_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("MA_SO_THUE").ToString()
                                txtTenNNT_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("TEN").ToString()
                                txtDiaChi_NK1.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_1").ToString()
                                txtDiaChi_NK2.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_2").ToString()
                                txtDiaChi_NK3.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_3").ToString()
                                txtDiaChi_NK4.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_4").ToString()
                                txtMaNuoc.Text = NGUOI_NHAP_KHAU.Rows(0)("MA_NUOC").ToString()
                            End If

                            txtMaDL_HQ.Text = dtData.Rows(0)("MA_DAI_LY_HQ").ToString()
                            txtTenDL_HQ.Text = dtData.Rows(0)("TEN_DAI_LY_HQ").ToString()
                            txtSoVD1.Text = dtData.Rows(0)("SO_VAN_DON_1").ToString()
                            txtSoLuong.Text = dtData.Rows(0)("SO_LUONG").ToString()
                            txtDVT_SL.Text = dtData.Rows(0)("DVT_SO_LUONG").ToString()
                            txtTong_TL.Text = dtData.Rows(0)("TONG_TRONG_LUONG").ToString()
                            txtDVT_TTL.Text = dtData.Rows(0)("DVT_TONG_TRONG_LUONG").ToString()
                            txtMaDD_LuuKho.Text = dtData.Rows(0)("MA_DIA_DIEM_LUU_KHO").ToString()
                            txtTenDD_LuuKho.Text = dtData.Rows(0)("TEN_DIA_DIEM_LUU_KHO").ToString()
                            txtMaDD_NH.Text = dtData.Rows(0)("MA_DIA_DIEM_NHAN_HANG_CUOI_CUNG").ToString()
                            txtTenDD_NH.Text = dtData.Rows(0)("TEN_DIA_DIEM_NHAN_HANG_CUOI_CUNG").ToString()
                            txtMaDD_XH.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG").ToString()
                            txtTenDD_XH.Text = dtData.Rows(0)("TEN_DIA_DIEM_XEP_HANG").ToString()
                            txtMa_PT_VC.Text = dtData.Rows(0)("MA_PHUONG_TIEN_VC").ToString()
                            txtTen_PT_VC.Text = dtData.Rows(0)("TEN_PHUONG_TIEN_VC").ToString()
                            txtSo_HD.Text = dtData.Rows(0)("SO_HOA_DON").ToString()
                            txtNgay_PH.Text = dtData.Rows(0)("NGAY_PHAT_HANH").ToString()
                            txtPT_TT.Text = dtData.Rows(0)("PHUONG_THUC_THANH_TOAN").ToString()
                            txtTong_GT_HD.Text = dtData.Rows(0)("TONG_TRI_GIA_HOA_DON").ToString()
                            txtNT_Tong_GT_HD.Text = dtData.Rows(0)("NGUYEN_TE_TONG_TRI_GIA_HOA_DON").ToString()
                            txtTong_GT_Thue.Text = dtData.Rows(0)("TONG_TRI_GIA_TINH_THUE").ToString()
                            txtGP_XNK1.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_1").ToString()
                            txtGP_XNK2.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_2").ToString()
                            txtGP_XNK3.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_3").ToString()
                            txtGP_XNK4.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_4").ToString()
                            txtGP_XNK5.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_5").ToString()
                            txtMaDD_XH1.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_1").ToString()
                            txtMaDD_XH2.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_2").ToString()
                            txtMaDD_XH3.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_3").ToString()
                            txtMaDD_XH4.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_4").ToString()
                            txtMaDD_XH5.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_5").ToString()
                            txtTenDD_LXH.Text = dtData.Rows(0)("TEN_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG").ToString()

                        End If

                        ltHangXK.Text = ""
                        For Each vnode As XmlNode In vxmlDoc.SelectNodes("Customs/Data/TT_HANG/HANG")
                            ltHangXK.Text &= "<tr>"
                            ltHangXK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE_XUAT_KHAU").Count & "'>" & vnode.SelectSingleNode("MA_HANG").InnerText & "</td>"
                            ltHangXK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE_XUAT_KHAU").Count & "'>" & vnode.SelectSingleNode("TEN_HANG").InnerText & "</td>"
                            Dim vcount As Integer = 0
                            For Each vnodex As XmlNode In vnode.SelectNodes("THUE_XUAT_KHAU")
                                If (vcount > 0) Then
                                    ltHangXK.Text &= "<tr>"
                                End If
                                ltHangXK.Text &= "<td >" & vnodex.SelectSingleNode("TRI_GIA_TINH_THUE").InnerText & "</td>"
                                ltHangXK.Text &= "<td >" & vnodex.SelectSingleNode("TIEN_THUE").InnerText & "</td>"
                                ltHangXK.Text &= "<td >" & vnodex.SelectSingleNode("MA_TIEN_TE").InnerText & "</td>"

                                vcount += 1
                                If (vcount > 1) Then
                                    ltHangXK.Text &= "</tr>"
                                End If
                            Next
                            If vcount <= 1 Then
                                ltHangXK.Text &= "</tr>"
                            End If

                        Next
                        'If HANG.Rows.Count > 0 Then

                        '    For index As Integer = 0 To HANG.Rows.Count - 1
                        '        For index1 As Integer = 0 To THUE_XUAT_KHAU.Rows.Count - 1
                        '            If THUE_XUAT_KHAU.Rows(index1)("hang_id").ToString().Equals(HANG.Rows(index)("hang_id").ToString()) Then

                        '                ltHangXK.Text &= "<tr>"

                        '                If index1 = 0 Then
                        '                    ltHangXK.Text &= "<td rowspan='" & HANG.Rows.Count & "'>" & HANG.Rows(index)("MA_HANG").ToString() & "</td>" & _
                        '            "<td rowspan='" & HANG.Rows.Count & "'>" & HANG.Rows(index)("TEN_HANG").ToString() & "</td>"
                        '                End If

                        '                ltHangXK.Text &= "<td>" & THUE_XUAT_KHAU.Rows(index1)("TRI_GIA_TINH_THUE").ToString() & "</td>" & _
                        '            "<td>" & THUE_XUAT_KHAU.Rows(index1)("TIEN_THUE").ToString() & "</td>" & _
                        '            "<td>" & THUE_XUAT_KHAU.Rows(index1)("MA_TIEN_TE").ToString() & "</td>"

                        '                ltHangXK.Text &= "</tr>"
                        '            End If

                        '        Next
                        '    Next

                        'End If
                    ElseIf msg_type.Equals("206") Then
                        divKQXK.Visible = False
                        divKQNK.Visible = True

                        Dim NGUOI_XUAT_KHAU As DataTable
                        NGUOI_XUAT_KHAU = ds.Tables("NGUOI_XUAT_KHAU")

                        Dim NGUOI_UY_THAC_XNK As DataTable
                        NGUOI_UY_THAC_XNK = ds.Tables("NGUOI_UY_THAC_XNK")

                        Dim NGUOI_NHAP_KHAU As DataTable
                        NGUOI_NHAP_KHAU = ds.Tables("NGUOI_NHAP_KHAU")

                        Dim HANG As DataTable
                        HANG = ds.Tables("HANG")

                        Dim THUE As DataTable
                        THUE = ds.Tables("THUE")

                        If dtData.Rows.Count > 0 Then
                            txtNK_MaHQ.Text = dtData.Rows(0)("MA_HQ").ToString()
                            txtNK_TenHQ.Text = dtData.Rows(0)("TEN_HQ").ToString()
                            txtNK_MaLH.Text = dtData.Rows(0)("MA_LH").ToString()
                            txtNK_MaXN.Text = dtData.Rows(0)("MA_XN").ToString()
                            txtNK_NgayDK.Text = dtData.Rows(0)("NGAY_DK").ToString()
                            txtNK_SoTK.Text = dtData.Rows(0)("SO_TK").ToString()

                            If NGUOI_NHAP_KHAU.Rows.Count > 0 Then
                                txtNK_MST_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("MA_SO_THUE").ToString()
                                txtNKTenNNT_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("TEN").ToString()
                                txtNK_DiaChiNNT_NK.Text = NGUOI_NHAP_KHAU.Rows(0)("DIA_CHI_1").ToString()
                            End If

                            If NGUOI_UY_THAC_XNK.Rows.Count > 0 Then
                                txtNK_MST_UT.Text = NGUOI_UY_THAC_XNK.Rows(0)("MA_SO_THUE").ToString()
                                txtNK_TenNNT_UT.Text = NGUOI_UY_THAC_XNK.Rows(0)("TEN").ToString()
                            End If

                            If NGUOI_XUAT_KHAU.Rows.Count > 0 Then
                                txtNK_MST_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("MA_SO_THUE").ToString()
                                txtNK_TenNNT_XK.Text = NGUOI_XUAT_KHAU.Rows(0)("TEN").ToString()
                                txtNK_DiaChi_XK1.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_1").ToString()
                                txtNK_DiaChi_XK2.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_2").ToString()
                                txtNK_DiaChi_XK3.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_3").ToString()
                                txtNK_DiaChi_XK4.Text = NGUOI_XUAT_KHAU.Rows(0)("DIA_CHI_4").ToString()
                                txtNK_MaNuoc.Text = NGUOI_XUAT_KHAU.Rows(0)("MA_NUOC").ToString()
                            End If
                            txtNK_TenUT_XK.Text = dtData.Rows(0)("NGUOI_UY_THAC_XUAT_KHAU").ToString()
                            txtNK_MaDL_HQ.Text = dtData.Rows(0)("MA_DAI_LY_HQ").ToString()
                            txtNK_TenDL_HQ.Text = dtData.Rows(0)("TEN_DAI_LY_HQ").ToString()
                            txtNK_SoVD1.Text = dtData.Rows(0)("SO_VAN_DON_1").ToString()
                            txtNK_SoVD2.Text = dtData.Rows(0)("SO_VAN_DON_2").ToString()
                            txtNK_SoVD3.Text = dtData.Rows(0)("SO_VAN_DON_3").ToString()
                            txtNK_SoVD4.Text = dtData.Rows(0)("SO_VAN_DON_4").ToString()
                            txtNK_SoVD5.Text = dtData.Rows(0)("SO_VAN_DON_5").ToString()
                            txtNK_SoLuong.Text = dtData.Rows(0)("SO_LUONG").ToString()
                            txtNK_DVT_SL.Text = dtData.Rows(0)("DVT_SO_LUONG").ToString()
                            txtNK_Tong_TL.Text = dtData.Rows(0)("TONG_TRONG_LUONG").ToString()
                            txtNK_DVT_TTL.Text = dtData.Rows(0)("DVT_TONG_TRONG_LUONG").ToString()
                            txtNK_MaDD_LuuKho.Text = dtData.Rows(0)("MA_DIA_DIEM_LUU_KHO").ToString()
                            txtNK_TenDD_LuuKho.Text = dtData.Rows(0)("TEN_DIA_DIEM_LUU_KHO").ToString()
                            txtMaDD_DH.Text = dtData.Rows(0)("MA_DIA_DIEM_DO_HANG").ToString()
                            txtTenDD_DH.Text = dtData.Rows(0)("TEN_DIA_DIEM_DO_HANG").ToString()
                            txtNK_MaDD_XH.Text = dtData.Rows(0)("MA_DIA_DIEM_XEP_HANG").ToString()
                            txtNK_TenDD_XH.Text = dtData.Rows(0)("TEN_DIA_DIEM_XEP_HANG").ToString()
                            txtNK_Ma_PT_VC.Text = dtData.Rows(0)("MA_PHUONG_TIEN_VC").ToString()
                            txtNK_Ten_PT_VC.Text = dtData.Rows(0)("TEN_PHUONG_TIEN_VC").ToString()
                            txtNgay_Hang_Den.Text = dtData.Rows(0)("NGAY_HANG_DEN").ToString()
                            txtNgay_Duoc_Phep_Nhap_Kho_DT.Text = dtData.Rows(0)("NGAY_DUOC_PHEP_NHAP_KHO_DAU_TIEN").ToString()
                            txtNK_So_HD.Text = dtData.Rows(0)("SO_HOA_DON").ToString()
                            txtNK_Ngay_PH.Text = dtData.Rows(0)("NGAY_PHAT_HANH").ToString()
                            txtNK_PT_TT.Text = dtData.Rows(0)("PHUONG_THUC_THANH_TOAN").ToString()
                            txtNK_Tong_GT_HD.Text = dtData.Rows(0)("TONG_TRI_GIA_HOA_DON").ToString()
                            txtNK_NT_Tong_GT_HD.Text = dtData.Rows(0)("NGUYEN_TE_TONG_TRI_GIA_HOA_DON").ToString()
                            txtNK_Tong_GT_Thue.Text = dtData.Rows(0)("TONG_TRI_GIA_TINH_THUE").ToString()
                            txtNK_GP_XNK1.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_1").ToString()
                            txtNK_GP_XNK2.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_2").ToString()
                            txtNK_GP_XNK3.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_3").ToString()
                            txtNK_GP_XNK4.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_4").ToString()
                            txtNK_GP_XNK5.Text = dtData.Rows(0)("GIAY_PHEP_XUAT_NHAP_KHAU_5").ToString()
                            txtNK_SO_TK_DAU_TIEN.Text = dtData.Rows(0)("SO_TK_DAU_TIEN").ToString()
                            txtNK_SO_TK_TAM_NHAP_TX.Text = dtData.Rows(0)("SO_TK_TAM_NHAP_TX").ToString()
                            txtNK_THOI_HAN_TAI_NHAP_TAI_XUAT.Text = dtData.Rows(0)("THOI_HAN_TAI_NHAP_TAI_XUAT").ToString()
                            txtNK_NGAY_THAY_DOI_DK.Text = dtData.Rows(0)("NGAY_THAY_DOI_DK").ToString()
                            txtNK_NGAY_THAY_DOI_KT.Text = dtData.Rows(0)("NGAY_THAY_DOI_KT").ToString()
                            txtNK_NGAY_HOAN_THANH_KT.Text = dtData.Rows(0)("NGAY_HOAN_THANH_KT").ToString()
                            txtNK_NGAY_THONG_QUAN.Text = dtData.Rows(0)("NGAY_THONG_QUAN").ToString()
                            txtNK_NGAY_HH_QUA_KVGS.Text = dtData.Rows(0)("NGAY_HH_QUA_KVGS").ToString()
                            txtNK_MA_PHAN_LOAI_KT.Text = dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString()
                            If dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString().Equals("1") Then
                                txtNK_MA_PHAN_LOAI_KT_DESC.Text = "Xanh"
                            ElseIf dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString().Equals("2") Then
                                txtNK_MA_PHAN_LOAI_KT_DESC.Text = "Vàng"
                            ElseIf dtData.Rows(0)("MA_PHAN_LOAI_KT").ToString().Equals("2") Then
                                txtNK_MA_PHAN_LOAI_KT_DESC.Text = "Đỏ"
                            End If

                            If Not vxmlDoc.SelectSingleNode("Customs/Data/PHI_VAN_CHUYEN") Is Nothing Then
                                txtNK_PHI_VAN_CHUYEN.Text = vxmlDoc.SelectSingleNode("Customs/Data/PHI_VAN_CHUYEN").InnerText
                            End If

                            If Not vxmlDoc.SelectSingleNode("Customs/Data/NGUYEN_TE_PHI_VAN_CHUYEN") Is Nothing Then
                                txtNK_NGUYEN_TE_PHI_VAN_CHUYEN.Text = vxmlDoc.SelectSingleNode("Customs/Data/NGUYEN_TE_PHI_VAN_CHUYEN").InnerText
                            End If

                            If Not vxmlDoc.SelectSingleNode("Customs/Data/PHI_BAO_HIEM") Is Nothing Then
                                txtNK_PHI_BAO_HIEM.Text = vxmlDoc.SelectSingleNode("Customs/Data/PHI_BAO_HIEM").InnerText
                            End If
                            If Not vxmlDoc.SelectSingleNode("Customs/Data/NGUYEN_TE_PHI_BAO_HIEM") Is Nothing Then
                                txtNK_NGUYEN_TE_PHI_BAO_HIEM.Text = vxmlDoc.SelectSingleNode("Customs/Data/NGUYEN_TE_PHI_BAO_HIEM").InnerText
                            End If
                            If Not vxmlDoc.SelectSingleNode("Customs/Data/MA_PL_BAO_HIEM") Is Nothing Then
                                txtNK_MA_PL_BAO_HIEM.Text = vxmlDoc.SelectSingleNode("Customs/Data/MA_PL_BAO_HIEM").InnerText
                            End If
                            If Not vxmlDoc.SelectSingleNode("Customs/Data/CHI_TIET_KHAI_TRI_GIA") Is Nothing Then
                                txtNK_CHI_TIET_KHAI_TRI_GIA.Text = vxmlDoc.SelectSingleNode("Customs/Data/CHI_TIET_KHAI_TRI_GIA").InnerText
                            End If
                            If Not vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT") Is Nothing Then
                                If Not vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT") Is Nothing Then
                                    txtNK_TY_GIA_TINH_THUE.Text = vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT/TY_GIA_TINH_THUE").InnerText
                                End If
                                If Not vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT") Is Nothing Then
                                    txtNK_MA_NGUYEN_TE_TGTT.Text = vxmlDoc.SelectSingleNode("Customs/Data/THONG_TIN_TGTT/MA_NGUYEN_TE_TGTT").InnerText
                                End If
                            End If
                        End If

                        ltHang_NK.Text = ""
                        For Each vnode As XmlNode In vxmlDoc.SelectNodes("Customs/Data/TT_HANG/HANG")
                            ltHang_NK.Text &= "<tr>"
                            ltHang_NK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE").Count & "'>" & vnode.SelectSingleNode("MA_HANG").InnerText & "</td>"
                            ltHang_NK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE").Count & "'>" & vnode.SelectSingleNode("TEN_HANG").InnerText & "</td>"
                            If Not vnode.SelectSingleNode("NUOC_NHAP_KHAU") Is Nothing Then
                                ltHang_NK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE").Count & "'>" & vnode.SelectSingleNode("NUOC_NHAP_KHAU").InnerText & "</td>"
                            Else
                                ltHang_NK.Text &= "<td rowspan='" & vnode.SelectNodes("THUE").Count & "'></td>"
                            End If

                            Dim vcount As Integer = 0
                            For Each vnodex As XmlNode In vnode.SelectNodes("THUE")
                                If (vcount > 0) Then
                                    ltHang_NK.Text &= "<tr>"
                                End If
                                ltHang_NK.Text &= "<td >" & vnodex.SelectSingleNode("LOAI_THUE").InnerText & "</td>"
                                ltHang_NK.Text &= "<td >" & vnodex.SelectSingleNode("TRI_GIA_TINH_THUE").InnerText & "</td>"
                                ltHang_NK.Text &= "<td >" & vnodex.SelectSingleNode("TIEN_THUE").InnerText & "</td>"
                                ltHang_NK.Text &= "<td >" & vnodex.SelectSingleNode("SO_TIEN_MIEN_GIAM").InnerText & "</td>"
                                vcount += 1
                                If (vcount > 1) Then
                                    ltHang_NK.Text &= "</tr>"
                                End If
                            Next
                            If vcount <= 1 Then
                                ltHang_NK.Text &= "</tr>"
                            End If

                        Next
                        'If HANG.Rows.Count > 0 Then

                        '    For index As Integer = 0 To HANG.Rows.Count - 1
                        '        For index1 As Integer = 0 To THUE.Rows.Count - 1
                        '            If THUE.Rows(index1)("hang_id").ToString().Equals(HANG.Rows(index)("hang_id").ToString()) Then

                        '                ltHang_NK.Text &= "<tr>"

                        '                If (index1 = 0) Then
                        '                    ltHang_NK.Text &= "<td rowspan='" & THUE.Rows.Count & "'>" & HANG.Rows(index)("MA_HANG").ToString() & "</td>" & _
                        '            "<td rowspan='" & THUE.Rows.Count & "'>" & HANG.Rows(index)("TEN_HANG").ToString() & "</td>" & _
                        '            "<td rowspan='" & THUE.Rows.Count & "'>" & HANG.Rows(index)("NUOC_NHAP_KHAU").ToString() & "</td>"
                        '                End If
                        '                ltHang_NK.Text &= "<td>" & THUE.Rows(index1)("LOAI_THUE").ToString() & "</td>" & _
                        '            "<td>" & THUE.Rows(index1)("TRI_GIA_TINH_THUE").ToString() & "</td>" & _
                        '            "<td>" & THUE.Rows(index1)("TIEN_THUE").ToString() & "</td>" & _
                        '            "<td>" & THUE.Rows(index1)("SO_TIEN_MIEN_GIAM").ToString() & "</td>"

                        '                ltHang_NK.Text &= "</tr>"

                        '            End If

                        '        Next

                        '    Next

                        'End If

                    End If

                End If


            End If
        Catch ex As Exception

        End Try

    End Sub

End Class
