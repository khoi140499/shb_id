﻿Imports Business_HQ
Imports System.Data
Imports log4net
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common
Imports CoreBankSV
Imports VBOracleLib
Imports System.Xml
Imports System.IO

Partial Class pages_HaiQuan_frmKiemSoatThueLePhi
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_objUser As New CTuUser
    Private Shared pv_cifNo As String = String.Empty
    Private illegalChars As String = "[&]"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then

        End If
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Kiểm tra trạng thái session
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))



        Else
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        'Check các quyền của user

        If Not IsPostBack Then
            'Load danh sách chứng từ thành lập bởi điểm thu
            LoadDSCT("99")


        End If
    End Sub

    Private Sub LoadDSCT(ByVal pv_strTrangThaiCT As String)
        Try
            cmdHuyCT.Visible = False
            Dim dsCT As DataSet = GetDSCT(pv_strTrangThaiCT)
            If (Not dsCT Is Nothing) And (dsCT.Tables(0).Rows.Count > 0) Then
                Me.grdDSCT.DataSource = dsCT
                Me.grdDSCT.DataBind()

            Else
                CreateDumpDSCTGrid()
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    Private Sub CreateDumpDSCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("SO_CT"))
        v_dt.Columns.Add(New DataColumn("KYHIEU_CT"))
        v_dt.Columns.Add(New DataColumn("MA_NV"))
        v_dt.Columns.Add(New DataColumn("TRANGTHAI_CT"))
        v_dt.Columns.Add(New DataColumn("TRANGTHAI_XULY"))
        v_dt.Columns.Add(New DataColumn("TEN_DN"))
        v_dt.Columns.Add(New DataColumn("Ma_KS"))
        grdDSCT.Controls.Clear()
        'Dim v_dumpRow As DataRow = v_dt.NewRow
        'v_dt.Rows.Add(v_dumpRow)
        grdDSCT.DataSource = v_dt
        grdDSCT.DataBind()
    End Sub

    Private Function GetDSCT(ByVal pv_strTrangThaiCT As String) As DataSet

        Try
            If pv_strTrangThaiCT.Equals("99") Then pv_strTrangThaiCT = ""
            Dim TrangThai_ChuyenThue As String = "0"
            If pv_strTrangThaiCT.Equals("06") Then
                TrangThai_ChuyenThue = "0"
                pv_strTrangThaiCT = "01"
            ElseIf pv_strTrangThaiCT.Equals("01") Then
                TrangThai_ChuyenThue = "1"
                pv_strTrangThaiCT = "01"
            Else
                TrangThai_ChuyenThue = "-1"
            End If

            'Dim dsCT As DataSet = HaiQuan.HaiQuan.GET_DS_CT_PHI_BO_NGANH(mv_objUser.Ngay_LV, pv_strTrangThaiCT, Session.Item("User"), TrangThai_ChuyenThue, mv_objUser.MA_CN)

            Dim dsCT As DataSet = HaiQuan.HaiQuan.GET_DS_CT_PHI_BO_NGANH(mv_objUser.Ngay_LV, pv_strTrangThaiCT, "", String.Empty, TrangThai_ChuyenThue, mv_objUser.MA_CN)

            Return dsCT
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            Return Nothing
        End Try
    End Function



    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
            Dim v_strRetArgument As String() = btnSelectCT.CommandArgument.ToString.Split(",")
            If (v_strRetArgument.Length > 4) Then
                Dim v_strSo_CT As String = v_strRetArgument(0)
                Dim v_strKyHieu_CT As String = v_strRetArgument(1)
                Dim v_strMa_NV As String = v_strRetArgument(2)
                Dim v_strTrangThai_CT As String = v_strRetArgument(3)
                Dim v_strSO_TN_CT As String = v_strRetArgument(4)
                'Load thông tin chi tiết khi người dùng chọn 1 CT cụ thể

                If (Not v_strSo_CT Is Nothing) Then

                    Dim ds As DataSet = New DataSet()
                    ds = HaiQuan.HaiQuan.GET_CT_PHI_BO_NGANH(v_strSo_CT)

                    If ds.Tables.Count > 0 Then
                        FillDataToForm(ds)
                        controlDisplay(v_strTrangThai_CT)
                    End If
                    'Dat lai gia tri
                    hdSO_CT.Value = v_strSo_CT
                    hdSO_TN_CT.Value = v_strSO_TN_CT
                    hdTrangThai_CT.Value = v_strTrangThai_CT



                End If

            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    Protected Sub FillDataToForm(ByVal dsCT As DataSet)
        Dim dr As DataRow = dsCT.Tables(0).Rows(0)
        textBoxLyDoTra.Text = ""
        textBoxSoHoso.Text = dr("so_hs").ToString()
        textBoxMa_DVQL.Text = dr("MA_DVQL").ToString()
        textBoxMa_NH_PH.Text = dr("MA_NH_PH").ToString()
        textBoxTen_NH_PH.Text = dr("TEN_NH_PH").ToString()
        textBoxTen_DVQL.Text = dr("TEN_DVQL").ToString()
        textBoxKyHieu_CT.Text = dr("KYHIEU_CT").ToString()
        textBoxSo_CT.Text = dr("SO_CT").ToString()
        textBoxNam_CT.Text = dr("NAM_CT_PT").ToString()
        textBoxMST.Text = dr("NNT_MST").ToString()
        textBoxTen_NNT.Text = dr("NNT_TEN_DV").ToString()
        textBoxDiaChi.Text = dr("NNT_DIACHI").ToString()
        textBoxMa_NT.Text = dr("TT_NT_MA_NT").ToString()
        textBoxTyGia.Text = dr("TT_NT_TYGIA").ToString()
        textBoxTongTien_NT.Text = VBOracleLib.Globals.Format_Number2(dr("TT_NT_TONGTIEN_NT").ToString())
        textBoxTongTien_VND.Text = VBOracleLib.Globals.Format_Number2(dr("TT_NT_TONGTIEN_VND").ToString())
        textBox_Ma_NH_TH.Text = dr("TAIKHOAN_NT_MA_NH_TH").ToString()
        textBox_Ten_NH_TH.Text = dr("TAIKHOAN_NT_TEN_NH_TH").ToString()


        If dr("TAIKHOAN_NT_MA_NH_TT").ToString() <> "" Then
            rowchecktaikhoanLienNgan.Visible = True
            txtMaNH_TT.Text = dr("TAIKHOAN_NT_MA_NH_TT").ToString()
            txtTenNH_TT.Text = dr("TAIKHOAN_NT_TEN_NH_TT").ToString()
        Else
            rowchecktaikhoanLienNgan.Visible = False
            txtMaNH_TT.Text = ""
            txtTenNH_TT.Text = ""
        End If

        
        textBox_TaiKhoan_TH.Text = dr("TAIKHOAN_NT_TK_TH").ToString()
        textBox_Ten_TaiKhoan_TH.Text = dr("TAIKHOAN_NT_TEN_TK_TH").ToString()
        NNT_QUAN_HUYEN.Text = dr("NNT_QUAN_HUYEN").ToString()
        NNT_TINH_TP.Text = dr("NNT_TINH_TP").ToString()
        NgayLap_CT.Text = Convert.ToDateTime(dr("NGAY_CT").ToString()).ToString("dd/MM/yyyy")
        TT_Khac.Text = dr("NNT_TT_KHAC").ToString()
        TK_Thanh_Toan_DiaChi.Text = dr("TK_Thanh_Toan_DiaChi").ToString()
        textBoxSoDu.Text = dr("SO_DU").ToString()
        textBoxTen_TKC.Text = dr("TEN_TK_CHUYEN").ToString()
        TT_NT_So_CMND.Text = dr("TT_NT_So_CMND").ToString()
        TT_NT_So_DT.Text = dr("TT_NT_So_DT").ToString()
        TT_NT_TK_THANH_TOAN_QUAN_HUYEN.Text = dr("TK_THANH_TOAN_QUAN_HUYEN").ToString()
        TT_NT_TK_THANH_TOAN_TINH_TP.Text = dr("TK_THANH_TOAN_TINH_TP").ToString()
        'textBoxLyDoTra.Text = dr("LYDO_HUY_KSV").ToString()

        txtCharge.Text = VBOracleLib.Globals.Format_Number2(dr("FEE").ToString())
        txtVAT.Text = VBOracleLib.Globals.Format_Number2(dr("VAT").ToString())
        Dim TongTienTruocVAT = (dr("FEE") + dr("VAT") + dr("TT_NT_TONGTIEN_VND"))
        txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number2(TongTienTruocVAT.ToString())
        hdfMa_NV.Value = dr("MA_NV").ToString()
        hdfMACN.Value = dr("MA_CN").ToString()
        txtRM_REF_NO.Text = dr("RM_REF_NO").ToString()
        txtREF_NO.Text = dr("REF_NO").ToString()

        Dim v_PTTT_Status As String
        Dim v_PTTT_Value As String
        If dr("PTTT") Is Nothing Then
            v_PTTT_Status = "0"
            v_PTTT_Value = "Tiền mặt"
        ElseIf dr("PTTT") = "0" Then

            v_PTTT_Value = "Tiền mặt"

        Else
            v_PTTT_Status = dr("PTTT").ToString()
            v_PTTT_Value = "Chuyển khoản"
        End If

        textBoxPTTT.Text = v_PTTT_Value
        textBoxTKC.Text = dr("TK_THANHTOAN").ToString()

        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu

        Dim dsChiTietCT As DataSet = HaiQuan.HaiQuan.ListChiTietBySoCT(dr("SO_CT").ToString())

        Dim v_dt As New DataTable()
        'v_dt.Columns.Add(New DataColumn("TAIKHOAN_NT_MA_NH_TH"))
        'v_dt.Columns.Add(New DataColumn("TAIKHOAN_NT_TEN_NH_TH"))
        'v_dt.Columns.Add(New DataColumn("TAIKHOAN_NT_TK_TH"))
        'v_dt.Columns.Add(New DataColumn("TAIKHOAN_NT_TEN_TK_TH"))

        dataGridViewLePhi.Controls.Clear()

        hdTrangThai_XuLy.Value = dr("TRANGTHAI_XULY").ToString()
        hdTrangThai_CT.Value = dr("TRANGTHAI_CT")

        labelMoneyByText.Text = mdlCommon.TCS_Dich_So(Double.Parse(dr("TT_NT_TONGTIEN_VND")), "đồng")

        labelSoCT.Text = dr("SO_CT").ToString()

        If hdTrangThai_XuLy.Value.Equals("1") And hdTrangThai_CT.Value.Equals("02") Then
            textBoxLyDoTra.ReadOnly = False
        ElseIf hdTrangThai_XuLy.Value.Equals("0") And hdTrangThai_CT.Value.Equals("02") Then
            textBoxLyDoTra.ReadOnly = False
        ElseIf hdTrangThai_CT.Value.Equals("05") Then
            textBoxLyDoTra.ReadOnly = False
        Else
            textBoxLyDoTra.ReadOnly = True

        End If
        Dim mdl As New mdlCommon()
        dataGridViewLePhi.DataSource = dsChiTietCT
        dataGridViewLePhi.DataBind()
        For i As Integer = 0 To dataGridViewLePhi.Rows.Count - 1
            For j As Integer = 0 To dataGridViewLePhi.Rows(i).Cells.Count - 1
                dataGridViewLePhi.Rows(i).Cells(4).Text = VBOracleLib.Globals.Format_Number2(dataGridViewLePhi.Rows(i).Cells(4).Text)
                dataGridViewLePhi.Rows(i).Cells(5).Text = VBOracleLib.Globals.Format_Number2(dataGridViewLePhi.Rows(i).Cells(5).Text)

            Next
        Next

        'Build Remarks
        textBoxDienGiai.Text = BuildRemarkForPayment()

        'Load info custom
        If hdTrangThai_CT.Value = "05" Then
            rowsodu.Visible = True

            If (textBoxTKC.Text.Trim.Length > 0) Then
                'If Integer.Parse(clsCTU.CheckGL(textBoxTKC.Text.Trim)) = 0 Then
                '    Dim xmlDoc As New XmlDocument
                '    Dim v_coreUtils As New CoreBankSV.ClsCoreBank
                '    Dim v_strReturn As String = v_coreUtils.Customer(textBoxTKC.Text.Trim, Session.Item("UserName"), Session.Item("MA_CN_USER_LOGON")) 'clsCoreBankUtilDAB.GetCustomerByAccount(txtTK_KH_NH.Text.Trim()) 'clsCoreBankUtilPGB.GetTK_KH_NH(txtTK_KH_NH.Text.Trim)
                '    v_strReturn = Regex.Replace(v_strReturn, illegalChars, "")
                '    xmlDoc.LoadXml(v_strReturn)
                '    Dim ds As New DataSet
                '    ds.ReadXml(New XmlTextReader(New StringReader(xmlDoc.InnerXml)))
                '    If ds.Tables.Count > 0 Then
                '        textBoxTen_TKC.Text = ds.Tables("params").Rows(0)("accountname")
                '        textBoxTen_TKC.Text &= "|" & ds.Tables("params").Rows(0)("cifno")
                '        'txtSoDu_KH_NH.Text = VBOracleLib.Globals.Format_Number(ds.Tables("params").Rows(0)("acy_avl_bal").ToString.Replace(",", ""), ".")
                '        'pv_cifNo = ds.Tables("body_etax").Rows(0)("cust_ref")
                '        pv_cifNo = ds.Tables("params").Rows(0)("cifno")
                '    Else
                '        'lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                '    End If
                'Else
                '    'txtSoDu_KH_NH.Text = ""
                'End If
           
            End If
        Else
            rowsodu.Visible = False
        End If
    End Sub

    Protected Sub cmdKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKS.Click

        If hdSO_CT.Value.Length > 0 Then
            Dim err_num As String = ""
            Dim err_msg As String = ""
            Dim trans_id As String = ""
            Dim so_tn_ct As String = ""
            Dim ngay_tn_ct As String = ""

            Dim str_Fee As String = txtCharge.Text.Replace(",", "")
            Dim str_VAT As String = txtVAT.Text.Replace(",", "")
            Dim str_RefNo As String = txtREF_NO.Text
            Dim strTongTienTT As String = textBoxTongTien_VND.Text.Replace(",", "")

            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim pv_cifNo As String = cifNo.Value
            Dim v_strSo_CT As String = hdSO_CT.Value
            Dim str_Desc As String = ""
            Dim str_loaiPhi As String = ""
            EnabledButtonWhenDoTransaction(False)
            Try
                If Not hdTrangThai_CT.Value.Equals("05") Then
                    clsCommon.ShowMessageBox(Me, "Không thể kiểm soát được chứng từ: " & hdSO_CT.Value)
                    Exit Sub
                End If

                If rdTypeMP.Checked = True Then
                    str_loaiPhi = "W"
                ElseIf rdTypePT.Checked = True Then
                    str_loaiPhi = "O"
                Else
                    str_loaiPhi = "B"
                End If

                Dim strTypeTransferNTTQ As String = "NTTQ"
                Dim dtGetTypeTransferNTTQ As DataTable = DataAccess.ExecuteToTable("select * from tcs_thamso_ht where ten_ts='TYPE_TRANSFER_PAYMENT_NTTQ'")

                If dtGetTypeTransferNTTQ.Rows.Count > 0 Then
                    strTypeTransferNTTQ = dtGetTypeTransferNTTQ.Rows(0)("GIATRI_TS").ToString
                End If

                Dim makerUser As HeThong.infUser = HeThong.buUsers.NHANVIEN_DETAIL(v_strMa_NV)
                'Goi ham Hach toan


                Dim v_objGDV As CTuUser
                v_objGDV = New CTuUser(v_strMa_NV)

                Dim v_coreUtils As New CorebankServiceESB.clsCoreBank
                Dim v_arrLogPay(4) As String
                v_arrLogPay(0) = str_Fee
                v_arrLogPay(1) = str_VAT
                v_arrLogPay(2) = v_objGDV.Ma_NV
                v_arrLogPay(3) = mv_objUser.Ma_NV
                v_arrLogPay(4) = v_strSo_CT

                Dim strSO_TK As String = textBoxTKC.Text.Trim()
                Dim strSO_TK_TH As String = textBox_TaiKhoan_TH.Text.Trim()
                Dim strAmount As String = textBoxTongTien_VND.Text.Trim()
                Dim strLoaiTien As String = HaiQuan.HaiQuan.GetCODE_TIENTE(textBoxMa_NT.Text.Trim())
                Dim strNgay_KB As String = DateTime.Now.ToString("dd/MM/yyyy")
                Dim strTenTK_TH As String = Globals.RemoveSign4VietnameseString(textBox_Ten_TaiKhoan_TH.Text.Trim)
                Dim strMaTinh As String = ""
                Dim strTenNH_TH As String = Globals.RemoveSign4VietnameseString(textBox_Ten_NH_TH.Text.Trim())
                Dim strTenTinh As String = ""
                Dim strMaNH_TH As String = textBox_Ma_NH_TH.Text.Trim()
                Dim errCode As String = ""
                Dim rm_ref_no As String = ""

                Dim str_PayMent As String = ""
                'Build Remarks
                str_Desc = textBoxDienGiai.Text

                Dim req_Internal_rmk As String = ""
                Dim sTenMaCN As String = ""
                sTenMaCN = Get_TenCN(hdfMACN.Value)
                req_Internal_rmk = hdfMACN.Value & "-" & sTenMaCN

                Dim vDt As DataTable = GetCtuThuePhiBBN(textBoxSo_CT.Text.Trim())
                Dim vDr As DataRow = Nothing
                If vDt IsNot Nothing Then
                    If vDt.Rows.Count > 0 Then
                        vDr = vDt.Rows(0)
                    End If
                End If
                Dim strSenderCode As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
                Dim v_strMaNHNhan As String = textBox_Ma_NH_TH.Text.Trim()

                Dim strMa_NH_Nhan As String = v_strMaNHNhan.Substring(2, 3)
                Dim strMa_NH_Chuyen As String = strSenderCode.Substring(2, 3)
                If strMa_NH_Chuyen.Equals(strMa_NH_Nhan) Then
                    str_PayMent = v_coreUtils.PaymentTQ(strSO_TK, strSO_TK_TH, strAmount, strLoaiTien, str_Desc, strNgay_KB, strTenNH_TH, strMaTinh, strTenTK_TH, _
                                                           strTenTK_TH, strTenNH_TH, strTenNH_TH, strTenTinh, strMaNH_TH.ToString().Substring(2, 3), strMaNH_TH.ToString().Substring(5, 3), _
                                                           strMaNH_TH.ToString().Substring(0, 2), "NPBN", v_strSo_CT, strMaNH_TH, v_strMa_NV, mv_objUser.Ma_NV, "ETAX_HQ", req_Internal_rmk, errCode, rm_ref_no)
                Else
                    str_PayMent = v_coreUtils.PaymentPBN("FINPOST", hdfMACN.Value, rm_ref_no, "F", "Y", "", "", "FP", strSO_TK, "C", strAmount, strLoaiTien, str_Desc, errCode, rm_ref_no)
                End If


                If str_PayMent = "" Then
                    'Accountings success
                    'Update status call HAIQUAN after call Accounttings success
                    HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "01", mv_objUser.Ma_NV, 0, String.Empty)

                    'Update so_ct_nh
                    Dim v_strSql As String = "UPDATE TCS_THUE_PHI_BO_NGANH SET SEQ_NO ='" & rm_ref_no & "' WHERE SO_CT = '" & hdSO_CT.Value & "'"
                    DatabaseHelp.Execute(v_strSql)

                    'Call service HaiQuan
                    Dim dsChiTietCT As DataSet = HaiQuan.HaiQuan.ListChiTietBySoCT(hdSO_CT.Value)

                    If dsChiTietCT.Tables(0).Rows.Count > 0 Then
                        CustomsServiceV3.ProcessMSG.guiCTPhiLienBo(hdSO_CT.Value, dsChiTietCT, err_num, err_msg, trans_id, so_tn_ct, ngay_tn_ct)

                        If err_num.Equals("0") Then
                            HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "01", mv_objUser.Ma_NV, 1, String.Empty)
                            LoadDSCT("99")
                            FillDataToForm(HaiQuan.HaiQuan.GET_CT_PHI_BO_NGANH(hdSO_CT.Value))
                            clsCommon.ShowMessageBox(Me, "Đã kiểm soát thành công chứng từ : " & hdSO_CT.Value)
                        Else
                            'Status Accounting 01, status call HaiQuan Error 0
                            HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "01", mv_objUser.Ma_NV, 0, String.Empty)
                            clsCommon.ShowMessageBox(Me, "Kiểm soát không thành công chứng từ : " & hdSO_CT.Value & "," & err_msg)
                        End If
                    End If
                    FillDataToForm(HaiQuan.HaiQuan.GET_CT_PHI_BO_NGANH(hdSO_CT.Value))
                Else
                    'Error accountings documents 
                    'Update status and result error to screen 
                    clsCommon.ShowMessageBox(Me, "Hạch toán không thành công chứng từ : " & _
                                             hdSO_CT.Value & ", Mã lỗi: " & v_arrLogPay(0) & ", Mô tả: " & v_arrLogPay(1))
                End If


            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi khi kiểm soát chứng từ : " & hdSO_CT.Value & ", Lỗi: " & ex.Message)

            Finally
                EnabledButtonWhenDoTransaction(True)
            End Try
            LoadDSCT("99")
        End If
    End Sub

    Private Function GetCtuThuePhiBBN(ByVal sSoCt As String) As DataTable
        Dim vDtTable As New DataTable
        Try
            Dim sSql As String = "SELECT MA_NH_PH,TEN_NH_PH,TO_CHAR(NGAY_CT,'dd/MM/yyyy') NGAY_CT,MA_DVQL,TEN_DVQL,NNT_MST,NNT_TEN_DV," & _
                                    "TT_NT_MA_NT,TT_NT_TYGIA,TT_NT_TONGTIEN_NT,TT_NT_TONGTIEN_VND, " & _
                                    "TAIKHOAN_NT_MA_NH_TH,TAIKHOAN_NT_TEN_NH_TH,TAIKHOAN_NT_TK_TH, " & _
                                    "TRANGTHAI_CT,MA_NV,PTTT,TK_THANHTOAN,TEN_TK_CHUYEN,MA_CN,REF_NO,RM_REF_NO " & _
                                "FROM TCS_THUE_PHI_BO_NGANH WHERE SO_CT = '" & sSoCt & "'"
            vDtTable = DataAccess.ExecuteToTable(sSql)
        Catch ex As Exception
        End Try
        Return vDtTable
    End Function

    Protected Sub cmdHuyCT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdHuyCT.Click
        Dim hd_so_ct, hd_so_tn_ct As String
        hd_so_ct = hdSO_CT.Value
        hd_so_tn_ct = hdSO_TN_CT.Value


        'If hd_so_ct.Length > 0 And hd_so_tn_ct.Length > 0 Then
        Dim err_num As String = ""
        Dim err_msg As String = ""
        Dim trans_id As String = ""
        Dim so_tn_ct As String = ""
        Dim ngay_tn_ct As String = ""
        'If Not textBoxLyDoTra.Text <> "" Then
        '    clsCommon.ShowMessageBox(Me, "Bạn phải nhập lý do hủy")
        '    textBoxLyDoTra.Focus()

        '    LoadDSCT("99")
        '    FillDataToForm(HaiQuan.HaiQuan.GET_CT_PHI_BO_NGANH(hdSO_CT.Value))
        '    Exit Sub

        'Else
        Try
            If Not hdTrangThai_CT.Value.Equals("02") Then
                clsCommon.ShowMessageBox(Me, "Không thể duyệt hủy được chứng từ: " & hdSO_CT.Value)
                Exit Sub
            End If

            CustomsServiceV3.ProcessMSG.getMSGHuy(hdSO_TN_CT.Value, "503", err_num, err_msg, so_tn_ct, ngay_tn_ct, trans_id)
            'If fail = 08 Else 07
            If hdTrangThai_XuLy.Value.Equals("1") And hdTrangThai_CT.Value.Equals("02") Then
                HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "07", "", 1, textBoxLyDoTra.Text)
            End If

            If hdTrangThai_XuLy.Value.Equals("0") And hdTrangThai_CT.Value.Equals("02") Then
                HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "08", "", 0, textBoxLyDoTra.Text)
            End If

            LoadDSCT("99")
            clsCommon.ShowMessageBox(Me, "Đã hủy thành công chứng từ : " & hdSO_CT.Value)
            'Else
            'clsCommon.ShowMessageBox(Me, "Hủy không thành công chứng từ : " & hdSO_CT.Value & "," & err_msg)
            'End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi khi hủy chứng từ : " & hdSO_CT.Value & "," & ex.Message)
        End Try
        'End If

        'End If
    End Sub

    Protected Sub controlDisplay(ByVal trangthai_ct As String)
        cmdInCT.Enabled = True
        If trangthai_ct.Equals("05") Then
            cmdHuyCT.Visible = False
            cmdKS.Enabled = True
            cmdChuyenTra.Enabled = True
            'cmdKS.Text = "Kiểm soát"
            cmdGuiLai.Enabled = False
        ElseIf trangthai_ct.Equals("08") Then
            cmdHuyCT.Visible = False
            cmdKS.Enabled = False
            cmdChuyenTra.Enabled = False
            cmdGuiLai.Enabled = True
        ElseIf trangthai_ct.Equals("01") And hdTrangThai_XuLy.Value.Equals("1") Then
            cmdHuyCT.Visible = False
            cmdKS.Enabled = False
            cmdChuyenTra.Enabled = False
            cmdGuiLai.Enabled = False
        ElseIf trangthai_ct.Equals("02") Then
            cmdHuyCT.Visible = False
            cmdKS.Enabled = False
            cmdChuyenTra.Enabled = True
            cmdGuiLai.Enabled = False
        ElseIf trangthai_ct.Equals("01") And hdTrangThai_XuLy.Value.Equals("0") Then
            cmdHuyCT.Visible = False
            cmdKS.Enabled = False
            cmdChuyenTra.Enabled = False
            cmdGuiLai.Enabled = True


        Else
            cmdHuyCT.Visible = False
            cmdKS.Enabled = False
            cmdChuyenTra.Enabled = False
            ' cmdKS.Text = "Kiểm soát"
        End If
    End Sub

    Protected Sub ddlTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrangThai.SelectedIndexChanged

        LoadDSCT(ddlTrangThai.SelectedValue)
    End Sub

    Private Sub TCS_IN_GNT_PHI()
        Try
            If hdSO_CT.Value.Length > 0 Then
                clsCommon.OpenNewWindow(Me, "../BaoCao/frmInGNT_PHI.aspx?so_ct=" & hdSO_CT.Value, "ManPower")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub cmdInCT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInCT.Click
        LogApp.AddDebug("cmdInCT_Click", "In GNT Thue Phi bo nganh")
        Try
            TCS_IN_GNT_PHI()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình in giấy nộp tiền thuế, lệ phí bộ ngành!")
        Finally
            cmdInCT.Enabled = True
        End Try
    End Sub

    Protected Sub cmdChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenTra.Click
        If hdSO_CT.Value.Length > 0 Then
            If Not textBoxLyDoTra.Text <> "" Then
                clsCommon.ShowMessageBox(Me, "Bạn phải nhập lý do chuyển trả")
                textBoxLyDoTra.Focus()

                LoadDSCT("99")
                FillDataToForm(HaiQuan.HaiQuan.GET_CT_PHI_BO_NGANH(hdSO_CT.Value))
                Exit Sub

            Else
                Try

                    Dim TT_ChuyenThue As String = hdTrangThai_XuLy.Value
                    Dim TT_ChungTu As String = hdTrangThai_CT.Value
                    Dim TT_UPDATE As String = "03"
                    If TT_ChuyenThue.Equals("1") And TT_ChungTu.Equals("02") Then
                        HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "01", "", 1, textBoxLyDoTra.Text)
                        TT_UPDATE = "01"


                    ElseIf TT_ChuyenThue.Equals("0") And TT_ChungTu.Equals("02") Then
                        HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "01", "", 0, textBoxLyDoTra.Text)
                        TT_UPDATE = "01"


                    Else
                        HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "03", "", 0, textBoxLyDoTra.Text)
                        TT_UPDATE = "03"
                    End If

                    LoadDSCT("99")
                    controlDisplay(TT_UPDATE)
                    FillDataToForm(HaiQuan.HaiQuan.GET_CT_PHI_BO_NGANH(hdSO_CT.Value))
                    clsCommon.ShowMessageBox(Me, "Đã chuyển trả thành công chứng từ : " & hdSO_CT.Value)


                Catch ex As Exception
                    clsCommon.ShowMessageBox(Me, "Có lỗi khi chuyển trả chứng từ : " & hdSO_CT.Value & "," & ex.Message)
                End Try
                LoadDSCT("99")
            End If
        End If
    End Sub

    Protected Sub grdDSCT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDSCT.PageIndexChanging
        LoadDSCT(ddlTrangThai.SelectedValue)
        grdDSCT.PageIndex = e.NewPageIndex
        grdDSCT.DataBind()
    End Sub

    Protected Sub cmdGuiLai_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGuiLai.Click
        If hdSO_CT.Value.Length > 0 Then
            Dim err_num As String = ""
            Dim err_msg As String = ""
            Dim trans_id As String = ""
            Dim so_tn_ct As String = ""
            Dim ngay_tn_ct As String = ""
            Try
                Dim dsChiTietCT As DataSet = HaiQuan.HaiQuan.ListChiTietBySoCT(hdSO_CT.Value)

                If hdTrangThai_CT.Value.Equals("08") Then
                    'send msg cancel
                    CustomsServiceV3.ProcessMSG.guiCTPhiLienBo(hdSO_CT.Value, dsChiTietCT, err_num, err_msg, trans_id, so_tn_ct, ngay_tn_ct)
                    If err_num.Equals("0") Then
                        HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "07", mv_objUser.Ma_NV, 1, textBoxLyDoTra.Text)
                        LoadDSCT("99")
                        clsCommon.ShowMessageBox(Me, "Đã gửi lại hủy thành công chứng từ : " & hdSO_CT.Value)
                        FillDataToForm(HaiQuan.HaiQuan.GET_CT_PHI_BO_NGANH(hdSO_CT.Value))
                    End If

                Else
                    'send msg accept
                    CustomsServiceV3.ProcessMSG.guiCTPhiLienBo(hdSO_CT.Value, dsChiTietCT, err_num, err_msg, trans_id, so_tn_ct, ngay_tn_ct)
                    If err_num.Equals("0") Then
                        HaiQuan.HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH(hdSO_CT.Value, "01", mv_objUser.Ma_NV, 1, textBoxLyDoTra.Text)
                        LoadDSCT("99")
                        clsCommon.ShowMessageBox(Me, "Đã gửi lại kiểm soát thành công chứng từ : " & hdSO_CT.Value)
                        FillDataToForm(HaiQuan.HaiQuan.GET_CT_PHI_BO_NGANH(hdSO_CT.Value))

                    End If

                    clsCommon.ShowMessageBox(Me, "Gửi không thành công chứng từ : " & hdSO_CT.Value & "," & err_msg)
                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi khi gửi lại chứng từ : " & hdSO_CT.Value & "," & ex.Message)
            End Try
            LoadDSCT("99")
        End If
    End Sub

    Private Function BuildRemarkForPayment() As String
        Dim v_strRetRemark As String = ""

        v_strRetRemark = "Nop phi bo nganh MBN: " & textBoxMa_DVQL.Text & " TEN: " & VBOracleLib.Globals.RemoveSign4VietnameseString(textBoxTen_DVQL.Text)
        v_strRetRemark = v_strRetRemark & " Cho MST: " & textBoxMST.Text & " TEN: " & VBOracleLib.Globals.RemoveSign4VietnameseString(textBoxTen_NNT.Text)
        v_strRetRemark = v_strRetRemark & " SHS: " & textBoxSoHoso.Text & " KHCT: " & textBoxKyHieu_CT.Text
        v_strRetRemark = v_strRetRemark & " SCT: " & textBoxSo_CT.Text & " NAM: " & textBoxNam_CT.Text
        Return v_strRetRemark
    End Function

    Private Function BuildCKTM() As String
        Dim v_intRowCnt As Integer = CInt(dataGridViewLePhi.Rows.Count)
        Dim v_strRetRemark As String = String.Empty

        For i = 0 To v_intRowCnt - 1
            v_strRetRemark &= "C" & ""
            v_strRetRemark &= "T" & dataGridViewLePhi.Rows(i).Cells(2).Text
        Next

        Return v_strRetRemark
    End Function

    Private Sub EnabledButtonWhenDoTransaction(ByVal blnEnabled As Boolean)
        cmdKS.Enabled = blnEnabled
        cmdChuyenTra.Enabled = blnEnabled
    End Sub



    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTK_KH_NH(ByVal pv_strTKNH As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        'Dim v_coreUtils As New CoreBankSV.ClsCoreBank
        Return "" 'v_coreUtils.Customer(pv_strTKNH.Trim, HttpContext.Current.Session("UserName"), HttpContext.Current.Session("MA_CN_USER_LOGON"))
    End Function

End Class
