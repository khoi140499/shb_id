﻿Imports Business_HQ
Imports Business_HQ.Common
Imports System.Data
Imports CustomsV3.MSG.MSG101
Imports System.Collections.Generic
Imports CustomsV3.MSG.MSG102
Imports Newtonsoft.Json


Partial Class pages_HaiQuan_frmLapCTuPhi_HQ
    Inherits System.Web.UI.Page

    Private m_User As CTuUser

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sSHKB As String = String.Empty
        Dim sMaDiemThu As String = String.Empty
        Dim sNgay_LV As String = ""

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not Session.Item("User") Is Nothing Then
            m_User = New CTuUser(Session.Item("User"))
            sSHKB = clsCTU.TCS_GetKHKB(CType(Session("User"), Integer))
            sMaDiemThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
        End If

        If Not IsPostBack Then
            If (Not ClientScript.IsStartupScriptRegistered(Me.GetType(), "InitDefValues")) Then
                ClientScript.RegisterStartupScript(Me.GetType(), "InitDefValues", PopulateDefaultValues(sMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrSHKB", HaiQuan.HaiQuanController.PopulateArrSHKB(sMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrDBHC", HaiQuan.HaiQuanController.PopulateArrDBHC(sSHKB), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrCQThu", HaiQuan.HaiQuanController.PopulateArrCQThu(sMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKGL", HaiQuan.HaiQuanController.PopulateArrTKGL(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrNT", HaiQuan.HaiQuanController.PopulateArrNT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTyGia", HaiQuan.HaiQuanController.PopulateArrTyGia(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLTHUE", HaiQuan.HaiQuanController.PopulateArrLoaiThue(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLTIEN", HaiQuan.HaiQuanController.PopulateArrLoaiTien(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKNoNSNN", HaiQuan.HaiQuanController.PopulateArrTKNoNSNN(sSHKB), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKCoNSNN", HaiQuan.HaiQuanController.PopulateArrTKCoNSNN(sSHKB), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLHSX", HaiQuan.HaiQuanController.PopulateArrLHSX(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTT_NH_B", HaiQuan.HaiQuanController.PopulateArrTT_NH_B(), True)
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTT_NH_TT", HaiQuan.HaiQuanController.PopulateArrTT_NH_TT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrMANHA", HaiQuan.HaiQuanController.PopulateArrDM_MA_NH_A(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrPTTT", HaiQuan.HaiQuanController.PopulateArrDM_PTTT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitPage", "jsThem_Moi();", True)
            End If
            If Not Request.Params("SoCT") Is Nothing Then
                Dim soCT As String = Request.Params("SoCT").ToString
                If soCT <> "" Then
                    Dim dtCT As DataTable
                    dtCT = HaiQuan.HaiQuanController.CtuLoad(Session.Item("User").ToString, clsCTU.TCS_GetNgayLV(Session.Item("User").ToString), "01", soCT)
                    If dtCT IsNot Nothing AndAlso dtCT.Rows.Count > 0 Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "PageLoad", "jsGetCTU('" & dtCT.Rows(0)("SO_CT").ToString & "');", True)
                    End If
                End If
            End If

        End If
    End Sub

    Private Function PopulateDefaultValues(ByVal sMaDiemThu As String) As String
        Dim myJavaScript As StringBuilder = New StringBuilder()
        Dim sSHKB = HaiQuan.HaiQuanController.GetSHKB(sMaDiemThu)
        myJavaScript.Append(" var dtmNgayLV =""" & mdlCommon.ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString)) & """;")
        myJavaScript.Append(" var dtmNgayNH =""" & mdlCommon.ConvertNumbertoString(clsCTU.TCS_GetNgayKH_NH(Session.Item("User").ToString)) & """;")
        myJavaScript.Append(" var defSHKB =""" & sSHKB & """;")
        myJavaScript.Append(" var defDBHC =""" & HaiQuan.HaiQuanController.GetDefaultDBHC(sMaDiemThu) & """;")
        myJavaScript.Append(" var defMaVangLai =""" & HaiQuan.HaiQuanController.GetDefaultMaVangLai() & """;")
        myJavaScript.Append(" var defMaNHNhan =""" & HaiQuan.HaiQuanController.GetDefaultMaNHTH(sSHKB) & """;")
        myJavaScript.Append(" var defMaNHTT =""" & HaiQuan.HaiQuanController.GetDefaultMaNHTT(sSHKB) & """;")
        myJavaScript.Append(" var defMaHQ =""" & HaiQuan.HaiQuanController.GetDefaultMaHQ(sMaDiemThu) & """;")
        myJavaScript.Append(" var curCtuStatus =""99"";")
        myJavaScript.Append(" var curMaNNT ="""";")
        myJavaScript.Append(" var curKyThue =""" & mdlCommon.GetKyThue(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString)) & """;")
        myJavaScript.Append(" var curSoCT ="""";")
        myJavaScript.Append(" var curSo_BT ="""";")
        myJavaScript.Append(" var curMa_NV =""" & CType(Session("User"), Integer).ToString & """;")
        myJavaScript.Append(" var curTeller_ID =""" & clsCTU.Get_TellerID(CType(Session("User"), Integer)) & """;")
        myJavaScript.Append(" var saiSo = """ & CTuCommon.Get_ParmValues("SAI_THUCTHU", sMaDiemThu).ToString() & """;")
        myJavaScript.Append(" var varLoai_CTU = """";")

        Return myJavaScript.ToString
    End Function




    Private Shared Function ValidUserSession() As Boolean
        Dim bRet As Boolean = False
        Try
            If clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then bRet = True
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Error valid user session: ", HttpContext.Current.Session("TEN_DN"))
            bRet = False
        End Try

        Return bRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function TruyVanLePhi_HQ(ByVal sMaSoThue As String, ByVal sSoTK As String, ByVal sNgayDK As String) As String
        Dim sRet As String = String.Empty
        Dim sXMLPhanHoi As String = String.Empty
        Dim sError As String = String.Empty
        Dim sErrorDesc As String = String.Empty

        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Throw New Exception("errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại.")
            End If

            If String.IsNullOrEmpty(sSoTK) Then sSoTK = String.Empty
            If String.IsNullOrEmpty(sNgayDK) Then sNgayDK = String.Empty

            sXMLPhanHoi = CustomsServiceV3.ProcessMSG.getMSG102(sMaSoThue, sNgayDK, sSoTK, sError, sErrorDesc)
            'sXMLPhanHoi = "<?xml version='1.0' encoding='UTF-8'?><Customs><Header><Application_Name>Payment</Application_Name><Application_Version>3.0</Application_Version><Sender_Code>99999999</Sender_Code><Sender_Name>Tổng cục hải quan</Sender_Name><Message_Version>3.0</Message_Version><Message_Type>202</Message_Type><Message_Name>Thông điệp trả lời số lệ phí phải thu</Message_Name><Transaction_Date>2016-06-22T11:31:01</Transaction_Date><Transaction_ID>a7082b20-76d7-4e29-bfd8-1080ee4cf267</Transaction_ID><Request_ID>92bfdf74-4020-467b-a2e0-63c8d52e0767</Request_ID></Header><Data><Item><Ma_Cuc>01</Ma_Cuc><Ten_Cuc>Cục Hải quan Hà Nội</Ten_Cuc><Ma_HQ_PH>01PR</Ma_HQ_PH><Ma_HQ_CQT>2995236</Ma_HQ_CQT><Ten_HQ_PH>Chi cục HQ Vĩnh Phúc</Ten_HQ_PH><Ma_DV>2500260507</Ma_DV><Ten_DV>Công ty Trách nhiệm hữu hạn một thành viên thương mại và xuất nhập khẩu PRIME</Ten_DV><Ma_Chuong>552</Ma_Chuong><Ma_NTK>2</Ma_NTK><Ten_NTK>TK Tạm gửi của cơ quan Hải quan</Ten_NTK><Ma_KB>1219</Ma_KB><Ten_KB>KBNN Phúc Yên  - Vĩnh Phúc</Ten_KB><TKKB>3591</TKKB><DuNo_TO>-140000</DuNo_TO><CT_No><Ma_HQ>01PR</Ma_HQ><Ten_HQ>Chi cục HQ Vĩnh Phúc</Ten_HQ><Ma_LH>XKD01</Ma_LH><Ten_LH>Xuất Kinh Doanh</Ten_LH><Nam_DK>2014</Nam_DK><So_TK>52</So_TK><Ngay_DK>2014-01-07</Ngay_DK><LoaiThue>LP</LoaiThue><Khoan>000</Khoan><TieuMuc>3052</TieuMuc><DuNo>-140000</DuNo></CT_No></Item><Item><Ma_Cuc>01</Ma_Cuc><Ten_Cuc>Cục Hải quan Hà Nội</Ten_Cuc><Ma_HQ_PH>01PR</Ma_HQ_PH><Ma_HQ_CQT>2995236</Ma_HQ_CQT><Ten_HQ_PH>Chi cục HQ Vĩnh Phúc</Ten_HQ_PH><Ma_DV>2500260507</Ma_DV><Ten_DV>Công ty Trách nhiệm hữu hạn một thành viên thương mại và xuất nhập khẩu PRIME</Ten_DV><Ma_Chuong>552</Ma_Chuong><Ma_NTK>2</Ma_NTK><Ten_NTK>TK Tạm gửi của cơ quan Hải quan</Ten_NTK><Ma_KB>1219</Ma_KB><Ten_KB>KBNN Phúc Yên  - Vĩnh Phúc</Ten_KB><TKKB>3591</TKKB><DuNo_TO>20000</DuNo_TO><CT_No><Ma_HQ>01PR</Ma_HQ><Ten_HQ>Chi cục HQ Vĩnh Phúc</Ten_HQ><Ma_LH>NKD05</Ma_LH><Ten_LH>Nhập Đầu Tư Kinh doanh</Ten_LH><Nam_DK>2014</Nam_DK><So_TK>3943</So_TK><Ngay_DK>2014-04-10</Ngay_DK><LoaiThue>LP</LoaiThue><Khoan>000</Khoan><TieuMuc>3052</TieuMuc><DuNo>20000</DuNo></CT_No></Item><Item><Ma_Cuc>01</Ma_Cuc><Ten_Cuc>Cục Hải quan Hà Nội</Ten_Cuc><Ma_HQ_PH>01PR</Ma_HQ_PH><Ma_HQ_CQT>2995236</Ma_HQ_CQT><Ten_HQ_PH>Chi cục HQ Vĩnh Phúc</Ten_HQ_PH><Ma_DV>2500260507</Ma_DV><Ten_DV>Công ty Trách nhiệm hữu hạn một thành viên thương mại và xuất nhập khẩu PRIME</Ten_DV><Ma_Chuong>552</Ma_Chuong><Ma_NTK>2</Ma_NTK><Ten_NTK>TK Tạm gửi của cơ quan Hải quan</Ten_NTK><Ma_KB>1219</Ma_KB><Ten_KB>KBNN Phúc Yên  - Vĩnh Phúc</Ten_KB><TKKB>3591</TKKB><DuNo_TO>20000</DuNo_TO><CT_No><Ma_HQ>01PR</Ma_HQ><Ten_HQ>Chi cục HQ Vĩnh Phúc</Ten_HQ><Ma_LH>NKD05</Ma_LH><Ten_LH>Nhập Đầu Tư Kinh doanh</Ten_LH><Nam_DK>2014</Nam_DK><So_TK>3948</So_TK><Ngay_DK>2014-04-10</Ngay_DK><LoaiThue>LP</LoaiThue><Khoan>000</Khoan><TieuMuc>3052</TieuMuc><DuNo>20000</DuNo></CT_No></Item><Item><Ma_Cuc>01</Ma_Cuc><Ten_Cuc>Cục Hải quan Hà Nội</Ten_Cuc><Ma_HQ_PH>01PR</Ma_HQ_PH><Ma_HQ_CQT>2995236</Ma_HQ_CQT><Ten_HQ_PH>Chi cục HQ Vĩnh Phúc</Ten_HQ_PH><Ma_DV>2500260507</Ma_DV><Ten_DV>Công ty Trách nhiệm hữu hạn một thành viên thương mại và xuất nhập khẩu PRIME</Ten_DV><Ma_Chuong>552</Ma_Chuong><Ma_NTK>2</Ma_NTK><Ten_NTK>TK Tạm gửi của cơ quan Hải quan</Ten_NTK><Ma_KB>1219</Ma_KB><Ten_KB>KBNN Phúc Yên  - Vĩnh Phúc</Ten_KB><TKKB>3591</TKKB><DuNo_TO>20000</DuNo_TO><CT_No><Ma_HQ>01PR</Ma_HQ><Ten_HQ>Chi cục HQ Vĩnh Phúc</Ten_HQ><Ma_LH>B11</Ma_LH><Ten_LH>Xuất kinh doanh, XKcủa doanh nghiệp đầu tư</Ten_LH><Nam_DK>2014</Nam_DK><So_TK>30000015186</So_TK><Ngay_DK>2014-04-11</Ngay_DK><LoaiThue>LP</LoaiThue><Khoan>000</Khoan><TieuMuc>3052</TieuMuc><DuNo>20000</DuNo></CT_No></Item><Item><Ma_Cuc>01</Ma_Cuc><Ten_Cuc>Cục Hải quan Hà Nội</Ten_Cuc><Ma_HQ_PH>01PR</Ma_HQ_PH><Ma_HQ_CQT>2995236</Ma_HQ_CQT><Ten_HQ_PH>Chi cục HQ Vĩnh Phúc</Ten_HQ_PH><Ma_DV>2500260507</Ma_DV><Ten_DV>Công ty Trách nhiệm hữu hạn một thành viên thương mại và xuất nhập khẩu PRIME</Ten_DV><Ma_Chuong>552</Ma_Chuong><Ma_NTK>2</Ma_NTK><Ten_NTK>TK Tạm gửi của cơ quan Hải quan</Ten_NTK><Ma_KB>1219</Ma_KB><Ten_KB>KBNN Phúc Yên  - Vĩnh Phúc</Ten_KB><TKKB>3591</TKKB><DuNo_TO>20000</DuNo_TO><CT_No><Ma_HQ>01PR</Ma_HQ><Ten_HQ>Chi cục HQ Vĩnh Phúc</Ten_HQ><Ma_LH>A41</Ma_LH><Ten_LH>Nhập kinh doanh của doanh nghiệp đầu tư</Ten_LH><Nam_DK>2014</Nam_DK><So_TK>10000046233</So_TK><Ngay_DK>2014-04-12</Ngay_DK><LoaiThue>LP</LoaiThue><Khoan>000</Khoan><TieuMuc>3052</TieuMuc><DuNo>20000</DuNo></CT_No></Item></Data><Error><ErrorNumber>0</ErrorNumber><ErrorMessage>Xử lý thành công</ErrorMessage></Error><Signature xmlns='http://www.w3.org/2000/09/xmldsig#'><SignedInfo><CanonicalizationMethod Algorithm='http://www.w3.org/TR/2001/REC-xml-c14n-20010315' /><SignatureMethod Algorithm='http://www.w3.org/2000/09/xmldsig#rsa-sha1' /><Reference URI=''><Transforms><Transform Algorithm='http://www.w3.org/2000/09/xmldsig#enveloped-signature' /></Transforms><DigestMethod Algorithm='http://www.w3.org/2000/09/xmldsig#sha1' /><DigestValue>CjCZLmMJdXsOnE2+kpboFfP7wb0=</DigestValue></Reference></SignedInfo><SignatureValue>UWJw/JuvPkKxujHWX+POxzpAnkfxUooJlWa4h0p3sNPrGTiDvgO2Aqit2zYUzlh6lGoz1fKPEgOPl/k0uerLeUfXWK1o1St/XHQEIclQJikvlOnqUw4t7XpfTqMUv6+zRp+WjyzMQJaoKdERfEDaH3f+RxjTxKdajNpaLSc8ek41GQnxGY/3yBy9D6Z5FOl+m0QPOBVlx+auZ6wKMoFGiK2yBo//0o3h6dWinSnZKfK4BW9sjQMWw37SAc8lyV+228tM/uy2YhUFneKLGLH3HvsR0YI1xSaVsTmJ441AgoaWsA6As8iYlWxPXSjBqkoZ7k3Ccj4AaeC2Sl/PHgDj0g==</SignatureValue><KeyInfo><X509Data><X509IssuerSerial><X509IssuerName>CN=VNPT Certification Authority, OU=VNPT-CA Trust Network, O=VNPT Group, C=VN</X509IssuerName><X509SerialNumber>111663843396468347755383557710472524059</X509SerialNumber></X509IssuerSerial><X509Certificate>MIIF/zCCA+egAwIBAgIQVAGsiVCsCs6lmTyX/l+xGzANBgkqhkiG9w0BAQUFADBpMQswCQYDVQQGEwJWTjETMBEGA1UEChMKVk5QVCBHcm91cDEeMBwGA1UECxMVVk5QVC1DQSBUcnVzdCBOZXR3b3JrMSUwIwYDVQQDExxWTlBUIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MDIxMDE1MjY0M1oXDTE3MDcwMzA5MjEwMFowdTELMAkGA1UEBhMCVk4xEjAQBgNVBAgMCUjDoCBO4buZaTEVMBMGA1UEBwwMQ+G6p3UgR2nhuqV5MRkwFwYDVQQKDBBC4buYIFTDgEkgQ0jDjU5IMSAwHgYDVQQDDBdU4buUTkcgQ+G7pEMgSOG6okkgUVVBTjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL69JOx8ksSE/mZtvhPdpVMV+ZnfZrWLSsxN87b2APMIpVboxq5HmE4Xs8nvEPasymsu0xBn5tTxMI15aAfxspQ5Ks4oDuMkwoO6L4WPiVwxvWtBuLCAdSa0aSVsy98Mod8Frcai6NwygVodojphfl0+88IYgc30it991vfgeh4cdimP5psNElpWlKQVa04VpB99dclDd/Nr3m4wZvyT5Th8s7vBbBPzVtr9ebv75LaM27OG8NBPaHEtJACeoaadla4tTNCPSrRCS4x8Oon4lBb4N0JyC2MOgUjCS51fiB6AVtfqCoS1NzHrCot8/JiHag8+CHH2afwWCZt6mokSKGUCAwEAAaOCAZUwggGRMHAGCCsGAQUFBwEBBGQwYjAyBggrBgEFBQcwAoYmaHR0cDovL3B1Yi52bnB0LWNhLnZuL2NlcnRzL3ZucHRjYS5jZXIwLAYIKwYBBQUHMAGGIGh0dHA6Ly9vY3NwLnZucHQtY2Eudm4vcmVzcG9uZGVyMB0GA1UdDgQWBBQIfYTiUnPDcfy7IZUiDO0vxzbJRTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFAZpwNXVAooVjUZ96XziaApVrGqvMGsGA1UdIARkMGIwYAYNKwYBBAGB7QMBAQMBBTBPMCYGCCsGAQUFBwICMBoeGABEAEkARAAtAEIAMgAuADAALQAzADYAbTAlBggrBgEFBQcCARYZaHR0cDovL3B1Yi52bnB0LWNhLnZuL3JwYTAxBgNVHR8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZucHQtY2Eudm4vdm5wdGNhLmNybDAOBgNVHQ8BAf8EBAMCBPAwHwYDVR0lBBgwFgYIKwYBBQUHAwIGCisGAQQBgjcKAwwwDQYJKoZIhvcNAQEFBQADggIBAB5iI3MXO5KdLT8grKhfKrzDH/VBR/qoBBRRpMrVC552fRpMEIWw08oOcGw2Z3XsUQ9dHSlawLGh3y8/qNXxCZWGUcE5Zeh9l76WDGAB5LSpUNLmD6Na25GBg3UznqKrTuETGMTgwyKYmLvOzPy2xpihtg1J2A5bsXC39ZbTthOyHtinlG/wmx4h9tGBQGXUaSBmVIt/bBw9BsN7yjZYQvAc1AIIegyFx28YY9Ycvfh2BbAStikHIIF22hTOe8/jbxLmQRrFj8J9yKjmQW8BvfvhTcxWS2MgZNrjEwJiYr/NK6iPNxYmCreP7aw74Uy659t/fX/WxOZe3HU8bZFJiudlWSgizKQpsx3Pk1YmPFYj2Ex1YPpUKscD9IStsMgWt/ylylZpupXvhNvW5V82tmRslKob8xnxsHfuKWaWk/GtvkziCdzSWqvU18d854YMMg5c+ZsQHvYVBN5HoIzHPvZHwEvsZtZYMYB8tcuoZGKuYeJwgXWYsucccXrR0R025Du86EQ/URAorkJTFaC6RfSLbvxPNBBv9fAgntf4ZPpaF+7CgTEwpC4K6QHIddGle6cyN8nAz1e9l9jtWtb+KOLdPvzLjTZbPgiJKNjw65LXu8NBzziwzyknK/MMIWJ7M5WKU5QXJyu3ahPONzcL22xqHgIikjh1jO4sivjTdXjQ</X509Certificate></X509Data></KeyInfo></Signature></Customs>"
            'If sNgayDK.EndsWith("1987") Then
            '    sXMLPhanHoi = "<?xml version='1.0' encoding='UTF-8'?><Customs><Header><Application_Name>Payment</Application_Name><Application_Version>3.0</Application_Version><Sender_Code>99999999</Sender_Code><Sender_Name>Tổng cục hải quan</Sender_Name><Message_Version>3.0</Message_Version><Message_Type>202</Message_Type><Message_Name>Thông điệp trả lời số lệ phí phải thu</Message_Name><Transaction_Date>2016-06-22T11:32:07</Transaction_Date><Transaction_ID>6429cb31-5809-4b3e-87dc-b2b8acc06ed4</Transaction_ID><Request_ID>77f1f06a-4fec-485a-8b88-aeec36045d10</Request_ID></Header><Data><Item><Ma_Cuc>01</Ma_Cuc><Ten_Cuc>Cục Hải quan Hà Nội</Ten_Cuc><Ma_HQ_PH>01PR</Ma_HQ_PH><Ma_HQ_CQT>2995236</Ma_HQ_CQT><Ten_HQ_PH>Chi cục HQ Vĩnh Phúc</Ten_HQ_PH><Ma_DV>2500260507</Ma_DV><Ten_DV>Công ty Trách nhiệm hữu hạn một thành viên thương mại và xuất nhập khẩu PRIME</Ten_DV><Ma_Chuong>552</Ma_Chuong><Ma_NTK>2</Ma_NTK><Ten_NTK>TK Tạm gửi của cơ quan Hải quan</Ten_NTK><Ma_KB>1219</Ma_KB><Ten_KB>KBNN Phúc Yên  - Vĩnh Phúc</Ten_KB><TKKB>3591</TKKB><DuNo_TO>20000</DuNo_TO><CT_No><Ma_HQ>01PR</Ma_HQ><Ten_HQ>Chi cục HQ Vĩnh Phúc</Ten_HQ><Ma_LH>A12</Ma_LH><Ten_LH>Nhập kinh doanh sản xuất</Ten_LH><Nam_DK>2014</Nam_DK><So_TK>10003660510</So_TK><Ngay_DK>2014-06-13</Ngay_DK><LoaiThue>LP</LoaiThue><Khoan>000</Khoan><TieuMuc>3052</TieuMuc><DuNo>20000</DuNo></CT_No></Item></Data><Error><ErrorNumber>0</ErrorNumber><ErrorMessage>Xử lý thành công</ErrorMessage></Error><Signature xmlns='http://www.w3.org/2000/09/xmldsig#'><SignedInfo><CanonicalizationMethod Algorithm='http://www.w3.org/TR/2001/REC-xml-c14n-20010315' /><SignatureMethod Algorithm='http://www.w3.org/2000/09/xmldsig#rsa-sha1' /><Reference URI=''><Transforms><Transform Algorithm='http://www.w3.org/2000/09/xmldsig#enveloped-signature' /></Transforms><DigestMethod Algorithm='http://www.w3.org/2000/09/xmldsig#sha1' /><DigestValue>T0nyzJX48y9sa1EMe+bU7iAgVYo=</DigestValue></Reference></SignedInfo><SignatureValue>jxvVOTAK8algsOgXH094x6C2J0OsYUvsqmKl0criFBEQPfYYGStZUyYVKr6hnSM5yPfxnRdPNEfOcY2zqbnX4B3hwtr33T8fLWqqhkR+GKx7y8JnwpNYgZLI6Vj0VcsFlNWUAOTaY8azTC9XdlIoK+NlDh8LpdOCrcyCTRPcwfZ6sxUK9ikHG3qskQ6fBF6Jb69Asz4fvUosPGDsqF9CjWWKTXzeFMtYvU5l/9Ww7QiCL+RrBymQgpkBXt0v5AlkX+KN+mNFslWes2QKcWaO97BG4dvZkd2vzjV2ctwiYgBMLvqOitqCBNMIY4E55kmj616DdnpCAwFyPQ4iyhxDMw==</SignatureValue><KeyInfo><X509Data><X509IssuerSerial><X509IssuerName>CN=VNPT Certification Authority, OU=VNPT-CA Trust Network, O=VNPT Group, C=VN</X509IssuerName><X509SerialNumber>111663843396468347755383557710472524059</X509SerialNumber></X509IssuerSerial><X509Certificate>MIIF/zCCA+egAwIBAgIQVAGsiVCsCs6lmTyX/l+xGzANBgkqhkiG9w0BAQUFADBpMQswCQYDVQQGEwJWTjETMBEGA1UEChMKVk5QVCBHcm91cDEeMBwGA1UECxMVVk5QVC1DQSBUcnVzdCBOZXR3b3JrMSUwIwYDVQQDExxWTlBUIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MDIxMDE1MjY0M1oXDTE3MDcwMzA5MjEwMFowdTELMAkGA1UEBhMCVk4xEjAQBgNVBAgMCUjDoCBO4buZaTEVMBMGA1UEBwwMQ+G6p3UgR2nhuqV5MRkwFwYDVQQKDBBC4buYIFTDgEkgQ0jDjU5IMSAwHgYDVQQDDBdU4buUTkcgQ+G7pEMgSOG6okkgUVVBTjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL69JOx8ksSE/mZtvhPdpVMV+ZnfZrWLSsxN87b2APMIpVboxq5HmE4Xs8nvEPasymsu0xBn5tTxMI15aAfxspQ5Ks4oDuMkwoO6L4WPiVwxvWtBuLCAdSa0aSVsy98Mod8Frcai6NwygVodojphfl0+88IYgc30it991vfgeh4cdimP5psNElpWlKQVa04VpB99dclDd/Nr3m4wZvyT5Th8s7vBbBPzVtr9ebv75LaM27OG8NBPaHEtJACeoaadla4tTNCPSrRCS4x8Oon4lBb4N0JyC2MOgUjCS51fiB6AVtfqCoS1NzHrCot8/JiHag8+CHH2afwWCZt6mokSKGUCAwEAAaOCAZUwggGRMHAGCCsGAQUFBwEBBGQwYjAyBggrBgEFBQcwAoYmaHR0cDovL3B1Yi52bnB0LWNhLnZuL2NlcnRzL3ZucHRjYS5jZXIwLAYIKwYBBQUHMAGGIGh0dHA6Ly9vY3NwLnZucHQtY2Eudm4vcmVzcG9uZGVyMB0GA1UdDgQWBBQIfYTiUnPDcfy7IZUiDO0vxzbJRTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFAZpwNXVAooVjUZ96XziaApVrGqvMGsGA1UdIARkMGIwYAYNKwYBBAGB7QMBAQMBBTBPMCYGCCsGAQUFBwICMBoeGABEAEkARAAtAEIAMgAuADAALQAzADYAbTAlBggrBgEFBQcCARYZaHR0cDovL3B1Yi52bnB0LWNhLnZuL3JwYTAxBgNVHR8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZucHQtY2Eudm4vdm5wdGNhLmNybDAOBgNVHQ8BAf8EBAMCBPAwHwYDVR0lBBgwFgYIKwYBBQUHAwIGCisGAQQBgjcKAwwwDQYJKoZIhvcNAQEFBQADggIBAB5iI3MXO5KdLT8grKhfKrzDH/VBR/qoBBRRpMrVC552fRpMEIWw08oOcGw2Z3XsUQ9dHSlawLGh3y8/qNXxCZWGUcE5Zeh9l76WDGAB5LSpUNLmD6Na25GBg3UznqKrTuETGMTgwyKYmLvOzPy2xpihtg1J2A5bsXC39ZbTthOyHtinlG/wmx4h9tGBQGXUaSBmVIt/bBw9BsN7yjZYQvAc1AIIegyFx28YY9Ycvfh2BbAStikHIIF22hTOe8/jbxLmQRrFj8J9yKjmQW8BvfvhTcxWS2MgZNrjEwJiYr/NK6iPNxYmCreP7aw74Uy659t/fX/WxOZe3HU8bZFJiudlWSgizKQpsx3Pk1YmPFYj2Ex1YPpUKscD9IStsMgWt/ylylZpupXvhNvW5V82tmRslKob8xnxsHfuKWaWk/GtvkziCdzSWqvU18d854YMMg5c+ZsQHvYVBN5HoIzHPvZHwEvsZtZYMYB8tcuoZGKuYeJwgXWYsucccXrR0R025Du86EQ/URAorkJTFaC6RfSLbvxPNBBv9fAgntf4ZPpaF+7CgTEwpC4K6QHIddGle6cyN8nAz1e9l9jtWtb+KOLdPvzLjTZbPgiJKNjw65LXu8NBzziwzyknK/MMIWJ7M5WKU5QXJyu3ahPONzcL22xqHgIikjh1jO4sivjTdXjQ</X509Certificate></X509Data></KeyInfo></Signature></Customs>"
            'End If
            sXMLPhanHoi = sXMLPhanHoi.Replace("<Customs>", "<Customs xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns=""http://www.cpandl.com"">")

            Dim msg202Converter As New CustomsV3.MSG.MSG202.MSG202()
            Dim oMSG202 As CustomsV3.MSG.MSG202.MSG202
            oMSG202 = msg202Converter.MSGToObject(sXMLPhanHoi)

            If oMSG202.Error.ErrorNumber <> "0" Then
                Throw New Exception("HQ_Error:" & oMSG202.Error.ErrorMessage)
            Else
                sRet = Newtonsoft.Json.JsonConvert.SerializeObject(oMSG202)
            End If
        Catch ex As Exception
            sRet = String.Empty

            If ex.Message.StartsWith("HQ_Error:") Then
                Throw New Exception("Lỗi trả về từ hải quan: " & ex.Message.Substring(9))
            Else
                'Throw ex
                Throw New Exception("Có lỗi trong quá trình lấy thông tin người nộp thuế")
            End If
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetCTU(ByVal sSoCT As String) As String
        Dim sRet As String = ""
        Dim sMaNV As String = ""
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If

            sMaNV = HttpContext.Current.Session("User")

            Dim objCTU As NewChungTu.infChungTu = Nothing
            objCTU = HaiQuan.HaiQuanController.GetChungTu(sSoCT)

            If objCTU IsNot Nothing Then
                'modify something before serialize to json object
                If objCTU.HDR IsNot Nothing AndAlso Not String.IsNullOrEmpty(objCTU.HDR.Ngay_HT) AndAlso objCTU.HDR.Ngay_HT.Length >= 10 Then
                    objCTU.HDR.Ngay_HT = objCTU.HDR.Ngay_HT.Substring(0, 10)
                End If
                If objCTU.ListDTL IsNot Nothing AndAlso objCTU.ListDTL.Count > 0 Then
                    For Each DTL In objCTU.ListDTL
                        If Not String.IsNullOrEmpty(DTL.NGAY_TK) AndAlso DTL.NGAY_TK.Length >= 10 Then
                            DTL.NGAY_TK = DTL.NGAY_TK.Substring(0, 10)
                        End If
                    Next
                End If

                sRet = Newtonsoft.Json.JsonConvert.SerializeObject(objCTU)
            End If
        Catch ex As Exception
            sRet = String.Empty
            Throw New Exception("Có lỗi trong quá trình lấy thông tin chứng từ")
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Ghi_CTu(ByVal ctuHDR As NewChungTu.infChungTuHDR, ByVal arrDTL() As NewChungTu.infChungTuDTL, ByVal sAction As String, ByVal sSaveType As String) As String
        Dim sMaNV As String = ""
        Dim sMa_DThu As String = ""
        Dim sRet As String = String.Empty
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sRet = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If
            'If Not ValidUserSession() Then
            '    sRet = "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            '    Throw New Exception(sRet)
            'End If

            sMaNV = HttpContext.Current.Session("User")
            sMa_DThu = HaiQuan.HaiQuanController.GetMaDiemThu(sMaNV)

            If String.IsNullOrEmpty(sMaNV) Then
                sRet = "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
                Throw New Exception(sRet)
            End If

            Dim listDTL As New List(Of NewChungTu.infChungTuDTL)
            If arrDTL IsNot Nothing AndAlso arrDTL.Length > 0 Then
                listDTL.AddRange(arrDTL)
            End If
            For Each DTL In listDTL
                DTL.Ma_DThu = sMa_DThu
            Next

            If ctuHDR IsNot Nothing Then
                ctuHDR.Ma_DThu = sMa_DThu
                ctuHDR.Ma_NV = CInt(sMaNV) 'get current user login

                ctuHDR.Ma_CN = HttpContext.Current.Session("MA_CN_USER_LOGON")
                ctuHDR.Ngay_TK = IIf(ctuHDR.Ngay_TK Is Nothing, Date.Now.ToString("dd/MM/yyyy"), ctuHDR.Ngay_TK)
                ctuHDR.LH_XNK = IIf(ctuHDR.LH_XNK Is Nothing, "", ctuHDR.LH_XNK)
                ctuHDR.NGAY_KH_NH = IIf(ctuHDR.NGAY_KH_NH Is Nothing, Date.Now.ToString("dd/MM/yyyy"), ctuHDR.NGAY_KH_NH)
                ctuHDR.LOAI_TT = IIf(ctuHDR.LOAI_TT Is Nothing, "", ctuHDR.LOAI_TT)

                Dim ref_no As String = String.Empty

                ctuHDR.RM_REF_NO = clsCTU.GenerateRMRef(clsCTU.TCS_GetMaChiNhanh(ctuHDR.Ma_NV), ref_no)
                ctuHDR.REF_NO = ref_no

                sRet = HaiQuan.HaiQuanController.GhiChungTu(ctuHDR, listDTL, sAction, sSaveType)
            End If

        Catch ex As Exception
            Select Case sAction
                Case "GHI"
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình hoàn thiện chứng từ" & ex.Message & "-" & ex.StackTrace)
                    Throw ex
                Case "GHIKS"
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình hoàn thiện & gửi kiểm soát chứng từ" & ex.Message & "-" & ex.StackTrace)
                    Throw ex
                Case "GHIDC"
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình điều chỉnh & gửi kiểm soát chứng từ" & ex.Message & "-" & ex.StackTrace)
                    Throw ex
                Case Else
                    Throw ex
            End Select
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Huy_CTU(ByVal sSoCT As String, ByVal sLyDo As String) As String
        Dim sRet As String = String.Empty
        Dim sMaNV As String = String.Empty
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sRet = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If

            sMaNV = HttpContext.Current.Session("User")

            sRet = HaiQuan.HaiQuanController.HuyChungTu(sSoCT, sLyDo, sMaNV)
        Catch ex As Exception
            sRet = ""
            Throw New Exception(ex.Message)
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function LoadCTList(ByVal sMaNV As String, ByVal sSoCTu As String) As String
        Dim dt As DataTable = Nothing
        Dim sTable As String = ""
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sTable = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If
            'If Not ValidUserSession() Then
            '    Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            'End If
            dt = mdlCommon.DataSet2Table(HaiQuan.HaiQuanController.GetListChungTu(clsCTU.TCS_GetNgayLV(sMaNV), sMaNV, HaiQuan.HaiQuanController.GetMaDiemThu(sMaNV), "04", sSoCTu, "P"))

            sTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width:100%;border-collapse:collapse;'>"

            Dim i As Integer = 0
            For Each dr As DataRow In dt.Rows
                sTable &= "<tr onclick=jsGetCTU('" & dr("SO_CT").ToString & "') " & "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"
                sTable &= "<td align='center' style='width: 20%'><img src='" & Business_HQ.CTuCommon.Get_ImgTrangThai(dr("TRANG_THAI").ToString, dr("tt_cthue").ToString, dr("ma_ks").ToString) & "' alt='" & Business_HQ.CTuCommon.Get_TrangThai(dr("TRANG_THAI").ToString) & "' /></td>"
                sTable &= "<td align='center' style='width: 30%'><a href='#'>" & dr("SO_CT").ToString & "</a></td>"
                sTable &= "<td align='center' style='width: 50%'>" & dr("TEN_DN").ToString & "/" & dr("SO_BT").ToString & "</td>"
                sTable &= "</tr>"
            Next
            sTable &= "<tr style='height: 24px;padding-right:2px;'><td align='right' colspan='3'>Tổng số chứng từ: <span style='font-weight:bold'>" & dt.Rows.Count & "</span></td></tr>"
            sTable &= "</table>"
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi dữ liệu chi tiết chứng từ", HttpContext.Current.Session("TEN_DN"))
            sTable = String.Empty
        End Try

        Return sTable
    End Function

    <System.Web.Services.WebMethod()> _
      Public Shared Function GetTK_KH_NH(ByVal pv_strTKNH As String) As String
        Dim sRet As String = String.Empty
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If
            Dim sMaCN As String = String.Empty
            sMaCN = HttpContext.Current.Session("MA_CN_USER_LOGON")

            sRet = Business_HQ.HaiQuan.HaiQuanController.GetTaiKhoanKH_NH(pv_strTKNH, sMaCN)
            'sua cho kienlongbank
            'Dim clsCore As New clsCoreBankUtilMSB()
            'Dim descAccount As String = String.Empty
            'Dim avaiBalance As String = String.Empty
            'Dim erroCode As String = String.Empty
            'Dim erroMessage As String = String.Empty

            'Dim dataKH As Business.infTK_KH_TH = clsCore.GetCustomerInfoInstance(pv_strTKNH, descAccount, avaiBalance, erroCode, erroMessage)

            'If dataKH IsNot Nothing Then
            '    Return JsonConvert.SerializeObject(dataKH)
            'Else
            '    Return Nothing
            'End If

        Catch ex As Exception
            sRet = String.Empty
            Throw ex
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenQuanHuyen(ByVal pv_strMaNV As String) As String
        Return clsCTU.Get_TenQuanHuyen(pv_strMaNV)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenTinh(ByVal pv_strMaNV As String) As String
        Return clsCTU.Get_TenTinhTP(pv_strMaNV)
    End Function

    <System.Web.Services.WebMethod()> _
   Public Shared Function Get_TenNNT(ByVal sMaNNT As String) As String
        Dim sRet As String = String.Empty
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If

            sRet = HaiQuan.HaiQuanController.GetTenNguoiNopThue(sMaNNT)
        Catch ex As Exception
            sRet = String.Empty
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenMaHQ(ByVal sMaHQ As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return HaiQuan.HaiQuanController.GetDataByMaHQ(sMaHQ)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_DataByCQT(ByVal sMaCQT As String, ByVal sMaHQ As String, ByVal sSHKB As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return HaiQuan.HaiQuanController.GetDataByCQT(sMaCQT, sMaHQ, sSHKB)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetData_SHKB(ByVal sLoaiDM As String, ByVal sSHKB As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return HaiQuan.HaiQuanController.GetDataBySHKB(sLoaiDM, sSHKB)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTT_NH(ByVal sSHKB As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return HaiQuan.HaiQuanController.GetTT_NganHang(sSHKB)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function onBlurMaTieuMuc(ByVal v_maTieuMuc As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Dim v_dt As DataTable = Nothing

        v_dt = HaiQuan.HaiQuanController.onBlurMaTieuMuc(v_maTieuMuc)

        If (v_dt IsNot Nothing) Then
            If (v_dt.Rows.Count > 0) Then
                Return Newtonsoft.Json.JsonConvert.SerializeObject(v_dt)
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function getNoiDungByMaTieuMuc(ByVal v_jSon As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Dim v_dt As DataTable = Nothing

        v_dt = HaiQuan.HaiQuanController.getNoiDungByMaTieuMuc(v_jSon)

        If (v_dt IsNot Nothing) Then
            If (v_dt.Rows.Count > 0) Then
                Return Newtonsoft.Json.JsonConvert.SerializeObject(v_dt)
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
End Class
