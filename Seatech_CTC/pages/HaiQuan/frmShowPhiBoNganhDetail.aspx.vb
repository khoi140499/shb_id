﻿Imports Business_HQ
Imports System.Data
Imports log4net
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common
Imports VBOracleLib
Imports log4net.Repository.Hierarchy

Partial Class pages_HaiQuan_frmShowPhiBoNganhDetail
    Inherits System.Web.UI.Page

    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim v_strSo_CT As String = ""
            If Not Request.QueryString.Item("So_CT") Is Nothing Then v_strSo_CT = Request.QueryString.Item("So_CT").ToString()


            

            If (Not v_strSo_CT Is Nothing) Then

                Dim ds As DataSet = New DataSet()
                ds = HaiQuan.HaiQuan.GET_CT_PHI_BO_NGANH(v_strSo_CT)

                If ds.Tables.Count > 0 Then
                    FillDataToForm(ds)
                End If
                'Dat lai gia tri
                hdSO_CT.Value = v_strSo_CT
               


            End If

        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    Protected Sub FillDataToForm(ByVal dsCT As DataSet)
        Dim dr As DataRow = dsCT.Tables(0).Rows(0)
        textBoxLyDoTra.Text = ""
        textBoxSoHoso.Text = dr("so_hs").ToString()
        textBoxMa_DVQL.Text = dr("MA_DVQL").ToString()
        textBoxMa_NH_PH.Text = dr("MA_NH_PH").ToString()
        textBoxTen_NH_PH.Text = dr("TEN_NH_PH").ToString()
        textBoxTen_DVQL.Text = dr("TEN_DVQL").ToString()
        textBoxKyHieu_CT.Text = dr("KYHIEU_CT").ToString()
        textBoxSo_CT.Text = dr("SO_CT").ToString()
        textBoxNam_CT.Text = dr("NAM_CT_PT").ToString()
        textBoxMST.Text = dr("NNT_MST").ToString()
        textBoxTen_NNT.Text = dr("NNT_TEN_DV").ToString()
        textBoxDiaChi.Text = dr("NNT_DIACHI").ToString()
        textBoxMa_NT.Text = dr("TT_NT_MA_NT").ToString()
        textBoxTyGia.Text = dr("TT_NT_TYGIA").ToString()
        textBoxTongTien_NT.Text = VBOracleLib.Globals.Format_Number2(dr("TT_NT_TONGTIEN_NT").ToString())
        textBoxTongTien_VND.Text = VBOracleLib.Globals.Format_Number2(dr("TongTien_Truoc_Vat").ToString())
        textBox_Ma_NH_TH.Text = dr("TAIKHOAN_NT_MA_NH_TH").ToString()
        textBox_Ten_NH_TH.Text = dr("TAIKHOAN_NT_TEN_NH_TH").ToString()
        textBox_TaiKhoan_TH.Text = dr("TAIKHOAN_NT_TK_TH").ToString()
        textBox_Ten_TaiKhoan_TH.Text = dr("TAIKHOAN_NT_TEN_TK_TH").ToString()
        NNT_QUAN_HUYEN.Text = dr("NNT_QUAN_HUYEN").ToString()
        NNT_TINH_TP.Text = dr("NNT_TINH_TP").ToString()
        NgayLap_CT.Text = Convert.ToDateTime(dr("NGAY_CT").ToString()).ToString("dd/MM/yyyy")
        TT_Khac.Text = dr("NNT_TT_KHAC").ToString()
        'TK_Thanh_Toan_DiaChi.Text = dr("TK_Thanh_Toan_DiaChi").ToString()
        textBoxSoDu.Text = dr("SO_DU").ToString()
        textBoxTen_TKC.Text = dr("TEN_TK_CHUYEN").ToString()
        'TT_NT_So_CMND.Text = dr("TT_NT_So_CMND").ToString()
        'TT_NT_So_DT.Text = dr("TT_NT_So_DT").ToString()
        'TT_NT_TK_THANH_TOAN_QUAN_HUYEN.Text = dr("TK_THANH_TOAN_QUAN_HUYEN").ToString()
        'TT_NT_TK_THANH_TOAN_TINH_TP.Text = dr("TK_THANH_TOAN_TINH_TP").ToString()
        textBoxLyDoTra.Text = dr("Lydo_huy_KSV").ToString()

        hdfMa_NV.Value = dr("MA_NV").ToString()
        hdfRM_REF_NO.Value = dr("RM_REF_NO").ToString()
        hdfREF_NO.Value = dr("REF_NO").ToString()

        txtCharge.Text = VBOracleLib.Globals.Format_Number2(dr("FEE").ToString())
        txtVAT.Text = VBOracleLib.Globals.Format_Number2(dr("VAT").ToString())
        Dim tongtien_truoc_vat As String = dr("TongTien_Truoc_Vat").ToString()
        Dim fee As String = dr("FEE").ToString()
        Dim vat As String = dr("VAT").ToString()
        Dim tong_trich_no As String = Double.Parse(tongtien_truoc_vat) + Double.Parse(fee) + Double.Parse(vat)
        txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number2(Double.Parse(tong_trich_no))


        Dim v_PTTT_Status As String = String.Empty
        Dim v_PTTT_Value As String
        If dr("PTTT") Is Nothing Then
            v_PTTT_Status = "3"
            v_PTTT_Value = "Nộp qua GL"
        ElseIf dr("PTTT") = "3" Then

            v_PTTT_Value = "Nộp qua GL"

        Else
            v_PTTT_Status = dr("PTTT").ToString()
            v_PTTT_Value = "Chuyển khoản"
        End If

        textBoxPTTT.Text = v_PTTT_Value
        textBoxTKC.Text = dr("TK_THANHTOAN").ToString()

        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu

        Dim dsChiTietCT As DataSet = HaiQuan.HaiQuan.ListChiTietBySoCT(dr("SO_CT").ToString())

        Dim v_dt As New DataTable()
        'v_dt.Columns.Add(New DataColumn("TAIKHOAN_NT_MA_NH_TH"))
        'v_dt.Columns.Add(New DataColumn("TAIKHOAN_NT_TEN_NH_TH"))
        'v_dt.Columns.Add(New DataColumn("TAIKHOAN_NT_TK_TH"))
        'v_dt.Columns.Add(New DataColumn("TAIKHOAN_NT_TEN_TK_TH"))


        dataGridViewLePhi.Controls.Clear()

        hdTrangThai_XuLy.Value = dr("TRANGTHAI_XULY").ToString()
        hdTrangThai_CT.Value = dr("TRANGTHAI_CT")

        labelMoneyByText.Text = mdlCommon.TCS_Dich_So(Double.Parse(txtTongTrichNo.Text), "đồng")

        labelSoCT.Text = dr("SO_CT").ToString()

        If hdTrangThai_XuLy.Value.Equals("1") And hdTrangThai_CT.Value.Equals("02") Then
            textBoxLyDoTra.ReadOnly = False
        ElseIf hdTrangThai_XuLy.Value.Equals("0") And hdTrangThai_CT.Value.Equals("02") Then
            textBoxLyDoTra.ReadOnly = False
        ElseIf hdTrangThai_CT.Value.Equals("05") Then
            textBoxLyDoTra.ReadOnly = False
        Else
            textBoxLyDoTra.ReadOnly = True

        End If
        Dim mdl As New mdlCommon()
        dataGridViewLePhi.DataSource = dsChiTietCT
        dataGridViewLePhi.DataBind()
        For i As Integer = 0 To dataGridViewLePhi.Rows.Count - 1
            For j As Integer = 0 To dataGridViewLePhi.Rows(i).Cells.Count - 1
                dataGridViewLePhi.Rows(i).Cells(4).Text = VBOracleLib.Globals.Format_Number2(dataGridViewLePhi.Rows(i).Cells(4).Text)
                dataGridViewLePhi.Rows(i).Cells(5).Text = VBOracleLib.Globals.Format_Number2(dataGridViewLePhi.Rows(i).Cells(5).Text)

            Next
        Next


        'Lay so du
        'sua cho kienlongbank
        'Dim v_objCoreBank As New clsCoreBankUtilMSB
        'If v_PTTT_Status.Equals("1") Then
        '    Dim v_strDescTKKH As String = String.Empty
        '    Dim v_strSoDuNH As String = String.Empty
        '    Dim v_strErrorCode As String = String.Empty
        '    Dim v_strErrorMsg As String = String.Empty

        '    If (v_objCoreBank.GetCustomerInfo(textBoxTKC.Text.Trim, v_strDescTKKH, v_strSoDuNH, v_strErrorCode, v_strErrorMsg).Equals("0")) Then
        '        textBoxTen_TKC.Text = v_strDescTKKH
        '        textBoxSoDu.Text = VBOracleLib.Globals.Format_Number(v_strSoDuNH, ",")
        '    Else
        '        lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
        '        Exit Sub
        '    End If
        '    trSoDuTK.Visible = True
        'Else
        '    trSoDuTK.Visible = False
        'End If
    End Sub

End Class
