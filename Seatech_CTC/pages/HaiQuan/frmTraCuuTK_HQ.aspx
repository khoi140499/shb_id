﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmTraCuuTK_HQ.aspx.vb" Inherits="pages_ChungTu_frmTraCuuTK_HQ" Title="Tra cứu thông tin tờ khai hải quan"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../../jsdropdowlist/jquery.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">TRA CỨU THÔNG TIN TỜ KHAI HẢI QUAN</asp:Label>
            </td>
        </tr>
        <tr>
            <td width='100%' align="left" valign="top" class='nav'>
                <div id="divTracuu">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width='90%' align="left" valign="top">
                                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                    <tr>
                                        <td width='100' align="left" valign="top" class="form_label">
                                            Mã số thuế: <span class="requiredField">(*)</span>
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtMaNNT_TK' class="inputflat" runat="server" style="width: 60%" />
                                        </td>
                                        <td align="left" valign="top" class="form_label">
                                            Số tờ khai:<span class="requiredField">(*)</span>
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtSo_TK_TK' class="inputflat" runat="server" style="width: 80%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='15%' align="left" valign="top" class="form_label">
                                            Năm đăng ký:<span class="requiredField">(*)</span>
                                        </td>
                                        <td width='35%' align="left" valign="top" class="form_control">
                                            <input type="text" id='txtNamDK' maxlength='13' runat="server" class="inputflat" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%-- <tr>
                            <td>
                                TEST XML: 
                                <asp:TextBox ID="txtTestMSG" runat="server" style="width: 100%;"></asp:TextBox>
                            </td>
                        </tr>--%>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnTraCuu" runat="server" Text="Tra Cứu" CssClass="ButtonCommand" />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnIn" runat="server" Text="In TK" CssClass="ButtonCommand" />
                                <asp:HiddenField ID="hdTT_TK" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="divKQXK" runat="server">
                    <p>
                        <b>Thông tin tờ khai xuất khẩu</b></p>
                    <table id="grdHeader" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 97%; border-collapse: collapse;">
                        <tr>
                            <td width="30%">
                                <b>Mã hải quan/Tên hải quan</b>
                            </td>
                            <td width="30%">
                                <asp:Label id="txtMaHQ" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td width="40%">
                                <asp:Label id="txtTenHQ" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã loại hình</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtMaLH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã xuất nhập</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtMaXN" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày đăng ký</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNgayDK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số tờ khai</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtSoTK" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số tờ khai đầu tien</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtSoTKDauTien" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số tờ khai tạm nhập tái xuất</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtSoTK_TamN_TX" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Thời hạn tái nhập tái xuất</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtThoiHan_TaiN_TX" class="inputflat" style="width: 99%;"
                                    runat="server" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày thay đổi đăng ký</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNgayTD_DK" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày thay đổi kiểm tra</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNgayTD_KT" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày hoàn thành kiểm tra</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNgayHT_KT" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày thông quan</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNgay_thong_quan" class="inputflat" style="width: 99%;"
                                    runat="server" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày hàng hóa qua khu vực giám sát</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNgayHHQua_KVGS" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã phân loại kiểm tra</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtMaPL_KT" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>Người xuất khẩu</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã số thuế/Tên</b>
                            </td>
                            <td>
                                <asp:Label id="txtMST_XK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtTenNNT_XK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtDiaChiNNT_XK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>Người ủy thác xuất nhập khẩu</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã số thuế/Tên</b>
                            </td>
                            <td>
                                <asp:Label id="txtMST_UT" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtTenNNT_UT" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>Người nhập khẩu</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã số thuế/Tên</b>
                            </td>
                            <td>
                                <asp:Label id="txtMST_NK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtTenNNT_NK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ 1</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtDiaChi_NK1" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ 2</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtDiaChi_NK2" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ 3</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtDiaChi_NK3" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ 4</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtDiaChi_NK4" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã nước</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtMaNuoc" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>&nbsp;</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên đại lý hải quan</b>
                            </td>
                            <td>
                                <asp:Label id="txtMaDL_HQ" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtTenDL_HQ" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số vận đơn 1</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtSoVD1" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số lượng</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtSoLuong" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Đợn vị tính số lượng</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtDVT_SL" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Tổng trọng lượng</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtTong_TL" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Đơn vị tính tổng trọng lượng</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtDVT_TTL" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên địa điểm lưu kho</b>
                            </td>
                            <td>
                                <asp:Label id="txtMaDD_LuuKho" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtTenDD_LuuKho" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên địa điểm nhận hàng cuối cùng</b>
                            </td>
                            <td>
                                <asp:Label id="txtMaDD_NH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtTenDD_NH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên địa điểm xếp hàng</b>
                            </td>
                            <td>
                                <asp:Label id="txtMaDD_XH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtTenDD_XH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên phương tiện vận chuyển</b>
                            </td>
                            <td>
                                <asp:Label id="txtMa_PT_VC" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtTen_PT_VC" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số hóa đơn</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtSo_HD" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày phát hành</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNgay_PH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Phương thức thanh toán</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtPT_TT" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Tổng/Nguyên tệ trị giá hóa đơn</b>
                            </td>
                            <td>
                                <asp:Label id="txtTong_GT_HD" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtNT_Tong_GT_HD" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Tổng trị giá tính thuế</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtTong_GT_Thue" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 1</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtGP_XNK1" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 2</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtGP_XNK2" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 3</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtGP_XNK3" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 4</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtGP_XNK4" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 5</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtGP_XNK5" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã địa điểm xếp hàng lên xe chở hàng 1</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtMaDD_XH1" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã địa điểm xếp hàng lên xe chở hàng 2</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtMaDD_XH2" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã địa điểm xếp hàng lên xe chở hàng 3</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtMaDD_XH3" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã địa điểm xếp hàng lên xe chở hàng 4</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtMaDD_XH4" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã địa điểm xếp hàng lên xe chở hàng 5</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtMaDD_XH5" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Tên địa điểm xếp hàng lên xe chở hàng</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtTenDD_LXH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                    </table>
                    <p>
                        <b>Thông tin hàng hóa</b></p>
                    <table id="Table1" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 97%; border-collapse: collapse;">
                        <tr>
                            <td>
                                <b>Mã hàng</b>
                            </td>
                            <td>
                                <b>Tên hàng</b>
                            </td>
                            <td>
                                <b>Trị giá tính thuế</b>
                            </td>
                            <td>
                                <b>Tiền thuế</b>
                            </td>
                            <td>
                                <b>Mã tiền tệ</b>
                            </td>
                        </tr>
                        <asp:Literal ID="ltHangXK" runat="server"></asp:Literal>
                        <%--<tr>
                            <td>Mã hàng</td>
                            <td>Tên hàng</td>
                            <td>Trị giá tính thuế</td>
                            <td>Tiền thuế</td>
                            <td>Mã tiền tệ</td>
                        </tr>     --%>
                    </table>
                </div>
                <div id="divKQNK" runat="server">
                    <p>
                        <b>Thông tin tờ khai nhập khẩu</b></p>
                    <table id="Table2" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 97%; border-collapse: collapse;">
                        <tr>
                            <td width="30%">
                                <b>Mã hải quan/Tên hải quan</b>
                            </td>
                            <td width="30%">
                                <asp:Label id="txtNK_MaHQ" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td width="40%">
                                <asp:Label id="txtNK_TenHQ" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã loại hình</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_MaLH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã xuất nhập</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_MaXN" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày đăng ký</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_NgayDK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số tờ khai</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_SoTK" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số tờ khai đầu tiên</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_SO_TK_DAU_TIEN" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số tờ khai tạm nhập tái xuất</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_SO_TK_TAM_NHAP_TX" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Thời hạn tái nhập tái xuất</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_THOI_HAN_TAI_NHAP_TAI_XUAT" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <b>Ngày thay đổi đăng ký</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_NGAY_THAY_DOI_DK" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <b>Ngày thay đổi kiểm tra</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_NGAY_THAY_DOI_KT" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày hoàn thành kiểm tra</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_NGAY_HOAN_THANH_KT" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày thông quan</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_NGAY_THONG_QUAN" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                          <tr>
                            <td>
                                <b>Ngày hàng hoá qua khu vực giám sát</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_NGAY_HH_QUA_KVGS" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <b>Mã phân loại kiểm tra</b>
                            </td>
                            <td >
                                <asp:Label id="txtNK_MA_PHAN_LOAI_KT" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                              <td >
                                <asp:Label id="txtNK_MA_PHAN_LOAI_KT_DESC" class="inputflat" style="width: 99%;" runat="server"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>Người nhập khẩu</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã số thuế/Tên</b>
                            </td>
                            <td>
                                <asp:Label id="txtNK_MST_NK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtNKTenNNT_NK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_DiaChiNNT_NK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>Người ủy thác xuất nhập khẩu</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã số thuế/Tên</b>
                            </td>
                            <td>
                                <asp:Label id="txtNK_MST_UT" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtNK_TenNNT_UT" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>Người xuất khẩu</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã số thuế/Tên</b>
                            </td>
                            <td>
                                <asp:Label id="txtNK_MST_XK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtNK_TenNNT_XK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ 1</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_DiaChi_XK1" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ 2</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_DiaChi_XK2" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ 3</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_DiaChi_XK3" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Địa chỉ 4</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_DiaChi_XK4" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã nước</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_MaNuoc" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>&nbsp;</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Người ủy thác xuất khẩu</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_TenUT_XK" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên đại lý hải quan</b>
                            </td>
                            <td>
                                <asp:Label id="txtNK_MaDL_HQ" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtNK_TenDL_HQ" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số vận đơn 1</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_SoVD1" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số vận đơn 2</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_SoVD2" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số vận đơn 3</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_SoVD3" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số vận đơn 4</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_SoVD4" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số vận đơn 5</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_SoVD5" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số lượng</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_SoLuong" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Đợn vị tính số lượng</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_DVT_SL" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Tổng trọng lượng</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_Tong_TL" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Đơn vị tính tổng trọng lượng</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_DVT_TTL" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên địa điểm lưu kho</b>
                            </td>
                            <td>
                                <asp:Label id="txtNK_MaDD_LuuKho" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtNK_TenDD_LuuKho" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên địa điểm dỡ hàng</b>
                            </td>
                            <td>
                                <asp:Label id="txtMaDD_DH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtTenDD_DH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên địa điểm xếp hàng</b>
                            </td>
                            <td>
                                <asp:Label id="txtNK_MaDD_XH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtNK_TenDD_XH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Mã/Tên phương tiện vận chuyển</b>
                            </td>
                            <td>
                                <asp:Label id="txtNK_Ma_PT_VC" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtNK_Ten_PT_VC" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày hàng đến</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNgay_Hang_Den" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày được phép nhập kho đầu tiên</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNgay_Duoc_Phep_Nhap_Kho_DT" class="inputflat" runat="server"
                                    style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Số hóa đơn</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_So_HD" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày phát hành</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_Ngay_PH" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Phương thức thanh toán</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_PT_TT" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Tổng/Nguyên tệ trị giá hóa đơn</b>
                            </td>
                            <td>
                                <asp:Label id="txtNK_Tong_GT_HD" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                            <td>
                                <asp:Label id="txtNK_NT_Tong_GT_HD" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Tổng trị giá tính thuế</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_Tong_GT_Thue" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 1</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_GP_XNK1" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 2</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_GP_XNK2" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 3</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_GP_XNK3" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 4</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_GP_XNK4" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Giấy phép xuất nhập khẩu 5</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_GP_XNK5" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <b>Phí vận chuyển</b>
                            </td>
                            <td >
                                <asp:Label id="txtNK_PHI_VAN_CHUYEN" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                              <td >
                                <asp:Label id="txtNK_NGUYEN_TE_PHI_VAN_CHUYEN" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <b>Phí bảo hiểm</b>
                            </td>
                            <td >
                                <asp:Label id="txtNK_PHI_BAO_HIEM" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                              <td >
                                <asp:Label id="txtNK_NGUYEN_TE_PHI_BAO_HIEM" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <b>Mã Phân bảo hiểm</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_MA_PL_BAO_HIEM" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <b>Chi tiết khai trị giá</b>
                            </td>
                            <td colspan="2">
                                <asp:Label id="txtNK_CHI_TIET_KHAI_TRI_GIA" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <b>Thông tin tỷ giá tính thuế</b>
                            </td>
                            <td >
                                <asp:Label id="txtNK_TY_GIA_TINH_THUE" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                              <td >
                                <asp:Label id="txtNK_MA_NGUYEN_TE_TGTT" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                    </table>
                    <p>
                        <b>Thông tin hàng hóa</b></p>
                    <table id="Table3" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 97%; border-collapse: collapse;">
                        <tr>
                            <td>
                                <b>Mã hàng</b>
                            </td>
                            <td>
                                <b>Tên hàng</b>
                            </td>
                            <td>
                                <b>Nước nhập khẩu</b>
                            </td>
                            <td>
                                <b>Loại thuế</b>
                            </td>
                            <td>
                                <b>Trị giá tính thuế</b>
                            </td>
                            <td>
                                <b>Tiền thuế</b>
                            </td>
                            <td>
                                <b>Số tiền miễn giảm</b>
                            </td>
                        </tr>
                        <asp:Literal ID="ltHang_NK" runat="server"></asp:Literal>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
