﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmKiemSoatThueLePhi.aspx.vb" Inherits="pages_HaiQuan_frmKiemSoatThueLePhi"
    Title="Kiểm soát chứng từ thuế, phí, lệ phí bộ ngành" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">

    <script type="text/javascript" src="../../javascript/jquery/1.11.2/jquery.min.js"></script>

    <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
            <td class="pageTitle" colspan="2">
                <asp:Label ID="Label1" runat="server">KIỂM SOÁT CHỨNG TỪ THUẾ, PHÍ, LỆ PHÍ, BỘ NGÀNH</asp:Label>
            </td>
        </tr>
        <tr>
            <td width='100%' align="left" valign="top" class='nav' colspan="2">
                <div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan='2' align="left" class="errorMessage">
                <table border="0" width="100%">
                    <tr id="rowProgress">
                        <td colspan="2" align="left">
                            <div style="background-color: Aqua; display: none;" id="divProgress">
                                <b style="font-weight: bold; font-size: 15pt">Đang xử lý. Xin chờ một lát...</b>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <div id="divStatus" runat="server" style="font-weight: bold; width: 100%;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="20%" valign="top">
                <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                    ShowHeader="True" ShowFooter="True" BorderColor="#989898" CssClass="grid_data"
                    PageSize="15" Width="100%">
                    <PagerStyle CssClass="cssPager" />
                    <AlternatingRowStyle CssClass="grid_item_alter" />
                    <HeaderStyle CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                    <RowStyle CssClass="grid_item" />
                    <Columns>
                        <asp:TemplateField HeaderText="TT">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <img src=" <%#Business_HQ.CTuCommon.Get_ImgTrangThai_KS(Eval("trangthai_ct").ToString(),Eval("trangthai_xuly").ToString(),Eval("ma_ks").ToString())%>"
                                    alt=" <%#Business_HQ.CTuCommon.Get_TrangThai_KS(Eval("trangthai_ct").ToString())%>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Số CT" FooterText="Tổng số CT:">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <FooterStyle HorizontalAlign="Left"></FooterStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <%--CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SO_CT") & "," & DataBinder.Eval(Container.DataItem, "KYHIEU_CT") & "," & DataBinder.Eval(Container.DataItem, "MA_NV") & "," & Eval("TRANGTHAI_CT").ToString() & "," & Eval("SO_TN_CT").ToString()  %>'--%>
                                <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "so_ct") %>'
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SO_CT") & "," & DataBinder.Eval(Container.DataItem, "KYHIEU_CT") & "," & DataBinder.Eval(Container.DataItem, "MA_NV") & "," & Eval("TRANGTHAI_CT").ToString() & "," & "  , " & DataBinder.Eval(Container.DataItem, "NGAY_XULY") & " "%>'
                                    OnClick="btnSelectCT_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User HT/KS">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <FooterStyle Font-Bold="true" />
                            <ItemTemplate>
                                <asp:Label ID="lblMaNV" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ten_dn") %>' />
                                <asp:Label ID="lblDelimiter" runat="server" Text='/' />
                                <asp:Label ID="lblSoBT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ten_ks") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <%#grdDSCT.Rows.Count.ToString%>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                </asp:GridView>
                <br />
                <b>Trạng thái chứng từ:</b>
                <br />
                <br />
                <asp:DropDownList ID="ddlTrangThai" runat="server" CssClass="inputflat" Width="100%"
                    AutoPostBack="true">
                    <asp:ListItem Value="99" Text="Tất cả"></asp:ListItem>
                    <asp:ListItem Value="05" Text="Chờ Kiểm soát"></asp:ListItem>
                    <asp:ListItem Value="01" Text="Đã Kiểm soát"></asp:ListItem>
                    <asp:ListItem Value="03" Text="Chuyển trả"></asp:ListItem>
                    <asp:ListItem Value="04" Text="CT bị Hủy"></asp:ListItem>
                    <%--<asp:ListItem Value="02" Text="Giao dịch hủy - chờ duyệt"></asp:ListItem>--%>
                    <asp:ListItem Value="06" Text="Chuyển thuế/HQ lỗi"></asp:ListItem>
                    <%--<asp:ListItem Value="07" Text="Kiểm soát duyệt hủy GD"></asp:ListItem>
                    <asp:ListItem Value="08" Text="Duyệt hủy chuyển thuế/HQ lỗi"></asp:ListItem>--%>
                </asp:DropDownList>
                <br />
                <br />
                <table>
                    <tr style="display: none">
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuaKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Chưa Kiểm soát
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuyenKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Chờ Kiểm soát
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/DaKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Đã Kiểm soát
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/KSLoi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Chuyển trả
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            CT bị Hủy
                        </td>
                    </tr>
                    <tr style="display:none">
                        <td style="width: 10%">
                            <img src="../../images/icons/HuyHS.gif" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Giao dịch hủy - chờ duyệt
                        </td>
                    </tr>
                    <tr style="display:none">
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy_KS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Kiểm soát duyệt hủy GD
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/TT_CThueLoi.gif" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Chuyển thuế/HQ lỗi
                        </td>
                    </tr>
                    <tr style="display:none">
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy_CT_Loi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Duyệt hủy chuyển thuế lỗi
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy_CT_Loi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Hủy chuyển thuế/HQ lỗi
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td colspan="2" width='100%' align="left">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr class="grid_header">
                                    <td align="left">
                                        Chi tiết thông tin thuế, phí bộ ngành số chứng từ:
                                        <asp:Label ID="labelSoCT" runat="server" Text=""></asp:Label>
                                        <div id="divTTBDS" style="display: none">
                                        </div>
                                    </td>
                                    <td height='15' align="right">
                                    </td>
                                </tr>
                            </table>
                            <div id="pnlHeader" style="height: auto; width: 100%;">
                                <table id="Table1" cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px;
                                    border-style: solid; font-family: Verdana; font-size: 8pt; width: 99%; border-collapse: collapse;">
                                    <tr id="rowMaNNT">
                                        <td width="24%">
                                            <b>Số hồ sơ</b>
                                        </td>
                                        <td width="35%">
                                            <asp:TextBox ID="textBoxSoHoso" ReadOnly="true" runat="server" class="inputflat validate[required,minSize[1],maxSize[14],custom[integer]]"
                                                Style="width: 98%;" onblur="{checkMaNNT(this);}"></asp:TextBox>
                                            <asp:TextBox runat="server" ID="txtRM_REF_NO" CssClass="inputflat" Visible="false"
                                                ReadOnly="true" Text="" />
                                            <asp:TextBox runat="server" ID="txtREF_NO" CssClass="inputflat" Visible="false" ReadOnly="true"
                                                Text="" />
                                        </td>
                                        <td width="35%" style="height: 25px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã/ Tên DVQL</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxMa_DVQL" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTen_DVQL" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã/ Tên NH PH</b>
                                        </td>
                                        <td width="35%">
                                            <asp:TextBox ID="textBoxMa_NH_PH" runat="server" ReadOnly="true" class="inputflat"
                                                Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td width="35%">
                                            <asp:TextBox ID="textBoxTen_NH_PH" runat="server" ReadOnly="true" class="inputflat"
                                                Style="width: 98%;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Ký hiệu CT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxKyHieu_CT" ReadOnly="true" runat="server" class="inputflat"
                                                Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <b><i>Thông tin người nộp tiền</i></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Số/ Năm CT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxSo_CT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                            <asp:HiddenField ID="hdSO_CT" runat="server" />
                                            <asp:HiddenField ID="hdSO_TN_CT" runat="server" />
                                            <asp:HiddenField ID="hdTrangThai_CT" runat="server" />
                                            <asp:HiddenField ID="hdTrangThai_XuLy" runat="server" />
                                            <asp:HiddenField ID="hdfMa_NV" runat="server" />
                                            <asp:HiddenField ID="hdfMACN" runat="server" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxNam_CT" ReadOnly="true" runat="server" class="inputflat"
                                                Style="width: 98%;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>MST/ Tên NNT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxMST" runat="server" ReadOnly="true" class="inputflat" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTen_NNT" runat="server" class="inputflat validate[required]"
                                                ReadOnly="true" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Quận/ Huyện</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="NNT_QUAN_HUYEN" runat="server" class="inputflat validate[required]"
                                                ReadOnly="true" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="NNT_TINH_TP" runat="server" class="inputflat validate[required]"
                                                ReadOnly="true" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Địa chỉ</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxDiaChi" runat="server" class="inputflat validate[required]"
                                                ReadOnly="true" Style="width: 98%;"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_Khac" runat="server" class="inputflat validate[required]" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã NT/Tỷ giá</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxMa_NT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTyGia" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <b><i>Tài khoản chuyển tiền</i></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Ngày lập chứng từ</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="NgayLap_CT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Hình thức CT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxPTTT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr id="tableRowTienMat">
                                        <td>
                                            <b>Số CMND/ Số ĐT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_NT_So_CMND" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_NT_So_DT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr1">
                                        <td>
                                            <b>Địa chỉ thanh toán</b>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="TK_Thanh_Toan_DiaChi" runat="server" class="inputflat" Style="width: 99%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="tableRowQuanHuyen">
                                        <td>
                                            <b>Quận(Huyện) / Tỉnh (Thành phố)</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_NT_TK_THANH_TOAN_QUAN_HUYEN" runat="server" class="inputflat"
                                                Style="width: 98%;" ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TT_NT_TK_THANH_TOAN_TINH_TP" runat="server" class="inputflat" Style="width: 99%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>TKC/ Tên TK</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTKC" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxTen_TKC" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="rowsodu" runat="server">
                                        <td>
                                            <b>Số dư</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBoxSoDu" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <b><i>Tài khoản nộp tiền</i></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã/ Tên NH TH(*)</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBox_Ma_NH_TH" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBox_Ten_NH_TH" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mã/ Tên TK TH</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBox_TaiKhoan_TH" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textBox_Ten_TaiKhoan_TH" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="rowchecktaikhoanLienNgan" runat="server">
                                        <td>
                                            <b>Mã/ Tên NH TT</b>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMaNH_TT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTenNH_TT" runat="server" class="inputflat" Style="width: 98%;"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Diễn giải</b>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="textBoxDienGiai" runat="server" class="inputflat" Style="width: 99%;"
                                                ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' height='2'>
                        </td>
                    </tr>
                    <%--<tr class="grid_header" runat="server" id="Tr1">
                        <td colspan="4" align="left">
                            Thông tin chi tiết tài khoản nộp tiền
                        </td>
                    </tr>--%>
                    <tr runat="server" id="Tr2">
                        <td align="center" colspan="4">
                            <div id="div1" style="min-height: 200px; height: auto; width: 100%;">
                                <asp:GridView ID="dataGridViewLePhi" runat="server" BackColor="White" Width="100%"
                                    BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical"
                                    AutoGenerateColumns="False">
                                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                    <Columns>
                                        <asp:BoundField DataField="sSTT" HeaderText="ID" />
                                        <asp:TemplateField HeaderText="Chọn">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" Enabled="False" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NDKT" HeaderText="NDKT" />
                                        <asp:BoundField DataField="TEN_NDKT" HeaderText="Tên NDKT" />
                                        <asp:BoundField DataField="SOTIEN_NT" HeaderText="Số tiền NT" />
                                        <asp:BoundField DataField="SOTIEN_VND" HeaderText="Số tiền VNĐ" />
                                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi chú" />
                                    </Columns>
                                    <%--<FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="Gainsboro" />--%>
                                    <HeaderStyle CssClass="grid_header"></HeaderStyle>
                                    <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
                                    <RowStyle CssClass="grid_item" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 10px;">
                            <b>Lý do trả</b> <span class="requiredField">(*)</span>
                        </td>
                        <td align="right">
                            <asp:TextBox ID="textBoxLyDoTra" runat="server" class="inputflat" Style="width: 98%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <table>
                                <tr align="left">
                                    <td>
                                        <b>Tổng tiền NT</b>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textBoxTongTien_NT" runat="server" class="inputflat" Style="width: 98%;
                                            text-align: right" ReadOnly="true"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr align="right">
                                    <td style="" align="left">
                                        <b>Tổng tiền VNĐ</b>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textBoxTongTien_VND" runat="server" class="inputflat" Style="width: 98%;
                                            text-align: right" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="right">
                                    <td style="" align="left">
                                        <b>Tổng trích nợ</b>
                                    </td>
                                    <td >
                                        <asp:TextBox ID="txtTongTrichNo" runat="server" Style="width: 98%;
                                            text-align: right; font-weight: bold" class="inputflat" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="right">
                                    <td style="" align="left">
                                        <b>Bằng chữ</b>
                                    </td>
                                    <td>
                                        <asp:Label ID="labelMoneyByText" runat="server" Style="font-weight: bold"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="right" style="display: none">
                                        PT tính phí
                                    </td>
                                    <td colspan="6" align="left" style="display: none">
                                        <asp:RadioButton ID="rdTypeMP" Text="Miễn phí" GroupName="RadioGroup" runat="server"
                                            Enabled="false" />
                                        <asp:RadioButton ID="rdTypePT" Text="Phí trong" Visible="false" GroupName="RadioGroup"
                                            runat="server" Enabled="false" />
                                        <asp:RadioButton ID="rdTypePN" Text="Phí ngoài" GroupName="RadioGroup" runat="server"
                                            Enabled="false" />
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td>
                                        CoreBank : Phí
                                    </td>
                                    <td align="left">
                                        <%--<input type="text" id="txtCharge" style="width: 100px; background-color: Yellow;
                                            text-align: right; font-weight: bold" class="inputflat" value="0" />--%>
                                        <asp:TextBox ID="txtCharge" runat="server" Style="width: 100px; background-color: Yellow;
                                            text-align: right; font-weight: bold" class="inputflat" ReadOnly="true"></asp:TextBox>
                                    </td>
                                    <td align="right">
                                        VAT
                                    </td>
                                    <td align="right">
                                        <%--<input type="text" id="txtVAT" style="width: 100px; background-color: Yellow; font-weight: bold;
                                            text-align: right" class="inputflat" value="0" disabled="disabled" />--%>
                                        <asp:TextBox ID="txtVAT" runat="server" Style="width: 100px; background-color: Yellow;
                                            text-align: right; font-weight: bold" class="inputflat" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' height='5'>
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td width='100%' align="center" valign="top" colspan="2" align="center">
                                        <asp:Button ID="cmdKS" runat="server" Text="Duyệt" class="ButtonCommand" Enabled="false"
                                            value="[(K)S]" />
                                        &nbsp;
                                        <asp:Button ID="cmdInCT" runat="server" Text="In CT" class="ButtonCommand" Enabled="false"
                                            Visible="false" />
                                        &nbsp;
                                       
                                        <asp:Button ID="cmdChuyenTra" runat="server" Text="Chuyển trả" class="ButtonCommand"
                                            value="[(C)huyển]" Enabled="false" />
                                        &nbsp;
                                         <asp:Button ID="cmdHuyCT" runat="server" Text="Duyệt hủy" class="ButtonCommand" value="[(H)ủy]"
                                            OnClientClick="return checkHuyCT();" Enabled="false" />
                                        &nbsp;
                                        <asp:Button ID="cmdGuiLai" runat="server" Text="Gửi lại HQ" class="ButtonCommand"
                                            value="[(G)Gửi lại]" Enabled="false" />
                                        &nbsp;
                                        <input type="hidden" id="cifNo" runat="server" />
                                        <input type="hidden" id="branchCode" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
        $(document).ready(function(){
        });
            function checkHuyCT() {
            var check= confirm("Bạn có chắc chắn muốn hủy chứng từ này không?");
            if(check==false)
            {
                return false;
            }
        }
        
    </script>

</asp:Content>
