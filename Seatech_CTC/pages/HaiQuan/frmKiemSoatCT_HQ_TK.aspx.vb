﻿Imports Business_HQ
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business_HQ.NewChungTu
Imports System.Xml
Imports Business_HQ.CTuCommon
Imports log4net
Imports GIPBankService
Imports System.IO
Imports Business_HQ.BaoCao
Imports Business.Common
'Imports CoreBankSV

Partial Class pages_ChungTu_frmKiemSoatCT_HQ_TK
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_objUser As New CTuUser
    Private illegalChars As String = "[&]"
    Private Shared strMaNhom_GDV As String = "" 'ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Private Shared strTT_TIENMAT As String = "" 'ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    Private Shared pv_cifNo As String = String.Empty
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Private Shared CORE_VERSION As String = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString()
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            CreateDumpChiTietCTGrid() 'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
            ClearCTHeader() 'Xóa thông tin ở grid header
            ShowHideCTRow(False)
            lblStatus.Text = "Hãy chọn một chứng từ cần kiểm soát"

        End If
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'BEGIN-TYNK CHECK NGAY NGHI
        'If Not Business_HQ.CTuCommon.GetNgayNghi() Then
        '    ' clsCommon.ShowMessageBox(Me, "Hôm nay là ngày nghỉ. Bạn không được vào chức năng này!!!")
        '    Response.Redirect("~/pages/frmHomeNV.aspx?CheckNN=1", False)
        '    Exit Sub
        'End If
        'END TYNK CHECK NGAY NGHI
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            'RemoveHandler cmdKS.Click, AddressOf cmdKS_Click
            RemoveHandler cmdChuyenThue.Click, AddressOf cmdChuyenThue_Click
            RemoveHandler cmdHuy.Click, AddressOf cmdHuy_Click
            RemoveHandler cmdChuyenTra.Click, AddressOf cmdChuyenTra_Click
            Exit Sub
        End If
        'Kiểm tra trạng thái session
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))
        Else
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        'Check các quyền của user

        If Not IsPostBack Then
            'Khởi tạo các giá trị khởi đầu cho các điều khiển
            'Load danh sách chứng từ thành lập bởi điểm thu
            LoadDSCT("99")
            LoadDM_Nhanvien()
            'LoadCateLoaiTienThue()
        End If
    End Sub

    Private Sub CreateDumpChiTietCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("MAQUY"))
        v_dt.Columns.Add(New DataColumn("MA_CHUONG"))
        v_dt.Columns.Add(New DataColumn("MA_KHOAN"))
        v_dt.Columns.Add(New DataColumn("MA_TMUC"))
        v_dt.Columns.Add(New DataColumn("NOI_DUNG"))
        v_dt.Columns.Add(New DataColumn("TTIEN"))
        v_dt.Columns.Add(New DataColumn("KY_THUE"))
        v_dt.Columns.Add(New DataColumn("SOTIEN_NT"))
        v_dt.Columns.Add(New DataColumn("SO_TK"))
        v_dt.Columns.Add(New DataColumn("MA_LHXNK"))
        v_dt.Columns.Add(New DataColumn("NGAY_TK"))
        v_dt.Columns.Add(New DataColumn("MA_LT"))
        v_dt.Columns.Add(New DataColumn("MA_HQ"))
        v_dt.Columns.Add(New DataColumn("SAC_THUE"))
        grdChiTiet.Controls.Clear()
        Dim v_dumpRow As DataRow = v_dt.NewRow
        v_dt.Rows.Add(v_dumpRow)
        grdChiTiet.DataSource = v_dt
        grdChiTiet.DataBind()
    End Sub

    Private Sub CreateDumpDSCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("SHKB"))
        v_dt.Columns.Add(New DataColumn("NGAY_KB"))
        v_dt.Columns.Add(New DataColumn("SO_CT"))
        v_dt.Columns.Add(New DataColumn("KYHIEU_CT"))
        v_dt.Columns.Add(New DataColumn("SO_BT"))
        v_dt.Columns.Add(New DataColumn("MA_DTHU"))
        v_dt.Columns.Add(New DataColumn("MA_NV"))
        v_dt.Columns.Add(New DataColumn("TRANG_THAI"))
        v_dt.Columns.Add(New DataColumn("TT_CTHUE"))
        v_dt.Columns.Add(New DataColumn("MA_NNTHUE"))
        v_dt.Columns.Add(New DataColumn("TEN_DN"))
        v_dt.Columns.Add(New DataColumn("response_msg"))
        v_dt.Columns.Add(New DataColumn("so_ct_nh"))
        v_dt.Columns.Add(New DataColumn("Ma_KS"))
        v_dt.Columns.Add(New DataColumn("SAVE_TYPE"))
        grdDSCT.Controls.Clear()
        'Dim v_dumpRow As DataRow = v_dt.NewRow
        'v_dt.Rows.Add(v_dumpRow)
        grdDSCT.DataSource = v_dt
        grdDSCT.DataBind()
    End Sub

    Private Sub ClearCTHeader()
        txtTenTKCo.Text = ""
        txtSHKB.Text = ""
        txtTenKB.Text = ""
        txtNGAY_KH_NH.Text = ""
        txtTKCo.Text = ""
        txtTKNo.Text = ""
        txtMaDBHC.Text = ""
        txtTenDBHC.Text = ""
        txtMaNNT.Text = ""
        txtTenNNT.Text = ""
        txtDChiNNT.Text = ""
        txtMaNNTien.Text = ""
        txtTenNNTien.Text = ""
        txtDChiNNT.Text = ""
        txtMaCQThu.Text = ""
        txtTenCQThu.Text = ""
        ddlLoaiThue.SelectedIndex = 0
        txtDescLoaiThue.Text = ""
        txtLHXNK.Text = ""
        txtDescLHXNK.Text = ""
        txtSoKhung.Text = ""
        txtSoMay.Text = ""
        txtToKhaiSo.Text = ""
        txtMaNT.Text = "VND"
        txtTenNT.Text = "Việt Nam Đồng"
        txtNgayDK.Text = ""
        txtSo_BK.Text = ""
        txtNgay_BK.Text = ""
        ddlHTTT.SelectedIndex = 0
        txtTK_KH_NH.Text = ""
        txtTenTK_KH_NH.Text = ""
        txtNGAY_KH_NH.Text = ""
        txtMA_NH_A.Text = ""
        txtTen_NH_A.Text = ""
        txtMA_NH_B.Text = ""
        txtTen_NH_B.Text = ""
        txtSoCT_NH.Text = ""
    End Sub

    Private Sub LoadDSCT(ByVal pv_strTrangThaiCT As String)
        Try
            Dim dsCT As DataSet = GetDSCT(pv_strTrangThaiCT)
            If (Not dsCT Is Nothing) And (dsCT.Tables(0).Rows.Count > 0) Then
                Me.grdDSCT.DataSource = dsCT
                Me.grdDSCT.DataBind()
            Else
                CreateDumpDSCTGrid()
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub
    Private Sub LoadDM_Nhanvien()
        Dim dt As DataTable = DM_NV()
        ddlMacker.DataSource = dt
        ddlMacker.DataTextField = "ten_dn"
        ddlMacker.DataValueField = "Ma_nv"
        ddlMacker.DataBind()
        ddlMacker.Items.Insert(0, New ListItem("Tất cả", "99"))
        ddlMacker.SelectedIndex = 0
    End Sub
    Private Function DM_NV() As DataTable
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable
        Dim dk As String = ""
        Dim strcheckHS As String = checkHSC()
        Dim str_Ma_Nhom As String = ""
        Dim strSQL As String = ""

        str_Ma_Nhom = LOAD_MN()

        dt = HaiQuan.HaiQuan.GET_DM_NV(strcheckHS, str_Ma_Nhom, Session.Item("MA_CN_USER_LOGON").ToString, Session.Item("User").ToString, strMaNhom_GDV)

        Return dt
    End Function
    Public Function LOAD_MN() As String
        Dim str_return As String = ""

        str_return = HaiQuan.HaiQuan.GET_MA_NHOM(Session.Item("User").ToString)

        Return str_return
    End Function
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    'Lay danh sach chung tu HAI QUAN
    Private Function GetDSCT(ByVal pv_strTrangThaiCT As String) As DataSet
        Try
            If pv_strTrangThaiCT.Equals("99") Then pv_strTrangThaiCT = ""
            If ddlMacker.SelectedValue = "99" Then
                mv_objUser.Ma_NV = ""
            Else
                mv_objUser.Ma_NV = ddlMacker.SelectedValue
            End If

            'mv_objUser.Ban_LV="" cho phep lay chung tu thu ho
            Dim dsCT As DataSet = ChungTu.buChungTu.CTU_GetListKiemSoat(mdlCommon.ConvertDateToNumber(mv_objUser.Ngay_LV), pv_strTrangThaiCT, _
                                  "", "04", mv_objUser.Ma_NV, Session.Item("User"), "")
            Return dsCT
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            Return Nothing
        End Try
    End Function

    Private Sub LoadCTChiTietAll(ByVal key As KeyCTu)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTThue As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty
            Dim v_strSO_CT As String = ""
            'Load chứng từ header
            Dim dsCT As New DataSet
            dsCT = ChungTu.buChungTu.CTU_Header_Set(key)
            If (dsCT.Tables(0).Rows.Count > 0) Then
                ShowHideCTRow(True)
                FillDataToCTHeader(dsCT)
                v_strTT = dsCT.Tables(0).Rows(0)("TRANG_THAI").ToString
                v_strLoaiThue = dsCT.Tables(0).Rows(0)("MA_LTHUE").ToString
                v_strPhuongThucTT = dsCT.Tables(0).Rows(0)("PT_TT").ToString
                v_str_Ma_NNT = dsCT.Tables(0).Rows(0)("Ma_NNThue").ToString
                v_strTTThue = dsCT.Tables(0).Rows(0)("TT_CTHUE").ToString
                v_strSO_CT = dsCT.Tables(0).Rows(0)("so_ct").ToString
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If

            'Load thông tin chi tiết
            Dim dsCTChiTiet As New DataSet
            dsCTChiTiet = ChungTu.buChungTu.CTU_ChiTiet_Set_CT_TK(v_strSO_CT)
            If dsCTChiTiet.Tables(0).Rows.Count > 0 Then
                FillDataToCTDetail(dsCTChiTiet, v_str_Ma_NNT)
                CaculateTongTien(dsCTChiTiet.Tables(0))
            Else
                lblStatus.Text = "Không lấy được thông tin chi tiết của chứng từ.Hãy kiểm tra lại"
                Exit Sub
            End If

            ShowHideButtons(v_strTT, v_strTTThue)
            ShowHideControlByLoaiThue(v_strLoaiThue)
            ShowHideControlByPT_TT(v_strPhuongThucTT, v_strTT)
            'Me.txtMatKhau.Focus()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub

    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet)
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty
            Dim dr As DataRow = dsCT.Tables(0).Rows(0)
            Dim v_strTrangThai As String = dr("TRANG_THAI").ToString()
            Dim v_strHTTT As String = GetString(dr("PT_TT")).ToString
            txtSHKB.Text = dr("SHKB").ToString()
            txtTenKB.Text = dr("TEN_KB").ToString()
            txtNGAY_KH_NH.Text = ConvertNumbertoString(dr("NGAY_KB").ToString())
            txtTKCo.Text = dr("TK_Co").ToString()
            txtTKNo.Text = dr("TK_No").ToString()
            txtMaDBHC.Text = dr("MA_XA").ToString()

            'Event: Them ma hai quan
            txtMaHQ.Text = dr("MA_HQ").ToString()
            txtDescMaHQ.Text = dr("TEN_HQ").ToString()
            txtMaHQPH.Text = dr("MA_HQ_PH").ToString()
            txtTenMaHQPH.Text = dr("TEN_HQ_PH").ToString()

            txtTimeLap.Text = GetString(dr("ngay_ht"))

            txtTenDBHC.Text = getDiaChi(dr("MA_XA").ToString())
            txtMaNNT.Text = dr("MA_NNTHUE").ToString()
            txtTenNNT.Text = dr("TEN_NNTHUE").ToString()
            txtDChiNNT.Text = dr("DC_NNTHUE").ToString()
            txtMaNNTien.Text = dr("Ma_NNTien").ToString()
            txtTenNNTien.Text = dr("TEN_NNTIEN").ToString()
            txtDChiNNTien.Text = dr("DC_NNTIEN").ToString()
            txtMaCQThu.Text = dr("MA_CQTHU").ToString()
            txtTenCQThu.Text = dr("TEN_CQTHU").ToString()
            If dr("ma_ntk").ToString = "1" Then
                txtTenTKCo.Text = "Tài khoản thu NSNN"
            ElseIf dr("ma_ntk").ToString = "2" Then
                txtTenTKCo.Text = "Tài khoản tạm thu"
            End If
            Dim dt As New DataTable
            dt.Columns.Add("ID_HQ", Type.GetType("System.String"))
            dt.Columns.Add("Name", Type.GetType("System.String"))
            dt.Columns.Add("Value", Type.GetType("System.String"))
            dt.Columns(0).AutoIncrement = True
            Dim R As DataRow = dt.NewRow
            R("ID_HQ") = dr("MA_LTHUE").ToString()
            R("Name") = GetTenLoaiThue(dr("MA_LTHUE").ToString()) ' dr("MA_LTHUE").ToString() & "-" & GetTenLoaiThue(dr("MA_LTHUE").ToString())
            R("Value") = dr("MA_LTHUE").ToString()
            dt.Rows.Add(R)
            ddlLoaiThue.DataSource = dt
            ddlLoaiThue.DataTextField = "Name"
            ddlLoaiThue.DataValueField = "Value"
            ddlLoaiThue.DataBind()

            strMaLH = GetString(dr("LH_XNK").ToString())
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)
            txtLHXNK.Text = strMaLH
            txtDescLHXNK.Text = strTenVT & "-" & strTenLH
            txtMaNT.Text = dr("Ma_NT").ToString()
            txtTenNT.Text = dr("Ten_NT").ToString()
            txtSoKhung.Text = GetString(dr("SO_KHUNG").ToString())
            txtSoMay.Text = GetString(dr("SO_MAY").ToString())
            txtToKhaiSo.Text = GetString(dr("SO_TK").ToString())
            txtNgayDK.Text = GetString(dr("NGAY_TK"))
            txtSo_BK.Text = GetString(dr("So_BK"))
            txtNgay_BK.Text = GetString(dr("Ngay_BK"))
            txtDienGiai.Text = GetString(dr("REMARKS"))
            If v_strHTTT.Trim.Length > 0 Then
                ddlHTTT.SelectedValue = v_strHTTT
                ddlHTTT.SelectedItem.Text = Get_TenPTTT(v_strHTTT)
            End If

            If v_strHTTT.Equals("03") Then
                txtTKGL.Text = GetString(dr("TK_KH_NH"))
                txtTenTKGL.Text = dr("TEN_KH_NH").ToString()
                'test
                'txtSoDu_KH_NH.Text = VBOracleLib.Globals.Format_Number(GetString(dr("tt_tthu")).ToString(), ",")
            End If

            rowTKGL.Visible = False
            txtTyGia.Text = VBOracleLib.Globals.Format_Number(dr("Ty_Gia").ToString(), ",")
            txtTTien_NT.Text = VBOracleLib.Globals.Format_Number(GetString(dr("ttien_nt")).ToString(), ",")
            rowLHXNK.Visible = False
            txtTK_KH_NH.Text = GetString(dr("TK_KH_NH"))
            txtTenTK_KH_NH.Text = dr("TEN_KH_NH").ToString
            'txtSoDu_KH_NH.Text = dr

            'Get money custom if documents not accept
            If v_strHTTT.Equals("01") Then
                txtSoDu_KH_NH.Text = VBOracleLib.Globals.Format_Number(GetTK_KH_NH(txtTK_KH_NH.Text), ",")
            End If

            txtNGAY_KH_NH.Text = GetString(dr("NGAY_KH_NH"))
            txtMA_NH_A.Text = GetString(dr("MA_NH_A"))
            txtTen_NH_A.Text = GetString(dr("TEN_NH_A"))
            txtMA_NH_B.Text = GetString(dr("MA_NH_B"))
            txtTen_NH_B.Text = GetString(dr("TEN_NH_B"))

            txtMA_NH_TT.Text = GetString(dr("MA_NH_TT"))
            txtTEN_NH_TT.Text = GetString(dr("TEN_NH_TT"))
            Dim strPTTP As String = ""
            strPTTP = dr("pt_tinhphi").ToString()

            Dim strTT_CITAD As String = ""
            strTT_CITAD = dr("TT_CITAD").ToString()
            If strTT_CITAD = "1" Then
                RadioButton1.Checked = True
            Else
                RadioButton2.Checked = True
            End If
            txtTenTKCo.Text = ""
            txtCharge.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD").ToString(), ",")
            txtVAT.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT").ToString(), ",")
            txtCharge2.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD2").ToString(), ",")
            txtVat2.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT2").ToString(), ",")
            txtRM_REF_NO.Text = dr("RM_REF_NO").ToString()
            txtREF_NO.Text = dr("REF_NO").ToString()
            txtSoCT_NH.Text = dr("SO_CT_NH").ToString()
            txtQuan_HuyenNNTien.Text = dr("QUAN_HUYENNNTIEN").ToString()
            txtTinh_TphoNNTien.Text = dr("TINH_TPNNTIEN").ToString()
            If dr("LY_DO_HUY").ToString().Trim <> "" Then
                txtLyDoHuy.Text = dr("LY_DO_HUY").ToString()
            Else
                txtLyDoHuy.Text = dr("LY_DO").ToString()
            End If
            hdfTrangThai_CT.Value = dr("trang_thai").ToString().Trim
            hdfTrangThai_CThue.Value = dr("tt_cthue").ToString().Trim
            hdfDienGiai.Value = dr("ghi_chu").ToString().Trim
            hdfMACN.Value = dr("ma_cn").ToString().Trim
            'ddlMaLoaiTienThue.SelectedValue = dr("LOAI_TT").ToString()
            txtGhiChu.Text = dr("ghi_chu").ToString().Trim
            txtDienGiaiHQ.Text = dr("dien_giai_hq").ToString().Trim
            Dim v_dt As DataTable = Nothing

            If dr("MA_HUYEN") IsNot Nothing Then
                If dr("MA_HUYEN").ToString().Length > 0 Then
                    txtQuanNNThue.Text = dr("MA_HUYEN").ToString()
                    txtTenQuanNNThue.Text = getDiaChi(dr("MA_HUYEN").ToString())
                End If
            End If

            If dr("MA_TINH") IsNot Nothing Then
                If dr("MA_TINH").ToString().Length > 0 Then
                    txtTinhNNThue.Text = dr("MA_TINH").ToString()
                    txtTenTinhNNThue.Text = getDiaChi(dr("MA_TINH").ToString())
                End If
            End If

            If dr("MA_QUAN_HUYENNNTIEN") IsNot Nothing Then
                If dr("MA_QUAN_HUYENNNTIEN").ToString().Length > 0 Then
                    txtQuanNNTien.Text = dr("MA_QUAN_HUYENNNTIEN").ToString()
                    txtTenQuanNNTien.Text = getDiaChi(dr("MA_QUAN_HUYENNNTIEN").ToString())
                End If
            End If

            If dr("MA_TINH_TPNNTIEN") IsNot Nothing Then
                If dr("MA_TINH_TPNNTIEN").ToString().Length > 0 Then
                    txtTinhNNTien.Text = dr("MA_TINH_TPNNTIEN").ToString()
                    txtTenTinhNNTien.Text = getDiaChi(dr("MA_TINH_TPNNTIEN").ToString())
                End If
            End If

            'txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dr("tt_tthu").ToString().Trim, ",")
            'If dr("SEQ_NO").ToString() <> "" Then
            '    lbSoFT.Text = "Số SEQ_NO:" & dr("SEQ_NO").ToString()
            '    If dr("so_xcard").ToString() <> "" Then
            '        lbSoFT.Text &= " Số XCARD :" & dr("so_xcard").ToString()
            '    End If
            'Else
            '    lbSoFT.Text = ""
            'End If

            lblStatus.Text = dr("MSG_LOI").ToString()
            txtCK_SoCMND.Text = dr("SO_CMND").ToString()
            txtCK_SoDT.Text = dr("SO_FONE").ToString()
            txtMaCuc.Text = dr("ma_cuc").ToString()
            TxtTenCuc.Text = dr("ten_cuc").ToString()
        Catch ex As Exception
            LogApp.WriteLog("FillDataToCTHeader", ex, "FillDataToCTHeader", HttpContext.Current.Session("TEN_DN"))

            Throw ex
        Finally
        End Try
    End Sub

    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet, ByVal v_strMa_NNT As String)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            Dim dr As DataRow
            Dim sodathu As String
            grdChiTiet.DataSource = v_dt
            grdChiTiet.DataBind()
            'For i As Integer = 0 To v_dt.Rows.Count - 1
            '    dr = v_dt.Rows(i)
            '    'sodathu = clsCTU.TCS_CalSoDaThu(v_strMa_NNT, dr("MaQuy").ToString(), dr("Ma_Chuong").ToString(), dr("Ma_Khoan").ToString(), dr("Ma_TMuc").ToString(), dr("Ky_Thue").ToString())
            '    sodathu = VBOracleLib.Globals.Format_Number(sodathu.Replace(".", ""), ",")
            '    grdChiTiet.Items(i).Cells(14).Text = sodathu
            '    grdChiTiet.Items(i).Cells(14).Enabled = False
            '    'CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text = VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ",")
            '    'CType(grdChiTiet.Items(i).Controls(7).Controls(1), TextBox).Text = VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(7).Controls(1), TextBox).Text, ",")
            'Next
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function ValidNHSHB_SP(ByVal strSHKB As String, ByVal strMa_NH_B As String, ByVal strLanKS As String, ByVal strSo_ct As String) As String
        Dim strResult As String = ""
        Dim dt As New DataTable
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong

        strResult = cls_songphuong.ValiadSHB(strSHKB)

        Dim strNHB As String = ""
        If strResult.Length > 8 Then
            Dim strarr() As String
            strarr = strResult.Split(";")
            strNHB = strarr(0).ToString
        End If

        Dim strClick As String = "0"
        strClick = ChungTu.buChungTu.CTU_Get_Valid_LanKS(strSo_ct)

        If strClick.Length > 0 Then
          
        Else
            strClick = "0"
        End If

        If strNHB.Length >= 8 Then
            If strNHB.Equals(strMa_NH_B) Then
                Return "TRUE;" & strClick
            Else
                Return "FALSE;" & strClick
            End If
        Else
            Return "TRUE;" & strClick
        End If

        'Return "FALSE"
    End Function
    Private Sub CaculateTongTien(ByVal pv_dt As DataTable)
        'Tính toán và cập nhật lại trường tổng tiền
        Dim dblTongTien As Double = 0
        Dim dblTongTrichNo As Double = 0
        For i As Integer = 0 To pv_dt.Rows.Count - 1
            dblTongTien += pv_dt.Rows(i)("TTIEN")
        Next
        Dim dblCharge As Double = CDbl(IIf((txtCharge.Text.Replace(",", "").Length > 0), txtCharge.Text.Replace(",", ""), "0"))
        Dim dblVAT As Double = CDbl(IIf((txtVAT.Text.Replace(",", "").Length > 0), txtVAT.Text.Replace(",", ""), "0"))
        txtTTIEN.Text = VBOracleLib.Globals.Format_Number(dblTongTien, ",")

        dblTongTrichNo = dblTongTien + dblCharge + dblVAT
        txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dblTongTrichNo, ",")
        lblTTien_Chu.Text = ConcertCurToText.ConvertCurr.So_chu(dblTongTrichNo)
    End Sub

    Private Function GetTenLoaiThue(ByVal pv_strMaLoaiThue As String) As String
        Select Case pv_strMaLoaiThue
            Case "01"
                Return "Thuế nội địa"
            Case "02"
                Return "Thuế thu nhập cá nhân"
            Case "03"
                Return "Thuế chước bạ"
            Case "04"
                Return "Thuế Hải Quan"
            Case "05"
                Return "Thu khác"
            Case "06"
                Return "Thu tài chính"
            Case Else '05'
                Return "Phạt vi phạm hành chính"
        End Select
    End Function

    Private Sub GetThongTinTKKH(ByVal v_strHTTT As String, ByVal v_strTrangThaiCT As String, _
                    ByVal pv_strSoTKKH As String, ByRef pv_strTenKH As String, ByRef pv_strTTKH As String)
        If (pv_strSoTKKH.Length > 0) Then 'Lay theo sotk
            If v_strHTTT.Equals("01") Then 'Chuyen Khoan
                If v_strTrangThaiCT.Equals("05") Then 'Cho KS lay thong tin tu corebank

                Else 'Cac truong hop khac thi lay thong tin tu db

                End If
            End If
        End If
    End Sub

    Private Sub ShowHideButtons(ByVal strTT As String, ByVal strTTThue As String)
        If strTT <> "01" Then
            lbSoFT.Text = ""
        End If
        Select Case strTT
            Case "00"
                lblStatus.Text = "Chứng từ chưa kiểm soát"
                cmdHuy.Enabled = False
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenBDS.Enabled = False
            Case "01"
                'lblStatus.Text = "Chứng từ đã kiểm soát"
                cmdHuy.Enabled = True
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenBDS.Enabled = False
            Case "02"
                lblStatus.Text = "Chứng từ đã hủy bởi GDV - đang chờ duyệt"
                cmdHuy.Enabled = True
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = True
                cmdChuyenBDS.Enabled = False
            Case "03"
                lblStatus.Text = "Chứng từ bị chuyển trả"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
            Case "04"
                lblStatus.Text = "Chứng từ đã bị hủy"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
            Case "05"
                lblStatus.Text = "Chứng từ chờ kiểm soát"
                'cmdKS.Enabled = True
                cmdChuyenTra.Enabled = True
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
            Case "06"
                lblStatus.Text = "Chứng từ hạch toán lỗi ở BDS"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = True
            Case "19"
                lblStatus.Text = "Điều chỉnh chờ kiểm soát"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
        End Select

        cmdChuyenThue.Enabled = False

        If strTT = "19" Then
            cmdChuyenThue.Enabled = True
        End If

        If strTTThue = "0" Then
            If strTT = "01" Then
                'lblStatus.Text = "Chứng từ đã duyệt nhưng chưa chuyển sang hải quan"
                cmdChuyenThue.Enabled = True
                cmdHuy.Enabled = False
            End If
        End If
        If strTTThue = "1" Then
            If strTT = "04" Then
                'lblStatus.Text = "Chứng từ đã hủy nhưng chưa chuyển sang hải quan"
                cmdChuyenThue.Enabled = True
            End If
        End If

    End Sub

    Private Sub ShowHideCTRow(ByVal pv_blnDisplayRow As Boolean)
        rowDChiNNT.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = False
        rowQuan_HuyenNNTien.Visible = False
        rowTinhNNTien.Visible = False
        rowDBHC.Visible = pv_blnDisplayRow
        rowCQThu.Visible = pv_blnDisplayRow
        rowTKNo.Visible = False
        rowTKCo.Visible = pv_blnDisplayRow
        rowHTTT.Visible = pv_blnDisplayRow
        rowTK_KH_NH.Visible = pv_blnDisplayRow
        rowSoDuNH.Visible = pv_blnDisplayRow
        rowNgay_KH_NH.Visible = pv_blnDisplayRow
        rowMaNT.Visible = pv_blnDisplayRow
        rowTyGia.Visible = pv_blnDisplayRow
        rowLoaiThue.Visible = pv_blnDisplayRow
        rowToKhaiSo.Visible = pv_blnDisplayRow
        rowNgayDK.Visible = pv_blnDisplayRow
        rowLHXNK.Visible = pv_blnDisplayRow
        'rowSoKhung.Visible = pv_blnDisplayRow
        'rowSoMay.Visible = pv_blnDisplayRow
        rowSoBK.Visible = pv_blnDisplayRow
        rowNgayBK.Visible = pv_blnDisplayRow
        rowNganHangChuyen.Visible = pv_blnDisplayRow
        rowNganHangNhan.Visible = pv_blnDisplayRow
        rowTKGL.Visible = pv_blnDisplayRow
        'rowLoaiTienThue.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowSoCMND.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = pv_blnDisplayRow
        rowSoDT.Visible = pv_blnDisplayRow
        rowTinhNNThue.Visible = pv_blnDisplayRow
        rowTPNNTien.Visible = pv_blnDisplayRow
        rowQuanNNThue.Visible = pv_blnDisplayRow
        rowQuanNNTien.Visible = pv_blnDisplayRow
        rowMaCuc.Visible = pv_blnDisplayRow
        rowMaHQPH.Visible = pv_blnDisplayRow
        rowNhanHangTT.Visible = pv_blnDisplayRow
        rowSHKB.Visible = pv_blnDisplayRow
        rowMaHQ.Visible = pv_blnDisplayRow
        rowSoKhung.Visible = pv_blnDisplayRow
        rowSoMay.Visible = pv_blnDisplayRow
    End Sub

    Private Sub ShowHideControlByLoaiThue(ByVal pv_strLoaiThue As String)
        Select Case pv_strLoaiThue
            Case "01" 'NguonNNT.NNThue_CTN, NguonNNT.NNThue_VL, NguonNNT.SoThue_CTN, NguonNNT.VangLai
                'An thong tin To khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "02"

            Case "03" 'NguonNNT.TruocBa
                'Show thong tin Truoc ba
                'rowSoKhung.Visible = True
                'rowSoMay.Visible = True

                'an thong tin to khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
            Case "04" 'NguonNNT.HaiQuan
                'Show thong tin to khai
                rowLHXNK.Visible = True
                rowToKhaiSo.Visible = True
                rowNgayDK.Visible = True

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case Else '"05" Khong xac dinh
        End Select
    End Sub

    Private Sub ShowHideControlByPT_TT(ByVal pv_strPT_TT As String, ByVal pv_strTT As String)
        If pv_strPT_TT = "01" Or pv_strPT_TT = "05" Then 'Chuyển khoản
            rowTK_KH_NH.Visible = True
            rowNganHangNhan.Visible = True
            rowNganHangChuyen.Visible = True
            rowTKGL.Visible = False
            rowNNTien.Visible = True
            rowSoCMND.Visible = False
            rowDChiNNTien.Visible = True
            rowSoDT.Visible = False
            If pv_strTT.Equals("05") Or pv_strTT.Equals("19") Then
                rowSoDuNH.Visible = True
                Dim v_strSoTKKH As String = txtTK_KH_NH.Text
                'sua cho kienlongbank
                'If v_strSoTKKH.Length > 0 Then
                '    Dim v_objCoreBank As New clsCoreBankUtilMSB
                '    Dim v_strDescTKKH As String = String.Empty
                '    Dim v_strSoDuNH As String = String.Empty
                '    Dim v_strErrorCode As String = String.Empty
                '    Dim v_strErrorMsg As String = String.Empty
                '    If (v_objCoreBank.GetCustomerInfo(v_strSoTKKH, v_strDescTKKH, v_strSoDuNH, v_strErrorCode, v_strErrorMsg).Equals("0")) Then
                '        txtTenTK_KH_NH.Text = v_strDescTKKH
                '        txtSoDu_KH_NH.Text = VBOracleLib.Globals.Format_Number(v_strSoDuNH, ",")
                '    Else
                '        lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                '        Exit Sub
                '    End If
                'End If
            Else
                rowSoDuNH.Visible = False
            End If
        ElseIf pv_strPT_TT = "00" Then  'Tiền mặt
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = False
            rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = True
        ElseIf pv_strPT_TT = "03" Then  'Chuyển trung gian
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = True
            rowNganHangChuyen.Visible = True
            rowTKGL.Visible = True
            rowSoDuNH.Visible = False
        Else 'Báo có
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = False
            rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = True
        End If
    End Sub

    Protected Sub cmdChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenTra.Click
        Try
            'If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
            '    lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới."
            '    Exit Sub
            'End If

            'If Check_Mode() <> gblnTTBDS Then
            '    lblStatus.Text = "Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại"
            '    Exit Sub
            'End If
            If txtLyDoHuy.Text.ToString().Trim() = "" Then
                lblStatus.Text = "Bạn phải nhập vào lý do chuyển trả."
                Exit Sub
            End If
            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT

                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    Exit Sub
                End If
                If strTT_Data = "02" Then
                    ChungTu.buChungTu.CTU_SetTrangThai(key, "01", "", "", "", "", "", "", "", txtLyDoHuy.Text.ToString(), True)
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    EnabledButtonWhenDoTransaction(False)
                    cmdHuy.Enabled = False
                    lblStatus.Text = "Chuyển trả chứng từ thành công."
                    Exit Sub
                Else
                    ChungTu.buChungTu.CTU_SetTrangThai(key, "03", "", "", "", "", "", "", "", txtLyDoHuy.Text.ToString(), True)
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    EnabledButtonWhenDoTransaction(False)
                    Dim strTK_KH_NH As String = txtTK_KH_NH.Text.Trim
                    Dim strPTTinhPhi As String = ""
                    Dim strTTien As String = txtTTIEN.Text.Trim.Replace(",", "")

                    Dim strFeeAmount As String = txtCharge2.Text.Trim.Replace(",", "")
                    Dim strVATAmount As String = txtVat2.Text.Trim.Replace(",", "")

                End If

                lblStatus.Text = "Chuyển trả chứng từ thành công."
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If
        Catch ex As Exception
            LogApp.WriteLog("cmdChuyenTra", ex, "Lỗi chuyển trả chứng từ", HttpContext.Current.Session("TEN_DN"))
        End Try
    End Sub

    'Protected Sub cmdKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKS.Click
    '    LogApp.AddDebug("cmdKS_Click", "KIEM SOAT CHUNG TU HQ: Kiem soat chung tu")
    '    Dim v_blnReturn As Boolean = False
    '    Dim CITAD_VERSION As String = ""
    '    Dim strNguyenTe As String = ""
    '    Dim key As New KeyCTu
    '    Try
    '        If clsCTU.checkGioGD() = "0" Then
    '            lblStatus.Text = "Đã hết giờ giao dịch trong ngày. Không được tiếp tục kiểm soát!"
    '            Exit Sub
    '        End If
    '        'Disable button tranh lam viec 2 lan
    '        EnabledButtonWhenDoTransaction(False)

    '        'Kiểm tra ngày làm việc
    '        'If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
    '        '    lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
    '        '    EnabledButtonWhenDoTransaction(True)
    '        '    Exit Sub
    '        'End If


    '        'Kiểm tra mật khẩu kiểm soát
    '        Dim strUser = mv_objUser.Ten_DN.ToString
    '        'If Not (txtMatKhau.Text.Length = 0) Then
    '        '    If Not Business_HQ.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
    '        '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
    '        '        EnabledButtonWhenDoTransaction(True)
    '        '        Exit Sub
    '        '    End If
    '        'Else
    '        '    lblStatus.Text = "Phải nhập mật khẩu khi kiểm soát."
    '        '    EnabledButtonWhenDoTransaction(True)
    '        '    Exit Sub
    '        'End If
    '        Dim tien_tongTN As Decimal = Convert.ToDecimal(txtTongTrichNo.Text.ToString().Replace(".", ""))
    '        If Not Business.Common.mdlCommon.CheckHanMuc_KSV(Session.Item("User").ToString(), tien_tongTN) Then
    '            lblStatus.Text = "Hạn mức của bạn không đủ để duyệt giao dịch này."
    '            Exit Sub
    '        End If

    '        ''''Dim strTT_CITAD = ConfigurationManager.AppSettings.Get("CITAD").ToString

    '        'Dat lai gia tri
    '        Dim v_strSHKB As String = hdfSHKB.Value
    '        Dim v_strNgay_KB As String = hdfNgay_KB.Value
    '        Dim v_strSo_CT As String = hdfSo_CT.Value
    '        Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
    '        Dim v_strSo_BT As String = hdfSo_BT.Value
    '        Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
    '        Dim v_strMa_NV As String = hdfMa_NV.Value
    '        Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
    '        Dim v_strMaNNT As String = hdfMaNNT.Value
    '        Dim v_objGDV As CTuUser
    '        v_objGDV = New CTuUser(v_strMa_NV)
    '        Dim strErrorNum As String = ""
    '        Dim strErrorMes As String = ""
    '        Dim pv_so_tn_ct As String = ""
    '        Dim pv_ngay_tn_ct As String = ""
    '        Dim MSGType As String = ""
    '        Dim hdr As New infDoiChieuNHHQ_HDR
    '        Dim dtls() As infDoiChieuNHHQ_DTL
    '        hdr.ten_kb = Globals.RemoveSign4VietnameseString(txtTenKB.Text)
    '        If ((Not v_strSHKB Is Nothing) _
    '             And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
    '            'Dim key As New KeyCTu
    '            key.Kyhieu_CT = v_strKyHieu_CT
    '            key.So_CT = v_strSo_CT
    '            key.SHKB = v_strSHKB
    '            key.Ngay_KB = v_strNgay_KB
    '            key.Ma_NV = CInt(v_strMa_NV)
    '            key.So_BT = v_strSo_BT
    '            key.Ma_Dthu = v_strMa_Dthu



    '            'Lấy trực tiếp phương thức thanh toán trong DB
    '            Dim strPT_TT As String = ChungTu.buChungTu.CTU_Get_PTTT(key)
    '            Dim v_strHost_REFNo As String = String.Empty
    '            Dim v_strRM_REF_NO As String = String.Empty
    '            Dim v_strBDS_REFNo As String = String.Empty
    '            Dim v_strExcutionDate As String = String.Empty
    '            Dim strTongTienTT As String = txtTTIEN.Text.Replace(",", "")
    '            Dim strSoduTaiKhoanKH As String = txtSoDu_KH_NH.Text.Replace(",", "")

    '            Dim v_strAmt As String = strTongTienTT 'txtTongTrichNo.Text.Replace(",", "") 'strTongTienTT
    '            Dim teller_id As String = clsCTU.Get_TellerID(v_strMa_NV) 'giao dich vien
    '            Dim approver_id As String = mv_objUser.TELLER_ID 'Kiem soat vien
    '            Dim v_total_ID As String

    '            Dim ma_ChiNhanh As String
    '            ma_ChiNhanh = clsCTU.TCS_GetMaCN_NH(mv_objUser.MA_CN)

    '            Dim v_strCert_code As String = String.Empty
    '            Dim v_strCert_type As String = String.Empty
    '            Dim v_strCif_no As String = String.Empty
    '            Dim v_strIssue_date As String = String.Empty
    '            Dim v_strIssue_place As String = String.Empty
    '            Dim v_strRollout_branch_no As String = String.Empty
    '            Dim v_strRollout_acct_no As String = String.Empty
    '            Dim v_strRollout_acct_name As String = String.Empty
    '            Dim v_strBnfc_bank_id As String = String.Empty
    '            Dim v_strBnfc_bank_name As String = String.Empty
    '            Dim v_strBnfc_tax_name As String = String.Empty

    '            Dim v_strErrorCode As String = String.Empty
    '            Dim v_strErrorMessage As String = String.Empty
    '            Dim v_strRemark As String = String.Empty
    '            Dim v_strTKCo As String = String.Empty
    '            v_strTKCo = txtTKCo.Text.Replace("-", "")
    '            'Kienvt
    '            v_strRollout_acct_name = Globals.RemoveSign4VietnameseString(txtTenNNT.Text)

    '            'Ghi nhan giao dich vao host
    '            v_strRollout_acct_no = txtTK_KH_NH.Text
    '            v_strRM_REF_NO = txtRM_REF_NO.Text

    '            If txtRM_REF_NO.Text.Length = 0 Then
    '                lblStatus.Text = "Không sinh được số tham chiếu(RM) giao dịch"
    '                EnabledButtonWhenDoTransaction(True)
    '                Exit Sub
    '            End If

    '            Dim v_strREF_NO As String = txtREF_NO.Text
    '            If txtREF_NO.Text.Length = 0 Then
    '                lblStatus.Text = "Không sinh được số tham chiếu(SEQ) giao dịch"
    '                EnabledButtonWhenDoTransaction(True)
    '                Exit Sub
    '            End If

    '            Dim v_strMaNHNhan As String = txtMA_NH_B.Text
    '            Dim v_strMaNHTT As String = txtMA_NH_TT.Text '01/10/2012-manhnv
    '            Dim v_strMaCqthu As String = txtTKCo.Text 'txtMaCQThu.Text
    '            Dim v_strTenCqthu As String = Globals.RemoveSign4VietnameseString(txtTenCQThu.Text)
    '            Dim v_strMaNHGui As String = txtMA_NH_A.Text
    '            Dim v_strTen_kb As String = Globals.RemoveSign4VietnameseString(txtTenKB.Text)
    '            If v_strRollout_acct_name.Length > 40 Then
    '                lblStatus.Text = "Tên NNT dài hơn 40 ký tự. Hãy kiểm tra lại"
    '                EnabledButtonWhenDoTransaction(True)
    '                Exit Sub
    '            End If
    '            ''''If strTT_CITAD <> "2.0" Then
    '            Dim v_arrOfRemarks As String = BuildRemarkForPayment(v_strRollout_acct_name, _
    '                    txtMaNNT.Text, txtTenKB.Text, mv_objUser.Ngay_LV, v_strMaNHTT)
    '            'ManhNV sua 210 -> 159
    '            If v_arrOfRemarks.Length > 255 Then
    '                lblStatus.Text = "Số lượng ký tự nhập vượt quá 255 ký tự.Hãy kiểm tra lại."
    '                EnabledButtonWhenDoTransaction(True)
    '                Exit Sub
    '            Else
    '                v_strRemark = v_arrOfRemarks
    '            End If
    '            'v_strRemark = v_arrOfRemarks
    '            Dim v_strRemark2 As String = BuildRemark2ForPayment(txtMaNNT.Text)
    '            If v_strRemark2.Length > 150 Then
    '                lblStatus.Text = "Số lượng ký ghi chú quá 150 ký tự.Hãy kiểm tra lại."
    '                EnabledButtonWhenDoTransaction(True)
    '                Exit Sub
    '            End If

    '            Dim vstrNgay_gui As String = txtNGAY_KH_NH.Text
    '            Dim str_MaGD As String = "TP8278"
    '            Dim v_fee As String = txtCharge.Text.Replace(",", "")
    '            Dim v_vat As String = txtVAT.Text.Replace(",", "")
    '            Dim v_strTranDate As String = clsCTU.TCS_GetNgayBDS().ToString
    '            Dim v_TTCUTOFFTIME As String = clsCTU.TCS_TTCUTOFFTIME().ToString
    '            Dim v_strPTHT As String = String.Empty
    '            Dim v_TKHachToan As String = String.Empty
    '            If v_TTCUTOFFTIME = "1" Then
    '                v_strPTHT = "TPAYB4CUTOFF"
    '                v_TKHachToan = "120101001"
    '            Else
    '                v_strPTHT = "TPAYAFCUTOFF"
    '                v_TKHachToan = "280701018"
    '            End If
    '            ' v_strTranDate = ConvertNumberToDate(v_strTranDate.ToString().ToString("dd/MM/yyyy")
    '            'ConvertNumberToDate(v_strNgay_KB).ToString("dd/MM/yyyy")
    '            'mv_objUser.MA_CN =>lay theo ma chi nhanh cua user tao dien
    '            If strPT_TT = "01" Then 'Chuyển khoản
    '                str_MaGD = "TP8277"
    '                If txtSoDu_KH_NH.Text.Length = 0 Then
    '                    lblStatus.Text = "Tài khoản khách hàng không đủ để thanh toán"
    '                    EnabledButtonWhenDoTransaction(True)
    '                    Exit Sub
    '                End If
    '                If (txtTenTK_KH_NH.Text.Length > 0) Then
    '                    v_strRollout_branch_no = txtTenTK_KH_NH.Text.Split("-")(0)
    '                    v_strCif_no = txtTenTK_KH_NH.Text.Split("-")(1)
    '                    'v_strRollout_acct_name = txtTenTK_KH_NH.Text.Split("-")(2)
    '                    v_strCert_code = txtTenTK_KH_NH.Text.Split("-")(3)
    '                End If
    '                'Kiểm tra tổng tiền trong chứng từ và số dư hiện tại
    '                If Double.Parse(strTongTienTT) > Double.Parse(strSoduTaiKhoanKH) Then
    '                    lblStatus.Text = "Tài khoản không đủ để thanh toán.Hãy kiểm tra lại"
    '                    EnabledButtonWhenDoTransaction(True)
    '                    Exit Sub
    '                End If

    '                'sua cho kienlongbank
    '                '    Dim v_objCoreBank As New clsCoreBankUtilMSB
    '                '    'Ghi nhận giao dịch vào host
    '                '    If (v_objCoreBank.Payment(v_strCif_no, v_strCert_type, v_strCert_code, _
    '                '         v_strIssue_date, v_strIssue_place, ma_ChiNhanh, v_strRollout_acct_no, _
    '                '         v_strRollout_acct_name, v_strTen_kb, v_strMaCqthu, v_strTenCqthu, _
    '                '         v_strMaNHGui, v_strMaNHNhan, vstrNgay_gui, v_strAmt, v_fee, v_vat, v_strRemark, _
    '                '         v_strRemark2, v_strErrorCode, v_strErrorMessage, v_strExcutionDate, _
    '                '         v_strHost_REFNo, teller_id, approver_id, v_strRM_REF_NO, v_strREF_NO, v_strTranDate, v_strPTHT, str_MaGD, v_TKHachToan, v_strMaNHTT)) Then
    '                '        'If (True) Then
    '                '        'Cập nhật lại trạng thái của chứng từ
    '                '        v_strTrangThai_CT = "06" 'Trạng thái chưa cập nhật vào BDS
    '                '        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_TKHachToan, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
    '                '        lblStatus.Text = "Hệ thống đã ghi nhận giao dịch vào host.Xin chờ một lúc để ghi nhận vào BDS"
    '                '    Else
    '                '        lblStatus.Text = "Đã có lỗi xảy ra khi hạch toán chứng từ.Mã lỗi: " & v_strErrorCode & ",mô tả:" & v_strErrorMessage
    '                '        EnabledButtonWhenDoTransaction(True)
    '                '        Exit Sub
    '                '    End If

    '                '    'Cập nhật lại trạng thái của chứng từ
    '                '    'v_strTrangThai_CT = "06"
    '                '    'ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
    '                '    'lblStatus.Text = "Hệ thống đã ghi nhận giao dịch vào host.Xin chờ một lúc để ghi nhận vào BDS"


    '                '    'Ghi nhận giao dịch vào bds


    '                '    'ElseIf strPT_TT = "00" Then 'Tiền mặt
    '                'ElseIf strPT_TT = "03" Then 'Trung gian
    '                '    Dim v_objCoreBank As New clsCoreBankUtilMSB
    '                '    'Dim isHighValue As Boolean = clsCommon.checkHighValue(v_strAmt)
    '                '    If (v_objCoreBank.PaymentGL(v_strCif_no, v_strCert_type, v_strCert_code, _
    '                '         v_strIssue_date, v_strIssue_place, ma_ChiNhanh, v_strRollout_acct_no, _
    '                '         v_strRollout_acct_name, v_strTen_kb, v_strMaCqthu, v_strTenCqthu, _
    '                '         v_strMaNHGui, v_strMaNHNhan, vstrNgay_gui, v_strAmt, v_fee, v_vat, v_strRemark, _
    '                '         v_strRemark2, v_strErrorCode, v_strErrorMessage, v_strExcutionDate, _
    '                '         v_strHost_REFNo, teller_id, approver_id, v_strRM_REF_NO, v_strREF_NO, v_strTranDate, v_strPTHT, str_MaGD, v_TKHachToan, v_strMaNHTT)) Then

    '                '        'Cập nhật lại trạng thái của chứng từ
    '                '        v_strTrangThai_CT = "06" 'Trạng thái chưa cập nhật vào BDS
    '                '        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_TKHachToan, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
    '                '        lblStatus.Text = "Hệ thống đã ghi nhận giao dịch vào host.Xin chờ một lúc để ghi nhận vào BDS"
    '                '    Else
    '                '        lblStatus.Text = "Đã có lỗi xảy ra khi hạch toán chứng từ.Mã lỗi: " & v_strErrorCode & ",mô tả:" & v_strErrorMessage
    '                '        ' EnabledButtonWhenDoTransaction(True)
    '                '        'Exit Sub
    '                '    End If

    '                'End If
    '                '    Dim v_strHostName As String = String.Empty
    '                '    Dim v_strDBName As String = String.Empty
    '                '    Dim v_strDBUser As String = String.Empty
    '                '    Dim v_strDBPass As String = String.Empty

    '                '    If strPT_TT = "01" Then
    '                '        v_total_ID = "924"
    '                '    Else
    '                '        v_total_ID = "925"
    '                '    End If
    '                '    ''v_total_ID = "924"
    '                '    If (clsCTU.TCS_GetThamSoBDS(mv_objUser.MA_CN, v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)) Then
    '                '        Dim v_objBDS As New BDSHelper(v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)
    '                '        If v_objBDS.IncreaseTotal(v_total_ID, teller_id, v_strAmt) Then
    '                '            'Cập nhật lại trạng thái của chứng từ thành đã kiểm soát
    '                '            v_strTrangThai_CT = "01" 'Trạng thái đã kiểm soát
    '                '            ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_TKHachToan, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
    '                '        Else
    '                '            lblStatus.Text = "Không thể cập nhật số tiền vào BDS.Hãy kiểm tra lại"
    '                '            EnabledButtonWhenDoTransaction(True)
    '                '            'Exit Sub
    '                '        End If
    '                '    Else
    '                '        lblStatus.Text = "Không thể lấy tham số của BDS hãy kiểm tra lại."
    '                '        EnabledButtonWhenDoTransaction(True)
    '                '        'Exit Sub
    '                '    End If
    '                '    v_strTrangThai_CT = "01" 'Trạng thái đã kiểm soát
    '                '    ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_TKHachToan, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
    '                '    lblStatus.Text = "Kiểm soát chứng từ thành công.Xin chờ một lát để gửi thông tin sang hải quan"
    '            Else ' nếu là citad 2.0
    '                '    Dim v_arrOfRemarks As String = BuildRemarkForPaymentCITAD20(v_strRollout_acct_name, _
    '                '       txtMaNNT.Text, txtTenKB.Text, mv_objUser.Ngay_LV, v_strMaNHTT)
    '                '    'ManhNV sua 210 -> 159
    '                '    If v_arrOfRemarks.Length >180 Then
    '                '        lblStatus.Text = "Số lượng ký tự nhập vượt quá 210 ký tự.Hãy kiểm tra lại."
    '                '        EnabledButtonWhenDoTransaction(True)
    '                '        Exit Sub
    '                '    Else
    '                '        v_strRemark = v_arrOfRemarks
    '                '    End If
    '                '    'v_strRemark = v_arrOfRemarks

    '                '    Dim vstrNgay_gui As String = txtNGAY_KH_NH.Text
    '                '    Dim str_MaGD As String = "TP8278"
    '                '    Dim v_fee As String = txtCharge.Text.Replace(",", "")
    '                '    Dim v_vat As String = txtVAT.Text.Replace(",", "")
    '                '    Dim v_strTranDate As String = clsCTU.TCS_GetNgayBDS().ToString
    '                '    Dim v_TTCUTOFFTIME As String = clsCTU.TCS_TTCUTOFFTIME().ToString
    '                '    Dim v_strPTHT As String = String.Empty
    '                '    Dim v_TKHachToan As String = String.Empty
    '                '    If v_TTCUTOFFTIME = "1" Then
    '                '        v_strPTHT = "TPAYB4CUTOFF"
    '                '        v_TKHachToan = "120101001"
    '                '    Else
    '                '        v_strPTHT = "TPAYAFCUTOFF"
    '                '        v_TKHachToan = "280701018"
    '                '    End If
    '                '    ' v_strTranDate = ConvertNumberToDate(v_strTranDate.ToString().ToString("dd/MM/yyyy")
    '                '    'ConvertNumberToDate(v_strNgay_KB).ToString("dd/MM/yyyy")
    '                '    'mv_objUser.MA_CN =>lay theo ma chi nhanh cua user tao dien
    '                '    If strPT_TT = "01" Then 'Chuyển khoản
    '                '        str_MaGD = "TP8277"
    '                '        If txtSoDu_KH_NH.Text.Length = 0 Then
    '                '            lblStatus.Text = "Tài khoản khách hàng không đủ để thanh toán"
    '                '            EnabledButtonWhenDoTransaction(True)
    '                '            Exit Sub
    '                '        End If
    '                '        If (txtTenTK_KH_NH.Text.Length > 0) Then
    '                '            v_strRollout_branch_no = txtTenTK_KH_NH.Text.Split("-")(0)
    '                '            v_strCif_no = txtTenTK_KH_NH.Text.Split("-")(1)
    '                '            'v_strRollout_acct_name = txtTenTK_KH_NH.Text.Split("-")(2)
    '                '            v_strCert_code = txtTenTK_KH_NH.Text.Split("-")(3)
    '                '        End If
    '                '        'Kiểm tra tổng tiền trong chứng từ và số dư hiện tại
    '                '        If Double.Parse(strTongTienTT) > Double.Parse(strSoduTaiKhoanKH) Then
    '                '            lblStatus.Text = "Tài khoản không đủ để thanh toán.Hãy kiểm tra lại"
    '                '            EnabledButtonWhenDoTransaction(True)
    '                '            Exit Sub
    '                '        End If


    '                '        Dim v_objCoreBank As New clsCoreBankUtilMSB
    '                '        'Ghi nhận giao dịch vào host
    '                '        If (v_objCoreBank.PaymentCITAD20(v_strCif_no, v_strCert_type, v_strCert_code, _
    '                '             v_strIssue_date, v_strIssue_place, ma_ChiNhanh, v_strRollout_acct_no, _
    '                '             v_strRollout_acct_name, v_strTen_kb, v_strMaCqthu, v_strTenCqthu, _
    '                '             v_strMaNHGui, v_strMaNHNhan, vstrNgay_gui, v_strAmt, v_fee, v_vat, v_strRemark, _
    '                '             v_strErrorCode, v_strErrorMessage, v_strExcutionDate, _
    '                '             v_strHost_REFNo, teller_id, approver_id, v_strRM_REF_NO, v_strREF_NO, v_strTranDate, v_strPTHT, str_MaGD, v_TKHachToan, v_strMaNHTT)) Then
    '                '            'If (True) Then
    '                '            'Cập nhật lại trạng thái của chứng từ
    '                '            v_strTrangThai_CT = "06" 'Trạng thái chưa cập nhật vào BDS
    '                '            ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_TKHachToan, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
    '                '            lblStatus.Text = "Hệ thống đã ghi nhận giao dịch vào host.Xin chờ một lúc để ghi nhận vào BDS"
    '                '        Else
    '                '            lblStatus.Text = "Đã có lỗi xảy ra khi hạch toán chứng từ.Mã lỗi: " & v_strErrorCode & ",mô tả:" & v_strErrorMessage
    '                '            EnabledButtonWhenDoTransaction(True)
    '                '            Exit Sub
    '                '        End If
    '                '        'Ghi nhận giao dịch vào bds


    '                '        'ElseIf strPT_TT = "00" Then 'Tiền mặt
    '                '    ElseIf strPT_TT = "03" Then 'Trung gian
    '                '        Dim v_objCoreBank As New clsCoreBankUtilMSB
    '                '        'Dim isHighValue As Boolean = clsCommon.checkHighValue(v_strAmt)
    '                '        If (v_objCoreBank.PaymentGLCITA20(v_strCif_no, v_strCert_type, v_strCert_code, _
    '                '             v_strIssue_date, v_strIssue_place, ma_ChiNhanh, v_strRollout_acct_no, _
    '                '             v_strRollout_acct_name, v_strTen_kb, v_strMaCqthu, v_strTenCqthu, _
    '                '             v_strMaNHGui, v_strMaNHNhan, vstrNgay_gui, v_strAmt, v_fee, v_vat, v_strRemark, _
    '                '             v_strErrorCode, v_strErrorMessage, v_strExcutionDate, _
    '                '             v_strHost_REFNo, teller_id, approver_id, v_strRM_REF_NO, v_strREF_NO, v_strTranDate, v_strPTHT, str_MaGD, v_TKHachToan, v_strMaNHTT)) Then

    '                '            'Cập nhật lại trạng thái của chứng từ
    '                '            v_strTrangThai_CT = "06" 'Trạng thái chưa cập nhật vào BDS
    '                '            ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_TKHachToan, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
    '                '            lblStatus.Text = "Hệ thống đã ghi nhận giao dịch vào host.Xin chờ một lúc để ghi nhận vào BDS"
    '                '        Else
    '                '            lblStatus.Text = "Đã có lỗi xảy ra khi hạch toán chứng từ.Mã lỗi: " & v_strErrorCode & ",mô tả:" & v_strErrorMessage
    '                '            ' EnabledButtonWhenDoTransaction(True)
    '                '            Exit Sub
    '                '        End If

    '            End If
    '            '    Dim v_strHostName As String = String.Empty
    '            '    Dim v_strDBName As String = String.Empty
    '            '    Dim v_strDBUser As String = String.Empty
    '            '    Dim v_strDBPass As String = String.Empty
    '            '    If strPT_TT = "01" Then
    '            '        v_total_ID = "924"
    '            '    Else
    '            '        v_total_ID = "925"
    '            '    End If
    '            '    ''v_total_ID = "924"
    '            '    If (clsCTU.TCS_GetThamSoBDS(mv_objUser.MA_CN, v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)) Then
    '            '        Dim v_objBDS As New BDSHelper(v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)
    '            '        If v_objBDS.IncreaseTotal(v_total_ID, teller_id, v_strAmt) Then
    '            '            'Cập nhật lại trạng thái của chứng từ thành đã kiểm soát
    '            '            v_strTrangThai_CT = "01" 'Trạng thái đã kiểm soát
    '            '            ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
    '            '        Else
    '            '            lblStatus.Text = "Không thể cập nhật số tiền vào BDS.Hãy kiểm tra lại"
    '            '            EnabledButtonWhenDoTransaction(True)
    '            '            Exit Sub
    '            '        End If
    '            '    Else
    '            '        lblStatus.Text = "Không thể lấy tham số của BDS hãy kiểm tra lại."
    '            '        EnabledButtonWhenDoTransaction(True)
    '            '        Exit Sub
    '            '    End If
    '            '    v_strTrangThai_CT = "01" 'Trạng thái đã kiểm soát
    '            '    ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
    '            '    lblStatus.Text = "Kiểm soát chứng từ thành công.Xin chờ một lát để gửi thông tin sang hải quan"

    '        End If


    '        v_strTrangThai_CT = "01" 'Trạng thái chưa cập nhật vào BDS
    '        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", mv_objUser.Ma_NV)
    '        EnabledButtonWhenDoTransaction(True)

    '        Try
    '            'sonmt: cap nhat ngay bao no, bao co
    '            Dim ngay_bao_no As DateTime = DateTime.Now
    '            Dim ngay_bao_co As DateTime = DateTime.Now
    '            Dim ngay_tiep_theo As DateTime = DateTime.Now.AddDays(1)
    '            Dim strTT_CUTOFFTIME As String = CTuCommon.GetValue_THAMSOHT("TT_CUTOFFTIME")
    '            If Not strTT_CUTOFFTIME.Equals("2") Then
    '                ngay_bao_no = DateTime.Now
    '                ngay_bao_co = DateTime.Now
    '            Else
    '                ngay_bao_no = DateTime.Now
    '                While (True) 'Kiem tra xem ngay tiep theo co vao ngay nghi hay khong, neu vao ngay nghi thi cong them 1 ngay cho den ngay lam viec tiep theo
    '                    Dim dtNgayNghi As DataTable = DataAccess.ExecuteReturnDataSet("SELECT ngay_nghi FROM tcs_dm_ngaynghi WHERE ngay_nghi = to_date('" & ngay_tiep_theo.ToString("dd/MM/yyyy") & "', 'dd/MM/yyyy')", CommandType.Text).Tables(0)
    '                    If dtNgayNghi.Rows.Count > 0 Then

    '                        Dim ngay_nghi As DateTime = dtNgayNghi.Rows(0)("ngay_nghi")

    '                        If ngay_nghi.ToString("dd/MM/yyyy").Equals(ngay_tiep_theo.ToString("dd/MM/yyyy")) Then
    '                            ngay_tiep_theo = ngay_tiep_theo.AddDays(1)
    '                        Else
    '                            Exit While
    '                        End If
    '                    Else
    '                        Exit While
    '                    End If

    '                End While

    '                ngay_bao_co = ngay_tiep_theo
    '            End If

    '            'Cap nhat ngay bao no, ngay bao co vao chung tu
    '            DataAccess.ExecuteNonQuery("update tcs_ctu_hdr set NGAY_BAONO= to_date('" & ngay_bao_no.ToString("dd/MM/yyyy HH:mm:ss") & "', 'dd/MM/yyyy HH24:MI:SS'), NGAY_BAOCO= to_date('" & ngay_bao_co.ToString("dd/MM/yyyy HH:mm:ss") & "', 'dd/MM/yyyy HH24:MI:SS') where so_ct='" & v_strSo_CT & "'", CommandType.Text)


    '        Catch ex As Exception

    '        End Try

    '        Dim blnSuccess As Boolean = True
    '        Dim v_strTransaction_id As String = String.Empty
    '        Dim desMSG_HQ As String = ""
    '        Try
    '            ''Gui thong tin Chung tu sang Hai quan
    '            strNguyenTe = txtMaNT.Text.ToString()
    '            'If strNguyenTe = "VND" Then
    '            '    Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M47.ToString())
    '            '    If strTN_CT <> "" Then
    '            '        pv_so_tn_ct = strTN_CT.Split(";")(0)
    '            '        pv_ngay_tn_ct = strTN_CT.Split(";")(1)
    '            '    End If

    '            '    CustomsServiceV3.ProcessMSG.getMSG26(v_strMaNNT, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, v_strMa_Dthu, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
    '            '    MSGType = TransactionHQ_Type.M47.ToString()
    '            'Else
    '            '    Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M49.ToString())
    '            '    If strTN_CT <> "" Then
    '            '        pv_so_tn_ct = strTN_CT.Split(";")(0)
    '            '        pv_ngay_tn_ct = strTN_CT.Split(";")(1)
    '            '    End If
    '            '    CustomsServiceV3.ProcessMSG.getMSG27(v_strMaNNT, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, v_strMa_Dthu, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
    '            '    MSGType = TransactionHQ_Type.M49.ToString()

    '            'End If

    '            Dim loai_ct As String = hdfLoai_CT.Value

    '            Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M47.ToString())
    '            If strTN_CT <> "" Then
    '                pv_so_tn_ct = strTN_CT.Split(";")(0)
    '                pv_ngay_tn_ct = strTN_CT.Split(";")(1)
    '            End If

    '            If loai_ct.Equals("P") Then
    '                'Le phi quan
    '                CustomsServiceV3.ProcessMSG.guiCTLePhiHQ(v_strMaNNT, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
    '                MSGType = TransactionHQ_Type.M41.ToString()
    '            Else
    '                'Thue hai quan
    '                CustomsServiceV3.ProcessMSG.guiCTThueHQ(v_strMaNNT, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
    '                MSGType = TransactionHQ_Type.M47.ToString()
    '            End If


    '            'strErrorNum = ""
    '            'Neu gui thong tin thanh cong
    '            'strErrorNum = "0"
    '            If strErrorNum = "0" Then
    '                lblStatus.Text = "Chứng từ đã được hạch toán và chuyển số liệu sang Cơ quan Hải quan thành công"
    '            Else
    '                blnSuccess = False
    '                lblStatus.Text = "Chứng từ đã được hạch toán. Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
    '                cmdKS.Enabled = False
    '                cmdChuyenTra.Enabled = False
    '                LoadDSCT(ddlTrangThai.SelectedValue)

    '            End If

    '        Catch ex As Exception
    '            blnSuccess = False
    '            lblStatus.Text = "Chứng từ đã được hạch toán. Có lỗi khi gửi số liệu sang Hải Quan. Mô tả: " & ex.ToString
    '            cmdKS.Enabled = False
    '            cmdChuyenTra.Enabled = False
    '            LoadDSCT(ddlTrangThai.SelectedValue)
    '            Exit Sub
    '        End Try
    '        'CAP NHAT TRANG THAI DA CHUYEN THUE

    '        'Insert du lieu vao bang DOICHIEU_NHHQ
    '        desMSG_HQ = lblStatus.Text.Trim
    '        If desMSG_HQ.Length > 400 Then
    '            desMSG_HQ = desMSG_HQ.Substring(0, 399)
    '        End If
    '        If strErrorNum = "0" Then
    '            clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, v_strMa_Dthu, strErrorNum, MSGType, "", pv_so_tn_ct, pv_ngay_tn_ct)
    '            hdr.transaction_id = v_strTransaction_id
    '            hdr.ten_nh_th = txtTen_NH_B.Text.Trim.ToString()
    '            ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)

    '        End If

    '        ChungTu.buChungTu.InsertMSGLoi(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)

    '        LoadDSCT(ddlTrangThai.SelectedValue)
    '        'ShowHideCTRow(True)
    '        EnabledButtonWhenDoTransaction(False)
    '        'lblStatus.Text = "Chứng từ đã được hạch toán và chuyển số liệu sang Cơ quan Hải quan thành công"
    '        cmdChuyenTra.Enabled = False
    '        'cmdHuy.Enabled = True
    '        hdfTrangThai_CT.Value = v_strTrangThai_CT
    '        hdfTrangThai_CThue.Value = IIf(blnSuccess, "1", "0")
    '        ''''Else
    '        lblStatus.Text = strErrorNum & " - " & strErrorMes
    '        ''''End If

    '    Catch ex As Exception
    '        Dim sbErrMsg As StringBuilder
    '        sbErrMsg = New StringBuilder()
    '        sbErrMsg.Append("Lỗi trong quá trình kiểm soát chứng từ")
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.Message)
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.StackTrace)

    '        LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

    '        If Not ex.InnerException Is Nothing Then
    '            sbErrMsg = New StringBuilder()

    '            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
    '            sbErrMsg.Append(vbCrLf)
    '            sbErrMsg.Append(ex.InnerException.Message)
    '            sbErrMsg.Append(vbCrLf)
    '            sbErrMsg.Append(ex.InnerException.StackTrace)

    '            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
    '        End If

    '        ' Throw ex
    '    Finally
    '    End Try
    'End Sub

    'đang sửa'

    'Protected Sub cmdKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKS.Click

    '    'Dim intRowcount1 As Integer
    '    'Dim str_Desc1 As String = BuildRemarkForPayment(intRowcount1)

    '    Dim Mota As String = ""
    '    Dim NewValue As String = ""
    '    Dim Action As String = "KIEMSOAT_CT_HQ_KIEMSOAT"
    '    Dim v_blnReturn As Boolean = False
    '    Dim CITAD_VERSION As String = ""
    '    Dim strErrorNum As String = ""
    '    Dim strErrorMes As String = ""
    '    Dim pv_so_tn_ct As String = ""
    '    Dim pv_ngay_tn_ct As String = ""
    '    Dim MSGType As String = ""
    '    Dim v_strTransaction_id As String = String.Empty
    '    Dim hdr As New infDoiChieuNHHQ_HDR
    '    Dim dtls() As infDoiChieuNHHQ_DTL
    '    Try
    '        'Disable button tranh lam viec 2 lan
    '        EnabledButtonWhenDoTransaction(False)
    '        Dim strUser = mv_objUser.Ten_DN.ToString
    '        'Check Hạn mức
    '        Dim tien_tongTN As Decimal = Convert.ToDecimal(txtTTIEN.Text.ToString().Replace(".", "").Replace(",", ""))

    '        If Not Business.Common.mdlCommon.CheckHanMuc_KSV(Session.Item("User").ToString(), tien_tongTN) Then
    '            lblStatus.Text = "Hạn mức của bạn không đủ để duyệt giao dịch này."
    '            Exit Sub
    '        End If
    '        'End Hạn mức
    '        'Kiểm tra ngày làm việc

    '        'If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
    '        '    lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
    '        '    EnabledButtonWhenDoTransaction(True)
    '        '    Exit Sub
    '        'End If


    '        Dim v_strErrorCode As String = String.Empty
    '        Dim v_strErrorMessage As String = String.Empty
    '        Dim v_objGDV As CTuUser
    '        'Dat lai gia tri
    '        Dim v_strSHKB As String = hdfSHKB.Value
    '        Dim v_strNgay_KB As String = hdfNgay_KB.Value
    '        Dim v_strSo_CT As String = hdfSo_CT.Value
    '        Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
    '        Dim v_strSo_BT As String = hdfSo_BT.Value
    '        Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
    '        Dim v_strMa_NV As String = hdfMa_NV.Value
    '        v_objGDV = New CTuUser(v_strMa_NV)
    '        Dim strHT_TT As String = ddlHTTT.Text.Trim
    '        Dim v_strMa_NH_B As String = txtMA_NH_B.Text.Trim
    '        Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value

    '        If ((Not v_strSHKB Is Nothing) _
    '             And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
    '            Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong

    '            Dim strResult As String = cls_songphuong.ValiadSHB(v_strSHKB)

    '            Dim warningMessage As String = "Lệnh thu ngân sách có thể thu tại SHB, bạn có muốn tiếp tục?"

    '            If strResult.Length >= 8 Then
    '                If strResult.Equals(v_strMa_NH_B) Then
    '                    'Me.Page.ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessWarningMessage", "jsProcessWarningMessage('" + warningMessage + "');", True)
    '                    Me.Page.ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
    '                Else
    '                    Me.Page.ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessWarningMessage", "jsProcessWarningMessage('" + warningMessage + "');", True)
    '                End If
    '            Else
    '                Me.Page.ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
    '            End If
    '        End If

    '    Catch ex As Exception
    '        Mota = " object:  result: Fail  add_info: " & NewValue
    '        LogApp.AddDebug(Action, Mota & ex.Message)
    '        Dim sbErrMsg As StringBuilder
    '        sbErrMsg = New StringBuilder()
    '        sbErrMsg.Append("Lỗi trong quá trình kiểm soát chứng từ")
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.Message)
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.StackTrace)

    '        LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

    '        If Not ex.InnerException Is Nothing Then
    '            sbErrMsg = New StringBuilder()

    '            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
    '            sbErrMsg.Append(vbCrLf)
    '            sbErrMsg.Append(ex.InnerException.Message)
    '            sbErrMsg.Append(vbCrLf)
    '            sbErrMsg.Append(ex.InnerException.StackTrace)

    '            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
    '        End If
    '        lblStatus.Text = sbErrMsg.ToString
    '        Throw ex
    '    End Try
    'End Sub
    Protected Sub cmdKSBK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKSBK.Click

        'Dim intRowcount1 As Integer
        'Dim str_Desc1 As String = BuildRemarkForPayment(intRowcount1)

        Dim Mota As String = ""
        Dim NewValue As String = ""
        Dim Action As String = "KIEMSOAT_CT_HQ_KIEMSOAT"
        Dim v_blnReturn As Boolean = False
        Dim CITAD_VERSION As String = ""
        Dim strErrorNum As String = ""
        Dim strErrorMes As String = ""
        Dim pv_so_tn_ct As String = ""
        Dim pv_ngay_tn_ct As String = ""
        Dim MSGType As String = ""
        Dim v_strTransaction_id As String = String.Empty
        Dim hdr As New infDoiChieuNHHQ_HDR
        Dim dtls() As infDoiChieuNHHQ_DTL
        Try
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)
            Dim strUser = mv_objUser.Ten_DN.ToString
            'Check Hạn mức
            Dim tien_tongTN As Decimal = Convert.ToDecimal(txtTTIEN.Text.ToString().Replace(".", "").Replace(",", ""))

            If Not Business.Common.mdlCommon.CheckHanMuc_KSV(Session.Item("User").ToString(), tien_tongTN) Then
                lblStatus.Text = "Hạn mức của bạn không đủ để duyệt giao dịch này."
                Exit Sub
            End If
            'End Hạn mức
            'Kiểm tra ngày làm việc

            'If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
            '    lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
            '    EnabledButtonWhenDoTransaction(True)
            '    Exit Sub
            'End If


            Dim v_strErrorCode As String = String.Empty
            Dim v_strErrorMessage As String = String.Empty
            Dim v_objGDV As CTuUser
            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            v_objGDV = New CTuUser(v_strMa_NV)
            Dim strHT_TT As String = ddlHTTT.Text.Trim

            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value

            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If

                'Cập nhật số lần nhấn kiểm soát: check cột save_type
                Dim solan As Integer = 0
                Dim strClick As String = ChungTu.buChungTu.CTU_Get_LanKS(key)
                If strClick.Length > 0 Then
                    solan = CInt(strClick) + 1
                    strClick = solan.ToString()
                Else
                    strClick = "1"
                End If
                ChungTu.buChungTu.CTU_SetLanKS(key, strClick)
                'end cập nhật số lần

                'Lấy trực tiếp phương thức thanh toán trong DB
                Dim strTongTienTT As String = txtTTIEN.Text.Replace(".", "").Replace(",", "")
                Dim strSoduTaiKhoanKH As String = txtSoDu_KH_NH.Text.Replace(".", "").Replace(",", "")
                Dim str_Fee As String = txtCharge.Text.Trim.Replace(".", "").Replace(",", "")
                Dim str_VAT As String = txtVAT.Text.Trim.Replace(".", "").Replace(",", "")
                'mv_objUser.MA_CN =>lay theo ma chi nhanh cua user tao dien

                Dim isGL As Boolean = True
                If strHT_TT = "01" Then
                    'If Integer.Parse(clsCTU.CheckGL(txtTK_KH_NH.Text.Trim)) = 0 Then
                    '    If txtSoDu_KH_NH.Text.Length = 0 Then
                    '        lblStatus.Text = "Tài khoản khách hàng không đủ để thanh toán"
                    '        EnabledButtonWhenDoTransaction(True)
                    '        Exit Sub
                    '    End If
                    '    If Double.Parse(strTongTienTT) > Double.Parse(strSoduTaiKhoanKH) Then
                    '        lblStatus.Text = "Tài khoản không đủ để thanh toán.Hãy kiểm tra lại"
                    '        EnabledButtonWhenDoTransaction(True)
                    '        Exit Sub
                    '    End If

                    '    isGL = False
                    'End If
                End If
                Dim v_strMaNNT As String = hdfMaNNT.Value
                Dim strMaNNT As String = txtMaNNT.Text.Trim
                Dim strTenNNT As String = Globals.RemoveSign4VietnameseString(txtTenNNT.Text.Trim)
                Dim strDiaChiNNT As String = Globals.RemoveSign4VietnameseString(txtDChiNNT.Text.Trim)
                Dim strSoCT_NH As String = txtSoCT_NH.Text.Trim
                Dim strErrorCode As String = ""
                Dim strErrorMessege As String = ""
                Dim a As Integer = 1
                Dim str_MaTinh As String = txtTinh_TphoNNTien.Text.Trim
                Dim str_TenTinh As String = Globals.RemoveSign4VietnameseString(txtQuan_HuyenNNTien.Text.Trim)
                Dim str_AccountNo As String = txtTK_KH_NH.Text.Trim
                Dim str_HTTT As String
                If strHT_TT = "01" Or strHT_TT = "05" Then
                    str_HTTT = "A"
                Else
                    str_HTTT = "G"
                End If
                Dim intRowcount As Integer
                Dim str_Desc As String = txtDienGiai.Text.Trim.ToString() 'BuildRemarkForPayment(intRowcount)
                If str_Desc.Length > 210 Then
                    lblStatus.Text = "Chứng từ hạch toán lỗi. Mô tả lỗi: Số ký tự Citad vượt quá 210 ký tự."
                    Exit Sub
                End If

                Dim str_clk As String = ""
                str_clk = BuildCKTM()
                Dim str_loaiPhi As String = ""
                'If rdTypeMP.Checked = True Then
                '    str_loaiPhi = "W"
                'ElseIf rdTypePN.Checked = True Then
                '    str_loaiPhi = "O"
                'Else
                str_loaiPhi = "B"
                'End If

                Dim strMa_NH_A As String = txtMA_NH_A.Text.Trim
                Dim strTen_NH_A As String = txtTen_NH_A.Text.Trim
                Dim str_TKKB As String = txtTKCo.Text.Trim
                Dim str_TenKB As String = Globals.RemoveSign4VietnameseString(txtTenKB.Text.Trim & txtTen_NH_B.Text.Trim)
                Dim str_TenCQT As String = Globals.RemoveSign4VietnameseString(txtTenCQThu.Text.Trim)
                If str_TenKB.Length > 70 Then
                    str_TenKB = str_TenKB.Substring(0, 69)
                End If
                If str_TenCQT.Length > 70 Then
                    str_TenCQT = str_TenCQT.Substring(0, 69)
                End If
                Dim strMa_NH_TT As String = txtMA_NH_TT.Text.Trim
                Dim strTen_NH_TT As String = txtTEN_NH_TT.Text.Trim
                Dim strMa_NH_B As String = txtMA_NH_B.Text.Trim
                Dim strTen_NH_B As String = txtTen_NH_B.Text.Trim
                Dim str_CIFNo As String = ""

                If txtTenTK_KH_NH.Text.Split("|").Length > 1 Then
                    str_CIFNo = txtTenTK_KH_NH.Text.Split("|")(1)
                End If

                Dim str_TenKH As String = txtTenTK_KH_NH.Text
                Dim str_RefNo As String = txtREF_NO.Text.Trim
                Dim strLoaiTien As String = HaiQuan.HaiQuan.GetCODE_TIENTE(txtMaNT.Text) '"704" 'txtMaNT.Text
                'Dim str_MaSanPham As String = txtSanPham.Text.Trim
                Dim str_TimeInput As String = txtTimeLap.Text
                Dim strTKCo As String = txtTKCo.Text.Trim
                Dim strDC_NNTien As String = txtDChiNNTien.Text.ToString() & " " & txtTenQuanNNTien.Text.ToString() & " " & txtTenTinhNNTien.Text.ToString()
                Dim strDC_NNThue As String = txtDChiNNT.Text.ToString() & " " & txtTenQuanNNThue.Text.ToString() & " " & txtTenTinhNNThue.Text.ToString()
                Dim TRAN_ID As String = ""
                Dim ngay_GD As String = ConvertNumberToDate(mv_objUser.Ngay_LV).ToString("dd/MM/yyyy")
                Dim ngay_GD_SP As String = ConvertNumberToDate(mv_objUser.Ngay_LV).ToString("yyyyMMdd")
                Dim strMa_NguyenTe As String = txtMaNT.Text.Trim
                Dim TT_CITAD As String = ""
                If strTKCo.Substring(0, 3) = "711" Then
                    TT_CITAD = "1"
                Else
                    TT_CITAD = "0"
                End If
                Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
                'xml to citad
                Dim str_DTLCitadXML As String = BuildDTLCitadXML(v_intRowCnt)
                Dim XMLtoCITAD As String = clsCTU.BuilContent_citad(txtMaNNT.Text, txtMaCQThu.Text, txtTenCQThu.Text, ddlLoaiThue.SelectedValue, txtNGAY_KH_NH.Text, _
                                        txtSoKhung.Text, txtSoMay.Text, txtToKhaiSo.Text, txtNgayDK.Text, txtLHXNK.Text, "", txtTenNNT.Text, str_DTLCitadXML)

                Dim v_dtCitad_HDR_xml As New DataTable
                Dim v_dtCitad_DTL_xml As New DataTable
                v_dtCitad_HDR_xml = clsCTU.TCS_BuildCITAD_HDR_XML()
                v_dtCitad_DTL_xml = clsCTU.TCS_BuildCITAD_DTL_XML(v_intRowCnt)

                v_dtCitad_HDR_xml.Rows(0).Item("MST") = strMaNNT
                v_dtCitad_HDR_xml.Rows(0).Item("CQT") = txtMaCQThu.Text
                v_dtCitad_HDR_xml.Rows(0).Item("TCQ") = txtTenCQThu.Text
                v_dtCitad_HDR_xml.Rows(0).Item("LTH") = ddlLoaiThue.SelectedValue
                v_dtCitad_HDR_xml.Rows(0).Item("NNT") = txtNGAY_KH_NH.Text.ToString.Split("/")(2) & txtNGAY_KH_NH.Text.ToString.Split("/")(1) & txtNGAY_KH_NH.Text.ToString.Split("/")(0)
                v_dtCitad_HDR_xml.Rows(0).Item("SKH") = txtSoKhung.Text
                v_dtCitad_HDR_xml.Rows(0).Item("SMA") = txtSoMay.Text
                v_dtCitad_HDR_xml.Rows(0).Item("STK") = txtToKhaiSo.Text
                If txtNgayDK.Text.ToString.Trim().Length >= 10 Then
                    v_dtCitad_HDR_xml.Rows(0).Item("NTK") = txtNgayDK.Text.ToString.Split("/")(2) & txtNgayDK.Text.ToString.Split("/")(1) & txtNgayDK.Text.ToString.Split("/")(0)
                Else
                    v_dtCitad_HDR_xml.Rows(0).Item("NTK") = txtNgayDK.Text.ToString
                End If
                v_dtCitad_HDR_xml.Rows(0).Item("XNK") = txtLHXNK.Text
                v_dtCitad_HDR_xml.Rows(0).Item("CQP") = ""
                v_dtCitad_HDR_xml.Rows(0).Item("TKH") = txtTenNNT.Text
                For i = 0 To v_intRowCnt - 1
                    v_dtCitad_DTL_xml.Rows(i).Item("MCH") = CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
                    v_dtCitad_DTL_xml.Rows(i).Item("NDK") = CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
                    v_dtCitad_DTL_xml.Rows(i).Item("STN") = CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text.Replace(".", "").Replace(",", "")
                    v_dtCitad_DTL_xml.Rows(i).Item("NDG") = CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text
                Next
                'Built to table
                'TCS_DS_CITAD
                Dim time_check_GT As Integer = 0
                time_check_GT = Now.Hour
                Dim strGT_CAOTHAP As String
                strGT_CAOTHAP = clsCTU.TCS_GETTS("TT_CITAD")
                Dim time_checkTS_GT As String
                time_checkTS_GT = clsCTU.TCS_GETTS("GIO_CUTOFF_CITAD")
                Dim TRX_TYPE As String = ""
                If Convert.ToDecimal(strTongTienTT) < Convert.ToDecimal(strGT_CAOTHAP) And time_check_GT < Convert.ToInt32(time_checkTS_GT) Then
                    TRX_TYPE = "101001"
                Else
                    TRX_TYPE = "201001"
                End If
                CITAD_VERSION = clsCTU.TCS_GETTS("CITAD_VERSION")
                Dim o_ci_code As String = strMa_NH_A
                Dim r_ci_code As String = strMa_NH_TT
                Dim o_indirect_code As String = strMa_NH_A
                Dim r_indirect_code As String = strMa_NH_B
                Dim trx_date As String = mv_objUser.Ngay_LV
                Dim create_date As Date = ConvertNumberToDate(v_strNgay_KB)
                Dim currency As String = strLoaiTien
                Dim amount As String = strTongTienTT
                Dim sd_name As String = strTenNNT
                Dim sd_addr As String = strDiaChiNNT
                Dim sd_accnt As String = ""
                Dim blnSuccess As Boolean = True
                Dim desMSG_HQ As String = ""
                If strHT_TT = "01" Or strHT_TT = "05" Then
                    sd_accnt = txtTK_KH_NH.Text.ToString()
                Else
                    sd_accnt = strTT_TIENMAT
                End If

                Dim rv_name As String = str_TenKB
                Dim rv_acctn As String = str_TKKB & "." & txtMaCQThu.Text.Trim

                Dim rv_addr As String = "MaCQT" & txtMaCQThu.Text.ToString() & Get_TenCQThue_DAB(txtMaCQThu.Text.Trim)
                Dim rv_accnt As String = strTKCo
                Dim content As String = "NopThue"   'giống MSB-Mac dinh là NOPTHUE
                Dim relation_no As String = Get_SoRelation_No()
                Dim content_ex As String = ""
                Dim reference As String = ""
                If CITAD_VERSION = "2.1" Then
                    If strTKCo.Substring(0, 3) = "711" Then
                        reference = "IBPSVST000"
                    Else
                        reference = str_clk
                        content = str_Desc
                    End If

                    If reference = "IBPSVST000" Then
                        content_ex = XMLtoCITAD
                    Else
                        content_ex = ""
                    End If
                Else
                    reference = str_clk
                    content = str_Desc
                    content_ex = ""
                End If

                Dim relation_no_ref As String = ""
                Dim ghi_chu As String = ""
                ''''Dim strMa_SP As String = txtSanPham.Text.Trim.Split("-")(0).ToString

                Dim v_content As String = String.Empty
                Dim v_contentEx As String = String.Empty

                If Not CITAD_VERSION.Equals("1.0") Then
                    str_DTLCitadXML = BuildDTLCitadXML(v_intRowCnt)
                    v_contentEx = clsCTU.BuilContent_citad(txtMaNNT.Text, txtMaCQThu.Text, txtTenCQThu.Text, ddlLoaiThue.SelectedValue, txtNGAY_KH_NH.Text, _
                                            txtSoKhung.Text, txtSoMay.Text, txtToKhaiSo.Text, txtNgayDK.Text, txtLHXNK.Text, "", txtTenNNT.Text, str_DTLCitadXML)

                Else
                    v_content = str_Desc
                End If

                NewValue = "newvalue=""SO_CT[SO_CT=" & v_strSo_CT & ",Ma_NNT=" & strMaNNT & ",SHKB= " & v_strSHKB & ", Ma_CN=" & v_objGDV.MA_CN & ",Ten_DN= " & mv_objUser.Ten_DN & " ]"" Description: " & ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) & " Exception:"

                Dim v_arrLogPay(4) As String
                v_arrLogPay(0) = str_Fee
                v_arrLogPay(1) = str_VAT
                v_arrLogPay(2) = v_objGDV.Ma_NV
                v_arrLogPay(3) = mv_objUser.Ma_NV
                v_arrLogPay(4) = str_RefNo

                str_Desc = Globals.RemoveSign4VietnameseString(str_Desc)
                If str_Desc.Length > 210 Then
                    lblStatus.Text = "Nội dung diễn giải quá 210 ký tự"
                    Exit Sub
                End If

                Dim strTypeTransferNTTQ As String = "NTTQ"
                Dim dtGetTypeTransferNTTQ As DataTable = DataAccess.ExecuteToTable("select * from tcs_thamso_ht where ten_ts='TYPE_TRANSFER_PAYMENT_NTTQ'")

                If dtGetTypeTransferNTTQ.Rows.Count > 0 Then
                    strTypeTransferNTTQ = dtGetTypeTransferNTTQ.Rows(0)("GIATRI_TS").ToString
                End If

                Dim makerUser As HeThong.infUser = HeThong.buUsers.NHANVIEN_DETAIL(v_strMa_NV)

                Dim str_PayMent As String = ""
                Dim rm_ref_no As String = ""
                Dim req_Internal_rmk As String = ""
                Dim sTenMaCN As String = ""
                sTenMaCN = Get_TenCN(hdfMACN.Value)
                req_Internal_rmk = strMaNNT & "-" & strTenNNT 'hdfMACN.Value & "-" & sTenMaCN ' [Mã số thuế]-[Tên người nộp thuế]
                Dim sMACN As String = hdfMACN.Value
                'check SP tại SHB
                Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong

                Dim strResult As String = cls_songphuong.ValiadSHB(v_strSHKB)

                Dim strNHB As String = ""
                Dim strACC_NHB As String = ""
                Dim strMA_CN_SP As String = ""
                If strResult.Length > 8 Then
                    Dim strarr() As String
                    strarr = strResult.Split(";")
                    strNHB = strarr(0).ToString
                    strACC_NHB = strarr(1).ToString
                    strMA_CN_SP = strarr(2).ToString
                End If


                Dim boolSP As Boolean = False
                If strNHB.Length >= 8 And strNHB.Equals(strMa_NH_B) Then
                    ''đi theo hàm thanh toán mới, sau đó gửi song phương
                    Dim strMACN_TK As String = ""
                    boolSP = True
                    'v_strErrorCode = "00000"
                    'rm_ref_no = "xxxxxxxxxx"
                    'Nếu thanh toán được kho bạc SHB mới truy vấn tài khoản để lấy mã chi nhánh tài khoản
                    If strHT_TT = "01" Then
                        If (txtTK_KH_NH.Text.Trim.Length > 0) Then
                            Dim xmlDoc As New XmlDocument
                            Dim ma_loi As String = ""
                            Dim so_du As String = ""
                            Dim ten_TK_KH As String = ""
                            Dim branch_tk As String = ""
                            Dim v_strReturn As String = cls_corebank.GetTK_KH_NH(txtTK_KH_NH.Text.Trim, ma_loi, so_du, branch_tk)

                            If ma_loi = "00000" Or ma_loi Is Nothing Then
                                strMACN_TK = branch_tk
                            Else
                                lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                                EnabledButtonWhenDoTransaction(True)
                                Exit Sub
                            End If
                        End If
                    ElseIf strHT_TT = "05" Then

                        strMACN_TK = sMACN
                    End If
                   
                    'cap nhat TTSP ve 1
                    ChungTu.buChungTu.CTU_Update_TTSP(v_strSo_CT, "1")
                    str_PayMent = cls_corebank.PaymentSP("TAX", ngay_GD_SP, v_strSo_CT, sMACN, strMACN_TK, str_AccountNo, strTongTienTT, txtMaNT.Text.Trim, req_Internal_rmk, str_Desc, _
                             v_strMa_NV, mv_objUser.Ma_NV, "ETAX_HQ", strTKCo, strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B, strACC_NHB, strMA_CN_SP, v_strErrorCode, rm_ref_no)
                Else
                    If Business.Common.mdlCommon.checkExist_NH_GT_TT_SHB(v_strSHKB, strMa_NH_B) Then
                        lblStatus.Text = "Kho bạc không được thu tại SHB, vui lòng kiểm tra lại ngân hàng hưởng."
                        EnabledButtonWhenDoTransaction(True)
                        Exit Sub
                    End If
                    If CORE_VERSION = "2" Then
                        'Core V2.0 20180330-------------------------------

                        Dim EcomStr As String = Globals.RemoveSign4VietnameseString(BuildEcomForPayment_V20(intRowcount))
                        'str_Desc = Globals.RemoveSign4VietnameseString(BuildRemarkForPayment_V20(intRowcount))
                        'Do dai ky tu cac truong la 200
                        Dim str_TenMST As String = V_Substring(txtTenNNT.Text, 200)
                        Dim str_MST As String = V_Substring(txtMaNNT.Text, 14)
                        Dim str_MaLThue As String = V_Substring(ddlLoaiThue.SelectedValue, 2)
                        'Ngay nop thue dinh dang dd-MM-yyyy

                        Dim str_NNT As String = DateTime.Now.ToString("yyyyMMdd").ToString()
                        Dim str_AddNNT As String = V_Substring(VBOracleLib.Globals.RemoveSign4VietnameseString(txtDChiNNT.Text), 200)
                        Dim strMaCQT As String = V_Substring(VBOracleLib.Globals.RemoveSign4VietnameseString(txtMaCQThu.Text), 7)
                        Dim strMaDBHC As String = V_Substring(txtMaDBHC.Text, 200)
                        Dim strGetTTTinhThanhKB As String = Business_HQ.HaiQuan.HaiQuanController.GetTTTinhThanhKB(strMaDBHC)
                        Dim strTemp As String() = strGetTTTinhThanhKB.Split(";")
                        Dim strTenDBHC As String = Globals.RemoveSign4VietnameseString(strTemp(1))
                        strTenDBHC = V_Substring(strTenDBHC, 200) 'V_Substring(txtTenDBHC.Text, 200)
                        str_PayMent = cls_corebank.PaymentTQ_V20(str_AccountNo, strTKCo, strTongTienTT, strLoaiTien, str_Desc, v_strNgay_KB, str_TenCQT, _
                                                                 str_MaTinh, str_TenCQT, str_TenCQT, strTen_NH_B, strTen_NH_B, str_TenTinh, _
                                                                  strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B.ToString().Substring(5, 3), strMa_NH_B.ToString().Substring(0, 2), "TAX", v_strSo_CT, strMa_NH_B, v_strMa_NV, mv_objUser.Ma_NV, "ETAX_HQ", req_Internal_rmk, _
                                                                    str_NNT, str_MaLThue, str_MST, strMaCQT, str_TenCQT, strMaDBHC, strTenDBHC, str_TenMST, str_AddNNT, EcomStr, _
                                                                    v_strErrorCode, rm_ref_no)

                        'Core V2.0 20180330-------------------------------
                    Else
                        str_PayMent = cls_corebank.PaymentTQ(str_AccountNo, strTKCo, strTongTienTT, strLoaiTien, str_Desc, v_strNgay_KB, str_TenCQT, _
                                          str_MaTinh, str_TenCQT, str_TenCQT, strTen_NH_B, strTen_NH_B, str_TenTinh, _
                                           strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B.ToString().Substring(5, 3), strMa_NH_B.ToString().Substring(0, 2), "TAX", v_strSo_CT, strMa_NH_B, v_strMa_NV, mv_objUser.Ma_NV, "ETAX_HQ", req_Internal_rmk, v_strErrorCode, rm_ref_no)
                    End If
                End If

                If v_strErrorCode = "00000" Then
                    Mota = " object: " & str_Desc & " result: Succes add_info: " & NewValue
                    LogApp.AddDebug(Action, Mota)
                    'Cập nhật lại trạng thái của chứng từ
                    v_strTrangThai_CT = "01" 'Trạng thái chưa cập nhật vào BDS
                    lbSoFT.Text = "Số FT:" & rm_ref_no

                    'cap nhat  ngay_kh_nh theo gio hien tai sysdate 
                    'Cap nhat ngay bao no, ngay bao co vao chung tu
                    DataAccess.ExecuteNonQuery("update tcs_ctu_hdr set ngay_kh_nh = sysdate where so_ct='" & v_strSo_CT & "'", CommandType.Text)

                    ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, rm_ref_no, "", "", mv_objUser.Ma_NV)
                    lblStatus.Text = "Kiểm soát chứng từ thành công.Xin chờ một lát để gửi thông tin sang HQ"

                    'gui di song phuong
                    If boolSP = True Then
                        Try
                            cls_songphuong.SendChungTuSP(v_strSo_CT, "TQ")
                        Catch ex As Exception
                            'chi ghi log, khong throw, van gui hai quan binh thuong
                            LogApp.WriteLog("KiemSoatCT_HQ_TK.SendChungTuSP", ex, "Lỗi send song phương", HttpContext.Current.Session("TEN_DN"))
                            'LogDebug.WriteLog("KiemSoatCT_HQ_TK.SendChungTuSP(Exception): " & ex.Message, EventLogEntryType.Error)
                        End Try
                    Else
                        'gui chuoi content_ex vào core
                        Try
                            Dim create_cex As New Business.CitadV25

                            Dim core_date As String = cls_corebank.Get_VALUE_DATE()

                            content_ex = create_cex.CreateContentEx(v_strSo_CT, "NTTQ", core_date)

                            content_ex = Business.Common.mdlCommon.EncodeBase64_ContentEx(content_ex)

                            cls_corebank.SendContentEx(v_strSo_CT, rm_ref_no, "NTTQ", content_ex, v_strErrorCode)
                            If v_strErrorCode = "00000" Then
                                'cap nhap send core thanh cong: 1 la thanh cong, 0 la chua thanh cong
                                DataAccess.ExecuteNonQuery("update TCS_CTU_HDR set SEND_CONTENT_EX = '1' where so_ct='" & v_strSo_CT & "'", CommandType.Text)
                            End If

                        Catch ex As Exception
                            'chi ghi log, khong throw, van gui thue binh thuong
                            LogApp.WriteLog("KiemSoatCT_HQ_TK.SendContentEx", ex, "Lỗi ghi chứng từ", HttpContext.Current.Session("TEN_DN"))
                        End Try
                    End If

                Else
                    Mota = " object: " & v_strSo_CT & " result: Fail add_info: " & NewValue
                    LogApp.AddDebug(Action, Mota)
                    lblStatus.Text = "Đã có lỗi xảy ra khi hạch toán chứng từ.Mã lỗi: " & v_arrLogPay(0) & ",mô tả:" & v_arrLogPay(1)
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If

                Dim loai_ct As String = hdfLoai_CT.Value
                'sonmt: cap nhat ngay bao no, bao co
                Dim ngay_bao_no As DateTime = DateTime.Now
                Dim ngay_bao_co As DateTime = DateTime.Now
                Dim ngay_tiep_theo As DateTime = DateTime.Now.AddDays(1)
                Dim strTT_CUTOFFTIME As String = CTuCommon.GetValue_THAMSOHT("TT_CUTOFFTIME")
                If Not strTT_CUTOFFTIME.Equals("2") Then
                    ngay_bao_no = DateTime.Now
                    ngay_bao_co = DateTime.Now
                Else
                    ngay_bao_no = DateTime.Now
                    While (True) 'Kiem tra xem ngay tiep theo co vao ngay nghi hay khong, neu vao ngay nghi thi cong them 1 ngay cho den ngay lam viec tiep theo
                        Dim dtNgayNghi As DataTable = DataAccess.ExecuteReturnDataSet("SELECT ngay_nghi FROM tcs_dm_ngaynghi WHERE ngay_nghi = to_date('" & ngay_tiep_theo.ToString("dd/MM/yyyy") & "', 'dd/MM/yyyy')", CommandType.Text).Tables(0)
                        If dtNgayNghi.Rows.Count > 0 Then

                            Dim ngay_nghi As DateTime = dtNgayNghi.Rows(0)("ngay_nghi")

                            If ngay_nghi.ToString("dd/MM/yyyy").Equals(ngay_tiep_theo.ToString("dd/MM/yyyy")) Then
                                ngay_tiep_theo = ngay_tiep_theo.AddDays(1)
                            Else
                                Exit While
                            End If
                        Else
                            Exit While
                        End If

                    End While

                    ngay_bao_co = ngay_tiep_theo
                End If

                'Cap nhat ngay bao no, ngay bao co vao chung tu
                DataAccess.ExecuteNonQuery("update tcs_ctu_hdr set ngay_baono= to_date('" & ngay_bao_no.ToString("dd/MM/yyyy HH:mm:ss") & "', 'dd/MM/yyyy HH24:MI:SS'), NGAY_BAOCO= to_date('" & ngay_bao_co.ToString("dd/MM/yyyy HH:mm:ss") & "', 'dd/MM/yyyy HH24:MI:SS') where so_ct='" & v_strSo_CT & "'", CommandType.Text)
                Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M47.ToString())
                If strTN_CT <> "" Then
                    pv_so_tn_ct = strTN_CT.Split(";")(0)
                    pv_ngay_tn_ct = strTN_CT.Split(";")(1)
                End If

                If loai_ct.Equals("P") Then
                    'Le phi quan
                    CustomsServiceV3.ProcessMSG.guiCTLePhiHQ(v_strMaNNT, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                    MSGType = TransactionHQ_Type.M41.ToString()
                Else
                    'Thue hai quan
                    CustomsServiceV3.ProcessMSG.guiCTThueHQ(v_strMaNNT, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                    MSGType = TransactionHQ_Type.M47.ToString()
                End If


                'strErrorNum = ""
                'Neu gui thong tin thanh cong
                'strErrorNum = "0"
                If strErrorNum = "0" Then
                    lblStatus.Text = "Chứng từ đã được hạch toán và chuyển số liệu sang Cơ quan Hải quan thành công"
                Else
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ đã được hạch toán. Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                    'cmdKS.Enabled = False
                    cmdChuyenTra.Enabled = False
                    LoadDSCT(ddlTrangThai.SelectedValue)

                End If
                'CAP NHAT TRANG THAI DA CHUYEN THUE

                'Insert du lieu vao bang DOICHIEU_NHHQ
                desMSG_HQ = lblStatus.Text.Trim
                If desMSG_HQ.Length > 400 Then
                    desMSG_HQ = desMSG_HQ.Substring(0, 399)
                End If
                If strErrorNum = "0" Then
                    clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, v_strMa_Dthu, strErrorNum, MSGType, "", pv_so_tn_ct, pv_ngay_tn_ct)
                    hdr.transaction_id = v_strTransaction_id
                    hdr.ten_nh_th = txtTen_NH_B.Text.Trim.ToString()
                    hdr.dien_giai = hdfDienGiai.Value.ToString()
                    ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)

                End If

                ChungTu.buChungTu.InsertMSGLoi(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)

                LoadDSCT(ddlTrangThai.SelectedValue)
                'ShowHideCTRow(True)
                EnabledButtonWhenDoTransaction(False)
                'lblStatus.Text = "Chứng từ đã được hạch toán và chuyển số liệu sang Cơ quan Hải quan thành công"
                cmdChuyenTra.Enabled = False
                'cmdHuy.Enabled = True
                hdfTrangThai_CT.Value = v_strTrangThai_CT
                hdfTrangThai_CThue.Value = IIf(blnSuccess, "1", "0")
            End If

        Catch ex As Exception
            Mota = " object:  result: Fail  add_info: " & NewValue
            LogApp.AddDebug(Action, Mota & ex.Message)
            LogApp.WriteLog("cmdKSBK", ex, "Lỗi Kiểm soát chứng từ", HttpContext.Current.Session("TEN_DN"))
            lblStatus.Text = ex.Message
            Throw ex
        End Try
    End Sub

    Private Function BuildRemarkForPayment(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim str_KTBatDau As String = "B1D "
        Dim str_KTKetThuc As String = "K1T nop thue"
        Dim v_strRetRemark As String = String.Empty
        Dim str_MST As String = "MST" & txtMaNNT.Text.Trim.ToString()
        Dim str_MaLThue As String = "NT" & ddlLoaiThue.SelectedValue.ToString()
        Dim str_NNT As String = "Ngay NT" & txtNGAY_KH_NH.Text.ToString()
        For i = 0 To v_intRowCnt - 1
            v_strRetRemark &= "C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
            v_strRetRemark &= "T" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
            v_strRetRemark &= "S" & VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ",")
            v_strRetRemark &= IIf(i < v_intRowCnt - 1, ";", "")
        Next
        v_strRetRemark &= "TK" & txtToKhaiSo.Text ' Globals.RemoveSign4VietnameseString(v_strMaNHNhan)
        v_strRetRemark &= "N" & txtNgayDK.Text
        v_strRetRemark &= "LH" & txtLHXNK.Text
        If v_intRowCnt > 1 Then
            v_strRetRemark = str_KTBatDau & str_MST & str_MaLThue & str_NNT & v_strRetRemark & str_KTKetThuc
        End If
        intRowcount = v_intRowCnt
        Return v_strRetRemark
    End Function
    Private Function BuildEcomForPayment_V20(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty
        'If v_intRowCnt > 1 Then
        For i = 0 To v_intRowCnt - 1


            'Noi dung kinh te- mã tiểu mục
            v_strRetRemark &= V_Substring(CType(grdChiTiet.Items(i).Controls(10).Controls(1), TextBox).Text, 200)
            'mã chương
            v_strRetRemark &= "~" & V_Substring(CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text, 200)
            'Số tiền
            v_strRetRemark &= "~" & V_Substring(VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(12).Controls(1), TextBox).Text, ",").Replace(",", ""), 200)
            'Nội dung
            v_strRetRemark &= "~" & V_Substring(CType(grdChiTiet.Items(i).Controls(11).Controls(1), TextBox).Text, 200)
            'Số tờ khai
            v_strRetRemark &= "~" & V_Substring(CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text, 30)
            'Ngày tờ khai  //Đinh dạng dd-MM-yyyy
            Dim ngay_tk As String = String.Empty
            If Not txtNgayDK.Text Is Nothing Then
                ngay_tk = DateTime.ParseExact(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MM-yyyy")
            End If

            v_strRetRemark &= "~" & V_Substring(ngay_tk, 12)
            'If i <> 0 Then
            v_strRetRemark &= "####"
            'End If
        Next

        'End If
        v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark)

        intRowcount = v_intRowCnt
        Return v_strRetRemark
    End Function
    Private Function BuildRemarkForPayment_V20(ByRef intRowcount As Integer) As String
        'Thong tin chưa co trong các truong moi
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty

        v_strRetRemark &= "LH" & txtLHXNK.Text 'Loai hinh xuat nhap khau
        v_strRetRemark &= ".DNCT " & txtGhiChu.Text.Trim ' ten kho bac

           
        Return V_Substring(v_strRetRemark, 210)
    End Function

    Private Function BuildDTLCitadXML(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim str_KTBatDau As String = "<VSTD>"
        Dim str_KTKetThuc As String = "</VSTD>"
        Dim v_strRetRemark As String = String.Empty

        'For i = 0 To v_intRowCnt - 1
        '    v_strRetRemark &= str_KTBatDau
        '    v_strRetRemark &= "<MCH>" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text & "</MCH>"
        '    v_strRetRemark &= "<NDK>" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text & "</NDK>"
        '    v_strRetRemark &= "<STN>" & CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text.Replace(",", "") & "</STN>"
        '    v_strRetRemark &= "<NDG>" & Globals.RemoveSign4VietnameseString(CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text) & "</NDG>"
        '    v_strRetRemark &= str_KTKetThuc
        'Next
        'intRowcount = v_intRowCnt
        Return v_strRetRemark
    End Function

    Private Function BuildCKTM() As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty

        'For i = 0 To v_intRowCnt - 1
        '    v_strRetRemark &= "C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
        '    v_strRetRemark &= "T" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
        'Next

        Return v_strRetRemark
    End Function

    Protected Sub cmdHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdHuy.Click
        LogApp.AddDebug("cmdHuy_Click", "KIEM SOAT CHUNG TU HQ: Huy chung tu")
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim i_char As Integer = confirmValue.LastIndexOf(",")
        i_char = i_char + 1
        confirmValue = confirmValue.Substring(i_char, confirmValue.Length - i_char)
        If confirmValue = "Yes" Then
        Else
            Exit Sub
        End If
        Dim v_blnReturn As Boolean = False
        Dim pv_so_tn_ct_ych As String = ""
        Dim pv_so_tn_ct As String = ""
        Dim pv_ngay_tn_ct As String = ""
        Dim loai_ct As String = hdfLoai_CT.Value
        Try

            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)
            cmdHuy.Enabled = False

            'Kiểm tra ngày làm việc
            'If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate2() Then
            '    lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
            '    'EnabledButtonWhenDoTransaction(True)
            '    cmdHuy.Enabled = True
            '    Exit Sub
            'End If

            'Kiểm tra mật khẩu kiểm soát
            Dim strUser = mv_objUser.Ten_DN.ToString
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If Not Business_HQ.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
            '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
            '        cmdHuy.Enabled = True
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi Duyệt hủy."
            '    cmdHuy.Enabled = True
            '    Exit Sub
            'End If

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            Dim v_strMaNNT As String = hdfMaNNT.Value
            Dim v_strTT_ChuyenThue As String = hdfTrangThai_CThue.Value
            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""

            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            Dim strLyDoHuy As String = ""
            strLyDoHuy = txtLyDoHuy.Text.Trim()
            If IsNullOrEmpty(strLyDoHuy) Then
                lblStatus.Text = "Bạn cần nhập lý do hủy!"
                txtLyDoHuy.Focus()
                cmdHuy.Enabled = True
                cmdChuyenTra.Enabled = True
                Exit Sub
            End If
            hdr.ten_kb = Globals.RemoveSign4VietnameseString(txtTenKB.Text)
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu


                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                Dim descERR_HQ As String = ""
                Dim str_TrangThaiCT_moi As String = ""
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    'EnabledButtonWhenDoTransaction(True)
                    cmdHuy.Enabled = True
                    Exit Sub
                End If
                'Gui thong tin Chung tu sang Hai quan
                'If (v_strTrangThai_CT = "02" And v_strTT_ChuyenThue = "1") Then
                'Trang thai 02: la trang thai huy cua chung tu da thanh cong 
                If (v_strTrangThai_CT = "02") Then
                    Dim strNguyenTe As String = txtMaNT.Text.ToString()
                    Dim strTN_CT As String
                    If loai_ct.Equals("P") Then
                        strTN_CT = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M41.ToString())
                    Else
                        strTN_CT = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M47.ToString())
                    End If
                    If strTN_CT <> "" Then
                        pv_so_tn_ct_ych = strTN_CT.Split(";")(0)
                        pv_ngay_tn_ct = strTN_CT.Split(";")(1)
                    End If

                    If loai_ct.Equals("P") Then
                        'Huy phi
                        CustomsServiceV3.ProcessMSG.getMSGHuy(pv_so_tn_ct_ych, "502", strErrorNum, strErrorMes, pv_so_tn_ct, pv_ngay_tn_ct, v_strTransaction_id)
                    Else
                        'Huy thue
                        CustomsServiceV3.ProcessMSG.getMSGHuy(pv_so_tn_ct_ych, "501", strErrorNum, strErrorMes, pv_so_tn_ct, pv_ngay_tn_ct, v_strTransaction_id)
                    End If

                    If strErrorNum = "0" Then
                        'v_strTrangThai_CT = "04" 'Trạng thái đã hủy
                        'str_TrangThaiCT_moi = "0"
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", mv_objUser.Ma_NV, "", "1", strLyDoHuy)

                        'Set lai trang thai khi huy boi kiem soat vien
                        Business_HQ.HaiQuan.HaiQuanController.HuyChungTu(v_strSo_CT, strLyDoHuy, mv_objUser.Ma_NV)

                        LoadDSCT(ddlTrangThai.SelectedValue)
                        cmdHuy.Enabled = False
                        lblStatus.Text = "Hủy và gửi chứng từ sang Hải quan thành công"
                    Else
                        'Huy chung tu loi update trang thai chuyen thue = 0
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", mv_objUser.Ma_NV, "", "0", strLyDoHuy)

                        'Set lai trang thai khi huy boi kiem soat vien
                        Business_HQ.HaiQuan.HaiQuanController.HuyChungTu(v_strSo_CT, strLyDoHuy, mv_objUser.Ma_NV)

                        lblStatus.Text = "Hủy không thành công. Lỗi từ HQ: " & strErrorNum & " - " & strErrorMes
                        cmdHuy.Enabled = True
                        str_TrangThaiCT_moi = "1"
                    End If
                    'Insert du lieu vao bang DOICHIEU_NHHQ
                    clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, v_strMa_Dthu, strErrorNum, TransactionHQ_Type.M51.ToString(), "", pv_so_tn_ct, pv_ngay_tn_ct)
                    'strLyDoHuy = txtLyDoHuy.Text.Trim()
                    hdr.transaction_id = v_strTransaction_id
                    hdr.ly_do_huy = strLyDoHuy
                    descERR_HQ = lblStatus.Text.Trim
                    If (descERR_HQ.Length > 400) Then
                        descERR_HQ = descERR_HQ.Substring(0, 399)
                    End If
                    ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, str_TrangThaiCT_moi, descERR_HQ)
                    ChungTu.buChungTu.InsertMSGLoi(hdr, dtls, str_TrangThaiCT_moi, descERR_HQ)
                Else
                    v_strTrangThai_CT = "04" 'Trạng thái đã hủy
                    ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", mv_objUser.Ma_NV, "", "0", "3")

                    LoadDSCT(ddlTrangThai.SelectedValue)
                    cmdHuy.Enabled = False
                    lblStatus.Text = "Hủy chứng từ chưa chuyển sang Hải quan thành công"
                End If
            End If
            'Load lai chung tu
            LoadDSCT("99")
        Catch ex As Exception
            LogApp.WriteLog("cmdHuy", ex, "Lỗi hủy chứng từ", HttpContext.Current.Session("TEN_DN"))
        Finally
        End Try
    End Sub

    Private Sub EnabledButtonWhenDoTransaction(ByVal blnEnabled As Boolean)
        'cmdKS.Enabled = blnEnabled
        cmdChuyenTra.Enabled = blnEnabled
    End Sub

    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
            Dim v_strRetArgument As String() = btnSelectCT.CommandArgument.ToString.Split(",")
            If (v_strRetArgument.Length > 3) Then
                Dim v_strSHKB As String = v_strRetArgument(0)
                Dim v_strNgay_KB As String = v_strRetArgument(1)
                Dim v_strSo_CT As String = v_strRetArgument(2)
                Dim v_strKyHieu_CT As String = v_strRetArgument(3)
                Dim v_strSo_BT As String = v_strRetArgument(4)
                Dim v_strMa_Dthu As String = v_strRetArgument(5)
                Dim v_strMa_NV As String = v_strRetArgument(6)
                Dim v_strTrangThai_CT As String = v_strRetArgument(7)
                Dim v_strMaNNT As String = v_strRetArgument(8)
                Dim v_strLOAI_CTU As String = v_strRetArgument(9)
                Dim v_strSO_CT_NH As String = v_strRetArgument(10)
                Dim v_strLanClickKS As String = v_strRetArgument(11)
                'Load thông tin chi tiết khi người dùng chọn 1 CT cụ thể
                If ((Not v_strSHKB Is Nothing) And (Not v_strMa_NV Is Nothing) _
                     And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                    Dim key As New KeyCTu
                    key.Ma_Dthu = v_strMa_Dthu
                    key.Kyhieu_CT = v_strKyHieu_CT
                    key.Ma_NV = v_strMa_NV
                    key.Ngay_KB = v_strNgay_KB
                    key.SHKB = v_strSHKB
                    key.So_BT = v_strSo_BT
                    key.So_CT = v_strSo_CT
                    LoadCTChiTietAll(key)

                    'Dat lai gia tri
                    hdfSHKB.Value = v_strSHKB
                    hdfNgay_KB.Value = v_strNgay_KB
                    hdfSo_CT.Value = v_strSo_CT
                    hdfKyHieu_CT.Value = v_strKyHieu_CT
                    hdfSo_BT.Value = v_strSo_BT
                    hdfMa_Dthu.Value = v_strMa_Dthu
                    hdfMa_NV.Value = v_strMa_NV
                    hdfTrangThai_CT.Value = v_strTrangThai_CT
                    hdfClickKS.Value = v_strLanClickKS
                    hdfMaNNT.Value = v_strMaNNT
                    hdfLoai_CT.Value = v_strLOAI_CTU
                    hdfSoCTNH.Value = v_strSO_CT_NH

                End If
                lbSoCT.Text = "Số CT: " & v_strSo_CT
                lbSoCT.Visible = True

                lbSoFT.Text = "Số FT: " & v_strSO_CT_NH
                lbSoFT.Visible = True
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    Protected Sub ddlTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrangThai.SelectedIndexChanged
        LoadDSCT(ddlTrangThai.SelectedValue)
        ClearCTHeader()
        CreateDumpChiTietCTGrid()
        ShowHideCTRow(False)
    End Sub

    Private Function BuildRemarkForPayment(ByVal pv_strTenNNT As String, ByVal pv_strMaNNT As String, _
                                 ByVal v_strMaNHNhan As String, ByVal pv_strNgayNop As String, ByVal pv_strMaNHTT As String) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty
        ' v_strRetRemark &= "MST:" & pv_strMaNNT & ","
        'For i = 0 To v_intRowCnt - 1
        '    v_strRetRemark &= "(C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
        '    v_strRetRemark &= ".TM" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
        '    'v_strRetRemark &= ",KT" & Globals.RemoveSign4VietnameseString(CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text.ToUpper)
        '    v_strRetRemark &= ".ST:" & CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text.Replace(",", "")
        '    v_strRetRemark &= ")"
        '    v_strRetRemark &= IIf(i < v_intRowCnt - 1, "+", "")
        'Next
        'If ddlLoaiThue.SelectedValue = "04" Then
        '    v_strRetRemark &= " .TK:" & txtToKhaiSo.Text ' Globals.RemoveSign4VietnameseString(v_strMaNHNhan)
        '    v_strRetRemark &= " .N:" & txtNgayDK.Text
        '    v_strRetRemark &= " .LH:" & txtLHXNK.Text
        'End If
        Return v_strRetRemark
    End Function

    Protected Sub grdDSCT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDSCT.PageIndexChanging
        Dim v_ds As DataSet = GetDSCT(ddlTrangThai.SelectedValue)
        grdDSCT.PageIndex = e.NewPageIndex
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
    End Sub

    Protected Sub cmdChuyenThue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenThue.Click
        LogApp.AddDebug("cmdChuyenThue_Click", "KIEM SOAT CHUNG TU HQ: Chuyen thue")
        Dim v_blnReturn As Boolean = False
        'Dim strNguyenTe As String = ""
        Try
            Dim strUser = mv_objUser.Ten_DN.ToString
            'Kiểm tra ngày làm việc
            'Kiểm tra mật khẩu kiểm soát
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If Not Business_HQ.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
            '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
            '        EnabledButtonWhenDoTransaction(False)
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi kiểm soát."
            '    EnabledButtonWhenDoTransaction(False)
            '    Exit Sub
            'End If
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)
            cmdChuyenThue.Enabled = False

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            'Neu ma trang thai 19 thi chuyen thanh trang thai 01 de sau do chuyen sang hai quan

            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""
            Dim pv_so_tn_ct_ych As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""
            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            hdr.ten_kb = Globals.RemoveSign4VietnameseString(txtTenKB.Text)
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If
                Dim blnSuccess As Boolean = True
                Dim MSGType As String = ""
                Dim desMSG_HQ As String = ""
                Try
                    Dim loai_ct As String = hdfLoai_CT.Value

                    Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M47.ToString())
                    If strTN_CT <> "" Then
                        pv_so_tn_ct = strTN_CT.Split(";")(0)
                        pv_ngay_tn_ct = strTN_CT.Split(";")(1)
                    End If

                    If loai_ct.Equals("P") Then
                        'Le phi quan
                        CustomsServiceV3.ProcessMSG.guiCTLePhiHQ(txtMaNNT.Text, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                        MSGType = TransactionHQ_Type.M41.ToString()
                    Else
                        'Thue hai quan
                        CustomsServiceV3.ProcessMSG.guiCTThueHQ(txtMaNNT.Text, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                        MSGType = TransactionHQ_Type.M47.ToString()
                    End If

                    ''Neu gui thong tin thanh cong
                    ' strErrorNum = "0"
                    Dim str_TTThue As String = String.Empty
                    If strErrorNum = "0" Then

                        If v_strTrangThai_CT = "01" And v_strTrangThai_CT = "19" Then
                            If blnSuccess = True Then
                                str_TTThue = "1"
                            Else
                                str_TTThue = "0"
                            End If
                            v_strTrangThai_CT = "01"
                        End If

                        If v_strTrangThai_CT = "04" Then
                            If blnSuccess = True Then
                                str_TTThue = "0"
                            Else
                                str_TTThue = "1"
                            End If
                        End If

                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT)
                        'Insert du lieu vao bang DOICHIEU_NHHQ
                        clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, "", strErrorNum, MSGType, "", pv_so_tn_ct, pv_ngay_tn_ct)
                        hdr.transaction_id = v_strTransaction_id
                        hdr.ten_nh_th = txtTen_NH_B.Text.Trim.ToString()
                        ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, str_TTThue)
                        lblStatus.Text = "Chứng từ đã chuyển số liệu sang Cơ quan Hải quan thành công"
                    Else

                        blnSuccess = False
                        lblStatus.Text = "Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                        'ShowHideCTRow(False)

                        If v_strTrangThai_CT = "01" And v_strTrangThai_CT = "19" Then
                            If blnSuccess = True Then
                                str_TTThue = "1"
                            Else
                                str_TTThue = "0"
                            End If
                            v_strTrangThai_CT = "01"
                        End If
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT)
                        cmdChuyenThue.Enabled = True
                    End If
                    desMSG_HQ = lblStatus.Text.Trim
                    If desMSG_HQ.Length > 400 Then
                        desMSG_HQ = desMSG_HQ.Substring(0, 399)
                    End If
                    ChungTu.buChungTu.InsertMSGLoi(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ chưa chuyển được số liệu sang Cơ quan Hải quan. " & ex.Message
                    Exit Sub
                End Try



            End If
            LoadDSCT(ddlTrangThai.SelectedValue)
        Catch ex As Exception
            LogApp.WriteLog("cmdChuyenThue", ex, "Lỗi gửi lại hải quan", HttpContext.Current.Session("TEN_DN"))
        Finally
        End Try
    End Sub

    Protected Sub grdChiTiet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdChiTiet.ItemDataBound

        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            'If Not e.Item.ItemType.ToString() = "Header" Then
            'e.Item.Cells(7).Text = VBOracleLib.Globals.Format_Number(e.Item.Cells(7).Text, ",")
            Dim SOTIEN As TextBox = CType(e.Item.FindControl("SOTIEN"), TextBox)
            SOTIEN.Text = VBOracleLib.Globals.Format_Number(SOTIEN.Text, ",")
            Dim SOTIEN_NT As TextBox = CType(e.Item.FindControl("SOTIEN_NT"), TextBox)
            SOTIEN_NT.Text = VBOracleLib.Globals.Format_Number2(SOTIEN_NT.Text)
        End If
    End Sub

    Protected Sub cmdChuyenBDS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenBDS.Click
        LogApp.AddDebug("cmdChuyenBDS_Click", "KIEM SOAT CHUNG TU HQ: Chuyen BDS")
        Dim v_blnReturn As Boolean = False

        Try
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)

            'Kiểm tra ngày làm việc
            'If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
            '    lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
            '    EnabledButtonWhenDoTransaction(True)
            '    Exit Sub
            'End If

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If
                Dim strTongTienTT As String = txtTTIEN.Text.Replace(",", "")
                Dim v_strAmt As String = strTongTienTT 'txtTongTrichNo.Text.Replace(",", "") 'strTongTienTT
                Dim teller_id As String = clsCTU.Get_TellerID(v_strMa_NV) 'giao dich vien
                Dim approver_id As String = mv_objUser.TELLER_ID 'Kiem soat vien

                Dim v_strHostName As String = String.Empty
                Dim v_strDBName As String = String.Empty
                Dim v_strDBUser As String = String.Empty
                Dim v_strDBPass As String = String.Empty
                Dim v_strHost_REFNo As String = String.Empty
                Dim v_strBDS_REFNo As String = String.Empty

                Dim strErrorNum As String = String.Empty
                Dim strErrorMes As String = String.Empty
                Dim v_strTransaction_id As String = String.Empty

                Dim hdr As New infDoiChieuNHHQ_HDR
                Dim dtls() As infDoiChieuNHHQ_DTL

                Dim v_total_ID As String = "924"
                lblStatus.Text = "Không thể lấy tham số của BDS hãy kiểm tra lại."
                If (clsCTU.TCS_GetThamSoBDS(mv_objUser.MA_CN, v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)) Then
                    Dim v_objBDS As New BDSHelper(v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)
                    If v_objBDS.IncreaseTotal(v_total_ID, teller_id, v_strAmt) Then
                        'Cập nhật lại trạng thái của chứng từ thành đã kiểm soát
                        v_strTrangThai_CT = "01" 'Trạng thái đã kiểm soát
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
                    Else
                        lblStatus.Text = "Không thể cập nhật số tiền vào BDS.Hãy kiểm tra lại"
                        EnabledButtonWhenDoTransaction(True)
                        Exit Sub
                    End If
                Else
                    lblStatus.Text = "Không thể lấy tham số của BDS hãy kiểm tra lại."
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If


                lblStatus.Text = "Kiểm soát chứng từ thành công.Xin chờ một lát để gửi thông tin sang thuế"

                'Chuyen thong tin sang thue
                Dim blnSuccess As Boolean = True
                Try

                    Dim loai_ct As String = hdfLoai_CT.Value
                    Dim v_strMaNNT As String = hdfMaNNT.Value
                    Dim MSGType As String = ""
                    Dim pv_so_tn_ct As String = ""
                    Dim pv_ngay_tn_ct As String = ""

                    Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M47.ToString())
                    If strTN_CT <> "" Then
                        pv_so_tn_ct = strTN_CT.Split(";")(0)
                        pv_ngay_tn_ct = strTN_CT.Split(";")(1)
                    End If


                    If loai_ct.Equals("P") Then
                        'Le phi quan
                        CustomsServiceV3.ProcessMSG.guiCTLePhiHQ(v_strMaNNT, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                        MSGType = TransactionHQ_Type.M41.ToString()
                    Else
                        'Thue hai quan
                        CustomsServiceV3.ProcessMSG.guiCTThueHQ(v_strMaNNT, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                        MSGType = TransactionHQ_Type.M47.ToString()
                    End If

                    'Neu gui thong tin thanh cong
                    If strErrorNum = "0" Then
                        lblStatus.Text = "Chứng từ đã được cập nhật BDS và chuyển số liệu sang Cơ quan Hải quan thành công"
                    Else
                        blnSuccess = False
                        lblStatus.Text = "Chứng từ đã được cập nhật BDS. Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                        Exit Sub
                    End If
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ đã được cập nhật thông tin vào BDS nhưng chưa chuyển được số liệu sang Cơ quan Thuế"
                    Exit Sub
                End Try

                'CAP NHAT TRANG THAI DA CHUYEN THUE
                v_strTrangThai_CT = "01" 'Trạng thái đã ks
                ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT)

                'Insert du lieu vao bang DOICHIEU_NHHQ
                clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, v_strMa_Dthu, strErrorNum, TransactionHQ_Type.M41.ToString())
                hdr.transaction_id = v_strTransaction_id
                ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, IIf(blnSuccess, "1", "0"))

                LoadDSCT(ddlTrangThai.SelectedValue)
                EnabledButtonWhenDoTransaction(True)

            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình kiểm soát chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            ' Throw ex
        Finally
        End Try
    End Sub

    Protected Sub ddlMacker_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMacker.SelectedIndexChanged

        LoadDSCT(ddlTrangThai.SelectedValue)
        ClearCTHeader()
        CreateDumpChiTietCTGrid()
        ShowHideCTRow(False)

    End Sub

    Protected Sub cmdRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        LogApp.AddDebug("cmdRefresh_Click", "KIEM SOAT CHUNG TU HQ: Lam moi danh sach chung tu")
        LoadDSCT(ddlTrangThai.SelectedValue)
    End Sub

    Protected Sub cmdDieuChinh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDieuChinh.Click
        Dim strNguyenTe As String = ""
        Dim v_strSHKB As String = hdfSHKB.Value
        Dim v_strNgay_KB As String = hdfNgay_KB.Value
        Dim v_strSo_CT As String = hdfSo_CT.Value
        Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
        Dim v_strSo_BT As String = hdfSo_BT.Value
        Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
        Dim v_strMa_NV As String = hdfMa_NV.Value
        Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
        Dim v_strMaNNT As String = hdfMaNNT.Value
        Dim strErrorNum As String = ""
        Dim strErrorMes As String = ""
        Dim pv_so_tn_ct As String = ""
        Dim pv_ngay_tn_ct As String = ""
        Dim v_strTransaction_id As String = String.Empty
        Dim blnSuccess As Boolean = True
        Dim desMSG_HQ As String = ""
        Dim hdr As New infDoiChieuNHHQ_HDR
        Dim dtls() As infDoiChieuNHHQ_DTL
        Try
            If strNguyenTe = "VND" Then
                CustomsServiceV3.ProcessMSG.getMSG24(v_strMaNNT, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, v_strMa_Dthu, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
            Else
                CustomsServiceV3.ProcessMSG.getMSG25(v_strMaNNT, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, v_strMa_Dthu, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
            End If
            If strErrorNum = "0" Then
                lblStatus.Text = "Điều chỉnh và chuyển số liệu sang Cơ quan Hải quan thành công"
            Else
                blnSuccess = False
                lblStatus.Text = "Điều chỉnh không thành công. Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                LoadDSCT(ddlTrangThai.SelectedValue)
            End If
        Catch ex As Exception
            blnSuccess = False
            lblStatus.Text = "Điều chỉnh không thành công. Có lỗi khi gửi số liệu sang Hải Quan. Mô tả: " & ex.ToString
            'cmdKS.Enabled = False
            cmdChuyenTra.Enabled = False
            LoadDSCT(ddlTrangThai.SelectedValue)
            Exit Sub
        End Try
        desMSG_HQ = lblStatus.Text.Trim
        If desMSG_HQ.Length > 400 Then
            desMSG_HQ = desMSG_HQ.Substring(0, 399)
        End If
        If strErrorNum = "0" Then
            clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, v_strMa_Dthu, strErrorNum, TransactionHQ_Type.M41.ToString(), "", pv_so_tn_ct, pv_ngay_tn_ct)
            hdr.transaction_id = v_strTransaction_id
            hdr.ten_nh_th = txtTen_NH_B.Text.Trim.ToString()
            ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)
        End If

        ChungTu.buChungTu.InsertMSGLoi(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)
        LoadDSCT(ddlTrangThai.SelectedValue)
        ShowHideCTRow(True)
        EnabledButtonWhenDoTransaction(False)
    End Sub

    Protected Sub btnIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIn.Click
        LogApp.AddDebug("cmdInCT_Click", "In GNT Thue hai quan")
        Try
            TCS_IN_GNT_HQ()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình in giấy nộp tiền thuế, lệ phí bộ ngành!")
        Finally
            btnIn.Enabled = True
        End Try
    End Sub

    Private Sub TCS_IN_GNT_HQ()
        Try
            If hdfSo_CT.Value.Length > 0 Then
                '   clsCommon.OpenNewWindow(Me, "../BaoCao/frmInGNT_HQ.aspx?so_ct=" & hdfSo_CT.Value, "ManPower")
                clsCommon.OpenNewWindow(Me, "../BaoCao/InGNTpdf_HQ.ashx?so_ct=" & hdfSo_CT.Value, "ManPower")
                'Response.Redirect("../BaoCao/frmInGNT_HQ.aspx?so_ct=" & hdfSo_CT.Value)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function BuildRemark2ForPayment(ByVal pv_strMaNNT As String) As String
        Dim strLoaiThue As String = ddlLoaiThue.SelectedValue.ToString().Substring(0, 2)
        Dim v_strRetRemark As String = String.Empty

        v_strRetRemark = pv_strMaNNT + "!" + txtMaCQThu.Text.ToString() + "!" + ddlLoaiThue.SelectedValue + "!("
        If ddlLoaiThue.SelectedValue = "04" Then
            v_strRetRemark &= txtToKhaiSo.Text ' Globals.RemoveSign4VietnameseString(v_strMaNHNhan)
            v_strRetRemark &= "/" & Business.Common.mdlCommon.ConvertDateToNumber(txtNgayDK.Text)
            v_strRetRemark &= "/" & txtLHXNK.Text
        End If
        v_strRetRemark = v_strRetRemark + ")"
        Return v_strRetRemark
    End Function

    Private Function BuildRemarkForPaymentCITAD20(ByVal pv_strTenNNT As String, ByVal pv_strMaNNT As String, _
                                  ByVal v_strMaNHNhan As String, ByVal pv_strNgayNop As String, ByVal pv_strMaNHTT As String) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty

        v_strRetRemark &= "MST:" & pv_strMaNNT & ","
        For i = 0 To v_intRowCnt - 1
            'SoTK 1
            v_strRetRemark &= "TK" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
            v_strRetRemark &= ",N" & CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text
            v_strRetRemark &= ",LH" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
            v_strRetRemark &= "(C" & CType(grdChiTiet.Items(i).Controls(7).Controls(1), TextBox).Text
            v_strRetRemark &= ",TM" & CType(grdChiTiet.Items(i).Controls(9).Controls(1), TextBox).Text
            v_strRetRemark &= ",ST:" & CType(grdChiTiet.Items(i).Controls(11).Controls(1), TextBox).Text.Replace(",", "")
            v_strRetRemark &= ");"
        Next

        v_strRetRemark &= "CT:" & txtTenKB.Text.Trim
        v_strRetRemark &= ".TAI:" & txtTen_NH_B.Text.Trim

        Return v_strRetRemark
    End Function

    'Get money customer
    Public Shared Function GetTK_KH_NH(ByVal pv_strTKNH As String) As Decimal
        Dim sRet As Decimal = 0
        Try
            Dim strXML As String = String.Empty
            Dim ma_loi As String = ""
            Dim so_du As String = ""
            Dim branch_tk As String = ""
            strXML = cls_corebank.GetTK_KH_NH(pv_strTKNH.Replace("-", ""), ma_loi, so_du, branch_tk)

            Dim doc As New XmlDocument()
            doc.LoadXml(strXML)
            Dim xnList As XmlNodeList = doc.SelectNodes("/PARAMETER")

            For Each itemNode As XmlNode In xnList
                Dim nodeParams As XmlNode = itemNode.SelectSingleNode("PARAMS")
                If nodeParams IsNot Nothing Then
                    Decimal.TryParse(nodeParams("ACY_AVL_BAL").InnerText.Replace(".", "").Replace(",", ""), sRet)
                End If
            Next

        Catch ex As Exception
            sRet = 0
            Throw ex
        End Try

        Return sRet
    End Function
    Public Shared Function V_Substring(ByVal pv_s As String, ByVal length As Integer) As String
        If Not pv_s Is Nothing And pv_s.Length > length Then
            pv_s = pv_s.Substring(0, length)
        End If
        Return pv_s
    End Function
End Class
