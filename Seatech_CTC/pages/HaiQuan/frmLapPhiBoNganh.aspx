﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmLapPhiBoNganh.aspx.vb" Inherits="pages_HaiQuan_frmLapPhiBoNganh"
    Title="Lập chứng từ phí bộ ban ngành" %>

<%@ Import Namespace="System.Web.Services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #table_Id
        {
            width: auto;
        }
        #table_Id thead
        {
        }
        #table_Id td
        {
            width: 10em;
            padding: 0.3em;
        }
        #table_Id tbody
        {
            background: #ccc;
        }
        div #pager
        {
            border-bottom: solid 1px #e4e5d7;
            background-color: #e4e5d7;
            font-size: 8pt;
            line-height: 20px;
            font-family: Arial,Verdana, Helvetica, sans-serif;
            font-size: 8pt;
            padding-left: 5px;
            padding-right: 5px;
            height: 20px;
        }
        div #pager span
        {
        }
        div #pager span.active
        {
            text-decoration: underline;
            cursor: auto;
            color: -webkit-link;
            padding-left: 5px;
            padding-right: 5px;
        }
        .immybox.immybox_witharrow
        {
            background-image: url(immybox-arrow.png);
        }
        .style1
        {
            height: 28px;
        }
    </style>

    <script type="text/javascript" src="../../javascript/jquery/1.11.2/jquery.min.js"></script>

    <link rel="Stylesheet" href="../../javascript/autocomplete/easy-autocomplete.min.css" />
    <link rel="Stylesheet" href="../../javascript/autocomplete/easy-autocomplete.themes.min.css" />

    <script language="javascript" src="../../javascript/ChuyenSoSangChu.js" type="text/javascript"></script>

    <script type="text/javascript" src="../../javascript/autocomplete/jquery.easy-autocomplete.js"></script>

    <script type="text/javascript" src="../../javascript/autocomplete/showModalDialog.js"></script>

    <script src="../../javascript/accounting.min.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top">
            <td align="center" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr height="12px">
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td width='100%' align="center" valign="top" style="padding-left: 0; padding-right: 0">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" valign="top">
                                        <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td valign="top" width="20%" height="700px">
                                                    <table id="table_Id" class="paginated" width="97%" style="border: 1px solid; margin: 0;
                                                        padding: 0; border-collapse: collapse;">
                                                        <thead>
                                                            <tr class="grid_header">
                                                                <td>
                                                                    TT
                                                                </td>
                                                                <td>
                                                                    Chứng từ
                                                                </td>
                                                                <td>
                                                                    User
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tbodyListCT">
                                                            <tr>
                                                                <td align="center">
                                                                    <b></b>
                                                                </td>
                                                                <td>
                                                                    <b>
                                                                        <label id="">
                                                                        </label>
                                                                    </b>
                                                                </td>
                                                                <td>
                                                                    <b></b>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td style="border-bottom: solid 1px #000000; border-right: solid 1px #000000; width: 10%;
                                                                    background-color: White">
                                                                </td>
                                                                <td align="center" style="border-bottom: solid 1px #000000; border-right: solid 1px #000000;
                                                                    background-color: White">
                                                                    <span>Tổng số CT</span>
                                                                </td>
                                                                <td id="tongCT" style="border-bottom: solid 1px #000000; border-right: solid 1px #000000;
                                                                    background-color: White">
                                                                </td>
                                                            </tr>
                                                            <tr style="border-bottom: solid 1px #e4e5d7;">
                                                                <td id="paging" align="left">
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <br />
                                                    <br />
                                                    <table width="100%">
                                                        <tr>
                                                            <td colspan="1">
                                                                Trạng thái chứng từ
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td>
                                                                <select id="ddlTrangThaiCT" class="inputflat">
                                                                    <option value="-1">Tất cả </option>
                                                                    <option value="05">Chờ kiểm soát </option>
                                                                    <option value="01">Đã kiểm soát </option>
                                                                    <%--<option value="00">Đã hoàn thiện </option>--%>
                                                                    <option value="03">Chuyển trả</option>
                                                                    <option value="04">Đã hủy</option>
                                                                    <%--<option value="02">Hủy bởi GDV chờ duyệt</option>--%>
                                                                    <option value="06">Chuyển thuế lỗi</option>
                                                                    <!--If Chuyển Thuê = 1 và chứng từ 1 thì cập nhật 07 nếu CT=0 và CTừ=1 cập nhật 08-->
                                                                    <%--<option value="07">Đã hủy bởi KSV </option>--%>
                                                                    <%--<option value="08">Duyệt hủy chuyển thuế lỗi </option>--%>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <table width="100%">
                                                        <tr style="display:none;">
                                                            <td style="width: 10%">
                                                                <img src="../../images/icons/ChuaKS.png" />
                                                            </td>
                                                            <td style="width: 90%" colspan="2">
                                                                Đã hoàn thiện
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%">
                                                                <img src="../../images/icons/ChuyenKS.png" />
                                                            </td>
                                                            <td style="width: 90%" colspan="2">
                                                                Chờ Kiểm soát
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%">
                                                                <img src="../../images/icons/DaKS.png" />
                                                            </td>
                                                            <td style="width: 90%" colspan="2">
                                                                Đã Kiểm soát
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%">
                                                                <img src="../../images/icons/KSLoi.png" />
                                                            </td>
                                                            <td style="width: 90%" colspan="2">
                                                                Chuyển trả
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%">
                                                                <img src="../../images/icons/Huy.png" />
                                                            </td>
                                                            <td style="width: 90%" colspan="2">
                                                                CT bị Hủy
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%">
                                                                <img src="../../images/icons/TT_CThueLoi.gif" />
                                                            </td>
                                                            <td style="width: 90%" colspan="2">
                                                                Chuyển thuế/HQ lỗi
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" style="height: auto; width: 70%;">
                                                    <table border='0' cellpadding='0' cellspacing='0' id="tableContent">
                                                        <tr>
                                                            <td colspan="2" width='100%' align="left">
                                                                <table width="100%" cellpadding="2" cellspacing="0" style="border: 1px solid; margin: 0;
                                                                    padding: 0; border-collapse: collapse;">
                                                                    <tr class="grid_header">
                                                                        <td align="left" colspan="4">
                                                                            TRUY VẤN CHỨNG TỪ HẢI QUAN
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" align="left" valign="top" class="style1">
                                                                            <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                                                                <tr>
                                                                                    <td style="width: 15%" align="left" valign="top" class="form_label">
                                                                                        <b>Số hồ sơ</b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="form_control" style="width: 30%">
                                                                                        <input type="text" id="textBoxSearchSo_Hoso" class="inputflat" maxlength="15" style="width: 95%;" />
                                                                                    </td>
                                                                                    <td align="left" style="width: 20%" valign="top" class="form_label">
                                                                                        <b>Mã đơn vị quản lý</b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="form_control" style="width: 30%">
                                                                                        <input type="text" id="textBoxSearchMa_DVQL" class="inputflat" maxlength="14" style="width: 95%;" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 20%" align="left" valign="top" class="form_label">
                                                                                        <b>Ký hiệu chứng từ</b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td style="width: 30%;" class="form_control">
                                                                                        <input type="text" id="textBoxSearchKyHieu_CT" class="inputflat" maxlength="10" style="width: 95%;" />
                                                                                    </td>
                                                                                    <td style="width: 15%" align="left" valign="top" class="form_label">
                                                                                        <b>Số chứng từ</b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td style="width: 30%;" class="form_control">
                                                                                        <input type="text" id="textBoxSearchSo_CT" class="inputflat" maxlength="10" style="width: 95%;" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_label">
                                                                                        <b>Năm chứng từ</b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_control">
                                                                                        <%--<input type="text" id='txtTest' runat="server" class="inputflat" />--%>
                                                                                        <input type="text" id="textBoxSearchNam_CT" class="inputflat" maxlength="4" style="width: 95%;" />
                                                                                    </td>
                                                                                    <td align="right" colspan="2" valign="top" class="form_control" style="height: 18px">
                                                                                        <%--<input type="text" id='txtTest' runat="server" class="inputflat" />--%>
                                                                                        <input type="button" id="btnTruyVanHQ" value="Truy vấn hải quan" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                                    <tr>
                                                                        <td colspan="2" width='100%' align="left">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr class="grid_header">
                                                                                    <td align="left">
                                                                                        TRUY VẤN CHỨNG TỪ LỆ PHÍ <span id="divSo_CT"></span>
                                                                                    </td>
                                                                                    <td height='15' align="right">
                                                                                        Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <div id="Div5" style="height: auto; width: 100%; overflow: scroll;">
                                                                                <table id="Table1" cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px;
                                                                                    border-style: solid; font-family: Verdana; font-size: 8pt; width: 99%; border-collapse: collapse;">
                                                                                    <tr id="rowMaNNT">
                                                                                        <td width="24%">
                                                                                            <b>Số hồ sơ</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td width="35%">
                                                                                            <input type="text" id="So_HS" class="inputflat" style="width: 90%" readonly="readonly" />
                                                                                        </td>
                                                                                        <td width="35%" style="height: 25px">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Mã/ Tên DVQL</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" class="inputflat" id="Ma_DVQL" style="width: 90%" readonly="readonly" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" class="inputflat" id="Ten_DVQL" style="width: 90%" readonly="readonly" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Mã/ Tên NH chuyển</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td width="35%">
                                                                                            <select id="ddl_NganHang" class="inputflat" style="max-width: 90%">
                                                                                            </select>
                                                                                        </td>
                                                                                        <td width="35%">
                                                                                            <input type="text" id="Ma_NH_PH" class="inputflat" style="width: 90%" readonly="readonly" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Ký hiệu CT</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input id="KyHieu_CT" class="inputflat" style="width: 90%" readonly="readonly" />
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <b><i>Thông tin người nộp tiền</i></b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Số/ Năm CT</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input id="So_CT" class="inputflat" readonly="readonly" style="width: 90%" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="Nam_CT" class="inputflat" style="width: 90%" readonly="readonly" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>MST/ Tên NNT</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="MST" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="Ten_NNT" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Quận/ Huyện</b>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="NNT_QUAN_HUYEN" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="NNT_TINH_TP" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Địa chỉ</b>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="DiaChi" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="TT_Khac" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Mã NT/Tỷ giá</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="Ma_NT" class="inputflat" style="width: 95%" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="TyGia" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <b><i>Tài khoản chuyển tiền</i></b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Ngày lập chứng từ</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="NgayLap_CT" readonly="readonly" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Hình thức CT</b>
                                                                                        </td>
                                                                                        <td>
                                                                                            <select id="PTTT" class="inputflat" style="width: 90%">
                                                                                                <option value="1">Chuyển khoản </option>
                                                                                                <%--<option value="3">Nộp qua GL</option>--%>
                                                                                            </select>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="tableRowTienMat" style="display: none">
                                                                                        <td class="style1">
                                                                                            <b>Số CMND/ Số ĐT</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td class="style1">
                                                                                            <input type="text" id="TT_NT_So_CMND" maxlength="12" class="inputflat" style="width: 90%"
                                                                                                onkeypress="return isNumber(event);" />
                                                                                        </td>
                                                                                        <td class="style1">
                                                                                            <input type="text" id="TT_NT_So_DT" maxlength="11" class="inputflat" style="width: 90%"
                                                                                                onkeypress="return isNumber(event);" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="rowTKGL">
                                                                                        <td>
                                                                                            <b>TK GL </b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="txtTKGL" maxlength="12" class="inputflat" style="width: 99%;"
                                                                                                onblur="jsGet_TenTKGL();" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="txtTenTKGL" class="inputflat" style="width: 92%;" disabled="disabled"
                                                                                                readonly="readonly" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="Tr1" style="display: none">
                                                                                        <td>
                                                                                            <b>Địa chỉ thanh toán</b>
                                                                                        </td>
                                                                                        <td colspan="2">
                                                                                            <input type="text" id="TK_Thanh_Toan_DiaChi" class="inputflat" style="width: 96%" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="tableRowQuanHuyen" style="display: none">
                                                                                        <td>
                                                                                            <b>Quận(Huyện) / Tỉnh (Thành phố)</b>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="TT_NT_TK_THANH_TOAN_QUAN_HUYEN" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="TT_NT_TK_THANH_TOAN_TINH_TP" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="tableRowTK" style="display: none">
                                                                                        <td>
                                                                                            <b>TKC/ Tên TK</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="TaiKhoan_Chuyen" class="inputflat" style="width: 90%" onblur="jsGetTK_KH_NH(); jsGetTenTK_KH_NH();"
                                                                                                maxlength="18" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="Ten_TaiKhoan_Chuyen" class="inputflat" style="width: 90%" />
                                                                                            <img id="imgSignature" alt="Hiển thị chữ ký khách hàng" src="../../images/icons/MaDB.png"
                                                                                                onclick="jsShowCustomerSignature();" style="width: 16px; height: 16px" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="tableRowSoDu" style="display: none">
                                                                                        <td>
                                                                                            <b>Số dư</b>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="So_Du" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <b><i>Thông tin nộp tiền</i></b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Mã/ Tên NH</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="Ma_NH_TH" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="Ten_NH_TH" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr >
                                                                                        <td>
                                                                                            <b>Mã/ Tên TK TH</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="TaiKhoan_TH" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="Ten_TaiKhoan_TH" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="rowCheckTaiKhoanNoiBo">
                                                                                        <td>
                                                                                            <b>Mã/ Tên NH TT</b><span class="requiredField">(*)</span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="txtTaiKhoan_TT" class="inputflat" style="width: 90%; background: Aqua" onkeypress="if (event.keyCode==13){ShowLov('DMNH_TT');}" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="txtTen_TaiKhoan_TT" class="inputflat" style="width: 90%" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan='2' height='2'>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan='2' align="left" class="errorMessage">
                                                                            <table border="0" width="100%">
                                                                                <tr id="rowProgress">
                                                                                    <td colspan="2" align="left">
                                                                                        <div style="background-color: Aqua; display: none;" id="divProgress">
                                                                                            <b style="font-weight: bold; font-size: 15pt">Đang xử lý. Xin chờ một lát...</b>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <div id="divStatus" style="font-weight: bold; width: 100%;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" width="100%">
                                                                            <div id="accordion" style="height: 100%">
                                                                                <h3 class="grid_header" style="margin-bottom: 0; text-align: left; padding-left: 5px;">
                                                                                    Thông tin chi tiết chứng từ</h3>
                                                                                <div style="padding: 0px; margin: 0px; height: 200px; min-height: 0px; overflow: scroll;
                                                                                    overflow-x: hidden">
                                                                                    <table id="tableData" class="grid_data" style="table-layout: fixed; width: 100%"
                                                                                        border="1" cellspacing="0">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th class="grid_header" style="width: 5%; text-align: center;">
                                                                                                    STT
                                                                                                </th>
                                                                                                <th class="grid_header" style="width: 5%; text-align: center;">
                                                                                                    Chọn
                                                                                                </th>
                                                                                                <th class="grid_header" style="width: 12%">
                                                                                                    NDKT
                                                                                                </th>
                                                                                                <th class="grid_header" style="width: 40%">
                                                                                                    Nội dung
                                                                                                </th>
                                                                                                <th class="grid_header">
                                                                                                    Số tiền
                                                                                                </th>
                                                                                                <th class="grid_header">
                                                                                                    Số tiền NT
                                                                                                </th>
                                                                                                <th class="grid_header">
                                                                                                    Ghi chú
                                                                                                </th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody id="divDraw" style="width: 100%">
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan='2' height='5'>
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="20%">
                                                                            <b>Lý do hủy</b><span class="requiredField">(*)</span>
                                                                        </td>
                                                                        <td width="80%">
                                                                            <input type="text" id="txtLyDoHuy" class="inputflat" style="width: 98%" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <br />
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="2">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td colspan="8">
                                                                                        <input type="hidden" id="hdnSoCT_NH" />
                                                                                        <input type="hidden" id="hdnSoLan_LapCT" />
                                                                                        <input type="hidden" id="hdnTongTienCu" value="0" />
                                                                                        <input type="hidden" id="hdnMA_KS" />
                                                                                        <input type="hidden" id="hdnSO_CT" />
                                                                                        <input type="hidden" id="hdnMA_NTK" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        <table width="300px">
                                                                                            <tr align="right">
                                                                                                <td align="right" valign="top">
                                                                                                    Tổng Tiền: &nbsp;
                                                                                                    <input type="text" id="txtTongTien" style="text-align: right; width: 150px; background-color: Yellow;
                                                                                                        font-weight: bold" class="inputflat" value="0" readonly disabled />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr align="right">
                                                                                                <td align="right" valign="top">
                                                                                                    Tổng Tiền NT: &nbsp;
                                                                                                    <input type="text" id="TongTien_NT" class="inputflat" readonly="readonly" value="0"
                                                                                                        style="width: 150px; text-align: right" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right" colspan="2">
                                                                                        <table width="600px">
                                                                                            <tr>
                                                                                                <td style="display: none;">
                                                                                                    Phí mặc định
                                                                                                </td>
                                                                                                <td style="display: none;">
                                                                                                    <input type="checkbox" id="chkChargeType" checked onclick="jsEnabledInputCharge();" />
                                                                                                </td>
                                                                                                <td style="display: none;">
                                                                                                    Phí
                                                                                                </td>
                                                                                                <td style="display: none;">
                                                                                                    <input type="text" id="txtCharge" style="text-align: right; width: 100px; background-color: Yellow;
                                                                                                        font-weight: bold" class="inputflat" value="0" disabled="disabled" onfocus="this.select()"
                                                                                                        onkeyup="valInteger(this)" onblur="jsCalCharge();" />
                                                                                                </td>
                                                                                                <td style="display: none;">
                                                                                                    VAT:
                                                                                                </td>
                                                                                                <td style="display: none;">
                                                                                                    <input type="text" id="txtVAT" style="text-align: right; width: 100px; background-color: Yellow;
                                                                                                        font-weight: bold" class="inputflat" value="0" readonly disabled="disabled" />
                                                                                                </td>
                                                                                                <td style="text-align: right;">
                                                                                                    Tổng trích nợ:
                                                                                                </td>
                                                                                                <td style="text-align: right; width: 160px;">
                                                                                                    <input type="text" id="txtTongTrichNo" style="text-align: right; width: 150px; background-color: Yellow;
                                                                                                        font-weight: bold" class="inputflat" value="0" readonly disabled="disabled" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan='2' height='5'>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="right">
                                                                <table>
                                                                    <tr align="right" style="display: none">
                                                                        <td style="" align="left">
                                                                            <b>Tổng tiền VNĐ</b>
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="TongTien_VND" class="inputflat" readonly="readonly" style="width: 100%;
                                                                                text-align: right" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr align="right">
                                                                        <td style="" align="left">
                                                                            <b>Bằng chữ: </b>
                                                                        </td>
                                                                        <td>
                                                                            <label id="labelMoneyByText" style="font-weight: bold">
                                                                            </label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <br />
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2' height='5'>
                                                                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                                    <tr>
                                                                        <td width='100%' align="center" valign="top" colspan="2">
                                                                            <input type="button" id="btnThemMoi" value="Lập mới" class="ButtonCommand" />
                                                                            &nbsp;
                                                                            <input type="button" id="btnLuu" value="Lưu" style="display:none;" class="ButtonCommand" />
                                                                            &nbsp;
                                                                            <input type="button" id="btnGhiKS" value="Ghi & chuyển KS" class="ButtonCommand" />
                                                                            &nbsp; &nbsp;
                                                                            <input type="button" id="btnInCT" value="In CT" class="ButtonCommand" />
                                                                            &nbsp; &nbsp;
                                                                            <input type="button" id="btnHuy" value="Hủy" class="ButtonCommand" />
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
        
         var charNumberLimit= 170; 
        var feeRate= 2;
        var minFee= 10000;    
        var charNumber=255;//200;
        var charRemark=5;
        var invalidCharacter ='';
        var temp_arrDBHC;
        var temp_arrSHKB;
        var temp_arrCQThu;
        var temp_arrTKNo;
        var temp_arrTKCo;
        var arrCQThu_SHKB;
        var defMaVangLai2='';

        var currentList =[];
        var listAdded =[];
        var listBankTransfer;
        var countriesArray =[];
        var enablePrint = false;
        var currentSoCT = '0';
        var currentCode = '-1';
        var type =0;
        var chungTuTempArray =[];
        var lydo_huy="";
        var listNDKT = [];
        var tongTienVND = 0;
        var tongTienNT = 0;
        
         $("#btnTruyVanHQ").click(function(){
            if(checkTruyVanValidate()){
                //resetAttribute();
                buttonState("-1");
                TruyVanThongTin();
                $("#Ma_NH_PH").val( $("#ddl_NganHang option:first-child").val());                
            }

        });
        
        $("#btnHuy").click(function(){
                   
                if(checkHuyCT(currentSoCT))
                {
                    if(currentCode=="05")
                    {
                        type=4;
                    }
                    if(currentCode=="03")
                    {
                        type=4;
                    }
                    if(currentCode=="01")
                    {                       
                        type=4;
                    }
                    if(currentCode=="02")
                    {
                        type=4;
                    }
                    
                    if(currentCode=="00")
                    {
                        type=4;
                    }
                    
                   
                }
                 buttonState(currentCode);
                 updateChungTu(type,lydo_huy);
                    
         });
         
         $("#btnThemMoi").click(function(){              
             resetAttribute();             
         });
         
         $("#btnGhiKS").click(function(){           
            if(currentCode=="-1")
            {
                if(createCT("05"))
                {
                    setTimeout(function(){
                      $('#btnGhiKS').prop("disabled", 'disabled');
                    }, 10);
                }
            }
                        
            if(currentCode=="00")
                type =1;
                
            if(currentCode=="05")
                type=1;
            
            if(currentCode=="03")
                type =2;
            
            buttonState(currentCode);            
            updateChungTu(type,"");
         });
         
        
        $("#btnLuu").click(function()
        {
           
            if(currentCode=="03")
            {
                type=1;
                buttonState(currentCode);
                currentCode=="00";
                updateChungTu(type,lydo_huy);                    
            }
            else
            {
                if(createCT("00"))
                {
                    setTimeout(function(){
                        $('#btnLuu').prop("disabled", 'disabled');
                    }, 10);
                  
                }
                
                buttonState(currentCode);                
                updateChungTu(type,"");      
            }
        });
        
        $("#btnInCT").click(function(){
          
            if(currentSoCT =='0')
            {
                return false;    
            }
            else
            {            
                var url= "../BaoCao/frmInGNT_PHI.aspx?so_ct="+currentSoCT;
                window.open(url, 'winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');
            }
        });
        
        function getCurrentDate()
        {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!

            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 
            var today = dd+'/'+mm+'/'+yyyy;
            return today;
        }
        
        $("#ddl_NganHang").change(function(){      
            $("#Ma_NH_PH").val(this.value);
        });
        
        function ToKhai(data){
        
            if(data!=null)
            {
            $("#So_HS").val(data.So_HS);    
            $("#Ma_DVQL").val(data.Ma_DVQL);    
            $("#Ten_DVQL").val(data.Ten_DVQL);    
            $("#KyHieu_CT").val(data.KyHieu_CT);    
            $("#So_CT").val(data.So_CT);    
            $("#Nam_CT").val(data.Nam_CT);            
            
            if(data.NguoiNopTien!=null)
            {   
                LoadNguoiNopTien(data.NguoiNopTien);
            }
            if(data.ThongTin_NopTien!=null)
            {
                ThongTinNopTien(data.ThongTin_NopTien);           
            }
            if(data.TaiKhoan_NopTien!=null)
            {
                TaiKhoan_NopTien(data.TaiKhoan_NopTien);
            }
            }
            $("#labelMoneyByText").text(So_chu($("#txtTongTrichNo").val()));
        };
        
        function LoadNguoiNopTien(data)
        {
            $("#MST").val(data.Ma_ST);
            $("#TT_Khac").val(data.TT_Khac);
            $("#Ten_NNT").val(data.Ten_NNT);
            $("#DiaChi").val(data.DiaChi);          
            
        }
        
        function ThongTinNopTien(data)
        {
            $("#Ma_NT").val(data.Ma_NT);
            $("#TongTien_NT").val(accounting.formatNumber( Math.round(data.TongTien_NT/100)*100,0,",","."));
            $("#txtTongTien").val(accounting.formatNumber( Math.round(data.TongTien_VND/100)*100,0,",","."));
            $("#txtTongTrichNo").val(accounting.formatNumber( Math.round(data.TongTien_VND/100)*100,0,",","."));
            $("#TyGia").val(data.TyGia);                      
        }        
        
        function TaiKhoan_NopTien(data)
        {
            $("#Ma_NH_TH").val(data.Ma_NH_TH);
            $("#Ten_NH_TH").val(data.Ten_NH_TH);            
            $("#TaiKhoan_TH").val(data.TaiKhoan_TH);          
            $("#Ten_TaiKhoan_TH").val(data.Ten_TaiKhoan_TH); 
            jsGetTEN_NH_TT(data.Ma_NH_TH);
                
        }
        
        function loadListChungTu(trangthai, tt_ct)
        {
            var viewModel  ={
                trangthai: trangthai,
                tt_ct: tt_ct
            };
            
            $.ajax({
            type: "POST",
            url: "frmLapPhiBoNganh.aspx/GetListChungTu",        
            contentType: "application/json; charset=utf-8",
            data:JSON.stringify(viewModel), 
            dataType: "json",
            success: function(data) {                       
                     var modelGen=JSON.parse(data.d);                                
                     var tr;
                     var json =modelGen.Table;                     
                     chungTuTempArray = json;
                     $("#tbodyListCT").children().remove();
                     $("#tongCT").empty();
                      $("#pager").empty();
                    $("#pager").remove();
                                       
                     $("#tbodyListCT").empty();
                     for (var i = 0; i < json.length; i++) 
                     {
                            tr = $('<tr class="grid_item"/>');
                            tr.append("<td style=' border:1pt solid black;'><img id='img"+i+"' src=" + getTTCT(json[i].TRANGTHAI_CT, json[i].TRANGTHAI_XULY) + "></img></td>");                                                       
                            tr.append("<td align='center'  style=' border:1pt solid black;'><a href='#' onclick='fillDatasToTextBox("+i+")'>" + json[i].SO_CT + "</a></td>");                          
                            tr.append("<td   style=' border:1pt solid black;'>"+json[i].TEN_DN+"</td>");
                            $('#tbodyListCT').append(tr);                           
                            $("#tongCT").empty();
                            $("#tongCT").append(json.length);   
                     }
                    
                     paginationList(15);
                 }
            });
        }
        
        function paginationList(listPaginationCount)
        {
            $('table.paginated').each(function() {
                var currentPage = 0;
                var numPerPage = listPaginationCount;    
                var $pager=$('.pager').remove();
                var $paging = $("#paging").remove();
                var $table = $(this);
                $table.bind('repaginate', function() {
                    $table.find('tbody#tbodyListCT tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody#tbodyListCT tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div id="pager"></div>');
              
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function(event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');
                    
                }
                
                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });
        }   
        
        
        function TruyVanThongTin()
        {
            
            var LePhiTraCuu = {
                So_HS : $("#textBoxSearchSo_Hoso").val(),
                Ma_DVQL : $("#textBoxSearchMa_DVQL").val(),
                KyHieu_CT : $("#textBoxSearchKyHieu_CT").val(),
                So_CT : $("#textBoxSearchSo_CT").val(),
                Nam_CT : $("#textBoxSearchNam_CT").val()               
            };
           
            $.ajax
            ({
            type: "POST",
            url: "frmLapPhiBoNganh.aspx/getVMTruyVanHQ",
            data: JSON.stringify({vmLePhi:LePhiTraCuu}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) 
                {            
                     var modelGen=JSON.parse(data.d);                   
                     ToKhai(modelGen);       
                     var tr;
                     if(modelGen!=null)
                     {
                         var json =modelGen.ChiTiet_CT;  
                         listAdded = json;
                         $("#divDraw").children().empty();
                          var checkbox='';
                         for (var i = 0; i < json.length; i++) 
                         {
                                checkbox = '';
                                if ((json[i].SoTien_VND>0)||json[i].SoTien_NT>0) checkbox="checked='checked'";
                                tr = $("<tr/>");
                                tr.append("<td style='width:5%;text-align:center;' >" + (i+1) + "</td>");
                                tr.append("<td  style='width:5%;text-align:center;' ><input  disabled type='checkbox' name='a' " + checkbox +" onclick='jsCalcTotalMoney();' value='1' id='checkBox"+i +"' class='inputflat' ></td>");
                                tr.append("<td  style='width:12%'><input type='text' readonly='readonly' id='ndkt_ndkt_"+i+"' value='" + json[i].NDKT + "' onkeypress='return fillNDKTByMaTMuc("+i+",event);' style='width:97%;' class='inputflat' /></td>");
                                tr.append("<td  style='width:40%'><input type='text' readonly='readonly' id='ndkt_ten_ndkt_"+i+"'  style='width:97%' value='" + json[i].Ten_NDKT + "' class='inputflat' /></td>");
                                tr.append("<td  style='width:15%'><input type='text' readonly='readonly' id='ndkt_sotien_vnd_"+i+"' value='" + accounting.formatNumber(json[i].SoTien_VND) + "' onblur='return fillVNDByNT("+i+","+$("#TyGia").val()+")' onkeypress='return isNumber(event);' style='width:97%;' class='inputflat' /></td>");    
                                tr.append("<td  style='width:15%'><input type='text' readonly='readonly' id='ndkt_sotien_nt_"+i+"' value='" + accounting.formatNumber(json[i].SoTien_NT,0,",",".") + "' onkeypress='return isNumber(event);' style='width:97%;' class='inputflat' /></td>");
                                tr.append("<td  style='width:12%'><input type='text' id='ndkt_ghichu_"+i+"' value='" + json[i].GhiChu + "' style='width:97%;' class='inputflat' /></td>");                                
                                
                                $('#divDraw').append(tr);                                                           
                                
                            //};
                         }
                         
                         if(json!=undefined && json.length <5)
                         {
                            var remainTR = 5- json.length;
                            for (var i = json.length; i < 5; i++) 
                            {
                                    tr = $('<tr/>');
                                    tr.append("<td  style='width:5%;text-align:center;'>" + (i+1) + "</td>");
                                    tr.append("<td  style='width:5%;text-align:center;'><input type='checkbox'  disabled name='a' onclick='jsCalcTotalMoney();' value='1' id='checkBox"+i +"' class='inputflat' ></td>");
                                    tr.append("<td  style='width:12%'><input type='text' readonly='readonly' id='ndkt_ndkt_"+i+"'  onkeypress='return fillNDKTByMaTMuc("+i+",event);' style='width:97%;' class='inputflat' /></td>");
                                    tr.append("<td style='width:40%'><input type='text' readonly='readonly' id='ndkt_ten_ndkt_"+i+"' style='width:97%' class='inputflat' /></td>");
                                    tr.append("<td style='width:15%'><input type='text' readonly='readonly' id='ndkt_sotien_vnd_"+i+"'  value='0' onblur='return fillVNDByNT("+i+","+$("#TyGia").val()+")' onkeypress='return isNumber(event);' style='width:97%;' class='inputflat' /></td>");    
                                    tr.append("<td style='width:15%'><input type='text' readonly='readonly' id='ndkt_sotien_nt_"+i+"'  value='0'onkeypress='return isNumber(event);' style='width:76px;' class='inputflat' /></td>");
                                    tr.append("<td style='width:12%'><input type='text' readonly='readonly' id='ndkt_ghichu_"+i+"' style='width:97%;' class='inputflat' /></td>");                                
                                    $('#divDraw').append(tr);                                                           
                               
                            }
                         }
                     jsCalcTotalMoney();
                    }
                }
            });
        }
        
        function getBankTransfer()
        {
            $.ajax({
            type: "POST",
            url: "frmLapPhiBoNganh.aspx/getListBankToTransfer",           
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                        listBankTransfer= $.parseJSON(data.d);                    
                       
                        $.each(listBankTransfer, function(index, value) 
                        {
                            $('#ddl_NganHang').append($('<option>').text(value).attr('value', index));                            
                        });
                       
                        //Lay gia tri dau tien 
                        $("#Ma_NH_PH").val( $("#ddl_NganHang option:first-child").val());
                      
                }
            });
        }
        
        function containsObject(obj, list) {
                var i;
                for (i = 0; i < list.length; i++) {
                    if (list[i] === obj) {
                        return true;
                    }
                }
            return false;
        }
        
        function jsCalcTotalMoney(){
            var dTotalMoney = 0;
            var dDetailMoney = 0;
            
            var tongTienNT = 0;
            var ctTongTienNT = 0;
            
            $('#tableData tr').each(function(){
                var ctrCheck = $(this).find("input[id^='checkBox']");
                var ctrInput = null;
                var ctrInputNT = null;
                if (ctrCheck) {
                    if (ctrCheck.is(":checked")){
                        
                        ctrInput = $(this).find("input[id^='ndkt_sotien_vnd_']");
                        ctrNDKT = $(this).find("input[id^='ndkt_ndkt_']");
                        
                        if (ctrInput) {
                            dDetailMoney = 0;
                            dDetailMoney = accounting.unformat(ctrInput.prop("value"));
                            if (!isNaN(dDetailMoney)) {
                                dTotalMoney = dTotalMoney + dDetailMoney;
                            }
                        }
                        
                        ctrInputNT = $(this).find("input[id^='ndkt_sotien_nt_']");
                        
                        if (ctrInputNT) {
                            ctTongTienNT = 0;
                            ctTongTienNT = accounting.unformat(ctrInputNT.prop("value"),".");
                            if (!isNaN(ctTongTienNT)) {
                                tongTienNT += ctTongTienNT;
                            }
                        }
                    } else {
                        ctrInput = null;
                        ctrInputNT = null;
                    }
                }
            });
            
            
            if (document.getElementById('chkChargeType').checked)
            {
                dblPhi =(dTotalMoney * feeRate)/minFee; 
                dblVAT=  (dTotalMoney * feeRate)/minFee/10;  
                if (dblPhi<minFee) 
                {
                    dblPhi=minFee;
                    dblVAT=dblPhi/10;
                }
            }
            else
            {
                dblPhi = parseFloat (accounting.unformat($('#txtCharge').val()));  
                dblVAT= dblPhi/10;          
            }
            
            dblTongTrichNo = dblPhi + dblVAT + dTotalMoney;
            //$('#txtVAT').val(accounting.formatNumber(dblVAT));
            //$('#txtCharge').val(accounting.formatNumber(dblPhi));
            //$('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo));       
            //$("#txtTongTrichNo").val(accounting.formatNumber( Math.round(dTotalMoney/100)*100,0,",","."));
            //$('#txtTongTien').val(accounting.formatNumber(dTotalMoney));
            //$("#txtTongTien").val(accounting.formatNumber( Math.round(dTotalMoney/100)*100,0,",","."));
            
            $("#labelMoneyByText").text(So_chu(accounting.unformat($('#txtTongTrichNo').val())));
            
            //if($("#Ma_NT").val() != 'VND')
                //$("#TongTien_NT").val(accounting.formatNumber(Math.round(tongTienNT/100)*100,0,",","."));
                //$('#TongTien_NT').val(accounting.formatNumber(tongTienNT,2,",","."));
            //else
                //$("#TongTien_NT").val(accounting.formatNumber(Math.round(tongTienNT/100)*100,0,",","."));
                //$('#TongTien_NT').val(accounting.formatNumber(tongTienNT));
        }    
        
        function getStringText(id)
        {
            return $("#"+ id).val();
        }
        
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }      
        
        function checkTruyVanValidate()
        {
            return true;
            var exist = true;
         
            var So_HS= $("#textBoxSearchSo_Hoso").val();
            var Ma_DVQL= $("#textBoxSearchMa_DVQL").val();
            var KyHieu_CT= $("#textBoxSearchKyHieu_CT").val();
            var So_CT= $("#textBoxSearchSo_CT").val();
            var Nam_CT= $("#textBoxSearchNam_CT").val();
            
            if (So_HS.length==0 || Ma_DVQL.length==0 ||  KyHieu_CT.length==0 ||  So_CT.length==0 ||  Nam_CT.length==0) 
            {
                alert("Bạn phải nhập đầy đủ thông tin truy vấn");
                exist=false;
            }
            return exist;            
        }
        
        function CheckValid()
        {
           var exist = true;
           $("#divStatus").text("");
           var Ma_NH_PH = $("#Ma_NH_PH").val();
           
           if(Ma_NH_PH.length ==0 )
           {
                 $("#divStatus").append("Mã ngân hàng không được để trống ");
                 exist=false;
                 $("#Ma_NH_PH").focus();
                 return false;
           }
           
           var KyHieu_CT = $("#KyHieu_CT").val();
           if(KyHieu_CT.length==0 || KyHieu_CT.length  >10 )
           {
                $("#divStatus").append("Mã ký hiệu chứng từ không được để trống hoặc lớn hơn 10 kí tự");
                exist=false;    
                $("#KyHieu_CT").focus(); 
                return false;
           }      
                
           var MST = $("#MST").val();           
           if(MST.length==0 )
           {
                $("#divStatus").append("Mã số thuế không được để trống");
                exist=false;     
                $("#MST").focus();
                return false;
           }
           
           var Ten_NNT = $("#Ten_NNT").val();
           if(Ten_NNT.length==0)
           {
                $("#divStatus").append("Tên người nộp tiền không được để trống");
                exist=false;     
                $("#Ten_NNT").focus();
                return false;
           }           
           
           var TongTien_NT = $("#TongTien_NT").val();
           if(TongTien_NT.length==0 )
           {
                $("#divStatus").append("Số tiền nguyên tệ không được để trống");
                exist=false;     
                $("#TongTien_NT").focus();
                return false;
           }
           
           var TongTien_VND = $("#txtTongTien").val();
           if(TongTien_VND.length==0 )
           {
                $("#divStatus").append("Số tiền không được để trống");
                exist=false;     
                $("#txtTongTien").focus();
                return false;
           }
           
           var currentPTTT=  $("#PTTT").val();
           if ( currentPTTT==3)
           {
                
                //Hiển thị tài khoản GL
                var txtTKGL= $("#txtTKGL").val();
               
                if (txtTKGL.length <=0 )
                {
                    $("#divStatus").append(" Yêu cầu bắt buộc nhập tài khoản GL");
                    if (txtTKGL.length <=0)
                    {
                        $("#txtTKGL").focus();
                        return false;
                    }
                    
                    exist =false;
                }
           }
           if ( currentPTTT==1)
           {
                var TaiKhoan_Chuyen= $("#TaiKhoan_Chuyen").val();                
                if (TaiKhoan_Chuyen.length <=0 )
                {
                    $("#divStatus").append("Tài khoản chuyển không được để trống");
                    $("#TaiKhoan_Chuyen").focus();                    
                    exist =false;
                    
                }
           }
           return exist;           
        }
        
        function jsGet_TenTKGL()
        {  
            if (arrTKGL.length>0)
            {
                for (var i=0;i<arrTKGL.length;i++)
                {
                    var arr = arrTKGL[i].split(';');                                       
                    if (arr[0] == $('#txtTKGL').val()){
                        $('#txtTenTKGL').val(arr[1]);
                        break;
                    }
                }            
            }
        }
        
        function jsEnabledInputCharge()
        {    
            var dblTongTien = parseFloat (accounting.unformat($('#txtTongTien').val()));         
            var dblPhi = 0;
            var dblVAT = 0;                      
            var dblTongTrichNo = 0;
            if (document.getElementById('chkChargeType').checked)
            {
                
//                $('#txtCharge').disabled  = true;
//                //neu ma checked thi tinh lai fee
//                dblPhi = dblTongTien * feeRate/minFee;  
//                dblVAT = dblTongTien * feeRate/minFee/10;
//                if (dblPhi<minFee) 
//                {
//                    dblPhi=minFee;
//                    dblVAT=dblPhi/10;
//                }    
//                dblTongTrichNo = dblPhi + dblVAT + dblTongTien;      
//                $('#txtCharge').val(accounting.formatNumber(dblPhi));                                        
//                $('#txtVAT').val(accounting.formatNumber(dblVAT));                            
//                $('#txtTongTien').val(accounting.formatNumber(dblTongTien));                  
//                $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo));
                getRowSelected("");
                                     
            }else{
                $('#txtCharge').prop('disabled',false);
                $('#txtCharge').val("0");                                        
                $('#txtVAT').val("0");
                $('#txtTongTrichNo').val(accounting.formatNumber($('#txtTongTien').val()));
            }    
        }
    
        function jsFormatNumber2(val)
        {
           return localeNumberFormat(val);
        }
        
        function getRowSelected(tbody)
        {
            var rowvalues = [];
            currentList.length=0;
            var index=0;
            var exist=false;            
            var table = document.getElementById('divDraw');
            var rowLength = table.rows.length; 
            for(var i=0; i<=rowLength-1; i+=1){
                var row = table.rows[i];
                if($('#checkBox'+i).is(":checked"))
                    {                 
                    var chiTiet_CT ={};
                    chiTiet_CT.STT = (i +1);
                    chiTiet_CT.NDKT = getStringText("ndkt_ndkt_"+i);
                    chiTiet_CT.TEN_NDKT = getStringText("ndkt_ten_ndkt_"+i);
                    chiTiet_CT.SOTIEN_NT = accounting.unformat(getStringText("ndkt_sotien_nt_"+i),".");
                    chiTiet_CT.SOTIEN_VND = accounting.unformat(getStringText("ndkt_sotien_vnd_"+i),".");
                    chiTiet_CT.GHICHU = getStringText("ndkt_ghichu_"+i);
                    //return;
                    var sotien_nt = chiTiet_CT.SOTIEN_NT;
                    var sotien_vnd = chiTiet_CT.SOTIEN_VND;                    
//                    if(sotien_nt <=1)
//                    {
//                        alert("Số tiền nguyên tệ không được nhỏ hơn 0");
//                        $("#ndkt_sotien_nt_"+i).focus();
//                        exist=false;                        
//                    }
//                    if(sotien_vnd < 1)
//                    {
//                        alert("Số tiền VNĐ không được nhỏ hơn 0");
//                        $("#ndkt_sotien_nt_"+i).focus();
//                        exist=false;
//                    }                    
                    if(sotien_nt >=1  || sotien_vnd >=1)
                    {   
                        currentList.push(chiTiet_CT);
                        exist=true;
                    }                   
                    }
                }
       
                var tongTien_VND = $("#txtTongTien").val();
                if(currentList.length>0)
                {
                    return exist;
                }
                else if(tongTien_VND <=0)
                {
                     alert("Số tiền nộp chứng từ phải lớn hơn 0");
                     
                     exist=false;  
                }
                return exist;          
            
         }
        
         function createCT(code)
         {
            var exist=false;
            if(!CheckValid())
            {
                exist=false;                
            }
            else if(!getRowSelected("divDraw"))
            {
                exist=false;                
            }
            else
            {   
                exist=true;
                var currentPTTT=  $("#PTTT").val();
                var tk_thanhtoan = "";
                var ten_tk_thanh_toan="";
                if(currentPTTT=="3")
                {
                    tk_thanhtoan= getStringText("txtTKGL");
                    ten_tk_thanh_toan=getStringText("txtTenTKGL");
                }
                if(currentPTTT=="1")
                {
                    tk_thanhtoan= getStringText("TaiKhoan_Chuyen");
                    ten_tk_thanh_toan=getStringText("Ten_TaiKhoan_Chuyen");
                }
                var CHUNGTU= {
                        MA_NH_PH: getStringText("Ma_NH_PH"),
                        TEN_NH_PH: $("#ddl_NganHang option:selected").text(),//getStringText("Ten_NH_PH"),
                        KYHIEU_CT:getStringText("KyHieu_CT"),
                        SO_CT: "",
                        NGAY_CT:'',
                        NGAY_BN :'',
                        NGAY_BC:'',
                        SO_HS: getStringText("So_HS"),
                        MA_DVQL: getStringText("Ma_DVQL"),
                        TEN_DVQL: getStringText("Ten_DVQL"),
                        KYHIEU_CT_PT: getStringText("KyHieu_CT"),
                        SO_CT_PT: getStringText("So_CT"),        
                        NAM_CT_PT: getStringText("Nam_CT"),
                        NNT_MST: getStringText("MST"),
                        NNT_TEN_DV: getStringText("Ten_NNT"),
                        NNT_DIACHI: getStringText("DiaChi"),
                        NNT_TT_KHAC: "",
                        TT_NT_MA_NT: getStringText("Ma_NT"),
                        TT_NT_TYGIA: getStringText("TyGia"),
                        TT_NT_TONGTIEN_NT: accounting.unformat(getStringText("TongTien_NT")),
                        TT_NT_TONGTIEN_VND: accounting.unformat(getStringText("txtTongTien")),                        
                        TAIKHOAN_NT_MA_NH_TH: getStringText("Ma_NH_TH"),
                        TAIKHOAN_NT_TEN_NH_TH: getStringText("Ten_NH_TH"),
                        TAIKHOAN_NT_MA_NH_TT: getStringText("txtTaiKhoan_TT"),
                        TAIKHOAN_NT_TEN_NH_TT: getStringText("txtTen_TaiKhoan_TT"),
                        TAIKHOAN_NT_TK_TH: getStringText("TaiKhoan_TH"),
                        TAIKHOAN_NT_TEN_TK_TH: getStringText("Ten_TaiKhoan_TH"),
                        TRANGTHAI_CT :code,
                        PTTT : $("#PTTT option:selected").val(),
                        TK_THANHTOAN: tk_thanhtoan,//getStringText("TaiKhoan_Chuyen"),
                        NNT_QUAN_HUYEN : getStringText("NNT_QUAN_HUYEN"),
                        NNT_TINH_TP : getStringText("NNT_TINH_TP"),
                        TT_NT_So_CMND: getStringText("TT_NT_So_CMND"),
                        TT_NT_So_DT : getStringText("TT_NT_So_DT"),
                        TK_THANH_TOAN_QUAN_HUYEN : getStringText("TT_NT_TK_THANH_TOAN_QUAN_HUYEN"),
                        TK_THANH_TOAN_TINH_TP : getStringText("TT_NT_TK_THANH_TOAN_TINH_TP"),
                        TEN_TK_CHUYEN: ten_tk_thanh_toan,//getStringText("Ten_TaiKhoan_Chuyen"),
                        SO_DU : accounting.unformat(getStringText("So_Du")),
                        TK_THANH_TOAN_DIACHI :getStringText("TK_Thanh_Toan_DiaChi"),
                        FEE : accounting.unformat(getStringText("txtCharge")),
                        VAT : accounting.unformat(getStringText("txtVAT")),
                        TONGTIEN_TRUOC_VAT : accounting.unformat(getStringText("txtTongTien"))
                        
                    };   
                    //console.log(JSON.stringify(currentList) );
                    $.ajax({
                    type: "POST",
                    url: "frmLapPhiBoNganh.aspx/createToKhaiHaiQuan",
                    data: JSON.stringify({tsc: CHUNGTU, LISTCHITIET_CT:JSON.stringify(currentList) }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) 
                        {
                            var jsonResult = JSON.parse(data.d);
                            $("#divStatus").text("");
                            $(this).prop("disabled",true);
                            if(jsonResult.Code==1)
                            {
                                enablePrint=true;
                                currentSoCT= jsonResult.SoCT;
                                currentCode=='-1';
                            }
                            else
                            {
                            }
                            $("#divStatus").text(jsonResult.Message);
                            
                            currentCode = code;
                            buttonState(code);
                            $("#tongCT").empty();
                            $("#pager").empty();
                            $("#pager").remove();
                            loadListChungTu("-1","-1");
                            if(currentCode=="05")
                            {
                                listChiTietCT(currentSoCT);  
                            }
                            exist=true;
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
                            exist=false;  
                        }     
                    });
                }
                return exist;
        }
           
        function checkHuyCT(soct) 
        {            
            lydo_huy = $("#txtLyDoHuy").val();
            if(currentCode=="01" &&  lydo_huy.length <=0)
            {
                 $("#divStatus").text("Lý do hủy không được để trống");
                 $("#txtLyDoHuy").focus();
                 return false;
            }
             
            if(soct=="0")
            {
                alert("Không tồn tại số chứng từ hủy"); 
                return;
            }
            var x=false;
            if (confirm("Bạn có chắc chắn là muốn hủy chứng từ số: "+ soct ) == true) {
                x = true;
            } else {
                x = false;
            }
           return x;
        }
         
        function updateChungTu(type, lydo_huy)
         {
            if(!CheckValid())
                return;
            
            if(currentSoCT!=0)
            {
                var currentPTTT=  $("#PTTT").val();
                var tk_thanhtoan = "";
                var ten_tk_thanh_toan="";
                if(currentPTTT=="3")
                {
                    tk_thanhtoan= getStringText("txtTKGL");
                    ten_tk_thanh_toan=getStringText("txtTenTKGL");
                }
                if(currentPTTT=="1")
                {
                    tk_thanhtoan= getStringText("TaiKhoan_Chuyen");
                    ten_tk_thanh_toan=getStringText("Ten_TaiKhoan_Chuyen");
                }
                var vmChungTu = 
                {
                    CODE : currentCode,
                    SO_CT : currentSoCT,
                    TYPE : type,
                    LYDO_HUY : lydo_huy  ,
                    PTTT: $("#PTTT option:selected").val(),                    
                    TK_THANHTOAN: tk_thanhtoan,
                    TEN_TK_CHUYEN:ten_tk_thanh_toan,
                    SO_DU:accounting.unformat(getStringText("So_Du")),
                    TT_NT_SO_CMND:getStringText("TT_NT_So_CMND"),
                    TT_NT_SO_DT:getStringText("TT_NT_So_DT"), 
                    NNT_TINH_TP:getStringText("NNT_TINH_TP"),                    
                    NNT_QUAN_HUYEN :getStringText("NNT_QUAN_HUYEN"),
                    TK_THANH_TOAN_TINH_TP:getStringText("TT_NT_TK_THANH_TOAN_TINH_TP"),
                    TK_THANH_TOAN_QUAN_HUYEN:getStringText("TT_NT_TK_THANH_TOAN_QUAN_HUYEN"),
                    MA_NH_PH :     $("#ddl_NganHang option:selected").val(),
                    TEN_NH_PH : $("#ddl_NganHang option:selected").text()    ,
                    TK_THANH_TOAN_DIACHI: getStringText("TK_Thanh_Toan_DiaChi"),
                    Ma_NT : getStringText("Ma_NT"),
                    TyGia:getStringText("TyGia"),
                    TongTien_NT:accounting.unformat(getStringText("TongTien_NT")),
                    TongTien_VND:accounting.unformat(getStringText("txtTongTien")),
                    Ma_NH_TH:getStringText("Ma_NH_TH"),
                    Ten_NH_TH:getStringText("Ten_NH_TH"),
                    TaiKhoan_TH:getStringText("TaiKhoan_TH"),
                    Ten_TaiKhoan_TH:getStringText("Ten_TaiKhoan_TH"),
                    DiaChi: getStringText("DiaChi"),
                    TT_Khac: getStringText("TT_Khac"), 
                    FEE : accounting.unformat(getStringText("txtCharge")),
                    VAT : accounting.unformat(getStringText("txtVAT")),
                    TONGTIEN_TRUOC_VAT : accounting.unformat(getStringText("txtTongTien")),   
                    TAIKHOAN_NT_MA_NH_TT: getStringText("txtTaiKhoan_TT"),
                    TAIKHOAN_NT_TEN_NH_TT: getStringText("txtTaiKhoan_TT"),
                        
                    LISTCHITIET_CT : JSON.stringify(currentList)
                };
                     
                $.ajax({
                type: "POST",
                url: "frmLapPhiBoNganh.aspx/updateChungTu",
                data: JSON.stringify(vmChungTu),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) {                     
                       //redraw => 
                       $("#divStatus").text(data.d.Message);                      
                       
                       $("#tongCT").empty();
                       $("#pager").empty();
                       $("#pager").remove();
                       buttonState(data.d.TrangThaiCT);
                       if(data.d.TrangThaiCT=="05")
                       {
                           listChiTietCT(currentSoCT);  
                       }
                       loadListChungTu("-1","-1");
                    }
                });
            }
           
         }
         
         function fillTempData(data)
         {    
            
           $("#divStatus").text("");
           $("#So_HS").val(data.SO_HS);    
           $("#Ma_DVQL").val(data.MA_DVQL);   
           $("#Ma_NH_PH").val(data.MA_NH_PH);    
           $("#Ten_NH_PH").val(data.TEN_NH_PH);    
           $("#Ma_DVQL").val(data.MA_DVQL);    
           $("#Ten_DVQL").val(data.TEN_DVQL);    
           $("#KyHieu_CT").val(data.KYHIEU_CT);    
           $("#So_CT").val(data.SO_CT_PT);    
           $("#Nam_CT").val(data.NAM_CT_PT);               
           $("#Ma_NH_TH").val(data.TAIKHOAN_NT_MA_NH_TH);
           $("#Ten_NH_TH").val(data.TAIKHOAN_NT_TEN_NH_TH); 
           
           if(data.TAIKHOAN_NT_MA_NH_TT == ""){
                $('#rowCheckTaiKhoanNoiBo').hide();
                $("#txtTaiKhoan_TT").val("");
                $("#txtTen_TaiKhoan_TT").val(""); 
           }else{
                $('#rowCheckTaiKhoanNoiBo').show();
                $("#txtTaiKhoan_TT").val(data.TAIKHOAN_NT_MA_NH_TT);
                $("#txtTen_TaiKhoan_TT").val(data.TAIKHOAN_NT_TEN_NH_TT); 
           }
          
                      
           $("#TaiKhoan_TH").val(data.TAIKHOAN_NT_TK_TH);          
           $("#Ten_TaiKhoan_TH").val(data.TAIKHOAN_NT_TEN_TK_TH);     
           $("#MST").val(data.NNT_MST);
           $("#TT_Khac").val(data.NNT_TT_KHAC);
           $("#Ten_NNT").val(data.NNT_TEN_DV);
           $("#DiaChi").val(data.NNT_DIACHI);       
           $("#Ma_NT").val(data.TT_NT_MA_NT);
           $("#TongTien_NT").val(accounting.formatNumber(data.TT_NT_TONGTIEN_NT));
           $("#txtTongTien").val(accounting.formatNumber(data.TT_NT_TONGTIEN_VND));
           $("#TyGia").val(data.TT_NT_TYGIA);              
           $("#NNT_QUAN_HUYEN").val(data.NNT_QUAN_HUYEN);   
           $("#NNT_TINH_TP").val(data.NNT_TINH_TP);   
           $("#TT_NT_So_CMND").val(data.TT_NT_SO_CMND);   
           $("#TT_NT_So_DT").val(data.TT_NT_SO_DT);   
           $("#TT_NT_TK_THANH_TOAN_QUAN_HUYEN").val(data.TK_THANH_TOAN_QUAN_HUYEN);   
           $("#TT_NT_TK_THANH_TOAN_TINH_TP").val(data.TK_THANH_TOAN_TINH_TP);   
           $("#TaiKhoan_Chuyen").val(data.TK_THANHTOAN);   
           $("#Ten_TaiKhoan_Chuyen").val(data.TEN_TK_CHUYEN);   
           $("#So_Du").val(data.SO_DU);   
           $("#TK_Thanh_Toan_DiaChi").val(data.TK_THANH_TOAN_DIACHI);   
           $("#divSo_CT").text(" : "+data.SO_CT); 
           $("#txtCharge").val(accounting.formatNumber(data.FEE));   
           $("#txtVAT").val(accounting.formatNumber(data.VAT));   
           $("#txtTongTrichNo").val(accounting.formatNumber(data.TONGTIEN_TRUOC_VAT + data.FEE +data.VAT )); 
           $("#txtTongTien").val(accounting.formatNumber(data.TONGTIEN_TRUOC_VAT) ); 
        
           
           
           
           if(!data.LY_DO==null)
           {
                $("#txtLyDoHuy").text(" : "+data.LY_DO); 
           }           
           $("#labelMoneyByText").text(So_chu(data.TONGTIEN_TRUOC_VAT + data.FEE +data.VAT));
           var PTTT = data.PTTT;
           $('#PTTT').val(PTTT);
           
           //Fix truong hop so du
           
           if(PTTT==3)
           {
                $("#tableRowSoDu").hide();
           }
           else
           {
               $("#tableRowSoDu").show();
               jsGetTK_KH_NH();
           }
           
            $('#PTTT option').map(function () {
                if ($(this).val() == PTTT) return this;
            }).attr('selected', 'selected');
            
            
            
           try
           {
                var currentDate = data.NGAY_CT.substring(0, 10).split('-');             
                var currentMonth = currentDate[1];
                if(currentMonth.length ==1)
                {
                    currentMonth = "0"+ currentMonth;
                }
                $("#NgayLap_CT").val(currentDate[2] +"/" + currentMonth +"/"+ currentDate[0]);  
                $("#NgayLap_CT").attr('disabled','disabled');
           }
           catch(err)
           {            
           }                         
           currentSoCT = data.SO_CT;            
           currentCode = data.TRANGTHAI_CT;            
           buttonState(currentCode);           
                               
        };
        
        function fillDatasToTextBox(index)
        {
            var objectChungTu = chungTuTempArray[index];             
            fillTempData(objectChungTu);
            listChiTietCT(objectChungTu.SO_CT);
        }
        
        
        function fillNDKTByMaTMuc(index,event)
        {
          if(event.keyCode == 13){
            
            if(currentCode=="00" || currentCode=="-1")
            {
                
                returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=MucTMuc&initParam=MA_DM_"+index,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
    	         
                if (returnValue != null) {
                    $('#ndkt_ndkt_'+index).val("");
                    $('#ndkt_ten_ndkt_'+index).val("");
                    var title=returnValue.Title;
                    var id=returnValue.ID;
                    $('#ndkt_ndkt_'+index).val(id);              
                    $('#ndkt_ten_ndkt_'+index).val(title);                        
                }
            }
            else
            {
                event.preventDefault();
            }
         }         
        }
        
        function fillVNDByNT(index, tygia)
        {
            //Hien phi bo ban nganh dang fix cung VND???
            var tien_vnd = accounting.unformat($("#ndkt_sotien_vnd_"+index).val());
            $('#ndkt_sotien_vnd_'+index).val("");
            $('#ndkt_sotien_nt_'+index).val("");
            $("#ndkt_sotien_vnd_"+index).val(accounting.formatNumber(tien_vnd));
            
            var tien_nt = tien_vnd;
            var toFix = 0;
            if($("#Ma_NT").val() != 'VND'){
                toFix = 2;
                var tyGia = $('#TyGia').val();
                tien_nt = (tienVnd/tyGia).toFixed(toFix);
                
                $("#ndkt_sotien_nt_"+index).val(accounting.formatNumber(tien_nt,toFix,",","."));
            }else
                $("#ndkt_sotien_nt_"+index).val(accounting.formatNumber(tien_nt));
                
                
            jsCalcTotalMoney(); 
        }
        
        function listTyGia(tygia)
        {
            $.ajax({
                type: "POST",
                url: "frmLapPhiBoNganh.aspx/getListTyGiaNT",                
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) 
                {          
                    var modelGen=JSON.parse(data.d);            
                     //redraw => 
                     var options = {

                      data: modelGen,

                      getValue: function(element) {
                        return element.TEN_NT;
                      },
                      template: {
                        type: "description",
                        fields: {
                            description: "TYGIA"
                        }
                     },
                      list: {
                            onSelectItemEvent: function() {
                                var selectedItemValue = $("#Ma_NT").getSelectedItemData().TYGIA;

                                $("#TyGia").val(selectedItemValue).trigger("change");
                            },
                            maxNumberOfElements: 18                           
                        }                      
                      
                    };
                    $("#Ma_NT").easyAutocomplete(options);
                }
                
            });
        }
        
        
        function listChiTietCT(so_ct)
        {
            var viewModel ={
                So_CT : so_ct
            };
            $.ajax({
                type: "POST",
                url: "frmLapPhiBoNganh.aspx/getListChiTietBySoCT",
                data: JSON.stringify(viewModel),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) 
                {                     
                     //redraw => 
                     var modelGen=JSON.parse(data.d);                     
                     //ToKhai(modelGen);               
                     //console.log(modelGen);
                     var tr;
                     if(modelGen!=null)
                     {
                         //var json =modelGen.ChiTiet_CT;  
                         ///listAdded = json;
                         
                         var json = modelGen.Table;
                         $("#divDraw").children().remove();
                         currentList= json;
                         for (var i = 0; i < json.length; i++) 
                         {
                                tr = $('<tr/>');
                                tr.append("<td style='width:5%;text-align:center;'>" + (i+1) + "</td>");
                                tr.append("<td style='width:5%';text-align:center;><input type='checkbox' name='a' disabled='disabled' checked='checked'  value='1' id='checkBox"+i+"' class='inputflat' /></td>");
                                tr.append("<td style='width:12%'><input type='text' readonly='readonly' id='ndkt_"+i+"' value='" + json[i].NDKT + "' readonly='readonly' onkeypress='return fillNDKTByMaTMuc("+i+",event);' class='inputflat' style='width:97%' /></td>");
                                tr.append("<td style='width:40%'><input type='text' readonly='readonly' id='ndkt_ten_ndkt_"+i+"'  style='width:97%' value='" + json[i].TEN_NDKT + "' readonly='readonly' class='inputflat' style='width:97%' /></td>");
                                tr.append("<td style='width:15%'><input type='text' readonly='readonly' id='ndkt_sotien_vnd_"+i+"' value='" + accounting.formatNumber(json[i].SOTIEN_VND) + "' readonly='readonly' onblur='return fillVNDByNT("+i+","+$("#TyGia").val()+")'  onkeypress='return isNumber(event);' class='inputflat' style='width:97%' /></td>");    
                                tr.append("<td style='width:15%'><input type='text' readonly='readonly' id='ndkt_sotien_nt_"+i+"' value='" + accounting.formatNumber(json[i].SOTIEN_NT,0,",",".") + "' readonly='readonly'  onkeypress='return isNumber(event);' class='inputflat' style='width:97%' /></td>");
                                tr.append("<td style='width:12%'><input type='text' readonly='readonly' id='ndkt_ghichu_"+i+"' value='" + json[i].GHICHU + "' class='inputflat' style='width:97%' /></td>");                                
                                $('#divDraw').append(tr);                                                           
                            };
                         }
                         if(currentCode=='00' || currentCode=='03')
                         {  
                            //console.log(json);
                             if(json.length <5)
                             {
                                var remainTR = 5- json.length;
                                 for (var i = json.length; i < 5; i++) 
                                {
                                        tr = $('<tr/>');
                                        tr.append("<td style='width:5%;text-align:center;'>" + (i+1) + "</td>");
                                        tr.append("<td style='width:5%;text-align:center;'><input type='checkbox' disabled='disabled' name='a' onclick='jsCalcTotalMoney();' value='1' id='checkBox"+i +"' class='inputflat' ></td>");
                                        tr.append("<td style='width:12%'><input type='text' readonly='readonly' id='ndkt_ndkt_"+i+"'  onkeypress='return fillNDKTByMaTMuc("+i+",event);' style='width:71px;' class='inputflat' /></td>");
                                        tr.append("<td style='width:40%'><input type='text' readonly='readonly' id='ndkt_ten_ndkt_"+i+"'  style='width:259px;' class='inputflat' /></td>");
                                        tr.append("<td style='width:15%'><input type='text' readonly='readonly' id='ndkt_sotien_vnd_"+i+"' value='0' readonly='readonly' onblur='return fillVNDByNT("+i+","+$("#TyGia").val()+")' onkeypress='return isNumber(event);' style='width:76px;' class='inputflat' /></td>");    
                                        tr.append("<td style='width:15%'><input type='text' readonly='readonly' id='ndkt_sotien_nt_"+i+"'  value='0' onkeypress='return isNumber(event);' style='width:76px;' class='inputflat' /></td>");
                                        tr.append("<td style='width:12%'><input type='text' readonly='readonly' id='ndkt_ghichu_"+i+"' style='width:76px;' class='inputflat' /></td>");                                
                                        $('#divDraw').append(tr);                                                           
                                   
                                }
                             }
                         }
                     }                 
                });
        }
        
        function getListNDKT()
        {            
                $.ajax({
                    type: "POST",
                    url: "frmLapPhiBoNganh.aspx/getListNDKT",                    
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) 
                        {                     
                             //redraw => 
                             
                             var modelGen=JSON.parse(data.d);                            
                            
                             listNDKT= modelGen;
                             
                        }
                    }
                );
        }
        function ShowLov(strType)
        {
            if (strType=="DMNH_TT") return FindDanhMuc('DMNH_TT','txtTaiKhoan_TT','txtTen_TaiKhoan_TT','txtTen_TaiKhoan_TT');
        }
        
        function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) 
        {        
            var strSHKB = "";
             var returnValue;
                
             returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&SHKB=" +strSHKB +"&initParam=" + $get(txtID).value,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
                if (returnValue != null) {
                    document.getElementById(txtID).value = returnValue.ID;
                    if (txtTitle!=null){
                        document.getElementById(txtTitle).value = returnValue.Title;
                    }
                    if (txtFocus != null) {
                        //if ($get('txtFocus').getAttribute('disabled') != 'disabled') {
                        try {
                        document.getElementById(txtFocus).focus();
                        }
                        catch (e) {};
                    }
            }
        }
        
        
        function jsGetTEN_NH_TT(data) {
            if (data != "") {
                document.getElementById('divStatus').innerHTML = '';
                PageMethods.chkChuyenKhoanNoiDia(data, GetTEN_NH_TT_Complete, GetTEN_NH_TT_Error);
            } 
        }
        
        function GetTEN_NH_TT_Complete(result, methodName) {
            if (result.length > 0) {
                var resultSpl = result.split(';');
                $("#rowCheckTaiKhoanNoiBo").show();
                $get('txtTaiKhoan_TT').value = resultSpl[0];
                $get('txtTen_TaiKhoan_TT').value = resultSpl[1];
            } else {
                $("#rowCheckTaiKhoanNoiBo").hide();
            }
        }
        function GetTEN_NH_TT_Error(error, userContext, methodName) {
            if (error !== null) {
                $("#rowCheckTaiKhoanNoiBo").hide();
                document.getElementById('divStatus').innerHTML = "";
            }
        }
        
        function jsGetTenTK_KH_NH() {
            if ($get('TaiKhoan_Chuyen').value.length > 0) {
                document.getElementById('divStatus').innerHTML = '';
                PageMethods.GetTEN_KH_NH($get('TaiKhoan_Chuyen').value.trim(), GetTEN_KH_NH_Complete, GetTEN_KH_NH_Error);
            } else {
                document.getElementById('divStatus').innerHTML = 'Tài khoản khách hàng không được để trống';
            }
        }

    function GetTEN_KH_NH_Complete(result, methodName) {
        if (result.length > 0) {
            $get('Ten_TaiKhoan_Chuyen').value = result;
        } else {
            document.getElementById('divStatus').innerHTML = 'Không lấy được tên tài khoản khách hàng';
            $get('Ten_TaiKhoan_Chuyen').value = '';
        }
    }
    function GetTEN_KH_NH_Error(error, userContext, methodName) {
        if (error !== null) {
            $get('Ten_TaiKhoan_Chuyen').value = '';
            document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
        }
    }
        
        
        function jsGetTK_KH_NH()
        {
            var vmChungTu = 
                    {
                                 
                        tk_kh_nh : $("#TaiKhoan_Chuyen").val()
                    };
                         
                    $.ajax({
                    type: "POST",
                    url: "frmLapPhiBoNganh.aspx/GET_TK_KH_NH",
                    data: JSON.stringify(vmChungTu),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {                     
                           //redraw => 
//                             console.log(data);   
//                            var modelGen=JSON.parse(data.d);    
//                            var desc =  modelGen.DescAccount;                               
//                            $("#Ten_TaiKhoan_Chuyen").val(modelGen.DescAccount);                                
//                            $("#So_Du").val(modelGen.Balance);
                                var xmlstring = data.d;
                                var xmlDoc = jsGetXMLDoc(xmlstring);
                                ProcessTK_KH_NH(xmlDoc);
                            
                        }
                    });
        }
        
            //XML to object
    function jsGetXMLDoc(xmlString)
    {
        var xmlDoc;
        //if (xmlString==null||xmlString.length==0) return;
        try //Internet Explorer
        {
            //alert (xmlString);
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async="false";
            xmlDoc.loadXML(xmlString);
            return xmlDoc; 
        }
        catch(e)
        {
            parser=new DOMParser();
            xmlDoc=parser.parseFromString(xmlString,"text/xml");
            return xmlDoc;
        }
    }
        
        function ProcessTK_KH_NH(xmlDoc)    
        {                       
            $get('So_Du').value ='';
            $get('Ten_TaiKhoan_Chuyen').value ='';
           var rootCTU_HDR = xmlDoc.getElementsByTagName('PARAMETER')[0];  
            if (rootCTU_HDR!=null)
           {                                                   
                if (rootCTU_HDR.getElementsByTagName("PARAMS")[0].firstChild!=null)
                {
                    if (rootCTU_HDR.getElementsByTagName("ERRCODE")[0].firstChild.nodeValue=="00000")
                    {
                	    if (rootCTU_HDR.getElementsByTagName("ACY_AVL_BAL")[0].firstChild!=null) $get('So_Du').value = xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].firstChild.nodeValue;        
                	    $('#So_Du').val(accounting.formatNumber($get('So_Du').value));
                	    //if (rootCTU_HDR.getElementsByTagName("AccountName")[0].firstChild!=null) $get('Ten_TaiKhoan_Chuyen').value = xmlDoc.getElementsByTagName("AccountName")[0].firstChild.nodeValue;
                	    //if (rootCTU_HDR.getElementsByTagName("Currency")[0].firstChild != null) $get('txtMaNT').value = xmlDoc.getElementsByTagName("Currency")[0].firstChild.nodeValue;                  	
                	    //if (rootCTU_HDR.getElementsByTagName("Limit")[0].firstChild!=null) flag_ThauChi = xmlDoc.getElementsByTagName("Limit")[0].firstChild.nodeValue;
                	    //alert("tk thau chi: " + flag_ThauChi);
                    }else if(rootCTU_HDR.getElementsByTagName("ERRCODE")[0].firstChild.nodeValue!="00000") {
                            document.getElementById('divStatus').innerHTML="Tài khoản không đúng";
                    }else  {
                        document.getElementById('divStatus').innerHTML="Lỗi trong quá trình lấy tài khoản ngân hàng.Mô tả : " + xmlDoc.getElementsByTagName("ResponseCode")[0].firstChild.nodeValue + "-" + xmlDoc.getElementsByTagName("Description")[0].firstChild.nodeValue;
                    }
                }       	
            }              
        }
    
        function jsShowCustomerSignature()
        {        
            if  ($('#TaiKhoan_Chuyen').val() !='' )
            {
                window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $('#TaiKhoan_Chuyen').val(), "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
            }
            else{
                 alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');}
        }
        
        function disableAllFields()
        {
            $("#Ma_NH_PH").attr('disabled','disabled');           
            $("#Ten_NH_PH").attr('disabled','disabled');          
            $("#txtLyDoHuy").attr('disabled','disabled');            
            $("#TaiKhoan_Chuyen").attr("disabled","disabled");
            $("#PTTT").attr("disabled","disabled");
            $("#NNT_QUAN_HUYEN").attr("disabled","disabled");
            $("#NNT_TINH_TP").attr("disabled","disabled");
            $("#ddl_NganHang").attr("disabled","disabled");
            $("#TT_NT_So_CMND").attr("disabled","disabled");
            $("#TT_NT_So_DT").attr("disabled","disabled");
            $("#TT_NT_TK_THANH_TOAN_QUAN_HUYEN").attr("disabled","disabled");
            $("#TT_NT_TK_THANH_TOAN_TINH_TP").attr("disabled","disabled");
            $("#TaiKhoan_Chuyen").attr("disabled","disabled");
            $("#Ten_TaiKhoan_Chuyen").attr("disabled","disabled");            
            $("#So_Du").attr("disabled","disabled");
            $("#TT_NT_So_CMND").attr("disabled","disabled");
            $("#TT_NT_So_DT").attr("disabled","disabled");
            $("#TK_Thanh_Toan_DiaChi").attr("disabled","disabled");
            $("#TT_NT_TK_THANH_TOAN_QUAN_HUYEN").attr("disabled","disabled");
            $("#TT_NT_TK_THANH_TOAN_TINH_TP").attr("disabled","disabled");
            $("#TaiKhoan_Chuyen").attr("disabled","disabled");
            $("#Ten_TaiKhoan_Chuyen").attr("disabled","disabled");
            $("#Ma_NT").attr("disabled","disabled");
            $("#So_Du").attr("disabled","disabled");
            $("#TyGia").attr("disabled","disabled");
            $("#TongTien_NT").attr("disabled","disabled");
            $("#txtTongTien").attr("disabled","disabled");
            $("#Ma_NH_TH").attr("disabled","disabled");
            $("#Ten_NH_TH").attr("disabled","disabled");
            $("#TaiKhoan_TH").attr("disabled","disabled");
            $("#Ten_TaiKhoan_TH").attr("disabled","disabled");
            $("#txtTaiKhoan_TT").attr("disabled","disabled");
            $("#txtTen_TaiKhoan_TT").attr("disabled","disabled");
        }
        
        function enableFunction()
        {
            $("#Ma_NH_PH").prop('disabled',false);
            $("#Ten_NH_PH").prop('disabled',false);            
            $("#txtLyDoHuy").prop('disabled',false);
            $("#TaiKhoan_Chuyen").attr("disabled",false);
            $("#PTTT").attr("disabled",false);
            $("#NNT_QUAN_HUYEN").attr("disabled",false);
            $("#NNT_TINH_TP").attr("disabled",false);
            $("#ddl_NganHang").attr("disabled",false);
            $("#TT_NT_So_CMND").attr("disabled",false);
            $("#TT_NT_So_DT").attr("disabled",false);
            $("#TT_NT_TK_THANH_TOAN_QUAN_HUYEN").attr("disabled",false);
            $("#TT_NT_TK_THANH_TOAN_TINH_TP").attr("disabled",false);
            $("#TaiKhoan_Chuyen").attr("disabled",false);
            $("#Ten_TaiKhoan_Chuyen").attr("disabled",false);            
            $("#So_Du").attr("disabled",false);
            $("#Ma_NT").attr("disabled",false);
            $("#So_Du").attr("disabled",false);
            $("#TyGia").attr("disabled",false);
            $("#TongTien_NT").attr("disabled",false);
            $("#txtTongTien").attr("disabled",false);
            $("#Ma_NH_TH").attr("disabled",false);
            $("#Ten_NH_TH").attr("disabled",false);
            $("#TaiKhoan_TH").attr("disabled",false);
            $("#Ten_TaiKhoan_TH").attr("disabled",false);
            $("#txtTaiKhoan_TT").attr("disabled",false);
            $("#txtTen_TaiKhoan_TT").attr("disabled",false);
        }
        
        function resetAttribute()
        {
            listAdded =[];      
            currentList.length=0;
            listAdded.length=0;     
            $("#divStatus").empty();
            enablePrint = false;
            currentSoCT = '0';
            currentCode = '-1';
            type =0;
            $("#Ma_NH_PH").val("");
            $("#Ten_NH_PH").val("");
            $("#So_HS").val("");
            $("#Ten_NH_PH").val("");
            $("#Ma_DVQL").val("");
            $("#Ten_DVQL").val("");
            $("#KyHieu_CT").val("");
            $("#So_CT").val("");
            $("#Nam_CT").val("");
            $("#MST").val("");
            $("#Ten_NNT").val("");
            $("#DiaChi").val("");
            $("#TT_Khac").val("");
            $("#Ma_NT").val("");
            $("#TyGia").val("");
            $("#TongTien_NT").val("");
            $("#txtTongTien").val("");
            $("#ChungTu_NDKT").val("");
            $("#ChungTu_Ten_NDKT").val("");
            $("#ChungTu_SoTien_NT").val("");
            $("#ChungTu_SoTienVND").val("");
            $("#ChungTu_GhiChu").val("");       
            $("#txtLyDoHuy").val("");            
            $("#NNT_QUAN_HUYEN").val("");   
            $("#NNT_TINH_TP").val("");   
            $("#TT_NT_So_CMND").val("");   
            $("#TT_NT_So_DT").val("");   
            $("#TT_NT_TK_THANH_TOAN_QUAN_HUYEN").val("");   
            $("#TT_NT_TK_THANH_TOAN_TINH_TP").val("");   
            $("#TaiKhoan_Chuyen").val("");   
            $("#Ten_TaiKhoan_Chuyen").val("");   
            $("#So_Du").val("");      
            $("#TK_Thanh_Toan_DiaChi").val(""); 
            
            $("#Ma_NH_TH").val("");   
            $("#Ten_NH_TH").val("");   
            $("#TaiKhoan_TH").val("");   
            $("#Ten_TaiKhoan_TH").val("");   
            
            $("#txtTaiKhoan_TT").val("");
            $("#txtTen_TaiKhoan_TT").val("");
            $('#divSo_CT').text("");
            
            //button them moi click
            
            $("#textBoxSearchSo_Hoso").val("");
            $("#textBoxSearchMa_DVQL").val("");
            $("#textBoxSearchKyHieu_CT").val("");
            $("#textBoxSearchSo_CT").val("");
            $("#textBoxSearchNam_CT").val("");            
            $("#PTTT").val($("#PTTT option:first").val());
            $("#PTTT").attr("disabled",false);
            showHidePTTT(1);
            $("#txtTongTien").val(0);            
            $("#txtCharge").val(0);            
            $("#txtVAT").val(0);            
            $("#txtTongTrichNo").val(0);            
                
            $('#labelMoneyByText').text("");    
            $("#divDraw").children().remove();            
        }
        
        function getTTCT(code, tt_xuly)
        {            
            var textReturn ='';
            if(code=="00")
            {
                textReturn="../../images/icons/ChuaKS.png";
            }
            if(code=="01" && tt_xuly==1) // Xu ly = 0 => Sao. If 1 => Tick
            {
                textReturn="../../images/icons/DaKS.png";
            }
            if(code=="01" && tt_xuly==0) // Xu ly = 0 => Sao. If 1 => Tick
            {
                textReturn="../../images/icons/TT_CThueLoi.gif";
            }
            if(code=="02")
            {
                textReturn="../../images/icons/HuyHs.gif";
            }
            if(code=="03")
            {
                textReturn="../../images/icons/KSLoi.png";
            }
            if(code=="04")
            {
                textReturn="../../images/icons/Huy.png";
            }
            if(code=="05")
            {
                textReturn="../../images/icons/ChuyenKS.png";
            }
            if(code=="06")
            {
                textReturn="../../images/icons/ChuaKS.png";
            }
            if(code=="07")
            {
                textReturn="../../images/icons/Huy_KS.png";
            }
             else if(code=="08")
            {
                textReturn="../../images/icons/Huy_CT_Loi.png";
            }
            return textReturn;
        }
        
        function showHidePTTT(pttt)
        {
            if (pttt==3)
            {
                $("#tableRowQuanHuyen").hide();
                $("#rowTKGL").show();
                $("#tableRowTK").hide();
                $("#tableRowSoDu").hide();
                $("#TaiKhoan_Chuyen").val("");
                $("#Ten_TaiKhoan_Chuyen").val("");
                $("#So_Du").val("");
            }
            else 
            {
                $("#tableRowQuanHuyen").hide();
                $("#rowTKGL").hide();
                $("#tableRowTK").show();
                $("#tableRowSoDu").show();
                $("#txtTKGL").val("");
                $("#txtTenTKGL").val("");
            }
        }
      
        function buttonState(state)
        {
            if(state=="00")
            {
                enableFunction();
                
                $("#btnThemMoi").attr("disabled",false);
                $("#btnLuu").attr("disabled",true);
                $("#btnGhiKS").attr("disabled",false);
                $("#btnHuy").attr("disabled",false);
                $("#btnInCT").attr("disabled",true);
                
            }
            if(state=="05")
            {
                $("#btnThemMoi").attr("disabled",false);
                $("#btnLuu").attr("disabled",true);
                $("#btnGhiKS").attr("disabled",true);
                $("#btnHuy").attr("disabled",true);
                $("#btnInCT").attr("disabled",false);
                disableAllFields();
            }
            if(state=="01")
            {
              $("#btnThemMoi").attr("disabled",false);
                $("#btnLuu").attr("disabled",true);
                $("#btnGhiKS").attr("disabled",true);
                $("#btnHuy").attr("disabled",false);
                $("#btnInCT").attr("disabled",false);
                enableFunction();
            }
            if(state=="04")
            {
               $("#btnThemMoi").attr("disabled",false);
                $("#btnLuu").attr("disabled",true);
                $("#btnGhiKS").attr("disabled",true);
                $("#btnHuy").attr("disabled",true);
                $("#btnInCT").attr("disabled",true);
                enableFunction();
            }
            if(state=="02")
            {
             $("#btnThemMoi").attr("disabled",false);
                $("#btnLuu").attr("disabled",true);
                $("#btnGhiKS").attr("disabled",true);
                $("#btnHuy").attr("disabled",true);
                $("#btnInCT").attr("disabled",false);
                enableFunction();
            }
            if(state=="03")
            {
              $("#btnThemMoi").attr("disabled",false);
                $("#btnLuu").attr("disabled",false);
                $("#btnGhiKS").attr("disabled",false);
                $("#btnHuy").attr("disabled",false);
                $("#btnInCT").attr("disabled",true);
                enableFunction();
            }
            
            if(state=="-1")
            {
                $("#btnThemMoi").attr("disabled",false);
                $("#btnLuu").attr("disabled",false);
                $("#btnGhiKS").attr("disabled",false);
                $("#btnHuy").attr("disabled",false);
                $("#btnInCT").attr("disabled",false);
                enableFunction();
            }
        }
        
        function valInteger(obj) {
        var i, strVal, blnChange;
        blnChange = false
        strVal = "";

        for (i = 0; i < (obj.value).length; i++) {
            switch (obj.value.charAt(i)) {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": strVal = strVal + obj.value.charAt(i);
                    break;
                default: blnChange = true;
                    break;
            }
        }
        if (blnChange) {
            obj.value = strVal;
        }
    }
    function valInteger_MaST(obj) {
        var i, strVal, blnChange;
        blnChange = false
        strVal = "";

        for (i = 0; i < (obj.value).length; i++) {
            switch (obj.value.charAt(i)) {
                case "-":
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": strVal = strVal + obj.value.charAt(i);
                    break;
                default: blnChange = true;
                    break;
            }
        }
        if (blnChange) {
            obj.value = strVal;
            
        }
    }
    
        
        function jsFormatNumber(txtCtl){
        document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.',''),'.');
    }
         
    function localeNumberFormat(amount,delimiter) {
	    try {
	   
		    amount = parseFloat(amount);
	    }catch (e) {
		    throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
		    return null;
	    }
	    if (delimiter == null || delimiter == undefined) { delimiter = '.'; }
	    
	    // convert to string
	    if (amount.match != 'function') { amount = amount.toString(); }

	    // validate as numeric
	    var regIsNumeric = /[^\d,\.-]+/igm;
	    var results = amount.match(regIsNumeric);

	    if (results != null) {
		    outputText('INVALID NUMBER', eOutput)
		    return null;
	    }

	    var minus = amount.indexOf('-') >= 0 ? '-' : '';
	    amount = amount.replace('-', '');
	    var amtLen = amount.length;
	    var decPoint = amount.indexOf(',');
	    var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

	    var wholeNumber = amount.substr(0, wholeNumberEnd);
	    
	    var numberEnd=amount.substr(decPoint,amtLen);
	    var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

	    var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
	    var rvsNumber = wholeNumber.split('').reverse().join('');
	    var output = '';

	    for (i = 0; i < wholeNumberEnd; i++) {
		    if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
		    output += rvsNumber.charAt(i);
	    }
	    output = minus +  output.split('').reverse().join('') + fraction ;
	    return output;
    }    
       
    String.prototype.replaceAll = function(strTarget,strSubString)
    {
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    } 
 
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
	        for (var k = 0; k <= str.length; k++){
	            if (k == locs[i]){
	            if (str.substring(k, k+1) != delim){
	                    str = str.substring(0,k) + delim + str.substring(k,str.length)
	                }
	            }
	        }
        }
        textbox.value = str
    }
        
    function jsCalCharge()
        {
            
           if( $('#txtCharge').val()=="") $('#txtCharge').val("0");
           var dblPhi = parseFloat ($('#txtCharge').val().replaceAll('.',''));
           
           var dblVAT = dblPhi/10;
           
           $('#txtVAT').val(dblVAT.toString().replace('.',','));               
           var dblTongTien = parseFloat ($('#txtTongTien').val().replaceAll('.',''));          
           var dblTongTrichNo = dblPhi + dblVAT + accounting.unformat(dblTongTien);
          
           $('#txtTongTrichNo').val(dblTongTrichNo.toString().replace('.',','));
           jsFormatNumber2($('#txtVAT').val()); 
           jsFormatNumber2($('#txtTongTrichNo').val());  
           jsFormatNumber2($('#txtCharge').val());
        }
        
    
 
        
        $(document).ready(function(){   
            $("#NgayLap_CT").val(getCurrentDate());   
            getBankTransfer();
            $("#pager").remove();
            $("#pager").empty();
            $("#tongCT").empty();
            getListNDKT();
            loadListChungTu("-1","-1"); 
            $("#TaiKhoan_Chuyen").attr("disabled","disabled");            
            
            $("#ddlTrangThaiCT").change(function(){
                $("#tongCT").empty();
                $("#pager").empty();
                $("#pager").remove();
                $("#tbodyListCT").empty();
                var tsort= this.value;                
                if(tsort=="06")
                {
                    loadListChungTu("01","0");
                }
                else if(tsort=="01")
                {
                    loadListChungTu(this.value,"-1");
                }
                else
                {
                    loadListChungTu(this.value,"-1");
                }
            });
            
            showHidePTTT($("#PTTT option:first-child").val());
            $("#PTTT").change(function(){
                $("#TaiKhoan_Chuyen").val("");               
                showHidePTTT(this.value);
            });
            
            listTyGia("");
            
            
        });
    </script>

</asp:Content>
