﻿Imports Business_HQ
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business_HQ.NewChungTu
Imports System.Xml
Imports GIPBankService
Imports System.IO



Partial Class pages_ChungTu_frmTraCuuTTHAI_GD_HQ
    Inherits System.Web.UI.Page
    Private mv_objUser As New CTuUser
    Private Shared strMaNhom_GDV As String = ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then

        End If
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))
        End If

        divKQXK.Visible = False

    End Sub

    Protected Sub btnTraCuu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTraCuu.Click

        Dim trans_id As String = ""
        If txtSoCT.Value.Trim.Length <= 0 Then
            clsCommon.ShowMessageBox(Me, "Số chứng từ không được để trống")
            Exit Sub
        End If
        trans_id = HaiQuan.HaiQuan.DCHIEU_NHHQ_GET_TRANSID(txtSoCT.Value)

        'If trans_id.Length <= 0 Then
        '    clsCommon.ShowMessageBox(Me, "Số chứng từ không tồn tại")
        '    Exit Sub
        'End If

        Dim strXmlResult As String = "" '"<?xml version="1.0" encoding="UTF-8"?><CUSTOMS> <Header> <Application_name>Tên ứng dụng gửi thông tin</Application_name> <Application_vesion>1.0</Application_vesion> <Sender_Code>12345678</Sender_Code> <Sender_Name>Ngân hàng XYZ</Sender_Name> <Message_Version>2.0</Message_Version> <Message_type>205</Message_type> <Message_name>Thông điệp truy vấn số thuế phải thu</Message_name> <Transaction_Date>2014-09-26 16:52:20</Transaction_Date> <Transaction_ID>1</Transaction_ID> <Request_ID>số tham chiếu giao dịch</Request_ID> </Header> <Data> <Ma_HQ>0105774554</Ma_HQ> <Ten_HQ>1601000234</Ten_HQ> <Ma_LH>Ký hiệu chứng từ bảo lãnh chung</Ma_LH> <Ma_XN>2016-02-02</Ma_XN> <Ngay_DK>2016-02-02</Ngay_DK> <SO_TK>2016-02-02</SO_TK> <SO_TK_DAU_TIEN>A12</SO_TK_DAU_TIEN> <SO_TK_TAM_NHAP_TX>D543</SO_TK_TAM_NHAP_TX> <THOI_HAN_TAI_NHAP_TAI_XUAT>2016-02-02 14:00:00</THOI_HAN_TAI_NHAP_TAI_XUAT> <NGAY_THAY_DOI_DK>2016-02-02 14:00:00</NGAY_THAY_DOI_DK> <NGAY_THAY_DOI_KT>2016-02-02 14:00:00</NGAY_THAY_DOI_KT> <NGAY_HOAN_THANH_KT>2016-02-02 14:00:00</NGAY_HOAN_THANH_KT> <NGAY_THONG_QUAN>2016-02-02 14:00:00</NGAY_THONG_QUAN> <NGAY_HH_QUA_KVGS>2016-02-02 14:00:00</NGAY_HH_QUA_KVGS> <MA_PHAN_LOAI_KT>1.Xanh</MA_PHAN_LOAI_KT> <NGUOI_XUAT_KHAU> <MA_SO_THUE>0105774554</MA_SO_THUE> <TEN>công ty thương mại cổ phần AC</TEN> <DIA_CHI_1>địa chỉ người xuất nhập khẩu</DIA_CHI_1> </NGUOI_XUAT_KHAU> <NGUOI_UY_THAC_XNK> <MA_SO_THUE>0105774554</MA_SO_THUE> <TEN>công ty thương mại cổ phần gdsh</TEN> </NGUOI_UY_THAC_XNK> <NGUOI_NHAP_KHAU> <MA_SO_THUE>0105774554</MA_SO_THUE> <TEN>công ty thương mại cổ phần AC</TEN> <DIA_CHI_1>địa chỉ người xuất nhập khẩu</DIA_CHI_1> <DIA_CHI_2>Địa chỉ</DIA_CHI_2> <DIA_CHI_3>địa chỉ</DIA_CHI_3> <DIA_CHI_4>Địa chỉ</DIA_CHI_4> <MA_NUOC>648q</MA_NUOC> </NGUOI_NHAP_KHAU> <MA_DAI_LY_HQ>Mã đại lý hải quan</MA_DAI_LY_HQ> <TEN_DAI_LY_HQ>tên đại lý hải quan</TEN_DAI_LY_HQ> <SO_VAN_DON_1>só vận đơn</SO_VAN_DON_1> <SO_LUONG>số lượng</SO_LUONG> <DVT_SO_LUONG>Đơn vị tính số lượng</DVT_SO_LUONG> <TONG_TRONG_LUONG>36838</TONG_TRONG_LUONG> <DVT_TONG_TRONG_LUONG>kg</DVT_TONG_TRONG_LUONG> <MA_DIA_DIEM_LUU_KHO>Q333</MA_DIA_DIEM_LUU_KHO> <TEN_DIA_DIEM_LUU_KHO>thanh xuân</TEN_DIA_DIEM_LUU_KHO> <MA_DIA_DIEM_NHAN_HANG_CUOI_CUNG>D245</MA_DIA_DIEM_NHAN_HANG_CUOI_CUNG> <TEN_DIA_DIEM_NHAN_HANG_CUOI_CUNG>minh khai</TEN_DIA_DIEM_NHAN_HANG_CUOI_CUNG> <MA_DIA_DIEM_XEP_HANG>D6734</MA_DIA_DIEM_XEP_HANG> <TEN_DIA_DIEM_XEP_HANG>từ liêm</TEN_DIA_DIEM_XEP_HANG> <MA_PHUONG_TIEN_VC>P47378</MA_PHUONG_TIEN_VC> <TEN_PHUONG_TIEN_VC>tên phương tiện vận chuyển</TEN_PHUONG_TIEN_VC> <SO_HOA_DON>747</SO_HOA_DON> <NGAY_PHAT_HANH>2016-02-02 14:00:00</NGAY_PHAT_HANH> <PHUONG_THUC_THANH_TOAN>chuyển khoản</PHUONG_THUC_THANH_TOAN> <TONG_TRI_GIA_HOA_DON>9854784</TONG_TRI_GIA_HOA_DON> <NGUYEN_TE_TONG_TRI_GIA_HOA_DON>USD</NGUYEN_TE_TONG_TRI_GIA_HOA_DON> <TONG_TRI_GIA_TINH_THUE>tổng giá trị tính thuế</TONG_TRI_GIA_TINH_THUE> <GIAY_PHEP_XUAT_NHAP_KHAU_1>Giấy phép nhập khẩu</GIAY_PHEP_XUAT_NHAP_KHAU_1> <GIAY_PHEP_XUAT_NHAP_KHAU_2>Giấy phép nhập khẩu</GIAY_PHEP_XUAT_NHAP_KHAU_2> <GIAY_PHEP_XUAT_NHAP_KHAU_3>Giấy phép nhập khẩu</GIAY_PHEP_XUAT_NHAP_KHAU_3> <GIAY_PHEP_XUAT_NHAP_KHAU_4>Giấy phép nhập khẩu</GIAY_PHEP_XUAT_NHAP_KHAU_4> <GIAY_PHEP_XUAT_NHAP_KHAU_5>Giấy phép nhập khẩu</GIAY_PHEP_XUAT_NHAP_KHAU_5> <MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_1>Mã Địa điểm xếp hàng lên xe chở hàng</MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_1> <MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_2>Mã Địa điểm xếp hàng lên xe chở hàng</MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_2> <MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_3>Mã Địa điểm xếp hàng lên xe chở hàng</MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_3> <MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_4>Mã Địa điểm xếp hàng lên xe chở hàng</MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_4> <MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_5>Mã Địa điểm xếp hàng lên xe chở hàng</MA_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG_5> <TEN_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG></TEN_DIA_DIEM_XEP_HANG_LEN_XE_CHO_HANG> <TT_HANG> <HANG> <MA_HANG>A434</MA_HANG> <TEN_HANG>tên hàng</TEN_HANG> <THUE_XUAT_KHAU> <TRI_GIA_TINH_THUE>223</TRI_GIA_TINH_THUE> <TIEN_THUE>35464</TIEN_THUE> <MA_TIEN_TE>VND</MA_TIEN_TE> </THUE_XUAT_KHAU> </HANG> </TT_HANG> </Data> <Error> <ErrorMessgase>hdfjhd</ErrorMessgase> <ErrorNumber>5555</ErrorNumber> </Error> <Signature> <SignedInfo> <CanonicalizationMethod>Phương thức chuẩn hoá dữ liệu</CanonicalizationMethod> <SignatureMethod>Thuật toán được sử dụng để ký số</SignatureMethod> <Reference> <Transforms> <Transform>Thuật toán được sử dụng để chuyển đổi thông điệp</Transform> </Transforms> <DigestMethod>Thuật toán sử dụng để băm</DigestMethod> <DigestValue>Giá trị của hàm băm theo thuật toán sha1</DigestValue> </Reference> </SignedInfo> <SignatureValue>Chữ ký số trên thông điệp</SignatureValue> <KeyInfo> <X509Data> <X509IssuerSerial> <X509IssuerName>Người được cấp chứng thư số</X509IssuerName> <X509SerialNumber>1223455677</X509SerialNumber> </X509IssuerSerial> <X509Certificate>Chứng thư số</X509Certificate> </X509Data> </KeyInfo> </Signature> </CUSTOMS>"
        Dim err_num As String = ""
        Dim err_desc As String = ""
        Try
            'strXmlResult = txtTestMSG.Text
            strXmlResult = CustomsServiceV3.ProcessMSG.getMSG107(trans_id, err_num, err_desc)

            If Not err_num.Equals("0") Then
                clsCommon.ShowMessageBox(Me, "Không tìm thấy tờ khai: " & err_desc)
                Exit Sub
            End If

            If strXmlResult.Trim().Length > 0 Then
                Dim msg_type As String = ""
                Dim ds As DataSet = New DataSet()
                Dim strReader As New StringReader(strXmlResult)
                ds.ReadXml(strReader)
                Dim dtHeader As DataTable = New DataTable()
                dtHeader = ds.Tables("Header")
                If dtHeader.Rows.Count > 0 Then
                    msg_type = dtHeader.Rows(0)("Message_type").ToString()
                End If
                Dim dtData As DataTable = New DataTable()
                dtData = ds.Tables("Data")

                divKQXK.Visible = True

                If dtData.Rows.Count > 0 Then
                    txtSo_TN_CT.Value = dtData.Rows(0)("So_TN_CT").ToString()
                    txtNgay_TN_CT.Value = dtData.Rows(0)("Ngay_TN_CT").ToString()
                    txtKQ_TraCuu.Value = dtData.Rows(0)("KQ_TraCuu").ToString()

                End If

            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi khi truy trạng thái giao dịch hải quan: " & ex.Message)
        End Try

    End Sub
End Class
