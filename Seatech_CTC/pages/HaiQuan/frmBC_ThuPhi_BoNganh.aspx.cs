﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Services;
using CustomsV3.MSG.MSG303LePhi;
using Newtonsoft.Json;
using Business_HQ;
using Business_HQ.HaiQuan;
using Business.BaoCao;

public partial class pages_HaiQuan_frmBC_ThuPhi_BoNganh : System.Web.UI.Page
{
    public CTuUser mv_objUser = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["User"] != null)
        {
            mv_objUser = new CTuUser(Session["User"].ToString());
        }
        else
        {
            Response.Redirect("~/pages/frmLogIn.aspx", false);
        }
    }

    [WebMethod]
    public static string getDanhMucChiNhanh()
    {
        CTuUser mv_objUser = new CTuUser(HttpContext.Current.Session["User"].ToString());
        var strCheckHSC = checkHSC();
        var dsDMChiNhanh = HaiQuan.GET_DM_CHINHANHFORPHIBONGANH(mv_objUser.MA_CN, strCheckHSC);
        if (dsDMChiNhanh == null)
        {
            return "Không tìm thấy bản ghi nào";
        }
        else
        {
            string strChiNhanh = JsonConvert.SerializeObject(dsDMChiNhanh, Formatting.Indented);
            return strChiNhanh;

        }
    }


    public static string checkHSC()
    {
        string returnValue = "";
        CTuUser mv_objUser = new CTuUser(HttpContext.Current.Session["User"].ToString());
        if (mv_objUser != null)
        {
            var ds = buBaoCao.checkHSC(mv_objUser.Ma_NV.ToString());
            if (ds != null)
            {
                returnValue = ds.Tables[0].Rows[0]["phan_cap"].ToString();
            }
        }
        return returnValue;
    }

    [WebMethod]
    public static string getPhiBoBanNganh(string SOHOSO, string TUNGAY, string DENNGAY, string MST, string NAM_CT, string TRANGTHAI_CT, string MA_CN, string ROOT)
    {
        //MSG303LePhi msg303LePhi = new MSG303LePhi();
        TCS_IN_BC_BO_BAN_NGANH tcs_in_bc_bo_ban_nganh = new TCS_IN_BC_BO_BAN_NGANH();
        CTuUser mv_objUser = new CTuUser(HttpContext.Current.Session["User"].ToString());
        tcs_in_bc_bo_ban_nganh.SOHOSO = SOHOSO;
        tcs_in_bc_bo_ban_nganh.TUNGAY = TUNGAY;
        tcs_in_bc_bo_ban_nganh.DENNGAY = DENNGAY;
        tcs_in_bc_bo_ban_nganh.MST = MST;
        tcs_in_bc_bo_ban_nganh.NAM_CT = NAM_CT;
        tcs_in_bc_bo_ban_nganh.TRANGTHAI_CT = TRANGTHAI_CT;
        tcs_in_bc_bo_ban_nganh.MA_CN = MA_CN;
        tcs_in_bc_bo_ban_nganh.RootID = ROOT;
        string tt_ct = "0";
        if (TRANGTHAI_CT == "06")
        {
            TRANGTHAI_CT = "01";
            tcs_in_bc_bo_ban_nganh.TRANGTHAI_CT = "01";
            tt_ct = "0";
        }
        else if (TRANGTHAI_CT == "01")
        {
            tt_ct = "-1";
            tcs_in_bc_bo_ban_nganh.TRANGTHAI_CT = "01";
        }
        else
        {
            tt_ct = "-1";
        }
        tcs_in_bc_bo_ban_nganh.TT_CT = tt_ct;
        tcs_in_bc_bo_ban_nganh.MA_NV = mv_objUser.Ma_NV;
        var dsPhiBoNganh = daLePhiHaiQuan.MSG303BaoCaoPhiBoBanNganh(tcs_in_bc_bo_ban_nganh);
        if (dsPhiBoNganh == null || dsPhiBoNganh.Tables == null || dsPhiBoNganh.Tables[0].Rows.Count == 0)
        {
            return JsonConvert.SerializeObject("Không tìm thấy bản ghi nào", Formatting.Indented);
        }
        else
        {
            string strPhiBoNganh = JsonConvert.SerializeObject(dsPhiBoNganh, Formatting.Indented);
            return strPhiBoNganh;

        }
    }

}
