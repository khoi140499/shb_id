﻿Imports System.Web.Services
Imports Business_HQ
Imports Business_HQ.HaiQuan
Imports CustomsV3.MSG.MSG303LePhi
Imports Newtonsoft.Json
Imports CustomsV3.MSG.MSG103
Imports CustomsServiceV3.Extension
Imports System.Data
Imports CustomsServiceV3
Imports VBOracleLib
Imports System.Configuration

Partial Class pages_HaiQuan_frmLapPhiBoNganh
    Inherits System.Web.UI.Page

    Public mv_objUser As CTuUser = Nothing
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("User") IsNot Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))
        Else
            Response.Redirect("~/pages/frmLogIn.aspx", False)
        End If

        If Not IsPostBack Then
            If (Not ClientScript.IsStartupScriptRegistered(Me.GetType(), "InitArrTKGL")) Then
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKGL", PopulateArrTKGL(), True)
            End If
        End If

        'Dim vCore As New clsCoreBankUtilMSB()
        'If vCore.PaymentCA(mv_objUser.MA_CN & DateTime.Now.Month.ToString() & DateTime.Now.Day.ToString() & DateTime.Now.Second, _
        '                    "03", mv_objUser.MA_CN, "11001019933792", "150000", "VND", "1", "200000", "10", "11001017127896", _
        '                     mv_objUser.Ma_NV.ToString(), mv_objUser.Ma_NV.ToString(), "", "Nộp tiền") Then

        'End If

    End Sub

    ''' <summary>
    ''' Lấy danh sách chứng từ đã lưu
    ''' Thông tin bao gồm chứng từ 05 từ các ngày trước và chứng từ trong ngày hiện tại    
    ''' </summary>
    ''' <returns></returns>
    <WebMethod()> _
    Public Shared Function GetListChungTu(ByVal trangthai As String, ByVal tt_ct As String) As String
        If trangthai = "-1" Then
            trangthai = ""
        End If
        Dim mv_objUser As New CTuUser(HttpContext.Current.Session("User").ToString())
        Dim ds = HaiQuan.GET_DS_CT_PHI_BO_NGANH(mv_objUser.Ngay_LV, trangthai, HttpContext.Current.Session("User").ToString(), String.Empty, tt_ct, mv_objUser.MA_CN)
        ' daLePhiHaiQuan.MSG303GetDSChungTu(trangthai);
        Dim jsonChungTu As String = String.Empty
        If ds IsNot Nothing AndAlso ds.Tables(0) IsNot Nothing Then
            jsonChungTu = JsonConvert.SerializeObject(ds)
        Else
            jsonChungTu = "Error"
        End If
        Return jsonChungTu
    End Function

    <WebMethod()> _
    Public Shared Function getListTyGiaNT() As String
        Dim listTyGia = HaiQuan.ListTyGia()
        Dim jsonTyGia As String = String.Empty

        If listTyGia IsNot Nothing Then
            jsonTyGia = JsonConvert.SerializeObject(listTyGia)
        Else
            jsonTyGia = "Error"
        End If
        Return jsonTyGia

    End Function

    <WebMethod()> _
    Public Shared Function getListChiTietBySoCT(ByVal So_CT As String) As String
        Dim ds = HaiQuan.ListChiTietBySoCT(So_CT)
        Dim jsonChungTu As String = String.Empty

        If ds IsNot Nothing AndAlso ds.Tables(0) IsNot Nothing Then
            jsonChungTu = JsonConvert.SerializeObject(ds)
        Else
            jsonChungTu = "Error"
        End If
        Return jsonChungTu

    End Function

    <WebMethod()> _
    Public Shared Function getListNDKT() As String
        Dim ds = HaiQuan.ListNDKT()
        Dim jsonNDKT As String = String.Empty

        If ds IsNot Nothing Then
            jsonNDKT = JsonConvert.SerializeObject(ds)
        Else
            jsonNDKT = "Error"
        End If
        Return jsonNDKT

    End Function


    <WebMethod()> _
    Public Shared Function getBankNameByID(ByVal bankNo As String) As String
        Return "Ngân hàng ACB"
    End Function

    <WebMethod()> _
    Public Shared Function getListBankToTransfer() As String
        Dim ds = daLePhiHaiQuan.GetListBankToTransfer()
        Dim jsonChungTu As String = String.Empty
        Dim strCountryList As String = "{"
        If ds IsNot Nothing AndAlso ds.Tables(0) IsNot Nothing Then
            Dim dataTable As System.Data.DataTable = ds.Tables(0)

            For Each item As DataRow In dataTable.Rows

                strCountryList += (""""c + item("MA_NH_CHUYEN").ToString() + """"c + ":" + """"c + item("TEN_NH_CHUYEN") + """"c + ",")
            Next
            strCountryList = strCountryList.Remove(strCountryList.Length - 1)

            'jsonChungTu = JsonConvert.SerializeObject(ds);
            strCountryList += "}"
        Else
            strCountryList = "Error"
        End If
        Return strCountryList
    End Function

    <WebMethod()> _
    Public Shared Function getVMTruyVanHQ(ByVal vmLePhi As VMLePhiHaiQuanTraCuu) As String
        'var strSearchResultFromHaiQuan = File.ReadAllText(HostingEnvironment.MapPath("~/App_Code/thongdiep.xml"));
        'VMLePhiHaiQuanTraCuu vMLePhiHaiQuanTraCuu = new VMLePhiHaiQuanTraCuu();

        'VMLePhiHaiQuanTraCuu vMLePhiHaiQuanTraCuu = new VMLePhiHaiQuanTraCuu();
        'vMLePhiHaiQuanTraCuu.So_HS = So_HS;
        'vMLePhiHaiQuanTraCuu.Ma_DVQL = Ma_DVQL;
        'vMLePhiHaiQuanTraCuu.KyHieu_CT = KyHieu_CT;
        'vMLePhiHaiQuanTraCuu.So_CT = So_CT;
        'vMLePhiHaiQuanTraCuu.Nam_CT = Nam_CT;

        'Initialize ProcessMSG
        Dim lePhi303 As New MSG303LePhi()

        Dim errorNum As String = ""
        Dim errorMessage As String = ""
        Dim strSearchResultFromHaiQuan As String = String.Empty
        Dim process As New ProcessMSG()
        Try
            strSearchResultFromHaiQuan = process.getMSG103(vmLePhi, errorNum, errorMessage)
            'strSearchResultFromHaiQuan = "<?xml version='1.0' encoding='UTF-8'?><Customs><Header><Application_Name>Payment</Application_Name><Application_Version>3.0</Application_Version><Sender_Code>99999999</Sender_Code><Sender_Name>Tổng cục hải quan</Sender_Name><Message_Version>3.0</Message_Version><Message_Type>203</Message_Type><Message_Name>Thông điệp trả lời số lệ phí phải thu</Message_Name><Transaction_Date>2016-06-14T14:45:58</Transaction_Date><Transaction_ID>fb19f6d1-e1a4-4584-b5eb-559f7ba895d0</Transaction_ID><Request_ID>f8fa44d7-0597-45c5-9189-7b18196ab916</Request_ID></Header><Data><So_HS>1501000000201</So_HS><Ma_DVQL>BGTVT03</Ma_DVQL><Ten_DVQL>Bộ Giao thông vận tải - Cục Đăng Kiểm Việt Nam</Ten_DVQL><KyHieu_CT>778480</KyHieu_CT><So_CT>1438080</So_CT><Nam_CT>2015</Nam_CT><NguoiNopTien><Ma_ST>0100100079014</Ma_ST><Ten_NNT>Nhà máy Điện Phú Mỹ</Ten_NNT><DiaChi>Thị trấn Phú Mỹ</DiaChi><TT_Khac>Trần Quân Thuyên</TT_Khac></NguoiNopTien><ThongTin_NopTien><Ma_NT>VND</Ma_NT><TyGia>1</TyGia><TongTien_NT>120275000</TongTien_NT><TongTien_VND>120275000</TongTien_VND></ThongTin_NopTien><ChiTiet_CT><STT>1</STT><NDKT>01031</NDKT><Ten_NDKT>Phí kiểm tra truớc thuế</Ten_NDKT><SoTien_NT>10000000</SoTien_NT><SoTien_VND>22726</SoTien_VND><GhiChu>546</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>2</STT><NDKT>01032</NDKT><Ten_NDKT>Chi phí phát sinh trước thuế</Ten_NDKT><SoTien_NT>10000000</SoTien_NT><SoTien_VND>0</SoTien_VND><GhiChu>546</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>3</STT><NDKT>01033</NDKT><Ten_NDKT>Thuế</Ten_NDKT><SoTien_NT>10000000</SoTien_NT><SoTien_VND>2272</SoTien_VND><GhiChu>546</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>4</STT><NDKT>01034</NDKT><Ten_NDKT>Lệ phí cấp chứng chỉ</Ten_NDKT><SoTien_NT>10000000</SoTien_NT><SoTien_VND>50000</SoTien_VND><GhiChu>546</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>5</STT><NDKT>01031</NDKT><Ten_NDKT>Phí kiểm tra truớc thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>27272726</SoTien_VND><GhiChu>205</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>6</STT><NDKT>01032</NDKT><Ten_NDKT>Chi phí phát sinh trước thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>0</SoTien_VND><GhiChu>205</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>7</STT><NDKT>01033</NDKT><Ten_NDKT>Thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>2727272</SoTien_VND><GhiChu>205</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>8</STT><NDKT>01034</NDKT><Ten_NDKT>Lệ phí cấp chứng chỉ</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>50000</SoTien_VND><GhiChu>205</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>9</STT><NDKT>01031</NDKT><Ten_NDKT>Phí kiểm tra truớc thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>27272726</SoTien_VND><GhiChu>204</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>10</STT><NDKT>01032</NDKT><Ten_NDKT>Chi phí phát sinh trước thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>0</SoTien_VND><GhiChu>204</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>11</STT><NDKT>01033</NDKT><Ten_NDKT>Thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>2727272</SoTien_VND><GhiChu>204</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>12</STT><NDKT>01034</NDKT><Ten_NDKT>Lệ phí cấp chứng chỉ</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>50000</SoTien_VND><GhiChu>204</GhiChu></ChiTiet_CT><TaiKhoan_NopTien><Ma_NH_TH>01203004</Ma_NH_TH><Ten_NH_TH>Ngân hàng TMCP Ngoại Thương Việt Nam - Chi nhánh Thăng Long</Ten_NH_TH><TaiKhoan_TH>0021000002046</TaiKhoan_TH><Ten_TaiKhoan_TH>Cục Đăng Kiểm Việt Nam</Ten_TaiKhoan_TH></TaiKhoan_NopTien></Data><Error><ErrorNumber>0</ErrorNumber><ErrorMessage>Xử lý thành công</ErrorMessage></Error><Signature xmlns='http://www.w3.org/2000/09/xmldsig#'><SignedInfo><CanonicalizationMethod Algorithm='http://www.w3.org/TR/2001/REC-xml-c14n-20010315' /><SignatureMethod Algorithm='http://www.w3.org/2000/09/xmldsig#rsa-sha1' /><Reference URI=''><Transforms><Transform Algorithm='http://www.w3.org/2000/09/xmldsig#enveloped-signature' /></Transforms><DigestMethod Algorithm='http://www.w3.org/2000/09/xmldsig#sha1' /><DigestValue>XVH/eb5QTvZBC6RYQ3gWJXJdz1U=</DigestValue></Reference></SignedInfo><SignatureValue>XxzwQPolsebZSRsx6DozJXgmusXmn7jTzUlE31w3i9lEhZNXq+qEZYHYsc8ycqHtSjnG6JPJbs8wQbkUt/99M6zj00d52XjVj62kjuT1+Qw+4EnmN1XB6EiMfdLpOHzkcQdX4q2+5OxIFiCuUOZmV1+/BDRXGRBWGHkImM5NKFGDiFcTNTixtuee9P23omP9pCiu9OAVtsKx1633Eekx1hszqPb4JXrQNiHncPZMmpGh8qJJiEnQ227ijwrAyM0zZ0m2ZsQgZX2WPqnia95SejOIE+PDCs0ILycpp/0UC9PfSHx8p4KxyUOI3n3pjU+HhfUICTiTOnzZzPwSp8G4Dg==</SignatureValue><KeyInfo><X509Data><X509IssuerSerial><X509IssuerName>CN=VNPT Certification Authority, OU=VNPT-CA Trust Network, O=VNPT Group, C=VN</X509IssuerName><X509SerialNumber>111663843396468347755383557710472524059</X509SerialNumber></X509IssuerSerial><X509Certificate>MIIF/zCCA+egAwIBAgIQVAGsiVCsCs6lmTyX/l+xGzANBgkqhkiG9w0BAQUFADBpMQswCQYDVQQGEwJWTjETMBEGA1UEChMKVk5QVCBHcm91cDEeMBwGA1UECxMVVk5QVC1DQSBUcnVzdCBOZXR3b3JrMSUwIwYDVQQDExxWTlBUIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MDIxMDE1MjY0M1oXDTE3MDcwMzA5MjEwMFowdTELMAkGA1UEBhMCVk4xEjAQBgNVBAgMCUjDoCBO4buZaTEVMBMGA1UEBwwMQ+G6p3UgR2nhuqV5MRkwFwYDVQQKDBBC4buYIFTDgEkgQ0jDjU5IMSAwHgYDVQQDDBdU4buUTkcgQ+G7pEMgSOG6okkgUVVBTjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL69JOx8ksSE/mZtvhPdpVMV+ZnfZrWLSsxN87b2APMIpVboxq5HmE4Xs8nvEPasymsu0xBn5tTxMI15aAfxspQ5Ks4oDuMkwoO6L4WPiVwxvWtBuLCAdSa0aSVsy98Mod8Frcai6NwygVodojphfl0+88IYgc30it991vfgeh4cdimP5psNElpWlKQVa04VpB99dclDd/Nr3m4wZvyT5Th8s7vBbBPzVtr9ebv75LaM27OG8NBPaHEtJACeoaadla4tTNCPSrRCS4x8Oon4lBb4N0JyC2MOgUjCS51fiB6AVtfqCoS1NzHrCot8/JiHag8+CHH2afwWCZt6mokSKGUCAwEAAaOCAZUwggGRMHAGCCsGAQUFBwEBBGQwYjAyBggrBgEFBQcwAoYmaHR0cDovL3B1Yi52bnB0LWNhLnZuL2NlcnRzL3ZucHRjYS5jZXIwLAYIKwYBBQUHMAGGIGh0dHA6Ly9vY3NwLnZucHQtY2Eudm4vcmVzcG9uZGVyMB0GA1UdDgQWBBQIfYTiUnPDcfy7IZUiDO0vxzbJRTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFAZpwNXVAooVjUZ96XziaApVrGqvMGsGA1UdIARkMGIwYAYNKwYBBAGB7QMBAQMBBTBPMCYGCCsGAQUFBwICMBoeGABEAEkARAAtAEIAMgAuADAALQAzADYAbTAlBggrBgEFBQcCARYZaHR0cDovL3B1Yi52bnB0LWNhLnZuL3JwYTAxBgNVHR8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZucHQtY2Eudm4vdm5wdGNhLmNybDAOBgNVHQ8BAf8EBAMCBPAwHwYDVR0lBBgwFgYIKwYBBQUHAwIGCisGAQQBgjcKAwwwDQYJKoZIhvcNAQEFBQADggIBAB5iI3MXO5KdLT8grKhfKrzDH/VBR/qoBBRRpMrVC552fRpMEIWw08oOcGw2Z3XsUQ9dHSlawLGh3y8/qNXxCZWGUcE5Zeh9l76WDGAB5LSpUNLmD6Na25GBg3UznqKrTuETGMTgwyKYmLvOzPy2xpihtg1J2A5bsXC39ZbTthOyHtinlG/wmx4h9tGBQGXUaSBmVIt/bBw9BsN7yjZYQvAc1AIIegyFx28YY9Ycvfh2BbAStikHIIF22hTOe8/jbxLmQRrFj8J9yKjmQW8BvfvhTcxWS2MgZNrjEwJiYr/NK6iPNxYmCreP7aw74Uy659t/fX/WxOZe3HU8bZFJiudlWSgizKQpsx3Pk1YmPFYj2Ex1YPpUKscD9IStsMgWt/ylylZpupXvhNvW5V82tmRslKob8xnxsHfuKWaWk/GtvkziCdzSWqvU18d854YMMg5c+ZsQHvYVBN5HoIzHPvZHwEvsZtZYMYB8tcuoZGKuYeJwgXWYsucccXrR0R025Du86EQ/URAorkJTFaC6RfSLbvxPNBBv9fAgntf4ZPpaF+7CgTEwpC4K6QHIddGle6cyN8nAz1e9l9jtWtb+KOLdPvzLjTZbPgiJKNjw65LXu8NBzziwzyknK/MMIWJ7M5WKU5QXJyu3ahPONzcL22xqHgIikjh1jO4sivjTdXjQ</X509Certificate></X509Data></KeyInfo></Signature></Customs>"

        Catch ex As Exception
            Throw
        End Try

        If strSearchResultFromHaiQuan = "" OrElse strSearchResultFromHaiQuan.Length = 0 Then
            'ProcessMSG.getMSG103(vMLePhiHaiQuanTraCuu, ref errorNum, ref errorMessage);
            'strSearchResultFromHaiQuan = "<?xml version='1.0' encoding='UTF-8'?><Customs><Header><Application_Name>Payment</Application_Name><Application_Version>3.0</Application_Version><Sender_Code>99999999</Sender_Code><Sender_Name>Tổng cục hải quan</Sender_Name><Message_Version>3.0</Message_Version><Message_Type>203</Message_Type><Message_Name>Thông điệp trả lời số lệ phí phải thu</Message_Name><Transaction_Date>2016-06-14T14:45:58</Transaction_Date><Transaction_ID>fb19f6d1-e1a4-4584-b5eb-559f7ba895d0</Transaction_ID><Request_ID>f8fa44d7-0597-45c5-9189-7b18196ab916</Request_ID></Header><Data><So_HS>1501000000201</So_HS><Ma_DVQL>BGTVT03</Ma_DVQL><Ten_DVQL>Bộ Giao thông vận tải - Cục Đăng Kiểm Việt Nam</Ten_DVQL><KyHieu_CT>778480</KyHieu_CT><So_CT>1438080</So_CT><Nam_CT>2015</Nam_CT><NguoiNopTien><Ma_ST>0100100079014</Ma_ST><Ten_NNT>Nhà máy Điện Phú Mỹ</Ten_NNT><DiaChi>Thị trấn Phú Mỹ</DiaChi><TT_Khac>Trần Quân Thuyên</TT_Khac></NguoiNopTien><ThongTin_NopTien><Ma_NT>VND</Ma_NT><TyGia>1</TyGia><TongTien_NT>120275000</TongTien_NT><TongTien_VND>120275000</TongTien_VND></ThongTin_NopTien><ChiTiet_CT><STT>1</STT><NDKT>01031</NDKT><Ten_NDKT>Phí kiểm tra truớc thuế</Ten_NDKT><SoTien_NT>10000000</SoTien_NT><SoTien_VND>22726</SoTien_VND><GhiChu>546</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>2</STT><NDKT>01032</NDKT><Ten_NDKT>Chi phí phát sinh trước thuế</Ten_NDKT><SoTien_NT>10000000</SoTien_NT><SoTien_VND>0</SoTien_VND><GhiChu>546</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>3</STT><NDKT>01033</NDKT><Ten_NDKT>Thuế</Ten_NDKT><SoTien_NT>10000000</SoTien_NT><SoTien_VND>2272</SoTien_VND><GhiChu>546</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>4</STT><NDKT>01034</NDKT><Ten_NDKT>Lệ phí cấp chứng chỉ</Ten_NDKT><SoTien_NT>10000000</SoTien_NT><SoTien_VND>50000</SoTien_VND><GhiChu>546</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>5</STT><NDKT>01031</NDKT><Ten_NDKT>Phí kiểm tra truớc thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>27272726</SoTien_VND><GhiChu>205</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>6</STT><NDKT>01032</NDKT><Ten_NDKT>Chi phí phát sinh trước thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>0</SoTien_VND><GhiChu>205</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>7</STT><NDKT>01033</NDKT><Ten_NDKT>Thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>2727272</SoTien_VND><GhiChu>205</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>8</STT><NDKT>01034</NDKT><Ten_NDKT>Lệ phí cấp chứng chỉ</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>50000</SoTien_VND><GhiChu>205</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>9</STT><NDKT>01031</NDKT><Ten_NDKT>Phí kiểm tra truớc thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>27272726</SoTien_VND><GhiChu>204</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>10</STT><NDKT>01032</NDKT><Ten_NDKT>Chi phí phát sinh trước thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>0</SoTien_VND><GhiChu>204</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>11</STT><NDKT>01033</NDKT><Ten_NDKT>Thuế</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>2727272</SoTien_VND><GhiChu>204</GhiChu></ChiTiet_CT><ChiTiet_CT><STT>12</STT><NDKT>01034</NDKT><Ten_NDKT>Lệ phí cấp chứng chỉ</Ten_NDKT><SoTien_NT>30000000123</SoTien_NT><SoTien_VND>50000</SoTien_VND><GhiChu>204</GhiChu></ChiTiet_CT><TaiKhoan_NopTien><Ma_NH_TH>01203004</Ma_NH_TH><Ten_NH_TH>Ngân hàng TMCP Ngoại Thương Việt Nam - Chi nhánh Thăng Long</Ten_NH_TH><TaiKhoan_TH>0021000002046</TaiKhoan_TH><Ten_TaiKhoan_TH>Cục Đăng Kiểm Việt Nam</Ten_TaiKhoan_TH></TaiKhoan_NopTien></Data><Error><ErrorNumber>0</ErrorNumber><ErrorMessage>Xử lý thành công</ErrorMessage></Error><Signature xmlns='http://www.w3.org/2000/09/xmldsig#'><SignedInfo><CanonicalizationMethod Algorithm='http://www.w3.org/TR/2001/REC-xml-c14n-20010315' /><SignatureMethod Algorithm='http://www.w3.org/2000/09/xmldsig#rsa-sha1' /><Reference URI=''><Transforms><Transform Algorithm='http://www.w3.org/2000/09/xmldsig#enveloped-signature' /></Transforms><DigestMethod Algorithm='http://www.w3.org/2000/09/xmldsig#sha1' /><DigestValue>XVH/eb5QTvZBC6RYQ3gWJXJdz1U=</DigestValue></Reference></SignedInfo><SignatureValue>XxzwQPolsebZSRsx6DozJXgmusXmn7jTzUlE31w3i9lEhZNXq+qEZYHYsc8ycqHtSjnG6JPJbs8wQbkUt/99M6zj00d52XjVj62kjuT1+Qw+4EnmN1XB6EiMfdLpOHzkcQdX4q2+5OxIFiCuUOZmV1+/BDRXGRBWGHkImM5NKFGDiFcTNTixtuee9P23omP9pCiu9OAVtsKx1633Eekx1hszqPb4JXrQNiHncPZMmpGh8qJJiEnQ227ijwrAyM0zZ0m2ZsQgZX2WPqnia95SejOIE+PDCs0ILycpp/0UC9PfSHx8p4KxyUOI3n3pjU+HhfUICTiTOnzZzPwSp8G4Dg==</SignatureValue><KeyInfo><X509Data><X509IssuerSerial><X509IssuerName>CN=VNPT Certification Authority, OU=VNPT-CA Trust Network, O=VNPT Group, C=VN</X509IssuerName><X509SerialNumber>111663843396468347755383557710472524059</X509SerialNumber></X509IssuerSerial><X509Certificate>MIIF/zCCA+egAwIBAgIQVAGsiVCsCs6lmTyX/l+xGzANBgkqhkiG9w0BAQUFADBpMQswCQYDVQQGEwJWTjETMBEGA1UEChMKVk5QVCBHcm91cDEeMBwGA1UECxMVVk5QVC1DQSBUcnVzdCBOZXR3b3JrMSUwIwYDVQQDExxWTlBUIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MDIxMDE1MjY0M1oXDTE3MDcwMzA5MjEwMFowdTELMAkGA1UEBhMCVk4xEjAQBgNVBAgMCUjDoCBO4buZaTEVMBMGA1UEBwwMQ+G6p3UgR2nhuqV5MRkwFwYDVQQKDBBC4buYIFTDgEkgQ0jDjU5IMSAwHgYDVQQDDBdU4buUTkcgQ+G7pEMgSOG6okkgUVVBTjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL69JOx8ksSE/mZtvhPdpVMV+ZnfZrWLSsxN87b2APMIpVboxq5HmE4Xs8nvEPasymsu0xBn5tTxMI15aAfxspQ5Ks4oDuMkwoO6L4WPiVwxvWtBuLCAdSa0aSVsy98Mod8Frcai6NwygVodojphfl0+88IYgc30it991vfgeh4cdimP5psNElpWlKQVa04VpB99dclDd/Nr3m4wZvyT5Th8s7vBbBPzVtr9ebv75LaM27OG8NBPaHEtJACeoaadla4tTNCPSrRCS4x8Oon4lBb4N0JyC2MOgUjCS51fiB6AVtfqCoS1NzHrCot8/JiHag8+CHH2afwWCZt6mokSKGUCAwEAAaOCAZUwggGRMHAGCCsGAQUFBwEBBGQwYjAyBggrBgEFBQcwAoYmaHR0cDovL3B1Yi52bnB0LWNhLnZuL2NlcnRzL3ZucHRjYS5jZXIwLAYIKwYBBQUHMAGGIGh0dHA6Ly9vY3NwLnZucHQtY2Eudm4vcmVzcG9uZGVyMB0GA1UdDgQWBBQIfYTiUnPDcfy7IZUiDO0vxzbJRTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFAZpwNXVAooVjUZ96XziaApVrGqvMGsGA1UdIARkMGIwYAYNKwYBBAGB7QMBAQMBBTBPMCYGCCsGAQUFBwICMBoeGABEAEkARAAtAEIAMgAuADAALQAzADYAbTAlBggrBgEFBQcCARYZaHR0cDovL3B1Yi52bnB0LWNhLnZuL3JwYTAxBgNVHR8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZucHQtY2Eudm4vdm5wdGNhLmNybDAOBgNVHQ8BAf8EBAMCBPAwHwYDVR0lBBgwFgYIKwYBBQUHAwIGCisGAQQBgjcKAwwwDQYJKoZIhvcNAQEFBQADggIBAB5iI3MXO5KdLT8grKhfKrzDH/VBR/qoBBRRpMrVC552fRpMEIWw08oOcGw2Z3XsUQ9dHSlawLGh3y8/qNXxCZWGUcE5Zeh9l76WDGAB5LSpUNLmD6Na25GBg3UznqKrTuETGMTgwyKYmLvOzPy2xpihtg1J2A5bsXC39ZbTthOyHtinlG/wmx4h9tGBQGXUaSBmVIt/bBw9BsN7yjZYQvAc1AIIegyFx28YY9Ycvfh2BbAStikHIIF22hTOe8/jbxLmQRrFj8J9yKjmQW8BvfvhTcxWS2MgZNrjEwJiYr/NK6iPNxYmCreP7aw74Uy659t/fX/WxOZe3HU8bZFJiudlWSgizKQpsx3Pk1YmPFYj2Ex1YPpUKscD9IStsMgWt/ylylZpupXvhNvW5V82tmRslKob8xnxsHfuKWaWk/GtvkziCdzSWqvU18d854YMMg5c+ZsQHvYVBN5HoIzHPvZHwEvsZtZYMYB8tcuoZGKuYeJwgXWYsucccXrR0R025Du86EQ/URAorkJTFaC6RfSLbvxPNBBv9fAgntf4ZPpaF+7CgTEwpC4K6QHIddGle6cyN8nAz1e9l9jtWtb+KOLdPvzLjTZbPgiJKNjw65LXu8NBzziwzyknK/MMIWJ7M5WKU5QXJyu3ahPONzcL22xqHgIikjh1jO4sivjTdXjQ</X509Certificate></X509Data></KeyInfo></Signature></Customs>"
        End If
        Dim msg303 As New MSG303LePhi()

        lePhi303 = msg303.MSGLePhiToObject(strSearchResultFromHaiQuan)

        Return JsonConvert.SerializeObject(lePhi303.Data)
    End Function

    <WebMethod()> _
    Public Shared Function GetTEN_KH_NH(ByVal pv_strTKNH As String) As String
        Dim strReturn As String = ""
        Dim ma_loi As String = ""
        Dim so_du As String = ""
        'If CORE_ON_OFF = "1" Then
        strReturn = cls_corebank.GetTen_TK_KH_NH(pv_strTKNH.Replace("-", ""))
        'Else 
        '    strReturn = "Tài khoản test"
        'End If
        Return strReturn
    End Function

    <WebMethod()> _
    Public Shared Function chkChuyenKhoanNoiDia(ByVal str_NH_TH As String) As String
        Dim strSenderCode As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
        Try
            Dim ds = HaiQuan.getTaiKhoanTrucTiep(str_NH_TH)
            Dim jsonResult As String = String.Empty

            If strSenderCode <> "" Or str_NH_TH <> "" Then
                Dim chkSenderCode As String = strSenderCode.Substring(2, 3)
                Dim chkMaNH As String = str_NH_TH.Substring(2, 3)

                If chkSenderCode <> chkMaNH Then
                    If ds IsNot Nothing Then
                        Dim dr As DataRow = ds.Tables(0).Rows(0)
                        Dim strMa As String = dr("ma_nganhang_tt").ToString()
                        Dim strTen As String = dr("ten_nganhang_tt").ToString()
                        jsonResult = (strMa + ";" + strTen)
                    End If
                End If
            End If

            Return jsonResult
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    <WebMethod()> _
    Public Shared Function GET_TK_KH_NH(ByVal tk_kh_nh As String) As String
        'sua cho kienlong
        'Dim clsCore As New clsCoreBankUtilMSB()
        'Dim descAccount As String = String.Empty
        'Dim avaiBalance As String = String.Empty
        'Dim erroCode As String = String.Empty
        'Dim erroMessage As String = String.Empty

        ''Dim dataUser As String = clsCore.GetCustomerInfo(tk_kh_nh, "", "", "", "")


        'Dim dataKH As Business.infTK_KH_TH = clsCore.GetCustomerInfoInstance(tk_kh_nh, descAccount, avaiBalance, erroCode, erroMessage)

        'If dataKH IsNot Nothing Then
        '    Return JsonConvert.SerializeObject(dataKH)
        'Else
        '    Return Nothing

        'End If

        ' j.ToString();

        Dim sRet As String = String.Empty
        Try

            If String.IsNullOrEmpty(tk_kh_nh) Then
                Return sRet
            End If
            Dim err As String = ""
            Dim balance As String = ""
            Dim branch_tk As String = ""
            Return cls_corebank.GetTK_KH_NH(tk_kh_nh.Trim, err, balance, branch_tk)

        Catch ex As Exception
            sRet = String.Empty
            Throw ex
        End Try

        Return sRet
    End Function

    <WebMethod()> _
    Public Shared Function updateChungTu(ByVal CODE As String, ByVal SO_CT As String, ByVal TYPE As String, ByVal LYDO_HUY As String, ByVal PTTT As String, ByVal TK_THANHTOAN As String, _
     ByVal TEN_TK_CHUYEN As String, ByVal SO_DU As String, ByVal TT_NT_SO_CMND As String, ByVal TT_NT_SO_DT As String, ByVal NNT_TINH_TP As String, ByVal NNT_QUAN_HUYEN As String, _
     ByVal TK_THANH_TOAN_TINH_TP As String, ByVal TK_THANH_TOAN_QUAN_HUYEN As String, ByVal MA_NH_PH As String, ByVal TEN_NH_PH As String, ByVal TK_THANH_TOAN_DIACHI As String, ByVal Ma_NT As String, _
     ByVal TyGia As String, ByVal TongTien_NT As String, ByVal TongTien_VND As String, ByVal Ma_NH_TH As String, ByVal Ten_NH_TH As String, ByVal TaiKhoan_TH As String, _
     ByVal Ten_TaiKhoan_TH As String, ByVal FEE As String, ByVal VAT As String, ByVal TONGTIEN_TRUOC_VAT As String, ByVal TAIKHOAN_NT_MA_NH_TT As String, ByVal TAIKHOAN_NT_TEN_NH_TT As String, ByVal LISTCHITIET_CT As String) As ChungTuCode
        'get current ma kiem soat vien
        'string ma_nv = _objUser.Ma_NV;

        Dim ListChiTietCT = JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of Business_HQ.VM_ChiTiet_CT))(LISTCHITIET_CT)

        Dim newType As Integer = Integer.Parse(TYPE)

        Dim newTT_CT As String = CODE
        If newTT_CT = "00" Then
            newTT_CT = "05"
        End If
        If newTT_CT = "05" AndAlso newType = CInt(TypeOfAction.Denied) Then
            newTT_CT = "03"
        End If

        If newTT_CT = "03" AndAlso newType = CInt(TypeOfAction.Denied) Then
            newTT_CT = "04"
        End If
        If newTT_CT = "02" AndAlso newType = CInt(TypeOfAction.Denied) Then
            newTT_CT = "07"
        End If

        If newTT_CT = "01" AndAlso newType = CInt(TypeOfAction.Denied) Then
            newTT_CT = "02"
        End If

        If newTT_CT = "03" AndAlso newType = CInt(TypeOfAction.Edit) Then
            newTT_CT = "05"
        End If

        If newTT_CT = "00" AndAlso newType = CInt(TypeOfAction.Denied) Then
            newTT_CT = "04"
        End If

        If newTT_CT = "03" AndAlso newType = CInt(TypeOfAction.CreateNew) Then
            newTT_CT = "00"
        End If

        Dim ma_nv As String = HttpContext.Current.Session("User").ToString()

        Dim messageReturn As String = String.Empty
        Try
            If CODE = "01" AndAlso newType = CInt(TypeOfAction.Denied) AndAlso LYDO_HUY.Length > 0 Then
                messageReturn = HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH_NGAY_XL(SO_CT, newTT_CT, ma_nv, LYDO_HUY, PTTT, TK_THANHTOAN, _
                 TEN_TK_CHUYEN, SO_DU, TT_NT_SO_CMND, TT_NT_SO_DT, NNT_TINH_TP, NNT_QUAN_HUYEN, _
                 TK_THANH_TOAN_TINH_TP, TK_THANH_TOAN_QUAN_HUYEN, MA_NH_PH, TEN_NH_PH, TK_THANH_TOAN_DIACHI, FEE, VAT, TONGTIEN_TRUOC_VAT, TAIKHOAN_NT_MA_NH_TT, TAIKHOAN_NT_TEN_NH_TT)
            Else
                messageReturn = HaiQuan.UPDATE_TRANGTHAI_CT_PHI_BO_NGANH_NGAY_XL(SO_CT, newTT_CT, ma_nv, PTTT, TK_THANHTOAN, TEN_TK_CHUYEN, _
                 SO_DU, TT_NT_SO_CMND, TT_NT_SO_DT, NNT_TINH_TP, NNT_QUAN_HUYEN, TK_THANH_TOAN_TINH_TP, _
                 TK_THANH_TOAN_QUAN_HUYEN, MA_NH_PH, TEN_NH_PH, TK_THANH_TOAN_DIACHI, Ma_NT, TyGia, _
                 TongTien_NT, TongTien_VND, Ma_NH_TH, Ten_NH_TH, TaiKhoan_TH, Ten_TaiKhoan_TH, FEE, VAT, TONGTIEN_TRUOC_VAT, _
                 ListChiTietCT)
            End If
            If newTT_CT = "05" Then
                messageReturn = Convert.ToString("Đã lưu và chuyển sang kiểm soát số chứng từ: ") & SO_CT
            Else
                messageReturn = Convert.ToString("Cập nhật thành công thông tin chứng từ: ") & SO_CT
            End If
        Catch
            messageReturn = Convert.ToString("Lỗi cập nhật thông tin chứng từ: ") & SO_CT
        End Try
        Dim chungTuCode As New ChungTuCode()
        chungTuCode.Code = 0
        chungTuCode.Message = messageReturn
        chungTuCode.SoCT = SO_CT
        chungTuCode.TrangThaiCT = newTT_CT
        Return chungTuCode

    End Function

    <WebMethod()> _
    Public Shared Function createToKhaiHaiQuan(ByVal tsc As TCS_LEPHI_HAIQUAN, ByVal LISTCHITIET_CT As String) As String
        Dim tcsLePhiHaiQuan As New TCS_LEPHI_HAIQUAN()

        'string statement = Common.getString<TCS_LEPHI_HAIQUAN>(rCS_LEPHI_HAIQUAN); //Common.getString(TCS_LEPHI_HAIQUAN);
        Dim mv_objUser As New Business_HQ.CTuUser(HttpContext.Current.Session("User").ToString())
        Dim ListChiTietCT = JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of CustomsV3.MSG.MSG303LePhi.ChiTiet_CT))(LISTCHITIET_CT)
        tsc.TRANGTHAI_XULY = 0
        Dim fmt2 As [String] = "##;(##)"
        Dim tt As String = tsc.TRANGTHAI_CT
        tsc.TRANGTHAI_CT = [String].Format("{0:" + fmt2 + "}", tt)


        tsc.TRANGTHAI_CT = tt
        tsc.MA_CN = mv_objUser.MA_CN
        tsc.MA_NV = Integer.Parse(mv_objUser.Ma_NV)
        Dim ngay_lv As String = mv_objUser.Ngay_LV
        tsc.NGAY_CT = ngay_lv
        tsc.NGAY_BN = ngay_lv
        tsc.NGAY_BC = ngay_lv

        Dim ref_no As String = String.Empty

        tsc.RM_REF_NO = clsCTU.GenerateRMRef(clsCTU.TCS_GetMaChiNhanh(tsc.MA_NV), ref_no)
        tsc.REF_NO = ref_no

        Dim result = daLePhiHaiQuan.MSG303ToDB(tsc, ListChiTietCT)

        Return JsonConvert.SerializeObject(result)
    End Function

    Private Function PopulateArrTKGL() As String
        Dim strSQL As String = "SELECT SO_TK, TEN_TK FROM TCS_DM_TK_TG"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        Dim noOfItems As Integer = dt.Rows.Count
        Dim myJavaScript As StringBuilder = New StringBuilder()
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            myJavaScript.Append(" var arrTKGL = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrTKGL[" & i & "] ='" & dt.Rows(i)("SO_TK") & ";" & dt.Rows(i)("TEN_TK") & "';")
            Next
            Return myJavaScript.ToString()
        Else
            myJavaScript.Append(" var arrTKGL = new Array(0);")
            Return myJavaScript.ToString()
        End If

    End Function

    Public Shared Function convertDate(ByVal strText As String) As String
        Dim strDay As String, strMonth As String, strYear As String
        strDay = strText.Substring(6, 2)
        strMonth = strText.Substring(4, 2)
        strYear = strText.Substring(0, 4)
        Return Convert.ToString((Convert.ToString(strDay & Convert.ToString("/")) & strMonth) + "/") & strYear
    End Function

    Public Class TK_KH_NH
        Public Property Account() As String
            Get
                Return m_Account
            End Get
            Set(ByVal value As String)
                m_Account = value
            End Set
        End Property
        Private m_Account As String
        Public Property Balance() As String
            Get
                Return m_Balance
            End Get
            Set(ByVal value As String)
                m_Balance = value
            End Set
        End Property
        Private m_Balance As String
        Public Property DescAccount() As String
            Get
                Return m_DescAccount
            End Get
            Set(ByVal value As String)
                m_DescAccount = value
            End Set
        End Property
        Private m_DescAccount As String
    End Class
End Class