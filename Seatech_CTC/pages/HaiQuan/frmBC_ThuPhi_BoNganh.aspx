﻿<%@ Page Language="C#" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="true"
    CodeFile="frmBC_ThuPhi_BoNganh.aspx.cs" Inherits="pages_HaiQuan_frmBC_ThuPhi_BoNganh" %>

<%@ Import Namespace="System.Web.Services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #table_Id table
        {
            width: auto;
            border-collapse: collapse;
        }
        #table_Id thead
        {
            background: #000;
            color: #fff;
        }
        #table_Id tbody, td
        {
        }
        #table_Id td
        {
            width: 10em;
        }
        #table_Id tbody
        {
        }
        div #pager
        {
            text-align: center;
            margin: 1em 0;
        }
        div #pager span
        {
            display: inline-block;
            width: 1.8em;
            height: 1.8em;
            line-height: 1.8;
            text-align: center;
            cursor: pointer;
            background: #000;
            color: #fff;
            margin-right: 0.5em;
        }
        div #pager span.active
        {
            background: #c00;
        }
        .td_head
        {
            border-bottom: solid 1px #000;
            border-top: solid 1px #000;
            border-left: solid 1px #000;
            border-right: solid 1px #000;
        }
    </style>

    <script type="text/javascript" src="../../javascript/google/js/jquery-1.8.0.min.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="../../javascript/date/datePicker.css" />

    <script src="../../javascript/date/date.js" type="text/javascript"></script>

    <script src="../../javascript/date/datePicker.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>
    
    <script src="../../javascript/accounting.min.js" type="text/javascript"></script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">Tra cứu chứng từ phí bộ ngành</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <span id="labelMessage"></span>
            </td>
        </tr>
        <tr>
            <td width='100%' id="tdTextSearch" align="center" valign="top" class='nav'>
                <div id="divTracuu">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="4" align="left" valign="top" class="style1">
                                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                    <tr>
                                        <td style="width: 15%" align="left" valign="top" class="form_label">
                                            Từ ngày
                                        </td>
                                        <td align="left" valign="top" class="form_control" style="width: 30%">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchTuNgayCT" class="inputflat date-pick dp-applied"
                                                    style="width: 98%;" onkeyup="javascript:return mask(this.value,this,'2,5','/');"
                                                    onblur="CheckDate(this);" onfocus="this.select()" />
                                            </span>
                                        </td>
                                        <td align="left" style="width: 20%" valign="top" class="form_label">
                                            Đến ngày
                                        </td>
                                        <td align="left" valign="top" class="form_control" style="width: 30%">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchDenNgayCT" class="inputflat date-pick dp-applied"
                                                    style="width: 98%;" onkeyup="javascript:return mask(this.value,this,'2,5','/');"
                                                    onblur="CheckDate(this);" onfocus="this.select()" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%" align="left" valign="top" class="form_label">
                                            <b>MST</b>
                                        </td>
                                        <td style="width: 30%;" class="form_control">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchMST" class="inputflat" style="width: 98%;" />
                                            </span>
                                        </td>
                                        <td style="width: 15%" align="left" valign="top" class="form_label">
                                            <b>Số chứng từ</b>
                                        </td>
                                        <td style="width: 30%;" class="form_control">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchSoHoSo" class="inputflat" style="width: 98%;" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_label">
                                            Năm chứng từ
                                        </td>
                                        <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_control">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchNam_CT" class="inputflat" style="width: 98%;" />
                                            </span>
                                        </td>
                                        <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_label">
                                            Trạng thái chứng từ
                                        </td>
                                        <td align="left" valign="top" class="form_control" style="height: 20px">
                                            <select id="ddlTrangThaiCT" class="inputflat" style="width: 80%; height: 18px">
                                                <option value="-1">Tất cả </option>
                                                <option value="05">Chờ kiểm soát </option>
                                                <option value="01">Đã kiểm soát</option>
                                                <option value="03">Chuyển trả</option>
                                                <option value="04">Đã hủy</option>
                                                <option value="06">Chuyển thuế/HQ lỗi</option>                                              
                                                
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                       
                                        <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_label">
                                            Mã chi nhánh
                                        </td>
                                        <td align="left" valign="top" class="form_control" style="height: 20px">
                                            <select id="ddlMaChiNhanh" class="inputflat" style="width: 80%; height: 18px">
                                               
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <input type="button" id="btnTruyVanKetQua" value="Tra Cứu" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan='2' align="left" class="errorMessage">
                <table border="0" width="100%">
                    <tr id="rowProgress">
                        <td colspan="2" align="left">
                            <div style="background-color: Aqua; display: none;" id="divProgress">
                                <b style="font-weight: bold; font-size: 15pt">Đang xử lý. Xin chờ một lát...</b>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <div id="divStatus" style="font-weight: bold; width: 100%;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr runat="server" id="Tr2">
                        <td align="center" colspan="4">
                            <div id="div1" style="min-height: 200px; height: auto; width: 100%;">
                                <table id="table_Id" class="paginated grid_data" style="width: 100%; border-color: #000000;
                                    width: 100%; border-collapse: collapse; font-size: 12pt">
                                    <tr>
                                        <th class="grid_header td_head" style="width: 3%">
                                            STT
                                        </th>
                                        <th class="grid_header td_head">
                                            Số HS
                                        </th>
                                        <th class="grid_header td_head">
                                            MST
                                        </th>
                                        <th class="grid_header td_head">
                                            Số CT
                                        </th>
                                        <th class="grid_header td_head">
                                            Năm CT
                                        </th>
                                        <th class="grid_header td_head">
                                            KH CT
                                        </th>
                                        <th class="grid_header td_head">
                                            Mã DVQL
                                        </th>
                                        <th class="grid_header td_head">
                                            TT NT
                                        </th>
                                        <th class="grid_header td_head">
                                            TT VND
                                        </th>
                                        <th class="grid_header td_head">
                                            Người lập
                                        </th>
                                        <th class="grid_header td_head">
                                            Người duyệt
                                        </th>
                                        <th class="grid_header td_head">
                                            Ngày lập
                                        </th>
                                        <th class="grid_header td_head">
                                            Ngày duyệt
                                        </th>
                                        <th class="grid_header td_head">
                                            Trạng thái
                                        </th>
                                    </tr>
                                    <tbody id="divDraw">
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
    
        $(document).ready(function(){
            $("#ddlTrangThaiCT").change(function(){
                fillData();
            });
            getDanhMucChiNhanh();
        });
        
        function getStringText(id)
        {
            return $("#"+ id).val();
        }
        
        function paginationList(listPaginationCount)
        {
            
            $('table.paginated').each(function() {
                var currentPage = 0;
                var numPerPage = listPaginationCount;    
                var $pager=$('.pager').remove();                
                var $table = $(this);
                $table.bind('repaginate', function() {
                    $table.find('tbody#divDraw tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody#divDraw tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div id="pager"></div>');
              
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function(event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');                    
                }
                
                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });
        }    
        
            $("#btnTruyVanKetQua").click(function(){            
            //Init model
            fillData();
            
        }); 
        
        function getDanhMucChiNhanh()
        {
             $.ajax({
                type: "POST",
                url: "frmBC_ThuPhi_BoNganh.aspx/getDanhMucChiNhanh",                
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) {
                         var modelGen=JSON.parse(data.d);    
                         
                         $("#ddlMaChiNhanh").append($('<option>').val("0").text("Tất cả"));
                         var listChiNhanh = modelGen.ListChiNhanh;
                         for(var i =0;i<listChiNhanh.length;i++)
                         {
                            $("#ddlMaChiNhanh").append($('<option>').val(listChiNhanh[i].ID).text(listChiNhanh[i].NAME));
                         }
                        
                   }                         
            });
        }
        
        function fillData()
        {
            var root=0;
            if($("#ddlMaChiNhanh option:selected").val() != "0")
            {
                root=1;
            }
            var InBaoCao = {
                SOHOSO: getStringText("textBoxSearchSoHoSo"),
                TUNGAY: getStringText("textBoxSearchTuNgayCT"),
                DENNGAY:getStringText("textBoxSearchDenNgayCT"),
                MST: getStringText("textBoxSearchMST"),
                NAM_CT:getStringText("textBoxSearchNam_CT"),
                TRANGTHAI_CT: $("#ddlTrangThaiCT option:selected").val(),
                MA_CN : $("#ddlMaChiNhanh option:selected").val(),
                ROOT :root
            };     
            
            $.ajax({
                type: "POST",
                url: "frmBC_ThuPhi_BoNganh.aspx/getPhiBoBanNganh",
                data: JSON.stringify(InBaoCao),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) {
                            /*Check json data*/ 
                            document.getElementById('divStatus').innerHTML = '';
                            try {
                                JSON.parse(data.d);
                            } catch (e) {
                                document.getElementById('divStatus').innerHTML = 'Không tìm thấy bản ghi nào';
                                return;
                            }
                         var modelGen=JSON.parse(data.d);            
                         var json =modelGen.Table;
                            
                         $("#divDraw").children().remove();
                         $("#pager").children().remove();
                         if(!(json==undefined))
                         {
                            for (var i = 0; i < json.length; i++) 
                         {
                            if(i%2==0)
                            {
                                tr = $('<tr class="grid_item td_head" />');
                            }
                            else
                            {
                                tr = $('<tr class="grid_item_alter td_head"  />');
                            }
                            if(json[i].MA_KS == null || json[i].MA_KS == ""){
                                json[i].NGUOIDUYET = "";
                                json[i].NGAY_XULY = "";
                            }
                            
                            tr.append('<td style="height:22px; text-align:center" class="td_head" >' + (i+1) + '</td>');
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + json[i].SO_HS + '</td>');
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + json[i].NNT_MST + '</td>');
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + json[i].SO_CT + '</td>');
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + json[i].NAM_CT_PT + '</td>');                          
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + json[i].KYHIEU_CT + '</td>');
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + json[i].MA_DVQL + '</td>');                         
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + accounting.formatNumber(json[i].TT_NT_TONGTIEN_NT) + '</td>');        
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + accounting.formatNumber(json[i].TT_NT_TONGTIEN_VND,2,".",",") + '</td>');        
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + json[i].NGUOILAP + '</td>');        
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + json[i].NGUOIDUYET + '</td>');                            
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + convertDateTime(json[i].NGAY_CT) + '</td>');        
                            tr.append('<td style="height:22px; text-align:center" class="td_head">' + convertDateTime(json[i].NGAY_XULY) + '</td>');
                            var trangthai_ct = json[i].TRANGTHAI_CT;
                            var tt_ct = json[i].TRANGTHAI_XULY;
                            
                            tr.append('<td  style="text-align:center" class="td_head">' +getTTCT(trangthai_ct,tt_ct) + '</td>');
                            
                            $('#divDraw').append(tr);  
                        }
                      
                        paginationList(20);       
                         }
                          
                                      
                    }
                });      
                
        }
        
        function convertDateTime(date)
        {
            var result='';            
            try
            {
                  var currentDate = date.substring(0, 10).split('-');
                  
                  var currentMonth = currentDate[1];
                  if(currentMonth.length ==1)
                  {
                    currentMonth = "0"+ currentMonth;
                  }
                  result=currentDate[2] +"-" + currentMonth +"-"+ currentDate[0];  
            }
            catch(err)
            {
               
            }
            return result;
        }
        
        
        function getTTCT(code,tt_ct)
        {  
            var textReturn ='';
            if(code=='00')
            {
                textReturn="Đã hoàn thiện";
            }
            if(code=='01' && tt_ct=='1')
            {
                textReturn="Đã kiểm soát";
            }
              if(code=='01' && tt_ct=='0')
            {
                textReturn="Chuyển thuế lỗi";
            }
            if(code=='02')
            {
                textReturn="Hủy bởi GDV chờ duyệt";
            }
            if(code=='03')
            {
                textReturn="Chuyển trả";
            }
            if(code=='04')
            {
                textReturn="Đã hủy";
            }
            if(code=='05')
            {
                textReturn="Chờ kiểm soát";
            }
            if(code=='07')
            {
                textReturn="Đã hủy bởi KSV";
            }
            
            if(code=='08')
            {
                textReturn="Duyệt hủy CT HQ lỗi";
            }
            
            
            return textReturn;
        }
        
        
    </script>

    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
    </script>

    <script type="text/javascript">
    
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd='0'+dd
            } 

            if(mm<10) {
                mm='0'+ mm
            } 

            today = dd+'/'+mm+'/'+yyyy;
            $("#textBoxSearchTuNgayCT").val(today);
            $("#textBoxSearchDenNgayCT").val(today);
            
        });
    </script>

</asp:Content>
