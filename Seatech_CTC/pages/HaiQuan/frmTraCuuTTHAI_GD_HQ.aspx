﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmTraCuuTTHAI_GD_HQ.aspx.vb" Inherits="pages_ChungTu_frmTraCuuTTHAI_GD_HQ" Title="Tra cứu thông tin trạng thái giao dịch hải quan" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../../jsdropdowlist/jquery.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">TRA CỨU THÔNG TIN TRẠNG THÁI GIAO DỊCH HẢI QUAN</asp:Label>
            </td>
        </tr>
        <tr>
            <td width='100%' align="left" valign="top" class='nav'>
                <div id="divTracuu">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width='90%' align="left" valign="top" >
                                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                    <tr>
                                        <td width='20%' align="left" valign="top" class="form_label">
                                            Số chứng từ: <span class="requiredField">(*)</span>
                                        </td>
                                        <td with='30%' align="left" valign="top" class="form_control">
                                            <input type="text" id='txtSoCT' class="inputflat" runat="server" style="width: 60%" />
                                        </td>
                                    </tr>
                                    
                                    
                                </table>
                            </td>
                        </tr>
                       
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnTraCuu" runat="server" Text="Tra Cứu" CssClass="ButtonCommand" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="divKQXK" runat="server">
                    <p><b>Thông tin trạng thái giao dịch</b></p>
                    <table id="grdHeader" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 97%; border-collapse: collapse;">
                        <tr>
                            <td width="30%">
                                <b>Số tiếp nhận chứng từ</b>
                            </td>
                            <td colspan="2">
                                <input type="text" id="txtSo_TN_CT" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ngày tiếp nhận chứng từ</b>
                            </td>
                            <td colspan="2">
                                <input type="text" id="txtNgay_TN_CT" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Kết quả tra cứu</b>
                            </td>
                            <td colspan="2">
                                <input type="text" id="txtKQ_TraCuu" class="inputflat" runat="server" style="width: 99%;" />
                            </td>
                        </tr>
                    </table>
                    
                </div>
                
            </td>
        </tr>
    </table>
</asp:Content>
