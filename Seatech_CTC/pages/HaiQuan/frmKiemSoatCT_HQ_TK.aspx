﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage07.master" AutoEventWireup="false"
    CodeFile="frmKiemSoatCT_HQ_TK.aspx.vb" Inherits="pages_ChungTu_frmKiemSoatCT_HQ_TK"
    Title="Kiểm soát chứng từ hải quan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">


        $(document).ready(function () {
            var strTT = $get('<%=hdfTrangThai_CT.ClientID%>').value

            if (strTT.length > 0) {
                if (strTT == "00" || strTT == "01" || strTT == "02" || strTT == "03" || strTT == "04" || strTT == "06" || strTT == "19") {
                    document.getElementById('cmdKS').setAttribute("disabled", "disabled");
                } else if (strTT == "05") {
                    document.getElementById('cmdKS').removeAttribute("disabled");
                }

            } else {
                document.getElementById('cmdKS').setAttribute("disabled", "disabled");
            }

        });
        function jsShowCustomerSignature() {
            if ($get('<%=txtTK_KH_NH.clientID%>').value != '') {
                window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $get('<%=txtTK_KH_NH.clientID%>').value, "", "dialogWidth:700px;dialogHeight:500px;help:0;status:0;", "_new");
            }
            else {
                alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');

            }
        }

        function DG_changeBackColor(row, highlight) {
            if (highlight) {
                row.style.cursor = "hand";
                lastColor = row.style.backgroundColor;
                row.style.backgroundColor = '#C6D6FF';
            }
            else
                row.style.backgroundColor = lastColor;
        };

        function jsFormatNumber(txtCtl) {
            document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.', ''), '.');
        }

        function localeNumberFormat(amount, delimiter) {
            try {

                amount = parseFloat(amount);
            } catch (e) {
                throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
                return null;
            }
            if (delimiter == null || delimiter == undefined) { delimiter = '.'; }

            // convert to string
            if (amount.match != 'function') { amount = amount.toString(); }

            // validate as numeric
            var regIsNumeric = /[^\d,\.-]+/igm;
            var results = amount.match(regIsNumeric);

            if (results != null) {
                outputText('INVALID NUMBER', eOutput)
                return null;
            }
        }
        function disableButton() {
            document.getElementById('cmdKS').setAttribute("disabled", "disabled");
            //document.getElementById('#cmdKS').disabled = true;
            document.getElementById('<%=cmdChuyenTra.clientID%>').disabled = true;
            document.getElementById('<%=cmdChuyenThue.clientID%>').disabled = true;
            //                document.getElementById('<%=cmdHuy.clientID%>').disabled=true;
        }
        function XacNhan() {
            var ldhuy;
            ldhuy = $get('<%=txtLyDoHuy.clientID%>').value.trim();
            if (ldhuy == '') {
                alert('Bạn phải nhập lý do hủy.');
                document.getElementById($get('<%=txtLyDoHuy.clientID%>')).focus();
                return;
            } else {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                if (confirm("Bạn có thực sự muốn Duyệt hủy cho chứng từ này?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
                document.forms[0].appendChild(confirm_value);
            }
        }



        function jsProcessWarningMessage(message) {
            var xxx = confirm("Thông báo : " + message + ' Bạn có muốn tiếp tục thực hiện hạch toán chứng từ ?');
            if (xxx == true) {
                document.getElementById("<%= cmdKSBK.ClientID%>").click();
            }
        };

        function jsProcessKS(message) {

            //console.log("xxxx");
            document.getElementById("<%= cmdKSBK.ClientID%>").click();
        };


        function jsValidateKS() {
            var strSHKB = $get('<%=txtSHKB.ClientID%>').value.trim();
            var strMa_NH_B = $get('<%=txtMA_NH_B.ClientID%>').value.trim();
            var strLanKS = $get('<%=hdfClickKS.ClientID%>').value.trim();
            var strSo_CT = $get('<%=hdfSo_CT.ClientID%>').value.trim();
            if (typeof strSHKB !== 'undefined' && strSHKB !== null && strSHKB.toString() !== '') {
                PageMethods.ValidNHSHB_SP(strSHKB, strMa_NH_B, strLanKS, strSo_CT, ValidateKS_Complete, ValidateKS_Error);
            }
        }



        function ValidateKS_Complete(result, methodName) {
            if (result.length > 0) {
                var kq = result;
                
                var arrkq = kq.split(";");
                var validKB = arrkq[0].toString();
                var validLanKS = arrkq[1];
                if (parseInt(validLanKS) > 0) {
                    alert('Chứng từ đã từng kiểm soát. Bạn cần check hạch toán CORE');
                    document.getElementById('cmdKS').setAttribute("disabled", "disabled");
                    //var aaa = confirm('Chứng từ đã từng nhấn KS, bạn có muốn tiếp tục?');
                    //if (aaa == true) {
                        //if (validKB == "TRUE") {
                            //document.getElementById("<%= cmdKSBK.ClientID%>").click();
                        //} else if (validKB == "FALSE") {
                            //var bbb = confirm('Kho bạc trên có mở tài khoản tại SHB. Bạn có muốn tiếp tục  hạch toán chứng từ với ngân hàng hưởng đã chọn?');
                            //if (bbb == true) {
                             //   document.getElementById("<%= cmdKSBK.ClientID%>").click();
                            //}
                        //}
                //}
            } else {
                    //console.log(validKB);
                if (validKB == "TRUE") {
                    //console.log("xxx");
                    document.getElementById("<%= cmdKSBK.ClientID%>").click();
                    } else if (validKB == "FALSE") {
                        var ccc = confirm('Kho bạc trên có mở tài khoản tại SHB. Bạn có muốn tiếp tục  hạch toán chứng từ với ngân hàng hưởng đã chọn?');
                        if (ccc == true) {
                            document.getElementById("<%= cmdKSBK.ClientID%>").click();
                        }
                    }
            }

        }
    }


    function ValidateKS_Error(error, userContext, methodName) {
        //console.log("yyyy");
    }
    </script>

    <style type="text/css">
        .tblHeader tr td, .tblNganHang tr td {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <%--<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>--%>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KIỂM SOÁT CHỨNG TỪ HẢI QUAN</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr height="8px">
                        <td></td>
                    </tr>
                    <tr>
                        <td width='100%' align="center" valign="top" style="padding-left: 3; padding-right: 3">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width='900' align="left" valign="top" class="frame">
                                        <asp:HiddenField ID="hdfMaNNT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSHKB" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfNgay_KB" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSo_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfKyHieu_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSo_BT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfMa_Dthu" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfMa_NV" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfTrangThai_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfTrangThai_CThue" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfLoai_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfDienGiai" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSoCTNH" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfMACN" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfClickKS" runat="server" Value="" />
                                        <table width="100%" align="center">
                                            <tr>
                                                <td valign="top" width="210px" height="300px" align="left">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr align="left">
                                                            <td>
                                                                <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                                                    ShowHeader="True" ShowFooter="True" BorderColor="#000" CssClass="grid_data" PageSize="15"
                                                                    Width="100%">
                                                                    <PagerStyle CssClass="cssPager" />
                                                                    <AlternatingRowStyle CssClass="grid_item_alter" />
                                                                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                                                                    <RowStyle CssClass="grid_item" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="TT">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <img src=" <%#Business_HQ.CTuCommon.Get_ImgTrangThai(Eval("TRANG_THAI").ToString(),Eval("TT_CTHUE").ToString(),Eval("Ma_KS").ToString())%>"
                                                                                    alt=" <%#Business_HQ.CTuCommon.Get_TrangThai(Eval("TRANG_THAI").ToString())%>" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Số CT" FooterText="Tổng số CT:">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <FooterStyle HorizontalAlign="Left"></FooterStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "so_ct") %>'
                                                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SHKB") & "," & DataBinder.Eval(Container.DataItem, "NGAY_KB") & "," & DataBinder.Eval(Container.DataItem, "SO_CT") & "," & DataBinder.Eval(Container.DataItem, "KYHIEU_CT") & "," & DataBinder.Eval(Container.DataItem, "SO_BT") & "," & DataBinder.Eval(Container.DataItem, "MA_DTHU") & "," & DataBinder.Eval(Container.DataItem, "MA_NV") & "," & Eval("TRANG_THAI").ToString() & "," & Eval("MA_NNTHUE").ToString() & "," & Eval("LOAI_CTU").ToString() & "," & Eval("SO_CT_NH").ToString() & "," & Eval("SAVE_TYPE").ToString()%>'
                                                                                    OnClick="btnSelectCT_Click" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Tên ĐN/Số BT">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <FooterStyle Font-Bold="true" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblMaNV" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TEN_DN") %>' />
                                                                                <asp:Label ID="lblDelimiter" runat="server" Text='/' />
                                                                                <asp:Label ID="lblSoBT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SO_BT") %>' />
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <%#grdDSCT.Rows.Count.ToString%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <asp:Button ID="cmdRefresh" runat="server" Text="[Refresh]" AccessKey="K" CssClass="ButtonCommand"
                                                                    UseSubmitBehavior="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">Trạng thái chứng từ:
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%'>
                                                                <asp:DropDownList ID="ddlTrangThai" runat="server" CssClass="inputflat" Width="100%"
                                                                    AutoPostBack="true">
                                                                    <asp:ListItem Value="99" Text="Tất cả"></asp:ListItem>
                                                                    <asp:ListItem Value="05" Text="Chờ duyệt"></asp:ListItem>
                                                                    <asp:ListItem Value="01" Text="Đã duyệt"></asp:ListItem>
                                                                    <asp:ListItem Value="03" Text="Chuyển trả"></asp:ListItem>
                                                                    <asp:ListItem Value="04" Text="Đã hủy "></asp:ListItem>
                                                                    <asp:ListItem Value="19" Text="Điều chỉnh chờ kiểm soát"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">Giao dịch viên thực hiện:
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%'>
                                                                <asp:DropDownList ID="ddlMacker" runat="server" CssClass="inputflat" Width="100%"
                                                                    AutoPostBack="true">
                                                                    <asp:ListItem Value="99" Text="Tất cả"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td style="width: 100%" colspan="3">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr style="display: none">
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/ChuaKS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Chưa Kiểm soát
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/ChuyenKS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Chờ Duyệt
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/DaKS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Đã Duyệt
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/KSLoi.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Chuyển trả
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/Huy.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Giao dịch hủy
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/HuyHS.gif" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Giao dịch hủy - chờ duyệt
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/Huy_KS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Kiểm soát duyệt hủy GD
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/TT_CThueLoi.gif" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Chuyển thông tin thuế lỗi
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/Huy_CT_Loi.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Duyệt hủy chuyển thuế lỗi
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/Huy_CT_Loi.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Hủy chuyển thuế/HQ lỗi
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/ChuaNhapBDS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Điều chỉnh chờ kiểm soát
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td style="width: 100%" colspan="3">Alt + K : Kiểm soát<br />
                                                                Alt + C : Chuyển trả
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td style="width: 100%" colspan="3">Đối với Firefox
                                                                <br />
                                                                Alt + Shift + K : Kiểm soát<br />
                                                                Alt + Shift + C : Chuyển trả
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" width="680px">
                                                    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                        <tr>
                                                            <td colspan="2" width='100%' align="right">
                                                                <table width="100%">
                                                                    <tr class="grid_header">
                                                                        <td height="15px" align="left">
                                                                            <%--Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập--%>
                                                                            THÔNG TIN CHỨNG TỪ THUẾ HẢI QUAN
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div id="Panel1" style="/*height: 380px; */ width: 100%; /*overflow-y: scroll; */">
                                                                    <table runat="server" id="grdHeader" cellspacing="0" cellpadding="1" rules="all"
                                                                        border="1" style="border-color: #989898; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse; margin-left: 2px;"
                                                                        class="tblHeader">
                                                                        <tr id="rowMaNNT">
                                                                            <td width="24%">
                                                                                <b>Mã số thuế/Tên </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <asp:TextBox runat="server" ID="txtMaNNT" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td width="51%">
                                                                                <asp:TextBox runat="server" ID="txtTenNNT" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDChiNNT">
                                                                            <td>
                                                                                <b>Địa chỉ người nộp thuế</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtDChiNNT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowQuanNNThue">
                                                                            <td>
                                                                                <b>Quận(Huyện)</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtQuanNNThue" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenQuanNNThue" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTinhNNThue">
                                                                            <td>
                                                                                <b>Thành phố(Tỉnh)</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTinhNNThue" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenTinhNNThue" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowToKhaiSo">
                                                                            <td>
                                                                                <b>Tờ khai số</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtToKhaiSo" CssClass="inputflat3" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtDescToKhaiSo" runat="server" Width="99%" CssClass="inputflat">
                                                                                </asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNgayDK" style="display: none">
                                                                            <td>
                                                                                <b>Ngày đăng ký</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtNgayDK" CssClass="inputflat3" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowLHXNK" style="display: none">
                                                                            <td>
                                                                                <b>Loại hình XNK</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtLHXNK" CssClass="inputflat3" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtDescLHXNK" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaCuc">
                                                                            <td>
                                                                                <b>Mã cục/Tên Cục</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaCuc" CssClass="inputflat3" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="TxtTenCuc" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaHQ">
                                                                            <td>
                                                                                <b>Mã HQ</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaHQ" CssClass="inputflat3" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtDescMaHQ" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowCQThu">
                                                                            <td>
                                                                                <b>CQ Thu </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaCQThu" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenCQThu" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaHQPH">
                                                                            <td>
                                                                                <b>Mã HQ PH</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaHQPH" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenMaHQPH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSHKB">
                                                                            <td width="24%">
                                                                                <b>Số hiệu KB </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <asp:TextBox runat="server" ID="txtSHKB" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td width="51%">
                                                                                <asp:TextBox runat="server" ID="txtTenKB" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDBHC" style="">
                                                                            <td style="">
                                                                                <b>Mã tỉnh/thành Kho bạc </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td style="">
                                                                                <asp:TextBox runat="server" ID="txtMaDBHC" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td style="">
                                                                                <asp:TextBox runat="server" ID="txtTenDBHC" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTKCo">
                                                                            <td style="">
                                                                                <b>TK Có NSNN </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTKCo" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenTKCo" CssClass="inputflat" Visible="false"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNgay_KH_NH">
                                                                            <td>
                                                                                <b>Ngày lập CT</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtNGAY_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtDesNGAY_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <table width="100%">
                                                                    <tr class="grid_header">
                                                                        <td height="15px" align="left">THÔNG TIN NGÂN HÀNG
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div id="Panel2" style="/*height: 380px; */ width: 100%; /*overflow-y: scroll; */">
                                                                    <table runat="server" id="tblNganHang" cellspacing="0" cellpadding="1" rules="all"
                                                                        border="1" style="border-color: #989898; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse; margin-left: 2px;"
                                                                        class="tblNganHang">
                                                                        <tr id="rowMaNT">
                                                                            <td style="width: 24%">
                                                                                <b>Nguyên tệ</b>
                                                                            </td>
                                                                            <td style="width: 25%">
                                                                                <asp:TextBox runat="server" ID="txtMaNT" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td style="width: 51%">
                                                                                <asp:TextBox runat="server" ID="txtTenNT" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTyGia">
                                                                            <td>
                                                                                <b>Tỷ giá</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTyGia" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr id="rowLoaiThue" style="display: none;">
                                                                            <td>
                                                                                <b>Loại thuế</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList runat="server" ID="ddlLoaiThue" CssClass="inputflat" Enabled="false"
                                                                                    Width="98%">
                                                                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                                                                    <%--<asp:ListItem Value="01" Text="01" Selected="True"></asp:ListItem>
                                                                                            <asp:ListItem Value="03" Text="03"></asp:ListItem>
                                                                                            <asp:ListItem Value="04" Text="04"></asp:ListItem>
                                                                                            <asp:ListItem Value="05" Text="05"></asp:ListItem>--%>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtDescLoaiThue" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowHTTT">
                                                                            <td>
                                                                                <b>Hình thức thanh toán</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList runat="server" ID="ddlHTTT" CssClass="inputflat" Enabled="false"
                                                                                    Width="100%">
                                                                                    <%--<asp:ListItem Value="00" Text="Nộp tiền mặt" Selected="True"></asp:ListItem>--%>
                                                                                    <asp:ListItem Value="01" Text="Chuyển khoản" Selected="True"></asp:ListItem>
                                                                                    <%--<asp:ListItem Value="02" Text="Điện chuyển tiền"></asp:ListItem>
                                                                                    <asp:ListItem Value="03" Text="Chuyển khoản nội bộ"></asp:ListItem>--%>
                                                                                    <asp:ListItem Value="05" Text="Tiền mặt"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenHTTT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTK_KH_NH">
                                                                            <td>
                                                                                <b>TK Chuyển </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="90%" />
                                                                                <img id="img1" alt="Chọn từ danh mục đặc biệt" src="../../images/icons/MaDB.png"
                                                                                    onclick="jsShowCustomerSignature();" style="width: 16px; height: 16px; display: none" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoDuNH">
                                                                            <td>
                                                                                <b>Số dư khả dụng</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtSoDu_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtDesc_SoDu_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTKGL">
                                                                            <td>
                                                                                <b>TK GL Chuyển </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTKGL" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenTKGL" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNNTien">
                                                                            <td>
                                                                                <b>MST /Tên người nộp tiền</b><%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDChiNNTien">
                                                                            <td>
                                                                                <b>Địa chỉ người nộp tiền</b><%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtDChiNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowQuanNNTien">
                                                                            <td>
                                                                                <b>Quận(Huyện)</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtQuanNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenQuanNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTPNNTien">
                                                                            <td>
                                                                                <b>Thành phố(Tỉnh)</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTinhNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenTinhNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoCMND" style="display: none;">
                                                                            <td>
                                                                                <b>Số chứng minh</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtCK_SoCMND" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoDT" style="display: none;">
                                                                            <td>
                                                                                <b>Số điện thoại</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtCK_SoDT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNganHangChuyen">
                                                                            <td>
                                                                                <b>Mã NH Chuyển </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMA_NH_A" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTen_NH_A" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNhanHangTT">
                                                                            <td>
                                                                                <b>Mã NH TT </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMA_NH_TT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTEN_NH_TT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNganHangNhan">
                                                                            <td>
                                                                                <b>Mã NH TH </b>
                                                                                <%--<span class="requiredField">(*)</span>--%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMA_NH_B" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTen_NH_B" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoBK" style="display: none">
                                                                            <td>
                                                                                <b>Số bảng kê</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtSo_BK" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNgayBK" style="display: none">
                                                                            <td>
                                                                                <b>Ngày bảng kê</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtNgay_BK" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowQuan_HuyenNNTien">
                                                                            <td>
                                                                                <b>Quận(huyện)</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtQuan_HuyenNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTinhNNTien">
                                                                            <td>
                                                                                <b>Thành phố(Tỉnh)</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtTinh_TphoNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTKNo" style="display: none">
                                                                            <td style="background-color: Aqua">
                                                                                <b>TK Nợ NSNN </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTKNo" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenTKNo" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoKhung">
                                                                            <td>
                                                                                <b>Số khung</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtSoKhung" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtDescSoKhung" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoMay">
                                                                            <td>
                                                                                <b>Số máy</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtSoMay" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtDescSoMay" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDienGiai" style="display: none">
                                                                            <td>
                                                                                <b>Diễn giải</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtDienGiai" Style="width: 99%;" CssClass="inputflat"
                                                                                    MaxLength="210" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDienGiaiHQ">
                                                                            <td align="left">
                                                                                <b>Diễn giải HQ</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtDienGiaiHQ" Style="width: 99%;" CssClass="inputflat"
                                                                                    TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                                                                <%--<textarea id="txtDienGiaiHQ" style="width: 99%;" class="inputflat" rows="2" cols="1"
                                                                                    maxlength="200"></textarea>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="visibility: hidden">
                                                            <td width="25%" style="height: 0px">
                                                                <asp:TextBox runat="server" ID="txtSo_BT" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                    Width="99%" />
                                                                <asp:TextBox runat="server" ID="txtSoCT" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                    Width="99%" />
                                                                <asp:TextBox runat="server" ID="txtSoCT_NH" CssClass="inputflat" ReadOnly="true"
                                                                    Visible="false" Width="99%" />
                                                            </td>
                                                            <td width="51%" style="height: 0px">
                                                                <asp:TextBox runat="server" ID="txtTRANG_THAI" CssClass="inputflat" ReadOnly="true"
                                                                    Visible="false" Width="99%" />
                                                                <asp:TextBox runat="server" ID="txtMA_NV" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                    Width="99%" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2' align="left" class="errorMessage">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="100%" align="right">
                                                                            <asp:Label ID="lbSoFT" runat="server" Style="font-weight: bold;" Visible="false"></asp:Label>
                                                                            / 
                                                                            <asp:Label ID="lbSoCT" runat="server" Style="font-weight: bold;" Visible="false"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" width='99%' colspan="2">
                                                                            <asp:UpdateProgress ID="updProgress" runat="server">
                                                                                <ProgressTemplate>
                                                                                    <div style="background-color: Aqua;">
                                                                                        <b style="font-size: 15pt">Đang lấy dữ liệu tại server.Xin chờ một lát...</b>
                                                                                    </div>
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress>
                                                                            <b>
                                                                                <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="grid_header">
                                                                        <td height="15px" align="left">CHI TIẾT CHỨNG TỪ THUẾ HẢI QUAN
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" width="100%">
                                                                            <asp:DataGrid ID="grdChiTiet" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                                ShowHeader="true" Width="100%" CssClass="grid_data">
                                                                                <HeaderStyle CssClass="grid_header"></HeaderStyle>
                                                                                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                                                                <ItemStyle CssClass="grid_item" />
                                                                                <Columns>
                                                                                    <asp:TemplateColumn Visible="false">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkRemove" runat="server" Enabled="false" Checked="true" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Mã quỹ" Visible="false">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="0px"></HeaderStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="MA_CAP" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MAQUY") %>'
                                                                                                Font-Bold="true" MaxLength="2" ToolTip="Ấn phím Enter để chọn mã cấp"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Số TK" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SO_TK" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "SO_TK") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Mã HQ" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="25px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="MA_HQ" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_HQ") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Sắc thuế" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="25px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SAC_THUE" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "SAC_THUE") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Mã LH" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="25px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="MA_LHXNK" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_LHXNK") %>'
                                                                                                Font-Bold="true" MaxLength="3"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Ngày TK" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="NGAY_TK" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "NGAY_TK") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Mã LT" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="MA_LT" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_LT") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Chương" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="MA_CHUONG" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_CHUONG") %>'
                                                                                                Font-Bold="true" MaxLength="3" ToolTip="Ấn phím Enter để chọn mã chương"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Khoản" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="0px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="MA_KHOAN" runat="server" Width="0%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_KHOAN") %>'
                                                                                                Font-Bold="true" MaxLength="3" ToolTip="Ấn phím Enter để chọn mã khoản"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Tiểu Mục" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="MA_MUC" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_TMUC") %>'
                                                                                                Font-Bold="true" MaxLength="4" ToolTip="Ấn phím Enter để chọn mã tiểu mục" AutoPostBack="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Nội dung" ItemStyle-HorizontalAlign="Left">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="NOI_DUNG" runat="server" Width="97%" BorderColor="white" CssClass="inputflat"
                                                                                                ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "NOI_DUNG") %>'
                                                                                                MaxLength="200" Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Số tiền" ItemStyle-HorizontalAlign="Right">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SOTIEN" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "TTIEN") %>'
                                                                                                AutoPostBack="true" onfocus="this.select()" Font-Bold="true">
                                                                                            </asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Số tiền NT" ItemStyle-HorizontalAlign="Right">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SOTIEN_NT" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "SOTIEN_NT") %>'
                                                                                                AutoPostBack="true" onfocus="this.select()" Font-Bold="true">
                                                                                            </asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Kỳ thuế" ItemStyle-HorizontalAlign="Right" Visible="false">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="KY_THUE" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "KY_THUE") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan='2' height='5'>
                <asp:TextBox runat="server" ID="txtRM_REF_NO" CssClass="inputflat" Visible="false"
                    ReadOnly="true" Text="" />
                <asp:TextBox runat="server" ID="txtREF_NO" CssClass="inputflat" Visible="false" ReadOnly="true"
                    Text="" />
                <asp:TextBox runat="server" ID="txtTimeLap" CssClass="inputflat" Visible="false"
                    ReadOnly="true" Text="" />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <table width="600px">
                    <tr align="right">
                        <td colspan="2">Tổng Tiền ngoại tệ:
                            <asp:TextBox runat="server" ID="txtTTien_NT" CssClass="inputflat" ReadOnly="true"
                                Text="0" Style="text-align: right" />
                        </td>
                        <td>Tổng Tiền nguyên tệ:
                            <asp:TextBox runat="server" ID="txtTTIEN" CssClass="inputflat" ReadOnly="true" Text="0"
                                Style="text-align: right" />
                        </td>
                    </tr>
                    <tr align="right">
                        <td align="right">Phí:
                            <asp:TextBox runat="server" ID="txtCharge" Width="100px" CssClass="inputflat" ReadOnly="true"
                                Text="0" Style="text-align: right" />
                        </td>
                        <td align="right">VAT:
                            <asp:TextBox runat="server" ID="txtVAT" Width="100px" CssClass="inputflat" ReadOnly="true"
                                Text="0" Style="text-align: right" />
                        </td>
                        <td align="right">Tổng trích nợ:
                            <asp:TextBox runat="server" ID="txtTongTrichNo" Width="120px" CssClass="inputflat"
                                ReadOnly="true" Text="0" Style="text-align: right" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">Bằng chữ:
                            <asp:Label runat="server" ID="lblTTien_Chu" Style="text-align: right" CssClass="inputflat"
                                ReadOnly="true" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <table width="100%">
                    <tr align="right" style="display: none">
                        <td align="right">eTax : Phí
                            <asp:TextBox runat="server" ID="txtCharge2" Width="100px" CssClass="inputflat" ReadOnly="true"
                                Text="0" Style="text-align: right" />
                        </td>
                        <td align="right">VAT
                            <asp:TextBox runat="server" ID="txtVat2" Width="100px" CssClass="inputflat" ReadOnly="true"
                                Text="0" Style="text-align: right" />
                        </td>
                        <td align="right">Tổng trích nợ
                            <%--<asp:TextBox runat="server" ID="txtTongTrichNo" Width="120px" CssClass="inputflat"
                                        Style="text-align: right" ReadOnly="true" Text="0" />--%>
                        </td>
                    </tr>
                    <tr align="right" style="display: none">
                        <td align="left" colspan="3">Đi Citad
                            <asp:RadioButton ID="RadioButton1" Text="Kiểu mới" GroupName="RGCitad" runat="server"
                                Enabled="false" />
                            <asp:RadioButton ID="RadioButton2" Text="Kiểu cũ" GroupName="RGCitad" runat="server"
                                Enabled="false" />
                        </td>
                    </tr>
                    <tr align="right">
                        <td align="right" valign="top" colspan="3">Ghi chú
                            <asp:TextBox ID="txtGhiChu" runat="server" CssClass="inputflat" Width="50%" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="right">
                        <td align="right" valign="top" colspan="3">Lý do chuyển trả
                            <asp:TextBox ID="txtLyDoHuy" runat="server" CssClass="inputflat" Width="50%"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td align="center" valign="top" class='text'>
                            <input type="button" id="cmdKS" class="ButtonCommand" value="[(K)iểm soát]" style="" accesskey="K" onclick="jsValidateKS(); disableButton();" />
                            <%--     <asp:Button ID="cmdKS" runat="server" Text="[(K)iểm soát]" CssClass="ButtonCommand" UseSubmitBehavior="false"
                                OnClientClick="disableButton();" />--%>
                            &nbsp;
                            <asp:Button ID="cmdDieuChinh" runat="server" Text="[Điều chỉnh]" AccessKey="K" CssClass="ButtonCommand"
                                UseSubmitBehavior="false" OnClientClick="disableButton();" Visible="false" />
                            &nbsp;
                            <asp:Button ID="cmdChuyenTra" runat="server" Text="[(C)huyển trả]" AccessKey="C" CssClass="ButtonCommand"
                                UseSubmitBehavior="false" OnClientClick="disableButton();" />
                            &nbsp;
                            <asp:Button ID="cmdHuy" runat="server" Text="[Duyệt CT(H)ủy]" CssClass="ButtonCommand"
                                UseSubmitBehavior="false" OnClientClick="XacNhan();" />&nbsp;
                            <asp:Button ID="cmdChuyenThue" runat="server" Text="[(G)ửi lại]" CssClass="ButtonCommand"
                                UseSubmitBehavior="false" OnClientClick="disableButton();" />&nbsp;
                            <asp:Button ID="cmdChuyenBDS" runat="server" Text="[Chuyển (B)DS]" AccessKey="B"
                                Visible="false" CssClass="ButtonCommand" UseSubmitBehavior="false" OnClientClick="disableButton();" />
                            <asp:Button ID="btnIn" runat="server" Text="[In CT]" AccessKey="I" CssClass="ButtonCommand"
                                UseSubmitBehavior="false" Visible="false" OnClientClick="disableButton();" />&nbsp;
                              <div style="display: none">
                                  <asp:Button ID="cmdKSBK" runat="server" Text="[(K)iểm soát] BK" CssClass="ButtonCommand"
                                      UseSubmitBehavior="false" OnClientClick="disableButton();" />
                              </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
