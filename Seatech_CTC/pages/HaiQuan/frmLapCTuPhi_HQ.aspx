﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmLapCTuPhi_HQ.aspx.vb"
    Inherits="pages_HaiQuan_frmLapCTuPhi_HQ" EnableEventValidation="true" MasterPageFile="~/shared/MasterPage05.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/ChuyenSoSangChu.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script src="../../javascript/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>

    <script src="../../javascript/jquery/1.11.2/jquery-ui.min.js" type="text/javascript"></script>

    <script src="../../javascript/json/json2.js" type="text/javascript"></script>

    <script src="../../javascript/accounting.min.js" type="text/javascript"></script>

    <script src="../../javascript/object/chungTu.js" type="text/javascript"></script>

    <script src="../../javascript/table/paging.js" type="text/javascript"></script>
    
    <link href="../../javascript/table/paging.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">
   
   //Bien javascript cuc bo
   var maTienTe = 'VND';
     var feeRate= 2;
        var minFee= 10000;  
    /********************CLASS DEFINITION********************/
    function infHDR(SO_XCARD , Ten_NH_B , TrangThai_BL , Kenh_CT , Ngay_HD_NT , Ngay_HDon , Ngay_VTD , HDon , VTD , HD_NT , MA_HQ , TEN_HQ , TEN_HQ_PH , LOAI_TT , TEN_MA_HQ , LY_DO_CTRA , KHCT , Ten_kb , Ten_nv , Huyen_nnthue , Tinh_nnthue , Ten_ks , Ten_cqthu , Lhxnk , Vt_lhxnk , Ten_lhxnk , Ten_nt , Ten_nh_a , Ttien_tthu , Ghi_chu , Tk_ns , Ten_tk_ns , Phuong_thuc , Hinh_thuc , Khoi_phuc , Reponse_code , Hq_tran_id , Hq_loai_thue , Res_hq , So_bl , Songay_bl , Kieu_bl , Ma_hq_ph , Dien_giai , Trangthai_lapct , Ngay_batdau , Ngay_ketthuc , Error_code_hq , So_ct_ctu , Dchieu_hq , Is_ma_hq , Ngay_dc_hq , Kb_thu_ho , Ma_loaitien , Ma_hq_cqt , So_bt_hq , QUAN_HUYEN_NNTIEN , TKHT_NH , TEN_TK_NO , TEN_TK_CO , TINH_TPHO_NNTIEN , So_RM , MATELLER_GDV , MATELLER_KSV , RM_REF_NO , REF_NO , TK_KB_NH , Mode , PT_TT , TEN_KH_NH , TK_GL_NH , SHKB , Ngay_KB , Ma_NV , So_BT , Ma_DThu , So_BThu , KyHieu_CT , So_CT , So_CT_TuChinh , So_CT_NH , Ma_NNTien , Ten_NNTien , DC_NNTien , Ma_NNThue , Ten_NNThue , DC_NNThue , Ly_Do , Ma_KS , Ma_TQ , So_QD , Ngay_QD , CQ_QD , Ngay_CT , Ngay_HT , Ma_CQThu , XA_ID , Ma_Tinh , Ten_tinh, Ma_Huyen , Ma_Xa , Ten_Xa , TK_No , TK_Co , So_TK , Ngay_TK , LH_XNK , DVSDNS , Ten_DVSDNS , Ma_LThue , TG_ID , Ma_NT , Ty_Gia , So_Khung , So_May , TK_KH_NH , NGAY_KH_NH , MA_NH_A , MA_NH_B , TTien , TT_TThu , Lan_In , Trang_Thai , TK_KH_Nhan , Ten_KH_Nhan , Diachi_KH_Nhan , TTien_NT , So_BT_KTKB , So_BK , Ngay_BK , TT_BDS , TT_NSTT , PHI_GD , PHI_VAT , PHI_GD2 , PHI_VAT2 , PT_TINHPHI , MA_NH_TT , TEN_NH_TT , CIF , SAN_PHAM , TT_CITAD , SEQ_NO , LOAI_BL , MA_CAP_CHUONG , TIME_BEGIN , SO_CMND , SO_FONE , MA_DV_DD , TEN_DV_DD , MA_NTK , Ma_CN , VTD2 , Ngay_VTD2 , VTD3 , Ngay_VTD3 , VTD4 , Ngay_VTD4 , VTD5 , Ngay_VTD5 , MA_HQ_KB, LOAI_CTU, Ma_Huyen_NNThue, Ma_Tinh_NNThue, MA_QUAN_HUYENNNTIEN, MA_TINH_TPNNTIEN, NGAY_BAOCO, NGAY_BAONO, TT_CTHUE) {
        this.SO_XCARD = SO_XCARD;
        this.Ten_NH_B = Ten_NH_B;
        this.TrangThai_BL = TrangThai_BL;
        this.Kenh_CT = Kenh_CT;
        this.Ngay_HD_NT = Ngay_HD_NT;
        this.Ngay_HDon = Ngay_HDon;
        this.Ngay_VTD = Ngay_VTD;
        this.HDon = HDon;
        this.VTD = VTD;
        this.HD_NT = HD_NT;
        this.MA_HQ = MA_HQ;
        this.TEN_HQ = TEN_HQ;
        this.TEN_HQ_PH = TEN_HQ_PH;
        this.LOAI_TT = LOAI_TT;
        this.TEN_MA_HQ = TEN_MA_HQ;
        this.LY_DO_CTRA = LY_DO_CTRA;
        this.KHCT = KHCT;
        this.Ten_kb = Ten_kb;
        this.Ten_nv = Ten_nv;
        this.Huyen_nnthue = Huyen_nnthue;
        this.Tinh_nnthue = Tinh_nnthue;
        this.Ten_ks = Ten_ks;
        this.Ten_cqthu = Ten_cqthu;
        this.Lhxnk = Lhxnk;
        this.Vt_lhxnk = Vt_lhxnk;
        this.Ten_lhxnk = Ten_lhxnk;
        this.Ten_nt = Ten_nt;
        this.Ten_nh_a = Ten_nh_a;
        this.Ttien_tthu = Ttien_tthu;
        this.Ghi_chu = Ghi_chu;
        this.Tk_ns = Tk_ns;
        this.Ten_tk_ns = Ten_tk_ns;
        this.Phuong_thuc = Phuong_thuc;
        this.Hinh_thuc = Hinh_thuc;
        this.Khoi_phuc = Khoi_phuc;
        this.Reponse_code = Reponse_code;
        this.Hq_tran_id = Hq_tran_id;
        this.Hq_loai_thue = Hq_loai_thue;
        this.Res_hq = Res_hq;
        this.So_bl = So_bl;
        this.Songay_bl = Songay_bl;
        this.Kieu_bl = Kieu_bl;
        this.Ma_hq_ph = Ma_hq_ph;
        this.Dien_giai = Dien_giai;
        this.Trangthai_lapct = Trangthai_lapct;
        this.Ngay_batdau = Ngay_batdau;
        this.Ngay_ketthuc = Ngay_ketthuc;
        this.Error_code_hq = Error_code_hq;
        this.So_ct_ctu = So_ct_ctu;
        this.Dchieu_hq = Dchieu_hq;
        this.Is_ma_hq = Is_ma_hq;
        this.Ngay_dc_hq = Ngay_dc_hq;
        this.Kb_thu_ho = Kb_thu_ho;
        this.Ma_loaitien = Ma_loaitien;
        this.Ma_hq_cqt = Ma_hq_cqt;
        this.So_bt_hq = So_bt_hq;
        this.QUAN_HUYEN_NNTIEN = QUAN_HUYEN_NNTIEN;
        this.TKHT_NH = TKHT_NH;
        this.TEN_TK_NO = TEN_TK_NO;
        this.TEN_TK_CO = TEN_TK_CO;
        this.TINH_TPHO_NNTIEN = TINH_TPHO_NNTIEN;
        this.So_RM = So_RM;
        this.MATELLER_GDV = MATELLER_GDV;
        this.MATELLER_KSV = MATELLER_KSV;
        this.RM_REF_NO = RM_REF_NO;
        this.REF_NO = REF_NO;
        this.TK_KB_NH = TK_KB_NH;
        this.Mode = Mode;
        this.PT_TT = PT_TT;
        this.TEN_KH_NH = TEN_KH_NH;
        this.TK_GL_NH = TK_GL_NH;
        this.SHKB = SHKB;
        this.Ngay_KB = Ngay_KB;
        this.Ma_NV = Ma_NV;
        this.So_BT = So_BT;
        this.Ma_DThu = Ma_DThu;
        this.So_BThu = So_BThu;
        this.KyHieu_CT = KyHieu_CT;
        this.So_CT = So_CT;
        this.So_CT_TuChinh = So_CT_TuChinh;
        this.So_CT_NH = So_CT_NH;
        this.Ma_NNTien = Ma_NNTien;
        this.Ten_NNTien = Ten_NNTien;
        this.DC_NNTien = DC_NNTien;
        this.Ma_NNThue = Ma_NNThue;
        this.Ten_NNThue = Ten_NNThue;
        this.DC_NNThue = DC_NNThue;
        this.Ly_Do = Ly_Do;
        this.Ma_KS = Ma_KS;
        this.Ma_TQ = Ma_TQ;
        this.So_QD = So_QD;
        this.Ngay_QD = Ngay_QD;
        this.CQ_QD = CQ_QD;
        this.Ngay_CT = Ngay_CT;
        this.Ngay_HT = Ngay_HT;
        this.Ma_CQThu = Ma_CQThu;
        this.XA_ID = XA_ID;
        this.Ma_Tinh = Ma_Tinh;
        this.Ten_tinh = Ten_tinh;
        this.Ma_Huyen = Ma_Huyen;
        this.Ma_Xa = Ma_Xa;
        this.Ten_Xa = Ten_Xa;
        this.TK_No = TK_No;
        this.TK_Co = TK_Co;
        this.So_TK = So_TK;
        this.Ngay_TK = Ngay_TK;
        this.LH_XNK = LH_XNK;
        this.DVSDNS = DVSDNS;
        this.Ten_DVSDNS = Ten_DVSDNS;
        this.Ma_LThue = Ma_LThue;
        this.TG_ID = TG_ID;
        this.Ma_NT = Ma_NT;
        this.Ty_Gia = Ty_Gia;
        this.So_Khung = So_Khung;
        this.So_May = So_May;
        this.TK_KH_NH = TK_KH_NH;
        this.NGAY_KH_NH = NGAY_KH_NH;
        this.MA_NH_A = MA_NH_A;
        this.MA_NH_B = MA_NH_B;
        this.TTien = TTien;
        this.TT_TThu = TT_TThu;
        this.Lan_In = Lan_In;
        this.Trang_Thai = Trang_Thai;
        this.TK_KH_Nhan = TK_KH_Nhan;
        this.Ten_KH_Nhan = Ten_KH_Nhan;
        this.Diachi_KH_Nhan = Diachi_KH_Nhan;
        this.TTien_NT = TTien_NT;
        this.So_BT_KTKB = So_BT_KTKB;
        this.So_BK = So_BK;
        this.Ngay_BK = Ngay_BK;
        this.TT_BDS = TT_BDS;
        this.TT_NSTT = TT_NSTT;
        this.PHI_GD = PHI_GD;
        this.PHI_VAT = PHI_VAT;
        this.PHI_GD2 = PHI_GD2;
        this.PHI_VAT2 = PHI_VAT2;
        this.PT_TINHPHI = PT_TINHPHI;
        this.MA_NH_TT = MA_NH_TT;
        this.TEN_NH_TT = TEN_NH_TT;
        this.CIF = CIF;
        this.SAN_PHAM = SAN_PHAM;
        this.TT_CITAD = TT_CITAD;
        this.SEQ_NO = SEQ_NO;
        this.LOAI_BL = LOAI_BL;
        this.MA_CAP_CHUONG = MA_CAP_CHUONG;
        this.TIME_BEGIN = TIME_BEGIN;
        this.SO_CMND = SO_CMND;
        this.SO_FONE = SO_FONE;
        this.MA_DV_DD = MA_DV_DD;
        this.TEN_DV_DD = TEN_DV_DD;
        this.MA_NTK = MA_NTK;
        this.Ma_CN = Ma_CN;
        this.VTD2 = VTD2;
        this.Ngay_VTD2 = Ngay_VTD2;
        this.VTD3 = VTD3;
        this.Ngay_VTD3 = Ngay_VTD3;
        this.VTD4 = VTD4;
        this.Ngay_VTD4 = Ngay_VTD4;
        this.VTD5 = VTD5;
        this.Ngay_VTD5 = Ngay_VTD5;
        this.MA_HQ_KB = MA_HQ_KB;
        this.LOAI_CTU = LOAI_CTU;
        this.Ma_Huyen_NNThue = Ma_Huyen_NNThue;
        this.Ma_Tinh_NNThue = Ma_Tinh_NNThue;
        this.MA_QUAN_HUYENNNTIEN = MA_QUAN_HUYENNNTIEN;
        this.MA_TINH_TPNNTIEN = MA_TINH_TPNNTIEN;
        this.NGAY_BAOCO = NGAY_BAOCO;
        this.NGAY_BAONO = NGAY_BAONO;
        this.TT_CTHUE = TT_CTHUE;
    }
    
    function infDTL(TT_BTOAN , Ma_nkt , Ma_nkt_cha , Ma_ndkt , Ma_ndkt_cha , Ma_tlpc , Ma_khtk , Ma_nt , Loai_thue_hq , PT_TT , ID , SHKB , Ngay_KB, Ma_NV , So_BT , Ma_DThu , CCH_ID , Ma_Cap , Ma_Chuong , LKH_ID , Ma_Loai , Ma_Khoan , MTM_ID , Ma_Muc , Ma_TMuc , Noi_Dung , DT_ID , Ma_TLDT, Ky_Thue , SoTien , SoTien_NT , MaQuy , Ma_DP , SO_CT , SO_TK , NGAY_TK , LH_XNK , MA_HQ , LOAI_TT , SAC_THUE, MA_LT) {
        this.TT_BTOAN = TT_BTOAN;
        this.Ma_nkt = Ma_nkt;
        this.Ma_nkt_cha = Ma_nkt_cha;
        this.Ma_ndkt = Ma_ndkt;
        this.Ma_ndkt_cha = Ma_ndkt_cha;
        this.Ma_tlpc = Ma_tlpc;
        this.Ma_khtk = Ma_khtk;
        this.Ma_nt = Ma_nt;
        this.Loai_thue_hq = Loai_thue_hq;
        this.PT_TT = PT_TT;
        this.ID = ID;
        this.SHKB = SHKB;
        this.Ngay_KB = Ngay_KB;
        this.Ma_NV = Ma_NV;
        this.So_BT = So_BT;
        this.Ma_DThu = Ma_DThu;
        this.CCH_ID = CCH_ID;
        this.Ma_Cap = Ma_Cap;
        this.Ma_Chuong = Ma_Chuong;
        this.LKH_ID = LKH_ID;
        this.Ma_Loai = Ma_Loai;
        this.Ma_Khoan = Ma_Khoan;
        this.MTM_ID = MTM_ID;
        this.Ma_Muc = Ma_Muc;
        this.Ma_TMuc = Ma_TMuc;
        this.Noi_Dung = Noi_Dung;
        this.DT_ID = DT_ID;
        this.Ma_TLDT = Ma_TLDT;
        this.Ky_Thue = Ky_Thue;
        this.SoTien = SoTien;
        this.SoTien_NT = SoTien_NT;
        this.MaQuy = MaQuy;
        this.Ma_DP = Ma_DP;
        this.SO_CT = SO_CT;
        this.SO_TK = SO_TK;
        this.NGAY_TK = NGAY_TK;
        this.LH_XNK = LH_XNK;
        this.MA_HQ = MA_HQ;
        this.LOAI_TT = LOAI_TT;
        this.SAC_THUE = SAC_THUE;
        this.MA_LT = MA_LT;
    }
    
    function infChungTu(HDR, ListDTL) {
        this.HDR = HDR;
        this.ListDTL = ListDTL;
    }
    
    function infHDRCompare(SoTK, Ma_HQ_PH, Ma_HQ_CQT, Ma_NTK, TKKB, MaSoThue) {
        this.SoTK = SoTK;
        this.Ma_HQ_PH = Ma_HQ_PH;
        this.Ma_HQ_CQT = Ma_HQ_CQT;
        this.Ma_NTK = Ma_NTK;
        this.TKKB = TKKB;
        this.MaSoThue = MaSoThue;
    }
    
    /********************END CLASS DEFINITION********************/
    
    $(document).ready(function () {
        /*
        $('#txtMaNNT').change(function(){
            jsCalCharacter();
        });
        
        $('#txtHuyen_NNT').change(function(){
            jsGetTenQuanHuyen_NNThue($(this).val());
        });
        
        $('#txtTinh_NNT').change(function(){
            jsGetTenTinh_NNThue($(this).val());
        });
        
        $('#txtMaHQ').change(function(){
            jsGetTenHQ();
            jsLoadByMaHQ();
        });
        
        $('#txtMaCQThu').change(function(){
            jsLoadByCQT();
        });
        
        $('#txtMaHQPH').change(function(){
            jsGet_DataByMaHQPH();
        });
        
        $('#txtSHKB').change(function(){
            jsGet_TenKB();
            jsGetTTNganHangNhan();
        });
        
        $('#txtTenMA_NH_B').change(function(){
            jsCalCharacter();
        });
        
        $('#txtTenKB').change(function(){
            jsCalCharacter();
        });
        
        $('#txtQuan_HuyenNNTien').change(function(){
            jsGetTenQuanHuyen_NNTien($(this).val());
        });
        
        $('#txtTinh_NNTien').change(function(){
            jsGetTenTinh_NNTien($(this).val());
        });
        
        $('#txtTongTien').change(function(){
            jsFormatNumber('txtTongTien');
            jsConvertCurrToText();
        });
        */
        
        //$('#ddlMaNT').change();
        
        try{
            $('#grdDSCT').paging({limit:15});
        }catch(err){
            paginationList(15);
        }   
        
        $('#ddlMa_NTK').change(function(){
            $('#ddlMaTKCo').val('');
        });
        
        jsSetLoaiTruyVanHQ();
    });
    
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
	        for (var k = 0; k <= str.length; k++){
	            if (k == locs[i]){
	            if (str.substring(k, k+1) != delim){
	                    str = str.substring(0,k) + delim + str.substring(k,str.length)
	                }
	            }
	        }
        }
        textbox.value = str
    }
    
    function jsCheckYear(elementInput) {
        var elmInput = elementInput;
        var yearNum = elmInput.value;
        if (yearNum !== '') {
            if (!validYear(yearNum)) {
                alert('Năm truyền vào không hợp lệ');
                elmInput.value = "";
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    
    function validYear(yearNum){
        var txt = /^[0-9]+$/;
        if (yearNum) {
            if (yearNum.length != 4) {
                return false;
            } else {
                if (txt.test(yearNum)) {
                    if ((parseInt(yearNum) < 1000) || (parseInt(yearNum) > 3000)) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }
    
    function jsSetLoaiTruyVanHQ(){
        //var valLoaiTVan = $('#ddlFilter_LoaiTien').val();
        $('#hdfLoaiCTu').val('P');
    }
    
    
    function jsCheckUserSession(param){
        var bRet = true;
        if (param) {
            var arrKQ = param.split(';');
                if(arrKQ[0] == "errUserSession"){
                    window.open("../../pages/Warning.html","_self");
                    bRet = false;
                }
        }
        return bRet;
    }
    
    function jsLoadCTList()
    {       
        var sSoCtu = $('#txtTraCuuSoCtu').val();
        if (!(typeof sSoCtu !== 'undefined' && sSoCtu !== null && sSoCtu.length > 0)) {
            sSoCtu = '';
        }
            
        PageMethods.LoadCTList(curMa_NV, sSoCtu, LoadCTList_Complete,LoadCTList_Error);                
    }
    
    function LoadCTList_Complete(result,methodName)
    {                             
        if (result.length > 0) {
            jsCheckUserSession(result);
            document.getElementById('divDSCT').innerHTML = result;     
        }
        try {
            $('#grdDSCT').paging({limit:15});
        } catch(err) {
            paginationList(15);
        }
    }
    
    function LoadCTList_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            document.getElementById('divStatus').innerHTML="Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
        }
    }
    
    function paginationList(listPaginationCount)
    {
        $('table#grdDSCT').each(function() {
            var currentPage = 0;
            var numPerPage = listPaginationCount;    
            var $pager=$('.pager').remove();
            var $paging = $("#paging").remove();
            var $table = $(this);
            $table.bind('repaginate', function() {
                $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            });
            $table.trigger('repaginate');
            var numRows = $table.find('tbody tr').length;
            var numPages = Math.ceil(numRows / numPerPage);
            var $pager = $('<div id="pager"></div>');
          
            for (var page = 0; page < numPages; page++) {
                $('<span class="page-number"></span>').text(page + 1).bind('click', {
                    newPage: page
                }, function(event) {
                    currentPage = event.data['newPage'];
                    $table.trigger('repaginate');
                    $(this).addClass('active').siblings().removeClass('active');
                }).appendTo($pager).addClass('clickable');
                
            }
            
            $pager.insertAfter($table).find('span.page-number:first').addClass('active');
        });
    }
   
    function jsTruyVanLePhi_HQ() {
        
        if($('#txtFilter_MaSoThue').val().length == 0){
            alert("Vui lòng nhập mã số thuế để truy vấn thông tin nợ thuế Hải quan!");
            $('#txtFilter_MaSoThue').focus();
            return;
        }
    
        jsShowDivStatus(false, "");
        jsShowHideDivProgress(true);
        jsEnableDisableControlButton('4');
        $('#hdfSo_CTu').val('');
        $('#hdfLoaiCTu').val('');
        $('#hdfIsEdit').val(false);
        $('#hdfDescCTu').val('');
        
        //$('#hdfHQResult').val('');
        $('#hdfHDRSelected').val('');
        $('#hdfDTLList').val('');
        $('#hdfDTLSelected').val('');
        
        $('#hdfNgayKB').val('');
        
        //jsBindKQTruyVanGrid(null);
        jsClearHDR_Panel();
        jsBindDTL_Grid(null, null);
        
        jsSetLoaiTruyVanHQ(); 
        
        var strMaSoThue = $('#txtFilter_MaSoThue').val();
        var strSoTK = $('#txtFilter_SoToKhai').val();
        var strNgayDK = $('#txtFilter_NgayDKy').val();
        var sLoaiTruyVan = 'P';
        //var sTestData = '';
        
        //Kiem tra xem co ma so thue chua => neu chua co => set hdfHQResult = null
        if($('#hdfMST').val().length==0){
            $('#hdfMST').val($('#txtFilter_MaSoThue').val());
            $('#hdfHQResult').val('');
            jsBindKQTruyVanGrid(null);
        }else{   //Neu co kiem tra xem ma so thue lan truoc voi lan truy van hien tai co trung khop nhau khong?
            if($('#hdfMST').val()!=$('#txtFilter_MaSoThue').val()){
                if(!confirm("Mã số thuế truy vấn hiện tại khác mã số thuế truy vấn lần trước. Bạn có muốn truy vấn không?"))
                    return;
                else{   //reset hdfHQResult = null
                    $('#hdfMST').val($('#txtFilter_MaSoThue').val());
                    $('#hdfHQResult').val('');
                    jsBindKQTruyVanGrid(null);
                }
            }
        }
        
        $.ajax({
                type: "POST",
                url: "frmLapCTuPhi_HQ.aspx/TruyVanLePhi_HQ",
                cache: false,
                data: JSON.stringify({
                    "sLoaiTruyVan": sLoaiTruyVan,
                    "sMaSoThue": strMaSoThue,
                    "sSoTK": strSoTK || null,
                    "sNgayDK": strNgayDK || null
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: TruyVanLePhiHQ_OnSuccess,
                failure: function (failure) {
                    if(failure !== null) 
                    {
                        var err;
                        err = JSON.parse(failure.responseText);
                        if (err) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            jsShowDivStatus(true, err.Message);
                        }
                    }
                    jsEnableDisableAddRowButton("1");
                    jsShowHideDivProgress(false);
                },
                error: function (request, status, error) {
                    var err;
                    if (typeof request !== 'undefined' && request !== null && typeof request.responseText !== 'undefined' && request.responseText !== null) {
                        err = JSON.parse(request.responseText);
                    }
                    
                    if (err) {
                        if (err.Message.indexOf(';') > -1) {
                            jsCheckUserSession(err.Message);
                        }
                        jsShowDivStatus(true, err.Message);
                    }
                    jsEnableDisableAddRowButton("1");
                    jsShowHideDivProgress(false);
                }
            }); 
    }
    
    function TruyVanLePhiHQ_OnSuccess(response) {
        var jsonObj = null;
        jsShowHideKQTruyVanHQ(true);
        
        if(typeof response !== 'undefined' && response !== null) {
            if (response.d) {
                if (!jsCheckUserSession(response.d)) {
                    return;
                }
                jsonObj = JSON.parse(response.d);
                if (jsonObj) {
                    //set object vua truy van vao objChung
                    jsonObj.Data.Item = setHQresult(jsonObj.Data.Item);
                    
                    //Save object to hiddenfield
                    $('#hdfHQResult').val(JSON.stringify(jsonObj));
                    //Bind Json result to grid
                    jsBindKQTruyVanGrid(jsonObj.Data);
                }
            }
            
        }
        jsEnableDisableAddRowButton("1"); //Hien thi nut them moi chi tiet
        jsShowHideDivProgress(false);
    }
    
    function setHQresult(arrRequestDataItem){
        if($('#hdfHQResult').val().length > 0){
            var objHQ_Old = JSON.parse($('#hdfHQResult').val());
            var arrHQ_Old = [];
            arrHQ_Old = objHQ_Old.Data.Item;
            
            var flag = true;
            var objItemTemp = {};
            //Duyet tat ca cac Item cu => xem cai moi co trung ko => trung ko add them vao
            for(var i=0; i<arrHQ_Old.length;i++){
                for(var j=0;j<arrRequestDataItem.length;j++){
                    if(!(arrHQ_Old[i].CT_No[0].So_TK == arrRequestDataItem[j].CT_No[0].So_TK && arrHQ_Old[i].Ma_NTK == arrRequestDataItem[j].Ma_NTK && arrHQ_Old[i].Ma_HQ_PH == arrRequestDataItem[j].Ma_HQ_PH && 
                            arrHQ_Old[i].Ma_HQ_CQT == arrRequestDataItem[j].Ma_HQ_CQT && arrHQ_Old[i].TKKB == arrRequestDataItem[j].TKKB)){
                        flag= true;
                    }else{
                        flag=false;
                        break;
                    }
                }
                if(flag)
                    arrRequestDataItem.push(arrHQ_Old[i])
            }
        }
        
        return arrRequestDataItem;
    }
    
    
    function jsShowHideDivProgress(bShow) {
        if (bShow) {
            document.getElementById('divProgress').style.display='';
        } else {
            document.getElementById('divProgress').innerHTML = 'Đang xử lý. Xin chờ một lát...';
            document.getElementById('divProgress').style.display='none';
        }
    }
    
    function jsBindKQTruyVanGrid(objParam) {
        //Clear before do anything
        $('#divResultTruyVan').val("");
        //can swithch case Thue/Phi HQ
        var sLoaiTruyVan = $('#hdfLoaiCTu').val();
        
        var top = [];
        top.push("<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdKQTruyVan' style='width: 100%; border-collapse: collapse;'>");
        top.push("<tr class='grid_header'>");
        top.push("<td align='center' style='width: 5%;'>Chọn</td>");
        top.push("<td align='center' style='width: 5%;'>STT</td>");
        top.push("<td align='center' style='width: 15%;'>Số tờ khai</td>");
        top.push("<td align='center' style='width: 15%;'>Mã HQ PH</td>");
        top.push("<td align='center' style='width: 15%;'>Mã HQ CQT</td>");
        top.push("<td align='center' style='width: 15%;'>Mã nhóm tài khoản</td>");
        top.push("<td align='center' style='width: 15%;'>Tài khoản kho bạc</td>");
        top.push("<td align='center' style='width: 15%;'>Tổng số tiền</td>");
        top.push("<td align='center' style='display: none;'>Mã số thuế</td>"); //invisible
        top.push("<td align='center' style='display: none;'>Mã loại thuế</td>"); //invisible
        top.push("</tr>");
        
        var arrObject = [];
        var content = [];
        if (typeof sLoaiTruyVan !== 'undefined' && sLoaiTruyVan !== null) {
            if (typeof objParam !== 'undefined' && objParam !== null) {
                sLoaiTruyVan = '"' + sLoaiTruyVan + '"';
                arrObject = objParam.Item;
                var iCount = 0;
                if (typeof arrObject !== 'undefined' && arrObject !== null && arrObject instanceof Array) {
                    var arrCTuNo = [];
                    for (var i = 0; i < arrObject.length; i++) {
                        arrCTuNo = arrObject[i].CT_No;
                        if (typeof arrCTuNo !== 'undefined' && arrCTuNo !== null && arrCTuNo instanceof Array) {
                            for (var j = 0; j < arrCTuNo.length; j++) {
                                if (arrCTuNo[j]) { //NOT: null, undefined, 0, NaN, false, or ""
                                    iCount++;
                                    content.push("<tr>");
                                    content.push("<td style='text-align: center;'><input type='checkbox' id='HDR_chkSelect_" + iCount + "' onclick='jsCheckTKhai(" + iCount + ", " + sLoaiTruyVan + ");' /></td>");
                                    content.push("<td><input type='text' id='HDR_STT_" + iCount + "' style='width: 98%;' ReadOnly='readonly' class='inputflat' value='" + iCount + "' /></td>");
                                    content.push("<td><input type='text' id='HDR_SO_TK_" + iCount + "' style='width: 98%;' ReadOnly='readonly' class='inputflat' value='" + arrCTuNo[j].So_TK + "' /></td>");
                                    content.push("<td><input type='text' id='HDR_Ma_HQ_PH_" + iCount + "' style='width: 98%;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].Ma_HQ_PH + "' /></td>");
                                    content.push("<td><input type='text' id='HDR_Ma_HQ_CQT_" + iCount + "' style='width: 98%;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].Ma_HQ_CQT + "' /></td>");
                                    content.push("<td><input type='text' id='HDR_Ma_NTK_" + iCount + "' style='width: 98%;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].Ma_NTK + "' /></td>");
                                    content.push("<td><input type='text' id='HDR_TKKB_" + iCount + "' style='width: 98%;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].TKKB + "' /></td>");
                                    content.push("<td><input type='text' id='HDR_DuNo_TO_" + iCount + "' style='width: 98%; text-align: right;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].DuNo_TO + "' /></td>");
                                    content.push("<td style='display: none;'><input type='text' id='HDR_MST_" + iCount + "' style='width: 98%;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].Ma_DV + "' /></td>");
                                    content.push("<td style='display: none;'><input type='text' id='HDR_Ma_LT_" + iCount + "' style='width: 98%; border-color: White;' ReadOnly='readonly' class='inputflat' value='11' /></td>");
                                    content.push("</tr>");
                                }
                            }
                        }
                    }
                }
            }
        }
        
        var bottom = "</table>";
        
        $('#divResultTruyVan').html(top.join("") + content.join("") + bottom);
        jsFormatNumberKQTruyVanGrid();
    }
    
    function jsCheckTKhai(param, sLoaiTruyVan) {
        //lấy thông tin mã và tên căn cứ theo msg HQ trả về chứ ko raise change event để lấy theo etax
	    //Get all checked header rows from hidden field:
	    var arrHDRSelected = [];
	    if ($('#hdfHDRSelected').val().length > 0) {
		    arrHDRSelected = JSON.parse($('#hdfHDRSelected').val());
	    }
	    var arrDTLList = [];
	    if ($('#hdfDTLList').val().length > 0) {
	        arrDTLList = JSON.parse($('#hdfDTLList').val())
	    }
	    var arrDTLSelected = [];
	    if ($('#hdfDTLSelected').val().length > 0) {
	        arrDTLSelected = JSON.parse($('#hdfDTLSelected').val())
	    }
		var arrDTLtmp = [];
		
	    //Procedure:
	    var grdKQTruyVan = $('#grdKQTruyVan');
	    if ($('#HDR_chkSelect_' + param).is(":checked")){ //checked - if compare result is true => add to hdfHDRList
		    var sSoTK, sMa_HQ_PH, sMa_HQ_CQT, sMa_NTK, sTKKB, sMaSoThue;
		    
		    sSoTK = grdKQTruyVan.find('#HDR_SO_TK_' + param).val().trim();
		    sMa_HQ_PH = grdKQTruyVan.find('#HDR_Ma_HQ_PH_' + param).val().trim();
		    sMa_HQ_CQT = grdKQTruyVan.find('#HDR_Ma_HQ_CQT_' + param).val().trim();
		    sMa_NTK = grdKQTruyVan.find('#HDR_Ma_NTK_' + param).val().trim();
		    sTKKB = $('#HDR_TKKB_' + param).val().trim();
		    sMaSoThue = $('#HDR_MST_' + param).val().trim();
		    var objTmp = new infHDRCompare(sSoTK, sMa_HQ_PH, sMa_HQ_CQT, sMa_NTK, sTKKB, sMaSoThue);
		    
		    var objRetHQ = JSON.parse($('#hdfHQResult').val());
		    var arrRetHQ = [];
		    if (typeof objRetHQ !== 'undefined' && objRetHQ !== null && objRetHQ.Data !== null) {
				arrRetHQ = objRetHQ.Data.Item;
            }
            
		    if (arrHDRSelected.length == 0){ //the first time or there are no selected row before
			    arrHDRSelected.push(objTmp);
			    $('#hdfHDRSelected').val(JSON.stringify(arrHDRSelected));
			    if ((typeof arrRetHQ !== 'undefined') && (arrRetHQ !== null) && (arrRetHQ instanceof Array) && (arrRetHQ.length > 0)) {
			        arrDTLtmp = arrRetHQ;
			        for (var i = 0; i < arrDTLtmp.length; i ++) {
			            var arrCT_No = arrDTLtmp[i].CT_No;
			            var arrDTLtemp = [];
	                    if (arrCT_No && arrCT_No.length > 0) {
	                        for (var j = 0; j < arrCT_No.length; j++) {
	                            if (arrCT_No[j]) {
	                                if (arrCT_No[j].So_TK == sSoTK) {
	                                    //chi lay cac CTu_No object thoa man dieu kien
	                                    arrDTLtemp.push(arrCT_No[j]);
	                                }
	                            }
	                        }
	                    }
	                    arrDTLtmp[i].CT_No = arrDTLtemp; //replace DTL array
			        }
			        
			        //remove item co CT_No rỗng
                    var arrDTL_Temp = [];
                    for (var i = 0; i < arrDTLtmp.length; i ++) {
                        var arrCTuNo = arrDTLtmp[i].CT_No;
	                    if (arrCTuNo && arrCTuNo.length > 0) {
	                        arrDTL_Temp.push(arrDTLtmp[i]);
	                    }
                    }
                    arrDTLtmp = arrDTL_Temp;
				    
				    if (typeof arrDTLtmp !== 'undefined' && arrDTLtmp !== null) {
				        //Bind HDR panel
				        if (arrDTLtmp && arrDTLtmp.length > 0) {
				            jsBindKQTruyVanHDR_Panel(sLoaiTruyVan, arrDTLtmp[0]);
				        } else {
				            jsBindKQTruyVanHDR_Panel(null, null);
				        }
			            
			            //And also bind detail grid view
			            if (typeof arrDTLtmp !== 'undefined' && arrDTLtmp !== null && arrDTLtmp.length > 0) {
			                arrDTLList = arrDTLtmp; //replace all values if it's contain before
			                arrDTLSelected.length = 0; //clear all selected DTL rows before if it have
			                $('#hdfDTLList').val(JSON.stringify(arrDTLList));
                            jsAddRowDTL_Grid(arrDTLList); //add them row vao grid
                        }
				    }
				    
			    }
		    }
		    else{ //next time
		        var diffProp = {msgRet: ""};
			    if (jsCheckTKhaiSameHeader(objTmp, "SoTK", ",", arrHDRSelected, diffProp)){
				    //check: if not exists this item in array => add it to array
			       if (!jsCheckObjExistsInArray(objTmp, null, null,  arrHDRSelected)){
			            
					    arrHDRSelected.push(objTmp);
					    $('#hdfHDRSelected').val(JSON.stringify(arrHDRSelected));
					    //And also bind detail grid view
					    if ((typeof arrRetHQ !== 'undefined') && (arrRetHQ !== null) && (arrRetHQ instanceof Array) && (arrRetHQ.length > 0)) {
					        if (typeof objTmp !== 'undefined' && objTmp !== null) {
				                //get arrDTLtmp
				                arrDTLtmp = arrRetHQ;
			                    for (var i = 0; i < arrRetHQ.length; i ++) {
			                        var arrCT_No = arrRetHQ[i].CT_No;
			                        var arrCTuNoTmp = [];
	                                if (arrCT_No && arrCT_No.length > 0) {
	                                    for (var j = 0; j < arrCT_No.length; j++) {
	                                        if (arrCT_No[j]) {
	                                            if (arrCT_No[j].So_TK == objTmp.SoTK) {
	                                                //chi lay cac CTu_No object thoa man dieu kien
	                                                arrCTuNoTmp.push(arrCT_No[j]);
	                                            }
	                                        }
	                                    }
	                                }
	                                arrDTLtmp[i].CT_No = arrCTuNoTmp; //replace DTL array
			                    }
					            
					            if (typeof arrDTLtmp !== 'undefined' && arrDTLtmp !== null && arrDTLtmp.length > 0) {
					                //remove item co CT_No rỗng cho đỡ chật đất
					                var arrDTL_Temp = [];
					                for (var i = 0; i < arrDTLtmp.length; i ++) {
					                    var arrCTuNo = arrDTLtmp[i].CT_No;
    					                if (arrCTuNo && arrCTuNo.length > 0) {
    					                    arrDTL_Temp.push(arrDTLtmp[i]);
    					                }
					                }
					                //arrDTLtmp = arrDTL_Temp;
				                    arrDTLList = arrDTLList.concat(arrDTL_Temp); //add new DTL row to DTL list
				                    //do nothing with arrDTLSelected
				                    $('#hdfDTLList').val(JSON.stringify(arrDTLList));
					                
				                    jsAddRowDTL_Grid(arrDTLtmp); //add them row vao grid
				                }
					        }
					    }
					}
			    } else{
			        if (diffProp && diffProp.msgRet) {
			           alert("Tờ khai này không cùng thông tin " + diffProp.msgRet + " với các tờ khai đã chọn: "); 
			        } else {
			            alert("Tờ khai này không cùng thông tin chung với các tờ khai đã chọn");
			        }
			        $('#HDR_chkSelect_' + param).prop('checked', false);
                }
            }
            
	    } else {
	        var sSoToKhai = '';
	        sSoToKhai = grdKQTruyVan.find('#HDR_SO_TK_' + param).val();
            if (sSoToKhai) {
                //first: remove row from detail
                //remove items from DTL Selected rows
                
                for (var j = 0; j < arrDTLSelected.length; j++) {
                    if (arrDTLSelected[j].So_TK == sSoToKhai) {
                        arrDTLSelected.splice(j, 1);
                    }
                }
                
                var arrSoTK = [];
                
                var arrDTLTemp = [];
                arrDTLTemp = JSON.parse(JSON.stringify(arrDTLList)); //not reference - OK
                
                for (var i = 0; i < arrDTLList.length; i++) {
                    var arrCTuNo = arrDTLList[i].CT_No;
                    if (arrCTuNo) {
                        if (arrCTuNo.length > 0) {
                            for (var j = 0; j < arrCTuNo.length; j++) {
                                if (arrCTuNo[j].So_TK == sSoToKhai) {
                                    arrDTLList[i].CT_No.splice(j, 1);
                                } else {
                                    arrDTLTemp[i].CT_No.splice(j, 1);
                                }
                            }
                        }
                    }
                }
                arrDTLtmp = arrDTLTemp;

                jsRemoveRowDTL_Grid(arrDTLtmp,sLoaiTruyVan);
                //and then: remove item from header
                var arrHDRSelectTmp = [];
                for (var j = 0; j < arrHDRSelected.length; j++) {                   
                    if (arrHDRSelected[j].SoTK !== sSoToKhai) {
                        arrHDRSelectTmp.push(arrHDRSelected[j]);
                    }
                }
                arrHDRSelected = arrHDRSelectTmp;
            }
		    $('#hdfHDRSelected').val(JSON.stringify(arrHDRSelected));
		    $('#hdfDTLList').val(JSON.stringify(arrDTLList));
		    
		    //if HDR selected is empty => clear form
		    if (arrHDRSelected.length <= 0) {
		        jsClearHDR_Panel();
		    }
		    
	    }
	    jsCalcTotalMoney();
	    jsCalCharacter();
	    
	    getNoiDungByMaTieuMuc();
	    changeNT();
    }
    
    function jsCheckTKhaiSameHeader(object, propExcept, splitChar, array, retVar){
        var sRet = false;
        sRet = jsCheckObjExistsInArrayWithRetVar(object, propExcept, splitChar, array, retVar);
        
        return sRet;
    }
    
    function jsCheckStringInArray(str, arr) {
        if (str) {
            if (arr && arr.length > 0) {
                for (var item in arr) {
                    if (str.trim() == arr[item].toString().trim()) {
                        return true; //break for
                    }
                }
            }
        }
        
        return false;
    }
    
    function jsCheckObjExistsInArray(attr, value, arrTmp){
        $.each(arrTmp, function(index, item){
            if(item[attr].toString() == value.toString()){
                   return true;     // break the $.each() loop
                }
        });
        return false;
    }
    
    function jsCheckObjExistsInArray(obj, propExcept, splitChar, array) {
        var sRet = true;
        var arrExcept = [];
        if (propExcept) {
            if (splitChar) {
                arrExcept = propExcept.split(splitChar);
            } else {
                arrExcept.push(propExcept);
            }
        }
        
        loopArray:
        for (var key in array){
            var arr = array[key];
            for (var prop in obj) {
                if (arrExcept && arrExcept.length > 0) {
                    if (!jsCheckStringInArray(prop, arrExcept)) {
                        if (obj.hasOwnProperty(prop)) {
                            if(obj[prop].toString() !== arr[prop].toString()){
                                sRet = false;
                                break loopArray;
                            }
                        }
                    }
                } else {
                    if (obj.hasOwnProperty(prop)) {
                        if(obj[prop].toString() !== arr[prop].toString()){
                                sRet = false;
                                break loopArray;
                        }
                    }
                }
            }
        }
        
        return sRet;
    }
    
    function jsCheckObjExistsInArrayWithRetVar(obj, propExcept, splitChar, array, retVar) {
        var sRet = true;
        var arrExcept = [];
        if (propExcept) {
            if (splitChar) {
                arrExcept = propExcept.split(splitChar);
            } else {
                arrExcept.push(propExcept);
            }
        }
        
        loopArray:
        for (var key in array){
            var arr = array[key];
            for (var prop in obj) {
                if (arrExcept && arrExcept.length > 0) {
                    if (!jsCheckStringInArray(prop, arrExcept)) {
                        if (obj.hasOwnProperty(prop)) {
                            if(obj[prop].toString() !== arr[prop].toString()){
                                sRet = false;
                                if (retVar) {
                                    retVar.msgRet = prop.toString();
                                }
                                break loopArray;
                            }
                        }
                    }
                } else {
                    if (obj.hasOwnProperty(prop)) {
                        if(obj[prop].toString() !== arr[prop].toString()){
                                sRet = false;
                                if (retVar) {
                                    retVar.msgRet = prop.toString();
                                }
                                break loopArray;
                        }
                    }
                }
            }
        }
        
        return sRet;
    }
    
    function jsClearHDR_Panel() {       
        var today = new Date();
    
        $('#lblSoCTu').text('');
        $('#txtMaNNT').val('');
        $('#txtTenNNT').val('');
        $('#txtDChiNNT').val('');
        $('#txtHuyen_NNT').val('');
        $('#txtTenHuyen_NNT').val('');
        $('#txtMaHQ').val('');
        $('#txtTenMaHQ').val('');
        $('#txtMaCQThu').val('');
        $('#txtTenCQThu').val('');
        $('#txtMaHQPH').val('');
        $('#txtTenMaHQPH').val('');
        $('#txtSHKB').val('');
        $('#txtTenKB').val('');
        $('#txtMaDBHC').val('');
        $('#txtTenDBHC').val('');
        $('#ddlMa_NTK').val('');
        $('#txtMaNNTien').val('');
        $('#txtTenNNTien').val('');
        $('#txtDChiNNTien').val('');
        $('#txtQuan_HuyenNNTien').val('');
        $('#txtTenQuan_HuyenNNTien').val('');
        $('#txtTinh_NNTien').val('');
        $('#txtTenTinh_NNTien').val('');
        if (typeof dtmNgayLV !== 'undefined' && dtmNgayLV !== null) {
            $('#txtNGAY_HT').val(dtmNgayLV);
        } else {
            $('#txtNGAY_HT').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
        }
        $('#ddlMaNT').val($('#ddlMaNT option:first').val());
        $('#txtTyGia').val('');
        $('#txtTK_KH_NH').val('');
        $('#txtTenTK_KH_NH').val('');
        //$('#txtNGAY_KH_NH').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
        $('#txtDesNGAY_KH_NH').val('');
        $('#ddlMA_NH_A').val('');
        $('#txtMA_NH_TT').val('');
        $('#txtTEN_NH_TT').val('');
        $('#txtMA_NH_B').val('');
        $('#txtTenMA_NH_B').val('');
        $('#txtTongTien').val('');
        $('#txtLyDoHuy').val('');
        
        $('#txtCmtnd').val('');        
        $('#txtDienThoai').val('');
        $('#txtTongTienNT').val('');
        
        $('#txtSoDu_KH_NH').val('');
        $('#txtDesc_SoDu_KH_NH').val('');
        $('#ddlPT_TT').val('01');
        
        $('#txtTKGL').val('');
        $('#txtTenTKGL').val('');
        $('#ddlMaTKCo').val('');
        
        onChangePTTT();
    }
    
    function jsBindKQTruyVanHDR_Panel(sLoaiTruyVan, obj) {
        //Caution: get thong tin tu msg HQ tra ve, ko lay thong tin tu etax
        if (obj) {
            var today = new Date();
            
            $('#txtMaNNT').val(obj.Ma_DV);
            $('#txtTenNNT').val(obj.Ten_DV);
            $('#txtDChiNNT').val('');
            $('#txtHuyen_NNT').val('');
            $('#txtMaCQThu').val(obj.Ma_HQ_CQT);
            //$('#txtTenCQThu').val(obj.Ten_cqthu);
            $('#txtMaHQPH').val(obj.Ma_HQ_PH);
            $('#txtTenMaHQPH').val(obj.Ten_HQ_PH);
            $('#txtSHKB').val(obj.Ma_KB);
            $('#txtTenKB').val(obj.Ten_KB);
            $('#txtMaDBHC').val('');
            $('#txtTenDBHC').val('');
            $('#ddlMa_NTK').val(obj.Ma_NTK);
            //$('#ddlMaTKNo').val('');
            $('#ddlMaTKCo').val(obj.TKKB);
            $('#txtMaNNTien').val('');
            $('#txtTenNNTien').val('');
            $('#txtDChiNNTien').val('');
            $('#txtQuan_HuyenNNTien').val('');
            $('#txtTinh_NNTien').val('');
            if (typeof dtmNgayLV !== 'undefined' && dtmNgayLV !== null) {
                $('#txtNGAY_HT').val(dtmNgayLV);
            } else {
                $('#txtNGAY_HT').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
            }
            $('#ddlMaNT').val($('#ddlMaNT option:first').val());
            $('#txtTK_KH_NH').val('');
            //$('#txtNGAY_KH_NH').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
            $('#ddlMA_NH_A').val($('#ddlMA_NH_A option:first').val());
            //$('#txtMA_NH_TT').val('');
            //$('#txtMA_NH_B').val('');
            $('#txtTongTien').val(obj.DuNo_TO);
            $('#txtLyDoHuy').val('');                
            
            $('#txtCmtnd').val('');
            $('#txtDienThoai').val('');
            
            var objCTuNo = obj.CT_No[0];
            if (objCTuNo) {
                //$('#txtMaHQ').val(objCTuNo.Ma_HQ);
                $('#txtMaHQ').val(objCTuNo.Ma_HQ);
                $('#txtTenMaHQ').val(objCTuNo.Ten_HQ);
            }
            
            $('#txtTongTienNT').val(obj.DuNo_TO);
            $('#txtTyGia').val(1);
            jsLoadByCQT_Default();
            jsGet_TenKB();
            jsGetTTNganHangNhan();            
        } else {
            jsClearHDR_Panel();
        }
        
        jsCalCharacter();
    }
    
    function jsBindHDR_Panel(obj) {
        //jsClearHDR_Panel();
    
        if (typeof obj !== 'undefined' && obj !== null) {
            $('#lblSoCTu').html('  - Số CT: ' + obj.So_CT + ' - Loại chứng từ: Lệ phí');
            
            $('#txtMaNNT').val(obj.Ma_NNThue);
            $('#txtTenNNT').val(obj.Ten_NNThue);
            $('#txtDChiNNT').val(obj.DC_NNThue);
            $('#txtHuyen_NNT').val(obj.Ma_Huyen);
            //$('#txtTenHuyen_NNT').val(obj.Huyen_nnthue);
            $('#txtTinh_NNT').val(obj.Ma_Tinh);
            //$('#txtTenTinh_NNT').val(obj.Tinh_nnthue);
            $('#txtMaHQ').val(obj.MA_HQ);
            $('#txtTenMaHQ').val(obj.TEN_HQ);
            $('#txtMaCQThu').val(obj.Ma_CQThu);
            $('#txtTenCQThu').val(obj.Ten_cqthu);
            $('#txtMaHQPH').val(obj.Ma_hq_ph);
            $('#txtTenMaHQPH').val(obj.TEN_HQ_PH);
            $('#txtSHKB').val(obj.SHKB);
            //$('#txtSHKB').val(obj.SHKB);
            $('#txtTenKB').val(obj.Ten_kb);
            $('#txtMaDBHC').val(obj.Ma_Xa);
            $('#txtTenDBHC').val(obj.Ten_Xa);
            $('#ddlMa_NTK').val(obj.MA_NTK);
            $('#ddlMaTKCo').val(obj.TK_Co);
            $('#txtMaNNTien').val(obj.Ma_NNTien);
            $('#txtTenNNTien').val(obj.Ten_NNTien);
            $('#txtDChiNNTien').val(obj.DC_NNTien);
            $('#txtQuan_HuyenNNTien').val(obj.MA_QUAN_HUYENNNTIEN);
            $('#txtTenQuan_HuyenNNTien').val(obj.QUAN_HUYEN_NNTIEN);
            $('#txtTinh_NNTien').val(obj.MA_TINH_TPNNTIEN);
            $('#txtTenTinh_NNTien').val(obj.TINH_TPHO_NNTIEN);
            $('#txtNGAY_HT').val(obj.Ngay_HT);
            $('#ddlMaNT').val(obj.Ma_NT);
            $('#txtTyGia').val(obj.Ty_Gia);
            $('#txtTK_KH_NH').val(obj.TK_KH_NH);
            $('#txtTenTK_KH_NH').val(obj.TEN_KH_NH);
            //$('#txtNGAY_KH_NH').val(obj.NGAY_KH_NH);
            $('#txtDesNGAY_KH_NH').val('');
            if (typeof obj.PT_TT !== 'undefined' && obj.PT_TT !== null && obj.PT_TT !== '') {
                $('#ddlPT_TT').val(obj.PT_TT);
            }
            $('#ddlMA_NH_A').val(obj.MA_NH_A);
            $('#txtMA_NH_TT').val(obj.MA_NH_TT);
            $('#txtTEN_NH_TT').val(obj.TEN_NH_TT);
            $('#txtMA_NH_B').val(obj.MA_NH_B);
            $('#txtTenMA_NH_B').val(obj.Ten_NH_B);
            $('#txtTongTien').val(accounting.formatNumber(obj.TTien));
            $('#txtLyDoHuy').val(obj.Ly_Do);
            
            $('#txtCmtnd').val('');
            $('#txtDienThoai').val('');
            $('#txtTK_KH_NH').val('');
            $('#txtTenTK_KH_NH').val('');
            
            if(obj.Ma_NT !='VND')
                $('#txtTongTienNT').val(accounting.formatNumber(obj.TTien_NT,2,",","."));
            else
                $('#txtTongTienNT').val(accounting.formatNumber(obj.TTien_NT));
            
           if(obj.PT_TT == "03"){
                $('#txtTKGL').val(obj.TK_KH_NH);
                jsGet_TenTKGL();
                
                $('#rowTKGL').css("display","");
                $('.trChuyenKhoan').css("display","none");
            }else if(obj.PT_TT == "01"){
                $('#txtTK_KH_NH').val(obj.TK_KH_NH);
                $('#txtTenTK_KH_NH').val(obj.TEN_KH_NH);
                
                $('#rowTKGL').css("display","none");
                $('.trChuyenKhoan').css("display","");
            }
            
            $('#hdfSoBT').val(obj.So_BT);
            
            jsGetTK_KH_NH();
        }
    }
    
    function jsBindDTL_Grid(objHDR, arrDTL) {
        jsClearDTL_Grid();
        //can swithch case Thue/Phi HQ
        var sLoaiTruyVan = $('#hdfLoaiCTu').val(); //objHDR.LOAI_CTU; //$('#hdfLoaiCTu').val();
        
        var top = [];
        top.push("<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdChiTiet' style='width: 100%; border-collapse: collapse;'>");
        top.push("<tr class='grid_header'>");
        top.push("<td align='center' style='width: 4%;'>Chọn</td>");
        top.push("<td align='center' style='width: 10%;'>Số tờ khai</td>");
        top.push("<td align='center' style='width: 10%;'>Ngày TK (dd/MM/yyyy)</td>");
        top.push("<td align='center' style='width: 5%;'>Mã LT</td>");
        top.push("<td align='center' style='width: 8%;'>Loại hình XNK</td>");
        top.push("<td align='center' style='width: 5%;'>Sắc thuế</td>");
        top.push("<td align='center' style='width: 5%;'>Mã chương</td>");
        top.push("<td align='center' style='width: 5%;'>Tiểu mục</td>");
        top.push("<td align='center' style='width: 24%;'>Nội dung</td>");
        top.push("<td align='center' style='width: 12%;'>Số tiền</td>");
        top.push("<td align='center' style='width: 12%;'>Ngoại tệ</td>");
        top.push("<td align='center' style='display: none;'>Thông tin bút toán</td>");
        top.push("</tr>");
        
        var content = [];
        if (typeof arrDTL !== 'undefined' && arrDTL !== null && arrDTL instanceof Array && arrDTL.length > 0) {
            for (var i = 0; i < arrDTL.length; i++) {
                if (arrDTL[i]) { //NOT: null, undefined, 0, NaN, false, or ""
                    content.push("<tr>");
                    content.push("<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + (i + 1) + "' checked='checked' onclick='jsCalcTotalMoney(); jsCalCharacter();jsConvertCurrToText();jsGenThuTuButToan();' /></td>");
                    content.push("<td><input type='text' id='DTL_SO_TK_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrDTL[i].SO_TK + "' onblur='jsGenThuTuButToan();' /></td>");
                    content.push("<td><input type='text' id='DTL_Ngay_TK_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}' value='" + arrDTL[i].NGAY_TK + "' /></td>");
                    content.push("<td><input type='text' id='DTL_MA_LT_" + (i + 1) + "' style='width: 98%;' class='inputflat' value='" + arrDTL[i].MA_LT + "' /></td>");
                    content.push("<td><input type='text' id='DTL_Lhxnk_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrDTL[i].LH_XNK + "' /></td>");
                    content.push("<td><input type='text' id='DTL_SAC_THUE_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrDTL[i].SAC_THUE + "' /></td>");
                    content.push("<td><input type='text' id='DTL_MA_CHUONG_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrDTL[i].Ma_Chuong + "' /></td>");
                    content.push("<td><input type='text' id='DTL_Ma_TMuc_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrDTL[i].Ma_TMuc + "' onblur='onBlurMaTieuMuc(" + (i+1) + ")' /></td>");
                    
                    content.push("<td><input type='text' id='DTL_Noi_Dung_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrDTL[i].Noi_Dung + "' /></td>");
                    
                    content.push("<td><input type='text' id='DTL_SoTien_" + (i + 1) + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onfocus='this.select()' onkeyup='ValidInteger(this); jsFormatNumber(this);' maxlength='17' onblur='jsCalcTotalMoney(); jsCalCharacter();jsCalcCurrency();' value='" + arrDTL[i].SoTien + "' /></td>");
                    content.push("<td><input type='text' id='DTL_SoTienNT_" + (i + 1) + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onfocus='this.select()' onkeyup='jsFormatNumber2(this);' maxlength='17' value='" + arrDTL[i].SoTien_NT + "' /></td>");
                    
                    content.push("<td style = 'display: none;'><input type='text' id='DTL_TT_BTOAN_" + (i + 1) + "' style='width: 98%; display: none;' class='inputflat' value='" + arrDTL[i].TT_BTOAN + "' /></td>");
                    content.push("</tr>");
                }
            }
        }
        
        var bottom = "</table>";
        
        $('#pnlCTietTTinTKhai').html(top.join("") + content.join("") + bottom);
        //alert(top.join("") + content.join("") + bottom);
        jsGenThuTuButToan();
        jsFormatNumberDTL_Grid();
    }
    
    function jsGenSoButToan_DTL(sLoaiTruyVan){
        //lấy tạm giá trị cột STT của dòng được check cuối cùng
        var iRet = 0;
        if (sLoaiTruyVan == "T") {
            $('#grdKQTruyVan tr').each(function(){
                var ctrCheck = $(this).find("input[id^='HDR_chkSelect_']");
                var ctrInput = null;
                if (ctrCheck) {
                    if (ctrCheck.is(":checked")){
                        ctrInput = $(this).find("input[id^='HDR_STT_']");
                        if (typeof ctrInput !== 'undefined' && ctrInput !== null && ctrInput.length > 0) {
                            iRet = ctrInput.prop("value");
                        }
                        return false; //break out each loop
                    }
                }
            });
        } else if (sLoaiTruyVan == "P") {
            
        }
        
        return iRet;
    }
    
    function jsFormatNumberKQTruyVanGrid(){
        $('#grdKQTruyVan tr').each(function(){
            var control = $(this).find("input[id^='HDR_DuNo_TO_']");
            if (typeof control !== 'undefined' && control !== null) {
                control.val(accounting.formatNumber(control.val()));
            }
        });
    }
    
    function jsFormatNumberDTL_Grid(){
        if($("#ddlMaNT option:selected").val() != 'VND')
        {
            $('#grdChiTiet tr').each(function(){
                var control = $(this).find("input[id^='DTL_SoTien_']");
                if (typeof control !== 'undefined' && control !== null) {
                    control.val(accounting.formatNumber(control.val()));
                }
                
                var control2 = $(this).find("input[id^='DTL_SoTienNT_']");
                if (typeof control2 !== 'undefined' && control2 !== null) {
                    control2.val(accounting.formatNumber(control2.val(),2,",","."));
                }
            });
        }
        else
        {
            $('#grdChiTiet tr').each(function(){
                var control = $(this).find("input[id^='DTL_SoTien_']");
                if (typeof control !== 'undefined' && control !== null) {
                    control.val(accounting.formatNumber(control.val()));
                }
                
                var control2 = $(this).find("input[id^='DTL_SoTienNT_']");
                if (typeof control2 !== 'undefined' && control2 !== null) {
                    control2.val(accounting.formatNumber(control2.val()));
                }
            });
        }
    }
    
    function jsClearDTL_Grid() {
        $('#pnlCTietTTinTKhai').val('');
    }
    
    function jsAddRowDTL_Grid(arrParam) { 
        //switch case Thue/Phi HQ
        var sLoaiTruyVan = $('#hdfLoaiCTu').val();
        
        //Get max number for ID:
        var sID = jsGenDTLrowID();
        var sTT_BToan = jsGenSoButToan_DTL(sLoaiTruyVan);
        var sAppendRows = '';
        var sNgayDK = '';
        //var today = new Date();
        
        //arrParam: array of Item inside HQ result (it's include DTL object array)
        if ((typeof arrParam !== 'undefined') 
            && (arrParam !== null) 
            && (arrParam instanceof Array)
            && (arrParam.length > 0)) { 
            for (var i = 0; i < arrParam.length; i++) {
                var arrCT_No = arrParam[i].CT_No;
                if (arrCT_No && arrCT_No.length > 0) {
                    for (var j = 0; j < arrCT_No.length; j++) {
                        if (!isNaN(sID)) {
                            sID++;
                            sNgayDK = '';
                            //sNgayDK = ('0' + today.getDate()).slice(-2) + "/" + ('0' + (today.getMonth() + 1)).slice(-2) + "/" + arrCT_No[j].Nam_DK;
                            sNgayDK = ('0' + arrCT_No[j].Ngay_DK.substr(8,2)).slice(-2) + '/' + ('0' + arrCT_No[j].Ngay_DK.substr(5,2)).slice(-2) + '/' + arrCT_No[j].Ngay_DK.substr(0,4);
                            sAppendRows = sAppendRows + '<tr>';
                            if(arrCT_No[j].DuNo > 0){
                                sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "' onclick='jsCalcTotalMoney(); jsCalCharacter();jsConvertCurrToText();jsGenThuTuButToan();' checked /></td>";
                            }else{
                                sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "' onclick='jsCalcTotalMoney(); jsCalCharacter();jsConvertCurrToText();jsGenThuTuButToan();' /></td>";
                            }
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SO_TK_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrCT_No[j].So_TK + "' onblur='jsGenThuTuButToan();' /></td>";
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ngay_TK_" + sID + "' style='width: 98%; border-color: White;' onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}' class='inputflat' maxlength='10' onblur='javascript:CheckDate(this)' value='" + sNgayDK + "' /></td>";
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_LT_" + sID + "' style='width: 98%;' class='inputflat' value='11' /></td>";
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Lhxnk_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrCT_No[j].Ma_LH + "' /></td>";
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SAC_THUE_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrCT_No[j].LoaiThue + "' /></td>";
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_CHUONG_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrParam[0].Ma_Chuong + "' /></td>";
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ma_TMuc_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrCT_No[j].TieuMuc + "' onblur='onBlurMaTieuMuc(" + sID + ")' /></td>";
                            
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Noi_Dung_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrCT_No[j].Noi_Dung + "' /></td>";
                            
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTien_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onfocus='this.select()' onkeyup='ValidInteger(this); jsFormatNumber(this);' maxlength='17' onblur='jsCalcTotalMoney(); jsCalCharacter();jsCalcCurrency(" + sID+ ");' value='" + arrCT_No[j].DuNo + "' /></td>";
                            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTienNT_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onfocus='this.select()' onkeyup='jsFormatNumber2(this);' maxlength='17' value='" + arrCT_No[j].DuNo + "' /></td>";
                            
                            sAppendRows = sAppendRows + "<td style = 'display: none;'><input type='text' id='DTL_TT_BTOAN_" + sID + "' style='width: 98%; display: none;' class='inputflat' value='" + sTT_BToan + "' /></td>";
                            sAppendRows = sAppendRows + '</tr>';
                        }
                    }
                }
            }
            $('#grdChiTiet tr:last').after(sAppendRows);
            
            jsGenThuTuButToan();
            jsFormatNumberDTL_Grid();
        } else { //add dummy rows to DTL grid
            
        }
    }
    
    function jsAddDummyRowDTL_Grid(iNumberRows){
   
        //Get max number for ID:
        var sID = jsGenDTLrowID();
        var sAppendRows = '';
        
        if (iNumberRows > 0) {
           
            for (var i = 0; i < iNumberRows; i++) {
                if (!isNaN(sID)) {
                    sID++;
                    sAppendRows = sAppendRows + '<tr>';
                    sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "' onclick='jsCalcTotalMoney(); jsCalCharacter();jsConvertCurrToText();jsGenThuTuButToan();' /></td>";
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SO_TK_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' onblur='jsGenThuTuButToan();' /></td>";
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ngay_TK_" + sID + "' style='width: 98%; border-color: White;' onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}' class='inputflat' maxlength='10' onblur='javascript:CheckDate(this)' value='' /></td>";
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_LT_" + sID + "' style='width: 98%;' class='inputflat' value='11' /></td>";
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Lhxnk_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' /></td>";
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SAC_THUE_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' /></td>";
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_CHUONG_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' /></td>";
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ma_TMuc_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' onblur='onBlurMaTieuMuc(" + sID + ")' /></td>";
                    
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Noi_Dung_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' /></td>";
                    
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTien_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onkeyup='ValidInteger(this); jsFormatNumber(this);' onblur='jsCalcTotalMoney(); jsCalCharacter();jsCalcCurrency(" + sID + ");' value='' /></td>";
                    sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTienNT_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onkeyup='jsFormatNumber2(this);' value='' /></td>";
                    
                    sAppendRows = sAppendRows + "<td style = 'display: none;'><input type='text' id='DTL_TT_BTOAN_" + sID + "' style='width: 98%; display: none;' class='inputflat' value='' /></td>";
                    sAppendRows = sAppendRows + '</tr>';
                   
                }
            }
            
            $('#grdChiTiet tr:last').after(sAppendRows);
            jsGenThuTuButToan();
        }
    }
    
    function jsRemoveRowDTL_Grid(arrParam, sLoaiTruyVan) {
        //switch case Thue/Phi HQ
        //var sLoaiTruyVan = $('#hdfLoaiCTu').val();
        var bFlag = false;
        $('#grdKQTruyVan tr').each(function(){
            var ctrCheck = $(this).find("input[id^='HDR_chkSelect_']");
            var ctrInput = null;
            if (ctrCheck) {
                if (ctrCheck.is(":checked")){
                    bFlag = true;
                    return false; //break out each loop
                }
            }
        });
        
        if (!bFlag) {
            //clear all DTL grid
            for (var i = 0; i < arrParam.length; i++) {
                $('#grdChiTiet tr').each(function(){
                    var ctrl = $(this).find("input[id^='DTL_SO_TK_']");
                    if (typeof ctrl !== 'undefined' && ctrl !== null && ctrl.length > 0) {
                        $(this).remove();
                    }
                });
            }
            
        } else {
            //arrParam: include SO_TK array to remove
            if ((typeof arrParam !== 'undefined') 
                && (arrParam !== null) 
                && (arrParam instanceof Array)
                && (arrParam.length > 0)) {
                
                for (var i = 0; i < arrParam.length; i++) {
                    $('#grdChiTiet tr').each(function(){
                        var sSoToKhai = $(this).find("input[id^='DTL_SO_TK_']").val();
                        //Thue/Phi khac nhau
                        if (sLoaiTruyVan == "T") {
                            if (arrParam[i].So_TK == sSoToKhai) {
                                $(this).remove();
                            }
                        } else if (sLoaiTruyVan == "P") {
                            var arrCTuNo = arrParam[i].CT_No;
                            if (arrCTuNo) {
                                if (arrCTuNo.length > 0) {
                                    for (var j = 0; j < arrCTuNo.length; j++) {
                                        if (arrCTuNo[j] && arrCTuNo[j].So_TK == sSoToKhai) {
                                            $(this).remove();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }
        jsFormatNumberDTL_Grid();
    }
    
    function jsGenDTLrowID(){
        //muc dich: sinh ra so ID cao nhat trong cac <tr> detail => tranh trung lap <tr> ID detail 
        var sRet = null;
        var sID = '';
        var arrID = [];
        
        $('#grdChiTiet tr').each(function(){
            sID = '';
            var ctrInput = $(this).find("input[id^='DTL_SO_TK_']");
            if (ctrInput) {
                sID = ctrInput.prop("id");
            }
            if (sID && sID.trim() != '') {
                var arrTmp = sID.trim().split('_');
                sID = arrTmp[arrTmp.length - 1];
                if (typeof sID !== 'undefined' && sID !== null && !isNaN(sID)) {
                    arrID.push(parseInt(sID));
                }
            }
        });
        if (arrID && arrID.length > 0) {
            sRet = Math.max.apply(Math,arrID);
        } else {
            sRet = 0;
        }
        
        return sRet;
    }
    
    function jsCalcTotalMoney(){
        var dTotalMoney = 0;
        var dDetailMoney = 0;
        
        var tongTienNT = 0;
        var ctTongTienNT = 0;
        //get detail
        $('#grdChiTiet tr').each(function(){
            var ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
            var ctrInput = null;
            var ctrInputNT = null;
            if (ctrCheck) {
                if (ctrCheck.is(":checked")){
                    ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                    if (ctrInput) {
                        dDetailMoney = 0;
                        dDetailMoney = accounting.unformat(ctrInput.prop("value"));
                        if (!isNaN(dDetailMoney)) {
                            dTotalMoney = dTotalMoney + dDetailMoney;
                        }
                    }
                    
                    ctrInputNT = $(this).find("input[id^='DTL_SoTienNT_']");
                    
                    if (ctrInputNT) {
                        ctTongTienNT = 0;
                        ctTongTienNT = accounting.unformat(ctrInputNT.prop("value"),".");
                        if (!isNaN(ctTongTienNT)) {
                            tongTienNT += ctTongTienNT;
                        }
                    }
                } else {
                    ctrInput = null;
                    ctrInputNT = null;
                }
            }
        });
        
       if (document.getElementById('chkChargeType').checked)
        {
            dblPhi =(dTotalMoney * feeRate)/minFee; 
            dblVAT=  (dTotalMoney * feeRate)/minFee/10;  
            if (dblPhi<minFee) 
            {
                dblPhi=minFee;
                dblVAT=dblPhi/10;
            }
        }
        else
        {
            dblPhi = parseFloat (accounting.unformat($('#txtCharge').val()));  
            dblVAT= dblPhi/10;          
        }
        
        dblTongTrichNo = dblPhi + dblVAT + dTotalMoney;
        $('#txtVAT').val(accounting.formatNumber(dblVAT));
        $('#txtCharge').val(accounting.formatNumber(dblPhi));
        $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo));       
        
        $('#txtTongTien').val(accounting.formatNumber(dTotalMoney));
        
        if($("#ddlMaNT option:selected").val() != 'VND')
            $('#txtTongTienNT').val(accounting.formatNumber(tongTienNT,2,",","."));
        else
            $('#txtTongTienNT').val(accounting.formatNumber(tongTienNT));
    }
    
    function ValidDate(dateString)
    {
        var regex_date = /^\d{1,2}\/\d{1,2}\/\d{4}$/; //pattern: dd/MM/yyyy
        if(!regex_date.test(dateString))
        {
            return false;
        }
        var parts   = dateString.split("/");
        var day     = parseInt(parts[0], 10);
        var month   = parseInt(parts[1], 10);
        var year    = parseInt(parts[2], 10);
        
        if(year < 1000 || year > 3000 || month == 0 || month > 12)
        {
            return false;
        }
        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        //For leap years
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        {
            monthLength[1] = 29;
        }
        
        return day > 0 && day <= monthLength[month - 1];
    }
    
    function jsCalCharacter(){
        var tempCharater='';
        
        tempCharater+= "MST" + $get('txtMaNNT').value;
        tempCharater+= " LT04";
        
//        if (document.getElementById("ddlMaLoaiThue").value=='04' )
//        {
//            tempCharater+= " TK:" + $get('txtToKhaiSo').value;
//            tempCharater+= " NGAYDK:" + $get('txtNgayDK').value;
//            tempCharater+= " LHXNK:" +$get('txtLHXNK').value;
//        }
       
        //get detail
        $('#grdChiTiet tr').each(function(){
            //sID = '';
            var ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
            var ctrInput = null;
            if (ctrCheck) {
                //sID = ctrInput.prop("id");
                if (ctrCheck.is(":checked")){
                    ctrInput = $(this).find("input[id^='DTL_SO_TK_']");
                    if (ctrInput) {
                        tempCharater += " TK:" + ctrInput.prop("value");
                    }
                    ctrInput = $(this).find("input[id^='DTL_Ngay_TK_']");
                    if (ctrInput) {
                        tempCharater += " NGAYDK:" + ctrInput.prop("value");
                    }
                    ctrInput = $(this).find("input[id^='DTL_Lhxnk_']");
                    if (ctrInput) {
                        tempCharater += " LHXNK:" + ctrInput.prop("value");
                    }
//                    ctrInput = $(this).find("input[id^='DTL_SAC_THUE_']");
//                    if (ctrInput) {
//                        tempCharater += " ST:" + ctrInput.prop("value");
//                    }
                    ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                    if (ctrInput) {
                        tempCharater += " TM:" + ctrInput.prop("value");
                    }
                    ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                    if (ctrInput) {
                        tempCharater += " ST:" + ctrInput.prop("value");
                    }
                } else {
                    ctrInput = null;
                }
            }
        });
        
        tempCharater+= " CT " + $get('txtTenKB').value;
        tempCharater+= " TAI " + $get('txtTenMA_NH_B').value;
        
        var charNumberLimit = 210;
        charNumber = charNumberLimit - tempCharater.length;
        document.getElementById('leftCharacter').visible = true;
        document.getElementById('leftCharacter').innerHTML = charNumber + "/" + charNumberLimit;
        if (charNumber <0)
        {
            return false;
        }
        else
        { 
            return true;
        }
    }
    
    function jsCheckMaNNT(param) {
        if((param.value.trim()).length!=0 && (param.value.trim()).length!=10 && (param.value.trim()).length!=13)
        {
            param.value = "";
		    param.focus();
		    alert("Mã số thuế không hợp lệ!");
		    return;
		}
		jsGet_TenNNT();
    }
    
    function jsGet_DataByMaHQPH() {
        $get('txtMaHQ').value = '';
        $get('txtTenMaHQ').value = '';
        $get('txtMaCQThu').value = '';
        $get('txtTenCQThu').value = '';
        $get('txtTenMaHQPH').value = '';
        $get('txtSHKB').value = '';
        $get('txtTenKB').value = '';
    
        PageMethods.Get_TenMaHQ($('#txtMaHQPH').val(), Get_DataByMaHQPH_Complete, Get_DataByMaHQPH_Error);
    }
    
    function Get_DataByMaHQPH_Complete(result, methodName) {
        jsCheckUserSession(result);
        if (result.length > 0) {
            var arr = result.split(';');
            $('#txtMaHQ').val($get('txtMaHQPH').value);
            $('#txtTenMaHQ').val(arr[1]);
            $('#txtMaCQThu').val(arr[0]);
            $('#txtTenCQThu').val(arr[1]);
            $('#txtTenMaHQPH').val(arr[1]);
            $('#txtSHKB').val(arr[2]); //auto fire jsGetTTNganHangNhan, jsGet_TenKB ...
        }
        jsCalCharacter();
    }
    function Get_DataByMaHQPH_Error(error, userContext, methodName) {
        jsCalCharacter();
    }
    
    function jsGetTenHQ(){
        $('#txtTenMaHQ').val('');
        PageMethods.Get_TenMaHQ($('#txtMaHQ').val(), GetTenHQ_Complete, GetTenHQ_Error);
    }
    function GetTenHQ_Complete(result){
        jsCheckUserSession(result);
        if (result.length > 0) {
            var arr = result.split(';');
            if (typeof arr !== 'undefined' && arr !== null && arr.length > 0) {
                $('#txtTenMaHQ').val(arr[1]);
            }
        }
        jsCalCharacter();
    }
    function GetTenHQ_Error(){
        jsCalCharacter();
    }
    
    
    function jsGet_TenNNT() {
        $get('txtTenNNT').value = '';
        PageMethods.Get_TenNNT($get('txtMaNNT').value, Get_TenNNT_Complete, Get_TenNNT_Error);
    }

    function Get_TenNNT_Complete(result, methodName) {
        jsCheckUserSession(result);
        if (result.length > 0) {
            $get('txtTenNNT').value = result;
        }
    }
    function Get_TenNNT_Error(error, userContext, methodName) {
        if (error !== null) {
            
        }
    }
    
    function jsShowDivStatus(bShow, sMessage) {
        if (bShow) {
            document.getElementById('divStatus').style.display = '';
            document.getElementById('divStatus').innerHTML = sMessage;
        } else {
            document.getElementById('divStatus').style.display = 'none';
            document.getElementById('divStatus').innerHTML = sMessage;
        }
    }
    
    function jsShowHideKQTruyVanHQ(bShow) {
        if (bShow) {
            document.getElementById('divKQTruyVan').style.display='';
        } else {
            document.getElementById('divKQTruyVan').style.display='none';
        }
    }
    
    function jsShowHideDivProgress(bShow) {
        if (bShow) {
            document.getElementById('divProgress').style.display='';
        } else {
            document.getElementById('divProgress').innerHTML = 'Đang xử lý. Xin chờ một lát...';
            document.getElementById('divProgress').style.display='none';
        }
    }
    
    function jsGhi_CTU(sAct) {
        //Bao gom Thue/Phi HQ
        if (jsValidBeforeGetFormData(sAct)) {
            if (true) {
                var objHDR = jsGetHDRObject();
                var objDTL = jsGetDTLArray();
                var sType = null;
                var bIsEdit = $('#hdfIsEdit').val();
                if (sAct == "GHI") {
                    if (bIsEdit == "true") {
                        sType = 'UPDATE';
                    } else {
                        sType = 'INSERT';
                    }
                } else if (sAct == "GHIKS") {
                    if (bIsEdit == "true") {
                        sType = 'UPDATE_KS';
                    } else {
                        sType = 'INSERT_KS';
                    }
                } else if (sAct == "GHIDC") {
                    if (bIsEdit == "true") {
                        sType = 'UPDATE_DC';
                    }
                } else {
                    return;
                }
                if (jsValidFormData(objHDR, objDTL, sAct, sType)) {
                   
                    //Imediate disable all save buttons after save click - and then enable them (if need) after execute save function
                    jsEnableDisableControlButton('0');
                    $.ajax({
                        type: 'POST',
                        url: 'frmLapCTuPhi_HQ.aspx/Ghi_CTu',
                        contentType: 'application/json; charset=utf-8',
                        dataType: "json",
                        data: JSON.stringify({
                            ctuHDR: objHDR,
                            arrDTL: objDTL,
                            sAction: sAct,
                            sSaveType: sType
                        }),
                        success: function(reponse) {
                            var sRet = reponse.d.split(';');
                            if (sRet && sRet[0] == 'Success') {
                                var sOut = '';
                                if ((sRet[2]) && (sRet[2].indexOf('/') > 0) && (sRet[2].split('/').length > 0)) {
                                    sOut = sRet[2].split('/');
                                }
                                
                                switch (sType) {
                                    case "INSERT":
                                        if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                            alert('Hoàn thiện thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                        } else {
                                            alert('Hoàn thiện thành công!');
                                        }
                                        jsEnableDisableControlButton('6');
                                        break;
                                    case "UPDATE":
                                        if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                            alert('Cập nhật thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                        } else {
                                            alert('Cập nhật thành công!');
                                        }
                                        jsEnableDisableControlButton('6');
                                        break;
                                    case "INSERT_KS":
                                        if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                            alert('Hoàn thiện & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                        } else {
                                            alert('Hoàn thiện & gửi kiểm soát thành công!');
                                        }
                                        jsEnableDisableControlButton('7');
                                        break;
                                    case "UPDATE_KS":
                                        if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                            alert('Cập nhật & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                        } else {
                                            alert('Cập nhât & gửi kiểm soát thành công!');
                                        }
                                        jsEnableDisableControlButton('7');
                                        break;
                                    case "UPDATE_DC":
                                        if (true) {
                                            alert('Cập nhật điều chỉnh & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                        }
                                        jsEnableDisableControlButton('3');
                                        break;
                                }
                                
                                //re-load lai chung tu len form (trong này đã có set hdfDescCTu)
                                if ((sOut) && (sOut.length > 1)) {
                                    jsGetCTU(sOut[1]);
                                } else {
                                    jsGetCTU(null);
                                }
                            } else {
                                $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                                switch (sType) {
                                    case "INSERT":
                                        alert('Hoàn thiện không thành công: ' + sRet[1]);
                                        jsEnableDisableControlButton('4');
                                        break;
                                    case "UPDATE":
                                        alert('Cập nhật không thành công: ' + sRet[1]);
                                        jsEnableDisableControlButton('4');
                                        break;
                                    case "INSERT_KS":
                                        alert('Hoàn thiện & gửi kiểm soát không thành công: ' + sRet[1]);
                                        jsEnableDisableControlButton('4');
                                        break;
                                    case "UPDATE_KS":
                                        alert('Cập nhật & gửi kiểm soát không thành công: ' + sRet[1]);
                                        jsEnableDisableControlButton('3');
                                        break;
                                    case "UPDATE_DC":
                                        alert('Cập nhật điều chỉnh & gửi kiểm soát không thành công: ' + sRet[1]);
                                        jsEnableDisableControlButton('10');
                                        break;
                                }
                            }
                            $('#hdfIsEdit').val(true);
                            jsLoadCTList();
                        },
                        error: function(request, status, error) {
                            var err;
                            err = JSON.parse(request.responseText);
                            if (typeof err !== 'undefined' && err !== null) {
                                if (err.Message.indexOf(';') > -1) {
                                    jsCheckUserSession(err.Message);
                                }
                                alert(err.Message);    
                                switch (sType) {
                                    case "INSERT":
                                        jsEnableDisableControlButton('4');
                                        break;
                                    case "UPDATE":
                                        jsEnableDisableControlButton('4');
                                        break;
                                    case "INSERT_KS":
                                        jsEnableDisableControlButton('4');
                                        break;
                                    case "UPDATE_KS":
                                        jsEnableDisableControlButton('3');
                                        break;
                                    case "UPDATE_DC":
                                        jsEnableDisableControlButton('10');
                                        break;
                                }
                            }
                        }
                    });
                }
            }
        }
        
    }
    
    function jsHuy_CTU() {
        //jsEnableDisableControlButton('0');
        jsCalCharacter();
        if (true) {
            var sSoCTu, sLyDoHuy;
            sSoCTu = $('#hdfSo_CTu').val();
            sLyDoHuy = $('#txtLyDoHuy').val();
            if (typeof sSoCTu !== 'undefined' && sSoCTu !== null && sSoCTu !== '') {
                if (typeof sLyDoHuy == 'undefined' || sLyDoHuy == null || sLyDoHuy == '') {
                    if(curCtuStatus != '00'){
                        alert('Vui lòng nhập vào lý do hủy.');
                        return;
                    }
                 }else{
                    $.ajax({
                        type: 'POST',
                        url: 'frmLapCTuPhi_HQ.aspx/Huy_CTU',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify({
                            sSoCT: sSoCTu,
                            sLyDo: sLyDoHuy || null
                        }),
                        success: function(reponse) {
                            var sRet = reponse.d.split(';');
                            if (typeof sRet !== 'undefined' && sRet !== null && typeof sRet[1] !== 'undefined' && sRet[1] !== '') {
                                var sRetSplit = sRet[1].split('/');
                                if (typeof sRetSplit !== 'undefined' && sRetSplit !== null && sRetSplit.length > 0) {
                                    alert(sRet[0] + ' - Số CT: ' + sRetSplit[1] + ' - Số BT: ' + sRetSplit[0] + ' - SHKB: ' + sRetSplit[2]);
                                    jsEnableDisableControlButton('1');
                                    $('#hdfDescCTu').val('SHKB/SoCT/SoBT:' +  + sRet[1]);
                                } else {
                                    alert('Hủy chứng từ không thành công. Vui lòng liên hệ quản lý hệ thống');
                                    $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                                }
                            } else {
                                alert('Hủy chứng từ không thành công. Vui lòng liên hệ quản lý hệ thống');
                                $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                            }                          
                            $('#hdfIsEdit').val(true);
                            jsLoadCTList();
                        },
                        error: function(request, status, error) {
                            var err;
                            err = JSON.parse(request.responseText);
                            if (err) {
                                if (err.Message.indexOf(';') > -1) {
                                    jsCheckUserSession(err.Message);
                                }
                                alert(err.Message);    
                                //console.log(request.responseText);
                                //jsEnableDisableControlButton('4');
                            }
                        }
                    });
                }
            } else {
                alert('Không xác định được chứng từ cần hủy');
            }
        }
    }
    
    function jsValidBeforeGetFormData(sAct) {
        if(sAct == "GHIDC")
            if($('#txtLyDoHuy').val().length == 0){
                alert("Xin vui lòng nhập Lý do điều chỉnh!");
                $('#txtLyDoHuy').focus();
                return false;
            }
           
        return true;
    }
    
    function jsValidFormData(HDR, DTL, sAction, sType) {
        if(HDR.Ten_NNThue.length > 70){
            alert('Tên người nộp thuế > 70 ký tự. Vui lòng kiểm tra lại');
            $('#txtTenNNT').focus();
            return false;
        }
    
        //valid total characters
        if (!jsCalCharacter()) {
            alert('Vượt quá số ký tự cho phép. Vui lòng kiểm tra lại');
            return false;
        }
        //validate HDR
        if (!HDR.Ma_NNThue) {
            alert('Vui lòng nhập vào Mã số thuế');
            $('#txtMaNNT').focus();
            return false;
        }
        if (!HDR.MA_HQ) {
            alert('Vui lòng nhập vào Mã HQ');
            $('#txtMaHQ').focus();
            return false;
        }
        if (!HDR.TEN_MA_HQ) {
            alert('Không tìm thấy thông tin HQ với mã nhập vào');
            $('#txtTenMaHQ').focus();
            return false;
        }
        if (!HDR.Ma_hq_cqt) {
            alert('Vui lòng nhập vào mã CQT thu');
            $('#txtMaCQThu').focus();
            return false;
        }
        if (!HDR.Ten_cqthu) {
            alert('Không tìm thấy thông tin CQT với mã nhập vào');
            $('#txtTenCQThu').focus();
            return false;
        }
        
        if (HDR.Ten_cqthu.length > 40) {
            alert('Tên cơ quan thu > 40 ký tự(lớn hơn ký tự cho phép của core). Vui lòng kiểm tra lại!');
            $('#txtTenCQThu').focus();
            return false;
        }        
        
        if (!HDR.Ma_hq_ph) {
            alert('Vui lòng nhập vào mã HQ phát hành');
            $('#txtMaHQPH').focus();
            return false;
        }
        if (!HDR.TEN_HQ_PH) {
            alert('Không tìm thấy thông tin HQ phát hành với mã nhập vào');
            $('#txtTenMaHQPH').focus();
            return false;
        }
        if (!HDR.SHKB) {
            alert('Vui lòng nhập vào SHKB');
            $('#txtSHKB').focus();
            return false;
        }
        if (!HDR.MA_NTK) {
            alert('Vui lòng nhập vào TK có NSNN');
            $('#ddlMa_NTK').focus();
            return false;
        }
        if (!HDR.Ten_kb) {
            alert('Không tìm thấy thông tin kho bạc với mã nhập vào');
            $('#txtTenKB').focus();
            return false;
        }
        if (!HDR.TK_Co) {
            alert('Vui lòng nhập vào tài khoản có');
            $('#ddlMaTKCo').focus();
            return false;
        }
        
         if(HDR.PT_TT =="03"){            
            if (!HDR.TK_GL_NH) {
                alert('Vui lòng nhập số tài khoản GL');
                $('#txtTKGL').focus();
                return false;
            }
         
        }else if(HDR.PT_TT == "01"){
            if (!HDR.TK_KH_NH) {
                alert('Vui lòng nhập vào mã tài khoản chuyển');
                $('#txtTK_KH_NH').focus();
                return false;
            }
            var total = accounting.unformat($('#txtTongTrichNo').val());
            if(HDR.TT_TTHU > total){
                alert('Tổng tiền trích nợ đang lớn hơn số tiền của khách hàng. Vui lòng kiểm tra lại.');
                return false;
            }
        }
//        if (!HDR.TEN_KH_NH) {
//            alert('Không tìm thấy thông tin tài khoản chuyển với mã nhập vào');
//            //$('#txtTenTK_KH_NH').focus();
//            return false;
//        }
//        if (!HDR.MA_NH_A) {
//            alert('Vui lòng nhập vào thông tin ngân hàng chuyển');
//            $('#ddlMA_NH_A').focus();
//            return false;
//        }
//        if (!HDR.MA_NH_TT) {
//            alert('Vui lòng nhập vào mã ngân hàng trực tiếp');
//            $('#txtMA_NH_TT').focus();
//            return false;
//        }
//        if (!HDR.TEN_NH_TT) {
//            alert('Không tìm thấy thông tin ngân hàng trực tiếp với mã nhập vào');
//            $('#txtTEN_NH_TT').focus();
//            return false;
//        }
        if (!HDR.MA_NH_B) {
            alert('Vui lòng nhập vào mã ngân hàng thụ hưởng');
            $('#txtMA_NH_B').focus();
            return false;
        }
        if (!HDR.Ten_NH_B) {
            alert('Không tìm thấy thông tin ngân hàng thụ hưởng với mã nhập vào');
            $('#txtTenMA_NH_B').focus();
            return false;
        }
        
        //validate DTL       
        if (!(DTL) || !(DTL instanceof Array) || !(DTL.length > 0)) { //IS: null, undefined, 0, NaN, false, or ""
            alert('Chứng từ thuế phải có chi tiết. Vui lòng kiểm tra lại');
            return false;
        }
        
        return true;
    }
    
    function jsEnableDisableAddRowButton(sCase){
        switch (sCase) {
            case "0":
                $('#btnAddRowDTL').prop('disabled', true);
                break;
            case "1":
                $('#btnAddRowDTL').prop('disabled', false);
                break;
        }
    }
    
    function jsEnableDisableCtrlBtnByTThaiCTu(sTThaiCTu, sTThaiCThue) {
        switch (sTThaiCTu) {
            case "00": //đã hoàn thiện
                jsEnableDisableControlButton("6");
                jsEnableDisableAddRowButton("1");
                break;
            case "01": //KS thành công
                if (sTThaiCThue == "1") { //gửi thuế thành công => cho phép gửi điều chỉnh
                    jsEnableDisableControlButton("10");
                    jsEnableDisableAddRowButton("1");
                } else { //
                    jsEnableDisableControlButton("3");
                    jsEnableDisableAddRowButton("0");
                }
                break;
            case "02": //GDV Hủy sau khi hoàn thiện (ko cần qua KSV duyệt)
                jsEnableDisableControlButton("1");
                jsEnableDisableAddRowButton("0");
                break;
            case "03": //Chuyển trả
                jsEnableDisableControlButton("9");
                jsEnableDisableAddRowButton("1");
                break;
            case "04": //Hủy
                jsEnableDisableControlButton("8");
                jsEnableDisableAddRowButton("0");
                break;
            case "05": //đã chuyển kiểm soát
                jsEnableDisableControlButton("8");
                jsEnableDisableAddRowButton("0");
                break;
        }
    }
    
    function jsEnableDisableControlButton(sCase){
        switch (sCase) {
            case "0": //disable all button
                $('#cmdThemMoi').prop('disabled', true);
                $('#cmdGhiKS').prop('disabled', true);
                $('#cmdChuyenKS').prop('disabled', true);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', true);
                $('#cmdHuyCT').prop('disabled', true);
                break;
            case "1": //cmdThemMoi - btnAddRowDTL
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', true);
                $('#cmdChuyenKS').prop('disabled', true);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', true);
                $('#cmdHuyCT').prop('disabled', true);
                break;
            case "2": //cmdThemMoi - cmdChuyenKS - cmdInCT - cmdHuyCT
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', true);
                $('#cmdChuyenKS').prop('disabled', false);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', false);
                $('#cmdHuyCT').prop('disabled', false);
                break;
            case "3": //cmdThemMoi - cmdInCT - cmdHuyCT
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', true);
                $('#cmdChuyenKS').prop('disabled', true);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', false);
                $('#cmdHuyCT').prop('disabled', false);
                break;
            case "4": //cmdThemMoi - cmdGhiKS - cmdChuyenKS
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', false);
                $('#cmdChuyenKS').prop('disabled', false);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', true);
                $('#cmdHuyCT').prop('disabled', true);
                break;
            case "5": //cmdThemMoi - cmdGhiKS - cmdChuyenKS - cmdInCT
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', false);
                $('#cmdChuyenKS').prop('disabled', false);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', false);
                $('#cmdHuyCT').prop('disabled', true);
                break;
            case "6": //cmdThemMoi - cmdChuyenKS - cmdHuyCT
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', true);
                $('#cmdChuyenKS').prop('disabled', false);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', true);
                $('#cmdHuyCT').prop('disabled', false);
                break;
            case "7": //cmdThemMoi - cmdChuyenKS - cmdInCT - cmdHuyCT
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', true);
                $('#cmdChuyenKS').prop('disabled', false);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', false);
                $('#cmdHuyCT').prop('disabled', false);
                break;
            case "8": //cmdThemMoi - cmdInCT
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', true);
                $('#cmdChuyenKS').prop('disabled', true);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', false);
                $('#cmdHuyCT').prop('disabled', true);
                break;
            case "9": //cmdThemMoi - cmdGhiKS - cmdChuyenKS - cmdHuyCT
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', false);
                $('#cmdChuyenKS').prop('disabled', false);
                $('#cmdDieuChinh').prop('disabled', true);
                $('#cmdInCT').prop('disabled', true);
                $('#cmdHuyCT').prop('disabled', false);
                break;
            case "10": //cmdThemMoi - cmdDieuChinh - cmdInCT - cmdHuyCT
                $('#cmdThemMoi').prop('disabled', false);
                $('#cmdGhiKS').prop('disabled', true);
                $('#cmdChuyenKS').prop('disabled', true);
                $('#cmdDieuChinh').prop('disabled', false);
                $('#cmdInCT').prop('disabled', false);
                $('#cmdHuyCT').prop('disabled', false);
                break;
                
            default:
                break;
        }
    }
    
    function jsGetFloatNumberFromString(str, exceptChar) {
        var fRet = 0;
        if (typeof str !== 'undefined' && str !== null && str.trim() !== '') {
            
        }
        
        return fRet;
    }
    
    function jsGetHDRObject() {
        var today = new Date();
        var sTmpString = '';
        
        var tongTien = accounting.unformat($('#txtTongTien').val());
        
        var tongTienNT = 0;
        if(maTienTe !='VND')
            tongTienNT = accounting.unformat($('#txtTongTienNT').val(),".");
        else
            tongTienNT = accounting.unformat($('#txtTongTienNT').val());
    
        var SO_XCARD = "";
        var Ten_NH_B = $('#txtTenMA_NH_B').val();
        var TrangThai_BL = "";
        var Kenh_CT = "";
        var Ngay_HD_NT = "";
        var Ngay_HDon = "";
        var Ngay_VTD = "";
        var HDon = "";
        var VTD = "";
        var HD_NT = "";
        var MA_HQ = $('#txtMaHQ').val();
        var TEN_HQ = $('#txtTenMaHQ').val();
        var TEN_HQ_PH = $('#txtTenMaHQPH').val();
        var TEN_MA_HQ = $('#txtTenMaHQ').val();
        var LY_DO_CTRA = $('#txtLyDoHuy').val();
        var KHCT = "";
        var Ten_kb = $('#txtTenKB').val();
        var Ten_nv = "";
        var Huyen_nnthue = $('#txtTenHuyen_NNT').val();
        var Tinh_nnthue = $('#txtTenTinh_NNT').val();
        var Ten_ks = "";
        var Ten_cqthu = $('#txtTenCQThu').val();
        var Lhxnk = "";
        var Vt_lhxnk = "";
        var Ten_lhxnk = "";
        var Ten_nt = $('#ddlMaNT option:selected').text();
        var Ten_nh_a = $('#ddlMA_NH_A option:selected').text();
        var Ttien_tthu = tongTien;
        var Ghi_chu = "";
        var Tk_ns = "";
        var Ten_tk_ns = "";
        var Phuong_thuc = "";
        var Hinh_thuc = "";
        var Khoi_phuc = "";
        var Reponse_code = "";
        var Hq_tran_id = "";
        var Hq_loai_thue = "";
        var Res_hq = "";
        var So_bl = "";
        var Songay_bl = "0";
        var Kieu_bl = "";
        var Ma_hq_ph = $('#txtMaHQPH').val();
        var Dien_giai = "";
        var Trangthai_lapct = "";
        var Ngay_batdau = "";
        var Ngay_ketthuc = "";
        var Error_code_hq = "";
        var So_ct_ctu = "";
        var Dchieu_hq = "";
        var Is_ma_hq = "";
        var Ngay_dc_hq = "";
        var Kb_thu_ho = "";
        var Ma_loaitien = "";
        var Ma_hq_cqt = $('#txtMaCQThu').val();
        var So_bt_hq = "0";
        var QUAN_HUYEN_NNTIEN = $('#txtTenQuan_HuyenNNTien').val();
        var TKHT_NH = "";
        var TEN_TK_CO = $('#ddlMa_NTK option:selected').text();
        var TEN_TK_NO = ""; //$('#ddlMaTKNo').val();
        var TINH_TPHO_NNTIEN = $('#txtTenTinh_NNTien').val();
        var So_RM = "";
        var MATELLER_GDV = "";
        var MATELLER_KSV = "";
        var RM_REF_NO = "";
        var REF_NO = "";
        var TK_KB_NH = "";
        var Mode = "";
        var PT_TT = $('#ddlPT_TT').val();
        var TEN_KH_NH = $('#txtTenTK_KH_NH').val();
        var TK_GL_NH = $("#txtTKGL").val();
        var SHKB = $('#txtSHKB').val();
        var Ngay_KB = today.getFullYear() + ('0' + (today.getMonth() + 1)).slice(-2) + ('0' + today.getDate()).slice(-2);
        if (typeof dtmNgayLV !== 'undefined' && dtmNgayLV !== null && dtmNgayLV.length > 0) {
            var arrNgayLV = dtmNgayLV.split('/');
            if (typeof arrNgayLV !== 'undefined' && arrNgayLV !== null && arrNgayLV.length > 0) {
                Ngay_KB = ('20' + arrNgayLV[2]).slice(-4) + ('0' + arrNgayLV[1]).slice(-2) + ('0' + arrNgayLV[0]).slice(-2)
            }
        }
        var Ma_NV = 0;
        var So_BT = 0;
        if($('#hdfSoBT').val() != 'undefinided' || $('#hdfSoBT').val() != null){
            if($('#hdfSoBT').val().length > 0)
                So_BT =  $('#hdfSoBT').val();
        }
            
        var Ma_DThu = "";
        var So_BThu = "0";
        var KyHieu_CT = "";
        var So_CT = "";
        if ($('#hdfSo_CTu').val()) {
            So_CT = $('#hdfSo_CTu').val();
        }
        var So_CT_TuChinh = "";
        var So_CT_NH = "";
        var Ma_NNTien = $('#txtMaNNTien').val();
        var Ten_NNTien = $('#txtTenNNTien').val();
        var DC_NNTien = $('#txtDChiNNTien').val();
        var Ma_NNThue = $('#txtMaNNT').val();
        var Ten_NNThue = $('#txtTenNNT').val();
        var DC_NNThue = $('#txtDChiNNT').val();
        var Ly_Do = $('#txtLyDoHuy').val();
        var Ma_KS = "";
        var Ma_TQ = "";
        var So_QD = "";
        var Ngay_QD = "";
        var CQ_QD = "";
        var Ngay_CT = '0';
        var tmpNgay_CT = $('#txtNGAY_HT').val();
        if (tmpNgay_CT && !(tmpNgay_CT.length < 10)) {
            Ngay_CT = tmpNgay_CT.slice(-4) + tmpNgay_CT.substr(3,2) + tmpNgay_CT.substr(0,2);
        }
        if (isNaN(parseInt(Ngay_CT))) {
            Ngay_CT = '0';
        }
        
        var Ngay_HT = $('#txtNGAY_HT').val();
        var Ma_CQThu = $('#txtMaCQThu').val();
        var XA_ID = "";
        var Ma_Tinh =  $('#txtTinh_NNT').val();
        var Ten_tinh = "";
        var Ma_Huyen = $('#txtHuyen_NNT').val();
        var Ma_Xa = $('#txtMaDBHC').val();
        var Ten_Xa = $('#txtTenDBHC').val();
        var TK_No = "";
        var TK_Co = $('#ddlMaTKCo').val();
        
        var So_TK = "";
        var LOAI_TT = "";
        var ctrInput = null;
        var ctrCheck = null;
        
        var arrSoTk = [];
    
        $('#grdChiTiet tr').each(function(){
            var ctrInput = null;
            var ctrCheck = null;
            
            ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
            if (ctrCheck) {
                if (ctrCheck.is(":checked")){
                    var objSoTk = {};
                    ctrInput = $(this).find("input[id^='DTL_SO_TK_']");
                    objSoTk["SO_TK"] = ctrInput.val();
                    if(arrSoTk.length >0){
                        var flag = true;                    
                        for(var j=0; j< arrSoTk.length;j++){
                            var objDtl = arrSoTk[j];
                            if(objSoTk.SO_TK != arrSoTk[j].SO_TK)
                                flag = true;
                            else{
                                flag = false;
                                break;
                            }
                        }
                        if(flag)  
                            arrSoTk.push(objSoTk);
                    }else
                        arrSoTk.push(objSoTk);
                }
            }
        });
        
        for(var i=0;i<arrSoTk.length;i++){
            if(So_TK.length==0)
                So_TK = arrSoTk[i].SO_TK;
            else
                So_TK = So_TK + ";" + arrSoTk[i].SO_TK;
        }
               
        var Ngay_TK = "";
        var LH_XNK = "";
        var DVSDNS = "";
        var Ten_DVSDNS = "";
        var Ma_LThue = "04";
        var TG_ID = "";
        var Ma_NT = $('#ddlMaNT option:selected').val();
        var Ty_Gia = 1;
        if ($('#txtTyGia').val()) {
            Ty_Gia  = accounting.unformat($('#txtTyGia').val());
        }
        var So_Khung = "";
        var So_May = "";
        var pttt = $("#ddlPT_TT option:selected").val();
         var TK_KH_NH="";
        if(pttt=="03")
        {
               TK_KH_NH = $('#txtTKGL').val();  
        }
        if(pttt=="01")
        {
             TK_KH_NH = $('#txtTK_KH_NH').val();
        }
        
        var NGAY_KH_NH = ""; //$('#txtNGAY_KH_NH').val();
        var MA_NH_A = $('#ddlMA_NH_A').val();
        var MA_NH_B = $('#txtMA_NH_B').val();
        var TTien = tongTien;
        
        var TT_TThu = "0";
        var Lan_In = "";
        var Trang_Thai = curCtuStatus;
        var TK_KH_Nhan = "";
        var Ten_KH_Nhan = "";
        var Diachi_KH_Nhan = "";
        var TTien_NT = tongTienNT;
        var So_BT_KTKB = "0";
        var So_BK = "";
        var Ngay_BK = "";
        var TT_BDS = "";
        var TT_NSTT = "";
        var PHI_GD = accounting.unformat($('#txtCharge').val());
        var PHI_VAT = accounting.unformat($('#txtVAT').val());
        var PHI_GD2 = "0";
        var PHI_VAT2 = "0";
        var PT_TINHPHI = "";
        var MA_NH_TT = $('#txtMA_NH_TT').val();
        var TEN_NH_TT = $('#txtTEN_NH_TT').val();
        var CIF = "";
        var SAN_PHAM = "";
        var TT_CITAD = "";
        var SEQ_NO = "";
        var LOAI_BL = "";
        var MA_CAP_CHUONG = "";
        var TIME_BEGIN = "";
        var SO_CMND = $("#txtCmtnd").val();
        var SO_FONE = $("#txtDienThoai").val();
        var MA_DV_DD = "";
        var TEN_DV_DD = "";
        var MA_NTK = $('#ddlMa_NTK').val();
        var Ma_CN = "";
        var VTD2 = "";
        var Ngay_VTD2 = "";
        var VTD3 = "";
        var Ngay_VTD3 = "";
        var VTD4 = "";
        var Ngay_VTD4 = "";
        var VTD5 = "";
        var Ngay_VTD5 = "";
        var MA_HQ_KB = "";
        var LOAI_CTU = $('#hdfLoaiCTu').val();
        var Ma_Huyen_NNThue = "";
        var Ma_Tinh_NNThue = "";
        var MA_QUAN_HUYENNNTIEN = $('#txtQuan_HuyenNNTien').val(); 
        var MA_TINH_TPNNTIEN = $('#txtTinh_NNTien').val();
        var NGAY_BAOCO = "";
        var NGAY_BAONO = "";
        var TT_CTHUE = 0;
        
        var objHDR = new infHDR(SO_XCARD , Ten_NH_B , TrangThai_BL , Kenh_CT , Ngay_HD_NT , Ngay_HDon , Ngay_VTD , HDon , VTD , HD_NT , MA_HQ , TEN_HQ , TEN_HQ_PH , LOAI_TT , TEN_MA_HQ , LY_DO_CTRA , KHCT , Ten_kb , Ten_nv , Huyen_nnthue , Tinh_nnthue , Ten_ks , Ten_cqthu , Lhxnk , Vt_lhxnk , Ten_lhxnk , Ten_nt , Ten_nh_a , Ttien_tthu , Ghi_chu , Tk_ns , Ten_tk_ns , Phuong_thuc , Hinh_thuc , Khoi_phuc , Reponse_code , Hq_tran_id , Hq_loai_thue , Res_hq , So_bl , Songay_bl , Kieu_bl , Ma_hq_ph , Dien_giai , Trangthai_lapct , Ngay_batdau , Ngay_ketthuc , Error_code_hq , So_ct_ctu , Dchieu_hq , Is_ma_hq , Ngay_dc_hq , Kb_thu_ho , Ma_loaitien , Ma_hq_cqt , So_bt_hq , QUAN_HUYEN_NNTIEN , TKHT_NH , TEN_TK_NO , TEN_TK_CO , TINH_TPHO_NNTIEN , So_RM , MATELLER_GDV , MATELLER_KSV , RM_REF_NO , REF_NO , TK_KB_NH , Mode , PT_TT , TEN_KH_NH , TK_GL_NH , SHKB , Ngay_KB , Ma_NV , So_BT , Ma_DThu , So_BThu , KyHieu_CT , So_CT , So_CT_TuChinh , So_CT_NH , Ma_NNTien , Ten_NNTien , DC_NNTien , Ma_NNThue , Ten_NNThue , DC_NNThue , Ly_Do , Ma_KS , Ma_TQ , So_QD , Ngay_QD , CQ_QD , Ngay_CT , Ngay_HT , Ma_CQThu , XA_ID , Ma_Tinh , Ten_tinh, Ma_Huyen , Ma_Xa , Ten_Xa , TK_No , TK_Co , So_TK , Ngay_TK , LH_XNK , DVSDNS , Ten_DVSDNS , Ma_LThue , TG_ID , Ma_NT , Ty_Gia , So_Khung , So_May , TK_KH_NH , NGAY_KH_NH , MA_NH_A , MA_NH_B , TTien , TT_TThu , Lan_In , Trang_Thai , TK_KH_Nhan , Ten_KH_Nhan , Diachi_KH_Nhan , TTien_NT , So_BT_KTKB , So_BK , Ngay_BK , TT_BDS , TT_NSTT , PHI_GD , PHI_VAT , PHI_GD2 , PHI_VAT2 , PT_TINHPHI , MA_NH_TT , TEN_NH_TT , CIF , SAN_PHAM , TT_CITAD , SEQ_NO , LOAI_BL , MA_CAP_CHUONG , TIME_BEGIN , SO_CMND , SO_FONE , MA_DV_DD , TEN_DV_DD , MA_NTK , Ma_CN , VTD2 , Ngay_VTD2 , VTD3 , Ngay_VTD3 , VTD4 , Ngay_VTD4 , VTD5 , Ngay_VTD5 , MA_HQ_KB, LOAI_CTU, Ma_Huyen_NNThue, Ma_Tinh_NNThue, MA_QUAN_HUYENNNTIEN, MA_TINH_TPNNTIEN, NGAY_BAOCO, NGAY_BAONO, TT_CTHUE);
                        
        return objHDR;
    }
    
    function jsGetDTLArray() {
        var sTmpString = '';
        var sLoaiTruyVan = $('#hdfLoaiCTu').val();
        
        var TT_BTOAN = "0";
        var Ma_nkt = "";
        var Ma_nkt_cha = "";
        var Ma_ndkt = "";
        var Ma_ndkt_cha = "";
        var Ma_tlpc = 0;
        var Ma_khtk = "";
        var Ma_nt = "";
        var Loai_thue_hq = "";
        var PT_TT = "";
        var ID = "0";
        var SHKB = "0";
        var Ngay_KB = 0;
        var Ma_NV = 0;
        var So_BT = 0;
        var Ma_DThu = "";
        var CCH_ID = "";
        var Ma_Cap = "";
        var Ma_Chuong = "";
        var LKH_ID = "";
        var Ma_Loai = "";
        var Ma_Khoan = "";
        var MTM_ID = "";
        var Ma_Muc = "";
        var Ma_TMuc = "";
        var Noi_Dung = "";
        var DT_ID = "";
        var Ma_TLDT = "";
        var Ky_Thue = "";
        var SoTien = 0;
        var SoTien_NT = 0;
        var MaQuy = "";
        var Ma_DP = "";
        var SO_CT = "";
        var SO_TK = "";
        var NGAY_TK = "";
        var LH_XNK = "";
        var MA_HQ = $('#txtMaHQ').val();
        var LOAI_TT = ""; 
        var SAC_THUE = "";
        var MA_LT = "";
        
        var arrDTL = [];
        var objDTL = null;
        //get detail
        var ctrCheck = null;
        var ctrInput = null;
        $('#grdChiTiet tr').each(function(){
            //sID = '';
            ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
            if (ctrCheck) {
                if (ctrCheck.is(":checked")){
                    ctrInput = $(this).find("input[id^='DTL_SO_TK_']");
                    if (ctrInput) {
                        SO_TK = ctrInput.prop("value");
                    }
                    ctrInput = $(this).find("input[id^='DTL_Ngay_TK_']");
                    if (ctrInput) {
                        NGAY_TK = ctrInput.prop("value");
                        if (!ValidDate(NGAY_TK)) {
                            arrDTL = [];
                            return false; //break each loop
                        }
                    }
                    ctrInput = $(this).find("input[id^='DTL_Lhxnk_']");
                    if (ctrInput) {
                        LH_XNK = ctrInput.prop("value");
                    }
                    ctrInput = $(this).find("input[id^='DTL_SAC_THUE_']");
                    if (ctrInput) {
                        SAC_THUE = ctrInput.prop("value");
                    }
                    ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                    if (ctrInput) {
                        Ma_TMuc = ctrInput.prop("value");
                    }
                    ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                    if (ctrInput) {
                        SoTien = accounting.unformat(ctrInput.prop("value"));
                    }
                    
                    ctrInput = $(this).find("input[id^='DTL_SoTienNT_']");
                    if (ctrInput) {
                        SoTien_NT = accounting.unformat(ctrInput.prop("value"),".");
                    }
                    
                    ctrInput = $(this).find("input[id^='DTL_Noi_Dung_']");
                    if (ctrInput) {
                        Noi_Dung = ctrInput.prop("value");
                    }
                    
                    ctrInput = $(this).find("input[id^='DTL_MA_LT_']");
                    if (ctrInput) {
                        MA_LT = ctrInput.prop("value");
                    }
                    ctrInput = $(this).find("input[id^='DTL_MA_CHUONG_']");
                    if (ctrInput) {
                        Ma_Chuong = ctrInput.prop("value");
                    }
                    ctrInput = $(this).find("input[id^='DTL_TT_BTOAN_']");
                    if (ctrInput) {
                        TT_BTOAN = ctrInput.prop("value");
                    }
                    
                    if (sLoaiTruyVan == "T") {  
                                  
                    } else if (sLoaiTruyVan == "P") {
                    
                    }
                    objDTL = new infDTL(TT_BTOAN , Ma_nkt , Ma_nkt_cha , Ma_ndkt , Ma_ndkt_cha , Ma_tlpc , Ma_khtk , Ma_nt , Loai_thue_hq , PT_TT , ID , SHKB , Ngay_KB, Ma_NV , So_BT , Ma_DThu , CCH_ID , Ma_Cap , Ma_Chuong , LKH_ID , Ma_Loai , Ma_Khoan , MTM_ID , Ma_Muc , Ma_TMuc , Noi_Dung , DT_ID , Ma_TLDT, Ky_Thue , SoTien , SoTien_NT , MaQuy , Ma_DP , SO_CT , SO_TK , NGAY_TK , LH_XNK , MA_HQ , LOAI_TT , SAC_THUE, MA_LT);
                    arrDTL.push(objDTL);
                } else {
                    ctrInput = null;
                }
            }
        });
        
        return arrDTL;
    }
    
    function jsThem_Moi() {
        jsEnableDisableControlButton("0");
        
        jsFillNguyenTe();
        jsFillMA_NH_A();
        jsFillPTTT();
        
        jsLoadCTList();
        
        //clear data
        $('#hdfSo_CTu').val('');
        $('#hdfLoaiCTu').val('');
        $('#hdfIsEdit').val(false);
        $('#hdfDescCTu').val('');

        $('#hdfHQResult').val('');
        $('#hdfHDRSelected').val('');
        $('#hdfDTLList').val('');
        $('#hdfDTLSelected').val('');
        
        jsBindKQTruyVanGrid(null);
        jsClearHDR_Panel();
        jsClearDTL_Grid()
        jsBindDTL_Grid(null, null);
        
        jsCalCharacter();
        jsEnableDisableControlButton("4");
        jsEnableDisableAddRowButton("1");
    }
    
    function jsGetCTU(sSoCT){
        jsShowDivStatus(false, "");
        jsShowHideDivProgress(true);
        jsShowHideKQTruyVanHQ(false);
        //clear data
        $('#hdfSo_CTu').val('');
        $('#hdfLoaiCTu').val('');
        $('#hdfIsEdit').val(false);
        $('#hdfDescCTu').val('');
        
        $('#hdfHQResult').val('');
        $('#hdfHDRSelected').val('');
        $('#hdfDTLList').val('');
        $('#hdfDTLSelected').val('');
        $('#hdfNgayKB').val('');
        
        jsBindKQTruyVanGrid(null);
        jsClearHDR_Panel();
        jsClearDTL_Grid()
        jsBindDTL_Grid(null, null);
        
        //NOT: null, undefined, 0, NaN, false, or ""
        if (sSoCT) { 
            $.ajax({
                    type: "POST",
                    url: "frmLapCTuPhi_HQ.aspx/GetCTU",
                    cache: false,
                    data: JSON.stringify({
                        "sSoCT": sSoCT
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: GetCtuHQ_OnSuccess,
                    error: function (request, status, error) {
                        jsShowDivStatus(true, request.responseText);
                        var err;
                        err = JSON.parse(request.responseText);
                        if (err) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            jsShowDivStatus(true, err.Message);
                        }
                        jsCalcTotalMoney();
                        jsCalCharacter();
                        jsShowHideDivProgress(false);
                    }
                });
        }
    }
    
    function GetCtuHQ_OnSuccess(response) {
        if(response != null) {
            //Load data
            if(typeof response !== 'undefined' && response !== null) {
                if (response.d) {
                    if (!jsCheckUserSession(response.d)) {
                        return;
                    }
                    var objCTu = JSON.parse(response.d);
                    if (objCTu) {
                        //Bind HDR panel
                        var HDR = objCTu.HDR;
                        if (typeof HDR !== 'undefined' && HDR !== null) {
                            //Bind hidden field
                            $('#hdfSo_CTu').val(HDR.So_CT);
                            $('#hdfLoaiCTu').val(HDR.LOAI_CTU);
                            curCtuStatus = HDR.Trang_Thai;
                            $('#hdfIsEdit').val(true);
                            var sDescCTu = "SHKB/SoCT/SoBT" + ":" + HDR.SHKB + "/" + HDR.So_CT + "/" + HDR.So_BT;
                            $('#hdfDescCTu').val(sDescCTu);
                            jsEnableDisableCtrlBtnByTThaiCTu(curCtuStatus, HDR.TT_CTHUE);
                        }
				        jsBindHDR_Panel(HDR);
                        //Bind dtl grid
                        var listDTL = objCTu.ListDTL;
                        jsBindDTL_Grid(HDR, listDTL);
                    }
                }
            }
         }
         jsCalcTotalMoney();
         //jsCalCharacter();
         jsConvertCurrToText();
         jsShowHideDivProgress(false);
         
    }
    
    function jsIn_CT(isBS)
    {
        var width  = screen.availWidth-100;
	 	var height = screen.availHeight-10;
		var left   = 0;
	 	var top    = 0;
	 	var params = 'width='+width+', height='+height;
	 	params += ', top='+top+', left='+left;
	 	params += ', directories=no';
	 	params += ', location=no';
	 	params += ', menubar=yes';
	 	params += ', resizable=no';
	 	params += ', scrollbars=yes';
	 	params += ', status=no';
	 	params += ', toolbar=no';
	 	
	 	var strSHKB, strSoCT, strSo_BT, MaNV, ngayLV;
	 	var descCTu = $('#hdfDescCTu').val();
	 	if (typeof descCTu !== 'undefined' && descCTu !== null) {
	 	    if (descCTu.indexOf(':') > -1) {
	 	        strSHKB = $('#hdfDescCTu').val().split(':')[1].split('/')[0];
                strSoCT = $('#hdfDescCTu').val().split(':')[1].split('/')[1];
                strSo_BT = $('#hdfDescCTu').val().split(':')[1].split('/')[2];
                MaNV = curMa_NV;
                ngayLV= $('#txtNGAY_HT').val();
	 	    }
	 	}
        //window.open ("../../pages/Baocao/frmInGNT.aspx?SoCT=" + strSoCT +"&SHKB=" + strSHKB + "&SoBT="+ strSo_BT +"&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=" + isBS ,"",params);                                  
        //InGNTpdf.ashx
	 	window.open("../../pages/Baocao/InGNTpdf.ashx?SoCT=" + strSoCT + "&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=" + isBS, "", params);
    }

    function jsLoadByCQT_Default() {
        $('#txtTenCQThu').val('');
        var strMaCQThu = $get('txtMaCQThu').value;
        var strMaHQ = $get('txtMaHQ').value;
        strMaHQ = ""; //ko lấy kèm Mã HQ
        if (strMaCQThu.length > 0) {
            PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, LoadByCQT_Default_Complete, LoadByCQT_Error);
        }
    }
    
    function LoadByCQT_Default_Complete(result, userContext, methodName) {
        jsCheckUserSession(result);
        if (result.lenght != 0) {
            var arr = result.split("|");
            if (arr[2]) {
                $get('txtTenCQThu').value = arr[2].toString();
                $('#txtMaHQ').val(arr[0]);
                $('#txtMaHQPH').val(arr[0]);
                $('#txtTenMaHQ').val(arr[2]);
                $('#txtTenMaHQPH').val(arr[2]);
            } else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin cơ quan thu với mã nhập vào';
            }
        }
    }
    

    function jsLoadByCQT() {
        $('#txtTenCQThu').val('');
        var strMaCQThu = $get('txtMaCQThu').value;
        var strMaHQ = $get('txtMaHQ').value;
        strMaHQ = ""; //ko lấy kèm Mã HQ
        if (strMaCQThu.length > 0) {
            PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, LoadByCQT_Complete, LoadByCQT_Error);
        }
    }

    function LoadByCQT_Complete(result, userContext, methodName) {
        jsCheckUserSession(result);
        if (result.lenght != 0) {
            var arr = result.split("|");
            if (arr[2]) {
                $get('txtTenCQThu').value = arr[2].toString();
                
            } else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin cơ quan thu với mã nhập vào';
            }
            if (arr[0]) {
                //lay ra ma HQ
                $('#txtMaHQ').val(arr[0].toString());
            } else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin hải quan với mã nhập vào';
            }
        }
    }
    function LoadByCQT_Error(error, userContext, methochedName) {
        if (error !== null) {
            document.getElementById('divStatus').innerHTML = 'Không lấy được dữ liệu theo cơ quan thu';
        }
    }
    
    function jsLoadByMaHQ(){
        var strMaCQThu = $get('txtMaCQThu').value;
        var strMaHQ = $get('txtMaHQ').value;

        if (strMaCQThu.length > 0) {
            PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, LoadByMaHQ_Complete, LoadByMaHQ_Error);
        }
    }
    function LoadByMaHQ_Complete(result, userContext, methodName){
        jsCheckUserSession(result);
        if (result.lenght != 0) {
            var arr = result.split("|");
            if (arr[1]) {
                $('#txtSHKB').val(arr[1].toString());
                jsGet_TenKB();
                jsGetTTNganHangNhan();
            } else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin SHKB với mã nhập vào';
            }
        }
    }
    function LoadByMaHQ_Error(error, userContext, methochedName){
        if (error !== null) {
            document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin SHKB với mã nhập vào';
        }
    }
    
    function jsGetTenQuanHuyen_NNThue(ma_xa){
        //var ma_xa = $('#txtHuyen').val();
        //var ma_xa1 = $get('txtHuyen_NNT').value
        if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== ''){
          PageMethods.GetTenQuanHuyen(ma_xa,GetTenQuanHuyenNNThue_Complete,GetTenQuanHuyenNNThue_Error);   
          jsGetTenTinh_NNThue(ma_xa);
        }else{
            document.getElementById('txtHuyen_NNT').value="";
            document.getElementById('txtTenHuyen_NNT').value="";
            document.getElementById('txtTinh_NNT').value="";
            document.getElementById('txtTenTinh_NNT').value="";
            jsCalCharacter();
        }
    }
    
    function GetTenQuanHuyenNNThue_Complete(result,methodName)
    {                
       if (result.length >0){ 
       var ten_Quan=result;
            document.getElementById('txtTenHuyen_NNT').value=ten_Quan;
       }else{
            alert('Mã Quận người nộp thuế không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtHuyen_NNT').focus();
            document.getElementById('txtHuyen_NNT').value="";
            document.getElementById('txtTenHuyen_NNT').value="";
            document.getElementById('txtTinh_NNT').value="";
            document.getElementById('txtTenTinh_NNT').value="";
       }
       jsCalCharacter();
    }
    
    function GetTenQuanHuyenNNThue_Error(error,userContext,methodName)
    {
        alert('Mã Quận người nộp thuế không chính xác. Vui lòng nhập lại.');
        document.getElementById('txtHuyen_NNT').focus();
        document.getElementById('txtHuyen_NNT').value="";
        document.getElementById('txtTenHuyen_NNT').value="";
        document.getElementById('txtTinh_NNT').value="";
        document.getElementById('txtTenTinh_NNT').value="";
        jsCalCharacter();
    }
    
    function jsGetTenTinh_NNThue(ma_xa){
        //var ma_xa = $('#txtHuyen_NNT').val();
        $('#txtTinh_NNT').val('');
        $('#txtTenTinh_NNT').val('');
        if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
            PageMethods.GetTenTinh(ma_xa,GetTenTinhNNThue_Complete,GetTenTinhNNThue_Error);   
        }
    }
    
    function GetTenTinhNNThue_Complete(result,methodName)
    {                
       if (result.length > 0){ 
       var ten_Tinh=result.split(';');
            document.getElementById('txtTinh_NNT').value=ten_Tinh[0];
            document.getElementById('txtTenTinh_NNT').value=ten_Tinh[1];
       }
       jsCalCharacter();
    }
    
    function GetTenTinhNNThue_Error(error,userContext,methodName)
    {
       jsCalCharacter();
    }
    
    function jsGetTenQuanHuyen_NNTien(ma_xa){
        if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== ''){
          PageMethods.GetTenQuanHuyen(ma_xa,GetTenQuanHuyenNNTien_Complete,GetTenQuanHuyenNNTien_Error);  
          jsGetTenTinh_NNTien(ma_xa);
        }else{
            document.getElementById('txtQuan_HuyenNNTien').value="";
            document.getElementById('txtTenQuan_HuyenNNTien').value="";
            document.getElementById('txtTinh_NNTien').value="";
            document.getElementById('txtTenTinh_NNTien').value="";
        }
    }
    
    function GetTenQuanHuyenNNTien_Complete(result,methodName)
    {                
       if (result.length >0){ 
       var ten_Quan=result;
            document.getElementById('txtTenQuan_HuyenNNTien').value=ten_Quan;
       }else{
            alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtQuan_HuyenNNTien').focus();
            document.getElementById('txtQuan_HuyenNNTien').value="";
            document.getElementById('txtTenQuan_HuyenNNTien').value="";
            document.getElementById('txtTinh_NNTien').value="";
            document.getElementById('txtTenTinh_NNTien').value="";
       }
    }
    
    function GetTenQuanHuyenNNTien_Error(error,userContext,methodName)
    {
            alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtQuan_HuyenNNTien').focus();
            document.getElementById('txtQuan_HuyenNNTien').value="";
            document.getElementById('txtTenQuan_HuyenNNTien').value="";
            document.getElementById('txtTinh_NNTien').value="";
            document.getElementById('txtTenTinh_NNTien').value="";
    }
    
    function jsGetTenTinh_NNTien(ma_xa){
        //var ma_xa = $('#txtHuyen_NNT').val();
        $('#txtTinh_NNTien').val('');
        $('#txtTenTinh_NNTien').val('');
        if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
            PageMethods.GetTenTinh(ma_xa,GetTenTinhNNTien_Complete,GetTenTinhNNTien_Error);   
        }
    }
    
    function GetTenTinhNNTien_Complete(result,methodName)
    {                
       if (result.length > 0){ 
       var ten_Tinh=result.split(';');
            document.getElementById('txtTinh_NNTien').value=ten_Tinh[0];
            document.getElementById('txtTenTinh_NNTien').value=ten_Tinh[1];
       }
       jsCalCharacter();
    }
    function GetTenTinhNNTien_Error(error,userContext,methodName)
    {
       jsCalCharacter();
    }
    
    function jsGet_TenKB() 
    {
        $('#txtTenKB').val('');
        if (arrSHKB.length>0)
        {
            for (var i=0;i<arrSHKB.length;i++)
            {
                var arr = arrSHKB[i].split(';');
                if (arr[0] == $('#txtSHKB').val()){
                    $('#txtTenKB').val(arr[1])
                    break;
                }
            }
        }
        jsCalCharacter();
    }
    
    function jsGet_TenDBHC()
    { 
        $('#txtTenDBHC').val('');
        if (typeof arrDBHC !== 'undefined' && arrDBHC !== null)
        {
            if ((arrDBHC instanceof Array) && (arrDBHC.length > 0)) {
                for (var i=0;i<arrDBHC.length;i++)
                {
                    var arr = arrDBHC[i].split(';');
                    if (arr[0] == $('#txtMaDBHC').val()){
                        $('#txtTenDBHC').val(arr[1]);
                        break;
                    }
                }
            }
        }
    }
    
    function jsGet_TenCQThu()
    {
        $('#txtTenCQThu').val('');
        if (arrCQThu.length>0)
        {
            for (var i=0;i<arrCQThu.length;i++)
            {
                var arr = arrCQThu[i].split(';');
                if (arr[1] == $('#txtMaCQThu').val()){
                    $('#txtTenCQThu').val(arr[2]);
                    break;
                }
            }
        }
    }
    
  function jsGet_TenTKGL()
        {         
           
            if (arrTKGL.length>0)
            {
                for (var i=0;i<arrTKGL.length;i++)
                {
                    
                    var arr = arrTKGL[i].split(';');
                    
                    
                    if (arr[0] == $('#txtTKGL').val()){
                        $('#txtTenTKGL').val(arr[1]);
                        break;
                    }
                }            
            }
        }
        
    function jsFillNguyenTe()
    {
        var cb = document.getElementById('ddlMaNT');
        cb.options.length = 0;
        
        if (arrMaNT.length > 0) {
            for (var i=0; i<arrMaNT.length; i++) {
                var arr = arrMaNT[i].split(';');                        
                var value = arr[0];
                var label = arr[1];            
                cb.options[cb.options.length] = new Option(label, value);
            }
        }
    }
    
    function jsShowTyGia() {
        $('#txtTyGia').val('');
        if (arrTyGia.length > 0) {
            for (var i=0; i<arrTyGia.length; i++) {
                var arr = arrTyGia[i].split(';');                        
                var value = arr[0];
                var label = arr[1];
                if (value == $('#ddlMaNT').val()) 
                {    
                    $('#txtTyGia').val(label);
                    break;
                }
            }
        }
    }
    
    function jsGetTen_TKNo()
    {           
    }
    
    function jsGetTen_TKCo()
    {
    }
    
    function jsGet_TenLHSX()
    {       
        $('#txtDescLHXNK').val('');
        if (arrLHSX.length > 0)
        {
            for (var i=0;i<arrLHSX.length;i++)
            {
                var arr = arrLHSX[i].split(';');
                if (arr[0] == $get('txtLHXNK').value.trim()){
                    $get('txtDescLHXNK').value = arr[1];
                    break;
                }
            }
        }
    }
    
    function jsGet_TenNH_B()
    {   
        //$('#txtTenMA_NH_B').val('');
        if (arrTT_NH_B.length>0)
        {
            for (var i=0;i<arrTT_NH_B.length;i++)
            {
                var arr = arrTT_NH_B[i].split(';');
                if (arr[0] == $get('txtMA_NH_B').value.trim()){
                    $('#txtTenMA_NH_B').val(arr[1]);
                    
                    break;
                }
            }
        }
    }
    
    function jsGet_TenNH_TT()
    {       
        //$get('txtTEN_NH_TT').value ='';
        if (arrTT_NH_TT.length>0)
        {
            for (var i=0;i<arrTT_NH_TT.length;i++)
            {
                var arr = arrTT_NH_TT[i].split(';');
                if (arr[0] == $get('txtMA_NH_TT').value.trim()){
                    $get('txtTEN_NH_TT').value = arr[1];
                    break;
                }
            }
        }
    }
    
    function jsFillMA_NH_A()
    {
        var cb = document.getElementById('ddlMA_NH_A');
        cb.options.length =0;

        if (arrMA_NHA.length > 0) {
            for (var i=0; i<arrMA_NHA.length; i++) {
                var arr = arrMA_NHA[i].split(';');                        
                var value = arr[0];
                var label = arr[1];            
                cb.options[cb.options.length] = new Option(label, value);
            }
        }
    }
    
    function jsFillPTTT()
    {
    }
    
    function ShowLov(strType)
    {
        if (strType=="NNT") return FindNNT_NEW('txtMaNNT', 'txtTenNNT');         
        if (strType=="SHKB") return FindDanhMuc('KhoBac','txtSHKB', 'txtTenKB','txtSHKB');                 
        if (strType=="DBHC") return FindDanhMuc('DBHC','txtMaDBHC', 'txtTenDBHC','txtMaCQThu'); 
        if (strType=="CQT") return FindDanhMuc('CQThu_HaiQuan','txtMaCQThu','txtTenCQThu','txtMTkNo');
        if (strType == "LHXNK") return FindDanhMuc('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSHKB');
        if (strType == "LHXNK") return FindDanhMuc('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSHKB');
        if (strType == "MHQ") return FindDanhMuc('Ma_HaiQuan', 'txtMaHQPH', 'txtTenMaHQPH', 'txtTenMaHQPH');
        if (strType == "MHQPH") return FindDanhMuc('Ma_HaiQuan', 'txtMaHQPH', 'txtTenMaHQPH', 'txtTenMaHQPH'); 
//        if (strType=="DMNH_TT") return FindDanhMuc('DMNH_TT','txtMA_NH_TT','txtTEN_NH_TT','txtTEN_NH_TT');
        if (strType=="DMNH_GT") return FindDanhMuc('DMNH_GT','txtMA_NH_B','txtTenMA_NH_B','txtTenMA_NH_B');
//        if (strType=="DBHC_NNT") return FindDanhMuc('DBHC_NNT','txtHuyen_NNT', 'txtTenHuyen_NNT','txtHuyen_NNT');
        if (strType=="DBHC_NNTHUE") return FindDanhMuc('DBHC_NNTHUE','txtQuan_HuyenNNTien', 'txtTenQuan_HuyenNNTien','txtQuan_HuyenNNTien');
    }
    
    function FindNNT_NEW(txtID, txtTitle) {
        var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?Src=TK&initParam=" + $get('txtMaNNT').value + "&SHKB=" + $get('txtSHKB').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
    }
    
    function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) 
    {        
        var strSHKB;
         var returnValue;
        if (document.getElementById('txtSHKB').value.length>0)
        {
            strSHKB=document.getElementById('txtSHKB').value;
        }
        else
        {
            strSHKB= defSHKB.split(';')[0];
        }
            
         returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&SHKB=" +strSHKB +"&initParam=" + $get(txtID).value,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                if (txtTitle!=null){
                    document.getElementById(txtTitle).value = returnValue.Title;
                    //fire change event if 
                    //$('#' + txtID).change();
                }
                if (txtFocus != null) {
                    //if ($get('txtFocus').getAttribute('disabled') != 'disabled') {
                        try {
                        document.getElementById(txtFocus).focus();
                        }
                    catch (e) {
                        
                        };
                }
        }
    }

    function jsSHKB_lostFocus(strSHKB)
    {
        jsLoadDM('KHCT',strSHKB );
        jsLoadDM('SHKB',strSHKB);
        //jsLoadDM('DBHC',strSHKB);
        jsLoadDM('CQTHU',strSHKB);
        jsLoadDM('CQTHU_DEF',strSHKB);
        jsLoadDM('DBHC_DEF',strSHKB);
        jsLoadDM('TAIKHOANNO',strSHKB);
        jsLoadDM('TAIKHOANCO',strSHKB);
        jsLoadDM('CQTHU_SHKB',strSHKB);
        jsLoadDM('TINH_SHKB',strSHKB);
     }
     
    function jsLoadDM(strLoaiDM,strSHKB)
    {
        if (strSHKB.length==0)
        {
            strSHKB=document.getElementById('txtSHKB').value;
        }
        if (strSHKB.length>0)
        {
            PageMethods.GetData_SHKB(strLoaiDM,strSHKB,LoadDM_Complete,LoadDM_Error); 
        }          
        else
        {
        }
    }
    
    function LoadDM_Complete(result,userContext,methodName)
    {                 
       jsCheckUserSession(result);
       var mangKQ = new Array;
       mangKQ = result.toString().split('|');
       
       if (mangKQ[0]=='TAIKHOANCO')
       {                      
           arrTKCoNSNN = new Array(mangKQ.length-2);
           for(i=1;i<mangKQ.length-1;i++){
                var strTemp = mangKQ[i].toString();
                arrTKCoNSNN[i-1] = strTemp;
           }
           
       }
       else if (mangKQ[0]=='CQTHU')
       {
           arrCQThu = new Array(mangKQ.length-2);
           for(i=1;i<mangKQ.length-1;i++){
                var strTemp = mangKQ[i].toString();
                arrCQThu[i-1] = strTemp;
                }
       }else if (mangKQ[0]=='CQTHU_SHKB')
       {
           arrCQThu_SHKB = new Array(mangKQ.length-2);
           for(i=1;i<mangKQ.length-1;i++){
                var strTemp = mangKQ[i].toString();
                arrCQThu_SHKB[i-1] = strTemp;
                }
       }
       else if (mangKQ[0]=='DBHC_DEF')
        {
           if (mangKQ[1].length!=0)
           {            
                document.getElementById('txtMaDBHC').value=mangKQ[1].split(';')[0];
                document.getElementById('txtTenDBHC').value=mangKQ[1].split(';')[1];
           }
           else
           {
                document.getElementById('txtMaDBHC').value='';
                document.getElementById('txtTenDBHC').value='';
           }
           
        }
    }
    
    function LoadDM_Error(error,userContext,methochedName)
    {
        if(error !== null) 
        {
            document.getElementById('divStatus').innerHTML='Không lấy được dữ liệu liên quan đến kho bạc này';
        }
    }
    
    function jsGetTTNganHangNhan()
    {
        try {
            var strSHKB = $('#txtSHKB').val();//$get('txtSHKB').value;
            var strDBHC='';//$get('txtMaDBHC').value;
            var strCQThu='';//$get('txtMaCQThu').value;            	
            PageMethods.GetTT_NH(strSHKB,GetTT_NganHangNhan_Complete,GetTT_NganHangNhan_Error);
        } catch (ex) {
            alert(ex.message);
        }
    }
    
    function GetTT_NganHangNhan_Complete(result,methodName)
    {   
        try {
            jsCheckUserSession(result);        
            if (result.length > 0){        
               var arr2 = result.split(';'); 
                $('#txtMA_NH_B').val(arr2[0]);
                $('#txtTenMA_NH_B').val(arr2[1]);
                
            }   
            else{
                $('#txtMA_NH_B').val('');
                $('#txtTenMA_NH_B').val('');
                
                document.getElementById('divStatus').innerHTML='Không tìm thấy thông tin ngân hàng nhận.Hãy kiểm tra lại'; 
            }    
            jsCalCharacter();
        } catch (ex) {
            alert(ex.message);
        } 
    }
    
    function GetTT_NganHangNhan_Error(error,userContext,methodName)
    {
        try {
            $('#txtMA_NH_B').val('');
            $('#txtTenMA_NH_B').val('');
            
            
            if(error !== null) 
            {
                document.getElementById('divStatus').innerHTML="Lỗi trong quá trình lấy thông tin ngân hàng nhận" + error.get_message();
            }
            jsCalCharacter();
        } catch (ex) {
            alert(ex.message);
        }
    }
    
    function jsGetTK_ddlDSTKKH(){
        jsGetTK_KH_NH();
    }
    
    function jsGetTK_KH_NH()
    {
       // mask($get('txtTK_KH_NH').value, $get('txtTK_KH_NH'), '3,6,9,17', '-');
        if ($get('txtTK_KH_NH').value.length > 0) {
            if ($get('txtTK_KH_NH').value.length >0 )
            {
                document.getElementById('divStatus').innerHTML = '';
                var strAccType = '';
                var intPP_TT = document.getElementById("ddlPT_TT").selectedIndex;
                if (intPP_TT == 1) {
                    strAccType = 'TG';
                } else {
                    strAccType = 'CK';
                }
                
                jsShowHideDivProgress(true);            
                PageMethods.GetTK_KH_NH($get('txtTK_KH_NH').value.trim(), GetTK_NH_Complete, GetTK_NH_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Định dạng tài khoản khách hàng không hợp lệ.Hãy nhập lại';
            } 
        }
    }
    
     function checkSS(param){
    
        var arrKQ = param.split (';');
            if(arrKQ[0] == "ssF"){
                window.open("../../pages/Warning.html","_self");
                return;
            }
        }
    
    function GetTK_NH_Complete(result,methodName)
    {       
        checkSS(result);
        checkSS(result);
        jsShowHideDivProgress(false);  
   
		if (result.length > 0) 
		{
			var tk_kh_nh=JSON.parse(result);
			        
			if (tk_kh_nh!=null)
			{   
                if (tk_kh_nh.ErrorCode=="0")
                {
                    $get('txtSoDu_KH_NH').value = tk_kh_nh.Balance;

                    jsFormatNumber('txtSoDu_KH_NH') ; 
                    var strTenKH="";
                    
                    strTenKH =tk_kh_nh.DescAccount;               

                    $get('txtTenTK_KH_NH').value = strTenKH; 
                    
//                    if($get('txtSoDu_KH_NH').value!=0)
//                        $get('txtDesc_SoDu_KH_NH').value =tk_kh_nh.Balance;
//                    else
//                        $get('txtDesc_SoDu_KH_NH').value ="0";

                }else{
                    document.getElementById('divStatus').innerHTML = 'KH không có TK tại MSB.';
                    $get('txtTenTK_KH_NH').value='';
                    $get('txtSoDu_KH_NH').value='';
                }
            }
		}
   }
    
    function GetTK_NH_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
        $get('txtTenTK_KH_NH').value='';
        $get('txtSoDu_KH_NH').value='';
        document.getElementById('divStatus').innerHTML="Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
        }
    }
    
    
    function changeNT()
    {
        maTienTe = $("#ddlMaNT option:selected").val();
        $("#txtTenNT").val(maTienTe);
        
        var arr = arrTyGia;
        var toFix = 0;
        var tyGia = 1;
        for (var i=0; i<arrTyGia.length; i++) {
            var arr = arrTyGia[i].split(';');                        
            var value = arr[0];
            tyGia = arr[1];
            toFix = arr[2];
            if (value==maTienTe) 
            {
                $('#txtTyGia').val(accounting.formatNumber(tyGia));
                break;
            }
        }
        
        if (maTienTe!='VND')
        {
            $('#grdChiTiet > tbody  > tr').each(function() {
                var ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                var ctrInput2 = $(this).find("input[id^='DTL_SoTienNT_']");
                if(ctrInput){
                    var tienVnd = accounting.unformat(ctrInput.prop("value"));
                    if(tienVnd >0)
                    {
                        var tienNT = (tienVnd/tyGia).toFixed(toFix);
                        ctrInput2.val(accounting.formatNumber(tienNT,toFix,",","."));
                    }else{
                        ctrInput.val("0");
                        ctrInput2.val("0");
                    }
                }
            });
        }
        else 
        {
            $('#grdChiTiet > tbody  > tr').each(function() {
                var ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                var ctrInput2 = $(this).find("input[id^='DTL_SoTienNT_']");
                
                if(ctrInput){
                    ctrInput2.val(ctrInput.val());
                }else{
                    ctrInput.val("0");
                    ctrInput2.val("0");
                }
            });
        }
        
        jsCalCharacter();
        jsCalcTotalMoney();
    }
    
    function jsShowCustomerSignature()
    {        
        if  ($get('txtTenTK_KH_NH').value !='' )
        {
            window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $get('txtTK_KH_NH').value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        }
        else{
             alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');}
    }
    
    function ValidNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    
    function ValidInteger(obj) {
        //alert(obj.value);
        var i, strVal, blnChange;
        blnChange = false
        strVal = "";

        for (i = 0; i < (obj.value).length; i++) {
            switch (obj.value.charAt(i)) {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": strVal = strVal + obj.value.charAt(i);
                    break;
                default: blnChange = true;
                    break;
            }
        }
        if (blnChange) {
            obj.value = strVal;
        }
    }
    
    function jsConvertCurrToText() {
        var vTongTien = accounting.unformat($('#txtTongTrichNo').val());
        document.getElementById('divBangChu').innerHTML = 'Bằng chữ : ' + So_chu(vTongTien);
    }
    
    String.prototype.replaceAll = function(strTarget,strSubString){
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    }
    
    function jsFormatNumber(control){
        var number = accounting.unformat(control.value);
        control.value = accounting.formatNumber(number);
    }
    
    function jsFormatNumber2(control){
        var number = accounting.unformat(control.value,".");
        if($("#ddlMaNT option:selected").val() != "VND")
            control.value = accounting.formatNumber(number,2,",",".");
        else
            control.value = accounting.formatNumber(number);
    }
    
    function onChangePTTT(){
        var pttt = $("#ddlPT_TT option:selected").val();
        
        if(pttt == "03"){
            $("#rowSoDuNH").css("display","none");
            $("#rowTK_KH_NH").css("display","none");
            $("#rowTKGL").css("display","");
        }else if(pttt == "01"){
            $("#rowSoDuNH").css("display","");
            $("#rowTK_KH_NH").css("display","");
            $("#rowTKGL").css("display","none");
        }
        
    }
    
    function onBlurMaTieuMuc(id){
        if($('#DTL_Ma_TMuc_' + id).val().length > 0){
            $.ajax({
                    type: "POST",
                    url: "frmLapCTuPhi_HQ.aspx/onBlurMaTieuMuc",
                    cache: false,
                    data: JSON.stringify({
                        "v_maTieuMuc": $('#DTL_Ma_TMuc_' + id).val()
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(reponse) {
                        
                        if(reponse.length = 0){
                            alert("Có lỗi khi Tìm kiếm Sắc thuế, Nội dung theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                            
                            $('#DTL_SAC_THUE_' + id).val('');
                            $('#DTL_Noi_Dung_' + id).val('');
                        }else{
                            var result = reponse;
                            var jsonData = JSON.parse(result.d);
                            
                            if(jsonData.length == 1){
                                if(jsonData[0].STT == 1){
                                    alert("Có lỗi khi Tìm kiếm Nội dung theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                                    $('#DTL_Noi_Dung_' + id).val('');
                                    $('#DTL_SAC_THUE_' + id).val(jsonData[0].VALUE);
                                }else if(jsonData[0].STT == 2){
                                    alert("Có lỗi khi Tìm kiếm Sắc thuế theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                                    $('#DTL_Noi_Dung_' + id).val(jsonData[0].VALUE);
                                    $('#DTL_SAC_THUE_' + id).val('');
                                }
                            }else{
                                for(var i = 0;i<jsonData.length; i++){
                                    if(jsonData[i].STT == 1)
                                        $('#DTL_SAC_THUE_' + id).val(jsonData[i].VALUE);
                                    else if(jsonData[i].STT == 2)
                                        $('#DTL_Noi_Dung_' + id).val(jsonData[i].VALUE);
                                }
                            }
                        }
                    
                    },error: function (request, status, error) {
                        alert("Có lỗi khi Tìm kiếm Sắc thuế, Nội dung theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                    }
                });
        }else{
            $('#DTL_SAC_THUE_' + id).val('');
            $('#DTL_Noi_Dung_' + id).val('');
        }
    }
    
    function getNoiDungByMaTieuMuc(){
        var arrTieuMuc = {};
        var sTieuMuc = "";
        $('#grdChiTiet > tbody  > tr').each(function() {
            var ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
            var ctrInput2 = $(this).find("input[id^='DTL_Noi_Dung_']");
            if(ctrInput){
                var maTieuMuc = ctrInput.val();
                sTieuMuc += "'" + maTieuMuc +"',";
                ctrInput2.val('');
            }
        });
        
        sTieuMuc = sTieuMuc.substring(0,sTieuMuc.length-1);
        
        if(arrTieuMuc){
            $.ajax({
                type: "POST",
                url: "frmLapCTu_HQ.aspx/getNoiDungByMaTieuMuc",
                cache: false,
                data: JSON.stringify({
                        "v_jSon": sTieuMuc
                    }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(reponse) {
                    if(reponse.d.length == 0){
                        $('#grdChiTiet > tbody  > tr').each(function() {
                            var ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                            var ctrInput2 = $(this).find("input[id^='DTL_Noi_Dung_']");
                            if(ctrInput){
                                ctrInput2.val("");
                            }
                        });
                    }else{
                        var result = reponse;
                        var jsonData = JSON.parse(result.d);
                        
                        for(var i = 0;i<jsonData.length; i++){
                             $('#grdChiTiet > tbody  > tr').each(function() {
                                var ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                                var ctrInput2 = $(this).find("input[id^='DTL_Noi_Dung_']");
                                
                                if(ctrInput.val() == jsonData[i].MA_TMUC){
                                    ctrInput2.val(jsonData[i].TEN);
                                }
                            });
                        }
                    }
                
                }
            });
        }
    }
    
    function jsGenThuTuButToan(){
        var arrSoTk = [];
        
        $('#grdChiTiet > tbody  > tr').each(function() {
            var ctrSoTk = $(this).find("input[id^='DTL_SO_TK_']");
            if (typeof ctrSoTk !== 'undefined' && ctrSoTk !== null ){
                var objSoTk = {};
                objSoTk["SO_TK"] = ctrSoTk.val();
                objSoTk["TT_BTOAN"] = "";
                //Kiem tra xem objGoc co gia tri chua
                if(arrSoTk.length >0){
                    var flag = true;
                    
                    for(var j=0; j< arrSoTk.length;j++){
                        var objDtl = arrSoTk[j];
                        if(objSoTk.SO_TK != arrSoTk[j].SO_TK)
                            flag = true;
                        else{
                            flag = false;
                            break;
                        }
                    }
                    if(flag)  
                        arrSoTk.push(objSoTk);
                }else
                    arrSoTk.push(objSoTk);
            }
        });
        
        
        
        for(var i=0; i<arrSoTk.length;i++){
            var objDtl = arrSoTk[i];
            objDtl.TT_BTOAN = i;
        }
                
        $('#grdChiTiet > tbody  > tr').each(function() {
            var ctrSoTk = $(this).find("input[id^='DTL_SO_TK_']");
            var ctrTtBToan = $(this).find("input[id^='DTL_TT_BTOAN_']");
            if (typeof ctrSoTk !== 'undefined' && ctrSoTk !== null ){
                 for(var jj=0; jj<arrSoTk.length;jj++){
                    var objDtl = arrSoTk[jj];
                    if(ctrSoTk.val()==objDtl.SO_TK){
                        ctrTtBToan.val(objDtl.TT_BTOAN);
                        break;
                    }
                }
            }
        });    
    }
    
    function jsCalcCurrency(id)
    {
        var tienVnd = accounting.unformat($("#DTL_SoTien_" + id).val());
        
        var arr = arrTyGia;
        var toFix = 0;
        var tyGia = 1;
        for (var i=0; i<arrTyGia.length; i++) {
            var arr = arrTyGia[i].split(';');                        
            var value = arr[0];
            tyGia = arr[1];
            if (value==$("#ddlMaNT option:selected").val()) 
            {
                toFix = arr[2];
                break;
            }
        }
        
        var tienNT = (tienVnd/tyGia).toFixed(toFix);
        if($("#ddlMaNT option:selected").val() != "VND")
            $("#DTL_SoTienNT_" + id).val(accounting.formatNumber(tienNT,toFix,",","."));
        else
            $("#DTL_SoTienNT_" + id).val(accounting.formatNumber(tienNT));
            
        jsCalcTotalMoney();
        jsConvertCurrToText();
    }
    
    
    function jsGetTenTinh_KB(ma_xa){
        $('#txtMaDBHC').val('');
        $('#txtTenDBHC').val('');
        if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
            PageMethods.GetTenTinh(ma_xa,GetTenTinh_KB_Complete,GetTenTinh_KB_Error);   
        }
    }
    
    function GetTenTinh_KB_Complete(result,methodName)
    {                
       if (result.length > 0){ 
       var ten_Tinh=result.split(';');
            document.getElementById('txtMaDBHC').value=ten_Tinh[0];
            document.getElementById('txtTenDBHC').value=ten_Tinh[1];
       }
    }
    
    function jsCalCharge()
    {
            
        if( $('#txtCharge').val()=="")
            $('#txtCharge').val("0");
        var dblPhi = parseFloat(accounting.unformat($('#txtCharge').val()));
        var dblVAT = parseInt(dblPhi/10);
                       
        var dblTongTien = parseFloat(accounting.unformat($('#txtTongTien').val()));
        var dblTongTrichNo = dblPhi + dblVAT + dblTongTien;
        
        $('#txtCharge').val(accounting.formatNumber(dblPhi));
        $('#txtVAT').val(accounting.formatNumber(dblVAT));
        $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo));
        //jsFormatNumber($get('txtVAT').value); 
        //jsFormatNumber($get('txtTongTrichNo').value);  
        //jsFormatNumber($get('txtCharge').value);
        
        jsConvertCurrToText();
    }
        
    function jsEnabledInputCharge()
    {
        var dblTongTien = parseFloat(accounting.unformat($('#txtTongTien').val()));         
        var dblPhi = 0;
        var dblVAT = 0;                      
        var dblTongTrichNo = 0;
        if (document.getElementById('chkChargeType').checked)
        {
            
            $('#txtCharge').disabled  = true;
            //neu ma checked thi tinh lai fee
            dblPhi = dblTongTien * feeRate/minFee;  
            dblVAT = parseInt(dblTongTien * feeRate/minFee/10);
            if (dblPhi<minFee) 
            {
                dblPhi=minFee;
                //dblVAT=dblPhi/10;
                dblVAT = parseInt(dblPhi/10);
                
            }
            dblTongTrichNo = dblPhi + dblVAT + dblTongTien; 
            $('#txtCharge').val(accounting.formatNumber(dblPhi));                                        
            $('#txtVAT').val(accounting.formatNumber(dblVAT));                            
            //$('#txtTongTien').val(accounting.formatNumber(dblTongTien));                    
                        
            $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo));
                                 
        }else{
            $('#txtCharge').prop('disabled',false);
            $('#txtCharge').val("0");                                        
            $('#txtVAT').val("0");
            $('#txtTongTrichNo').val($('#txtTongTien').val());
        }    
        jsConvertCurrToText();
    }
    
    function valInteger(obj) {
        var i, strVal, blnChange;
        blnChange = false
        strVal = "";

        for (i = 0; i < (obj.value).length; i++) {
            switch (obj.value.charAt(i)) {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": strVal = strVal + obj.value.charAt(i);
                    break;
                default: blnChange = true;
                    break;
            }
        }
        if (blnChange) {
            obj.value = strVal;
        }
    }
    
    function GetTenTinh_KB_Error(error,userContext,methodName)
    {
    }
    
    function jsCalTotal()
    {        
        var dblTongTien = 0;        
        var dblPhi = 0;
        var dblVAT = 0;                      
        var dblTongTrichNo = 0;
       
                if (!document.getElementById("chkRemove1").checked)
                {
                    $get('SOTIEN_1').value="";
                }
                if (!document.getElementById("chkRemove2").checked)
                { 
                    $get('SOTIEN_2').value="";
                }
                if (!document.getElementById("chkRemove3").checked)
                {
                    $get('SOTIEN_3').value="";
                }
                if (!document.getElementById("chkRemove4").checked)
                {
                    $get('SOTIEN_4').value="";
                }
                
                if ($get('SOTIEN_1').value.length > 0) {            
                    dblTongTien += parseFloat ($get('SOTIEN_1').value.replaceAll('.',''));    
                    jsFormatNumber('SOTIEN_1');
                }    
                if ($get('SOTIEN_2').value.length > 0) {
                    dblTongTien += parseFloat ($get('SOTIEN_2').value.replaceAll('.',''));
                    jsFormatNumber('SOTIEN_2');
                }
                if ($get('SOTIEN_3').value.length > 0){
                    dblTongTien += parseFloat ($get('SOTIEN_3').value.replaceAll('.',''));
                    jsFormatNumber('SOTIEN_3');
                }
                 
                if ($get('SOTIEN_4').value.length > 0) {        
                    dblTongTien += parseFloat ($get('SOTIEN_4').value.replaceAll('.',''));
                    jsFormatNumber('SOTIEN_4');
                }
                if (document.getElementById("ddlMaHTTT").value=="01")
                { 
                    if (document.getElementById('chkChargeType').checked)
                    {
                        dblPhi =(dblTongTien * feeRate)/minFee; 
                        dblVAT=  (dblTongTien * feeRate)/minFee/10;  
                         if (dblPhi<minFee) 
                        {
                            dblPhi=minFee;
                            dblVAT=dblPhi/10;
                        }          
                    }else
                    {
                        dblPhi = parseFloat ($get('txtCharge').value.replaceAll('.',''));  
                        dblVAT= dblPhi/10;          
                    }
                   
                   
                    $get('txtVAT').value = dblVAT.toString().replace('.',',');
                    $get('txtTongTien').value = dblTongTien;
                    dblTongTrichNo = dblPhi + dblVAT + dblTongTien;
                    $get('txtTongTrichNo').value = dblTongTrichNo.toString().replace('.',',');
                    dblPhi=dblPhi.toString().replaceAll('.',',')
                    localeNumberFormat(dblPhi,'.');  
                    $get('txtCharge').value = dblPhi;
                    jsFormatNumber('txtTongTien');  
                    jsFormatNumber('txtCharge');
                    jsFormatNumber('txtVAT');
                    jsFormatNumber('txtTongTrichNo');  
            }
            else
            {
                
                $get('txtCharge').value = 0;
                $get('txtVAT').value = 0;
                $get('txtTongTien').value = dblTongTien;
                dblTongTrichNo =dblTongTien;
                $get('txtTongTrichNo').value = dblTongTrichNo.toString().replace('.',',');
                jsFormatNumber('txtTongTien');  
                jsFormatNumber('txtCharge');
                jsFormatNumber('txtVAT');
                jsFormatNumber('txtTongTrichNo'); 
                
            }
        //NextFocus ();      
    }      
 
    
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <input type="hidden" id="hdfSoBT" value="" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top">
            <!--DANH SACH CHUNG TU-->
            <td valign="top" width="20%">
                <asp:Panel ID="pnlDSCT" runat="server" Height="450px" Width="98%" ScrollBars="Vertical">
                    <table class="grid_data" border="0" cellpadding="0" cellspacing="0" width="100%"
                        style="padding: 10px 0;display:none;">
                        <tr>
                            <td colspan="2">
                                <input type="text" id="txtTraCuuSoCtu" value="" style="width: 110px;" />
                            </td>
                            <td style="text-align: center;">
                                <input type="button" id="btnTraCuuSoCtu" value="Tra cứu" onclick="jsLoadCTList()"
                                    style="width: 70px;" />
                            </td>
                        </tr>
                    </table>
                    <table class="grid_data" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class='grid_header'>
                            <td style="width: 10%">
                                TT
                            </td>
                            <td style="width: 25%">
                                Số CT
                            </td>
                            <td style="width: 65%">
                                User lập/Số BT
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div id='divDSCT'>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuaKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Chưa Kiểm soát
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuyenKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Chờ Kiểm soát
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/DaKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Đã Kiểm soát
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuaNhapBDS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Điều chỉnh chờ kiểm soát
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/KSLoi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Chuyển trả
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            CT bị Hủy bởi GDV
                        </td>
                    </tr>
                    <%--<tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/HuyHS.gif" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            CT Hủy bởi GDV - chờ duyệt
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy_CT_Loi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Duyệt hủy chuyển thuế lỗi
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy_KS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            CT bị Hủy bởi KSV
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/TT_CThueLoi.gif" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Chuyển thuế/HQ lỗi
                        </td>
                    </tr>
                    <%--<tr style="display: none">
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy_CT_Loi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">
                            Hủy chuyển thuế/HQ lỗi
                        </td>
                    </tr>--%>
                    <tr class="img">
                        <td align="left" style="width: 100%" colspan="3">
                            <hr style="width: 50%" />
                        </td>
                    </tr>
                </table>
                Alt + M : Thêm mới<br />
                <%--Alt + G : Ghi<br />--%>
                Alt + K : Ghi và chuyển KS<br />
                <%-- Alt + C : Chuyển KS<br />--%>
                Alt + I : In chứng từ<br />
                Alt + H : Hủy chứng từ<br />
            </td>
            <!--THONG TIN CHUNG TU-->
            <td valign="top" width="70%">
                <!--TRUY VAN HQ-->
                <div style="width: 100%; margin: 0; padding: 0;">
                    <table width="100%" cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px;
                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                        <tr class="grid_header">
                            <td align="left" colspan="4">
                                TRUY VẤN THUẾ HẢI QUAN
                            </td>
                        </tr>
                        <tr align="left">
                            <td>
                                <b>Mã số thuế</b>&nbsp;<span class="requiredField">(*)</span>
                            </td>
                            <td style="width: 35%;">
                                <input id="txtFilter_MaSoThue" type="text" class="inputflat" style="width: 150px;"
                                    onblur="{jsCheckMaNNT(this);}" value="2500150335" />
                            </td>
                            <td style="width: 15%;">
                                <b>Số tờ khai</b>
                            </td>
                            <td style="width: 35%;">
                                <input id="txtFilter_SoToKhai" type="text" class="inputflat" style="width: 150px;"
                                    value="10003732923" />
                            </td>
                        </tr>
                        <tr align="left">
                            <td>
                                <b>Năm đăng ký</b>
                            </td>
                            <td align="left" colspan="3">
                                <input id="txtFilter_NgayDKy" type="text" class="inputflat" style="width: 150px;"
                                    maxlength="4" onkeypress="return ValidNumber(event);" onblur="javascript:jsCheckYear(this)"
                                    onfocus="this.select()" />
                                <b>(YYYY)</b>
                            </td>
                            <tr align="left">
                                <td colspan="4" align="center">
                                    <input id="btnTruyVanHQ" type="button" value="Truy vấn HQ" class="ButtonCommand"
                                        onclick="jsTruyVanLePhi_HQ();" />
                                </td>
                            </tr>
                    </table>
                </div>
                <!--KET QUA TRUY VAN HQ-->
                <div id="divKQTruyVan" style="width: 100%; margin: 0; padding: 0; display: none;">
                    <br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="grid_header">
                            <td align="left">
                                TRUY VẤN THUẾ HẢI QUAN
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divResultTruyVan" style="width: 100%; height: 200px; overflow: scroll;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--THONG BAO-->
                <div class="errorMessage" style="width: 100%; margin: 0; padding: 0;">
                    <div id="divStatus" style="width: 100%; margin: 0; padding: 0; background-color: Aqua;
                        font-weight: bold; font-size: 18pt; display: none;">
                        <br />
                    </div>
                    <div id="divProgress" style="width: 100%; margin: 0; padding: 0; background-color: Aqua;
                        font-weight: bold; font-size: 18pt; display: none;">
                        <b style="">Đang xử lý. Xin chờ một lát...</b>
                    </div>
                </div>
                <!--THONG TIN TO KHAI-->
                <br />
                <div id="divTTinToKhai" style="width: 100%; margin: 0; padding: 0;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr align="left" class="grid_header">
                            <td align="left" style="width: 70%;">
                                <div style="float: left;">
                                    THÔNG TIN CHỨNG TỪ LỆ PHÍ HẢI QUAN
                                </div>
                                <div id="lblSoCTu" style="float: left; display: inline;">
                                </div>
                            </td>
                            <td align="right">
                                Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <div id="pnlTTinTKhai" style="width: 100%;">
                                    <table id="grdHeader" cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px;
                                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                        <tr id="rowMaNNT">
                                            <td width="24%">
                                                <b>Mã số thuế</b><span class="requiredField">(*)</span>
                                            </td>
                                            <td width="25%">
                                                <input id="txtMaNNT" type="text" class="inputflat" style="width: 98%;" onblur="{jsCheckMaNNT(this);}" />
                                            </td>
                                            <td width="51%" style="height: 25px">
                                                <input id="txtTenNNT" type="text" class="inputflat" style="width: 98%;" maxlength="140" />
                                            </td>
                                        </tr>
                                        <tr id="rowDChiNNT">
                                            <td>
                                                <b>Địa chỉ người nộp thuế</b>
                                            </td>
                                            <td colspan="2">
                                                <input id="txtDChiNNT" type="text" class="inputflat" style="width: 98.7%;" />
                                            </td>
                                        </tr>
                                        <tr id="rowHuyen" style="display:none">
                                            <td>
                                                <b>Quận(Huyện) người nộp thuế</b>
                                            </td>
                                            <td>
                                                <input id="txtHuyen_NNT" type="text" class="inputflat" style="width: 98%; background: Aqua"
                                                    onkeypress="if (event.keyCode==13){ShowLov('DBHC_NNT');}" onblur="jsGetTenQuanHuyen_NNThue(this.value);" />
                                            </td>
                                            <td>
                                                <input id="txtTenHuyen_NNT" type="text" class="inputflat" style="width: 98%;" readonly="readonly">
                                            </td>
                                        </tr>
                                        <tr id="rowTinh"  style="display:none">
                                            <td class="style1">
                                                <b>Tỉnh(Thành phố) người nộp thuế</b>
                                            </td>
                                            <td class="style1">
                                                <input id="txtTinh_NNT" type="text" class="inputflat" style="width: 98%;" onblur="jsGetTenTinh_NNThue(this.value);" />
                                            </td>
                                            <td class="style1">
                                                <input id="txtTenTinh_NNT" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>
                                        <%--<tr id="rowTTNo">
                                            <td class="style1">
                                                <b>Trạng thái nợ</b>
                                            </td>
                                            <td class="style1">
                                                <input id="txtTTNo" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                            <td class="style1">
                                                <input id="txtTen_TTN" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>--%>
                                        <tr id="rowMaHQ">
                                            <td class="style1">
                                                <b>Mã HQ </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td class="style1">
                                                <input id="txtMaHQ" type="text" style="width: 98%; background: Aqua;" class="inputflat" />
                                            </td>
                                            <td class="style1">
                                                <input id="txtTenMaHQ" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr id="rowCQThu">
                                            <td class="style2">
                                                <b>CQ Thu </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td class="style2">
                                                <input id="txtMaCQThu" type="text" onkeypress="if (event.keyCode==13){ShowLov('CQT');}"
                                                    onblur="jsLoadByCQT();" style="width: 98%; background: Aqua;" class="inputflat" />
                                            </td>
                                            <td class="style2">
                                                <input id="txtTenCQThu" type="text" class="inputflat" style="width: 98%;" />
                                            </td>
                                        </tr>
                                        <tr id="rowMaHQPH">
                                            <td class="style1">
                                                <b>Mã HQ PH </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td class="style1">
                                                <input id="txtMaHQPH" type="text" onkeypress="if (event.keyCode==13){ShowLov('MHQ');}"
                                                    style="width: 98%; background: Aqua;" class="inputflat" />
                                            </td>
                                            <td class="style1">
                                                <input id="txtTenMaHQPH" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr id="rowSHKB">
                                            <td width="24%" class="style1">
                                                <b>Số hiệu KB </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td width="25%" class="style1">
                                                <input id="txtSHKB" type="text" class="inputflat" style="width: 98%; background: Aqua;"
                                                    onkeypress="if (event.keyCode==13){ShowLov('SHKB');}" onblur="" />
                                            </td>
                                            <td width="51%" class="style1">
                                                <input id="txtTenKB" type="text" class="inputflat" style="width: 98%;" maxlength="60" />
                                            </td>
                                        </tr>
                                        <tr id="rowDBHC" style="">
                                            <td style="">
                                                <b>Mã tỉnh/thành Kho bạc </b><span class="requiredField"></span>
                                            </td>
                                            <td style="">
                                                <input id="txtMaDBHC" type="text" class="inputflat" style="width: 98%;" onkeypress="if (event.keyCode==13){ShowLov('DBHC');}"
                                                    onblur="jsGetTenTinh_KB(this.value);" onchange="jsGet_TenDBHC();" />
                                            </td>
                                            <td style="">
                                                <input id="txtTenDBHC" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr id="rowTKCo">
                                            <td>
                                                <b>TK Có NSNN </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td>
                                                <select id="ddlMa_NTK" style="width: 100%;" class="inputflat">
                                                    <option value="1" selected="selected">Tài khoản thu NSNN</option>
                                                    <option value="2">Tài khoản tạm thu</option>
                                                    <option value="3">Hoàn thuế GTGT</option>
                                                </select>
                                            </td>
                                            <td style="">
                                                <input type="text" id="ddlMaTKCo" class="inputflat" style="width: 99%;" maxlength="18" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Ngày HT/ Ngày CT</b>
                                            </td>
                                            <td>
                                                <input id="txtNGAY_HT" type="text" class="inputflat" style="width: 98%;" maxlength="10"
                                                    readonly="readonly" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <!--THONG TIN NGAN HANG-->
                <div id="divTTinNganHang" style="width: 100%; margin: 0; padding: 0;">
                    <table cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px;
                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                        <tr class="grid_header">
                            <td align="left" colspan="3">
                                THÔNG TIN NGÂN HÀNG
                            </td>
                        </tr>
                        <tr id="rowMaNT" align="left">
                            <td style="width: 25%;">
                                <b>Nguyên tệ</b>
                            </td>
                            <td style="width: 25%;">
                                <select id="ddlMaNT" style="width: 98%" class="inputflat" onchange="changeNT();">
                                    <option></option>
                                </select>
                            </td>
                            <td>
                                <input id="txtTenNT" class="inputflat" style="width: 98%;" readonly="readonly" value="VND" />
                            </td>
                        </tr>
                        <tr id="rowTyGia" align="left">
                            <td>
                                <b>Tỷ giá</b>
                            </td>
                            <td align="left">
                                <input id="txtTyGia" type="text" class="inputflat" style="width: 98%;" readonly="readonly">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="rowPT_TT" align="left">
                            <td>
                                <b>Phương thức thanh toán</b>
                            </td>
                            <td>
                                <select id="ddlPT_TT" style="width: 98%" class="inputflat" onchange="onChangePTTT()">
                                    
                                    <option value="01" selected="selected">Chuyển khoản</option>
                                    <option value="03">Nộp tiền GL</option>
                                </select>
                            </td>
                            <td>
                                <input id="txtTenHTTT" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowTK_KH_NH" align="left" class="trChuyenKhoan">
                            <td>
                                <b>TK Chuyển </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtTK_KH_NH" class="inputflat" style="width: 98%;" onblur="jsGetTK_KH_NH();"
                                    maxlength="16" />
                            </td>
                            <td>
                                <input id="txtTenTK_KH_NH" class="inputflat" style="width: 92%;" disabled="true"
                                    readonly="readonly" />
                                <img id="imgSignature" alt="Hiển thị chữ ký khách hàng" src="../../images/icons/MaDB.png"
                                    onclick="jsShowCustomerSignature();" style="width: 16px; height: 16px" />
                            </td>
                        </tr>
                        <tr id="rowSoDuNH" align="left" class="trChuyenKhoan">
                            <td>
                                <b>Số dư khả dụng</b>
                            </td>
                            <td>
                                <input id="txtSoDu_KH_NH" class="inputflat" style="width: 98%;" readonly="readonly" />
                            </td>
                            <td>
                                <input id="txtDesc_SoDu_KH_NH" type="text" class="inputflat" style="width: 98%;"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowTKGL" align="left" style="display:none">
                            <td>
                                <b>TK GL </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtTKGL" class="inputflat" style="width: 98%;" onblur="jsGet_TenTKGL();"
                                    maxlength="16" />
                            </td>
                            <td>
                                <input id="txtTenTKGL" class="inputflat" style="width: 92%;" disabled="true"
                                    readonly="readonly" />
                            
                            </td>
                        </tr>
                        <tr id="rowTienMat" align="left" class="trTienMat" style="display: none">
                            <td>
                                <b>CMTND/Điện thoại </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtCmtnd" class="inputflat" style="width: 98%;" />
                            </td>
                            <td>
                                <input id="txtDienThoai" type="text" class="inputflat" style="width: 98%;" />
                            </td>
                        </tr>
                        <tr id="rowNNTien" align="left" style="display:none">
                            <td>
                                <b>MST/Tên người nộp tiền</b>
                            </td>
                            <td>
                                <input id="txtMaNNTien" type="text" class="inputflat" style="width: 98%;" maxlength="14"
                                    onkeyup="" />
                            </td>
                            <td>
                                <input id="txtTenNNTien" type="text" class="inputflat" style="width: 98%;" />
                            </td>
                        </tr>
                        <tr id="rowDChiNNTien" align="left" style="display:none">
                            <td>
                                <b>Địa chỉ người nộp tiền</b>
                            </td>
                            <td colspan="2">
                                <input id="txtDChiNNTien" type="text" maxlength="50" class="inputflat" style="width: 98.7%;" />
                            </td>
                        </tr>
                        <tr id="rowHuyenNNTien" align="left" style="display:none">
                            <td>
                                <b>Quận(Huyện) người nộp tiền</b>
                            </td>
                            <td>
                                <input id="txtQuan_HuyenNNTien" type="text" class="inputflat" style="width: 98%;"
                                    onkeypress="if (event.keyCode==13){ShowLov('DBHC_NNTHUE');}" onblur="jsGetTenQuanHuyen_NNTien(this.value);" />
                            </td>
                            <td>
                                <input id="txtTenQuan_HuyenNNTien" type="text" class="inputflat" style="width: 98%;"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowTinhNNTien" align="left" style="display:none">
                            <td class="style1">
                                <b>Tỉnh(Thành phố) người nộp tiền</b>
                            </td>
                            <td class="style1">
                                <input id="txtTinh_NNTien" type="text" class="inputflat" style="width: 98%;" readonly="readonly"
                                    onblur="jsGetTenTinh_NNTien(this.value);" />
                            </td>
                            <td class="style1">
                                <input id="txtTenTinh_NNTien" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowNHChuyen" align="left">
                            <td>
                                <b>Mã NH Chuyển </b><span class="requiredField">(*)</span>
                            </td>
                            <td colspan="2">
                                <select id="ddlMA_NH_A" style="width: 98.7%;" class="inputflat">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr id="rowNHThanhToan" align="left" style="display: none">
                            <td>
                                <b>Mã NH trực tiếp </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtMA_NH_TT" class="inputflat" style="width: 98%;" maxlength="8" onblur="jsGet_TenNH_TT()"
                                    onkeypress="if (event.keyCode==13){ShowLov('DMNH_TT');jsGet_TenNH_TT();}" onkeyup="" />
                            </td>
                            <td>
                                <input id="txtTEN_NH_TT" class="inputflat" style="width: 98%;" readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowNHNhan" align="left">
                            <td>
                                <b>Mã NH thụ hưởng </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtMA_NH_B" class="inputflat" style="width: 98%;" maxlength="8" onblur="jsGet_TenNH_B()"
                                    onkeypress="if (event.keyCode==13){ShowLov('DMNH_GT');jsGet_TenNH_B();}" onkeyup="" />
                            </td>
                            <td>
                                <input id="txtTenMA_NH_B" class="inputflat" style="width: 98%;" readonly="readonly" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <!--CHI TIET TO KHAI-->
                <div id="divCTietToKhai" style="width: 100%; margin: 0; padding: 0;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="grid_header">
                            <td align="left">
                                CHI TIẾT CHỨNG TỪ LỆ PHÍ HẢI QUAN
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div id="pnlCTietTTinTKhai" style="height: 200px; min-height: 170px; width: 100%;
                                    overflow: scroll;">
                                </div>
                                <div id="" style="width: 100%; vertical-align: middle;" align="right">
                                    <input id="btnAddRowDTL" type="button" value="Thêm dòng" class="ButtonCommand" onclick="jsAddDummyRowDTL_Grid(1);" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <!--THONG TIN KHAC-->
                <div id="divTTinKhac" style="width: 100%; margin: 0; padding: 0;">
                    <table cellspacing="3" cellpadding="1" style="width: 100%;">
                        <tr align="left">
                            <td align="left" valign="middle" class="text" style="width: 50%; font-family: @Arial Unicode MS;
                                font-weight: bold; color: red">
                                <span style="">Số ký tự còn lại: </span>
                                <label id="leftCharacter" style="font-weight: bold" visible="false">
                                </label>
                            </td>
                            <td align="right" class="text" style="width: 50%;">
                                <span style="font-family: @Arial Unicode MS; font-weight: bold;">Tổng Tiền: </span>
                                <input type="text" id="txtTongTien" style="background-color: Yellow; text-align: right;
                                    font-weight: bold" class="inputflat" value="0" readonly="readonly" disabled="disabled" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="text" style="width: 100%;" colspan="2">
                                <span style="font-family: @Arial Unicode MS; font-weight: bold;">Tổng Tiền NT: </span>
                                <input type="text" id="txtTongTienNT" style="background-color: Yellow; text-align: right;
                                    font-weight: bold" class="inputflat" value="0" readonly="readonly" disabled="disabled" />
                            </td>
                        </tr>
                         <tr>
                                        <td align="right" colspan="2">
                                            <table width="600px">
                                                <tr>
                                                    <td>
                                                        Phí mặc định
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" id="chkChargeType" checked onclick="jsEnabledInputCharge();" />
                                                    </td>
                                                    <td>
                                                        Phí
                                                    </td>
                                                    <td>
                                                        <input type="text" id="txtCharge" style="text-align: right; width: 100px; background-color: Yellow;
                                                            font-weight: bold" class="inputflat" value="0" disabled="disabled" onfocus="this.select()"
                                                            onkeyup="valInteger(this)" onblur="jsCalCharge();" />
                                                    </td>
                                                    <td>
                                                        VAT:
                                                    </td>
                                                    <td>
                                                        <input type="text" id="txtVAT" style="text-align: right; width: 100px; background-color: Yellow;
                                                            font-weight: bold" class="inputflat" value="0" readonly disabled="disabled" />
                                                    </td>
                                                    <td>
                                                        Tổng trích nợ:
                                                    </td>
                                                    <td>
                                                        <input type="text" id="txtTongTrichNo" style="text-align: right; width: 150px; background-color: Yellow;
                                                            font-weight: bold" class="inputflat" value="0" readonly disabled="disabled" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                        <tr>
                            <td align="right" style="width: 99%" colspan="2">
                                <div id='divBangChu' style="font-weight: bold;">
                                </div>
                            </td>
                        </tr>
                        <tr id="rowLyDoHuy" align="left">
                            <td align="left" style="width: 99%" colspan="2" valign="middle">
                                <span style="font-weight: bold;">Lý do hủy/điều chỉnh :</span>
                                <input type="text" id="txtLyDoHuy" style="width: 99%; border-color: black; font-weight: bold"
                                    maxlength="100" class="inputflat" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td colspan="8">
                                            <input type="hidden" id="hdnSoCT_NH" />
                                            <input type="hidden" id="hdnSoLan_LapCT" />
                                            <input type="hidden" id="hdnTongTienCu" value="0" />
                                            <input type="hidden" id="hdnMA_KS" />
                                            <input type="hidden" id="hdnSO_CT" />
                                            <input type="hidden" id="hdnMA_NTK" />
                                        </td>
                                    </tr>
                                   
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2' height='5'>
                            </td>
                        </tr>
                    </table>
                    <input id="hdfSo_CTu" type="hidden" />
                    <input id="hdfLoaiCTu" type="hidden" />
                    <input id="hdfHQResult" type="hidden" />
                    <input id="hdfHDRSelected" type="hidden" />
                    <input id="hdfDTLList" type="hidden" />
                    <input id="hdfDTLSelected" type="hidden" />
                    <input id="hdfIsEdit" type="hidden" />
                    <input id="hdfDescCTu" type="hidden" />
                    <input id="hdfMST" type="hidden" />
                    <input id="hdfNgayKB" type="hidden" />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width='100%' align="center" valign="top" class='text' colspan='2' align="right">
                <input type="button" id="cmdThemMoi" class="ButtonCommand" onclick="jsThem_Moi();"
                    value="[Lập Mới]" accesskey="M" />
                &nbsp;
                <input type="button" id="cmdGhiKS" class="ButtonCommand" value="[Hoàn thiện]" onclick="jsGhi_CTU('GHI');"
                    accesskey="K" />
                &nbsp;
                <input type="button" id="cmdChuyenKS" class="ButtonCommand" value="[Chuyển KS]" style=""
                    accesskey="C" onclick="jsGhi_CTU('GHIKS');" visible="true" />
                &nbsp;
                <input type="button" id="cmdDieuChinh" class="ButtonCommand" value="[Điều chỉnh]"
                    style="" accesskey="C" onclick="jsGhi_CTU('GHIDC');" visible="true" />
                &nbsp;
                <input type="button" id="cmdInCT" value="[In CT]" class="ButtonCommand" onclick="jsIn_CT('1');"
                    accesskey="I" />
                &nbsp;
                <input type="button" id="cmdHuyCT" class="ButtonCommand" value="[(H)ủy]" onclick="jsHuy_CTU();"
                    accesskey="H" />
                <%--&nbsp;
                <input type="button" id="Button1" class="ButtonCommand" value="[XXXXX]" onclick="jsCalCharacter();"
                    accesskey="H" />--%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
