﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/shared/MasterPage05.master"
    EnableEventValidation="true" CodeFile="frmLapCTu_HQ.aspx.vb" Inherits="pages_HaiQuan_frmLapCTu_HQ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/ChuyenSoSangChu.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script src="../../javascript/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>

    <script src="../../javascript/jquery/1.11.2/jquery-ui.min.js" type="text/javascript"></script>

    <script src="../../javascript/json/json2.js" type="text/javascript"></script>

    <script src="../../javascript/accounting.min.js" type="text/javascript"></script>

    <script src="../../javascript/object/chungTu.js" type="text/javascript"></script>

    <script src="../../javascript/table/paging.js" type="text/javascript"></script>

    <link href="../../javascript/table/paging.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">

        //Bien javascript cuc bo
        var maTienTe = 'VND';
        var feeRate = 2;
        var minFee = 10000;
        var isGL = false;
        function infHDRCompare(SoTK, Ma_HQ_PH, Ma_HQ_CQT, Ma_NTK, TKKB, MaSoThue, TTNo, TTNo_CT) {
            this.SoTK = SoTK;
            this.Ma_HQ_PH = Ma_HQ_PH;
            this.Ma_HQ_CQT = Ma_HQ_CQT;
            this.Ma_NTK = Ma_NTK;
            this.TKKB = TKKB;
            this.MaSoThue = MaSoThue;
            this.TTNo = TTNo;
            this.TTNo_CT = TTNo_CT;
        }

        $(document).ready(function () {
            onChangePTTT();

            $('#txtHuyen_NNT').change(function () {
                jsGetTenQuanHuyen_NNThue($(this).val());
            });

            $('#txtMaDBHC').change(function () {
                jsGetTenTinh_KB($(this).val());
            });

            $('#txtQuan_HuyenNNTien').change(function () {
                jsGetTenQuanHuyen_NNTien($(this).val());
            });

            $('#txtTKGL').change(function () {
                jsGet_TenTKGL($(this).val());
            });

            $('#txtTongTrichNo').change(function () {
                jsConvertCurrToText();
            });


            try {
                $('#grdDSCT').paging({ limit: 15 });
            } catch (err) {
                paginationList(15);
            }

            $('#ddlMa_NTK').change(function () {
                $('#ddlMaTKCo').val('');
            });

            $("#ddlMaNT option[value='VND']").prop('selected', true);

            //$('#ddlMaNT').change(); 
        });

        function paginationList(listPaginationCount) {
            $('table#grdDSCT').each(function () {
                var currentPage = 0;
                var numPerPage = listPaginationCount;
                var $pager = $('.pager').remove();
                var $paging = $("#paging").remove();
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div id="pager"></div>');

                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');

                }

                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });
        }

        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }

        function jsCheckYear(elementInput) {
            var elmInput = elementInput;
            var yearNum = elmInput.value;
            if (yearNum !== '') {
                if (!validYear(yearNum)) {
                    alert('Năm truyền vào không hợp lệ');
                    elmInput.value = "";
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }

        function validYear(yearNum) {
            var txt = /^[0-9]+$/;
            if (yearNum) {
                if (yearNum.length != 4) {
                    return false;
                } else {
                    if (txt.test(yearNum)) {
                        if ((parseInt(yearNum) < 1000) || (parseInt(yearNum) > 3000)) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }

        function jsTinhPhi() {

            $('#txtTongTrichNo').val(accounting.formatNumber('0')).change();
            var httt = document.getElementById("ddlPT_TT").value;
            var strAcountGL = document.getElementById('txtTKGL').value;
            var strAcountNo = document.getElementById('txtTK_KH_NH').value;
            if (httt == "03") {
                LoaiCK = "G";
                if (strAcountGL == "") {

                    alert("Chưa có tài khoản GL");
                    return;
                }
            }

            var strAmount = $get('txtTongTien').value.replaceAll(',', '');

            if (strAmount == "0") {
                alert("Không tính phí với số tiền bằng 0");
                return;
            }
            //var strAcountNo = document.getElementById('txtTK_KH_NH').value;
            if (strAcountNo == "") {
                if (document.getElementById('ddlPT_TT').value != '00') {
                    alert("Chưa có tài khoản trích nợ");
                    return;
                }
            }
            var payType = "";
            if (httt == "00")
                payType = "TM";
            else if (httt == "01" || httt == "05") {
                if (isGL == true)
                    payType = "A";
                else
                    payType = "A";
            }

            var pProvince = $get('txtMaDBHC').value;
            var accountNo = ""
            if (strAcountNo != "") {
                accountNo = $get('txtTK_KH_NH').value;
            }
            else if (strAcountGL != "") {
                accountNo = $get('txtTKGL').value;
            }
            var Type_Tranfer = "A";
            var Sender_bank_code = $get('ddlMA_NH_A').value;
            var Sender_bank_name = $get('ddlMA_NH_A').options[$get('ddlMA_NH_A').selectedIndex].text;
            var cust_ID = $get('txtMaNNT').value;
            var ben_account = $get('ddlMaTKCo').value;
            var ben_name_id = $get('txtTenKB').value;
            var ben_bank_id = $get('txtMA_NH_TT').value;
            var Ben_bank_id_name = $get('txtTEN_NH_TT').value;
            var ben_bank_id_2 = $get('txtMA_NH_B').value;
            var Ben_bank_id_2_name = $get('txtTenMA_NH_B').value;
            var Related_cust = $get('cifNo').value;
            var Amount = strAmount;
            var currency = "VND";
            //alert(currency)

            if (ben_account == "") {
                alert("Thông tin TK Có NSNN không được để trống.");
                return;
            }

            PageMethods.GetFee(pProvince, accountNo, Type_Tranfer, Sender_bank_code, Sender_bank_name, cust_ID, ben_account, ben_name_id, ben_bank_id, Ben_bank_id_name, ben_bank_id_2, Ben_bank_id_2_name, Related_cust, Amount, currency, TinhPhi_Complete, TinhPhi_Error);
        }
        function TinhPhi_Complete(result, methodName) {
            checkSS(result);

            if (result.length > 0) {
                xmlDoc = $.parseXML(result),
                $xml = $(xmlDoc),
                $msg_stat = $xml.find("errcode");
                var v_fee = "0";
                var v_vat = "0";

                if ($msg_stat.text() == "") {
                    v_fee = $xml.find("FeeAmount").text();

                    v_vat = Math.round(parseFloat(v_fee) / 10)

                    $get('txtCharge').value = v_fee;
                    $get('txtVAT').value = v_vat;
                } else {
                    document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình tính phí của khách hàng.';
                    $get('txtCharge').value = "0";
                    $get('txtVAT').value = "0";
                    $get('txtSanPham').value = "";
                }

                var v_ttien = $get('txtTongTien').value.replace(/\./g, '');

                $('#txtVAT').val(accounting.formatNumber(v_vat));
                $('#txtCharge').val(accounting.formatNumber(v_fee));
                $('#txtTongTien').val(accounting.formatNumber(v_ttien));
                TotalMoney();
                checkTinhPhi = 1;

                document.getElementById('divStatus').innerHTML = "";

            } else
                document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình tính phí của khách hàng.';
        }
        function TinhPhi_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình tính phí" + error.get_message();
            }

        }

        function TotalMoney() {
            parseFloat(accounting.unformat($('#txtCharge').val()));
            var dblVAT = parseFloat(accounting.unformat($('#txtVAT').val()));
            var dblPhi = parseFloat(accounting.unformat($('#txtCharge').val()));

            var dblTongTien = parseFloat(accounting.unformat($('#txtTongTien').val()));
            //dblPhi=dblPhi.toString().replaceAll('.','')

            var dblTongTrichNo = parseFloat(dblPhi) + parseFloat(dblVAT) + parseFloat(dblTongTien);
            $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();

        }
        function jsCalCharacter_V20() {
            var tempCharater = '';
            var sChungCheck = '';
            var countRow = 0;
            //get detail
            $('#grdChiTiet > tbody  > tr').each(function () {
                //sID = '';
                var ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
                var ctrInput = null;

                var sChung = '';
                var sDetail = '';
                /*Sinh ra thanh phan chung*/


                if (ctrCheck) {
                    if (ctrCheck.is(":checked")) {

                        sChung = '';
                        sDetail = '';
                        ctrInput = $(this).find("input[id^='DTL_Lhxnk_']");
                        if (ctrInput) {
                            sChung += "LH:" + ctrInput.prop("value");
                        }

                        if (sChungCheck.indexOf(sChung) < 0) {
                            if (sChungCheck == "") {
                                sChungCheck += sChung;
                            }
                            else {
                                sChungCheck += sChung;
                            }
                        }
                        countRow += 1;
                    }
                    else {
                        ctrInput = null;
                    }
                }

            });
            //        if (countRow >5)//Toi da 5 dong
            //        {
            //            return false;
            //        }
            tempCharater += sChungCheck;
            tempCharater += ".DNCT " + $get('txtGhiChu').value;

            $get('txtDienGiai').value = tempCharater;

            var numberRemark = $get('txtDienGiai').value.length;
            var charNumberLimit = 210;
            charNumber = charNumberLimit - numberRemark;
            document.getElementById('leftCharacter').visible = true;
            document.getElementById('leftCharacter').innerHTML = charNumber + "/" + charNumberLimit;
            if (charNumber < 0) {
                return false;
            }
            else {
                return true;
            }

        }
        //    function jsCalCharacter(){
        ////        alert("jsCalCharacter");
        //        if(v_coreVersion=="2"){
        //            return jsCalCharacter_V20();
        //        }else{
        //            var tempCharater = '';

        //            if ($get('txtMaNNT').value != "") {

        //                tempCharater += $get('txtTenNNT').value;
        //            
        //                tempCharater += "MST" + $get('txtMaNNT').value;

        //                tempCharater += "LThue04";

        //                tempCharater += "NgayNT" + $get('txtNGAY_HT').value + ";";
        //                
        //          
        //            var sChungCheck = '';
        //            
        //            //get detail
        //            $('#grdChiTiet > tbody  > tr').each(function(){
        //                //sID = '';
        //                var ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
        //                var ctrInput = null;
        //                
        //                var sChung = '';            
        //                var sDetail = '';
        //                /*Sinh ra thanh phan chung*/
        //                            
        //                            
        //                if (ctrCheck) {
        //                    if (ctrCheck.is(":checked")){
        //                    
        //                        sChung = '';
        //                        sDetail = '';

        //                        /*Detail*/
        //                        ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
        //                        if (ctrInput) {
        //                            sDetail += "NDKT" + ctrInput.prop("value"); +";"
        //                        }

        //                        ctrInput = $(this).find("input[id^='DTL_MA_CHUONG_']");
        //                        if (ctrInput) {
        //                            sDetail += "C" + ctrInput.prop("value"); +";"
        //                        }

        //                        ctrInput = $(this).find("input[id^='DTL_SoTien_']");
        //                        if (ctrInput) {
        //                            sDetail += "S" + ctrInput.prop("value"); +";"
        //                        }

        //                        ctrInput = $(this).find("input[id^='DTL_Noi_Dung_']");
        //                        if (ctrInput) {
        //                            sDetail += " ND " + ctrInput.prop("value") + ";";
        //                        }

        //                        /*Chung*/
        //                        ctrInput = $(this).find("input[id^='DTL_Ngay_TK_']");
        //                        if (ctrInput) {
        //                            sChung += "NDK:" + ctrInput.prop("value"); +";"
        //                        }


        //                        ctrInput = $(this).find("input[id^='DTL_Lhxnk_']");
        //                        if (ctrInput) {
        //                            sChung += "LH:" + ctrInput.prop("value"); + ";"
        //                        }
        //                        

        //                        ctrInput = $(this).find("input[id^='DTL_SO_TK_']");
        //                        if (ctrInput) {
        //                            sChung += "TK:" + ctrInput.prop("value"); + ";"
        //                        }
        //                        
        //                        
        //                        /*------------------------------------------*/
        //                        sChungCheck += sDetail;
        //                        if(sChungCheck.indexOf(sChung) < 0)
        //                        {
        //                            if(sChungCheck == "")
        //                            {
        //                                sChungCheck += sChung; //+ " (";                        
        //                            }
        //                            else
        //                            {
        //                                //sChungCheck += //") ";
        //                                sChungCheck += sChung; //+ " (";
        //                            }
        //                        }  
        //                        
        //                        
        //                                     
        //                    }
        //                    else {
        //                        ctrInput = null;
        //                    }                                                
        //                }    
        //                        
        //            });

        //            if  (sChungCheck != '')
        //            {
        //                sChungCheck += "- ";
        //                tempCharater += sChungCheck;
        //            }
        //                    
        //            //tempCharater+= "CT " + $get('txtTenKB').value;
        //            //tempCharater+= " TAI " + $get('txtTenMA_NH_B').value;

        //            tempCharater += "" + $get('txtGhiChu').value;

        //            tempCharater=tempCharater.replaceAll(",","");
        //            /*Set value for txtDienGiai*/
        //            $get('txtDienGiai').value = tempCharater; 
        //            }
        //            
        //                var numberRemark=$get('txtDienGiai').value.length;
        //                var charNumberLimit = 210;
        //                charNumber = charNumberLimit - numberRemark;
        //                document.getElementById('leftCharacter').visible = true;
        //                document.getElementById('leftCharacter').innerHTML = charNumber + "/" + charNumberLimit;
        //                if (charNumber <0)
        //                {
        //                    return false;
        //                }
        //                else
        //                { 
        //                    return true;
        //                }
        //            
        //        }
        //    }
        //Nhamlt _20180531_Sua theo chuan moi cong van 2936 cua TCHQ
        function jsMapST_Diengiai(Sacthue) {
            //XK	Thuế xuất khẩu	ST1
            //NK	Thuế nhập khẩu	ST2
            //VA	Thuế giá trị gia tăng	ST3
            //TD	Thuế tiêu thụ đặc biệt	ST4
            //TV	Thuế tự vệ chống bán phá giá	ST5
            //MT	Thuế bảo vệ môi trường	ST6
            //TC	Thuế chống trợ cấp	ST7
            //PB	Thuế chống phân biệt đối xử	ST8
            //PG	Thuế chống bán phá giá	ST10
            //KH	Thuế khác	ST9
            switch (Sacthue) {
                case "XK": return "ST1";
                case "NK": return "ST2";
                case "VA": return "ST3";
                case "TD": return "ST4";
                case "TV": return "ST5";
                case "MT": return "ST6";
                case "TC": return "ST7";
                case "PB": return "ST8";
                case "PG": return "ST10";
                case "KH": return "ST9";
                case "LP": return "ST11";
            }
        }
        function jsCalCharacter() {

            if (v_coreVersion == "2") {
                return jsCalCharacter_V20();
            } else {
                if (v_remark2936 == "2") {
                    var tempCharater = '';

                    if ($get('txtMaNNT').value != "") {

                        tempCharater += $get('txtTenNNT').value;

                        tempCharater += "MST" + $get('txtMaNNT').value;

                        tempCharater += "LThue04";
                        //dtmNgayCore
                        //tempCharater += "NgayNT" + $get('txtNGAY_HT').value + ";";
                        tempCharater += "NgayNT" + dtmNgayCore + ";";


                        var sChungCheck = '';

                        //get detail
                        $('#grdChiTiet > tbody  > tr').each(function () {
                            //sID = '';
                            var ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
                            var ctrInput = null;

                            var sChung = '';
                            var sDetail = '';
                            /*Sinh ra thanh phan chung*/


                            if (ctrCheck) {
                                if (ctrCheck.is(":checked")) {

                                    sChung = '';
                                    sDetail = '';

                                    /*Detail*/
                                    ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                                    if (ctrInput) {
                                        sDetail += "NDKT" + ctrInput.prop("value"); +";"
                                    }

                                    ctrInput = $(this).find("input[id^='DTL_MA_CHUONG_']");
                                    if (ctrInput) {
                                        sDetail += "C" + ctrInput.prop("value"); +";"
                                    }

                                    ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                                    if (ctrInput) {
                                        sDetail += "S" + ctrInput.prop("value"); +";"
                                    }

                                    ctrInput = $(this).find("input[id^='DTL_Noi_Dung_']");
                                    if (ctrInput) {
                                        sDetail += " ND " + ctrInput.prop("value") + ";";
                                    }

                                    /*Chung*/
                                    ctrInput = $(this).find("input[id^='DTL_Ngay_TK_']");
                                    if (ctrInput) {
                                        sChung += "NDK:" + ctrInput.prop("value"); +";"
                                    }


                                    ctrInput = $(this).find("input[id^='DTL_Lhxnk_']");
                                    if (ctrInput) {
                                        sChung += "LH:" + ctrInput.prop("value"); + ";"
                                    }


                                    ctrInput = $(this).find("input[id^='DTL_SO_TK_']");
                                    if (ctrInput) {
                                        sChung += "TK:" + ctrInput.prop("value"); + ";"
                                    }


                                    /*------------------------------------------*/
                                    sChungCheck += sDetail;
                                    if (sChungCheck.indexOf(sChung) < 0) {
                                        if (sChungCheck == "") {
                                            sChungCheck += sChung; //+ " (";                        
                                        }
                                        else {
                                            //sChungCheck += //") ";
                                            sChungCheck += sChung; //+ " (";
                                        }
                                    }



                                }
                                else {
                                    ctrInput = null;
                                }
                            }

                        });

                        if (sChungCheck != '') {
                            sChungCheck += "- ";
                            tempCharater += sChungCheck;
                        }

                        //tempCharater+= "CT " + $get('txtTenKB').value;
                        //tempCharater+= " TAI " + $get('txtTenMA_NH_B').value;

                        tempCharater += "" + $get('txtGhiChu').value;

                        tempCharater = tempCharater.replaceAll(",", "");
                        /*Set value for txtDienGiai*/
                        $get('txtDienGiai').value = tempCharater;
                    }

                    var numberRemark = $get('txtDienGiai').value.length;
                    var charNumberLimit = 210;
                    charNumber = charNumberLimit - numberRemark;
                    document.getElementById('leftCharacter').visible = true;
                    document.getElementById('leftCharacter').innerHTML = charNumber + "/" + charNumberLimit;
                    if (charNumber < 0) {
                        return false;
                    }
                    else {
                        return true;
                    }

                } else {

                    return jsCalCharacter_new();
                }


            }
        }
        function jsCalCharacter_new() {
            var sChuong = '';
            var stokhai = '';
            var sNgayDK = '';
            var sLHXNK = '';
            var sMA_LT = '';
            var tempCharater = '';
            var detail = '';
            var header = '';
            var arrstokhai = [];
            var objDTL = jsGetDTLArray2();

            for (var i = 0; i < objDTL.length; i++) {
                if ((i + 1) < objDTL.length) {
                    if (objDTL[i].SO_TK != objDTL[i + 1].SO_TK) {
                        arrstokhai.push(i + 1);
                    }
                }
            }
            var index = 0;
            var tmp = 0;

            var strMST = $get('txtMaNNT').value;
            var strChiNhanhMST = "";
            if (strMST.trim().length == 13)
            {
                strChiNhanhMST = strMST.substring(10, 13);
                strMST = strMST.substring(0, 10).toString() + "-" + strChiNhanhMST.toString();
            }

            if (arrstokhai.length == 0) {
                header += "HQDT+ID0";
                header += "+MST" + strMST;
                for (var j = 0; j < objDTL.length; j++) {
                    stokhai = objDTL[j].SO_TK;
                    sNgayDK = objDTL[j].NGAY_TK;
                    sLHXNK = objDTL[j].MA_LHXNK;
                    sChuong = objDTL[j].MA_CHUONG;
                    sMA_LT = objDTL[j].MA_LT;
                }
                var datetime = getDateNNT();
                //dtmNgayCore
                header += "+C" + sChuong;
                //header += "+NNT" + datetime;
                header += "+NNT" + dtmNgayCore;
                header += "+HQ" + $get('txtMaHQPH').value;
                header += "-" + $get('txtMaHQ').value;
                header += "-" + $get('txtMaCQThu').value;
                header += "+TK" + stokhai;
                header += "+NDK" + sNgayDK.replaceAll("/", "");
                header += "+LH" + sLHXNK;
                header += "+NTK" + document.getElementById('ddlMa_NTK').value; //ma nhom tai khoan
                header += "+LT" + sMA_LT;
                header += "+KB" + $get('txtSHKB').value;
                var sTKNS = $get('ddlMaTKCo').value
                if (sTKNS.length > 4) {
                    sTKNS = sTKNS.substring(0, 4);
                }
                header += "+TKNS" + sTKNS;
                header += "+VND";
                for (var j = 0; j < objDTL.length; j++) {
                    detail += "(";
                    detail += "TM" + objDTL[j].MA_TMUC;
                    detail += "+ST" + jsGet_SacThue(objDTL[j].SAC_THUE);
                    detail += "+T" + objDTL[j].SOTIEN_NT;
                    detail += ")";
                }
                if (detail != '') {
                    header += detail;
                }
            }
            else {
                for (var i = 0; i < arrstokhai.length + 1; i++) {
                    index = arrstokhai[i]
                    //console.log("index:" + arrstokhai[i]);
                    header += "(";
                    header += "HQDT+ID0";
                    header += "+MST" + strMST;
                    for (var j = 0; j < index; j++) {
                        if (tmp < i) {
                            break;
                        }
                        stokhai = objDTL[j].SO_TK;
                        sNgayDK = objDTL[j].NGAY_TK;
                        sLHXNK = objDTL[j].MA_LHXNK;
                        sChuong = objDTL[j].MA_CHUONG;
                        sMA_LT = objDTL[j].MA_LT;
                    }
                    if (i > tmp) {
                        if ((i + 1) < arrstokhai.length) {

                            //console.log("i: " + i + "> tmp: " + tmp);
                            //console.log("arrstokhai[i]: " + arrstokhai[i]);
                            for (var j = arrstokhai[i - 1]; j < arrstokhai[i]; j++) {

                                stokhai = objDTL[j].SO_TK;
                                sNgayDK = objDTL[j].NGAY_TK;
                                sLHXNK = objDTL[j].MA_LHXNK;
                                sChuong = objDTL[j].MA_CHUONG;
                                sMA_LT = objDTL[j].MA_LT;
                            }
                        }
                        else {
                            // console.log("i: " + i + "> tmp: " + tmp);
                            // console.log("objDTL.length: " + objDTL.length);
                            //console.log("arrstokhai[i]: " + arrstokhai[i-1]);
                            for (var j = arrstokhai[i - 1]; j < objDTL.length; j++) {
                                stokhai = objDTL[j].SO_TK;
                                sNgayDK = objDTL[j].NGAY_TK;
                                sLHXNK = objDTL[j].MA_LHXNK;
                                sChuong = objDTL[j].MA_CHUONG;
                                sMA_LT = objDTL[j].MA_LT;
                            }
                        }
                    }
                    var datetime = getDateNNT();

                    header += "+C" + sChuong;
                    header += "+NNT" + dtmNgayCore;
                    header += "+HQ" + $get('txtMaHQPH').value;
                    header += "-" + $get('txtMaHQ').value;
                    header += "-" + $get('txtMaCQThu').value;
                    header += "+TK" + stokhai;
                    header += "+NDK" + sNgayDK.replaceAll("/", "");
                    header += "+LH" + sLHXNK;
                    header += "+NTK" + document.getElementById('ddlMa_NTK').value; //ma nhom tai khoan
                    header += "+LT" + +sMA_LT;
                    header += "+KB" + $get('txtSHKB').value;
                    var sTKNS = $get('ddlMaTKCo').value
                    if (sTKNS.length > 4) {
                        sTKNS = sTKNS.substring(0, 4);
                    }
                    header += "+TKNS" + sTKNS;
                    header += "+VND";
                    for (var j = 0; j < index; j++) {
                        if (tmp < i) {
                            break;
                        }
                        detail += "(";
                        detail += "TM" + objDTL[j].MA_TMUC;
                        detail += "+ST" + jsGet_SacThue(objDTL[j].SAC_THUE);
                        detail += "+T" + objDTL[j].SOTIEN_NT;
                        detail += ")";
                    }
                    if (i > tmp) {
                        if ((i + 1) < arrstokhai.length) {
                            detail = "";
                            for (var j = arrstokhai[i - 1]; j < arrstokhai[i]; j++) {
                                detail += "(";
                                detail += "TM" + objDTL[j].MA_TMUC;
                                detail += "+ST" + jsGet_SacThue(objDTL[j].SAC_THUE);
                                detail += "+T" + objDTL[j].SOTIEN_NT;
                                detail += ")";
                            }
                        }
                        else {
                            detail = "";
                            for (var j = arrstokhai[i - 1]; j < objDTL.length; j++) {
                                detail += "(";
                                detail += "TM" + objDTL[j].MA_TMUC;
                                detail += "+ST" + jsGet_SacThue(objDTL[j].SAC_THUE);
                                detail += "+T" + objDTL[j].SOTIEN_NT;
                                detail += ")";
                            }
                        }
                    }
                    if (detail != '') {
                        header += detail;
                    }
                    header += ")";
                    //
                    tmp = tmp + i;
                }

            }
            tempCharater += header;
            tempCharater = tempCharater.replaceAll(",", "");

            $get('txtDienGiai').value = tempCharater;
            var charNumberLimit = 210;
            charNumber = charNumberLimit - tempCharater.length;
            document.getElementById('leftCharacter').visible = true;
            document.getElementById('leftCharacter').innerHTML = charNumber + "/" + charNumberLimit;

            return true;
            //if (charNumber < 0) {
            //    return false;
            //}
            //else {
            //    return true;
            //}
        }
        function jsGet_SacThue(pvSacThue) {
            var maST = '';
            //console.log(arrSacThue);
            if (typeof arrSacThue !== 'undefined' && arrSacThue !== null) {
                if ((arrSacThue instanceof Array) && (arrSacThue.length > 0)) {
                    for (var i = 0; i < arrSacThue.length; i++) {
                        var arr = arrSacThue[i].split(';');
                        if (arr[0] == pvSacThue) {
                            maST = arr[1];
                            break;
                        }
                    }
                }
            }
            return maST;
        }

        function getDateNNT() {
            var currentdate = new Date();
            var date = "";
            var month = "";
            if (parseInt(currentdate.getDate()) < 10) {
                date = "0" + currentdate.getDate();
            }
            else {
                date = currentdate.getDate();
            }
            if (parseInt(currentdate.getMonth()) + 1 < 10) {
                month = "0" + parseInt(currentdate.getMonth() + 1)
            }
            else {
                month = parseInt(currentdate.getMonth() + 1);
            }
            var datetime = date.toString() + month.toString() + currentdate.getFullYear();
            return datetime;
        }
        function jsCalTotal() {
            var dblTongTien = 0;
            var dblPhi = 0;
            var dblVAT = 0;
            var dblTongTrichNo = 0;

            if ($get('SOTIEN_1').value.length > 0) {
                if (document.getElementById("chkRemove1").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_1').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_1');
                }
            }
            if ($get('SOTIEN_2').value.length > 0) {
                if (document.getElementById("chkRemove2").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_2').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_2');
                }
            }

            if ($get('SOTIEN_3').value.length > 0) {
                if (document.getElementById("chkRemove3").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_3').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_3');
                }
            }

            if ($get('SOTIEN_4').value.length > 0) {
                if (document.getElementById("chkRemove4").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_4').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_4');
                }
            }
            if (document.getElementById("ddlMaHTTT").value == "01" || document.getElementById("ddlMaHTTT").value == "00") {

                dblVAT = $get('txtVAT').value.replaceAll('.', '');
                dblPhi = $get('txtCharge').value.replaceAll('.', '');

                $get('txtTongTien').value = dblTongTien;

                dblTongTrichNo = parseFloat(dblPhi) + parseFloat(dblVAT) + parseFloat(dblTongTien);

                $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();
                jsFormatNumber('txtTongTien');
                jsFormatNumber('txtCharge');
                jsFormatNumber('txtVAT');
                //jsFormatNumber('txtTongTrichNo');
                $get('hdnTongTienCu').value = $get('txtTongTien').value;
                jsConvertCurrToText($("#txtTongTien").val().replaceAll('.', ''));
            } else {

                $get('txtCharge').value = 0;
                $get('txtVAT').value = 0;
                $get('txtTongTien').value = dblTongTien;
                dblTongTrichNo = dblTongTien;

                $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();
                jsFormatNumber('txtTongTien');
                jsFormatNumber('txtCharge');
                jsFormatNumber('txtVAT');
                jsFormatNumber('txtTongTrichNo');
                $get('hdnTongTienCu').value = $get('txtTongTrichNo').value;
                jsConvertCurrToText($("#txtTongTien").val().replaceAll('.', ''));
            }

            if ($("#txtTongTien").val().replaceAll('.', '') != _TTIEN) {
                _TTIEN = $("#txtTongTien").val().replaceAll('.', '');
                ResetTinhPhi();
            }

        }

        function jsCheckUserSession(param) {
            var bRet = true;
            if (param) {
                var arrKQ = param.split(';');
                if (arrKQ[0] == "errUserSession") {
                    window.open("../../pages/Warning.html", "_self");
                    bRet = false;
                }
            }
            return bRet;
        }

        function jsLoadCTList() {
            var sSoCtu = $('#txtTraCuuSoCtu').val();
            if (sSoCtu == 'undefined' || sSoCtu == '' || sSoCtu == null)
                sSoCtu = '';
            PageMethods.LoadCTList(curMa_NV, sSoCtu, LoadCTList_Complete, LoadCTList_Error);
        }

        function LoadCTList_Complete(result, methodName) {
            if (result.length > 0) {
                jsCheckUserSession(result);
                document.getElementById('divDSCT').innerHTML = result;
            }
            try {
                $('#grdDSCT').paging({ limit: 15 });
            } catch (err) {
                paginationList(15);
            }

        }

        function LoadCTList_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
        //xxx)Lay thong tin tai ngan hang song phuong
        function jsValidNHSHB_SP(strSHKB) {
            if ($get('txtSHKB').value.length > 0) {
                PageMethods.ValidNHSHB_SP($get('txtSHKB').value.trim(), ValidNHSHB_SP_Complete, ValidNHSHB_SP_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy SHKB';
            }
        }

        function ValidNHSHB_SP_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length < 8) {
                document.getElementById("hndNHSHB").value = '';
            } else {
                document.getElementById("hndNHSHB").value = result;
                if (result.length > 0) {
                    $get('txtMA_NH_B').value = result;
                    jsGet_TenNH_B();
                }
            }
        }
        function ValidNHSHB_SP_Error(error, userContext, methodName) {
            if (error !== null) {

                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng hưởng" + error.get_message();
            }
        }


        //xxx)Lay thong tin tai ngan hang song phuong jsValidNHSHB_SP_GhiCtu
        function jsValidNHSHB_SP_GhiCtu(strSHKB) {
            if ($get('txtSHKB').value.length > 0) {
                PageMethods.ValidNHSHB_SP($get('txtSHKB').value.trim(), ValidNHSHB_SP_GhiCtu_Complete, ValidNHSHB_SP_GhiCtu_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy SHKB';
            }
        }

        function ValidNHSHB_SP_GhiCtu_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length < 8) {
                document.getElementById("hndNHSHB").value = '';
            } else {
                document.getElementById("hndNHSHB").value = result;
            }
        }
        function ValidNHSHB_SP_GhiCtu_Error(error, userContext, methodName) {
            if (error !== null) {

                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng hưởng" + error.get_message();
            }
        }

        function jsGet_NNThue_HQ() {


            if ($('#txtFilter_MaSoThue').val().length == 0) {
                alert("Vui lòng nhập mã số thuế để truy vấn thông tin nợ thuế Hải quan!");
                $('#txtFilter_MaSoThue').focus();
                return;
            }

            jsShowDivStatus(false, "");
            jsShowHideDivProgress(true);
            jsEnableDisableControlButton('4');
            //clear data before proccess
            $('#hdfSo_CTu').val('');
            $('#hdfIsEdit').val(false);
            $('#hdfDescCTu').val('');

            //$('#hdfHQResult').val('');
            $('#hdfHDRSelected').val('');

            $('#hdfNgayKB').val('');

            //jsBindKQTruyVanGrid(null);
            jsClearHDR_Panel();
            jsBindDTL_Grid(null, null);

            //jsCalCharacter();

            var strMaSoThue = $('#txtFilter_MaSoThue').val();
            var strSoTK = $('#txtFilter_SoToKhai').val();
            var strNgayDK = $('#txtFilter_NgayDKy').val();

            //Kiem tra xem co ma so thue chua => neu chua co => set hdfHQResult = null
            if ($('#hdfMST').val().length == 0) {
                $('#hdfMST').val($('#txtFilter_MaSoThue').val());
                $('#hdfHQResult').val('');
                jsBindKQTruyVanGrid(null);
            } else {   //Neu co kiem tra xem ma so thue lan truoc voi lan truy van hien tai co trung khop nhau khong?
                if ($('#hdfMST').val() != $('#txtFilter_MaSoThue').val()) {
                    if (!confirm("Mã số thuế truy vấn hiện tại khác mã số thuế truy vấn lần trước. Bạn có muốn truy vấn không?"))
                        return;
                    else {   //reset hdfHQResult = null
                        $('#hdfMST').val($('#txtFilter_MaSoThue').val());
                        $('#hdfHQResult').val('');
                        jsBindKQTruyVanGrid(null);
                    }
                }
            }

            $.ajax({
                type: "POST",
                url: "frmLapCTu_HQ.aspx/Get_NNThue_HQ",
                cache: false,
                data: JSON.stringify({
                    "sMaSoThue": strMaSoThue,
                    "sSoTK": strSoTK || null,
                    "sNgayDK": strNgayDK || null
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: GetNNThueHQ_OnSuccess,
                failure: function (failure) {
                    if (failure !== null) {
                        var err;
                        err = JSON.parse(failure.responseText);
                        if (err) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            jsShowDivStatus(true, err.Message);
                        }
                    }
                    jsEnableDisableAddRowButton("1");
                    jsShowHideDivProgress(false);
                },
                error: function (request, status, error) {
                    var err;
                    err = JSON.parse(request.responseText);
                    if (err) {
                        if (err.Message.indexOf(';') > -1) {
                            jsCheckUserSession(err.Message);
                        }
                        jsShowDivStatus(true, err.Message);
                    }
                    jsEnableDisableAddRowButton("1");
                    jsShowHideDivProgress(false);
                }
            });
        }

        function GetNNThueHQ_OnSuccess(response) {
            var jsonObj = null;
            jsShowHideGet_NNThue_HQ(true);

            if (typeof response !== 'undefined' && response !== null) {
                if (response.d) {
                    if (!jsCheckUserSession(response.d)) {
                        return;
                    }
                    jsonObj = JSON.parse(response.d);
                    if (jsonObj) {
                        //set object vua truy van vao objChung
                        jsonObj.Data.Item = setHQresult(jsonObj.Data.Item);
                        //Save object to hiddenfield
                        $('#hdfHQResult').val(JSON.stringify(jsonObj));
                        //Bind Json result to grid
                        //alert("AAAAAaaa");
                        jsBindKQTruyVanGrid(jsonObj.Data);
                        //  alert("AAAAAaaa");
                        /*Load Remark*/
                        jsCalCharacter();
                    }
                }

            }
            jsEnableDisableAddRowButton("1"); //Hien thi nut them moi chi tiet
            jsShowHideDivProgress(false);

        }

        function setHQresult(arrRequestDataItem) {
            if ($('#hdfHQResult').val().length > 0) {
                var objHQ_Old = JSON.parse($('#hdfHQResult').val());
                var arrHQ_Old = [];
                arrHQ_Old = objHQ_Old.Data.Item;

                var flag = true;
                var objItemTemp = {};
                //Duyet tat ca cac Item cu => xem cai moi co trung ko => trung ko add them vao
                for (var i = 0; i < arrHQ_Old.length; i++) {
                    for (var j = 0; j < arrRequestDataItem.length; j++) {
                        if (!(arrHQ_Old[i].So_TK == arrRequestDataItem[j].So_TK && arrHQ_Old[i].Ma_NTK == arrRequestDataItem[j].Ma_NTK && arrHQ_Old[i].Ma_HQ_PH == arrRequestDataItem[j].Ma_HQ_PH &&
                                arrHQ_Old[i].Ma_HQ_CQT == arrRequestDataItem[j].Ma_HQ_CQT && arrHQ_Old[i].TKKB == arrRequestDataItem[j].TKKB)) {
                            flag = true;
                        } else {
                            flag = false;
                            break;
                        }
                    }
                    if (flag)
                        arrRequestDataItem.push(arrHQ_Old[i])
                }
            }

            return arrRequestDataItem;
        }

        function jsBindKQTruyVanGrid(objParam) {

            var top = [];
            top.push("<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdKQTruyVan' style='width: 100%; border-collapse: collapse;'>");
            top.push("<tr class='grid_header'>");
            top.push("<td align='center' style='width: 5%;'>Chọn</td>");
            top.push("<td align='center' style='width: 5%;'>STT</td>");
            top.push("<td align='center' style='width: 15%;'>Số tờ khai</td>");
            top.push("<td align='center' style='width: 15%;'>Mã HQ PH</td>");
            top.push("<td align='center' style='width: 15%;'>Mã HQ CQT</td>");
            top.push("<td align='center' style='width: 15%;'>Mã nhóm tài khoản</td>");
            top.push("<td align='center' style='width: 15%;'>Tài khoản kho bạc</td>");
            top.push("<td align='center' style='width: 15%;'>Tổng số tiền</td>");
            top.push("<td align='center' style='display: none;'>Mã số thuế</td>"); //invisible
            top.push("<td align='center' style='display: none;'>Mã loại thuế</td>"); //invisible
            top.push("</tr>");

            var arrObject = [];
            var content = [];
            if (typeof objParam !== 'undefined' && objParam !== null) {
                arrObject = objParam.Item;
                var iCount = 0;
                if (typeof arrObject !== 'undefined' && arrObject !== null && arrObject instanceof Array) {
                    for (var i = 0; i < arrObject.length; i++) {
                        if (arrObject[i]) { //NOT: null, undefined, 0, NaN, false, or ""
                            iCount++;
                            content.push("<tr>");
                            content.push("<td style='text-align: center;'><input type='checkbox' id='HDR_chkSelect_" + iCount + "' onclick='jsCheckTKhai(" + iCount + ");' /></td>");
                            content.push("<td><input type='text' id='HDR_STT_" + iCount + "' style='width: 98%; border-color: White;' ReadOnly='readonly' class='inputflat' value='" + iCount + "' /></td>");
                            content.push("<td><input type='text' id='HDR_SO_TK_" + iCount + "' style='width: 98%; border-color: White;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].So_TK + "' /></td>");
                            content.push("<td><input type='text' id='HDR_Ma_HQ_PH_" + iCount + "' style='width: 98%; border-color: White;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].Ma_HQ_PH + "' /></td>");
                            content.push("<td><input type='text' id='HDR_Ma_HQ_CQT_" + iCount + "' style='width: 98%; border-color: White;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].Ma_HQ_CQT + "' /></td>");
                            content.push("<td><input type='text' id='HDR_Ma_NTK_" + iCount + "' style='width: 98%; border-color: White;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].Ma_NTK + "' /></td>");
                            content.push("<td><input type='text' id='HDR_TKKB_" + iCount + "' style='width: 98%; border-color: White;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].TKKB + "' /></td>");
                            content.push("<td><input type='text' id='HDR_DuNo_TO_" + iCount + "' style='width: 98%; border-color: White; text-align: right;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].DuNo_TO + "' /></td>");
                            content.push("<td style='display: none;'><input type='text' id='HDR_MST_" + iCount + "' style='width: 98%; border-color: White;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].Ma_DV + "' /></td>");
                            content.push("<td style='display: none;'><input type='text' id='HDR_Ma_LT_" + iCount + "' style='width: 98%; border-color: White;' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].Ma_LT + "' />");
                            content.push("<input type='text' id='HDR_TTNo_" + iCount + "' style='width: 98%; ' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].TTNo + "' />");
                            content.push("<input type='text' id='HDR_TTNo_CT_" + iCount + "' style='width: 98%; ' ReadOnly='readonly' class='inputflat' value='" + arrObject[i].TTNo_CT + "' /></td>");
                            content.push("</tr>");
                        }
                    }
                }
            }

            var bottom = "</table>";

            $('#divResultTruyVan').html(top.join("") + content.join("") + bottom);
            jsFormatNumberKQTruyVanGrid();
        }

        function jsCheckTKhai(param) {
            //lấy thông tin mã và tên căn cứ theo msg HQ trả về chứ ko raise change event để lấy theo etax
            //Get all checked header rows from hidden field:
            var arrHDRSelected = [];
            if ($('#hdfHDRSelected').val().length > 0) {
                arrHDRSelected = JSON.parse($('#hdfHDRSelected').val());
            }

            //Procedure:
            var grdKQTruyVan = $('#grdKQTruyVan');
            if ($('#HDR_chkSelect_' + param).is(":checked")) { //checked - if compare result is true => add to hdfHDRList
                var sSoTK, sMa_HQ_PH, sMa_HQ_CQT, sMa_NTK, sTKKB, sMaSoThue;

                sSoTK = grdKQTruyVan.find('#HDR_SO_TK_' + param).val().trim();
                sMa_HQ_PH = grdKQTruyVan.find('#HDR_Ma_HQ_PH_' + param).val().trim();
                sMa_HQ_CQT = grdKQTruyVan.find('#HDR_Ma_HQ_CQT_' + param).val().trim();
                sMa_NTK = grdKQTruyVan.find('#HDR_Ma_NTK_' + param).val().trim();
                sTKKB = $('#HDR_TKKB_' + param).val().trim();
                sMaSoThue = $('#HDR_MST_' + param).val().trim();
                sTTNo = $('#HDR_TTNo_' + param).val().trim();
                sTTNo_CT = $('#HDR_TTNo_CT_' + param).val().trim();

                var objTmp = new infHDRCompare(sSoTK, sMa_HQ_PH, sMa_HQ_CQT, sMa_NTK, sTKKB, sMaSoThue, sTTNo, sTTNo_CT);

                var objRetHQ = JSON.parse($('#hdfHQResult').val());
                var arrRetHQ = [];
                if (typeof objRetHQ !== 'undefined' && objRetHQ !== null && objRetHQ.Data !== null) {
                    arrRetHQ = objRetHQ.Data.Item;
                }

                if (arrHDRSelected.length == 0) { //the first time or there are no selected row before

                    var arrDTLtmp = [];

                    arrHDRSelected.push(objTmp);
                    $('#hdfHDRSelected').val(JSON.stringify(arrHDRSelected));
                    if ((typeof arrRetHQ !== 'undefined') && (arrRetHQ !== null) && (arrRetHQ instanceof Array) && (arrRetHQ.length > 0)) {
                        for (var i = 0; i < arrRetHQ.length; i++) {
                            //So_TK la duy nhat cho moi to khai => neu da xac dinh duoc thi out of loop
                            if (arrRetHQ[i].So_TK == sSoTK && arrRetHQ[i].Ma_NTK == sMa_NTK && arrRetHQ[i].Ma_HQ_PH == sMa_HQ_PH && arrRetHQ[i].Ma_HQ_CQT == sMa_HQ_CQT && arrRetHQ[i].TKKB == sTKKB && arrRetHQ[i].TTNo == sTTNo && arrRetHQ[i].TTNo_CT == sTTNo_CT) {
                                //day du lieu DTL (bao gom thong tin HDR) vao mang tam arrDTLtmp, phuc vu viec bind DTL grid
                                arrDTLtmp.push(arrRetHQ[i]);
                                break;
                            }
                        }

                        if (typeof arrDTLtmp !== 'undefined' && arrDTLtmp !== null) {
                            //Bind HDR panel
                            if (arrDTLtmp && arrDTLtmp.length > 0) {
                                jsBindKQTruyVanHDR_Panel(arrDTLtmp[0]);
                                jsAddRowDTL_Grid2(arrDTLtmp);
                            } else {
                                jsBindKQTruyVanHDR_Panel(null);
                            }
                        }

                    }
                } else { //next time

                    var arrDTLtmp = [];
                    var diffProp = { msgRet: "" };
                    if (jsCheckTKhaiSameHeader(objTmp, "SoTK", ",", arrHDRSelected, diffProp)) {
                        //check: if not exists this item in array => add it to array
                        if (!jsCheckObjExistsInArray(objTmp, null, null, arrHDRSelected)) {

                            arrHDRSelected.push(objTmp);
                            $('#hdfHDRSelected').val(JSON.stringify(arrHDRSelected));
                            //And also bind detail grid view
                            if ((typeof arrRetHQ !== 'undefined') && (arrRetHQ !== null) && (arrRetHQ instanceof Array) && (arrRetHQ.length > 0)) {
                                if (typeof objTmp !== 'undefined' && objTmp !== null) {
                                    for (var i = 0; i < arrRetHQ.length; i++) {
                                        if (arrRetHQ[i].So_TK == objTmp.SoTK && arrRetHQ[i].Ma_NTK == objTmp.Ma_NTK && arrRetHQ[i].Ma_HQ_PH == objTmp.Ma_HQ_PH && arrRetHQ[i].Ma_HQ_CQT == objTmp.Ma_HQ_CQT && arrRetHQ[i].TKKB == objTmp.TKKB && arrRetHQ[i].TTNo == objTmp.TTNo && arrRetHQ[i].TTNo_CT == objTmp.TTNo_CT) {
                                            arrDTLtmp.push(arrRetHQ[i]);
                                            break;
                                        }
                                    }
                                    jsAddRowDTL_Grid2(arrDTLtmp); //add them row vao grid
                                }
                            }
                        }
                    } else {
                        if (diffProp && diffProp.msgRet) {
                            alert("Tờ khai này không cùng thông tin " + diffProp.msgRet + " với các tờ khai đã chọn: ");
                        } else {
                            alert("Tờ khai này không cùng thông tin chung với các tờ khai đã chọn");
                        }
                        $('#HDR_chkSelect_' + param).prop('checked', false);
                    }
                }

                //Them doan tinh toan lai so thu tu but toan
                jsGenThuTuButToan();

            } else {    //uncheck
                var sSoToKhai = '';
                sSoToKhai = grdKQTruyVan.find('#HDR_SO_TK_' + param).val();
                if (sSoToKhai) {
                    jsRemoveRowDTL_Grid2(sSoToKhai);

                    var arrHDRSelectTmp = [];
                    for (var j = 0; j < arrHDRSelected.length; j++) {
                        if (arrHDRSelected[j].SoTK !== sSoToKhai) {
                            arrHDRSelectTmp.push(arrHDRSelected[j]);
                        }
                    }
                    arrHDRSelected = arrHDRSelectTmp;
                }
                $('#hdfHDRSelected').val(JSON.stringify(arrHDRSelected));

                //if HDR selected is empty => clear form
                if (arrHDRSelected.length <= 0) {
                    jsClearHDR_Panel();
                }

            }
            jsCalcTotalMoney();
            jsCalCharacter();

            getNoiDungByMaTieuMuc();

            changeNT();
            jsConvertCurrToText();
        }

        function jsCheckTKhaiSameHeader(object, propExcept, splitChar, array, retVar) {
            var sRet = false;
            sRet = jsCheckObjExistsInArrayWithRetVar(object, propExcept, splitChar, array, retVar);

            return sRet;
        }

        function jsCheckStringInArray(str, arr) {
            if (str) {
                if (arr && arr.length > 0) {
                    for (var item in arr) {
                        if (str.trim() == arr[item].toString().trim()) {
                            return true; //break for
                        }
                    }
                }
            }

            return false;
        }

        function jsCheckObjExistsInArray(attr, value, arrTmp) {
            $.each(arrTmp, function (index, item) {
                if (item[attr].toString() == value.toString()) {
                    return true;     // break the $.each() loop
                }
            });
            return false;
        }

        function jsCheckObjExistsInArray(obj, propExcept, splitChar, array) {
            var sRet = true;
            var arrExcept = [];
            if (propExcept) {
                if (splitChar) {
                    arrExcept = propExcept.split(splitChar);
                } else {
                    arrExcept.push(propExcept);
                }
            }

            loopArray:
                for (var key in array) {
                    var arr = array[key];
                    for (var prop in obj) {
                        if (arrExcept && arrExcept.length > 0) {
                            if (!jsCheckStringInArray(prop, arrExcept)) {
                                if (obj.hasOwnProperty(prop)) {
                                    if (obj[prop].toString() !== arr[prop].toString()) {
                                        sRet = false;
                                        break loopArray;
                                    }
                                }
                            }
                        } else {
                            if (obj.hasOwnProperty(prop)) {
                                if (obj[prop].toString() !== arr[prop].toString()) {
                                    sRet = false;
                                    break loopArray;
                                }
                            }
                        }
                    }
                }

            return sRet;
        }

        function jsCheckObjExistsInArrayWithRetVar(obj, propExcept, splitChar, array, retVar) {
            var sRet = true;
            var arrExcept = [];
            if (propExcept) {
                if (splitChar) {
                    arrExcept = propExcept.split(splitChar);
                } else {
                    arrExcept.push(propExcept);
                }
            }

            loopArray:
                for (var key in array) {
                    var arr = array[key];
                    for (var prop in obj) {
                        if (arrExcept && arrExcept.length > 0) {
                            if (!jsCheckStringInArray(prop, arrExcept)) {
                                if (obj.hasOwnProperty(prop)) {
                                    if (obj[prop].toString() !== arr[prop].toString()) {
                                        sRet = false;
                                        if (retVar) {
                                            retVar.msgRet = prop.toString();
                                        }
                                        break loopArray;
                                    }
                                }
                            }
                        } else {
                            if (obj.hasOwnProperty(prop)) {
                                if (obj[prop].toString() !== arr[prop].toString()) {
                                    sRet = false;
                                    if (retVar) {
                                        retVar.msgRet = prop.toString();
                                    }
                                    break loopArray;
                                }
                            }
                        }
                    }
                }

            return sRet;
        }

        function jsClearHDR_Panel() {
            var today = new Date();

            $('#lblSoCTu').text('');
            $('#txtMaNNT').val('');
            $('#txtTenNNT').val('');
            $('#txtDChiNNT').val('');
            $('#txtHuyen_NNT').val('');
            $('#txtTenHuyen_NNT').val('');
            $('#txtMaHQ').val('');
            $('#txtTenMaHQ').val('');
            $('#txtMaCQThu').val('');
            $('#txtTenCQThu').val('');
            $('#txtMaHQPH').val('');
            $('#txtTenMaHQPH').val('');
            $('#txtSHKB').val('');
            $('#txtTenKB').val('');
            $('#txtMaDBHC').val('');
            $('#txtTenDBHC').val('');
            $('#ddlMa_NTK').val($('#ddlMa_NTK option:first').val());
            $('#txtMaNNTien').val('');
            $('#txtTenNNTien').val('');
            $('#txtDChiNNTien').val('');
            $('#txtQuan_HuyenNNTien').val('');
            $('#txtTenQuan_HuyenNNTien').val('');
            $('#txtTinh_NNTien').val('');
            $('#txtTenTinh_NNTien').val('');
            if (typeof dtmNgayLV !== 'undefined' && dtmNgayLV !== null) {
                $('#txtNGAY_HT').val(dtmNgayLV);
            } else {
                $('#txtNGAY_HT').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
            }
            //$('#ddlMaNT').val($('#ddlMaNT option:first').val());
            $("#ddlMaNT option[value='VND']").prop('selected', true);
            $('#txtTyGia').val('');
            //$('#ddlMaLoaiTienThue').val($('#ddlMaLoaiTienThue option:first').val());
            $('#txtTK_KH_NH').val('');
            $('#txtTenTK_KH_NH').val('');
            //$('#txtNGAY_KH_NH').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
            $('#txtDesNGAY_KH_NH').val('');
            //$('#ddlMA_NH_A').val('');
            $('#txtMA_NH_TT').val('');
            $('#txtTEN_NH_TT').val('');
            $('#txtMA_NH_B').val('');
            $('#txtTenMA_NH_B').val('');
            $('#txtTongTien').val('');
            $('#txtLyDoHuy').val('');

            $('#txtCmtnd').val('');
            $('#txtDienThoai').val('');
            $('#txtTongTienNT').val('');

            $('#txtSoDu_KH_NH').val('');
            $('#txtDesc_SoDu_KH_NH').val('');
            $('#ddlPT_TT').val('01');

            $('#txtTKGL').val('');
            $('#txtTenTKGL').val('');
            $('#ddlMaTKCo').val('');
            $('#txtMaCuc').val('');
            $('#txtTenCuc').val('');

            $('#txtDienGiai').val('');
            $('#txtDienGiaiHQ').val('');

            $('#txtGhiChu').val('');

            $('#txtCharge').val('');
            $('#txtVAT').val('');
            $('#txtTongTrichNo').val('');

            onChangePTTT();
        }

        function jsBindKQTruyVanHDR_Panel(obj) {
            //Caution: get thong tin tu msg HQ tra ve, ko lay thong tin tu etax
            if (obj) {
                var today = new Date();
                $('#txtMaNNT').val(obj.Ma_DV);
                $('#txtTenNNT').val(obj.Ten_DV);
                $('#txtDChiNNT').val('');
                $('#txtHuyen_NNT').val('');
                $('#txtMaHQ').val(obj.Ma_HQ);
                //$('#txtMaHQ').val(obj.Ma_HQ);
                $('#txtTenMaHQ').val(obj.Ten_HQ);
                $('#txtMaCQThu').val(obj.Ma_HQ_CQT);
                $('#txtTenCQThu').val(obj.Ten_HQ_PH);
                $('#txtMaHQPH').val(obj.Ma_HQ_PH);
                $('#txtTenMaHQPH').val(obj.Ten_HQ_PH);
                $('#txtSHKB').val(obj.Ma_KB);
                $('#txtMaCuc').val(obj.Ma_Cuc);
                $('#txtTenCuc').val(obj.Ten_Cuc);

                $('#txtDienGiai').val(obj.REMARKS);
                $('#txtDienGiaiHQ').val(obj.DIENGIAI_HQ);

                //Handx =>SHKB => lay ra Ma ngan hang
                jsGetTTNganHangNhan();

                //$('#txtSHKB').val(obj.Ma_KB);
                $('#txtTenKB').val(obj.Ten_KB);
                $('#txtMaDBHC').val('');
                $('#txtTenDBHC').val('');
                //$('#ddlMa_NTK').val(obj.Ma_NTK);
                $('#ddlMa_NTK').val(obj.Ma_NTK);
                //$('#ddlMaTKNo').val('');
                $('#ddlMaTKCo').val(obj.TKKB);
                $('#txtMaNNTien').val('');
                $('#txtTenNNTien').val('');
                $('#txtDChiNNTien').val('');
                $('#txtQuan_HuyenNNTien').val('');
                $('#txtTinh_NNTien').val('');
                $('#ddlMaLoaiThue').val(obj.Ma_LT);
                if (typeof dtmNgayLV !== 'undefined' && dtmNgayLV !== null) {
                    $('#txtNGAY_HT').val(dtmNgayLV);
                } else {
                    $('#txtNGAY_HT').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
                }
                //$('#ddlMaNT').val($('#ddlMaNT option:first').val());
                $("#ddlMaNT option[value='VND']").prop('selected', true);
                //$('#ddlMaLoaiTienThue').val(obj.Ma_LT);
                $('#txtTK_KH_NH').val('');
                //$('#txtNGAY_KH_NH').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
                $('#ddlMA_NH_A').val($('#ddlMA_NH_A option:first').val());
                //$('#txtMA_NH_TT').val('');
                //$('#txtMA_NH_B').val('');
                $('#txtTongTien').val(obj.DuNo_TO);
                $('#txtLyDoHuy').val('');

                $('#txtCmtnd').val('');
                $('#txtDienThoai').val('');

                $('#txtTongTienNT').val(obj.DuNo_TO);
                $('#txtTyGia').val(1);
                $('#txtGhiChu').val(obj.Ten_KB);
               // jsLoadByCQT_Default();
            } else {
                jsClearHDR_Panel();
            }

            jsCalCharacter();
        }

        function jsBindHDR_Panel(obj) {
            //jsClearHDR_Panel();

            if (typeof obj !== 'undefined' && obj !== null) {
                //            if (obj.LOAI_CTU == 'T') {
                //                $('#lblSoCTu').html('  - Số CT: ' + obj.So_CT + ' - Số FT: ' + obj.So_CT_NH + ' - Loại chứng từ: Thuế');
                //            } else if (obj.LOAI_CTU == 'P') {
                //            $('#lblSoCTu').html('  - Số CT: ' + obj.So_CT + ' - Số FT: ' + obj.So_CT_NH + ' - Loại chứng từ: Lệ phí');
                //        }
                //
                $('#lblSoCTu').html(' - Số CT: ' + obj.So_CT + ' - Số FT: ' + obj.So_CT_NH);
                $('#txtMaNNT').val(obj.Ma_NNThue);
                $('#txtTenNNT').val(obj.Ten_NNThue);
                $('#txtDChiNNT').val(obj.DC_NNThue);
                $('#txtHuyen_NNT').val(obj.Ma_Huyen).change();
                //$('#txtTenHuyen_NNT').val(obj.Huyen_nnthue);

                $('#txtMaCuc').val(obj.MA_CUC);
                $('#txtTenCuc').val(obj.TEN_CUC);

                $('#txtDienGiai').val(obj.REMARKS);
                $('#txtDienGiaiHQ').val(obj.DIENGIAI_HQ);

                $('#txtTinh_NNT').val(obj.Ma_Tinh);

                $('#txtMaHQ').val(obj.MA_HQ);
                $('#txtTenMaHQ').val(obj.TEN_HQ);
                $('#txtMaCQThu').val(obj.Ma_CQThu);
                $('#txtTenCQThu').val(obj.Ten_cqthu);
                $('#txtMaHQPH').val(obj.Ma_hq_ph);
                $('#txtTenMaHQPH').val(obj.TEN_HQ_PH);
                $('#txtSHKB').val(obj.SHKB);
                //$('#txtSHKB').val(obj.SHKB);
                $('#txtTenKB').val(obj.Ten_kb);

                $('#txtMaDBHC').val(obj.Ma_Xa).change();/*change de lay len ten dia ban hanh chinh*/
                //$('#txtTenDBHC').val(obj.Ten_Xa);
                //Tai khoan GL
                $('#txtTKGL').val(obj.TK_GL_NH).change();

                $('#ddlMa_NTK').val(obj.MA_NTK);
                $('#ddlMaTKCo').val(obj.TK_Co);
                $('#txtMaNNTien').val(obj.Ma_NNTien);
                $('#txtTenNNTien').val(obj.Ten_NNTien);
                $('#txtDChiNNTien').val(obj.DC_NNTien);
                $('#txtQuan_HuyenNNTien').val(obj.MA_QUAN_HUYENNNTIEN).change();
                //$('#txtTenQuan_HuyenNNTien').val(obj.QUAN_HUYEN_NNTIEN);
                $('#txtTinh_NNTien').val(obj.MA_TINH_TPNNTIEN);
                $('#txtTenTinh_NNTien').val(obj.TINH_TPHO_NNTIEN);
                $('#ddlMaLoaiThue').val(obj.Ma_LThue);
                $('#txtNGAY_HT').val(obj.Ngay_HT);
                $('#ddlMaNT').val(obj.Ma_NT);
                $('#txtTyGia').val(obj.Ty_Gia);
                //$('#ddlMaLoaiTienThue').val(obj.LOAI_TT);
                $('#txtTK_KH_NH').val(obj.TK_KH_NH);
                $('#txtTenTK_KH_NH').val(obj.TEN_KH_NH);
                //$('#txtNGAY_KH_NH').val(obj.NGAY_KH_NH);
                $('#txtDesNGAY_KH_NH').val('');
                if (typeof obj.PT_TT !== 'undefined' && obj.PT_TT !== null && obj.PT_TT !== '') {
                    $('#ddlPT_TT').val(obj.PT_TT);
                }
                $('#ddlMA_NH_A').val(obj.MA_NH_A);
                $('#txtMA_NH_TT').val(obj.MA_NH_TT);
                $('#txtTEN_NH_TT').val(obj.TEN_NH_TT);
                $('#txtMA_NH_B').val(obj.MA_NH_B);
                $('#txtTenMA_NH_B').val(obj.Ten_NH_B);
                $('#txtTongTien').val(accounting.formatNumber(obj.TTien));
                $('#txtLyDoHuy').val(obj.Ly_Do);

                $('#txtCmtnd').val('');
                $('#txtDienThoai').val('');
                $('#txtTK_KH_NH').val('');
                $('#txtTenTK_KH_NH').val('');
                $('#txtVAT').val(accounting.formatNumber(obj.PHI_VAT));
                $('#txtCharge').val(accounting.formatNumber(obj.PHI_GD));

                if (obj.Ma_NT != 'VND')
                    $('#txtTongTienNT').val(accounting.formatNumber(obj.TTien_NT, 2, ",", "."));
                else
                    $('#txtTongTienNT').val(accounting.formatNumber(obj.TTien_NT));

                if (obj.PT_TT == "03") {
                    $('#txtTKGL').val(obj.TK_KH_NH);
                    $('#txtTenTKGL').val(obj.TEN_KH_NH);
                    //jsGet_TenTKGL();

                    $('#rowTKGL').css("display", "");
                    $('#rowTK_KH_NH').css("display", "none");
                    $('#rowSoDuNH').css("display", "none");
                    //$('.trChuyenKhoan').css("display","none");
                } else if (obj.PT_TT == "01" || obj.PT_TT == "05") {
                    $('#txtTK_KH_NH').val(obj.TK_KH_NH);
                    $('#txtTenTK_KH_NH').val(obj.TEN_KH_NH);

                    $('#rowTKGL').css("display", "none");
                    $('#rowTK_KH_NH').css("display", "");
                    $('#rowSoDuNH').css("display", "");
                    //$('.trChuyenKhoan').css("display","");
                }
                if (obj.PT_TT == "01") {
                    jsGetTK_KH_NH();
                }

                $('#hdfSoBT').val(obj.So_BT);


                //shb
                $('#txtGhiChu').val(obj.Ghi_chu);
            }
        }

        function jsBindDTL_Grid(objHDR, arrDTL) {
            jsClearDTL_Grid();

            var top = [];
            top.push("<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdChiTiet' style='width: 100%; border-collapse: collapse;'>");
            top.push("<tr class='grid_header'>");
            top.push("<td align='center' style='width: 4%;'>Chọn</td>");
            top.push("<td align='center' style='width: 10%;'>Số tờ khai</td>");
            top.push("<td align='center' style='width: 10%;'>Ngày TK (dd/MM/yyyy)</td>");
            top.push("<td align='center' style='width: 5%;'>Mã LT</td>");
            top.push("<td align='center' style='width: 8%;'>Loại hình XNK</td>");
            top.push("<td align='center' style='width: 5%;'>Sắc thuế</td>");
            top.push("<td align='center' style='width: 5%;'>Mã chương</td>");
            top.push("<td align='center' style='width: 5%;'>Tiểu mục</td>");
            top.push("<td align='center' style='width: 24%;'>Nội dung</td>");
            top.push("<td align='center' style='width: 12%;'>Số tiền</td>");
            top.push("<td align='center' style='width: 12%;'>Ngoại tệ</td>");
            top.push("<td align='center' style='display: none;'>Thông tin bút toán</td>");
            top.push("</tr>");

            var content = [];
            if (typeof arrDTL !== 'undefined' && arrDTL !== null && arrDTL instanceof Array && arrDTL.length > 0) {
                for (var i = 0; i < arrDTL.length; i++) {
                    if (arrDTL[i]) { //NOT: null, undefined, 0, NaN, false, or ""
                        content.push("<tr>");
                        content.push("<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + (i + 1) + "' checked='checked' onblur='jsCalCharacter();' onclick='jsCalcTotalMoney(); jsCalCharacter();jsConvertCurrToText();jsGenThuTuButToan();' /></td>");
                        content.push("<td><input type='text' id='DTL_SO_TK_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrDTL[i].SO_TK + "' onblur='jsGenThuTuButToan();jsCalCharacter();' /></td>");
                        content.push("<td><input type='text' id='DTL_Ngay_TK_" + (i + 1) + "' style='width: 98%; border-color: White;'  class='inputflat' onbur='jsCalCharacter();' onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}' value='" + arrDTL[i].NGAY_TK + "' /></td>");
                        content.push("<td><input type='text' id='DTL_MA_LT_" + (i + 1) + "' style='width: 98%;' class='inputflat' onblur='jsCalCharacter();' value='" + arrDTL[i].MA_LT + "' /></td>");
                        content.push("<td><input type='text' id='DTL_Lhxnk_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' onblur='jsCalCharacter();' value='" + arrDTL[i].LH_XNK + "' /></td>");
                        content.push("<td><input type='text' id='DTL_SAC_THUE_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' maxlength='2' onblur='jsCalCharacter();' value='" + arrDTL[i].SAC_THUE + "' /></td>");
                        content.push("<td><input type='text' id='DTL_MA_CHUONG_" + (i + 1) + "' maxlength='3' style='width: 98%; border-color: White;' class='inputflat' onblur='jsCalCharacter();' value='" + arrDTL[i].Ma_Chuong + "' /></td>");
                        content.push("<td><input type='text' id='DTL_Ma_TMuc_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' onblur='jsCalCharacter();' value='" + arrDTL[i].Ma_TMuc + "' onblur='onBlurMaTieuMuc(" + (i + 1) + ")' onkeypress='if (event.keyCode==13){ShowMLNS(1," + (i + 1) + ")}' /></td>");

                        content.push("<td><input type='text' id='DTL_Noi_Dung_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' onblur='jsCalCharacter();' value='" + arrDTL[i].Noi_Dung + "' /></td>");

                        content.push("<td><input type='text' id='DTL_SoTien_" + (i + 1) + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onfocus='this.select()' onkeyup='ValidInteger(this); jsFormatNumber(this);jsCalcTotalMoney();' maxlength='17' onblur='jsCalcTotalMoney(); jsCalCharacter();jsCalcCurrency(" + (i + 1) + ");' value='" + arrDTL[i].SoTien + "' /></td>");
                        content.push("<td><input type='text' id='DTL_SoTienNT_" + (i + 1) + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onblur='jsCalCharacter();jsCalcTotalMoney();' onfocus='this.select()' onkeyup='jsFormatNumber2(this);' maxlength='17' readonly value='" + arrDTL[i].SoTien_NT + "' /></td>");

                        content.push("<td style = 'display: none;'><input type='text' id='DTL_TT_BTOAN_" + (i + 1) + "' style='width: 98%; display: none;' class='inputflat' value='" + arrDTL[i].TT_BTOAN + "' /></td>");
                        content.push("</tr>");
                    }
                }
            }

            var bottom = "</table>";

            $('#pnlCTietTTinTKhai').html(top.join("") + content.join("") + bottom);
            jsFormatNumberDTL_Grid();
        }

        function jsGenSoButToan_DTL() {
            //lấy tạm giá trị cột STT của dòng được check cuối cùng
            var iRet = 0;
            $('#grdKQTruyVan tr').each(function () {
                var ctrCheck = $(this).find("input[id^='HDR_chkSelect_']");
                var ctrInput = null;
                if (ctrCheck) {
                    if (ctrCheck.is(":checked")) {
                        ctrInput = $(this).find("input[id^='HDR_STT_']");
                        if (typeof ctrInput !== 'undefined' && ctrInput !== null && ctrInput.length > 0) {
                            iRet = ctrInput.prop("value");
                        }
                        return false; //break out each loop
                    }
                }
            });

            return iRet;
        }

        function jsFormatNumberKQTruyVanGrid() {
            $('#grdKQTruyVan tr').each(function () {
                var control = $(this).find("input[id^='HDR_DuNo_TO_']");
                if (typeof control !== 'undefined' && control !== null) {
                    control.val(accounting.formatNumber(control.val()));
                }
            });
        }

        function jsFormatNumberDTL_Grid() {
            if ($("#ddlMaNT option:selected").val() != 'VND') {
                $('#grdChiTiet tr').each(function () {
                    var control = $(this).find("input[id^='DTL_SoTien_']");
                    if (typeof control !== 'undefined' && control !== null) {
                        control.val(accounting.formatNumber(control.val()));
                    }

                    var control2 = $(this).find("input[id^='DTL_SoTienNT_']");
                    if (typeof control2 !== 'undefined' && control2 !== null) {
                        control2.val(accounting.formatNumber(control2.val(), 2, ",", "."));
                    }
                });
            }
            else {
                $('#grdChiTiet tr').each(function () {
                    var control = $(this).find("input[id^='DTL_SoTien_']");
                    if (typeof control !== 'undefined' && control !== null) {
                        control.val(accounting.formatNumber(control.val()));
                    }

                    var control2 = $(this).find("input[id^='DTL_SoTienNT_']");
                    if (typeof control2 !== 'undefined' && control2 !== null) {
                        control2.val(accounting.formatNumber(control2.val()));
                    }
                });
            }
        }

        function jsClearDTL_Grid() {
            $('#pnlCTietTTinTKhai').val('');
        }

        function jsAddDummyRowDTL_Grid(iNumberRows) {
            var sID = jsGenDTLrowID();
            var sAppendRows = '';
            sID++;
            sAppendRows = sAppendRows + '<tr>';
            sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "' onclick='jsCalcTotalMoney(); jsCalCharacter();jsConvertCurrToText();jsGenThuTuButToan();' /></td>";
            //sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "' onclick='jsCalCharacter();jsConvertCurrToText();jsGenThuTuButToan();' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SO_TK_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' onblur='jsGenThuTuButToan();' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ngay_TK_" + sID + "' style='width: 98%; border-color: White;' onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}' class='inputflat' maxlength='10' onblur='javascript:CheckDate(this)' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_LT_" + sID + "' style='width: 98%;' class='inputflat' value='' /></td>";

            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Lhxnk_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SAC_THUE_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' maxlength='2' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_CHUONG_" + sID + "' maxlength='3' style='width: 98%; border-color: White;' class='inputflat' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ma_TMuc_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' onblur='onBlurMaTieuMuc(" + sID + ")' onkeypress='if (event.keyCode==13){ShowMLNS(1," + sID + ")}' /></td>";

            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Noi_Dung_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' /></td>";

            //sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTien_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onkeyup='ValidInteger(this); jsFormatNumber(this);' onblur='jsCalcTotalMoney(); jsCalCharacter();jsCalcCurrency(" + sID + ");' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTien_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onkeyup='ValidInteger(this); jsFormatNumber(this);' onblur='jsCalCharacter();jsCalcCurrency(" + sID + ");' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTienNT_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onkeyup='jsFormatNumber2(this);' onblur='jsCalcTotalMoney();' readonly value='' /></td>";

            sAppendRows = sAppendRows + "<td style = 'display: none;'><input type='text' id='DTL_TT_BTOAN_" + sID + "' style='width: 98%; display: none;' class='inputflat' value='' /></td>";
            sAppendRows = sAppendRows + '</tr>';

            $('#grdChiTiet tr:last').after(sAppendRows);
        }

        function jsRemoveRowDTL_Grid2(sSoTK) {
            $('#grdChiTiet tr').each(function () {
                var ctrl = $(this).find("input[id^='DTL_SO_TK_']");
                if (typeof ctrl !== 'undefined' && ctrl !== null) {
                    if (ctrl.prop("value") == sSoTK) {
                        $(this).remove();
                    }
                }
            });
            jsFormatNumberDTL_Grid();
        }

        function jsGenDTLrowID() {
            //muc dich: sinh ra so ID cao nhat trong cac <tr> detail => tranh trung lap <tr> ID detail 
            var sRet = null;
            var sID = '';
            var arrID = [];

            $('#grdChiTiet tr').each(function () {
                sID = '';
                var ctrInput = $(this).find("input[id^='DTL_SO_TK_']");
                if (ctrInput) {
                    sID = ctrInput.prop("id");
                }
                if (sID && sID.trim() != '') {
                    var arrTmp = sID.trim().split('_');
                    sID = arrTmp[arrTmp.length - 1];
                    if (typeof sID !== 'undefined' && sID !== null && !isNaN(sID)) {
                        arrID.push(parseInt(sID));
                    }
                }
            });
            if (arrID && arrID.length > 0) {
                sRet = Math.max.apply(Math, arrID);
            } else {
                sRet = 0;
            }

            return sRet;
        }

        function jsCalcTotalMoney() {
            var dTotalMoney = 0;
            var dDetailMoney = 0;

            var tongTienNT = 0;
            var ctTongTienNT = 0;
            //get detail
            $('#grdChiTiet tr').each(function () {
                var ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
                var ctrInput = null;
                var ctrInputNT = null;
                if (ctrCheck) {
                    if (ctrCheck.is(":checked")) {
                        ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                        if (ctrInput) {
                            dDetailMoney = 0;
                            dDetailMoney = accounting.unformat(ctrInput.prop("value"));
                            if (!isNaN(dDetailMoney)) {
                                dTotalMoney = dTotalMoney + dDetailMoney;
                            }
                        }
                        $(this).find("input[id^='DTL_SoTienNT_']").val(ctrInput.prop("value"));
                        ctrInputNT = $(this).find("input[id^='DTL_SoTienNT_']");

                        if (ctrInputNT) {
                            ctTongTienNT = 0;
                            ctTongTienNT = accounting.unformat(ctrInputNT.prop("value"), ".");
                            if (!isNaN(ctTongTienNT)) {
                                tongTienNT += ctTongTienNT;
                            }
                        }
                    } else {
                        ctrInput = null;
                        ctrInputNT = null;
                    }
                }
            });

            dblPhi = 0;
            dblVAT = 0;

            if (dTotalMoney > 0 && FEE_RATE != undefined && FEE_MAX != undefined && FEE_MIN != undefined) {
                dblPhi = parseFloat(dTotalMoney) * parseFloat(FEE_RATE);
                if (dblPhi > FEE_MAX) {
                    dblPhi = FEE_MAX;
                }
                if (dblPhi < FEE_MIN) {
                    dblPhi = FEE_MIN;
                }

                dblVAT = parseFloat(dblPhi) * parseFloat(VAT_RATE);
                if (dblVAT > VAT_MAX) {
                    dblVAT = VAT_MAX;
                }
                if (dblVAT < VAT_MIN) {
                    dblVAT = VAT_MIN;
                }
            }


            dblTongTrichNo = parseFloat(dblPhi) + parseFloat(dblVAT) + parseFloat(dTotalMoney);
            $('#txtVAT').val(accounting.formatNumber(dblVAT));
            $('#txtCharge').val(accounting.formatNumber(dblPhi));
            $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();
            $('#txtTongTien').val(accounting.formatNumber(dTotalMoney));

            if ($("#ddlMaNT option:selected").val() != 'VND')
                $('#txtTongTienNT').val(accounting.formatNumber(tongTienNT, 2, ",", "."));
            else
                $('#txtTongTienNT').val(accounting.formatNumber(tongTienNT));
        }


        function ValidDate(dateString) {
            var regex_date = /^\d{1,2}\/\d{1,2}\/\d{4}$/; //pattern: dd/MM/yyyy
            if (!regex_date.test(dateString)) {
                return false;
            }
            var parts = dateString.split("/");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);

            if (year < 1000 || year > 3000 || month == 0 || month > 12) {
                return false;
            }
            var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

            //For leap years
            if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
                monthLength[1] = 29;
            }

            return day > 0 && day <= monthLength[month - 1];
        }

        //    function jsCalCharacter(){
        //        var tempCharater='';
        //        
        //        tempCharater+= "MST" + $get('txtMaNNT').value + " ";
        //        
        //        //get detail
        //        $('#grdChiTiet tr').each(function(){
        //            var ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
        //            var ctrInput = null;
        //            if (ctrCheck) {
        //                if (ctrCheck.is(":checked")){
        //                    ctrInput = $(this).find("input[id^='DTL_SO_TK_']");
        //                    if (ctrInput) {
        //                        tempCharater += "TK:" + ctrInput.prop("value");
        //                    }
        //                    ctrInput = $(this).find("input[id^='DTL_Ngay_TK_']");
        //                    if (ctrInput) {
        //                        tempCharater += " N:" + ctrInput.prop("value");
        //                    }
        //                    ctrInput = $(this).find("input[id^='DTL_Lhxnk_']");
        //                    if (ctrInput) {
        //                        tempCharater += " LH:" + ctrInput.prop("value");
        //                    }
        //                    ctrInput = $(this).find("input[id^='DTL_MA_CHUONG_']");
        //                    if (ctrInput) {
        //                        tempCharater += "(C:" + ctrInput.prop("value");
        //                    }
        //                    ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
        //                    if (ctrInput) {
        //                        tempCharater += " TM:" + ctrInput.prop("value");
        //                    }
        //                    ctrInput = $(this).find("input[id^='DTL_SoTien_']");
        //                    if (ctrInput) {
        //                        tempCharater += " ST:" + accounting.unformat(ctrInput.prop("value")) + ");";
        //                    }
        //                    //tempCharater+= " LT" + document.getElementById("ddlMaLoaiThue").value;
        //                } else {
        //                    ctrInput = null;
        //                }
        //            }
        //        });
        //        
        //        tempCharater+= "CT " + $get('txtTenKB').value;
        //        tempCharater+= " TAI " + $get('txtTenMA_NH_B').value;
        //        
        //        var charNumberLimit = 180;
        //        charNumber = charNumberLimit - tempCharater.length;
        //        document.getElementById('leftCharacter').visible = true;
        //        document.getElementById('leftCharacter').innerHTML = charNumber + "/" + charNumberLimit;        
        //        
        //        if (charNumber <0)
        //        {
        //            return false;
        //        }
        //        else
        //        { 
        //            return true;
        //        }
        //    }

        function jsCheckMaNNT(param) {
            if ((param.value.trim()).length != 0 && (param.value.trim()).length != 10 && (param.value.trim()).length != 13) {
                param.value = "";
                param.focus();
                alert("Mã số thuế không hợp lệ!");
                return;
            }
            jsGet_TenNNT();
        }

        function jsGet_DataByMaHQPH() {
            $get('txtMaHQ').value = '';
            $get('txtTenMaHQ').value = '';
            $get('txtMaCQThu').value = '';
            $get('txtTenCQThu').value = '';
            $get('txtTenMaHQPH').value = '';
            $get('txtSHKB').value = '';
            $get('txtTenKB').value = '';

            PageMethods.Get_TenMaHQ($('#txtMaHQPH').val(), Get_DataByMaHQPH_Complete, Get_DataByMaHQPH_Error);
        }

        function Get_DataByMaHQPH_Complete(result, methodName) {
            jsCheckUserSession(result);
            if (result.length > 0) {
                var arr = result.split(';');
                $('#txtMaHQ').val($get('txtMaHQPH').value);
                $('#txtTenMaHQ').val(arr[1]);
                $('#txtMaCQThu').val(arr[0]);
                $('#txtTenCQThu').val(arr[1]);
                $('#txtTenMaHQPH').val(arr[1]);
                $('#txtSHKB').val(arr[2]); //auto fire jsGetTTNganHangNhan, jsGet_TenKB ...

            }
            jsCalCharacter();
        }
        function Get_DataByMaHQPH_Error(error, userContext, methodName) {
            jsCalCharacter();
        }


        function jsGet_TenNNT() {
            //$get('txtTenNNT').value = '';
            PageMethods.Get_TenNNT($get('txtMaNNT').value, Get_TenNNT_Complete, Get_TenNNT_Error);
        }

        function Get_TenNNT_Complete(result, methodName) {
            //jsCheckUserSession(result);
            if (result.length > 0) {
                $get('txtTenNNT').value = result;
            }
        }
        function Get_TenNNT_Error(error, userContext, methodName) {
            if (error !== null) {

            }
        }

        function jsShowDivStatus(bShow, sMessage) {
            if (bShow) {
                document.getElementById('divStatus').style.display = '';
                document.getElementById('divStatus').innerHTML = sMessage;
            } else {
                document.getElementById('divStatus').style.display = 'none';
                document.getElementById('divStatus').innerHTML = sMessage;
            }
        }

        function jsShowHideGet_NNThue_HQ(bShow) {
            if (bShow) {
                document.getElementById('divKQTruyVan').style.display = '';
            } else {
                document.getElementById('divKQTruyVan').style.display = 'none';
            }
        }

        function jsShowHideDivProgress(bShow) {
            if (bShow) {
                document.getElementById('divProgress').style.display = '';
            } else {
                document.getElementById('divProgress').innerHTML = 'Đang xử lý. Xin chờ một lát...';
                document.getElementById('divProgress').style.display = 'none';
            }
        }

        function jsHuy_CTU() {
            //jsEnableDisableControlButton('0');
            jsCalCharacter();
            if (true) {
                var sSoCTu, sLyDoHuy;
                sSoCTu = $('#hdfSo_CTu').val();
                sLyDoHuy = $('#txtDienGiaiHQ').val();
                if (typeof sSoCTu !== 'undefined' && sSoCTu !== null && sSoCTu !== '') {
                    if (typeof sLyDoHuy == 'undefined' || sLyDoHuy == null || sLyDoHuy == '') {
                        if (!(curCtuStatus == '00' || curCtuStatus == '03')) {
                            alert('Vui lòng nhập lý do hủy vào trường diễn giải HQ.');
                            return;
                        }

                    }
                    $.ajax({
                        type: 'POST',
                        url: 'frmLapCTu_HQ.aspx/Huy_CTU',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify({
                            sSoCT: sSoCTu,
                            sLyDo: sLyDoHuy || null
                        }),
                        success: function (reponse) {
                            var sRet = reponse.d.split(';');
                            if (typeof sRet !== 'undefined' && sRet !== null && typeof sRet[1] !== 'undefined' && sRet[1] !== '') {
                                var sRetSplit = sRet[1].split('/');
                                if (typeof sRetSplit !== 'undefined' && sRetSplit !== null && sRetSplit.length > 0) {
                                    alert(sRet[0] + ' - Số CT: ' + sRetSplit[1] + ' - Số BT: ' + sRetSplit[0] + ' - SHKB: ' + sRetSplit[2]);
                                    jsEnableDisableControlButton('1');
                                    $('#hdfDescCTu').val('SHKB/SoCT/SoBT:' + +sRet[1]);
                                } else {
                                    alert('Hủy chứng từ không thành công. Vui lòng liên hệ quản lý hệ thống');
                                    $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                                }
                            } else {
                                alert('Hủy chứng từ không thành công. Vui lòng liên hệ quản lý hệ thống');
                                $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                            }
                            $('#hdfIsEdit').val(true);
                            jsLoadCTList();
                        },
                        error: function (request, status, error) {
                            var err;
                            err = JSON.parse(request.responseText);
                            if (err) {
                                if (err.Message.indexOf(';') > -1) {
                                    jsCheckUserSession(err.Message);
                                }
                                alert(err.Message);
                                //jsEnableDisableControlButton('4');
                            }
                        }
                    });
                } else {
                    alert('Không xác định được chứng từ cần hủy');
                }
            }
        }

        function jsValidBeforeGetFormData(sAct) {
            if (sAct == "GHIDC")
                if ($('#txtLyDoHuy').val().length == 0) {
                    alert("Xin vui lòng nhập Lý do điều chỉnh!");
                    $('#txtLyDoHuy').focus();
                    return false;
                }

            return true;
        }

        function jsEnableDisableAddRowButton(sCase) {
            switch (sCase) {
                case "0":
                    $('#btnAddRowDTL').prop('disabled', true);
                    break;
                case "1":
                    $('#btnAddRowDTL').prop('disabled', false);
                    break;
            }
        }

        function jsEnableDisableCtrlBtnByTThaiCTu(sTThaiCTu, sTThaiCThue) {
            switch (sTThaiCTu) {
                case "00": //đã hoàn thiện
                    jsEnableDisableControlButton("6");
                    jsEnableDisableAddRowButton("1");
                    break;
                case "01": //KS thành công
                    if (sTThaiCThue == "1") { //gửi thuế thành công => cho phép gửi điều chỉnh
                        jsEnableDisableControlButton("10");
                        jsEnableDisableAddRowButton("1");
                    } else { //
                        jsEnableDisableControlButton("3");
                        jsEnableDisableAddRowButton("0");
                    }
                    break;
                case "02": //GDV Hủy sau khi hoàn thiện (ko cần qua KSV duyệt)
                    jsEnableDisableControlButton("1");
                    jsEnableDisableAddRowButton("0");
                    break;
                case "03": //Chuyển trả
                    jsEnableDisableControlButton("9");
                    jsEnableDisableAddRowButton("1");
                    break;
                case "04": //Hủy
                    jsEnableDisableControlButton("8");
                    jsEnableDisableAddRowButton("0");
                    break;
                case "05": //đã chuyển kiểm soát
                    jsEnableDisableControlButton("8");
                    jsEnableDisableAddRowButton("0");
                    break;
            }
        }

        function jsEnableDisableControlButton(sCase) {
            switch (sCase) {
                case "0": //disable all button
                    $('#cmdThemMoi').prop('disabled', true);
                    $('#cmdGhiKS').prop('disabled', true);
                    $('#cmdChuyenKS').prop('disabled', true);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', true);
                    break;
                case "1": //cmdThemMoi - btnAddRowDTL
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    $('#cmdChuyenKS').prop('disabled', true);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', true);
                    break;
                case "2": //cmdThemMoi - cmdChuyenKS - cmdInCT - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    $('#cmdChuyenKS').prop('disabled', false);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    break;
                case "3": //cmdThemMoi - cmdInCT - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    $('#cmdChuyenKS').prop('disabled', true);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    break;
                case "4": //cmdThemMoi - cmdGhiKS - cmdChuyenKS
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', false);
                    $('#cmdChuyenKS').prop('disabled', false);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', true);
                    break;
                case "5": //cmdThemMoi - cmdGhiKS - cmdChuyenKS - cmdInCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', false);
                    $('#cmdChuyenKS').prop('disabled', false);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', true);
                    break;
                case "6": //cmdThemMoi - cmdChuyenKS - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    $('#cmdChuyenKS').prop('disabled', false);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', false);
                    break;
                case "7": //cmdThemMoi - cmdChuyenKS - cmdInCT - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    $('#cmdChuyenKS').prop('disabled', false);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    break;
                case "8": //cmdThemMoi - cmdInCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    $('#cmdChuyenKS').prop('disabled', true);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', true);
                    break;
                case "9": //cmdThemMoi - cmdGhiKS - cmdChuyenKS - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', false);
                    $('#cmdChuyenKS').prop('disabled', false);
                    $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', false);
                    break;
                case "10": //cmdThemMoi - cmdDieuChinh - cmdInCT - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    $('#cmdChuyenKS').prop('disabled', true);
                    $('#cmdDieuChinh').prop('disabled', false);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    break;

                default:
                    break;
            }
        }

        function jsGetFloatNumberFromString(str, exceptChar) {
            var fRet = 0;
            if (typeof str !== 'undefined' && str !== null && str.trim() !== '') {

            }

            return fRet;
        }

        function jsThem_Moi() {
            jsEnableDisableControlButton("0");

            jsFillNguyenTe();
            jsFillMA_NH_A();
            jsFillPTTT();
            //console.log('1');
            jsLoadCTList();
            //console.log('2');
            //clear data
            $('#hdfSo_CTu').val('');
            $('#hdfIsEdit').val(false);
            $('#hdfDescCTu').val('');

            $('#hdfHQResult').val('');
            $('#hdfHDRSelected').val('');
            $('#hdfNgayKB').val('');

            jsBindKQTruyVanGrid(null);
            jsClearHDR_Panel();
            jsClearDTL_Grid()
            jsBindDTL_Grid(null, null);

            jsCalCharacter();
            jsEnableDisableControlButton("4");
            jsEnableDisableAddRowButton("1");
        }

        function jsGetCTU(sSoCT) {
            jsShowDivStatus(false, "");
            jsShowHideDivProgress(true);
            jsShowHideGet_NNThue_HQ(false);
            //clear data
            $('#hdfSo_CTu').val('');
            $('#hdfIsEdit').val(false);
            $('#hdfDescCTu').val('');

            $('#hdfHQResult').val('');
            $('#hdfHDRSelected').val('');
            $('#hdfNgayKB').val('');

            jsBindKQTruyVanGrid(null);
            jsClearHDR_Panel();
            jsClearDTL_Grid()
            jsBindDTL_Grid(null, null);

            //NOT: null, undefined, 0, NaN, false, or ""
            if (sSoCT) {
                $.ajax({
                    type: "POST",
                    url: "frmLapCTu_HQ.aspx/GetCTU",
                    cache: false,
                    data: JSON.stringify({
                        "sSoCT": sSoCT
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: GetCtuHQ_OnSuccess,
                    error: function (request, status, error) {
                        jsShowDivStatus(true, request.responseText);
                        var err;
                        err = JSON.parse(request.responseText);
                        if (err) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            jsShowDivStatus(true, err.Message);
                        }

                        jsCalCharacter();
                        //TinhPhi();
                        jsShowHideDivProgress(false);
                    }
                });
            }
        }

        function GetCtuHQ_OnSuccess(response) {
            if (response != null) {
                //Load data
                if (typeof response !== 'undefined' && response !== null) {
                    if (response.d) {
                        if (!jsCheckUserSession(response.d)) {
                            return;
                        }
                        var objCTu = JSON.parse(response.d);
                        if (objCTu) {
                            //Bind HDR panel
                            var HDR = objCTu.HDR;
                            if (typeof HDR !== 'undefined' && HDR !== null) {
                                //Bind hidden field
                                $('#hdfSo_CTu').val(HDR.So_CT);
                                $('#hdfNgayKB').val(HDR.Ngay_KB);
                                curCtuStatus = HDR.Trang_Thai;
                                $('#hdfIsEdit').val(true);
                                var sDescCTu = "SHKB/SoCT/SoBT" + ":" + HDR.SHKB + "/" + HDR.So_CT + "/" + HDR.So_BT;
                                $('#hdfDescCTu').val(sDescCTu);
                                jsEnableDisableCtrlBtnByTThaiCTu(curCtuStatus, HDR.TT_CTHUE);
                            }
                            jsBindHDR_Panel(HDR);
                            //Bind dtl grid
                            var listDTL = objCTu.ListDTL;
                            jsBindDTL_Grid(HDR, listDTL);
                        }
                    }
                }
            }
            //TinhPhi();
            jsCalCharacter();
            TotalMoney();
            jsConvertCurrToText();
            jsShowHideDivProgress(false);

        }

        function jsIn_CT(isBS) {
            var width = screen.availWidth - 100;
            var height = screen.availHeight - 10;
            var left = 0;
            var top = 0;
            var params = 'width=' + width + ', height=' + height;
            params += ', top=' + top + ', left=' + left;
            params += ', directories=no';
            params += ', location=no';
            params += ', menubar=yes';
            params += ', resizable=no';
            params += ', scrollbars=yes';
            params += ', status=no';
            params += ', toolbar=no';

            var strSHKB, strSoCT, strSo_BT, MaNV, ngayLV;
            var descCTu = $('#hdfDescCTu').val();
            if (typeof descCTu !== 'undefined' && descCTu !== null) {
                if (descCTu.indexOf(':') > -1) {
                    strSHKB = $('#hdfDescCTu').val().split(':')[1].split('/')[0];
                    strSoCT = $('#hdfDescCTu').val().split(':')[1].split('/')[1];
                    strSo_BT = $('#hdfDescCTu').val().split(':')[1].split('/')[2];
                    MaNV = curMa_NV;
                    ngayLV = $('#txtNGAY_HT').val();
                }
            }
            // window.open("../../pages/BaoCao/frmInGNT_HQ.aspx?so_ct=" + strSoCT, "", params);
            window.open("../../pages/BaoCao/InGNTpdf_HQ.ashx?so_ct=" + strSoCT, "", params);
        }

        function jsLoadByCQT_Default() {
            $('#txtTenCQThu').val('');
            var strMaCQThu = $get('txtMaCQThu').value;
            var strMaHQ = $get('txtMaHQ').value;
            //console.log("MaHQ: " + strMaHQ);
            var strMaHQPH = $get('txtMaHQPH').value;
            //strMaHQ = ""; //ko lấy kèm Mã HQ
            if (strMaCQThu.length > 0) {
                PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, strMaHQPH, LoadByCQT_Default_Complete, LoadByCQT_Error);
            }
        }

        //jsLoadSHKBByCQT
        function jsLoadSHKBByCQT() {
            $('#txtTenCQThu').val('');
            var strMaCQThu = $get('txtMaCQThu').value;
            var strMaHQ = $get('txtMaHQ').value;
            var strMaHQPH = $get('txtMaHQPH').value;
            if (strMaCQThu.length > 0 || strMaHQ.length > 0 || strMaHQPH.length > 0) {
                PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, strMaHQPH, LoadSHKBByCQT_Complete, LoadSHKBByCQT_Error);
            }
        }
        function LoadSHKBByCQT_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            if (result.lenght != 0) {
                var arr = result.split("|");
                if (arr[2]) {
                    $get('txtTenCQThu').value = arr[2].toString();
                    $('#txtSHKB').val(arr[1]);
                    $('#txtMaCQThu').val(arr[3]);
                    jsGet_TenKB();
                    jsGetTTNganHangNhan();
                } else {
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin cơ quan thu với mã nhập vào';
                }
            }
        }
        function LoadSHKBByCQT_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = 'Không lấy được dữ liệu theo cơ quan thu';
            }
        }
        function jsGetTenKB() {
            $('#txtTenCQThu').val('');
            var strMaCQThu = $get('txtMaCQThu').value;
            var strMaHQ = $get('txtMaHQ').value;
            var strMaHQPH = $get('txtMaHQPH').value;
            if (strMaCQThu.length > 0 || strMaHQ.length > 0 || strMaHQPH.length > 0) {
                PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, strMaHQPH, GetTenKB_Complete, GetTenKB_Error);
            }
        }
        function GetTenKB_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            if (result.lenght != 0) {
                var arr = result.split("|");
                if (arr[2]) {
                    $get('txtTenCQThu').value = arr[2].toString();
                    $('#txtSHKB').val(arr[1]);
                    $('#txtMaCQThu').val(arr[3]);
                    jsGet_TenKB();
                    jsGetTTNganHangNhan();
                } else {
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin tên kho bạc';
                }
            }
        }
        function GetTenKB_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = 'Không lấy được tên kho bạc';
            }
        }
        function jsLoadByCQT() {
            $('#txtTenCQThu').val('');
            var strMaCQThu = "";// $get('txtMaCQThu').value;
            var strMaHQ = "";// $get('txtMaHQ').value;
            var strMaHQPH = $get('txtMaHQPH').value;
            if (strMaCQThu.length > 0 || strMaHQ.length > 0 || strMaHQPH.length > 0) {
                PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, strMaHQPH, LoadByCQT_Complete, LoadByCQT_Error);
            }
        }

        function LoadByCQT_Default_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            if (result.lenght != 0) {
                var arr = result.split("|");

                if (arr[2]) {
                    $get('txtTenCQThu').value = arr[2].toString();
                    $('#txtMaHQ').val(arr[0]);
                    $('#txtMaHQPH').val(arr[0]);
                    $('#txtTenMaHQ').val(arr[2]);
                    $('#txtTenMaHQPH').val(arr[2]);

                } else {
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin cơ quan thu với mã nhập vào';
                }
            }
        }


        function LoadByCQT_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            if (result.lenght != 0) {
                var arr = result.split("|");
                if (arr[2]) {
                    $get('txtTenCQThu').value = arr[2].toString();
                    $('#txtMaHQ').val(arr[0]);
                    $('#txtMaHQPH').val(arr[0]);
                    $('#txtTenMaHQ').val(arr[2]);
                    $('#txtTenMaHQPH').val(arr[2]);

                    $('#txtSHKB').val(arr[1]);
                    $('#txtMaCQThu').val(arr[3]);
                    jsGet_TenKB();
                    jsGetTTNganHangNhan();
                } else {
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin cơ quan thu với mã nhập vào';
                }
            }
        }
        function LoadByCQT_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = 'Không lấy được dữ liệu theo cơ quan thu';
            }
        }
        //Load Ten CQTHU
        function jsLoadTenCQT(txtMaMuc, txtNoiDung) {
            var userContext = {};
            userContext.ControlID = txtNoiDung;

            var strMaNDKT = document.getElementById(txtMaMuc).value;
            if (strMaNDKT.length > 0) {
                PageMethods.Load_TenCQT(strMaNDKT, Load_TenCQT_Complete, Load_TenCQT_Error, userContext);
            }
        }

        function Load_TenCQT_Complete(result, userContext, methodName) {
            checkSS(result);
            if (result.length > 0) {
                document.getElementById(userContext.ControlID).value = result;
                document.getElementById('divStatus').innerHTML = "Tìm thấy Tên cơ quan thu phù hợp";
            } else {
                document.getElementById('divStatus').innerHTML = "Không tìm thấy Tên cơ quan thu phù hợp.Hãy tìm lại";
            }
        }
        function Load_TenCQT_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy nội dung kinh tế" + error.get_message();
            }
        }


        function jsGetTenQuanHuyen_NNThue(ma_xa) {
            //var ma_xa = $('#txtHuyen').val();
            //var ma_xa1 = $get('txtHuyen_NNT').value
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenQuanHuyen(ma_xa, GetTenQuanHuyenNNThue_Complete, GetTenQuanHuyenNNThue_Error);
                jsGetTenTinh_NNThue(ma_xa);
            } else {
                document.getElementById('txtHuyen_NNT').value = "";
                document.getElementById('txtTenHuyen_NNT').value = "";
                document.getElementById('txtTinh_NNT').value = "";
                document.getElementById('txtTenTinh_NNT').value = "";
                jsCalCharacter();
            }
        }

        function GetTenQuanHuyenNNThue_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Quan = result;
                document.getElementById('txtTenHuyen_NNT').value = ten_Quan;
            } else {
                alert('Mã Quận người nộp thuế không chính xác. Vui lòng nhập lại.');
                document.getElementById('txtHuyen_NNT').focus();
                document.getElementById('txtHuyen_NNT').value = "";
                document.getElementById('txtTenHuyen_NNT').value = "";
                document.getElementById('txtTinh_NNT').value = "";
                document.getElementById('txtTenTinh_NNT').value = "";
            }
            jsCalCharacter();
        }

        function GetTenQuanHuyenNNThue_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp thuế không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtHuyen_NNT').focus();
            document.getElementById('txtHuyen_NNT').value = "";
            document.getElementById('txtTenHuyen_NNT').value = "";
            document.getElementById('txtTinh_NNT').value = "";
            document.getElementById('txtTenTinh_NNT').value = "";
            jsCalCharacter();
        }

        function jsGetTenTinh_NNThue(ma_xa) {
            //var ma_xa = $('#txtHuyen_NNT').val();
            $('#txtTinh_NNT').val('');
            $('#txtTenTinh_NNT').val('');
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenTinh(ma_xa, GetTenTinhNNThue_Complete, GetTenTinhNNThue_Error);
            }
        }

        function GetTenTinhNNThue_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtTinh_NNT').value = ten_Tinh[0];
                document.getElementById('txtTenTinh_NNT').value = ten_Tinh[1];
            }
            jsCalCharacter();
        }

        function GetTenTinhNNThue_Error(error, userContext, methodName) {
            jsCalCharacter();
        }

        function jsGetTenQuanHuyen_NNTien(ma_xa) {
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenQuanHuyen(ma_xa, GetTenQuanHuyenNNTien_Complete, GetTenQuanHuyenNNTien_Error);
                jsGetTenTinh_NNTien(ma_xa);
            } else {
                document.getElementById('txtQuan_HuyenNNTien').value = "";
                document.getElementById('txtTenQuan_HuyenNNTien').value = "";
                document.getElementById('txtTinh_NNTien').value = "";
                document.getElementById('txtTenTinh_NNTien').value = "";
            }
        }

        function GetTenQuanHuyenNNTien_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Quan = result;
                document.getElementById('txtTenQuan_HuyenNNTien').value = ten_Quan;
            } else {
                alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
                document.getElementById('txtQuan_HuyenNNTien').focus();
                document.getElementById('txtQuan_HuyenNNTien').value = "";
                document.getElementById('txtTenQuan_HuyenNNTien').value = "";
                document.getElementById('txtTinh_NNTien').value = "";
                document.getElementById('txtTenTinh_NNTien').value = "";
            }
        }

        function GetTenQuanHuyenNNTien_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtQuan_HuyenNNTien').focus();
            document.getElementById('txtQuan_HuyenNNTien').value = "";
            document.getElementById('txtTenQuan_HuyenNNTien').value = "";
            document.getElementById('txtTinh_NNTien').value = "";
            document.getElementById('txtTenTinh_NNTien').value = "";
        }

        function jsGetTenTinh_NNTien(ma_xa) {
            //var ma_xa = $('#txtHuyen_NNT').val();
            $('#txtTinh_NNTien').val('');
            $('#txtTenTinh_NNTien').val('');
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenTinh(ma_xa, GetTenTinhNNTien_Complete, GetTenTinhNNTien_Error);
            }
        }

        function GetTenTinhNNTien_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtTinh_NNTien').value = ten_Tinh[0];
                document.getElementById('txtTenTinh_NNTien').value = ten_Tinh[1];
            }
            jsCalCharacter();
        }
        function GetTenTinhNNTien_Error(error, userContext, methodName) {
            jsCalCharacter();
        }

        function jsGet_TenKB() {
            $('#txtTenKB').val('');
            if (arrSHKB.length > 0) {
                for (var i = 0; i < arrSHKB.length; i++) {
                    var arr = arrSHKB[i].split(';');
                    if (arr[0] == $('#txtSHKB').val()) {
                        $('#txtTenKB').val(arr[1])
                        break;
                    }
                }
            }
            jsCalCharacter();
        }

        function jsGet_TenDBHC() {
            $('#txtTenDBHC').val('');
            if (typeof arrDBHC !== 'undefined' && arrDBHC !== null) {
                if ((arrDBHC instanceof Array) && (arrDBHC.length > 0)) {
                    for (var i = 0; i < arrDBHC.length; i++) {
                        var arr = arrDBHC[i].split(';');
                        if (arr[0] == $('#txtMaDBHC').val()) {
                            $('#txtTenDBHC').val(arr[1]);
                            break;
                        }
                    }
                }
            }
        }

        function jsGet_TenCQThu() {
            $('#txtTenCQThu').val('');
            if (arrCQThu.length > 0) {
                for (var i = 0; i < arrCQThu.length; i++) {
                    var arr = arrCQThu[i].split(';');
                    if (arr[1] == $('#txtMaCQThu').val()) {
                        $('#txtTenCQThu').val(arr[2]);
                        break;
                    }
                }
            }
        }

        function jsFillNguyenTe() {
            var cb = document.getElementById('ddlMaNT');
            cb.options.length = 0;

            if (arrMaNT.length > 0) {
                for (var i = 0; i < arrMaNT.length; i++) {
                    var arr = arrMaNT[i].split(';');
                    var value = arr[0];
                    var label = arr[1];
                    cb.options[cb.options.length] = new Option(label, value);
                }
            }

            $("#ddlMaNT option[value='VND']").prop('selected', true);
        }

        function jsShowTyGia() {
            $('#txtTyGia').val('');
            if (arrTyGia.length > 0) {
                for (var i = 0; i < arrTyGia.length; i++) {
                    var arr = arrTyGia[i].split(';');
                    var value = arr[0];
                    var label = arr[1];
                    if (value == $('#ddlMaNT').val()) {
                        $('#txtTyGia').val(label);
                        break;
                    }
                }
            }
        }

        function jsGetTen_TKNo() {
        }

        function jsGet_TenLHSX() {
            $('#txtDescLHXNK').val('');
            if (arrLHSX.length > 0) {
                for (var i = 0; i < arrLHSX.length; i++) {
                    var arr = arrLHSX[i].split(';');
                    if (arr[0] == $get('txtLHXNK').value.trim()) {
                        $get('txtDescLHXNK').value = arr[1];
                        break;
                    }
                }
            }
        }

        function checkSS(param) {

            var arrKQ = param.split(';');
            if (arrKQ[0] == "ssF") {
                window.open("../../pages/Warning.html", "_self");
                return;
            }
        }

        function jsGet_TenNH_B() {
            $('#txtTenMA_NH_B').val('');
            $('#txtMA_NH_TT').val('');
            $('#txtTEN_NH_TT').val('');
            if (arrTT_NH_B.length > 0) {
                for (var i = 0; i < arrTT_NH_B.length; i++) {
                    var arr = arrTT_NH_B[i].split(';');
                    if (arr[0] == $get('txtMA_NH_B').value.trim() && arr[4] == $get('txtSHKB').value.trim()) {
                        $('#txtTenMA_NH_B').val(arr[1]);
                        $('#txtMA_NH_TT').val(arr[2]);
                        $('#txtTEN_NH_TT').val(arr[3]);
                        break;
                    }
                }
            }
            if ($get('txtMA_NH_TT').value.trim().length <= 0 )
            {
                alert('Mã ngân hàng thụ hưởng (gián tiếp) không chính xác!');
                
            }
        }

        function jsGet_TenNH_TT() {
            //Comment lai vi arrTT_NH_TT chua dc khoi tao o dau ca gay ra loi
            //        if (arrTT_NH_TT.length>0)
            //        {
            //            for (var i=0;i<arrTT_NH_TT.length;i++)
            //            {
            //                var arr = arrTT_NH_TT[i].split(';');
            //                if (arr[0] == $get('txtMA_NH_TT').value.trim()){
            //                    $get('txtTEN_NH_TT').value = arr[1];
            //                    break;
            //                }
            //            }
            //        }
        }

        function jsFillMA_NH_A() {
            var cb = document.getElementById('ddlMA_NH_A');
            cb.options.length = 0;

            if (arrMA_NHA.length > 0) {
                for (var i = 0; i < arrMA_NHA.length; i++) {
                    var arr = arrMA_NHA[i].split(';');
                    var value = arr[0];
                    var label = arr[1];
                    cb.options[cb.options.length] = new Option(label, value);
                }
            }
        }

        function jsFillPTTT() {
        }

        function ShowLov(strType) {
            if (strType == "NNT") return FindNNT_NEW('txtMaNNT', 'txtTenNNT');
            if (strType == "SHKB") return FindDanhMuc('KhoBac', 'txtSHKB', 'txtTenKB', 'txtSHKB');
            if (strType == "DBHC") return FindDanhMuc('DBHCTINH_KB', 'txtMaDBHC', 'txtTenDBHC', 'txtMaDBHC');
            if (strType == "CQT") return FindDanhMuc('CQThu_HaiQuan', 'txtMaCQThu', 'txtTenCQThu', 'txtMTkNo');
            if (strType == "LHXNK") return FindDanhMuc('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSHKB');
            if (strType == "LHXNK") return FindDanhMuc('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSHKB');
            if (strType == "MHQ") return FindDanhMuc('Ma_HaiQuan', 'txtMaHQ', 'txtTenMaHQ', 'txtTenMaHQ');
            if (strType == "MHQPH") return FindDanhMuc('Ma_HaiQuan', 'txtMaHQPH', 'txtTenMaHQPH', 'txtTenMaHQPH');
            if (strType == "DMNH_TT") return FindDanhMuc('DMNH_TT', 'txtMA_NH_TT', 'txtTEN_NH_TT', 'txtTEN_NH_TT');
            if (strType == "DMNH_GT") return FindDanhMuc('DMNH_GT', 'txtMA_NH_B', 'txtTenMA_NH_B', 'txtTenMA_NH_B');
            if (strType == "DBHC_NNT") return FindDanhMuc('DBHCHUYEN', 'txtHuyen_NNT', 'txtTenHuyen_NNT', 'txtHuyen_NNT');//DBHC_NNT
            if (strType == "DBHC_NNTHUE") return FindDanhMuc('DBHCHUYEN', 'txtQuan_HuyenNNTien', 'txtTenQuan_HuyenNNTien', 'txtQuan_HuyenNNTien');//DBHC_NNTHUE
        }

        function FindNNT_NEW(txtID, txtTitle) {
            var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?Src=TK&initParam=" + $get('txtMaNNT').value + "&SHKB=" + $get('txtSHKB').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
        }

        function FindDanhMuc(strPage, txtID, txtTitle, txtFocus) {
            ShowDMKB(strPage, txtID, txtTitle, txtFocus);
            //var strSHKB;
            //var returnValue;
            //if (document.getElementById('txtSHKB').value.length > 0) {
            //    strSHKB = document.getElementById('txtSHKB').value;
            //}
            //else {
            //    strSHKB = defSHKB.split(';')[0];
            //}

            //returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            //if (returnValue != null) {
            //    document.getElementById(txtID).value = returnValue.ID;
            //    if (txtTitle != null) {
            //        document.getElementById(txtTitle).value = returnValue.Title;
            //    }
            //    if (txtFocus != null) {
            //        //if ($get('txtFocus').getAttribute('disabled') != 'disabled') {
            //        try {
            //            document.getElementById(txtFocus).focus();
            //        }
            //        catch (e) { };
            //    }
            //}
        }

        function jsSHKB_lostFocus(strSHKB) {
            jsLoadDM('KHCT', strSHKB);
            jsLoadDM('SHKB', strSHKB);
            //jsLoadDM('DBHC',strSHKB);
            jsLoadDM('CQTHU', strSHKB);
            jsLoadDM('CQTHU_DEF', strSHKB);
            jsLoadDM('DBHC_DEF', strSHKB);
            jsLoadDM('TAIKHOANNO', strSHKB);
            jsLoadDM('TAIKHOANCO', strSHKB);
            jsLoadDM('CQTHU_SHKB', strSHKB);
            jsLoadDM('TINH_SHKB', strSHKB);
        }

        function jsLoadDM(strLoaiDM, strSHKB) {
            if (strSHKB.length == 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            if (strSHKB.length > 0) {
                PageMethods.GetData_SHKB(strLoaiDM, strSHKB, LoadDM_Complete, LoadDM_Error);
            }
            else {
            }
        }

        function LoadDM_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            var mangKQ = new Array;
            mangKQ = result.toString().split('|');

            if (mangKQ[0] == 'TAIKHOANCO') {
                arrTKCoNSNN = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrTKCoNSNN[i - 1] = strTemp;
                }

            }
            else if (mangKQ[0] == 'CQTHU') {
                arrCQThu = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrCQThu[i - 1] = strTemp;
                }
            } else if (mangKQ[0] == 'CQTHU_SHKB') {
                arrCQThu_SHKB = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrCQThu_SHKB[i - 1] = strTemp;
                }
            }
            else if (mangKQ[0] == 'DBHC_DEF') {
                if (mangKQ[1].length != 0) {
                    document.getElementById('txtMaDBHC').value = mangKQ[1].split(';')[0];
                    document.getElementById('txtTenDBHC').value = mangKQ[1].split(';')[1];
                }
                else {
                    document.getElementById('txtMaDBHC').value = '';
                    document.getElementById('txtTenDBHC').value = '';
                }

            }
        }

        function LoadDM_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = 'Không lấy được dữ liệu liên quan đến kho bạc này';
            }
        }

        function jsGetTTNganHangNhan() {
            try {
                var strSHKB = $('#txtSHKB').val();//$get('txtSHKB').value;
                var strDBHC = '';//$get('txtMaDBHC').value;
                var strCQThu = '';//$get('txtMaCQThu').value;            	
                PageMethods.GetTT_NH(strSHKB, GetTT_NganHangNhan_Complete, GetTT_NganHangNhan_Error);
            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTT_NganHangNhan_Complete(result, methodName) {
            try {
                jsCheckUserSession(result);
                if (result.length > 0) {
                    var arr2 = result.split(';');
                    $('#txtMA_NH_B').val(arr2[0]);
                    $('#txtTenMA_NH_B').val(arr2[1]);
                    $('#txtMA_NH_TT').val(arr2[2]);
                    $('#txtTEN_NH_TT').val(arr2[3]);
                    jsValidNHSHB_SP($get('txtSHKB').value);
                    var sMaTinh = $get('txtMA_NH_B').value.substring(0, 2);
                    jsGetTTTinhThanhKB(sMaTinh);
                }
                else {
                    $('#txtMA_NH_B').val('');
                    $('#txtTenMA_NH_B').val('');
                    $('#txtMA_NH_TT').val('');
                    $('#txtTEN_NH_TT').val('');
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin ngân hàng nhận.Hãy kiểm tra lại';
                }
                jsCalCharacter();
            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTT_NganHangNhan_Error(error, userContext, methodName) {
            try {
                $('#txtMA_NH_B').val('');
                $('#txtTenMA_NH_B').val('');
                $('#txtMA_NH_TT').val('');
                $('#txtTEN_NH_TT').val('');

                if (error !== null) {
                    document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng nhận" + error.get_message();
                }
                jsCalCharacter();
            } catch (ex) {
                alert(ex.message);
            }
        }

        function jsGetTTTinhThanhKB(sMaTinh) {
            try {
                if (sMaTinh != "") {
                    PageMethods.GetTTTinhThanhKB(sMaTinh, GetTTTinhThanhKB_Complete, GetTTTinhThanhKB_Error);
                }

            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTTTinhThanhKB_Complete(result, methodName) {
            try {
                jsCheckUserSession(result);
                if (result.length > 0) {
                    var arr2 = result.split(';');
                    $('#txtMaDBHC').val(arr2[0]);
                    $('#txtTenDBHC').val(arr2[1]);
                }
                else {
                    $('#txtMaDBHC').val('');
                    $('#txtTenDBHC').val('');
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin tỉnh thành kho bạc.Hãy kiểm tra lại';
                }
                jsCalCharacter();
            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTTTinhThanhKB_Error(error, userContext, methodName) {
            try {
                $('#txtMaDBHC').val('');
                $('#txtTenDBHC').val('');

                if (error !== null) {
                    document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin tỉnh thành kho bạc : " + error.get_message();
                }
                jsCalCharacter();
            } catch (ex) {
                alert(ex.message);
            }
        }

        function jsGetTK_ddlDSTKKH() {
            $get('txtTK_KH_NH').value = document.getElementById('ddlDSTK_KH').value;
            jsGetTK_KH_NH();
        }

        function jsGetTK_KH_NH() {
            // mask($get('txtTK_KH_NH').value, $get('txtTK_KH_NH'), '3,6,9,17', '-');
            if ($get('txtTK_KH_NH').value.length > 0) {
                if (checkGLBeforGet($get('txtTK_KH_NH').value) == false) {
                    document.getElementById('divStatus').innerHTML = '';
                    var strAccType = '';
                    var intPP_TT = document.getElementById("ddlPT_TT").selectedIndex;
                    //if (intPP_TT == 0)
                    if (intPP_TT == 3) {
                        strAccType = 'TG';
                    } else {
                        strAccType = 'CK';
                    }

                    jsShowHideDivProgress(true);
                    
                    var pttt = $("#ddlPT_TT option:selected").val();
                    //console.log("PT_TT: " + pttt);

                    if (pttt == "01")
                    {
                        PageMethods.GetTK_KH_NH($get('txtTK_KH_NH').value.trim(), GetTK_NH_Complete, GetTK_NH_Error);
                    } else if (pttt == "05")
                    {
                    }
                    
                }
                else {
                    document.getElementById('divStatus').innerHTML = 'Định dạng tài khoản khách hàng không hợp lệ.Hãy nhập lại';
                }
            }
        }

        function checkGLBeforGet(tkGL) {
            for (var i = 0; i < arrGL.length; i++) {
                var arr = arrGL[i].split(';');
                if (arr[0] == tkGL) {
                    isGL = true;
                    glName = arr[1];
                    return true;
                }
            }
            return false;
        }

        function jsShowHideRowProgress(blnShow) {
            if (blnShow) {
                document.getElementById("divProgress").style.display = '';
            } else {
                document.getElementById("divProgress").style.display = 'none';
            }
        }
        function loadXMLString(txt) {
            if (window.DOMParser) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(txt, "text/xml");
            }
            else // code for IE
            {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(txt);
            }
            return xmlDoc;
        }
        function GetTK_NH_Complete(result, methodName) {
            checkSS(result);
            jsShowHideRowProgress(false);

            if (result.length > 0) {
                var xmlDoc = loadXMLString(result);
                var rootCTU_HDR = xmlDoc.getElementsByTagName('PARAMETER')[0];
                if (rootCTU_HDR != null) {

                    if (xmlDoc.getElementsByTagName("RETCODE")[0].childNodes[0].nodeValue == "00000") {
                        if (xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes != null) $get('txtTK_KH_NH').value = xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes[0].nodeValue;
                        if (xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes != null) $get('txtSoDu_KH_NH').value = xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes[0].nodeValue;
                        $('#txtSoDu_KH_NH').val(accounting.formatNumber($get('txtSoDu_KH_NH').value));
                        //jsFormatNumber('txtSoDu_KH_NH');
                        if (rootCTU_HDR.getElementsByTagName("CRR_CY_CODE")[0].childNodes.length != 0) $get('hdLimitAmout').value = rootCTU_HDR.getElementsByTagName("CRR_CY_CODE")[0].childNodes[0].nodeValue;
                        else $get('hdLimitAmout').value = "0";
                    } else {
                        $get('txtTK_KH_NH').value = '';
                        $get('txtTenTK_KH_NH').value = '';
                        $get('txtSoDu_KH_NH').value = '';
                        document.getElementById('divStatus').innerHTML = rootCTU_HDR.getElementsByTagName("ERRCODE")[0].firstChild.nodeValue;
                    }
                }
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình truy vấn số dư tài khoản.';
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
            }

        }

        function GetTK_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
            }
        }

        function jsGetTenTK_KH_NH() {
            if ($get('txtTK_KH_NH').value.length > 0) {
                document.getElementById('divStatus').innerHTML = '';
                var strAccType = '';
                var intPP_TT = document.getElementById("ddlPT_TT").selectedIndex;
                var ma_NT = document.getElementById("txtTenNT").value;
                if (intPP_TT == 3) {
                    strAccType = 'TG';
                } else {
                    strAccType = 'CK';
                }
                jsShowHideRowProgress(true);
                PageMethods.GetTEN_KH_NH($get('txtTK_KH_NH').value.trim(), GetTEN_KH_NH_Complete, GetTEN_KH_NH_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Tài khoản khách hàng không được để trống';
            }
        }

        function GetTEN_KH_NH_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length > 0) {
                $get('txtTenTK_KH_NH').value = result;
            } else {
                document.getElementById('divStatus').innerHTML = 'Không lấy được tên tài khoản khách hàng';
                $get('txtTenTK_KH_NH').value = '';
            }
        }
        function GetTEN_KH_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                $get('txtTenTK_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
            }
        }

        function changeNT() {
            maTienTe = $("#ddlMaNT option:selected").val();
            $("#txtTenNT").val(maTienTe);

            var arr = arrTyGia;
            var toFix = 0;
            var tyGia = 1;
            for (var i = 0; i < arrTyGia.length; i++) {
                var arr = arrTyGia[i].split(';');
                var value = arr[0];
                tyGia = arr[1];
                toFix = arr[2];
                if (value == maTienTe) {
                    $('#txtTyGia').val(accounting.formatNumber(tyGia));
                    break;
                }
            }

            if (maTienTe != 'VND') {
                $('#grdChiTiet > tbody  > tr').each(function () {
                    var ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                    var ctrInput2 = $(this).find("input[id^='DTL_SoTienNT_']");
                    if (ctrInput) {
                        var tienVnd = accounting.unformat(ctrInput.prop("value"));
                        if (tienVnd > 0) {
                            var tienNT = (tienVnd / tyGia).toFixed(toFix);
                            ctrInput2.val(accounting.formatNumber(tienNT, toFix, ",", "."));
                        } else {
                            ctrInput.val("0");
                            ctrInput2.val("0");
                        }
                    }
                });
            }
            else {
                $('#grdChiTiet > tbody  > tr').each(function () {
                    var ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                    var ctrInput2 = $(this).find("input[id^='DTL_SoTienNT_']");

                    if (ctrInput) {
                        ctrInput2.val(ctrInput.val());
                    } else {
                        ctrInput.val("0");
                        ctrInput2.val("0");
                    }
                });
            }

            jsCalCharacter();
            jsCalcTotalMoney();
        }

        //    function jsShowCustomerSignature()
        //    {        
        //        if  ($get('txtTenTK_KH_NH').value !='' )
        //        {
        //            window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $get('txtTK_KH_NH').value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        //        }
        //        else{
        //             alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');}
        //    }

        function jsShowCustomerSignature() {
            if ($get('txtTenTK_KH_NH').value != '') {
                var cifNo = $get('cifNo').value;

                window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $get('txtTK_KH_NH').value + "&cifNo=" + cifNo, "", "dialogWidth:700px;dialogHeight:500px;help:0;status:0;", "_new");
            } else {
                alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');
            }
        }

        function ValidNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function ValidInteger(obj) {
            //alert(obj.value);
            var i, strVal, blnChange;
            blnChange = false
            strVal = "";

            for (i = 0; i < (obj.value).length; i++) {
                switch (obj.value.charAt(i)) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9": strVal = strVal + obj.value.charAt(i);
                        break;
                    default: blnChange = true;
                        break;
                }
            }
            if (blnChange) {
                obj.value = strVal;
            }
        }

        function jsConvertCurrToText() {
            var vTongTien = accounting.unformat($('#txtTongTrichNo').val());
            document.getElementById('divBangChu').innerHTML = 'Bằng chữ : ' + So_chu(vTongTien);
        }

        String.prototype.replaceAll = function (strTarget, strSubString) {
            var strText = this;
            var intIndexOfMatch = strText.indexOf(strTarget);
            while (intIndexOfMatch != -1) {
                strText = strText.replace(strTarget, strSubString)
                intIndexOfMatch = strText.indexOf(strTarget);
            }
            return (strText);
        }

        function jsFormatNumber(control) {
            var number = accounting.unformat(control.value);
            control.value = accounting.formatNumber(number);
        }

        function jsFormatNumber2(control) {
            var number = accounting.unformat(control.value, ".");
            if ($("#ddlMaNT option:selected").val() != "VND")
                control.value = accounting.formatNumber(number, 2, ",", ".");
            else
                control.value = accounting.formatNumber(number);
        }

        function onChangePTTT() {
            var pttt = $("#ddlPT_TT option:selected").val();

            if (pttt == "03") {
                $("#rowSoDuNH").css("display", "none");
                $("#rowTK_KH_NH").css("display", "none");
                $("#rowTKGL").css("display", "");
            } else if (pttt == "01" || pttt == "05") {
                if (pttt == "05") {
                    getSoTK_TruyVanCore(pttt);
                    $get('txtTK_KH_NH').disabled = true;
                }
                else {
                    $get('txtTK_KH_NH').disabled = false;
                }
                $("#rowSoDuNH").css("display", "");
                $("#rowTK_KH_NH").css("display", "");
                $("#rowTKGL").css("display", "none");
            }

        }
        function getSoTK_TruyVanCore(pttt) {
            PageMethods.getSoTK_TruyVanCore(pttt, getSoTK_TruyVanCore_Complete, getSoTK_TruyVanCore_Error);

        }

        function getSoTK_TruyVanCore_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length > 0) {
                $get('txtTK_KH_NH').value = result;
            } else {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }
        function getSoTK_TruyVanCore_Error(error, userContext, methodName) {
            if (error !== null) {
                jsShowHideRowProgress(false);
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }
        //
        function onBlurMaTieuMuc(id) {
            if ($('#DTL_Ma_TMuc_' + id).val().length > 0) {
                $.ajax({
                    type: "POST",
                    url: "frmLapCTu_HQ.aspx/onBlurMaTieuMuc",
                    cache: false,
                    data: JSON.stringify({
                        "v_maTieuMuc": $('#DTL_Ma_TMuc_' + id).val()
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (reponse) {

                        if (reponse.length = 0) {
                            alert("Có lỗi khi Tìm kiếm Sắc thuế, Nội dung theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());

                            $('#DTL_SAC_THUE_' + id).val('');
                            $('#DTL_Noi_Dung_' + id).val('');
                        } else {
                            var result = reponse;
                            var jsonData = JSON.parse(result.d);

                            if (jsonData.length == 1) {
                                if (jsonData[0].STT == 1) {
                                    alert("Có lỗi khi Tìm kiếm Nội dung theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                                    $('#DTL_Noi_Dung_' + id).val('');
                                    $('#DTL_SAC_THUE_' + id).val(jsonData[0].VALUE);
                                } else if (jsonData[0].STT == 2) {
                                    alert("Có lỗi khi Tìm kiếm Sắc thuế theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                                    $('#DTL_Noi_Dung_' + id).val(jsonData[0].VALUE);
                                    $('#DTL_SAC_THUE_' + id).val('');
                                }
                            } else {
                                for (var i = 0; i < jsonData.length; i++) {
                                    if (jsonData[i].STT == 1)
                                        $('#DTL_SAC_THUE_' + id).val(jsonData[i].VALUE);
                                    else if (jsonData[i].STT == 2)
                                        $('#DTL_Noi_Dung_' + id).val(jsonData[i].VALUE);
                                }
                            }
                        }

                    }, error: function (request, status, error) {
                        alert("Có lỗi khi Tìm kiếm Sắc thuế, Nội dung theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                    }
                });
            } else {
                $('#DTL_SAC_THUE_' + id).val('');
                $('#DTL_Noi_Dung_' + id).val('');
            }
        }

        function getNoiDungByMaTieuMuc() {
            var arrTieuMuc = {};
            var sTieuMuc = "";
            $('#grdChiTiet > tbody  > tr').each(function () {
                var ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                var ctrInput2 = $(this).find("input[id^='DTL_Noi_Dung_']");
                if (ctrInput) {
                    var maTieuMuc = ctrInput.val();
                    sTieuMuc += "'" + maTieuMuc + "',";
                    ctrInput2.val('');
                }
            });

            sTieuMuc = sTieuMuc.substring(0, sTieuMuc.length - 1);

            if (arrTieuMuc) {
                $.ajax({
                    type: "POST",
                    url: "frmLapCTu_HQ.aspx/getNoiDungByMaTieuMuc",
                    cache: false,
                    data: JSON.stringify({
                        "v_jSon": sTieuMuc
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (reponse) {
                        if (reponse.d.length == 0) {
                            $('#grdChiTiet > tbody  > tr').each(function () {
                                var ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                                var ctrInput2 = $(this).find("input[id^='DTL_Noi_Dung_']");
                                if (ctrInput) {
                                    ctrInput2.val("");
                                }
                            });
                        } else {
                            var result = reponse;
                            var jsonData = JSON.parse(result.d);

                            for (var i = 0; i < jsonData.length; i++) {
                                $('#grdChiTiet > tbody  > tr').each(function () {
                                    var ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                                    var ctrInput2 = $(this).find("input[id^='DTL_Noi_Dung_']");

                                    if (ctrInput.val() == jsonData[i].MA_TMUC) {
                                        ctrInput2.val(jsonData[i].TEN);
                                    }
                                });
                            }
                        }

                    }
                });
            }
        }

        function jsCalcCurrency(id) {
            var tienVnd = accounting.unformat($("#DTL_SoTien_" + id).val());

            var arr = arrTyGia;
            var toFix = 0;
            var tyGia = 1;
            for (var i = 0; i < arrTyGia.length; i++) {
                var arr = arrTyGia[i].split(';');
                var value = arr[0];
                tyGia = arr[1];
                if (value == $("#ddlMaNT option:selected").val()) {
                    toFix = arr[2];
                    break;
                }
            }

            var tienNT = (tienVnd / tyGia).toFixed(toFix);
            if ($("#ddlMaNT option:selected").val() != "VND")
                $("#DTL_SoTienNT_" + id).val(accounting.formatNumber(tienNT, toFix, ",", "."));
            else
                $("#DTL_SoTienNT_" + id).val(accounting.formatNumber(tienNT));

            jsCalcTotalMoney();
            jsConvertCurrToText();
        }

        function jsCalCharge() {

            if ($('#txtCharge').val() == "")
                $('#txtCharge').val("0");
            var dblPhi = parseFloat(accounting.unformat($('#txtCharge').val()));
            var dblVAT = parseInt(dblPhi / 10);

            var dblTongTien = parseFloat(accounting.unformat($('#txtTongTien').val()));
            var dblTongTrichNo = dblPhi + dblVAT + dblTongTien;

            $('#txtCharge').val(accounting.formatNumber(dblPhi));
            $('#txtVAT').val(accounting.formatNumber(dblVAT));

            $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();
            //jsFormatNumber($get('txtVAT').value); 
            //jsFormatNumber($get('txtTongTrichNo').value);  
            //jsFormatNumber($get('txtCharge').value);

            jsConvertCurrToText();
        }

        function jsEnabledInputCharge() {
            var dblTongTien = parseFloat(accounting.unformat($('#txtTongTien').val()));
            var dblPhi = 0;
            var dblVAT = 0;
            var dblTongTrichNo = 0;
            if (document.getElementById('chkChargeType').checked) {
                $('#txtCharge').disabled = true;
                //neu ma checked thi tinh lai fee
                dblPhi = dblTongTien * feeRate / minFee;
                dblVAT = parseInt(dblTongTien * feeRate / minFee / 10);
                if (dblPhi < minFee) {
                    dblPhi = minFee;
                    //dblVAT=dblPhi/10;
                    dblVAT = parseInt(dblPhi / 10);

                }
                dblTongTrichNo = dblPhi + dblVAT + dblTongTien;
                $('#txtCharge').val(accounting.formatNumber(dblPhi));
                $('#txtVAT').val(accounting.formatNumber(dblVAT));

                $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();

            } else {
                $('#txtCharge').prop('disabled', false);
                $('#txtCharge').val("0");
                $('#txtVAT').val("0");
                //$('#txtTongTrichNo').val($('#txtTongTien').val());
                $('#txtTongTrichNo').val($('#txtTongTien').val()).change();
            }
            jsConvertCurrToText();
        }

        function valInteger(obj) {
            var i, strVal, blnChange;
            blnChange = false
            strVal = "";

            for (i = 0; i < (obj.value).length; i++) {
                switch (obj.value.charAt(i)) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9": strVal = strVal + obj.value.charAt(i);
                        break;
                    default: blnChange = true;
                        break;
                }
            }
            if (blnChange) {
                obj.value = strVal;
            }
        }

        function jsGetTenTinh_KB(ma_xa) {
            $('#txtMaDBHC').val('');
            $('#txtTenDBHC').val('');
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenTinh(ma_xa, GetTenTinh_KB_Complete, GetTenTinh_KB_Error);
            }
        }

        function GetTenTinh_KB_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtMaDBHC').value = ten_Tinh[0];
                document.getElementById('txtTenDBHC').value = ten_Tinh[1];
            }
        }

        function GetTenTinh_KB_Error(error, userContext, methodName) {
        }


        function ShowMLNS(strType, id) {
            if (strType == 1)
                return FindDanhMuc('MucTMuc', 'DTL_Ma_TMuc_' + id, 'DTL_Noi_Dung_' + id, 'DTL_SoTien_' + id);
        }

        function jsGenThuTuButToan() {
            var arrSoTk = [];

            $('#grdChiTiet > tbody  > tr').each(function () {
                var ctrSoTk = $(this).find("input[id^='DTL_SO_TK_']");
                if (typeof ctrSoTk !== 'undefined' && ctrSoTk !== null) {
                    var objSoTk = {};
                    objSoTk["SO_TK"] = ctrSoTk.val();
                    objSoTk["TT_BTOAN"] = "";
                    //Kiem tra xem objGoc co gia tri chua
                    if (arrSoTk.length > 0) {
                        var flag = true;

                        for (var j = 0; j < arrSoTk.length; j++) {
                            var objDtl = arrSoTk[j];
                            if (objSoTk.SO_TK != arrSoTk[j].SO_TK)
                                flag = true;
                            else {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                            arrSoTk.push(objSoTk);
                    } else
                        arrSoTk.push(objSoTk);
                }
            });



            for (var i = 0; i < arrSoTk.length; i++) {
                var objDtl = arrSoTk[i];
                objDtl.TT_BTOAN = i;
            }

            $('#grdChiTiet > tbody  > tr').each(function () {
                var ctrSoTk = $(this).find("input[id^='DTL_SO_TK_']");
                var ctrTtBToan = $(this).find("input[id^='DTL_TT_BTOAN_']");
                if (typeof ctrSoTk !== 'undefined' && ctrSoTk !== null) {
                    for (var jj = 0; jj < arrSoTk.length; jj++) {
                        var objDtl = arrSoTk[jj];
                        if (ctrSoTk.val() == objDtl.SO_TK) {
                            ctrTtBToan.val(objDtl.TT_BTOAN);
                            break;
                        }
                    }
                }
            });
        }

        function jsGhi_CTU2(sAct) {
            $('#cmdChuyenKS').prop('disabled', true);
            var objHDR = jsGetHDRObject2();
            var objDTL = jsGetDTLArray2();
            //-----------------------------------------
            objHDR.SO_CT_NH = '';
            objHDR.NGAY_IMPORT = '';
            objHDR.TEN_FILE = '';
            objHDR.TT_NO = '';
            //-----------------------------------------
            var sType = null;
            jsValidMin();
            var bIsEdit = $('#hdfIsEdit').val();
            if (sAct == "GHI") {
                if (bIsEdit == "true")
                    sType = 'UPDATE';
                else
                    sType = 'INSERT';
            } else if (sAct == "GHIKS") {
                if (bIsEdit == "true")
                    sType = 'UPDATE_KS';
                else
                    sType = 'INSERT_KS';
            } else if (sAct == "GHIDC") {
                if (bIsEdit == "true")
                    sType = 'UPDATE_DC';
            } else
                return;
            if (!jsValidFormData2(objHDR, objDTL, sAct, sType)) {
                $('#cmdChuyenKS').prop('disabled', false);
            }
            jsValidNHSHB_SP_GhiCtu($get('txtSHKB').value);
            if (document.getElementById('hndNHSHB').value.length >= 8 && document.getElementById('hndNHSHB').value != $get('txtMA_NH_B').value) {
                if (!confirm("Kho bạc trên có mở tài khoản tại SHB. Bạn có muốn tiếp tục lưu chứng từ với ngân hàng hưởng đã chọn?")) {
                    jsEnableButtons(1);
                    return;
                }

            }
            if (jsValidFormData2(objHDR, objDTL, sAct, sType)) {
                $.ajax({
                    type: 'POST',
                    url: 'frmLapCTu_HQ.aspx/Ghi_CTu2',
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    data: JSON.stringify({
                        ctuHdr: objHDR,
                        ctuDtl: objDTL,
                        sAction: sAct,
                        sSaveType: sType
                    }),
                    success: function (reponse) {
                        var sRet = reponse.d.split(';');
                        if (sRet && sRet[0] == 'Success') {
                            var sOut = '';
                            if ((sRet[2]) && (sRet[2].indexOf('/') > 0) && (sRet[2].split('/').length > 0)) {
                                sOut = sRet[2].split('/');
                            }

                            switch (sType) {
                                case "INSERT":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Hoàn thiện thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Hoàn thiện thành công!');
                                    }
                                    jsEnableDisableControlButton('6');
                                    break;
                                case "UPDATE":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Cập nhật thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Cập nhật thành công!');
                                    }
                                    jsEnableDisableControlButton('6');
                                    break;
                                case "INSERT_KS":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Hoàn thiện & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Hoàn thiện & gửi kiểm soát thành công!');
                                    }
                                    jsEnableDisableControlButton('7');
                                    break;
                                case "UPDATE_KS":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Cập nhật & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Cập nhât & gửi kiểm soát thành công!');
                                    }
                                    jsEnableDisableControlButton('7');
                                    break;
                                case "UPDATE_DC":
                                    if (true) {
                                        alert('Cập nhật điều chỉnh & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    }
                                    jsEnableDisableControlButton('3');
                                    break;
                            }

                            //re-load lai chung tu len form (trong này đã có set hdfDescCTu)
                            if ((sOut) && (sOut.length > 1)) {
                                jsGetCTU(sOut[1]);
                            } else {
                                jsGetCTU(null);
                            }
                            $('#hdfIsEdit').val(true);
                        } else {
                            $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                            switch (sType) {
                                case "INSERT":
                                    alert('Hoàn thiện không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "UPDATE":
                                    alert('Cập nhật không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "INSERT_KS":
                                    alert('Hoàn thiện & gửi kiểm soát không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "UPDATE_KS":
                                    alert('Cập nhật & gửi kiểm soát không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('3');
                                    break;
                                case "UPDATE_DC":
                                    alert('Cập nhật điều chỉnh & gửi kiểm soát không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('10');
                                    break;
                            }
                        }
                       jsLoadCTList();
                    },
                    error: function (request, status, error) {
                        var err;
                        err = JSON.parse(request.responseText);
                        if (typeof err !== 'undefined' && err !== null) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            alert(err.Message);
                            switch (sType) {
                                case "INSERT":
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "UPDATE":
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "INSERT_KS":
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "UPDATE_KS":
                                    jsEnableDisableControlButton('3');
                                    break;
                                case "UPDATE_DC":
                                    jsEnableDisableControlButton('10');
                                    break;
                            }
                        }
                    }
                });
            }
        }


        function jsValidFormData2(HDR, DTL, sAction, sType) {
            if (HDR.TEN_NNTHUE.length > 70) {
                alert('Tên người nộp thuế > 70 ký tự. Vui lòng kiểm tra lại');
                $('#txtTenNNT').focus();
                return false;
            }
            //valid total characters
            if (!jsCalCharacter()) {
                alert('Vượt quá số ký tự cho phép. Vui lòng kiểm tra lại');
                return false;
            }
            //validate HDR
            if (!HDR.MA_NNTHUE) {
                alert('Vui lòng nhập vào Mã số thuế');
                $('#txtMaNNT').focus();
                return false;
            }
            if (!HDR.MA_HQ) {
                alert('Vui lòng nhập vào Mã HQ');
                $('#txtMaHQ').focus();
                return false;
            }
            //        if (!HDR.TEN_MA_HQ) {
            //            alert('Không tìm thấy thông tin HQ với mã nhập vào');
            //            $('#txtTenMaHQ').focus();
            //            return false;
            //        }
            if (!HDR.MA_CQTHU) {
                alert('Vui lòng nhập vào mã CQT thu');
                $('#txtMaCQThu').focus();
                return false;
            }
            if ($('#txtTenCQThu').val().length == 0) {
                alert('Không tìm thấy thông tin CQT với mã nhập vào');
                $('#txtTenCQThu').focus();
                return false;
            }

            //        if ($('#txtTenCQThu').val().length > 40) {
            //            alert('Tên cơ quan thu > 40 ký tự(lớn hơn ký tự cho phép của core). Vui lòng kiểm tra lại!');
            //            $('#txtTenCQThu').focus();
            //            return false;
            //        }

            if (!HDR.MA_HQ_PH) {
                alert('Vui lòng nhập vào mã HQ phát hành');
                $('#txtMaHQPH').focus();
                return false;
            }
            if (!HDR.TEN_HQ_PH) {
                alert('Không tìm thấy thông tin HQ phát hành với mã nhập vào');
                $('#txtTenMaHQPH').focus();
                return false;
            }
            if (!HDR.SHKB) {
                alert('Vui lòng nhập vào SHKB');
                $('#txtSHKB').focus();
                return false;
            }
            if (!HDR.MA_NTK) {
                alert('Vui lòng nhập vào TK có NSNN');
                $('#ddlMa_NTK').focus();
                return false;
            }
            //txtDChiNNT
            if ($('#txtDChiNNT').val().length == 0) {
                alert('Vui lòng nhập vào thông tin địa chỉ người nộp thuế!');
                $('#txtDChiNNT').focus();
                return false;
            }

            if ($('#txtTenKB').val().length == 0) {
                alert('Không tìm thấy thông tin kho bạc với mã nhập vào');
                $('#txtTenKB').focus();
                return false;
            }
            //        if ($('#txtCharge').val().length == 1) {
            //            alert('Tính phí trước khi ghi chứng từ');
            //            $('#txtCharge').focus();
            //            return false;
            //        }
            if (!HDR.TK_CO) {
                alert('Vui lòng nhập vào tài khoản có');
                $('#ddlMaTKCo').focus();
                return false;
            }

            if (HDR.PT_TT == "03") {
                if (!HDR.TK_GL_NH) {
                    alert('Vui lòng nhập số tài khoản GL');
                    $('#txtTKGL').focus();
                    return false;
                }

            } else if (HDR.PT_TT == "01" || HDR.PT_TT == "05") {
                if (!HDR.TK_KH_NH) {
                    alert('Vui lòng nhập vào mã tài khoản chuyển');
                    $('#txtTK_KH_NH').focus();
                    return false;
                }
                if (document.getElementById("ddlPT_TT").value == "05") {
                    //đối với tiền mặt thì tạm không check số dư
                    isGL = true;
                } else {
                    var total = accounting.unformat($('#txtTongTien').val().replaceAll(',', ''));
                    if (HDR.TT_TTHU > total) {
                        alert('Tổng tiền trích nợ đang lớn hơn số tiền của khách hàng. Vui lòng kiểm tra lại.');
                        return false;
                    }
                }
             
            }
            

            if (document.getElementById("ddlPT_TT").value == "01" || document.getElementById("ddlPT_TT").value == "05") {
                if (jsCheckEmptyVal('txtTK_KH_NH', document.getElementById('divStatus').id, "Tài khoản chuyển không được để trống") == false)
                    return false;
                if (isGL == false) {
                    if (jsValidSoDuKH()) {
                        alert("TK của quý khách không đủ số dư để thực hiện giao dịch");
                        return false;
                    }
                }
            }
            if (document.getElementById("ddlPT_TT").value == "03") {
                if (jsCheckEmptyVal('txtTKGL', document.getElementById('divStatus').id, "Tài khoản chuyển không được để trống") == false)
                    return false;
                if (!checkTKGL()) {
                    alert("Tài khoản GL không đúng");
                    return false;
                }
            }
            if (document.getElementById("ddlPT_TT").value == "00") {
                if (jsCheckEmptyVal('txtTenNNTien', document.getElementById('divStatus').id, "Tên người nộp tiền không được để trống") == false)
                    return false;
            }
            if (!HDR.MA_NH_A) {
                alert('Vui lòng nhập vào thông tin ngân hàng chuyển');
                $('#ddlMA_NH_A').focus();
                return false;
            }
            if (!HDR.MA_NH_TT) {
                alert('Vui lòng nhập vào mã ngân hàng trực tiếp');
                $('#txtMA_NH_TT').focus();
                return false;
            }
            if (!HDR.TEN_NH_TT) {
                alert('Không tìm thấy thông tin ngân hàng trực tiếp với mã nhập vào');
                $('#txtTEN_NH_TT').focus();
                return false;
            }
            if (!HDR.MA_NH_B) {
                alert('Vui lòng nhập vào mã ngân hàng thụ hưởng');
                $('#txtMA_NH_B').focus();
                return false;
            }
            if (!HDR.TEN_NH_B) {
                alert('Không tìm thấy thông tin ngân hàng thụ hưởng với mã nhập vào');
                $('#txtTenMA_NH_B').focus();
                return false;
            }

            //validate DTL       
            if (!(DTL) || !(DTL instanceof Array) || !(DTL.length > 0)) { //IS: null, undefined, 0, NaN, false, or ""
                alert('Chứng từ thuế phải có chi tiết. Vui lòng kiểm tra lại');
                return false;
            }
            var tttien_dtl = 0;
            var tttien_nt_dtl = 0;
            var flag = true;
            for (var j = 0; j < DTL.length; j++) {
                var objDTL = DTL[j];
                var ii = j + 2;
                if (objDTL.SO_TK.length == 0) {
                    alert('Số tờ khai bị đang trống. Xin vui lòng nhập số tờ khai!');
                    flag = false;
                    break;
                }
                if (objDTL.MA_LHXNK.length == 0) {
                    alert('Mã loại hình xuất nhập khẩu bị đang trống. Xin vui lòng nhập loại hình xuất nhập khẩu!');
                    flag = false;
                    break;
                }
                if (objDTL.SAC_THUE.length == 0) {
                    alert('Mã sắc thuế bị đang trống. Xin vui lòng nhập mã sắc thuế!');
                    flag = false;
                    break;
                }
                if (objDTL.MA_CHUONG.length == 0) {
                    alert('Mã chương bị đang trống. Xin vui lòng nhập mã chương!');
                    flag = false;
                    break;
                }
                if (objDTL.MA_TMUC.length == 0) {
                    alert('Mã tiểu mục bị đang trống. Xin vui lòng nhập mã tiểu mục!');
                    flag = false;
                    break;
                }
                if (objDTL.NOI_DUNG.length == 0) {
                    alert('Trường nội dung tiểu mục bị đang trống. Xin vui lòng nhập nội dung!');
                    flag = false;
                    break;
                }
                if (objDTL.SOTIEN.length == 0) {
                    alert('Số tiền nộp thuế bị đang trống. Xin vui lòng nhập số tiền!');
                    flag = false;
                    break;
                } else {
                    if (parseFloat(objDTL.SOTIEN) <= 0) {
                        alert('Số tiền nộp thuế bị đang nhỏ hơn 0. Xin vui lòng nhập số tiền!');
                        flag = false;
                        break;
                    }
                }
                if (objDTL.SOTIEN_NT.length == 0) {
                    alert('Số tiền nộp thuế bị đang trống. Xin vui lòng nhập số tiền!');
                    flag = false;
                    break;
                } else {
                    if (parseFloat(objDTL.SOTIEN_NT) <= 0) {
                        alert('Số tiền nộp thuế bị đang nhỏ hơn 0. Xin vui lòng nhập số tiền!');
                        flag = false;
                        break;
                    }
                }
            }

            for (var j = 0; j < DTL.length; j++) {
                var objDTL = DTL[j];
                var ii = j + 2;
                if (objDTL.SOTIEN.length == 0) {
                    alert('Số tiền nộp thuế bị đang trống. Xin vui lòng nhập số tiền!');
                    flag = false;
                    break;
                } else {
                    if (parseFloat(objDTL.SOTIEN) <= 0) {
                        alert('Số tiền nộp thuế bị đang nhỏ hơn 0. Xin vui lòng nhập số tiền!');
                        flag = false;
                        break;
                    }
                }

                tttien_dtl += parseFloat(objDTL.SOTIEN);
                if (objDTL.SOTIEN_NT.length == 0) {
                    alert('Số tiền nộp thuế bị đang trống. Xin vui lòng nhập số tiền!');
                    flag = false;
                    break;
                } else {
                    if (parseFloat(objDTL.SOTIEN_NT) <= 0) {
                        alert('Số tiền nộp thuế bị đang nhỏ hơn 0. Xin vui lòng nhập số tiền!');
                        flag = false;
                        break;
                    }
                }

                tttien_nt_dtl += parseFloat(objDTL.SOTIEN_NT);

            }
            if (parseFloat(HDR.TTIEN) != parseFloat(HDR.TTIEN_NT)) {
                alert('Tổng tiền VND và tổng tiền nguyên tệ đang không khớp. Xin vui lòng nhập lại số tiền!');
                return false;
            }

            if (parseFloat(HDR.TTIEN) != tttien_dtl) {
                alert('Tổng tiền nộp và tổng tiền chi tiết nộp thuế đang không khớp. Xin vui lòng nhập lại số tiền!');
                return false;
                //flag = false;
            }
            if (parseFloat(HDR.TTIEN_NT) != tttien_nt_dtl) {
                alert('Tổng tiền nguyên tệ nộp và tổng tiền nguyên tệ chi tiết nộp thuế đang không khớp. Xin vui lòng nhập lại số tiền!');
                return false;
                //flag = false;
            }

            return flag;
        }
        function checkTKGL() {
            for (var i = 0; i < arrTKGL.length; i++) {
                var arr = arrTKGL[i].split(';');
                if (arr[0] == $get('txtTKGL').value) {
                    return true;
                }
            }
            return false;
        }
        function jsCheckEmptyVal(cmpCtrl, divCtrl, strMessage) {
            if (document.getElementById(cmpCtrl).value == '') {
                document.getElementById(divCtrl).innerHTML = strMessage;
                return false;
            }
            return true;
        }

        function jsValidSoDuKH() {
            var dblTongTrichNo = parseFloat($get('txtTongTien').value.replaceAll(',', ''));
            var dblTongTrichCu = parseFloat($get('hdnTongTienCu').value.replaceAll(',', ''));
            if ($get('hdnTongTienCu').value == null || $get('hdnTongTienCu').value == "" || $get('txtTK_KH_NH').value.substring(0, 3) == 'VND') {
                dblTongTrichCu = 0;
            } else
                dblTongTrichCu = parseFloat($get('hdnTongTienCu').value.replaceAll(',', ''));
            var dblSoDuKH = parseFloat($get('txtSoDu_KH_NH').value.replaceAll('.', '').replaceAll(',', ''));

            if (dblTongTrichNo - dblTongTrichCu <= dblSoDuKH) {
                return false;
            } else {
                return true;
            }
        }


        function jsGet_TenTKGL() {

            if (arrTKGL.length > 0) {
                for (var i = 0; i < arrTKGL.length; i++) {

                    var arr = arrTKGL[i].split(';');
                    $get('txtTK_KH_NH').value = arr[0];
                    $get('txtTenTK_KH_NH').value = arr[1];
                    if (arr[0] == $('#txtTKGL').val()) {
                        $('#txtTenTKGL').val(arr[1]);
                        break;
                    }
                }
            }
        }


        function jsAddRowDTL_Grid2(arrParam) {

            var sID = jsGenDTLrowID();
            var sTT_BToan = jsGenSoButToan_DTL();
            var sAppendRows = '';
            var sNgayDK = '';

            //arrParam: array of Item inside HQ result (it's include DTL object array)
            if ((typeof arrParam !== 'undefined') && (arrParam !== null) && (arrParam instanceof Array) && (arrParam.length > 0)) {
                for (var i = 0; i < arrParam.length; i++) {
                    var arrCT_No = arrParam[i].CT_No;
                    if (arrCT_No && arrCT_No.length > 0) {
                        for (var j = 0; j < arrCT_No.length; j++) {
                            if (!isNaN(sID)) {
                                sID++;
                                sNgayDK = ('0' + arrParam[i].Ngay_DK.substr(8, 2)).slice(-2) + '/' + ('0' + arrParam[i].Ngay_DK.substr(5, 2)).slice(-2) + '/' + arrParam[i].Ngay_DK.substr(0, 4);
                                sAppendRows = sAppendRows + '<tr>';
                                if (arrCT_No[j].DuNo > 0) {
                                    sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "' onblur='jsCalCharacter();' onclick='jsCalcTotalMoney(); jsCalCharacter();jsConvertCurrToText();jsGenThuTuButToan();' checked /></td>";
                                } else {
                                    sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "' onblur='jsCalCharacter();' onclick='jsCalcTotalMoney(); jsCalCharacter();jsConvertCurrToText();jsGenThuTuButToan();' /></td>";
                                }
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SO_TK_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrParam[i].So_TK + "' onblur='jsGenThuTuButToan();jsCalCharacter();' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ngay_TK_" + sID + "' style='width: 98%; border-color: White;' onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}' class='inputflat' maxlength='10' onblur='javascript:CheckDate(this);jsCalCharacter();' value='" + sNgayDK + "' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_LT_" + sID + "' style='width: 98%;' class='inputflat' onblur='jsCalCharacter();' value='" + arrParam[i].Ma_LT + "' /></td>";

                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Lhxnk_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' onblur='jsCalCharacter();' value='" + arrParam[i].Ma_LH + "' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SAC_THUE_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' maxlength='2' onblur='jsCalCharacter();' value='" + arrCT_No[j].LoaiThue + "' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_CHUONG_" + sID + "' maxlength='3' style='width: 98%; border-color: White;' class='inputflat' onblur='jsCalCharacter();' value='" + arrParam[i].Ma_Chuong + "' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ma_TMuc_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' onblur='jsCalCharacter();' value='" + arrCT_No[j].TieuMuc + "' onblur='onBlurMaTieuMuc(" + sID + ")' onkeypress='if (event.keyCode==13){ShowMLNS(1," + sID + ")}' /></td>";

                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Noi_Dung_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' onblur='jsCalCharacter();' value='" + arrCT_No[j].Noi_Dung + "' /></td>";

                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTien_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onblur='jsCalCharacter();' onfocus='this.select()' onkeyup='ValidInteger(this); jsFormatNumber(this);jsCalcTotalMoney();' maxlength='17' onblur='jsCalcTotalMoney(); jsCalCharacter();jsCalcCurrency(" + sID + ");' value='" + arrCT_No[j].DuNo + "' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTienNT_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onblur='jsCalCharacter();jsCalcTotalMoney()' onfocus='this.select()' onkeyup='jsFormatNumber2(this);' maxlength='17' readonly value='" + arrCT_No[j].DuNo + "' /></td>";

                                sAppendRows = sAppendRows + "<td style = 'display: none;'><input type='text' id='DTL_TT_BTOAN_" + sID + "' style='width: 98%; display: none;' class='inputflat' value='" + sTT_BToan + "' /></td>";
                                sAppendRows = sAppendRows + '</tr>';
                            }
                        }
                    }
                }
                $('#grdChiTiet tr:last').after(sAppendRows);
                jsFormatNumberDTL_Grid();
            }
        }




        function TinhPhi() {
            var dTotalMoney = 0;
            var dDetailMoney = 0;

            var tongTienNT = 0;
            var ctTongTienNT = 0;
            //get detail
            $('#grdChiTiet tr').each(function () {
                var ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
                var ctrInput = null;
                var ctrInputNT = null;
                if (ctrCheck) {
                    if (ctrCheck.is(":checked")) {
                        ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                        if (ctrInput) {
                            dDetailMoney = 0;
                            dDetailMoney = accounting.unformat(ctrInput.prop("value"));
                            if (!isNaN(dDetailMoney)) {
                                dTotalMoney = dTotalMoney + dDetailMoney;
                            }
                        }

                        ctrInputNT = $(this).find("input[id^='DTL_SoTienNT_']");

                        if (ctrInputNT) {
                            ctTongTienNT = 0;
                            ctTongTienNT = accounting.unformat(ctrInputNT.prop("value"), ".");
                            if (!isNaN(ctTongTienNT)) {
                                tongTienNT += ctTongTienNT;
                            }
                        }
                    } else {
                        ctrInput = null;
                        ctrInputNT = null;
                    }
                }
            });


            if (document.getElementById('chkChargeType').checked)
                //if (cmdTinhPhi_onclick)
            {
                dblPhi = (dTotalMoney * feeRate) / minFee;
                dblVAT = (dTotalMoney * feeRate) / minFee / 10;
                if (dblPhi < minFee) {
                    dblPhi = minFee;
                    dblVAT = dblPhi / 10;
                }
            }
            else {
                dblPhi = parseFloat(accounting.unformat($('#txtCharge').val()));
                dblVAT = dblPhi / 10;
            }
            //TinhPhi(dTotalMoney);
            dblTongTrichNo = dblPhi + dblVAT + dTotalMoney;
            $('#txtVAT').val(accounting.formatNumber(dblVAT));
            $('#txtCharge').val(accounting.formatNumber(dblPhi));

            $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();
            $('#txtTongTien').val(accounting.formatNumber(dTotalMoney));

            if ($("#ddlMaNT option:selected").val() != 'VND')
                $('#txtTongTienNT').val(accounting.formatNumber(tongTienNT, 2, ",", "."));
            else
                $('#txtTongTienNT').val(accounting.formatNumber(tongTienNT));
        }

        
        function jsValidMin() {
            var TKCo = document.getElementById('ddlMaTKCo').value;
            if (TKCo.length < 4) {

                alert("TK Có NSNN cần tối thiểu 4 ký tự");
            }
        };

        function jsFillGhiChu() {
            $('#txtGhiChu').val($('#txtTenKB').val());
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <input type="hidden" id="hdfSoBT" value="" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top">
            <!--DANH SACH CHUNG TU-->
            <td valign="top" width="20%" style="text-align: left;">
                <asp:Panel ID="pnlDSCT" runat="server" Height="450px" Width="98%" Style="overflow: auto;">
                    <table class="grid_data" border="0" cellpadding="0" cellspacing="0" width="100%"
                        style="padding: 10px 0; display: none;">
                        <tr>
                            <td>
                                <input type="text" id="txtTraCuuSoCtu" value="" style="width: 110px;" />
                            </td>
                            <td style="text-align: center;">
                                <input type="button" id="btnTraCuuSoCtu" value="Tra cứu" onclick="jsLoadCTList()"
                                    style="width: 70px;" />
                            </td>
                        </tr>
                    </table>
                    <table class="grid_data" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class='grid_header'>
                            <td style="width: 10%">TT
                            </td>
                            <td style="width: 25%">Số CT
                            </td>
                            <td style="width: 65%">User lập/Số BT
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div id='divDSCT'>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr style="display: none">
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuaKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Chưa Kiểm soát
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuyenKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Chờ Duyệt
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/DaKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Đã Duyệt
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/KSLoi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Chuyển trả
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Giao dịch hủy
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/HuyHS.gif" />
                        </td>
                        <td style="width: 90%" colspan="2">Giao dịch hủy - chờ duyệt
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy_KS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Kiểm soát duyệt hủy GD
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/TT_CThueLoi.gif" />
                        </td>
                        <td style="width: 90%" colspan="2">Chuyển thông tin thuế lỗi
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy_CT_Loi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Duyệt hủy chuyển thuế lỗi
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy_CT_Loi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Hủy chuyển thuế/HQ lỗi
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuaNhapBDS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Điều chỉnh chờ kiểm soát
                        </td>
                    </tr>
                </table>
            </td>
            <!--THONG TIN CHUNG TU-->
            <td valign="top" width="80%">
                <!--TRUY VAN HQ-->
                <div style="width: 100%; margin: 0; padding: 0;">
                    <table width="100%" cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                        <tr class="grid_header">
                            <td align="left" colspan="4">TRUY VẤN THUẾ HẢI QUAN
                            </td>
                        </tr>
                        <tr align="left">
                            <td>
                                <b>Mã số thuế</b>&nbsp;<span class="requiredField">(*)</span>
                            </td>
                            <td style="width: 35%;">
                                <input id="txtFilter_MaSoThue" maxlength="14" type="text" class="inputflat" style="width: 150px;"
                                    onblur="{jsCheckMaNNT(this);}" />
                            </td>
                            <td style="width: 15%;">
                                <b>Số tờ khai</b>
                            </td>
                            <td style="width: 35%;">
                                <input id="txtFilter_SoToKhai" type="text" class="inputflat" style="width: 150px;" />
                            </td>
                        </tr>
                        <tr align="left">
                            <td>
                                <b>Năm đăng ký</b>
                            </td>
                            <td align="left">
                                <input id="txtFilter_NgayDKy" type="text" class="inputflat" style="width: 150px;"
                                    maxlength="4" onkeypress="return ValidNumber(event);" onblur="javascript:jsCheckYear(this)"
                                    onfocus="this.select()" />
                                <b>(YYYY)</b>
                            </td>
                            <td style="display: none">
                                <b>Loại chứng từ</b>
                            </td>
                            <td align="left" style="display: none">
                                <select id="ddlFilter_LoaiTien" style="width: 155px;" class="inputflat">
                                    <option value="T" selected>Chứng từ thuế</option>
                                    <option value="P">Chứng từ phí</option>
                                </select>
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="4" align="center">
                                <input id="btnTruyVanHQ" type="button" value="Truy vấn HQ" class="ButtonCommand"
                                    onclick="jsGet_NNThue_HQ();" />
                            </td>
                        </tr>
                    </table>
                </div>
                <!--KET QUA TRUY VAN HQ-->
                <div id="divKQTruyVan" style="width: 100%; margin: 0; padding: 0; display: none;">
                    <br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="grid_header">
                            <td align="left">TRUY VẤN THUẾ HẢI QUAN
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divResultTruyVan" style="width: 100%; height: 200px; overflow: auto;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--THONG BAO-->
                <div class="errorMessage" style="width: 100%; margin: 0; padding: 0;">
                    <div id="divStatus" style="width: 100%; margin: 0; padding: 0; background-color: Aqua; font-weight: bold; font-size: 18pt; display: none;">
                        <br />
                    </div>
                    <div id="divProgress" style="width: 100%; margin: 0; padding: 0; background-color: Aqua; font-weight: bold; font-size: 18pt; display: none;">
                        Đang xử lý. Xin chờ một lát...
                    </div>
                </div>
                <!--THONG TIN TO KHAI-->
                <br />
                <div id="divTTinToKhai" style="width: 100%; margin: 0; padding: 0;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr align="left" class="grid_header">
                            <td align="left" style="width: 70%;">
                                <div style="float: left;">
                                    THÔNG TIN CHỨNG TỪ THUẾ HẢI QUAN&nbsp;
                                </div>
                                <div id="lblSoCTu" style="float: left; display: inline;">
                                </div>
                            </td>
                            <td align="right">Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <div id="pnlTTinTKhai" style="width: 100%;">
                                    <table id="grdHeader" cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                        <tr id="rowMaNNT">
                                            <td width="24%">
                                                <b>Mã số thuế</b><span class="requiredField">(*)</span>
                                            </td>
                                            <td width="25%">
                                                <input id="txtMaNNT" maxlength="14" type="text" class="inputflat" style="width: 98%;" onblur="{jsCheckMaNNT(this);}" />
                                            </td>
                                            <td width="51%" style="height: 25px">
                                                <input id="txtTenNNT" type="text" class="inputflat" style="width: 98%;" maxlength="140" />
                                            </td>
                                        </tr>
                                        <tr id="rowDChiNNT">
                                            <td>
                                                <b>Địa chỉ người nộp thuế</b><span class="requiredField">(*)</span>
                                            </td>
                                            <td colspan="2">
                                                <input id="txtDChiNNT" type="text" class="inputflat" style="width: 98.7%;" />
                                            </td>
                                        </tr>
                                        <tr id="rowHuyen" style="">
                                            <td>
                                                <b>Quận(Huyện) người nộp thuế</b>
                                            </td>
                                            <td>
                                                <input id="txtHuyen_NNT" type="text" class="inputflat" style="width: 98%; background: Aqua"
                                                    onkeypress="if (event.keyCode==13){ShowLov('DBHC_NNT');}" onblur="jsGetTenQuanHuyen_NNThue(this.value);" />
                                            </td>
                                            <td>
                                                <input id="txtTenHuyen_NNT" type="text" class="inputflat" style="width: 98%;" readonly="readonly">
                                            </td>
                                        </tr>
                                        <tr id="rowTinh" style="">
                                            <td class="style1">
                                                <b>Tỉnh(Thành phố) người nộp thuế</b>
                                            </td>
                                            <td class="style1">
                                                <input id="txtTinh_NNT" type="text" class="inputflat" style="width: 98%;" onblur="jsGetTenTinh_NNThue(this.value);" />
                                            </td>
                                            <td class="style1">
                                                <input id="txtTenTinh_NNT" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>
                                        <%--<tr id="rowTTNo">
                                            <td class="style1">
                                                <b>Trạng thái nợ</b>
                                            </td>
                                            <td class="style1">
                                                <input id="txtTTNo" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                            <td class="style1">
                                                <input id="txtTen_TTN" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>--%>
                                        <tr id="rowHQCuc">
                                            <td class="style1">
                                                <b>Mã cục/Tên cục </b>
                                                <%--<span class="requiredField">(*)</span>--%>
                                            </td>
                                            <td class="style1">
                                                <input id="txtMaCuc" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                            <td class="style1">
                                                <input id="txtTenCuc" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr id="rowMaHQPH">
                                            <td class="style1">
                                                <b>Mã HQ PH </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td class="style1">
                                                <input id="txtMaHQPH" maxlength="4" type="text" onblur="jsLoadByCQT();" onkeypress="if (event.keyCode==13){ShowLov('MHQPH');}"
                                                    style="width: 98%; background: #0072BC;" class="inputflat" />
                                            </td>
                                            <td class="style1">
                                                <input id="txtTenMaHQPH" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr id="rowCQThu">
                                            <td class="style2">
                                                <b>CQ Thu </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td class="style2">
                                               <!-- <input id="txtMaCQThu" type="text" onkeypress="if (event.keyCode==13){ShowLov('CQT');}"
                                                    onblur="jsLoadTenCQT('txtMaCQThu','txtTenCQThu');jsLoadSHKBByCQT();" style="width: 98%; background: Aqua;"
                                                    class="inputflat" />-->
                                                <input id="txtMaCQThu" type="text" 
                                                    style="width: 98%; "
                                                    class="inputflat" disabled="disabled" />
                                                
                                                <%--jsLoadByCQT();--%>
                                            </td>
                                            <td class="style2">
                                                <input id="txtTenCQThu" type="text" class="inputflat" style="width: 98%;"  disabled="disabled" />
                                            </td>
                                        </tr>
                                       
                                         <tr id="rowMaHQ">
                                            <td class="style1">
                                                <b>Mã HQ </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td class="style1">
                                                <input id="txtMaHQ" maxlength="4" type="text" onkeypress="if (event.keyCode==13){ShowLov('MHQ');}"
                                                    style="width: 98%; background: Aqua;" class="inputflat" />
                                              <%--  <input id="txtMaHQ" type="text" onkeypress="if (event.keyCode==13){ShowLov('MHQ');}"
                                                    style="width: 98%; background: Aqua;" class="inputflat" onblur="jsLoadByCQT(this.value);"
                                                    onchange="jsLoadByCQT(this.value);" />--%>
                                            </td>
                                            <td class="style1">
                                                <input id="txtTenMaHQ" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr id="rowSHKB">
                                            <td width="24%" class="style1">
                                                <b>Số hiệu KB </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td width="25%" class="style1">
                                                <input id="txtSHKB" type="text" maxlength="4" class="inputflat" style="width: 98%; background: Aqua;"
                                                    onkeypress="if (event.keyCode==13){ShowLov('SHKB');}" onblur="jsGetTTNganHangNhan();jsGet_TenKB();" />
                                            </td>
                                            <td width="51%" class="style1">
                                                <input id="txtTenKB" type="text" class="inputflat" style="width: 98%;" maxlength="60" onchange="jsFillGhiChu();" onblur="jsFillGhiChu();" />
                                            </td>
                                        </tr>
                                        <tr id="rowDBHC" style="">
                                            <td style="">
                                                <b>Mã tỉnh/thành Kho bạc </b><span class="requiredField"></span>
                                            </td>
                                            <td style="">
                                                <input id="txtMaDBHC" type="text" class="inputflat" style="width: 98%;" onkeypress="if (event.keyCode==13){ShowLov('DBHC');}" />
                                                <%--onblur="jsGetTenTinh_KB(this.value);" onchange="jsGet_TenDBHC();"--%>
                                            </td>
                                            <td style="">
                                                <input id="txtTenDBHC" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                                            </td>
                                        </tr>
                                        <tr id="rowTKCo">
                                            <td>
                                                <b>TK Có NSNN </b><span class="requiredField">(*)</span>
                                            </td>
                                            <td>
                                                <select id="ddlMa_NTK" style="width: 100%;" class="inputflat">
                                                    <option value="1" selected="selected">Tài khoản thu NSNN</option>
                                                    <option value="2">Tài khoản tạm thu</option>
                                                    <option value="3">Hoàn thuế GTGT</option>
                                                </select>
                                            </td>
                                            <td style="">
                                                <input type="text" id="ddlMaTKCo" class="inputflat" style="width: 99%;" onblur="javascript:jsValidMin();" maxlength="18" />
                                            </td>
                                        </tr>
                                        <tr id="rowLoaiThue" style="display: none;">
                                            <td>
                                                <b>Loại thuế</b>
                                            </td>
                                            <td>
                                                <select id="ddlMaLoaiThue" style="width: 98%" class="inputflat">
                                                    <option value="04" selected="selected">Thuế Hải Quan</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtTenLoaiThue" type="text" class="inputflat" style="width: 98%;" disabled="true"
                                                    readonly="readonly" value="" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Ngày HT/ Ngày CT</b>
                                            </td>
                                            <td>
                                                <input id="txtNGAY_HT" type="text" class="inputflat" style="width: 98%;" maxlength="10"
                                                    readonly="readonly" />
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <!--THONG TIN NGAN HANG-->
                <div id="divTTinNganHang" style="width: 100%; margin: 0; padding: 0;">
                    <table cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                        <tr class="grid_header">
                            <td align="left" colspan="3">THÔNG TIN NGÂN HÀNG
                            </td>
                        </tr>
                        <tr id="rowMaNT" align="left">
                            <td style="width: 25%;">
                                <b>Nguyên tệ</b>
                            </td>
                            <td style="width: 25%;">
                                <select id="ddlMaNT" style="width: 98%" class="inputflat" onchange="changeNT();">
                                    <option></option>
                                </select>
                            </td>
                            <td>
                                <input id="txtTenNT" class="inputflat" style="width: 98%;" readonly="readonly" value="VND" />
                            </td>
                        </tr>
                        <tr id="rowTyGia" align="left">
                            <td>
                                <b>Tỷ giá</b>
                            </td>
                            <td align="left">
                                <input id="txtTyGia" type="text" class="inputflat" style="width: 98%;" />
                            </td>
                            <td></td>
                        </tr>
                        <tr id="rowPT_TT" align="left">
                            <td>
                                <b>Phương thức thanh toán</b>
                            </td>
                            <td colspan="2">
                                <select id="ddlPT_TT" style="width: 98%" onload="onChangePTTT()" class="inputflat"
                                    onchange="onChangePTTT()">
                                    <%--<option value="03">Nộp tiền GL</option>--%>
                                    <option value="01" selected="selected">Chuyển khoản</option>
                                    <option value="05">Tiền mặt</option>
                                </select>
                            </td>
                            <td style="display: none">
                                <input id="txtTenHTTT" type="text" class="inputflat" style="width: 98%;" readonly="readonly; " />
                            </td>
                        </tr>
                        <tr id="rowTK_KH_NH" align="left" style="display: none">
                            <td>
                                <b>TK Chuyển </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtTK_KH_NH" class="inputflat" style="width: 98%;" onblur="jsGetTK_KH_NH();jsGetTenTK_KH_NH();"
                                    maxlength="16" />
                            </td>
                            <td>
                                <input id="txtTenTK_KH_NH" class="inputflat" style="width: 92%;" disabled="true"
                                    readonly="readonly" />
                                <img id="imgSignature" alt="Hiển thị chữ ký khách hàng" src="../../images/icons/MaDB.png"
                                    onclick="jsShowCustomerSignature();" style="width: 16px; height: 16px; display: none" />
                            </td>
                        </tr>
                        <tr id="rowTKGL" align="left" style="display: none">
                            <td>
                                <b>TK GL </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtTKGL" class="inputflat" style="width: 98%;" onblur="jsGet_TenTKGL();"
                                    maxlength="16" />
                            </td>
                            <td>
                                <input id="txtTenTKGL" class="inputflat" style="width: 92%;" disabled="true" readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowSoDuNH" align="left" style="display: none">
                            <td>
                                <b>Số dư khả dụng</b>
                            </td>
                            <td>
                                <input id="txtSoDu_KH_NH" class="inputflat" style="width: 98%;" readonly="readonly" />
                            </td>
                            <td>
                                <input id="txtDesc_SoDu_KH_NH" type="text" class="inputflat" style="width: 98%;"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowTienMat" align="left" class="trTienMat" style="display: none">
                            <td>
                                <b>CMTND/Điện thoại </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtCmtnd" class="inputflat" style="width: 98%;" />
                            </td>
                            <td>
                                <input id="txtDienThoai" type="text" class="inputflat" style="width: 98%;" />
                            </td>
                        </tr>
                        <tr id="rowNNTien" align="left" style="">
                            <td>
                                <b>MST/Tên người nộp tiền</b>
                            </td>
                            <td>
                                <input id="txtMaNNTien" type="text" class="inputflat" style="width: 98%;" maxlength="14"
                                    onkeyup="" />
                            </td>
                            <td>
                                <input id="txtTenNNTien" type="text" class="inputflat" style="width: 98%;" />
                            </td>
                        </tr>
                        <tr id="rowDChiNNTien" align="left" style="">
                            <td>
                                <b>Địa chỉ người nộp tiền</b>
                            </td>
                            <td colspan="2">
                                <input id="txtDChiNNTien" type="text" maxlength="50" class="inputflat" style="width: 98.7%;" />
                            </td>
                        </tr>
                        <tr id="rowHuyenNNTien" align="left" style="">
                            <td>
                                <b>Quận(Huyện) người nộp tiền</b>
                            </td>
                            <td>
                                <input id="txtQuan_HuyenNNTien" type="text" class="inputflat" style="width: 98%;"
                                    onkeypress="if (event.keyCode==13){ShowLov('DBHC_NNTHUE');}" onblur="jsGetTenQuanHuyen_NNTien(this.value);" />
                            </td>
                            <td>
                                <input id="txtTenQuan_HuyenNNTien" type="text" class="inputflat" style="width: 98%;"
                                    readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowTinhNNTien" align="left" style="">
                            <td class="style1">
                                <b>Tỉnh(Thành phố) người nộp tiền</b>
                            </td>
                            <td class="style1">
                                <input id="txtTinh_NNTien" type="text" class="inputflat" style="width: 98%;" readonly="readonly"
                                    onblur="jsGetTenTinh_NNTien(this.value);" />
                            </td>
                            <td class="style1">
                                <input id="txtTenTinh_NNTien" type="text" class="inputflat" style="width: 98%;" readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowNHChuyen" align="left">
                            <td>
                                <b>Mã NH Chuyển </b><span class="requiredField">(*)</span>
                            </td>
                            <td colspan="2">
                                <select id="ddlMA_NH_A" style="width: 98.7%;" class="inputflat">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr id="rowNHThanhToan" align="left" visible="false">
                            <td>
                                <b>Mã NH trực tiếp </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtMA_NH_TT" class="inputflat" style=" width: 98%;" maxlength="8" visible="false"
                                    onblur="jsGet_TenNH_TT()" onkeypress="if (event.keyCode==13){ShowLov('DMNH_TT');jsGet_TenNH_TT();}"
                                    onkeyup="" disabled="disabled" />
                            </td>
                            <td>
                                <input id="txtTEN_NH_TT" class="inputflat" style="width: 98%;" readonly="readonly"
                                    visible="false" disabled="disabled" />
                            </td>
                        </tr>
                        <tr id="rowNHNhan" align="left">
                            <td>
                                <b>Mã NH thụ hưởng </b><span class="requiredField">(*)</span>
                            </td>
                            <td>
                                <input id="txtMA_NH_B" class="inputflat" style="background: aqua; width: 98%;" maxlength="8" onblur="jsGet_TenNH_B()"
                                    onkeypress="if (event.keyCode==13){ShowLov('DMNH_GT');jsGet_TenNH_B();}"  />
                            </td>
                            <td>
                                <input id="txtTenMA_NH_B" class="inputflat" style="width: 98%;" readonly="readonly" />
                            </td>
                        </tr>
                        <tr id="rowDienGiai" style="display: none">
                            <td align="left">
                                <b>Diễn giải</b>
                            </td>
                            <td colspan="2">
                                <textarea id="txtDienGiai" style="width: 99%;" class="inputflat" rows="2" cols="1" readonly="readonly"
                                    maxlength="210" onblur="jsCalCharacter(1);"></textarea>
                            </td>
                        </tr>
                        <tr id="rowDienGiaiHQ">
                            <td align="left">
                                <b>Diễn giải HQ</b>
                            </td>
                            <td colspan="2">
                                <textarea id="txtDienGiaiHQ" style="width: 99%;" class="inputflat" rows="2" cols="1"
                                    maxlength="200"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <!--CHI TIET TO KHAI-->
                <div id="divCTietToKhai" style="width: 100%; margin: 0; padding: 0;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="grid_header">
                            <td align="left">CHI TIẾT CHỨNG TỪ THUẾ HẢI QUAN
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div id="pnlCTietTTinTKhai" style="height: 200px; min-height: 170px; width: 100%; overflow: auto;">
                                </div>
                                <div id="" style="width: 100%; vertical-align: middle;" align="right">
                                    <input id="btnAddRowDTL" type="button" value="Thêm dòng" class="ButtonCommand" onclick="jsAddDummyRowDTL_Grid(1);" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <!--THONG TIN KHAC-->
                <div id="divTTinKhac" style="width: 100%; margin: 0; padding: 0;">
                    <table cellspacing="3" cellpadding="1" style="width: 100%;">
                        <tr align="left">
                            <td align="left" valign="middle" class="text" style="width: 50%; font-family: @Arial Unicode MS; font-weight: bold; color: red">
                                <span style="">Số ký tự còn lại: </span>
                                <label id="leftCharacter" style="font-weight: bold" visible="false">
                                </label>
                            </td>
                            <td align="right" class="text" style="width: 50%;">
                                <span style="font-family: @Arial Unicode MS; font-weight: bold;">Tổng Tiền: </span>
                                <input type="text" id="txtTongTien" style="background-color: Yellow; text-align: right; font-weight: bold"
                                    class="inputflat" value="0" readonly="readonly" disabled="disabled" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="text" style="width: 100%;" colspan="2">
                                <span style="font-family: @Arial Unicode MS; font-weight: bold;">Tổng Tiền NT: </span>
                                <input type="text" id="txtTongTienNT" style="background-color: Yellow; text-align: right; font-weight: bold"
                                    class="inputflat" value="0" readonly="readonly" disabled="disabled" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <table width="700px">
                                    <tr align="right">
                                        <td>Phí(Dự tính):
                                        </td>
                                        <td>
                                            <input type="text" id="txtCharge" style="text-align: right; width: 100px; background-color: Yellow; font-weight: bold"
                                                class="inputflat" value="0" onfocus="this.select()" />
                                        </td>
                                        <td>VAT(Dự tính):
                                        </td>
                                        <td>
                                            <input type="text" id="txtVAT" style="text-align: right; width: 100px; background-color: Yellow; font-weight: bold"
                                                class="inputflat" value="0" />
                                        </td>
                                        <td align="right" colspan="3" class="style2">
                                            <b>Tổng trích nợ(Dự tính):</b>
                                        </td>
                                        <td>
                                            <input type="text" id="txtTongTrichNo" style="text-align: right; width: 150px; background-color: Yellow; font-weight: bold"
                                                class="inputflat" value="0" />
                                        </td>
                                        <%--<td align="right" colspan="4">
                                            <input type="button" id="cmdTinhPhi" class="buttonField" value="Tính phí" onclick="jsTinhPhi()"
                                                visible="true" />
                                        </td>--%>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 99%" colspan="2">
                                <div id='divBangChu' style="font-weight: bold;">
                                </div>
                            </td>
                        </tr>
                        <tr id="rowGhiChu" align="left">
                            <td align="left" style="width: 99%" colspan="2" valign="middle">
                                <span style="font-weight: bold;">Ghi chú:</span>
                                <input type="text" id="txtGhiChu" style="width: 99%; border-color: black; font-weight: bold"
                                    maxlength="100" class="inputflat" onblur="jsCalCharacter(1);" onchange="jsCalCharacter(1);" />
                            </td>
                        </tr>
                        <tr id="rowLyDoHuy" align="left">
                            <td align="left" style="width: 99%" colspan="2" valign="middle">
                                <span style="font-weight: bold;">Lý do chuyển trả:</span>
                                <input type="text" id="txtLyDoHuy" style="width: 99%; border-color: black; font-weight: bold"
                                    maxlength="100" class="inputflat" />
                            </td>
                        </tr>
                    </table>
                    <input id="hdfSo_CTu" type="hidden" />
                    <input id="hdfLoaiCTu" type="hidden" value="T" />
                    <input id="hdfHQResult" type="hidden" />
                    <input id="hdfHDRSelected" type="hidden" />
                    <input id="hdfIsEdit" type="hidden" />
                    <input id="hdfDescCTu" type="hidden" />
                    <input id="hdfNgayKB" type="hidden" />
                    <input id="hdfMST" type="hidden" />
                    <input type="hidden" id="cifNo" />
                    <input type="hidden" id="branchCode" />
                    <input type="hidden" id="hdnTongTienCu" value="0" />

                    <input type="hidden" id="hdLimitAmout" value="0" />
                    <input type="hidden" id="hndNHSHB" />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
            </td>
        </tr>
        <tr>
            <td width='100%' align="center" valign="top" class='text' colspan='2' align="right">
                <input type="button" id="cmdThemMoi" class="ButtonCommand" onclick="jsThem_Moi();"
                    value="[Lập (M)ới]" accesskey="M" />
                &nbsp;
                <input type="button" id="cmdGhiKS" class="ButtonCommand" value="[Hoàn thiện]" onclick="jsGhi_CTU2('GHI');"
                    accesskey="K" style="display: none" />
                &nbsp;
                <input type="button" id="cmdChuyenKS" class="ButtonCommand" value="[Ghi & (K)S]" style=""
                    accesskey="C" onclick="jsGhi_CTU2('GHIKS');" visible="true"/>
                &nbsp;
                <input type="button" id="cmdDieuChinh" class="ButtonCommand" value="[Điều (C)hỉnh]"
                    style="" accesskey="C" onclick="jsGhi_CTU2('GHIDC');" visible="true" />
                &nbsp;
                <input type="button" id="cmdInCT" value="[(I)n CT]" class="ButtonCommand" onclick="jsIn_CT('1');"
                    accesskey="I" />
                &nbsp;
                <input type="button" id="cmdHuyCT" class="ButtonCommand" value="[(H)ủy]" onclick="jsHuy_CTU();"
                    accesskey="H" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
            </td>
        </tr>
    </table>
      <div id="dialog-popup" style="display: none;">
        </div>
    <div id="wapaler" style="display: none;">
        </div>
    <div class="msgBox" id="msgBox_SHKB" style=" display:none; opacity: 1; min-height: 100px;width:730px;">
            <div class="msgBoxTitle">DANH MỤC</div>
            <div>
                <div>
                    <div class="msgBoxContent" id="msgBox_SHKB_CONTENT">
                     
                      <table border="0" cellpadding="2" cellspacing="1" class="form_input" style="width:100%">
        <tr class="grid-heading">
            <td align="center" class="style2" style ="width:30%" >
                <b>Mã danh mục</b> 
            </td>
            <td  align="center" class="style3" >
                <b>Tên danh mục</b>
            </td>
        </tr>
        <tr >
            
            <td>
            <input type="text" id="msgBox_txtMaDanhMuc"  onkeydown="if(event.keyCode==13) {msgBox_jsSearch(); event.keyCode=9;}" class="inputflat" />&nbsp;
            </td>
         
          
            <td align="center">
             <input type="text" id="msgBox_txtTenDanhMuc" onkeydown="if(event.keyCode==13) {msgBox_jsSearch(); event.keyCode=9;}"  class="inputflat" />&nbsp;
             <%-- <input type="button" id="msgBox_btnSearch" value=" ... " class="inputflat" onclick="msgBox_jsSearch();" />--%>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="style1">
                <div id='msgBox_divDanhMuc' style="max-height:300px;overflow:auto">
                </div>
                <div id='msgBox_divStatus'>
                </div>
                
            </td>
        </tr>
    </table>           
                    </div>
                     
                </div>
                   <div class="msgBoxButtons" id="msgBox_SHKB_ConfirmDivButton">
                       <input type="hidden" id="msgBox_result" value="" />
                        <input id="msgBox-btn-close" class="btn btn-primary" onclick="jsMsgBoxClose();" type="button" name="Ok" value="Đóng" />
                  </div>
                
            </div>
        </div>
    <link href="../../css/dialog.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function jsDiaglog() {
            $('#msgBox_result').val('');
            document.getElementById('msgBox_txtMaDanhMuc').value = "";
            document.getElementById('msgBox_txtTenDanhMuc').value = "";
            // console.log('Start');
            $('#wapaler').css("display", "block");
            $('#msgBox_SHKB').css("display", "block");

            jsDivAutoCenter('#msgBox_SHKB');
            msgBox_jsSearch();
        }


        function jsMsgBoxClose() {

            $('#wapaler').css("display", "none");
            $('#msgBox_SHKB').css("display", "none");


        }
        var msgBox_type = "";
        var msgBox_SHKB = "";
        var msgBox_txtID = "";
        var msgBox_txtTitle = "";
        var msgBox_txtFocus = "";
        function ShowDMKB(strPage, txtID, txtTitle, txtFocus) {
            document.getElementById('msgBox_result').value = "";
            msgBox_type = strPage;

            msgBox_txtID = txtID;
            msgBox_txtTitle = txtTitle;
            msgBox_txtFocus = txtFocus;
            jsDiaglog();


        }
        function jsDivAutoCenter(divName) {
            //      console.log('em chay phat' + divName);
            $(divName).css({
                position: 'fixed',
                left: ($(window).width() - $(divName).outerWidth()) / 2,
                top: ($(window).height() - $(divName).outerHeight()) / 2,
                height: 'auto'
            });
        }
        //pop-up
        $(window).resize(function () {

            jsDivAutoCenter('.msgBox');
        });
        function msgBox_jsSearch() {
            var strKey = document.getElementById('msgBox_txtMaDanhMuc').value;
            var strTenDM = document.getElementById('msgBox_txtTenDanhMuc').value;
            var strPage = msgBox_type;
            var strSHKB = "";
            if (document.getElementById('txtSHKB').value.length > 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            var curMa_Dthu = '';
            var curMa_DBHC = '';
            PageMethods.SearchDanhMuc(curMa_Dthu, curMa_DBHC, strPage, strKey, strTenDM, strSHKB, SearchDanhMuc_Complete, SearchDanhMuc_Error);
        }
        function SearchDanhMuc_Complete(result, methodName) {
            //document.getElementById('divDanhMuc').innerHTML = result;          
            document.getElementById('msgBox_divDanhMuc').innerHTML = jsBuildResultTable(result);

        }
        function SearchDanhMuc_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('msgBox_divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
        function SelectDanhMuc(strID, strTitle) {
            //var arr = new Array();
            //arr["ID"] = strID;
            //arr["Title"] = strTitle;
            //document.getElementById('msgBox_result').value = arr;
            jsMsgBoxClose();
            if (strID.length > 0) {
                document.getElementById(msgBox_txtID).value = strID;
             //   console.log('msgBox_type: ' + msgBox_type);
                if (msgBox_type == "KhoBac") {

                    jsSHKB_lostFocus(strID);
                } else if (msgBox_type == "DMNH_GT") {

                    jsGet_TenNH_B();
                }
                else if (msgBox_type == "Ma_HaiQuan" && msgBox_txtID == 'txtMaHQPH') {
                   // alert('OK');
                    jsLoadByCQT();
                }
                
                if (strTitle != null) {
                    document.getElementById(msgBox_txtTitle).value = strTitle;
                }
                
                if (msgBox_txtFocus != null) {
                 //   console.log(msgBox_txtFocus);
                    document.getElementById(msgBox_txtFocus).focus();
                }
            }
            //window.parent.returnValue = arr;
            //window.parent.close();
        }
        function getParam(param) {
            var query = window.location.search.substring(1);
            var parms = query.split('&');
            for (var i = 0; i < parms.length; i++) {
                var pos = parms[i].indexOf('=');
                if (pos > 0) {
                    var key = parms[i].substring(0, pos).toLowerCase();
                    var val = parms[i].substring(pos + 1);
                    if (key == param.toLowerCase())
                        return val;
                }
            }
            jsDivAutoCenter('.msgBox');
            return '';
        }

        function jsBuildResultTable(strInput) {
            var strRet = '';
            if (strInput.length > 0) {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                var arr = strInput.split("|");
                for (var i = 0; i < arr.length - 1; i++) {
                    var strID = arr[i].split(";")[0];
                    var strTitle = arr[i].split(";")[1];
                    strRet += "<tr onclick=\"SelectDanhMuc('" + strID + "','" + strTitle + "');\" class='PH'>" + "<td width='150' align='left'><b>" + strID + "</b></td><td  align='left'>" + strTitle + "</td></tr><tr><td colspan='2' height='8' ></td></tr> <tr><td colspan='2' class='line'></td></tr>";
                }
                strRet += "</table>";
            } else {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td width='100%' align='left' class='nav'>Không tồn tại danh mục này !</td></tr></table>";
            }
            return strRet;
        }
    </script>
</asp:Content>
