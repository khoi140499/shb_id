﻿Imports System.Data
Imports VBOracleLib
Imports Business.BaoCao
Imports Business.XLCuoiNgay
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables

''' <summary>
'''  Mục đích  : Báo cáo in bảng kê chứng từ
'''  Người tạo : Kiênvt
'''  Ngày tạo  :
''' </summary>
''' <remarks></remarks>

Partial Class pages_XLCuoiNgay_frmLapBKCT
    Inherits System.Web.UI.Page

    Private mv_strNgay As String = String.Empty

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String
    Private mv_strTenDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private Shared strMaNhom_GDV As String = ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Private mv_strLoaiBaoCao As String

#Region "Form Events"

    Private Sub frmLapBKCT_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Not IsPostBack Then
            Try
                DM_DiemThu() 'Load dm điểm thu được phép phân quyền
                DM_LoaiThue() 'Load dm loại thuế
                DM_BangKe() 'Load dm bang ke
                DM_NV() 'Load Dm nhan vien
                txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                txtDenNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx")
    End Sub

    Private Sub cmdIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        cmdIn.Enabled = False
        Try
            If (Not CheckThongTin()) Then
                Return
            Else
                GetReportType() 'Chon loai bao cao

                Select Case mv_strLoaiBaoCao '(mdlSystemVariables.gLoaiBaoCao)
                    Case mdlCommon.Report_Type.CTU_LIETKE
                        TCS_CTU_LIETKE()
                    Case mdlCommon.Report_Type.CTU_BKE
                        TCS_BK_CTU_CHITIET()
                    Case mdlCommon.Report_Type.CTU_CHITIET
                        TCS_BK_CTU_WITH_DTL_CHITIET()
                    Case mdlCommon.Report_Type.CTU_MLNS, _
                        mdlCommon.Report_Type.CTU_CQTCHITIET, mdlCommon.Report_Type.CTU_CQTTONG, _
                        mdlCommon.Report_Type.CTU_DBCHITIET, mdlCommon.Report_Type.CTU_DBTONG
                        TCS_BK_CTU_WITH_DTL()
                    Case mdlCommon.Report_Type.CTU_MAU10
                        TCS_CTU_BANGKE10()
                    Case mdlCommon.Report_Type.CTU_MAU10_1068
                        TCS_CTU_BANGKE10_1068()
                    Case mdlCommon.Report_Type.CTU_MAU10_1068_tong
                        TCS_CTU_BANGKE10_1068_tong()
                    Case mdlCommon.Report_Type.CTU_HUY
                        TCS_CTU_HUY()
                    Case Else
                        TCS_BK_CTU_NO_DTL()
                End Select
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub

    Private Sub GetReportType()
        Select Case cboBangKeCT.SelectedValue.ToString.ToUpper
            Case "R1"
                'mdlSystemVariables.gLoaiBaoCao = Report_Type.CTU_BKE
                mv_strLoaiBaoCao = Report_Type.CTU_BKE
            Case "R2"
                'mdlSystemVariables.gLoaiBaoCao = Report_Type.CTU_LIETKE
                mv_strLoaiBaoCao = Report_Type.CTU_LIETKE
            Case "R3"
                'mdlSystemVariables.gLoaiBaoCao = Report_Type.CTU_SOCT
                mv_strLoaiBaoCao = Report_Type.CTU_SOCT
            Case "R11"
                'mdlSystemVariables.gLoaiBaoCao = Report_Type.CTU_DBCHITIET
                mv_strLoaiBaoCao = Report_Type.CTU_DBCHITIET
            Case "R12"
                'mdlSystemVariables.gLoaiBaoCao = Report_Type.CTU_DBTONG
                mv_strLoaiBaoCao = Report_Type.CTU_DBTONG
            Case "R14"
                'mdlSystemVariables.gLoaiBaoCao = Report_Type.CTU_MAU10_1068
                mv_strLoaiBaoCao = Report_Type.CTU_MAU10_1068
            Case "R15"
                'mdlSystemVariables.gLoaiBaoCao = Report_Type.CTU_HUY
                mv_strLoaiBaoCao = Report_Type.CTU_HUY
            Case "R16"
                'mdlSystemVariables.gLoaiBaoCao = Report_Type.CTU_HUY
                mv_strLoaiBaoCao = Report_Type.CTU_MAU10_1068_tong
        End Select

    End Sub

#End Region

#Region "Load danh mục vào commbobox"

    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strSQL As String = "SELECT a.ma_dthu FROM tcs_dm_diemthu a "
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Dim iCount As Integer 'So diem thu 
            Dim dtDiemthu As DataTable
            dtDiemthu = dsDiemThu.Tables(0)
            iCount = dsDiemThu.Tables(0).Rows.Count    
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub

    Private Sub DM_LoaiThue()
        Try
            Dim dsLT As DataSet = Get_DM_LoaiThue()
            clsCommon.ComboBoxWithValue(cboLoaiThue, dsLT, 1, 0, "Tất cả")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DM_BangKe()
        Me.cboLoaiThue.Items.Add(New ListItem("dislay", "value"))
        Dim htBangKe As New Hashtable()
        htBangKe.Add("R1", "Chi tiết")
        htBangKe.Add("R2", "Liệt kê")
        htBangKe.Add("R3", "Số chứng từ")
        htBangKe.Add("R11", "Địa bàn thu chi tiết")
        htBangKe.Add("R12", "Địa bàn thu tổng")
        htBangKe.Add("R14", "Bảng kê chứng từ")
        htBangKe.Add("R15", "Bảng kê chứng từ hủy duyệt")
        htBangKe.Add("R16", "Bảng kê giấy nộp tiền tổng")
        Me.cboBangKeCT.Items.Clear()
        Me.cboBangKeCT.DataSource = htBangKe
        Me.cboBangKeCT.DataValueField = "Key"
        Me.cboBangKeCT.DataTextField = "Value"
        Me.cboBangKeCT.DataBind()
        Me.cboBangKeCT.SelectedIndex = 0
    End Sub

    Private Sub DM_NV()

        Dim dsNV As DataSet
        Dim dk As String = "AND BAN_LV='" & mv_strMaDiemThu & "'"

        Dim strSQL As String = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a Where a.ma_nhom='02' " & dk
        Try
            dsNV = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(CboNhanVien, dsNV, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub
#End Region

#Region "Ẩn hiện control"
    Private Sub AnHienRadioBaoCao()
        'optMLNS.Enabled = False
        'optMaDTNT.Enabled = False
        'optThuTuNhap.Enabled = False
        'optTKCo.Enabled = False
        'optTKNo.Enabled = False
        'optCQTChiTiet.Enabled = False
        'optCQTTong.Enabled = False
        'optDBChitiet.Enabled = True
        'optDBTong.Enabled = True
        'optLoaithue.Enabled = False
    End Sub
    Private Sub AnHienChiTiet()
        txtTKCo.Enabled = False
        txtTKNo.Enabled = False
        cboLoaiThue.Enabled = True
        txtMaNNThue.Enabled = True
        txtMaDB.Enabled = True
    End Sub
    Private Sub AnHienSoCT()
        txtTKCo.Enabled = False
        txtTKNo.Enabled = False
        cboLoaiThue.Enabled = False
        txtMaNNThue.Enabled = False
        txtMaDB.Enabled = False
    End Sub
    Private Sub AnHienMLNS()
        txtTKCo.Enabled = False
        txtTKNo.Enabled = False
        cboLoaiThue.Enabled = False
        txtMaNNThue.Enabled = False
        txtMaDB.Enabled = False
    End Sub
    Private Sub AnHienDTNT()
        txtTKCo.Enabled = False
        txtTKNo.Enabled = False
        cboLoaiThue.Enabled = False
        txtMaNNThue.Enabled = True
        txtMaDB.Enabled = False
    End Sub
    Private Sub AnHienTKNo()
        txtTKCo.Enabled = False
        txtTKNo.Enabled = True
        cboLoaiThue.Enabled = False
        txtMaNNThue.Enabled = False
        txtMaDB.Enabled = False
    End Sub
    Private Sub AnHienTKCo()
        txtTKCo.Enabled = True
        txtTKNo.Enabled = False
        cboLoaiThue.Enabled = False
        txtMaNNThue.Enabled = False
        txtMaDB.Enabled = False
    End Sub
    Private Sub AnHienDiaBan()
        txtTKCo.Enabled = False
        txtTKNo.Enabled = False
        cboLoaiThue.Enabled = False
        txtMaNNThue.Enabled = False
        txtMaDB.Enabled = True
    End Sub
    Private Sub AnHienLoaiThue()
        txtTKCo.Enabled = False
        txtTKNo.Enabled = False
        cboLoaiThue.Enabled = True
        txtMaNNThue.Enabled = False
        txtMaDB.Enabled = False
    End Sub
    Private Sub AnHienKhac()
        txtTKCo.Enabled = False
        txtTKNo.Enabled = False
        cboLoaiThue.Enabled = False
        txtMaNNThue.Enabled = False
        txtMaDB.Enabled = False
    End Sub
#End Region

#Region "Điều kiện tra cứu"

    Private Function WhereNgay() As String      
        Dim strWhereNgay As String = ""
        Dim strNgayStart, strNgayEnd As String
        strNgayStart = ConvertDateToNumber(txtTuNgay.Text.ToString())
        strNgayEnd = ConvertDateToNumber(txtDenNgay.Text.ToString())
        If cboNgayTinh.SelectedValue = "2" Then
            strWhereNgay = strWhereNgay & " and (hdr.Ngay_KB >= " & strNgayStart & ") "
            strWhereNgay = strWhereNgay & " and (hdr.Ngay_KB <= " & strNgayEnd & ") "
        ElseIf cboNgayTinh.SelectedValue = "1" Then
            strWhereNgay = strWhereNgay & " and ( TO_CHAR(hdr.ngay_kh_nh,'yyyymmdd') >= " & strNgayStart & ") "
            strWhereNgay = strWhereNgay & " and ( TO_CHAR(hdr.ngay_kh_nh,'yyyymmdd') <= " & strNgayEnd & ") "
        Else 'Ngay he thong
            strWhereNgay = strWhereNgay & " and ( TO_CHAR(hdr.ngay_ht,'yyyymmdd') >= " & strNgayStart & ") "
            strWhereNgay = strWhereNgay & " and ( TO_CHAR(hdr.ngay_ht,'yyyymmdd') <= " & strNgayEnd & ") "
        End If

        If txtTuNgay.Text.ToString() = txtDenNgay.Text.ToString() Then
            mv_strNgay = "Ngày: " & txtTuNgay.Text.ToString()
        Else
            mv_strNgay = "Từ ngày: " & txtTuNgay.Text.ToString() & " đến ngày: " & txtDenNgay.Text.ToString()
        End If
        Return strWhereNgay
    End Function
    Private Function WhereNgay1068tong() As String
        Dim strWhereNgay As String = ""
        Dim strNgayStart, strNgayEnd As String
        strNgayStart = ConvertDateToNumber(txtTuNgay.Text.ToString())
        strNgayEnd = ConvertDateToNumber(txtDenNgay.Text.ToString())

        strWhereNgay = strWhereNgay & " and (hdr.Ngay_KB = " & strNgayStart & ") "
        
        If txtTuNgay.Text.ToString() = txtDenNgay.Text.ToString() Then
            mv_strNgay = "Ngày: " & txtTuNgay.Text.ToString()
        Else
            mv_strNgay = "Từ ngày: " & txtTuNgay.Text.ToString() & " đến ngày: " & txtDenNgay.Text.ToString()
        End If
        Return strWhereNgay
    End Function
    Private Function WhereBanThu() As String
        Dim strWhereBanThu As String = ""
        strWhereBanThu = " and ( hdr.ma_dthu in ('" & mv_strMaDiemThu & "')) "
        Return strWhereBanThu
    End Function

    Private Function WhereDiaBan() As String
        Dim strWhereDB As String = ""
        If (txtMaDB.Enabled = True) Then
            If (txtMaDB.Text.Trim() <> "") Then
                strWhereDB = " and ((hdr.ma_xa) like '" & RemoveApostrophe(Trim(txtMaDB.Text.Replace(".", ""))) & "%') "
            End If
        End If
        Return strWhereDB
    End Function

    Private Function WhereTKCo() As String
        Dim strTKCo As String = ""
        If ((txtTKCo.Enabled = True) And (txtTKCo.Text.Trim() <> "")) Then
            strTKCo = " and (hdr.TK_Co like '" & RemoveApostrophe(Trim(txtTKCo.Text.Replace(".", ""))) & "%') "
        End If
        Return strTKCo
    End Function

    Private Function WhereTKNo() As String
        Dim strTKNo As String = ""
        If ((txtTKNo.Enabled = True) And (txtTKNo.Text.Trim() <> "")) Then
            strTKNo = " and (hdr.TK_No like '" & RemoveApostrophe(Trim(txtTKNo.Text.Replace(".", ""))) & "%') "
        End If
        Return strTKNo
    End Function

    Private Function WhereNNT() As String
        Dim strWhereNNT As String = ""
        If (txtMaNNThue.Enabled = True) Then
            If (txtMaNNThue.Text.Trim() <> "") Then
                strWhereNNT = " and (hdr.ma_nnthue like '" & RemoveApostrophe(Trim(txtMaNNThue.Text)) & "%') "
            End If
        End If
        Return strWhereNNT
    End Function

    Private Function WhereLoaiThue() As String
        Dim strWhereLThue As String = ""
        If (cboLoaiThue.Enabled = True) Then
            If (cboLoaiThue.SelectedIndex > 0) Then
                strWhereLThue = " and (hdr.ma_lthue = '" & cboLoaiThue.SelectedValue.ToString() & "') "
            End If
        End If
        Return strWhereLThue
    End Function

    Private Function WhereMaNV() As String
        Dim strWhereMaNV As String = ""
        If Not CboNhanVien.SelectedValue().Trim().Equals("") Then
            strWhereMaNV = " And (hdr.Ma_NV = " & CboNhanVien.SelectedValue().Trim() & ") "
        End If
        Return strWhereMaNV
    End Function

    Private Function WhereSoTK() As String
        Dim strWhereSoTK As String = ""
        Dim strFromSoTK As String = ""
        Dim strToSoTK As String = ""
        If Not txtTuSo.Text.Trim.Equals("") Then
            strFromSoTK = " And (hdr.so_ct > = '" & (txtTuSo.Text.Trim) & "') "
        End If
        If Not txtDenSo.Text.Trim.Equals("") Then
            strToSoTK = " And (hdr.so_ct < = '" & (txtDenSo.Text.Trim) & "') "
        End If
        Return strFromSoTK & strToSoTK
    End Function

    Private Function WhereAll() As String
        Dim strWhereAll As String
        strWhereAll = WhereNgay() & WhereBanThu() & WhereDiaBan() & WhereTKCo() & _
                    WhereTKNo() & WhereNNT() & WhereLoaiThue() & WhereMaNV() & WhereSoTK()
        Return strWhereAll
    End Function
    Private Function Where1068Tong() As String
        Dim strWhereAll As String
        strWhereAll = WhereNgay1068tong() & WhereBanThu() & WhereDiaBan() & WhereTKCo() & _
                    WhereTKNo() & WhereNNT() & WhereLoaiThue() & WhereMaNV() & WhereSoTK()
        Return strWhereAll
    End Function

#End Region

#Region "TCS_CTU"

    Private Sub TCS_BK_CTU_NO_DTL()
        Try
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buXLCN.TCS_BK_CTU_NO_DTL(strWhere)

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            ds.Tables(0).TableName = "TCS_BKE_CTU"

            Dim frm As New infBaoCao
            frm.TCS_DS = ds 'Dataset chứa dữ liệu
            frm.TCS_Ten_KB = mv_strTenKB 'Tên kho bạc
            frm.TCS_MaDiemThu = mv_strTenDiemThu 'Tên điểm thu
            frm.TCS_Ngay_CT = mv_strNgay 'mdlSystemVariables.gdtmNgayLV 'Ngày làm việc
            frm.TCS_BanThu = mv_strTenDiemThu 'Thực ra là tên NSD

            Dim dblTongTien As Double
            dblTongTien = mdlCommon.GetTongTien(ds, "Tien") 'Lấy tổng tiền
            frm.TCS_DichSo = mdlCommon.TCS_Dich_So(dblTongTien, "đồng") 'Chuyển số thành chữ

            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao & "", "ManPower")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub TCS_BK_CTU_WITH_DTL()
        Try
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buXLCN.TCS_BK_CTU_WITH_DTL(strWhere)

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            ds.Tables(0).TableName = "TCS_BKE_CTU"

            Dim frm As New infBaoCao
            frm.TCS_DS = ds 'Dataset chứa dữ liệu
            frm.TCS_Ten_KB = mv_strTenKB 'Tên kho bạc
            frm.TCS_MaDiemThu = mv_strTenDiemThu 'Tên điểm thu
            frm.TCS_Ngay_CT = mv_strNgay 'ConvertNumberToDate(mdlSystemVariables.gdtmNgayLV) 'Ngày làm việc
            frm.TCS_BanThu = mv_strTenDiemThu 'Thực ra là tên NSD

            Dim dblTongTien As Double
            dblTongTien = mdlCommon.GetTongTien(ds, "Tien") 'Lấy tổng tiền
            frm.TCS_DichSo = mdlCommon.TCS_Dich_So(dblTongTien, "đồng") 'Chuyển số thành chữ

            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao & "", "ManPower")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub TCS_BK_CTU_WITH_DTL_CHITIET()
        Try
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buXLCN.TCS_BK_CTU_WITH_DTL_CHITIET(strWhere)

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            ds.Tables(0).TableName = "TCS_BKE_CTU"

            Dim frm As New infBaoCao
            frm.TCS_DS = ds 'Dataset chứa dữ liệu
            frm.TCS_Ten_KB = mv_strTenKB 'Tên kho bạc
            frm.TCS_MaDiemThu = mv_strTenDiemThu 'Tên điểm thu
            frm.TCS_Ngay_CT = mv_strNgay 'ConvertNumberToDate(mdlSystemVariables.gdtmNgayLV) 'Ngày làm việc
            frm.TCS_BanThu = mv_strTenDiemThu 'Thực ra là tên NSD

            Dim dblTongTien As Double
            dblTongTien = mdlCommon.GetTongTien(ds, "Tien") 'Lấy tổng tiền
            frm.TCS_DichSo = mdlCommon.TCS_Dich_So(dblTongTien, "đồng") 'Chuyển số thành chữ

            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao & "", "ManPower")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub TCS_BK_CTU_CHITIET()
        Try
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As New DataSet

            ds = buXLCN.TCS_BK_CTU_CHITIET(strWhere)

            Dim strLoaiThue As String = cboLoaiThue.SelectedValue.ToString()

            If (strLoaiThue = mdlSystemVariables.gstrLT_TB_XeMay) Then
                '                mdlSystemVariables.gLoaiBaoCao = mdlCommon.Report_Type.CTU_CQTHU_THUE
                mv_strLoaiBaoCao = mdlCommon.Report_Type.CTU_CQTHU_THUE

            Else
                'mdlSystemVariables.gLoaiBaoCao = mdlCommon.Report_Type.CTU_CQTHU
                mv_strLoaiBaoCao = mdlCommon.Report_Type.CTU_CQTHU
            End If

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            ds.Tables(0).TableName = "TCS_CTU"

            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            frm.TCS_Ten_KB = mv_strTenKB 'Tên kho bạc
            frm.TCS_Tungay = txtTuNgay.Text.ToString()
            frm.TCS_Denngay = txtDenNgay.Text.ToString()
            frm.TCS_Ten_KTT = mdlSystemVariables.gstrTen_KTT
            frm.Ten_KeToan = mdlSystemVariables.gstrHoTenND
            frm.TCS_MaDiemThu = mv_strTenDiemThu
            Dim dblTongTien As Double
            dblTongTien = mdlCommon.GetTongTien(ds, "so_tien") 'Lấy tổng tiền
            frm.TCS_DichSo = mdlCommon.TCS_Dich_So(dblTongTien, "đồng") 'Chuyển số thành chữ

            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao & "", "ManPower")
        Catch ex As Exception
            Throw ex
        Finally
            'mdlSystemVariables.gLoaiBaoCao = mdlCommon.Report_Type.CTU_BKE
            mv_strLoaiBaoCao = mdlCommon.Report_Type.CTU_BKE
        End Try
    End Sub

    Private Sub TCS_CTU_LIETKE()
        Try
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buXLCN.TCS_CTU_LIETKE(strWhere)

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            ds.Tables(0).TableName = "TCS_CTU"

            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            frm.TCS_Ten_KB = mv_strTenKB 'Tên kho bạc
            frm.TCS_Ma_KB = mv_strSHKB 'Số kiệu kho bạc
            frm.TCS_Ngay_CT = mv_strNgay
            frm.TCS_Ten_KTT = mdlSystemVariables.gstrTen_KTT
            frm.Ten_KeToan = mdlSystemVariables.gstrHoTenND
            frm.TCS_MaDiemThu = mv_strTenDiemThu
            Dim dblTongTien As Double
            dblTongTien = mdlCommon.GetTongTien(ds, "SO_TIEN") 'Lấy tổng tiền
            frm.TCS_DichSo = mdlCommon.TCS_Dich_So(dblTongTien, "đồng") 'Chuyển số thành chữ

            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao & "", "ManPower")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub TCS_CTU_BANGKE10()
        Try
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buBaoCao.TCS_BKE_MAU10(strWhere)

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            ds.Tables(0).TableName = "BKE_CTU"

            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            frm.TCS_Ten_KB = mv_strTenKB 'Tên kho bạc

            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao & "", "ManPower")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub TCS_CTU_BANGKE10_1068()
        Try
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buBaoCao.TCS_BKE_MAU10_1068(strWhere)

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            ds.Tables(0).TableName = "BKE_GNT"

            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            frm.TCS_Ten_KB = mv_strTenKB.ToUpper()

            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao & "", "ManPower")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub TCS_CTU_BANGKE10_1068_tong()
        Try
            Dim strNgay As Date
            Dim strNgayKH_NH As String
            Dim strNgayKB As String

            strNgayKB = ConvertDateToNumber(txtTuNgay.Text.ToString())
            Dim strWhere As String = Where1068Tong()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buBaoCao.TCS_BKE_MAU10_1068(strWhere)
            Dim dt As DataTable
            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If
            ds.Tables(0).TableName = "BKE_GNT"
            'ds.WriteXml("c:\baocao.xml", XmlWriteMode.WriteSchema)
            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            frm.TCS_Ten_KB = mv_strTenKB.ToUpper()
            'Lay ngoai gio

            strNgay = ConvertNumberToDate(strNgayKB)
            'strNgay = Date.Parse(strNgayKB, "yyyyMMdd")

            If strNgay.DayOfWeek = DayOfWeek.Monday Then
                ' Nếu là thứ 2 thì chuyển sang thứ 7 tuần sau
                strNgay = strNgay.AddDays(-2)
            Else
                ' Không thì giam di một ngày
                strNgay = strNgay.AddDays(-1)
            End If
            strNgayKH_NH = strNgay.ToString("dd/MM/yyyy")
            strNgayKH_NH = ConvertDateToNumber(strNgayKH_NH)
            dt = getCT_TheoGio(mv_strMaDiemThu, strNgayKB, strNgayKH_NH)
            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                frm.Ngay_NgoaiGio = dt.Rows(0)("ngay_kh_nh").ToString
                frm.SoMon_NgoaiGio = dt.Rows(0)("soluong").ToString
                frm.TienNgoaiGio = dt.Rows(0)("tien").ToString
            Else
                frm.Ngay_NgoaiGio = ConvertNumberToDate(strNgayKH_NH)
                frm.SoMon_NgoaiGio = "0"
                frm.TienNgoaiGio = "0"
            End If
            dt = getCT_TheoGio(mv_strMaDiemThu, strNgayKB, strNgayKB)
            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                frm.Ngay_TrongGio = dt.Rows(0)("ngay_kh_nh").ToString
                frm.SoMon_TrongGio = dt.Rows(0)("soluong").ToString
                frm.TienTrongGio = dt.Rows(0)("tien").ToString
            Else
                frm.Ngay_TrongGio = ConvertNumberToDate(strNgayKB)
                frm.SoMon_TrongGio = "0"
                frm.TienTrongGio = "0"
            End If
            'frm.TCS_Ma_KB = mv_strSHKB 'Số kiệu kho bạc
            'frm.TCS_Ngay_CT = mv_strNgay
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao & "", "ManPower")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub TCS_CTU_HUY()
        Try
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buBaoCao.TCS_CTU_HUY(strWhere)

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            ds.Tables(0).TableName = "BKE_CT_HUY"
            'ds.WriteXml("c:\baocao.xml", XmlWriteMode.WriteSchema)
            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            frm.TCS_Ten_KB = mv_strTenKB 'Tên kho bạc
            frm.TCS_Ma_KB = mv_strSHKB 'Số kiệu kho bạc
            frm.TCS_Ngay_CT = mv_strNgay
            frm.TCS_Ten_KTT = mdlSystemVariables.gstrTen_KTT
            frm.Ten_KeToan = mdlSystemVariables.gstrHoTenND
            frm.TCS_MaDiemThu = mv_strTenDiemThu
            Dim dblTongTien As Double
            dblTongTien = mdlCommon.GetTongTien(ds, "SO_TIEN")

            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao & "", "ManPower")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
  Private Shared Function getCT_TheoGio(ByVal strMaDiemThu As String, ByVal strNgayKB As String, ByVal strNgayKH_NH As String) As DataTable
        Dim strSQL As String
        Dim strWhere As String = ""
        Dim dt As DataTable
        Try
            If strNgayKB <> strNgayKH_NH Then
                strWhere = " and to_char(ngay_kh_nh,'yyyyMMdd') >=" + strNgayKH_NH + " and to_char(ngay_kh_nh,'yyyyMMdd') <" + strNgayKB
            Else
                strWhere = " and to_char(ngay_kh_nh,'yyyyMMdd') =" + strNgayKB
            End If
            strSQL = " select ngay_kh_nh,count(*) soluong, sum(ttien) tien from tcs_ctu_hdr where ma_dthu='" + strMaDiemThu + "' and ngay_kb=" + strNgayKB + strWhere + " and trang_thai=1 group by ngay_kh_nh order by ngay_kh_nh"
            dt = DataAccess.ExecuteToTable(strSQL)
            ' If Not dt Is Nothing Then
            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

#End Region

#Region "Điều khiển"

    Private Function CheckThongTin() As Boolean
        If (mdlCommon.ConvertDateToNumber(txtTuNgay.Text.ToString()) > ConvertDateToNumber(txtDenNgay.Text.ToString())) Then
            clsCommon.ShowMessageBox(Me, "Giá trị từ ngày phải trước giá trị đến ngày.")
            txtTuNgay.Focus()
            Return False
        End If
        Return True
    End Function
#End Region

End Class
