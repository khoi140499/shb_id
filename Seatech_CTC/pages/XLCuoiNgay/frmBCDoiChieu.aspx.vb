﻿Imports System.Data
Imports VBOracleLib
Imports Business.BaoCao
Imports Business.XLCuoiNgay
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables

Partial Class pages_XLCuoiNgay_frmBC_DoiChieu
    Inherits System.Web.UI.Page

    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String

#Region "Page event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                DM_DiemThu() 'Load dm điểm thu được phép phân quyền
                DM_KenhCtu()
                txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                txtDenNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                clsCommon.addPopUpCalendarToImage(btnCalendar1, txtTuNgay, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnCalendar2, txtDenNgay, "dd/mm/yyyy")
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub




    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        If ConvertDateToNumber(txtTuNgay.Text.ToString()) > ConvertDateToNumber(txtDenNgay.Text.ToString) Then
            clsCommon.ShowMessageBox(Me, "Từ ngày không được lớn hơn đến ngày!")
            Exit Sub
        End If
        Try
            TCS_BKE_DOICHIEU()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub

#End Region

#Region "Call Report"
    Private Sub TCS_BKE_DOICHIEU()
        Try
            mv_strLoaiBaoCao = Report_Type.BKE_DOICHIEU
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buBaoCao.TCS_BKE_DOICHIEU(strWhere)
            ' ds.WriteXml("C://baocaodoichieu.xml", XmlWriteMode.WriteSchema)
            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            'ds.Tables(0).TableName = "BKE_DC"
            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            If cboDiemThu.SelectedValue <> "" Then
                frm.TCS_MaDiemThu = cboDiemThu.SelectedItem.Text
            Else
                frm.TCS_MaDiemThu = "Tất cả" 'mv_strMaDiemThu.ToUpper()
            End If
            'frm.TCS_MaDiemThu = mv_strTenDiemThu
            frm.TCS_Ngay_CT = mv_strNgay
            frm.Ngay = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 7, 2)
            frm.Thang = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 5, 2)
            frm.Nam = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 1, 4)
            frm.TCS_Tungay = txtTuNgay.Text.ToString()
            frm.TCS_Denngay = txtDenNgay.Text.ToString()
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao, "ManPower")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Load danh muc"
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a "
            Try
                dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                clsCommon.ComboBoxWithValue(cboDiemThu, dsDiemThu, 1, 0, "Tất cả")

            Catch ex As Exception
                log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
                Throw ex
            Finally

            End Try
        Else

            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where   branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
            Try
                dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                ' clsCommon.ComboBoxWithValue(cboDiemThu, dsDiemThu, 1, 0)
                clsCommon.ComboBoxWithValue(cboDiemThu, dsDiemThu, 1, 0, "Tất cả")
                'cboDiemThu.DataSource = dsDiemThu
                'cboDiemThu.DataTextField = dsDiemThu.Tables(0).Columns(1).ToString
                'cboDiemThu.DataValueField = dsDiemThu.Tables(0).Columns(0).ToString
                'cboDiemThu.DataBind()

            Catch ex As Exception
                log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
                Throw ex
            Finally

            End Try

        End If

    End Sub

    Private Sub DM_KenhCtu()

        Dim dsKCTu As DataSet
        Dim dk As String = ""
        'dk = "And ma_cn like'%" & cboDiemThu.SelectedValue.ToString & "%'"

        Dim strSQL As String = "SELECT a.ma_kenh,a.ten_kenh FROM tcs_kenh_ct a " ' & dk
        Try
            dsKCTu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(DDKenhCtu, dsKCTu, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub
    'Private Sub DM_DiemThu()
    '    Dim cnDiemThu As New DataAccess
    '    Dim dsDiemThu As DataSet
    '    Dim strwhere As String = ""
    '    Dim strcheckHSC As String = ""
    '    Dim item As ListItem
    '    strcheckHSC = checkHSC()
    '    If strcheckHSC = "0" Then
    '        'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
    '        ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"

    '        Dim strSQL As String = "SELECT a.ma_dthu,a.Ten FROM tcs_dm_diemthu a " & strwhere
    '        Try
    '            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '            clsCommon.ComboBoxWithValue(cboDiemThu, dsDiemThu, 1, 0, "Tất cả")

    '        Catch ex As Exception
    '            Throw ex
    '        Finally
    '            If (Not IsNothing(cnDiemThu)) Then DataAccess.Dispose()
    '        End Try
    '    Else
    '        item = New ListItem(mv_strTenDiemThu, mv_strMaDiemThu, True)
    '        item.Selected = True
    '        cboDiemThu.Items.Add(item)
    '    End If

    'End Sub

#End Region

#Region "Where Clause"
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    Private Function WhereNgay() As String
        Dim strWhereNgay As String
        Dim strNgayStart As String
        Dim strNgayEnd As String
        strNgayStart = ConvertDateToNumber(txtTuNgay.Text.ToString())
        strNgayEnd = ConvertDateToNumber(txtDenNgay.Text.ToString())
        strWhereNgay = " and (TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') >=" & strNgayStart & ") "
        strWhereNgay = strWhereNgay & " and (TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') <=" & strNgayEnd & ") "
        mv_strNgay = "Ngày " & txtTuNgay.Text.ToString()
        Return strWhereNgay
    End Function

    Private Function WhereMaDthu() As String
        Dim strWhereMaNV As String = ""
        Dim strcheckHSC As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If cboDiemThu.SelectedValue <> "" Then
            strWhereMaNV = " And (d.id= '" & cboDiemThu.SelectedValue & "') "
        Else
            If strcheckHSC <> "1" Then
                strWhereMaNV = " And (d.branch_id= '" & Session.Item("MA_CN_USER_LOGON").ToString.Substring(0, 6).ToString & "') "
            End If
        End If

        Return strWhereMaNV
    End Function

    Private Function WhereTrang_Thai() As String
        Dim strWhereMaNV As String = ""
        If Not DDTrangThai.SelectedValue().Trim().Equals("") Then
            If DDTrangThaiCT.SelectedValue = "04" Then
                If DDTrangThai.SelectedValue().Trim() = "1" Then
                    strWhereMaNV = " And (hdr.tt_cthue =0) "
                Else
                    strWhereMaNV = " And (hdr.tt_cthue =1) "
                End If
            Else
                If DDTrangThaiCT.SelectedValue = "01" Then
                    strWhereMaNV = " And (hdr.tt_cthue =" & DDTrangThai.SelectedValue().Trim() & ") "
                End If

            End If
        End If
        Return strWhereMaNV
    End Function
    Private Function WhereTrang_ThaiCT() As String
        Dim strWhereMaNV As String = ""
        If DDTrangThaiCT.SelectedValue().Trim().Equals("") Then
            If DDTrangThai.SelectedValue().Trim().Equals("") Then
                strWhereMaNV = " and (trang_thai = '01' or (trang_thai = '04' and hdr.ma_ks<>'0') ) "
            End If
            If DDTrangThai.SelectedValue().Trim().Equals("0") Then
                strWhereMaNV = " and ((trang_thai = '01' and hdr.tt_cthue =0) or (trang_thai = '04' and hdr.tt_cthue =1 and hdr.ma_ks<>'0') ) "
            End If
            If DDTrangThai.SelectedValue().Trim().Equals("1") Then
                strWhereMaNV = " and ((trang_thai = '01' and hdr.tt_cthue =1) or (trang_thai = '04' and hdr.tt_cthue =0 and hdr.ma_ks<>'0') ) "
            End If
        End If
        If DDTrangThaiCT.SelectedValue().Trim().Equals("01") Then
            strWhereMaNV = " and trang_thai = '01' "
        End If
        If DDTrangThaiCT.SelectedValue().Trim().Equals("04") Then
            strWhereMaNV = " and (trang_thai = '04' and hdr.ma_ks<>'0') "
        End If
        Return strWhereMaNV
    End Function
    Private Function WhereLoai_thue() As String
        Dim strWhereLoaiThue As String = ""
        If Not ddLoaiThue.SelectedValue().Trim().Equals("") Then
            If Not ddLoaiThue.SelectedValue = "00" Then
                If ddLoaiThue.SelectedValue = "04" Then
                    strWhereLoaiThue = " And (hdr.ma_lthue = '04') "
                Else
                    strWhereLoaiThue = " And (hdr.ma_lthue <> '04') "
                End If
            End If
        End If
        Return strWhereLoaiThue
    End Function
    Private Function WhereKenh_Ctu() As String
        Dim strWhereKenhCtu As String = ""
        If Not DDKenhCtu.SelectedValue().Trim().Equals("") Then
            strWhereKenhCtu = " And (hdr.Kenh_ct = " & DDKenhCtu.SelectedValue().Trim() & ") "
        End If
        Return strWhereKenhCtu
    End Function
    Private Function WhereAll() As String
        Dim strWhereAll As String
        strWhereAll = WhereNgay() & WhereMaDthu() & WhereTrang_Thai() & WhereTrang_ThaiCT() & WhereLoai_thue() & WhereKenh_Ctu()
        Return strWhereAll
    End Function
#End Region

End Class
