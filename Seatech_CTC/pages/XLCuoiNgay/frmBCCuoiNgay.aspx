﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmBCCuoiNgay.aspx.vb" Inherits="pages_XLCuoiNgay_frmBC_CuoiNgay" Title="LẬP BẢNG KÊ CHỨNG TỪ TRONG NGÀY" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">LẬP BẢNG KÊ CHỨNG TỪ TRONG NGÀY</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                <tr align="left">
                                    <td align="left" style="width:20%" class="form_label"><asp:Label ID="Label3" runat="server" Text="Từ ngày" CssClass="label"></asp:Label></td>
                                    <td class="form_control"><span style="display:block; width:50%; position:relative;vertical-align:middle;">
                                        <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                        onblur="CheckDate(this);" onfocus="this.select()" />
                                    </span></td>
                                    <td align="left" style="width:20%" class="form_label"><asp:Label ID="Label9" runat="server" Text="Đến ngày" CssClass="label"></asp:Label></td>
                                    <td class="form_control">
                                    <span style="display:block; width:50%; position:relative; vertical-align:middle;">
                                        <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                         onblur="CheckDate(this);" onfocus="this.select()" />
                                    </span></td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label4" runat="server" Text="Nhân viên thực hiện" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                       <asp:DropDownList ID="cboNhanVien" Width="150px" runat="server" CssClass="inputflat">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="form_label">
                                       <asp:Label ID="Label8" runat="server" Text="Nguyên Tệ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="cboNguyenTe" Width="150px" runat="server" CssClass="inputflat">
                                            <%--<asp:ListItem Text="Tất cả" Value="1" Selected="True"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label5" runat="server" Text="Trạng thái" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="DDTrangThai" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Đã chuyển" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Chưa chuyển" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                   <td class="form_label">
                                        <asp:Label ID="Label6" runat="server" Text="Trạng thái CTừ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="ddTrangThai_CT" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Đã duyệt" Value="01"></asp:ListItem>
                                             <asp:ListItem Text="CT hủy- chờ duyệt" Value="02"></asp:ListItem>
                                            <asp:ListItem Text="Đã hủy" Value="04"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label"><asp:Label ID="Label7" runat="server" Text="Loại thuế" CssClass="label"></asp:Label></td>
                                
                                    <td class="form_control">
                                        <asp:DropDownList ID="ddLoaiThue" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="00" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Thuế nội địa" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="Thuế xuất nhập khẩu" Value="04"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="Label11" runat="server" Text="Kênh chứng từ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="cboKenhCtu" Width="150px" runat="server" CssClass="inputflat"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                 </tr>
                                 <tr align="left" >
                                    <td class="form_label" >
                                        <asp:Label ID="Label2" runat="server" Text="Chi nhánh(PGD)" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control"  style="width:30%">
                                        <asp:DropDownList ID="cboDiemThu" Width="150px" runat="server" CssClass="inputflat"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td >
                                    <td class="form_label" >
                                   <%-- <asp:Label ID="Label9" runat="server" Text="Mẫu bảng kê CT" CssClass="label"></asp:Label>--%>
                                    </td>
                                    <td class="form_label" >
                                    <%-- <asp:DropDownList ID="cboBangKeCT" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Bảng kê trước giờ cut-offtime" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Bảng kê sau giờ cut-offtime" Value="0"></asp:ListItem>
                                        </asp:DropDownList>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                <asp:Button ID="cmdIn" runat="server" CssClass="ButtonCommand" Text="[In BC]"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand" Text="[Thoát]">
                </asp:Button>&nbsp;
            </td>
        </tr>
    </table>
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>
