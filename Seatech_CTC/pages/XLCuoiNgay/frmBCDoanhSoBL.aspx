﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false" EnableEventValidation="false"
    CodeFile="frmBCDoanhSoBL.aspx.vb" Inherits="pages_XLCuoiNgay_frmBCCTDoanhSoBL" Title="BÁO CÁO CHI TIẾT SỐ DƯ BẢO LÃNH THUẾ NHẬP KHẨU" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>
    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        
  
    function validForm(){
              
        if($get('<%=txtDenNgay.clientID%>').value.length == 0){
            alert("Bạn phải nhập ngày kết thúc");
            return false;
        }
        return true;
    }
    
    /****************************TEN_CQTHU*********************************/
       
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
            for (var k = 0; k <= str.length; k++){
                if (k == locs[i]){
                if (str.substring(k, k+1) != delim){
                        str = str.substring(0,k) + delim + str.substring(k,str.length)
                    }
                }
            }
            }
            textbox.value = str
    }
    
       function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) 
    {        
        returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + $get(txtID).value,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        if (returnValue != null) {
            document.getElementById(txtID).value = returnValue.ID;
            if (txtTitle!=null){
                document.getElementById(txtTitle).value = returnValue.Title;                    
            }
            if (txtFocus != null) {
                //if ($get('txtFocus').getAttribute('disabled') != 'disabled') {
                    try {
                        document.getElementById(txtFocus).focus();
                    }
                catch (e) {
                        document.getElementById(txtFocus).focus();
                    };
                //}
            }
        }
        //NextFocus();
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">BÁO CÁO CHI TIẾT SỐ DƯ BẢO LÃNH THUẾ NHẬP KHẨU</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                <tr align="left">
                                    <td class="form_label" >
                                        <asp:Label ID="Label3" runat="server" Text="Chi nhánh" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control" colspan="3">
                                        <%--<asp:TextBox ID="txtMaCN" runat="server" CssClass="inputflat"></asp:TextBox>
                                        <img src="../../images/search.jpeg" width="14" id="btnSearch" runat="server" height="14" onclick="ShowLov('CNNganHang')" />--%>
                                         <asp:DropDownList ID="txtMaCN"  runat="server" CssClass="inputflat"
                                           style="width: 95%; border-color: White; font-weight: bold; text-align: left;">
                                        </asp:DropDownList>
                                    </td>
                                   <%-- <td class="form_label" style="width: 15%">
                                        <asp:Label ID="Label4" runat="server" Text="Tên chi nhánh" CssClass="label"></asp:Label>
                                    </td>--%>
                                  <%--  <td class="form_control">
                                        <asp:TextBox ID="txtTenCN" runat="server" Enabled="false" CssClass="inputflat" style="display:none;" 
                                            Width="175px"></asp:TextBox>
                                    </td>--%>
                                </tr>
                                <tr align="left">
                                    <td width='20%' class="form_label">
                                        <asp:Label ID="Label10" runat="server" Text="Đến ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='35%' class="form_control">
                                        <span style="display:block; width:54%; position:relative; vertical-align:middle;">
                                            <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                             onblur="CheckDate(this);" onfocus="this.select()" />
                                        </span>
                                    </td>
                                       <td class="form_label">
                                        <asp:Label ID="Label5" runat="server" Text="Loại báo cáo" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="ddlLoaiBC" runat="server" CssClass="inputflat" 
                                            Width="176px">
                                            <asp:ListItem Text="Báo cáo tổng hợp" Value="01" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Báo cáo chi tiết" Value="02"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left" style= "display:none">
                                  
                                    <td class="form_label" style="width: 15%;display:none">
                                        <asp:Label ID="Label6" runat="server" Text="Nguyên Tệ" style="display:none" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control" >                                        
                                        <asp:DropDownList ID="cboNguyenTe" Width="150px" style="display:none" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="1" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                      <td class="form_label" style="width: 15%">
                                        <asp:Label ID="Label4" runat="server" Text="Tên chi nhánh" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtTenCN" runat="server" Enabled="false" CssClass="inputflat" style="display:none;" 
                                            Width="175px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                <asp:Button ID="cmdIn" runat="server" CssClass="ButtonCommand" Text="[In BC]" OnClientClick="javascript: return validForm();"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand" Text="[Thoát]">
                </asp:Button>&nbsp;
            </td>
        </tr>
    </table>
     <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
        });
    </script>	
</asp:Content>
