﻿Imports ETAX_GDT
Imports System.Data
Imports System.IO

Partial Class pages_DoiChieu_frmDCXacNhanNTDT
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
            lbGioBC.Text = clsCommon.TCS_GETTS("GIO_DC_CT")
        End If
    End Sub

    Private Sub LoadDefaultGrid()
        Dim dtDF As New DataTable
        dtDF.Columns.Add("kenh")
        dtDF.Columns.Add("gnt")
        dtDF.Columns.Add("tien")

        Dim row As DataRow = dtDF.NewRow
        row(0) = "01"
        row(1) = "0"
        row(2) = "0"
        dtDF.Rows.Add(row)

        Dim row1 As DataRow = dtDF.NewRow
        row1(0) = "00"
        row1(1) = "0"
        row1(2) = "0"
        dtDF.Rows.Add(row1)
        For index As Integer = 0 To dtDF.Rows.Count - 1
            Dim strKenh As String = dtDF.Rows(index)("kenh").ToString
            Dim strSoGNT As String = dtDF.Rows(index)("gnt").ToString
            Dim strTongTien As String = dtDF.Rows(index)("tien").ToString

            dtDF.Rows(index)(0) = GetTenKenh(strKenh)

            dtDF.Rows(index)(2) = String.Format("{0:N0}", Decimal.Parse(strTongTien))
        Next
        dtgdata.DataSource = dtDF
        dtgdata.DataBind()
    End Sub

    Protected Sub btnGuiDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuiDC.Click
        Dim tong_tc As Integer = 0
        Dim tong_tn_tct As Integer = 0
        Dim tong_tn_nh As Integer = 0
        Dim tong_lech_tct As Integer = 0
        Dim tong_lech_nh As Integer = 0
        Dim errorCode As String = ""
        Dim errorMSG As String = ""
        Dim tong_gnt As String = ""
        Dim tong_tien As String = ""
        Dim tg_gui As String = ""
        Dim tu_ngay As String = ""
        Dim den_ngay As String = ""
        Try

            Dim dtResult As New DataTable
            ProcMegService.ProMsg.getMSG32(txtTuNgay.Value, errorCode, errorMSG, tong_gnt, tong_tien, tg_gui, tu_ngay, den_ngay, dtResult)

            For index As Integer = 0 To dtResult.Rows.Count - 1
                Dim strKenh As String = dtResult.Rows(index)("kenh").ToString
                Dim strSoGNT As String = dtResult.Rows(index)("gnt").ToString
                Dim strTongTien As String = dtResult.Rows(index)("tien").ToString

                dtResult.Rows(index)(0) = GetTenKenh(strKenh)

                dtResult.Rows(index)(2) = String.Format("{0:N0}", Decimal.Parse(strTongTien))
            Next

            dtgdata.DataSource = dtResult
            dtgdata.DataBind()

            lbTongGNT.Text = tong_gnt
            lbTongTien.Text = VBOracleLib.Globals.Format_Number(tong_tien, ",")
            lbTGGui.Text = tg_gui
            lbTuNgay.Text = tu_ngay
            lbDenNgay.Text = den_ngay
            If errorCode = "01" Then
                clsCommon.ShowMessageBox(Me, "Gửi báo cáo thành công")
            Else
                clsCommon.ShowMessageBox(Me, "Gửi cáo cáo không thành công. Lỗi: " + errorMSG)
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Gửi cáo cáo thất bại. Lỗi: " + ex.Message)
        End Try

    End Sub

    Private Function GetTenKenh(ByVal strMaKenh As String) As String
        Dim strResult As String = ""
        Dim dt As DataTable = VBOracleLib.DataAccess.ExecuteToTable("select * from tcs_dm_hthuc_nthue where ma_hthuc_nthue='" & strMaKenh & "'")

        If dt.Rows.Count > 0 Then
            strResult = dt.Rows(0)(1).ToString
        End If
        Return strResult
    End Function

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim strHeader As String = ""
        strHeader += "<tr><td>Tổng số giấy nộp tiền thành công</td><td>" + lbTongGNT.Text + "</td></tr>"
        strHeader += "<tr><td>Tổng số tiền đã nộp thành công</td><td>" + lbTongTien.Text + "</td></tr>"
        strHeader += "<tr><td>Thời gian gửi báo cáo</td><td>" + lbTGGui.Text + "</td></tr>"
        strHeader += "<tr><td>Báo cáo từ ngày</td><td>" + lbTuNgay.Text + "</td></tr>"
        strHeader += "<tr><td>Đến ngày</td><td>" + lbDenNgay.Text + "</td></tr>"
        strHeader = "<br/><table border='1'>" + strHeader + "</table><br/><br/>"

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=BC_XACNHAN_NTDT.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Response.ContentEncoding = System.Text.Encoding.Unicode
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble())
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            dtgdata.RenderControl(hw1)
            Response.Output.Write(strHeader + sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
    End Sub

    Private Sub LoadGrid()
        If txtTuNgay.Value.Equals("") Then
            LoadDefaultGrid()
            Return
        End If

        Dim dtResult As DataTable = VBOracleLib.DataAccess.ExecuteToTable("select a.tong_gnt,a.tong_tien,a.tu_ngay_dc,a.den_ngay_dc,a.ngay_dc,a.ngay_gui_bc,a.so_gnt as gnt,a.so_tien as tien,b.dgiai_hthuc_nthue as kenh from tcs_bcao_gip a,tcs_dm_hthuc_nthue b WHERE a.kenh=b.ma_hthuc_nthue and a.ngay_dc='" & txtTuNgay.Value & "'")
        dtgdata.DataSource = dtResult
        dtgdata.DataBind()
        If dtResult.Rows.Count > 0 Then
            Dim strTongGnt As String = dtResult.Rows(0)("tong_gnt").ToString
            Dim strTongTien As String = dtResult.Rows(0)("tong_tien").ToString
            Dim strNgayGuiBc As String = dtResult.Rows(0)("ngay_gui_bc").ToString
            Dim strBaoCaoTuNgay As String = dtResult.Rows(0)("tu_ngay_dc").ToString
            Dim strBaoCaoDenNgay As String = dtResult.Rows(0)("den_ngay_dc").ToString

            lbTongGNT.Text = strTongGnt
            lbTongTien.Text = String.Format("{0:N0}", Decimal.Parse(strTongTien))
            lbDenNgay.Text = strBaoCaoDenNgay
            lbTuNgay.Text = strBaoCaoTuNgay
            lbTGGui.Text = strNgayGuiBc
        Else
            clsCommon.ShowMessageBox(Me, "Không tìm thấy dữ liệu")
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        LoadGrid()
    End Sub
End Class
