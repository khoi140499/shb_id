﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmBaoCaoPhiBoNganh.aspx.cs"
    Inherits="pages_XLCuoiNgay_frmBaoCaoPhiBoNganh" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>

    <script type="text/javascript" src="../../javascript/google/js/jquery-1.8.0.min.js"></script>

    <script src="../../javascript/accounting.min.js" type="text/javascript"></script>

    <link href="../../css/myStyles.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        #btnIn
        {
            width: 14px;
        }
        .style1
        {
            width: 95px;
        }
        #table_Id table
        {
            width: auto;
            border-collapse: collapse;
        }
        #table_Id thead
        {
            background: #000;
            color: #fff;
        }
        #table_Id tbody, td
        {
        }
        #table_Id td
        {
            width: 10em;
        }
        #table_Id tbody
        {
        }
        div #pager
        {
            text-align: center;
            margin: 1em 0;
        }
        div #pager span
        {
            width: 1.8em;
            height: 1.8em;
            line-height: 1.8;
            text-align: center;
            cursor: pointer;
            background: #000;
            color: #fff;
            margin-right: 0.5em;
        }
        div #pager span.active
        {
            background: #c00;
        }
        .td_head
        {
            border-bottom: solid 1px #000;
            border-top: solid 1px #000;
            border-left: solid 1px #000;
            border-right: solid 1px #000;
        }
    </style>
</head>
<body>
    <form runat="server" method="post">
    <div>
        <table cellspacing="0" cellpadding="0" width="95%" border="0" bgcolor="#ffffff" align="center"
            style="border-color: #5DC8D2; border-style: solid; border-width: 2px; margin-top: 10%">
            <!--BEGIN TOP -->
            <tr valign="top" align="center">
            </tr>
            <!-- END TOP -->
            <tr>
                <td style="width: 10px;">
                </td>
                <td style="" valign="top" align="center">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="Table1" runat="server">
                        <tr>
                            <td align="center" style="font-size: 16px; font-weight: bold; padding: 10px;">
                                <asp:Label ID="Label1" runat="server">Kết quả tra cứu chứng từ phí bộ ngành</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span id="labelMessage"></span>
                            </td>
                        </tr>
                        <tr style="text-align: center">
                            <td width='100%' id="tdTextSearch" align="center" valign="top" class='nav'>
                                <div id="divTracuu">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr align="center">
                                            <td width='90%' align="center" valign="top">
                                                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                                    <tr>
                                                        <td width='15%' align="right" valign="top" class="" rowspan="2">
                                                            Ngày báo cáo:
                                                        </td>
                                                        <td width='15%' align="left" valign="top" class="" rowspan="2" runat="server">
                                                            <%=(DateTime.Now).ToString("dd/MM/yyyy")%>
                                                        </td>
                                                        <td width='15%' align="left" valign="top" class="">
                                                            Từ ngày:
                                                        </td>
                                                        <td width='15%' align="left" valign="top" class="" runat="server">
                                                            <div style="display: none" runat="server" id="divTungay">
                                                                <%=(tungay)%></div>
                                                            <div id="divNgayBD" runat="server">
                                                                <%=(tungay)%></div>
                                                        </td>
                                                        <td width='15%' align="left" valign="top" class="">
                                                            Tổng tiền VND:
                                                        </td>
                                                        <td width='15%' align="left" valign="top" class="style1" runat="server">
                                                            <div style="display: none" runat="server" id="divTongtienvnd">
                                                                <%=(tongtienvnd)%></div>
                                                            <div id="tongtienvnd">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width='15%' align="left" valign="top" class="">
                                                            Đến ngày:
                                                        </td>
                                                        <td width='15%' align="left" valign="top" class="" runat="server">
                                                            <div style="display: none" runat="server" id="divDenngay">
                                                                <%=(denngay)%></div>
                                                            <div id="divNgayKT" runat="server">
                                                                <%=(denngay) %></div>
                                                        </td>
                                                        <td width='15%' align="left" valign="top" class="">
                                                            <%--<input type="text" id='txtTest' runat="server" class="inputflat" />--%>
                                                            Tổng tiền USD:
                                                        </td>
                                                        <td width='15%' align="left" valign="top" class="style1" runat="server">
                                                            <div style="display: none" runat="server" id="divTongtienusd">
                                                                <%=(tongtienusd)%></div>
                                                            <div id="tongtienusd">
                                                            </div>
                                                        </td>
                                                        <td width='15%' align="left" valign="top" class="" style="width: 15%">
                                                            Tổng giao dịch
                                                        </td>
                                                        <td width='15%' align="left" colspan="2" valign="top" class="" runat="server">
                                                            <div style="display: none" runat="server" id="divTonggiaodich">
                                                                <%=(tonggiaodich)%></div>
                                                            <div id="tonggiaodich">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2' align="left" class="errorMessage">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                    <tr>
                                        <td colspan='2' height='2'>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="Tr2">
                                        <td align="center" colspan="4">
                                            <div id="div1" style="min-height: 200px; height: auto; width: 100%;" runat="server">
                                                <table class="TFtable paginated grid_data" id="table_Id" style="width: 100%">
                                                    <thead>
                                                        <tr style="background: #b8d1f3">
                                                            <th class="grid_header">
                                                                STT
                                                            </th>
                                                            <th class="grid_header">
                                                                Số hồ sơ
                                                            </th>
                                                            <th class="grid_header">
                                                                Mã số thuế
                                                            </th>
                                                            <th class="grid_header">
                                                                Số CT
                                                            </th>
                                                            <th class="grid_header">
                                                                Năm CT
                                                            </th>
                                                            <th class="grid_header">
                                                                Mã DVQL
                                                            </th>
                                                            <th class="grid_header">
                                                                Ký hiệu CT
                                                            </th>
                                                            <th class="grid_header">
                                                                Mã nguyên tệ
                                                            </th>
                                                            <th class="grid_header">
                                                                Tỷ giá
                                                            </th>
                                                            <th class="grid_header">
                                                                Tổng tiền VND
                                                            </th>
                                                            <th class="grid_header">
                                                                Tổng tiền NT
                                                            </th>
                                                            <th class="grid_header">
                                                                Người lập
                                                            </th>
                                                            <th class="grid_header">
                                                                Người duyệt
                                                            </th>
                                                            <th class="grid_header">
                                                                Mã NH thụ hưởng
                                                            </th>
                                                            <th class="grid_header">
                                                                Tài khoản thụ hưởng
                                                            </th>
                                                            <th class="grid_header">
                                                                Tên NDKT  
                                                            </th>
                                                            <th class="grid_header">
                                                                Trạng thái CT
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="divDraw" runat="server" style="overflow: auto;">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- BEGIN BOTTOM-->
            <!-- END BOTTOM-->
        </table>
        <table>
            <tr>
                <td>
                    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                        <tr>
                            <td width='100%' align="center" valign="top" colspan="2" align="center">
                                <asp:Button ID="btnIn" Text="Export" runat="server" CssClass="ButtonCommand" value="Export"
                                    Width="99px" OnClick="btnIn_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">
    
        var tongtien_vnd=0;var tongtien_usd=0;
        
        function getStringText(id)
        {
            return $("#"+ id).val();
        }
        
        $("#buttonIn").click(function(){
            $.ajax({
            type: "POST",
            url: "frmBaoCaoPhiBoNganh.aspx/GetFileBaoCao",
           
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
             
                    
               }     
          
            });
        });

        $(document).ready(function() {
            $.ajax({
                type: "POST",
                url: "frmBaoCaoPhiBoNganh.aspx/getPhiBoBanNganh",

                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) {
                    var modelGen = JSON.parse(data.d);


                    var json = modelGen;

                    $("#divDraw").children().remove();

                    for (var i = 0; i < json.length; i++) {

                        var tr;
                        if (i % 2 == 0) {
                            tr = $('<tr/>');
                        }
                        else {
                            tr = $('<tr/>');
                        }

                        tr.append('<td>' + (i + 1) + '</td>');
                        tr.append('<td>' + json[i].SO_HS + '</td>');
                        tr.append('<td>' + json[i].NNT_MST + '</td>');
                        tr.append('<td>' + json[i].SO_CT + '</td>');
                        tr.append('<td>' + json[i].NAM_CT_PT + '</td>');
                        tr.append('<td>' + json[i].MA_DVQL + '</td>');
                        tr.append('<td>' + json[i].KYHIEU_CT + '</td>');
                        tr.append('<td>' + json[i].TT_NT_MA_NT + '</td>');
                        tr.append('<td>' + json[i].TT_NT_TYGIA + '</td>');

                        tr.append('<td>' + accounting.formatNumber(json[i].TT_NT_TONGTIEN_VND, 0, ",", ".") + '</td>');
                        tr.append('<td>' + accounting.formatNumber(json[i].TT_NT_TONGTIEN_NT) + '</td>');
                        tr.append('<td>' + json[i].NGUOILAP + '</td>');
                        tr.append('<td>' + json[i].NGUOIDUYET + '</td>');
                        tr.append('<td>' + json[i].TAIKHOAN_NT_MA_NH_TH + '</td>');
                        tr.append('<td>' + json[i].TAIKHOAN_NT_TEN_TK_TH + '</td>');

                        var trangthai_ct = json[i].TRANGTHAI_CT;
                        var tt_ct = json[i].TRANGTHAI_XULY;
                        var resultTTText = getTTCT(trangthai_ct, tt_ct);
                        //if ($("#buttonIn").click) {
                            tr.append('<td>' + drawChiTietCT(json[i].LISTCHITIET, i) + '</td>');
                        //}
                        tr.append('<td>' + resultTTText + "</td>");

                        tr.append('</tr>');

                        tongtien_vnd += parseInt(json[i].TT_NT_TONGTIEN_NT);
                        tongtien_usd += parseInt(json[i].TT_NT_TONGTIEN_VND);

                        $('#divDraw').append(tr);

                    }

                    paginationList(20);
                    $("#tongtienvnd").empty();
                    $("#tongtienusd").empty();
                    $("#tonggiaodich").empty();
                    $("#tongtienvnd").text(accounting.formatNumber(tongtien_vnd, 0, ",", "."));
                    $("#tongtienusd").text(accounting.formatNumber(tongtien_usd));
                    $("#tonggiaodich").text(json.length);
                    //                 
                }


            });
        });
        
        function drawChiTietCT(data, index)
        {
            if(data!=undefined)
            {
            var tr = '';
            //console.log(data);
            for (var i = 0; i < data.length; i++) {
                
                tr += '<span/>';
                tr += (i+1).toString() + ' ';
                //tr += data[i].NDKT.toString() + ' ';
                tr +=data[i].Ten_NDKT.toString() + ' ';
                //tr +=data[i].SoTien_VND.toString() + ' ';
                //tr +=data[i].SoTien_NT.toString() + ' ';                
                tr +='</span><br/>';                
                 //$(this).append(tr);  
            }}
            return tr;
        }
        
        function paginationList(listPaginationCount)
        {
            
            $('table.paginated').each(function() {
                var currentPage = 0;
                var numPerPage = listPaginationCount;    
                var $pager=$('.pager').remove();                
                var $table = $(this);
                $table.bind('repaginate', function() {
                    $table.find('tbody#divDraw tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody#divDraw tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div id="pager"></div>');
              
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function(event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');                    
                }
                
                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });
        }    
        
        function getTTCT(code, tt_ct)
        {            
         
            
            var textReturn ='';
            if(code=='00')
            {
                textReturn="Đã hoàn thiện";
            }
            if(code=='01' && tt_ct=="1")
            {
                textReturn="Đã kiểm soát";
            }
            if(code=='01' && tt_ct=="0")
            {
                textReturn="Chuyển thuế lỗi";
            }
            if(code=='02')
            {
                textReturn="Hủy bởi GDV chờ duyệt";
            }
            if(code=='03')
            {
                textReturn="Chuyển trả";
            }
            if(code=='04')
            {
                textReturn="Đã hủy bởi GDV";
            }
            if(code=='05')
            {
                textReturn="Chờ kiểm soát";
            }
            if(code=='06')
            {
                textReturn="Chuyển Thuế/HQ lỗi";
            }
            if(code=='07')
            {
                textReturn="Đã hủy bởi KSV";
            }
            if(code=='08')
            {
                textReturn="Duyệt hủy CT HQ lỗi";
            }
            
            return textReturn;
        }
        
    </script>

    </form>
</body>
</html>
