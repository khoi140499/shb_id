﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmLapBKCT.aspx.vb" Inherits="pages_XLCuoiNgay_frmLapBKCT" Title="Lập bảng kê chứng từ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">			
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
                }
                textbox.value = str
        }
         
	function ShowLov(strType)
    {
        if (strType=="NNT") return FindNNT_NEW('<%=txtMaNNThue.ClientID %>', '<%=txtTenNNThue.ClientID %>');                 
        if (strType=="DBHC") return FindDanhMuc('DBHC','<%=txtMaDB.ClientID %>', '<%=txtTenDiaBan.ClientID %>','<%=txtTKCo.ClientID %>'); 
        if (strType=="TKCo") return FindDanhMuc('TaiKhoan','<%=txtTKCo.ClientID %>','<%=txtTenTKCo.ClientID %>','<%=txtTKNo.ClientID %>'); 
        if (strType=="TKNo") return FindDanhMuc('TaiKhoan','<%=txtTKNo.ClientID %>','<%=txtTenTKNo.ClientID %>','<%=txtMaNNThue.ClientID %>');                    
    }
    
     function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) {
        var returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                if (txtTitle!=null){
                    document.getElementById(txtTitle).value = returnValue.Title;                    
                }
                if (txtFocus!=null){
                    document.getElementById(txtFocus).focus();
                }
            }
        }
    }
    
     function FindNNT_NEW(txtID, txtTitle) 
     {
        var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?initParam=" +  document.getElementById(txtID).value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
        }
    }   
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">LẬP BẢNG KÊ CHỨNG TỪ</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="lblFromDate" runat="server" Text="Từ ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td style="width: 180px" class="form_control">
                                        <asp:TextBox runat="server" ID="txtTuNgay" Width="100px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="javascript:return mask(this.value,this,'2,5','/');" MaxLength="10"></asp:TextBox>&nbsp;
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="lblToDate" runat="server" Text="Đến ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox runat="server" ID="txtDenNgay" Width="100px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="javascript:return mask(this.value,this,'2,5','/');" MaxLength="10"></asp:TextBox>&nbsp;
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label2" runat="server" Text="Từ số CT" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtTuSo" runat="server" CssClass="inputflat" Width="150px"> </asp:TextBox>
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="Label3" runat="server" Text="Đến số CT" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtDenSo" runat="server" CssClass="inputflat" Width="150px"> </asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label4" runat="server" Text="Loại thuế" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="cboLoaiThue" Width="100%" runat="server" CssClass="inputflat">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="Label5" runat="server" Text="Mẫu bảng kê CT" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="cboBangKeCT" Width="90%" runat="server" CssClass="inputflat">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label6" runat="server" Text="Mã NV" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="CboNhanVien" Width="100%" runat="server" CssClass="inputflat">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="Label7" runat="server" Text="Tính theo ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="cboNgayTinh" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Ngày NH" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Ngày KB" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Ngày HT" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label8" runat="server" Text="Địa bàn" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtMaDB" runat="server" CssClass="inputflat" Width="100%" BackColor="Aqua"
                                            onkeypress="if (event.keyCode==13){ShowLov('DBHC');}"> </asp:TextBox>
                                    </td>
                                    <td colspan="2" class="form_control">
                                        <asp:TextBox ID="txtTenDiaBan" runat="server" CssClass="inputflat" Width="95%" Enabled="false"> </asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label9" runat="server" Text="Tài khoản Có" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtTKCo" runat="server" CssClass="inputflat" Width="100%" BackColor="Aqua"
                                            onkeypress="if (event.keyCode==13){ShowLov('TKCo');}"> </asp:TextBox>
                                    </td>
                                    <td colspan="2" class="form_control">
                                        <asp:TextBox ID="txtTenTKCo" runat="server" CssClass="inputflat" Width="95%" Enabled="false"> </asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label10" runat="server" Text="Tài khoản Nợ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtTKNo" runat="server" CssClass="inputflat" Width="100%" BackColor="Aqua"
                                            onkeypress="if (event.keyCode==13){ShowLov('TKNo');}"> </asp:TextBox>
                                    </td>
                                    <td colspan="2" class="form_control">
                                        <asp:TextBox ID="txtTenTKNo" runat="server" CssClass="inputflat" Width="95%" Enabled="false"> </asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label11" runat="server" Text="Mã NNT" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtMaNNThue" runat="server" CssClass="inputflat" Width="100%" BackColor="Aqua"
                                            onkeypress="if (event.keyCode==13){ShowLov('NNT');}"> </asp:TextBox>
                                    </td>
                                    <td colspan="2" class="form_control">
                                        <asp:TextBox ID="txtTenNNThue" runat="server" CssClass="inputflat" Width="95%" Enabled="false"> </asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="center" style="height: 40px">
                            <asp:Button ID="cmdIn" runat="server" CssClass="ButtonCommand" Text="[In BC]" UseSubmitBehavior="false">
                            </asp:Button>&nbsp;
                            <asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand" Text="[Thoát]" UseSubmitBehavior="false">
                            </asp:Button>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
