﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false" CodeFile="frmBCThue_PGB.aspx.vb" Inherits="pages_XLCuoiNgay_frmBCThue_PGB" title="Báo cáo thuế - PGBank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script language="javascript" type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
    </script>
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" Runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">BÁO CÁO THUẾ - PGBANK</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                <tr align="left">
                                    <td width='20%' class="form_label">
                                        <asp:Label ID="Label10" runat="server" Text="Từ ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='35%' class="form_control">
                                        <asp:TextBox runat="server" ID="txtTuNgay" Width="120px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"></asp:TextBox>
                                            <asp:Image ID="btnCalendar1" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="Label3" runat="server" Text="Đến ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                         <asp:TextBox runat="server" ID="txtDenNgay" Width="120px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"></asp:TextBox>
                                            <asp:Image ID="btnCalendar2" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label4" runat="server" Text="Nhân viên thực hiện" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                       <asp:DropDownList ID="cboNhanVien" Width="150px" runat="server" CssClass="inputflat">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="form_label">
                                       <asp:Label ID="Label8" runat="server" Text="Nguyên Tệ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="cboNguyenTe" Width="150px" runat="server" CssClass="inputflat">
                                            <%--<asp:ListItem Text="Tất cả" Value="1" Selected="True"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="Label5" runat="server" Text="Trạng thái" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="DDTrangThai" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Đã nhận" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Chưa nhận" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                   <td class="form_label">
                                        <asp:Label ID="Label6" runat="server" Text="Trạng thái CTừ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="ddTrangThai_CT" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Đã duyệt" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="CT hủy- chờ duyệt" Value="02"></asp:ListItem>
                                            <asp:ListItem Text="Đã hủy" Value="04"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label"><asp:Label ID="Label7" runat="server" Text="Loại thuế" CssClass="label"></asp:Label></td>
                                
                                    <td class="form_control">
                                        <asp:DropDownList ID="ddLoaiThue" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="00" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Thuế nội địa" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="Thuế xuất nhập khẩu" Value="04"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="Label11" runat="server" Text="Kênh chứng từ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="cboKenhCtu" Width="150px" runat="server" CssClass="inputflat"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                 </tr>
                                 
                                <tr align="left" >
                                    <td class="form_label" >
                                        <asp:Label ID="lblLoaiKH" runat="server" Text="Loại KH" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control"  style="width:30%">
                                        <asp:DropDownList ID="ddlLoaiKH" Width="150px" runat="server" CssClass="inputflat"
                                            AutoPostBack="true">
                                          <%--  <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                            <asp:ListItem Text="C" Value="C"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td >
                                    <td class="form_label" >
                                    <asp:Label ID="lblNhomKH" runat="server" Text="Nhóm KH" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_label" >
                                     <asp:DropDownList ID="ddlNhomKH" Width="150px" runat="server" CssClass="inputflat">
                                            <%--<asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="CNTN-TD" Value="CNTN-TD"></asp:ListItem>
                                            <asp:ListItem Text="PETROLIMEX" Value="PETROLIMEX"></asp:ListItem>
                                            <asp:ListItem Text="PETRO_CN" Value="PETRO_CN"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left" >
                                    <td class="form_label" >
                                        <asp:Label ID="Label2" runat="server" Text="Chi nhánh(PGD)" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control"  style="width:30%">
                                        <asp:DropDownList ID="cboDiemThu" Width="150px" runat="server" CssClass="inputflat"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td >
                                    <td class="form_label" >
                                   <%-- <asp:Label ID="Label9" runat="server" Text="Mẫu bảng kê CT" CssClass="label"></asp:Label>--%>
                                    </td>
                                    <td class="form_label" >
                                    <%-- <asp:DropDownList ID="cboBangKeCT" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Bảng kê trước giờ cut-offtime" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Bảng kê sau giờ cut-offtime" Value="0"></asp:ListItem>
                                        </asp:DropDownList>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                <asp:Button ID="cmdIn" runat="server" CssClass="ButtonCommand" Text="[In BC]"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand" Text="[Thoát]">
                </asp:Button>&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>

