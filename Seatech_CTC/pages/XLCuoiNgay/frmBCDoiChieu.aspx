﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmBCDoiChieu.aspx.vb" Inherits="pages_XLCuoiNgay_frmBC_DoiChieu"
    Title="LẬP BẢNG KÊ ĐỐI CHIẾU THU NSNN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
                }
                textbox.value = str
        }
       
    </script>
 <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">LẬP BẢNG KÊ ĐỐI CHIẾU THU NSNN</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                <tr align="left">
                                    <td width='20%' class="form_label">
                                        <asp:Label ID="Label10" runat="server" Text="Từ ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='35%' class="form_control">
                                        <asp:TextBox runat="server" ID="txtTuNgay" Width="120px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="javascript:return mask(this.value,this,'2,5','/');" MaxLength="10"></asp:TextBox>
                                        <asp:Image ID="btnCalendar1" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                    </td>
                                    <td  class="form_label">
                                        <asp:Label ID="Label4" runat="server" Text="Đến ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td  class="form_control">
                                        <asp:TextBox runat="server" ID="txtDenNgay" Width="120px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="javascript:return mask(this.value,this,'2,5','/');" MaxLength="10"></asp:TextBox>
                                            <asp:Image ID="btnCalendar2" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                    </td> 
                                </tr>
                                 <tr align="left">
                                
                                    <td class="form_label">
                                        <asp:Label ID="Label5" runat="server" Text="Trạng thái chuyển" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="DDTrangThai" Width="150px" runat="server" CssClass="inputflat">
                                        <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Đã chuyển" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Chưa chuyển" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="Label3" runat="server" Text="Trạng thái CTừ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="DDTrangThaiCT" Width="150px" runat="server" CssClass="inputflat">
                                        <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Đã duyệt" Value="01"></asp:ListItem>
                                        <asp:ListItem Text="Hủy duyệt" Value="04"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                 <td class="form_label">
                                        <asp:Label ID="Label2" runat="server" Text="Chi nhánh(PGD)" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control" >
                                        <asp:DropDownList ID="cboDiemThu" Width="150px" runat="server" CssClass="inputflat"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                       <td class="form_label">
                                        <asp:Label ID="Label6" runat="server" Text="Loại thuế" CssClass="label"></asp:Label>
                                    </td>
                                     <td class="form_control">
                                        <asp:DropDownList ID="ddLoaiThue" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="00" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Thuế nội địa" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="Thuế xuất nhập khẩu" Value="04"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                 <td class="form_label">
                                        <asp:Label ID="Label7" runat="server" Text="Kênh chứng từ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control" >
                                        <asp:DropDownList ID="DDKenhCtu" Width="150px" runat="server" CssClass="inputflat"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                <asp:Button ID="cmdIn" runat="server" CssClass="ButtonCommand" Text="[In BC]"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand" Text="[Thoát]">
                </asp:Button>&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
