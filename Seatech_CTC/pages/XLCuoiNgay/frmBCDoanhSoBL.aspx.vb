﻿Imports System.Data
Imports VBOracleLib
Imports Business.BaoCao
Imports Business.XLCuoiNgay
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business

Partial Class pages_XLCuoiNgay_frmBCCTDoanhSoBL
    Inherits System.Web.UI.Page

    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    Private strMaCN_User As String = ""
    Private ds As DataSet
    Private frm As infBaoCao

#Region "Page event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)
            strMaCN_User = clsCTU.TCS_GetMaChiNhanh(Session("User").ToString)
        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                'BeanToForm()
                DM_DiemThu()
                clsCommon.get_NguyenTe(cboNguyenTe)
                ' txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                'clsCommon.addPopUpCalendarToImage(btnCalendar1, txtTuNgay, "dd/mm/yyyy")
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            TCS_SODU()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình lấy dữ liệu in báo cáo!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub

#End Region

    Private Sub BeanToForm()
        Try

            If Not Session.Item("MA_CN_USER_LOGON") Is Nothing Then
                Dim v_strMaCN = Session.Item("MA_CN_USER_LOGON").ToString()

                txtMaCN.Text = v_strMaCN.ToString()

                If Not v_strMaCN Is Nothing Then
                    txtTenCN.Text = clsCTU.TCS_GetTenCNNH(v_strMaCN.Substring(0, 6))
                Else
                    txtTenCN.Text = ""
                End If

                If IsHSC(v_strMaCN) Then
                    txtMaCN.Enabled = True
                    'btnSearch.Disabled = False
                Else
                    txtMaCN.Enabled = False
                    ' btnSearch.Disabled = True

                End If

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    Private Function checkHSC(ByVal strMa_CN As String) As String
        Dim returnValue = ""
        returnValue = buBaoCao.checkHSC_MaCN(strMa_CN)
        Return returnValue
    End Function
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a  "

        ElseIf strcheckHSC = "2" Then

            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        Else
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(txtMaCN, dsDiemThu, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try



    End Sub


    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Get_s the ten CNNH.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/28/2012 00:00:00
    '''  Mục đích:	    Ajax lấy tên chi nhánh
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="pv_strMaCN">The PV_STR ma CN.</param>
    ''' <returns>System.String.</returns>
    <System.Web.Services.WebMethod()> _
Public Shared Function Get_TenCNNH(ByVal pv_strMaCN As String) As String

        If Not pv_strMaCN Is Nothing Then
            Return clsCTU.TCS_GetTenCNNH(pv_strMaCN.Substring(0, 6))
        Else
            Return ""
        End If

    End Function

    Private Function IsHSC(ByVal v_strMaCN As String) As Boolean
        Try
            Dim v_bHO As Boolean = False
            Dim v_strHSC As String = clsCTU.TCS_GETTS("MA_CN_HO")

            If v_strHSC.Length > 0 Then
                Dim v_arrHSC As Array = v_strHSC.Split(",")

                If v_arrHSC.Length > 0 Then
                    For index As Integer = 0 To v_arrHSC.Length - 1

                        If v_strMaCN = v_arrHSC(index) Then
                            v_bHO = True
                            Exit For
                        End If

                    Next

                End If

            End If
            Return v_bHO
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#Region "Call Report"
    Private Sub TCS_SODU()
        Try
            Dim strMaNT As String = cboNguyenTe.SelectedValue.ToString()
            Dim strMaCN = txtMaCN.SelectedValue.ToString()
            Dim strCheckHS As String = ""
            strCheckHS = checkHSC(strMaCN_User)
            If strMaCN = "" And strCheckHS <> "1" Then
                strMaCN = strMaCN_User & ";" & strCheckHS
            End If
            If ddlLoaiBC.SelectedValue = "01" Then
                ds = New DataSet
                frm = New infBaoCao
                ds = New DALTcs_dchieu_nhhq_hdr().getBCTHDoanhSoBaoLanhXNKhau(txtDenNgay.Value.Trim(), strMaCN, strMaNT)

                frm.TCS_DS = ds
                ' frm.TCS_Tungay = txtTuNgay.Text.ToString()
                frm.TCS_Denngay = txtDenNgay.Value.ToString()
                Session("objBaoCao") = frm 'Gọi form show báo cáo
                clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & Report_Type.TCS_BC_TH_BL_DOANHSO, "ManPower")
            Else
                ds = New DataSet
                frm = New infBaoCao
                ds = New DALTcs_dchieu_nhhq_hdr().getBCCTietDoanhSoBaoLanhXNKhau(txtDenNgay.Value.Trim(), strMaCN, strMaNT)
                frm.TCS_DS = ds
                ' frm.TCS_Tungay = txtTuNgay.Text.ToString()
                frm.TCS_Denngay = txtDenNgay.Value.ToString()
                Session("objBaoCao") = frm 'Gọi form show báo cáo
                clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & Report_Type.TCS_BC_BL_DOANHSO, "ManPower")
            End If

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình in báo cáo!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub



#End Region

End Class
