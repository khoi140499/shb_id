﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Services;
using Newtonsoft.Json;
using System.IO;
using Business;
using Business_HQ;
using CustomsV3.MSG.MSG303LePhi;
using Business_HQ.HaiQuan;
using Business.BaoCao;


public partial class pages_XLCuoiNgay_frmBaoCaoPhiBoNganh : System.Web.UI.Page
{
    public static string sohoso = string.Empty;
    public static string tungay = string.Empty;
    public static string denngay = string.Empty;
    public static string mst = string.Empty;
    public static string nam_ct = string.Empty;
    public static string trangthai_ct = string.Empty;
    public static string ma_cn = string.Empty;
    public static int tongtienvnd = 0;
    public static int tongtienusd = 0;
    public static int tonggiaodich = 0;
    public static DataSet dsPhiBoNganhReport = new DataSet();
    public static TCS_IN_BC_BO_BAN_NGANH tcs_in_bc_bo_ban_nganh;

    protected void Page_Load(object sender, EventArgs e)
    {

        sohoso = HttpContext.Current.Request.QueryString["SOHOSO"];
        tungay = HttpContext.Current.Request.QueryString["TUNGAY"];
        denngay = HttpContext.Current.Request.QueryString["DENNGAY"];
        mst = HttpContext.Current.Request.QueryString["MST"];
        nam_ct = HttpContext.Current.Request.QueryString["NAM_CT"];
        trangthai_ct = HttpContext.Current.Request.QueryString["TRANGTHAI_CT"];
        ma_cn = HttpContext.Current.Request.QueryString["MA_CN"];
    }




    [WebMethod]
    public static string getPhiBoBanNganh()
    {
        Business.CTuUser mv_objUser = new Business.CTuUser(HttpContext.Current.Session["User"].ToString());
        //MSG303LePhi msg303LePhi = new MSG303LePhi();
        TCS_IN_BC_BO_BAN_NGANH tcs_in_bc_bo_ban_nganh = new TCS_IN_BC_BO_BAN_NGANH();
        tcs_in_bc_bo_ban_nganh.SOHOSO = sohoso;
        tcs_in_bc_bo_ban_nganh.TUNGAY = tungay;
        tcs_in_bc_bo_ban_nganh.DENNGAY = denngay;
        tcs_in_bc_bo_ban_nganh.MST = mst;
        tcs_in_bc_bo_ban_nganh.NAM_CT = nam_ct;
        tcs_in_bc_bo_ban_nganh.MA_NV = mv_objUser.Ma_NV;
        tcs_in_bc_bo_ban_nganh.MA_CN = ma_cn;
        tcs_in_bc_bo_ban_nganh.RootID = checkHSC();

        string tt_ct = "0";
        if (trangthai_ct == "06")
        {
            trangthai_ct = "01";
            tcs_in_bc_bo_ban_nganh.TRANGTHAI_CT = "01";
            tt_ct = "0";
        }
        else if (trangthai_ct == "01")
        {
            tt_ct = "1";
            tcs_in_bc_bo_ban_nganh.TRANGTHAI_CT = "01";
        }
        else
        {
            tt_ct = "-1";
            tcs_in_bc_bo_ban_nganh.TRANGTHAI_CT = trangthai_ct;
        }
        tcs_in_bc_bo_ban_nganh.TT_CT = tt_ct;

        var dsPhiBoNganh = daLePhiHaiQuan.MSG303BaoCaoPhiBoBanNganh(tcs_in_bc_bo_ban_nganh);

        dsPhiBoNganhReport = dsPhiBoNganh;
        if (dsPhiBoNganh == null || dsPhiBoNganh.Tables == null)
        {
            return "Không tìm thấy bản ghi nào";
        }
        else
        {
            System.Collections.Generic.List<VMReportPhi> ListVMReportPhi = new System.Collections.Generic.List<VMReportPhi>();
            foreach (DataRow dataRow in dsPhiBoNganh.Tables[0].Rows)
            {
                VMReportPhi vmReportPhi = new VMReportPhi();
                vmReportPhi.SO_HS = dataRow["SO_HS"].ToString();
                vmReportPhi.TAIKHOAN_NT_MA_NH_TH = dataRow["TAIKHOAN_NT_MA_NH_TH"].ToString();
                vmReportPhi.TAIKHOAN_NT_TEN_TK_TH = dataRow["TAIKHOAN_NT_TEN_TK_TH"].ToString();
                vmReportPhi.TRANGTHAI_CT = dataRow["TRANGTHAI_CT"].ToString();
                vmReportPhi.TRANGTHAI_XULY = dataRow["TRANGTHAI_XULY"].ToString();
                vmReportPhi.TT_NT_MA_NT = dataRow["TT_NT_MA_NT"].ToString();
                vmReportPhi.TT_NT_TONGTIEN_NT = dataRow["TT_NT_TONGTIEN_NT"].ToString();
                vmReportPhi.TT_NT_TONGTIEN_VND = dataRow["TT_NT_TONGTIEN_VND"].ToString();
                vmReportPhi.TT_NT_TYGIA = dataRow["TT_NT_TYGIA"].ToString();
                vmReportPhi.CHUNGTU_CT_NDKT = dataRow["CHUNGTU_CT_NDKT"].ToString();
                vmReportPhi.CHUNGTU_CT_TEN_NDKT = dataRow["CHUNGTU_CT_TEN_NDKT"].ToString();
                vmReportPhi.KYHIEU_CT = dataRow["KYHIEU_CT"].ToString();
                vmReportPhi.MA_DVQL = dataRow["MA_DVQL"].ToString();
                vmReportPhi.NAM_CT_PT = dataRow["NAM_CT_PT"].ToString();
                vmReportPhi.NGUOIDUYET = dataRow["NGUOIDUYET"].ToString();
                vmReportPhi.NGUOILAP = dataRow["NGUOILAP"].ToString();
                vmReportPhi.NNT_MST = dataRow["NNT_MST"].ToString();
                vmReportPhi.SO_CT = dataRow["SO_CT"].ToString();

                System.Collections.Generic.List<VM_ChiTiet_CT> ListVM_ChiTiet_CT = new System.Collections.Generic.List<VM_ChiTiet_CT>();
                var dsChiTietCT = HaiQuan.ListChiTietBySoCT(dataRow["SO_CT"].ToString());
                if (dsChiTietCT != null)
                {
                    foreach (DataRow dataRowChild in dsChiTietCT.Tables[0].Rows)
                    {
                        VM_ChiTiet_CT vM_ChiTiet_CT = new VM_ChiTiet_CT();

                        vM_ChiTiet_CT.STT = dataRowChild["STT"].ToString();

                        vM_ChiTiet_CT.SoTien_NT = dataRowChild["SoTien_NT"].ToString();
                        vM_ChiTiet_CT.SoTien_VND = dataRowChild["SoTien_VND"].ToString();
                        vM_ChiTiet_CT.NDKT = dataRowChild["NDKT"].ToString();
                        vM_ChiTiet_CT.Ten_NDKT = dataRowChild["Ten_NDKT"].ToString();
                        vM_ChiTiet_CT.GhiChu = dataRowChild["GhiChu"].ToString();
                        ListVM_ChiTiet_CT.Add(vM_ChiTiet_CT);

                    }
                }
                vmReportPhi.LISTCHITIET = ListVM_ChiTiet_CT;
                ListVMReportPhi.Add(vmReportPhi);

            }
            string strPhiBoNganh = JsonConvert.SerializeObject(ListVMReportPhi, Formatting.Indented);
            return strPhiBoNganh;

        }


    }

    public static string checkHSC()
    {
        string returnValue = "";
        Business.CTuUser mv_objUser = new Business.CTuUser(HttpContext.Current.Session["User"].ToString());
        if (mv_objUser != null)
        {
            var ds = buBaoCao.checkHSC(mv_objUser.Ma_NV.ToString());
            if (ds != null)
            {
                returnValue = ds.Tables[0].Rows[0]["phan_cap"].ToString();
            }
        }
        return returnValue;
    }

    protected void btnIn_Click(object sender, EventArgs e)
    {

        int startWith = 1;
        int rowspan = 1;
        string tBody = "<tbody>";
        System.Collections.Generic.List<VMReportPhi> ListVMReportPhi = new System.Collections.Generic.List<VMReportPhi>();
        VMReportPhi vmReportPhi = new VMReportPhi();
        if (dsPhiBoNganhReport != null && dsPhiBoNganhReport.Tables[0] != null)
        {
            DataTable data = dsPhiBoNganhReport.Tables[0];
            foreach (DataRow row in data.Rows)
            {
                string sRowExport = string.Empty;
                if (startWith % 2 == 0)
                {
                    sRowExport = "<tr style=\"background: #b8d1f3;\">";
                }
                else
                {
                    sRowExport = "<tr style=\"background: #dae5f4;\">";

                }

                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + startWith + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["SO_HS"] == null ? "" : row["SO_HS"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["NNT_MST"] == null ? "" : row["NNT_MST"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["SO_CT"] == null ? "" : row["SO_CT"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["NAM_CT_PT"] == null ? "" : row["NAM_CT_PT"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["MA_DVQL"] == null ? "" : row["MA_DVQL"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["KYHIEU_CT"] == null ? "" : row["KYHIEU_CT"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["TT_NT_MA_NT"] == null ? "" : row["TT_NT_MA_NT"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["TT_NT_TYGIA"] == null ? "" : row["TT_NT_TYGIA"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["TT_NT_TONGTIEN_VND"] == null ? "" : row["TT_NT_TONGTIEN_VND"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["TT_NT_TONGTIEN_NT"] == null ? "" : row["TT_NT_TONGTIEN_NT"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["NGUOILAP"] == null ? "" : row["NGUOILAP"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["NGUOIDUYET"] == null ? "" : row["NGUOIDUYET"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["TAIKHOAN_NT_MA_NH_TH"] == null ? "" : row["TAIKHOAN_NT_MA_NH_TH"]) + "</td>";
                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["TAIKHOAN_NT_TEN_TK_TH"] == null ? "" : row["TAIKHOAN_NT_TEN_TK_TH"]) + "</td>";

                Business.CTuUser mv_objUser = new Business.CTuUser(HttpContext.Current.Session["User"].ToString());
                TCS_IN_BC_BO_BAN_NGANH tcs_in_bc_bo_ban_nganh = new TCS_IN_BC_BO_BAN_NGANH();
                tcs_in_bc_bo_ban_nganh.SOHOSO = sohoso;
                tcs_in_bc_bo_ban_nganh.TUNGAY = tungay;
                tcs_in_bc_bo_ban_nganh.DENNGAY = denngay;
                tcs_in_bc_bo_ban_nganh.MST = mst;
                tcs_in_bc_bo_ban_nganh.NAM_CT = nam_ct;
                tcs_in_bc_bo_ban_nganh.MA_NV = mv_objUser.Ma_NV;
                tcs_in_bc_bo_ban_nganh.MA_CN = ma_cn;
                var dsPhiBoNganh = daLePhiHaiQuan.MSG303BaoCaoPhiBoBanNganh(tcs_in_bc_bo_ban_nganh);
                System.Collections.Generic.List<VM_ChiTiet_CT> ListVM_ChiTiet_CT = new System.Collections.Generic.List<VM_ChiTiet_CT>();

                
                var dsChiTietCT = HaiQuan.ListChiTietBySoCT(row["SO_CT"].ToString());
                if (dsChiTietCT != null)
                {
                    foreach (DataRow dataRowChild in dsChiTietCT.Tables[0].Rows)
                    {
                        VM_ChiTiet_CT vM_ChiTiet_CT = new VM_ChiTiet_CT();
                        vM_ChiTiet_CT.STT = dataRowChild["STT"].ToString();
                        vM_ChiTiet_CT.SoTien_NT = dataRowChild["SoTien_NT"].ToString();
                        vM_ChiTiet_CT.SoTien_VND = dataRowChild["SoTien_VND"].ToString();
                        vM_ChiTiet_CT.NDKT = dataRowChild["NDKT"].ToString();
                        vM_ChiTiet_CT.Ten_NDKT = dataRowChild["Ten_NDKT"].ToString();
                        vM_ChiTiet_CT.GhiChu = dataRowChild["GhiChu"].ToString();
                        ListVM_ChiTiet_CT.Add(vM_ChiTiet_CT);
                    }
                }

                string sNDKT = "";
                int iCount = 0;
                foreach (var item in ListVM_ChiTiet_CT)
                {
                    iCount++;
                    sNDKT += string.Format("<span>{0}" + ". " + item.Ten_NDKT + "</span><br/>", iCount);
                }

                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='left' rowspan=" + rowspan + " >" + sNDKT + "</td>";

                //sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["CHUNGTU_CT_NDKT"] == null ? "" : row["CHUNGTU_CT_NDKT"]) + "</td>";
                //sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + (row["CHUNGTU_CT_TEN_NDKT"] == null ? "" : row["CHUNGTU_CT_TEN_NDKT"]) + "</td>";
                //sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + vM_ChiTiet_CT.Ten_NDKT + "</td>";
                //check trang thai
                string trangthai_result = string.Empty;
                string trangthai_ct = row["TRANGTHAI_CT"].ToString();
                string tt_ct = row["TRANGTHAI_XULY"].ToString();

                if (trangthai_ct == "01" && tt_ct == "1")
                {
                    trangthai_result = "Đã kiểm soát ";
                }
                else if (trangthai_ct == "01" && tt_ct == "0")
                {
                    trangthai_result = "Chuyển thuế lỗi";
                }
                else if (trangthai_ct == "03")
                {
                    trangthai_result = "Chứng từ chuyển trả";
                }

                else if (trangthai_ct == "04")
                {
                    trangthai_result = "Đã hủy bởi GDV ";
                }

                else if (trangthai_ct == "02")
                {
                    trangthai_result = "Hủy bởi GDV chờ duyệt";
                }

                else if (trangthai_ct == "05")
                {
                    trangthai_result = "Chờ kiểm soát";
                }

                else if (trangthai_ct == "08")
                {
                    trangthai_result = "Hủy chuyển thuế lỗi";
                }

                else if (trangthai_ct == "07")
                {
                    trangthai_result = "Đã hủy bởi KSV  ";
                }

                else
                {
                    trangthai_result = "Không xác định";
                }


                sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + trangthai_result + "</td>";




                sRowExport += "</tr>";
                tBody += sRowExport;
                tongtienusd += (row["TT_NT_TONGTIEN_NT"] == null ? 0 : int.Parse(row["TT_NT_TONGTIEN_NT"].ToString()));
                tongtienvnd += (row["TT_NT_TONGTIEN_VND"] == null ? 0 : int.Parse(row["TT_NT_TONGTIEN_VND"].ToString()));
                startWith = startWith + 1;
            }
            tonggiaodich = data.Rows.Count;
            tBody += "</tbody>";

            divDraw.InnerHtml = tBody;
            divTongtienusd.Visible = true;
            divTongtienvnd.Visible = true;
            divTonggiaodich.Visible = true;
            divTungay.Visible = false;
            divDenngay.Visible = false;
            //divTungay.Visible = true;
            //divDenngay.Visible = true;
            //}
        }

        //var dsPhiBoNganh = daLePhiHaiQuan.MSG303BaoCaoPhiBoBanNganh(tcs_in_bc_bo_ban_nganh);
        //System.Collections.Generic.List<VM_ChiTiet_CT> ListVM_ChiTiet_CT = new System.Collections.Generic.List<VM_ChiTiet_CT>();

        //VM_ChiTiet_CT vM_ChiTiet_CT = new VM_ChiTiet_CT();
        //foreach (DataRow dataRow in dsPhiBoNganh.Tables[0].Rows)
        //{
        //    var dsChiTietCT = HaiQuan.ListChiTietBySoCT(dataRow["SO_CT"].ToString());
        //    if (dsChiTietCT != null)
        //    {
        //        foreach (DataRow dataRowChild in dsChiTietCT.Tables[0].Rows)
        //        {
        //            vM_ChiTiet_CT.STT = dataRowChild["STT"].ToString();
        //            vM_ChiTiet_CT.SoTien_NT = dataRowChild["SoTien_NT"].ToString();
        //            vM_ChiTiet_CT.SoTien_VND = dataRowChild["SoTien_VND"].ToString();
        //            vM_ChiTiet_CT.NDKT = dataRowChild["NDKT"].ToString();
        //            vM_ChiTiet_CT.Ten_NDKT = dataRowChild["Ten_NDKT"].ToString();
        //            vM_ChiTiet_CT.GhiChu = dataRowChild["GhiChu"].ToString();
        //            ListVM_ChiTiet_CT.Add(vM_ChiTiet_CT);
        //        }
        //    }
        //    //vmReportPhi.LISTCHITIET = ListVM_ChiTiet_CT;
        //    //ListVMReportPhi.Add(vmReportPhi);
        //    sRowExport += "<td style='padding:7px; border:#4e95f4 1px solid;' align='center' rowspan=" + rowspan + " >" + vM_ChiTiet_CT.Ten_NDKT + "</td>";
        //}       
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=BaoCao.xls");
        HttpContext.Current.Response.Charset = "utf-8";
        StringWriter strWriter = new StringWriter();
        HtmlTextWriter htmlTextWriter = new HtmlTextWriter(strWriter);
        //div1.InnerHtml = div1.InnerHtml + divDraw.InnerHtml;

        Table1.RenderControl(htmlTextWriter);
        Response.Output.Write(strWriter);
        Response.Flush();
        Response.End();

    }
}
