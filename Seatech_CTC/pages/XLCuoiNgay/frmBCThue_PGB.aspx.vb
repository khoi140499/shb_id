﻿Imports System.Data
Imports VBOracleLib
Imports Business.BaoCao
Imports Business.XLCuoiNgay
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Partial Class pages_XLCuoiNgay_frmBCThue_PGB
    Inherits System.Web.UI.Page
    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    Private Shared strMaNhom_GDV As String = ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                DM_DiemThu() 'Load dm điểm thu được phép phân quyền
                ' DM_BangKe() 'Load dm bang ke
                DM_KenhCtu()
                DM_NV() 'Load danh sach nhan vine
                get_NguyenTe()
                DM_Nhom_KH()
                DM_LoaiKH()
                txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User")))
                txtDenNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                clsCommon.addPopUpCalendarToImage(btnCalendar1, txtTuNgay, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnCalendar2, txtDenNgay, "dd/mm/yyyy")
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        Dim str_Ma_Nhom As String = ""
        Dim dt As New DataTable
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        str_Ma_Nhom = LOAD_MN()
        If str_Ma_Nhom = strMaNhom_GDV Then
            strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a where a.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        Else
            If strcheckHSC = "1" Then
                'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
                ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
                strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a  "

            ElseIf strcheckHSC = "2" Then

                strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
            Else
                strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
            End If
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If str_Ma_Nhom = strMaNhom_GDV Then
                dt = dsDiemThu.Tables(0)
                cboDiemThu.DataSource = dt
                cboDiemThu.DataTextField = "name"
                cboDiemThu.DataValueField = "id"
                cboDiemThu.DataBind()
            Else
                clsCommon.ComboBoxWithValue(cboDiemThu, dsDiemThu, 1, 0, "Tất cả")
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try



    End Sub
    Private Sub DM_KenhCtu()

        Dim dsKCTu As DataSet
        Dim dk As String = ""
        'dk = "And ma_cn like'%" & cboDiemThu.SelectedValue.ToString & "%'"

        Dim strSQL As String = "SELECT a.ma_kenh,a.ten_kenh FROM tcs_kenh_ct a " ' & dk
        Try
            dsKCTu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(cboKenhCtu, dsKCTu, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub
    Private Sub DM_LoaiKH()

        Dim dsKCTu As DataSet
        Dim dk As String = ""
        'dk = "And ma_cn like'%" & cboDiemThu.SelectedValue.ToString & "%'"

        Dim strSQL As String = "SELECT a.ma_loai, a.ten_loai FROM tcs_loai_kh a where a.tinh_trang='1'" ' & dk
        Try
            dsKCTu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(ddlLoaiKH, dsKCTu, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub
    Private Sub DM_Nhom_KH()

        Dim dsKCTu As DataSet
        Dim dk As String = ""
        'dk = "And ma_cn like'%" & cboDiemThu.SelectedValue.ToString & "%'"

        Dim strSQL As String = "SELECT a.ma_nhom, a.ten_nhom  FROM tcs_dm_nhom_kh a where tinh_trang='1' " ' & dk
        Try
            dsKCTu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(ddlNhomKH, dsKCTu, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub
    Private Sub get_NguyenTe()
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable
        Try
            Dim strSQL As String = "SELECT * from Tcs_dm_nguyente "
            dt = DataAccess.ExecuteToTable(strSQL)

            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then

                For i As Integer = 0 To dt.Rows.Count
                    Dim item As ListItem
                    item = New ListItem(dt.Rows(i)("TEN").ToString(), dt.Rows(i)("Ma_NT").ToString())
                    cboNguyenTe.Items.Add(item)
                Next

            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Function LOAD_MN() As String
        Dim str_return As String = ""
        Dim strSQL_MaNhom As String = "select ma_nhom from tcs_dm_nhanvien where ma_nv='" & Session.Item("User").ToString & "'"
        Dim dt As New DataTable
        dt = DataAccess.ExecuteToTable(strSQL_MaNhom)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            str_return = dt.Rows(0)("ma_nhom").ToString
        End If
        Return str_return
    End Function
    Private Sub DM_NV()

        Dim dsNV As DataSet
        Dim dk As String = ""
        Dim str_Ma_Nhom As String = ""
        Dim strSQL As String = ""
        Dim dt As New DataTable
        Dim strcheckHS As String = checkHSC()
        If cboDiemThu.Text.Equals("") Then
            If strcheckHS <> "1" Then
                If strcheckHS = "2" Then
                    dk = "and b.branch_id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "' "
                Else
                    dk = "and b.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "' "
                End If
            End If
        Else
            dk = "And ma_cn ='" & cboDiemThu.SelectedValue.ToString & "'"
        End If
        str_Ma_Nhom = LOAD_MN()
        If str_Ma_Nhom = strMaNhom_GDV Then
            strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nv='" & Session.Item("User").ToString & "' and a.ma_nhom='" & strMaNhom_GDV & "' " & dk
        Else
            strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nhom='" & strMaNhom_GDV & "' " & dk
        End If

        Try
            dsNV = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If str_Ma_Nhom = strMaNhom_GDV Then
                dt = dsNV.Tables(0)
                cboNhanVien.DataSource = dt
                cboNhanVien.DataTextField = "ten_dn"
                cboNhanVien.DataValueField = "ma_nv"
                cboNhanVien.DataBind()
            Else
                clsCommon.ComboBoxWithValue(cboNhanVien, dsNV, 1, 0, "Tất cả")
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub
    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            TCS_BC_THUE_PGB()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub

    Protected Sub cboDiemThu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDiemThu.SelectedIndexChanged
        DM_NV()
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub
   
#Region "Where Clause"
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    Private Function WhereNgay() As String
        Dim strWhereNgay As String
        Dim strNgayStart As String
        strNgayStart = ConvertDateToNumber(txtTuNgay.Text.ToString())
        Dim strNgayFinish As String = ConvertDateToNumber(txtDenNgay.Text.ToString())

        strWhereNgay = " and ( (TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') >=" & strNgayStart & " and TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') <=" & strNgayFinish & ") or (hdr.ngay_kb >= " & strNgayStart & " and hdr.ngay_kb<=" & strNgayFinish & " and tt_cthue = '1'))"
        'strWhereNgay += "and (TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') <=" & strNgayFinish & ")"
        'If cboBangKeCT.SelectedValue = "" Then
        '    strWhereNgay = strWhereNgay '& " and (TO_CHAR(hdr.ngay_ht,'yyyymmdd') =" & strNgayStart & ") "
        'ElseIf cboBangKeCT.SelectedValue = "0" Then 'Ngoai gio
        '    strWhereNgay = strWhereNgay & " and hdr.ngay_KB >" & strNgayStart & " "
        'Else
        '    strWhereNgay = strWhereNgay & " and hdr.ngay_KB =" & strNgayStart & " "
        'End If

        mv_strNgay = "Từ ngày " & txtTuNgay.Text.ToString() & " đến ngày " & txtDenNgay.Text.ToString()
        Return strWhereNgay
    End Function

    Private Function WhereMaDthu() As String
        Dim strWhereMaNV As String = ""
        Dim strcheckHSC As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If cboDiemThu.SelectedValue <> "" Then
            strWhereMaNV = " And (cn.id= '" & cboDiemThu.SelectedValue & "') "
        Else
            If strcheckHSC <> "1" Then
                If strcheckHSC = "2" Then
                    strWhereMaNV = " And (cn.branch_id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "') "
                Else
                    strWhereMaNV = " And (cn.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "') "
                End If

            End If
        End If

        Return strWhereMaNV
    End Function

    Private Function WhereNV() As String
        Dim strWhereMaNV As String = ""
        If Not cboNhanVien.SelectedValue().Trim().Equals("") Then
            strWhereMaNV = " And (hdr.Ma_NV = " & cboNhanVien.SelectedValue().Trim() & ") "
        End If
        Return strWhereMaNV
    End Function
    Private Function WhereTrang_Thai() As String
        Dim strWhereMaNV As String = ""
        If Not DDTrangThai.SelectedValue().Trim().Equals("") Then
            If ddTrangThai_CT.SelectedValue = "04" Then
                If DDTrangThai.SelectedValue().Trim() = "1" Then
                    strWhereMaNV = " And (hdr.tt_cthue =0) "
                Else
                    strWhereMaNV = " And (hdr.tt_cthue =1) "
                End If
            Else
                If ddTrangThai_CT.SelectedValue = "01" Then
                    strWhereMaNV = " And (hdr.tt_cthue =" & DDTrangThai.SelectedValue().Trim() & ") "
                End If

            End If
        End If
        Return strWhereMaNV
    End Function
    Private Function WhereLoai_thue() As String
        Dim strWhereLoaiThue As String = ""
        If Not ddLoaiThue.SelectedValue().Trim().Equals("") Then
            If Not ddLoaiThue.SelectedValue = "00" Then
                If ddLoaiThue.SelectedValue = "04" Then
                    strWhereLoaiThue = " And (hdr.ma_lthue = '04') "
                Else
                    strWhereLoaiThue = " And (hdr.ma_lthue <> '04') "
                End If
            End If
        End If
        Return strWhereLoaiThue
    End Function
    Private Function WhereKenh_Ctu() As String
        Dim strWhereKenhCtu As String = ""
        If Not cboKenhCtu.SelectedValue().Trim().Equals("") Then
            strWhereKenhCtu = " And (hdr.Kenh_ct = " & cboKenhCtu.SelectedValue().Trim() & ") "
        End If
        Return strWhereKenhCtu
    End Function
    Private Function WhereLoaiKH() As String
        Dim strWhereLoaiKH As String = ""
        If Not ddlLoaiKH.SelectedValue().Trim().Equals("") Then
            strWhereLoaiKH = " And (p.tran_type = '" & ddlLoaiKH.SelectedValue().Trim() & "') "
        End If
        Return strWhereLoaiKH
    End Function
    Private Function WhereNhomKH() As String
        Dim strWhereNhomKH As String = ""
        If Not ddlNhomKH.SelectedValue().Trim().Equals("") Then
            strWhereNhomKH = " And (p.cert_code = '" & ddlNhomKH.SelectedValue().Trim() & "') "
        End If
        Return strWhereNhomKH
    End Function
    Private Function WhereTrang_ThaiCT() As String
        Dim strWhereMaNV As String = ""
        If ddTrangThai_CT.SelectedValue().Trim().Equals("") Then
            If DDTrangThai.SelectedValue().Trim().Equals("") Then
                strWhereMaNV = " and (hdr.trang_thai = '01' or (hdr.trang_thai = '04' and hdr.ma_ks<>'0') or hdr.trang_thai = '02' ) "
            End If
            If DDTrangThai.SelectedValue().Trim().Equals("0") Then
                strWhereMaNV = " and ((hdr.trang_thai = '01' and hdr.tt_cthue =0) or (trang_thai = '04' and hdr.tt_cthue =1 and hdr.ma_ks<>'0')or ( hdr.trang_thai = '02')  ) "
            End If
            If DDTrangThai.SelectedValue().Trim().Equals("1") Then
                strWhereMaNV = " and ((trang_thai = '01' and hdr.tt_cthue =1) or (hdr.trang_thai = '04' and hdr.tt_cthue =0 and hdr.ma_ks<>'0')  or (hdr.trang_thai = '02') ) "
            End If
        End If
        If ddTrangThai_CT.SelectedValue().Trim().Equals("01") Then
            strWhereMaNV = " and hdr.trang_thai = '01' "
        End If
        If ddTrangThai_CT.SelectedValue().Trim().Equals("02") Then
            strWhereMaNV = " and hdr.trang_thai = '02' "
        End If
        If ddTrangThai_CT.SelectedValue().Trim().Equals("04") Then
            strWhereMaNV = " and (hdr.trang_thai = '04' and hdr.ma_ks<>'0') "
        End If
        Return strWhereMaNV
    End Function

    'Private Function WhereBC() As String
    '    Dim Where As String = ""
    '    If cboBangKeCT.SelectedValue = "1" Then
    '        Where = " AND (hdr.tkht = '120101001') "
    '    Else
    '        If cboBangKeCT.SelectedValue = "0" Then
    '            Where = " AND (hdr.tkht = '280701018')"
    '        End If
    '    End If
    '    Return Where
    'End Function
    Private Function WhereNguyenTe() As String
        Dim Where As String = ""
        Where = "AND hdr.ma_nt = '" & cboNguyenTe.SelectedValue.ToString() & "'"
        Return Where
    End Function

    Private Function WhereAll() As String
        Dim strWhereAll As String
        strWhereAll = WhereNgay() & WhereMaDthu() & WhereNV() & WhereTrang_Thai() & WhereTrang_ThaiCT() & WhereLoai_thue() & WhereKenh_Ctu() & WhereNguyenTe() & WhereLoaiKH() & WhereNhomKH()
        Return strWhereAll
    End Function
    Private Function WhereThuHo() As String
        Dim strWhereAll As String
        strWhereAll = WhereNgay() & WhereNV()
        Return strWhereAll
    End Function
#End Region

#Region "Call Report"
    Private Sub TCS_BC_THUE_PGB()
        Try
            mv_strLoaiBaoCao = Report_Type.CTU_BCTHUE_PGB
            Dim strWhere As String = WhereAll()
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            Dim ds As DataSet = buBaoCao.TCS_BC_THUE_PGB(strWhere)
            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If
            'ds.WriteXml("d:\\baocaotong.xml", XmlWriteMode.WriteSchema)
            'ds.Tables(0).TableName = "BKE_GNT"
            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            If cboDiemThu.SelectedValue <> "" Then
                frm.TCS_MaDiemThu = cboDiemThu.SelectedItem.Text
            Else
                frm.TCS_MaDiemThu = "Tất cả" 'mv_strMaDiemThu.ToUpper()
            End If
            Dim strMauBC As String = ""

            'If cboBangKeCT.SelectedValue = "" Then
            '    strMauBC = "Totall"
            'ElseIf cboBangKeCT.SelectedValue = "1" Then
            '    strMauBC = "Trước giờ cut-offtime"
            'Else
            '    strMauBC = "Sau giờ cut-offtime"
            'End If
            If ddLoaiThue.SelectedValue = "01" Then
                strMauBC = strMauBC & " - Thuế nội địa"
            End If
            If ddLoaiThue.SelectedValue = "04" Then
                strMauBC = strMauBC & " - Thuế xuất nhập khẩu"
            End If
            frm.Mau_BaoCao = strMauBC
            frm.TCS_Ngay_CT = mv_strNgay
            frm.Ngay = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 7, 2)
            frm.Thang = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 5, 2)
            frm.Nam = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 1, 4)
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            'clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx", "ManPower")
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao, "ManPower")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



#End Region
End Class
