﻿Imports System.Data
Imports VBOracleLib
Imports Business.BaoCao
Imports Business.XLCuoiNgay
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables

Partial Class pages_XLCuoiNgay_frmBC_Tong
    Inherits System.Web.UI.Page

    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String

#Region "Page event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                'get_NguyenTe()
                clsCommon.get_NguyenTe(cboNguyenTe)
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            TCS_BC_TONGCN()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub

#End Region

#Region "Call Report"
    Private Sub TCS_BC_TONGCN()
        Try
            Dim ds As DataSet
            'If cboBangKeCT.SelectedValue = "1" Then
            If cboLoaiBaoCao.SelectedValue.Substring(0, 1) = "1" Then
                mv_strLoaiBaoCao = Report_Type.TCS_BC_TONG
            Else
                mv_strLoaiBaoCao = Report_Type.TCS_BC_TONG_CT
            End If
            '02/10/2012-manhnv
            Dim WhereLoaiThue As String = ""
            If cboLoaiBaoCao.SelectedValue.Substring(1, 1) = "1" Then
                WhereLoaiThue = " AND hdr.ma_lthue <> '04' "
            ElseIf cboLoaiBaoCao.SelectedValue.Substring(1, 1) = "2" Then
                WhereLoaiThue = " AND hdr.ma_lthue = '04' "
            End If
            Dim WhereNguyenTe As String = ""

            WhereNguyenTe = "AND hdr.ma_nt = '" & cboNguyenTe.SelectedValue.ToString() & "'"
            'End If

            '-----
            Dim strWhere As String = WhereNgay() & WhereLoaiThue & WhereNguyenTe
            If Not Session.Item("WhereAll") Is Nothing Then
                Session.Item("WhereAll") = ""
            End If
            Session.Item("WhereAll") = strWhere
            ds = buBaoCao.TCS_BC_TONG_CN(strWhere)
            If IsEmptyDataSet(ds) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo")
                Return
            End If
            Dim frm As New infBaoCao
            frm.TCS_DS = ds
            frm.TCS_Ngay_CT = mv_strNgay
            frm.Mau_BaoCao = cboLoaiBaoCao.SelectedItem.Text
            frm.Ngay = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 7, 2)
            frm.Thang = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 5, 2)
            frm.Nam = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 1, 4)
            frm.TCS_Tungay = txtTuNgay.Value.ToString()
            frm.TCS_Denngay = txtDenNgay.Value.ToString()
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao, "ManPower")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



#End Region

#Region "Where Clause"
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Private Function WhereNgay() As String
        Dim strWhereNgay As String
        Dim strNgayStart As String
        Dim strNgayEnd As String
        strNgayStart = ConvertDateToNumber(txtTuNgay.Value.ToString())
        strNgayEnd = ConvertDateToNumber(txtDenNgay.Value.ToString())
        strWhereNgay = " and (TO_CHAR(hdr.ngay_ks,'yyyymmdd') >=" & strNgayStart & ") "
        strWhereNgay = strWhereNgay & " and (TO_CHAR(hdr.ngay_ks,'yyyymmdd') <=" & strNgayEnd & ") "


        mv_strNgay = "Ngày " & txtTuNgay.Value.ToString()
        Return strWhereNgay
    End Function

    Private Sub get_NguyenTe()
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable
        Try
            Dim strSQL As String = "SELECT * from Tcs_dm_nguyente "
            dt = DataAccess.ExecuteToTable(strSQL)

            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then

                For i As Integer = 0 To dt.Rows.Count
                    Dim item As ListItem
                    item = New ListItem(dt.Rows(i)("TEN").ToString(), dt.Rows(i)("Ma_NT").ToString())
                    cboNguyenTe.Items.Add(item)
                Next

            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class
