﻿Imports Business
Imports System.Diagnostics
Imports VBOracleLib
Partial Class pages_frmShowError
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Session.Item("User") Is Nothing Then
                'Dim user As String = Session.Item("User").ToString()
                'HeThong.buUsers.UpdateTTLogin(user, "0")
                'Dim ht As Hashtable
                'ht = DirectCast(Application("SESSION_LIST"), Hashtable)
                'If ht.Count > 0 And ht.ContainsKey(user) = True Then
                '    ht.Remove(user)
                'End If
                Session.RemoveAll()
            End If
            ' Response.Redirect("~/pages/frmLogin.aspx")
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình logout ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())
        End Try
    End Sub
End Class
