﻿
Partial Class pages_frmWarning
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.Params("param") Is Nothing Then
                lbThongBao.Text = "User của bạn đang đăng nhập trên một máy khác hoặc phiên làm việc của bản đã hết hiệu lực. Vui lòng liên hệ quản trị hệ thống hoặc đăng nhập lại để tiếp tục chương trình."
            End If
        End If
    End Sub

    Protected Sub btnDNL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDNL.Click
        Response.Redirect("frmLogin.aspx", False)
    End Sub
End Class
