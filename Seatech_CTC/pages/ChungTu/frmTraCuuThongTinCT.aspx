﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmTraCuuThongTinCT.aspx.vb" Inherits="pages_ChungTu_frmTraCuuThongTinCT"
    Title="Tra cứu thông tin chứng từ Thuế" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">TRA CỨU THÔNG TIN CHỨNG TỪ THUẾ</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="color: Blue">
                <asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr align="center">
            <td width='100%' align="center" valign="top" class='nav'>
                <div id="divTracuu">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody align="center">
                            <tr>
                                <td width="100%" align="left" valign="top">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                        <tbody>
                                            <tr style="height: 25px;">
                                                <td width="15%" align="left" valign="middle" class="form_label">
                                                    Mã số thuế
                                                </td>
                                                <td width="35%" align="left" valign="top" class="form_control">
                                                    <asp:TextBox ID="txtMST" MaxLength="16" runat="server" CssClass="inputflat" Style="width: 80%;
                                                        border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                </td>
                                                <td class="form_label" valign="middle">
                                                    MST người nộp tiền
                                                </td>
                                                <td class="form_control">
                                                    <asp:TextBox ID="txtMST_NNT" runat="server" CssClass="inputflat" Style="width: 80%;
                                                        border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr align="left" style="height: 25px;">
                                                <td width="15%" align="left" valign="middle" class="form_label">
                                                    Từ ngày
                                                </td>
                                                <td width="35%" align="left" valign="top" class="form_control">
                                                    <span style="display:block; width:40%; position:relative;vertical-align:middle;">
                                                        <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                                        onblur="CheckDate(this);" onfocus="this.select()" />
                                                    </span>
                                                </td>
                                                <td width="15%" align="left" valign="middle" class="form_label">
                                                    Đến ngày
                                                </td>
                                                <td align="left" valign="top" class="form_control">
                                                    <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                                                        <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                                         onblur="CheckDate(this);" onfocus="this.select()" />
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr align="left" style="height: 25px;">
                                                <td class="form_label" valign="middle" style="width: 12%">
                                                    Số chứng từ
                                                </td>
                                                <td class="form_control">
                                                    <asp:TextBox ID="txtSO_CT" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                        MaxLength="17" Style="width: 80%; border-color: White; font-weight: bold;"></asp:TextBox>
                                                </td>
                                                <td class="form_label" valign="middle">
                                                    Trạng thái
                                                </td>
                                                <td class="form_control">
                                                    <asp:DropDownList ID="ddlTrangThai" runat="server" Style="width: 100%" CssClass="inputflat">
                                                        <asp:ListItem Value="">Tất cả</asp:ListItem>
                                                        <%-- <asp:ListItem Value="00">Lập mới</asp:ListItem>  --%>
                                                        <asp:ListItem Value="01">Đã duyệt</asp:ListItem>
                                                        <asp:ListItem Value="04">Đã hủy</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 10px">
                                    <asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" CssClass="ButtonCommand" />
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                     <asp:BoundColumn DataField="SO_CT" HeaderText="ID" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="so_ct" HeaderText="Số CT">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <%--<asp:TemplateColumn HeaderText="Số CT" HeaderStyle-Width="">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "so_ct") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMChuong.aspx?ID=" & DataBinder.Eval(Container.DataItem, "so_ct") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width=""></HeaderStyle>
                        </asp:TemplateColumn>--%>
                        <asp:BoundColumn DataField="ngay_ct" HeaderText="Ngày CT">
                            <HeaderStyle  Width="60px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_nnthue" HeaderText="Mã số Thuế">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_nnthue" HeaderText="Tên MST">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="dc_nntien" HeaderText="Địa chỉ">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_nntien" HeaderText="MST NNT">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_nntien" HeaderText="Tên NNT">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="tk_co" HeaderText="TK Có">
                            <HeaderStyle Width="20px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_cqthu" HeaderText="Mã CQT">
                            <HeaderStyle Width="40px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_nh_a" HeaderText="NH Chuyển">
                            <HeaderStyle Width="40px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_nh_b" HeaderText="NH Thụ hưởng">
                            <HeaderStyle Width="40px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_ntk" HeaderText="Mã NTK">
                            <HeaderStyle Width="10px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ttien" HeaderText="Số tiền">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table> 
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
        });
    </script>
</asp:Content>
