﻿Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business.NewChungTu
Imports System.Xml
Imports GIPBankService


Partial Class pages_ChungTu_TraCuuCT_API
    Inherits System.Web.UI.Page
    Private mv_objUser As New CTuUser
    Private Shared strMaNhom_GDV As String = ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            If (Not ClientScript.IsStartupScriptRegistered(Me.GetType(), "InitDefValues")) Then
                ClientScript.RegisterStartupScript(Me.GetType(), "InitDefValues", PopulateDefaultValues(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrMaNV", DM_NV(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrMaCN", DM_DiemThu(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrKenhCT", DM_KenhCtu(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLTHUE", DM_LOAITHUE(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitPage", "jsFillMaNV();jsFillMa_CN();jsFillNgay();jsFillLTHUE();", True)
            End If
        End If
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Private Function PopulateDefaultValues() As String
        Dim myJavaScript As StringBuilder = New StringBuilder()
        myJavaScript.Append(" var curMa_NV ='" & CType(Session("User"), Integer).ToString & "';")
        myJavaScript.Append(" var curMa_CN ='" & Session("MA_CN_USER_LOGON").ToString & "';")
        Return myJavaScript.ToString
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetCTU(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, ByVal pv_strSoBT As String, ByVal pv_strMaNV As String, ByVal pv_strNgayKB As String) As String
        Return clsCTU.TCS_GetCTU(pv_strSHKB, pv_strSoCT, pv_strSoBT, pv_strMaNV, pv_strNgayKB)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function LoadCTChiTiet(ByVal pv_strMaNTT As String, ByVal pv_strSHKB As String, _
                                         ByVal pv_strSoCT As String, ByVal pv_strSoBT As String, ByVal pv_strMaNV As String, _
                                         ByVal strNgayKb As String) As String
        Try
            Dim dsCT As DataSet
            Dim dt As DataTable
            Dim key As New KeyCTu
            key.Ma_Dthu = clsCTU.TCS_GetMaDT(pv_strMaNV)
            key.Kyhieu_CT = clsCTU.TCS_GetKHCT(pv_strSHKB) 'clsCTU.TCS_GetKHCT(pv_strMaNV)
            key.Ma_NV = pv_strMaNV
            key.Ngay_KB = strNgayKb
            key.SHKB = pv_strSHKB
            key.So_BT = pv_strSoBT
            key.So_CT = pv_strSoCT
            dsCT = ChungTu.buChungTu.CTU_ChiTiet_Set(key)
            dt = dsCT.Tables(0)

            If Not dsCT Is Nothing Then
                Dim v_strBuildTable As String = ""
                v_strBuildTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width:100%;border-collapse:collapse;' >"
                v_strBuildTable &= "<tr class='grid_header'>"
                v_strBuildTable &= " <td align='center' style=width: 50px;> Mã chương</td>" 'bỏ mã quỹ: <td align='center' style=width: 50px;> Mã qũy</td>
                v_strBuildTable &= " <td align='center' style=width: 50px;> NDKT</td>"
                v_strBuildTable &= "<td align='center' style=width: 200px;> Nội dung</td> <td align='center' style=width: 70px;> Số tiền</td>"
                v_strBuildTable &= "<td align='center' style=width: 70px;> Số đã thu</td> " 'bỏ kỳ thuế: <td align='center' style=width: 70px;> Kỳ thuế</td> 
                v_strBuildTable &= "</tr>"
                Dim i As Integer = 0
                Dim soDaThu As String
                For Each dr As DataRow In dt.Rows
                    Dim dSoTien As Decimal
                    Dim strSoTien As String = ""
                    If Not IsNullOrEmpty(dr("TTien")) Then
                        dSoTien = Convert.ToDecimal(dr("TTien").ToString)
                        strSoTien = dSoTien.ToString("#,#")
                        strSoTien = strSoTien.Replace(",", ".")
                    End If
                    soDaThu = clsCTU.TCS_CalSoDaThu(pv_strMaNTT, dr("MaQuy").ToString(), dr("Ma_Chuong").ToString(), dr("Ma_Khoan").ToString(), dr("Ma_TMuc").ToString(), dr("Ky_Thue").ToString())
                    v_strBuildTable &= "<tr>"
                    'v_strBuildTable &= "<td  class='form_control'><input type='text' style='width:100%;text-align:center;border:1px;border-color:Gray;display:none'  disabled='disabled' id='txtMaQuy_" & dt.Rows.IndexOf(dr).ToString & "'  value='" & dr("MaQuy").ToString & "' /> </td>"
                    v_strBuildTable &= "<td  class='form_control'><input type='text'  style='width:100%;text-align:center;border:1px;border-color:Gray' disabled='disabled' id='txtMaChuong_" & dt.Rows.IndexOf(dr).ToString & "'  value='" & dr("Ma_Chuong").ToString & "'/></td>"
                    v_strBuildTable &= "<td  class='form_control'><input type='text' style='width:100% ;text-align:center;border:1px;border-color:Gray' disabled='disabled' id='txtNDKT_" & dt.Rows.IndexOf(dr).ToString & "'  value='" & dr("Ma_TMuc").ToString & "'  /></td>"
                    v_strBuildTable &= "<td  class='form_control'><input type='text'  style='width:100% ;text-align:left;border:1px;border-color:Gray' disabled='disabled' size='200px'  maxlength='200' id='txtNoiDung_" & dt.Rows.IndexOf(dr).ToString & "' value='" & dr("Noi_Dung").ToString & "' /></td>"
                    v_strBuildTable &= "<td  class='form_control'><input type='text'  style='width:100%;text-align:right;border:1px;border-color:Gray' disabled='disabled' id='txtSoTien_" & dt.Rows.IndexOf(dr).ToString & "'    value='" & strSoTien & "'/></td>"
                    v_strBuildTable &= "<td  class='form_control'><input type='text'  style='width:100% ;text-align:right;border:1px;border-color:Gray' disabled='disabled' id='txtSoTienDT_" & dt.Rows.IndexOf(dr).ToString & "'  value='" & soDaThu & "'   /></td>"
                    'v_strBuildTable &= "<td align='right'><input type='text'  style='width:100%;text-align:center;border:1px;border-color:White;display:none' disabled='disabled' id='MaLoai_" & dt.Rows.IndexOf(dr).ToString & "' value='" & dr("Ky_Thue").ToString & "' /> </td>"
                    v_strBuildTable &= "</tr>"
                Next
                v_strBuildTable &= "</table>"
                Return v_strBuildTable
            Else
                Return ""
            End If


        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi load thông tin chi tiết chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    '<System.Web.Services.WebMethod()> _
    'Public Shared Function GetTK_KH_NH(ByVal pv_strTKNH As String) As String
    '    Return clsCTU.GetTK_KH_NH(pv_strTKNH.Replace("-", ""))
    'End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Huy_CT(ByVal v_strLThue As String, ByVal v_strTT_CTHUE As String, ByVal v_strMaNNT As String, ByVal v_strSHKB As String, ByVal v_strSo_CT As String, ByVal v_strSo_BT As String, ByVal v_strMa_NV As String, ByVal v_strNgayKB As String, ByVal v_strMKNhap As String, ByVal v_strMKSession As String) As String

        Dim v_blnReturn As Boolean = False
        Try
            If clsCTU.checkGioGD() = "0" Then
                Return "0;Đã hết giờ giao dịch trong ngày. Không được tiếp tục hủy lệnh!"
                Exit Function
            End If
            If Not v_strMKSession.Equals(EncryptStr(v_strMKNhap)) Then
                Return "0;Mật khẩu không đúng!"
                Exit Function
            End If

            Dim v_strNgay_KB As String = v_strNgayKB.Substring(6, 4) & v_strNgayKB.Substring(3, 2) & v_strNgayKB.Substring(0, 2)

            Dim v_strMa_Dthu As String = clsCTU.TCS_GetMaDT(v_strMa_NV)

            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""

            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            Dim strLyDoHuy As String = ""

            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = ""
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu


                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)

                If (strTT_Data <> "01") Then
                    Return "1;Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    Exit Function
                End If
                'Gui thong tin Chung tu sang Hai quan
                If v_strLThue = "04" Then
                    'CustomsService.ProcessMSG.getMSG32(v_strMaNNT, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, v_strMa_Dthu, v_strNgay_KB, strErrorNum, strErrorMes, v_strTransaction_id)

                    '2012-09-26-manhnv
                    'Insert du lieu vao bang DOICHIEU_NHHQ
                    ' clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, strErrorNum, TransactionHQ_Type.M51.ToString(), v_strNgay_KB)
                    'strLyDoHuy = drpLyDoHuy.SelectedValue.Trim()
                    hdr.transaction_id = v_strTransaction_id
                    hdr.ly_do_huy = strLyDoHuy
                    ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls)
                    '----------------------
                    'Neu gui thong tin Huy Chung tu sang Hai quan thanh cong
                    If strErrorNum = "0" Then

                        'CAP NHAT TRANG THAI DA CHUYEN HAI QUAN
                        ChungTu.buChungTu.CTU_SetTrangThai(key, "04", "", "", "", "", "", "", "0", strLyDoHuy)
                        Return "1;Hủy thành công."
                        Exit Function
                    Else
                        If strErrorNum = "00000" Then
                            Return "0;Hủy không thành công. Có lỗi khi gửi đi HQ: " & strErrorNum & " - " & strErrorMes
                        Else
                            Return "0;Hủy không thành công. Lỗi từ HQ: " & strErrorNum & " - " & strErrorMes
                        End If
                        Exit Function
                    End If
                Else
                    If strTT_Data = "01" And v_strTT_CTHUE = "1" Then
                        Try
                            LogDebug.WriteLog("huyCTU - MST:" & v_strMaNNT & " SHKB: " & key.SHKB & " Ma NV: " & key.Ma_NV, EventLogEntryType.Information)
                            GIPBankInterface.huyCTU(v_strMaNNT, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, v_strNgay_KB)
                            LogDebug.WriteLog("TB thanh cong - MST:" & v_strMaNNT & " SHKB: " & key.SHKB & " Ma NV: " & key.Ma_NV, EventLogEntryType.Information)
                        Catch ex As Exception
                            Return "0;Chưa chuyển được số liệu hủy sang Cơ quan Thuế"
                            Exit Function
                        End Try
                        'strLyDoHuy = drpLyDoHuy.SelectedValue.Trim()
                        'CAP NHAT TRANG THAI DA CHUYEN THUE
                        'v_strTrangThai_CT = "04" 'Trạng thái đã huy
                        ChungTu.buChungTu.CTU_SetTrangThai(key, "04", "", "", "", "", "", "", "0", strLyDoHuy)
                        Return "1;Đã chuyển dữ liệu hủy sang Cơ quan Thuế thành công"
                    Else
                        'strLyDoHuy = drpLyDoHuy.SelectedValue.Trim()
                        'CAP NHAT TRANG THAI DA CHUYEN THUE
                        'v_strTrangThai_CT = "04" 'Trạng thái đã huy
                        ChungTu.buChungTu.CTU_SetTrangThai(key, "04", "", "", "", "", "", "", "1", strLyDoHuy)
                        Return "1;Hủy thành công."
                    End If
                End If
            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("0;Lỗi trong quá trình hủy chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

            If Not ex.InnerException Is Nothing Then
                sbErrMsg = New StringBuilder()

                sbErrMsg.Append("0;Lỗi trong quá trình hủy chứng từ")
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.Message)
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.StackTrace)

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
            End If
            Return sbErrMsg.ToString

        Finally
        End Try

        Return "0; "
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function LoadCTList(ByVal strTuSo As String, ByVal strDenSo As String, ByVal strTuSoFT As String, ByVal strDenSoFT As String, ByVal strTuNgay As String, _
                                     ByVal strDenNgay As String, ByVal strMaNNT As String, ByVal strTenNNT As String, _
                                     ByVal strTKNo As String, ByVal strTKCo As String, ByVal strKHCT As String, _
                                     ByVal strSoTien As String, ByVal strTTCT As String, ByVal ddlLoaiThue As String, _
                                     ByVal strMaNV As String, ByVal strMa_CN As String, ByVal pv_strMaNN As String, ByVal pv_strMaCN As String, _
                                     ByVal pv_strKenhCT As String, ByVal pv_strSo_FCC As String, ByVal pv_strSo_refCITAD As String) As String
        Try
            Dim dtCT As DataTable
            dtCT = dtlCTU_Load(strTuSo, strDenSo, strTuSoFT, strDenSoFT, strTuNgay, strDenNgay, strMaNNT, _
                               strTenNNT, strTKNo, strTKCo, strKHCT, strSoTien, strTTCT, ddlLoaiThue, strMaNV, strMa_CN, pv_strMaNN, pv_strMaCN, "", pv_strSo_FCC, pv_strSo_refCITAD)
            If Not dtCT Is Nothing Then
                Dim v_strBuildTable As String = ""
                v_strBuildTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width:100%;border-collapse:collapse;'>"
                Dim i As Integer = 0
                For Each dr As DataRow In dtCT.Rows
                    If i <= 80 Then
                        v_strBuildTable &= "<tr onclick=jsGetCTU('" & dr("SHKB").ToString & "','" & dr("SO_CT").ToString & "','" & dr("SO_BT").ToString & "','" & dr("TTien").ToString & "','" & dr("TT_TTHU").ToString & "','" & dr("MA_NV").ToString & "','" & dr("NGAY_KB").ToString & "','" & dr("tt_cthue").ToString & "') " & "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"
                        v_strBuildTable &= "<td align='center' style='width:10%'><img src='" & Business.CTuCommon.Get_ImgTrangThai(dr("TRANG_THAI").ToString, dr("tt_cthue").ToString, dr("ma_ks").ToString) & "' alt='" & Business.CTuCommon.Get_TrangThai(dr("TRANG_THAI").ToString) & "' /></td>"
                        v_strBuildTable &= "<td align='center' style='width:30%'><a href='#'>" & dr("SO_CT").ToString & "</a></td>"
                        v_strBuildTable &= "<td align='center' style='width:60%'>" & dr("TEN_DN").ToString & "/" & dr("SO_BT").ToString & "</td>"
                        v_strBuildTable &= "</tr>"
                    Else
                        Exit For
                    End If
                    i += 1
                Next
                v_strBuildTable &= "</table>"
                Return v_strBuildTable
            Else
                Return ""
            End If
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi load danh sách chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function LoadCTListToDay(ByVal strTuSo As String, ByVal strDenSo As String, ByVal strTuSoFT As String, ByVal strDenSoFT As String, ByVal strTuNgay As String, _
                                 ByVal strDenNgay As String, ByVal strMaNNT As String, ByVal strTenNNT As String, _
                                 ByVal strTKNo As String, ByVal strTKCo As String, ByVal strKHCT As String, _
                                 ByVal strSoTien As String, ByVal strTTCT As String, ByVal ddlLoaiThue As String, _
                                 ByVal strMaNV As String, ByVal strMa_CN As String, ByVal pv_strMaNN As String, ByVal pv_strMaCN As String, _
                                 ByVal pv_strKenhCT As String, ByVal pv_strSo_FCC As String, ByVal pv_strSo_refCITAD As String) As String
        LogApp.AddDebug("cmdTraCuu_Click", "TRA CUU CHUNG TU: tra cuu chung tu")
        Try
            Dim dtCT As DataTable
            dtCT = dtlCTU_Load(strTuSo, strDenSo, strTuSoFT, strDenSoFT, strTuNgay, strDenNgay, strMaNNT, _
                               strTenNNT, strTKNo, strTKCo, strKHCT, strSoTien, strTTCT, ddlLoaiThue, strMaNV, strMa_CN, pv_strMaNN, pv_strMaCN, "", pv_strSo_FCC, pv_strSo_refCITAD)
            If Not dtCT Is Nothing Then
                Dim v_strBuildTable As String = ""
                v_strBuildTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width:100%;border-collapse:collapse;'>"
                Dim i As Integer = 0
                For Each dr As DataRow In dtCT.Rows
                    If i <= 80 Then
                        If dr("Loai_Thue").ToString.Equals("04") Then
                            v_strBuildTable &= "<tr onclick=jsShowChiTietCT_HQ('" & dr("SO_CT").ToString & "') " & "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"
                        Else
                            v_strBuildTable &= "<tr onclick=jsShowChiTietCT('" & dr("SO_CT").ToString & "') " & "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"
                        End If
                        v_strBuildTable &= "<td align='center' style='width:4%'>" & (i + 1).ToString & "</td>"
                        v_strBuildTable &= "<td align='center' style='width:8%'><img src='" & Business.CTuCommon.Get_ImgTrangThai(dr("TRANG_THAI").ToString, dr("tt_cthue").ToString, dr("ma_ks").ToString) & "' alt='" & Business.CTuCommon.Get_TrangThai(dr("TRANG_THAI").ToString) & "' /></td>"
                        v_strBuildTable &= "<td align='center' style='width:12%'>" & dr("SO_CT_NH").ToString & "</td>"
                        v_strBuildTable &= "<td align='center' style='width:12%'><a href='#'>" & dr("SO_CT").ToString & "</a></td>"
                        'v_strBuildTable &= "<td align='center' style='width:8%'>" & dr("RELATION_NO").ToString & "</td>"
                        v_strBuildTable &= "<td align='right' style='width:10%'>" & Format_Number(dr("TTien").ToString, ".") & "</td>"
                        'v_strBuildTable &= "<td align='center' style='width:10%'>" & dr("TT_CITAD").ToString & "</td>"
                        v_strBuildTable &= "<td align='center' style='width:10%'>" & dr("NGUOI_LAP").ToString & "</td>"
                        v_strBuildTable &= "<td align='center' style='width:10%'>" & dr("NGUOI_DUYET").ToString & "</td>"
                        v_strBuildTable &= "<td align='center' style='width:15%'>" & dr("NGAY_HT").ToString & "</td>"
                        v_strBuildTable &= "<td align='center'>" & dr("NGAY_KS").ToString & "</td>"
                        v_strBuildTable &= "</tr>"
                    Else
                        Exit For
                    End If
                    i += 1
                Next
                v_strBuildTable &= "</table>"
                Return v_strBuildTable
            Else
                Return ""
            End If
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi load danh sách chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    Protected Shared Function dtlCTU_Load(ByVal strTuSo As String, ByVal strDenSo As String, _
                                          ByVal strTuSoFT As String, ByVal strDenSoFT As String, _
                                          ByVal strTuNgay As String, ByVal strDenNgay As String, _
                                          ByVal strMaNNT As String, ByVal strTenNNT As String, _
                                          ByVal strTKNo As String, ByVal strTKCo As String, _
                                          ByVal strKHCT As String, ByVal strSoTien As String, _
                                          ByVal strTTCT As String, ByVal ddlLoaiThue As String, _
                                          ByVal strMaNV As String, ByVal strMa_CN As String, _
                                          ByVal pv_strMaNN As String, ByVal pvCN_NSD As String, _
                                          ByVal strKenhCT As String, ByVal pv_strSo_FCC As String, _
                                          ByVal pv_strSo_refCITAD As String) As DataTable


        Dim strWhere As String = ""
        Dim dt As New DataTable
        Dim strSQL As String
        If strTuSo <> "" Then
            strWhere += " AND UPPER(hdr.SO_CT)>=" & Globals.EscapeQuote(strTuSo.ToUpper) & ""
        End If
        If strDenSo <> "" Then
            strWhere += " AND  UPPER(hdr.SO_CT)<=" & Globals.EscapeQuote(strDenSo.ToString.Trim().ToUpper) & ""
        End If

        If pv_strSo_FCC <> "" Then
            strWhere += " AND UPPER(hdr.SO_CT_NH)=" & Globals.EscapeQuote(pv_strSo_FCC.ToUpper)
        End If
        If pv_strSo_refCITAD <> "" Then
            strWhere += " AND UPPER(hdr.SO_CT_NH)=" & Globals.EscapeQuote(pv_strSo_refCITAD.ToUpper.Trim())
        End If
        If strTuSoFT <> "" Then
            strTuSoFT = strTuSoFT.Substring(2)
            strWhere += " AND substr(hdr.SO_CT_NH,3) >=" & Globals.EscapeQuote(strTuSoFT) & ""
        End If
        If strDenSoFT <> "" Then
            strDenSoFT = strDenSoFT.Substring(2)
            strWhere += " AND substr(hdr.SO_CT_NH,3) <=" & Globals.EscapeQuote(strDenSoFT) & ""
        End If
        Dim strNgayStart As String
        strNgayStart = ConvertDateToNumber(strTuNgay.ToString())
        Dim strNgayFinish As String = ConvertDateToNumber(strDenNgay.ToString())
        'bo tim kiem theo ngay ngay kb
        If strNgayFinish = "0" And strNgayStart <> "0" Then
            strNgayFinish = DateTime.Now.ToString("yyyyMMdd")
            strWhere += " and ( (TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') >=" & strNgayStart & " and TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') <=" & strNgayFinish & "))"
        ElseIf strNgayFinish <> "0" And strNgayStart = "0" Then
            strWhere += " and (TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') <=" & strNgayFinish & ")"
        ElseIf strNgayStart <> "0" And strNgayFinish <> "0" Then
            strWhere += " and ( (TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') >=" & strNgayStart & " and TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') <=" & strNgayFinish & ") )"
        End If

        'If strTuNgay <> "" Then
        '    strWhere += " and ( (TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') >=" & strNgayStart & " and TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd') <=" & strNgayFinish & ") or (hdr.ngay_kb >= " & strNgayStart & " and hdr.ngay_kb<=" & strNgayFinish & "))"
        'End If
        'If strDenNgay <> "" Then
        '    strWhere += " AND TO_CHAR(hdr.ngay_KH_NH,'yyyymmdd' ) <=" & ConvertDateToNumber(strDenNgay.ToString())
        'End If


        If strMaNNT <> "" Then
            strWhere += " AND hdr.MA_NNTHUE LIKE '%" & strMaNNT & "%'"
        End If
        If strTenNNT <> "" Then
            strWhere += " AND UPPER(hdr.TEN_NNTHUE) LIKE upper('%" & strTenNNT & "%')"
        End If

        If strTKCo <> "" Then
            strWhere += " AND hdr.TK_CO LIKE '%" & strTKCo & "%'"
        End If


        If strTKNo <> "" Then
            strWhere += " AND hdr.TK_NO LIKE '%" & strTKNo & "%'"
        End If

        If ddlLoaiThue <> "00" Then
            If ddlLoaiThue = "04" Then
                strWhere += " AND hdr.MA_LTHUE = '04'"
            Else
                strWhere += " AND hdr.MA_LTHUE <> '04'"
            End If
            'strWhere += " AND hdr.MA_LTHUE =" & Globals.EscapeQuote(ddlLoaiThue)
        End If

        If strKHCT <> "" Then
            strWhere += " AND hdr.KYHIEU_CT LIKE '%" & strKHCT & "%'"
        End If

        If strTTCT = "99" Then
            strWhere += ""
        ElseIf strTTCT = "04" Then
            strWhere += " AND hdr.TRANG_THAI=04 AND hdr.MA_KS=0"
        ElseIf strTTCT = "06" Then
            strWhere += " AND hdr.TRANG_THAI=04 AND hdr.MA_KS<> 0"
        ElseIf strTTCT = "02" Then
            strWhere += " AND hdr.TRANG_THAI=02"
        ElseIf strTTCT = "08" Then
            strWhere += " AND hdr.TRANG_THAI=04 AND hdr.MA_KS <> 0 and TT_CTHUE='1'"
        ElseIf strTTCT = "07" Then
            strWhere += " AND hdr.TRANG_THAI=01 AND hdr.MA_KS <> 0 and TT_CTHUE='0'"
        Else
            strWhere += " AND hdr.TRANG_THAI=" & Globals.EscapeQuote(strTTCT)
        End If

        If strSoTien <> "" Then
            strWhere += " AND hdr.TTIEN=" & CInt(strSoTien)
        End If
        'If strMaNV <> "" Then
        '    strWhere += " And (hdr.Ma_NV = " & strMaNV & ") "
        'End If


        strWhere += " And (UPPER(hdr.Kenh_CT) = '02') "

        'If strMa_CN <> "" Then
        '    strWhere += " and cn.id = '" & strMa_CN & "'"

        'End If


        Try

            'strSQL = "SELECT hdr.SHKB,hdr.KYHIEU_CT, hdr.SO_CT,hdr.Ma_NV, hdr.SO_BT,hdr.TRANG_THAI, hdr.So_BThu, hdr.TTien, hdr.TT_TTHU, hdr.NGAY_KB,hdr.MA_XA,hdr.tt_cthue,hdr.SO_CT_NH,nv.TEN_DN,hdr.ma_ks," & _
            '    " hdr.Ma_LTHUE as Loai_Thue, hdr.MA_NNTHUE,HDR.TEN_NNTHUE,lt.ten as ten_thue,TO_CHAR(hdr.ngay_ht,'dd/MM/yyyy hh24:mm:ss') as ngay_ht,TO_CHAR(hdr.ngay_ks,'dd/MM/yyyy hh24:mm:ss') as ngay_ks,hdr.seq_no, " & _
            '    " (CASE  WHEN HDR.MA_NV=0 THEN '' ELSE (SELECT NV1.TEN_DN FROM TCS_DM_NHANVIEN NV1 WHERE NV1.MA_NV=HDR.MA_NV) END) AS NGUOI_LAP, " & _
            '    " (CASE  WHEN HDR.MA_KS=0 THEN '' ELSE (SELECT NV1.TEN_DN FROM TCS_DM_NHANVIEN NV1 WHERE NV1.MA_NV=HDR.MA_KS) END) AS NGUOI_DUYET, " & _
            '    " (CASE  WHEN ci.tt_chuyen>0 THEN 'Ðã chuyển' ELSE 'Chưa chuyển' END) AS TT_CITAD,ci.RELATION_NO " & _
            '    " FROM TCS_CTU_HDR hdr,tcs_dm_nhanvien nv,tcs_dm_chinhanh cn,tcs_dm_lthue lt,tcs_ds_citad ci  " & _
            '    " WHERE hdr.ma_nv=nv.ma_nv(+) and nv.ma_cn = cn.id(+) and hdr.so_ct = ci.so_ct(+) and hdr.Ma_LTHUE=lt.ma_lthue " & strWhere & _
            '    " ORDER BY hdr.trang_thai,hdr.so_ct DESC "

            strSQL = "SELECT hdr.SHKB,hdr.KYHIEU_CT, hdr.SO_CT,hdr.Ma_NV, hdr.SO_BT,hdr.TRANG_THAI, hdr.So_BThu, hdr.TTien, hdr.TT_TTHU, hdr.NGAY_KB,hdr.MA_XA,hdr.tt_cthue,hdr.SO_CT_NH,nv.TEN_DN,hdr.ma_ks," & _
            " hdr.Ma_LTHUE as Loai_Thue, hdr.MA_NNTHUE,HDR.TEN_NNTHUE,lt.ten as ten_thue,TO_CHAR(hdr.ngay_ht,'dd/MM/yyyy hh24:mm:ss') as ngay_ht,TO_CHAR(hdr.ngay_ks,'dd/MM/yyyy hh24:mm:ss') as ngay_ks,hdr.seq_no, " & _
            " (CASE  WHEN HDR.MA_NV=0 THEN '' ELSE (SELECT NV1.TEN_DN FROM TCS_DM_NHANVIEN NV1 WHERE NV1.MA_NV=HDR.MA_NV) END) AS NGUOI_LAP, " & _
            " (CASE  WHEN HDR.MA_KS=0 THEN '' ELSE (SELECT NV1.TEN_DN FROM TCS_DM_NHANVIEN NV1 WHERE NV1.MA_NV=HDR.MA_KS) END) AS NGUOI_DUYET " & _
            " FROM TCS_CTU_HDR hdr,tcs_dm_nhanvien nv,tcs_dm_chinhanh cn,tcs_dm_lthue lt  " & _
            " WHERE hdr.ma_nv=nv.ma_nv(+) and nv.ma_cn = cn.id(+) and hdr.Ma_LTHUE=lt.ma_lthue " & strWhere & _
            " ORDER BY hdr.trang_thai,hdr.so_ct DESC "


            dt = DatabaseHelp.ExecuteToTable(strSQL)
            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function LOAD_MN() As String
        Dim str_return As String = ""
        Dim strSQL_MaNhom As String = "select ma_nhom from tcs_dm_nhanvien where ma_nv='" & Session.Item("User").ToString & "'"
        Dim dt As New DataTable
        dt = DataAccess.ExecuteToTable(strSQL_MaNhom)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            str_return = dt.Rows(0)("ma_nhom").ToString
        End If
        Return str_return
    End Function
    Private Function DM_NV() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable
        Dim dk As String = ""
        Dim strcheckHS As String = checkHSC()
        Dim str_Ma_Nhom As String = ""
        Dim strSQL As String = ""
        'If cboDiemThu.Text.Equals("") Then
        If strcheckHS <> "1" Then
            If strcheckHS = "2" Then
                dk = "and b.branch_id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "' "
            Else
                dk = "and b.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "' "
            End If
        End If
        'Else
        'dk = "And ma_cn ='" & cboDiemThu.SelectedValue.ToString & "'"
        ' End If
        str_Ma_Nhom = LOAD_MN()
        'If str_Ma_Nhom = strMaNhom_GDV Then
        '    strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nv='" & Session.Item("User").ToString & "' and a.ma_nhom='" & strMaNhom_GDV & "' " & dk
        'Else
        '    strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nhom='" & strMaNhom_GDV & "' " & dk
        'End If
        'If strcheckHS = "1" Then
        strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a"
        'End If
        'Dim strSQL As String = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nhom='02' " & dk
        dt = DataAccess.ExecuteToTable(strSQL)
        Dim noOfItems As Integer = dt.Rows.Count
        Dim myJavaScript As StringBuilder = New StringBuilder()
        myJavaScript.Append(" var arrMaNV= new Array(" & noOfItems.ToString & ");")
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMaNV[" & i & "] ='" & dt.Rows(i)("Ma_NV") & ";" & dt.Rows(i)("ten_DN") & "';")
            Next
            'Return myJavaScript.ToString()
        End If
        Return myJavaScript.ToString()
        'Return String.Empty
    End Function
    Private Function DM_DiemThu() As String
        Dim cnDiemThu As New DataAccess
        Dim dsDiemThu As DataTable
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        Dim str_Ma_Nhom As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        'If strcheckHSC = "1" Then
        '    'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
        '    ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
        '    strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a "
        'ElseIf strcheckHSC = "2" Then

        '    strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where    (a.branch_id = '" & Session.Item("MA_CN_USER_LOGON").ToString & "' or a.id = '" & Session.Item("MA_CN_USER_LOGON").ToString & "')"
        'Else
        '    strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where    a.id = '" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        'End If

        str_Ma_Nhom = LOAD_MN()
        'If str_Ma_Nhom = strMaNhom_GDV Then
        '    strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a where a.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        'Else
        '    If strcheckHSC = "1" Then
        'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
        ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
        strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a  "

        '    ElseIf strcheckHSC = "2" Then

        '        strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        '    Else
        '        strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        '    End If
        'End If
        strSQL &= " order by a.id"
        Try
            dsDiemThu = DataAccess.ExecuteToTable(strSQL)
            Dim noOfItems As Integer = dsDiemThu.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrMaCN= new Array(" & noOfItems.ToString & ");")
            If Not VBOracleLib.Globals.IsNullOrEmpty(dsDiemThu) Then
                For i As Integer = 0 To dsDiemThu.Rows.Count - 1
                    myJavaScript.Append(" arrMaCN[" & i & "] ='" & dsDiemThu.Rows(i)("id") & ";" & dsDiemThu.Rows(i)("name") & "';")
                Next
                'Return myJavaScript.ToString()
            End If
            Return myJavaScript.ToString()
            'Return String.Empty
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi load danh mục điểm thu", HttpContext.Current.Session.Item("TEN_DN"))
            Return String.Empty
            Throw ex
        Finally
            'If (Not IsNothing(cnDiemThu)) Then DataAccess.Dispose()
        End Try

    End Function
    Private Function DM_KenhCtu() As String
        Dim cnKenhCtu As New DataAccess
        Dim dsKCTu As DataTable
        Dim dk As String = ""
        'dk = "And ma_cn like'%" & cboDiemThu.SelectedValue.ToString & "%'"

        Dim strSQL As String = "SELECT a.ma_kenh,a.ten_kenh FROM tcs_kenh_ct a " ' & dk
        Try
            dsKCTu = DataAccess.ExecuteToTable(strSQL)
            'clsCommon.ComboBoxWithValue(cboKenhCtu, dsKCTu, 1, 0, "Tất cả")
            Dim noOfItems As Integer = dsKCTu.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrKenhCT= new Array(" & noOfItems.ToString & ");")
            If Not VBOracleLib.Globals.IsNullOrEmpty(dsKCTu) Then
                For i As Integer = 0 To dsKCTu.Rows.Count - 1
                    myJavaScript.Append(" arrKenhCT[" & i & "] ='" & dsKCTu.Rows(i)("ma_kenh") & ";" & dsKCTu.Rows(i)("ten_kenh") & "';")
                Next
                'Return myJavaScript.ToString()
            End If
            Return myJavaScript.ToString()
            'Return String.Empty
        Catch ex As Exception
            Return String.Empty
            Throw ex
        Finally
            'DataAccess.Dispose()
        End Try
    End Function
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = BaoCao.buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    Private Function DM_LOAITHUE() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT * from TCS_DM_LTHUE ORDER BY MA_LTHUE "
        dt = DataAccess.ExecuteToTable(strSQL)
        Dim noOfItems As Integer = dt.Rows.Count
        Dim myJavaScript As StringBuilder = New StringBuilder()
        myJavaScript.Append(" var arrMaLTHUE= new Array(3);")
        myJavaScript.Append(" arrMaLTHUE[0] ='00;<< Tất cả >>';")
        myJavaScript.Append(" arrMaLTHUE[1] ='01;Thuế Nội địa';")
        myJavaScript.Append(" arrMaLTHUE[2] ='04;Thuế Hải quan';")
        'If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
        '    For i As Integer = 0 To dt.Rows.Count - 1

        '        myJavaScript.Append(" arrMaLTHUE[" & i & "] ='" & dt.Rows(i)("MA_LTHUE") & ";" & dt.Rows(i)("TEN") & "';")

        '    Next
        '    'Return myJavaScript.ToString()
        'End If
        Return myJavaScript.ToString()
        'Return String.Empty
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Image.Attributes.Add("onclick", "popUpCalendar(document.forms[0]." + TextBox.ClientID + ", document.forms[0]." + TextBox.ClientID + ", '" + datetimeFormat + "'); return false;")
        ' btnCalendar1.Attributes.Add("onclick", "popUpCalendar(document.forms[0]." + txtTuNgay + ", document.forms[0]." + TextBox.ClientID + ", '" + datetimeFormat + "')
        'clsCommon.addPopUpCalendarToImage(btnCalendar1, txtTuNgay, "dd/mm/yyyy")
        'clsCommon.addPopUpCalendarToImage(btnCalendar2, txtDenNgay, "dd/mm/yyyy")
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))
            hdfMaNV.Value = mv_objUser.Ma_NV
            hdfMaNhom.Value = mv_objUser.Ma_Nhom
            hdfMK.Value = mv_objUser.Mat_Khau
        End If
        ClientScript.RegisterStartupScript(Me.GetType(), "GetDSCT_ToDay", "jsDSCT_ToDay();", True)
    End Sub
End Class
