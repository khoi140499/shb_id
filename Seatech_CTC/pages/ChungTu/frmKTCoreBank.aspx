﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmKTCoreBank.aspx.vb" Inherits="pages_ChungTu_frmKTCoreBank" Title="Untitled Page" %>

<%--<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KIỂM TRA DỮ LIỆU</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <table id="Table2" width="100%" border="0" cellpadding="2" cellspacing="1" class="form_input">
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblTuNgay" runat="server" CssClass="label">Ngày làm việc</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox runat="server" ID="txtTuNgay" Width="120px" CssClass="inputflat"></asp:TextBox>&nbsp;
                            <%--<rjs:PopCalendar ID="cldrTuNgay" runat="server" Control="txtTuNgay"  Separator="/" Format="dd/mm/yyyy"  />--%>
                        </td>
                        <td colspan="2" align="left" class="form_control">
                            <asp:Button ID="cmdKiemTra" runat="server" CssClass="ButtonCommand" Text="[Kiểm tra]"
                                TabIndex="5"></asp:Button>&nbsp;<asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand"
                                    Text="[Thoát]" TabIndex="5"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
            </td>
        </tr>
        <tr>
            <td>
                <div style="vertical-align top; height: 200px; overflow: auto; width: 100%;">
                    <asp:DataGrid ID="dtg" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                        Width="100%" CssClass="grid_data">
                        <HeaderStyle CssClass="grid_header"></HeaderStyle>
                        <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                        <ItemStyle CssClass="grid_item" />
                        <Columns>
                            <asp:BoundColumn DataField="Ngay_KB" HeaderText="Ngày">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MA_DTHU" HeaderText="Mã DT" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KyHieu_CT" HeaderText="Ký hiệu CT" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="So_CT" HeaderText="Số CT" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="So_BT" HeaderText="Mã NV/ Số BT" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MA_NNTHUE" HeaderText="Mã NNT" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TEN_NNTHUE" HeaderText="Tên NNT" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ORG_REF" HeaderText="Số TC gốc" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PAY_IN_CTC" HeaderText="SDHT tại CTC" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PAY_IN_BANK" HeaderText="SDHT tại NH" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
