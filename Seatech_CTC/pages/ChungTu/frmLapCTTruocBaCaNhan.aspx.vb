﻿Imports ProcMegService
Imports Newtonsoft.Json
Imports log4net
Imports System.Data
Imports VBOracleLib
Imports System.Xml.XmlDocument
Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.Common
Imports Business.ChungTu
Imports Business.NewChungTu
Imports Business.BaoCao
Imports CorebankServiceESB
Imports System.Xml

Partial Class pages_ChungTu_frmLapCTTruocBaCaNhan
    Inherits System.Web.UI.Page

    Public Enum eCTUAction
        Them_Moi = 0
        Ghi = 1
        Chuyen_KS = 2
        Ghi_ChuyeKS = 3
        Huy = 4
        KhoiPhuc = 5
        InCT = 6
        KiemSoat = 7
        ChuyenTra = 8
        Huy_KS = 9
    End Enum

    Private Shared DefSHKB As String = ""
    Private Shared DefCQThu As String = ""
    Private Shared DefDBHC As String = ""

    Private Shared mblnEdit As Boolean = False
    Private Shared mNguonNNT As Integer = 0
    Private Shared url As String = ""

    Private Const ERR_SYSTEM_OK As String = "0"

    Private Const gc_MAX_SOTIEN_LENGTH As Integer = 15
    Private Const gc_MAX_KYTHUE_LENGTH As Integer = 7
    Private mv_objUser As New CTuUser
    Dim v_strKHCT As String = ""
    Private Ma_CN As String = ""
    Private mv_SesssionID As String = ""
    ' Private mv_objUser As New CTuUser
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Private Shared cls_songphuong As New CorebankServiceESB.clsSongPhuong
    Private Shared CORE_ON_OFF As String = ConfigurationManager.AppSettings.Get("CORE.ON").ToString()
    Private Shared CORE_VERSION As String = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString()
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private Shared TIME_BEGIN As Date = DateTime.Now

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenTieuMuc(ByVal pv_strMaTMuc As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return clsCTU.Get_TenTieuMuc(pv_strMaTMuc)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function jsGet_KBInfo(ByVal pv_strSHKB As String) As String
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If
            Dim tenKB As String = clsCTU.Get_TenSHKB(pv_strSHKB)
            If Not tenKB Is Nothing Then
                Return tenKB
            End If

            'Return ProcMegService.ProMsg.getTr 'clsCTU.TCS_Check_NNThue(pv_strMaNNT, strNgay_LV)
        Catch ex As Exception
            logger.Error(ex.Message & " - " & ex.StackTrace)
        End Try
        Return ""
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function jsGet_DBHC_KBInfo(ByVal pv_strSHKB As String) As String
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If
            Dim DBHC_KB As String = clsCTU.Get_DBHC_SHKB(pv_strSHKB)
            If Not DBHC_KB Is Nothing Then
                Return DBHC_KB
            End If

            'Return ProcMegService.ProMsg.getTr 'clsCTU.TCS_Check_NNThue(pv_strMaNNT, strNgay_LV)
        Catch ex As Exception
            logger.Error(ex.Message & " - " & ex.StackTrace)
        End Try
        Return ""
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function CallSo_DT(ByVal pv_strMaNNT As String, ByVal pv_strMaQuy As String, ByVal pv_strMa_Chuong As String, ByVal pv_strMaKhoan As String, ByVal pv_strMa_Muc As String, ByVal pv_strKyThue As String, ByVal txtSoDaThu As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return txtSoDaThu & ";" & clsCTU.TCS_CalSoDaThu(pv_strMaNNT, pv_strMaQuy, pv_strMa_Chuong, pv_strMaKhoan, pv_strMa_Muc, pv_strKyThue)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function LoadCTList(ByVal sMaNV As String, ByVal sSoCtu As String) As String
        Dim dtCT As DataTable = Nothing
        Dim sTable As String = ""
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sTable = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If

            Dim dsCT As DataSet = ChungTu.buChungTu.CTU_GetListCT(clsCTU.TCS_GetNgayLV(sMaNV), CInt(sMaNV), clsCTU.TCS_GetMaDT(CInt(sMaNV)), "01", sSoCtu)

            dtCT = dsCT.Tables(0)

            sTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width:100%;border-collapse:collapse;'>"

            Dim i As Integer = 0
            For Each dr As DataRow In dtCT.Rows
                sTable &= "<tr onclick=jsGetCTU('" & dr("SO_CT").ToString & "') " & "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"
                sTable &= "<td align='center' style='width: 20%'><img src='" & Business.CTuCommon.Get_ImgTrangThai(dr("TRANG_THAI").ToString, dr("tt_cthue").ToString, dr("ma_ks").ToString) & "' alt='" & Business.CTuCommon.Get_TrangThai(dr("TRANG_THAI").ToString) & "' /></td>"
                sTable &= "<td align='center' style='width: 30%'><a href='#'>" & dr("SO_CT").ToString & "</a></td>"
                sTable &= "<td align='center' style='width: 50%'>" & dr("TEN_DN").ToString & "/" & dr("SO_BT").ToString & "</td>"
                sTable &= "</tr>"
            Next

            sTable &= "<tr style='height: 24px;padding-right:2px;'><td align='right' colspan='3'>Tổng số chứng từ: <span style='font-weight:bold'>" & dtCT.Rows.Count & "</span></td></tr>"

            sTable &= "</table>"
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi dữ liệu chi tiết chứng từ", HttpContext.Current.Session("TEN_DN"))
            sTable = String.Empty
        End Try

        Return sTable
    End Function
    'Lay danh sach chung tu Thue: trang thai 01
    Private Shared Function dtlCTU_Load(ByVal strUser As String, ByVal lNgayKB As Long, ByVal pv_strTrangThai As String, ByVal SoCT As String) As DataTable
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strWhereSTT As String = ""
        Try
            Select Case pv_strTrangThai
                Case "02"
                    strWhereSTT = " AND TT_TTHU<>0 AND MA_TQ<>0"
                Case "01"
                    strWhereSTT = " AND TT_TTHU=0 "
            End Select

            If SoCT <> "" Then
                strWhereSTT = strWhereSTT & " AND So_CT='" & SoCT & "'"
            End If

            strSQL = " SELECT SHKB,KYHIEU_CT, SO_CT,Ma_NV, SO_BT,TRANG_THAI, So_BThu,TTien,TT_TTHU, NGAY_KB,MA_XA" & _
                    " FROM TCS_CTU_HDR  " & _
                    " WHERE NGAY_KB = " & lNgayKB & _
                    " AND TRANG_THAI = '00' " & strWhereSTT & _
                    " AND ISACTIVE = '1'" & _
                    " AND MA_LTHUE = '01'" & _
                    " AND Ma_NV = " & CInt(strUser) & _
                    " ORDER BY so_bt DESC "
            '  " AND MA_DTHU='" & clsCTU.TCS_GetMaDT(strUser) & "'" & _

            dt = DatabaseHelp.ExecuteToTable(strSQL)
            Return dt
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi load chứng từ (trạng thái 01)", HttpContext.Current.Session("TEN_DN"))
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetCTU(ByVal sSoCT As String) As String
        Dim sRet As String = ""
        Dim sMaNV As String = ""
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If

            sMaNV = HttpContext.Current.Session("User")

            'Dim objCTU As Business_HQ.NewChungTu.infChungTu = Nothing
            'objCTU = Business_HQ.HaiQuan.HaiQuanController.GetChungTu(sSoCT)

            Dim objCTU As Business.NewChungTu.infChungTu = Nothing
            objCTU = Business.ChungTu.buChungTu.GetChungTu(sSoCT)
            If objCTU IsNot Nothing Then
                'modify something before serialize to json object
                If objCTU.HDR IsNot Nothing AndAlso Not String.IsNullOrEmpty(objCTU.HDR.Ngay_HT) AndAlso objCTU.HDR.Ngay_HT.Length >= 10 Then
                    objCTU.HDR.Ngay_HT = objCTU.HDR.Ngay_HT.Substring(0, 10)
                End If
                If objCTU.ListDTL IsNot Nothing AndAlso objCTU.ListDTL.Count > 0 Then
                    For Each DTL In objCTU.ListDTL
                        If Not String.IsNullOrEmpty(DTL.NGAY_TK) AndAlso DTL.NGAY_TK.Length >= 10 Then
                            DTL.NGAY_TK = DTL.NGAY_TK.Substring(0, 10)
                        End If
                    Next
                End If

                sRet = Newtonsoft.Json.JsonConvert.SerializeObject(objCTU)
            End If
        Catch ex As Exception
            sRet = String.Empty
            Throw New Exception("Có lỗi trong quá trình lấy thông tin chứng từ")
        End Try

        Return sRet
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTTTinhThanhKB(ByVal sSHKB As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return Business_HQ.HaiQuan.HaiQuanController.GetTTTinhThanhKB(sSHKB)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenQuanHuyen(ByVal pv_strMaNV As String) As String
        Return clsCTU.Get_TenQuanHuyen(pv_strMaNV)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function getSoTK_TruyVanCore(ByVal pv_strTienMat As String) As String
        Dim strReturn As String = ""
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        Dim objUser As CTuUser = New CTuUser(HttpContext.Current.Session("User"))
        Dim macn As String = objUser.MA_CN
        'strReturn = getSoTK_TienMat()
        Dim strSQL As String = "SELECT TAIKHOAN_TIENMAT FROM tcs_dm_chinhanh WHERE branch_id = '" & macn & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)("TAIKHOAN_TIENMAT").ToString
        Else
            Return ""
        End If
        Return strReturn
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenTinh(ByVal pv_strMaNV As String) As String
        Return clsCTU.Get_TenTinhTP(pv_strMaNV)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function jsTruyVanMST(ByVal pv_strType As String, ByVal pv_strMaNNT As String, ByVal strSoTK As String, ByVal strCCCD As String, ByVal strSOQD As String) As String
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If
            Dim obj As ProcMegService.MSG.MSG047.infoTRUYVANTHUE = New MSG.MSG047.infoTRUYVANTHUE
            If pv_strType.Equals("01") Then
                obj = ProcMegService.ProMsg.getMSG05NEW(pv_strMaNNT)
            ElseIf pv_strType.Equals("02") Then
                'thuế cá nhân
                obj = ProcMegService.ProMsg.TruyVanSothue02(pv_strMaNNT, "13")
            ElseIf pv_strType.Equals("03") Then
                ' thuế  TB ô tô - xe máy
                obj = ProcMegService.ProMsg.TruyVanSothue03(pv_strMaNNT, strSoTK)
            ElseIf pv_strType.Equals("08") Then
                ' thuế  TB nhà đất
                obj = ProcMegService.ProMsg.TruyVanSothue08(strSoTK, pv_strMaNNT, strCCCD, strSOQD)
            Else
                'sau ko cần if cái 01 cho nó gọn, đỡ 1 dòng - để tạm cho dễ nhìn
                obj = ProcMegService.ProMsg.getMSG05NEW(pv_strMaNNT)
            End If

            Return JsonConvert.SerializeObject(obj)
            'Return ProcMegService.ProMsg.getTr 'clsCTU.TCS_Check_NNThue(pv_strMaNNT, strNgay_LV)
        Catch ex As Exception
            logger.Error(ex.Message & " - " & ex.StackTrace)
        End Try
        Return ""
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Update_CTU_Status(ByVal pv_strMaNV As String, ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                            ByVal pv_strSoBT As String, ByVal pv_strCTUAction As String, ByVal pv_strSoFT As String, ByVal pv_strCurStatus As String, ByVal pv_strHoldDebit As String) As String
        Dim Mota As String = ""
        Dim NewValue As String = ""
        Dim Action As String = "LAPCT_THUE_HUY"
        Try
            Dim v_strReturnMsg As String = String.Empty
            'If GetBdsStatus() <> gblnTTBDS Then
            '    v_strReturnMsg = "1;Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại"
            '    Return v_strReturnMsg
            'End If
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
                ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
            End If
            Dim key As New KeyCTu
            key.Kyhieu_CT = clsCTU.TCS_GetKHCT(pv_strSHKB)
            key.So_CT = pv_strSoCT
            key.SHKB = pv_strSHKB
            key.Ngay_KB = clsCTU.TCS_GetNgayLV(pv_strMaNV)
            key.Ma_NV = CInt(pv_strMaNV)
            key.So_BT = CInt(pv_strSoBT)
            key.Ma_Dthu = clsCTU.TCS_GetMaDT_KB(pv_strSHKB)
            Dim strMaCN As String = clsCTU.TCS_GetMaChiNhanh(pv_strMaNV)
            Dim strTenDN As String = clsCTU.TCS_GetTenDN(pv_strMaNV)
            Dim strFunction As String = "D"
            Dim strErrNum As String = ""
            Dim strErrMsg As String = ""
            Dim strNumOK As String = "0"
            Dim strLyDoHuy As String = pv_strHoldDebit
            'Load tham so gui huy sang corebank
            Dim str_TS_HUY() As String = pv_strHoldDebit.Split(";")
            '
            If CInt(pv_strMaNV) <> 0 Then
                If (pv_strCTUAction = eCTUAction.Chuyen_KS) Then
                    ChungTu.buChungTu.CTU_ChuyenKS(key, False)
                    v_strReturnMsg = "Chứng từ đã được chuyển sang trạng thái chờ kiểm soát."
                    'clsCoreBankUtilPGB.HoldDebitAccount(objHDR.TK_KH_NH, objHDR.PT_TINHPHI, objHDR.TTien, objHDR.PHI_GD, objHDR.PHI_VAT, "H")
                    strNumOK = "05"
                    Mota = " object: " & pv_strSoCT & " result: Fail  Reason: chung tu chuyen sang trang thai cho kiem soat newvalue=""SO_CT[So_CT=" & pv_strSoCT & "]"" Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:"
                    LogApp.AddDebug(Action, Mota)
                ElseIf (pv_strCTUAction = eCTUAction.Huy) Then
                    If pv_strCurStatus = "02" Then
                        Mota = " object: " & pv_strSoCT & " result: Success  Reason: newvalue=""SO_CT[So_CT=" & pv_strSoCT & "]"" Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:"
                        LogApp.AddDebug(Action, Mota)
                        ChungTu.buChungTu.CTU_Huy_Da_KS(key, False, strLyDoHuy)
                        v_strReturnMsg = "Hủy CT chuyển kiểm soát thành công."
                        strNumOK = "04"
                    Else
                        Mota = " object: " & pv_strSoCT & " result: Success  Reason: newvalue=""SO_CT[So_CT=" & pv_strSoCT & "]"" Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:"
                        LogApp.AddDebug(Action, Mota)
                        ChungTu.buChungTu.CTU_Huy(key, False, strLyDoHuy)
                        v_strReturnMsg = "Hủy chứng từ thành công."
                        strNumOK = "04"
                    End If

                ElseIf (pv_strCTUAction = eCTUAction.KhoiPhuc) Then
                    Mota = " object: " & pv_strSoCT & " result: Success  Reason: khoi phuc chung tu newvalue=""SO_CT[So_CT=" & pv_strSoCT & "]"" Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:"
                    LogApp.AddDebug(Action, Mota)
                    ChungTu.buChungTu.CTU_KhoiPhuc(key, False)
                    v_strReturnMsg = "Khôi phục chứng từ thành công."
                End If
            Else
                Return String.Empty
            End If
            Return strNumOK & ";" & v_strReturnMsg & ";" & "SHKB/SoCT/SoBT" & ":" & key.SHKB & "/" & key.So_CT & "/" & key.So_BT
        Catch ex As Exception
            Mota = " object: " & pv_strSoCT & " result: Fail  Reason: newvalue=""SO_CT[So_CT=" & pv_strSoCT & "]"" Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:" & ex.Message
            LogApp.AddDebug(Action, Mota)
            clsCommon.WriteLog(ex, "Lỗi cập nhật chứng từ", HttpContext.Current.Session("TEN_DN"))
            Return String.Empty
        End Try
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function jsGetLoaiNNT(ByVal pv_strType As String) As String
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If
            Return mdlCommon.TCS_GET_TEN_LOAI_NNT(pv_strType)

            'Return ProcMegService.ProMsg.getTr 'clsCTU.TCS_Check_NNThue(pv_strMaNNT, strNgay_LV)
        Catch ex As Exception
            logger.Error(ex.Message & " - " & ex.StackTrace)
        End Try
        Return ""
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function jsGet_TenTKGL(ByVal pv_strType As String) As String
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If
            Return mdlCommon.TCS_GET_TEN_TAIKHOAN_GL(pv_strType)

            'Return ProcMegService.ProMsg.getTr 'clsCTU.TCS_Check_NNThue(pv_strMaNNT, strNgay_LV)
        Catch ex As Exception
            logger.Error(ex.Message & " - " & ex.StackTrace)
        End Try
        Return ""
    End Function
    'Private Function TK_TM() As String
    '    Dim dt As DataTable

    '    Dim strSQL As String = "SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS IN ('TK_TIENMAT')"
    '    dt = DataAccess.ExecuteToTable(strSQL)

    '    If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
    '        Dim noOfItems As Integer = dt.Rows.Count
    '        Dim myJavaScript As StringBuilder = New StringBuilder()
    '        myJavaScript.Append(" var arrTKTM= new Array(" & noOfItems.ToString & ");")
    '        myJavaScript.Append(" arrTKTM[" & 0 & "] ='" & dt.Rows(0)("GIATRI_TS") & "';")
    '        Return myJavaScript.ToString()
    '    End If
    '    Return String.Empty
    'End Function
    Private Function DM_CHUONG() As String
        Dim dt As DataTable

        Dim strSQL As String = "select ma_chuong from tcs_dm_cap_chuong where tinh_trang='1'"
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrDMCHUONG= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrDMCHUONG[" & i & "] ='" & dt.Rows(i)("ma_chuong") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function

    Private Function PopulateArrDBHC(ByVal strSHKB As String) As String
        'Dim strSQL As String = "SELECT b.shkb ma_kb, b.ma_tinh, A.TEN FROM TCS_DM_XA A, TCS_DM_KHOBAC B  WHERE  B.ma_tinh= A.MA_XA AND B.SHKB='" & strSHKB & "' " '"SELECT a.ma_kb,b.ma_xa, b.ten FROM tcs_map_tk_nh_kb a, tcs_dm_xa b WHERE a.dbhc = b.ma_xa and a.ma_kb='" & strSHKB & "'"
        Dim strSQL As String = "SELECT b.shkb ma_kb, b.ma_db, A.TEN FROM TCS_DM_XA A, TCS_DM_KHOBAC B  WHERE  B.ma_db= A.MA_XA AND B.SHKB='" & strSHKB & "' " '"SELECT a.ma_kb,b.ma_xa, b.ten FROM tcs_map_tk_nh_kb a, tcs_dm_xa b WHERE a.dbhc = b.ma_xa and a.ma_kb='" & strSHKB & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrDBHC = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrDBHC[" & i & "] ='" & dt.Rows(i)("ma_kb") & ";" & dt.Rows(i)("ma_db") & ";" & dt.Rows(i)("ten") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function DM_TIEUMUC() As String
        Dim dt As DataTable

        Dim strSQL As String = "select ma_tmuc from tcs_dm_muc_tmuc where tinh_trang='1'"
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrDMTIEUMUC= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrDMTIEUMUC[" & i & "] ='" & dt.Rows(i)("ma_tmuc") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function DM_PTTT() As String
        Dim dt As DataTable

        Dim strSQL As String = "SELECT ma_pttt,ten_pttt FROM tcs_dm_pttt where status=1 order by ten_pttt"
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrPTTT= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrPTTT[" & i & "] ='" & dt.Rows(i)("ma_pttt") & ";" & dt.Rows(i)("ten_pttt") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        Dim v_strSHKB As String = String.Empty
        Dim v_strMaDiemThu As String = String.Empty
        Dim strNgay_LV As String = ""
        Dim Ma_CN As String = Session.Item("MA_CN_USER_LOGON").ToString()


        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not Session.Item("User") Is Nothing Then
            v_strSHKB = clsCTU.TCS_GetKHKB(CType(Session("User"), Integer))
            v_strMaDiemThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
            v_strKHCT = clsCTU.TCS_GetKHCT(v_strSHKB)
        End If
        If Not IsPostBack Then
            If (Not ClientScript.IsStartupScriptRegistered(Me.GetType(), "InitDefValues")) Then
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitVarTimeout", PopulateVarTimeout(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitDefValues", PopulateDefaultValues(v_strMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrSHKB", PopulateArrSHKB(v_strMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrDBHC", PopulateArrDBHC(v_strSHKB), True)
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitArrDBHC", PopulateArrDBHC(v_strSHKB), True)
                ' ClientScript.RegisterStartupScript(Me.GetType(), "InitArrCQThu", PopulateArrCQThu(v_strMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKGL", PopulateArrTKGL(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrNT", DM_NT(), True)
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitArrMaSanPham", DM_SANPHAM(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLTHUE", DM_LOAITHUE(), True)
                ' ClientScript.RegisterStartupScript(Me.GetType(), "InitArrMaTINHTHANH", DM_TINHTHANH(), True)
                '  ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKNoNSNN", PopulateArrTKNoNSNN(v_strSHKB), True)
                ' ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKCoNSNN", PopulateArrTKCoNSNN(v_strSHKB), True)
                '  ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLHSX", PopulateArrLHSX(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTT_NH_B", PopulateArrTT_NH_B(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTT_NH_TT", PopulateArrTT_NH_TT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrMANHA", DM_MA_NH_A(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrPTTT", DM_PTTT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrDMCHUONG", DM_CHUONG(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrDMTIEUMUC", DM_TIEUMUC(), True)
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitArrDMPhiSP", DM_PHI_SANPHAM(), True)
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitArrDMPhiKiemdem", DM_PHI_KIEMDEM(), True)
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitPage", "jsThem_Moi();", True)

                'ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKTM", TK_TM(), True)
            End If
            If Not Request.Params("SoCT") Is Nothing Then
                Dim soCT As String = Request.Params("SoCT").ToString
                If soCT <> "" Then
                    Dim dtCT As DataTable
                    dtCT = dtlCTU_Load(Session.Item("User").ToString, clsCTU.TCS_GetNgayLV(Session.Item("User").ToString), "01", soCT)
                    If dtCT.Rows.Count > 0 Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "PageLoad", "jsGetCTU('" & dtCT.Rows(0)("SO_CT").ToString & "');", True)
                    End If
                End If
            End If
            'Dim v_objCoreBank As New clsCoreBankUtil
            'If Not Session.Item("User") Is Nothing Then
            '    'divApplet.InnerHtml = v_objCoreBank.GetAppletAvailBDSNote(clsCTU.Get_TellerID(Session.Item("User")).ToString)
            'End If
        End If
    End Sub
    Private Function PopulateDefaultValues(ByVal pv_strMaDiemThu As String) As String
        Dim myJavaScript As StringBuilder = New StringBuilder()
        myJavaScript.Append(" var dtmNgayLV ='" & Business.Common.mdlCommon.ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString)) & "';")
        myJavaScript.Append(" var dtmNgayNH ='" & Business.Common.mdlCommon.ConvertNumbertoString(clsCTU.TCS_GetNgayKH_NH(Session.Item("User").ToString)) & "';")
        myJavaScript.Append(" var dtmNgayCore ='" & ConvertNumbertoString(cls_corebank.Get_VALUE_DATE()) & "';")
        myJavaScript.Append(" var defSHKB ='" & GetDefaultSHKB(pv_strMaDiemThu) & "';")
        myJavaScript.Append(" var defDBHC ='" & GetDefaultDBHC(pv_strMaDiemThu) & "';")
        myJavaScript.Append(" var defCQThu ='" & GetDefaultCQThu(pv_strMaDiemThu) & "';")
        myJavaScript.Append(" var defKHCT ='" & v_strKHCT & "';")
        myJavaScript.Append(" var defMaVangLai ='" & GetDefaultMaVangLai() & "';")

        '07/06/2011 Kienvt: them ma ngan hang nhan theo kho bac
        myJavaScript.Append(" var defMaNHNhan ='" & GetDefaultMaNHNhan(pv_strMaDiemThu) & "';")
        'end
        myJavaScript.Append(" var curCtuStatus ='99';")
        myJavaScript.Append(" var curNgay_LV ='" & clsCTU.TCS_GetNgayLV(Session.Item("User").ToString) & "';")
        myJavaScript.Append(" var curBDS_Status ='" & GetBdsStatus() & "';")
        myJavaScript.Append(" var curMaNNT ='';")
        myJavaScript.Append(" var curKyThue ='" & Business_HQ.HaiQuan.HaiQuanController.GetKyThue(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString)) & "';")
        myJavaScript.Append(" var curSoCT ='';")
        myJavaScript.Append(" var curSo_BT ='';")
        myJavaScript.Append(" var curMa_NV ='" & CType(Session("User"), Integer).ToString & "';")
        myJavaScript.Append(" var curTeller_ID ='" & clsCTU.Get_TellerID(CType(Session("User"), Integer)) & "';")
        myJavaScript.Append(" var saiSo =' " & CTuCommon.Get_ParmValues("SAI_THUCTHU", pv_strMaDiemThu).ToString() & "';")
        myJavaScript.Append(" var sodong ='" & GetSoDong() & "';")
        'myJavaScript.Append(" var tk_tienmat ='" & clsCTU.TCS_GETTS("TK_TIENMAT").ToString() & "';")

        Return myJavaScript.ToString
    End Function
    Private Function PopulateArrTT_NH_B() As String
        Dim strSQL As String = "SELECT DISTINCT( MA_GIANTIEP) as MA_NH_B,  TEN_GIANTIEP as TEN_NH_B, MA_TRUCTIEP as MA_NH_TT,  TEN_TRUCTIEP AS TEN_NH_TT,SHKB  FROM tcs_dm_nh_giantiep_tructiep  ORDER BY MA_GIANTIEP"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrTT_NH_B = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrTT_NH_B[" & i & "] ='" & dt.Rows(i)("MA_NH_B") & ";" & dt.Rows(i)("TEN_NH_B") & ";" & dt.Rows(i)("MA_NH_TT") & ";" & dt.Rows(i)("TEN_NH_TT") & ";" & dt.Rows(i)("SHKB") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function PopulateArrTT_NH_TT() As String
        Dim strSQL As String = "SELECT DISTINCT( MA_TRUCTIEP) as MA_NH_TT,  TEN_TRUCTIEP as TEN_NH_TT  FROM tcs_dm_nh_giantiep_tructiep  ORDER BY MA_TRUCTIEP"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrTT_NH_TT = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrTT_NH_TT[" & i & "] ='" & dt.Rows(i)("MA_NH_TT") & ";" & dt.Rows(i)("TEN_NH_TT") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function PopulateArrSHKB(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT b.shkb,b.ten from  TCS_DM_KHOBAC b WHERE   b.tinh_trang=1"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrSHKB = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrSHKB[" & i & "] ='" & dt.Rows(i)("SHKB") & ";" & dt.Rows(i)("TEN") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function PopulateArrTKGL() As String
        Dim strSQL As String = "SELECT SO_TK, TEN_TK FROM TCS_DM_TK_TG"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        Dim noOfItems As Integer = dt.Rows.Count
        Dim myJavaScript As StringBuilder = New StringBuilder()
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            myJavaScript.Append(" var arrTKGL = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrTKGL[" & i & "] ='" & dt.Rows(i)("SO_TK") & ";" & dt.Rows(i)("TEN_TK") & "';")
            Next
            Return myJavaScript.ToString()
        Else
            myJavaScript.Append(" var arrTKGL = new Array(0);")
            Return myJavaScript.ToString()
        End If

    End Function
    Private Function GetDefaultDBHC(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = " SELECT A.MA_XA, A.TEN FROM TCS_DM_XA A, TCS_THAMSO B WHERE A.MA_XA = B.GIATRI_TS AND B.TEN_TS = 'MA_DBHC' AND B.MA_DTHU = '" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString & ";" & dt.Rows(0)(1).ToString
        End If
        Return String.Empty
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function ConvertCurrToText(ByVal pv_strSoTien As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return ConcertCurToText.ConvertCurr.So_chu(pv_strSoTien.Trim)
        ' Return "123"
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetData_SHKB(ByVal strLoaiDM As String, ByVal strSHKB As String) As String
        Dim v_strReturnMsg As String = strLoaiDM & "|"
        Dim strSql As String = ""
        Dim strMaDBHC As String = ""
        strSHKB = strSHKB.Trim
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Select Case strLoaiDM
            Case "DBHC_DEF".ToUpper
                'strSql = "SELECT B.MA_TINH,B.TEN FROM TCS_DM_KHOBAC A, TCS_DM_cqthu B WHERE  A.shkb= B.shkb AND A.SHKB='" & strSHKB & "' and ma_hq is null"
                strSql = " SELECT B.ma_tinh,c.ten FROM TCS_DM_KHOBAC A, TCS_DM_cqthu B,tcs_dm_xa c WHERE b.trang_thai='1' and A.shkb= B.shkb and b.dbhc_tinh=c.ma_xa AND A.SHKB='" & strSHKB & "' and ma_hq is null"
            Case "DBHC".ToUpper
                'GetString_DBThu("", strSHKB)
                strMaDBHC = Business.Common.mdlCommon.GetMaDBHC_KB(strSHKB)
                strSql = " SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE b.ma_cha ='" & strMaDBHC & "' union SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE b.ma_xa ='" & strMaDBHC & "' "
            Case "TaiKhoanNo".ToUpper
                'strSql = " SELECT distinct a.ma_kb,a.tk_kb, b.ten_tk FROM tcs_map_tk_nh_kb a, tcs_dm_taikhoan b WHERE a.tk_kb = b.tk  AND a.ma_kb = '" & strSHKB & "'"

                strSql = "SELECT distinct a.shkb,a.tk,a.ten_tk FROM tcs_dm_taikhoan A WHERE a.shkb = '" & strSHKB & "' AND a.TK_KB_NH='1'"
            Case "TaiKhoanCo".ToUpper
                strSql = " SELECT dbhc,MA_CQTHU,tk,ten_tk FROM TCS_DM_TAIKHOAN WHERE TRIM(MA_CQTHU) IS NOT NULL AND SHKB='" & strSHKB & "'AND TK_KB_NH='0'" ' DBHC IN (SELECT DBHC FROM TCS_MAP_TK_NH_KB WHERE MA_KB='" & strSHKB & "') AND TINH_TRANG='1' and tk_kb_nh='0'"
                'Case "CQThu_DEF".ToUpper
                ' strSql = " SELECT  A.MA_CQTHU, A.TEN FROM TCS_DM_CQTHU A, TCS_THAMSO B WHERE A.MA_CQTHU = B.GIATRI_TS AND A.MA_DTHU = B.MA_DTHU AND B.TEN_TS = 'CQ_THU' AND A.MA_DTHU IN (SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "')"
            Case "CQTHU_SHKB".ToUpper
                strSql = " SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU WHERE trang_thai='1' and SHKB='" & strSHKB & "' and ma_hq is null"
                'strSql = " SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU WHERE MA_DTHU in(SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "')"
                'strSql = " SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU WHERE MA_DTHU in(SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='MA_DBHC' " & _
                '        " AND giatri_ts IN(SELECT X.ma_xa  FROM tcs_dm_xa X WHERE  X.ma_cha=(SELECT   ma_cha FROM   tcs_dm_xa " & _
                '        " WHERE   ma_xa =(SELECT   giatri_ts FROM   tcs_thamso WHERE   ten_ts = 'MA_DBHC'AND ma_dthu =(SELECT   ma_dthu  FROM   tcs_thamso WHERE ten_ts = 'SHKB' AND giatri_ts ='" & strSHKB & "')))))"
            Case "CQTHU".ToUpper
                strSql = " SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU where trang_thai='1' " 'WHERE MA_DTHU in(SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "')"
            Case "LHSX".ToUpper
                strSql = "SELECT MA_LH as MA_DM, TEN_LH as TEN_DM, TEN_VT FROM TCS_DM_LHINH  ORDER BY TEN_VT"
            Case "SHKB".ToUpper
                'strSql = " SELECT b.shkb,b.ten FROM TCS_THAMSO a,TCS_DM_KHOBAC b WHERE  instr(a.giatri_ts , b.shkb )>0  And a.ten_ts='SHKB_DThu' and b.shkb='" & strSHKB & "' and a.ma_dthu='" & gstrMaDiemThu & "'"
            Case "KHCT".ToUpper
                strSql = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' AND ROWNUM=1 " 'AND MA_DTHU=(SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "')"
            Case "TINH_SHKB".ToUpper
                strSql = "select xa.ma_tinh,xa.ten from tcs_dm_khobac a, tcs_dm_xa xa,tcs_dm_cqthu c where c.trang_thai='1' and a.ma_tinh=xa.ma_xa and a.shkb=c.shkb and a.shkb='" & strSHKB & "'" 'strSql = "SELECT B.MA_TINH,B.TEN FROM TCS_DM_KHOBAC A, TCS_DM_cqthu B WHERE  A.shkb= B.shkb AND A.SHKB='" & strSHKB & "' and ma_hq is not null"
                'strSql = " SELECT B.MA_TINH,c.ten FROM TCS_DM_KHOBAC A, TCS_DM_cqthu B,tcs_dm_xa c WHERE  A.shkb= B.shkb and b.dbhc_tinh=c.ma_xa AND A.SHKB='" & strSHKB & "' and ma_hq is null" 'strSql = "SELECT B.MA_TINH,B.TEN FROM TCS_DM_KHOBAC A, TCS_DM_cqthu B WHERE  A.shkb= B.shkb AND A.SHKB='" & strSHKB & "' and ma_hq is null"
        End Select
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then

            Dim noOfItems As Integer = dt.Rows.Count
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim v_strTemp As String = String.Empty
                For j As Integer = 0 To dt.Columns.Count - 1
                    v_strTemp &= dt.Rows(i)(j).ToString & ";"
                Next
                v_strReturnMsg = v_strReturnMsg & v_strTemp.Substring(0, v_strTemp.LastIndexOf(";")) & "|"
            Next
            Return v_strReturnMsg
        End If
        Return v_strReturnMsg
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_DataByCQT(ByVal sMaCQT As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return Business_HQ.HaiQuan.HaiQuanController.GetDataByCQT_new(sMaCQT)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTT_NH_NHAN(ByVal strSHKB As String) As String
        Dim strResult As String = "null"
        Dim dt As New DataTable
        Dim strSql As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Try
            strSql = "Select MA_GIANTIEP, TEN_GIANTIEP, MA_TRUCTIEP, TEN_TRUCTIEP  From TCS_DM_NH_GIANTIEP_TRUCTIEP" & _
                " Where SHKB='" & strSHKB.Trim & "'"
            dt = DataAccess.ExecuteToTable(strSql)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString & ";" & dt.Rows(0)(2).ToString() & ";" & dt.Rows(0)(3).ToString
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi lấy trạng thái Ngân hàng nhận", HttpContext.Current.Session("TEN_DN"))
        Finally
            If Not dt Is Nothing Then
                dt.Dispose()
            End If
        End Try
        Return ""
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function ValidNHSHB_SP(ByVal strSHKB As String) As String
        Dim strResult As String = ""
        Dim dt As New DataTable
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        strResult = cls_songphuong.ValiadSHB(strSHKB)

        Dim strNHB As String = ""
        If strResult.Length > 8 Then
            Dim strarr() As String
            strarr = strResult.Split(";")
            strNHB = strarr(0).ToString
        End If

        Dim strSql As String = ""

        If strResult.Length > 0 Then
            strSql = "Select MA_GIANTIEP  From TCS_DM_NH_GIANTIEP_TRUCTIEP where shkb = '" & strSHKB & "' and ma_giantiep='" & strNHB & "'"
            dt = DataAccess.ExecuteToTable(strSql)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                Return dt.Rows(0)(0).ToString()
            Else
                Return ""
            End If
        End If

        Return strNHB
    End Function
    Private Function GetDefaultSHKB(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT b.shkb,b.ten FROM TCS_THAMSO a,TCS_DM_KHOBAC b WHERE a.giatri_ts=b.shkb And a.ten_ts='SHKB' and a.ma_dthu='" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString & ";" & dt.Rows(0)(1).ToString
        End If
        Return String.Empty
    End Function

    Private Function GetDefaultCQThu(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT A.MA_CQTHU, A.TEN FROM TCS_DM_CQTHU A, TCS_THAMSO B WHERE A.MA_CQTHU = B.GIATRI_TS AND A.MA_DTHU = B.MA_DTHU AND B.TEN_TS = 'CQ_THU' AND A.MA_DTHU='" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString
        End If
        Return String.Empty
    End Function

    Private Function GetDefaultMaNHNhan(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT b.shkb,b.ten FROM TCS_THAMSO a,TCS_DM_KHOBAC b WHERE a.giatri_ts=b.shkb And a.ten_ts='SHKB' and a.ma_dthu='" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        Dim v_strSHKB As String = String.Empty
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            v_strSHKB = dt.Rows(0)(0).ToString
        End If
        'strSQL = "SELECT A.MA, A.TEN FROM TCS_DM_NGANHANG A WHERE A.SHKB='" + v_strSHKB + "'"
        strSQL = "Select MA_GIANTIEP MA, TEN_GIANTIEP TEN, MA_TRUCTIEP, TEN_TRUCTIEP  From TCS_DM_NH_GIANTIEP_TRUCTIEP" & _
                " Where SHKB='" & v_strSHKB.Trim & "'"
        dt = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString() & ";" & dt.Rows(0)(2).ToString() & ";" & dt.Rows(0)(3).ToString()
        End If
        Return String.Empty

    End Function

    Private Function GetDefaultMaVangLai() As String
        If Load_DM_NNT_DB().Tables.Count > 0 Then
            Return Load_DM_NNT_DB().Tables(0).Rows(0)(0)
        Else
            Return String.Empty
        End If
    End Function
    Private Function Load_DM_NNT_DB() As DataSet
        Dim strSQL As String
        Try
            strSQL = "Select distinct MA_NNT as ID, TEN_NNT as Title " & _
                " From TCS_DM_NNT_KB " & _
                " Order By MA_NNT"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi load danh mục người nộp thuế", Session.Item("TEN_DN"))
            Return Nothing
        End Try
    End Function
    Private Function GetSoDong() As String
        Dim strSQL As String = "Select count(menh_gia) from tcs_dm_loaitien "
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        Return dt.Rows(0)(0).ToString
    End Function

    Private Shared Function GetBdsStatus() As String
        Dim strSQL As String
        Dim TTBDS As String
        Try
            strSQL = "Select TEN_TS,GIATRI_TS From TCS_THAMSO WHERE MA_DTHU='00' AND TEN_TS='MODE'"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If dt.Rows.Count > 0 Then
                TTBDS = dt.Rows(0)("GIATRI_TS").ToString.ToUpper
            Else
                TTBDS = ""
            End If
            Return TTBDS
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi lấy thông tin trạng thái BDS", HttpContext.Current.Session("TEN_DN"))
            Return String.Empty
        End Try
    End Function
    Private Function PopulateVarTimeout() As String

        Dim time_out As Integer = CInt(clsCTU.TCS_GETTS("TIME_OUT")) * 60 * 1000
        Dim app_root_path As String = ConfigurationManager.AppSettings.Get("APP_ROOT_PATH").ToString()


        Dim myJavaScript As StringBuilder = New StringBuilder()
        myJavaScript.Append(" var _timeout = " & time_out & ";")
        myJavaScript.Append(" var _rootPath = '" & app_root_path & "';")

        Return myJavaScript.ToString()
    End Function
    Private Function DM_NT() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT * from Tcs_dm_nguyente "
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrMaNT= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMaNT[" & i & "] ='" & dt.Rows(i)("Ma_NT") & ";" & dt.Rows(i)("TEN") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function DM_LOAITHUE() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT * from TCS_DM_LTHUE WHERE MA_LTHUE <> '04' ORDER BY MA_LTHUE "
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrMaLTHUE= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMaLTHUE[" & i & "] ='" & dt.Rows(i)("MA_LTHUE") & ";" & dt.Rows(i)("TEN") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function DM_MA_NH_A() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT ma_nh_chuyen,ten_nh_chuyen FROM tcs_nganhang_chuyen order by id"
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrMA_NHA= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMA_NHA[" & i & "] ='" & dt.Rows(i)("ma_nh_chuyen") & ";" & dt.Rows(i)("ten_nh_chuyen") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTK_KH_NH(ByVal pv_strTKNH As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If

        'Dim v_objCore As New CorebankServiceESB.clsCoreBank()
        Dim strReturn As String = ""
        Dim ma_loi As String = ""
        Dim so_du As String = ""
        Dim branch_tk As String = ""
        strReturn = cls_corebank.GetTK_KH_NH(pv_strTKNH.Replace("-", ""), ma_loi, so_du, branch_tk)

        Return strReturn

        Return ""
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTEN_KH_NH(ByVal pv_strTKNH As String) As String
        Dim strReturn As String = ""
        Dim ma_loi As String = ""
        Dim so_du As String = ""
        'If CORE_ON_OFF = "1" Then
        strReturn = clsCoreBank.GetTen_TK_KH_NH(pv_strTKNH.Replace("-", ""))
        'Else
        '    strReturn = "Tài khoản test"
        'End If
        Return strReturn
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Ghi_CTu(ByVal ctuHdr As Business.NewChungTu.infChungTuHDR, ByVal ctuDtl() As Business.NewChungTu.infChungTuDTL, _
                                    ByVal sAction As String, ByVal sSaveType As String, ByVal pv_strXML As String) As String
        Dim sMaNV As String = ""
        Dim sMa_DThu As String = ""
        Dim sRet As String = String.Empty
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sRet = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If

            sMaNV = HttpContext.Current.Session("User")

            If String.IsNullOrEmpty(sMaNV) Then
                sRet = "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
                Throw New Exception(sRet)
            End If

            If ctuHdr IsNot Nothing Then
                sRet = GhiChuyenKS(ctuHdr, ctuDtl, sSaveType, pv_strXML, sMaNV)

            End If

        Catch ex As Exception
            Select Case sAction
                Case "GHI"
                    Business.CTuCommon.WriteLog(ex, "Lỗi trong quá trình hoàn thiện chứng từ", HttpContext.Current.Session("TEN_DN"))
                    Throw ex
                Case "GHIKS"
                    Business.CTuCommon.WriteLog(ex, "Lỗi trong quá trình hoàn thiện & gửi kiểm soát chứng từ", HttpContext.Current.Session("TEN_DN"))
                    Throw ex
                Case "GHIDC"
                    Business.CTuCommon.WriteLog(ex, "Lỗi trong quá trình điều chỉnh & gửi kiểm soát chứng từ", HttpContext.Current.Session("TEN_DN"))
                    Throw ex
                Case Else
                    Throw ex
            End Select
        End Try

        Return sRet
    End Function

    Private Shared Function GhiChuyenKS(ByVal objHDR As Business.NewChungTu.infChungTuHDR, ByVal objDTL() As Business.NewChungTu.infChungTuDTL, ByVal mblnEdit As String, ByVal pv_strXML As String, Optional ByVal pv_intMaNV As Integer = 0) As String
        Dim Mota As String = ""
        Dim NewValue As String = ""
        Dim Action As String = "LAPCT_THUE_GHIKS_INSERT"
        Try
            Dim objUser As CTuUser = New CTuUser(HttpContext.Current.Session("User"))
            Dim macn As String = objUser.MA_CN

            Dim v_strReturn As String
            Dim sRet As String = ""
            'set nhung data con thieu truoc khi insert /update
            SetFullDataCTU(objHDR, objDTL, mblnEdit, pv_intMaNV)

            'XMLToBean(pv_strXML, objHDR, objDTL, pv_intMaNV)

            'If objHDR.PT_TT = "00" Then
            '    objHDR.TK_KH_NH = HttpContext.Current.Session.Item("UserName").ToString()
            'End If

            Dim key As New KeyCTu
            key.Ma_Dthu = objHDR.Ma_DThu
            key.Ma_NV = objHDR.Ma_NV
            key.Ngay_KB = objHDR.Ngay_KB
            key.SHKB = objHDR.SHKB
            key.So_BT = objHDR.So_BT
            key.Kyhieu_CT = objHDR.KyHieu_CT
            key.So_CT = objHDR.So_CT
            key.TT_BDS = objHDR.TT_BDS
            key.TT_NSTT = objHDR.TT_NSTT
            objHDR.Trang_Thai = "05"
            If key.Ngay_KB() <> GetDate(key.SHKB) And key.SHKB <> clsCTU.TCS_GetKHKB(pv_intMaNV) Then
                v_strReturn = "Ngày của kho bạc " & key.SHKB & " đã thay đổi. Không lập được chứng từ."
                Return v_strReturn
            End If


            'Nếu thông tin chi tiết không hợp lệ : --> Thoát
            Dim blnVND As Boolean = True
            If (objHDR.Ma_NT <> "VND") Then blnVND = False
            Dim intVitriLoi() As Integer = Valid_DTL(objDTL, True, blnVND, True)
            If (Not IsNothing(intVitriLoi)) Then
                If intVitriLoi.Length > 1 Then
                    Return intVitriLoi(1).ToString
                End If

            End If

            If Not mblnEdit.Equals("UPDATE_KS") Then
                If Not ChungTu.buChungTu.Insert_New(objHDR, objDTL, "0", , , , , macn.ToString()) Then
                    Mota = " object: " & objHDR.So_CT & " result: Fail  add_info: newvalue=""SO_CT[So_CT=" & objHDR.So_CT & "]"" Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:"
                    LogApp.AddDebug(Action, Mota)
                    v_strReturn = "Lỗi ghi và kiểm soát chứng từ"

                    Return v_strReturn
                Else
                    sRet = "Success;Hoàn thiện chứng từ thành công"
                End If
                Mota = " object: " & objHDR.So_CT & " result: Success  add_info: newvalue=""SO_CT[So_CT=" & objHDR.So_CT & "]"" Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:"
                LogApp.AddDebug(Action, Mota)


            Else            'Sửa chứng từ
                Action = "LAPCT_THUE_GHIKS_UPDATE"
                objHDR.SHKB = key.SHKB
                objHDR.Ngay_KB = key.Ngay_KB
                objHDR.Ma_NV = key.Ma_NV
                objHDR.So_BT = key.So_BT
                objHDR.Ma_DThu = key.Ma_Dthu
                'If objHDR.Trang_Thai = "00" Or objHDR.Trang_Thai = "03" Then
                If (Not Business.buChungTu.Update_New(objHDR, objDTL)) Then
                    Mota = " object: " & objHDR.So_CT & " result: Fail  add_info: newvalue=""SO_CT[So_CT=" & objHDR.So_CT & "]"" Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:"
                    LogApp.AddDebug(Action, Mota)
                    v_strReturn = "Cập nhật chứng từ không thành công"

                    Return v_strReturn
                Else
                    sRet = "Success;Cập nhật chứng từ thành công"
                End If
                '    Else
                '    sRet = ""
                '    Throw New Exception("Trạng thái chứng từ không cho phép cập nhật")
                '    Return sRet
                'End If

                Mota = " object: " & objHDR.So_CT & " result: Success  add_info: newvalue=""SO_CT[So_CT=" & objHDR.So_CT & "]"" Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:"
                LogApp.AddDebug(Action, Mota)
            End If


            TIME_BEGIN = DateTime.Now
            'Neu thanh cong tra ve so bt
            ' sRet = "Success;Hoàn thiện chứng từ thành công"
            Return sRet & ";" & key.SHKB & "/" & key.So_CT & "/" & key.So_BT & "/05"
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi ghi và chuyển kiểm soát chứng từ", HttpContext.Current.Session("TEN_DN"))
            Mota = " object: result: Success  Reason: Description: " & Date.Now.ToString("dd/MM/yyyy") & " Exception:"
            LogApp.AddDebug(Action, Mota)
            Return ex.ToString()
        End Try
    End Function

    Private Shared Sub XMLToBean(ByVal pv_strXML As String, ByRef hdr As NewChungTu.infChungTuHDR, _
                                 ByRef dtls As NewChungTu.infChungTuDTL(), Optional ByVal pv_intMaNV As Integer = 0)

        Try
            Dim strValue As String
            Dim dtl As NewChungTu.infChungTuDTL
            Dim dblTTien As Double = 0.0

            Dim v_xmlDocument As New XmlDocument
            Dim v_nodeCTU_HDR As XmlNodeList
            Dim v_nodeCTU_DTL As XmlNodeList
            Dim strXmlRoot As String = "/CTC_CTU/CTU_HDR/"

            v_xmlDocument.LoadXml(pv_strXML.Replace("&", "&amp;"))

            v_nodeCTU_HDR = v_xmlDocument.SelectNodes("/CTC_CTU/CTU_HDR")

            '1)Neu o trang thai edit
            If (mblnEdit) Then  'Nếu sửa chứng từ, lấy bộ key cũ
                ' Ngay KB
                hdr.Ngay_KB = clsCTU.TCS_GetNgayLV(pv_intMaNV)
                Dim str_nam_kb As String
                Dim str_thang_kb As String
                str_nam_kb = Mid(hdr.Ngay_KB, 3, 2)
                str_thang_kb = Mid(hdr.Ngay_KB, 5, 2)

                ' Mã NV
                hdr.Ma_NV = pv_intMaNV
                ' Số bút toán
                hdr.So_BT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.So_BT).InnerText
                ' Mã điểm thu
                'hdr.Ma_DThu = clsCTU.TCS_GetMaDT_KB(pv_intMaNV)
                ' Số CT
                hdr.So_CT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SoCT).InnerText
                ' Ky hieu CT
                'hdr.KyHieu_CT = clsCTU.TCS_GetKHCT(pv_intMaNV)

                hdr.TT_BDS = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TT_BDS).InnerText

                hdr.TT_NSTT = CTuCommon.GetNSTT(hdr.Ngay_KB, hdr.Ma_NV, hdr.So_CT, hdr.So_BT, hdr.Ma_DThu) 'v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TT_BDS).InnerText

                '2)Neu o trang thai insert moi
            Else                'Nếu nhập mới chứng từ, tạo bộ key mới
                ' Ngay KB
                hdr.Ngay_KB = clsCTU.TCS_GetNgayLV(pv_intMaNV)
                Dim str_nam_kb As String
                Dim str_thang_kb As String
                str_nam_kb = Mid(hdr.Ngay_KB, 3, 2)
                str_thang_kb = Mid(hdr.Ngay_KB, 5, 2)
                'Ma_CN = mv_objUser.MA_CN
                ' Mã NV
                hdr.Ma_NV = pv_intMaNV
                ' Số bút toán
                hdr.So_BT = CTuCommon.Get_SoBT(clsCTU.TCS_GetNgayLV(pv_intMaNV), hdr.Ma_NV)
                ' Mã điểm thu
                'hdr.Ma_DThu = clsCTU.TCS_GetMaDT(pv_intMaNV)
                ' Số CT
                hdr.So_CT = str_nam_kb + str_thang_kb + CTuCommon.Get_SoCT()
                ' Ky hieu CT
                '  hdr.KyHieu_CT = clsCTU.TCS_GetKHCT(pv_intMaNV)
                'Trang thai bds hien tai
                hdr.TT_BDS = gblnTTBDS

                hdr.TT_NSTT = "N"


            End If


            'Khoi tao phan HDR
            If Not v_nodeCTU_HDR Is Nothing Then
                'sonmt
                hdr.Dien_giai = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.DIEN_GIAI).InnerText
                'hdr.Ghi_chu = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.GHI_CHU).InnerText
                hdr.MA_HUYEN_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Huyen_NNTIEN).InnerText
                'hdr.TEN_HUYEN_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_HUYEN).InnerText
                hdr.MA_TINH_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_TINH).InnerText
                'hdr.TEN_TINH_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_TINH).InnerText

                hdr.SO_CMND = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SO_CMND).InnerText
                hdr.SO_FONE = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SO_FONE).InnerText

                'SHKB
                hdr.SHKB = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SHKB).InnerText
                ' Mã điểm thu
                hdr.Ma_DThu = hdr.SHKB
                ' Mã NNTien
                hdr.KyHieu_CT = clsCTU.TCS_GetKHCT(hdr.SHKB)
                hdr.Ma_NNTien = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaNNTien).InnerText

                ' Tên NNTien
                hdr.Ten_NNTien = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenNNTien).InnerText

                ' Địa chỉ NNTien
                hdr.DC_NNTien = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.DChiNNTien).InnerText

                ' Mã NNT
                hdr.Ma_NNThue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaNNT).InnerText

                ' Tên NNT
                hdr.Ten_NNThue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenNNT).InnerText
                ' Địa chỉ NNT
                hdr.DC_NNThue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.DChiNNT).InnerText
                ' Số bút toán FT
                hdr.So_CT_NH = "" 'v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SoCTNH).InnerText

                If (hdr.DC_NNThue = "") Then hdr.DC_NNThue = ""

                ' Lý do
                hdr.Ly_Do = ""
                ' Mã TQ
                hdr.Ma_TQ = 0

                'hdr.So_QD = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SO_QD).InnerText

                'Dim strValueNgayQD As String = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.NGAY_QD).InnerText
                'Dim strNgay_DK As String = strValueNgayQD
                'If (strValueNgayQD.Replace("/", "").Replace("-", "").Trim() <> "") Then
                '    hdr.Ngay_QD = ToValue(strValueNgayQD, "DATE")
                'Else
                '    hdr.Ngay_QD = "null"
                'End If

                'hdr.Ngay_QD = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.NGAY_QD).InnerText
                'hdr.CQ_QD = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.CQ_QD).InnerText

                hdr.Ngay_QD = ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") 'GetDBSysDate() '
                hdr.CQ_QD = ""

                ' Ngay CT
                hdr.Ngay_CT = clsCTU.TCS_GetNgayLV(pv_intMaNV)
                ' Ngay HT
                hdr.Ngay_HT = ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") 'GetDBSysDate() '

                hdr.Ma_CQThu = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaCQThu).InnerText
                'THEM MA SAN PHAM
                hdr.MA_NH_TT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_TT).InnerText
                hdr.SAN_PHAM = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_SANPHAM).InnerText
                If hdr.SAN_PHAM.Length > 10 Then
                    hdr.SAN_PHAM = ""
                End If

                hdr.TT_CITAD = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TT_CITAD).InnerText
                hdr.MA_NTK = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NTK).InnerText
                ' DBHC
                hdr.Ma_Xa = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaDBHC).InnerText
                hdr.XA_ID = Get_XaID(hdr.Ma_Xa)

                hdr.Ma_Huyen = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_HUYEN).InnerText

                hdr.TEN_HUYEN_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_HUYEN).InnerText

                hdr.Ma_Tinh = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_TINH).InnerText

                hdr.TEN_HUYEN_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_TINH).InnerText


                ' TK No
                hdr.TK_No = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TKNo).InnerText.Replace(".", "").Replace(" ", "")
                hdr.TEN_TK_NO = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenTKNo).InnerText
                Dim v_strSHKB As String = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SHKB).InnerText
                ' Dim v_strMaDBHC As String = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaDBHC).InnerText

                'Dim arrTMP As ArrayList = CTuCommon.MapTK_Load(v_strMaDBHC, v_strSHKB, clsCTU.TCS_GetDBThu(pv_intMaNV), clsCTU.TCS_GetMaNHKB(pv_intMaNV))
                'Dim v_strMaHTTT As String = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.HTTT).InnerText

                'If v_strMaHTTT = "01" Then 'Chuyen khoan
                hdr.TK_GL_NH = "NULL"
                'Else
                '    If arrTMP.Count > 0 Then
                '        hdr.TK_GL_NH = arrTMP(3).ToString().Trim()
                '    End If
                'End If

                'If arrTMP.Count > 0 Then
                '    hdr.TK_KB_NH = arrTMP(4).ToString().Trim()
                'Else
                '    hdr.TK_KB_NH = ""
                'End If

                ' TK Co
                hdr.TK_Co = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TKCo).InnerText
                hdr.TEN_TK_CO = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenTKCo).InnerText
                ' So TKhai
                hdr.So_TK = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.ToKhaiSo).InnerText

                ' Ngay TK
                strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.NgayDK).InnerText
                If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
                    hdr.Ngay_TK = ToValue(strValue, "DATE")
                Else
                    hdr.Ngay_TK = "null"
                End If

                ' LHXNK
                If (v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.LHXNK).InnerText.Length > 0) Then
                    Dim strTenVT, strTenLH, strMaLH, strTen As String
                    strTenVT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.LHXNK).InnerText
                    strTen = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.DescLHXNK).InnerText

                    If (strTenVT = "" Or strTen = "") Then
                        hdr.LH_XNK = ""
                    Else
                        'Dim s As String()
                        's = strTen.Split("-")
                        strMaLH = strTenVT
                        strTenLH = strTen

                        If (Valid_LHXNK(strMaLH)) Then
                            hdr.LH_XNK = strMaLH
                        Else
                            CTuCommon.Get_LHXNK_byTenVT(strTenVT, strMaLH, strTenLH)
                            hdr.LH_XNK = strMaLH
                        End If

                        If (hdr.LH_XNK.Length > 20) Then
                            hdr.LH_XNK = hdr.LH_XNK.Substring(0, 20)
                        End If
                    End If
                Else
                    hdr.LH_XNK = ""
                End If

                ' DVSDNS
                hdr.DVSDNS = ""
                hdr.Ten_DVSDNS = ""

                ' Ma_NT
                'hdr.Ma_NT = "VND"
                'hdr.Ma_NT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaNT).InnerText
                ' Ti_gia
                'strValue = 'Trim(CType(grdHeader.Rows(RowHeader.TiGia).Cells.Item(1).Controls(0), TextBox).Text)
                ' TG_ID
                hdr.TG_ID = "1062"

                hdr.Ty_Gia = "1"
                'hdr.Ty_Gia = get_TyGia(hdr.Ma_NT, hdr.TG_ID)
                hdr.Ma_NT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaNT).InnerText
                'hdr.Ty_Gia = ChungTu.buChungTu.get_tygia(hdr.Ma_NT, hdr.TG_ID)
                ' Mã Loại Thuế
                hdr.Ma_LThue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.LoaiThue).InnerText

                ' So Khung - So May
                'hdr.So_Khung = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SoKhung).InnerText
                'hdr.So_May = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SoMay).InnerText
                'Phuong thuc thanh toan
                hdr.PT_TT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.HTTT).InnerText
                'hdr.TT_TThu = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TTIEN_TN).InnerText
                If Not gblnTTBDS Is Nothing Then
                    If gblnTTBDS.ToUpper = "O" Then
                        hdr.Mode = "1"
                    Else 'If gblnTTBDS.ToUpper = "F" Then
                        hdr.Mode = "0"
                    End If
                Else
                    hdr.Mode = "0"
                End If


                ' TK_KH_KB
                'If Get_MaNH(hdr.Ma_Xa, clsCTU.TCS_GetMaDT_KB(hdr.SHKB), clsCTU.TCS_GetKHKB(pv_intMaNV)).Rows.Count > 0 Then
                '    hdr.MA_NH_A = Get_MaNH(hdr.Ma_Xa, clsCTU.TCS_GetMaDT_KB(hdr.SHKB), clsCTU.TCS_GetKHKB(pv_intMaNV)).Rows(0).Item("ma_nh_a").ToString
                '    hdr.MA_NH_B = Get_MaNH(hdr.Ma_Xa, clsCTU.TCS_GetMaDT_KB(hdr.SHKB), clsCTU.TCS_GetKHKB(pv_intMaNV)).Rows(0).Item("ma_nh_b").ToString
                'Else
                '    hdr.MA_NH_A = ""
                '    hdr.MA_NH_B = ""
                'End If
                'KIENVT : THEM THONG TIN VE NGAN HANG
                hdr.MA_NH_A = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_A).InnerText

                ' hdr.MA_NH_B = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_B).InnerText
                'hdr.MA_NH_TT = v_nodeCTU_HDR.Item(0).SelectSingleNode(strXmlRoot + clsCTU.ColHDR.MA_NH_TT.ToString()).InnerText
                ' hoapt sua ma ngan hang truc tiep gian tiep
                Dim strMaNH As String = GetTT_NH_NHAN(v_strSHKB)
                hdr.MA_NH_B = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_B).InnerText 'strMaNH.Split(";")(0).ToString
                hdr.MA_NH_TT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_TT).InnerText 'strMaNH.Split(";")(2).ToString
                hdr.Ten_NH_B = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ten_NH_B).InnerText
                hdr.TEN_NH_TT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_NH_TT).InnerText
                ' TK_KH_NH 
                hdr.TK_KH_NH = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TK_KH_NH).InnerText.Replace("-", "")
                hdr.TEN_KH_NH = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenTK_KH_NH).InnerText

                'Ngay_KH_NH
                strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.NGAY_KH_NH).InnerText
                hdr.NGAY_KH_NH = ToValue(strValue, "DATE")

                hdr.TK_KH_Nhan = ""
                hdr.Ten_KH_Nhan = ""
                hdr.Diachi_KH_Nhan = ""

                'THEM PHI GIAO DỊCH,VAT

                hdr.PHI_GD = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_GD).InnerText.Replace(".", "")
                Dim num As Integer
                If Integer.TryParse(hdr.PHI_GD, num) Then
                    hdr.PHI_GD = Integer.Parse(hdr.PHI_GD)
                Else
                    hdr.PHI_GD = "0"
                End If

                hdr.PHI_VAT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_VAT).InnerText.Replace(".", "")
                If hdr.PHI_VAT = "" Then
                    hdr.PHI_VAT = "0"
                End If

                hdr.PT_TINHPHI = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PT_TINHPHI).InnerText.Replace(".", "")

                ' quận_huyên NNTien
                hdr.QUAN_HUYEN_NNTIEN = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Huyen_NNTIEN).InnerText
                ' Tinh _NNTien
                hdr.TINH_TPHO_NNTIEN = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Tinh_NNTIEN).InnerText
                Dim ref_no As String = String.Empty

                hdr.RM_REF_NO = hdr.So_CT
                hdr.REF_NO = hdr.So_CT

                ' So BK
                hdr.So_BK = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.So_BK).InnerText

                ' Ngay BK
                If (v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ngay_BK).InnerText.Length > 0) Then
                    strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ngay_BK).InnerText
                    If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
                        If strValue = "0" Then
                            hdr.Ngay_BK = ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") 'GetDBSysDate() '
                        Else
                            hdr.Ngay_BK = ToValue(strValue, "DATE")
                        End If
                    End If
                Else
                    hdr.Ngay_BK = ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") 'GetDBSysDate() '
                End If

                hdr.Lan_In = "0" '?????

                hdr.Ma_KS = 0

                hdr.Trang_Thai = "00" '????

                'sonmt
                'hdr.LOAI_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.LOAI_NNT).InnerText
                'hdr.TEN_LOAI_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_LOAI_NNT).InnerText
            End If

            v_nodeCTU_DTL = v_xmlDocument.SelectNodes("/CTC_CTU/CTU_DTL/ITEMS")
            Dim k As Integer = 0

            'Khoi tao phan DTL
            If Not v_nodeCTU_DTL Is Nothing Then
                ReDim Preserve dtls(v_nodeCTU_DTL.Count - 1)
                For i As Integer = 0 To v_nodeCTU_DTL.Count - 1 'v_nodeCTU_DTL.Item(0).ChildNodes.Count - 1
                    dtl = New NewChungTu.infChungTuDTL
                    'For j As Integer = 0 To v_nodeCTU_DTL.Item(i).ChildNodes.Count

                    'So tu tang
                    dtl.ID = getDataKey("TCS_CTU_DTL_SEQ.NEXTVAL")

                    ' SHKB
                    dtl.SHKB = hdr.SHKB

                    ' Ngày KB
                    dtl.Ngay_KB = hdr.Ngay_KB

                    ' Mã NV
                    dtl.Ma_NV = hdr.Ma_NV

                    ' Số Bút toán
                    dtl.So_BT = hdr.So_BT

                    ' Mã điểm thu
                    dtl.Ma_DThu = hdr.Ma_DThu

                    'Mã quỹ
                    dtl.MaQuy = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.MaQuy).InnerText

                    ' Mã Cấp
                    dtl.Ma_Cap = ""

                    ' Mã Chương
                    dtl.Ma_Chuong = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.CChuong).InnerText

                    ' CCH_ID
                    dtl.CCH_ID = Get_CCHID(dtl.Ma_Chuong)

                    ' Mã Loại
                    dtl.Ma_Loai = ""

                    ' Mã Khoản
                    dtl.Ma_Khoan = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.LKhoan).InnerText

                    ' CCH_ID
                    dtl.LKH_ID = Get_LKHID(dtl.Ma_Khoan)

                    ' Mã Mục
                    dtl.Ma_Muc = ""

                    ' Mã TMục
                    dtl.Ma_TMuc = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.MTieuMuc).InnerText

                    ' MTM_ID
                    dtl.MTM_ID = Get_MTMucID(dtl.Ma_TMuc)

                    ' Mã TLDT
                    dtl.Ma_TLDT = "null"
                    ' DT_ID
                    dtl.DT_ID = "null"

                    '  Nội dung
                    dtl.Noi_Dung = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.NoiDung).InnerText

                    ' Tiền nguyên tệ
                    strValue = ""
                    If "".Equals(strValue) Then
                        strValue = "0"
                    End If
                    dtl.SoTien_NT = CDbl(strValue.Replace(" ", ""))
                    dtl.SoTien = StoN(v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.Tien).InnerText.Replace(".", ""))
                    'If hdr.Ma_NT.Equals("VND") Then

                    'Else
                    '    dtl.SoTien_NT = StoN(v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.Tien).InnerText.Replace(".", ""))
                    '    dtl.SoTien = dtl.SoTien_NT * hdr.Ty_Gia
                    'End If
                    ' Tiền 

                    dblTTien += dtl.SoTien

                    ' Kỳ thuế
                    dtl.Ky_Thue = RemoveApostrophe(v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.KyThue).InnerText)

                    ' Mã dự phòng
                    dtl.Ma_DP = "null"

                    'Next
                    If dtl.SoTien <> 0 Then
                        dtls(k) = dtl 'Kienvt : Chi insert nhung ban ghi co so tien <> 0
                    End If
                    k += 1
                Next
            End If

            ' Tiền thực thu
            hdr.TTien = dblTTien

            If (hdr.Ma_NT.ToUpper() <> "VND") Then
                hdr.TTien_NT = hdr.TTien / hdr.Ty_Gia
            Else
                hdr.TTien_NT = hdr.TTien
            End If

            ''Nếu không nhập số thực thu (chuyển khoản)
            'If (Not clsCTU.mblnNhapThucThu) Then
            '    hdr.Ma_TQ = hdr.Ma_NV
            '    hdr.TT_TThu = 0
            'End If
        Catch ex As Exception
            LogApp.WriteLog("XMLToBean", ex, "Lỗi fill XmlToBean", HttpContext.Current.Session("TEN_DN"))
            'clsCommon.WriteLog(ex, "Lỗi fill XmlToBean", HttpContext.Current.Session("TEN_DN"))
            Throw ex
        End Try
    End Sub

    Private Shared Sub SetFullDataCTU(ByRef hdr As NewChungTu.infChungTuHDR, _
                                ByRef dtls As NewChungTu.infChungTuDTL(), ByVal mblnEdit As String, Optional ByVal pv_intMaNV As Integer = 0)

        Try
            Dim strValue As String
            Dim dtl As NewChungTu.infChungTuDTL
            Dim dblTTien As Double = 0.0

            '1)Neu o trang thai edit
            If (mblnEdit.Equals("UPDATE_KS")) Then  'Nếu sửa chứng từ, lấy bộ key cũ
                ' Ngay KB
                hdr.Ngay_KB = clsCTU.TCS_GetNgayLV(pv_intMaNV)
                Dim str_nam_kb As String
                Dim str_thang_kb As String
                str_nam_kb = Mid(hdr.Ngay_KB, 3, 2)
                str_thang_kb = Mid(hdr.Ngay_KB, 5, 2)
                hdr.MA_CN = HttpContext.Current.Session("MA_CN_USER_LOGON")
                ' Mã NV
                hdr.Ma_NV = pv_intMaNV

                hdr.TT_NSTT = CTuCommon.GetNSTT(hdr.Ngay_KB, hdr.Ma_NV, hdr.So_CT, hdr.So_BT, hdr.Ma_DThu)

                '2)Neu o trang thai insert moi
            Else                'Nếu nhập mới chứng từ, tạo bộ key mới
                ' Ngay KB
                hdr.Ngay_KB = clsCTU.TCS_GetNgayLV(pv_intMaNV)
                Dim str_nam_kb As String
                Dim str_thang_kb As String
                str_nam_kb = Mid(hdr.Ngay_KB, 3, 2)
                str_thang_kb = Mid(hdr.Ngay_KB, 5, 2)
                hdr.MA_CN = HttpContext.Current.Session("MA_CN_USER_LOGON")
                ' Mã NV
                hdr.Ma_NV = pv_intMaNV
                ' Số bút toán
                hdr.So_BT = CTuCommon.Get_SoBT(clsCTU.TCS_GetNgayLV(pv_intMaNV), hdr.Ma_NV)
                ' Mã điểm thu
                'hdr.Ma_DThu = clsCTU.TCS_GetMaDT(pv_intMaNV)
                ' Số CT
                hdr.So_CT = str_nam_kb + str_thang_kb + CTuCommon.Get_SoCT()
                'hdr.REF_CORE_TTSP = str_nam_kb + str_thang_kb + CTuCommon.Get_SoRefCore_TTSP()
                ' Ky hieu CT
                '  hdr.KyHieu_CT = clsCTU.TCS_GetKHCT(pv_intMaNV)
                'Trang thai bds hien tai
                hdr.TT_BDS = ""

                hdr.TT_NSTT = "N"
            End If


            'Khoi tao phan HDR
            If Not hdr Is Nothing Then

                hdr.Ma_DThu = hdr.SHKB
                ' Mã NNTien
                hdr.KyHieu_CT = clsCTU.TCS_GetKHCT(hdr.SHKB)
                ' Số bút toán FT
                hdr.So_CT_NH = "" 'v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SoCTNH).InnerText
                ' hdr.Ten_NH_B = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ten_NH_B).InnerText
                If (hdr.DC_NNThue = "") Then hdr.DC_NNThue = ""
                ' Mã TQ
                hdr.Ma_TQ = 0
                strValue = hdr.Ngay_QD
                If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
                    hdr.Ngay_QD = Business.Common.mdlCommon.ToValue(strValue, "DATE")
                Else
                    hdr.Ngay_QD = "null"
                End If
                ' Ngay CT
                hdr.Ngay_CT = clsCTU.TCS_GetNgayLV(pv_intMaNV)
                ' Ngay HT
                hdr.Ngay_HT = Business.Common.mdlCommon.ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") 'GetDBSysDate() '

                ' So BK
                hdr.So_BK = hdr.So_BK

                hdr.TT_CITAD = "0"
                ' DBHC
                hdr.XA_ID = Business.Common.mdlCommon.Get_XaID(hdr.Ma_Xa)
                hdr.Ma_Huyen = ""

                Dim v_strSHKB As String = hdr.SHKB
                Dim v_strMaDBHC As String = hdr.Ma_Xa


                'If v_strMaHTTT = "01" Then 'Chuyen khoan
                hdr.TK_GL_NH = "NULL"

                '
                strValue = hdr.NGAY_KH_NH
                hdr.NGAY_KH_NH = Business.Common.mdlCommon.ToValue(strValue, "DATE")

                ' Ngay TK
                strValue = hdr.Ngay_TK
                If Not String.IsNullOrEmpty(strValue) Then
                    If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
                        hdr.Ngay_TK = Business.Common.mdlCommon.ToValue(strValue, "DATE")
                    Else
                        hdr.Ngay_TK = "null"
                    End If
                Else
                    hdr.Ngay_TK = "null"
                End If


                ' DVSDNS
                hdr.DVSDNS = ""
                hdr.Ten_DVSDNS = ""

                hdr.Ty_Gia = ChungTu.buChungTu.get_tygia(hdr.Ma_NT, hdr.TG_ID)

                hdr.MA_NH_B = hdr.MA_NH_B
                ' TK_KH_NH 
                If hdr.PT_TT = "01" Then
                    hdr.TK_KH_NH = hdr.TK_KH_NH
                    hdr.TEN_KH_NH = hdr.TEN_KH_NH
                ElseIf hdr.PT_TT = "00" Then
                    hdr.TK_GL_NH = hdr.TK_KH_NH
                    'hdr.TK_KH_NH = Business.Common.mdlCommon.Get_TellerNV(hdr.Ma_NV)
                    'hdr.TEN_KH_NH = Business.Common.mdlCommon.Get_TellerNV(hdr.Ma_NV)
                ElseIf hdr.PT_TT = "05" Then
                    hdr.TK_KH_NH = hdr.TK_KH_NH
                    hdr.TEN_KH_NH = ""
                End If
                '' PTTT dựa theo số tk
                'If (IsNumeric(hdr.TK_KH_NH) = False) Then
                '    hdr.PT_TT = "03"
                'Else
                '    hdr.PT_TT = "01"
                'End If
                'Ngay_KH_NH
                'strValue = hdr.Ngay_CT
                'hdr.NGAY_KH_NH = Business.Common.mdlCommon.ToValue(strValue, "DATE")

                hdr.TK_KH_Nhan = ""
                hdr.Ten_KH_Nhan = ""
                hdr.Diachi_KH_Nhan = ""

                Dim ref_no As String = String.Empty

                hdr.RM_REF_NO = hdr.So_CT
                hdr.REF_NO = hdr.So_CT


                hdr.TIME_BEGIN = Business.Common.mdlCommon.ToValue(TIME_BEGIN.ToString("dd/MM/yyyy HH:mm:ss"), "DATE")
                ' Ngay BK
                hdr.Ngay_BK = "null"
                'If (v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ngay_BK).InnerText.Length > 0) Then
                '    strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ngay_BK).InnerText
                '    If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
                '        hdr.Ngay_BK = Business.Common.mdlCommon.ToValue(strValue, "DATE")
                '    Else
                '        hdr.Ngay_BK = "null"
                '    End If
                'Else
                '    hdr.Ngay_BK = "null"
                'End If

                hdr.Lan_In = "0" '?????

                hdr.Ma_KS = 0

                hdr.Trang_Thai = "00" '????

                hdr.Kenh_CT = "00"
                If hdr.Ma_LThue.Equals("02") Then
                    'thuế cá nhân
                    hdr.Kenh_CT = "10"
                ElseIf hdr.Ma_LThue.Equals("03") Then
                    'thuế tb ô tô xe máy
                    hdr.Kenh_CT = "11"
                ElseIf hdr.Ma_LThue.Equals("08") Then
                    'thuế trước bạ nhà đất
                    hdr.Kenh_CT = "12"
                End If

            End If

            Dim k As Integer = 0

            'Khoi tao phan DTL
            If Not dtls Is Nothing Then
                For i As Integer = 0 To dtls.Count - 1
                    'dtl = New NewChungTu.infChungTuDTL
                    dtls(i).SO_CT = hdr.So_CT
                    'So tu tang
                    dtls(i).ID = Business.Common.mdlCommon.getDataKey("TCS_CTU_DTL_SEQ.NEXTVAL")

                    ' SHKB
                    dtls(i).SHKB = hdr.SHKB

                    ' Ngày KB
                    dtls(i).Ngay_KB = hdr.Ngay_KB

                    ' Mã NV
                    dtls(i).Ma_NV = hdr.Ma_NV

                    ' Số Bút toán
                    dtls(i).So_BT = hdr.So_BT

                    ' Mã điểm thu
                    dtls(i).Ma_DThu = hdr.Ma_DThu

                    'Mã quỹ
                    ' dtl.MaQuy = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.MaQuy).InnerText

                    ' Mã Cấp
                    dtls(i).Ma_Cap = ""

                    ' CCH_ID
                    dtls(i).CCH_ID = Business.Common.mdlCommon.Get_CCHID(dtls(i).Ma_Chuong)

                    ' Mã Loại
                    dtls(i).Ma_Loai = ""

                    ' Mã Khoản
                    'dtl.Ma_Khoan = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.LKhoan).InnerText

                    ' CCH_ID
                    dtls(i).LKH_ID = Business.Common.mdlCommon.Get_LKHID(dtls(i).Ma_Khoan)

                    ' Mã Mục
                    dtls(i).Ma_Muc = ""

                    ' Mã TMục
                    'dtl.Ma_TMuc = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.MTieuMuc).InnerText

                    ' MTM_ID
                    dtls(i).MTM_ID = Business.Common.mdlCommon.Get_MTMucID(dtls(i).Ma_TMuc)

                    ' Mã TLDT
                    dtls(i).Ma_TLDT = "null"
                    ' DT_ID
                    dtls(i).DT_ID = "null"



                    ' Tiền nguyên tệ
                    strValue = ""
                    If "".Equals(strValue) Then
                        strValue = "0"
                    End If
                    dtls(i).SoTien_NT = CDbl(strValue.Replace(" ", ""))
                    dtls(i).SoTien = Business.Common.mdlCommon.StoN(dtls(i).SoTien)
                    'If hdr.Ma_NT.Equals("VND") Then

                    'Else
                    '    dtl.SoTien_NT = StoN(v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.Tien).InnerText.Replace(".", ""))
                    '    dtl.SoTien = dtl.SoTien_NT * hdr.Ty_Gia
                    'End If
                    ' Tiền 

                    dblTTien += dtls(i).SoTien

                    ' Kỳ thuế
                    dtls(i).Ky_Thue = Business.Common.mdlCommon.RemoveApostrophe(dtls(i).Ky_Thue)

                    ' Mã dự phòng
                    dtls(i).Ma_DP = "null"


                    k += 1
                Next
            End If

            ' Tiền thực thu
            hdr.TTien = dblTTien

            If (hdr.Ma_NT.ToUpper() <> "VND") Then
                hdr.TTien_NT = hdr.TTien / hdr.Ty_Gia
            Else
                hdr.TTien_NT = hdr.TTien
            End If


        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi fill SetFullDataCTU", HttpContext.Current.Session("TEN_DN"))
            Throw ex
        End Try
    End Sub
    Private Shared Function GetDate(ByVal strSHKB As String) As Long
        Dim strSQL As String
        Dim dt As DataTable
        strSQL = " SELECT TO_NUMBER (TO_CHAR (TO_DATE (giatri_ts, 'DD/MM/YYYY'), 'YYYYMMDD')) FROM tcs_thamso "
        strSQL += " WHERE ten_ts = 'NGAY_LV'"
        dt = DataAccess.ExecuteToTable(strSQL)
        If Not dt Is Nothing Then
            Return dt.Rows(0)(0)
        End If
    End Function
#Region "Valid _DTL"

    Private Shared Function Valid_DTL(ByVal dtl As NewChungTu.infChungTuDTL, _
                ByVal fblnBatBuocMLNS As Boolean, _
                ByVal fblnVND As Boolean, _
                ByVal fblnBatBuocTLDT As Boolean) As Integer
        Try
            Dim v_intRet As Integer = -1

            If (fblnBatBuocMLNS = True) Then
                'If (Not Valid_MaQuy(dtl.MaQuy, True)) Then
                '    'lblStatus.Text = "Thông tin 'Mã quỹ' không hợp lệ"
                '    If MsgBox("Mã quỹ không đúng. Bạn có muốn tiếp tục không?", MsgBoxStyle.OkCancel, "Thông báo") = MsgBoxResult.Ok Then
                '        ' execute command
                '        Return True
                '    Else
                '        Return clsCTU.ColDTL.MaQuy
                '    End If

                'End If
                If (Not Business.Common.mdlCommon.Valid_Chuong(dtl.Ma_Chuong, True)) Then
                    'lblStatus.Text = "Thông tin 'Chương' không hợp lệ"
                    If MsgBox("Mã Chương không đúng", MsgBoxStyle.OkCancel, "Thông báo") = MsgBoxResult.Ok Then
                        ' execute command
                        Return True
                    Else
                        Return clsCTU.ColDTL.CChuong
                    End If
                End If
                'If (Not Valid_Khoan(dtl.Ma_Khoan, True)) Then
                '    If MsgBox("Mã Ngành kinh tế không đúng. Bạn có muốn tiếp tục không?", MsgBoxStyle.OkCancel, "Thông báo") = MsgBoxResult.Ok Then
                '        ' execute command
                '        Return True
                '    Else
                '        Return clsCTU.ColDTL.LKhoan
                '    End If
                '    'lblStatus.Text = "Thông tin 'Ngành kinh tế' không hợp lệ"

                'End If
                If (Not Business.Common.mdlCommon.Valid_TMuc(dtl.Ma_TMuc, True)) Then
                    'lblStatus.Text = "Thông tin 'Nội dung kinh tế' không hợp lệ"
                    If MsgBox("Thông tin nội dung kinh tế không đúng. Bạn có muốn tiếp tục không?", MsgBoxStyle.OkCancel, "Thông báo") = MsgBoxResult.Ok Then
                        ' execute command
                        Return True
                    Else
                        Return clsCTU.ColDTL.MTieuMuc
                    End If

                End If
                'If ((Not Valid_TLDT(dtl.Ma_TLDT, True)) And (fblnBatBuocTLDT = True)) Then
                '    lblStatus.Text = "Thông tin Tỷ lệ phân chia không hợp lệ"
                '    Return ColDTL.TLDT
                'End If
            End If

            If (dtl.SoTien = 0) Then
                'lblStatus.Text = "Số tiền không được để 0"
                If (fblnVND) Then
                    Return clsCTU.ColDTL.Tien
                    'Else
                    'Return ColDTL.TienNT
                End If
            End If
            'If (Not Valid_KyThue(dtl.Ky_Thue)) Then
            '    'lblStatus.Text = "Thông tin Kỳ thuế không hợp lệ (MM/yyyy)"
            '    Return clsCTU.ColDTL.KyThue
            'End If
            Return -1
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi valid dữ liệu dtl", HttpContext.Current.Session("TEN_DN"))
            Return -1
        End Try
    End Function

    Private Shared Function Valid_DTL(ByVal dtl() As NewChungTu.infChungTuDTL, _
            ByVal fblnBatBuocMLNS As Boolean, _
            ByVal fblnVND As Boolean, _
            ByVal fblnBatBuocTLDT As Boolean) As Integer()
        Dim intReturn(1) As Integer
        Try
            'Dim strMaQuy As String = ""
            Dim i, iCol As Integer
            For i = 0 To dtl.Length - 1
                iCol = Valid_DTL(dtl(i), fblnBatBuocMLNS, fblnVND, fblnBatBuocTLDT)
                If (iCol <> -1) Then
                    intReturn(0) = i
                    intReturn(1) = iCol
                    Return intReturn
                End If

                'If (strMaQuy <> "") And (strMaQuy <> dtl(i).MaQuy) Then
                '    'lblStatus.Text = "Chỉ cho phép 1 mã quỹ trên 1 chứng từ"
                '    intReturn(0) = i
                '    intReturn(1) = clsCTU.ColDTL.MaQuy
                '    Return intReturn
                'End If

                'strMaQuy = dtl(i).MaQuy
            Next
            Return Nothing
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Lỗi valid dữ liệu dtl", HttpContext.Current.Session("TEN_DN"))
            Return Nothing
        End Try
    End Function
#End Region

    <System.Web.Services.WebMethod()> _
    Public Shared Function SearchDanhMuc(ByVal strMa_DThu As String, ByVal strMa_DBHC As String, ByVal strPage As String, ByVal strKey As String, ByVal strTenDM As String, ByVal strSHKB As String) As String
        Try
            Dim strResult As String = ""
            Dim dt As DataTable
            Dim strMaDanhMuc As String = strKey.Trim
            Dim strTenDanhMuc As String = strTenDM
            Dim lovDM As New Business.lovDanhMuc(strMa_DThu, strSHKB)
            dt = lovDM.ShowDanhMuc(strPage, strMaDanhMuc, strTenDanhMuc, "", "", strMa_DBHC)

            If Not Globals.IsNullOrEmpty(dt) Then
                Dim strID, strTitle As String
                Dim i As Integer = 0
                For i = 0 To dt.Rows.Count - 1
                    If i <= 900 Then
                        strID = dt.Rows(i)(0).ToString
                        strTitle = dt.Rows(i)(1).ToString
                        strResult += strID & ";" & strTitle.Replace("'", " ") & "|"
                    Else
                        Exit For
                    End If
                Next
            Else
                strResult = ""
            End If

            Return strResult
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            '  Throw ex
            Return String.Empty
        End Try
    End Function

End Class
