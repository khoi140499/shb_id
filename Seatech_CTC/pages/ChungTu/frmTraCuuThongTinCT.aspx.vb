﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Imports VBOracleLib
Imports ProcMegService
Partial Class pages_ChungTu_frmTraCuuThongTinCT
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                LoadForm()
            Catch ex As Exception

            End Try
        End If
    End Sub
    Protected Sub LoadForm()
        txtTuNgay.Value = DateTime.Now.ToString("dd/MM/yyyy")
        txtDenNgay.Value = DateTime.Now.ToString("dd/MM/yyyy")
    End Sub
    Protected Sub LoadParam()
        txtTuNgay.Value = DateTime.Now.ToString("dd/MM/yyyy")
        txtDenNgay.Value = DateTime.Now.ToString("dd/MM/yyyy")
    End Sub
    Protected Sub LoadTT_Ctu_Thue()
         
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim pv_TuNgay As String = txtTuNgay.Value.Trim.ToString()
        Dim pv_DenNgay As String = txtDenNgay.Value.Trim.ToString()
        Dim pv_MST As String = txtMST.Text.Trim.ToString()
        Dim pv_MST_NNT As String = txtMST_NNT.Text.Trim.ToString()
        Dim pv_SO_CT As String = txtSO_CT.Text.Trim.ToString()
        Dim pv_errCODE As String = ""
        Dim pv_errMSG As String = ""
        If pv_MST = "" Then
            lblError.Text = "Bạn phải nhập vào thông tin Mã số thuế."
            Exit Sub
        End If
        Dim pv_TrangThai As String = ddlTrangThai.SelectedValue.ToString()
        Try
            ProcMegService.ProMsg.getMSG22(pv_MST, pv_MST_NNT, pv_SO_CT, pv_TuNgay, pv_DenNgay, pv_TrangThai)
            loadGridView()
            lblError.Text = "Tìm thấy thông tin Thuế cho MST : " & pv_MST
        Catch ex As Exception
            lblError.Text = "Lỗi trong quá trình tra cứu thông tin Thuế : Error Messages = " & ex.Message.ToString
            dtgdata.DataSource = Nothing
            dtgdata.DataBind()
        End Try
    End Sub
    Protected Sub loadGridView()
        Dim pv_TuNgay As String = txtTuNgay.Value.Trim.ToString()
        Dim pv_DenNgay As String = txtDenNgay.Value.Trim.ToString()
        Dim pv_MST As String = txtMST.Text.Trim.ToString()
        Dim pv_MST_NNT As String = txtMST_NNT.Text.Trim.ToString()
        Dim pv_SO_CT As String = txtSO_CT.Text.Trim.ToString()
        Dim ds As DataSet
        Dim strSql As String = "SELECT hdr.so_ct, TO_CHAR(hdr.ngay_ct,'dd/MM/yyyy') as ngay_ct, hdr.ma_nntien, hdr.ten_nntien, hdr.dc_nntien, hdr.ma_nnthue, " & _
                                " hdr.ten_nnthue, hdr.ma_cqthu, hdr.tk_co, hdr.ma_nh_a, hdr.ma_nh_b,hdr.ma_ntk ,sum(dtl.sotien) as ttien " & _
                                " FROM tcs_ctu_hdr_thue hdr,tcs_ctu_dtl_thue dtl " & _
                                " WHERE       hdr.so_ct=dtl.so_ct "
        Dim WhereClause As String = ""
        Try
            If pv_MST <> "" Then
                WhereClause += " AND ma_nnthue = '" + pv_MST + "'"
            End If
            If pv_TuNgay <> "" Then
                WhereClause += " AND ngay_ct >= to_date('" & pv_TuNgay & "','dd/MM/yyyy')"
            End If
            If pv_DenNgay <> "" Then
                WhereClause += " AND ngay_ct <= to_date('" & pv_DenNgay & "','dd/MM/yyyy')"
            End If
            If pv_SO_CT <> "" Then
                WhereClause += " AND so_ct = '" & pv_SO_CT & "'"
            End If
            If pv_MST_NNT <> "" Then
                WhereClause += " AND ma_nntien = '" + pv_MST_NNT + "'"
            End If
            strSql &= WhereClause & "          group by hdr.so_ct,hdr.ngay_ct,hdr.ma_nntien, hdr.ten_nntien, hdr.dc_nntien, hdr.ma_nnthue, hdr.ten_nnthue, hdr.ma_cqthu, hdr.tk_co, hdr.ma_nh_a, hdr.ma_nh_b, hdr.ma_ntk"
            ds = DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
            dtgdata.DataSource = ds.Tables(0)
            dtgdata.DataBind()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        loadGridView()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub
End Class
