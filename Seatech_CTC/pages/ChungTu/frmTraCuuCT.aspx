﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmTraCuuCT.aspx.vb" Inherits="pages_ChungTu_frmTraCuuCT" Title="Tra cứu chứng từ nộp NSNN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
     var arrControl =  new Array();
            arrControl[0]='txtSHKB';
            arrControl[1]='txtTenKB';
            arrControl[2]='txtDChiNNT';
            arrControl[3]='txtMaNNTien';
            arrControl[4]='txtTenNNTien';
            arrControl[5]='txtDChiNNTien';
            arrControl[6]='txtMaDBHC';
            arrControl[7]='txtTenCQThu';
            arrControl[8]='txtMaCQThu';
            arrControl[9]='txtTenCQThu';
            arrControl[10]='ddlTKNo';
            arrControl[11]='txtNGAY_KH_NH';
            arrControl[12]='txtTenNNT';
            arrControl[13]='ddlTKCo';
            arrControl[14]='txtMaNNT';
            arrControl[15]='txtTK_KH_NH';
            arrControl[16]='txtSoDu_KH_NH';
            arrControl[17]='txtMaNT';
            arrControl[18]='txtTenNT';
            arrControl[19]='ddlLoaiThue';
            arrControl[20]='txtDescLoaiThue';
            arrControl[21]='txtToKhaiSo';
            arrControl[22]='txtNgayDK';
            arrControl[23]='txtLHXNK';
            arrControl[24]='txtDescLHXNK';
            arrControl[25]='txtSoKhung';
            arrControl[26]='txtSoMay';
            arrControl[27]='txtSo_BK';
            arrControl[28]='txtNgay_BK';
            arrControl[29]='txtTTIEN';
            arrControl[30]='txtTT_TTHU';
            arrControl[31]='txtMA_NV';
            arrControl[32]='txtSoCT';
            arrControl[33]='txtSo_BT';
            arrControl[34]='txtTRANG_THAI';
            arrControl[35]='ddlHTTT';
            arrControl[36]='txtTenTKNo';
            arrControl[37]='txtTenTKCo';
            arrControl[38]='txtNgayLV';
            arrControl[39]='txtTenTK_KH_NH';
            arrControl[40]='txtHuyen_NNTIEN';
            arrControl[41] = 'txtTinh_NNTIEN';
            arrControl[42] = 'txtMA_NH_A';
            arrControl[43] = 'txtTen_NH_A';
            arrControl[44] = 'txtMA_NH_TT';
            arrControl[45] = 'txtTEN_NH_TT';
            arrControl[46] = 'txtMA_NH_B';
            arrControl[47] = 'txtTen_NH_B';
            arrControl[48] = 'txtKHCT';
    var v_tt_chuyenthue = '';   
    function jsShowChiTietCT(_soct,_lthue)
    {        
        if(_lthue == '04'){
            //  window.showModalDialog("../../pages/HaiQuan/frmChiTietCT.aspx?soct=" + _soct, "", "dialogWidth:800px;dialogHeight:650px;help:0;status:0;","_new");
            window.open("../../pages/HaiQuan/frmChiTietCT.aspx?soct=" + _soct, "", "dialogWidth:800px;dialogHeight:650px;help:0;status:0;", "_new");
        }
        else{
            //  window.showModalDialog("../../pages/ChungTu/frmChiTietCT.aspx?soct=" + _soct, "", "dialogWidth:800px;dialogHeight:650px;help:0;status:0;","_new");
            window.open("../../pages/ChungTu/frmChiTietCT.aspx?soct=" + _soct, "", "dialogWidth:800px;dialogHeight:650px;help:0;status:0;", "_new");
        }
    }     
    function showHuy(){
        var strNhom = $get('<%=hdfMaNhom.clientID%>').value;
        if(strNhom != "04"){
            document.getElementById('tdPwd').style.display='none';
        }else{
            document.getElementById('tdPwd').style.display='';
        }
    }
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
	        for (var k = 0; k <= str.length; k++){
	            if (k == locs[i]){
	            if (str.substring(k, k+1) != delim){
	                    str = str.substring(0,k) + delim + str.substring(k,str.length)
	                }
	            }
	        }
        }
        textbox.value = str
    }
    function jsFillLTHUE()
    {
//        var cb = document.getElementById('ddlMaLoaiThue');
//        var arr = arrMaLTHUE;
//        cb.options.length =0;
//       for (var i=0; i<arrMaLTHUE.length; i++) {
//            var arr = arrMaLTHUE[i].split(';');                        
//            var value = arr[0];
//            var label = arr[1];            
//            cb.options[cb.options.length] = new Option(label, value);
//        }
    }
    function jsDSCT_ToDay()
    {
        document.getElementById("divKQToDay").style.display='';
        document.getElementById("divKQ").style.display='none';
        document.getElementById("divTracuu").style.display='';
        var strRefId, strTuSo, strDenSo, strTuSoFT, strDenSoFT, strTuNgay, strDenNgay, strMaNNT, strTenNNT, strTKNo, strTKCo, strKHCT, stroSoTien, strMaNV, strTT_TC, strLoaiThue, strSo_FCC, strSo_refCITAD;
        strTuSo=document.getElementById("txtTuSoCT").value;
        strDenSo=document.getElementById("txtDenSoCT").value;
        strTuSoFT=document.getElementById("txtTuSoFT").value;
        strDenSoFT=document.getElementById("txtDenSoFT").value;
        strTuNgay=document.getElementById("txtTuNgay").value;
        strDenNgay=document.getElementById("txtDenNgay").value;
        strTKNo=document.getElementById("txtTKNo").value.replaceAll('.','');
        strTKCo=document.getElementById("txtTKCoTC").value.replaceAll('.','');
        strMaNNT=document.getElementById("txtMaNNT_TK").value;
        strTenNNT=document.getElementById("txtTenNNT_TK").value;
        strKHCT=document.getElementById("txtKHCT1").value;
        stroSoTien=document.getElementById("txtSoTien").value.replaceAll('.','');
        strMaNV=document.getElementById("CboNhanVien").value;
        strTT_TC=document.getElementById("cboTT").value;
        strLoaiThue=document.getElementById("ddlMaLoaiThue").value; 
        strMa_CN=document.getElementById("cboMa_CN").value;
        strKenh_CT = document.getElementById("cboKenhCT").value;
        strRefId = document.getElementById("txtRefId").value
        PageMethods.LoadCTListToDay(strRefId,strTuSo, strDenSo, strTuSoFT, strDenSoFT, strTuNgay, strDenNgay, strMaNNT, strTenNNT, strTKNo, strTKCo, strKHCT, stroSoTien, strTT_TC, strLoaiThue, strMaNV, strMa_CN, curMa_NV, curMa_CN, strKenh_CT, LoadCTListToDay_Complete, LoadCTList_Error);
    }
    function LoadCTListToDay_Complete(result)
    {            
        document.getElementById('divDSCT_ToDay').innerHTML = result;    
    }
    function jsTracuu()
    {
        var  strTuSo, strDenSo, strTuSoFT, strDenSoFT, strTuNgay, strDenNgay, strMaNNT, strTenNNT, strTKNo, strTKCo, strKHCT, stroSoTien, strMaNV, strTT_TC, strLoaiThue, strSo_FCC, strSo_refCITAD;
        document.getElementById("divKQ").style.display= '';
        document.getElementById("divKQToDay").style.display='none';
        document.getElementById("divTracuu").style.display='none';
        strTuSo=document.getElementById("txtTuSoCT").value;
        strDenSo=document.getElementById("txtDenSoCT").value;
        strTuSoFT=document.getElementById("txtTuSoFT").value;
        strDenSoFT=document.getElementById("txtDenSoFT").value;
        strTuNgay=document.getElementById("txtTuNgay").value;
        strDenNgay=document.getElementById("txtDenNgay").value;
        strTKNo=document.getElementById("txtTKNo").value.replaceAll('.','');
        strTKCo=document.getElementById("txtTKCoTC").value.replaceAll('.','');
        strMaNNT=document.getElementById("txtMaNNT_TK").value;
        strTenNNT=document.getElementById("txtTenNNT_TK").value;
        strKHCT=document.getElementById("txtKHCT1").value;
        stroSoTien=document.getElementById("txtSoTien").value.replaceAll('.','');
        strMaNV=document.getElementById("CboNhanVien").value;
        strTT_TC=document.getElementById("cboTT").value;
        strLoaiThue=document.getElementById("ddlMaLoaiThue").value; 
        strMa_CN=document.getElementById("cboMa_CN").value;
        strKenh_CT = document.getElementById("cboKenhCT").value;
        strSo_FCC  = document.getElementById("txtSo_FCC").value;
        strSo_refCITAD = document.getElementById("txtSo_refCITAD").value;
        
        
        PageMethods.LoadCTList(strTuSo, strDenSo, strTuSoFT, strDenSoFT, strTuNgay, strDenNgay, strMaNNT, strTenNNT, strTKNo, strTKCo, strKHCT, stroSoTien, strTT_TC, strLoaiThue, strMaNV, strMa_CN, curMa_NV, curMa_CN, strKenh_CT,strSo_FCC,strSo_refCITAD , LoadCTList_Complete, LoadCTList_Error);
    }
     String.prototype.replaceAll = function(strTarget,strSubString)
    {
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    } 
    
    function jsFillMaNV()
    {
        var cb = document.getElementById('CboNhanVien');   
        var arrNV =arrMaNV; 
        cb.options.length=0; //
        cb.options[0] = new Option('<< Tất cả >>', ''); 
        for (var i=0; i<arrNV.length; i++) {
            var arr = arrNV[i].split(';');                        
            var value = arr[0];
            var label = arr[1];            
            cb.options[cb.options.length] = new Option(label, value);
        }
         
    }
     function jsFillNgay()
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = dd+'/'+mm+'/'+yyyy;
        
         document.getElementById('txtTuNgay').value=today;  
         document.getElementById('txtDenNgay').value=today;  
    }
     function jsFillMa_CN()
    {
        var cb = document.getElementById('cboMa_CN');   
        var arrCN =arrMaCN; 
        cb.options.length=0; //
        cb.options[0] = new Option('<< Tất cả >>', ''); 
        for (var i=0; i<arrCN.length; i++) {
            var arr = arrCN[i].split(';');                        
            var value = arr[0];
            var label = arr[1];            
            cb.options[cb.options.length] = new Option(label, value);
        }
         
    }
     function jsFillKenh_CT()
    {
        var cb = document.getElementById('cboKenhCT');   
        var arrCN =arrKenhCT; 
        cb.options.length=0; //
        cb.options[0] = new Option('<< Tất cả >>', ''); 
        for (var i=0; i<arrCN.length; i++) {
            var arr = arrCN[i].split(';');                        
            var value = arr[0];
            var label = arr[1];            
            cb.options[cb.options.length] = new Option(label, value);
        }
         
    }
    function LoadCTList_Complete(result,methodName)
    {            
        document.getElementById('divDSCT').innerHTML = result;    
        showHuy(); 
        //document.getElementById('tblChiTiet').style.display='none';
    }
    function LoadCTList_Error(error,userContext,methodName)
    {
     if(error !== null) 
        {
           document.getElementById('divStatus').innerHTML="Lỗi trong quá trình lấy danh sách chứng từ: " + error.get_message();
        }
    }
     //4)Lay mot chung tu cu the
    function jsGetCTU(strSHKB,strSoCT,strSoBT,strSoTien,strSoTT,strMaNV,strNgayKB,strTTCThue)
    {
        jsTracuu();
        v_tt_chuyenthue = strTTCThue;
        PageMethods.GetCTU(strSHKB,strSoCT,strSoBT,strMaNV,strNgayKB,GetCTU_Complete,GetCTU_Error);
        var strMaNTT= document.getElementById("txtMaNNT").value;
        PageMethods.LoadCTChiTiet(strMaNTT,strSHKB,strSoCT,strSoBT,strMaNV,strNgayKB,GetCTU_ChiTiet_Complete,GetCTU_Error);
    }
    function GetCTU_ChiTiet_Complete(result,methodName)
    {
        document.getElementById('gridChitiet').innerHTML=result;
    }
     function jsGetXMLDoc(xmlString)
    {
        var xmlDoc;
        try //Internet Explorer
        {
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async="false";
            xmlDoc.loadXML(xmlString);
            return xmlDoc; 
        }
        catch(e)
        {
            parser=new DOMParser();
            xmlDoc=parser.parseFromString(xmlString,"text/xml");
            return xmlDoc;
        }
    }
     function GetCTU_Complete(result,methodName)
     
     {  
     //alert('abc');
        var xmlstring = result;
        var xmlDoc = jsGetXMLDoc(xmlstring);
        var strTrang_Thai=XMLToBean(xmlDoc,arrControl);
        
    }
    function GetCTU_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            document.getElementById('divStatus').innerHTML="Lỗi trong quá trình lấy thông tin chứng từ" + error.get_message();
        }
    }
    function XMLToBean(xmlDoc,arrControlName)
    {        
        var strRtTrang_Thai ="00";    
        var strLoaiThue ="";
        var strHTTT = "";
        var rootCTU_HDR = xmlDoc.getElementsByTagName('CTU_HDR')[0];
        document.getElementById('divDescCTU').innerHTML = 'SHKB/SoCT/SoBT:' + rootCTU_HDR.getElementsByTagName("SHKB")[0].firstChild.nodeValue + "/" + rootCTU_HDR.getElementsByTagName("SoCT")[0].firstChild.nodeValue + "/" + rootCTU_HDR.getElementsByTagName("So_BT")[0].firstChild.nodeValue ;
        document.getElementById('<%=hdfTT_CTHUE.clientID%>').value=rootCTU_HDR.getElementsByTagName("TT_CTHUE")[0].firstChild.nodeValue;
        //alert(document.getElementById('<%=hdfTT_CTHUE.clientID%>').value);
        for (var i=0;i<= arrControlName.length - 1;i++)
            {
            
            if (rootCTU_HDR.getElementsByTagName(arrControlName[i].substring(3,99))[0].firstChild!=null)
                {  
              
                  document.getElementById(arrControlName[i]).value = rootCTU_HDR.getElementsByTagName(arrControlName[i].substring(3,99))[0].firstChild.nodeValue;                        
                }
            }
          document.getElementById('txtTT_TTHU').value=localeNumberFormat(document.getElementById('txtTT_TTHU').value,'.');
          if (rootCTU_HDR.getElementsByTagName("TRANG_THAI")[0].firstChild.nodeValue=='01' || rootCTU_HDR.getElementsByTagName("TRANG_THAI")[0].firstChild.nodeValue=='05')
          {
             document.getElementById('cmdInCT').disabled = false;
          }else
          {
            document.getElementById('cmdInCT').disabled = true;
          }
          ShowHideControlByLoaiThue();
          ShowHideControlByPT_TT();          
        }
    function jsGetIndexByValue(ddlObject,cmpVal)
    {
        var obj = document.getElementById(ddlObject);
        for (var i=0;i<obj.length;i++){
            if(obj.options[i].value==cmpVal)
            {
                return i;
            }
        }             
        return 0;   
    }       
     function ShowHideControlByPT_TT()
    {        
         var intPP_TT = document.getElementById("ddlHTTT").selectedIndex;                
        var rTK_KH_NH = document.getElementById("rowTK_KH_NH");        
        var rSoDuNH = document.getElementById("rowSoDuNH");
        document.getElementById("rowTKNo").style.display='none';
        if (intPP_TT == 0 ){//"NH_TKTM"){
            rTK_KH_NH.style.display = 'none';
            rSoDuNH.style.display = 'none'; 
        }  
        if (intPP_TT == 1 ){//"NH_TKTH_KB"){
            rTK_KH_NH.style.display = '';
            rSoDuNH.style.display = ''; 
        } 
        if (intPP_TT == 2 ){//"NH_TKCT"){
            rTK_KH_NH.style.display = 'none';
            rSoDuNH.style.display = 'none';
        } 
        if (intPP_TT == 3 ){//"NH_TKTG"){
            rTK_KH_NH.style.display = '';
            rSoDuNH.style.display = 'none';
        }   
        rSoDuNH.style.display = 'none';                                     
    }
    function ShowHideControlByLoaiThue()
    {   
        var intLoaiThue = document.getElementById("ddlLoaiThue").value;
        var rSoKhung=document.getElementById("rowSoKhung"); 
        var rSoMay=document.getElementById("rowSoMay"); 
        
        var rToKhaiSo=document.getElementById("rowToKhaiSo"); 
        var rNgayDK=document.getElementById("rowNgayDK"); 
        var rLHXNK=document.getElementById("rowLHXNK"); 

        if (intLoaiThue=="01")//"01"
        {
            rSoKhung.style.display = 'none';
            rSoMay.style.display = 'none';
            rToKhaiSo.style.display='none';
            rNgayDK.style.display='none';
            rLHXNK.style.display='none';        
            $get('txtDescLoaiThue').value = 'Thuế công thương nghiệp';
        } 
        else if (intLoaiThue=="03")//"03"
        {
            rSoKhung.style.display = '';
            rSoMay.style.display = '';
            
            rToKhaiSo.style.display='none';
            rNgayDK.style.display='none';
            rLHXNK.style.display='none';
            $get('txtDescLoaiThue').value = 'Thuế Nhà đất,xe máy';   
        }
        else if (intLoaiThue=="04")//"04"
        {
            rSoKhung.style.display = 'none';
            rSoMay.style.display = 'none';
            
            rToKhaiSo.style.display='';
            rNgayDK.style.display='';
            rLHXNK.style.display='';            
            
            $get('txtDescLoaiThue').value = 'Thuế Hải Quan';    
        }
        else //"05
        {
            rSoKhung.style.display = 'none';
            rSoMay.style.display = 'none';
            rToKhaiSo.style.display='none';
            rNgayDK.style.display='none';
            rLHXNK.style.display='none';    
            $get('txtDescLoaiThue').value = 'Các loại thuế khác';                             
        }        
    }
    function localeNumberFormat(amount,  delimiter) {
	        try {
		        amount = parseFloat(amount);
	        }
	        catch (e) {
		        throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount);
		        return null;
	        }

	        if (delimiter == null || delimiter == undefined) { delimiter = '.'; }

	        // convert to string
	        if (amount.match != 'function') { amount = amount.toString(); }

	        // validate as numeric
	        var regIsNumeric = /[^\d,\.-]+/igm;
	        var results = amount.match(regIsNumeric);

	        if (results != null) {
		        outputText('INVALID NUMBER', eOutput)
		        return null;
	        }

	        var minus = amount.indexOf('-') >= 0 ? '-' : '';
	        amount = amount.replace('-', '');
	        var amtLen = amount.length;
	        var decPoint = amount.indexOf(',');
	        var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

	        var wholeNumber = amount.substr(0, wholeNumberEnd);
	        var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

	        var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
	        var rvsNumber = wholeNumber.split('').reverse().join('');
	        var output = '';

	        for (i = 0; i < wholeNumberEnd; i++) {
		        if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
		        output += rvsNumber.charAt(i);
	        }

	        output = minus +  output.split('').reverse().join('') + fraction;

	        return output;

    }
    function jTraCuuCT()
    {
        document.getElementById("divKQ").style.display= 'none';
        document.getElementById("divTracuu").style.display='';
        jsDSCT_ToDay();
        for (var i=0;i<= arrControl.length - 1;i++)
            {
                  document.getElementById(arrControl[i]).value = '';                        
            }
             document.getElementById('gridChitiet').innerHTML='';
    }
    function jsIn_CT()
    {
      if (document.getElementById('divDescCTU').innerHTML.length>0)
            {                
                var strSHKB = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[0];
                var strSoCT = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[1];
                var strSo_BT = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[2];
                var MaNV = document.getElementById('txtMA_NV').value;
                var ngayLV= document.getElementById('txtNgayLV').value;
            }
        // window.open("../../pages/Baocao/frmInGNT.aspx?SoCT=" + strSoCT +"&BS=&SHKB=" + strSHKB + "&SoBT="+ strSo_BT +"&NgayKB=" + ngayLV + "&MaNV=" + MaNV , "");                      
        //
        window.open("../../pages/Baocao/InGNTpdf.ashx?SoCT=" + strSoCT + "&BS=&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV, "");
    }
   
     function jsHuy_CT()
    {
        document.getElementById('cmdHuy').disabled = true;       
        var strMaNTT= document.getElementById("txtMaNNT").value;
        var strNgayKB= document.getElementById("txtNgayLV").value;
        var strMaNV= document.getElementById("txtMA_NV").value;
        var strTrangThai= document.getElementById("txtTRANG_THAI").value;
        var strSHKB = document.getElementById("txtSHKB").value;
        var strSoBT = document.getElementById("txtSo_BT").value;
        var strSoCT = document.getElementById("txtSoCT").value;
        var strLoaiThue=document.getElementById("ddlLoaiThue").value; 
        var strMatKhauNhap = document.getElementById("txtPwd").value;
        var strUser = $get('<%=hdfMaNV.clientID%>').value;
        var strNhom = $get('<%=hdfMaNhom.clientID%>').value;
        var strMatKhauSesion = $get('<%=hdfMK.clientID%>').value;
        var TT_CTHUE = $get('<%=hdfTT_CTHUE.clientID%>').value;
        if (strNhom != "04"){
            alert('Bạn không có quyền hủy!');
            document.getElementById('cmdHuy').disabled = true; 
            return; }
        else{
            document.getElementById('cmdHuy').disabled = true; 
        }
        if (strTrangThai != "01") {
            alert('Chỉ cho phép hủy chứng từ đã được kiểm soát!');
            document.getElementById('cmdHuy').disabled = true; 
            return;
        }
        if (strMatKhauNhap == ""){
           alert('Hãy nhập mật khẩu để thực hiện hủy chứng từ!');
           document.getElementById("txtPwd").focus
           document.getElementById('cmdHuy').disabled = true; 
           return;
        }
        PageMethods.Huy_CT(strLoaiThue,TT_CTHUE,strMaNTT,strSHKB,strSoCT,strSoBT,strMaNV,strNgayKB,strMatKhauNhap,strMatKhauSesion,Huy_CT_Complete,Huy_CT_Error);
    }
    function Huy_CT_Complete(result,methodName)
    {
        if(result.substring(0,1) != "1"){
            document.getElementById('cmdHuy').disabled = true;
            document.getElementById("txtPwd").value = '';
            alert(result.substring(2));
            jsTracuu();
        }else {
            document.getElementById('cmdHuy').disabled = true;
            document.getElementById('cmdInCT').disabled = true;
            document.getElementById("txtPwd").value = '';
            alert(result.substring(2));
            jsTracuu();
        }
        
    }
    function Huy_CT_Error(result,methodName)
    {
        alert(result.substring(0,1));
        document.getElementById('cmdHuy').disabled = true;
        document.getElementById("txtPwd").value = '';
    }
      function jsCalTotal(){
    
        if ($get('txtSoTien').value.length > 0) {            
                    jsFormatNumber('txtSoTien');
                }  
    }
    function jsFormatNumber(txtCtl){
        document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.',''),'.');
    }
         
    function localeNumberFormat(amount,delimiter) {
	    try {
	   
		    amount = parseFloat(amount);
	    }catch (e) {
		    throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
		    return null;
	    }
	    if (delimiter == null || delimiter == undefined) { delimiter = '.'; }
	    
	    // convert to string
	    if (amount.match != 'function') { amount = amount.toString(); }

	    // validate as numeric
	    var regIsNumeric = /[^\d,\.-]+/igm;
	    var results = amount.match(regIsNumeric);

	    if (results != null) {
		    outputText('INVALID NUMBER', eOutput)
		    return null;
	    }

	    var minus = amount.indexOf('-') >= 0 ? '-' : '';
	    amount = amount.replace('-', '');
	    var amtLen = amount.length;
	    var decPoint = amount.indexOf(',');
	    var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

	    var wholeNumber = amount.substr(0, wholeNumberEnd);
	    
	    var numberEnd=amount.substr(decPoint,amtLen);
	    var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

	    var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
	    var rvsNumber = wholeNumber.split('').reverse().join('');
	    var output = '';

	    for (i = 0; i < wholeNumberEnd; i++) {
		    if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
		    output += rvsNumber.charAt(i);
	    }
	    output = minus +  output.split('').reverse().join('') + fraction ;
	    return output;
    }    
       
    String.prototype.replaceAll = function(strTarget,strSubString)
    {
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    } 
 
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
	        for (var k = 0; k <= str.length; k++){
	            if (k == locs[i]){
	            if (str.substring(k, k+1) != delim){
	                    str = str.substring(0,k) + delim + str.substring(k,str.length)
	                }
	            }
	        }
        }
        textbox.value = str
    } 
    jQuery.fn.filterByText = function(textbox) {
    return this.each(function() {
        var select = this;
        var options = [];
        $(select).find('option').each(function() {
            options.push({value: $(this).val(), text: $(this).text()});
        });
        $(select).data('options', options);

        $(textbox).bind('change keyup', function() {
            var options = $(select).empty().data('options');
            var search = $.trim($(this).val());
            var regex = new RegExp(search,"gi");

            $.each(options, function(i) {
                var option = options[i];
                if(option.text.match(regex) !== null) {
                    $(select).append(
                        $('<option>').text(option.text).val(option.value)
                    );
                }
            });
        });
    });
};

$(function() {
    $('#cboMa_CN').filterByText($('#txtSearchMaCN'),true);
});
    function ExportDivDataToExcel()
    {
    var html = $("#<%=Panel3.clientID %>").html();
    html = $.trim(html);
    html = html.replace(/>/g,'&gt;');
    html = html.replace(/</g,'&lt;');
    $("input[id$='HdnValue']").val(html);
    }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">TRA CỨU CHỨNG TỪ</asp:Label>
            </td>
        </tr>
        <tr>
            <td width='100%' align="left" valign="top" class='nav'>
                <div id="divTracuu">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width='90%' align="left" valign="top" >
                                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                    <tr>
                                        <td width='15%' align="left" valign="top" class="form_label">
                                            Từ Số CT:
                                        </td>
                                        <td width='35%' align="left" valign="top" class="form_control">
                                            <input type="text" id='txtTuSoCT' maxlength='13' class="inputflat" />
                                        </td>
                                        <td width='15%' align="left" valign="top" class="form_label">
                                            Đến Số CT:
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtDenSoCT' maxlength='13' class="inputflat" />
                                        </td>
                                    </tr>
                                    <tr style="display:none ">
                                        <td align="left" valign="top" class="form_label">
                                            Từ Số FT:
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtTuSoFT' maxlength='13' class="inputflat" />
                                        </td>
                                        <td align="left" valign="top" class="form_label">
                                            Đến Số FT:
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtDenSoFT' maxlength='13' class="inputflat" />
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td align="left" class="form_label">Từ ngày</td>
                                        <td class="form_control"><span style="display:block; width:49%; position:relative;vertical-align:middle;">
                                            <input type="text" id='txtTuNgay' maxlength='10' class="inputflat date-pick dp-applied" style="width: 84%; display:block; float:left; margin-right:1px;"
                                            onblur="CheckDate(this);" onfocus="this.select()" />
                                        </span></td>
                                        <td align="left" class="form_label">Đến ngày</td>
                                        <td class="form_control">
                                        <span style="display:block; width:49%; position:relative; vertical-align:middle;">
                                            <input type="text" id='txtDenNgay' maxlength='10' class="inputflat date-pick dp-applied" style="width: 84%; display:block; float:left; margin-right:1px;"
                                             onblur="CheckDate(this);" onfocus="this.select()" />
                                        </span></td>
                                    </tr>
                                    <tr>
                                        <td width='100' align="left" valign="top" class="form_label">
                                            Loại Thuế:<br /><br />
                                            Số ref:
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <select id="ddlMaLoaiThue" style="width: 90%" class="inputflat" onchange="ShowHideControlByLoaiThue();">
                                                <option value="00" selected="selected"><< Tất cả >></option>
                                                <option value="01" >Thuế Nội Địa</option>
                                                <option value="04" >Thuế Hải Quan</option>
                                                <%--<option value="00" selected="true"><< Tất cả >></option>
                                                <option value="01">Thuế Nội Địa</option>
                                                <option value="04" >Thuế Hải Quan</option>--%>
                                            </select><br />
                                            <input type="text" id='txtRefId' class="inputflat" style="width: 90%; display:block; float:left; margin-right:1px;margin-top:10px" />
                                        </td>
                                        <td width='100' align="left" valign="top" class="form_label">
                                          Mã chi nhánh
                                        </td>
                                         <td  align="left" valign="top" class="form_control">
                                         <input type="text" id='txtSearchMaCN' class="inputflat" style="width: 89%" />
                                          <select id="cboMa_CN" style="width: 90%" class="inputflat">
                                            </select>                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='100' align="left" valign="top" class="form_label">
                                            Mã số thuế:
                                        </td>
                                        <td align="left" valign="top" class="form_control" >
                                            <input type="text" id='txtMaNNT_TK' class="inputflat" style="width: 89%"/>
                                        </td>
                                        <td align="left" valign="top" class="form_label">
                                            Tên người nộp thuế:
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtTenNNT_TK' class="inputflat" style="width: 89%" />
                                        </td>
                                    </tr>
                                    <tr style="display:none">
                                        <td align="left" valign="top" class="form_label">
                                            TK Nợ:
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtTKNo' style="width: 60%" class="inputflat" />
                                        </td>
                                        <td align="left" valign="top" class="form_label">
                                            TK Có:
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtTKCoTC' class="inputflat" style="width: 80%" />
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td width='100' align="left" valign="top" class="form_label">
                                            Kênh chứng từ:
                                        </td>
                                        <td align="left" valign="top" class="form_label">
                                            <select id="cboKenhCT" style="width: 90%" class="inputflat">
                                            </select>
                                        </td>
                                        <td width='100' align="left" valign="middle" class="form_label">
                                            Số Tiền:
                                        </td>
                                        <td align="left" valign="top" class="form_label">
                                            <input type="text" id='txtSoTien' style="width: 89%" class="inputflat" onblur="jsCalTotal();"/>
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td width='100' align="left" valign="top" class="form_label">
                                            User lập CT:
                                        </td>
                                        <td align="left" valign="top" class="form_label">
                                            <select id="CboNhanVien" style="width: 90%" class="inputflat">
                                            </select>
                                        </td>
                                        <td width='100' align="left" valign="top" class="form_label">
                                            Trạng Thái CT:
                                        </td>
                                        <td align="left" valign="top" class="form_label">
                                            <select id="cboTT" style="width: 90%" class="inputflat">
                                                <option value='99'><< Tất cả >></option>
                                               <%-- <option value='00'>Chưa kiểm soát </option>--%>
                                                <option value='05'>Chờ kiểm soát </option>
                                                <option value='01'>Đã kiểm soát </option>
                                                <option value='07'>Chuyển thuế lỗi </option>
                                                <option value='03'>CT chuyển trả</option>
                                                <option value='04'>Đã hủy bởi GDV</option>
                                                <option value='06'>Đã hủy bởi KSV</option>
                                                <option value='02'>Hủy bởi GDV - chờ duyệt</option>
                                                <option value='08'>Hủy chuyển thuế lỗi</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr align="left" style="display:none ">
                                        <td width='100' align="left" valign="middle" class="form_label">
                                            Ký Hiệu CT:
                                        </td>
                                        <td align="left" valign="top" class="form_label">
                                            <input type="text" id='txtKHCT1' style="width: 60%" class="inputflat" />
                                        </td>
                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <br />
                                <asp:HiddenField ID="hdfMaNV" runat="server" Value="" />
                                <asp:HiddenField ID="hdfMK" runat="server" Value="" />
                                <asp:HiddenField ID="hdfMaNhom" runat="server" Value="" />
                                <asp:HiddenField ID="hdfTT_CTHUE" runat="server" Value="" />
                                <input type="button" class="ButtonCommand" value="Tra Cứu" id="cmdTC" onclick="jsDSCT_ToDay()" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" OnClientClick="ExportDivDataToExcel()" />
                                <asp:HiddenField ID="HdnValue" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="divKQToDay">
                    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel3" runat="server" Height="450px" Width="100%" ScrollBars="Vertical" BorderWidth="1px" BorderColor="Black">
                                    <table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT_ToDay' style='width: 100%;
                                        border-collapse: collapse;'>
                                        <tr class='grid_header'>
                                            <td style='width:5%'>
                                                STT
                                            </td>
                                            <td style='width:6%'>
                                                Trạng thái
                                            </td>
                                            <td style='width:13%'>
                                                Số ref
                                            </td>
                                            <td style='width:9%'>
                                                Số chứng từ
                                            </td>
                                            <td style='width:12%'>
                                                Số tiền
                                            </td>
                                            <td style='width:10%'>
                                                User lập
                                            </td>
                                            <td style='width:10%'>
                                                User duyệt
                                            </td>
                                            <td style='width:18%'>
                                                Ngày lập
                                            </td>
                                            <td>
                                                Ngày duyệt
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="divDSCT_ToDay">
                                    </div>
                                </asp:Panel>
                                <table>
                                    <tr style="display:none">
                                        <td style="width: 10%">
                                            <img src="../../images/icons/ChuaKS.png" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            Chưa Kiểm soát
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td style="width: 10%">
                                            <img src="../../images/icons/ChuyenKS.png" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            Chờ Kiểm soát
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <img src="../../images/icons/DaKS.png" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            Đã Kiểm soát
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <img src="../../images/icons/KSLoi.png" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            Chuyển trả
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <img src="../../images/icons/Huy.png" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            CT bị Hủy bởi GDV
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <img src="../../images/icons/HuyHS.gif" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            CT Hủy bởi GDV - chờ duyệt
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <img src="../../images/icons/Huy_CT_Loi.png" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            Duyệt hủy chuyển thuế lỗi
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="width: 10%">
                                            <img src="../../images/icons/Huy_KS.png" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            CT bị Hủy bởi KSV
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <img src="../../images/icons/TT_CThueLoi.gif" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            Chuyển thuế/HQ lỗi
                                        </td>
                                    </tr>
                                    <tr style="display:none">
                                        <td style="width: 10%">
                                            <img src="../../images/icons/Huy_CT_Loi.png" />
                                        </td>
                                        <td style="width: 90%" colspan="2">
                                            Hủy chuyển thuế/HQ lỗi
                                        </td>
                                    </tr>
                                    <tr class="img">
                                        <td align="left" style="width: 100%" colspan="3">
                                            <hr style="width: 50%" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="divKQ" style="display: none">
                    <table width="100%" align="center">
                        <tr>
                            <td valign="top" width="210px" height="300px" align="left">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="left">
                                        <td>
                                            <asp:Panel ID="Panel2" runat="server" Height="450px" Width="100%" ScrollBars="Vertical">
                                                <table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width: 100%;
                                                    border-collapse: collapse;'>
                                                    <tr class='grid_header'>
                                                        <td class="20%">
                                                            TT
                                                        </td>
                                                        <td style="width: 30%">
                                                            Số CT
                                                        </td>
                                                        <td style="width: 50%">
                                                            Tên ĐN/Số BT
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div id="divDSCT">
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" width="680px">
                                <table border='0' cellpadding='0' cellspacing='0' width='100%' id='tblChiTiet'>
                                    <tr>
                                        <td colspan="2" width='100%' align="left">
                                            <table width="100%">
                                                <tr class="grid_header">
                                                    <td height='15' align="right">
                                                        Trường (*) là bắt buộc nhập
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="Panel1" style="height: 380px; width: 100%; overflow-y: scroll;">
                                                <table id="grdHeader" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                                                    border-style: solid; font-family: Verdana; font-size: 8pt; width: 97%; border-collapse: collapse;">

                                                    <tr id="rowMaNNT">
                                                        <td width="24%">
                                                            <b>Mã số thuế/Tên</b>
                                                        </td>
                                                        <td width="25%">
                                                            <input type="text" id="txtMaNNT" style="width: 99%" disabled="true" class="inputflat"
                                                                backcolor="Aqua" />
                                                        </td>
                                                        <td width="51%">
                                                            <input type="text" id="txtTenNNT"  style="width: 99%" class="inputflat" disabled="disabled"
                                                                maxlength="40" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowDChiNNT">
                                                        <td>
                                                            <b>Địa chỉ người nộp thuế</b>
                                                        </td>
                                                        <td colspan="2">
                                                            <input type="text" id="txtDChiNNT" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowNNTien">
                                                        <td>
                                                            <b>MST/Tên người nộp tiền</b>
                                                        </td>
                                                        <td >
                                                            <input type="text" id="txtMaNNTien" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td >
                                                            <input type="text" id="txtTenNNTien" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowDChiNNTien" style="display:none">
                                                        <td style="display:none">
                                                            <b>Địa chỉ người nộp tiền</b>
                                                        </td>
                                                        <td colspan="2">
                                                            <input type="text" id="txtDChiNNTien" style="width: 99%; display:none" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowQuan_HuyenNNTien" style="display:none" visible="false">
                                                        <td>
                                                            <b>Quận(Huyện)</b>
                                                        </td>
                                                        <td colspan="2">
                                                            <input type="text" id="txtHuyen_NNTIEN" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowTinh_NNTien" style="display:none">
                                                        <td>
                                                            <b>Thành phố(Tỉnh)</b>
                                                        </td>
                                                        <td colspan="2">
                                                            <input type="text" id="txtTinh_NNTIEN" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowSHKB">
                                                        <td width="24%">
                                                            <b>Số hiệu KB</b>
                                                        </td>
                                                        <td width="25%">
                                                            <input type="text" class="inputflat" id="txtSHKB" disabled="true" style="width: 99%"
                                                                backcolor="Aqua" />
                                                        </td>
                                                        <td width="51%">
                                                            <input type="text" id="txtTenKB" class="inputflat" style="width: 99%" disabled="disabled" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowKHCT">
                                                        <td>
                                                            <b>Mã hiệu CT</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtKHCT" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="Text2" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowDBHC">
                                                        <td>
                                                            <b>ĐBHC</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtMaDBHC" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTenDBHC" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowCQThu">
                                                        <td>
                                                            <b>CQ Thu</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtMaCQThu" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTenCQThu" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowTKNo" style="display:none">
                                                        <td style="background-color: Aqua">
                                                            <b>TK Nợ NSNN</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="ddlTKNo" class="inputflat" style="width: 99%" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTenTKNo" class="inputflat" style="width: 99%" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowTKCo">
                                                        <td style="background-color: pink">
                                                            <b>TK Có NSNN</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="ddlTKCo" class="inputflat" style="width: 99%" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTenTKCo" class="inputflat" style="width: 99%" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowHTTT">
                                                        <td>
                                                            <b>Hình thức thanh toán</b>
                                                        </td>
                                                        <td>
                                                            <select id="ddlHTTT" style="width: 99%" class="inputflat" disabled="disabled">
                                                             <option value="00" selected>Nộp tiền mặt</option>
                                                                <option value="01">Chuyển khoản</option>
                                                                <option value="02">Điện chuyển tiền</option>
                                                                <option value="03">Chuyển tiền nội bộ</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTenHTTT" style="width: 99%" class="inputflat" visible="false"
                                                                disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowTK_KH_NH">
                                                        <td>
                                                            <b>TK Chuyển</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTK_KH_NH" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTenTK_KH_NH" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowMaNT">
                                                        <td>
                                                            <b>Nguyên tệ</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtMaNT" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTenNT" style="width: 99%" class="inputflat" value="Việt Nam Đồng"
                                                                disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowSoDuNH" style=" display:none">
                                                        <td>
                                                            <b>Số dư NH</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtSoDu_KH_NH" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtDesc_SoDu_KH_NH" style="width: 99%" class="inputflat"
                                                                disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowNgay_KH_NH">
                                                        <td>
                                                            <b>Ngày chuyển</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtNGAY_KH_NH" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtDesNGAY_KH_NH" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                     <tr id="rowNH_Chuyen">
                                                        <td>
                                                            <b>Mã NH chuyển</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtMA_NH_A" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTen_NH_A" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                     <tr id="rowNH_TT">
                                                        <td>
                                                            <b>Mã MH TT</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtMA_NH_TT" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTEN_NH_TT" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                     <tr id="rowNH_TH">
                                                        <td>
                                                            <b>Ma NH TH</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtMA_NH_B" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTen_NH_B" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowLoaiThue">
                                                        <td>
                                                            <b>Loại thuế</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="ddlLoaiThue" class="inputflat" style="width: 99%" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtDescLoaiThue" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowToKhaiSo">
                                                        <td>
                                                            <b>Tờ khai số</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtToKhaiSo" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlToKhaiSo" runat="server" Width="99%" CssClass="inputflat">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr id="rowNgayDK">
                                                        <td>
                                                            <b>Ngày đăng ký</b>
                                                        </td>
                                                        <td colspan="2">
                                                            <input type="text" id="txtNgayDK" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowLHXNK">
                                                        <td>
                                                            <b>Loại hình XNK</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtLHXNK" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtDescLHXNK" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowSoKhung">
                                                        <td>
                                                            <b>Số khung</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtSoKhung" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtDescSoKhung" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowSoMay">
                                                        <td>
                                                            <b>Số máy</b>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtSoMay" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtDescSoMay" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowSoBK" style="display:none;">
                                                        <td>
                                                            <b>Số bảng kê</b>
                                                        </td>
                                                        <td colspan="2">
                                                            <input type="text" id="txtSo_BK" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="rowNgayBK" style="display:none;">
                                                        <td>
                                                            <b>Ngày bảng kê</b>
                                                        </td>
                                                        <td colspan="2">
                                                            <input type="text" id="txtNgay_BK" style="width: 99%" class="inputflat" disabled="true" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="visibility:hidden" >
                                        <td width="25%" style="height: 0px">
                                            <input type="text" id="txtSo_BT" class="inputflat" style="visibility: hidden" />
                                            <input type="text" class="inputflat" id="txtSoCT" disabled="true" style="visibility: hidden"
                                                backcolor="Aqua" />
                                        </td>
                                        <td width="51%" style="visibility:hidden"  >
                                            <input type="text" id="txtTRANG_THAI" class="inputflat" style="visibility:hidden"   />
                                            <input type="text" id="txtMA_NV" class="inputflat" style="visibility:hidden"   />
                                            <input type="text" id="txtNgayLV" class="inputflat" style="visibility:hidden" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' align="left" class="errorMessage" width='100%'>
                                            <table width='100%'>
                                                <tr>
                                                    <td align="left">
                                                        <div id="divStatus" style="font-weight: bold" />
                                                    </td>
                                                    <td align="right">
                                                        <div id="divDescCTU" style="font-weight: bold">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" width="100%">
                                            <div id='gridChitiet'>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' height='5'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" class='text'>
                                            <div id='divTienMat' style="width: 20px; height: 20px">
                                            </div>
                                        </td>
                                        <td align="right">
                                            <table width="500px">
                                                <tr align="right">
                                                    <td align="right" valign="top">
                                                        &nbsp;
                                                    </td>
                                                    <td align="right" valign="top">
                                                        Tổng Tiền: &nbsp;<input type="text" id='txtTTIEN' style="text-align:right" class="inputflat" disabled="true"
                                                            value="0" />
                                                    </td>
                                                </tr>
                                                <tr align="right" style="display:none">
                                                    <td align="right" valign="top">
                                                        &nbsp;
                                                    </td>
                                                    <td align="right" valign="top">
                                                        Số đã thu: &nbsp;<input type="text" id='txtTT_TTHU' class="inputflat"  value='0'
                                                            disabled="true" />
                                                            
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign="top">
                                                        <input type="button" id="cmdTraCuu"  value="Quay lại" class="ButtonCommand" onclick="jTraCuuCT();" />
                                                        &nbsp;
                                                        <input type="button" disabled="disabled" id="cmdInCT" value="[In CT]" class="ButtonCommand" onclick="jsIn_CT();" />   
                                                        <table>
                                                        <tr style="display:none ">
                                                        <td id="tdPwd" style="">
                                                        Mật khẩu &nbsp; <input type="password" id='txtPwd' class="inputflat" value='' size="10"  />
                                                        <input type="button"  visible="true" id="cmdHuy" value="[Hủy]" class="ButtonCommand" onclick="jsHuy_CT();" />
                                                        </td>
                                                        </tr>
                                                        </table>                                                     
                                                    </td>
                                                    
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan='2' height='5'>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>	
</asp:Content>
