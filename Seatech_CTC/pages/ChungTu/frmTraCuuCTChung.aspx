﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmTraCuuCTChung.aspx.vb" 
Inherits="pages_ChungTu_frmTraCuuCTChung" title="Tra cứu chứng từ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }

        function jsShowChiTietCT(_soct) {
            window.open("../../NTDT/frmChiTietCTNTDT.aspx?soct=" + _soct, "", "dialogWidth:800px;dialogHeight:650px;help:0;status:0;", "_new");
        }
    </script>
    <style type="text/css">
        .dataView{padding-bottom:10px}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">TRA CỨU CHỨNG TỪ</asp:Label>
            </td>
        </tr>
        <tr>
        <td>
    <div style="width:100%; margin-bottom:30px">
          <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" />
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width:35%">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                        onblur="CheckDate(this);" onfocus="this.select()" />
                </span></td>
            </tr>
              <tr>
                  <td align="left" class="form_label">Từ số chứng từ</td>
                  <td class="form_control">
                      <asp:TextBox ID="txtTuSo_CT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                  <td align="left" class="form_label">Đến số chứng từ</td>
                  <td class="form_control">
                      <asp:TextBox ID="txtDenSo_CT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
              </tr>
              <tr>
                  <td align="left" class="form_label">Mã số thuế</td>
                  <td class="form_control">
                      <asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                  <td align="left" class="form_label">Tên người nộp thuế</td>
                  <td class="form_control">
                      <asp:TextBox ID="txtTenNNT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
              </tr>
              <tr>
                  <td align="left" class="form_label">Loại thuế</td>
                  <td class="form_control">
                      <asp:DropDownList ID="drpLoaiThue" runat="server" Width="91%" CssClass="inputflat">
                      </asp:DropDownList></td>
                  <td align="left" class="form_label">Số tài khoản</td>
                  <td class="form_control">
                      <asp:TextBox ID="txtTKNH" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>

              </tr>
              <tr>
                  <td align="left" class="form_label">Trạng thái</td>
                  <td class="form_control">
                      <asp:DropDownList ID="drpTrangThai" runat="server" Width="91%" CssClass="inputflat">
                      </asp:DropDownList>
                      <asp:HiddenField ID="hdfMA_CN" runat="server" />
                  </td>

                  <td align="left" class="form_label">Chi nhánh</td>
                  <td class="form_control">
                      <asp:DropDownList ID="txtMaCN" runat="server" CssClass="inputflat"
                          AutoPostBack="false" Style="width: 90%; border-color: White; font-weight: bold; text-align: left;">
                      </asp:DropDownList>
                      <asp:HiddenField ID="HiddenField1" runat="server" />
                  </td>
              </tr>

              <tr>
                  <td align="left" class="form_label">Mã tham chiếu</td>
                  <td class="form_control">
                      <asp:TextBox ID="txtMaTC" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
              </tr>

              <tr>
                  <td align="center" colspan="4">
                      <asp:Button ID="btnTimKiem" runat="server" Text="Tìm kiếm" />
                      <asp:Button ID="btnExport" runat="server" Text="Export" />
                  </td>
              </tr>
          </table>
    </div>
    <div class="dataView">
        <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False" ShowFooter="true"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="True" PageSize='15'>
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%#Container.DataItemIndex + 1%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate><b>Tổng</b></FooterTemplate>
                    <FooterStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <%--<%-- Mã chi nhánh --%>
                <%--<asp:BoundField DataField="ID" HeaderText="Mã chi nhánh">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>--%>
                <%-- Tên chi nhánh --%>
                <%--<asp:BoundField DataField="NAME" HeaderText="Tên chi nhánh">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="REF_NO" HeaderText="Mã Ref">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>--%>
                <%--tuanda
                thêm cột Số CT core--%>
                <%-- số chứng từ core --%>
                <asp:BoundField DataField="SO_CTU_CORE" HeaderText="Số CT Core">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Số chứng từ">
                    <ItemTemplate>
						<%--<%#Eval("SO_CTU")%>--%>
						<a href="#" id="SO_CT" onclick="jsShowChiTietCT(<%# Eval("SO_CTU") %>)">
							<asp:Label ID="Label1" runat="server" Text='<%# Eval("SO_CTU") %>'></asp:Label>
						</a>
					</ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderTemplate>
                        Số chứng từ
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <asp:Label ID="totalCT" runat="server" ForeColor="Blue" ></asp:Label>
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <%-- Mã tham chiếu --%>
                <asp:BoundField DataField="MA_THAMCHIEU" HeaderText="Mã tham chiếu">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="MST_NNOP" HeaderText="Mã số thuế">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="TEN_NNOP" HeaderText="Tên người nộp thuế">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="STK_NHANG_NOP" HeaderText="Số tài khoản">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                 <asp:BoundField DataField="TEN_LTHUE" HeaderText="Loại thuế">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                 <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("MA_NGUYENTE")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <HeaderTemplate>
                        Nguyên tệ
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <div><span style="font-weight:bold;color:blue">VND</span>/<span style="font-weight:bold;color:blue">USD</span></div>
                         
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("TONG_TIEN", "{0:n2}")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Số tiền
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <asp:Label ID="totalAmount" runat="server" ForeColor="Blue"></asp:Label>
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:BoundField DataField="TEN_TTHAI_CTU" HeaderText="Trạng thái">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:GridView>
        <asp:GridView ID="grdExport" runat="server" AutoGenerateColumns="False" ShowFooter="true" Visible="false"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="false" >
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%#Container.DataItemIndex + 1%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate><b>Tổng</b></FooterTemplate>
                    <FooterStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <%--<%-- Mã chi nhánh --%>
                <asp:BoundField DataField="ID" HeaderText="Mã chi nhánh">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <%-- Tên chi nhánh --%>
                <asp:BoundField DataField="NAME" HeaderText="Tên chi nhánh">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="REF_NO" HeaderText="Mã Ref">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="SO_CTU_CORE" HeaderText="Số CT Core">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Số chứng từ">
                    <ItemTemplate>
                        <%#Eval("SO_CTU")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderTemplate>
                        Số chứng từ
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <asp:Label ID="totalCT" runat="server" ForeColor="Blue" ></asp:Label>
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <%-- Mã tham chiếu --%>
                <asp:BoundField DataField="MA_THAMCHIEU" HeaderText="Mã tham chiếu">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="MST_NNOP" HeaderText="Mã số thuế">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="TEN_NNOP" HeaderText="Tên người nộp thuế">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="STK_NHANG_NOP" HeaderText="Số tài khoản">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                 <asp:BoundField DataField="TEN_LTHUE" HeaderText="Loại thuế">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                 <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("MA_NGUYENTE")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <HeaderTemplate>
                        Nguyên tệ
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <div><span style="font-weight:bold;color:blue">VND</span>/<span style="font-weight:bold;color:blue">USD</span></div>
                         
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("TONG_TIEN", "{0:n2}")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Số tiền
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <asp:Label ID="totalAmount" runat="server" ForeColor="Blue"></asp:Label>
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:BoundField DataField="TEN_TTHAI_CTU" HeaderText="Trạng thái">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:GridView>
    </div>
    </td>
        </tr>
    </table>
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>	
</asp:Content>

