﻿<%@ Page Title="" Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="LapCTThueMoi.aspx.vb" Inherits="pages_ChungTu_LapCTThueMoi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/ChuyenSoSangChu.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script src="../../javascript/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>

    <script src="../../javascript/jquery/1.11.2/jquery-ui.min.js" type="text/javascript"></script>


    <script src="../../javascript/json/json2.js" type="text/javascript"></script>

    <script src="../../javascript/accounting.min.js" type="text/javascript"></script>

    <script src="../../javascript/object/chungTu.js" type="text/javascript"></script>

    <script src="../../javascript/table/paging.js" type="text/javascript"></script>
    <link href="../../javascript/table/paging.css" rel="stylesheet" type="text/css" />

    <%-- <script language="javascript" src="../../images/javascript/jquery/script/default/jquery-1.4.3.min.js"
        type="text/javascript"></script>--%>
    <%--    <link href="../../javascript/table/paging.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .inputflat {
            width: 96%;
        }

        .auto-style1 {
            height: 30px;
        }
    </style>
    <script src="../../javascript/accounting.min.js" type="text/javascript"></script>
    <script src="../../javascript/ChuyenSoSangChu.js" type="text/javascript"></script>
    <script type="text/javascript">
        var mv_Ma_NT = '';
        var _TTIEN = 0;
        var charNumberLimit = 210;



    </script>


    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {

            document.querySelector('#txtSHKB').onclick = function () {

                document.getElementById("txtTenKB").value = '';

            }
        }, false)
    </script>

    <script type="text/javascript">
        function jsFillMA_NH_A() {
            var cb = document.getElementById('ddlMA_NH_A');
            cb.options.length = 0;
            for (var i = 0; i < arrMA_NHA.length; i++) {
                var arr = arrMA_NHA[i].split(';');
                var value = arr[0];
                var label = arr[1];
                cb.options[cb.options.length] = new Option(label, value);
            }
        }
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        function jsNoiDungKT(txtMaMuc, txtNoiDung) {
            var userContext = {};
            userContext.ControlID = txtNoiDung;

            var strMaNDKT = document.getElementById(txtMaMuc).value;
            if (strMaNDKT.length > 0) {
                PageMethods.Get_TenTieuMuc(strMaNDKT, Get_NoiDungKT_Complete, Get_NoiDungKT_Error, userContext);
            }
        }

        function Get_NoiDungKT_Complete(result, userContext, methodName) {
            // checkSS();
            if (!jsCheckUserSession(result))
                return;
            if (result.length > 0) {
                document.getElementById(userContext.ControlID).value = result;
                document.getElementById('divStatus').innerHTML = "Tìm thấy nội dung kinh tế phù hợp";
            } else {
                document.getElementById('divStatus').innerHTML = "Không tìm thấy nội dung kinh tế phù hợp.Hãy tìm lại";
            }
        }
        function Get_NoiDungKT_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy nội dung kinh tế" + error.get_message();
            }
        }
        function jsFillLTHUE() {
            var cb = document.getElementById('ddlMaLoaiThue');
            var cb0 = document.getElementById('CboSearchLThue');
            var arr = arrMaLTHUE;
            $('#ddlMaLoaiThue').find('option').remove();
            $('#CboSearchLThue').find('option').remove();
            for (var i = 0; i < arrMaLTHUE.length; i++) {
                var arr = arrMaLTHUE[i].split(';');
                var value = arr[0];
                var label = arr[1];
                var opt = document.createElement('option');
                var opt1 = document.createElement('option');
                opt.value = value;
                opt.innerHTML = label;
                opt1.value = value;
                opt1.innerHTML = label;
                cb.appendChild(opt);
                cb0.appendChild(opt1);
            }
            jsChangLthue();
        }
        function jsShowHideGet_NNThue_HQ(bShow) {
            if (bShow) {
                document.getElementById('divKQTruyVan').style.display = '';
            } else {
                document.getElementById('divKQTruyVan').style.display = 'none';
            }
        }
        function fncFormatNumber(amount) {
            return localeNumberFormat(amount, '.');
        }

        function fncFormatNumberChiTiet(idAmount) {

            var amount = idAmount.value;
            var amount = amount.replaceAll('.', '');
            var hienthi = localeNumberFormat(amount, '.');
            idAmount.value = hienthi;
            //return localeNumberFormat(amount, '.');
        }

        function jsFormatNumber(txtCtl) {
            //var strData = $('#' + txtCtl).val();
            //console.log(strData);
            //strData = strData.replace(/./gi, ''); //replaceAll('.', '');
            //console.log(strData);
            //document.getElementById(txtCtl).value = localeNumberFormat(strData, '.');
            document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.', ''), '.');
        }
        function localeNumberFormat(amount, delimiter) {
            try {

                amount = parseFloat(amount);
            } catch (e) {
                throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
                return null;
            }
            if (delimiter == null || delimiter == undefined) { delimiter = '.'; }

            // convert to string
            if (amount.match != 'function') { amount = amount.toString(); }

            // validate as numeric
            var regIsNumeric = /[^\d,\.-]+/igm;
            var results = amount.match(regIsNumeric);

            if (results != null) {
                //outputText('INVALID NUMBER', eOutput)
                return null;
            }

            var minus = amount.indexOf('-') >= 0 ? '-' : '';
            amount = amount.replace('-', '');
            var amtLen = amount.length;
            var decPoint = amount.indexOf(',');
            var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

            var wholeNumber = amount.substr(0, wholeNumberEnd);

            var numberEnd = amount.substr(decPoint, amtLen);
            var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

            var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
            var rvsNumber = wholeNumber.split('').reverse().join('');
            var output = '';

            for (i = 0; i < wholeNumberEnd; i++) {
                if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
                output += rvsNumber.charAt(i);
            }
            output = minus + output.split('').reverse().join('') + fraction;
            return output;
        }

        String.prototype.replaceAll = function (strTarget, strSubString) {
            var strText = this;
            var intIndexOfMatch = strText.indexOf(strTarget);
            while (intIndexOfMatch != -1) {
                strText = strText.replace(strTarget, strSubString)
                intIndexOfMatch = strText.indexOf(strTarget);
            }
            return (strText);
        }
        function jsShowDivStatus(bShow, sMessage) {
            if (bShow) {
                document.getElementById('divStatus').style.display = '';
                document.getElementById('divStatus').innerHTML = sMessage;
            } else {
                document.getElementById('divStatus').style.display = 'none';
                document.getElementById('divStatus').innerHTML = sMessage;
            }
        }
        function jsShowHideDivProgress(bShow) {
            if (bShow) {
                document.getElementById('divProgress').style.display = '';
            } else {
                document.getElementById('divProgress').innerHTML = 'Đang xử lý. Xin chờ một lát...';
                document.getElementById('divProgress').style.display = 'none';
            }
        }
        function jsChangLthue() {
            var searchMaLthue = $('#CboSearchLThue').val();
            $("#rowS_Nhadat").css("display", "none");
            $("#rowS_Nhadat_CCCD").css("display", "none");
            $("#rowS_Nhadat_SOQD").css("display", "none");
            $("#rowS_Oto").css("display", "none");
            $("#rowTruyVanID").css("display", "none");
            if (searchMaLthue == '08') {
                document.getElementById('rowS_Nhadat').style.display = '';
                document.getElementById('rowS_Nhadat_CCCD').style.display = '';
                document.getElementById('rowS_Nhadat_SOQD').style.display = '';
            }
            if (searchMaLthue == '03') {
                $("#rowS_Oto").css("display", "");
            }
            if (searchMaLthue == '00') {
                $("#rowTruyVanMST").css("display", "none");
                $("#rowTruyVanID").css("display", "");
            } else {

                $("#rowTruyVanMST").css("display", "");
            }
            //txtSearch_MST
            $get('txtSearch_MST').value = '';

            $('#ddlMaLoaiThue').val(searchMaLthue);
            ShowHideControlByLoaiThue();
        }
        function jsChangeMA_NTK() {
            if (document.getElementById("ddlMa_NTK").value == "1") {
                document.getElementById('ddlMaTKCo').value = "7111";
            } else if ((document.getElementById("ddlMa_NTK").value == "2")) {
                document.getElementById('ddlMaTKCo').value = "3942";
            } else {
                document.getElementById('ddlMaTKCo').value = "";
            }
        }
        function jsGet_KBInfo() {
            var strSHKB = $get('txtSHKB').value;
            var strDBHC = '';
            var strCQThu = '';

            PageMethods.jsGet_KBInfo(strSHKB, jsGet_KBInfo_Complete, jsGet_KBInfo_Error);
        }
        function jsGet_KBInfo_Complete(result) {
            checkSS(result);
            //alert(result.toString());
            if (result.length > 0) {
                var tenKB = result;
                //alert(arr2[0]);
                $get('txtTenKB').value = tenKB;
                jsValidNHSHB_SP($get('txtSHKB').value);
                document.getElementById('divStatus').innerHTML = '';
            }
            else {
                $get('txtTenKB').value = '';

                document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin ngân hàng nhận.Hãy kiểm tra lại';
            }
        }
        function jsGet_KBInfo_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng nhận" + error.get_message();
            }
        }


        function jsGet_DBHC_KBInfo() {
            var strSHKB = $get('txtSHKB').value;
            var strDBHC = '';
            var strCQThu = '';

            PageMethods.jsGet_DBHC_KBInfo(strSHKB, jsGet_DBHC_KBInfo_Complete, jsGet_DBHC_KBInfo_Error);
        }
        function jsGet_DBHC_KBInfo_Complete(result) {
            checkSS(result);
            //alert(result.toString());
            if (result.length > 0) {
                var dbhc_kb = result;
                $get('txtMaDBHC').value = dbhc_kb;
                jsGet_TenDBHC();
                document.getElementById('divStatus').innerHTML = '';

            }
            else {
                $get('txtMaDBHC').value = '';

                document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin mã địa bàn hành chính kho bạc.Hãy kiểm tra lại';
            }
        }
        function jsGet_DBHC_KBInfo_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin mã địa bàn hành chính kho bạc" + error.get_message();
            }
        }


        //xxx)Lay thong tin tai ngan hang song phuong
        function jsValidNHSHB_SP(strSHKB) {
            if ($get('txtSHKB').value.length > 0) {
                PageMethods.ValidNHSHB_SP($get('txtSHKB').value.trim(), ValidNHSHB_SP_Complete, ValidNHSHB_SP_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy SHKB';
            }
        }

        function ValidNHSHB_SP_Complete(result, methodName) {
            jsShowHideDivProgress(false);
            if (result.length < 8) {
                document.getElementById("hndNHSHB").value = '';
            } else {
                document.getElementById("hndNHSHB").value = result;
                if (result.length > 0) {
                    $get('txtMA_NH_B').value = result;
                    jsGet_TenNH_B();
                }


            }
        }
        function ValidNHSHB_SP_Error(error, userContext, methodName) {
            if (error !== null) {

                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng hưởng" + error.get_message();
            }
        }

        //xxx)Lay thong tin tai ngan hang song phuong jsValidNHSHB_SP_GhiCtu
        function jsValidNHSHB_SP_GhiCtu(strSHKB) {
            if ($get('txtSHKB').value.length > 0) {
                PageMethods.ValidNHSHB_SP($get('txtSHKB').value.trim(), ValidNHSHB_SP_GhiCtu_Complete, ValidNHSHB_SP_GhiCtu_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy SHKB';
            }
        }

        function ValidNHSHB_SP_GhiCtu_Complete(result, methodName) {
            jsShowHideDivProgress(false);
            if (result.length < 8) {
                document.getElementById("hndNHSHB").value = '';
            } else {
                document.getElementById("hndNHSHB").value = result;
            }
        }
        function ValidNHSHB_SP_GhiCtu_Error(error, userContext, methodName) {
            if (error !== null) {

                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng hưởng" + error.get_message();
            }
        }

        function checkSS(param) {

            var arrKQ = param.split(';');
            if (arrKQ[0] == "ssF") {
                window.open("../../pages/Warning.html", "_self");
                return;
            }
        }

        function jsFillNguyenTe() {
            var cb = document.getElementById('cboNguyenTe');
            var arr = arrMaNT;
            cb.options.length = 0;
            for (var i = 0; i < arrMaNT.length; i++) {
                var arr = arrMaNT[i].split(';');
                var value = arr[0];
                var label = arr[1];
                cb.options[cb.options.length] = new Option(label, value);
            }
            $('#cboNguyenTe').val('VND');
        }
        function jsCheckUserSession(param) {
            var bRet = true;
            if (param) {
                var arrKQ = param.split(';');
                if (arrKQ[0] == "errUserSession") {
                    window.open("../../pages/Warning.html", "_self");
                    bRet = false;
                }
            }
            return bRet;
        }
        function ShowHideControlByPT_TT() {
            //rowTKGL
            var intPP_TT = document.getElementById("ddlMaHTTT").value;
            var rTK_KH_NH = document.getElementById("rowTK_KH_NH");
            var rSoDuNH = document.getElementById("rowSoDuNH");
            //var rTKGL = document.getElementById("rowTKGL");
            var rNHChuyen = document.getElementById("rowNHChuyen");
            var rNHNhan = document.getElementById("rowNHNhan");
            var rNHTT = document.getElementById("rowNHTT");
            var rTr1 = document.getElementById("rowNNTien");
            var rTr2 = document.getElementById("rowCMT");
            var rTr3 = document.getElementById("rowDChiNNTien");
            var rTr4 = document.getElementById("rowSDT");
            var rowHuyenNNTien = document.getElementById("rowHuyenNNTien");
            var rowTinhNNTien = document.getElementById("rowTinhNNTien");
            var rowMST_NNTIEN = document.getElementById("MST_NNTIEN");

            if (intPP_TT == '00') {
                rTK_KH_NH.style.display = '';
                rSoDuNH.style.display = '';
                //rTKGL.style.display = '';
                rNHChuyen.style.display = '';
                rNHTT.style.display = '';
                rNHNhan.style.display = '';
                rTr1.style.display = '';
                rTr2.style.display = '';
                rTr3.style.display = '';
                rTr4.style.display = '';
                rowTinhNNTien.style.display = '';
                rowHuyenNNTien.style.display = '';
                rowMST_NNTIEN.style.display = '';

            }
            if (intPP_TT == '01') {
                rTK_KH_NH.style.display = '';
                rSoDuNH.style.display = '';
                //rTKGL.style.display = 'none';

                rNHChuyen.style.display = '';
                rNHNhan.style.display = '';
                rNHTT.style.display = '';
                rTr1.style.display = '';
                rTr2.style.display = 'none';
                rTr3.style.display = '';
                rTr4.style.display = 'none';
                rowTinhNNTien.style.display = '';
                rowHuyenNNTien.style.display = '';
                rowMST_NNTIEN.style.display = 'none';

            }

            if (intPP_TT == '03') {
                rTK_KH_NH.style.display = 'none';
                rSoDuNH.style.display = 'none';
                //rTKGL.style.display = '';
                rNHChuyen.style.display = '';
                rNHNhan.style.display = '';
                rNHTT.style.display = '';
                rTr1.style.display = 'none';
                rTr2.style.display = 'none';
                rTr3.style.display = 'none';
                rTr4.style.display = 'none';
                rowTinhNNTien.style.display = 'none';
                rowHuyenNNTien.style.display = 'none';


            }

        }

        //function jsEnabledInputCharge() {
        //    var dblTongTien = parseFloat(accounting.unformat($('#txtTongTien').val()));
        //    var dblPhi = 0;
        //    var dblVAT = 0;
        //    var dblTongTrichNo = 0;
        //    if (document.getElementById('chkChargeType').checked) {
        //        $('#txtCharge').disabled = true;
        //        //neu ma checked thi tinh lai fee
        //        dblPhi = dblTongTien * feeRate / minFee;
        //        dblVAT = parseInt(dblTongTien * feeRate / minFee / 10);
        //        if (dblPhi < minFee) {
        //            dblPhi = minFee;
        //            //dblVAT=dblPhi/10;
        //            dblVAT = parseInt(dblPhi / 10);

        //        }
        //        dblTongTrichNo = dblPhi + dblVAT + dblTongTien;
        //        $('#txtCharge').val(accounting.formatNumber(dblPhi));
        //        $('#txtVat').val(accounting.formatNumber(dblVAT));

        //        $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo));

        //    } else {
        //        $('#txtCharge').prop('disabled', false);
        //        $('#txtCharge').val("0");
        //        $('#txtVat').val("0");
        //        //$('#txtTongTrichNo').val($('#txtTongTien').val());
        //        $('#txtTongTrichNo').val($('#txtTongTien').val());
        //    }
        //    jsConvertCurrToText();
        //}

        //document.getElementById("rowTKGL").style.display='none'; 
        function jsClearGridHeader(clearDefVal, clearMa_NNT) {
            //document.getElementById("rowTKGL").style.display = 'none';

            //if (clearDefVal == true) $get('txtSHKB').value = '';
            //if (clearDefVal == true) $get('txtTenKB').value = '';
            //if (clearMa_NNT == true) $get('txtMaNNT').value = '';
            //curSoCT = '';
            //curSo_BT = '';

            ////$get('txtSoCTTimKiem').value = '';
            //$get('txtTenNNT').value = '';
            //$get('txtDChiNNT').value = '';
            //$get('txtMaNNTien').value = '';
            //$get('txtTenNNTien').value = '';
            //$get('txtDChiNNTien').value = '';
            //$get('hdnSoCT_NH').value = '';
            //$get('hdnSO_CT').value = '';
            //$get('hdnTongTienCu').value = '';
            //$get('hdnSoLan_LapCT').value = '';
            //if (clearDefVal == true) $get('txtMaDBHC').value = '';
            //if (clearDefVal == true) $get('txtTenDBHC').value = '';
            //if (clearDefVal == true) $get('txtMaCQThu').value = '';
            //if (clearDefVal == true) $get('txtTenCQThu').value = '';


            //$get('txtCQQD').value = '';
            //$get('txtTenHTTT').value = '';
            //$get('txtTK_KH_NH').value = '';
            //$get('txtTKGL').value = '';
            //$get('txtTenTKGL').value = '';
            //$get('txtTenTK_KH_NH').value = '';
            //$get('txtSoDu_KH_NH').value = '';
            //$get('txtDesc_SoDu_KH_NH').value = '';
            ////$get('txtNGAY_KH_NH').value = '';
            //$get('txtDesNGAY_KH_NH').value = '';
            //$get('txtTenLoaiThue').value = '';
            //$get('txtToKhaiSo').value = '';
            //$get('txtNgayDK').value = '';
            //$get('txtLHXNK').value = '';
            //$get('txtDescLHXNK').value = '';
            //$get('txtSoKhung').value = '';
            //$get('txtDescSoKhung').value = '';
            //$get('txtSoMay').value = '';
            //$get('txtDescSoMay').value = '';
            //$get('txtSo_BK').value = '';
            //$get('txtNgayBK').value = '';
            //$get('txtQuan_HuyenNNTien').value = '';
            //$get('txtTinh_NNTien').value = '';
            //$get('txtKyHieuCT').value = '';
            ////$get('txtSanPham').value = '';
            //$get('txtQuan_HuyenNNTien').value = '';
            //$get('txtTenQuan_HuyenNNTien').value = '';
            //$get('txtHuyen_NNT').value = '';
            //$get('txtTenHuyen_NNT').value = '';
            //$get('txtTinh_NNTien').value = '';
            //$get('txtTenTinh_NNTien').value = '';
            //$get('txtTinh_NNT').value = '';
            //$get('txtTenTinh_NNT').value = '';
            //$get('txtCK_SoCMND').value = '';
            //$get('txtCK_SoDT').value = '';
            //document.getElementById('ddlMaHTTT').selectedIndex = 0;
            //document.getElementById('ddlMaLoaiThue').selectedIndex = 0;
            //document.getElementById("rowSHKB").style.display = 'none';
            //document.getElementById("rowKHCT").style.display = 'none';
            //document.getElementById("rowMaNNT").style.display = '';
            //document.getElementById("rowNNTien").style.display = 'none';
            //document.getElementById("rowDChiNNT").style.display = 'none';
            //document.getElementById("rowDChiNNTien").style.display = 'none';
            //document.getElementById("rowHuyen").style.display = 'none';
            //document.getElementById("rowTinh").style.display = 'none';
            //document.getElementById("rowHuyenNNTien").style.display = 'none';
            //document.getElementById("rowTinhNNTien").style.display = 'none';
            //document.getElementById("rowDBHC").style.display = 'none';
            //document.getElementById("rowCQThu").style.display = 'none';
            //document.getElementById("rowTKNo").style.display = 'none';
            //document.getElementById("rowTKCo").style.display = 'none';
            //document.getElementById("rowHTTT").style.display = 'none';
            //document.getElementById("rowTK_KH_NH").style.display = 'none';
            ////         document.getElementById("rowDSTK_KH").style.display='none';
            //document.getElementById("rowSoDuNH").style.display = 'none';
            //document.getElementById("rowNgay_KH_NH").style.display = 'none';
            ////07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
            //document.getElementById("rowNHChuyen").style.display = 'none';
            //document.getElementById("rowNHNhan").style.display = 'none';
            //document.getElementById("rowDienGiaiNHB").style.display = 'none';
            //document.getElementById("rowNHTT").style.display = 'none';
            ////end
            //document.getElementById("rowMaNT").style.display = 'none';
            //document.getElementById("rowLoaiThue").style.display = 'none';
            //document.getElementById("rowToKhaiSo").style.display = 'none';
            //document.getElementById("rowNgayDK").style.display = 'none';
            //document.getElementById("rowLHXNK").style.display = 'none';
            //document.getElementById("rowSoKhung").style.display = 'none';
            //document.getElementById("rowSoMay").style.display = 'none';
            //document.getElementById("rowSoBK").style.display = 'none';
            //document.getElementById("rowNgayBK").style.display = 'none';
            //document.getElementById("rowHuyen").style.display = 'none';
            //document.getElementById("rowTinh").style.display = 'none';
            //document.getElementById("rowTKGL").style.display = 'none';
            ////them 4 thong tin khi chon HTTT=CK
            //document.getElementById("rowNNTien").style.display = 'none';
            //document.getElementById("rowCMT").style.display = 'none';
            //document.getElementById("rowDChiNNTien").style.display = 'none';
            //document.getElementById("rowSDT").style.display = 'none';
            //document.getElementById("rowCQQD").style.display = 'none';
            //document.getElementById("rowSO_CQQD").style.display = 'none';
            //document.getElementById("rowNGAY_CQQD").style.display = 'none';
            //document.getElementById("rowLyDoHuy").style.display = 'none';
            //document.getElementById("rowLoaiNNT").style.display = 'none';
            ////sonmt
            //$get('txtMaLoaiNNT').value = '';
            //$get('txtTenLoaiNNT').value = '';

        }

        function jsTruyVanMST() {
            var searchMaLthue = $('#CboSearchLThue').val();
            //$('#ddlMaLoaiThue').val(searchMaLthue);
            //ShowHideControlByLoaiThue();

            var strSOHS = '';
            var strCCCD = '';
            var strSOQD = '';

            var ID_KHOANNOP = '';
            var LOAI_DL = '';

            if (searchMaLthue == '08') {
                if ($('#txtSearch_SOHS').val().length <= 0 && $('#txtSearch_SOQD').val().length <= 0) {
                    alert('Vui lòng nhập số hồ sơ hoặc số quyết định');
                    $('#txtSearch_SOHS').focus();
                    return;
                }
                strSOHS = $('#txtSearch_SOHS').val();
                strSOQD = $('#txtSearch_SOQD').val();

                //
                if ($('#txtSearch_MST').val().length <= 0) {
                    alert('Vui lòng nhập mã số thuế');
                    $('#txtSearch_MST').focus();
                    return;
                }

                if ($('#txtSearch_CCCD').val().length <= 0) {
                    alert('Vui lòng nhập số CMND/CCCD');
                    $('#txtSearch_CCCD').focus();
                    return;
                }
                strCCCD = $('#txtSearch_CCCD').val();

            }
            if (searchMaLthue == '03') {
                if ($('#txtSearch_SOTK').val().length <= 0) {
                    alert('Vui lòng nhập số tờ khai');
                    $('#txtSearch_SOTK').focus();
                    return;
                }
                strSOHS = $('#txtSearch_SOTK').val();
            }
            if (searchMaLthue == '01' || searchMaLthue == '02' || searchMaLthue == '05' || searchMaLthue == '06' || searchMaLthue == '07' || searchMaLthue == '08') {
                if ($('#txtSearch_MST').val().length <= 0) {
                    alert('Vui lòng nhập mã số thuế');
                    $('#txtSearch_MST').focus();
                    return;
                }
            }
            if (searchMaLthue == '00') {
                if ($('#txtSearch_ID_KHOANNOP').val().length <= 0) {
                    alert('Vui lòng nhập ID khoản nộp');
                    $('#txtSearch_ID_KHOANNOP').focus();
                    return;
                }
                ID_KHOANNOP = $('#txtSearch_ID_KHOANNOP').val();
            }

            jsClearGridHeader(true, true);
            jsClearHDR_Panel();
            $get('hdfDescCTu').value = '';
            jsClearGridDetail();
            //jsLoadCTList();
            jsShowDivStatus(false, "");
            jsShowHideDivProgress(true);
            //clear thong tin
            jsClearTableHDR();
            jsEnableGridHeader(false);
            jsEnableGridDetail(false);
            jsEnableButtons(0);
            $('#cmdGhiKS').prop('disabled', false);

            //truy van ma so thue
            var strMST = $('#txtSearch_MST').val();
            $('#hdfHQResult').val('');
            //jsLoadCTList();
            PageMethods.jsTruyVanMST(searchMaLthue, ID_KHOANNOP, strMST, LOAI_DL, strSOQD, strSOHS,strCCCD, jsTruyVanMST_Complete, jsTruyVanMST_Error);
        }
        function jsTruyVanMST_Complete(pData) {
            jsShowHideGet_NNThue_HQ(true);
            try {
                if (!jsCheckUserSession(pData))
                    return;
                if (pData.length <= 0) {
                    jsShowDivStatus(true, 'Có lỗi khi truy vấn');
                    jsShowHideDivProgress(false);
                    return;
                }
                $('#hdfHQResult').val(pData);
                var objResult = JSON.parse(pData);
                //doan nay fill thong tin nguoi nop thue
                //txtMaNNT
                $('#txtMaNNT').val(objResult.MST);
                //txtTenNNT
                $('#txtTenNNT').val(objResult.TEN_NNT);
                //txtMaLoaiNNT
                $('#txtMaLoaiNNT').val(objResult.LOAI_NNT);
                //txtDChiNNT
                $('#txtDChiNNT').val(objResult.DIA_CHI_NNT);
                //txtQuan_HuyenNNTien
                $('#txtQuan_HuyenNNTien').val(objResult.MA_HUYEN_NNT);
                //txtTenQuan_HuyenNNTien
                $('#txtTenQuan_HuyenNNTien').val(objResult.TEN_HUYEN_NNT);
                //txtTinh_NNTien
                $('#txtTinh_NNTien').val(objResult.MA_TINH_NNT);
                //txtTenTinh_NNTien
                $('#txtTenTinh_NNTien').val(objResult.TEN_TINH_NNT);

                jsGetLoaiNNT();
                jsBindKQTruyVanGrid(objResult);
                jsShowDivStatus(false, '');
                jsShowHideDivProgress(false);
                //var searchMaLthue = $('#CboSearchLThue').val();
                //$('#ddlMaLoaiThue').val(searchMaLthue);
                ShowHideControlByLoaiThue();
                // console.log(objResult);
                //luu data
            } catch (e) {
                jsShowDivStatus(true, 'Có lỗi khi truy vấn');
                jsShowHideDivProgress(false);
                //var searchMaLthue = $('#CboSearchLThue').val();
                //$('#ddlMaLoaiThue').val(searchMaLthue);
                ShowHideControlByLoaiThue();
            }
        }


        function jsTruyVanMST_Error() {
            //$('#divStatus').html();
            jsShowDivStatus(true, 'Có lỗi khi truy vấn');
            jsShowHideDivProgress(false);
            //var searchMaLthue = $('#CboSearchLThue').val();
            //$('#ddlMaLoaiThue').val(searchMaLthue);
            ShowHideControlByLoaiThue();

        }


        function jsFillPTTT() {
            var cb = document.getElementById('ddlMaHTTT');
            cb.options.length = 0;
            for (var i = 0; i < arrPTTT.length; i++) {
                var arr = arrPTTT[i].split(';');
                var value = arr[0];
                var label = arr[1];
                cb.options[cb.options.length] = new Option(label, value);
            }
        }
        function valInteger(obj) {
            var i, strVal, blnChange;
            blnChange = false
            strVal = "";

            for (i = 0; i < (obj.value).length; i++) {
                switch (obj.value.charAt(i)) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9": strVal = strVal + obj.value.charAt(i);
                        break;
                    default: blnChange = true;
                        break;
                }
            }
            if (blnChange) {
                obj.value = strVal;
            }
        }
        function jsCheckFormatKyThue(strKyThue) {
            //console.log(strKyThue);
            if (strKyThue.length <= 0)
                return;
            strKyThue = strKyThue.trim().toUpperCase();
            if (strKyThue.length < 10 || strKyThue.length > 10) {
                alert("Định dạng trường kỳ thuế không hợp lệ!");
                return false;
            }
            if (Validate_Format(strKyThue) == true) {
                if (validateTime(strKyThue) == true) {

                } else {
                    var arrKTthue = strKyThue.split("/");
                    var formatNQ = arrKTthue[0].toString() + "/" + arrKTthue[1].toString();
                    var formatNam = arrKTthue[2].toString();

                    var A = ['00/01', '00/02', '00/03', '00/04', '00/05', '00/06', '00/07', '00/08', '00/09', '00/10', '00/11', '00/12', '00/Q1', '00/Q2', '00/Q3', '00/Q4', '00/K1', '00/K2', '00/K3', '00/K4', '00/K5', '00/K6', '00/K7', '00/K8', '00/K9', '00/CN', '00/QT'];
                    var i;
                    var checkmatch = 0;
                    for (i = 0; i < A.length; i++) {
                        if (formatNQ == A[i].toString()) {
                            checkmatch += 1;
                        }
                    }
                    // console.log(checkmatch);
                    if (checkmatch > 0) {
                        if (typeof formatNam === 'number') {
                            // yes it is numeric

                            if (formatNam % 1 === 0) {
                                // yes it's an integer.
                            }
                            else {
                                alert("Định dạng trường kỳ thuế không hợp lệ!");
                                return false;
                            }
                        }
                    } else {

                        alert("Định dạng trường kỳ thuế không hợp lệ!");
                        return false;
                    }

                }

            } else {

                alert("Định dạng trường kỳ thuế không hợp lệ!");
                return false;
            }

            return true;

        }
        function checkMaNNT(obj) {
            if ((obj.value.trim()).length != 0 && (obj.value.trim()).length != 10 && (obj.value.trim()).length != 14) {
                obj.value = "";
                obj.focus();
                alert("Mã số thuế không hợp lệ!");
                return;
            }
        }

        function jsCheckTK_TienMat(soTaiKhoan) {
            //tk_tienmat
            // alert(soTaiKhoan);
            if (soTaiKhoan.length > 8) {
                var endSoTK = soTaiKhoan.substring(soTaiKhoan.length - 8, soTaiKhoan.length);
                var arr = tk_tienmat.split(';');
                //alert(tk_tienmat);
                for (var i = 0; i < arr.length; i++) {
                    //alert(arr[i]);
                    //alert(endSoTK);
                    if (arr[i] == endSoTK) {
                        return true;
                    }
                }
            } else {
                return false;
            }
            return false;
        }
    </script>
    <script type="text/javascript">

        function jsLoadCTList() {
            var sSoCtu = $('#txtSoCTTimKiem').val();
            if (sSoCtu == 'undefined' || sSoCtu == '' || sSoCtu == null)
                sSoCtu = '';
            PageMethods.LoadCTList(curMa_NV, sSoCtu, LoadCTList_Complete, LoadCTList_Error);
        }

        function LoadCTList_Complete(result, methodName) {
            if (result.length > 0) {
                jsCheckUserSession(result);
                document.getElementById('divDSCT').innerHTML = result;
            }
            try {
                $('#grdDSCT').paging({ limit: 15 });
            } catch (err) {
                paginationList(15);
            }

        }

        function LoadCTList_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
        function paginationList(listPaginationCount) {
            $('table#grdDSCT').each(function () {
                console.log("1.numPerPage:" + numPerPage)
                var currentPage = 0;
                var numPerPage = listPaginationCount;
                console.log("2.numPerPage:" + numPerPage)
                var $pager = $('.pager').remove();
                var $paging = $("#paging").remove();
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div id="pager"></div>');
                console.log("numRows:" + numRows)
                console.log("numPages:" + numPages)
                console.log("listPaginationCount:" + listPaginationCount)
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');

                }

                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });
        }

        function jsGetCTU(sSoCT) {
            jsShowDivStatus(false, "");
            jsShowHideDivProgress(true);
            jsShowHideGet_NNThue_HQ(false);
            //clear data
            $('#hdfSo_CTu').val('');
            $('#hdfIsEdit').val(false);
            $('#hdfDescCTu').val('');

            $('#hdfHQResult').val('');
            $('#hdfHDRSelected').val('');
            $('#hdfNgayKB').val('');

            jsBindKQTruyVanGrid(null);
            jsClearHDR_Panel();
            jsClearDTL_Grid()
            jsBindDTL_Grid(null, null);

            //NOT: null, undefined, 0, NaN, false, or ""
            if (sSoCT) {
                $.ajax({
                    type: "POST",
                    url: "frmLapCTTruocBaCaNhan.aspx/GetCTU",
                    cache: false,
                    data: JSON.stringify({
                        "sSoCT": sSoCT
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: GetCtuHQ_OnSuccess,
                    error: function (request, status, error) {
                        jsShowDivStatus(true, request.responseText);
                        var err;
                        err = JSON.parse(request.responseText);
                        if (err) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            jsShowDivStatus(true, err.Message);
                        }

                        jsCalCharacter();
                        //TinhPhi();
                        jsShowHideDivProgress(false);
                    }
                });
            }
        }

        function GetCtuHQ_OnSuccess(response) {
            if (response != null) {
                //Load data

                checkTinhPhi = 1;
                if (typeof response !== 'undefined' && response !== null) {
                    if (response.d) {
                        if (!jsCheckUserSession(response.d)) {
                            return;
                        }
                        var objCTu = JSON.parse(response.d);
                        if (objCTu) {
                            //Bind HDR panel
                            var HDR = objCTu.HDR;
                            if (typeof HDR !== 'undefined' && HDR !== null) {
                                if (HDR.PT_TINHPHI == 'O') {
                                    chkChargeTypePN.checked = true
                                }
                                else {
                                    chkChargeTypeMP.checked = true
                                }
                                //Bind hidden field
                                $('#hdfSo_CTu').val(HDR.So_CT);
                                $('#hdfNgayKB').val(HDR.Ngay_KB);
                                curCtuStatus = HDR.Trang_Thai;
                                $('#hdfIsEdit').val(true);
                                var sDescCTu = "SHKB/SoCT/SoBT" + ":" + HDR.SHKB + "/" + HDR.So_CT + "/" + HDR.So_BT;
                                $('#hdfDescCTu').val(sDescCTu);
                                curSoCT = HDR.So_CT
                                jsEnableDisableCtrlBtnByTThaiCTu(curCtuStatus, HDR.TT_CTHUE);

                                $('#txtQuan_HuyenNNTien').val(HDR.QUAN_HUYEN_NNTIEN);

                                jsGetTenQuanHuyen(HDR.QUAN_HUYEN_NNTIEN);

                                jsBindKQTruyVanGrid(objCTu.DTL);
                                jsShowDivStatus(false, '');
                                jsShowHideDivProgress(false);

                                ShowHideControlByLoaiThue();
                            }
                            if (curCtuStatus == "03" || curCtuStatus == "00" || curCtuStatus == "99") {
                                jsEnableGridHeader(false);
                                jsEnableGridDetail(false);
                            } else {
                                jsEnableGridHeader(true);
                                jsEnableGridDetail(true);
                            }




                            jsBindHDR_Panel(HDR);
                            $('#hdfIsEdit').val(true);
                            //Bind dtl grid
                            //console.log("1xxxx");
                            var listDTL = objCTu.ListDTL;
                            //console.log("2xxxx");
                            jsBindDTL_Grid(HDR, listDTL);
                        }
                    }
                }
            }
            //TinhPhi();
            jsCalCharacter();
            TotalMoney();
            //jsConvertCurrToText();
            jsShowHideDivProgress(false);
            jsLoadCTList();
            //var bIsEdit = $('#hdfIsEdit').val();
            //console.log("EDIT: " + bIsEdit)
        }
        function jsClearHDR_Panel() {

            curSoCT = '';
            curSo_BT = '';
            var searchMaLthue = $('#CboSearchLThue').val();
            $('#ddlMaLoaiThue').val(searchMaLthue);
            ShowHideControlByLoaiThue();

            //document.getElementById('ddlMaLoaiThue').selectedIndex = 0;
            $get('txtMaNNT').value = '';
            $get('txtTenNNT').value = '';
            $get('txtMaLoaiNNT').value = '';
            $get('txtTenLoaiNNT').value = '';
            $get('txtDChiNNT').value = '';
            $get('txtQuan_HuyenNNTien').value = '';
            $get('txtTenQuan_HuyenNNTien').value = '';
            $get('txtTinh_NNTien').value = '';
            $get('txtTenTinh_NNTien').value = '';
            $get('txtMaCQThu').value = '';
            $get('txtTenCQThu').value = '';
            $get('txtSHKB').value = '';
            $get('txtTenKB').value = '';
            document.getElementById('ddlMaHTTT').selectedIndex = 0;
            $get('txtTK_KH_NH').value = '';
            // $get('txtTKGL').value = '';
            //$get('txtTenTKGL').value = '';
            $get('txtTenTK_KH_NH').value = '';
            $get('txtSoDu_KH_NH').value = '';
            $get('txtDesc_SoDu_KH_NH').value = '';
            $get('txtMaNNTien').value = '';
            $get('txtTenNNTien').value = '';
            $get('txtDChiNNTien').value = '';
            $get('txtHuyen_NNT').value = '';
            $get('txtTenHuyen_NNT').value = '';
            $get('txtTinh_NNTien').value = '';
            $get('txtTinh_NNT').value = '';
            $get('txtTenTinh_NNT').value = '';
            $get('txtCK_SoCMND').value = '';
            $get('txtCK_SoDT').value = '';
            $get('txtToKhaiSo').value = '';

            $get('txtMA_NH_TT').value = '';
            $get('txtTEN_NH_TT').value = '';
            $get('txtMA_NH_B').value = '';
            $get('txtTenMA_NH_B').value = '';
            $get('txtDG_NHB').value = '';
            $get('txtSoKhung').value = '';
            $get('txtDacDiemPhuongTien').value = '';
            $get('txtSoMay').value = '';
            $get('txtSo_BK').value = '';

            $get('txtSOQD').value = '';
            $get('txtNgayQD').value = '';

            $get('txtTongTien').value = '';
            $get('txtTongTrichNo').value = '';


            //$get('hdfDescCTu').value = '';
            $get('hdfIsEdit').value = '';
            $get('hdfSo_CTu').value = '';
            $get('hdfSoBT').value = '';
            $get('hdfNgayKB').value = '';
            $get('hdnTongTienCu').value = '';


            document.getElementById("rowSoKhung").style.display = 'none';
            document.getElementById("rowSoMay").style.display = 'none';
            document.getElementById("rowDacDiemPhuongTien").style.display = 'none';
            //document.getElementById("rowTKGL").style.display = 'none';

            document.getElementById("rowSO_CQQD").style.display = 'none';
            document.getElementById("rowNGAY_CQQD").style.display = 'none';

        }
        function jsClearDTL_Grid() {
            for (var i = 1; i < 5; i++) {
                $('#chkRemove' + i).prop("checked", false);
                $('#ID_KHOANNOP_' + i).val('');
                $('#MA_CHUONG_' + i).val('');
                $('#MA_MUC_' + i).val('');
                $('#SOTIEN_' + i).val('');
                $('#MA_DBHC_' + i).val('');
                $('#KY_THUE_' + i).val('');
                $('#NOI_DUNG_' + i).val('');
            }
        }
        function jsEnableGridHeader(blnEnable) {
            //if( document.getElementById('txtMaNNT').value==defMaVangLai)
            //{
            document.getElementById('txtTenNNT').disabled = blnEnable;
            //}
            document.getElementById('cboNguyenTe').disabled = blnEnable;
            document.getElementById('ddlMaHTTT').disabled = blnEnable;
            document.getElementById('ddlMaLoaiThue').disabled = blnEnable;
            document.getElementById('ddlMaTKCo').disabled = blnEnable;
            document.getElementById('ddlMaTKNo').disabled = blnEnable;
            document.getElementById('ddlMa_NTK').disabled = blnEnable;
            document.getElementById('txtTKNo').disabled = blnEnable;
            document.getElementById('txtTKCo').disabled = 'none';
            //document.getElementById('grdHeader').disabled=blnEnable;                         
            //kienvt : enabled control in disabled grdheader
            //document.getElementById('txtVat2').disabled = blnEnable;
            document.getElementById('txtTenLoaiNNT').disabled = blnEnable;
            document.getElementById('txtMaLoaiNNT').disabled = blnEnable;

            document.getElementById('txtCharge').disabled = blnEnable;
            document.getElementById('txtCharge2').disabled = blnEnable;
            //document.getElementById('txtSanPham').disabled = blnEnable;

            document.getElementById('ddlMa_NTK').disabled = blnEnable;
            //document.getElementById('ddlMaTinhThanh').disabled = blnEnable;
            //document.getElementById('ddlMaQuanHuyen').disabled = blnEnable;
            $get('txtCQQD').disabled = blnEnable;
            $get('txtSOQD').disabled = blnEnable;
            $get('txtNgayQD').disabled = blnEnable;
            $get('txtSHKB').disabled = blnEnable;
            $get('txtMaNNT').disabled = blnEnable;
            $get('txtTenNNT').disabled = blnEnable;
            $get('txtDChiNNT').disabled = blnEnable;
            $get('txtMaNNTien').disabled = blnEnable;
            $get('txtTenNNTien').disabled = blnEnable;
            $get('txtDChiNNTien').disabled = blnEnable;
            $get('txtQuan_HuyenNNTien').disabled = blnEnable;
            $get('txtHuyen_NNT').disabled = blnEnable;
            $get('txtTenHuyen_NNT').disabled = blnEnable;
            $get('txtTinh_NNT').disabled = blnEnable;
            $get('txtTenTinh_NNT').disabled = blnEnable;
            $get('txtTenQuan_HuyenNNTien').disabled = blnEnable;
            $get('txtTenTinh_NNTien').disabled = blnEnable;
            $get('txtTinh_NNTien').disabled = blnEnable;
            $get('txtMA_NH_TT').disabled = blnEnable;
            $get('txtMA_NH_B').disabled = blnEnable;
            $get('txtMaDBHC').disabled = blnEnable;
            $get('txtMaCQThu').disabled = blnEnable;
            $get('txtTK_KH_NH').disabled = blnEnable;
            //         $get('ddlDSTK_KH').disabled = blnEnable;
            //$get('txtTKGL').disabled = blnEnable;
            $get('txtNGAY_KH_NH').disabled = blnEnable;
            $get('txtDesNGAY_KH_NH').disabled = blnEnable;

            $get('txtSoKhung').disabled = blnEnable;
            $get('txtSoMay').disabled = blnEnable;
            $get('txtDacDiemPhuongTien').disabled = blnEnable;

            $get('chkChargeTypeMP').disabled = blnEnable;
            $get('chkChargeTypePT').disabled = blnEnable;
            $get('chkChargeTypePN').disabled = blnEnable;
            document.getElementById('ddlMaHTTT').disabled = blnEnable;
            document.getElementById('ddlMaLoaiThue').disabled = blnEnable;
            document.getElementById('ddlMA_NH_A').disabled = blnEnable;

            document.getElementById('txtMaNNTien').disabled = blnEnable;
            document.getElementById('txtTenNNTien').disabled = blnEnable;
            document.getElementById('txtCK_SoCMND').disabled = blnEnable;
            document.getElementById('txtDChiNNTien').disabled = blnEnable;
            document.getElementById('txtDG_NHB').disabled = blnEnable;
            document.getElementById('txtCK_SoDT').disabled = blnEnable;

        }

        function jsEnableGridDetail(blnEnable) {
            //document.getElementById('grdChiTiet').disabled=blnEnable;
            //Kienvt:Sua lai enabled grid thi an tung dieu kihen
            $get('chkRemove1').disabled = blnEnable;
            $get('MA_CHUONG_1').disabled = blnEnable;
            $get('MA_KHOAN_1').disabled = blnEnable;
            $get('MA_MUC_1').disabled = blnEnable;
            $get('NOI_DUNG_1').disabled = blnEnable;
            $get('SOTIEN_1').disabled = blnEnable;
            $get('KY_THUE_1').disabled = blnEnable;

            $get('chkRemove2').disabled = blnEnable;
            $get('MA_CHUONG_2').disabled = blnEnable;
            $get('MA_KHOAN_2').disabled = blnEnable;
            $get('MA_MUC_2').disabled = blnEnable;
            $get('NOI_DUNG_2').disabled = blnEnable;
            $get('SOTIEN_2').disabled = blnEnable;
            $get('KY_THUE_2').disabled = blnEnable;

            $get('chkRemove3').disabled = blnEnable;
            $get('MA_CHUONG_3').disabled = blnEnable;
            $get('MA_KHOAN_3').disabled = blnEnable;
            $get('MA_MUC_3').disabled = blnEnable;
            $get('NOI_DUNG_3').disabled = blnEnable;
            $get('SOTIEN_3').disabled = blnEnable;
            $get('KY_THUE_3').disabled = blnEnable;

            $get('chkRemove4').disabled = blnEnable;
            $get('MA_CHUONG_4').disabled = blnEnable;
            $get('MA_KHOAN_4').disabled = blnEnable;
            $get('MA_MUC_4').disabled = blnEnable;
            $get('NOI_DUNG_4').disabled = blnEnable;
            $get('SOTIEN_4').disabled = blnEnable;
            $get('KY_THUE_4').disabled = blnEnable;

        }
        function jsBindHDR_Panel(obj) {
            jsClearHDR_Panel();

            if (typeof obj !== 'undefined' && obj !== null) {

                $('#txtMaNNT').val(obj.Ma_NNThue);
                $('#txtTenNNT').val(obj.Ten_NNThue);
                $('#txtDChiNNT').val(obj.DC_NNThue);


                $('#txtHuyen_NNT').val(obj.Ma_Huyen);
                $('#txtTinh_NNT').val(obj.Ma_Tinh);

                $('#txtQuan_HuyenNNTien').val(obj.QUAN_HUYEN_NNTIEN);
                //$('#txtTenQuan_HuyenNNTien').val(obj.QUAN_HUYEN_NNTIEN);
                $('#txtTinh_NNTien').val(obj.TINH_TPHO_NNTIEN);
                //$('#txtTenTinh_NNTien').val(obj.TINH_TPHO_NNTIEN);

                jsGetTenQuanHuyen_NNTien(obj.QUAN_HUYEN_NNTIEN);
                jsGetTenTinh_NNTien(obj.TINH_TPHO_NNTIEN);
                $('#txtMaCQThu').val(obj.Ma_CQThu);
                $('#txtTenCQThu').val(obj.Ten_cqthu);
                $('#txtSHKB').val(obj.SHKB);
                $('#txtTenKB').val(obj.Ten_kb);
                $('#ddlMa_NTK').val(obj.MA_NTK);
                $('#ddlMaTKCo').val(obj.TK_Co);

                if (typeof obj.PT_TT !== 'undefined' && obj.PT_TT !== null && obj.PT_TT !== '') {
                    $('#ddlMaHTTT').val(obj.PT_TT);
                }

                $('#txtTK_KH_NH').val(obj.TK_KH_NH);
                $('#txtTenTK_KH_NH').val(obj.TEN_KH_NH);
                //Tai khoan GL
                //$('#txtTKGL').val(obj.TK_GL_NH);
                $('#txtMaNNTien').val(obj.Ma_NNTien);
                $('#txtTenNNTien').val(obj.Ten_NNTien);
                $('#txtDChiNNTien').val(obj.DC_NNTien);
                $('#ddlMaLoaiThue').val(obj.Ma_LThue);
                //console.log("txtMaDBHC:" + obj.Ma_Xa);
                $('#txtMaDBHC').val(obj.Ma_Xa);/*change de lay len ten dia ban hanh chinh*/
                //$('#txtTenDBHC').val(jsGetTTTinhThanhKB(obj.Ma_Xa));  //jsGet_TenDBHC()
                jsGet_TenDBHC();
                $('#ddlMA_NH_A').val(obj.MA_NH_A);
                $('#txtMA_NH_B').val(obj.MA_NH_B);
                $('#txtTenMA_NH_B').val(obj.Ten_NH_B);
                $('#txtMA_NH_TT').val(obj.MA_NH_TT);
                $('#txtTEN_NH_TT').val(obj.TEN_NH_TT);
                $('#txtSoKhung').val(obj.So_Khung);
                $('#txtSoMay').val(obj.So_May);
                $('#txtDacDiemPhuongTien').val(obj.DAC_DIEM_PTIEN);


                $('#txtMaHS').val(obj.MA_HS);
                $('#txtSoThuaDat').val(obj.SO_THUADAT);
                $('#txtSoToBanDo').val(obj.SO_TO_BANDO);
                $('#txtDiaChiTS').val(obj.DIA_CHI_TS);
                $('#txtDBHCTS').val(obj.MA_DBHC_TS);
                $('#txtTenDBHCTS').val(obj.TEN_DBHC_TS);
                $('#txtMA_GCN').val(obj.MA_GCN);

                jsGetTenQuanHuyenNNT(obj.Ma_Huyen);
                ShowHideControlByLoaiThue();

                $('#txtSOQD').val(obj.So_QD);
                $('#txtNgayQD').val(obj.Ngay_QD);
                //
                //$('#txtTongTien').val(accounting.formatNumber(obj.TTien));
                //if (obj.Ma_NT != 'VND')
                //    $('#txtTongTienNT').val(accounting.formatNumber(obj.TTien_NT, 2, ",", "."));
                //else
                //    $('#txtTongTienNT').val(accounting.formatNumber(obj.TTien_NT));


                _TTIEN = obj.TTien;
                $('#txtTongTien').val(fncFormatNumber(obj.TTien));



                //jsFormatNumber('txtTongTien');
                //jsFormatNumber('txtTongTrichNo');
                //jsConvertCurrToText($("#txtTongTien").val().replaceAll('.', ''));

                $('#txtVat').val(accounting.formatNumber(obj.PHI_VAT));
                $('#txtCharge').val(accounting.formatNumber(obj.PHI_GD));

                $('#txtVat2').val(accounting.formatNumber(obj.PHI_VAT2));
                $('#txtCharge2').val(accounting.formatNumber(obj.PHI_GD2));

                //
                $('#txtCK_SoCMND').val(obj.SO_CMND);
                $('#txtCK_SoDT').val(obj.SO_FONE);
                $('#txtLyDoHuy').val(obj.Ly_Do);

                $('#hdfSo_CTu').val(obj.So_CT);
                //

                $('#txtMaLoaiNNT').val(obj.LOAI_NNT);
                $('#txtTenLoaiNNT').val(obj.TEN_LOAI_NNT);

                jsGetLoaiNNT();

                $('#txtNGAY_HT').val(obj.Ngay_HT);
                $('#cboNguyenTe').val(obj.Ma_NT);
                //$('#txtTyGia').val(obj.Ty_Gia);
                //$('#ddlMaLoaiTienThue').val(obj.LOAI_TT);

                //console.log("obj.NGAY_KH_NH: " + obj.NGAY_KH_NH);
                $('#txtNGAY_KH_NH').val(obj.NGAY_KH_NH);
                $('#txtDesNGAY_KH_NH').val('');




                $('#txtTK_KH_NH').val(obj.TK_KH_NH);
                $('#txtTenTK_KH_NH').val(obj.TEN_KH_NH);


                jsGetTK_KH_NH();

                //Fill Combobox Phi

                var arrSp = obj.SAN_PHAM.split('|');
                $get('cboPhiKiemDem').value = arrSp[0];
                $get('cboPhiSanPham').value = arrSp[1];

                $('#hdfSoBT').val(obj.So_BT);
            }
        }

        function jsBindDTL_Grid(objHDR, arrDTL) {
            jsClearDTL_Grid();
            //console.log("1yyyy");
            if (typeof arrDTL !== 'undefined' && arrDTL !== null && arrDTL instanceof Array && arrDTL.length > 0) {
                //console.log("2yyyy");
                for (var i = 0; i < arrDTL.length; i++) {
                    // console.log(arrDTL);
                    if (arrDTL[i]) { //NOT: null, undefined, 0, NaN, false, or ""
                        //chkRemove1
                        //console.log("4xxxx");
                        $('#chkRemove' + (i + 1)).prop("checked", true);
                        $('#ID_KHOANNOP_' + (i + 1)).val(arrDTL[i].ID_KHOANNOP);
                        $('#MA_CHUONG_' + (i + 1)).val(arrDTL[i].Ma_Chuong);
                        $('#MA_MUC_' + (i + 1)).val(arrDTL[i].Ma_TMuc);
                        $('#NOI_DUNG_' + (i + 1)).val(arrDTL[i].Noi_Dung);
                        $('#SOTIEN_' + (i + 1)).val(fncFormatNumber(arrDTL[i].SoTien));
                        $('#MA_DBHC_' + (i + 1)).val(arrDTL[i].MA_DBHC);
                        $('#KY_THUE_' + (i + 1)).val(arrDTL[i].Ky_Thue);

                    }

                }
            }
            //jsFormatNumberDTL_Grid();
        }

        function jsGetTTTinhThanhKB(sMaTinh) {
            try {
                if (sMaTinh != "") {
                    PageMethods.GetTTTinhThanhKB(sMaTinh, GetTTTinhThanhKB_Complete, GetTTTinhThanhKB_Error);
                }

            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTTTinhThanhKB_Complete(result, methodName) {
            try {
                jsCheckUserSession(result);
                if (result.length > 0) {
                    var arr2 = result.split(';');
                    $('#txtMaDBHC').val(arr2[0]);
                    $('#txtTenDBHC').val(arr2[1]);
                }
                else {
                    $('#txtMaDBHC').val('');
                    $('#txtTenDBHC').val('');
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin tỉnh thành kho bạc.Hãy kiểm tra lại';
                }
                jsCalCharacter();
            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTTTinhThanhKB_Error(error, userContext, methodName) {
            try {
                $('#txtMaDBHC').val('');
                $('#txtTenDBHC').val('');

                if (error !== null) {
                    document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin tỉnh thành kho bạc : " + error.get_message();
                }
                jsCalCharacter();
            } catch (ex) {
                alert(ex.message);
            }
        }

        function jsGetTenQuanHuyen_NNThue(ma_xa) {
            //var ma_xa = $('#txtHuyen').val();
            //var ma_xa1 = $get('txtHuyen_NNT').value
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenQuanHuyen(ma_xa, GetTenQuanHuyenNNThue_Complete, GetTenQuanHuyenNNThue_Error);
                jsGetTenTinh_NNThue(ma_xa);
            } else {
                document.getElementById('txtHuyen_NNT').value = "";
                document.getElementById('txtTenHuyen_NNT').value = "";
                document.getElementById('txtTinh_NNT').value = "";
                document.getElementById('txtTenTinh_NNT').value = "";
                jsCalCharacter();
            }
        }

        function GetTenQuanHuyenNNThue_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Quan = result;
                document.getElementById('txtTenHuyen_NNT').value = ten_Quan;
            } else {
                alert('Mã Quận người nộp thuế không chính xác. Vui lòng nhập lại.');
                document.getElementById('txtHuyen_NNT').focus();
                document.getElementById('txtHuyen_NNT').value = "";
                document.getElementById('txtTenHuyen_NNT').value = "";
                document.getElementById('txtTinh_NNT').value = "";
                document.getElementById('txtTenTinh_NNT').value = "";
            }
            jsCalCharacter();
        }

        function GetTenQuanHuyenNNThue_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp thuế không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtHuyen_NNT').focus();
            document.getElementById('txtHuyen_NNT').value = "";
            document.getElementById('txtTenHuyen_NNT').value = "";
            document.getElementById('txtTinh_NNT').value = "";
            document.getElementById('txtTenTinh_NNT').value = "";
            jsCalCharacter();
        }

        function jsGetTenTinh_NNThue(ma_xa) {
            //var ma_xa = $('#txtHuyen_NNT').val();
            $('#txtTinh_NNT').val('');
            $('#txtTenTinh_NNT').val('');
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenTinh(ma_xa, GetTenTinhNNThue_Complete, GetTenTinhNNThue_Error);
            }
        }

        function GetTenTinhNNThue_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtTinh_NNT').value = ten_Tinh[0];
                document.getElementById('txtTenTinh_NNT').value = ten_Tinh[1];
            }
            jsCalCharacter();
        }

        function GetTenTinhNNThue_Error(error, userContext, methodName) {
            jsCalCharacter();
        }

        function jsGetTenQuanHuyen_NNTien(ma_xa) {
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenQuanHuyen(ma_xa, GetTenQuanHuyenNNTien_Complete, GetTenQuanHuyenNNTien_Error);
                jsGetTenTinh_NNTien(ma_xa);
            } else {
                document.getElementById('txtQuan_HuyenNNTien').value = "";
                document.getElementById('txtTenQuan_HuyenNNTien').value = "";
                document.getElementById('txtTinh_NNTien').value = "";
                document.getElementById('txtTenTinh_NNTien').value = "";
            }
        }

        function GetTenQuanHuyenNNTien_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Quan = result;
                document.getElementById('txtTenQuan_HuyenNNTien').value = ten_Quan;
            } else {
                alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
                document.getElementById('txtQuan_HuyenNNTien').focus();
                document.getElementById('txtQuan_HuyenNNTien').value = "";
                document.getElementById('txtTenQuan_HuyenNNTien').value = "";
                document.getElementById('txtTinh_NNTien').value = "";
                document.getElementById('txtTenTinh_NNTien').value = "";
            }
        }

        function GetTenQuanHuyenNNTien_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtQuan_HuyenNNTien').focus();
            document.getElementById('txtQuan_HuyenNNTien').value = "";
            document.getElementById('txtTenQuan_HuyenNNTien').value = "";
            document.getElementById('txtTinh_NNTien').value = "";
            document.getElementById('txtTenTinh_NNTien').value = "";
        }

        function jsGetTenTinh_NNTien(ma_xa) {
            //var ma_xa = $('#txtHuyen_NNT').val();
            $('#txtTinh_NNTien').val('');
            $('#txtTenTinh_NNTien').val('');
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenTinh(ma_xa, GetTenTinhNNTien_Complete, GetTenTinhNNTien_Error);
            }
        }

        function GetTenTinhNNTien_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtTinh_NNTien').value = ten_Tinh[0];
                document.getElementById('txtTenTinh_NNTien').value = ten_Tinh[1];
            }
            jsCalCharacter();
        }
        function GetTenTinhNNTien_Error(error, userContext, methodName) {
            jsCalCharacter();
        }

        function jsEnableDisableCtrlBtnByTThaiCTu(sTThaiCTu, sTThaiCThue) {
            switch (sTThaiCTu) {
                case "00": //đã hoàn thiện
                    jsEnableDisableControlButton("6");
                    //jsEnableDisableAddRowButton("1");

                    $('#cmdGhiKS').prop('disabled', false);

                    $('#cmdInCT').prop('disabled', true);
                    $('#btnInPhieuHachToan').prop('disabled', true);

                    break;
                case "01": //KS thành công
                    if (sTThaiCThue == "1") { //gửi thuế thành công => cho phép gửi điều chỉnh
                        jsEnableDisableControlButton("10");
                        //jsEnableDisableAddRowButton("1");
                    } else { //
                        jsEnableDisableControlButton("3");
                        //jsEnableDisableAddRowButton("0");

                        $('#cmdInPhieuHachToan').prop('disabled', false);
                    }
                    $('#cmdInCT').prop('disabled', false);
                    //$('#btnInPhieuHachToan').prop('disabled', false);

                    break;
                case "02": //GDV Hủy sau khi hoàn thiện (ko cần qua KSV duyệt)
                    jsEnableDisableControlButton("1");
                    $('#cmdInCT').prop('disabled', true);
                    $('#btnInPhieuHachToan').prop('disabled', true);
                    //jsEnableDisableAddRowButton("0");
                    break;
                case "03": //Chuyển trả
                    jsEnableDisableControlButton("9");
                    $('#cmdInCT').prop('disabled', true);
                    $('#btnInPhieuHachToan').prop('disabled', true);
                    //jsEnableDisableAddRowButton("1");
                    break;
                case "04": //Hủy
                    jsEnableDisableControlButton("8");
                    $('#cmdInCT').prop('disabled', true);
                    $('#btnInPhieuHachToan').prop('disabled', true);
                    //jsEnableDisableAddRowButton("0");
                    break;
                case "05": //đã chuyển kiểm soát
                    jsEnableDisableControlButton("8");
                    //jsEnableDisableAddRowButton("0");
                    $('#cmdInCT').prop('disabled', false);
                    $('#btnInPhieuHachToan').prop('disabled', true);
                    break;
            }
        }

        function jsEnableDisableControlButton(sCase) {
            switch (sCase) {
                case "0": //disable all button
                    $('#cmdThemMoi').prop('disabled', true);
                    $('#cmdGhiKS').prop('disabled', true);
                    //$('#cmdChuyenKS').prop('disabled', true);
                    //$('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', true);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "1": //cmdThemMoi - btnAddRowDTL
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    //$('#cmdChuyenKS').prop('disabled', true);
                    //$('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', true);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "2": //cmdThemMoi - cmdChuyenKS - cmdInCT - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    //$('#cmdChuyenKS').prop('disabled', false);
                    //$('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "3": //cmdThemMoi - cmdInCT - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    //$('#cmdChuyenKS').prop('disabled', true);
                    //$('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "4": //cmdThemMoi - cmdGhiKS - cmdChuyenKS
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', false);
                    //$('#cmdChuyenKS').prop('disabled', false);
                    //$('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', true);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "5": //cmdThemMoi - cmdGhiKS - cmdChuyenKS - cmdInCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', false);
                    //$('#cmdChuyenKS').prop('disabled', false);
                    //$('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', true);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "6": //cmdThemMoi - cmdChuyenKS - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    //$('#cmdChuyenKS').prop('disabled', false);
                    //$('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', false);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "7": //cmdThemMoi - cmdChuyenKS - cmdInCT - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    //$('#cmdChuyenKS').prop('disabled', false);
                    //$('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "8": //cmdThemMoi - cmdInCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    // $('#cmdChuyenKS').prop('disabled', true);
                    //$('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', true);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "9": //cmdThemMoi - cmdGhiKS - cmdChuyenKS - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', false);
                    //$('#cmdChuyenKS').prop('disabled', false);
                    // $('#cmdDieuChinh').prop('disabled', true);
                    $('#cmdInCT').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', false);
                    //$('#cmdInPhieuHachToan').prop('disabled', true);
                    break;
                case "10": //cmdThemMoi - cmdDieuChinh - cmdInCT - cmdHuyCT
                    $('#cmdThemMoi').prop('disabled', false);
                    $('#cmdGhiKS').prop('disabled', true);
                    //$('#cmdChuyenKS').prop('disabled', true);
                    //$('#cmdDieuChinh').prop('disabled', false);
                    $('#cmdInCT').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    //$('#cmdInPhieuHachToan').prop('disabled', false);
                    break;

                default:
                    break;
            }
        }

        function jsConvertCurrToText(TTien_TN) {
            PageMethods.ConvertCurrToText(TTien_TN, ConvertCurrToText_Complete, ConvertCurrToText_Error);
        }
        function ConvertCurrToText_Complete(result, methodName) {
            checkSS(result);
            if (result.length > 0) {
                document.getElementById('divBangChu').innerHTML = 'Bằng chữ : ' + result;
            }
        }
        function ConvertCurrToText_Error(error, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình tính phí" + error.get_message();
            }
        }
        function TotalMoney() {
            //parseFloat(accounting.unformat($('#txtCharge').val()));
            var dblVAT = parseFloat($('#txtVat').val().replaceAll('.', ''));
            var dblPhi = parseFloat($('#txtCharge').val().replaceAll('.', ''));

            var dblVAT2 = parseFloat($('#txtVat2').val().replaceAll('.', ''));
            var dblPhi2 = parseFloat($('#txtCharge2').val().replaceAll('.', ''));

            var dblTongTien = parseFloat($('#txtTongTien').val().replaceAll('.', ''));
            //dblPhi=dblPhi.toString().replaceAll('.','')

            var dblTongTrichNo = parseFloat(dblPhi) + parseFloat(dblVAT) + parseFloat(dblPhi2) + parseFloat(dblVAT2) + parseFloat(dblTongTien);
            //console.log(dblTongTrichNo);
            $('#txtTongTrichNo').val(fncFormatNumber(dblTongTrichNo));
            // jsFormatNumber('txtTongTrichNo');

        }
        function ShowHideControlByLoaiThue() {
            var intLoaiThue = document.getElementById("ddlMaLoaiThue").value;
            var rSoKhung = document.getElementById("rowSoKhung");
            var rSoMay = document.getElementById("rowSoMay");
            var rCQQD = document.getElementById("rowCQQD");
            var rowSO_CQQD = document.getElementById("rowSO_CQQD");
            var rowNGAY_CQQD = document.getElementById("rowNGAY_CQQD");
            var rowDacDiemPhuongTien = document.getElementById("rowDacDiemPhuongTien");
            // var rToKhaiSo = document.getElementById("rowToKhaiSo");
            // var rNgayDK = document.getElementById("rowNgayDK");
            // var rLHXNK = document.getElementById("rowLHXNK");

            var rMaHS = document.getElementById("rowMaHS");
            var rSoTD_ToBanDo = document.getElementById("rowSoTD_ToBanDo");
            var rDiaChiTS = document.getElementById("rowDiaChiTS");
            var rDBHCTS = document.getElementById("rowDBHCTS");
            var rGCN = document.getElementById("rowGCN");


            if (intLoaiThue == '01') {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rowDacDiemPhuongTien.style.display = 'none';
                //   rToKhaiSo.style.display = 'none'; 
                //  rNgayDK.style.display = 'none';
                //  rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = 'none';
                rowNGAY_CQQD.style.display = 'none';

                rMaHS.style.display = 'none';
                rSoTD_ToBanDo.style.display = 'none';
                rDiaChiTS.style.display = 'none';
                rDBHCTS.style.display = 'none';
                rGCN.style.display = 'none';
            }
            else if (intLoaiThue == '02') {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rowDacDiemPhuongTien.style.display = 'none';
                //  rToKhaiSo.style.display = 'none';
                //  rNgayDK.style.display = 'none';
                //   rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = 'none';
                rowNGAY_CQQD.style.display = 'none';

                rMaHS.style.display = 'none';
                rSoTD_ToBanDo.style.display = 'none';
                rDiaChiTS.style.display = 'none';
                rDBHCTS.style.display = 'none';
                rGCN.style.display = 'none';
            }
            else if (intLoaiThue == '05') {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rowDacDiemPhuongTien.style.display = 'none';
                //   rToKhaiSo.style.display = 'none';
                //   rNgayDK.style.display = 'none';
                //  rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = 'none';
                rowNGAY_CQQD.style.display = 'none';

                rMaHS.style.display = 'none';
                rSoTD_ToBanDo.style.display = 'none';
                rDiaChiTS.style.display = 'none';
                rDBHCTS.style.display = 'none';
                rGCN.style.display = 'none';
            }
            else if (intLoaiThue == '06') {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rowDacDiemPhuongTien.style.display = 'none';
                //  rToKhaiSo.style.display = 'none';
                //rNgayDK.style.display = 'none';
                //    rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = 'none';
                rowNGAY_CQQD.style.display = 'none';

                rMaHS.style.display = 'none';
                rSoTD_ToBanDo.style.display = 'none';
                rDiaChiTS.style.display = 'none';
                rDBHCTS.style.display = 'none';
                rGCN.style.display = 'none';
            }
            else if (intLoaiThue == '03')//"03"
            {
                rSoKhung.style.display = '';
                rSoMay.style.display = '';
                rowDacDiemPhuongTien.style.display = '';
                //  rToKhaiSo.style.display = 'none';
                //  rNgayDK.style.display = 'none';
                //   rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = '';
                rowNGAY_CQQD.style.display = '';

                rMaHS.style.display = 'none';
                rSoTD_ToBanDo.style.display = 'none';
                rDiaChiTS.style.display = 'none';
                rDBHCTS.style.display = 'none';
                rGCN.style.display = 'none';
            }
            else if (intLoaiThue == '07') {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rowDacDiemPhuongTien.style.display = 'none';
                //   rToKhaiSo.style.display = 'none';
                //  rNgayDK.style.display = 'none';
                //  rLHXNK.style.display = 'none';
                rCQQD.style.display = '';
                rowSO_CQQD.style.display = '';
                rowNGAY_CQQD.style.display = '';

                rMaHS.style.display = 'none';
                rSoTD_ToBanDo.style.display = 'none';
                rDiaChiTS.style.display = 'none';
                rDBHCTS.style.display = 'none';
                rGCN.style.display = 'none';
            }
            else if (intLoaiThue == '08')//"08" thuế TB nhà đất
            {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rowDacDiemPhuongTien.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = '';
                rowNGAY_CQQD.style.display = '';

                rMaHS.style.display = '';
                rSoTD_ToBanDo.style.display = '';
                rDiaChiTS.style.display = '';
                rDBHCTS.style.display = '';
                rGCN.style.display = '';

            }
        }
        function jsBindKQTruyVanGrid(objParam) {

            var top = [];
            top.push("<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdKQTruyVan' style='width: 100%; border-collapse: collapse;'>");
            top.push("<tr class='grid_header'>");
            top.push("<td align='center' style='width: 5%;'>Chọn</td>");
            top.push("<td align='center' style='width: 5%;'>STT</td>");
            top.push("<td align='center' style='width: 5%;'>Thứ tự ưu tiên</td>");
            top.push("<td align='center' style='width: 5%;'>ID Khoản nộp</td>");
            top.push("<td align='center' style='width: 15%;'>CQT</td>");
            top.push("<td align='center' style='width: 15%;'>kho bạc</td>");
            top.push("<td align='center'>Nội dung</td>");
            top.push("<td align='center' style='width: 7%;'>TK kho bạc</td>");
            top.push("<td align='center' style='width: 10%;'>Số tiền</td>");
            top.push("</tr>");

            var arrObject = [];
            var content = [];
            if (typeof objParam !== 'undefined' && objParam !== null) {
                arrObject = objParam.ROW_SOTHUE;
                var iCount = 0;
                if (typeof arrObject !== 'undefined' && arrObject !== null && arrObject instanceof Array) {
                    for (var i = 0; i < arrObject.length; i++) {
                        if (arrObject[i]) { //NOT: null, undefined, 0, NaN, false, or ""
                            iCount++;
                            content.push("<tr>");

                            content.push("<td style='text-align: center;'><input type='checkbox' id='HDR_chkSelect_" + iCount + "' onclick='jsCheckTKhai(" + iCount + ");' />");
                            content.push("<input type='hidden' id='HDR_TEN_CQTHU_" + iCount + "' value='" + arrObject[i].TEN_CQTHU + "' />");
                            content.push("<input type='hidden' id='HDR_NOI_DUNG_" + iCount + "' value='" + arrObject[i].TEN_TMUC + "' />");
                            content.push("<input type='hidden' id='HDR_KY_THUE_" + iCount + "' value='" + arrObject[i].KY_THUE + "' />");
                            content.push("<input type='hidden' id='HDR_MA_CHUONG_" + iCount + "' value='" + arrObject[i].CHUONG + "' />");
                            content.push("</td>");
                            content.push("<td><input type='hidden' id='HDR_STT_" + iCount + "' value='" + iCount + "' />" + iCount + "</td>");
                            content.push("<td><input type='hidden' id='HDR_THUTU_UUTIEN_" + iCount + "' value='" + arrObject[i].THUTU_UUTIEN + "' />" + arrObject[i].THUTU_UUTIEN + "</td>");
                            content.push("<td><input type='hidden' id='HDR_ID_KHOANNOP_" + iCount + "' value='" + arrObject[i].ID_KHOANNOP + "' />" + arrObject[i].ID_KHOANNOP + "</td>");
                            content.push("<td><input type='hidden' id='HDR_MA_CQT_" + iCount + "' value='" + arrObject[i].MA_CQTHU + "' />" + arrObject[i].MA_CQTHU + "-" + arrObject[i].TEN_CQTHU + "</td>");
                            content.push("<td><input type='hidden' id='HDR_SHKB_" + iCount + "' value= '" + arrObject[i].SHKB + "' />" + arrObject[i].SHKB + "-" + arrObject[i].TEN_KB + "</td>");
                            content.push("<td><input type='hidden' id='HDR_MA_TMUC_" + iCount + "' value='" + arrObject[i].TIEU_MUC + "' />" + arrObject[i].TIEU_MUC + "-" + arrObject[i].TEN_TMUC + "</td>");
                            content.push("<td><input type='hidden' id='HDR_TKKB_" + iCount + "' value='" + arrObject[i].TK_NSNN + "' />" + arrObject[i].TK_NSNN + "</td>");
                            content.push("<td align='right'><input type='hidden' id='HDR_MA_NT_" + iCount + "' value='" + arrObject[i].LOAI_TIEN + "' />" +
                                "<input type='hidden' id='HDR_KY_THUE_" + iCount + "' value='" + arrObject[i].KY_THUE + "'/>" +
                                 "<input type='hidden' id='HDR_NOI_DUNG_" + iCount + "' value='" + arrObject[i].TEN_TMUC + "'/>" +
                                 "<input type='hidden' id='HDR_NO_CUOI_KY_" + iCount + "' value='" + arrObject[i].SO_TIEN + "' />" + fncFormatNumber(arrObject[i].SO_TIEN) + "</td>");
                            content.push("<td align='right'><input type='hidden' id='HDR_SO_KHUNG_" + iCount + "' value='" + arrObject[i].SO_KHUNG + "' />" +
                               "<input type='hidden' id='HDR_SO_MAY_" + iCount + "' value='" + arrObject[i].SO_MAY + "'/>" +
                                "<input type='hidden' id='HDR_DAC_DIEM_PTIEN_" + iCount + "' value='" + arrObject[i].DACDIEM_PTIEN + "'/>" +
                                 "<input type='hidden' id='HDR_SO_QDINH_" + iCount + "' value='" + arrObject[i].SO_QDINH_TBAO + "'/>" +
                                  "<input type='hidden' id='HDR_NGAY_QDINH_" + iCount + "' value='" + arrObject[i].NGAY_QUYET_DINH + "'/>" +
                                    "<input type='hidden' id='HDR_MA_DBHC_" + iCount + "' value='" + arrObject[i].MA_DBHC + "' />" +
                                     "<input type='hidden' id='HDR_MA_XA_HSKT_" + iCount + "' value='" + arrObject[i].MA_XA_HSKT + "' />" +
                                    "<input type='hidden' id='HDR_MA_HS_" + iCount + "' value='" + arrObject[i].MA_HS + "' />" +
                                    "<input type='hidden' id='HDR_SO_THUADAT_" + iCount + "' value='" + arrObject[i].SO_THUADAT + "' />" +
                                    "<input type='hidden' id='HDR_SO_TO_BANDO_" + iCount + "' value='" + arrObject[i].SO_TO_BANDO + "' />" +
                                    "<input type='hidden' id='HDR_DIA_CHI_TS_" + iCount + "' value='" + arrObject[i].DIA_CHI_HSKT + "' />" +
                                    "<input type='hidden' id='HDR_SO_" + iCount + "' value='" + arrObject[i].SO_GIAYTO_HSKT + "' />" +
                                "<input type='hidden' id='HDR_MA_GCN_" + iCount + "' value='" + arrObject[i].MA_GCN + "' />" +
                            "<input type='hidden' id='HDR_CQUAN_TQUYEN_" + iCount + "' value='" + arrObject[i].CQUAN_TQUYEN + "' />" +
                            "<input type='hidden' id='HDR_TEN_CQUAN_TQUYEN_" + iCount + "' value='" + arrObject[i].TEN_CQUAN_TQUYEN + "' />" +
                            "<input type='hidden' id='HDR_LOAI_THUE_" + iCount + "' value='" + arrObject[i].LOAI_THUE + "' />" +
                            "<input type='hidden' id='HDR_TEN_LOAI_THUE_" + iCount + "' value='" + arrObject[i].TEN_LOAI_THUE + "' /></td>");
                            content.push("</tr>");
                        }
                    }
                }
            }

            var bottom = "</table>";
            $('#divResultTruyVan').html(top.join("") + content.join("") + bottom);
            //jsFormatNumberKQTruyVanGrid();
        }
        function jsCheckTKhai(param) {
            //NEU LA CHECKED
            if ($('#HDR_chkSelect_' + param).is(":checked")) {
                var strMA_CQTHU = $('#HDR_MA_CQT_' + param).val();
                var strSHKB = $('#HDR_SHKB_' + param).val();
                //  console.log(strSHKB);
                var strMA_TMUC = $('#HDR_MA_TMUC_' + param).val();
                var strTK_KB = $('#HDR_TKKB_' + param).val();
                var strMA_NT = $('#HDR_MA_NT_' + param).val();
                var strMaCHuong = $('#HDR_MA_CHUONG_' + param).val().trim();
                var strNO_CUOI_KY = $('#HDR_NO_CUOI_KY_' + param).val().trim();
                var strKY_THUE = $('#HDR_KY_THUE_' + param).val().trim();
                var strNOI_DUNG = $('#HDR_NOI_DUNG_' + param).val().trim();

                var strID_KHOANNOP = $('#HDR_ID_KHOANNOP_' + param).val().trim();
                var strMA_DBHC = $('#HDR_MA_DBHC_' + param).val().trim();
                //so sanh voi cai hien co tren man hinh

                //lấy thông tin số khung, số máy, đặc điểm phương tiện, mã địa bàn với lệnh thuế trước bạ
                var strSoKhung = $('#HDR_SO_KHUNG_' + param).val();
                var strSoMay = $('#HDR_SO_MAY_' + param).val();
                var strDacDiemPhuongTien = $('#HDR_DAC_DIEM_PTIEN_' + param).val();
                var strMaDBHCTS = $('#HDR_MA_XA_HSKT_' + param).val();
                var strSoQuyetDinh = $('#HDR_SO_QDINH_' + param).val();
                var strNgayQuyetDinh = $('#HDR_NGAY_QDINH_' + param).val();

                //lấy thông tin thuế TB nhà đất
                var strMaHS = $('#HDR_MA_HS_' + param).val();
                var strSO_THUADAT = $('#HDR_SO_THUADAT_' + param).val();
                var strSO_TO_BANDO = $('#HDR_SO_TO_BANDO_' + param).val();
                var strDIA_CHI_TS = $('#HDR_DIA_CHI_TS_' + param).val();
                var strMA_GCN = $('#HDR_MA_GCN_' + param).val();

                var strCMND = $('#HDR_SO_' + param).val();


                if ($('#txtMaCQThu').val().length > 0) {
                    if (strMA_CQTHU != $('#txtMaCQThu').val()) {
                        alert('Không thể thêm tiểu mục do có KBNN khác với các tiểu mục đã chọn!');
                        $('#HDR_chkSelect_' + param).attr('checked', false);
                        return;
                    }
                } else {
                    $('#txtMaCQThu').val(strMA_CQTHU);
                    $('#txtTenCQThu').val($('#HDR_TEN_CQTHU_' + param).val());
                }
                if ($('#txtSHKB').val().length > 0) {
                    if (strSHKB != $('#txtSHKB').val()) {
                        alert('Không thể thêm tiểu mục do có KBNN khác với các tiểu mục đã chọn!');
                        $('#HDR_chkSelect_' + param).attr('checked', false);
                        return;
                    }
                } else {
                    $('#txtSHKB').val(strSHKB);
                    jsGet_KBInfo();
                    jsGet_DBHC_KBInfo();
                }
                //tai khoan thu ngan sach
                if ($('#ddlMaTKCo').val().length > 0) {
                    if (strTK_KB != $('#ddlMaTKCo').val()) {
                        alert('Không thể thêm tiểu mục do có tài khoản KBNN khác với các tiểu mục đã chọn!');
                        $('#HDR_chkSelect_' + param).attr('checked', false);
                        return;
                    }
                } else {

                    if (strTK_KB.sub(0, 2) == '35' || strTK_KB.sub(0, 2) == '39' || strTK_KB.sub(0, 2) == '37')
                        $('#ddlMa_NTK').val('2');
                    else if (strTK_KB.sub(0, 2) == '89')
                        $('#ddlMa_NTK').val('3');
                    else
                        $('#ddlMa_NTK').val('1');
                    $('#ddlMaTKCo').val(strTK_KB);
                    // $('#ddlMaTKCo').val($('#HDR_TEN_CQTHU_' + param).val());
                }
                if (mv_Ma_NT != '') {
                    if (mv_Ma_NT != strMA_NT) {
                        alert('Không thể thêm tiểu mục do có Mã nguyên tệ khác với các tiểu mục đã chọn!');
                        $('#HDR_chkSelect_' + param).attr('checked', false);
                        return;
                    }
                } else {
                    mv_Ma_NT = strMA_NT;
                    $('#cboNguyenTe').val(strMA_NT);
                }
                //thêm cho thuế trước bạ ở đây
                $('#txtSoKhung').val(strSoKhung);
                $('#txtSoMay').val(strSoMay);
                $('#txtDacDiemPhuongTien').val(strDacDiemPhuongTien);
                $('#txtSOQD').val(strSoQuyetDinh);
                $('#txtNgayQD').val(strNgayQuyetDinh);

                // thên thuế TB nhà đất ở đây
                $('#txtMaHS').val(strMaHS);
                $('#txtSoThuaDat').val(strSO_THUADAT);
                $('#txtSoToBanDo').val(strSO_TO_BANDO);
                $('#txtDiaChiTS').val(strDIA_CHI_TS);
                $('#txtDBHCTS').val(strMaDBHCTS);
                $('#txtTenDBHCTS').val('');
                $('#txtMA_GCN').val(strMA_GCN);

                $('#txtCK_SoCMND').val(strCMND);

                jsGetTTNganHangNhan();

                //do du lieu dtl
                var x = 0;
                for (var i = 1; i < 5; i++) {
                    var strMaMucx = $('#MA_MUC_' + i).val().trim();
                    if (strMaMucx == '') {
                        $('#MA_CHUONG_' + i).val(strMaCHuong);
                        $('#ID_KHOANNOP_' + i).val(strID_KHOANNOP);
                        $('#MA_MUC_' + i).val(strMA_TMUC);
                        $('#SOTIEN_' + i).val(fncFormatNumber(strNO_CUOI_KY));
                        $('#KY_THUE_' + i).val(strKY_THUE);
                        $('#MA_DBHC_' + i).val(strMA_DBHC);
                        $('#NOI_DUNG_' + i).val(strNOI_DUNG);
                        x = i;
                        break;
                    } else {
                        if (strMaMucx == strMA_TMUC) {
                            alert('Không thể thêm tiểu mục do trùng dòng chi tiết trên chứng từ với các tiểu mục đã chọn!');
                            $('#HDR_chkSelect_' + param).attr('checked', false);
                            return;
                        }
                    }
                }
                //don du lieu dtl

                //neu x ==0 thi thuong bao qua so dong chi tiet
                if (x == 0) {
                    alert('Không thể thêm tiểu mục do Vượt quá số dòng chi tiết trên chứng từ với các tiểu mục đã chọn!');
                    $('#HDR_chkSelect_' + param).attr('checked', false);
                    return;
                }


            } else {
                //neu bo check
                //xoa dtl truoc
                var strMA_CQTHU = $('#HDR_MA_CQT_' + param).val();
                var strSHKB = $('#HDR_SHKB_' + param).val();
                //  console.log(strSHKB);
                var strMA_TMUC = $('#HDR_MA_TMUC_' + param).val();
                var strTK_KB = $('#HDR_TKKB_' + param).val();
                var strMA_NT = $('#HDR_MA_NT_' + param).val();
                var strMaCHuong = $('#HDR_MA_CHUONG_' + param).val().trim();
                var strNO_CUOI_KY = $('#HDR_NO_CUOI_KY_' + param).val().trim();
                var strKY_THUE = $('#HDR_KY_THUE_' + param).val().trim();

                var strID_KHOANNOP = $('#HDR_ID_KHOANNOP_' + param).val().trim();
                var strMA_DBHC = $('#HDR_MA_DBHC_' + param).val().trim();

                var x = 0;
                for (var i = 1; i < 5; i++) {
                    var strMaMucx = $('#MA_MUC_' + i).val().trim();
                    if (strMaMucx == strMA_TMUC) {
                        $('#ID_KHOANNOP_' + i).val('');
                        $('#MA_CHUONG_' + i).val('');
                        $('#MA_MUC_' + i).val('');
                        $('#SOTIEN_' + i).val('');
                        $('#MA_DBHC_' + i).val('');
                        $('#KY_THUE_' + i).val('');
                        $('#NOI_DUNG_' + i).val('');
                        break;
                    }
                }
                //don du lieu dtl
                for (var i = 1; i < 4; i++) {
                    //var xs = i + 1;
                    var strMaMucx = $('#MA_MUC_' + i).val().trim();
                    if (strMaMucx == '') {
                        for (var xs = i + 1; xs < 5; xs++) {
                            if ($('#MA_MUC_' + xs).val().trim().length > 0) {
                                $('#ID_KHOANNOP_' + i).val($('#ID_KHOANNOP_' + xs).val().trim());
                                $('#MA_CHUONG_' + i).val($('#MA_CHUONG_' + xs).val().trim());
                                $('#MA_MUC_' + i).val($('#MA_MUC_' + xs).val().trim());
                                $('#SOTIEN_' + i).val($('#SOTIEN_' + xs).val().trim());
                                $('#MA_DBHC_' + i).val($('#MA_DBHC_' + xs).val().trim());
                                $('#KY_THUE_' + i).val($('#KY_THUE_' + xs).val().trim());
                                $('#NOI_DUNG_' + i).val($('#NOI_DUNG_' + xs).val().trim());
                                $('#ID_KHOANNOP_' + xs).val('');
                                $('#MA_CHUONG_' + xs).val('');
                                $('#MA_MUC_' + xs).val('');
                                $('#SOTIEN_' + xs).val('');
                                $('#MA_DBHC_' + xs).val('');
                                $('#KY_THUE_' + xs).val('');
                                $('#NOI_DUNG_' + xs).val('');
                                x = xs;
                                break;
                            }
                        }
                    }
                }
                //neu ko con chi tiet thi xoa thong tin HDR
                if (x == 0) {
                    $('#txtMaCQThu').val('');
                    $('#txtTenCQThu').val('');
                    $('#txtSHKB').val('');
                    $('#txtTenKB').val('');
                    $('#ddlMaTKCo').val('');
                    $('#txtMA_NH_TT').val('');
                    $('#txtTEN_NH_TT').val('');
                    $('#txtMA_NH_B').val('');
                    $('#txtTenMA_NH_B').val('');
                    $('#txtSoKhung').val('');
                    $('#txtSoMay').val('');
                    $('#txtDacDiemPhuongTien').val('');
                    $('#txtSOQD').val('');
                    $('#txtNgayQD').val('');

                    $('#txtMaHS').val('');
                    $('#txtSoThuaDat').val('');
                    $('#txtSoToBanDo').val('');
                    $('#txtDiaChiTS').val('');
                    $('#txtDBHCTS').val('');
                    $('#txtTenDBHCTS').val('');
                    $('#txtMA_GCN').val('');

                }

            }
        }

        function jsClearTableHDR() {
            //neu bo check
            //xoa dtl truoc
            var strMA_CQTHU = "";
            var strSHKB = "";
            var strMA_TMUC = "";
            var strTK_KB = "";
            var strMA_NT = "";
            var strMaCHuong = "";
            var strNO_CUOI_KY = "";
            var strKY_THUE = "";


            var x = 0;
            for (var i = 1; i < 5; i++) {
                var strMaMucx = $('#MA_MUC_' + i).val().trim();
                console.log("aaaaa: " + i);
                if (strMaMucx == strMA_TMUC) {
                    $('#MA_CHUONG_' + i).val('');
                    $('#MA_MUC_' + i).val('');
                    $('#SOTIEN_' + i).val('');
                    $('#KY_THUE_' + i).val('');
                    $('#NOI_DUNG_' + i).val('');
                    break;
                }
            }
            //don du lieu dtl
            for (var i = 1; i < 4; i++) {
                //var xs = i + 1;
                var strMaMucx = $('#MA_MUC_' + i).val().trim();
                if (strMaMucx == '') {
                    for (var xs = i + 1; xs < 5; xs++) {
                        if ($('#MA_MUC_' + xs).val().trim().length > 0) {
                            $('#MA_CHUONG_' + i).val($('#MA_CHUONG_' + xs).val().trim());
                            $('#MA_MUC_' + i).val($('#MA_MUC_' + xs).val().trim());
                            $('#SOTIEN_' + i).val($('#SOTIEN_' + xs).val().trim());
                            $('#KY_THUE_' + i).val($('#KY_THUE_' + xs).val().trim());
                            $('#NOI_DUNG_' + i).val($('#NOI_DUNG_' + xs).val().trim());
                            $('#MA_CHUONG_' + xs).val('');
                            $('#MA_MUC_' + xs).val('');
                            $('#SOTIEN_' + xs).val('');
                            $('#KY_THUE_' + xs).val('');
                            $('#NOI_DUNG_' + xs).val('');
                            x = xs;
                            break;
                        }
                    }
                }
            }
            //neu ko con chi tiet thi xoa thong tin HDR
            if (x == 0) {
                $('#txtMaCQThu').val('');
                $('#txtTenCQThu').val('');
                $('#txtSHKB').val('');
                $('#txtTenKB').val('');
                $('#ddlMaTKCo').val('');
                $('#txtMA_NH_TT').val('');
                $('#txtTEN_NH_TT').val('');
                $('#txtMA_NH_B').val('');
                $('#txtTenMA_NH_B').val('');
                $('#txtSoKhung').val('');
                $('#txtSoMay').val('');
                $('#txtDacDiemPhuongTien').val('');
                $('#txtSOQD').val('');
                $('#txtNgayQD').val('');
                $('#txtMaHS').val('');
                $('#txtSoThuaDat').val('');
                $('#txtSoToBanDo').val('');
                $('#txtDiaChiTS').val('');
                $('#txtDBHCTS').val('');
                $('#txtTenDBHCTS').val('');
                $('#txtMA_GCN').val('');

            }
        }

        //function jsCalCharacter() {

        //}

        function jsCalCharacter() {
            //        alert ("jsCalcharacter");
            //if (v_coreVersion == "2") {
            //    return jsCalCharacter_V20();
            //} else {
            var tempCharater = '';

            //Cộng phần head MST:..
            //Cộng kỳ thuế
            charRemark = 17;
            $get('txtMaNNT').value = $get('txtMaNNT').value;
            $get('txtTenNNT').value = $get('txtTenNNT').value;
            tempCharater += $get('txtMaNNT').value.trim();
            tempCharater += $get('txtTenNNT').value.trim();
            tempCharater += dtmNgayCore;
            //tempCharater += $get('txtNGAY_KH_NH').value;
            tempCharater += $get('ddlMaLoaiThue').value;

            //alert($get('txtMaNNT').value + $get('txtTenNNT').value + $get('txtNGAY_KH_NH').value + $get('ddlMaLoaiThue').value);

            if (document.getElementById("ddlMaLoaiThue").value == '04') {
                charRemark = 24;
                tempCharater += $get('txtToKhaiSo').value;
                tempCharater += $get('txtNgayDK').value;
                tempCharater += $get('txtLHXNK').value;
            }
            if (document.getElementById("ddlMaLoaiThue").value == '03') {
                charRemark += 8;
                tempCharater += $get('txtSoKhung').value.trim();
                tempCharater += $get('txtSoMay').value.trim();
            }
            if (document.getElementById("ddlMaLoaiThue").value == '07') {
                charRemark += 20;
                tempCharater += $get('txtCQQD').value.trim();
                tempCharater += $get('txtSOQD').value.trim();
                tempCharater += $get('txtNgayQD').value.trim();
            }
            // tempCharater+=$get('txtMaNNT').value;

            if (document.getElementById("chkRemove1").checked && document.getElementById('SOTIEN_1').value.replaceAll('.', '') != 0) {
                charRemark = charRemark + 14;
                tempCharater += $get('MA_CHUONG_1').value;
                tempCharater += $get('MA_MUC_1').value;
                tempCharater += $get('NOI_DUNG_1').value;
                tempCharater += $get('SOTIEN_1').value.replaceAll('.', '');
                tempCharater += $get('KY_THUE_1').value;

                //alert($get('MA_CHUONG_1').value + $get('MA_MUC_1').value + $get('NOI_DUNG_1').value + $get('SOTIEN_1').value + $get('KY_THUE_1').value);
            }
            if (document.getElementById("chkRemove2").checked && document.getElementById('SOTIEN_2').value.replaceAll('.', '') != 0) {
                charRemark = charRemark + 14;
                tempCharater += $get('MA_CHUONG_2').value;
                tempCharater += $get('MA_MUC_2').value;
                tempCharater += $get('NOI_DUNG_2').value;
                tempCharater += $get('SOTIEN_2').value.replaceAll('.', '');
                tempCharater += $get('KY_THUE_2').value;

                // alert($get('MA_CHUONG_2').value + $get('MA_MUC_2').value + $get('NOI_DUNG_2').value + $get('SOTIEN_2').value + $get('KY_THUE_2').value);
            }
            if (document.getElementById("chkRemove3").checked && document.getElementById('SOTIEN_3').value.replaceAll('.', '') != 0) {
                charRemark = charRemark + 14;
                tempCharater += $get('MA_CHUONG_3').value;
                tempCharater += $get('MA_MUC_3').value;
                tempCharater += $get('NOI_DUNG_3').value;
                tempCharater += $get('SOTIEN_3').value.replaceAll('.', '');
                tempCharater += $get('KY_THUE_3').value;
            }
            if (document.getElementById("chkRemove4").checked && document.getElementById('SOTIEN_4').value.replaceAll('.', '') != 0) {
                charRemark = charRemark + 14;
                tempCharater += $get('MA_CHUONG_4').value;
                tempCharater += $get('MA_MUC_4').value;
                tempCharater += $get('NOI_DUNG_4').value;
                tempCharater += $get('SOTIEN_4').value.replaceAll('.', '');
                tempCharater += $get('KY_THUE_4').value;
            }
            //$get('txtRemarks').value = tempCharater;
            document.getElementById('leftCharacter').visible = false;
            charNumber = charNumberLimit - charRemark - tempCharater.length + 1;
            document.getElementById('leftCharacter').innerHTML = charNumber;
            if (charNumber < 0) {
                return false;
            }
            else {
                return true;
            }
            //}
        }
        function jsCalTotal() {
            var dblTongTien = 0;
            var dblPhi = 0;
            var dblVAT = 0;
            var dblTongTrichNo = 0;
            var dblVAT2 = 0;
            var dblPhi2 = 0;
            if ($get('SOTIEN_1').value.length > 0) {
                if (document.getElementById("chkRemove1").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_1').value.replaceAll('.', ''));

                }
            }
            if ($get('SOTIEN_2').value.length > 0) {
                if (document.getElementById("chkRemove2").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_2').value.replaceAll('.', ''));
                }
            }

            if ($get('SOTIEN_3').value.length > 0) {
                if (document.getElementById("chkRemove3").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_3').value.replaceAll('.', ''));

                }
            }

            if ($get('SOTIEN_4').value.length > 0) {
                if (document.getElementById("chkRemove4").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_4').value.replaceAll('.', ''));

                }
            }
            //console.log("tong tien: " + dblTongTien);
            if (document.getElementById("ddlMaHTTT").value == "01" || document.getElementById("ddlMaHTTT").value == "00") {
                //$get('txtVat').value = dblVAT.toString().replaceAll('.','');
                if ($get('txtVat').value > 0) {
                    dblVAT = $get('txtVat').value.replaceAll('.', '');
                }
                if ($get('txtCharge').value > 0) {
                    dblPhi = $get('txtCharge').value.replaceAll('.', '');
                }

                //dblPhi = $get('txtCharge').value.replaceAll('.', '');
                if ($get('txtVat2').value > 0) {
                    dblVAT2 = $get('txtVat').value.replaceAll('.', '');
                }
                if ($get('txtCharge2').value > 0) {
                    dblPhi2 = $get('txtCharge2').value.replaceAll('.', '');
                }
                //dblVAT2 = $get('txtVat2').value.replaceAll('.', '');
                //dblPhi2 = $get('txtCharge2').value.replaceAll('.', '');

                $get('txtTongTien').value = fncFormatNumber(dblTongTien);
                //dblPhi=dblPhi.toString().replaceAll('.','')

                dblTongTrichNo = parseFloat(dblPhi) + parseFloat(dblVAT) + parseFloat(dblPhi2) + parseFloat(dblVAT2) + parseFloat(dblTongTien);
                $get('txtTongTrichNo').value = fncFormatNumber(dblTongTrichNo);
                //alert($get('txtTongTrichNo').value);
                //localeNumberFormat(dblPhi,'.');
                //$get('txtCharge').value = dblPhi;
                //jsFormatNumber('txtTongTien');
                //jsFormatNumber('txtCharge');
                //jsFormatNumber('txtVat');
                //jsFormatNumber('txtCharge2');
                //jsFormatNumber('txtVat2');
                //jsFormatNumber('txtTongTrichNo');
                document.getElementById('divBangChu').innerHTML = 'Bằng chữ : ' + So_chu($("#txtTongTien").val().replaceAll('.', ''));
            }
            else {

                $get('txtCharge').value = 0;
                $get('txtVat').value = 0;
                $get('txtCharge2').value = 0;
                $get('txtVat2').value = 0;
                $get('txtTongTien').value = fncFormatNumber(dblTongTien);
                dblTongTrichNo = dblTongTien;
                $get('txtTongTrichNo').value = fncFormatNumber(dblTongTrichNo);
                //alert($get('txtTongTrichNo').value);
                //jsFormatNumber('txtTongTien');
                //jsFormatNumber('txtCharge');
                //jsFormatNumber('txtVat');
                //jsFormatNumber('txtCharge2');
                //jsFormatNumber('txtVat2');
                //jsFormatNumber('txtTongTrichNo');
                //jsConvertCurrToText($("#txtTongTien").val().replaceAll('.', ''));
                document.getElementById('divBangChu').innerHTML = 'Bằng chữ : ' + So_chu($("#txtTongTien").val().replaceAll('.', ''));
            }

            if ($("#txtTongTien").val().replaceAll('.', '') != _TTIEN) {
                _TTIEN = $("#txtTongTien").val().replaceAll('.', '');
                // ResetTinhPhi();
            }
            var dblTongTien = parseFloat($get('txtTongTien').value.replaceAll('.', ''));
            $get('txtTongTrichNo').value = fncFormatNumber(dblTongTien);
            //jsFormatNumber('txtTongTrichNo');
            //jsConvertCurrToText($("#txtTongTrichNo").val().replaceAll('.',''));
        }
        function jsGetLoaiNNT() {
            var pLoai = $('#txtMaLoaiNNT').val();
            if (pLoai == null)
                return;
            if (pLoai.trim().length <= 0)
                return;

            PageMethods.jsGetLoaiNNT(pLoai, jsGetLoaiNNT_Complete, jsGetLoaiNNT_Error);
        }
        function jsGetLoaiNNT_Complete(pData) {

            if (!jsCheckUserSession(pData))
                return;
            if (pData.length <= 0) {

                return;
            }
            $('#txtTenLoaiNNT').val(pData);

        }
        function jsGetLoaiNNT_Error() {

        }


        function loadXMLString(txt) {
            if (window.DOMParser) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(txt, "text/xml");
            }
            else // code for IE
            {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(txt);
            }
            return xmlDoc;
        }


        // valid length

        function jsValidLength(id, value) {
            if (id == 'txtMaLoaiNNT' && value.length < 4) {
                alert("Mã/Tên Loại NNT cần tối thiểu 4 ký tự");
                document.getElementById("txtMaLoaiNNT").value = "";
                document.getElementById("txtMaLoaiNNT").focus();
            }
            else if (id == 'txtQuan_HuyenNNTien' || id == 'txtTinh_NNTien') {
                if (value.length < 5) {
                    alert("Mã Quận(Huyện)/Tỉnh(Thành phố) cần tối thiểu 5 ký tự");
                    document.getElementById("txtQuan_HuyenNNTien").value = "";
                    document.getElementById("txtTinh_NNTien").value = "";
                    document.getElementById("txtQuan_HuyenNNTien").focus();
                }
            }
            else if (id == 'txtHuyen_NNT' || id == 'txtTinh_NNT') {
                if (value.length < 5) {
                    alert("Mã Quận(Huyện)/Tỉnh(Thành phố) cần tối thiểu 5 ký tự");
                    document.getElementById("txtHuyen_NNT").value = "";
                    document.getElementById("txtTinh_NNT").value = "";
                    document.getElementById("txtHuyen_NNT").focus();
                }
            }
            else if (id == 'txtSHKB' && value.length < 4) {
                alert("Số hiệu KB cần tối thiểu 4 ký tự");
                document.getElementById("txtSHKB").value = "";
                document.getElementById("txtSHKB").focus();
            }
            else if (id == 'txtMaCQThu' && value.length < 7) {
                alert("Mã CQ Thu cần tối thiểu 7 ký tự");
                document.getElementById("txtMaCQThu").value = "";
                document.getElementById("txtMaCQThu").focus();
            }
            else if (id == 'ddlMaTKCo' && value.length < 4) {
                alert("Mã TK Có NSNN cần tối thiểu 4 ký tự");
                document.getElementById("ddlMaTKCo").value = "";
                document.getElementById("ddlMaTKCo").focus();
            }
            else if (id == 'txtCK_SoCMND' && value.length < 9) {
                alert("Số chứng minh cần tối thiểu 9 ký tự");
                document.getElementById("txtCK_SoCMND").value = "";
                document.getElementById("txtCK_SoCMND").focus();
            }
            else if (id == 'txtCK_SoDT' && value.length < 9) {
                alert("Số điện thoại cần tối thiểu 9 ký tự");
                document.getElementById("txtCK_SoDT").value = "";
                document.getElementById("txtCK_SoDT").focus();
            }
            else if (id == 'txtMA_NH_TT' && value.length < 8) {
                alert("Mã NH trực tiếp cần tối thiểu 8 ký tự");
                document.getElementById("txtMA_NH_TT").value = "";
                document.getElementById("txtMA_NH_TT").focus();
            }
            else if (id == 'txtMA_NH_B' && value.length < 8) {
                alert("Mã NH gián tiếp cần tối thiểu 8 ký tự");
                document.getElementById("txtMA_NH_B").value = "";
                document.getElementById("txtMA_NH_TT").focus();
            }

        };
        //
        //function jsShowHideRowProgress(blnShow) {
        //    if (blnShow) {
        //        document.getElementById("rowProgress").style.display = '';
        //    }
        //    else {
        //        document.getElementById("rowProgress").style.display = 'none';
        //    }
        //}
        function getSoTK_TruyVanCore() {
            //jsGetTK_KH_NH(); jsGetTenTK_KH_NH();
            $get('txtTK_KH_NH').value = '';
            $get('txtTenTK_KH_NH').value = '';
            $get('txtSoDu_KH_NH').value = '';
            var pttt = $("#ddlMaHTTT option:selected").val();
            //console.log("PT_TT(CORE): " + pttt);
            if (pttt == "05") {
                PageMethods.getSoTK_TruyVanCore(document.getElementById("ddlMaHTTT").value, getSoTK_TruyVanCore_Complete, getSoTK_TruyVanCore_Error);
            }
            else {
                $get('txtTK_KH_NH').disabled = false;
            }
        }

        function getSoTK_TruyVanCore_Complete(result, methodName) {
            //jsShowHideRowProgress(false);
            if (result.length > 0) {
                $get('txtTK_KH_NH').value = result;
                $get('txtTK_KH_NH').disabled = true;
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';

            } else {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }
        function getSoTK_TruyVanCore_Error(error, userContext, methodName) {
            if (error !== null) {
                //jsShowHideRowProgress(false);
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }
        //function getSoTK_TruyVanCore() {
        //    //jsGetTK_KH_NH(); jsGetTenTK_KH_NH();
        //    var pttt = $("#ddlMaHTTT option:selected").val();
        //    //console.log("PT_TT(CORE): " + pttt);
        //    if (pttt == "05") {
        //        PageMethods.GetTK_KH_NH(document.getElementById("ddlMaHTTT").value, getSoTK_TruyVanCore_Complete, getSoTK_TruyVanCore_Error);
        //    }
        //    else {
        //        $get('txtTK_KH_NH').disabled = false;
        //    }
        //}

        //function getSoTK_TruyVanCore_Complete(result, methodName) {
        //    jsShowHideDivProgress(false);
        //    if (result.length > 0) { 
        //        $get('txtTK_KH_NH').value = result;
        //        $get('txtTK_KH_NH').disabled = true;
        //        $get('txtTenTK_KH_NH').value = '';
        //        $get('txtSoDu_KH_NH').value = '';

        //    } else {
        //        document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
        //    }
        //}
        //function getSoTK_TruyVanCore_Error(error, userContext, methodName) {
        //    if (error !== null) {
        //        jsShowHideDivProgress(false);
        //        $get('txtTenTK_KH_NH').value = '';
        //        $get('txtSoDu_KH_NH').value = '';
        //        document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
        //    }
        //}
        //

        function jsGetTK_KH_NH() {
            if (1 > 0)//kienvt : tai khoan fix la 14 + 4'-'
            {
                //set lai tham so timeout
                //SessionExpireAlert(_timeout, _rootPath);

                document.getElementById('divStatus').innerHTML = '';
                var strAccType = '';
                var intPP_TT = document.getElementById("ddlMaHTTT").value;
                var ma_NT = document.getElementById("txtTenNT").value;
                //alert(intPP_TT);
                if (intPP_TT == '01') {
                    strAccType = 'CK';

                } else if (intPP_TT == '00') {
                    strAccType = 'TM';
                }

                jsShowHideDivProgress(true);
                var pttt = $("#ddlMaHTTT option:selected").val();
                if (pttt == "01") {
                    PageMethods.GetTK_KH_NH($get('txtTK_KH_NH').value.trim(), GetTK_NH_Complete, GetTK_NH_Error);
                } else if (pttt == "05") {
                    $get('txtTK_KH_NH').disabled = true;
                }
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Tài khoản khách hàng không được để trống';
            }
        }
        function GetTK_NH_Complete(result, methodName) {
            if (!jsCheckUserSession(result))
                return;

            jsShowHideDivProgress(false);
            try {
                //if (result.length > 0) {
                //    var arrTK = result.split(';');
                //    if (arrTK[0] == "0") {
                //        $get('txtTK_KH_NH').value = arrTK[2];
                //        $get('txtTenTK_KH_NH').value = arrTK[3];
                //        //$get('txtSoDu_KH_NH').value=arrTK[4];
                //        $('#txtSoDu_KH_NH').val(fncFormatNumber(arrTK[4]));

                //    } else {
                //        document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình truy vấn số dư tài khoản.<br />Mã lỗi:' + arrTK[0] + ' - Mô tả lỗi: ' + arrTK[1];
                //        $get('txtTK_KH_NH').value = '';
                //        $get('txtTenTK_KH_NH').value = '';
                //        $get('txtSoDu_KH_NH').value = '';
                //    }
                //} else {
                //    document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình truy vấn số dư tài khoản.<br />Không trả ra thông tin tài khoản!';
                //}

                //
                if (result.length > 0) {
                    var xmlDoc = loadXMLString(result);
                    var rootCTU_HDR = xmlDoc.getElementsByTagName('PARAMETER')[0];
                    if (rootCTU_HDR != null) {

                        if (xmlDoc.getElementsByTagName("RETCODE")[0].childNodes[0].nodeValue == "00000") {
                            if (xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes != null) $get('txtTK_KH_NH').value = xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes[0].nodeValue;
                            if (xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes != null) $get('txtSoDu_KH_NH').value = xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes[0].nodeValue;
                            jsFormatNumber('txtSoDu_KH_NH');

                        } else {
                            $get('txtTK_KH_NH').value = '';
                            $get('txtSoDu_KH_NH').value = '';
                            document.getElementById('divStatus').innerHTML = "Không lấy được thông tin tài khoản. Mã lỗi " + xmlDoc.getElementsByTagName("RETCODE")[0].childNodes[0].nodeValue;
                        }
                    } else {
                        document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình truy vấn số dư tài khoản.';
                        $get('txtTenTK_KH_NH').value = '';
                        $get('txtSoDu_KH_NH').value = '';

                    }
                }

            } catch (err) {
                document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình truy vấn số dư tài khoản.Mô tả <br/ >' + err.message;
            }
        }

        function GetTK_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                jsShowHideDivProgress(false);
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
                ThongBao += "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
                document.getElementById('divStatus').innerHTML = ThongBao;
            }
        }

        //2')Lay tên khách hàng
        function jsGetTenTK_KH_NH() {
            if ($get('txtTK_KH_NH').value.length > 0) {
                document.getElementById('divStatus').innerHTML = '';
                var strAccType = '';
                var intPP_TT = document.getElementById("ddlMaHTTT").selectedIndex;
                var ma_NT = document.getElementById("txtTenNT").value;
                if (intPP_TT == 3) {
                    strAccType = 'TG';
                } else {
                    strAccType = 'CK';
                }
                jsShowHideDivProgress(true);
                PageMethods.GetTEN_KH_NH($get('txtTK_KH_NH').value.trim(), GetTEN_KH_NH_Complete, GetTEN_KH_NH_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Tài khoản khách hàng không được để trống';
            }
        }

        function GetTEN_KH_NH_Complete(result, methodName) {
            jsShowHideDivProgress(false);
            if (result.length > 0) {
                $get('txtTenTK_KH_NH').value = result;
            } else {
                document.getElementById('divStatus').innerHTML = 'Không lấy được tên tài khoản khách hàng';
                $get('txtTenTK_KH_NH').value = '';
            }
        }
        function GetTEN_KH_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                $get('txtTenTK_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
            }
        }

    </script>
    <script type="text/javascript">
        function jsGetHDRObject2() {
            var objHdrHq = {};

            var tongTien = $('#txtTongTien').val().replaceAll('.', '');
            var tFee = $('#txtCharge').val().replaceAll('.', '');
            var tVAT = $('#txtVat').val().replaceAll('.', '');

            var tFee2 = $('#txtCharge2').val().replaceAll('.', '');
            var tVAT2 = $('#txtVat2').val().replaceAll('.', '');

            var tongTienThucThu = tongTien + tFee + tVAT + tFee2 + tVAT2

            objHdrHq.Ma_NT = $('#cboNguyenTe option:selected').val();
            var maTienTe = objHdrHq.Ma_NT;
            var tongTienNT = 0;
            //if (maTienTe != 'VND')
            //    tongTienNT = accounting.unformat($('#txtTongTienNT').val(), ".");
            //else
            tongTienNT = $('#txtTongTienNT').val();




            objHdrHq.Ma_LThue = document.getElementById("ddlMaLoaiThue").value;
            objHdrHq.Ma_NNThue = $('#txtMaNNT').val().trim();
            objHdrHq.Ten_NNThue = $('#txtTenNNT').val();
            objHdrHq.DC_NNThue = $('#txtDChiNNT').val();

            objHdrHq.Ma_Huyen_NNThue = $('#txtQuan_HuyenNNTien').val();
            objHdrHq.Ma_Tinh_NNThue = $('#txtTinh_NNTien').val();

            jsGetTenQuanHuyen(objHdrHq.Ma_Huyen_NNThue);

            objHdrHq.Huyen_nnthue = $('#txtTenQuan_HuyenNNTien').val();
            objHdrHq.Tinh_nnthue = $('#txtTenTinh_NNTien').val();

            objHdrHq.Ten_Huyen_NNThue = $('#txtTenQuan_HuyenNNTien').val();
            objHdrHq.Ten_Tinh_NNThue = $('#txtTenTinh_NNTien').val();

            objHdrHq.MA_QUAN_HUYENNNTIEN = $('#txtHuyen_NNT').val();
            objHdrHq.MA_HUYEN_NNT = $('#txtHuyen_NNT').val();

            objHdrHq.MA_TINH_TPNNTIEN = $('#txtTinh_NNT').val();

            objHdrHq.TEN_QUAN_HUYENNNTIEN = $('#txtTenHuyen_NNT').val();
            objHdrHq.TEN_TINH_TPNNTIEN = $('#txtTenTinh_NNT').val();

            objHdrHq.Ma_CQThu = $('#txtMaCQThu').val();
            objHdrHq.Ten_cqthu = $('#txtTenCQThu').val();
            objHdrHq.SHKB = $("#txtSHKB").val();
            objHdrHq.Ten_kb = $("#txtTenKB").val();
            objHdrHq.MA_NTK = $('#ddlMa_NTK').val();
            objHdrHq.TK_Co = $('#ddlMaTKCo').val();
            objHdrHq.PT_TT = $('#ddlMaHTTT').val();

            objHdrHq.TK_KH_NH = $('#txtTK_KH_NH').val();
            objHdrHq.TEN_KH_NH = $('#txtTenTK_KH_NH').val();
            //objHdrHq.TK_GL_NH = $('#txtTKGL').val();
            //objHdrHq.TEN_TK_GL_NH = $('#txtTenTKGL').val();

            //objHdrHq.KyHieu_CT = $('#txtKyHieuCT').val();
            objHdrHq.NGAY_KH_NH = $('#txtNGAY_KH_NH').val();

            objHdrHq.Ma_NNTien = $('#txtMaNNTien').val();
            objHdrHq.Ten_NNTien = $('#txtTenNNTien').val();
            objHdrHq.DC_NNTien = $('#txtDChiNNTien').val();

            objHdrHq.Ma_Xa = $('#txtMaDBHC').val();

            objHdrHq.MA_NH_A = $('#ddlMA_NH_A').val();
            objHdrHq.MA_NH_B = $('#txtMA_NH_B').val();
            objHdrHq.Ten_NH_B = $('#txtTenMA_NH_B').val();
            objHdrHq.MA_NH_TT = $('#txtMA_NH_TT').val();
            objHdrHq.TEN_NH_TT = $('#txtTEN_NH_TT').val();

            objHdrHq.So_Khung = $('#txtSoKhung').val();
            objHdrHq.So_May = $('#txtSoMay').val();
            objHdrHq.DAC_DIEM_PTIEN = $('#txtDacDiemPhuongTien').val();
            objHdrHq.So_QD = $('#txtSOQD').val();
            objHdrHq.Ngay_QD = $('#txtNgayQD').val();

            objHdrHq.Ngay_CT = 0;
            objHdrHq.Ngay_HT = "";

            //objHdrHq.TY_GIA = accounting.unformat($('#txtTyGia').val());;
            objHdrHq.TTien = tongTien;
            objHdrHq.TTien_NT = tongTienNT;
            objHdrHq.TT_TThu = tongTienThucThu;

            objHdrHq.Trang_Thai = curCtuStatus;
            if ($('#chkChargeTypeMP').is(':checked')) {
                objHdrHq.PT_TINHPHI = "W";
            }
            else {
                objHdrHq.PT_TINHPHI = "O";
            }
            //objHdrHq.PT_TINHPHI = "";
            objHdrHq.PHI_GD = tFee.toString();
            objHdrHq.PHI_VAT = tVAT.toString();
            objHdrHq.PHI_GD2 = tFee2.toString();
            objHdrHq.PHI_VAT2 = tVAT2.toString();
            objHdrHq.TT_CTHUE = 0;
            objHdrHq.TKHT_NH = "";
            objHdrHq.LOAI_TT = "";       //Chuyen xuong HDR

            objHdrHq.Kenh_CT = "01";
            var vMaSp = $('#cboPhiKiemDem option:selected').val() + "|" + $('#cboPhiSanPham option:selected').val();
            objHdrHq.SAN_PHAM = "";
            objHdrHq.TT_CITAD = "0";
            objHdrHq.SO_CMND = $("#txtCK_SoCMND").val();
            objHdrHq.SO_FONE = $("#txtCK_SoDT").val();
            objHdrHq.LOAI_CTU = "T";
            objHdrHq.REMARKS = "";
            objHdrHq.So_CT = $("#hdfSo_CTu").val();

            if ($('#hdfSoBT').val() != 'undefinided' || $('#hdfSoBT').val() != null) {
                if ($('#hdfSoBT').val().length > 0)
                    objHdrHq.SO_BT = $('#hdfSoBT').val();
            } else {
                objHdrHq.So_BT = 0;// $("#hdfSoBT").val();
            }
            if ($('#hdfNgayKB').val() != 'undefinided' || $('#hdfNgayKB').val() != null) {
                if ($('#hdfNgayKB').val().length > 0)
                    objHdrHq.Ngay_KB = $('#hdfNgayKB').val();
            } else {
                objHdrHq.Ngay_KB = 0;
            }
            //console.log("BT: " + objHdrHq.So_BT)
            //console.log("NGAY_KB: " + objHdrHq.Ngay_KB)
            objHdrHq.Ma_DThu = "";
            objHdrHq.Ly_Do = $("#txtLyDoHuy").val();
            objHdrHq.LOAI_NNT = $("#txtMaLoaiNNT").val();
            objHdrHq.TEN_LOAI_NNT = $("#txtTenLoaiNNT").val();

            objHdrHq.MA_HS = $("#txtMaHS").val();
            objHdrHq.SO_THUADAT = $("#txtSoThuaDat").val();
            objHdrHq.SO_TO_BANDO = $("#txtSoToBanDo").val();
            objHdrHq.DIA_CHI_TS = $("#txtDiaChiTS").val();
            objHdrHq.MA_DBHC_TS = $("#txtDBHCTS").val();
            objHdrHq.TEN_DBHC_TS = $("#txtTenDBHCTS").val();
            objHdrHq.MA_GCN = $("#txtMA_GCN").val();

            return objHdrHq;
        }

        function jsGetDTLArray2() {
            var arrDTL = [];
            var objDTL = {};
            var ctrCheck = null;
            var ctrInput = null;

            if (document.getElementById("chkRemove1").checked) {
                if ($get('SOTIEN_1').value != '') {
                    objDTL = {}
                    objDTL.Ma_Chuong = $get('MA_CHUONG_1').value;
                    objDTL.Ma_Khoan = $get('MA_KHOAN_1').value;
                    objDTL.Ma_TMuc = $get('MA_MUC_1').value;
                    objDTL.Noi_Dung = $get('NOI_DUNG_1').value;
                    objDTL.SoTien = $get('SOTIEN_1').value.replaceAll('.', '');
                    objDTL.Ky_Thue = $get('KY_THUE_1').value;
                    //console.log("objDTL1: " + objDTL);
                    //console.log("arrDTL1: " + arrDTL);
                    arrDTL.push(objDTL);
                }
            }
            if (document.getElementById("chkRemove2").checked) {
                if ($get('SOTIEN_2').value != '') {
                    objDTL = {}
                    objDTL.Ma_Chuong = $get('MA_CHUONG_2').value;
                    objDTL.Ma_Khoan = $get('MA_KHOAN_2').value;
                    objDTL.Ma_TMuc = $get('MA_MUC_2').value;
                    objDTL.Noi_Dung = $get('NOI_DUNG_2').value;
                    objDTL.SoTien = $get('SOTIEN_2').value.replaceAll('.', '');
                    objDTL.Ky_Thue = $get('KY_THUE_2').value;
                    //console.log("objDTL2: " + objDTL);
                    //console.log("arrDTL2: " + arrDTL);
                    arrDTL.push(objDTL);
                }
            }
            if (document.getElementById("chkRemove3").checked) {
                if ($get('SOTIEN_3').value != '') {
                    objDTL = {}
                    objDTL.Ma_Chuong = $get('MA_CHUONG_3').value;
                    objDTL.Ma_Khoan = $get('MA_KHOAN_3').value;
                    objDTL.Ma_TMuc = $get('MA_MUC_3').value;
                    objDTL.Noi_Dung = $get('NOI_DUNG_3').value;
                    objDTL.SoTien = $get('SOTIEN_3').value.replaceAll('.', '');
                    objDTL.Ky_Thue = $get('KY_THUE_3').value;

                    arrDTL.push(objDTL);
                }
            }
            if (document.getElementById("chkRemove4").checked) {
                if ($get('SOTIEN_4').value != '') {
                    objDTL = {}
                    objDTL.Ma_Chuong = $get('MA_CHUONG_4').value;
                    objDTL.Ma_Khoan = $get('MA_KHOAN_4').value;
                    objDTL.Ma_TMuc = $get('MA_MUC_4').value;
                    objDTL.Noi_Dung = $get('NOI_DUNG_4').value;
                    objDTL.SoTien = $get('SOTIEN_4').value.replaceAll('.', '');
                    objDTL.Ky_Thue = $get('KY_THUE_4').value;

                    arrDTL.push(objDTL);
                }
            }
            return arrDTL;
        }

        function jsFormToBean() {       //alert($get('hdnSO_CT').value);
            var xmlResult = "<?xml version='1.0'?>";
            xmlResult += "<CTC_CTU>";
            xmlResult += "<CTU_HDR>";
            xmlResult += "<NgayLV></NgayLV>";
            xmlResult += "<SoCT>" + $get('hdnSO_CT').value + "</SoCT>";
            xmlResult += "<So_BT>" + curSo_BT + "</So_BT>";
            xmlResult += "<SHKB>" + $get('txtSHKB').value + "</SHKB>";
            xmlResult += "<TenKB></TenKB>";
            xmlResult += "<MaNNT>" + $get('txtMaNNT').value + "</MaNNT>";
            xmlResult += "<TenNNT>" + $get('txtTenNNT').value + "</TenNNT>";
            xmlResult += "<DChiNNT>" + $get('txtDChiNNT').value + "</DChiNNT>";
            xmlResult += "<MaNNTien>" + $get('txtMaNNTien').value + "</MaNNTien>";
            xmlResult += "<TenNNTien>" + $get('txtTenNNTien').value + "</TenNNTien>";
            xmlResult += "<DChiNNTien>" + $get('txtDChiNNTien').value + "</DChiNNTien>";
            xmlResult += "<MaCQThu>" + $get('txtMaCQThu').value + "</MaCQThu>";
            xmlResult += "<TenCQThu></TenCQThu>";
            xmlResult += "<MaDBHC>" + $get('txtMaDBHC').value + "</MaDBHC>";

            xmlResult += "<TenDBHC></TenDBHC>";
            xmlResult += "<TKNo>" + document.getElementById('ddlMaTKNo').value.replaceAll('-', '') + "</TKNo>";
            xmlResult += "<TenTKNo>" + $get('txtTKNo').value + "</TenTKNo>";
            xmlResult += "<TKCo>" + document.getElementById('ddlMaTKCo').value.replaceAll('-', '') + "</TKCo>";
            xmlResult += "<TenTKCo>" + $get('txtTKCo').value + "</TenTKCo>";
            xmlResult += "<HTTT>" + document.getElementById('ddlMaHTTT').value + "</HTTT>";
            xmlResult += "<SoCTNH>" + document.getElementById('hdnSoCT_NH').value + " </SoCTNH>";
            if (document.getElementById('ddlMaHTTT').value == "01") {
                xmlResult += "<TK_KH_NH>" + $get('txtTK_KH_NH').value + "</TK_KH_NH>";
                xmlResult += "<TenTK_KH_NH>" + $get('txtTenTK_KH_NH').value + "</TenTK_KH_NH>";
            }
            if (document.getElementById('ddlMaHTTT').value == "05") {
                xmlResult += "<TK_KH_NH>" + $get('txtTK_KH_NH').value + "</TK_KH_NH>";
                xmlResult += "<TenTK_KH_NH>" + $get('txtTenTK_KH_NH').value + "</TenTK_KH_NH>";
            }
            if (document.getElementById('ddlMaHTTT').value == "03") {
                xmlResult += "<TK_KH_NH>" + $get('txtTKGL').value + "</TK_KH_NH>";
                xmlResult += "<TenTK_KH_NH>" + $get('txtTenTKGL').value + "</TenTK_KH_NH>";
            }
            xmlResult += "<SoDu_KH_NH></SoDu_KH_NH>";
            xmlResult += "<NGAY_KH_NH>" + $get('txtNGAY_KH_NH').value + "</NGAY_KH_NH>";
            //07/06/2011 Kienvt:Them ma ngan hang A,B
            //xmlResult +="<MA_NH_A>" + $get('txtMA_NH_A').value + "</MA_NH_A>";                 
            xmlResult += "<MA_NH_A>" + document.getElementById('ddlMA_NH_A').value + "</MA_NH_A>";
            xmlResult += "<Ten_NH_A></Ten_NH_A>";
            xmlResult += "<MA_NH_B>" + $get('txtMA_NH_B').value + "</MA_NH_B>";
            xmlResult += "<Ten_NH_B>" + $get('txtTenMA_NH_B').value + "</Ten_NH_B>";
            //end
            xmlResult += "<LoaiThue>" + document.getElementById('ddlMaLoaiThue').value + "</LoaiThue>";
            xmlResult += "<DescLoaiThue></DescLoaiThue>";
            xmlResult += "<MaNT>" + $get('txtTenNT').value + "</MaNT>";
            xmlResult += "<TenNT></TenNT>";
            xmlResult += "<ToKhaiSo>" + $get('txtToKhaiSo').value + "</ToKhaiSo>";
            xmlResult += "<NgayDK></NgayDK>";
            xmlResult += "<LHXNK></LHXNK>";
            xmlResult += "<DescLHXNK></DescLHXNK>";
            xmlResult += "<SoKhung>" + $get('txtSoKhung').value + "</SoKhung>";
            xmlResult += "<SoMay>" + $get('txtSoMay').value + "</SoMay>";
            xmlResult += "<So_BK>" + $get('txtSo_BK').value + "</So_BK>";
            xmlResult += "<NgayBK></NgayBK>";
            xmlResult += "<TRANG_THAI></TRANG_THAI>";
            xmlResult += "<TT_BDS>" + curBDS_Status + "</TT_BDS>";
            xmlResult += "<TT_NSTT>N</TT_NSTT>";
            xmlResult += "<PHI_GD>" + $get('txtCharge').value + "</PHI_GD>";
            xmlResult += "<PHI_VAT>" + $get('txtVat').value + "</PHI_VAT>";


            if (document.getElementById('chkChargeTypeMP').checked) {
                xmlResult += "<PT_TINHPHI>" + document.getElementById('chkChargeTypeMP').value + "</PT_TINHPHI>";//tu dong
            } else if (document.getElementById('chkChargeTypePT').checked) {
                xmlResult += "<PT_TINHPHI>" + document.getElementById('chkChargeTypePT').value + "</PT_TINHPHI>";//tu dong
            } else {
                xmlResult += "<PT_TINHPHI>" + document.getElementById('chkChargeTypePN').value + "</PT_TINHPHI>";//tu dong
            }
            xmlResult += "<Huyen_NNTIEN>" + $get('txtQuan_HuyenNNTien').value + "</Huyen_NNTIEN>";
            xmlResult += "<Tinh_NNTIEN>" + $get('txtTinh_NNTien').value + "</Tinh_NNTIEN>";
            xmlResult += "<TTIEN> </TTIEN>";
            xmlResult += "<TT_TTHU> </TT_TTHU>";
            xmlResult += "<MA_NV> </MA_NV>";
            xmlResult += "<DSToKhai> </DSToKhai>";
            /****************************BEGIN********************************/
            xmlResult += "<MA_HQ> </MA_HQ>";
            xmlResult += "<LOAI_TT> </LOAI_TT>"; //ASDF
            xmlResult += "<TEN_HQ> </TEN_HQ>";
            xmlResult += "<MA_HQ_PH> </MA_HQ_PH>";
            xmlResult += "<TEN_HQ_PH> </TEN_HQ_PH>";

            /****************************END**********************************/

            xmlResult += "<MA_NH_TT>" + $get('txtMA_NH_TT').value + "</MA_NH_TT>";
            xmlResult += "<TEN_NH_TT>" + $get('txtTEN_NH_TT').value + "</TEN_NH_TT>";
            xmlResult += "<MA_KS></MA_KS>";
            xmlResult += "<KHCT>" + $get('txtKyHieuCT').value + " </KHCT>";
            xmlResult += "<TT_CTHUE></TT_CTHUE>";
            xmlResult += "<MA_SANPHAM></MA_SANPHAM>";
            xmlResult += "<TTIEN_TN> " + $get('txtTongTrichNo').value.replaceAll(".", "") + " </TTIEN_TN>";
            //if (document.getElementById('Radio1').checked) {
            //    xmlResult += "<TT_CITAD>" + document.getElementById('Radio1').value + "</TT_CITAD>";
            //} else {
            //    xmlResult += "<TT_CITAD>" + document.getElementById('Radio3').value + "</TT_CITAD>";
            //}
            xmlResult += "<TT_CITAD></TT_CITAD>";
            xmlResult += "<MA_NTK>" + document.getElementById('ddlMa_NTK').value + "</MA_NTK>";

            //sonmt
            xmlResult += "<DIEN_GIAI></DIEN_GIAI>";
            xmlResult += "<MA_HUYEN>" + document.getElementById('txtHuyen_NNT').value + "</MA_HUYEN>";
            xmlResult += "<TEN_HUYEN>" + document.getElementById('txtTenHuyen_NNT').value + "</TEN_HUYEN>";
            xmlResult += "<MA_TINH>" + document.getElementById('txtTinh_NNT').value + "</MA_TINH>";
            xmlResult += "<TEN_TINH>" + document.getElementById('txtTenTinh_NNT').value + "</TEN_TINH>";

            xmlResult += "<HUYEN_NNTHAY></HUYEN_NNTHAY>";
            xmlResult += "<TINH_NNTHAY></TINH_NNTHAY>";
            xmlResult += "<SO_QD>" + document.getElementById('txtSOQD').value + "</SO_QD>";

            xmlResult += "<CQ_QD>" + document.getElementById('txtCQQD').value + "</CQ_QD>";
            xmlResult += "<NGAY_QD></NGAY_QD>";
            xmlResult += "<GHI_CHU></GHI_CHU>";

            //sonmt
            xmlResult += "<LOAI_NNT>" + $get('txtMaLoaiNNT').value.trim() + "</LOAI_NNT>";
            xmlResult += "<TEN_LOAI_NNT>" + $get('txtTenLoaiNNT').value.trim() + "</TEN_LOAI_NNT>";

            xmlResult += "<SO_CMND>" + $get('txtCK_SoCMND').value.trim() + "</SO_CMND>";
            xmlResult += "<SO_FONE>" + $get('txtCK_SoDT').value.trim() + "</SO_FONE>";

            xmlResult += "</CTU_HDR>";
            xmlResult += "<CTU_DTL>";

            if (document.getElementById("chkRemove1").checked) {
                if ($get('SOTIEN_1').value != '') {
                    xmlResult += "<ITEMS>";
                    xmlResult += "<ChkRemove></ChkRemove>";
                    xmlResult += "<MA_CAP>" + $get('MA_CAP_1').value + "</MA_CAP>";
                    xmlResult += "<MA_CHUONG>" + $get('MA_CHUONG_1').value + "</MA_CHUONG>";
                    xmlResult += "<MA_KHOAN>000</MA_KHOAN>";
                    xmlResult += "<MA_MUC>" + $get('MA_MUC_1').value + "</MA_MUC>";
                    xmlResult += "<NOI_DUNG>" + $get('NOI_DUNG_1').value + "</NOI_DUNG>";
                    xmlResult += "<SOTIEN>" + $get('SOTIEN_1').value + "</SOTIEN>";
                    xmlResult += "<SoDT></SoDT>";
                    xmlResult += "<KY_THUE>" + $get('KY_THUE_1').value + "</KY_THUE>";
                    xmlResult += "</ITEMS>";
                }
            }
            if (document.getElementById("chkRemove2").checked) {
                if ($get('SOTIEN_2').value != '') {
                    xmlResult += "<ITEMS>";
                    xmlResult += "<ChkRemove></ChkRemove>";
                    xmlResult += "<MA_CAP>" + $get('MA_CAP_2').value + "</MA_CAP>";
                    xmlResult += "<MA_CHUONG>" + $get('MA_CHUONG_2').value + "</MA_CHUONG>";
                    xmlResult += "<MA_KHOAN>000</MA_KHOAN>";
                    xmlResult += "<MA_MUC>" + $get('MA_MUC_2').value + "</MA_MUC>";
                    xmlResult += "<NOI_DUNG>" + $get('NOI_DUNG_2').value + "</NOI_DUNG>";
                    xmlResult += "<SOTIEN>" + $get('SOTIEN_2').value + "</SOTIEN>";
                    xmlResult += "<SoDT></SoDT>";
                    xmlResult += "<KY_THUE>" + $get('KY_THUE_2').value + "</KY_THUE>";
                    xmlResult += "</ITEMS>";
                }
            }
            if (document.getElementById("chkRemove3").checked) {
                if ($get('SOTIEN_3').value != '') {
                    xmlResult += "<ITEMS>";
                    xmlResult += "<ChkRemove></ChkRemove>";
                    xmlResult += "<MA_CAP>" + $get('MA_CAP_3').value + "</MA_CAP>";
                    xmlResult += "<MA_CHUONG>" + $get('MA_CHUONG_3').value + "</MA_CHUONG>";
                    xmlResult += "<MA_KHOAN>000</MA_KHOAN>";
                    xmlResult += "<MA_MUC>" + $get('MA_MUC_3').value + "</MA_MUC>";
                    xmlResult += "<NOI_DUNG>" + $get('NOI_DUNG_3').value + "</NOI_DUNG>";
                    xmlResult += "<SOTIEN>" + $get('SOTIEN_3').value + "</SOTIEN>";
                    xmlResult += "<SoDT></SoDT>";
                    xmlResult += "<KY_THUE>" + $get('KY_THUE_3').value + "</KY_THUE>";
                    xmlResult += "</ITEMS>";
                }
            }
            if (document.getElementById("chkRemove4").checked) {
                if ($get('SOTIEN_4').value != '') {
                    xmlResult += "<ITEMS>";
                    xmlResult += "<ChkRemove></ChkRemove>";
                    xmlResult += "<MA_CAP>" + $get('MA_CAP_4').value + "</MA_CAP>";
                    xmlResult += "<MA_CHUONG>" + $get('MA_CHUONG_4').value + "</MA_CHUONG>";
                    xmlResult += "<MA_KHOAN>000</MA_KHOAN>";
                    xmlResult += "<MA_MUC>" + $get('MA_MUC_4').value + "</MA_MUC>";
                    xmlResult += "<NOI_DUNG>" + $get('NOI_DUNG_4').value + "</NOI_DUNG>";
                    xmlResult += "<SOTIEN>" + $get('SOTIEN_4').value + "</SOTIEN>";
                    xmlResult += "<SoDT></SoDT>";
                    xmlResult += "<KY_THUE>" + $get('KY_THUE_4').value + "</KY_THUE>";
                    xmlResult += "</ITEMS>";
                }
            }




            xmlResult += "</CTU_DTL>";
            xmlResult += "</CTC_CTU>";
            //Lay thong tin cua phan chi tiet
            return xmlResult;
        }

        function jsGhi_CTU() {

            var objHDR = jsGetHDRObject2();
            //console.log(objHDR);
            var objDTL = jsGetDTLArray2();
            //console.log(objDTL);
            var sAct = "GHIDC";
            var sType = "";

            var bIsEdit = $('#hdfIsEdit').val();
            //console.log("bIsEdit: " + bIsEdit);
            if (bIsEdit == "true") {
                sType = "UPDATE_KS";
            } else {
                sType = "INSERT_KS";
            }

            jsValidNHSHB_SP_GhiCtu($get('txtSHKB').value);
            if (document.getElementById('hndNHSHB').value.length >= 8 && document.getElementById('hndNHSHB').value != $get('txtMA_NH_B').value) {
                if (!confirm("Kho bạc trên có mở tài khoản tại SHB. Bạn có muốn tiếp tục lưu chứng từ với ngân hàng hưởng đã chọn?")) {
                    jsEnableButtons(1);
                    return;
                }
            }

            for (var i = 1; i <= 4; i++) {
                if (document.getElementById("chkRemove" + i).checked) {
                    if (document.getElementById("MA_CHUONG_" + i).value.length < 3) {
                        alert("Mã chương " + i + " cần tối thiểu 3 ký tự");
                        document.getElementById("MA_CHUONG_" + i).value = "";
                        document.getElementById("MA_CHUONG_" + i).focus();
                        return;
                    } else if (document.getElementById("MA_MUC_" + i).value.length < 4) {
                        alert("Mã NDKT " + i + " cần tối thiểu 4 ký tự");
                        document.getElementById("MA_MUC_" + i).value = "";
                        document.getElementById("MA_MUC_" + i).focus();
                        return;
                    } else if (document.getElementById("NOI_DUNG_" + i).value.length < 1) {
                        alert("Nội dung " + i + " cần tối thiểu 4 ký tự");
                        document.getElementById("NOI_DUNG_" + i).value = "";
                        document.getElementById("NOI_DUNG_" + i).focus();
                        return;
                    }
                }
            }

            var strXML = jsFormToBean();
            //ghi thử xuống database
            $('#cmdGhiKS').prop('disabled', true);
            if (jsValidFormData(objHDR, objDTL, sAct, sType)) {

                $.ajax({
                    type: 'POST',
                    url: 'frmLapCTTruocBaCaNhan.aspx/Ghi_CTu',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({
                        ctuHdr: objHDR,
                        ctuDtl: objDTL,
                        sAction: sAct,
                        sSaveType: sType,
                        pv_strXML: strXML,
                    }),
                    success: function (reponse) {
                        var sRet = reponse.d.split(';');
                        //console.log(sRet);
                        if (sRet && sRet[0] == 'Success') {
                            //console.log(sRet[2]);
                            var sOut = '';
                            if ((sRet[2]) && (sRet[2].indexOf('/') > 0) && (sRet[2].split('/').length > 0)) {
                                sOut = sRet[2].split('/');
                            }

                            switch (sType) {
                                case "INSERT_KS":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Hoàn thiện & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Hoàn thiện & gửi kiểm soát thành công!');
                                    }
                                    jsEnableDisableControlButton('7');
                                    break;
                                case "UPDATE_KS":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Cập nhật & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Cập nhât & gửi kiểm soát thành công!');
                                    }
                                    jsEnableDisableControlButton('7');
                                    break;
                            }

                            //re-load lai chung tu len form (trong này đã có set hdfDescCTu)
                            if ((sOut) && (sOut.length > 1)) {
                                jsGetCTU(sOut[1]);
                            } else {
                                jsGetCTU(null);
                            }
                        } else {
                            $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                            switch (sType) {
                                case "INSERT_KS":
                                    alert('Hoàn thiện & gửi kiểm soát không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "UPDATE_KS":
                                    alert('Cập nhật & gửi kiểm soát không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('3');
                                    break;
                            }
                        }
                        $('#hdfIsEdit').val(true);
                        jsLoadCTList();
                    },
                    error: function (request, status, error) {
                        var err;
                        err = JSON.parse(request.responseText);

                    }
                });
            } else {
                //valid lỗi thì cho phép nhấn ghi ks lại
                $('#cmdGhiKS').prop('disabled', false);
            }


        }
        function IsNumeric(strString)//  check for valid numeric strings	     
        {
            var strValidChars = "0123456789.-";
            var strChar;
            var blnResult = true;

            if (strString.length == 0) return false;

            //  test strString consists of valid characters listed above
            for (i = 0; i < strString.length && blnResult == true; i++) {
                strChar = strString.charAt(i);
                if (strValidChars.indexOf(strChar) == -1) {
                    blnResult = false;
                }
            }
            return blnResult;
        }
        function isValidCitadCharacter(strString) {
            var iChars = "#\\!@#$%^*+=[]\\\'{}|\"<>?~_";
            for (var i = 0; i < strString.length; i++) {
                if (iChars.indexOf(strString.charAt(i)) != -1) {
                    return false;
                }
            }
            return true;
        }
        function isValidCitadCharacterNoiDung(strString) {
            //20150721 them validCitadCharacter
            var iChars = "#\\!@#$%^*+=[]\\\'{}|\"<>?~_";
            for (var i = 0; i < strString.length; i++) {
                if (iChars.indexOf(strString.charAt(i)) != -1) {
                    return false;
                }
            }
            return true;
        }

        function jsValidFormData(HDR, DTL, sAction, sType) {

            var tongTien = $('#txtTongTien').val().replaceAll('.', '');
            var tFee = $('#txtCharge').val().replaceAll('.', '');
            var tVAT = $('#txtVat').val().replaceAll('.', '');

            var tFee2 = $('#txtCharge2').val().replaceAll('.', '');
            var tVAT2 = $('#txtVat2').val().replaceAll('.', '');

            var tongTienThucThu = tongTien + tFee + tVAT + tFee2 + tVAT2
            if (tongTien <= 0) {
                alert('Chưa có dữ liệu nộp thuế !');
                return;
            }

            if (HDR.Ten_NNThue.length == 0) {
                alert('Tên người nộp thuế đang bị trống. Vui lòng kiểm tra lại');
                $('#txtTenNNT').focus();
                return false;
            }

            if (IsNumeric(HDR.Ten_NNThue)) {
                alert('Tên NNT không chấp nhận chỉ có số. Vui lòng kiểm tra lại.');
                document.getElementById('txtTenNNT').focus();
                return false;
            }

            if ($('#txtDChiNNT').val().length == 0) {
                alert("Cập nhật địa chỉ người nộp thuế");
                $('#txtDChiNNT').focus();
                return false;
            }


            if (HDR.MA_NH_TT.length != 8) {
                alert('Mã ngân hàng thanh toán không hợp lệ(bắt buộc 8 ký tự).');
                document.getElementById('txtMA_NH_TT').focus();
                return false;
            }
            if (HDR.MA_NH_B.length != 8) {
                alert('Mã ngân hàng thụ hưởng không hợp lệ(bắt buộc 8 ký tự).');
                document.getElementById('txtMA_NH_B').focus();
                return false;
            }
            var str_SO_CMND = document.getElementById('txtCK_SoCMND').value;
            if (str_SO_CMND.length > 15) {
                alert('Số CMND không được dài quá 15 ký tự. Vui lòng kiểm tra lại.');
                document.getElementById('txtCK_SoCMND').focus();
                return false;
            }
            var str_SO_DT = document.getElementById('txtCK_SoDT').value;
            if (str_SO_DT.length > 20) {
                alert('Số điện thoại không được dài quá 20 ký tự. Vui lòng kiểm tra lại.');
                document.getElementById('txtCK_SoDT').focus();
                return false;
            }

            var str_MaNNTien = document.getElementById('txtMaNNTien').value;
            if (str_MaNNTien.length > 14) {
                alert('Mã số thuế nộp thay không được dài quá 14 ký tự. Vui lòng kiểm tra lại.');

                return false;
            }

            //Valid ky tu la
            if (isValidCitadCharacter($get('txtTenNNT').value) == false) {
                alert("Tên NNT không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            } if (isValidCitadCharacter($get('txtMaNNTien').value) == false) {
                alert("Tên người nôp thay không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacter($get('txtDChiNNTien').value) == false) {
                alert("Địa chỉ người nộp tiền không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (document.getElementById('txtDChiNNTien').value.length > 70) {
                alert("Địa chỉ người nộp tiền không được vượt quá 70 ký tự.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacter($get('txtDChiNNT').value) == false) {
                alert("Địa chỉ NNT không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            //if (document.getElementById('txtDChiNNT').value.length>70)
            //{
            //    alert("Địa chỉ người nộp thuế không được vượt quá 70 ký tự.Hãy kiểm tra lại");
            //    return false;
            //}
            if (isValidCitadCharacterNoiDung($get('NOI_DUNG_1').value) == false) {
                alert("Nội dung nhập không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacterNoiDung($get('NOI_DUNG_2').value) == false) {
                alert("Nội dung nhập không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacterNoiDung($get('NOI_DUNG_3').value) == false) {
                alert("Nội dung nhập không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacterNoiDung($get('NOI_DUNG_4').value) == false) {
                alert("Nội dung nhập không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (parseInt($('#leftCharacter').text()) < 0) {
                alert('Vượt quá số ký tự cho phép. Vui lòng kiểm tra lại');
                return false;
            }
            ////valid total characters
            //if (!jsCalCharacter()) {
            //    alert('Vượt quá số ký tự cho phép. Vui lòng kiểm tra lại');
            //    return false;
            //}
            //validate HDR
            if (!HDR.Ma_NNThue) {
                alert('Vui lòng nhập vào Mã số thuế');
                $('#txtMaNNT').focus();
                return false;
            }

            if (HDR.Ma_NNThue.length > 14) {
                alert('Mã số thuế không được dài quá 14 ký tự. Vui lòng kiểm tra lại.');

                return false;
            }

            if (!HDR.Ma_CQThu) {
                alert('Vui lòng nhập vào mã CQT thu');
                $('#txtMaCQThu').focus();
                return false;
            }

            if (HDR.Ma_CQThu.length > 7) {
                alert('Mã CQ thu không được dài quá 7 ký tự. Vui lòng kiểm tra lại.');

                return false;
            }

            if ($('#txtTenCQThu').val().length == 0) {
                alert('Không tìm thấy thông tin CQT với mã nhập vào');
                $('#txtTenCQThu').focus();
                return false;
            }

            if (!HDR.SHKB) {
                alert('Vui lòng nhập vào SHKB');
                $('#txtSHKB').focus();
                return false;
            }

            if (HDR.SHKB.length > 4) {
                alert('SHKB không được dài quá 4 ký tự. Vui lòng kiểm tra lại.');

                return false;
            }
            if (!HDR.MA_NTK) {
                alert('Vui lòng nhập vào TK có NSNN');
                $('#ddlMa_NTK').focus();
                return false;
            }
            if ($('#txtTenKB').val().length == 0) {
                alert('Không tìm thấy thông tin kho bạc với mã nhập vào');
                $('#txtTenKB').focus();
                return false;
            }

            var str_DBHC = document.getElementById('txtMaDBHC').value;
            if (str_DBHC.trim().length < 5) {
                alert('Vui lòng nhập vào mã địa bàn hành chính hoặc mã địa bàn hành chính chưa đủ ký tự');
                $('#txtMaDBHC').focus();
                return false;
            }

            if (!HDR.TK_Co) {
                alert('Vui lòng nhập vào tài khoản có');
                $('#ddlMaTKCo').focus();
                return false;
            }

            if (HDR.TK_Co.length < 4) {
                alert('Tài khoản có không được ngắn quá 4 ký tự. Vui lòng kiểm tra lại.');

                return false;
            }



            if (!HDR.TK_KH_NH) {
                alert('Vui lòng nhập vào mã tài khoản chuyển');
                $('#txtTK_KH_NH').focus();
                return false;
            }




            var total = $('#txtTongTrichNo').val().replaceAll('.', '');
            var totalMoneyCus = $('#txtSoDu_KH_NH').val().replaceAll('.', '');
            if (parseFloat(total) > parseFloat(totalMoneyCus)) {
                alert('Tổng tiền trích nợ đang lớn hơn số tiền của khách hàng. Vui lòng kiểm tra lại.');
                return false;
            }


            if (document.getElementById("ddlMaHTTT").value == "01") {
                if (jsCheckEmptyVal('txtTK_KH_NH', document.getElementById('divStatus').id, "Tài khoản chuyển không được để trống") == false)
                    return false;
                //if (isGL == false) {
                //    if (jsValidSoDuKH()) {
                //        alert("TK của quý khách không đủ số dư để thực hiện giao dịch");
                //        return false;
                //    }
                //}
            }
            //if (document.getElementById("ddlMaHTTT").value == "03") {
            //    if (jsCheckEmptyVal('txtTKGL', document.getElementById('divStatus').id, "Tài khoản chuyển không được để trống") == false)
            //        return false;
            //    if (!checkTKGL()) {
            //        alert("Tài khoản GL không đúng");
            //        return false;
            //    }
            //}
            //if (document.getElementById("ddlMaHTTT").value == "00") {
            //    if (jsCheckEmptyVal('txtMaNNTien', document.getElementById('divStatus').id, "Mã người nộp tiền không được để trống") == false) {
            //        alert("Mã người nộp tiền không được để trống");
            //        $('#txtMaNNTien').focus();
            //        return false;
            //    }
            //    if (jsCheckEmptyVal('txtTenNNTien', document.getElementById('divStatus').id, "Tên người nộp tiền không được để trống") == false) {
            //        alert("Tên người nộp tiền không được để trống");
            //        $('#txtTenNNTien').focus();
            //        return false;
            //    }
            //}
            if (!HDR.MA_NH_A) {
                alert('Vui lòng nhập vào thông tin ngân hàng chuyển');
                $('#ddlMA_NH_A').focus();
                return false;
            }
            if (!HDR.MA_NH_TT) {
                alert('Vui lòng nhập vào mã ngân hàng trực tiếp');
                $('#txtMA_NH_TT').focus();
                return false;
            }
            if (!HDR.TEN_NH_TT) {
                alert('Không tìm thấy thông tin ngân hàng trực tiếp với mã nhập vào');
                $('#txtTEN_NH_TT').focus();
                return false;
            }
            if (!HDR.MA_NH_B) {
                alert('Vui lòng nhập vào mã ngân hàng thụ hưởng');
                $('#txtMA_NH_B').focus();
                return false;
            }
            if (!HDR.Ten_NH_B) {
                alert('Không tìm thấy thông tin ngân hàng thụ hưởng với mã nhập vào');
                $('#txtTenMA_NH_B').focus();
                return false;
            }
            if (HDR.Ma_LThue == "03") {
                if (!HDR.So_Khung) {
                    alert('Số khung không được để trống');
                    $('#txtSoKhung').focus();
                    return false;
                }
                if (!HDR.So_May) {
                    alert('Số máy không được để trống');
                    $('#txtSoMay').focus();
                    return false;
                }
                if (!HDR.DAC_DIEM_PTIEN) {
                    alert('Đặc điểm phương tiện không được để trống');
                    $('#txtDacDiemPhuongTien').focus();
                    return false;
                }
            }


            if (HDR.Ma_LThue == "08") {
                if (!HDR.MA_HS) {
                    alert('Mã hồ sơ không được để trống');
                    $('#txtMaHS').focus();
                    return false;
                }
                if (!HDR.SO_THUADAT) {
                    alert('Số thửa đất không được để trống');
                    $('#txtSoThuaDat').focus();
                    return false;
                }
                if (!HDR.SO_TO_BANDO) {
                    alert('Số tờ bản đồ không được để trống');
                    $('#txtSoToBanDo').focus();
                    return false;
                }
                if (!HDR.DIA_CHI_TS) {
                    alert('Địa chỉ tài sản không được để trống');
                    $('#txtDiaChiTS').focus();
                    return false;
                }
                if (!HDR.MA_DBHC_TS) {
                    alert('Địa bàn hành chính tài sản không được để trống');
                    $('#txtDBHCTS').focus();
                    return false;
                }
            }


            //validate DTL       
            if (!(DTL) || !(DTL instanceof Array) || !(DTL.length > 0)) { //IS: null, undefined, 0, NaN, false, or ""
                alert('Chứng từ thuế phải có chi tiết. Vui lòng kiểm tra lại');
                return false;
            }

            var flag = true;
            for (var j = 0; j < DTL.length; j++) {
                var objDTL = DTL[j];
                var ii = j + 2;
                if (objDTL.Ma_Chuong.length == 0) {
                    alert('Mã chương bị đang trống. Xin vui lòng nhập mã chương!');
                    flag = false;
                    break;
                }

                if (!jsCheckDM_CHUONG(objDTL.Ma_Chuong)) {
                    alert('Mã chương không tồn tại trong danh mục. Xin vui lòng nhập mã chương!');
                    flag = false;
                    break;
                }

                if (objDTL.Ma_TMuc.length == 0) {
                    alert('Mã tiểu mục bị đang trống. Xin vui lòng nhập mã tiểu mục!');
                    flag = false;
                    break;
                }

                //if (!jsCheckDM_TIEUMUC(objDTL.Ma_TMuc)) {
                //    alert('Mã tiểu mục không tồn tại trong danh mục. Xin vui lòng nhập mã tiểu mục!');
                //    flag = false;
                //    break;
                //}


                if (objDTL.Noi_Dung.length == 0) {
                    alert('Trường nội dung tiểu mục bị đang trống. Xin vui lòng nhập nội dung!');
                    flag = false;
                    break;
                }
                if (objDTL.SoTien.length == 0) {
                    alert('Số tiền nộp thuế bị đang trống. Xin vui lòng nhập số tiền!');
                    flag = false;
                    break;
                } else {
                    if (parseFloat(objDTL.SoTien) <= 0) {
                        alert('Số tiền nộp thuế bị đang nhỏ hơn 0. Xin vui lòng nhập số tiền!');
                        flag = false;
                        break;
                    }
                }
                if (objDTL.Ky_Thue.length == 0) {
                    alert('Trường kỳ thuế đang trống. Xin vui lòng nhập thông tin kỳ thuế!');
                    flag = false;
                    break;
                }
            }

            return flag;
        }

        function showNT() {
            var cb = document.getElementById('cboNguyenTe').value;
            $get('txtTenNT').value = cb;
        }

        function jsCheckDM_CHUONG(strChuong) {
            //xxxxxxxx
            // console.log(strTIEUMUC + " strTIEUMUC")
            if (arrDMCHUONG.length > 0) {
                for (var i = 0; i < arrDMCHUONG.length; i++) {
                    var arr = arrDMCHUONG[i].split(';');
                    if (arr[0] == strChuong) {
                        return true;
                    }
                }
            }
            return false;
        }
        function jsCheckDM_TIEUMUC(strTIEUMUC) {
            //xxxxxxxx
            //console.log(strTIEUMUC + " strTIEUMUC")
            if (arrDMTIEUMUC.length > 0) {
                for (var i = 0; i < arrDMTIEUMUC.length; i++) {
                    var arr = arrDMTIEUMUC[i].split(';');
                    if (arr[0] == strTIEUMUC) {
                        return true;
                    }
                }
            }
            return false;
        }
        function jsCheckEmptyVal(cmpCtrl, divCtrl, strMessage) {
            if (document.getElementById(cmpCtrl).value == '') {
                document.getElementById(divCtrl).innerHTML = strMessage;
                return false;
            }
            return true;
        }

        // hủy chứng từ
        function jsUpdate_CTU_Status(strCTUAction) {

            //set lai tham so timeout
            //SessionExpireAlert(_timeout, _rootPath);
            //console.log("1x");
            var blnEnableEdit = false;
            var strSo_FT = document.getElementById('hdfSo_CTu').value;
            var strLyDoHuy = document.getElementById('txtLyDoHuy').value;
            // console.log("2x");
            if (document.getElementById('hdfDescCTu').value.length > 0) {
                //console.log("3x");
                var arrDesCTU = document.getElementById('hdfDescCTu').value.split(":")[1].split("/");
                //console.log("4x");
                if (strCTUAction == '2') {//Chuyen kiem soat
                    curCtuStatus = '05';
                    jsEnableButtons(5);//disable tat ca cac nut                 
                    blnEnableEdit = true;
                } else if (strCTUAction == '4') {//Huy
                    if (strLyDoHuy == '') {
                        alert('Bạn phải nhập vào lý do Hủy.');
                        return;
                    }
                    if (!confirm("Bạn có thực sự muốn hủy chứng từ này không?")) {
                        return;
                    }
                    if (document.getElementById('hdnMA_KS').value.toString() != '' && curCtuStatus == '01') {
                        curCtuStatus = '02';
                        jsEnableButtons(3);//disable tat ca cac nut tru nut khoi phuc                
                        blnEnableEdit = true;
                    }
                    else {
                        curCtuStatus = '04';
                        jsEnableButtons(3);//disable tat ca cac nut tru nut khoi phuc                
                        blnEnableEdit = true;
                    }
                } else if (strCTUAction == '5') {//Khoi phuc
                    curCtuStatus = '99';//de trang thai 99 chu khong phai 00
                    jsEnableButtons(2);//enable tat ca cac nut tru nut in                                
                    blnEnableEdit = false;
                }
                // console.log("9x");
                //Lay trang thai cua chung tu cho biet
                jsEnableGridHeader(blnEnableEdit);
                jsEnableGridDetail(blnEnableEdit);
                document.getElementById('cmdThemMoi').disabled = true;
                document.getElementById('cmdGhiKS').disabled = true;
                //document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                //document.getElementById('cmdInBS').disabled = true;
                //document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = true;
                //alert(curCtuStatus);
                //PageMethods.Update_CTU_Status(curMa_NV,arrDesCTU[0],arrDesCTU[1],arrDesCTU[2],arrDesCTU[3],arrDesCTU[4],strCTUAction,Update_CTU_Status_Complete,Update_CTU_Status_Error);
                PageMethods.Update_CTU_Status(curMa_NV, arrDesCTU[0], arrDesCTU[1], arrDesCTU[2], strCTUAction, strSo_FT, curCtuStatus, strLyDoHuy, Update_CTU_Status_Complete, Update_CTU_Status_Error);
            }
        }
        function Update_CTU_Status_Complete(result, userContext, methodName) {
            checkSS(result);
            if (result.length > 0) {
                var arrKQ = result.split(';');
                if (arrKQ[0] == "1") {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];
                    //                alert('Hủy chứng từ');
                    //                document.getElementById('cmdInCT').disabled = true ;

                }
                else if (arrKQ[0] == "05") {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];
                    jsLoadCTList();
                    document.getElementById('hdfDescCTu').value = arrKQ[2];
                    //alert('Đổi trạng thái 05');
                    document.getElementById('cmdInCT').disabled = false;
                }
                else if (arrKQ[0] == "04") {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];
                    jsLoadCTList();
                    document.getElementById('hdfDescCTu').value = arrKQ[2];
                    //alert('Hủy');
                    document.getElementById('cmdInCT').disabled = true;
                }
                document.getElementById('cmdThemMoi').disabled = false;
                document.getElementById('txtLyDoHuy').disabled = true;

                jsLoadCTList();
            }
            else {
                document.getElementById('divStatus').innerHTML = "Thao tác không thành công.Hãy thực hiện lại!!!"
                //phuc hoi nguyen trang thai cac nut truoc thuc hien
                document.getElementById('cmdThemMoi').disabled = false;
                document.getElementById('cmdGhiKS').disabled = false;
                //document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = false;
                document.getElementById('cmdInCT').disabled = true;
                //document.getElementById('cmdInBS').disabled = false;
                // document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = false;
            }
        }
        function Update_CTU_Status_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình cập nhật trạng thái chứng từ" + error.get_message();
                //phuc hoi nguyen trang thai cac nut truoc thuc hien cmdThemMoi
                document.getElementById('cmdGhiKS').disabled = false;
                document.getElementById('cmdThemMoi').disabled = false;
                //document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = false;
                document.getElementById('cmdInCT').disabled = true;
                // document.getElementById('cmdInBS').disabled = false;
                //document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = false;
            }
        }
        function jsEnableButtons(intButtonType) {
            //alert (intButtonType);
            if (intButtonType == 0) //trang thai ban dau : ko cho hien thi nut nao ca
            {
                document.getElementById('cmdGhiKS').disabled = true;
                //document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                // document.getElementById('cmdInBS').disabled = true;
                // document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = true;
                //document.getElementById('cmdTinhPhi').disabled = true;
            }
            else if (intButtonType == 1) //sau khi da co ma so thue ->hien thi 2 nut ghi
            {
                document.getElementById('cmdGhiKS').disabled = false;
                // document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                //document.getElementById('cmdInBS').disabled = true;
                //document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = true;
                //document.getElementById('cmdTinhPhi').disabled = false;
            }
            else if (intButtonType == 2) //sau khi an nut ghi thanh cong 
            {
                document.getElementById('cmdGhiKS').disabled = false;
                //document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = false;
                document.getElementById('cmdInCT').disabled = true;
                // document.getElementById('cmdInBS').disabled = true;
                // document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = false;
                //document.getElementById('cmdTinhPhi').disabled = false;
            }
            else if (intButtonType == 3) // sau khi an nut huy thanh cong
            {
                document.getElementById('cmdGhiKS').disabled = true;
                // document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                // document.getElementById('cmdInBS').disabled = true;
                // document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = true;
                // document.getElementById('cmdTinhPhi').disabled = true;
            }
            else if (intButtonType == 4) // chung tu da huy cho phep khoi phuc chung tu
            {
                document.getElementById('cmdGhiKS').disabled = true;
                // document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                // document.getElementById('cmdInBS').disabled = true;
                // document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = true;
                //document.getElementById('cmdTinhPhi').disabled = true;
            }
            else if (intButtonType == 5) // chung tu da chuyen sang trang thai cho kiem soat 
            {
                document.getElementById('cmdGhiKS').disabled = true;
                //  document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = false;
                //  document.getElementById('cmdInBS').disabled = true;
                // document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = true;
                // document.getElementById('cmdTinhPhi').disabled = true;
            }
            else if (intButtonType == 6) // chung tu  da duoc kiem soat
            {
                document.getElementById('cmdGhiKS').disabled = true;
                // document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = false;
                //document.getElementById('cmdInBS').disabled = true;
                // document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = false;
                //document.getElementById('cmdTinhPhi').disabled = true;

            }
            else if (intButtonType == 7) // chung tu o trang thai Chuyen tra
            {
                document.getElementById('cmdGhiKS').disabled = false;
                // document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = false;
                document.getElementById('cmdInCT').disabled = true;
                // document.getElementById('cmdInBS').disabled = true;
                // document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = false;
            } else if (intButtonType == 8) // sau khi an nut huy chung tu da kiem soat thanh cong
            {
                document.getElementById('cmdGhiKS').disabled = true;
                //document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                // document.getElementById('cmdInBS').disabled = true;
                // document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = true;
                // document.getElementById('cmdTinhPhi').disabled = true;
            }
            //        if(document.getElementById('ddlMaHTTT').value=='00')
            //        {
            //            jsEnabledButtonByBDS();     
            //        }
        }
        function jsEnableGridHeader(blnEnable) {
            //if( document.getElementById('txtMaNNT').value==defMaVangLai)
            //{
            document.getElementById('txtTenNNT').disabled = blnEnable;
            //}
            document.getElementById('cboNguyenTe').disabled = blnEnable;
            document.getElementById('ddlMaHTTT').disabled = blnEnable;
            document.getElementById('ddlMaLoaiThue').disabled = blnEnable;
            document.getElementById('ddlMaTKCo').disabled = blnEnable;
            document.getElementById('ddlMaTKNo').disabled = blnEnable;
            document.getElementById('ddlMa_NTK').disabled = blnEnable;
            document.getElementById('txtTKNo').disabled = blnEnable;
            document.getElementById('txtTKCo').disabled = 'none';

            document.getElementById('txtTenLoaiNNT').disabled = blnEnable;
            document.getElementById('txtMaLoaiNNT').disabled = blnEnable;

            document.getElementById('txtCharge').disabled = blnEnable;
            document.getElementById('txtCharge2').disabled = blnEnable;


            $get('txtCQQD').disabled = blnEnable;
            $get('txtSOQD').disabled = blnEnable;
            $get('txtNgayQD').disabled = blnEnable;
            $get('txtSHKB').disabled = blnEnable;
            $get('txtMaNNT').disabled = blnEnable;
            $get('txtTenNNT').disabled = blnEnable;
            $get('txtDChiNNT').disabled = blnEnable;
            $get('txtMaNNTien').disabled = blnEnable;
            $get('txtTenNNTien').disabled = blnEnable;
            $get('txtDChiNNTien').disabled = blnEnable;
            $get('txtQuan_HuyenNNTien').disabled = blnEnable;
            $get('txtHuyen_NNT').disabled = blnEnable;
            $get('txtTenHuyen_NNT').disabled = blnEnable;
            $get('txtTinh_NNT').disabled = blnEnable;
            $get('txtTenTinh_NNT').disabled = blnEnable;
            $get('txtTenQuan_HuyenNNTien').disabled = blnEnable;
            $get('txtTenTinh_NNTien').disabled = blnEnable;
            $get('txtTinh_NNTien').disabled = blnEnable;
            $get('txtMA_NH_TT').disabled = blnEnable;
            $get('txtMA_NH_B').disabled = blnEnable;
            //$get('txtMaDBHC').disabled = blnEnable;
            $get('txtMaCQThu').disabled = blnEnable;
            $get('txtTK_KH_NH').disabled = blnEnable;
            //         $get('ddlDSTK_KH').disabled = blnEnable;
            //$get('txtTKGL').disabled = blnEnable;
            $get('txtNGAY_KH_NH').disabled = blnEnable;
            $get('txtDesNGAY_KH_NH').disabled = blnEnable;

            $get('txtSoKhung').disabled = blnEnable;
            $get('txtSoMay').disabled = blnEnable;

            $get('chkChargeTypeMP').disabled = blnEnable;
            $get('chkChargeTypePT').disabled = blnEnable;
            $get('chkChargeTypePN').disabled = blnEnable;
            document.getElementById('ddlMaHTTT').disabled = blnEnable;
            document.getElementById('ddlMaLoaiThue').disabled = blnEnable;
            document.getElementById('ddlMA_NH_A').disabled = blnEnable;
            //document.getElementById('txtLyDoHuy').disabled = blnEnable;
            document.getElementById('txtMaNNTien').disabled = blnEnable;
            document.getElementById('txtTenNNTien').disabled = blnEnable;
            document.getElementById('txtCK_SoCMND').disabled = blnEnable;
            document.getElementById('txtDChiNNTien').disabled = blnEnable;
            document.getElementById('txtDG_NHB').disabled = blnEnable;
            document.getElementById('txtCK_SoDT').disabled = blnEnable;

        }

        function jsEnableGridDetail(blnEnable) {
            //document.getElementById('grdChiTiet').disabled=blnEnable;
            //Kienvt:Sua lai enabled grid thi an tung dieu kihen
            $get('chkRemove1').disabled = blnEnable;
            $get('MA_CHUONG_1').disabled = blnEnable;
            $get('MA_KHOAN_1').disabled = blnEnable;
            $get('MA_MUC_1').disabled = blnEnable;
            $get('NOI_DUNG_1').disabled = blnEnable;
            $get('SOTIEN_1').disabled = blnEnable;
            $get('KY_THUE_1').disabled = blnEnable;

            $get('chkRemove2').disabled = blnEnable;
            $get('MA_CHUONG_2').disabled = blnEnable;
            $get('MA_KHOAN_2').disabled = blnEnable;
            $get('MA_MUC_2').disabled = blnEnable;
            $get('NOI_DUNG_2').disabled = blnEnable;
            $get('SOTIEN_2').disabled = blnEnable;
            $get('KY_THUE_2').disabled = blnEnable;

            $get('chkRemove3').disabled = blnEnable;
            $get('MA_CHUONG_3').disabled = blnEnable;
            $get('MA_KHOAN_3').disabled = blnEnable;
            $get('MA_MUC_3').disabled = blnEnable;
            $get('NOI_DUNG_3').disabled = blnEnable;
            $get('SOTIEN_3').disabled = blnEnable;
            $get('KY_THUE_3').disabled = blnEnable;

            $get('chkRemove4').disabled = blnEnable;
            $get('MA_CHUONG_4').disabled = blnEnable;
            $get('MA_KHOAN_4').disabled = blnEnable;
            $get('MA_MUC_4').disabled = blnEnable;
            $get('NOI_DUNG_4').disabled = blnEnable;
            $get('SOTIEN_4').disabled = blnEnable;
            $get('KY_THUE_4').disabled = blnEnable;

        }
        function checkSS(param) {

            var arrKQ = param.split(';');
            if (arrKQ[0] == "ssF") {
                window.open("../../pages/Warning.html", "_self");
                return;
            }
        }

        function jsIn_CT(isBS) {
            //set lai tham so timeout
            //SessionExpireAlert(_timeout, _rootPath);

            var width = screen.availWidth - 100;
            var height = screen.availHeight - 10;
            var left = 0;
            var top = 0;
            var params = 'width=' + width + ', height=' + height;
            params += ', top=' + top + ', left=' + left;
            params += ', directories=no';
            params += ', location=no';
            params += ', menubar=yes';
            params += ', resizable=no';
            params += ', scrollbars=yes';
            params += ', status=no';
            params += ', toolbar=no';
            if (document.getElementById('hdfDescCTu').value.length > 0) {
                var strSHKB = document.getElementById('hdfDescCTu').value.split(':')[1].split('/')[0];
                var strSoCT = document.getElementById('hdfDescCTu').value.split(':')[1].split('/')[1];
                var strSo_BT = document.getElementById('hdfDescCTu').value.split(':')[1].split('/')[2];
                var MaNV = curMa_NV;
                var ngayLV = dtmNgayLV;
            }
            //window.open("../../pages/Baocao/InCtu.ashx?SoCT=" + strSoCT + "&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=" + isBS, "", params);
            window.open("../../pages/Baocao/InGNTpdf.ashx?SoCT=" + strSoCT + "&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=" + isBS, "", params);
        }

        function ShowLov(strType) {
            if (strType == "NNT") return FindNNT_NEW('txtMaNNT', 'txtTenNNT');
            //if (strType == "SHKB") return FindDanhMuc('KhoBac', 'txtSHKB', 'txtTenKB', 'txtSHKB');
            //if (strType == "DBHC") return FindDanhMuc('DBHC', 'txtMaDBHC', 'txtTenDBHC', 'txtMaCQThu');
            //if (strType == "CQT") return FindDanhMuc('CQThu_Thue', 'txtMaCQThu', 'txtTenCQThu', 'ddlMaTKNo');
            //if (strType == "LHXNK") return FindDanhMuc('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSo_BK');
            //if (strType == "DMNH_TT") return FindDanhMuc('DMNH_TT', 'txtMA_NH_TT', 'txtTEN_NH_TT', 'txtTEN_NH_TT');
            //if (strType == "DMNH_GT") return FindDanhMuc('DMNH_GT', 'txtMA_NH_B', 'txtTenMA_NH_B', 'txtTenMA_NH_B');
            //if (strType == "DBHC_NNT") return FindDanhMuc('DBHC_NNT', 'txtHuyen_NNT', 'txtTenHuyen_NNT', '');
            //if (strType == "DBHC_NNTHUE") return FindDanhMuc('DBHC_NNTHUE', 'txtQuan_HuyenNNTien', 'txtTenQuan_HuyenNNTien', 'txtQuan_HuyenNNTien');
            ////sonmt
            //if (strType == "LOAI_NNTHUE") return FindDanhMuc('LOAI_NNTHUE', 'txtMaLoaiNNT', 'txtTenLoaiNNT', 'txtMaLoaiNNT');


            if (strType == "SHKB") return ShowDMKB('KhoBac', 'txtSHKB', 'txtTenKB', 'txtSHKB');
            if (strType == "DBHC") return ShowDMKB('DBHC', 'txtMaDBHC', 'txtTenDBHC', 'txtMaCQThu');
            if (strType == "CQT") return ShowDMKB('CQThu_Thue', 'txtMaCQThu', 'txtTenCQThu', 'ddlMaTKNo');
            if (strType == "LHXNK") return ShowDMKB('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSo_BK');
            if (strType == "DMNH_TT") return ShowDMKB('DMNH_TT', 'txtMA_NH_TT', 'txtTEN_NH_TT', 'txtTEN_NH_TT');
            if (strType == "DMNH_GT") return ShowDMKB('DMNH_GT', 'txtMA_NH_B', 'txtTenMA_NH_B', 'txtTenMA_NH_B');
            if (strType == "DBHC_NNT") return ShowDMKB('DBHC_NNT', 'txtHuyen_NNT', 'txtTenHuyen_NNT', '');
            if (strType == "DBHC_NNTHUE") return ShowDMKB('DBHC_NNTHUE', 'txtQuan_HuyenNNTien', 'txtTenQuan_HuyenNNTien', 'txtQuan_HuyenNNTien');
            //sonmt
            // if (strType == "LOAI_NNTHUE") return FindDanhMuc('LOAI_NNTHUE', 'txtMaLoaiNNT', 'txtTenLoaiNNT', 'txtMaLoaiNNT');
            if (strType == "LOAI_NNTHUE") return ShowDMKB('LOAI_NNTHUE', 'txtMaLoaiNNT', 'txtTenLoaiNNT', 'txtMaLoaiNNT');


        }
        //function FindNNT_NEW(txtID, txtTitle) {
        //    var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?initParam=" + $get('txtMaNNT').value + "&SHKB=" + $get('txtSHKB').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        //    if (returnValue != null) {
        //        document.getElementById(txtID).value = returnValue.ID;
        //        document.getElementById(txtTitle).value = returnValue.Title;
        //    }
        //    NextFocus();
        //}
        function FindNNT_NEW(txtID, txtTitle) {
            var strSHKB;
            if (document.getElementById('txtSHKB').value.length > 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            else {
                strSHKB = defSHKB.split(';')[0];
            }
            var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?SHKB=" + strSHKB + "&initParam=" + $get('txtMaNNT').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
            NextFocus();
        }
        function NextFocus(event) {
            if (event.keyCode == 13) {
                event.preventDefault;
                event.keyCode = 9;
                // return false;
            }

        }
        function FindDanhMuc(strPage, txtID, txtTitle, txtFocus) {
            var strSHKB;
            var returnValue;
            if (document.getElementById('txtSHKB').value.length > 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            else {
                strSHKB = defSHKB.split(';')[0];
            }

            returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                if (strPage == "KhoBac") {
                    jsSHKB_lostFocus(returnValue.ID);
                }
                if (txtTitle != null) {
                    document.getElementById(txtTitle).value = returnValue.Title;

                }
                if (txtFocus != null) {
                    document.getElementById(txtFocus).focus();
                }
            }

        }
        function ShowMLNS(strType) {
            //if (strType == "MC_01") return FindDanhMuc('CapChuong', 'MA_CHUONG_1', null, 'MA_MUC_1');
            //if (strType == "MC_02") return FindDanhMuc('CapChuong', 'MA_CHUONG_2', null, 'MA_MUC_2');
            //if (strType == "MC_03") return FindDanhMuc('CapChuong', 'MA_CHUONG_3', null, 'MA_MUC_3');
            //if (strType == "MC_04") return FindDanhMuc('CapChuong', 'MA_CHUONG_4', null, 'MA_MUC_4');

            //if (strType == "MM_01") return FindDanhMuc('MucTMuc', 'MA_MUC_1', 'NOI_DUNG_1', 'NOI_DUNG_1');
            //if (strType == "MM_02") return FindDanhMuc('MucTMuc', 'MA_MUC_2', 'NOI_DUNG_2', 'NOI_DUNG_2');
            //if (strType == "MM_03") return FindDanhMuc('MucTMuc', 'MA_MUC_3', 'NOI_DUNG_3', 'NOI_DUNG_3');
            //if (strType == "MM_04") return FindDanhMuc('MucTMuc', 'MA_MUC_4', 'NOI_DUNG_4', 'NOI_DUNG_4');

            if (strType == "MC_01") return ShowDMKB('CapChuong', 'MA_CHUONG_1', null, 'MA_MUC_1');
            if (strType == "MC_02") return ShowDMKB('CapChuong', 'MA_CHUONG_2', null, 'MA_MUC_2');
            if (strType == "MC_03") return ShowDMKB('CapChuong', 'MA_CHUONG_3', null, 'MA_MUC_3');
            if (strType == "MC_04") return ShowDMKB('CapChuong', 'MA_CHUONG_4', null, 'MA_MUC_4');

            if (strType == "MM_01") return ShowDMKB('MucTMuc', 'MA_MUC_1', 'NOI_DUNG_1', 'NOI_DUNG_1');
            if (strType == "MM_02") return ShowDMKB('MucTMuc', 'MA_MUC_2', 'NOI_DUNG_2', 'NOI_DUNG_2');
            if (strType == "MM_03") return ShowDMKB('MucTMuc', 'MA_MUC_3', 'NOI_DUNG_3', 'NOI_DUNG_3');
            if (strType == "MM_04") return ShowDMKB('MucTMuc', 'MA_MUC_4', 'NOI_DUNG_4', 'NOI_DUNG_4');
        }

        function jsLoadByCQT() {

            var strMaCQThu = $get('txtMaCQThu').value;
            if (strMaCQThu.length > 0) {
                PageMethods.Get_DataByCQT(strMaCQThu, LoadByCQT_Complete, LoadByCQT_Error);
            }
        }
        function LoadByCQT_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            if (result.lenght != 0) {
                var arr = result.split("|");
                if (arr[2]) {
                    $get('txtTenCQThu').value = arr[2].toString();
                    $get('txtSHKB').value = arr[1].toString();
                }
                else {
                    $get('txtTenCQThu').value = "";
                    //$get('txtMaCQThu').value = "";
                    alert("Mã cơ quan thu này đã ngừng hoạt động");
                    $get('txtTenCQThu').value = "";
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin cơ quan thu với mã nhập vào';
                }
            }
        }
        function LoadByCQT_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = 'Không lấy được dữ liệu theo cơ quan thu';
            }
        }
        function jsCallSo_DaThu(txtMaChuong, txtMa_TMuc, txtKyThue, txtSo_DThu) {
            var strMaChuong = document.getElementById(txtMaChuong).value;
            var strMaTMuc = document.getElementById(txtMa_TMuc).value;
            var strKyThue = document.getElementById(txtKyThue).value;
            var strMa_NNT = document.getElementById('txtMaNNT').value;
            if (strMa_NNT.trim == "" || strKyThue == "" || strMaChuong == "" || strMaTMuc == "") { return; }
            PageMethods.CallSo_DT(strMa_NNT, "", strMaChuong, "", strMaTMuc, strKyThue, txtSo_DThu, CallSo_DT_Complete, CallSo_DT_Error);
        }
        //ManhNV sua lay NH truc tiep - gian tiep 01/10/2012
        function jsGetTTNganHangNhan() {
            //07/06/2011: Kienvt lay thong tin ngan hang nhan
            var strSHKB = $get('txtSHKB').value;
            var strDBHC = '';//$get('txtMaDBHC').value;
            var strCQThu = '';//$get('txtMaCQThu').value;   

            PageMethods.GetTT_NH_NHAN(strSHKB, GetTT_NganHangNhan_Complete, GetTT_NganHangNhan_Error);

        }
        //Kienvt: them phan get thong tin ngan hang nhan
        function GetTT_NganHangNhan_Complete(result, methodName) {
            checkSS(result);
            //alert(result.toString());
            if (result.length > 0) {
                var arr2 = result.split(';');
                //alert(arr2[0]);
                $get('txtMA_NH_B').value = arr2[0];
                $get('txtTenMA_NH_B').value = arr2[1];
                $get('txtMA_NH_TT').value = arr2[2];
                $get('txtTEN_NH_TT').value = arr2[3];
                //20150715 - Handx bo auto lay truong nay
                //document.getElementById('txtDG_NHB').value=$get('txtTenMA_NH_B').value;
                jsValidNHSHB_SP($get('txtSHKB').value);
                document.getElementById('divStatus').innerHTML = '';
            }
            else {
                $get('txtMA_NH_B').value = '';
                $get('txtTenMA_NH_B').value = '';
                $get('txtMA_NH_TT').value = '';
                $get('txtTEN_NH_TT').value = '';
                //07/06/2011 : Kienvt focus vao truong tai khoan khi so nhap bi sai
                //$get('txtTK_KH_NH').value='';
                //$get('txtTK_KH_NH').focus();
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin ngân hàng nhận.Hãy kiểm tra lại';
            }
        }
        //END-ManhNV sua lay NH truc tiep - gian tiep 01/10/2012 
        //Kienvt: them phan get thong tin ngan hang nhan
        function GetTT_NganHangNhan_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng nhận" + error.get_message();
            }
        }

        function jsGet_TenKB() {
            if (arrSHKB.length > 0) {
                for (var i = 0; i < arrSHKB.length; i++) {
                    var arr = arrSHKB[i].split(';');
                    if (arr[0] == $get('txtSHKB').value.trim()) {
                        $get('txtTenKB').value = arr[1];
                        break;
                    } else {
                        $get('txtTenKB').value = '';
                    }
                }
            }
        }
        //function jsGet_TenTKGL() {
        //    //alert(arrTKGL)
        //    if (arrTKGL.length > 0) {
        //        for (var i = 0; i < arrTKGL.length; i++) {
        //            var arr = arrTKGL[i].split(';');
        //            if ($get('txtTKGL').value == arr[0]) {
        //                $get('txtTK_KH_NH').value = arr[0];
        //                //$get('txtTenTK_KH_NH').value = arr[1];
        //                $get('txtTenTKGL').value = arr[1];
        //            }
        //            break;
        //        }
        //    }
        //}

        function jsSHKB_lostFocus(strSHKB) {
            // jsLoadDM('SHKB',strSHKB);
            jsLoadDM('KHCT', strSHKB);
            jsLoadDM('DBHC', strSHKB);
            jsLoadDM('CQTHU', strSHKB);
            jsLoadDM('CQTHU_DEF', strSHKB);
            jsLoadDM('DBHC_DEF', strSHKB);
            jsLoadDM('TAIKHOANNO', strSHKB);
            jsLoadDM('TAIKHOANCO', strSHKB);
            jsLoadDM('CQTHU_SHKB', strSHKB);

            jsLoadDM('TINH_SHKB', strSHKB);
            //jsGanNH_HUONG();
            jsCalCharacter();
        }
        function jsGanNH_HUONG() {
            document.getElementById('txtDG_NHB').value = $get('txtTenMA_NH_B').value;
        }
        function jsLoadDM(strLoaiDM, strSHKB) {
            //alert(strSHKB);
            if (strSHKB.length == 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            if (strSHKB.length > 0) {
                PageMethods.GetData_SHKB(strLoaiDM, strSHKB, LoadDM_Complete, LoadDM_Error);
            }
            else {
                jsClearGridHeader(true, true);//khong xoa gt mac dinh                                 
                jsClearGridDetail();
                jsSetDefaultValues();
            }
        }
        function LoadDM_Complete(result, userContext, methodName) {
            checkSS(result);
            var mangKQ = new Array;
            mangKQ = result.toString().split('|');
            if (mangKQ[0] == 'DBHC') {
                arrDBHC = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrDBHC[i - 1] = strTemp;
                }

            }

            else if (mangKQ[0] == 'TAIKHOANNO') {
                arrTKNoNSNN = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrTKNoNSNN[i - 1] = strTemp;
                }
                // jsFillTKNo();
            }
            else if (mangKQ[0] == 'TAIKHOANCO') {
                arrTKCoNSNN = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrTKCoNSNN[i - 1] = strTemp;
                }
                //jsFillTKCo();
            }
            else if (mangKQ[0] == 'CQTHU') {
                arrCQThu = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrCQThu[i - 1] = strTemp;
                }
            }
            else if (mangKQ[0] == 'KHCT') {

                if (mangKQ[1].length != 0) {

                    document.getElementById('txtKyHieuCT').value = mangKQ[1].split(';')[0];
                }
                else {
                    document.getElementById('txtKyHieuCT').value = '';
                }
            }
            else if (mangKQ[0] == 'CQTHU_SHKB') {
                //tạm thời đóng lại- chỉ cho load cơ quan thu ra kho bạc
                if (mangKQ[1].length != 0) {
                    //document.getElementById('txtMaCQThu').value = mangKQ[1].split(';')[1];
                    //document.getElementById('txtTenCQThu').value = mangKQ[1].split(';')[2];
                }
                else {
                    //document.getElementById('txtMaCQThu').value='';
                    //document.getElementById('txtTenCQThu').value = '';
                    //alert("Mã cơ quan thu này đã ngừng hoạt động");
                    // document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin Cơ quan thu với SHKB này.';
                }

            }
            else if (mangKQ[0] == 'TINH_SHKB') {
                if (mangKQ[1].length != 0) {
                    document.getElementById('txtMaDBHC').value = mangKQ[1].split(';')[0];
                    document.getElementById('txtTenDBHC').value = mangKQ[1].split(';')[1];
                }
                else {
                    document.getElementById('txtMaDBHC').value = '';
                    document.getElementById('txtTenDBHC').value = '';
                }

            }

        }
        function LoadDM_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML += 'Không lấy được dữ liệu liên quan đến kho bạc này';
                //phuc hoi nguyen trang thao cac nut truoc thuc hien
            }
        }
        function jsGet_TenNH_TT() {
            if (arrTT_NH_TT.length > 0) {
                $get('txtTEN_NH_TT').value = '';
                for (var i = 0; i < arrTT_NH_TT.length; i++) {
                    var arr = arrTT_NH_TT[i].split(';');
                    if (arr[0] == $get('txtMA_NH_TT').value.trim()) {
                        $get('txtTEN_NH_TT').value = arr[1];
                        break;
                    }
                }
            }
            if ($get('txtMA_NH_TT').value == '') {
                //alert('Không tìm thấy thông tin NH thanh toán');
            }
        }
        function jsGet_TenNH_B() {
            if (arrTT_NH_B.length > 0) {
                $get('txtTenMA_NH_B').value = '';
                for (var i = 0; i < arrTT_NH_B.length; i++) {
                    var arr = arrTT_NH_B[i].split(';');
                    if (arr[0] == $get('txtMA_NH_B').value.trim() && arr[4] == $get('txtSHKB').value.trim()) {
                        $get('txtTenMA_NH_B').value = arr[1];
                        $get('txtMA_NH_TT').value = arr[2];
                        $get('txtTEN_NH_TT').value = arr[3];
                        break;
                    }
                }
            }
            if ($get('txtTenMA_NH_B').value == '') {
                alert('Không tìm thấy thông tin NH thụ hưởng');
            }
        }
        function jsThem_Moi() {
            //jsClearGridHeader(true, true);//khong xoa gt mac dinh      
            jsClearHDR_Panel();
            $get('hdfDescCTu').value = '';
            jsClearGridDetail();
            jsEnableGridHeader(false);
            jsEnableGridDetail(false);
            $('#cmdGhiKS').prop('disabled', false);
            jsEnableButtons(0);
            jsLoadCTList();
            // jsSetDefaultValues();
        }
        function jsSetDefaultValues() {
            document.getElementById('txtNGAY_KH_NH').value = dtmNgayNH;//dtmNgayLV;  
            document.getElementById('txtTenNT').value = 'VND';
            document.getElementById('cboNguyenTe').value = 'VND';
            if (defSHKB.length > 0) {
                var arr1 = defSHKB.split(';');
                if ($get('txtSHKB').value.length == 0) {
                    $get('txtSHKB').value = arr1[0];
                    $get('txtTenKB').value = arr1[1];
                    jsLoadDM('TAIKHOANNO', arr1[0]);
                    jsLoadDM('TAIKHOANCO', arr1[0]);

                }
            }
            if (defCQThu.length > 0) {
                var arr2 = defCQThu.split(';');
                if ($get('txtMaCQThu').value.length == 0) {
                    $get('txtMaCQThu').value = arr2[0];
                    $get('txtTenCQThu').value = arr2[1];
                }
            }
            if (defKHCT.length > 0) {
                var arr2 = defKHCT.split(';');
                if ($get('txtKyHieuCT').value.length == 0) {
                    $get('txtKyHieuCT').value = arr2[0];

                }
            }
            if (defDBHC.length > 0) {
                var arr3 = defDBHC.split(';');
                if ($get('txtMaDBHC').value.length == 0) {
                    $get('txtMaDBHC').value = arr3[0];
                    $get('txtTenDBHC').value = arr3[1];
                }
            }
            //07/06/11 Them default cho defMaNHNhan
            if (defMaNHNhan.length > 0) {
                var arr4 = defMaNHNhan.split(';');
                if ($get('txtMA_NH_B').value.length == 0) {
                    $get('txtMA_NH_B').value = arr4[0];
                    $get('txtTenMA_NH_B').value = arr4[1];
                    $get('txtMA_NH_TT').value = arr4[2];
                    $get('txtTEN_NH_TT').value = arr4[3];
                    //jsGanNH_HUONG();
                }
            }
        }
        function jsClearGridDetail() {
            // $get('MA_KHOAN_1').value = '';     
            $get('MA_MUC_1').value = '';
            $get('NOI_DUNG_1').value = '';
            $get('SOTIEN_1').value = '';
            $get('SODATHU_1').value = '';
            $get('KY_THUE_1').value = curKyThue;


            $get('MA_CHUONG_2').value = '';
            //$get('MA_KHOAN_2').value = '';     
            $get('MA_MUC_2').value = '';
            $get('NOI_DUNG_2').value = '';
            $get('SOTIEN_2').value = '';
            $get('SODATHU_2').value = '';
            $get('KY_THUE_2').value = '';


            $get('MA_CHUONG_3').value = '';
            //  $get('MA_KHOAN_3').value = '';     
            $get('MA_MUC_3').value = '';
            $get('NOI_DUNG_3').value = '';
            $get('SOTIEN_3').value = '';
            $get('SODATHU_3').value = '';
            $get('KY_THUE_3').value = '';


            $get('MA_CHUONG_4').value = '';
            // $get('MA_KHOAN_4').value = '';     
            $get('MA_MUC_4').value = '';
            $get('NOI_DUNG_4').value = '';
            $get('SOTIEN_4').value = '';
            $get('SODATHU_4').value = '';
            $get('KY_THUE_4').value = '';

            //document.getElementById('rowGridDetail1').style.display = '';
            //document.getElementById('rowGridDetail2').style.display = 'none';
            //document.getElementById('rowGridDetail3').style.display = 'none';
            //document.getElementById('rowGridDetail4').style.display = 'none';

            $get('txtTongTien').value = '0';
            $get('txtCharge').value = '0';
            $get('txtVat').value = '0';
            $get('txtCharge2').value = '0';
            $get('txtVat2').value = '0';
            $get('txtTongTrichNo').value = '0';
            // document.getElementById('chkChargeType').checked=true;

            document.getElementById('chkRemove1').checked = false;
            document.getElementById('chkRemove2').checked = false;
            document.getElementById('chkRemove3').checked = false;
            document.getElementById('chkRemove4').checked = false;
        }

        function jsGetTenQuanHuyen(ma_xa) {

            if (ma_xa != "") {
                PageMethods.GetTenQuanHuyen(ma_xa, GetTenQuanHuyen_Complete, GetTenQuanHuyen_Error);
                jsGetTenTinh(ma_xa);
            } else {
                document.getElementById('txtQuan_HuyenNNTien').value = "";
                document.getElementById('txtTenQuan_HuyenNNTien').value = "";
                document.getElementById('txtTinh_NNTien').value = "";
                document.getElementById('txtTenTinh_NNTien').value = "";
            }

        }
        function GetTenQuanHuyen_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Quan = result;
                document.getElementById('txtTenQuan_HuyenNNTien').value = ten_Quan;
            } else {
                alert('Mã Quận không chính xác. Vui lòng nhập lại.');
                document.getElementById('txtQuan_HuyenNNTien').focus();
            }
        }
        function GetTenQuanHuyen_Error(error, userContext, methodName) {
            alert('Mã Quận không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtQuan_HuyenNNTien').focus();
            document.getElementById('txtQuan_HuyenNNTien').value = "";
            document.getElementById('txtTenQuan_HuyenNNTien').value = "";
            document.getElementById('txtTinh_NNTien').value = "";
            document.getElementById('txtTenTinh_NNTien').value = "";
        }
        function jsGetTenTinh(ma_xa) {
            PageMethods.GetTenTinh(ma_xa, GetTenTinh_Complete, GetTenTinh_Error);
        }
        function GetTenTinh_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtTinh_NNTien').value = ten_Tinh[0];
                document.getElementById('txtTenTinh_NNTien').value = ten_Tinh[1];
            }
        }
        function GetTenTinh_Error(error, userContext, methodName) {
            //document.getElementById('hdnSoLan_LapCT').value='0' ;
        }
        function jsGetTenQuanHuyenNNT(ma_xa) {
            if (ma_xa.toString() != "") {
                PageMethods.GetTenQuanHuyen(ma_xa, GetTenQuanHuyenNNT_Complete, GetTenQuanHuyenNNT_Error);
                jsGetTenTinhNNT(ma_xa);
            } else {
                document.getElementById('txtHuyen_NNT').value = "";
                document.getElementById('txtTenHuyen_NNT').value = "";
                document.getElementById('txtTinh_NNT').value = "";
                document.getElementById('txtTenTinh_NNT').value = "";
            }
        }
        function GetTenQuanHuyenNNT_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Quan = result;
                document.getElementById('txtTenHuyen_NNT').value = ten_Quan;
            } else {
                alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
                document.getElementById('txtHuyen_NNT').focus();
            }
        }
        function GetTenQuanHuyenNNT_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtHuyen_NNT').focus();
            document.getElementById('txtHuyen_NNT').value = "";
            document.getElementById('txtTenHuyen_NNT').value = "";
            document.getElementById('txtTinh_NNT').value = "";
            document.getElementById('txtTenTinh_NNT').value = "";
        }
        function jsGetTenTinhNNT(ma_xa) {
            PageMethods.GetTenTinh(ma_xa, GetTenTinhNNT_Complete, GetTenTinhNNT_Error);
        }
        function GetTenTinhNNT_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtTinh_NNT').value = ten_Tinh[0];
                document.getElementById('txtTenTinh_NNT').value = ten_Tinh[1];
            }
        }
        function GetTenTinhNNT_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtHuyen_NNT').focus();
        }
        function InPhieuHachToan() {
            var SO_CT = curSoCT;
            if (SO_CT != "") {
                window.open("../../pages/Baocao/frmPhieuHachToan.aspx?SO_CT=" + SO_CT);
            }
        }
        function jsGet_TenDBHC() {
            //console.log("XXXXXX");
            if (arrDBHC.length > 0) {
                $get('txtTenDBHC').value = '';
                for (var i = 0; i < arrDBHC.length; i++) {
                    var arr = arrDBHC[i].split(';');
                    if (arr[1] == $get('txtMaDBHC').value.trim()) {
                        $get('txtTenDBHC').value = arr[2];
                        break;
                    }
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width='900' align="left" valign="top">
                <table width="100%" align="left">

                    <tr>
                        <td valign="top" width="190px" height="480px">
                            <asp:Panel ID="Panel2" runat="server" Height="450px" Width="180px" ScrollBars="Vertical">
                                <table class='grid_data' cellspacing='0' rules='all' border='1' style='width: 100%; border-collapse: collapse;'>
                                    <tr>
                                        <td colspan="3">
                                            <b>Số CT</b>
                                            <input type="text" id='txtSoCTTimKiem' class="form_input" style="width: 70%" maxlength='12' onkeyup="valInteger(this)" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <input type="button" class="ButtonCommand" id="cmdTimKiemCT" value="Tìm kiếm" onclick='jsLoadCTList();' />
                                        </td>
                                    </tr>
                                    <tr class='grid_header'>
                                        <td style="width: 20%">TT
                                        </td>
                                        <td style="width: 30%">Số CT
                                        </td>
                                        <td style="width: 50%">User lập/Số BT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <div id='divDSCT' />
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>
                            <table>
                                <tr>
                                    <td style="width: 10%">
                                        <img src="../../images/icons/ChuaKS.png" />
                                    </td>
                                    <td style="width: 90%" colspan="2">Chưa Kiểm soát
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 10%">
                                        <img src="../../images/icons/ChuyenKS.png" />
                                    </td>
                                    <td style="width: 90%" colspan="2">Chờ Kiểm soát
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <img src="../../images/icons/DaKS.png" />
                                    </td>
                                    <td style="width: 90%" colspan="2">Đã Kiểm soát
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <img src="../../images/icons/KSLoi.png" />
                                    </td>
                                    <td style="width: 90%" colspan="2">Chuyển trả
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <img src="../../images/icons/Huy.png" />
                                    </td>
                                    <td style="width: 90%" colspan="2">CT bị Hủy bởi GDV
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <img src="../../images/icons/HuyHS.gif" />
                                    </td>
                                    <td style="width: 90%" colspan="2">CT Hủy bởi GDV - chờ duyệt
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <img src="../../images/icons/Huy_CT_Loi.png" />
                                    </td>
                                    <td style="width: 90%" colspan="2">Chuyển thông điệp hủy nộp thuế lỗi
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <img src="../../images/icons/Huy_KS.png" />
                                    </td>
                                    <td style="width: 90%" colspan="2">CT bị Hủy bởi KSV
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <img src="../../images/icons/TT_CThueLoi.gif" />
                                    </td>
                                    <td style="width: 90%" colspan="2">Chuyển thông điệp nộp thuế lỗi
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td style="width: 10%">
                                        <img src="../../images/icons/Huy_CT_Loi.png" />
                                    </td>
                                    <td style="width: 90%" colspan="2">Hủy chuyển thuế/HQ lỗi
                                    </td>
                                </tr>
                                <tr class="img">
                                    <td align="left" style="width: 100%" colspan="3">
                                        <hr style="width: 50%" />
                                    </td>
                                </tr>
                            </table>
                            Alt + M : Thêm mới<br />

                            Alt + K : Ghi và chuyển KS<br />

                            Alt + I : In chứng từ<br />
                            Alt + H : Hủy chứng từ<br />
                        </td>
                        <td valign="top">
                            <table width="100%">
                                <tr class="grid_header">
                                    <td align="left">LẬP CHỨNG TỪ THUẾ NỘI ĐỊA
                                                                            <div style="display: none" id="divTTBDS">
                                                                            </div>
                                    </td>
                                    <td height='15' align="right">Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập
                                    </td>
                                </tr>
                            </table>
                            <div id="divTTTruyVan">
                                <table width="100%" cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                    <tr class="grid_header">
                                        <td align="left" colspan="2">TRUY VẤN SỔ THUẾ
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Loai Thuế</td>
                                        <td>
                                            <select class="inputflat" id="CboSearchLThue" onchange="jsChangLthue();" style="width: 206px;">
                                                <!-- <option value="01" selected="selected">Thuế Nội địa</option>
                                            <option value="02">Thuế cá nhân</option>
                                            <option value="03">Thuế trước bạ nhà đất</option>
                                            <option value="08">Thuế trước bạ Oto xe máy</option>
                                            <option value="05">Thu khác</option>-->
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="rowS_Nhadat" style="display: none">
                                        <td class="auto-style1">Số hồ sơ </td>
                                        <td class="auto-style1">
                                            <input type="text" id="txtSearch_SOHS" class="inputflat" style="width: 200px;" />
                                        </td>
                                    </tr>
                                    <tr id="rowS_Oto" style="display: none">
                                        <td>Số hồ sơ/Số quyết định/Số thông báo</td>
                                        <td>
                                            <input type="text" id="txtSearch_SOTK" class="inputflat" style="width: 200px;" />
                                        </td>
                                    </tr>
                                    <tr id="rowTruyVanID" style="display: none">
                                        <td>ID khoản nộp</td>
                                        <td>
                                            <input type="text" id="txtSearch_ID_KHOANNOP" class="inputflat" style="width: 200px;" />
                                        </td>
                                    </tr>
                                    <tr id="rowTruyVanMST">
                                        <td>Mã số Thuế </td>
                                        <td>
                                            <input type="text" id="txtSearch_MST" maxlength="14" class="inputflat" style="width: 200px;" onchange="javascript:jsValidLength(this.id,this.value);" />
                                        </td>
                                    </tr>
                                    <tr id="rowS_Nhadat_SOQD" style="display: none">
                                        <td>Số quyết định</td>
                                        <td>
                                            <input type="text" id="txtSearch_SOQD" class="inputflat" style="width: 200px;" />
                                        </td>
                                    </tr>
                                    <tr id="rowS_Nhadat_CCCD" style="display: none">
                                        <td>Số CMNN/CCCD </td>
                                        <td>
                                            <input type="text" id="txtSearch_CCCD" class="inputflat" style="width: 200px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="middle" style="text-align: center">
                                            <input type="button" class="ButtonCommand" id="Button1" value="Truy vấn" onclick="jsTruyVanMST();" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--KET QUA TRUY VAN HQ-->
                            <div id="divKQTruyVan" style="width: 100%; margin: 0; padding: 0; display: none;">
                                <br />
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="grid_header">
                                        <td align="left">SỔ THUẾ
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="divResultTruyVan" style="width: 100%; max-height: 200px; overflow: auto;">
                                                <table class='grid_data' cellspacing='0' rules='all' border='1' id='grdKQTruyVan' style='width: 100%; border-collapse: collapse;'>
                                                    <tr class='grid_header'>
                                                        <td align='center' style='width: 5%;'>Chọn</td>
                                                        <td align='center' style='width: 5%;'>STT</td>
                                                        <td align='center' style='width: 5%;'>Thứ tự</td>
                                                        <td align='center' style='width: 5%;'>ID</td>
                                                        <td align='center' style='width: 15%;'>CQT</td>
                                                        <td align='center' style='width: 15%;'>kho bạc</td>
                                                        <td align='center'>Nội dung</td>
                                                        <td align='center' style='width: 15%;'>TK kho bạc</td>
                                                        <td align='center' style='width: 15%;'>Số tiền</td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--THONG BAO-->
                            <div class="errorMessage" style="width: 100%; margin: 0; padding: 0;">
                                <div id="divStatus" style="width: 100%; margin: 0; padding: 0; background-color: Aqua; font-weight: bold; font-size: 18pt; display: none;">
                                    <br />
                                </div>
                                <div id="divProgress" style="width: 100%; margin: 0; padding: 0; background-color: Aqua; font-weight: bold; font-size: 18pt; display: none;">
                                    <b style="">Đang xử lý. Xin chờ một lát...</b>
                                </div>
                            </div>
                            <table id="grdHeader" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                <tr class="grid_header">
                                    <td align="left" colspan="3">THÔNG TIN CHỨNG TỪ
                                    </td>
                                </tr>
                                <tr id="rowLoaiThue">
                                    <td>
                                        <b>Loại thuế</b>
                                    </td>
                                    <td>
                                        <select id="ddlMaLoaiThue" style="width: 100%" class="inputflat" onchange="ShowHideControlByLoaiThue();" disabled="disabled">
                                        </select>
                                    </td>
                                    <td>
                                        <input type="hidden" id="txtTenLoaiThue" class="inputflat" style="width: 98%;" />
                                    </td>
                                </tr>
                                <tr id="rowToKhaiSo" style="display: none">
                                    <td>
                                        <b>Tờ khai số </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtToKhaiSo" class="inputflat" style="width: 99%;" />
                                    </td>
                                    <td>
                                        <select id="ddlToKhaiSo" style="width: 99%" class="inputflat" onchange="jsOnChange_SoTK();">
                                        </select>
                                        <%--  <asp:DropDownList ID="ddlToKhaiSo" runat="server" Width="100%" CssClass="inputflat">
                                                </asp:DropDownList>--%>
                                    </td>
                                </tr>
                                <tr id="Tr1" style="display: none">
                                    <td width="24%">
                                        <b>Giờ GDV lập </b>
                                    </td>
                                    <td width="25%">
                                        <input type="text" id="txtTime_Begin" class="inputflat" style="width: 98%;" disabled="disabled" />
                                    </td>
                                    <td width="24%">
                                        <b>Giờ GDV ghi </b>
                                        <input type="text" id="txtNgay_HT" class="inputflat" style="width: 60%;" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="Tr3" style="display: none">
                                    <td width="24%">
                                        <b>Giờ KSV duyệt </b>
                                    </td>
                                    <td width="25%">
                                        <input type="text" id="txtNgay_KS" class="inputflat" style="width: 98%;" disabled="disabled" />
                                    </td>
                                    <td width="24%"></td>
                                </tr>


                                <tr id="rowMaNNT">
                                    <td width="24%">
                                        <b>Mã số thuế </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <input type="text" id="txtMaNNT" maxlength="14" class="inputflat" onblur="checkMaNNT(this);" />
                                    </td>
                                    <td width="51%">
                                        <input type="text" id="txtTenNNT" class="inputflat" style="width: 92%;" maxlength="200" />
                                        <img id="imgMaDB" alt="Chọn từ danh mục đặc biệt" src="../../images/icons/MaDB.png"
                                            onclick="jsGetMaDB();" style="width: 16px; height: 16px;" />
                                    </td>
                                </tr>
                                <tr id="rowLoaiNNT">
                                    <td>
                                        <b>Mã/Tên Loại NNT</b>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMaLoaiNNT" maxlength="4" class="inputflat" style="background: Aqua" onkeypress="if (event.keyCode==13){ShowLov('LOAI_NNTHUE');}"
                                            onchange="javascript:jsValidLength(this.id,this.value);jsGetLoaiNNT();" onblur="jsGetLoaiNNT()" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenLoaiNNT" class="inputflat" style="width: 98%;" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr id="rowDChiNNT">
                                    <td>
                                        <b>Địa chỉ người nộp thuế</b> <span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtDChiNNT" class="inputflat" maxlength="200" style="width: 98%;" />
                                    </td>
                                </tr>
                                <tr id="rowHuyen">
                                    <td>
                                        <b>Quận(Huyện)</b>
                                    </td>
                                    <td>
                                        <input type="text" id="txtQuan_HuyenNNTien" maxlength="5" class="inputflat" style="background: Aqua" onkeypress="if (event.keyCode==13){ShowLov('DBHC_NNTHUE');}" onchange="javascript:jsValidLength(this.id,this.value);jsGetTenQuanHuyen(this.value);" onblur="javascript:jsGetTenQuanHuyen(this.value);" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenQuan_HuyenNNTien" class="inputflat" style="width: 98%;" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr id="rowTinh">
                                    <td class="style1">
                                        <b>Tỉnh(Thành phố)</b>
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtTinh_NNTien" class="inputflat" readonly="readonly" />
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtTenTinh_NNTien" class="inputflat" style="width: 98%;" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr id="rowCQThu">
                                    <td class="style1">
                                        <b>CQ Thu </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtMaCQThu" maxlength="7" style="background: Aqua;" onkeypress="if (event.keyCode==13){ShowLov('CQT');}"
                                            onchange="javascript:jsValidLength(this.id,this.value);jsLoadByCQT();" onblur="javascript:jsLoadByCQT();" class="inputflat" />
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtTenCQThu" class="inputflat" style="width: 98%;" disabled />
                                    </td>
                                </tr>
                                <tr id="rowSHKB">
                                    <td width="24%">
                                        <b>Số hiệu KB </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <input type="text" id="txtSHKB" class="inputflat" style="background: Aqua"
                                            onkeypress="if (event.keyCode==13){ShowLov('SHKB');}" maxlength="4" onblur="jsGet_KBInfo();jsGet_DBHC_KBInfo();" onchange="jsValidLength(this.id,this.value);jsGet_KBInfo();jsGet_DBHC_KBInfo();" />

                                    </td>
                                    <td width="51%">
                                        <input type="text" id="txtTenKB" class="inputflat" style="width: 98%;" disabled="disabled" />
                                    </td>
                                </tr>
                                <%-- <tr id="rowDBHC" style="display: none">
                                    <td>
                                        <b>Mã tỉnh/thành Kho bạc </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMaDBHC" class="inputflat"
                                            disabled="disabled" onkeypress="if (event.keyCode==13){ShowLov('DBHC');}" onblur="{jsGet_TenDBHC();jsGETTinh_TP_NNThue(this.value);}"
                                            onchange="jsGet_TenDBHC();" />

                                    </td>
                                    <td>
                                        <input type="text" id="txtTenDBHC" class="inputflat" style="width: 98%;" disabled="disabled" />
                                    </td>
                                </tr>--%>
                                <tr id="rowKHCT">
                                    <td width="24%">
                                        <b>Mã hiệu/ Số CT </b>
                                    </td>
                                    <td width="25%">
                                        <input type="text" id="txtKyHieuCT" class="inputflat" disabled="disabled" />
                                    </td>
                                    <td width="51%">
                                        <input type="text" id="txtSoCT" class="inputflat" style="width: 98%;" disabled="disabled" maxlength="60" />
                                    </td>
                                </tr>
                                <tr id="rowDBHC">
                                    <td>
                                        <b>ĐBHC </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMaDBHC" maxlength="5" class="inputflat" style="width: 99%; background: Aqua"
                                            onchange="javascript:jsValidLength(this.id,this.value);jsGet_TenDBHC();" onkeypress="if (event.keyCode==13){ShowLov('DBHC');}" />
                                        <%-- <script type="text/javascript">
                                                                                                new actb(document.getElementById('txtMaDBHC'), temp_arrDBHC, 'txtTenDBHC');					
                                                                                            </script>--%>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenDBHC" class="inputflat" style="width: 99%;" disabled />
                                    </td>
                                </tr>
                                <tr id="rowTinh1" style="display: none">
                                    <td class="style1">
                                        <b>Tỉnh/Thành phố</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtTinh_NNTien1" class="inputflat" disabled="disabled" />
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtQuan_HuyenNNTien1" class="inputflat" style="width: 98%;" disabled="disabled" />
                                    </td>
                                </tr>



                                <tr id="rowTKNo" style="display: none">
                                    <td style="background-color: Aqua">
                                        <b>TK Nợ NSNN </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="ddlMaTKNo" class="inputflat" style="width: 98%;" onblur="jsGetTen_TKNo();"
                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'3,6,9,16','-');}"
                                            maxlength="18" />
                                    </td>
                                    <td class="style1" style="display: none">
                                        <input type="text" id="txtTKNo" class="inputflat" style="width: 98%; display: none"
                                            visible="false" />
                                    </td>
                                </tr>
                                <tr id="rowTKCo">
                                    <td>
                                        <b>TK Có NSNN </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <select id="ddlMa_NTK" style="width: 100%" class="inputflat" onchange="jsChangeMA_NTK();">
                                            <option value="1" selected="selected">Tài khoản thu NSNN</option>
                                            <option value="2">Tài khoản tạm thu</option>
                                            <option value="3">Thu hoàn thuế GTGT</option>

                                        </select>
                                    </td>
                                    <td style="">
                                        <input type="text" id="ddlMaTKCo" class="inputflat" style="width: 98%;" maxlength="4" onchange="jsValidLength(this.id,this.value);" />
                                    </td>
                                    <td visible="false" style="display: none">
                                        <input type="text" id="txtTKCo" class="inputflat" style="width: 10%; display: none; height: 0px"
                                            visible="false" />
                                    </td>
                                </tr>
                                <tr id="rowHTTT">
                                    <td>
                                        <b>Hình thức thanh toán</b>
                                    </td>
                                    <td>
                                        <select id="ddlMaHTTT" style="width: 100%" class="inputflat" onchange="ShowHideControlByPT_TT();getSoTK_TruyVanCore();">
                                        </select>
                                    </td>
                                    <td>
                                        <input type="hidden" id="txtTenHTTT" />
                                    </td>
                                </tr>

                                <tr id="rowMaNT">
                                    <td>
                                        <b>Nguyên tệ</b>
                                    </td>
                                    <td>
                                        <select id="cboNguyenTe" style="width: 100%" class="inputflat" onchange="showNT();">
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenNT" class="inputflat" style="width: 99%;" disabled="true"
                                            readonly="true" value="VND" />
                                    </td>
                                </tr>
                                <tr id="rowTK_KH_NH">
                                    <td>
                                        <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTK_KH_NH" class="inputflat" onblur="jsGetTK_KH_NH();jsGetTenTK_KH_NH();"
                                            maxlength="18" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenTK_KH_NH" class="inputflat" style="width: 98%;" disabled="disabled" />

                                    </td>
                                </tr>
                                <tr id="rowSoDuNH">
                                    <td>
                                        <b>Số dư khả dụng</b>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSoDu_KH_NH" class="inputflat" readonly="readonly" disabled="disabled" />
                                    </td>
                                    <td style="display: none">
                                        <input type="text" id="txtDesc_SoDu_KH_NH" class="inputflat" style="width: 98%; visibility: visible"
                                            disabled="disabled" readonly="readonly" />
                                    </td>

                                </tr>

                                <%-- <tr id="rowTKGL">
                                    <td>
                                        <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTKGL" maxlength="12" class="inputflat"
                                            onblur="jsGet_TenTKGL();" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenTKGL" class="inputflat" style="width: 98%;" readonly="readonly" />
                                    </td>
                                </tr>--%>
                                <tr id="rowNNTien">
                                    <td>
                                        <b>MST/Tên người nộp tiền</b><span class="requiredField" id="MST_NNTIEN"></span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMaNNTien" class="inputflat" onkeyup="valInteger(this)" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenNNTien" class="inputflat" style="width: 98%;" />
                                    </td>
                                </tr>
                                <tr id="rowDChiNNTien">
                                    <td>
                                        <b>Địa chỉ người nộp tiền</b>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtDChiNNTien" class="inputflat" style="width: 98%;" />
                                    </td>
                                </tr>
                                <tr id="rowHuyenNNTien">
                                    <td>
                                        <b>Quận(Huyện)</b>
                                    </td>
                                    <td>
                                        <input type="text" id="txtHuyen_NNT" maxlength="5" class="inputflat" style="background: Aqua;" onkeypress="if (event.keyCode==13){ShowLov('DBHC_NNT');}" onchange="javascript:jsValidLength(this.id,this.value);jsGetTenQuanHuyenNNT(this.value);" onblur="javascript:jsGetTenQuanHuyenNNT(this.value);" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenHuyen_NNT" class="inputflat" style="width: 98%;" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr id="rowTinhNNTien">
                                    <td class="style1">
                                        <b>Tỉnh(Thành phố)</b>
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtTinh_NNT" maxlength="5" class="inputflat" disabled="disabled" readonly="readonly" />
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtTenTinh_NNT" class="inputflat" disabled="disabled" style="width: 98%;" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr id="rowCMT">
                                    <td>
                                        <b>Số chứng minh</b><%--<span class="requiredField">(*)</span>--%>
                                    </td>
                                    <td>
                                        <input type="text" id="txtCK_SoCMND" class="inputflat" maxlength="12" onchange="javascript:jsValidLength(this.id,this.value);" />
                                    </td>
                                    <td></td>
                                </tr>

                                <tr id="rowSDT">
                                    <td>
                                        <b>Số điện thoại</b><%--<span class="requiredField">(*)</span>--%>
                                    </td>
                                    <td>
                                        <input type="text" id="txtCK_SoDT" class="inputflat" maxlength="12" onchange="javascript:jsValidLength(this.id,this.value);" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr id="rowNgay_KH_NH">
                                    <td>
                                        <b>Ngày lập CT</b>
                                    </td>
                                    <td>
                                        <input type="text" id="txtNGAY_KH_NH" class="inputflat" style="width: 98%;" maxlength="10"
                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="javascript:CheckDate(this)" onfocus="this.select()" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtDesNGAY_KH_NH" class="inputflat" style="width: 98%;" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowNHChuyen">
                                    <td>
                                        <b>Mã NH Chuyển </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <select id="ddlMA_NH_A" style="width: 98%" class="inputflat">
                                            <%--<option value="30319001" selected="true">30319001-NGAN HANG TMCP ĐẠI DƯƠNG</option>--%>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="rowNHTT">
                                    <td>
                                        <b>Mã NH trực tiếp </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMA_NH_TT" class="inputflat" style="background: Aqua;" maxlength="8" onkeyup="valInteger(this)" onkeypress="if (event.keyCode==13){ShowLov('DMNH_TT');jsGet_TenNH_TT();}" onchange="jsGet_TenNH_TT();" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTEN_NH_TT" class="inputflat" style="width: 98%;" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr id="rowNHNhan">
                                    <td>
                                        <b>Mã NH gián tiếp </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMA_NH_B" class="inputflat" style="background: Aqua" maxlength="8" onblur="jsGet_TenNH_B()" onkeypress="if (event.keyCode==13){ShowLov('DMNH_GT');jsGet_TenNH_B();}" onkeyup="valInteger(this)" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenMA_NH_B" class="inputflat" style="width: 98%;" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr id="rowDienGiaiNHB" style="display: none">
                                    <td>
                                        <b>Ngân hàng thụ hưởng </b>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtDG_NHB" class="inputflat" maxlength="200" style="width: 98%;" onblur="jsCalCharacter();" />
                                    </td>
                                </tr>

                                <tr id="rowSoKhung">
                                    <td>
                                        <b>Số khung</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSoKhung" class="inputflat" style="width: 98%;" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr id="rowSoMay">
                                    <td>
                                        <b>Số máy</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSoMay" class="inputflat" style="width: 98%;" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr id="rowSoBK" style="display: none">
                                    <td>
                                        <b>Số bảng kê</b>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtSo_BK" class="inputflat" style="width: 98%;" />
                                    </td>
                                </tr>
                                <tr id="rowDacDiemPhuongTien">
                                    <td>
                                        <b>Đặc điểm phương tiện</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtDacDiemPhuongTien" class="inputflat" maxlength="200" />
                                    </td>

                                </tr>
                                <tr id="rowSO_CQQD">
                                    <td>
                                        <b>Số quyết định</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtSOQD" class="inputflat" maxlength="30" />
                                    </td>

                                </tr>
                                <tr id="rowCQQD">
                                    <td>
                                        <b>Cơ quan quyết định</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtCQQD" class="inputflat" maxlength="50" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenCQQD" class="inputflat" style="width: 98%;" />
                                    </td>
                                </tr>

                                <tr id="rowNGAY_CQQD">
                                    <td>
                                        <b>Ngày quyết định</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id='txtNgayQD' maxlength='10' class="inputflat" style="width: 40%"
                                            onkeyup="javascript:return mask(this.value,this,'2,5','/');" onblur="CheckDate(this);"
                                            onfocus="this.select()" />

                                    </td>
                                </tr>
                                <tr id="rowMaHS">
                                    <td>
                                        <b>Mã hồ sơ</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtMaHS" class="inputflat" maxlength="50" />
                                    </td>
                                </tr>
                                <tr id="rowSoTD_ToBanDo">
                                    <td>
                                        <b>Số thửa đất/số tờ bản đồ</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSoThuaDat" class="inputflat" maxlength="10" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtSoToBanDo" class="inputflat" style="width: 98%;" maxlength="200" />
                                    </td>
                                </tr>
                                <tr id="rowDiaChiTS">
                                    <td>
                                        <b>Địa chỉ tài sản</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtDiaChiTS" class="inputflat" maxlength="500" />
                                    </td>
                                </tr>
                                <tr id="rowDBHCTS">
                                    <td>
                                        <b>Mã ĐBHC tài sản</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtDBHCTS" class="inputflat" maxlength="5" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenDBHCTS" class="inputflat" maxlength="200" />
                                    </td>
                                </tr>
                                <tr id="rowGCN">
                                    <td>
                                        <b>Mã giấy chuyển nhượng </b>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMA_GCN" class="inputflat" maxlength="200" />
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" rules="all" border="1" id="grdChiTiet" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                <tr class="grid_header">
                                    <td style="width: 30px;"></td>
                                    <td align="center" style="display: none; width: 0%" visible="false"></td>
                                    <td align="center" style="width: 70px;">ID Khoản nộp
                                    </td>
                                    <td align="center" style="width: 70px;">Mã chương
                                    </td>
                                    <td align="center" style="display: none" visible="false"></td>
                                    <td align="center" style="width: 50px;">NDKT
                                    </td>
                                    <td align="center" style="width: 250px;">Nội dung
                                    </td>
                                    <td align="center" style="width: 90px;">Số tiền
                                    </td>
                                    <td align="center" style="width: 90px; display: none">Số đã thu
                                    </td>
                                    <td align="center" style="width: 60px;">Mã ĐBHC
                                    </td>
                                    <td align="center" style="width: 80px;">Kỳ thuế
                                    </td>
                                </tr>
                                <tr id="rowGridDetail1" class="grid_item">
                                    <td>
                                        <input id="chkRemove1" type="checkbox" onclick="jsCalCharacter(); jsCalTotal();" />
                                    </td>
                                    <td style="display: none">
                                        <input type="text" id="MA_CAP_1" visible="false" style="width: 0%; display: none;"
                                            class="inputflat" readonly="readonly" disabled="disabled" onkeypress="if (event.keyCode==13){ShowMLNS('MQ_01');NextFocus();}"
                                            value="01" />
                                    </td>
                                    <td>
                                        <input type="text" id="ID_KHOANNOP_1" style="width: 95%; border-color: White; font-weight: bold" maxlength="50" class="inputflat" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_CHUONG_1" style="width: 90%; border-color: White; background: Aqua; text-align: center; font-weight: bold"
                                            maxlength="3" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MC_01');NextFocus();}" />
                                    </td>
                                    <td style="display: none">
                                        <input type="text" id="MA_KHOAN_1" style="width: 0; display: none; border-color: White; background-color: Yellow; font-weight: bold"
                                            class="inputflat" visible="false" disabled="disabled"
                                            onkeypress="if (event.keyCode==13){ShowMLNS('MK_01');NextFocus();}" onkeyup="if (event.keyCode!=191){return valInteger(this);}" />
                                        <%-- onkeyup="if (event.keyCode!=191){return valInteger(this);}"--%>
                                    </td>
                                    <td>
                                        <input type="text" id="MA_MUC_1" style="width: 90%; border-color: White; background: Aqua; text-align: center; font-weight: bold"
                                            maxlength="4" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MM_01');NextFocus();}"
                                            onkeyup="return valInteger(this)" onblur="jsNoiDungKT('MA_MUC_1','NOI_DUNG_1');jsCallSo_DaThu('MA_CHUONG_1','MA_MUC_1','KY_THUE_1','SODATHU_1');" />
                                    </td>
                                    <td>
                                        <input type="text" id="NOI_DUNG_1" style="width: 95%; border-color: White; font-weight: bold"
                                            maxlength="210" class="inputflat" onblur="jsCalCharacter();" />
                                    </td>
                                    <td>
                                        <input type="text" id="SOTIEN_1" style="width: 90%; border-color: White; font-weight: bold; text-align: right; margin-bottom: 0px;"
                                            class="inputflat" onfocus="this.select()" onblur="fncFormatNumberChiTiet(this);jsCalCharacter();jsCalTotal();"
                                            onkeyup="valInteger(this)" maxlength="17" />
                                    </td>
                                    <td style="width: 90px; display: none">
                                        <input type="text" id="SODATHU_1" style="width: 90%; border-color: White; font-weight: bold; text-align: right"
                                            readonly="readonly" class="inputflat" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_DBHC_1" style="width: 95%; border-color: White; font-weight: bold" maxlength="5" class="inputflat" />
                                    </td>
                                    <td>
                                        <input type="text" id="KY_THUE_1" style="width: 90%; border-color: White; font-weight: bold; text-align: center"
                                            class="inputflat" maxlength="10" onblur="javascript:jsCheckFormatKyThue(this.value);"
                                            onkeyup=""
                                            onkeypress="if (event.keyCode==13){jsAddNewDetailRow('1');}" />
                                    </td>
                                </tr>
                                <tr id="rowGridDetail2" class="grid_item_alter">
                                    <td>
                                        <input id="chkRemove2" type="checkbox" onclick="jsCalCharacter(); jsCalTotal();" />
                                    </td>
                                    <td style="display: none">
                                        <input type="text" id="MA_CAP_2" visible="false" style="width: 0%; display: none;"
                                            class="inputflat" readonly="readonly" disabled="disabled" onkeypress="if (event.keyCode==13){ShowMLNS('MQ_01');NextFocus();}"
                                            value="01" />
                                    </td>
                                    <td>
                                        <input type="text" id="ID_KHOANNOP_2" style="width: 95%; border-color: White; font-weight: bold" maxlength="50" class="inputflat" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_CHUONG_2" style="width: 90%; border-color: White; background: Aqua; text-align: center; font-weight: bold"
                                            maxlength="3" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MC_02');NextFocus();}"
                                            onkeyup="return valInteger(this)" />
                                    </td>
                                    <td style="display: none">
                                        <input type="text" id="MA_KHOAN_2" style="width: 0%; display: none; border-color: White; background-color: Yellow; font-weight: bold"
                                            disabled="disabled" class="inputflat"
                                            visible="false" onkeypress="if (event.keyCode==13){ShowMLNS('MK_02');NextFocus();}"
                                            onkeyup="return valInteger(this)" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_MUC_2" style="width: 90%; border-color: White; background: Aqua; text-align: center; font-weight: bold"
                                            maxlength="4" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MM_02');NextFocus();}"
                                            onkeyup="return valInteger(this)" onblur="jsNoiDungKT('MA_MUC_2','NOI_DUNG_2');jsCallSo_DaThu('MA_CHUONG_2','MA_MUC_2','KY_THUE_2','SODATHU_2');" />
                                    </td>
                                    <td>
                                        <input type="text" id="NOI_DUNG_2" style="width: 95%; border-color: White; font-weight: bold"
                                            maxlength="200" class="inputflat" onblur="jsCalCharacter();" />
                                    </td>
                                    <td>
                                        <input type="text" id="SOTIEN_2" style="width: 90%; border-color: White; font-weight: bold; text-align: right"
                                            class="inputflat" onfocus="this.select()" onblur="fncFormatNumberChiTiet(this);jsCalCharacter();jsCalTotal();"
                                            onkeyup="valInteger(this)" maxlength="17" />
                                    </td>
                                    <td style="width: 90px; display: none">
                                        <input type="text" id="SODATHU_2" style="width: 90%; border-color: White; font-weight: bold; text-align: right"
                                            readonly="readonly" class="inputflat" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_DBHC_2" style="width: 95%; border-color: White; font-weight: bold" maxlength="5" class="inputflat" />
                                    </td>
                                    <td>
                                        <input type="text" id="KY_THUE_2" style="width: 90%; border-color: White; font-weight: bold; text-align: center"
                                            class="inputflat" maxlength="10" onblur="javascript:jsCheckFormatKyThue(this.value);"
                                            onkeyup=""
                                            onkeypress="if (event.keyCode==13){jsAddNewDetailRow('2');}" />
                                    </td>
                                </tr>
                                <tr id="rowGridDetail3" class="grid_item">
                                    <td>
                                        <input id="chkRemove3" type="checkbox" onclick="jsCalCharacter(); jsCalTotal();" />
                                    </td>
                                    <td style="display: none">
                                        <input type="text" id="MA_CAP_3" visible="false" style="width: 0%; display: none;"
                                            class="inputflat" readonly="readonly" disabled="disabled" onkeypress="if (event.keyCode==13){ShowMLNS('MQ_01');NextFocus();}"
                                            value="01" />
                                    </td>
                                    <td>
                                        <input type="text" id="ID_KHOANNOP_3" style="width: 95%; border-color: White; font-weight: bold" maxlength="50" class="inputflat" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_CHUONG_3" style="width: 90%; border-color: White; background: Aqua; text-align: center; font-weight: bold"
                                            maxlength="3" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MC_03');NextFocus();}"
                                            onkeyup="return valInteger(this)" />
                                    </td>
                                    <td style="display: none">
                                        <input type="text" id="MA_KHOAN_3" style="width: 0%; display: none; border-color: White; background-color: Yellow; font-weight: bold"
                                            disabled="disabled" class="inputflat"
                                            onkeypress="if (event.keyCode==13){ShowMLNS('MK_03');NextFocus();}" onkeyup="return valInteger(this)"
                                            visible="false" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_MUC_3" style="width: 90%; border-color: White; background: Aqua; text-align: center; font-weight: bold"
                                            maxlength="4" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MM_03');NextFocus();}"
                                            onkeyup="return valInteger(this)" onblur="jsNoiDungKT('MA_MUC_3','NOI_DUNG_3');jsCallSo_DaThu('MA_CHUONG_3','MA_MUC_3','KY_THUE_3','SODATHU_3');" />
                                    </td>
                                    <td>
                                        <input type="text" id="NOI_DUNG_3" style="width: 95%; border-color: White; font-weight: bold"
                                            maxlength="200" class="inputflat" onblur="jsCalCharacter();" />
                                    </td>
                                    <td>
                                        <input type="text" id="SOTIEN_3" style="width: 90%; border-color: White; font-weight: bold; text-align: right"
                                            class="inputflat" onfocus="this.select()" onblur="fncFormatNumberChiTiet(this);jsCalCharacter();jsCalTotal();"
                                            onkeyup="valInteger(this)" maxlength="17" />
                                    </td>
                                    <td style="width: 90px; display: none">
                                        <input type="text" id="SODATHU_3" style="width: 90%; border-color: White; font-weight: bold; text-align: right"
                                            readonly="readonly" disabled="disabled" class="inputflat" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_DBHC_3" style="width: 95%; border-color: White; font-weight: bold" maxlength="5" class="inputflat" />
                                    </td>
                                    <td>
                                        <input type="text" id="KY_THUE_3" style="width: 90%; border-color: White; font-weight: bold; text-align: center"
                                            class="inputflat" maxlength="10" onblur="javascript:jsCheckFormatKyThue(this.value);"
                                            onkeyup=""
                                            onkeypress="if (event.keyCode==13){jsAddNewDetailRow('3');}" />
                                    </td>
                                </tr>
                                <tr id="rowGridDetail4" class="grid_item_alter">
                                    <td>
                                        <input id="chkRemove4" type="checkbox" onclick="jsCalCharacter(); jsCalTotal();" />
                                    </td>
                                    <td style="display: none">
                                        <input type="text" id="MA_CAP_4" visible="false" style="width: 0%; display: none;"
                                            class="inputflat" readonly="readonly" disabled="disabled" onkeypress="if (event.keyCode==13){ShowMLNS('MQ_01');NextFocus();}"
                                            value="01" />
                                    </td>
                                    <td>
                                        <input type="text" id="ID_KHOANNOP_4" style="width: 95%; border-color: White; font-weight: bold" maxlength="50" class="inputflat" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_CHUONG_4" style="width: 90%; border-color: White; background: Aqua; text-align: center; font-weight: bold"
                                            maxlength="3" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MC_04');NextFocus();}"
                                            onkeyup="return valInteger(this)" />
                                    </td>
                                    <td style="display: none">
                                        <input type="text" id="MA_KHOAN_4" style="width: 0%; display: none; border-color: White; background-color: Yellow; font-weight: bold"
                                            disabled="disabled" class="inputflat"
                                            onkeypress="if (event.keyCode==13){ShowMLNS('MK_04');NextFocus();}" onkeyup="return valInteger(this)"
                                            visible="false" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_MUC_4" style="width: 90%; border-color: White; background: Aqua; text-align: center; font-weight: bold"
                                            maxlength="4" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MM_04');NextFocus();}"
                                            onkeyup="return valInteger(this)" onblur="jsNoiDungKT('MA_MUC_4','NOI_DUNG_4');jsCallSo_DaThu('MA_CHUONG_4','MA_MUC_4','KY_THUE_4','SODATHU_4');" />
                                    </td>
                                    <td>
                                        <input type="text" id="NOI_DUNG_4" style="width: 95%; border-color: White; font-weight: bold"
                                            maxlength="200" class="inputflat" onblur="jsCalCharacter();" />
                                    </td>
                                    <td>
                                        <input type="text" id="SOTIEN_4" style="width: 90%; border-color: White; font-weight: bold; text-align: right"
                                            class="inputflat" onfocus="this.select()" onblur="fncFormatNumberChiTiet(this);jsCalCharacter();jsCalTotal();"
                                            onkeyup="valInteger(this)" maxlength="17" />
                                    </td>
                                    <td style="width: 90px; display: none">
                                        <input type="text" id="SODATHU_4" style="width: 90%; border-color: White; font-weight: bold; text-align: right"
                                            readonly="readonly" class="inputflat" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input type="text" id="MA_DBHC_4" style="width: 95%; border-color: White; font-weight: bold" maxlength="5" class="inputflat" />
                                    </td>
                                    <td>
                                        <input type="text" id="KY_THUE_4" style="width: 90%; border-color: White; font-weight: bold; text-align: center"
                                            class="inputflat" maxlength="10" onblur="javascript:jsCheckFormatKyThue(this.value);"
                                            onkeyup="" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" style="width: 100%">
                                <tr>
                                    <td align="left" valign="top" class='text' style="width: 300px;">
                                        <span style="font-family: @Arial Unicode MS; font-weight: bold; color: red">Số ký tự còn lại</span>
                                        <label id="leftCharacter" style="width: 150px; font-weight: bold" visible="false"></label>
                                    </td>
                                    <td align="right">
                                        <table width="300px">
                                            <tr align="right">
                                                <td align="right" valign="top">Tổng Tiền &nbsp;
                                                                            <input type="text" id="txtTongTien" class="inputflat" style="width: 150px; background: yellow; text-align: right;"
                                                                                value="0" readonly="readonly" disabled="disabled" />
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 99%" colspan="2">
                                        <div id='divBangChu' style="font-weight: bold;" />
                                    </td>
                                </tr>
                                <tr id="rowLyDoHuy">
                                    <td align="left" style="width: 99%" colspan="2">Lý do hủy/chuyển trả :&nbsp;<input type="text" id="txtLyDoHuy" style="width: 80%; border-color: black; font-weight: bold"
                                        maxlength="100" class="inputflat" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="6">
                                                    <input type="hidden" id="hdnSoCT_NH" />
                                                    <input type="hidden" id="hdnSoLan_LapCT" />
                                                    <input type="hidden" id="hdnTongTienCu" value="0" />
                                                    <input type="hidden" id="hdnMA_KS" />
                                                    <input type="hidden" id="hddAction" value="1" />
                                                    <input type="hidden" id="hdLimitAmout" value="0" />
                                                    <input type="hidden" id="HdnSeq_No" />
                                                    <input type="hidden" id="hndNHSHB" />
                                                    <input id="hdfSo_CTu" type="hidden" />
                                                    <input id="hdfLoaiCTu" type="hidden" value="T" />
                                                    <input id="hdfHDRSelected" type="hidden" />
                                                    <input id="hdfIsEdit" type="hidden" />
                                                    <input id="hdfDescCTu" type="hidden" />
                                                    <input id="hdfNgayKB" type="hidden" />
                                                    <input id="hdfMST" type="hidden" />
                                                    <input type="hidden" id="cifNo" />
                                                    <input type="hidden" id="branchCode" />
                                                    <input id="hdfSoBT" type="hidden" />
                                                    <input type="hidden" id="hdnSO_CT" />

                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">PT tính phí</td>
                                                <td colspan="5" align="left">
                                                    <input type="radio" name="chkChargeType" id="chkChargeTypeMP" value="W" />Miễn phí
                                                                        <input type="radio" size="0" name="chkChargeType" id="chkChargeTypePT" value="B" style="visibility: hidden" visible="false" /><%--Phí trong--%>
                                                    <input type="radio" name="chkChargeType" id="chkChargeTypePN" value="O" checked="checked" />Phí ngoài
                                                </td>
                                            </tr>

                                            <tr style="display: none">
                                                <td>Loại phí
                                                </td>
                                                <td align="left" width="280px" colspan="2">
                                                    <select id="cboPhiSanPham" style="width: 98%" class="inputflat"></select>
                                                </td>
                                                <td align="right">
                                                    <input type="text" id="txtCharge" style="width: 100px; background-color: Yellow; text-align: right; font-weight: bold"
                                                        class="inputflat" value="0" onfocus="this.select()"
                                                        onkeyup="valInteger(this)" onchange="jsVATtoText();" />
                                                </td>
                                                <td align="right">VAT
                                                </td>
                                                <td align="right">
                                                    <input type="text" id="txtVat" style="width: 100px; background-color: Yellow; font-weight: bold; text-align: right"
                                                        class="inputflat" value="0" disabled="disabled" />
                                                </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td>Phí kiểm đếm
                                                </td>
                                                <td align="left" width="280px" colspan="2">
                                                    <select id="cboPhiKiemDem" style="width: 98%" class="inputflat"></select>
                                                </td>
                                                <td align="right">

                                                    <input type="text" id="txtCharge2" style="width: 100px; background-color: Yellow; text-align: right; font-weight: bold"
                                                        class="inputflat" value="" onfocus="this.select()"
                                                        onkeyup="valInteger(this)" onchange="jsVATtoText2()" />
                                                </td>
                                                <td align="right">VAT
                                                </td>
                                                <td align="right">
                                                    <input type="text" id="txtVat2" style="width: 100px; background-color: Yellow; font-weight: bold; text-align: right"
                                                        class="inputflat" value="" disabled="disabled" />
                                                </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td colspan="6" align="right">
                                                    <input type="button" id="cmdSanPham" class="ButtonCommand" value="Tính phí" onclick="jsTinhPhiMoi();" />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="right" colspan="8" style="padding-top: 5px;">Tổng trích nợ
                                                                            <input type="text" id="txtTongTrichNo" style="width: 150px; background-color: Yellow; text-align: right; font-weight: bold"
                                                                                class="inputflat" value="0" disabled="disabled" onchange="jsConvertCurrToText(this)" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='2' height='5'>
                                        <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                            <tr>
                                                <td width='100%' align="center" valign="top" class='text' colspan='2' align="right">
                                                    <input type="button" id="cmdThemMoi" class="ButtonCommand" onclick="jsThem_Moi();"
                                                        value="[Lập (M)ới]" accesskey="M" />&nbsp;
                                                    
                                                    <input type="button" id="cmdGhiKS" class="ButtonCommand" value="[Ghi & (K)S]" onclick="jsGhi_CTU();"
                                                        accesskey="K" />
                                                    &nbsp;
                                                    
                                                 
                                                    
                                                    <input type="button" id="cmdInCT" value="[(I)n CT]" class="ButtonCommand" disabled="True" onclick="jsIn_CT('1');"
                                                        accesskey="I" />
                                                    &nbsp;
                                                    <input type="hidden" id="btnInPhieuHachToan" value="[In phiếu hạch toán]" class="ButtonCommand" disabled="True" onclick="InPhieuHachToan();" />
                                                    &nbsp;
                                                    <input type="button" id="cmdHuyCT" class="ButtonCommand" value="[(H)ủy]" disabled="True" onclick="jsUpdate_CTU_Status('4');"
                                                        accesskey="H" />


                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>
    <div id="dialog-popup" style="display: none;">
    </div>
    <div id="wapaler" style="display: none;">
    </div>

    <div class="msgBox" id="msgBox_SHKB" style="display: none; opacity: 1; min-height: 100px; width: 730px;">
        <div class="msgBoxTitle">DANH MỤC</div>
        <div>
            <div>
                <div class="msgBoxContent" id="msgBox_SHKB_CONTENT">

                    <table border="0" cellpadding="2" cellspacing="1" class="form_input" style="width: 100%">
                        <tr class="grid-heading">
                            <td align="center" class="style2" style="width: 30%">
                                <b>Mã danh mục</b>
                            </td>
                            <td align="center" class="style3">
                                <b>Tên danh mục</b>
                            </td>
                        </tr>
                        <tr>

                            <td align="center">
                                <input type="text" id="msgBox_txtMaDanhMuc" onkeydown="if(event.keyCode==13) {msgBox_jsSearch(); event.keyCode=9;}" class="inputflat" />
                            </td>


                            <td align="center">
                                <input type="text" id="msgBox_txtTenDanhMuc" onkeydown="if(event.keyCode==13) {msgBox_jsSearch(); event.keyCode=9;}" class="inputflat" />&nbsp;
							<%-- <input type="button" id="msgBox_btnSearch" value=" ... " class="inputflat" onclick="msgBox_jsSearch();" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="style1">
                                <div id='msgBox_divDanhMuc' style="max-height: 300px; overflow: auto">
                                </div>
                                <div id='msgBox_divStatus'>
                                </div>

                            </td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="msgBoxButtons" id="msgBox_SHKB_ConfirmDivButton">
                <input type="hidden" id="msgBox_result" value="" />
                <input id="msgBox-btn-close" class="btn btn-primary" onclick="jsMsgBoxClose();" type="button" name="Ok" value="Đóng" />
            </div>

        </div>
    </div>
    <link href="../../css/dialog.css" rel="stylesheet" />
    <input id="hdfHQResult" type="hidden" />
    <script type="text/javascript">
        $(document).ready(function () {
            jsChangLthue();
            jsFillPTTT();
            jsFillNguyenTe();
            jsFillLTHUE();
            jsFillMA_NH_A();
            jsLoadCTList();
        });
        //chỗ này cho show danh mục trên chrome
        function jsDiaglog() {
            $('#msgBox_result').val('');
            document.getElementById('msgBox_txtMaDanhMuc').value = "";
            document.getElementById('msgBox_txtTenDanhMuc').value = "";
            // console.log('Start');
            $('#wapaler').css("display", "block");
            $('#msgBox_SHKB').css("display", "block");

            jsDivAutoCenter('#msgBox_SHKB');
            msgBox_jsSearch();
        }


        function jsMsgBoxClose() {

            $('#wapaler').css("display", "none");
            $('#msgBox_SHKB').css("display", "none");


        }
        var msgBox_type = "";
        var msgBox_SHKB = "";
        var msgBox_txtID = "";
        var msgBox_txtTitle = "";
        var msgBox_txtFocus = "";
        function ShowDMKB(strPage, txtID, txtTitle, txtFocus) {
            document.getElementById('msgBox_result').value = "";
            msgBox_type = strPage;

            msgBox_txtID = txtID;
            msgBox_txtTitle = txtTitle;
            msgBox_txtFocus = txtFocus;
            jsDiaglog();


        }
        function jsDivAutoCenter(divName) {
            //      console.log('em chay phat' + divName);
            $(divName).css({
                position: 'fixed',
                left: ($(window).width() - $(divName).outerWidth()) / 2,
                top: ($(window).height() - $(divName).outerHeight()) / 2,
                height: 'auto'
            });
        }
        //pop-up
        $(window).resize(function () {

            jsDivAutoCenter('.msgBox');
        });
        function msgBox_jsSearch() {
            var strKey = document.getElementById('msgBox_txtMaDanhMuc').value;
            var strTenDM = document.getElementById('msgBox_txtTenDanhMuc').value;
            var strPage = msgBox_type;
            var strSHKB = "";
            if (document.getElementById('txtSHKB').value.length > 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            var curMa_Dthu = '';
            var curMa_DBHC = document.getElementById('txtMaDBHC').value;   //txtMaDBHC
            PageMethods.SearchDanhMuc(curMa_Dthu, curMa_DBHC, strPage, strKey, strTenDM, strSHKB, SearchDanhMuc_Complete, SearchDanhMuc_Error);
        }
        function SearchDanhMuc_Complete(result, methodName) {
            //document.getElementById('divDanhMuc').innerHTML = result;          
            document.getElementById('msgBox_divDanhMuc').innerHTML = jsBuildResultTable(result);

        }
        function SearchDanhMuc_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('msgBox_divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
        function SelectDanhMuc(strID, strTitle) {
            //var arr = new Array();
            //arr["ID"] = strID;
            //arr["Title"] = strTitle;
            //document.getElementById('msgBox_result').value = arr;
            jsMsgBoxClose();
            if (strID.length > 0) {
                document.getElementById(msgBox_txtID).value = strID;
                if (msgBox_type == "KhoBac") {

                    jsSHKB_lostFocus(strID);
                }
                if (strTitle != null && msgBox_txtTitle != null) {
                    if (msgBox_txtTitle.length > 0)
                        document.getElementById('' + msgBox_txtTitle).value = strTitle;

                }
                if (msgBox_txtFocus != null) {
                    console.log(msgBox_txtFocus);
                    document.getElementById(msgBox_txtFocus).focus();
                }
            }
            //window.parent.returnValue = arr;
            //window.parent.close();
        }
        function getParam(param) {
            var query = window.location.search.substring(1);
            var parms = query.split('&');
            for (var i = 0; i < parms.length; i++) {
                var pos = parms[i].indexOf('=');
                if (pos > 0) {
                    var key = parms[i].substring(0, pos).toLowerCase();
                    var val = parms[i].substring(pos + 1);
                    if (key == param.toLowerCase())
                        return val;
                }
            }
            jsDivAutoCenter('.msgBox');
            return '';
        }

        function jsBuildResultTable(strInput) {
            var strRet = '';
            if (strInput.length > 0) {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                var arr = strInput.split("|");
                for (var i = 0; i < arr.length - 1; i++) {
                    var strID = arr[i].split(";")[0];
                    var strTitle = arr[i].split(";")[1];
                    strRet += "<tr onclick=\"SelectDanhMuc('" + strID + "','" + strTitle + "');\" class='PH'>" + "<td width='200' align='left'><b>" + strID + "</b></td><td  align='left'>" + strTitle + "</td></tr><tr><td colspan='2' height='8' ></td></tr> <tr><td colspan='2' class='line'></td></tr>";
                }
                strRet += "</table>";
            } else {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td width='100%' align='left' class='nav'>Không tồn tại danh mục này !</td></tr></table>";
            }
            return strRet;
        }


    </script>
</asp:Content>

