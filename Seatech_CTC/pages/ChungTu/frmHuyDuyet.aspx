﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmHuyDuyet.aspx.vb" Inherits="pages_ChungTu_frmHuyDuyet" Title="Hủy chứng từ đã kiểm soát"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">HỦY CHỨNG TỪ ĐÃ KIỂM SOÁT</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table border='0' cellpadding='2' cellspacing='1' width='100%' class="form_input">
                    <tr align="left">
                        <td style="width: 120px;" class="form_label">
                            <asp:Label ID="lblSoCT" CssClass="label" runat="server">Số CT</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtSoCT" runat="server" Width="120px" MaxLength="50" CssClass="inputflat"></asp:TextBox>
                        </td>
                        <td style="width: 150px;" class="form_label">
                            <asp:Label ID="lblPTTT" CssClass="label" runat="server">Phương thức thanh toán</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID='ddlPTTT' runat="server" CssClass="inputflat" Width="200px">
                                <asp:ListItem Text ="Nộp tiền mặt" Value ="00" Selected="True" ></asp:ListItem>
                                <asp:ListItem Text ="Chuyển khoản" Value ="01" ></asp:ListItem>
                                <asp:ListItem Text ="Điện chuyển tiền" Value ="02" ></asp:ListItem>
                                <asp:ListItem Text ="Nộp trung gian" Value ="03" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="form_label">
                            <asp:Button ID="cmdTim" runat="server" Text="Tìm kiếm" CssClass="ButtonCommand" />&nbsp;
                            <asp:Button ID="cmdHuy" runat="server" Text="Hủy KS" CssClass="ButtonCommand" />
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblMaNV" CssClass="label" runat="server">Mã NV</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID='ddlMaNV' runat="server" CssClass="inputflat" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td class="form_control" colspan='3'>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;" align="center" colspan="2">
            </td>
        </tr>
        <tr>
            <td align="left" class="errorMessage" colspan='2'>
                <asp:DataGrid ID="dgChungTu" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" AllowPaging="True" BorderColor="#989898" CssClass="grid_data">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="SHKB" HeaderText="SHKB">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DBHC" HeaderText="ĐBHC">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="80px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SO_CT" HeaderText="Số CT">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SO_BT" HeaderText="Số BT">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PT_TT" HeaderText="PTTT" Visible="false" >
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>                       
                        <asp:BoundColumn DataField="TTIEN" HeaderText="Số tiền">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Ma_NV" HeaderText="Mã NV">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TK_KH_NH" HeaderText="TK ghi có (TK Khách hàng/TK GL)">
                            <HeaderStyle Width="180px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TK_KB_NH" HeaderText="TK ghi nợ (TK Kho bạc)">
                            <HeaderStyle Width="150px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <div id='divTienMat' style="width: 0px; height: 0px;">
    </div>
</asp:Content>
