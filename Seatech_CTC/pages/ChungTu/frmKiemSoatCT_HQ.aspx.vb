﻿Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business.NewChungTu
Imports System.Xml
Imports Business.CTuCommon
Imports log4net
Imports GIPBankService
Imports System.IO
Imports CorebankServiceESB

Partial Class pages_ChungTu_frmKiemSoatCT_HQ
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_objUser As New CTuUser
    Private illegalChars As String = "[&]"
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Private Shared CORE_ON_OFF As String = ConfigurationManager.AppSettings.Get("CORE.ON").ToString()
    Private Shared CORE_VERSION As String = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString()

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            CreateDumpChiTietCTGrid() 'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
            ClearCTHeader() 'Xóa thông tin ở grid header
            ShowHideCTRow(False)
            lblStatus.Text = "Hãy chọn một chứng từ cần kiểm soát"

        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'BEGIN-TYNK CHECK NGAY NGHI
        'If Not Business.CTuCommon.GetNgayNghi() Then
        '    ' clsCommon.ShowMessageBox(Me, "Hôm nay là ngày nghỉ. Bạn không được vào chức năng này!!!")
        '    Response.Redirect("~/pages/frmHomeNV.aspx?CheckNN=1", False)
        '    Exit Sub
        'End If
        'END TYNK CHECK NGAY NGHI
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdKS.Click, AddressOf cmdKS_Click
            RemoveHandler cmdChuyenThue.Click, AddressOf cmdChuyenThue_Click
            RemoveHandler cmdHuy.Click, AddressOf cmdHuy_Click
            RemoveHandler cmdChuyenTra.Click, AddressOf cmdChuyenTra_Click
            Exit Sub
        End If
        'Kiểm tra trạng thái session
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))
        Else
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        'Check các quyền của user

        If Not IsPostBack Then
            'Khởi tạo các giá trị khởi đầu cho các điều khiển
            'Load danh sách chứng từ thành lập bởi điểm thu
            LoadDSCT("99")
        End If
        'cmdKS.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(cmdKS, "").ToString())
        'cmdChuyenTra.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(cmdChuyenTra, "").ToString())
        'cmdHuy.Attributes.Add("onclick", "this.disabled=true;" + Page.ClientScript.GetPostBackEventReference(cmdHuy, "").ToString())
    End Sub
   
    Private Sub CreateDumpChiTietCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("MAQUY"))
        v_dt.Columns.Add(New DataColumn("MA_CHUONG"))
        v_dt.Columns.Add(New DataColumn("MA_KHOAN"))
        v_dt.Columns.Add(New DataColumn("MA_TMUC"))
        v_dt.Columns.Add(New DataColumn("NOI_DUNG"))
        v_dt.Columns.Add(New DataColumn("TTIEN"))
        v_dt.Columns.Add(New DataColumn("SODATHUTK"))
        v_dt.Columns.Add(New DataColumn("KY_THUE"))

        grdChiTiet.Controls.Clear()
        Dim v_dumpRow As DataRow = v_dt.NewRow
        v_dt.Rows.Add(v_dumpRow)
        grdChiTiet.DataSource = v_dt
        grdChiTiet.DataBind()
    End Sub

    Private Sub CreateDumpDSCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("SHKB"))
        v_dt.Columns.Add(New DataColumn("NGAY_KB"))
        v_dt.Columns.Add(New DataColumn("SO_CT"))
        v_dt.Columns.Add(New DataColumn("KYHIEU_CT"))
        v_dt.Columns.Add(New DataColumn("SO_BT"))
        v_dt.Columns.Add(New DataColumn("MA_DTHU"))
        v_dt.Columns.Add(New DataColumn("MA_NV"))
        v_dt.Columns.Add(New DataColumn("TRANG_THAI"))
        v_dt.Columns.Add(New DataColumn("TT_CTHUE"))
        v_dt.Columns.Add(New DataColumn("MA_NNTHUE"))
        v_dt.Columns.Add(New DataColumn("TEN_DN"))
        v_dt.Columns.Add(New DataColumn("response_msg"))
        v_dt.Columns.Add(New DataColumn("so_ct_nh"))
        v_dt.Columns.Add(New DataColumn("Ma_KS"))
        grdDSCT.Controls.Clear()
        'Dim v_dumpRow As DataRow = v_dt.NewRow
        'v_dt.Rows.Add(v_dumpRow)
        grdDSCT.DataSource = v_dt
        grdDSCT.DataBind()
    End Sub

    Private Sub ClearCTHeader()
        txtTenTKCo.Text = ""
        txtSHKB.Text = ""
        txtTenKB.Text = ""
        txtNGAY_KH_NH.Text = ""
        txtTKCo.Text = ""
        txtTKNo.Text = ""
        txtMaDBHC.Text = ""
        txtTenDBHC.Text = ""
        txtMaNNT.Text = ""
        txtTenNNT.Text = ""
        txtDChiNNT.Text = ""
        txtMaNNTien.Text = ""
        txtTenNNTien.Text = ""
        txtDChiNNT.Text = ""
        txtMaCQThu.Text = ""
        txtTenCQThu.Text = ""
        ddlLoaiThue.SelectedIndex = 0
        txtDescLoaiThue.Text = ""
        txtLHXNK.Text = ""
        txtDescLHXNK.Text = ""
        txtSoKhung.Text = ""
        txtSoMay.Text = ""
        txtToKhaiSo.Text = ""
        txtMaNT.Text = "VND"
        txtTenNT.Text = "Việt Nam Đồng"
        txtNgayDK.Text = ""
        txtSo_BK.Text = ""
        txtNgay_BK.Text = ""
        ddlHTTT.SelectedIndex = 0
        txtTK_KH_NH.Text = ""
        txtTenTK_KH_NH.Text = ""
        txtNGAY_KH_NH.Text = ""
        txtMA_NH_A.Text = ""
        txtTen_NH_A.Text = ""
        txtMA_NH_B.Text = ""
        txtTen_NH_B.Text = ""
        txtSoCT_NH.Text = ""
        txtSoCT.Text = ""
        txtKHCT.Text = ""
    End Sub

    Private Sub LoadDSCT(ByVal pv_strTrangThaiCT As String)
        Try
            Dim dsCT As DataSet = GetDSCT(pv_strTrangThaiCT)
            If (Not dsCT Is Nothing) And (dsCT.Tables(0).Rows.Count > 0) Then
                Me.grdDSCT.DataSource = dsCT
                Me.grdDSCT.DataBind()
            Else
                CreateDumpDSCTGrid()
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    'Lay danh sach chung tu HAI QUAN
    Private Function GetDSCT(ByVal pv_strTrangThaiCT As String) As DataSet
        Try
            If pv_strTrangThaiCT.Equals("99") Then pv_strTrangThaiCT = ""
            'Load danh sách chứng từ thành lập bởi điểm thu
            'Trường hợp thu hộ thì chứng từ sẽ có maDT <> maDT ban đầu của giao dịch viên
            'trong trường hợp này mv_strUserKS<>"" có nghĩa là lấy tất cả chưng từ mà giao dịch viên
            'thực hiện không phân biệt theo mã DT nữa
            'If (Not mv_objUser.Ngay_LV Is Nothing) And (mv_objUser.Ngay_LV.Length > 0) Then
            ''Dim dsCT As DataSet = ChungTu.buChungTu.CTU_GetListKiemSoat_Set(mv_objUser.Ngay_LV, pv_strTrangThaiCT, _
            ''                      mv_objUser.Ban_LV, "", mv_objUser.Ma_NV, "")

            'mv_objUser.Ban_LV="" cho phep lay chung tu thu ho
            Dim dsCT As DataSet = ChungTu.buChungTu.CTU_GetListKiemSoat(mv_objUser.Ngay_LV, pv_strTrangThaiCT, _
                                  "", "04", "", mv_objUser.Ma_NV, "")
            Return dsCT
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            Return Nothing
        End Try
    End Function

    Private Sub LoadCTChiTietAll(ByVal key As KeyCTu)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTThue As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty

            'Load chứng từ header
            Dim dsCT As New DataSet
            dsCT = ChungTu.buChungTu.CTU_Header_Set(key)
            If (dsCT.Tables(0).Rows.Count > 0) Then
                ShowHideCTRow(True)
                FillDataToCTHeader(dsCT)
                v_strTT = dsCT.Tables(0).Rows(0)("TRANG_THAI").ToString
                v_strLoaiThue = dsCT.Tables(0).Rows(0)("MA_LTHUE").ToString
                v_strPhuongThucTT = dsCT.Tables(0).Rows(0)("PT_TT").ToString
                v_str_Ma_NNT = dsCT.Tables(0).Rows(0)("Ma_NNThue").ToString
                v_strTTThue = dsCT.Tables(0).Rows(0)("TT_CTHUE").ToString
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If

            'Load thông tin chi tiết
            Dim dsCTChiTiet As New DataSet
            dsCTChiTiet = ChungTu.buChungTu.CTU_ChiTiet_Set(key)
            If dsCTChiTiet.Tables(0).Rows.Count > 0 Then
                FillDataToCTDetail(dsCTChiTiet, v_str_Ma_NNT)
                CaculateTongTien(dsCTChiTiet.Tables(0))
            Else
                lblStatus.Text = "Không lấy được thông tin chi tiết của chứng từ.Hãy kiểm tra lại"
                Exit Sub
            End If

            ShowHideButtons(v_strTT, v_strTTThue)
            ShowHideControlByLoaiThue(v_strLoaiThue)
            ShowHideControlByPT_TT(v_strPhuongThucTT, v_strTT)
            Me.txtMatKhau.Focus()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub

    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet)
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty
            Dim dr As DataRow = dsCT.Tables(0).Rows(0)
            Dim v_strTrangThai As String = dr("TRANG_THAI").ToString()
            Dim v_strHTTT As String = GetString(dr("PT_TT")).ToString
            txtSHKB.Text = dr("SHKB").ToString()
            txtTenKB.Text = dr("TEN_KB").ToString()
            txtNGAY_KH_NH.Text = ConvertNumbertoString(dr("NGAY_KB").ToString())
            txtTKCo.Text = dr("TK_Co").ToString()
            txtTKNo.Text = dr("TK_No").ToString()
            txtMaDBHC.Text = dr("MA_XA").ToString()
            '*******************************************'
            'Author: Anhld
            'Event: Them ma hai quan
            txtMaHQ.Text = dr("MA_HQ").ToString()
            txtDescMaHQ.Text = dr("TEN_HQ").ToString()
            txtMaHQPH.Text = dr("MA_HQ_PH").ToString()
            txtTenMaHQPH.Text = dr("TEN_HQ_PH").ToString()
            'ddlMaLoaiTienThue.SelectedValue = dr("LOAI_TT").ToString()
            'txtTimeLap.Text = GetString(dr("ngay_ht"))
            'End
            '*******************************************'
            txtTenDBHC.Text = dr("TENXA").ToString()
            txtMaNNT.Text = dr("MA_NNTHUE").ToString()
            txtTenNNT.Text = dr("TEN_NNTHUE").ToString()
            txtDChiNNT.Text = dr("DC_NNTHUE").ToString()
            txtMaNNTien.Text = dr("Ma_NNTien").ToString()
            txtTenNNTien.Text = dr("TEN_NNTIEN").ToString()
            txtDChiNNTien.Text = dr("DC_NNTIEN").ToString()
            txtMaCQThu.Text = dr("MA_CQTHU").ToString()
            If dr("TEN_HQ").ToString().Trim <> "" Then
                txtTenCQThu.Text = dr("TEN_HQ").ToString()
            Else
                txtTenCQThu.Text = dr("TEN_CQTHU").ToString()
            End If

            txtSoCT.Text = dr("So_CT").ToString()
            txtKHCT.Text = dr("KyHieu_CT").ToString()
            If dr("MA_NTK").ToString() = "1" Then
                txtTenTKCo.Text = "Tài khoản thu NSNN"
            Else
                txtTenTKCo.Text = "Tài khoản tạm thu"
            End If
            Dim dt As New DataTable
            dt.Columns.Add("ID_HQ", Type.GetType("System.String"))
            dt.Columns.Add("Name", Type.GetType("System.String"))
            dt.Columns.Add("Value", Type.GetType("System.String"))
            dt.Columns(0).AutoIncrement = True
            Dim R As DataRow = dt.NewRow
            R("ID_HQ") = dr("MA_LTHUE").ToString()
            R("Name") = dr("MA_LTHUE").ToString() & "-" & GetTenLoaiThue(dr("MA_LTHUE").ToString())
            R("Value") = dr("MA_LTHUE").ToString()
            dt.Rows.Add(R)
            ddlLoaiThue.DataSource = dt
            ddlLoaiThue.DataTextField = "Name"
            ddlLoaiThue.DataValueField = "Value"
            ddlLoaiThue.DataBind()

            'txtDescLoaiThue.Text = GetTenLoaiThue(dr("MA_LTHUE").ToString())
            Dim dt_LT_THUE As New DataTable
            dt_LT_THUE.Columns.Add("ID")
            dt_LT_THUE.Columns.Add("Name")
            dt_LT_THUE.Columns(0).AutoIncrement = True
            Dim R_LT_THUE As DataRow = dt_LT_THUE.NewRow
            'R_LT_THUE("ID") = GetString(dr("LOAI_TT"))
            R_LT_THUE("Name") = Get_LTIEN_THUE(dr("LOAI_TT").ToString())
            dt_LT_THUE.Rows.Add(R_LT_THUE)
            ddlMaLoaiTienThue.DataSource = dt_LT_THUE
            ddlMaLoaiTienThue.DataTextField = "Name"
            ddlMaLoaiTienThue.DataValueField = "ID"
            ddlMaLoaiTienThue.DataBind()
            strMaLH = GetString(dr("LH_XNK").ToString())
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)
            txtLHXNK.Text = strMaLH
            txtDescLHXNK.Text = strMaLH & "-" & strTenLH
            txtMaNT.Text = dr("Ma_NT").ToString()
            txtTenNT.Text = dr("Ten_NT").ToString()
            txtSoKhung.Text = GetString(dr("SO_KHUNG").ToString())
            txtSoMay.Text = GetString(dr("SO_MAY").ToString())
            txtToKhaiSo.Text = GetString(dr("SO_TK").ToString())
            txtNgayDK.Text = GetString(dr("NGAY_TK"))
            txtSo_BK.Text = GetString(dr("So_BK"))
            txtNgay_BK.Text = GetString(dr("Ngay_BK"))
            ddlHTTT.SelectedValue = v_strHTTT
            ddlHTTT.SelectedItem.Text = Get_TenPTTT(v_strHTTT)
            rowTKGL.Visible = False
            'If v_strHTTT = "03" Then
            '    rowTKGL.Visible = True
            '    rowSoDuNH.Visible = False
            '    rowTK_KH_NH.Visible = False
            '    txtTKGL.Text = GetString(dr("TK_KH_NH"))
            '    txtTenTKGL.Text = dr("TEN_KH_NH").ToString
            'End If
            rowLHXNK.Visible = False
            txtTK_KH_NH.Text = GetString(dr("TK_KH_NH"))
            txtTenTK_KH_NH.Text = dr("TEN_KH_NH").ToString
            txtNGAY_KH_NH.Text = GetString(dr("NGAY_KH_NH"))
            txtMA_NH_A.Text = GetString(dr("MA_NH_A"))
            txtTen_NH_A.Text = GetString(dr("TEN_NH_A"))
            txtMA_NH_B.Text = GetString(dr("MA_NH_B"))
            txtTen_NH_B.Text = GetString(dr("TEN_NH_B"))
            '02/10/2012-manhnv
            txtMA_NH_TT.Text = GetString(dr("MA_NH_TT"))
            txtTEN_NH_TT.Text = GetString(dr("TEN_NH_TT"))
            txtDienGiai.Text = dr("DIEN_GIAI").ToString()
            txtGhiChu.Text = dr("GHI_CHU").ToString()
            Dim strPTTP As String = ""
            strPTTP = dr("pt_tinhphi").ToString()
            If strPTTP = "O" Then
                rdTypePN.Checked = True
            ElseIf strPTTP = "B" Then
                rdTypePT.Checked = True
            ElseIf strPTTP = "W" Then
                rdTypeMP.Checked = True
            End If
            Dim strTT_CITAD As String = ""
            strTT_CITAD = dr("TT_CITAD").ToString()
            If strTT_CITAD = "1" Then
                RadioButton1.Checked = True
            Else
                RadioButton2.Checked = True
            End If
            'txtTenTKCo.Text = ""
            txtCharge.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD").ToString(), ".")
            txtVAT.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT").ToString(), ".")
            txtRM_REF_NO.Text = dr("RM_REF_NO").ToString()
            txtREF_NO.Text = dr("REF_NO").ToString()
            txtSoCT_NH.Text = dr("SO_CT_NH").ToString()
            txtQuan_HuyenNNTien.Text = dr("QUAN_HUYENNNTIEN").ToString()
            txtTinh_TphoNNTien.Text = dr("TINH_TPNNTIEN").ToString()
            If dr("LY_DO_HUY").ToString().Trim <> "" Then
                drpLyDoHuy.SelectedValue = dr("LY_DO_HUY").ToString()
            End If
            hdfTrangThai_CT.Value = dr("trang_thai").ToString().Trim
            hdfTrangThai_CThue.Value = dr("tt_cthue").ToString().Trim
            txtSanPham.Text = dr("ma_sanpham").ToString()
            txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dr("tt_tthu").ToString().Trim, ".")
            If dr("so_ct_nh").ToString() <> "" Then
                lbSoFT.Text = "Số FT:" & dr("so_ct_nh").ToString()
            Else
                lbSoFT.Text = ""
            End If
            lblStatus.Text = dr("MSG_LOI").ToString()
            txtQuanNNThue.Text = dr("MAHUYEN_NNT").ToString().Trim
            txtTenQuanNNThue.Text = dr("TENHUYEN_NNT").ToString().Trim
            txtTinhNNThue.Text = dr("MATINH_NNT").ToString().Trim
            txtTenTinhNNThue.Text = dr("TENTINH_NNT").ToString().Trim

            txtQuan_HuyenNNTien.Text = dr("tenhuyen_nnthay").ToString().Trim
            'txtQuan_HuyenNNTien.Text = dr("TENHUYEN_NNT").ToString().Trim
            txtTinh_TphoNNTien.Text = dr("tentinh_nnthay").ToString().Trim
            ' txtTenTinhNNThue.Text = dr("TENTINH_NNT").ToString().Trim
            hdfExistCT.Value = clsCTU.CTU_Check_Exist_NNT_HQ(txtMaNNT.Text.ToString(), txtToKhaiSo.Text.ToString())
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình FillDataToCTHeader")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            Throw ex
        Finally
        End Try
    End Sub

    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet, ByVal v_strMa_NNT As String)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            Dim dr As DataRow
            Dim sodathu As String
            grdChiTiet.DataSource = v_dt
            grdChiTiet.DataBind()
            For i As Integer = 0 To v_dt.Rows.Count - 1
                dr = v_dt.Rows(i)
                sodathu = clsCTU.TCS_CalSoDaThu(v_strMa_NNT, dr("MaQuy").ToString(), dr("Ma_Chuong").ToString(), dr("Ma_Khoan").ToString(), dr("Ma_TMuc").ToString(), dr("Ky_Thue").ToString())
                grdChiTiet.Items(i).Cells(7).Text = sodathu
                grdChiTiet.Items(i).Cells(7).Enabled = False
                CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text = VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".")
            Next
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    Private Sub CaculateTongTien(ByVal pv_dt As DataTable)
        'Tính toán và cập nhật lại trường tổng tiền
        Dim dblTongTien As Double = 0
        Dim dblTongTrichNo As Double = 0
        For i As Integer = 0 To pv_dt.Rows.Count - 1
            dblTongTien += pv_dt.Rows(i)("TTIEN")
        Next
        Dim dblCharge As Double = CDbl(IIf((txtCharge.Text.Replace(".", "").Length > 0), txtCharge.Text.Replace(".", ""), "0"))
        Dim dblVAT As Double = CDbl(IIf((txtVAT.Text.Replace(".", "").Length > 0), txtVAT.Text.Replace(".", ""), "0"))
        txtTTIEN.Text = VBOracleLib.Globals.Format_Number(dblTongTien, ".")
        dblTongTrichNo = dblTongTien + dblCharge + dblVAT
        'txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dblTongTrichNo, ".")
    End Sub

    Private Function GetTenLoaiThue(ByVal pv_strMaLoaiThue As String) As String
        Select Case pv_strMaLoaiThue
            Case "01"
                Return "Thuế khác (Thuế công thương nghiệp)"
            Case "03"
                Return "Trước bạ (Nhà đất,xe máy)"
            Case "04"
                Return "Thuế Hải Quan"
            Case Else '05'
                Return "Không xác định"
        End Select
    End Function

    Private Sub GetThongTinTKKH(ByVal v_strHTTT As String, ByVal v_strTrangThaiCT As String, _
                    ByVal pv_strSoTKKH As String, ByRef pv_strTenKH As String, ByRef pv_strTTKH As String)
        If (pv_strSoTKKH.Length > 0) Then 'Lay theo sotk
            If v_strHTTT.Equals("01") Then 'Chuyen Khoan
                If v_strTrangThaiCT.Equals("05") Then 'Cho KS lay thong tin tu corebank

                Else 'Cac truong hop khac thi lay thong tin tu db

                End If
            End If
        End If
    End Sub

    Private Sub ShowHideButtons(ByVal strTT As String, ByVal strTTThue As String)
        If strTT <> "01" Then
            lbSoFT.Text = ""
        End If
        Select Case strTT
            Case "00"
                lblStatus.Text = "Chứng từ chưa kiểm soát"
                cmdHuy.Enabled = False
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "01"
                'lblStatus.Text = "Chứng từ đã kiểm soát"
                cmdHuy.Enabled = False
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = True
            Case "02"
                lblStatus.Text = "Chứng từ đã hủy bởi GDV - đang chờ duyệt"
                cmdHuy.Enabled = True
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = True
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "03"
                lblStatus.Text = "Chứng từ bị chuyển trả"
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "04"
                lblStatus.Text = "Chứng từ đã bị hủy"
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "05"
                lblStatus.Text = "Chứng từ chờ kiểm soát"
                cmdKS.Enabled = True
                cmdChuyenTra.Enabled = True
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "06"
                lblStatus.Text = "Chứng từ hạch toán lỗi ở BDS"
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = True
                cmdINCT.Enabled = False
        End Select
        cmdChuyenThue.Enabled = False
        If strTTThue = "0" Then
            If strTT = "01" Then
                'lblStatus.Text = "Chứng từ đã duyệt nhưng chưa chuyển sang hải quan"
                cmdChuyenThue.Enabled = True
                cmdHuy.Enabled = False
            End If
        End If
        If strTTThue = "1" Then
            If strTT = "04" Then
                'lblStatus.Text = "Chứng từ đã hủy nhưng chưa chuyển sang hải quan"
                cmdChuyenThue.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowHideCTRow(ByVal pv_blnDisplayRow As Boolean)
        rowDChiNNT.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = pv_blnDisplayRow
        rowQuan_HuyenNNTien.Visible = False
        rowTinhNNTien.Visible = pv_blnDisplayRow
        rowDBHC.Visible = pv_blnDisplayRow
        rowCQThu.Visible = pv_blnDisplayRow
        rowTKNo.Visible = False
        rowTKCo.Visible = pv_blnDisplayRow
        rowHTTT.Visible = pv_blnDisplayRow
        rowTK_KH_NH.Visible = pv_blnDisplayRow
        rowSoDuNH.Visible = pv_blnDisplayRow
        rowNgay_KH_NH.Visible = pv_blnDisplayRow
        rowMaNT.Visible = pv_blnDisplayRow
        rowLoaiThue.Visible = pv_blnDisplayRow
        rowToKhaiSo.Visible = pv_blnDisplayRow
        rowNgayDK.Visible = pv_blnDisplayRow
        rowLHXNK.Visible = pv_blnDisplayRow
        rowSoKhung.Visible = pv_blnDisplayRow
        rowSoMay.Visible = pv_blnDisplayRow
        rowSoBK.Visible = pv_blnDisplayRow
        rowNgayBK.Visible = pv_blnDisplayRow
        rowNganHangChuyen.Visible = pv_blnDisplayRow
        rowNganHangNhan.Visible = pv_blnDisplayRow
        rowTKGL.Visible = pv_blnDisplayRow
        rowLoaiTienThue.Visible = pv_blnDisplayRow
        'rowDienGiai.Visible = pv_blnDisplayRow
    End Sub

    Private Sub ShowHideControlByLoaiThue(ByVal pv_strLoaiThue As String)
        Select Case pv_strLoaiThue
            Case "01" 'NguonNNT.NNThue_CTN, NguonNNT.NNThue_VL, NguonNNT.SoThue_CTN, NguonNNT.VangLai
                'An thong tin To khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "02"

            Case "03" 'NguonNNT.TruocBa
                'Show thong tin Truoc ba
                rowSoKhung.Visible = True
                rowSoMay.Visible = True

                'an thong tin to khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
            Case "04" 'NguonNNT.HaiQuan
                'Show thong tin to khai
                rowLHXNK.Visible = True
                rowToKhaiSo.Visible = True
                rowNgayDK.Visible = True

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case Else '"05" Khong xac dinh
        End Select
    End Sub

    Private Sub ShowHideControlByPT_TT(ByVal pv_strPT_TT As String, ByVal pv_strTT As String)
        If pv_strPT_TT = "01" Or pv_strPT_TT = "03" Then 'Chuyển khoản
            rowTK_KH_NH.Visible = True
            rowNganHangNhan.Visible = True
            rowNganHangChuyen.Visible = True
            rowTKGL.Visible = False

            If pv_strTT.Equals("05") Then
                rowSoDuNH.Visible = True
                rowTK_KH_NH.Visible = True
                Dim v_strSoTKKH As String = txtTK_KH_NH.Text
                If v_strSoTKKH.Length > 0 Then
                    'Dim v_objCoreBank As New clsCoreBankUtilMSB
                    Dim v_strDescTKKH As String = String.Empty
                    Dim v_strSoDuNH As String = String.Empty
                    Dim v_strErrorCode As String = String.Empty
                    Dim v_strErrorMsg As String = String.Empty
                    'If (v_objCoreBank.GetCustomerInfo(v_strSoTKKH, v_strDescTKKH, v_strSoDuNH, v_strErrorCode, v_strErrorMsg).Equals("0")) Then
                    '    txtTenTK_KH_NH.Text = v_strDescTKKH
                    '    txtSoDu_KH_NH.Text = VBOracleLib.Globals.Format_Number(v_strSoDuNH, ".")
                    'Else
                    '    lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                    '    Exit Sub
                    'End If
                    'If (txtTK_KH_NH.Text.Trim.Length > 0) Then
                    '    Dim xmlDoc As New XmlDocument

                    '    Dim v_strReturn As String = clsCoreBankUtilPGB.GetTK_KH_NH(txtTK_KH_NH.Text.Trim)
                    '    v_strReturn = Regex.Replace(v_strReturn, illegalChars, "")
                    '    xmlDoc.LoadXml(v_strReturn)
                    '    Dim ds As New DataSet
                    '    ds.ReadXml(New XmlTextReader(New StringReader(xmlDoc.InnerXml)))
                    '    If ds.Tables.Count > 0 Then
                    '        txtTenTK_KH_NH.Text = ds.Tables(0).Rows(0)("CIFNO") & "|" & ds.Tables(0).Rows(0)("BRANCH_CODE") & "|" & ds.Tables(0).Rows(0)("ACCOUNTNAME") & "|" & ds.Tables(0).Rows(0)("CUSTOMER_TYPE") & "|" & ds.Tables(0).Rows(0)("CUSTOMER_CATEGORY")
                    '        txtSoDu_KH_NH.Text = VBOracleLib.Globals.Format_Number(ds.Tables(0).Rows(0)("ACY_AVL_BAL"), ".")
                    '    Else
                    '        lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                    '    End If
                    'End If
                End If
            Else
                rowSoDuNH.Visible = False
            End If
        ElseIf pv_strPT_TT = "00" Then  'Tiền mặt
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = False
            rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = True
            'ElseIf pv_strPT_TT = "03" Then  'Chuyển trung gian
            '    rowTK_KH_NH.Visible = False
            '    rowNganHangNhan.Visible = True
            '    rowNganHangChuyen.Visible = True
            '    rowTKGL.Visible = True
            '    rowSoDuNH.Visible = False
        Else 'Báo có
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = False
            rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = True
        End If
    End Sub

    Protected Sub cmdChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenTra.Click
        Try
            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới."
                Exit Sub
            End If

            'If Check_Mode() <> gblnTTBDS Then
            '    lblStatus.Text = "Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại"
            '    Exit Sub
            'End If

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT

                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    Exit Sub
                End If
                If strTT_Data = "02" Then
                    ChungTu.buChungTu.CTU_SetTrangThai(key, "01", "", "", "", "", "", "", v_strTrangThai_CT)
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    EnabledButtonWhenDoTransaction(False)
                    cmdHuy.Enabled = False
                    Exit Sub
                Else
                    ChungTu.buChungTu.CTU_SetTrangThai(key, "03")
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    EnabledButtonWhenDoTransaction(False)
                    Dim strTK_KH_NH As String = txtTK_KH_NH.Text.Trim
                    Dim strPTTinhPhi As String = ""
                    Dim strTTien As String = txtTTIEN.Text.Trim.Replace(".", "")
                    If rdTypeMP.Checked = True Then
                        strPTTinhPhi = "W"
                    ElseIf rdTypePN.Checked = True Then
                        strPTTinhPhi = "O"
                    Else
                        strPTTinhPhi = "B"
                    End If

                    Dim strFeeAmount As String = txtCharge.Text.Trim.Replace(".", "")
                    Dim strVATAmount As String = txtVAT.Text.Trim.Replace(".", "")

                End If
                
                lblStatus.Text = "Chuyển trả chứng từ thành công."
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            logger.Error("Error source: " & ex.Source & vbNewLine _
                  & "Error code: System error!" & vbNewLine _
                  & "Error message: " & ex.Message)
        End Try
    End Sub

    Protected Sub cmdKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKS.Click
        Dim v_blnReturn As Boolean = False
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim i_char As Integer = confirmValue.LastIndexOf(",")
        i_char = i_char + 1
        confirmValue = confirmValue.Substring(i_char, confirmValue.Length - i_char)
        If confirmValue = "Có" Then
        Else
            Exit Sub
        End If
        Try
            'If clsCTU.checkGioGD() = "0" Then
            '    lblStatus.Text = "Đã hết giờ giao dịch trong ngày. Không được tiếp tục kiểm soát!"
            '    Exit Sub
            'End If
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)

            'Kiểm tra ngày làm việc
            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
                EnabledButtonWhenDoTransaction(True)
                Exit Sub
            End If

            'Kiểm tra mật khẩu kiểm soát
            'Kiểm tra mật khẩu kiểm soát
            Dim strUser = mv_objUser.Ten_DN.ToString
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If Not Business.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
            '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
            '        EnabledButtonWhenDoTransaction(True)
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi kiểm soát."
            '    EnabledButtonWhenDoTransaction(True)
            '    Exit Sub
            'End If
            
            'Check Hạn mức
            Dim tien_tongTN As Decimal = Convert.ToDecimal(txtTongTrichNo.Text.ToString().Replace(".", ""))

            If Not Business.Common.mdlCommon.CheckHanMuc_KSV(Session.Item("User").ToString(), tien_tongTN) Then
                lblStatus.Text = "Hạn mức của bạn không đủ để duyệt giao dịch này."
                Exit Sub
            End If
            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            Dim v_strMaNNT As String = hdfMaNNT.Value
            Dim v_objGDV As CTuUser
            v_objGDV = New CTuUser(v_strMa_NV)
            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""

            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            hdr.ten_kb = Globals.RemoveSign4VietnameseString(txtTenKB.Text)
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu


                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If

                Dim strTongTienTT As String = txtTTIEN.Text.Replace(".", "")
                Dim strSoduTaiKhoanKH As String = txtSoDu_KH_NH.Text.Replace(".", "")
                Dim strHT_TT As String = ddlHTTT.Text.Trim
                Dim v_strErrorCode As String = ""
                Dim v_strErrorMessage As String = ""
                Dim v_strPM_rm_ref_no As String = ""
                Dim v_strPM_so_du As String = ""
                If strHT_TT = "03" And txtSoDu_KH_NH.Text.Length = 0 Then
                    lblStatus.Text = "Tài khoản khách hàng không đủ để thanh toán"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If
                Dim strMaNNT As String = txtMaNNT.Text.Trim
                Dim strTenNNT As String = Globals.RemoveSign4VietnameseString(txtTenNNT.Text.Trim)
                Dim strDiaChiNNT As String = Globals.RemoveSign4VietnameseString(txtDChiNNT.Text.Trim)
                Dim strSoCT_NH As String = txtSoCT_NH.Text.Trim
                Dim strErrorCode As String = ""
                Dim strErrorMessege As String = ""
                Dim a As Integer = 1
                Dim str_MaTinh As String = txtTinh_TphoNNTien.Text.Trim
                Dim str_TenTinh As String = Globals.RemoveSign4VietnameseString(txtQuan_HuyenNNTien.Text.Trim)
                Dim str_AccountNo As String = txtTK_KH_NH.Text.Trim
                Dim str_HTTT As String
                If strHT_TT = "01" Then
                    str_HTTT = "A"
                Else
                    str_HTTT = "G"
                End If
                Dim intRowcount As Integer
                Dim str_Desc As String = BuildRemarkForPayment(intRowcount)
                Dim str_clk As String = ""
                If intRowcount = 1 Then
                    str_clk = BuildCKTM()
                End If
                Dim str_loaiPhi As String = ""
                If rdTypeMP.Checked = True Then
                    str_loaiPhi = "W"
                ElseIf rdTypePN.Checked = True Then
                    str_loaiPhi = "O"
                ElseIf rdTypePT.Checked = True Then
                    str_loaiPhi = "B"
                End If
                
                Dim strMa_NH_A As String = txtMA_NH_A.Text.Trim
                Dim strTen_NH_A As String = txtTen_NH_A.Text.Trim
                Dim str_TKKB As String = txtTKCo.Text.Trim
                Dim str_TenKB As String = Globals.RemoveSign4VietnameseString(txtTenKB.Text.Trim & "-" & txtTenCQThu.Text)
                Dim strTenCQT As String = Globals.RemoveSign4VietnameseString(txtTenCQThu.Text.Trim)
                Dim strMa_NH_TT As String = txtMA_NH_TT.Text.Trim
                Dim strTen_NH_TT As String = txtTEN_NH_TT.Text.Trim
                Dim strMa_NH_B As String = txtMA_NH_B.Text.Trim
                Dim strTen_NH_B As String = txtTen_NH_B.Text.Trim
                Dim str_CIFNo As String = txtTenTK_KH_NH.Text.Split("|")(0)
                Dim str_RefNo As String = txtREF_NO.Text.Trim
                Dim strLoaiTien As String = txtMaNT.Text
                Dim str_MaSanPham As String = txtSanPham.Text.Trim
                Dim str_TimeInput As String = txtTimeLap.Text
                Dim strTKCo As String = txtTKCo.Text.Trim
                Dim str_Fee As String = txtCharge.Text.Trim.Replace(".", "")
                Dim str_VAT As String = txtVAT.Text.Trim.Replace(".", "")
                Dim str_TenKH As String = txtTenTK_KH_NH.Text
                Dim Tran_ID As String = ""
                Dim ngay_GD As String = ConvertNumberToDate(mv_objUser.Ngay_LV).ToString("dd/MM/yyyy")
                Dim TT_CITAD As String = ""
                Dim strMa_NguyenTe As String = clsCTU.GetMA_NguyenTe(txtMaNT.Text.Trim)
                If RadioButton1.Checked = True And strMa_NH_TT.Substring(2, 3).ToString() <> "101" And strTKCo = "7111" Then
                    TT_CITAD = "1"
                Else
                    TT_CITAD = "0"
                End If
                Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
                'xml to citad
                Dim str_DTLCitadXML As String = BuildDTLCitadXML(v_intRowCnt)
                Dim XMLtoCITAD As String = clsCTU.BuilContent_citad(txtMaNNT.Text, txtMaCQThu.Text, txtTenCQThu.Text, ddlLoaiThue.SelectedValue, txtNGAY_KH_NH.Text, _
                                        txtSoKhung.Text, txtSoMay.Text, txtToKhaiSo.Text, txtNgayDK.Text, txtLHXNK.Text, "", txtTenNNT.Text, str_DTLCitadXML)

                Dim v_dtCitad_HDR_xml As New DataTable
                Dim v_dtCitad_DTL_xml As New DataTable
                v_dtCitad_HDR_xml = clsCTU.TCS_BuildCITAD_HDR_XML()
                v_dtCitad_DTL_xml = clsCTU.TCS_BuildCITAD_DTL_XML(v_intRowCnt)

                v_dtCitad_HDR_xml.Rows(0).Item("MST") = strMaNNT
                v_dtCitad_HDR_xml.Rows(0).Item("CQT") = txtMaCQThu.Text
                v_dtCitad_HDR_xml.Rows(0).Item("TCQ") = txtTenCQThu.Text
                v_dtCitad_HDR_xml.Rows(0).Item("LTH") = ddlLoaiThue.SelectedValue
                v_dtCitad_HDR_xml.Rows(0).Item("NNT") = txtNGAY_KH_NH.Text.ToString.Split("/")(2) & txtNGAY_KH_NH.Text.ToString.Split("/")(1) & txtNGAY_KH_NH.Text.ToString.Split("/")(0)
                v_dtCitad_HDR_xml.Rows(0).Item("SKH") = txtSoKhung.Text
                v_dtCitad_HDR_xml.Rows(0).Item("SMA") = txtSoMay.Text
                v_dtCitad_HDR_xml.Rows(0).Item("STK") = txtToKhaiSo.Text
                v_dtCitad_HDR_xml.Rows(0).Item("NTK") = txtNgayDK.Text.ToString.Split("/")(2) & txtNgayDK.Text.ToString.Split("/")(1) & txtNgayDK.Text.ToString.Split("/")(0)
                v_dtCitad_HDR_xml.Rows(0).Item("XNK") = txtLHXNK.Text
                v_dtCitad_HDR_xml.Rows(0).Item("CQP") = ""
                v_dtCitad_HDR_xml.Rows(0).Item("TKH") = txtTenNNT.Text

                For i = 0 To v_intRowCnt - 1
                    v_dtCitad_DTL_xml.Rows(i).Item("MCH") = CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
                    v_dtCitad_DTL_xml.Rows(i).Item("NDK") = CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
                    v_dtCitad_DTL_xml.Rows(i).Item("STN") = VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".").Replace(".", "")
                    v_dtCitad_DTL_xml.Rows(i).Item("NDG") = CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text
                Next
                Dim str_PayMent As String = ""
                Dim rm_ref_no As String = ""
                'If CORE_ON_OFF = "1" Then
                If CORE_VERSION = "2" Then
                    'Core V2.0 20180330-------------------------------
                    Dim EcomStr As String = Globals.RemoveSign4VietnameseString(BuildRemarkForPayment_V20(intRowcount))
                    'Dim str_TenMST As String = txtTenNNT.Text
                    'Dim str_MST As String = txtMaNNT.Text
                    'Dim str_MaLThue As String = ddlLoaiThue.SelectedValue
                    'Dim str_NNT As String = txtNgayDK.Text.ToString
                    'Dim str_AddNNT As String = txtDChiNNT.Text
                    'Dim strMaCQT As String = txtMaCQThu.Text
                    'Dim strMaDBHC As String = txtMaDBHC.Text

                    'str_PayMent = cls_corebank.PaymentTQ_V20(str_AccountNo, strTKCo, strTongTienTT, strMa_NguyenTe, str_Desc, v_strNgay_KB, strTenCQT, _
                    '                                         str_MaTinh, strTenCQT, strTenCQT, strTen_NH_B, strTen_NH_B, str_TenTinh, _
                    '                                          strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B.ToString().Substring(5, 3), strMa_NH_B.ToString().Substring(0, 2), "TAX", v_strSo_CT, strMa_NH_B, v_strMa_NV, mv_objUser.Ma_NV, "ETAX_HQ", "", _
                    '                                            str_NNT, str_MaLThue, str_MST, strMaCQT, strTenCQT, strMaDBHC, str_TenMST, str_AddNNT, EcomStr, _
                    '                                            v_strErrorCode, rm_ref_no)

                    'Core V2.0 20180330-------------------------------
                Else
                    str_PayMent = cls_corebank.PaymentTQ(str_AccountNo, strTKCo, strTongTienTT, strMa_NguyenTe, str_Desc, v_strNgay_KB, strTenCQT, _
                                      str_MaTinh, strTenCQT, strTenCQT, strTen_NH_B, strTen_NH_B, str_TenTinh, _
                                       strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B.ToString().Substring(5, 3), strMa_NH_B.ToString().Substring(0, 2), "TAX", v_strSo_CT, strMa_NH_B, v_strMa_NV, mv_objUser.Ma_NV, "ETAX_HQ", "", v_strErrorCode, rm_ref_no)
                End If
                'Else
                '    str_PayMent = "hdr_Tran_Id=LPSOUTPLAIN~*res_Status=00000~*res_Result_Code=00000~*res_Ref_No=LPSF051009032697~*res_Avail_Bal=494~*res_Curr_Bal=507~*res_Acc_CCY_Cd=VND~*res_Lcy_Amt=10~*"
                'End If

                'clsCTU.PAYMENT_SHB(str_PayMent, v_strPM_rm_ref_no, v_strPM_so_du, v_strErrorCode, v_strErrorMessage)

                If v_strErrorCode = "00000" Then
                    v_strTrangThai_CT = "01" 'Trạng thái chưa cập nhật vào BDS
                    ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, rm_ref_no, "", "", mv_objUser.Ma_NV)
                    lblStatus.Text = "Kiểm soát chứng từ thành công"
                    lbSoFT.Text = "Số CT_NH:" & rm_ref_no
                    Dim ma_loi As String = ""
                    Dim so_du As String = ""
                    Dim strReturn_SODU As String = ""
                    'If CORE_ON_OFF = "1" Then
                    '    strReturn_SODU = cls_corebank.GetTK_KH_NH(txtTK_KH_NH.Text.ToString().Replace("-", ""), ma_loi, so_du)
                    '    txtSoDu_KH_NH.Text = so_du
                    'Else
                    '    strReturn_SODU = "<PARAMETER><PARAMS><BANKACCOUNT>111000222</BANKACCOUNT><ACY_AVL_BAL>1000000000</ACY_AVL_BAL><CRR_CY_CODE>VND</CRR_CY_CODE><RETCODE>00000</RETCODE><ERRCODE></ERRCODE></PARAMS></PARAMETER>"
                    'End If
                Else
                    lblStatus.Text = "Chứng từ hạch toán lỗi. Mã lỗi " & v_strErrorCode & " Mô tả lỗi: " & strErrorMessege & ". Vui lòng liên hệ QTV để được trợ giúp."
                    Exit Sub
                End If
                'EnabledButtonWhenDoTransaction(True)

                Dim blnSuccess As Boolean = True
                Dim v_strTransaction_id As String = String.Empty
                Dim desMSG_HQ As String = ""
                Try
                    ''Gui thong tin Chung tu sang Hai quan
                    'CustomsService.ProcessMSG.getMSG22(v_strMaNNT, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, v_strMa_Dthu, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                    'strErrorNum = ""
                    'Neu gui thong tin thanh cong
                    'strErrorNum = "0"
                    If strErrorNum = "0" Then
                        lblStatus.Text = "Chứng từ đã được hạch toán và chuyển số liệu sang Cơ quan Hải quan thành công"
                    Else
                        blnSuccess = False
                        lblStatus.Text = "Chứng từ đã được hạch toán. Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                        cmdKS.Enabled = False
                        cmdChuyenTra.Enabled = False
                        LoadDSCT(ddlTrangThai.SelectedValue)

                    End If
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ đã được hạch toán. Có lỗi khi gửi số liệu sang Hải Quan. Mô tả: " & ex.ToString
                    cmdKS.Enabled = False
                    cmdChuyenTra.Enabled = False
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    Exit Sub
                End Try
                'CAP NHAT TRANG THAI DA CHUYEN THUE

                'Insert du lieu vao bang DOICHIEU_NHHQ
                desMSG_HQ = lblStatus.Text.Trim
                If desMSG_HQ.Length > 400 Then
                    desMSG_HQ = desMSG_HQ.Substring(0, 399)
                End If
                If strErrorNum = "0" Then
                    clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, v_strMa_Dthu, strErrorNum, TransactionHQ_Type.M41.ToString(), "", pv_so_tn_ct, pv_ngay_tn_ct)
                    hdr.transaction_id = v_strTransaction_id
                    hdr.ten_nh_th = txtTen_NH_B.Text.Trim.ToString()
                    ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)
                End If

                ChungTu.buChungTu.InsertMSGLoi(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)

                LoadDSCT(ddlTrangThai.SelectedValue)
                ShowHideCTRow(True)
                EnabledButtonWhenDoTransaction(False)
                'lblStatus.Text = "Chứng từ đã được hạch toán và chuyển số liệu sang Cơ quan Hải quan thành công"
                cmdChuyenTra.Enabled = False
                cmdINCT.Enabled = True
                'cmdHuy.Enabled = True
                hdfTrangThai_CT.Value = v_strTrangThai_CT
                hdfTrangThai_CThue.Value = IIf(blnSuccess, "1", "0")
            Else
                lblStatus.Text = strErrorNum & " - " & strErrorMes
            End If

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình kiểm soát chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            ' Throw ex
        Finally
        End Try
    End Sub
    Private Function BuildRemarkForPayment(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim str_TenMST As String = txtTenNNT.Text.Trim.ToString()
        Dim str_KTBatDau As String = "B1D "
        Dim str_KTKetThuc As String = "K1T nop thue"
        Dim v_strRetRemark As String = String.Empty
        Dim str_MST As String = " MST" & txtMaNNT.Text.Trim.ToString()
        Dim str_MaLThue As String = "LThue" & ddlLoaiThue.SelectedValue.ToString()
        Dim str_NNT As String = "NgayNT" & txtNGAY_KH_NH.Text.ToString() & ";"
        For i = 0 To v_intRowCnt - 1
            v_strRetRemark &= "NDKT" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
            v_strRetRemark &= "C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
            v_strRetRemark &= "S" & VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".").Replace(".", "")
            v_strRetRemark &= " ND " & CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text
            'v_strRetRemark &= ",KT" & CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text
            v_strRetRemark &= ";"
        Next
        v_strRetRemark &= "TK:" & txtToKhaiSo.Text ' Globals.RemoveSign4VietnameseString(v_strMaNHNhan)
        v_strRetRemark &= "NDK:" & txtNgayDK.Text
        v_strRetRemark &= "LH:" & txtLHXNK.Text
        Dim str_GhiChu As String = "." & txtGhiChu.Text.Trim
        v_strRetRemark = str_TenMST & str_MST & str_MaLThue & str_NNT & v_strRetRemark & str_GhiChu
        v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark)
        'If v_intRowCnt > 1 Then
        '    v_strRetRemark = str_KTBatDau & str_MST & str_MaLThue & str_NNT & v_strRetRemark & str_KTKetThuc
        'Else
        '    v_strRetRemark = str_MST & str_MaLThue & str_NNT & v_strRetRemark
        'End If
        intRowcount = v_intRowCnt
        Return v_strRetRemark
    End Function
    Private Function BuildRemarkForPayment_V20(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty
        For i = 0 To v_intRowCnt - 1
            If i = 0 Then
                v_strRetRemark &= "####"
            Else
                v_strRetRemark &= "###"
            End If
            v_strRetRemark &= "~" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
            v_strRetRemark &= "~" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
            v_strRetRemark &= "~" & VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".").Replace(".", "")
            v_strRetRemark &= "~" & CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text
            'v_strRetRemark &= ",KT" & CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text
            v_strRetRemark &= "~" & txtToKhaiSo.Text
            v_strRetRemark &= "~" & txtNgayDK.Text
        Next

        Dim str_GhiChu As String = "." & txtGhiChu.Text.Trim

        v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark)

        intRowcount = v_intRowCnt
        Return v_strRetRemark
    End Function
    Private Function BuildDTLCitadXML(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim str_KTBatDau As String = "<VSTD>"
        Dim str_KTKetThuc As String = "</VSTD>"
        Dim v_strRetRemark As String = String.Empty

        For i = 0 To v_intRowCnt - 1
            v_strRetRemark &= str_KTBatDau
            v_strRetRemark &= "<MCH>" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text & "</MCH>"
            v_strRetRemark &= "<NDK>" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text & "</NDK>"
            v_strRetRemark &= "<STN>" & VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".") & "</STN>"
            v_strRetRemark &= "<NDG>" & CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text & "</NDG>"
            v_strRetRemark &= str_KTKetThuc
        Next
        intRowcount = v_intRowCnt
        Return v_strRetRemark
    End Function
    Private Function BuildCKTM() As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty

        For i = 0 To v_intRowCnt - 1
            v_strRetRemark &= "C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
            v_strRetRemark &= "T" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
        Next

        Return v_strRetRemark
    End Function
    Protected Sub cmdHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdHuy.Click
        Dim v_blnReturn As Boolean = False
        Dim pv_so_tn_ct_ych As String = ""
        Dim pv_so_tn_ct As String = ""
        Dim pv_ngay_tn_ct As String = ""
        Try
            If clsCTU.checkGioGD() = "0" Then
                lblStatus.Text = "Đã hết giờ giao dịch trong ngày. Không được tiếp tục hủy lệnh!"
                Exit Sub
            End If
            'Disable button tranh lam viec 2 lan
            'EnabledButtonWhenDoTransaction(False)
            cmdHuy.Enabled = False

            'Kiểm tra ngày làm việc
            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
                'EnabledButtonWhenDoTransaction(True)
                cmdHuy.Enabled = True
                Exit Sub
            End If

            'Kiểm tra mật khẩu kiểm soát
            Dim strUser = mv_objUser.Ten_DN.ToString
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If Not Business.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
            '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
            '        EnabledButtonWhenDoTransaction(False)
            '        cmdHuy.Enabled = True
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi Duyệt hủy."
            '    EnabledButtonWhenDoTransaction(False)
            '    cmdHuy.Enabled = True
            '    Exit Sub
            'End If

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            Dim v_strMaNNT As String = hdfMaNNT.Value
            Dim v_strTT_ChuyenThue As String = hdfTrangThai_CThue.Value
            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""

            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            Dim strLyDoHuy As String = ""
            hdr.ten_kb = Globals.RemoveSign4VietnameseString(txtTenKB.Text)
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu


                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                Dim descERR_HQ As String = ""
                Dim str_TrangThaiCT_moi As String = ""
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    'EnabledButtonWhenDoTransaction(True)
                    cmdHuy.Enabled = True
                    Exit Sub
                End If
                'Gui thong tin Chung tu sang Hai quan
                If (v_strTrangThai_CT = "01" And v_strTT_ChuyenThue = "1") Or (v_strTrangThai_CT = "02" And v_strTT_ChuyenThue = "1") Then
                    Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M41.ToString())
                    pv_so_tn_ct_ych = strTN_CT.Split(";")(0)
                    pv_ngay_tn_ct = strTN_CT.Split(";")(1)
                    'CustomsService.ProcessMSG.getMSG32(pv_so_tn_ct_ych, strErrorNum, strErrorMes, pv_so_tn_ct, pv_ngay_tn_ct, v_strTransaction_id)
                    'strErrorNum = "0"
                    If strErrorNum = "0" Then
                        'CAP NHAT TRANG THAI DA CHUYEN HAI QUAN
                        v_strTrangThai_CT = "04" 'Trạng thái đã hủy
                        str_TrangThaiCT_moi = "0"
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", mv_objUser.Ma_NV, "", "0", strLyDoHuy)


                        LoadDSCT(ddlTrangThai.SelectedValue)
                        'ClearCTHeader()
                        'ClearCTDetail()
                        'ShowHideCTRow(False)
                        'EnabledButtonWhenDoTransaction(True)
                        cmdHuy.Enabled = False
                        lblStatus.Text = "Hủy và gửi chứng từ sang Hải quan thành công"
                    Else
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", mv_objUser.Ma_NV, "", "1", strLyDoHuy)
                        lblStatus.Text = "Hủy không thành công. Lỗi từ HQ: " & strErrorNum & " - " & strErrorMes
                        cmdHuy.Enabled = True
                        str_TrangThaiCT_moi = "1"
                    End If
                    'Insert du lieu vao bang DOICHIEU_NHHQ
                    clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, v_strMa_Dthu, strErrorNum, TransactionHQ_Type.M51.ToString(), "", pv_so_tn_ct, pv_ngay_tn_ct)
                    strLyDoHuy = drpLyDoHuy.SelectedValue.Trim()
                    hdr.transaction_id = v_strTransaction_id
                    hdr.ly_do_huy = strLyDoHuy
                    descERR_HQ = lblStatus.Text.Trim
                    If (descERR_HQ.Length > 400) Then
                        descERR_HQ = descERR_HQ.Substring(0, 399)
                    End If
                    ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, str_TrangThaiCT_moi, descERR_HQ)
                    ChungTu.buChungTu.InsertMSGLoi(hdr, dtls, str_TrangThaiCT_moi, descERR_HQ)
                Else
                    v_strTrangThai_CT = "04" 'Trạng thái đã hủy
                    ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", mv_objUser.Ma_NV, "", "0", "3")


                    LoadDSCT(ddlTrangThai.SelectedValue)
                    'ClearCTHeader()
                    'ClearCTDetail()
                    'ShowHideCTRow(False)
                    'EnabledButtonWhenDoTransaction(True)
                    cmdHuy.Enabled = False
                    lblStatus.Text = "Hủy chứng từ chưa chuyển sang Hải quan thành công"
                End If
                '----------------------
                'Neu gui thong tin Huy Chung tu sang Hai quan thanh cong



            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình hủy chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            ' Throw ex
        Finally
        End Try
    End Sub

    Private Sub EnabledButtonWhenDoTransaction(ByVal blnEnabled As Boolean)
        cmdKS.Enabled = blnEnabled
        cmdChuyenTra.Enabled = blnEnabled
    End Sub

    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
            Dim v_strRetArgument As String() = btnSelectCT.CommandArgument.ToString.Split(",")
            If (v_strRetArgument.Length > 3) Then
                Dim v_strSHKB As String = v_strRetArgument(0)
                Dim v_strNgay_KB As String = v_strRetArgument(1)
                Dim v_strSo_CT As String = v_strRetArgument(2)
                Dim v_strKyHieu_CT As String = v_strRetArgument(3)
                Dim v_strSo_BT As String = v_strRetArgument(4)
                Dim v_strMa_Dthu As String = v_strRetArgument(5)
                Dim v_strMa_NV As String = v_strRetArgument(6)
                Dim v_strTrangThai_CT As String = v_strRetArgument(7)
                Dim v_strMaNNT As String = v_strRetArgument(8)
                'Load thông tin chi tiết khi người dùng chọn 1 CT cụ thể
                If ((Not v_strSHKB Is Nothing) And (Not v_strMa_NV Is Nothing) _
                     And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                    Dim key As New KeyCTu
                    key.Ma_Dthu = v_strMa_Dthu
                    key.Kyhieu_CT = v_strKyHieu_CT
                    key.Ma_NV = v_strMa_NV
                    key.Ngay_KB = v_strNgay_KB
                    key.SHKB = v_strSHKB
                    key.So_BT = v_strSo_BT
                    key.So_CT = v_strSo_CT
                    LoadCTChiTietAll(key)

                    'Dat lai gia tri
                    hdfSHKB.Value = v_strSHKB
                    hdfNgay_KB.Value = v_strNgay_KB
                    hdfSo_CT.Value = v_strSo_CT
                    hdfKyHieu_CT.Value = v_strKyHieu_CT
                    hdfSo_BT.Value = v_strSo_BT
                    hdfMa_Dthu.Value = v_strMa_Dthu
                    hdfMa_NV.Value = v_strMa_NV
                    hdfTrangThai_CT.Value = v_strTrangThai_CT
                    hdfMaNNT.Value = v_strMaNNT
                End If
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    Protected Sub ddlTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrangThai.SelectedIndexChanged
        LoadDSCT(ddlTrangThai.SelectedValue)
        'ClearCTHeader()
        CreateDumpChiTietCTGrid()
        ShowHideCTRow(False)
    End Sub

    Private Function BuildRemarkForPayment(ByVal pv_strTenNNT As String, ByVal pv_strMaNNT As String, _
                                 ByVal v_strMaNHNhan As String, ByVal pv_strNgayNop As String, ByVal pv_strMaNHTT As String) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty
        ' v_strRetRemark &= "MST:" & pv_strMaNNT & "."
        For i = 0 To v_intRowCnt - 1
            v_strRetRemark &= "(C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
            v_strRetRemark &= ".TM" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
            'v_strRetRemark &= ",KT" & Globals.RemoveSign4VietnameseString(CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text.ToUpper)
            v_strRetRemark &= ".ST:" & CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text.Replace(".", "")
            v_strRetRemark &= ")"
            v_strRetRemark &= IIf(i < v_intRowCnt - 1, "+", "")
        Next
        'If ddlLoaiThue.SelectedValue = "04" Then
        '    v_strRetRemark &= " .TK:" & txtToKhaiSo.Text ' Globals.RemoveSign4VietnameseString(v_strMaNHNhan)
        '    v_strRetRemark &= " .N:" & txtNgayDK.Text
        '    v_strRetRemark &= " .LH:" & txtLHXNK.Text
        'End If
        Return v_strRetRemark
    End Function

    Protected Sub grdDSCT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDSCT.PageIndexChanging
        Dim v_ds As DataSet = GetDSCT(ddlTrangThai.SelectedValue)
        grdDSCT.PageIndex = e.NewPageIndex
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
    End Sub

    Protected Sub cmdChuyenThue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenThue.Click
        Dim v_blnReturn As Boolean = False
        Try
            Dim strUser = mv_objUser.Ten_DN.ToString
            'Kiểm tra ngày làm việc
            'Kiểm tra mật khẩu kiểm soát
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If Not Business.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
            '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
            '        EnabledButtonWhenDoTransaction(False)
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi kiểm soát."
            '    EnabledButtonWhenDoTransaction(False)
            '    Exit Sub
            'End If
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)
            cmdChuyenThue.Enabled = False
            'Kiểm tra ngày làm việc
            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
                EnabledButtonWhenDoTransaction(True)
                Exit Sub
            End If

            'Kiểm tra mật khẩu kiểm soát



            'Kiểm tra có đang chuyển/nhân dữ liệu hay không
            'If CheckBathRun() <> "0" Then
            '    lblStatus.Text = "Hệ thống đang kết xuất dữ liệu sang kho bạc. Hãy chờ và kiểm soát lại"
            'Exit Sub
            'End If

            'Kiểm tra trạng thái của BDS hiện thời
            'If Check_Mode() <> gblnTTBDS Then
            '    lblStatus.Text = "Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại"
            'Exit Sub
            'End If

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""
            Dim pv_so_tn_ct_ych As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""
            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            hdr.ten_kb = Globals.RemoveSign4VietnameseString(txtTenKB.Text)
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If
                Dim blnSuccess As Boolean = True
                Dim MSGType As String = ""
                Dim desMSG_HQ As String = ""
                Try
                    If v_strTrangThai_CT = "01" Then
                        'CustomsService.ProcessMSG.getMSG22(txtMaNNT.Text, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, v_strMa_Dthu, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                        MSGType = TransactionHQ_Type.M41.ToString()
                    ElseIf v_strTrangThai_CT = "04" Then
                        Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M41.ToString())
                        pv_so_tn_ct_ych = strTN_CT.Split(";")(0)
                        pv_ngay_tn_ct = strTN_CT.Split(";")(1)
                        'CustomsService.ProcessMSG.getMSG32(pv_so_tn_ct_ych, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                        MSGType = TransactionHQ_Type.M51.ToString()
                    End If
                    'Neu gui thong tin thanh cong
                    If strErrorNum = "0" Then
                        Dim str_TTThue As String = String.Empty
                        If v_strTrangThai_CT = "01" Then
                            If blnSuccess = True Then
                                str_TTThue = "1"
                            Else
                                str_TTThue = "0"
                            End If

                        End If
                        If v_strTrangThai_CT = "04" Then
                            If blnSuccess = True Then
                                str_TTThue = "0"
                            Else
                                str_TTThue = "1"
                            End If
                        End If
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT)
                        'Insert du lieu vao bang DOICHIEU_NHHQ
                        clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, "", strErrorNum, MSGType, "", pv_so_tn_ct, pv_ngay_tn_ct)
                        hdr.transaction_id = v_strTransaction_id
                        hdr.ten_nh_th = txtTen_NH_B.Text.Trim.ToString()
                        ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, str_TTThue)
                        lblStatus.Text = "Chứng từ đã chuyển số liệu sang Cơ quan Hải quan thành công"
                    Else
                        blnSuccess = False
                        lblStatus.Text = "Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                        'ShowHideCTRow(False)
                        cmdChuyenThue.Enabled = True
                    End If
                    desMSG_HQ = lblStatus.Text.Trim
                    If desMSG_HQ.Length > 400 Then
                        desMSG_HQ = desMSG_HQ.Substring(0, 399)
                    End If
                    ChungTu.buChungTu.InsertMSGLoi(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ chưa chuyển được số liệu sang Cơ quan Hải quan"
                    Exit Sub
                End Try



            End If
            LoadDSCT(ddlTrangThai.SelectedValue)
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình chuyển sang hải quan")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            ' Throw ex
        Finally
        End Try
    End Sub

    Protected Sub grdChiTiet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdChiTiet.ItemDataBound

        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            'If Not e.Item.ItemType.ToString() = "Header" Then
            'e.Item.Cells(7).Text = VBOracleLib.Globals.Format_Number(e.Item.Cells(7).Text, ".")
            Dim SOTIEN As TextBox = CType(e.Item.FindControl("SOTIEN"), TextBox)
            SOTIEN.Text = VBOracleLib.Globals.Format_Number(SOTIEN.Text, ".")
        End If
    End Sub

    Protected Sub cmdChuyenBDS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenBDS.Click
        Dim v_blnReturn As Boolean = False

        Try
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)

            'Kiểm tra ngày làm việc
            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
                EnabledButtonWhenDoTransaction(True)
                Exit Sub
            End If

            'Kiểm tra mật khẩu kiểm soát
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If (mv_objUser.Mat_Khau.Equals(EscapeQuote(EncryptStr(txtMatKhau.Text)))) Then
            '        lblStatus.Text = "Mật khẩu chuyển BDS không hợp lệ.Hãy thử lại"
            '        EnabledButtonWhenDoTransaction(True)
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi chuyển BDS."
            '    EnabledButtonWhenDoTransaction(True)
            '    Exit Sub
            'End If

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If
                Dim strTongTienTT As String = txtTTIEN.Text.Replace(".", "")
                Dim v_strAmt As String = strTongTienTT 'txtTongTrichNo.Text.Replace(".", "") 'strTongTienTT
                Dim teller_id As String = clsCTU.Get_TellerID(v_strMa_NV) 'giao dich vien
                Dim approver_id As String = mv_objUser.TELLER_ID 'Kiem soat vien

                Dim v_strHostName As String = String.Empty
                Dim v_strDBName As String = String.Empty
                Dim v_strDBUser As String = String.Empty
                Dim v_strDBPass As String = String.Empty
                Dim v_strHost_REFNo As String = String.Empty
                Dim v_strBDS_REFNo As String = String.Empty

                Dim strErrorNum As String = String.Empty
                Dim strErrorMes As String = String.Empty
                Dim v_strTransaction_id As String = String.Empty

                Dim hdr As New infDoiChieuNHHQ_HDR
                Dim dtls() As infDoiChieuNHHQ_DTL

                Dim v_total_ID As String = "924"
                lblStatus.Text = "Không thể lấy tham số của BDS hãy kiểm tra lại."
                If (clsCTU.TCS_GetThamSoBDS(mv_objUser.MA_CN, v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)) Then
                    Dim v_objBDS As New BDSHelper(v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)
                    If v_objBDS.IncreaseTotal(v_total_ID, teller_id, v_strAmt) Then
                        'Cập nhật lại trạng thái của chứng từ thành đã kiểm soát
                        v_strTrangThai_CT = "01" 'Trạng thái đã kiểm soát
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
                    Else
                        lblStatus.Text = "Không thể cập nhật số tiền vào BDS.Hãy kiểm tra lại"
                        EnabledButtonWhenDoTransaction(True)
                        Exit Sub
                    End If
                Else
                    lblStatus.Text = "Không thể lấy tham số của BDS hãy kiểm tra lại."
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If


                lblStatus.Text = "Kiểm soát chứng từ thành công.Xin chờ một lát để gửi thông tin sang thuế"

                'Chuyen thong tin sang thue
                Dim blnSuccess As Boolean = True
                Try
                    'Gui thong tin Chung tu sang Hai quan
                    'CustomsService.ProcessMSG.getMSG22(v_strMa_NV, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, v_strMa_Dthu, strErrorNum, strErrorMes, v_strTransaction_id)

                    'Neu gui thong tin thanh cong
                    If strErrorNum = "0" Then
                        lblStatus.Text = "Chứng từ đã được cập nhật BDS và chuyển số liệu sang Cơ quan Hải quan thành công"
                    Else
                        blnSuccess = False
                        lblStatus.Text = "Chứng từ đã được cập nhật BDS. Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                        Exit Sub
                    End If
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ đã được cập nhật thông tin vào BDS nhưng chưa chuyển được số liệu sang Cơ quan Thuế"
                    Exit Sub
                End Try

                'CAP NHAT TRANG THAI DA CHUYEN THUE
                v_strTrangThai_CT = "01" 'Trạng thái đã ks
                ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT)

                'Insert du lieu vao bang DOICHIEU_NHHQ
                clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, v_strMa_Dthu, strErrorNum, TransactionHQ_Type.M41.ToString())
                hdr.transaction_id = v_strTransaction_id
                ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, IIf(blnSuccess, "1", "0"))

                LoadDSCT(ddlTrangThai.SelectedValue)
                EnabledButtonWhenDoTransaction(True)

            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình kiểm soát chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

 log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())
            ' Throw ex
        Finally
        End Try
    End Sub

    Protected Sub cmdINCT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdINCT.Click
        Try
            hdfInCTU.Value = "1"
        Catch ex As Exception

        End Try
    End Sub
End Class

