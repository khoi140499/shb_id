﻿Imports System.Data
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Partial Class pages_ChungTu_frmKTCoreBank
    Inherits System.Web.UI.Page


    Private Sub frmKTDuLieu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try
        End If
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub

    Private Function strWheres(ByVal strNgayLV As String) As String
        Dim strResult As String = "SELECT A.SHKB, A.NGAY_KB, A.MA_DTHU, A.NGAY_CT, A.MA_NV, A.SO_BT, A.KYHIEU_CT," & _
        " A.SO_CT, A.MA_NNTHUE, A.TEN_NNTHUE, A.TK_KH_NH, A.TTIEN PAY_IN_CTC," & _
        " A.REF_NO ORG_REF, 0 PAY_IN_BANK FROM TCS_CTU_HDR A," & _
        " (SELECT   MAX (CASE WHEN DORC = 'C' THEN ACCOUNT ELSE '' END) CR_ACCOUNT," & _
        " MAX (CASE WHEN DORC = 'D' THEN ACCOUNT ELSE '' END) DR_ACCOUNT, AMOUNT," & _
        " SUBSTR (TO_CHAR (REMARK), LENGTH (REMARK) - 13) REF_NO" & _
        " FROM(ETAX.CTC_COMPARE)" & _
        " GROUP BY AMOUNT, SUBSTR (TO_CHAR (REMARK), LENGTH (REMARK) - 13)) B" & _
        " WHERE(A.NGAY_CT = " & strNgayLV & ")" & _
        " AND A.PT_TT = '01' AND A.TRANG_THAI = '01' AND A.REF_NO = B.REF_NO AND (A.TTIEN <> B.AMOUNT)"
        Return strResult
    End Function

    Private Sub cmdKiemTra_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKiemTra.Click
        Try
            'Goi message ket xuat table
            Dim v_objCoreBank As New clsCoreBankUtil
            'If v_objCoreBank.CompareResult() Then
            '    'So sanh tren 2 table 
            '    Dim ds As New DataSet
            '    Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strWheres(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString)))
            '    If Globals.IsNullOrEmpty(dt) Then
            '        clsCommon.ShowMessageBox(Me, "Không tìm thấy chứng từ hạch toán sai.")
            '        txtTuNgay.Focus()
            '        Exit Sub
            '    Else
            '        'Bind vao grid cac du lieu sai
            '        Me.dtg.DataSource = dt
            '        Me.dtg.DataBind()
            '    End If
            'Else
            '    clsCommon.ShowMessageBox(Me, "Kết xuất table không thành công")
            'End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình kiểm tra chứng từ.")
        End Try
    End Sub
End Class
